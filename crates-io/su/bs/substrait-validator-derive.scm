(define-module (crates-io su bs substrait-validator-derive) #:use-module (crates-io))

(define-public crate-substrait-validator-derive-0.0.1 (c (n "substrait-validator-derive") (v "0.0.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02ki8z6hqxq6wi7gynqgzpx4r12hf1nkkd4gfvzvy4xnbb647g3w")))

(define-public crate-substrait-validator-derive-0.0.2 (c (n "substrait-validator-derive") (v "0.0.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vb4frd4kfwmxsx11bbd4fawqd7ha221qz04b32lbvpr5828nqr1")))

(define-public crate-substrait-validator-derive-0.0.3 (c (n "substrait-validator-derive") (v "0.0.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lc7g5grgnxv7qrz3dr0s4dbrp29i599fnvzml4v3qbyhw6xzpz1")))

(define-public crate-substrait-validator-derive-0.0.4 (c (n "substrait-validator-derive") (v "0.0.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ysfm16igyb28yr0zb6y073vqc2wddjdphibbfc2qvxyg13a4d30")))

(define-public crate-substrait-validator-derive-0.0.5 (c (n "substrait-validator-derive") (v "0.0.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w3g4hzc5c2h4rifrzfgjk99v4lz97n5av8b64v061zg4jgvx2vc")))

(define-public crate-substrait-validator-derive-0.0.6 (c (n "substrait-validator-derive") (v "0.0.6") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gzas4aw45xss3x2r7pd1g7b0v1nih3577xwi4b9ywa7c0k9m1fr")))

(define-public crate-substrait-validator-derive-0.0.7 (c (n "substrait-validator-derive") (v "0.0.7") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0brvx18b26z9n3gj37qjfqhw5m0gpjqvl2b9s3bhr609q0dhfagp")))

(define-public crate-substrait-validator-derive-0.0.8 (c (n "substrait-validator-derive") (v "0.0.8") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z0d8psi6c2id0dhzlmh4lfy3960zjngshycbd5i68785w5p3zsb")))

(define-public crate-substrait-validator-derive-0.0.9 (c (n "substrait-validator-derive") (v "0.0.9") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15mds1x52mad71sb0h621iyqiwpry6h8ic1vfbnpl5k658zhs3xw")))

(define-public crate-substrait-validator-derive-0.0.10 (c (n "substrait-validator-derive") (v "0.0.10") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cfc4fmv57kxf531jzysrdfr38brgdvjyg2y2c2mxd997iaymf13")))

(define-public crate-substrait-validator-derive-0.0.11 (c (n "substrait-validator-derive") (v "0.0.11") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n4mmhvmidgx0n82wz8rdnihf8c0f2sz1mx3f17npn578nv4d0bn")))

