(define-module (crates-io su bs substrate-test-utils) #:use-module (crates-io))

(define-public crate-substrate-test-utils-2.0.0-rc6 (c (n "substrate-test-utils") (v "2.0.0-rc6") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "substrate-test-utils-derive") (r "^0.8.0-rc6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "0m7hfb3f318mc4v3p6rdv13s46dijk4ph4x10hm3pngdx438axja")))

(define-public crate-substrate-test-utils-2.0.0 (c (n "substrate-test-utils") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "substrate-test-utils-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "09cxjh26srm0xcqjhk43v0xhpkrnnm1vgkngddzmqism5lnbag6r")))

(define-public crate-substrate-test-utils-2.0.1 (c (n "substrate-test-utils") (v "2.0.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "substrate-test-utils-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rngggbfwf8kgd15wcjdvgk5r43zknba707ifnci12b08clq8kcp")))

(define-public crate-substrate-test-utils-3.0.0 (c (n "substrate-test-utils") (v "3.0.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "substrate-test-utils-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ah8in7s8gqs6z29d051ya27hhx2gzh9abyvvxn9a8yrh6980ikg")))

