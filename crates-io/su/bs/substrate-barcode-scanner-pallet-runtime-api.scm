(define-module (crates-io su bs substrate-barcode-scanner-pallet-runtime-api) #:use-module (crates-io))

(define-public crate-substrate-barcode-scanner-pallet-runtime-api-2.0.0 (c (n "substrate-barcode-scanner-pallet-runtime-api") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0") (k 0)))) (h "11mfjam1lnap2adfn3yr85vs26zy46ijwzc58vpwmic84acrj7id") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

