(define-module (crates-io su bs subscriber-rs) #:use-module (crates-io))

(define-public crate-subscriber-rs-0.0.1 (c (n "subscriber-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fly0g7da47wwgwz11rg8hb82hjj5a67x426bbdpql1kl7ksjbn3") (f (quote (("std" "futures/std" "alloc") ("default" "std") ("alloc"))))))

