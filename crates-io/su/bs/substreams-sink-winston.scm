(define-module (crates-io su bs substreams-sink-winston) #:use-module (crates-io))

(define-public crate-substreams-sink-winston-0.1.0 (c (n "substreams-sink-winston") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "11cprpxkp2kmc8v02iqcgmlpxzb135w9jj2lva6h9nkyd8k67id8")))

(define-public crate-substreams-sink-winston-0.1.1 (c (n "substreams-sink-winston") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1w2kx88jmsqnm9lgvz2hfiy87zvp1fnwmxi54rdpkggyypkmcpd0")))

(define-public crate-substreams-sink-winston-0.1.2 (c (n "substreams-sink-winston") (v "0.1.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0gb3xp9hbcs0qww5f6c76rm4hjkg2kg4avkbmj2563n253sx80jj")))

