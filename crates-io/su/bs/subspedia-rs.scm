(define-module (crates-io su bs subspedia-rs) #:use-module (crates-io))

(define-public crate-subspedia-rs-0.1.0 (c (n "subspedia-rs") (v "0.1.0") (h "1a9pzxbnfj9sjd953h3ia2sn1vl0r9vp96mjpyag7vs49yd5cv1k")))

(define-public crate-subspedia-rs-0.1.1 (c (n "subspedia-rs") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.8") (d #t) (k 0)))) (h "1ldk7ic1rgvq0pwib7i4dz1k6xz9vb5s6cbi589mna1agwazfyrf")))

(define-public crate-subspedia-rs-0.1.2 (c (n "subspedia-rs") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.8") (d #t) (k 0)))) (h "0b5pg4x4ck3rsjhg0brg0159kpsygrqlvpb7afayn3mz3w6v7rrs")))

(define-public crate-subspedia-rs-0.1.3 (c (n "subspedia-rs") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.8") (d #t) (k 0)))) (h "0ly9i3mi3kwm9iklylrdxvsxcqqih2vfqx0vckb1fdz9lw6kssyp")))

(define-public crate-subspedia-rs-0.1.4 (c (n "subspedia-rs") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^0.1.8") (d #t) (k 0)))) (h "13k67q7hw2fhlnk1gzry7llrd1irr71w8i0swn237bnfl106j3pw")))

