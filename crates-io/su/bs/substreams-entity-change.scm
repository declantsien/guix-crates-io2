(define-module (crates-io su bs substreams-entity-change) #:use-module (crates-io))

(define-public crate-substreams-entity-change-0.1.0 (c (n "substreams-entity-change") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.3.0") (d #t) (k 0)))) (h "1y807kcd6y13yh87s882n8jvyrihbrm2a29axl425sv405ka2p4v") (r "1.60")))

(define-public crate-substreams-entity-change-0.2.0 (c (n "substreams-entity-change") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.4.0") (d #t) (k 0)))) (h "02370vdkb67gcn1ksdj28hyhh49v90z4blhm4p7fias4b12s271v") (r "1.60")))

(define-public crate-substreams-entity-change-0.2.1 (c (n "substreams-entity-change") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.4.0") (d #t) (k 0)))) (h "1msyyzghwqzjrzc075f6h4qrp2b44iv3c9nvf5j2ri1b58qzzgqc") (r "1.60")))

(define-public crate-substreams-entity-change-0.3.0 (c (n "substreams-entity-change") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "1xipi33fm687sxwll3f32208ff4ca9vfq8f88dbqqhzv9rwv07y8") (r "1.60")))

(define-public crate-substreams-entity-change-1.0.0 (c (n "substreams-entity-change") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "1fgfgvairz0p4hagzdl6pyfm86vd4dd3g2xg2qypzldfkdmak471") (r "1.60")))

(define-public crate-substreams-entity-change-1.1.0 (c (n "substreams-entity-change") (v "1.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "00jkl3wbrxjnm8cs94dxafiv6zpq77haza16nvas5w7ar7df1ynm") (r "1.63")))

(define-public crate-substreams-entity-change-1.2.0 (c (n "substreams-entity-change") (v "1.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "0lp6yrpwj6lm1hlqpyylm1s3m8fnb49jjqrqc6ppwy6wg6fj7whx") (r "1.63")))

(define-public crate-substreams-entity-change-1.2.1 (c (n "substreams-entity-change") (v "1.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "03yadpblr7mj90315wmkq89v496p3r2rrq8hz6rigwkmdvrv83mn") (r "1.63")))

(define-public crate-substreams-entity-change-1.2.2 (c (n "substreams-entity-change") (v "1.2.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "1fqlbhijcpfg290synx7apm888i3ny3wskld3pxf9h00pmcf4zmz") (r "1.63")))

(define-public crate-substreams-entity-change-1.3.0 (c (n "substreams-entity-change") (v "1.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "17l8nd7sdkvd3alqn2anj1gxbhzykfy8sa7cskba714j5b0x08yl")))

(define-public crate-substreams-entity-change-1.3.1 (c (n "substreams-entity-change") (v "1.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "1h13x9awghqmx0mml7n7jlwnb86d3rkh6gmhcp5ihdfmgf8j2x70")))

(define-public crate-substreams-entity-change-1.3.2 (c (n "substreams-entity-change") (v "1.3.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "0z8hz4c0v3a81hk98b4iksi59aazc1dxlc7d2nfnbzxb4fhzriz2")))

