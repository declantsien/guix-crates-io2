(define-module (crates-io su bs substrate-spec-version) #:use-module (crates-io))

(define-public crate-substrate-spec-version-0.1.0 (c (n "substrate-spec-version") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "subxt") (r "^0.35.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13h98wvyzj8wa27d38vfk63q0hzn1rc7zyd712w3l4xca7qy4pzp")))

