(define-module (crates-io su bs substreams-antelope) #:use-module (crates-io))

(define-public crate-substreams-antelope-0.1.0 (c (n "substreams-antelope") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "11hifxvkrdbx1gkvh8ffixnkj5cxaf93137kys4657njqs4g5rhf")))

(define-public crate-substreams-antelope-0.1.1 (c (n "substreams-antelope") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0jyi3fcmh50d47y2wl9d273wr8blrrkn5myw339hfpiwfl2zx20x")))

(define-public crate-substreams-antelope-0.3.0 (c (n "substreams-antelope") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "^0.3.0") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "^0.3.0") (d #t) (k 0)))) (h "04w945rjwmgpmrh0ddk43hlxpan3vj9zby83ikyzx2gq8mi07jl3") (r "1.69")))

(define-public crate-substreams-antelope-0.3.1 (c (n "substreams-antelope") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "1ncr72dhyd0rx8rxkfhax8vi9y84zigjhrf3dalwka42h6vwjfnd") (r "1.69")))

(define-public crate-substreams-antelope-0.3.2 (c (n "substreams-antelope") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "1hvnm6r027i69ikzv4ndrx5698a42hm3smyfwwa0mmp9rgf9lgmi") (r "1.69")))

(define-public crate-substreams-antelope-0.3.3 (c (n "substreams-antelope") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "0kycch8avi08w6dy3rlvgk9pqslgaz2jys3fg9zqb3qwq46xii72") (r "1.75")))

(define-public crate-substreams-antelope-0.3.4 (c (n "substreams-antelope") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "1a77sha22s7w63f8441wzvaqj1iz73f0lzsc0h2rdp7hryjv7gj4") (r "1.75")))

(define-public crate-substreams-antelope-0.4.0 (c (n "substreams-antelope") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "036cmpik6w3nn0gc2jawh01z30nasqpk1rl6cbf6jacaiqapmmqv") (r "1.75")))

(define-public crate-substreams-antelope-0.4.1 (c (n "substreams-antelope") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "02p9813adqgzg6a0h0g68q6c6cwiwxx0avrsy6583dbsiayprk9v") (r "1.75")))

(define-public crate-substreams-antelope-0.3.5 (c (n "substreams-antelope") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-antelope-abigen") (r "0.*") (d #t) (k 0)) (d (n "substreams-antelope-core") (r "0.*") (d #t) (k 0)))) (h "02cc8b66hz29g3cqqsd2hng4bsa773p09s0gd1im1p9higapgapy") (r "1.75")))

