(define-module (crates-io su bs subsetter) #:use-module (crates-io))

(define-public crate-subsetter-0.1.0 (c (n "subsetter") (v "0.1.0") (d (list (d (n "ttf-parser") (r "^0.15") (d #t) (k 2)))) (h "061wm8papq6vab06wnmqi8wfghs605yhp1rc3plmg9qymgfasrbr")))

(define-public crate-subsetter-0.1.1 (c (n "subsetter") (v "0.1.1") (d (list (d (n "ttf-parser") (r "^0.15") (d #t) (k 2)))) (h "1ygjaz4nd8m5k5h966s3i3wqgiy78nz5jk5x00ibm2gz7flbish9")))

