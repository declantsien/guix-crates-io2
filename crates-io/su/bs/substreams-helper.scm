(define-module (crates-io su bs substreams-helper) #:use-module (crates-io))

(define-public crate-substreams-helper-0.0.1 (c (n "substreams-helper") (v "0.0.1") (h "0bj8rmr7znwc8k406874vxkinvf23fl4d9pdmsszp6wpri0c8237")))

(define-public crate-substreams-helper-0.0.2 (c (n "substreams-helper") (v "0.0.2") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "ethabi") (r "^17.0") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "pad") (r "^0.1") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "prost") (r "^0.10.1") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "substreams-ethereum") (r "^0.1.9") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "18fq5jv41npandvpjws8g6frbyq82xgyp2ad857s6fbj87gxga2a")))

