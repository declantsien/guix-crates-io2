(define-module (crates-io su bs substreams-bitcoin) #:use-module (crates-io))

(define-public crate-substreams-bitcoin-1.0.0 (c (n "substreams-bitcoin") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-bitcoin-core") (r "^1.0.0") (d #t) (k 0)))) (h "0562j1h5q29cws2xmwzl0n5sb5c4cf956fbkysccqmkarbanik1s") (r "1.60")))

