(define-module (crates-io su bs subsee) #:use-module (crates-io))

(define-public crate-subsee-0.1.0 (c (n "subsee") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "frame-metadata") (r "^14.0.0-rc.1") (f (quote ("v12" "v13" "v14" "std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "scale") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1r3mj6zmnv0wkgv0q70bj0srrmnbxyvajmdxd15wsd9d8bc3y7mj")))

