(define-module (crates-io su bs substreams-sink-prometheus) #:use-module (crates-io))

(define-public crate-substreams-sink-prometheus-0.1.0 (c (n "substreams-sink-prometheus") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1s303w4s63kz9yqgzbdc5bv39ak38m5pm8iijryhhymh4qn3iivf")))

(define-public crate-substreams-sink-prometheus-0.1.1 (c (n "substreams-sink-prometheus") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1pilzwjli1dn5f3gbr1yq34ip6hqng6wvlachwgsbqzch5scl022")))

(define-public crate-substreams-sink-prometheus-0.1.2 (c (n "substreams-sink-prometheus") (v "0.1.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1v9d8221f79xxdmqjkqjwmxs9zn1ai8c3hxrhpk52ridpgwfh4lb")))

(define-public crate-substreams-sink-prometheus-0.1.3 (c (n "substreams-sink-prometheus") (v "0.1.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1jmip8lsp49s3vq01ba5g1h5f3mzcakwd974mbbsz6jjqh6lsg5p")))

(define-public crate-substreams-sink-prometheus-0.1.4 (c (n "substreams-sink-prometheus") (v "0.1.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0z7m86l3p9vkx2q2wzl4fkl2728n1frxz4826sg9dm5hl5cdx7d8")))

(define-public crate-substreams-sink-prometheus-0.1.5 (c (n "substreams-sink-prometheus") (v "0.1.5") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0yh81kgc4fscqhxxhsj0j2bx6vpsnvfd643v7j3jgnw63ms6p9v6")))

(define-public crate-substreams-sink-prometheus-0.1.6 (c (n "substreams-sink-prometheus") (v "0.1.6") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0qrrrn3hq60gqcka2vby2vs5z0d80vvjp0wx5ipkzilz5a1zxfva")))

(define-public crate-substreams-sink-prometheus-0.1.7 (c (n "substreams-sink-prometheus") (v "0.1.7") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1csx4qxpcl2hal9w606l3frzzjxprgjfk3mi93l9vg22slwdrp9z")))

(define-public crate-substreams-sink-prometheus-0.1.8 (c (n "substreams-sink-prometheus") (v "0.1.8") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1cwi7730sxfbv3r97g9vzhzajlikd3xa117v7vb40kavi47k39pz")))

(define-public crate-substreams-sink-prometheus-0.1.9 (c (n "substreams-sink-prometheus") (v "0.1.9") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0076ix77kwla14r96lamm2j6pc7m87rcbzpnmz6wggi1w0b3s7wv")))

