(define-module (crates-io su bs substrate-keybase-keystore) #:use-module (crates-io))

(define-public crate-substrate-keybase-keystore-0.2.0 (c (n "substrate-keybase-keystore") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "keybase-keystore") (r "^0.2.0") (d #t) (k 0)) (d (n "substrate-subxt") (r "^0.10.1") (d #t) (k 0)) (d (n "sunshine-core") (r "^0.2.0") (d #t) (k 0)))) (h "1yill7ac0p2z0z3py70kk9fnp83dghpzps3nk9sqi8vxm23c8bn3")))

