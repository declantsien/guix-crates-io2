(define-module (crates-io su bs substrate-test-utils-derive) #:use-module (crates-io))

(define-public crate-substrate-test-utils-derive-0.8.0-rc6 (c (n "substrate-test-utils-derive") (v "0.8.0-rc6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0agzm558yzfhwxdsy4jnp8a2hzi3ha7cjggfn4vrkfzzygzvnir0")))

(define-public crate-substrate-test-utils-derive-0.8.0 (c (n "substrate-test-utils-derive") (v "0.8.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "17isz5cijc7c1282736la7janaa1sd0z9v635yjcjxsny3s91gi6")))

(define-public crate-substrate-test-utils-derive-0.8.1 (c (n "substrate-test-utils-derive") (v "0.8.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "05zimpca6bl7nrvbxq6p0pxymnidx9pmn2rbq1y2kyj81qqgpiy9")))

(define-public crate-substrate-test-utils-derive-0.9.0 (c (n "substrate-test-utils-derive") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "03hszpgdsy0jd03ks02v2jig3gyvwzgakpwprzz67iyg9751d04q")))

