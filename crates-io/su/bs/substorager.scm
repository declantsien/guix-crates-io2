(define-module (crates-io su bs substorager) #:use-module (crates-io))

(define-public crate-substorager-0.1.0 (c (n "substorager") (v "0.1.0") (d (list (d (n "array-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^1.3.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.0") (d #t) (k 0)))) (h "1pn9sxhvg3cn3d1nncxdzrxb4lfwhmgclmilyyx5n7b7k0s6aznf") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.1 (c (n "substorager") (v "0.1.1") (d (list (d (n "array-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^1.3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.0") (d #t) (k 0)))) (h "1bcc3vjjd2h2b83bl0545ab46gb01lpvs2isl5l9jfyr4cl8y4ln") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.2 (c (n "substorager") (v "0.1.2") (d (list (d (n "array-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^1.3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.1") (d #t) (k 0)))) (h "0rq2vjxmkni4cs8hgrma76z9rkjq2d19wvmvray2cqpyrpb01i41") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.3 (c (n "substorager") (v "0.1.3") (d (list (d (n "array-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.1") (d #t) (k 0)))) (h "04swpmr9f8z82iv9isyw6h9s9bbljjsxzrmqf742hnr8avzh72h2") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.4 (c (n "substorager") (v "0.1.4") (d (list (d (n "array-bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.1") (d #t) (k 0)))) (h "0nx14i1r5sf8bl6f8956vw0d49x88nrgbiyvjnkyln0rlq4pa8rh") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.5 (c (n "substorager") (v "0.1.5") (d (list (d (n "array-bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.1") (d #t) (k 0)))) (h "0fmlbvrxw6z4jqng98g1zrhfa0yqxlmk9hjz30s8y2jds35d80r9") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.6 (c (n "substorager") (v "0.1.6") (d (list (d (n "array-bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.2") (d #t) (k 0)))) (h "0dvr0b6k6k1k8lb46dpi0mqsc363598f1p8l5ffkmy7m6kv0909p") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.7 (c (n "substorager") (v "0.1.7") (d (list (d (n "array-bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.3") (d #t) (k 0)))) (h "1wfzi3avma4mniqiwcslpc17y2kmmrqv2qi5qdnfl1ss970wb259") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.1.8 (c (n "substorager") (v "0.1.8") (d (list (d (n "array-bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.3") (d #t) (k 0)))) (h "10ymmjg0q6vn49cwwn55gcgk4rsrcalgjh2irai21v6vq7rza04k") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.2.0 (c (n "substorager") (v "0.2.0") (d (list (d (n "array-bytes") (r "^1.3.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.7") (d #t) (k 0)))) (h "03y3rahd16324dq91sfzvdm04yvy0j6fm34adv48v8frj6vr4lp2") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.3.0 (c (n "substorager") (v "0.3.0") (d (list (d (n "array-bytes") (r "^1.3.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1.7") (d #t) (k 0)))) (h "1jsy8bs661jivvzfidl91vk9k74b2ackwbmnin1dvgzrf026q8gv") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.3.1 (c (n "substorager") (v "0.3.1") (d (list (d (n "array-bytes") (r "^1.4") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.1") (d #t) (k 0)))) (h "0b6c013dspnz5y8ixkw6pmhnkmmi7km4p01yi87k5r71whagwssp") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.7.0 (c (n "substorager") (v "0.7.0") (d (list (d (n "array-bytes") (r "^1.5") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.7") (d #t) (k 0)))) (h "11msf6i8mjv4qnzbqvpqq2q95si6039fl9vph7gh5cw2i5sjjjzp") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc13 (c (n "substorager") (v "0.9.0-rc13") (d (list (d (n "array-bytes") (r "^4.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc13") (d #t) (k 0)))) (h "0m1k7w0s41xcw335x29scwx38sadnwqaayjrkz2gkiw7a3jwphcr") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc14 (c (n "substorager") (v "0.9.0-rc14") (d (list (d (n "array-bytes") (r "^4.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc14") (d #t) (k 0)))) (h "0l0hjvpadzcvx92kfkip7pb8jv596sm3a6azx99qnkx1sr3yk1mb") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc15 (c (n "substorager") (v "0.9.0-rc15") (d (list (d (n "array-bytes") (r "^4.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc15") (d #t) (k 0)))) (h "0fdmf5h9qc8ndsqga3y7q277ph495d62js6lfxbd5r6g1rb2nfs0") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc16 (c (n "substorager") (v "0.9.0-rc16") (d (list (d (n "array-bytes") (r "^4.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc16") (d #t) (k 0)))) (h "1c2bmxa78p8ws8p8b1xanggwcpbmygjcfpk5fg1an356iifbk4in") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc17 (c (n "substorager") (v "0.9.0-rc17") (d (list (d (n "array-bytes") (r "^4.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc17") (d #t) (k 0)))) (h "0d60cjg339rj8b31w6a8ma88xikjfcmk3chbwlfcxwn1r9clhjl5") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc18 (c (n "substorager") (v "0.9.0-rc18") (d (list (d (n "array-bytes") (r "^4.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc18") (d #t) (k 0)))) (h "0qlwjbgqmzv1bj6w8v5h578mb4lmmvmy2b3118dq0dg1q3wmxh50") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc19 (c (n "substorager") (v "0.9.0-rc19") (d (list (d (n "array-bytes") (r "^4.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc19") (d #t) (k 0)))) (h "1b1sfw1wbkd319prycn3f394h3kc7456wimp3j9hif0lj6yyi6bi") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0-rc20 (c (n "substorager") (v "0.9.0-rc20") (d (list (d (n "array-bytes") (r "^6.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0-rc20") (d #t) (k 0)))) (h "0nwcg9vqhv70wg8684g0i9lj4wdjqpmb4n7xxl1masgdrjs0nv56") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.0 (c (n "substorager") (v "0.9.0") (d (list (d (n "array-bytes") (r "^6.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.0") (d #t) (k 0)))) (h "0110vg047cgfgz0iv2xp2q0ydlgqc024xbq0wd9gzdmzvp1f4nz4") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.1 (c (n "substorager") (v "0.9.1") (d (list (d (n "array-bytes") (r "^6.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.1") (d #t) (k 0)))) (h "18hmr9br0dnv6lvpd9gylf18x8xscvi5kb0prlljnvajj9gfjw7s") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.9.2 (c (n "substorager") (v "0.9.2") (d (list (d (n "array-bytes") (r "^6.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.9.1") (d #t) (k 0)))) (h "0mglqk35a6q4dcif81nvrkqcqkvfqzwxj5r1bibz5645gc0qk3z4") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.10.0 (c (n "substorager") (v "0.10.0") (d (list (d (n "array-bytes") (r "^6.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.10") (d #t) (k 0)))) (h "1cgyfkclqzl17nyfp02nigdqy3s7d2irsz61xmxwczrxbzxdl752") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.10.1 (c (n "substorager") (v "0.10.1") (d (list (d (n "array-bytes") (r "^6.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.10") (d #t) (k 0)))) (h "1v8a9ysgp3qs5y2s69ck94bj9ixm5jax7h9j4b8pmsrcfkylzif0") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-substorager-0.11.0 (c (n "substorager") (v "0.11.0") (d (list (d (n "array-bytes") (r "^6.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subhasher") (r "^0.10") (d #t) (k 0)))) (h "1bz5g5sgpbi1k4w1f5sllrfds54vcg86ipdh11fp4asn300qgb7v") (f (quote (("codec" "parity-scale-codec"))))))

