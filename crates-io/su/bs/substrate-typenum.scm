(define-module (crates-io su bs substrate-typenum) #:use-module (crates-io))

(define-public crate-substrate-typenum-1.16.0 (c (n "substrate-typenum") (v "1.16.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive" "max-encoded-len"))) (o #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1257x3bya84y2kfqmdhzh70659mq5k2j957378impiy2jcg0kw7h") (f (quote (("strict") ("no_std") ("i128") ("force_unix_path_separator") ("derive_scale" "scale-info" "parity-scale-codec"))))))

