(define-module (crates-io su bs substreams-solana) #:use-module (crates-io))

(define-public crate-substreams-solana-0.1.0 (c (n "substreams-solana") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "substreams") (r "^0.0.12") (d #t) (k 0)))) (h "0hb24bjicgicshmplvzj0mcwg6zh68diswkz335cj0hkigvbn7x4") (r "1.60")))

(define-public crate-substreams-solana-0.2.0 (c (n "substreams-solana") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1rysav6dy5iyb7fp4j747vnflwickg3fnvd0kir97bxqci5avrfm") (r "1.64")))

(define-public crate-substreams-solana-0.10.1 (c (n "substreams-solana") (v "0.10.1") (d (list (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-solana-core") (r "^0.10.1") (d #t) (k 0)) (d (n "substreams-solana-macro") (r "^0.10.1") (d #t) (k 0)))) (h "10hlw71n6kv6y7pcwhi074f00x4m7jgdki06yhwynzvzw8y8jvk2") (r "1.60")))

(define-public crate-substreams-solana-0.10.2 (c (n "substreams-solana") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)) (d (n "substreams-solana-core") (r "^0.10.2") (d #t) (k 0)) (d (n "substreams-solana-macro") (r "^0.10.2") (d #t) (k 0)))) (h "0fwzdhazsv3ffp2s02pw1688nalx0icpx83jxvizf3dqnxab7q9i") (r "1.60")))

(define-public crate-substreams-solana-0.11.1 (c (n "substreams-solana") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)) (d (n "substreams-solana-core") (r "^0.11.1") (d #t) (k 0)) (d (n "substreams-solana-macro") (r "^0.11.1") (d #t) (k 0)))) (h "07b07hcn97vp3bx0li5h3apy2kvdhy9vd99jnxk8dgyd62l0wwgx") (r "1.60")))

