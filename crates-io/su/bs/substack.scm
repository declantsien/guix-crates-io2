(define-module (crates-io su bs substack) #:use-module (crates-io))

(define-public crate-substack-1.0.0 (c (n "substack") (v "1.0.0") (h "0jm6gj4abmd2rgn6zz0dj2hkc7qrziw81ax7pf9kway2bl2nhnlx")))

(define-public crate-substack-1.0.1 (c (n "substack") (v "1.0.1") (h "0bdsjchkmxgivvi4m4vg6i51s4z7x15xsg5y25alibwfyypkaaa4")))

(define-public crate-substack-1.1.0 (c (n "substack") (v "1.1.0") (h "0zsfak5017l80hvwznn6w5rvkasjp0dg6kx7gbk9sj0a1zcc7k7z")))

