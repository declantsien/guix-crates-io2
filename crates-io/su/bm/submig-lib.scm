(define-module (crates-io su bm submig-lib) #:use-module (crates-io))

(define-public crate-submig-lib-0.0.0 (c (n "submig-lib") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1wlyl1hs3hsnrcdjhjn2s9p2q2jjxj4wyavz3nz8yjcidjnyr64j")))

(define-public crate-submig-lib-0.0.1 (c (n "submig-lib") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1r2qf862v8b4bm3mqvpyasnlrsbfp67w7d2pzam66d5jvk10rr1r")))

(define-public crate-submig-lib-0.0.2 (c (n "submig-lib") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01d500d0llvk7pyd9gv8chinffrwg3ylq8sd4yzs7bhx2iq8f97q")))

(define-public crate-submig-lib-0.0.3 (c (n "submig-lib") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yrk1v7x32gc2x18bbrljykaif7sa1p3w09wki064l8p83cg76nr")))

(define-public crate-submig-lib-0.0.4 (c (n "submig-lib") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qwzcnla0g8sdcxgafrwpxfhyglnzwhx74h8cs62sgak0mpb7984")))

