(define-module (crates-io su bm submetadatan) #:use-module (crates-io))

(define-public crate-submetadatan-0.1.1 (c (n "submetadatan") (v "0.1.1") (d (list (d (n "parity-scale-codec") (r "^1.3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (o #t) (d #t) (k 0)))) (h "1n2g1ngf16q1fk0phy00q9kw040mq2j9djwwykb5v7fyc9wphs0j") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec")))) (y #t)))

(define-public crate-submetadatan-0.1.2 (c (n "submetadatan") (v "0.1.2") (d (list (d (n "parity-scale-codec") (r "^1.3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (o #t) (d #t) (k 0)))) (h "149xm8z6h438yrsav5hq4zqv3gpsqfd47yif2w85hv9722rd30a8") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.1.3 (c (n "submetadatan") (v "0.1.3") (d (list (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (o #t) (d #t) (k 0)))) (h "1ygxlzwxrl5hmgndx3sj0b02nd14jy5rj06mciwrr4in3gs34l8x") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec")))) (y #t)))

(define-public crate-submetadatan-0.1.4 (c (n "submetadatan") (v "0.1.4") (d (list (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (o #t) (d #t) (k 0)))) (h "0b8rmjl4nqksi5dvm4lh1s2n492d7ncs4hrr7sgjslfhks8bhw4f") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.1.5 (c (n "submetadatan") (v "0.1.5") (d (list (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "0mp659mhbcf97r3l5rhsv26slznfl7dx266j3v8p4rj5gkyvl8n9") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.1.6 (c (n "submetadatan") (v "0.1.6") (d (list (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "0x941bgj69gw9ckqbhfcqbzv70d7ib6mbckckznkc2yi3fb4hp5r") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.1.7 (c (n "submetadatan") (v "0.1.7") (d (list (d (n "parity-scale-codec") (r "^2.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "1jgv2191g9rgxmrh96hcsj14qar0fp1wnadrdvb54pymbn327d10") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.2.0 (c (n "submetadatan") (v "0.2.0") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "0qg9ddqcf9x0pfrplx0n1glnhcqkyfa553mkc5jbfpx0p1f12vjm") (f (quote (("simplify-metadata" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.3.0 (c (n "submetadatan") (v "0.3.0") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "11gzy613arlfv1j44hajl1z7mds0pqaapb2xg1jf5412nm5cygzc") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.3.1 (c (n "submetadatan") (v "0.3.1") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "0qygqan9kfjz56rhl29r954szp3n3g922jyz3gjxk3hbhvkk41yh") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.3.2 (c (n "submetadatan") (v "0.3.2") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "1pr93arhvaca3134p73in6qnirqr0g5grpv6n3wsasi7m3ss1b8g") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.3.3 (c (n "submetadatan") (v "0.3.3") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "0zxind7c41xjijvb5dbncph9rsyz6sd4j9amjms3msa422zhppzr") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.4.0 (c (n "submetadatan") (v "0.4.0") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "1fampcxkmw256l7kp5zcsb4niaxm8lq2kaw6dg16b3djf9ywnxmp") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.5.0 (c (n "submetadatan") (v "0.5.0") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "0dymqsfammb9dk7wir9m0mkdhn3jpndiy4dwwnkmlz5l7rrfjhyc") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec")))) (y #t)))

(define-public crate-submetadatan-0.5.1 (c (n "submetadatan") (v "0.5.1") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (o #t) (d #t) (k 0)))) (h "15c6jd3a1ambpxxnvrjgmlv212rqjj08pklqahqmf0lf1vp2ncvq") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.5.2 (c (n "submetadatan") (v "0.5.2") (d (list (d (n "parity-scale-codec") (r "^2.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (o #t) (d #t) (k 0)))) (h "0hli459d5cw0lb5qxlky2b9q32nz5rigzxiahpn5pdbyfjh168wj") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.5.3 (c (n "submetadatan") (v "0.5.3") (d (list (d (n "parity-scale-codec") (r "^2.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (o #t) (d #t) (k 0)))) (h "07156s3larkaglj36pybq14qln3mqgi8zd33mb4gdx9lagya87ij") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.5.4 (c (n "submetadatan") (v "0.5.4") (d (list (d (n "parity-scale-codec") (r "^2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "substorager") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pxl1vxgwa6jjzph8fh15zrf00kqg9njvzxvvjlhbzdsry20wfr8") (f (quote (("simple" "thiserror") ("codec" "parity-scale-codec" "substorager/codec"))))))

(define-public crate-submetadatan-0.7.0 (c (n "submetadatan") (v "0.7.0") (d (list (d (n "frame-metadata") (r "^14.2") (f (quote ("v14"))) (d #t) (k 0)) (d (n "scale-info") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jqrrv4kj4lv5yal1afywmlvskx7bm1gjwkkgf844h08d24f8r1n")))

(define-public crate-submetadatan-0.9.0-rc13 (c (n "submetadatan") (v "0.9.0-rc13") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qi3d2ar23rgwjcc5ykc1lgr4q0xcsfj0lpkin867g2kvfh9swsa")))

(define-public crate-submetadatan-0.9.0-rc14 (c (n "submetadatan") (v "0.9.0-rc14") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nqrndca4vlmw465pkhq1ri0m8prsn6s0hfk3cz9jicvhyg8h1bg")))

(define-public crate-submetadatan-0.9.0-rc15 (c (n "submetadatan") (v "0.9.0-rc15") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d0q9rg8yni18cgcl4dkh33bk258fpws7azq6l21q54y37kb0ibl")))

(define-public crate-submetadatan-0.9.0-rc16 (c (n "submetadatan") (v "0.9.0-rc16") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f5vdkrxj07ngafdcjkz64l3azrnxbfj5k9kl5zj90y01vm5l9yr")))

(define-public crate-submetadatan-0.9.0-rc17 (c (n "submetadatan") (v "0.9.0-rc17") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vfl75r9f1agzr34zmgqq46as9wnmd5a4wbn5b138whgbdahgy7a")))

(define-public crate-submetadatan-0.9.0-rc18 (c (n "submetadatan") (v "0.9.0-rc18") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gg8hhjrkjvjdxb0sbzp48w9qw0lam3lrx2rabqk84wdvbdmkxiv")))

(define-public crate-submetadatan-0.9.0-rc19 (c (n "submetadatan") (v "0.9.0-rc19") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l9xxrn77mq78w9sckqdp4fzdnqg9mcgqrlf9a834m6y601fdyhl")))

(define-public crate-submetadatan-0.9.0-rc20 (c (n "submetadatan") (v "0.9.0-rc20") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "024h3psd56yl6p8s9pz0h9iyycs1i1s2j3nxz43avspi2m249yvv")))

(define-public crate-submetadatan-0.9.0 (c (n "submetadatan") (v "0.9.0") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ksacaiwxi7zdkyvfyhdam399h48yrqy2pynfvsjcnfcz4gsn7il")))

(define-public crate-submetadatan-0.9.1 (c (n "submetadatan") (v "0.9.1") (d (list (d (n "frame-metadata") (r "^15.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hf7m8gx36ppkm0w8av14cvcgnmm2b3j9gsd00h19jcx83hz71b7")))

(define-public crate-submetadatan-0.9.2 (c (n "submetadatan") (v "0.9.2") (d (list (d (n "frame-metadata") (r "^15.1") (d #t) (k 0)) (d (n "scale-info") (r "^2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y5cg4xr078w488180nm02fd7l1skj51i6z9kgxpi6q28vzdqq5r")))

(define-public crate-submetadatan-0.10.0 (c (n "submetadatan") (v "0.10.0") (d (list (d (n "array-bytes") (r "^6.1") (d #t) (k 0)) (d (n "frame-metadata") (r "^15.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.4") (d #t) (k 0)) (d (n "scale-info") (r "^2.5") (d #t) (k 0)) (d (n "substorager") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "089lw04n00y75bgcdy71p6plpvrckdwrlldpd69zk0i836iv3grm") (f (quote (("cmp"))))))

(define-public crate-submetadatan-0.10.1 (c (n "submetadatan") (v "0.10.1") (d (list (d (n "array-bytes") (r "^6.1") (d #t) (k 0)) (d (n "frame-metadata") (r "^15.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.5") (d #t) (k 0)) (d (n "scale-info") (r "^2.6") (d #t) (k 0)) (d (n "substorager") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z3lag6hw6c5zy11d1cngmsvbcy6c3i0zzwb8dlx2xplk2v5qr3g") (f (quote (("cmp"))))))

(define-public crate-submetadatan-0.11.0 (c (n "submetadatan") (v "0.11.0") (d (list (d (n "array-bytes") (r "^6.2") (d #t) (k 0)) (d (n "frame-metadata") (r "^15.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.6") (d #t) (k 0)) (d (n "scale-info") (r "^2.11") (d #t) (k 0)) (d (n "substorager") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cpvd07db3mzw3hd4n4nm45bf0myk1varffkr47abswxaqvfwkb0") (f (quote (("cmp"))))))

