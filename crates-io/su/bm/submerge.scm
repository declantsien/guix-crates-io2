(define-module (crates-io su bm submerge) #:use-module (crates-io))

(define-public crate-submerge-0.1.0 (c (n "submerge") (v "0.1.0") (h "1i2wappyzajv1iarzw8nq9qc16qbw9j1ldkzlwbs9znlg30i5sjf")))

(define-public crate-submerge-0.2.0 (c (n "submerge") (v "0.2.0") (d (list (d (n "brevet") (r "^0.4") (d #t) (k 0)) (d (n "clepsydra") (r "^0.2") (d #t) (k 0)) (d (n "newel") (r "^0.2") (d #t) (k 0)) (d (n "redb") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0xr99srggzxhryr7007xzws7rn1lv1lnjkb6rl5wlvqapzckfcfk")))

