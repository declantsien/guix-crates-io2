(define-module (crates-io su bm submig) #:use-module (crates-io))

(define-public crate-submig-0.0.0 (c (n "submig") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13davbxpxjds6h70vbh4bdcvhx47rg46h0sx1g0qdvm5nazfgwnj")))

(define-public crate-submig-0.0.1 (c (n "submig") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "submig-lib") (r ">=0") (d #t) (k 0)))) (h "02ga1hmai7hmz82vcqnn6avfsvz9n71wy22dgw7622q3mpz0513x")))

(define-public crate-submig-0.0.2 (c (n "submig") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "submig-lib") (r ">=0") (d #t) (k 0)))) (h "02z7vs32q8bxj3mf3x10c51q7pkbwkxr6f2hlzjfdcmhzp8aqn8v")))

(define-public crate-submig-0.0.3 (c (n "submig") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "submig-lib") (r ">=0") (d #t) (k 0)))) (h "1nzwrzwpibczqxw4gv0f5ycg021yyibbr94vmyf2gbjpm7wh4i8l")))

(define-public crate-submig-0.0.4 (c (n "submig") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "submig-lib") (r ">=0") (d #t) (k 0)))) (h "1xm643vs2nd225s4q01vnx2vlqm8mipvxz9yyiispx900vxp3nhm")))

