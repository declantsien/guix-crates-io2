(define-module (crates-io su bm submeta) #:use-module (crates-io))

(define-public crate-submeta-0.1.0 (c (n "submeta") (v "0.1.0") (d (list (d (n "array-bytes") (r "^6.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "frame-metadata") (r "^15.0.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.3.0") (k 0)))) (h "13i9ljwvjyc0fmz8ngg9j099cmxgj9ddlzypyjyjh5zpnz5p6v37")))

