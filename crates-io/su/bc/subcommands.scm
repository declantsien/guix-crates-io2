(define-module (crates-io su bc subcommands) #:use-module (crates-io))

(define-public crate-subcommands-0.1.0 (c (n "subcommands") (v "0.1.0") (h "05lrqq3q5xz6iwvccs7f0w4gqyyri9rd0i6fl6jvdbb9wwmj939f")))

(define-public crate-subcommands-0.1.1 (c (n "subcommands") (v "0.1.1") (h "0iydvb233a9xipkqkc4d5mfr96yxvk1gz5lzz0skwqyhfc009z76")))

(define-public crate-subcommands-0.2.0 (c (n "subcommands") (v "0.2.0") (h "1hq6dmkij7rzfqmi1zkfbi8g46l3cgb0a046nlvq7yya0q5g16ya")))

