(define-module (crates-io su bc subcomponent) #:use-module (crates-io))

(define-public crate-subcomponent-0.1.0 (c (n "subcomponent") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1s5kl9xf17yxh04gxl7r3dc5x03a3hxqvn8a1hynybrfsdpx28ai")))

