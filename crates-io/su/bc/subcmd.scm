(define-module (crates-io su bc subcmd) #:use-module (crates-io))

(define-public crate-subcmd-0.0.1 (c (n "subcmd") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1ydb73qhmgi4m91mvl317sp8y6r98gpvcs5zp5v4r3h1kr7h397a")))

(define-public crate-subcmd-0.0.2 (c (n "subcmd") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "128hz8bwgjbc8gzaqs9i577iy8xnx6mn27wkjr5rzr4anc4i9wvx")))

(define-public crate-subcmd-0.1.0 (c (n "subcmd") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.5.1") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "1j4d6qdcabklcsvm22w4lk0wwb9p9h5s604yjggyajg3m9g6s8v4")))

