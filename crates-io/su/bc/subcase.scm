(define-module (crates-io su bc subcase) #:use-module (crates-io))

(define-public crate-subcase-0.1.0 (c (n "subcase") (v "0.1.0") (h "1sw8wrclmpffkjxp28fh756v96vv6lyjxs42kw1ypfy46fjhhdxk") (y #t) (r "1.60")))

(define-public crate-subcase-0.1.1 (c (n "subcase") (v "0.1.1") (h "07pcmwdpc0nz45rvwby9fkaqc7zjx72mcbd8wx5qyf6p5kzl9k6y") (r "1.56.1")))

(define-public crate-subcase-0.2.0 (c (n "subcase") (v "0.2.0") (h "06k36r78nk0h96d768s4vsb4sinv98y727pspdky28ha31r7xkxb") (r "1.56.1")))

(define-public crate-subcase-0.2.1 (c (n "subcase") (v "0.2.1") (h "15ch0pq88jn0kfpv2sb95yv8cczz4gjal3q1124pwhlinnq6bg7b") (r "1.56.1")))

(define-public crate-subcase-0.2.2 (c (n "subcase") (v "0.2.2") (h "069axjsyklsi5aasg80nfq6i30khqw7zb1vv0nbxlj212qz10j8y") (r "1.56.1")))

(define-public crate-subcase-0.3.0 (c (n "subcase") (v "0.3.0") (h "1f3cfsy0yhvmh2rilq1m0b3fmhlqzhhxzqzflr9fdg636l0hw5mr") (r "1.56.1")))

(define-public crate-subcase-0.4.0 (c (n "subcase") (v "0.4.0") (h "1ri63ys5j277jq840vj3i2kjwmkq86kdvpaccyy24x27fj4l5x9w") (r "1.59.0")))

