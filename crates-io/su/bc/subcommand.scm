(define-module (crates-io su bc subcommand) #:use-module (crates-io))

(define-public crate-subcommand-0.1.0 (c (n "subcommand") (v "0.1.0") (h "145n60mrbqr71x26lxxx8p1w9qsfhff4ijx1ghq159z90s1165bi")))

(define-public crate-subcommand-0.1.1 (c (n "subcommand") (v "0.1.1") (h "0qf6nj45lrqbm2rs8hvni7b8pjkm1k5pz81ipdz5hrn0w06ka66z")))

(define-public crate-subcommand-0.1.2 (c (n "subcommand") (v "0.1.2") (h "10skfwl41y6rdv8jl8g40s3mv8xqqcjfhmi2zd69fq1xyxikv60j")))

