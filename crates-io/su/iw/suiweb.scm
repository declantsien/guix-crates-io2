(define-module (crates-io su iw suiweb) #:use-module (crates-io))

(define-public crate-suiweb-0.1.0 (c (n "suiweb") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nagnc0iq7x3ywg054f2ypd1f4acjdig0286sibwlhddcql4wqaz")))

(define-public crate-suiweb-0.1.1 (c (n "suiweb") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i31ax3ncyjssa7xzn2sfi10gpq3xp4p84y2619py24ay4s2593p")))

(define-public crate-suiweb-0.1.2 (c (n "suiweb") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mxa8nhh4vyjcrqdvfaakmvmnkgx2zbjrgcjxrr0g3l2g6fy5wdy")))

(define-public crate-suiweb-0.1.3 (c (n "suiweb") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmzs7cd4jjg7vpkd3j5bb3wpkghl9dzxxzx86qidp9vhp1abj6n")))

(define-public crate-suiweb-0.1.4 (c (n "suiweb") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14d7np1vhl8cz2cchlji90mvmn82fsf89fnk1l2fld987g9n871g")))

(define-public crate-suiweb-0.1.5 (c (n "suiweb") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pinata-sdk") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17qjisvkj5zbr2xj8j149rwyz9a0cjgcbw77i78zmg3ifjri96v5")))

