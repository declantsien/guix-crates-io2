(define-module (crates-io su nn sunny-anchor) #:use-module (crates-io))

(define-public crate-sunny-anchor-0.1.0 (c (n "sunny-anchor") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)))) (h "1b69r3j3f1868k972wnpf9j5byc9bpx7r6q8shlzrf5mlw2v6gpc") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sunny-anchor-0.1.1 (c (n "sunny-anchor") (v "0.1.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)))) (h "0xmq3i4zjlc46hla1fwjjq0y8ggqd91k1in4qgz4x4cgnq1lmc4n") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sunny-anchor-0.2.0 (c (n "sunny-anchor") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)))) (h "0rbjxdwx285j2vrc1hn1fxrr4dg670qwav38azzkf4s6ds7i226w") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sunny-anchor-0.3.0 (c (n "sunny-anchor") (v "0.3.0") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)))) (h "1gwr10jp6dc4ywv6622csrbldbq1kfcqrfsb6ajysqhcklk8chf7") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sunny-anchor-0.3.1 (c (n "sunny-anchor") (v "0.3.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)))) (h "0719xhnirr9yy34l8s9hdmv9z6bqa6rk115mabf95rd1cfy8sdwi") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

