(define-module (crates-io su bn subnet_calculator) #:use-module (crates-io))

(define-public crate-subnet_calculator-0.1.0 (c (n "subnet_calculator") (v "0.1.0") (h "0khi91p4hd3jsnsy0qs5n5n2vjbsq5n25gvzvy7705n18jdmjgj4")))

(define-public crate-subnet_calculator-0.2.0 (c (n "subnet_calculator") (v "0.2.0") (h "1v9xq0xl3g86c0y0lc2235f6c3q9dp27v25w9pl7lgdpkf3sj9q8")))

