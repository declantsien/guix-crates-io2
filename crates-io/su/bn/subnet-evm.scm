(define-module (crates-io su bn subnet-evm) #:use-module (crates-io))

(define-public crate-subnet-evm-0.0.0 (c (n "subnet-evm") (v "0.0.0") (d (list (d (n "big-num-manager") (r "^0.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0wbcqks03m21hs2kq77iwh3p25nk6p4w1c2lbm76sfb7kzxbq6rg") (r "1.62")))

