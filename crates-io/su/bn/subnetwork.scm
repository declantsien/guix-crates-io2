(define-module (crates-io su bn subnetwork) #:use-module (crates-io))

(define-public crate-subnetwork-0.1.0 (c (n "subnetwork") (v "0.1.0") (h "1pdm8ilzccmhgjswhlm5zphr44j3x0cvbx828lpc9c61k2h70d0p")))

(define-public crate-subnetwork-0.1.1 (c (n "subnetwork") (v "0.1.1") (h "0fs3hi8gvz2fx7w542ib5d0jhm157g3djs32plxgb8xfajrxx24q")))

(define-public crate-subnetwork-0.1.2 (c (n "subnetwork") (v "0.1.2") (h "06rjxyqwswah6y7d0pjv9fgvmd4lxghk8vvcjzy507fvfr0jc4hp")))

(define-public crate-subnetwork-0.1.3 (c (n "subnetwork") (v "0.1.3") (h "1drpdm0r1lkr1724saf139id2vd3ja6dbwnixl0r1625ipkg520p")))

(define-public crate-subnetwork-0.1.4 (c (n "subnetwork") (v "0.1.4") (h "0v19qs8brp4dsk66lgza5qpbl7i7k20niw3m2ypwn8x53ccczqgb")))

(define-public crate-subnetwork-0.1.5 (c (n "subnetwork") (v "0.1.5") (h "092k8nbrr2g17dswjxnz50psqylphdlirx5dwkpqaal29hbbk8gg")))

(define-public crate-subnetwork-0.1.6 (c (n "subnetwork") (v "0.1.6") (h "0wfbjwrrh3gs1ydcjm7fnx7madn2hvhlpm2g5fyln0jcd9a3n0zy")))

(define-public crate-subnetwork-0.1.7 (c (n "subnetwork") (v "0.1.7") (h "08f0m0gmyyriq97jwh1q95g3cna2j0wqllb7ldbp5qlqpi0cjr0f")))

(define-public crate-subnetwork-0.1.8 (c (n "subnetwork") (v "0.1.8") (h "1kh1x7jsq15cn8f9ivax5f8hqvjn1kpc1xqqi251qwq3zfd3kl1b")))

(define-public crate-subnetwork-0.1.9 (c (n "subnetwork") (v "0.1.9") (h "0dgbvndzcq0wfp7y9iicnv1s7h7qs522qcybb4dd7x62vqy33nbv")))

(define-public crate-subnetwork-0.2.0 (c (n "subnetwork") (v "0.2.0") (h "1m2nb25d6gbhmy58cvwncmn3pphxfqg5vl6q4rafj97gd4hd6jps")))

(define-public crate-subnetwork-0.2.1 (c (n "subnetwork") (v "0.2.1") (h "14dg3hd46gqrp5i7v8wip1yk7cvv89aqvp3cgdpvv63bxllp65qf")))

(define-public crate-subnetwork-0.2.2 (c (n "subnetwork") (v "0.2.2") (h "041z3ijrnjcpnghn27h6drvs6hp4rss4q22znb6437ahvp81h1vf")))

(define-public crate-subnetwork-0.2.3 (c (n "subnetwork") (v "0.2.3") (h "1j7akrvap7038pcpbiv9nwhvgr6j623cbzypaxkxgzwanh0axwbk")))

(define-public crate-subnetwork-0.2.4 (c (n "subnetwork") (v "0.2.4") (h "1ix37mw9mcx0fz6gh8if34lg3gv6wyc9dq0rr8278nlyab1hz2f8")))

(define-public crate-subnetwork-0.2.5 (c (n "subnetwork") (v "0.2.5") (h "18z4cqwaqfwl5za9r5ijnyd6mha3g7qbxkk89b558i2zjh4987nj")))

(define-public crate-subnetwork-0.2.6 (c (n "subnetwork") (v "0.2.6") (h "15is98jksgwyv07yd7ga3slazyp7l2raclhig2l0r4xk831av3my")))

(define-public crate-subnetwork-0.2.7 (c (n "subnetwork") (v "0.2.7") (h "1gqa5g22x8vr6r8i1fmg594wfng3vh5s86jr8vqwfg8x2fj30bbz")))

(define-public crate-subnetwork-0.2.8 (c (n "subnetwork") (v "0.2.8") (h "0h0gipyakbamkvzl5plp3zrbyk0p7j2ff33ibgiw5vj438wnx0ap")))

(define-public crate-subnetwork-0.2.9 (c (n "subnetwork") (v "0.2.9") (h "1yr25518bagdinpvwhaqga1y7gqv860ghs1q4d7qc5qvpq8r5gi9")))

(define-public crate-subnetwork-0.3.0 (c (n "subnetwork") (v "0.3.0") (h "0bwdxjprfbss1xsgbjqv5g6drbz7gpkq4z4izb231mdynhvdxkh1")))

(define-public crate-subnetwork-0.3.1 (c (n "subnetwork") (v "0.3.1") (h "00a0566wnf403k2y6fpjzn7ni4hn52b53f3r71gcga0xl9vkz1fa")))

(define-public crate-subnetwork-0.3.2 (c (n "subnetwork") (v "0.3.2") (h "0yp8kp154z8ri3fxpdg52szscfpsps50d187hlz1f9spnaij7aq2")))

(define-public crate-subnetwork-0.3.3 (c (n "subnetwork") (v "0.3.3") (h "1bvbvjscvnkxb10hv4fc53db32d3vpj8fjv20p4f0fcrbkpgm1p0")))

(define-public crate-subnetwork-0.3.4 (c (n "subnetwork") (v "0.3.4") (h "09m1k4xyk8mf75y2bnds1izlbnx16c4i6a18ks2dazinyfb2lwbx")))

(define-public crate-subnetwork-0.3.5 (c (n "subnetwork") (v "0.3.5") (h "1by0ny811sqvy8296bb5jga6yrpr5nr4i2xxa4cx8hpcscqwrnad")))

(define-public crate-subnetwork-0.3.6 (c (n "subnetwork") (v "0.3.6") (h "1amw15g2fm9mqb0bnb6m3x0mj40vq22jpzjx2s9vh8i8s2vb6pia")))

(define-public crate-subnetwork-0.3.7 (c (n "subnetwork") (v "0.3.7") (h "0b5r9f6xg0zz6ca5w2mzm6z35w6djxw8fzzjk74hihy5mai935cy")))

(define-public crate-subnetwork-0.3.8 (c (n "subnetwork") (v "0.3.8") (h "0xynhbqfjhcgiw9i6vizr6kan7r1wwhjw9ys53clqc53ll5c4hh1")))

(define-public crate-subnetwork-0.3.9 (c (n "subnetwork") (v "0.3.9") (h "0m5wdhdar0w05i3hbqzzsk22mkrrf2jfqd4ibfjr23a8y63cy2w6")))

(define-public crate-subnetwork-0.3.10 (c (n "subnetwork") (v "0.3.10") (h "0avkrysph9pcbpx4hfq8bpfij5k2asv69wwpv8zf22977w6g7kvp")))

(define-public crate-subnetwork-0.4.0 (c (n "subnetwork") (v "0.4.0") (h "1bhrh4j1zyr7wi5mb0rnxx5ppf35ahrllsjc3yghps2k31mw2gk6")))

(define-public crate-subnetwork-0.4.1 (c (n "subnetwork") (v "0.4.1") (h "1v3yzad7qcw5wkxwpj0sviw5r06161qr21kzga6j3dgjw2ipza0h")))

