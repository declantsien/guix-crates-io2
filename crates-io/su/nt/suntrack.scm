(define-module (crates-io su nt suntrack) #:use-module (crates-io))

(define-public crate-suntrack-0.1.0 (c (n "suntrack") (v "0.1.0") (d (list (d (n "asciifolding") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "0a5pd23m7dygi7pxkf2z3d6mn6n9bxqsc13q3c6rz6b7pxlj1czq")))

(define-public crate-suntrack-0.1.1 (c (n "suntrack") (v "0.1.1") (d (list (d (n "asciifolding") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "0q0qsab1gavrqjmwqbny4v7mzglqg5jlysqjaql42qyrnp4kgmqy")))

