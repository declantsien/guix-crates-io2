(define-module (crates-io su ba subar) #:use-module (crates-io))

(define-public crate-subar-0.1.0 (c (n "subar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "mpd_client") (r "^1.3.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "00h5zsvb2vmvy8ydcw1vnb9xh2hisizvqnz2yl8106v6p7bhz1ya")))

