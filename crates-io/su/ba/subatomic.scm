(define-module (crates-io su ba subatomic) #:use-module (crates-io))

(define-public crate-subatomic-0.1.0 (c (n "subatomic") (v "0.1.0") (h "1nf4ahkd8bh1g5yd9r26p3gkbba5ivhavga76v0ciy8lp0n0mxl8") (f (quote (("u64") ("u32") ("default" "u32" "u64"))))))

(define-public crate-subatomic-0.2.0 (c (n "subatomic") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1js0684xr1kfx3yvcyvhcmy3fliwkxiqfd7gxxavbv5sldwfwfhx") (f (quote (("u64") ("u32") ("default" "u32" "u64"))))))

(define-public crate-subatomic-0.2.1 (c (n "subatomic") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bkcgdw5m12w6l29q3dqkzpmbcqn305zd0vai2rfpl0iflxhf7zn") (f (quote (("u64") ("u32") ("default" "u32" "u64"))))))

