(define-module (crates-io su ba subalfred-util) #:use-module (crates-io))

(define-public crate-subalfred-util-0.9.0-rc13 (c (n "subalfred-util") (v "0.9.0-rc13") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1ip3h5s4k41zdcpf4psaffldzshnqgc23z9kci8cg0fcxkw1nki5")))

(define-public crate-subalfred-util-0.9.0-rc14 (c (n "subalfred-util") (v "0.9.0-rc14") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "039inh00hblj2g8zs78mnsq69g7k033bxj0ns9lg72ag324l25a7")))

(define-public crate-subalfred-util-0.9.0-rc15 (c (n "subalfred-util") (v "0.9.0-rc15") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1c376cdmpvh9ypmmrqg6b41sgrrx6p029kpx0ay93db8ps2kaq1a")))

(define-public crate-subalfred-util-0.9.0-rc16 (c (n "subalfred-util") (v "0.9.0-rc16") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "053fkcvbs25xsldlhq90w3sy2ggxc1ag418fpg2rql0sqibiaac5")))

(define-public crate-subalfred-util-0.9.0-rc17 (c (n "subalfred-util") (v "0.9.0-rc17") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "0djxh4p73rxp85sz459n0z2hszdha3qi78cradsipdv4bflsaq8b")))

(define-public crate-subalfred-util-0.9.0-rc18 (c (n "subalfred-util") (v "0.9.0-rc18") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1xqxgmfhi7skqk57x6gyjd49iilmzb97ralhdy33v7a5gpxs66lv")))

(define-public crate-subalfred-util-0.9.0-rc19 (c (n "subalfred-util") (v "0.9.0-rc19") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "02sd09yfpagx4n1xff04a6adywi4h19v680497ngi3wnzf9cg2df")))

(define-public crate-subalfred-util-0.9.0-rc20 (c (n "subalfred-util") (v "0.9.0-rc20") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1352gjk3v3mlbnzw3iwrjdgffwwnlbjjhnvrc82km5xa5wdhh4r7")))

(define-public crate-subalfred-util-0.9.0 (c (n "subalfred-util") (v "0.9.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1z7bisfgf8vp4j8ggmd98ivyjpn40fckbivkf9lrrcjhddy83zzg")))

(define-public crate-subalfred-util-0.9.1 (c (n "subalfred-util") (v "0.9.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "19zrkkm129sa7zzj0gznbsdw1wjpn03bmmhm0wqfvangw7qmx5la")))

(define-public crate-subalfred-util-0.9.2 (c (n "subalfred-util") (v "0.9.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "0pn2x0wzrfzaz6pfxdrl77yzhx6cn13spgbvjqv30mgbfyh5n1pz")))

(define-public crate-subalfred-util-0.9.3 (c (n "subalfred-util") (v "0.9.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1nbbr5yklx88qbhzwh3dk9y8n2jqd6djrk8lhqxrq61v2p6w6jwc")))

