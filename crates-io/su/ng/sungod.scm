(define-module (crates-io su ng sungod) #:use-module (crates-io))

(define-public crate-sungod-0.1.0 (c (n "sungod") (v "0.1.0") (h "0y4c6cc5psh635qwcbsfjzcxwzin3g4n8as1dw8fb8f2i4yxklzm")))

(define-public crate-sungod-0.1.1 (c (n "sungod") (v "0.1.1") (h "0gd9rkwk4gi0pn2bgigybmxwgfaifn55mapd1apkrczjxai5pq3c")))

(define-public crate-sungod-0.1.2 (c (n "sungod") (v "0.1.2") (h "1rypwwmmkrcndclqdfak6d8b0pb538hpb70q31nhh3qxnl09n1g6")))

(define-public crate-sungod-0.2.0 (c (n "sungod") (v "0.2.0") (h "0yqmzqsfk763214hz5vprsys4gix44rl1yai3cw9z3zw5s5i4z5z") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-sungod-0.2.1 (c (n "sungod") (v "0.2.1") (h "0fqkb1q3yq41spmksqcrxkfmmdxzf70iph6ily2jz8n9k1j3p089") (f (quote (("std") ("default" "std"))))))

(define-public crate-sungod-0.2.2 (c (n "sungod") (v "0.2.2") (h "0q8gmpfxhc20kmmpq65ip4hr3iggi6fy6wkxs3yf2304x2v8xvif") (f (quote (("std") ("default" "std"))))))

(define-public crate-sungod-0.3.0 (c (n "sungod") (v "0.3.0") (h "08b9bazk2x0jgci8ck1mx0ml0310ig2dhz15zchvchzlvnpjh13y") (f (quote (("std") ("default_is_random" "std") ("default" "std"))))))

(define-public crate-sungod-0.3.1 (c (n "sungod") (v "0.3.1") (h "1x66d4i082mfx9dfgmjk38bl3rijlzznij3vmdpm1jjf2p8wlldk") (f (quote (("std") ("default_is_random" "std") ("default" "std"))))))

