(define-module (crates-io su su susudb) #:use-module (crates-io))

(define-public crate-susudb-1.0.0 (c (n "susudb") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0ncx5vw390bkfkc5dfnw6z0b30lxjbwj2pi107fw991wcl48ppw1") (y #t)))

(define-public crate-susudb-1.0.20190609230450 (c (n "susudb") (v "1.0.20190609230450") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0l1bjv5iph8cqpkn0y6s8csav8ij0idbf31d95fwvmk104p6awcp")))

(define-public crate-susudb-1.0.20190709015154 (c (n "susudb") (v "1.0.20190709015154") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "10732kl60slari6kgpym6iay64m57apjg51yfgg1aghb9rvmkr4n")))

(define-public crate-susudb-1.0.20191009134440 (c (n "susudb") (v "1.0.20191009134440") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0vjzg770z379qkm0nms5bs4if6a888gi3a7zpycjsf2ganklxffa")))

(define-public crate-susudb-1.0.20191309225933 (c (n "susudb") (v "1.0.20191309225933") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0dh8k4g8zw6x1z7w2qqdl8whqv5922fkinz084i516i4abxpz71y")))

