(define-module (crates-io su su susu) #:use-module (crates-io))

(define-public crate-susu-0.1.0 (c (n "susu") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1w8w1r4hvifh2pcsv12z2f6wvi9xng63k4mak75yp8jajskpv5p5")))

(define-public crate-susu-0.1.20190409163015 (c (n "susu") (v "0.1.20190409163015") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1y6nfdxdfvr4c183vbr797b52yzwl72cyv3bd53fp4dswn347axn")))

(define-public crate-susu-0.1.20190409173253 (c (n "susu") (v "0.1.20190409173253") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "0h5dgp93llhqfapi5dxk4c50c4p1d57jr23m27r10aii9fv9pgbn")))

(define-public crate-susu-0.1.20190409214629 (c (n "susu") (v "0.1.20190409214629") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "0wfdrh3s8g77ri3v2kb1ykgiscyab4bqycv9f2slr9g5nvi6dxpd")))

(define-public crate-susu-0.1.20190409215312 (c (n "susu") (v "0.1.20190409215312") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1ikn9h5jpv3q3gs8sdi5kjn3mjp3cawqghhi8i8v6bv8khdnzbws")))

(define-public crate-susu-0.1.20190409225553 (c (n "susu") (v "0.1.20190409225553") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "05dy8jizv2bs7j948srhlbqypwih90npcw8agg7lxf8hpidkfn2a")))

(define-public crate-susu-0.1.20190509101552 (c (n "susu") (v "0.1.20190509101552") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0xk0xm666mhgzzjvch9lp81l51h3fgzpggszjmaizqnzlbxpvhqz")))

(define-public crate-susu-0.1.20190509145508 (c (n "susu") (v "0.1.20190509145508") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0qcg952a50njr9ygjcprbrsglv7zbpycbf9rr1sik4sniq65gzgh")))

(define-public crate-susu-0.1.20190509190436 (c (n "susu") (v "0.1.20190509190436") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0vr8l31a2apvr258pr6vxagngag0z3hm9pqhr7j2ad3a6ayd0krp")))

