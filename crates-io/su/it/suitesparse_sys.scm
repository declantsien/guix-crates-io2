(define-module (crates-io su it suitesparse_sys) #:use-module (crates-io))

(define-public crate-suitesparse_sys-0.1.0 (c (n "suitesparse_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0881wmql379qbjw80wrxfq3plx8yqag9mj7w3in0yqrzjnmibx83") (f (quote (("umfpack") ("ldl") ("klu") ("dynamic") ("cxsparse") ("cholmod") ("amd"))))))

