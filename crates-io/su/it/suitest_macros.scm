(define-module (crates-io su it suitest_macros) #:use-module (crates-io))

(define-public crate-suitest_macros-0.1.0 (c (n "suitest_macros") (v "0.1.0") (h "1q9p9fj4hvqxqk10ydzlgazry41h74s3wlyxr45xjrfmqd5b7p1c")))

(define-public crate-suitest_macros-0.1.1 (c (n "suitest_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0r8wn4dbd130pbb6q329kr6krf2g623jabaz4da526jpd2mymgsn")))

(define-public crate-suitest_macros-0.1.2 (c (n "suitest_macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ssngs4yakyavgl305sbr14b5xfhwk7p0vi1bzb80qq4wi7cmmhp")))

