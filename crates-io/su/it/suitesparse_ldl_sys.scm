(define-module (crates-io su it suitesparse_ldl_sys) #:use-module (crates-io))

(define-public crate-suitesparse_ldl_sys-0.1.0 (c (n "suitesparse_ldl_sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r0jdn04wavg04di5ja65dgznlqkgwgl2vq70izyxvzlz1mdhwv3")))

(define-public crate-suitesparse_ldl_sys-0.2.0 (c (n "suitesparse_ldl_sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wj9jvdp3109hkdxihy14lw9agxp3mrbfwypnbsski2fgrcl1qdy")))

(define-public crate-suitesparse_ldl_sys-0.3.0 (c (n "suitesparse_ldl_sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "suitesparse-src") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "06xl0xfrd7x7k1aklgqasv35mqm4rmalw452yg1sdlxjmrz15ind") (f (quote (("static" "suitesparse-src/ldl")))) (l "ldl")))

