(define-module (crates-io su it suitesparse-src) #:use-module (crates-io))

(define-public crate-suitesparse-src-0.1.0 (c (n "suitesparse-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.60") (d #t) (k 1)))) (h "0wrxnd6qljifyl33ldbd3djkcdmbs18qw0fxc6s6qlq1r555mk2h") (f (quote (("ldl") ("camd")))) (l "suitesparse-src")))

