(define-module (crates-io su it suite) #:use-module (crates-io))

(define-public crate-suite-0.0.0 (c (n "suite") (v "0.0.0") (h "0k9xkhmbmvljzr5mr799cwzizl234cl3jbd02dhxvr7amikqwbrh")))

(define-public crate-suite-0.0.1 (c (n "suite") (v "0.0.1") (h "0mc71qrsw26di11lcb9bcq7asv7mcc76b8k898dr572fgn1m393h")))

