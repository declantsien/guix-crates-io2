(define-module (crates-io su it suitesparse_graphblas_sys) #:use-module (crates-io))

(define-public crate-suitesparse_graphblas_sys-0.1.0 (c (n "suitesparse_graphblas_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "06fr20im6ls67ykq1bi80i6xq5a44hklcyk02y2pm27cbwjgd89k")))

(define-public crate-suitesparse_graphblas_sys-0.1.1 (c (n "suitesparse_graphblas_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "05jxh9n0nvcllz3fygkixiiybish4v7cx9i1l9rpk5jblv9m1xfa")))

(define-public crate-suitesparse_graphblas_sys-0.1.3 (c (n "suitesparse_graphblas_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1yplkgk0mnfqv1fblxln14pz09bm62vk95932c65l7ag5zza23qk")))

(define-public crate-suitesparse_graphblas_sys-0.1.4 (c (n "suitesparse_graphblas_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0fa1nigz11g7sh18939idw8i1vcldqdrh33l1lnc7h17j9m8s65y")))

(define-public crate-suitesparse_graphblas_sys-0.1.5 (c (n "suitesparse_graphblas_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "0fx5rk4a05a4l87czpawa8jxi98vv6ldwa9apc5xvvasivmal0vl")))

(define-public crate-suitesparse_graphblas_sys-0.1.6 (c (n "suitesparse_graphblas_sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.16") (d #t) (k 1)))) (h "1g28i9pxpcdh96khqknbq5wg6g880mar3ap8z6jf8mklxw761h17")))

(define-public crate-suitesparse_graphblas_sys-0.2.0 (c (n "suitesparse_graphblas_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.18") (d #t) (k 1)))) (h "0gh53p8f5n2j6dxra5qbsl80hi56gf935l1yfs00sfz4yhh4ci62") (f (quote (("disable-just-in-time-compiler") ("build-standard-kernels")))) (y #t)))

(define-public crate-suitesparse_graphblas_sys-0.2.1 (c (n "suitesparse_graphblas_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.18") (d #t) (k 1)))) (h "13qhbr785gd8h92hk0sm7bmxkxn1y26y2jjwja218xdl79l6sslm") (f (quote (("disable-just-in-time-compiler") ("build-standard-kernels"))))))

