(define-module (crates-io su it suitest) #:use-module (crates-io))

(define-public crate-suitest-0.1.0 (c (n "suitest") (v "0.1.0") (h "15asfy79bsxhgxbqc34m8m1skmjlvacbgs64k7lqjwqynrhb50wv")))

(define-public crate-suitest-0.1.1 (c (n "suitest") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "suitest_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "0cp7f1y1zwz8y0xfgk8vxxv6d1mxb9nyxkaymannyyqndb2km7zz")))

(define-public crate-suitest-0.1.2 (c (n "suitest") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "suitest_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "0m4hyagfmwl4m4cw925xqdkmcbdp5bryaby2k3mcjmiibml2qspf")))

