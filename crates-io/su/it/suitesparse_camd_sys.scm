(define-module (crates-io su it suitesparse_camd_sys) #:use-module (crates-io))

(define-public crate-suitesparse_camd_sys-0.1.0 (c (n "suitesparse_camd_sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1jaiv3wc0xalrb9ycnb99flmn6bv74fx37xv80pwvav01sdaviv3")))

(define-public crate-suitesparse_camd_sys-0.1.1 (c (n "suitesparse_camd_sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "suitesparse-src") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "03bjq4x6rhjvd3nimw3r56ykpdhfj6ywiw3i8l5cj0k3bk64g8yp") (f (quote (("static" "suitesparse-src/camd")))) (l "camd")))

