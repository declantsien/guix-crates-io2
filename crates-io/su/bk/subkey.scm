(define-module (crates-io su bk subkey) #:use-module (crates-io))

(define-public crate-subkey-2.0.1 (c (n "subkey") (v "2.0.1") (d (list (d (n "sc-cli") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1j05w3c8j5gyjvxp3iks6imvw6n9k248kz97apw54bnxgkik2plc") (y #t)))

(define-public crate-subkey-3.0.0 (c (n "subkey") (v "3.0.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.30.0") (d #t) (k 0)))) (h "0qnmd70p2ys4mvbnrsgi06n9x21aaz1kslihxqq8hvizq9fxj42l")))

(define-public crate-subkey-4.0.0 (c (n "subkey") (v "4.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.31.0") (d #t) (k 0)))) (h "0vl1d3jz104zmriwnln2w58jk7ghi0smvry0m1rqg43kcm9wlcca")))

(define-public crate-subkey-5.0.0-dev.1 (c (n "subkey") (v "5.0.0-dev.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "=0.32.0-dev.1") (d #t) (k 0)))) (h "1b20887v12w6h8nbgn384ld4rgb1bj3nkizmyvb5j4l3m3lzlb9j")))

(define-public crate-subkey-5.0.0 (c (n "subkey") (v "5.0.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.32.0") (d #t) (k 0)))) (h "16hqk5w7yhsxzq3vvqcgj2f53aq2f6d75h5m4lr97vz59yp69s00")))

(define-public crate-subkey-6.0.0 (c (n "subkey") (v "6.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.33.0") (d #t) (k 0)))) (h "0xjjx9p6ln1mycbjhzdsyrxgdfksnhqwdvysi47apiqan2kq8fcd")))

(define-public crate-subkey-7.0.0 (c (n "subkey") (v "7.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.34.0") (d #t) (k 0)))) (h "0kdk8p1l1zh1w47g0s2irz8ivjs4yfq2ry9slnicfir80vr7m2bi")))

(define-public crate-subkey-8.0.0 (c (n "subkey") (v "8.0.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.35.0") (d #t) (k 0)))) (h "0a8244xq1wf69mj6fkgp8l9yyv6nrn4vkbrj8c79y4n6bgqkdrl3")))

(define-public crate-subkey-9.0.0 (c (n "subkey") (v "9.0.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.36.0") (d #t) (k 0)))) (h "08d2nis3lckc0hr6vq06dfk6dhqx4vdbkc7psxbldckm1dkl90k9")))

(define-public crate-subkey-10.0.0 (c (n "subkey") (v "10.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.37.0") (d #t) (k 0)))) (h "00lm066p1v0np0ghljwv8vnw8d123fxbnfiw45zmw34f53y0nnmm")))

(define-public crate-subkey-11.0.0 (c (n "subkey") (v "11.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.38.0") (d #t) (k 0)))) (h "00513kq8r8ncn4hljq6kk4lar1cgh6bzqwgymhli8ygck42rk0i7")))

(define-public crate-subkey-12.0.0 (c (n "subkey") (v "12.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.39.0") (d #t) (k 0)))) (h "0gaflpdba0p7j996q2xi2fx2bhr6yv9kf3agfqa43nzhfc6jbbls")))

(define-public crate-subkey-13.0.0 (c (n "subkey") (v "13.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.40.0") (d #t) (k 0)))) (h "07q29da5py2jg3qm6w694xw2f48s5xp4nsbd0wkbmkivfbvs17vf")))

(define-public crate-subkey-14.0.0 (c (n "subkey") (v "14.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.41.0") (d #t) (k 0)))) (h "0rq1hidby8rlrb8634dsnbyrqsdhadk59aqmdid5vc9kr0gxqnh4")))

(define-public crate-subkey-15.0.0 (c (n "subkey") (v "15.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sc-cli") (r "^0.42.0") (d #t) (k 0)))) (h "1qy8kz8l747mx3yzr6m8c08rjmls7s2gviw16xgfx1jwpqbk2wxb")))

