(define-module (crates-io su ml sumlib) #:use-module (crates-io))

(define-public crate-sumlib-0.0.0 (c (n "sumlib") (v "0.0.0") (h "1cjd1n6h9xii3aq3hhqf1h43r463dzlwmbb2zl113l2mijlnyiz9")))

(define-public crate-sumlib-0.0.3 (c (n "sumlib") (v "0.0.3") (h "12cc6hlbpq01wx6zzn6jgq427cz0ag9bxk6pkgnqxblfkwbv1yff")))

(define-public crate-sumlib-0.0.4 (c (n "sumlib") (v "0.0.4") (h "1lnr5hybr4jd342glyqb0chwnm73gznjanfrhvj3lbabyri5wlnh")))

