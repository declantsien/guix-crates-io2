(define-module (crates-io su ml sumlibrary) #:use-module (crates-io))

(define-public crate-sumlibrary-0.1.1 (c (n "sumlibrary") (v "0.1.1") (h "0whzy1k9idlkp5w1g0qxv4s178yimdisk1x3f4xx02cvm5xa53kw") (y #t)))

(define-public crate-sumlibrary-0.1.2 (c (n "sumlibrary") (v "0.1.2") (h "02hdsl4wyj1hxsf5d0azwcnyxrd68w0z4m03i93560rnq6qq16jv")))

