(define-module (crates-io su lf sulfa) #:use-module (crates-io))

(define-public crate-sulfa-0.1.0 (c (n "sulfa") (v "0.1.0") (h "0lllpjihi38iz12dwqwcjac3lycfag4wh1nz28p11a61gagzz39p")))

(define-public crate-sulfa-0.1.1 (c (n "sulfa") (v "0.1.1") (h "1yv2rrglhcmhiv12ngbk69hmycgl3bkvzh3vwk43zzgg12dd9x7z")))

(define-public crate-sulfa-0.0.0 (c (n "sulfa") (v "0.0.0") (h "12g8bmg7rb7kz3ax1siyqyip3yp3z81p7q511lp50228ck0qdmdf") (y #t)))

