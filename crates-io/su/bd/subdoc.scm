(define-module (crates-io su bd subdoc) #:use-module (crates-io))

(define-public crate-subdoc-0.0.0 (c (n "subdoc") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wswmv7aywp37fkbj68q7s1f9dfqgjs7w3l51vqxa3lzh1qniy8n")))

