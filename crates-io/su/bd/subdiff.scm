(define-module (crates-io su bd subdiff) #:use-module (crates-io))

(define-public crate-subdiff-0.1.0 (c (n "subdiff") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "temporary") (r "^0.6.3") (d #t) (k 2)))) (h "0n2sixr5q329vwdll3x677ffbmr5v0yk9qlm5jdsn15v7bjhrdcs")))

