(define-module (crates-io su bi subito) #:use-module (crates-io))

(define-public crate-subito-0.1.0 (c (n "subito") (v "0.1.0") (h "0r0lanx2hv6g9g4anfgpvpbldfha32jxj09r8saxg7978fcwqy0a")))

(define-public crate-subito-0.1.1 (c (n "subito") (v "0.1.1") (d (list (d (n "borsh") (r "^0") (d #t) (k 0)) (d (n "nothing") (r "^0") (d #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)))) (h "01f2vhnnn59f940nz69ndp27hanq7m2cdflb97x0rjavn7xvqc8w")))

