(define-module (crates-io su bi subimg) #:use-module (crates-io))

(define-public crate-subimg-0.2.0 (c (n "subimg") (v "0.2.0") (d (list (d (n "cev") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "053zp8w2hjjkiwpcr0a1hqbqd3i1hz4v9jx3smdg9kbriq0lk2wk")))

