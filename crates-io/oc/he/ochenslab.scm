(define-module (crates-io oc he ochenslab) #:use-module (crates-io))

(define-public crate-ochenslab-0.0.1 (c (n "ochenslab") (v "0.0.1") (h "1mvrdss8z1h5f1b4d3micknp4vv21ybyk43yjwiyz7wp8fz2kj0n")))

(define-public crate-ochenslab-0.0.2 (c (n "ochenslab") (v "0.0.2") (h "1pq4whwsk2x7rp2i8d7l2iwbzdsp4r71ci40lhhhwyal68myxm9n")))

