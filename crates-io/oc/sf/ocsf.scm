(define-module (crates-io oc sf ocsf) #:use-module (crates-io))

(define-public crate-ocsf-0.0.1-alpha (c (n "ocsf") (v "0.0.1-alpha") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nhdd4bpp9r8pfn0jff1x588xxwh4gjbwbmgffjacnhfzxwishjy") (r "1.66")))

(define-public crate-ocsf-0.0.1-alpha1 (c (n "ocsf") (v "0.0.1-alpha1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h35p2b1qfasjxm3k3zpjxpng03111ms10gnaphvvysr87gyjycc") (r "1.66")))

(define-public crate-ocsf-0.0.1-alpha2 (c (n "ocsf") (v "0.0.1-alpha2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)))) (h "136d865sadp3zk0cd8g07674hsycrsnhrn2k63xz247jy2hkm5sr") (r "1.66")))

