(define-module (crates-io oc i_ oci_rs) #:use-module (crates-io))

(define-public crate-oci_rs-0.3.0 (c (n "oci_rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0z0zym64rkilf9dm4d3gq807skfbfaiskmajadviqga4d6g479iq")))

(define-public crate-oci_rs-0.3.1 (c (n "oci_rs") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1x979j0b3qvfzka9hp6jcyqynqykp84064ifyc1vkx6mwqck068y")))

(define-public crate-oci_rs-0.3.2 (c (n "oci_rs") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1hnw97rjwbp2dmvss2g1hzb3ibd9sbksp69lc59l5l8kiclq74yn") (y #t)))

(define-public crate-oci_rs-0.4.0 (c (n "oci_rs") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0pb4x7gxwib4v4lss821zn81gvqqdx59n3ylx13cy73xq8slib0g")))

(define-public crate-oci_rs-0.5.0 (c (n "oci_rs") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1znli41l72hqvr11a2cllp85s5ffwwv7dnzf4585rdj49yr98b0w")))

(define-public crate-oci_rs-0.6.0 (c (n "oci_rs") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0ccb9cfrqnb01a30a91djawvx8v6lp2j13is0m9y7a7xrch03qm6")))

(define-public crate-oci_rs-0.7.0 (c (n "oci_rs") (v "0.7.0") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1mqkg3cgpd5hsqb4bkdsj6sib0ydx22mq1yipwmcwzhdwj6z9w0d")))

(define-public crate-oci_rs-0.8.0 (c (n "oci_rs") (v "0.8.0") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "12ip3l9wc3y6szgyvajhgj24yxxxbwd7czj4sdlaf07xqrspq3b2")))

