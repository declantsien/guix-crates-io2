(define-module (crates-io oc ct occt-sys) #:use-module (crates-io))

(define-public crate-occt-sys-0.1.0 (c (n "occt-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0281g67kiaam9r9pp04352gdkq6qjzhbbcmqx59bh02ar8zf55ww")))

(define-public crate-occt-sys-0.2.0 (c (n "occt-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1688744f4szfxr9l6bazz996w3wr1d3iwff9qpbw30zlazcagjks")))

(define-public crate-occt-sys-0.3.0 (c (n "occt-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "02h35jv8n5m1az15xhcpv4fv58l3pmy2abmwvx3vyhhradkf5xxb")))

