(define-module (crates-io oc b3 ocb3) #:use-module (crates-io))

(define-public crate-ocb3-0.0.0 (c (n "ocb3") (v "0.0.0") (h "01430y10xvhc4k4hknqvz406ydcb0dl26l2267xymra3xgs8msm3")))

(define-public crate-ocb3-0.1.0 (c (n "ocb3") (v "0.1.0") (d (list (d (n "aead") (r "^0.5") (k 0)) (d (n "aead") (r "^0.5") (f (quote ("dev"))) (k 2)) (d (n "aes") (r "^0.8") (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1nyyj0rx870iv24ad8j2x55qlabald1pazkpslyq8727dhky15n1") (f (quote (("stream" "aead/stream") ("std" "aead/std" "alloc") ("rand_core" "aead/rand_core") ("heapless" "aead/heapless") ("getrandom" "aead/getrandom" "rand_core") ("default" "alloc" "getrandom") ("arrayvec" "aead/arrayvec") ("alloc" "aead/alloc")))) (r "1.60")))

