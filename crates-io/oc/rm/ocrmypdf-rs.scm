(define-module (crates-io oc rm ocrmypdf-rs) #:use-module (crates-io))

(define-public crate-ocrmypdf-rs-0.0.1 (c (n "ocrmypdf-rs") (v "0.0.1") (h "0ybg0xa4z6a1yzjq9yni51v1ibk0m5pknlmhp0c53my9blwb4xvw")))

(define-public crate-ocrmypdf-rs-0.0.2 (c (n "ocrmypdf-rs") (v "0.0.2") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)))) (h "14n9caw0lmi8b6br5y0lsmd2pgcs9xxp5svgdrn4ivj0b61qm7dc")))

(define-public crate-ocrmypdf-rs-0.0.3 (c (n "ocrmypdf-rs") (v "0.0.3") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)))) (h "08li389ar6z1qawl46dgfhwg1zmasrbbfd0jsi6ap0sxcyqggild")))

(define-public crate-ocrmypdf-rs-0.0.4 (c (n "ocrmypdf-rs") (v "0.0.4") (d (list (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "05pkckyyvzrnyvybpfrzgmwi0jxvh7mgs8gr0l5xrp5r15cypg2y")))

(define-public crate-ocrmypdf-rs-0.0.5 (c (n "ocrmypdf-rs") (v "0.0.5") (d (list (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "0fsyjd7aqds84vwpgl9wfbyxddx88lpcw48940wbajfbpqq5cfka")))

(define-public crate-ocrmypdf-rs-0.0.6 (c (n "ocrmypdf-rs") (v "0.0.6") (d (list (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "1zx55mh3zz1syc990r9k3wxsjbnz6dsgx0wgws1mpzi2s5c7cfpq")))

(define-public crate-ocrmypdf-rs-0.0.7 (c (n "ocrmypdf-rs") (v "0.0.7") (d (list (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "09jm6qqlq6qz0yzbzk5m98h74vvxgph77sqz0hhs5g7ihm1hrc1a")))

(define-public crate-ocrmypdf-rs-0.0.8 (c (n "ocrmypdf-rs") (v "0.0.8") (d (list (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "0hqcw64h09416i22zifcc5rpj3p4xkfs73bc5wahx82xwq9mzqf7")))

