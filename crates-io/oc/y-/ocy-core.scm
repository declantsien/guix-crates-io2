(define-module (crates-io oc y- ocy-core) #:use-module (crates-io))

(define-public crate-ocy-core-0.1.0 (c (n "ocy-core") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0iqxv6xpqk7hvihj2q7acj67m9g9bcf96m81ij3ip68w7k0mh2xz")))

(define-public crate-ocy-core-0.1.1 (c (n "ocy-core") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1f1nf6qzixwpyw0c1q26bbvks2bng3xl62cgg3zm5658grmc0vkh")))

(define-public crate-ocy-core-0.1.2 (c (n "ocy-core") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1dk3pc9zzc69pwlhmyyi1s90rsgirjzjmjmgq6vjn31kcfbiggsk")))

(define-public crate-ocy-core-0.1.3 (c (n "ocy-core") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0lk0h0slfby6xj0gcy9dngb60qyzf16n0ci4n248rafbj99b0nld")))

(define-public crate-ocy-core-0.1.4 (c (n "ocy-core") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "12hplz91gbddnlii22hf6v8md2cf2z9mb2wnnwghzjzsfazna89i")))

(define-public crate-ocy-core-0.1.5 (c (n "ocy-core") (v "0.1.5") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "101zk0wpa1d975f335pnpnnb45wn8db0mjjcyxyl6jvai00z8q3w")))

(define-public crate-ocy-core-0.1.6 (c (n "ocy-core") (v "0.1.6") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "030kxjs900xs8v74kibzzlxz24c7j5v05j1vfx3kbxjcadw6qjh7")))

(define-public crate-ocy-core-0.1.7 (c (n "ocy-core") (v "0.1.7") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0im52n70s3psbfvb2wvbyh2qmgf7q91vvxkxhypvbfhnldiqkrdd")))

