(define-module (crates-io oc ti octicons) #:use-module (crates-io))

(define-public crate-octicons-0.1.0 (c (n "octicons") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 1)) (d (n "svgparser") (r "^0.5") (d #t) (k 1)))) (h "0bz82cdra9970wn2sw79y2vd1nzm4yj8jwhqrmq8z7qdr97y0lzj")))

(define-public crate-octicons-0.1.1 (c (n "octicons") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 1)) (d (n "svgparser") (r "^0.5") (d #t) (k 1)))) (h "10gmkffsl7wmdiccg3zs25ijw69b26rp1nk1f82p9g4r2bhn8f9j")))

(define-public crate-octicons-0.2.0 (c (n "octicons") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "xmlparser") (r "^0.13") (d #t) (k 1)))) (h "12sn1p9j5z3bq77cw04ig9fji3kfcbb8hgz1qairfd9lpnyq1793")))

