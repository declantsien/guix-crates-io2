(define-module (crates-io oc to octocrate-api-builder) #:use-module (crates-io))

(define-public crate-octocrate-api-builder-0.1.0 (c (n "octocrate-api-builder") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03rg6a2c5assg11vdkdd24wsvhnjiv1pnqa5fpz15a9ym4xg1pcv")))

(define-public crate-octocrate-api-builder-0.1.1 (c (n "octocrate-api-builder") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0z19r8r37bpaxfm9xqmiz17bmmzhm23sd2dn8sm3254vh9bvnlpr")))

