(define-module (crates-io oc to octoproxy-easycert) #:use-module (crates-io))

(define-public crate-octoproxy-easycert-0.1.0 (c (n "octoproxy-easycert") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive" "help" "std"))) (k 0)) (d (n "rcgen") (r "^0.10.0") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "time") (r "^0.3.6") (k 0)))) (h "1idlvsi8yphnrhnahj1g0qf1ygmj07v324dx6q3mcz5xpm0j5i5c")))

