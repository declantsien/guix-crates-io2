(define-module (crates-io oc to octosql) #:use-module (crates-io))

(define-public crate-octosql-0.0.1 (c (n "octosql") (v "0.0.1") (d (list (d (n "arrow") (r "^1.0.0") (d #t) (k 0)) (d (n "datafusion") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "sqlparser") (r "^0.6.1") (d #t) (k 0)))) (h "0cq1xhkf1rchhiddzwjcszph913psxgfglabkxk9a8gqk84i9wvb")))

