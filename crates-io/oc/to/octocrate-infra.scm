(define-module (crates-io oc to octocrate-infra) #:use-module (crates-io))

(define-public crate-octocrate-infra-0.1.0 (c (n "octocrate-infra") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (f (quote ("use_pem"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "0pkszwbm847d022d32h4bgj9n80giz6gz1mdhm84vx5yyn7k47h1")))

(define-public crate-octocrate-infra-0.1.1 (c (n "octocrate-infra") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8.2.0") (f (quote ("use_pem"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "0p9xzlq9f24hxqwyi660rsdr0j6p9ciw9w2j861hq62kx4v6mvdj")))

