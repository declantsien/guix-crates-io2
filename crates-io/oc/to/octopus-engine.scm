(define-module (crates-io oc to octopus-engine) #:use-module (crates-io))

(define-public crate-octopus-engine-0.1.0 (c (n "octopus-engine") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jw31iry09w5nvg93k35qyhx2b4kmc5x55gja88zffbxkrj76z98")))

(define-public crate-octopus-engine-0.2.0 (c (n "octopus-engine") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bhjlqvi13a3zfqfscxjwfmnfzzqq8bkaylv2iac3dpdzp4pp0w5")))

