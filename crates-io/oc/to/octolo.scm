(define-module (crates-io oc to octolo) #:use-module (crates-io))

(define-public crate-octolo-0.1.0 (c (n "octolo") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1z4q6lzqvn3vlk8scjydzz6y2sp4y1z3bni9znbwm40qipgm70xz")))

(define-public crate-octolo-0.1.1 (c (n "octolo") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "020qh3pbnrc1iwkbb4jyyhv6z62mcxwgfb0hb1qsz5m4jfm13mcd")))

