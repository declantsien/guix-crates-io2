(define-module (crates-io oc to octosurfer) #:use-module (crates-io))

(define-public crate-octosurfer-0.1.0 (c (n "octosurfer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "grep") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "octocrab") (r "^0.18.1") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (f (quote ("colors" "stderr"))) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jijicqcq0agv4j34wlvnmp2m7cxx9n5bisvwjzd3dqiwbcrdwjj")))

