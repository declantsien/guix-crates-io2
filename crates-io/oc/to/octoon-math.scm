(define-module (crates-io oc to octoon-math) #:use-module (crates-io))

(define-public crate-octoon-math-0.1.0 (c (n "octoon-math") (v "0.1.0") (h "1956g6mk85nq4lc159yzyi1qjn5wa0n8d7sm53wwpfykkza3qi7s")))

(define-public crate-octoon-math-0.1.1 (c (n "octoon-math") (v "0.1.1") (h "0h9v0qw3hw4ldb5fnrww4r6yngrib0fmyaqndaslbcim6amch93f")))

(define-public crate-octoon-math-0.1.2 (c (n "octoon-math") (v "0.1.2") (h "04nrzmagxiimn7pdjf334kkn7n2c0f3szvmv68nvswnw25p26flh")))

(define-public crate-octoon-math-0.1.3 (c (n "octoon-math") (v "0.1.3") (h "03y2d5y5y13bz5cy2l683c6sicksvfn27nv6695ghzkz1yk3q1zm")))

(define-public crate-octoon-math-0.1.4 (c (n "octoon-math") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "08l8jvg4c4d1xa92kgg6rczz0yp5mgnlqv06m2h88n2w2kni8nqp")))

(define-public crate-octoon-math-0.1.5 (c (n "octoon-math") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pq033xzi4n5majirw239rn826w8d6jwafsnwm9vr3154kisxf19") (f (quote (("serialize" "serde"))))))

(define-public crate-octoon-math-0.1.6 (c (n "octoon-math") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ay9zzw70ar8ahqspcly6p22ix1p26dqgdpnwwm8dlca9a36py38") (f (quote (("serialize" "serde"))))))

(define-public crate-octoon-math-0.1.7 (c (n "octoon-math") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0psx86fwnfdnwm9h3axhdxl6v68d36jad3ykldb0n57ql6diakj3") (f (quote (("serialize" "serde"))))))

