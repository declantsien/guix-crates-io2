(define-module (crates-io oc to octo) #:use-module (crates-io))

(define-public crate-octo-0.1.0 (c (n "octo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbase") (r "^0.5.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shapefile") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1zcm6wyrcrf4liwjs5c2rhf7d4hsbz4ysqjk7db6gyfr2f3r9zz8") (y #t)))

