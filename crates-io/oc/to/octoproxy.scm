(define-module (crates-io oc to octoproxy) #:use-module (crates-io))

(define-public crate-octoproxy-0.1.0 (c (n "octoproxy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive" "help" "std"))) (k 0)) (d (n "octoproxy-client") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-easycert") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-server") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-tui") (r "^0.1.0") (d #t) (k 0)))) (h "065hbcnnrvf128pqp3c9g9ir908s86rqx5za49amvypz08qmfms0") (y #t)))

(define-public crate-octoproxy-0.1.1 (c (n "octoproxy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive" "help" "std"))) (k 0)) (d (n "octoproxy-client") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-easycert") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-server") (r "^0.1.0") (d #t) (k 0)) (d (n "octoproxy-tui") (r "^0.1.0") (d #t) (k 0)))) (h "1yy1024n4j4p406lai9p7fxnbr1sn22rj6wpn1vz8wvrqqp5hpfb")))

