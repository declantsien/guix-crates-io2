(define-module (crates-io oc to octolotl) #:use-module (crates-io))

(define-public crate-octolotl-0.1.0 (c (n "octolotl") (v "0.1.0") (d (list (d (n "miette") (r "^5.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jgbcb1a4220870pv87skmkav4l1qxm57gzp7kg7mv02if8vv59j")))

(define-public crate-octolotl-0.1.1 (c (n "octolotl") (v "0.1.1") (d (list (d (n "miette") (r "^5.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0whv8cbcbf0fhb83xj5jkbv6szy78bp4iccqvc3ivv7z4s2d3iyv")))

