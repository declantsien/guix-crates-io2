(define-module (crates-io oc to octopus) #:use-module (crates-io))

(define-public crate-octopus-0.1.0-alpha.1 (c (n "octopus") (v "0.1.0-alpha.1") (d (list (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0rknsy1a05lgbizw66b5fir20rlsvg115bwpv0rkn5bfrxlwrhrh") (f (quote (("default")))) (y #t)))

(define-public crate-octopus-0.1.0-alpha.2 (c (n "octopus") (v "0.1.0-alpha.2") (d (list (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "179mnljxcykf2sv3pj64swr3msy3yr7ykq7k85s462j85iczdd1h") (f (quote (("default")))) (y #t)))

(define-public crate-octopus-0.1.0-alpha.3 (c (n "octopus") (v "0.1.0-alpha.3") (d (list (d (n "array-macro") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0vn7zhr736cf44dyzcbjvk0qkjw3rm9h6pik0ykl2dl3jz36agl6") (f (quote (("default")))) (y #t)))

(define-public crate-octopus-0.0.0 (c (n "octopus") (v "0.0.0") (h "0c4lrvcv5l3mpy11v1g5p5kpl8955kr1la77q86w6j9fh4s9n07r")))

