(define-module (crates-io oc to octopus-cli) #:use-module (crates-io))

(define-public crate-octopus-cli-0.1.0 (c (n "octopus-cli") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "octopus-engine") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1svvcdan0957nmw55y2y2wg9w3qdhlan5ja2r9cy10c1yvmj0r8b")))

(define-public crate-octopus-cli-0.2.0 (c (n "octopus-cli") (v "0.2.0") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "octopus-engine") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1vajy08s287jd153bs5igacp1jj9sa83ss9k0493f0c7y1n5wyvw")))

