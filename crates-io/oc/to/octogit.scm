(define-module (crates-io oc to octogit) #:use-module (crates-io))

(define-public crate-octogit-2.0.2 (c (n "octogit") (v "2.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0fx9vx82szpvdhjmzs01srz5xl7qp5bz71v0yr5isl874kgzwhkf")))

(define-public crate-octogit-2.1.0 (c (n "octogit") (v "2.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "04z3m7w0r999dvyk2znspd04fwqcv3is2rbnqk1cc151ar3lyr23")))

(define-public crate-octogit-2.2.0 (c (n "octogit") (v "2.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1cq7x9i67fcfd3jc6wc7iv8254y4qlm23gm1i0lwn2qcw88xd72v")))

