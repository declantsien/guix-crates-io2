(define-module (crates-io oc ka ockam_node_no_std) #:use-module (crates-io))

(define-public crate-ockam_node_no_std-0.1.0 (c (n "ockam_node_no_std") (v "0.1.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "1rff2zkgayn6wpxw08zganapqnw13xii9f7bvpa4618ppig8w4pf") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.2.0 (c (n "ockam_node_no_std") (v "0.2.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0xmqjg9cn78zcmixh20ygfmji4ydp0fl62qsnglsrbclhz0sfjwz") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.3.0 (c (n "ockam_node_no_std") (v "0.3.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "1mcngwpmk5a40m858sm89z9wqjasm5zgm72nmq7mdyks2vfhzppv") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.4.0 (c (n "ockam_node_no_std") (v "0.4.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0bdbgb2kafqgzzwzfsl66hzyf4qm8sz5w0v4w8dn3fnwnlrqznwi") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.5.0 (c (n "ockam_node_no_std") (v "0.5.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "1qqqlfvyjp6lh7hswvqg99hhl19wlisdann4n94r8cqkkp91g629") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.6.0 (c (n "ockam_node_no_std") (v "0.6.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0j7h99pb8v9rlvcmmgbw2mszl7gff3wnyr8g2qc780h3s79xn4fa") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.7.0 (c (n "ockam_node_no_std") (v "0.7.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0gzlzrgl4lrsyi5cnl2cdgqfv6dx6j7qa5plrk0kzlm387hyxgy4") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.8.0 (c (n "ockam_node_no_std") (v "0.8.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "1h3mk2vjkh3mss6i8yplb65qfhvfgivv5ssq36pgvyqgb4iwzi1j") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.9.0 (c (n "ockam_node_no_std") (v "0.9.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0ldvz3wy3wij891cvbsvfqv6zvmrj3r4girh55k05khgp60anfyp") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.10.0 (c (n "ockam_node_no_std") (v "0.10.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "05gvy3fc2s27mwj9fp74c7m7ccb0vz1izbv78fx6kd29621digxc") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.11.0 (c (n "ockam_node_no_std") (v "0.11.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0fj4bl2z1d32ks56bdj8kfwcaywirwpb25zzzsl5i6xma8ji47pc") (f (quote (("std" "alloc") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.12.0 (c (n "ockam_node_no_std") (v "0.12.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "06wiyzpb3p0qr5nc31sc7ivf58idfvyaprnw2iy6hm5hgm72nm3y") (f (quote (("std" "alloc") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.13.0 (c (n "ockam_node_no_std") (v "0.13.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "17wnadxi7lgv4090bmw4kbl3n90irl4jj8qjrq2hrdxxadi335wc") (f (quote (("std" "alloc") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_no_std-0.14.0 (c (n "ockam_node_no_std") (v "0.14.0") (d (list (d (n "executor") (r "^0.7.0") (d #t) (k 0)))) (h "0x4yxgm76ni0hjvarzwi1dxv90a66xykgdhwiipr7qz80zq04ln2") (f (quote (("std" "alloc") ("no_std") ("default" "std") ("alloc"))))))

