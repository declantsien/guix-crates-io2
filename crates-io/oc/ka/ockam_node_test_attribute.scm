(define-module (crates-io oc ka ockam_node_test_attribute) #:use-module (crates-io))

(define-public crate-ockam_node_test_attribute-0.1.0 (c (n "ockam_node_test_attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dnxpps31l0744fym6mcp6p5b9k774mk2s47ad6bd4gla1j1g0lg") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_test_attribute-0.2.0 (c (n "ockam_node_test_attribute") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0s1k9kj0ibls16rpvc01cazrkcksx4s9gay2f86s5z63jjqq9mm6") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_test_attribute-0.3.0 (c (n "ockam_node_test_attribute") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05221hij2sswidm5pd7hrgfhl5ccirfg9zmb7q74ag9i4cs3kzc9") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_node_test_attribute-0.4.0 (c (n "ockam_node_test_attribute") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17xqs1k0ks8w8nl8v7jnjbq62yiqz5zgrz64jw8c0a7mpz0xgjza") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_node_test_attribute-0.5.0 (c (n "ockam_node_test_attribute") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1y9sm1s1kyf25799y8apgdhd04cv4xyk63827cxrar0qg02ps34i") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

