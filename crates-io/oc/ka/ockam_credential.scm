(define-module (crates-io oc ka ockam_credential) #:use-module (crates-io))

(define-public crate-ockam_credential-0.0.0 (c (n "ockam_credential") (v "0.0.0") (h "014qnp8pb0h4h49d2w7cqm6gcx0cld0zxhbj168aw36p4dzw4mpw") (y #t)))

(define-public crate-ockam_credential-0.1.0 (c (n "ockam_credential") (v "0.1.0") (d (list (d (n "heapless") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "ockam_core") (r "^0.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kafska1kpyk8kz89dlad2cplrvhvzdn29fgz7bv9wpwj8w83ihn") (f (quote (("std" "ockam_core/std" "alloc") ("no_std" "heapless") ("default" "std") ("alloc" "serde/alloc")))) (y #t)))

(define-public crate-ockam_credential-0.2.0 (c (n "ockam_credential") (v "0.2.0") (d (list (d (n "bbs") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ff") (r "^0.6") (o #t) (d #t) (k 0) (p "ff-zeroize")) (d (n "heapless") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "ockam_core") (r "^0.3.0") (k 0)) (d (n "pairing-plus") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1sdf1xwz639b47zfp43vdcv58zrbjqfbqdkwsa0yqh3bcxdh6imr") (f (quote (("std" "ockam_core/std" "alloc" "bbs" "digest/std" "ff" "pairing-plus" "sha2/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc" "serde/alloc")))) (y #t)))

