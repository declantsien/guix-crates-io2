(define-module (crates-io oc ka ockam_macro) #:use-module (crates-io))

(define-public crate-ockam_macro-0.1.0 (c (n "ockam_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1id3lf118hdjck47vsaw28prdnbw391ghjvbgjpkimx7a3kfc7d4")))

