(define-module (crates-io oc ka ockam_vault_test_attribute) #:use-module (crates-io))

(define-public crate-ockam_vault_test_attribute-0.1.0 (c (n "ockam_vault_test_attribute") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c7p074zig805wmgkgzfypf2vrkj7b34vvmsiwjgb784rd5p2yy3")))

(define-public crate-ockam_vault_test_attribute-0.2.0 (c (n "ockam_vault_test_attribute") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fr02nm1gp2xk8vkxslh6g8gjnlh50bjz7r0c9048lvs92aprfaq")))

(define-public crate-ockam_vault_test_attribute-0.3.0 (c (n "ockam_vault_test_attribute") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p7qndw513idm76clks2g89kz220nd7f29qdi95rw2nrkkqkx503")))

(define-public crate-ockam_vault_test_attribute-0.3.1 (c (n "ockam_vault_test_attribute") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hr7alxccya4r7c5kr8as2qrllnavw1pyzpk8vvnjnch7f0jhr94")))

(define-public crate-ockam_vault_test_attribute-0.3.2 (c (n "ockam_vault_test_attribute") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pg4bai4lphcybd747xb6lb2qiq79lqf0g892bk2il9gqb9r0556")))

(define-public crate-ockam_vault_test_attribute-0.4.0 (c (n "ockam_vault_test_attribute") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pfwb1bip4hn4rlhdivfzyklhlili4kv4vz8avm0mc8l5g0m7262")))

(define-public crate-ockam_vault_test_attribute-0.5.0 (c (n "ockam_vault_test_attribute") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xn6lqyk75jin0rwhgrbhk38ixnydpmx6kwn5dch61jd55d6s41a")))

(define-public crate-ockam_vault_test_attribute-0.6.0 (c (n "ockam_vault_test_attribute") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xkkmsj5v843zks4chf61q8x7b2n2hkyx1af464sgswwrx4bxvv7")))

(define-public crate-ockam_vault_test_attribute-0.7.0 (c (n "ockam_vault_test_attribute") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "183crhp042nc1vhrb8pxkddvqxplwaj5nkadhrxiq114w8zk7y1j")))

(define-public crate-ockam_vault_test_attribute-0.8.0 (c (n "ockam_vault_test_attribute") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03cr66dgm9sv3br57yyr5ladic6lxl7ab0azxjdf55wrxmmqlaf1")))

(define-public crate-ockam_vault_test_attribute-0.9.0 (c (n "ockam_vault_test_attribute") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yzk8iqgxmipqjhmpyr96x3b0z7d1j3xqvww2pq1ij8wwfigs9fc")))

(define-public crate-ockam_vault_test_attribute-0.10.0 (c (n "ockam_vault_test_attribute") (v "0.10.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05knpxiqzaislqw05whghrk7pxfw2vwj3xrdni104xiqd01i5d6l")))

(define-public crate-ockam_vault_test_attribute-0.11.0 (c (n "ockam_vault_test_attribute") (v "0.11.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sk2jqsgfpb2qdkc50dq5mkdxaghfpc3530v1wh753y8zmlrf9ai")))

(define-public crate-ockam_vault_test_attribute-0.12.0 (c (n "ockam_vault_test_attribute") (v "0.12.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wwv3k5bschkxird9xm19imzcfp0clj5g27illdlccjdz61ba869")))

(define-public crate-ockam_vault_test_attribute-0.13.0 (c (n "ockam_vault_test_attribute") (v "0.13.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i3pb6b0v41hbx5560k9xwbhyck922vlm1pfa9hav3xvi78j0vlm")))

(define-public crate-ockam_vault_test_attribute-0.14.0 (c (n "ockam_vault_test_attribute") (v "0.14.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b55hjz668y2a6z1j7fwmlkpyhmsjs9fbw43xhidpbg0b26hfk76")))

(define-public crate-ockam_vault_test_attribute-0.15.0 (c (n "ockam_vault_test_attribute") (v "0.15.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i6mhh8sr0mbnyc0s543isf2mdvyh0mlhwyd8j2wzhrsar4a19gs")))

(define-public crate-ockam_vault_test_attribute-0.16.0 (c (n "ockam_vault_test_attribute") (v "0.16.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05hbz972x2pjdm2p29hcrc9s97xla8p1147dlf5ylfsnic4q84h3")))

(define-public crate-ockam_vault_test_attribute-0.17.0 (c (n "ockam_vault_test_attribute") (v "0.17.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w1k031f84v8nlwbajxgx1xjdryhc9i95byk40g52mzq8d3nwhna")))

(define-public crate-ockam_vault_test_attribute-0.18.0 (c (n "ockam_vault_test_attribute") (v "0.18.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qpi9qyz5mb5zpc6g65ibkh7jgkzdd807hf612a0kpg3c3ar61fg")))

(define-public crate-ockam_vault_test_attribute-0.19.0 (c (n "ockam_vault_test_attribute") (v "0.19.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rrfx6wx3ycld28bxy3lp5kr0y219zm2na54wiycayzfpj9k7x4v")))

(define-public crate-ockam_vault_test_attribute-0.20.0 (c (n "ockam_vault_test_attribute") (v "0.20.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d3pn5fk93ykzmgybpcmd0d0sykrczq8xakb14crnkq22cy77pk8")))

(define-public crate-ockam_vault_test_attribute-0.21.0 (c (n "ockam_vault_test_attribute") (v "0.21.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k7sqhxrda68ak47hy8z2rd984w41lymw9kwffg41i9f18193fjx")))

(define-public crate-ockam_vault_test_attribute-0.22.0 (c (n "ockam_vault_test_attribute") (v "0.22.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xgglpz7650b57xma0g5j26zjg5xni8cnbgpc5wbhlqimza7mhg5")))

(define-public crate-ockam_vault_test_attribute-0.23.0 (c (n "ockam_vault_test_attribute") (v "0.23.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11cblq88225bv38z8yds26mc5zc7fwvpb3rzq8r2dxm021z5acnm")))

(define-public crate-ockam_vault_test_attribute-0.24.0 (c (n "ockam_vault_test_attribute") (v "0.24.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cckk70amqkki75jkzdlfka27gsp14ygipwmn7pgj5xjadqc8nr4")))

(define-public crate-ockam_vault_test_attribute-0.25.0 (c (n "ockam_vault_test_attribute") (v "0.25.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gl53xgq8bwn1l7dwqbjyya391hccpc7431wvvca6vw0mddl5vf3")))

(define-public crate-ockam_vault_test_attribute-0.26.0 (c (n "ockam_vault_test_attribute") (v "0.26.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c6a8brvkj9qaqn1y4ga7gdbflsi0msfnkcp1c4v3vwxwz4cspb7")))

(define-public crate-ockam_vault_test_attribute-0.27.0 (c (n "ockam_vault_test_attribute") (v "0.27.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12yhw4vzasx3qm242468m6h2smrs0iy4yzz0ml9li49jbwij3dyd")))

(define-public crate-ockam_vault_test_attribute-0.28.0 (c (n "ockam_vault_test_attribute") (v "0.28.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16zllwx8b8pwd8byss6zxkbg9nnijyr846jpbggirx1qpa6kh5hb")))

(define-public crate-ockam_vault_test_attribute-0.29.0 (c (n "ockam_vault_test_attribute") (v "0.29.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bkyp4hrgqzfm03m6bjmk9c0lcy1q3wz4j3k67rajbnwm002y554")))

