(define-module (crates-io oc ka ockam_macros) #:use-module (crates-io))

(define-public crate-ockam_macros-0.2.0 (c (n "ockam_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nc6jkig1x6nyh76h52h5cj3nkv5jz042mcgrg3m7janqw2hac7y") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_macros-0.3.0 (c (n "ockam_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i9434y54hnka1sacqmznh64dmxrylhxi11vn9sxy4hv899vzi7w") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_macros-0.4.0 (c (n "ockam_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02wgd12ckidl7zgkkg305q1hi2981ymphchrdq0927sliaxbspcm") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.5.0 (c (n "ockam_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ik4ffs6n6ifid1az2f53hll17i1azj4080gnbzrpng9lnch4c3v") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.6.0 (c (n "ockam_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sl926wcjxsfjw0yy85nvnqawigricqqsaf05qa5faw9ccdqsvrn") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.7.0 (c (n "ockam_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sqayx222bxj6jyvjld2lanpmghi4cn6s8p5fglwpv25i5cx9152") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.8.0 (c (n "ockam_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pf24xhnyn166cbfy7rjyg1amnw3dcj3zavh7l99kj3q2ic2a88q") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.9.0 (c (n "ockam_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07vai2gkchjchjmjwjwmk4wqa9kmm6hrm5if2jl8lmnh48iv629c") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.10.0 (c (n "ockam_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dcc6dsy10179qg1gji1k7494p0r019i3ihnq05fa6mzbz3cr08j") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.11.0 (c (n "ockam_macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18fn5mqj0bzv8cr6bxn0dkj5r126dinqy04wjzk4dhjyz0l3aw5d") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.12.0 (c (n "ockam_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04r48ydnh636bhr4v4gx4cipvx2a0zh9b5vy1i25wfzgvplrszjk") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.13.0 (c (n "ockam_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15fh9ncfvh12k23szimpkrb91f91jrgk45sq6ivkpbqz2ywh4wzh") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.14.0 (c (n "ockam_macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y0p1yv3awryzsbiaqnsrnj51w296d5f8iz5cnjl15ql5c40y2sr") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.15.0 (c (n "ockam_macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w6yx2n3w5ac1fr0vsvpn9sdnbxphwz8gp4gyn7j9j832wlfnd77") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.16.0 (c (n "ockam_macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "197m05gvw0l1dzd86bp7hw0xv59dqmpy8xrw55av03b18ykbcpz6") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.18.0 (c (n "ockam_macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d2xvv1mlw6drigf78qc9zrrqg7ny9fv8ka2x2ywvq5s6lwzxilc") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.19.0 (c (n "ockam_macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zixs05j85ir5myj7jl9bsz2phxy02h7xa6lxl1glv52j22ar1sc") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-ockam_macros-0.20.0 (c (n "ockam_macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09mqmx4z24yvglws3sn756q7gq23p25plcd9a450rpy91kw5vnd2") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.21.0 (c (n "ockam_macros") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "014z97md6bmmwryxrjk5i490asvq0y6bf73105b4bicg3468ngqb") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.22.0 (c (n "ockam_macros") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z98pfy58ifk6b0slacxha805gzagxpz866cak2imh86ffbcgl5v") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.23.0 (c (n "ockam_macros") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0llds6az0i72gi2yvj3wddam5kx0i9frrg49is0pi643685kzv2c") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.24.0 (c (n "ockam_macros") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02gjy9ywgr5vnpqf2mq1259znk2k7zhshmqhq61if6c359xa5pgd") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.25.0 (c (n "ockam_macros") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4q85zlw8vznzw4zrbfyi4mj035854inpnrdk6h22f8g3vr1rf2") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.26.0 (c (n "ockam_macros") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5q23psif4rl0ffww3pxmwchibvn46fsxvhg9bf8kp0i78wvm1b") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.27.0 (c (n "ockam_macros") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrj5rwm8pjfgdbmgvclqxglbag4kj54655p1yyk39i5l12v85lj") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.28.0 (c (n "ockam_macros") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cljxgp3yix5s5dmsnblqgi5lpvslaiagcg5pamcl39pn7qlqzfr") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.29.0 (c (n "ockam_macros") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vq4ggnlmxgdr6n1yv8z5k7x43bw78pgl0f4gg6cd64n9425c1sy") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.30.0 (c (n "ockam_macros") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi2msjhhzzscskd6x67jjqa1qxzhypyq028dmgpw8xaqis4cyny") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.31.0 (c (n "ockam_macros") (v "0.31.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16rs880iyxwlcv9c0h0gjcq7zc06v05dimmaihvlpcdhafapvqdr") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.32.0 (c (n "ockam_macros") (v "0.32.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kdynk4gqf7iain06a3nsil92qfv6h7ln6cnlg0kxi2dmzw9iiwb") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.33.0 (c (n "ockam_macros") (v "0.33.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fb7rqwjlwgknqwsfxwja7bhzgl65iyrfwff0dmhnllrpc56dkk5") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

(define-public crate-ockam_macros-0.34.0 (c (n "ockam_macros") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zkpxcsggzj2wn9m8q9pf3allch8l8c21jp0fbz4gksp5gz5zch3") (f (quote (("std" "alloc") ("no_std") ("default") ("alloc")))) (r "1.56.0")))

