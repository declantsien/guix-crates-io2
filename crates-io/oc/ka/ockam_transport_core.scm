(define-module (crates-io oc ka ockam_transport_core) #:use-module (crates-io))

(define-public crate-ockam_transport_core-0.1.0 (c (n "ockam_transport_core") (v "0.1.0") (d (list (d (n "ockam_core") (r "^0.27.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1q4z2b150dbrisvhmcla6fkd0sk297n21vab20af9xbvcpxwsr9b")))

(define-public crate-ockam_transport_core-0.2.0 (c (n "ockam_transport_core") (v "0.2.0") (d (list (d (n "ockam_core") (r "^0.28.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "042l9dm2ndyhz2wjwzi6kj352v4s1s9gs6l13mwa6y5dlqc7738m")))

(define-public crate-ockam_transport_core-0.3.0 (c (n "ockam_transport_core") (v "0.3.0") (d (list (d (n "ockam_core") (r "^0.29.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1a3jpkgzvq7cg6bcr32w3vrl8aif3im70zm1xrhjbqj2drvxx4mc")))

(define-public crate-ockam_transport_core-0.4.0 (c (n "ockam_transport_core") (v "0.4.0") (d (list (d (n "ockam_core") (r "^0.30.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bgx00g33qpym2z10c6ywqbx5lli9siqicm5mk898w5f4q5x2pf7")))

(define-public crate-ockam_transport_core-0.5.0 (c (n "ockam_transport_core") (v "0.5.0") (d (list (d (n "ockam_core") (r "^0.31.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12fr908mip3w21xjdb317wdvmvapal3830v144l88nyqa6b5q0mi")))

(define-public crate-ockam_transport_core-0.6.0 (c (n "ockam_transport_core") (v "0.6.0") (d (list (d (n "ockam_core") (r "^0.32.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1ynlkq4yk54s7xag7x63acnhy3dv4lk8z04ajfaway20jl7nni7s")))

(define-public crate-ockam_transport_core-0.7.0 (c (n "ockam_transport_core") (v "0.7.0") (d (list (d (n "ockam_core") (r "^0.33.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "11ldza130s4zjjfadn13qa6ff642419714bzj8467vk4sr068xqf")))

(define-public crate-ockam_transport_core-0.8.0 (c (n "ockam_transport_core") (v "0.8.0") (d (list (d (n "ockam_core") (r "^0.34.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0f5l83c7b5msvkvysi1r0pmm1v681i61lfihr3gfwppwmpgzhvhn")))

(define-public crate-ockam_transport_core-0.9.0 (c (n "ockam_transport_core") (v "0.9.0") (d (list (d (n "ockam_core") (r "^0.35.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "098pai9azyspk6jd3qs5vf4dhq2qvaiswsyv468wi41fxk8l27la") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.10.0 (c (n "ockam_transport_core") (v "0.10.0") (d (list (d (n "ockam_core") (r "^0.36.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1q1rr3zjipq2dcn8af6sjnm50jp39s94sd183cjzjsm7nk8flcf1") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.11.0 (c (n "ockam_transport_core") (v "0.11.0") (d (list (d (n "ockam_core") (r "^0.37.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1s6yqkls7a7rzs0my34rka56zajvbkqnzbzd9vz9mnn5pdlsxh7i") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.12.0 (c (n "ockam_transport_core") (v "0.12.0") (d (list (d (n "ockam_core") (r "^0.38.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "133hw47kavyk71v5fvh3ir3jr468r07kjv8gm2w7vbcq1i0276xd") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.13.0 (c (n "ockam_transport_core") (v "0.13.0") (d (list (d (n "ockam_core") (r "^0.39.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1vx27kd4np6w1fbl111kv1s8han5k9k9x6p90p030b5yv75yyn3l") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.14.0 (c (n "ockam_transport_core") (v "0.14.0") (d (list (d (n "ockam_core") (r "^0.40.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "16mw972527if1fg7hr5d5ka1diwwkripms9aahw1diqb8ss89sd1") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.15.0 (c (n "ockam_transport_core") (v "0.15.0") (d (list (d (n "ockam_core") (r "^0.41.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0nyqa0jlyyddhkk3nfk6s2gcmmqi6s3s2nm9yvjv5ika55czq9db") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.16.0 (c (n "ockam_transport_core") (v "0.16.0") (d (list (d (n "ockam_core") (r "^0.42.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0y051j33vyqgl8959lhg5yhy2xfisp065yjaq3dqwfa0nkxb0lgx") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc"))))))

(define-public crate-ockam_transport_core-0.17.0 (c (n "ockam_transport_core") (v "0.17.0") (d (list (d (n "ockam_core") (r "^0.43.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0gz0jigr0wycfsmnk6yx5rc3wcq7d2lag5bbdjbwrf1m9q7mmv0b") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.18.0 (c (n "ockam_transport_core") (v "0.18.0") (d (list (d (n "ockam_core") (r "^0.44.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1g6i2yhnaqfsfqxm616b2i0f490al9kmmklkwshfngbi4syd2szi") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.19.0 (c (n "ockam_transport_core") (v "0.19.0") (d (list (d (n "ockam_core") (r "^0.45.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1i6gkji3xgic2ps4dg3ay5nhhw4184bqhkfyfa3pakls2yrxzw8l") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.20.0 (c (n "ockam_transport_core") (v "0.20.0") (d (list (d (n "ockam_core") (r "^0.46.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0kd3lj763h4n5d2w7rnn3x994k8z0f96i2dvvxzsnws5w419prfa") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.21.0 (c (n "ockam_transport_core") (v "0.21.0") (d (list (d (n "ockam_core") (r "^0.48.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "166w6skrsza8zsc0df0ngzfm2dhgp3hr9n3mqvqi790m13x6b4k8") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.22.0 (c (n "ockam_transport_core") (v "0.22.0") (d (list (d (n "ockam_core") (r "^0.49.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0hzpcz1diby05fddwayr7z90hdkhg22320x47ims5xs4wxbrbid8") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.23.0 (c (n "ockam_transport_core") (v "0.23.0") (d (list (d (n "ockam_core") (r "^0.50.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "19kq1km0i17slwfkw0bbgd1w2q4iz5p764knklmjnv0scx4nkl38") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.24.0 (c (n "ockam_transport_core") (v "0.24.0") (d (list (d (n "ockam_core") (r "^0.51.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0wxsssxi60jwf5iam0kcqhn4fd4b5slnhvz5y6j674kp30y1sh23") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.25.0 (c (n "ockam_transport_core") (v "0.25.0") (d (list (d (n "ockam_core") (r "^0.52.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1g2s5qq2kybimjc65nmrdp0bvgig2gf90gy6w2w3qcklwirsnmxp") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.26.0 (c (n "ockam_transport_core") (v "0.26.0") (d (list (d (n "ockam_core") (r "^0.53.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1rzbzrxq58dzjvkkvy1i3nsadganczjx9y7lwpxawq8jn2vs648z") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.27.0 (c (n "ockam_transport_core") (v "0.27.0") (d (list (d (n "ockam_core") (r "^0.54.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0rya46nh2ir3im9dc0h8phjggz79l58a14djs1fcwxnkl2ij2890") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.28.0 (c (n "ockam_transport_core") (v "0.28.0") (d (list (d (n "ockam_core") (r "^0.55.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "12h7ynqqpiiks3xy1hn6aqk2sfgw924ia8v0bn3l3kx9jw6k2a14") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.29.0 (c (n "ockam_transport_core") (v "0.29.0") (d (list (d (n "ockam_core") (r "^0.56.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1hywhlax3p0hb1xnhf53qmy13qdn0qiyf2ff9bb3fbwvl1lh7j3r") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.30.0 (c (n "ockam_transport_core") (v "0.30.0") (d (list (d (n "ockam_core") (r "^0.57.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1yb4q3kajjyf9a236kwdp28bvrgr9mg6wfmafpy2f2apcffslqdf") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.31.0 (c (n "ockam_transport_core") (v "0.31.0") (d (list (d (n "ockam_core") (r "^0.58.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0scw6gx4b8mys09y3y155cfnwqbwyjplfjsdbwphncnib1frn0zz") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.32.0 (c (n "ockam_transport_core") (v "0.32.0") (d (list (d (n "ockam_core") (r "^0.59.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1l5sqrfbry4i2nnxb6phs6ylk2brimmfncsisgnrf5qkay4303i1") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.34.0 (c (n "ockam_transport_core") (v "0.34.0") (d (list (d (n "ockam_core") (r "^0.61.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "11ppvllgnsq8gv7dxhg5x9fbx0pgndky6ybkahn002mb9p94ycmh") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.35.0 (c (n "ockam_transport_core") (v "0.35.0") (d (list (d (n "ockam_core") (r "^0.62.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0fzdqa6k3ka5fpy7bnn7l5f2g70jqqwbk58cfyaypmf337v1vx4n") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.37.0 (c (n "ockam_transport_core") (v "0.37.0") (d (list (d (n "ockam_core") (r "^0.64.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1fn7bs1v62qcmnkalbcnjzf544yh2gjdw25xphg5zi7i3j737myc") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.38.0 (c (n "ockam_transport_core") (v "0.38.0") (d (list (d (n "ockam_core") (r "^0.65.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "15ia6pwgdgcb5krsp8jypf9rbzda3yasxi5cppbvx5a8qkljcj8c") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (y #t) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.39.0 (c (n "ockam_transport_core") (v "0.39.0") (d (list (d (n "ockam_core") (r "^0.66.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1b549zjz7ghlb8sdgmaijj44k7xyp9d54db4a4fpabn5m2nm29sb") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.40.0 (c (n "ockam_transport_core") (v "0.40.0") (d (list (d (n "ockam_core") (r "^0.67.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "14sghl1fqvig1q571l628jkd4l1awsbm7gfl3vv45qdvwnm48nwa") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.41.0 (c (n "ockam_transport_core") (v "0.41.0") (d (list (d (n "ockam_core") (r "^0.68.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1zwwsjr6bb5nga70y8xqia8ry4896ndfw2aq4v83hwa32ipb3nnj") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.42.0 (c (n "ockam_transport_core") (v "0.42.0") (d (list (d (n "ockam_core") (r "^0.69.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "017skxjfz5ab21bmk9b66m1cwhh2fvf8bx565kjlyc0xrjni76l6") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.43.0 (c (n "ockam_transport_core") (v "0.43.0") (d (list (d (n "ockam_core") (r "^0.70.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "06a3n0ib488hlfm066iqkpgnz7c0akdv0wh3fi5qaf2l3ql37ks8") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.45.0 (c (n "ockam_transport_core") (v "0.45.0") (d (list (d (n "ockam_core") (r "^0.72.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "11asj9s2k1d8h9jhwzrz18mp1kaqpgaaf6hwf4yjvhalrk5frrrz") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.46.0 (c (n "ockam_transport_core") (v "0.46.0") (d (list (d (n "ockam_core") (r "^0.73.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1yqvfxnjf4r0wiciw751r31y10bwpg90110yyidxkvwzh19mi01w") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.47.0 (c (n "ockam_transport_core") (v "0.47.0") (d (list (d (n "ockam_core") (r "^0.74.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0g8znzsmp15zhfqpmwsg6sw94xzfzxl14ai2md7v3jiz4fbl4q79") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.48.0 (c (n "ockam_transport_core") (v "0.48.0") (d (list (d (n "ockam_core") (r "^0.75.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0gi2lmsgsnb26indkfbxdv7jcmrclq0zv56a5v6i13bh6xym8sif") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.49.0 (c (n "ockam_transport_core") (v "0.49.0") (d (list (d (n "ockam_core") (r "^0.76.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0z3zifja95cy6djwby1d6ypsin30kxms1671n7vy35igz7mhnf9c") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.50.0 (c (n "ockam_transport_core") (v "0.50.0") (d (list (d (n "ockam_core") (r "^0.77.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1gl5pcx0hbqxza1b3qxgx80baxvk6qp8k15mhds44ngzf4d2djyb") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.51.0 (c (n "ockam_transport_core") (v "0.51.0") (d (list (d (n "ockam_core") (r "^0.78.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1wfpkbbksnn505byxrr8sdvbql7n27pppa3i28dgmqj30lnzr738") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.52.0 (c (n "ockam_transport_core") (v "0.52.0") (d (list (d (n "ockam_core") (r "^0.79.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1a75sgadnwzv7cvjl93vqfmbmg6asl33aq68flcmcd0df7jf054h") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.53.0 (c (n "ockam_transport_core") (v "0.53.0") (d (list (d (n "ockam_core") (r "^0.80.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1dcsl7gdksdy3jl5yfmsk9nlgzcsx3s5hfzrcsc9gmwcl231z1c6") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.54.0 (c (n "ockam_transport_core") (v "0.54.0") (d (list (d (n "ockam_core") (r "^0.81.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0jij5swyfm3qf8w2850dbjvfkkj7bshsbqg127jywll493l1wpsj") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.55.0 (c (n "ockam_transport_core") (v "0.55.0") (d (list (d (n "ockam_core") (r "^0.82.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "117hhg85qyv1ajgaz1qynshh1rfrvyjyk3nm7lhs6hykxb56rbdx") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.56.0 (c (n "ockam_transport_core") (v "0.56.0") (d (list (d (n "ockam_core") (r "^0.83.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0w9pmvwrwvjk9ka8nvmhq83ljx5zmzsza9mayhcq0mv0hbyy50nz") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.57.0 (c (n "ockam_transport_core") (v "0.57.0") (d (list (d (n "ockam_core") (r "^0.84.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1vxd8cbr0hqgyrvhnnssv0ll7ilc0jibw4i0kn5hxf38qzch55f0") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.58.0 (c (n "ockam_transport_core") (v "0.58.0") (d (list (d (n "ockam_core") (r "^0.85.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1j87dp91x411hi59sm8zsg66wq3wa7rikank07y7si4ws0lqjvah") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.59.0 (c (n "ockam_transport_core") (v "0.59.0") (d (list (d (n "ockam_core") (r "^0.86.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1vids6ixndpya5bmkqvrrg3r6s2zawl2gqx07gb8syq00i4drxjy") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.60.0 (c (n "ockam_transport_core") (v "0.60.0") (d (list (d (n "ockam_core") (r "^0.87.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0h78mlqdd8iadlsry7n3vb4jn0nynbmhp3jrw0wa1n8xxknd92bg") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.61.0 (c (n "ockam_transport_core") (v "0.61.0") (d (list (d (n "ockam_core") (r "^0.88.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1391f72qay1fxd08k4w71bp4abb3y6bwpsij3x8al0riiwpqnxsl") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.62.0 (c (n "ockam_transport_core") (v "0.62.0") (d (list (d (n "ockam_core") (r "^0.89.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "03r3p3d9kzmrplz95h98r5ck45y7x30gnjkpf50nnr97mndipawh") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.63.0 (c (n "ockam_transport_core") (v "0.63.0") (d (list (d (n "ockam_core") (r "^0.90.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0kqpkx55b7mcligrg49h55mj1bv34wjvr41gvwlm816zpqzkrhzp") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.64.0 (c (n "ockam_transport_core") (v "0.64.0") (d (list (d (n "ockam_core") (r "^0.91.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1xil1v3z552spzrw92bqx5k1j9kl7aw8dclps1ff773v8c99idsj") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.65.0 (c (n "ockam_transport_core") (v "0.65.0") (d (list (d (n "ockam_core") (r "^0.92.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1s3hg04lkqd8q9nnj9i5xlbba8xbbqxlf9kxn6wphp8y43ckz3c0") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.66.0 (c (n "ockam_transport_core") (v "0.66.0") (d (list (d (n "ockam_core") (r "^0.93.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0db0xanaik73zaxx3qi5xbmrj935xsrx915qxf3qjx6crzi1lxz7") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.67.0 (c (n "ockam_transport_core") (v "0.67.0") (d (list (d (n "ockam_core") (r "^0.94.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1bnj02smxja6vqq2h9pqh4x5xzqjvm9rdlkggrh7ncr1zs14lbz9") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.68.0 (c (n "ockam_transport_core") (v "0.68.0") (d (list (d (n "ockam_core") (r "^0.95.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1lqvg65bxl2pqqa9k4yjggzcsicspsrml1ccakaahkkh3vg6di5l") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.69.0 (c (n "ockam_transport_core") (v "0.69.0") (d (list (d (n "ockam_core") (r "^0.96.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "08yk2wbvkxwq27wjq95grwn41nnj9dk68saa63x93hxm42k928p3") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.70.0 (c (n "ockam_transport_core") (v "0.70.0") (d (list (d (n "ockam_core") (r "^0.97.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "13lssykw1ry4nck92r7lpdgc50862a53r53aja4zk42ikm04wvgn") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.71.0 (c (n "ockam_transport_core") (v "0.71.0") (d (list (d (n "ockam_core") (r "^0.98.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "17zxypc71in0bajh9z0wgj0labxzsv4xr7x2icdyqwwawfcpzaqw") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.72.0 (c (n "ockam_transport_core") (v "0.72.0") (d (list (d (n "ockam_core") (r "^0.99.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1w8myz4nxmhcc8gl7kg70k066crlilynrqsg26vj8a2qhvhrsahf") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.73.0 (c (n "ockam_transport_core") (v "0.73.0") (d (list (d (n "ockam_core") (r "^0.100.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0x1pzzdv4gh595qn8f48299wldxfnjqh31nlr5vv70y0p3wsb0dk") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.74.0 (c (n "ockam_transport_core") (v "0.74.0") (d (list (d (n "ockam_core") (r "^0.101.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "05a2xyd94gnxlniklzf2vmpm22v1rl72yqdqldg32f739g1i0pdj") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.75.0 (c (n "ockam_transport_core") (v "0.75.0") (d (list (d (n "ockam_core") (r "^0.102.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0kyz3mdw2za93cxi8iv5a0gzwkim95i06aic94cmq3w9y6a6kdya") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.76.0 (c (n "ockam_transport_core") (v "0.76.0") (d (list (d (n "ockam_core") (r "^0.103.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0vk0klph8xwqznyrjm5n0f1vzxzibbxskskpdn15z29k289ikbz0") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.77.0 (c (n "ockam_transport_core") (v "0.77.0") (d (list (d (n "ockam_core") (r "^0.104.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1xr75z372jsp974kzgnk74l6anikfjzjxygnln98i5fkqix1psld") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.78.0 (c (n "ockam_transport_core") (v "0.78.0") (d (list (d (n "ockam_core") (r "^0.105.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "15hm091iydignmakn4by3z2zhr79s580ibsrkav5mdlxkx69wgi9") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.79.0 (c (n "ockam_transport_core") (v "0.79.0") (d (list (d (n "ockam_core") (r "^0.106.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0xd0y7jsr08pbm2zw40wy7b2m4alqgwifh2wawiv2pw6y2d1zpzk") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.80.0 (c (n "ockam_transport_core") (v "0.80.0") (d (list (d (n "ockam_core") (r "^0.107.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1hay73sjblyg1pypwbhfxblb19ggslajdcb8a474c6j5nkgkf4bq") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.81.0 (c (n "ockam_transport_core") (v "0.81.0") (d (list (d (n "ockam_core") (r "^0.108.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "16nj5gwkg5958ydzrml96whvw09n2mqaxn60fx8sbxfr7ny3fsrq") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

(define-public crate-ockam_transport_core-0.82.0 (c (n "ockam_transport_core") (v "0.82.0") (d (list (d (n "ockam_core") (r "^0.109.0") (k 0)))) (h "10hz7afvdlnrwyvx0cg3yzx1hw8vgqvilj73h0xcbcdwcf6avlig") (f (quote (("std" "ockam_core/std") ("no_std" "ockam_core/no_std") ("default" "std") ("alloc" "ockam_core/alloc")))) (r "1.56.0")))

