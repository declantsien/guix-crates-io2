(define-module (crates-io oc ka ockam_vault_test_suite) #:use-module (crates-io))

(define-public crate-ockam_vault_test_suite-0.1.0 (c (n "ockam_vault_test_suite") (v "0.1.0") (d (list (d (n "ockam_core") (r "^0.10.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.4.0") (d #t) (k 0)))) (h "1z8ly466vmb5r1ha9ldl76mydyrqw07b1f54s4m5nvqbbpl9j48p")))

(define-public crate-ockam_vault_test_suite-0.1.1 (c (n "ockam_vault_test_suite") (v "0.1.1") (d (list (d (n "ockam_core") (r "^0.11.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.4.1") (d #t) (k 0)))) (h "1vb38cqws50qc9mg4kd5px1rxnzz1vmi8cf6nv8i1q2wa6ba2dzq")))

(define-public crate-ockam_vault_test_suite-0.1.2 (c (n "ockam_vault_test_suite") (v "0.1.2") (d (list (d (n "ockam_core") (r "^0.11.1") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.5.1") (d #t) (k 0)))) (h "0sxll8cc2cawjwhjznzsxhhs5j07v3imxghafqkhf4lf9ayb6s88")))

(define-public crate-ockam_vault_test_suite-0.1.3 (c (n "ockam_vault_test_suite") (v "0.1.3") (d (list (d (n "ockam_core") (r "^0.11.2") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.5.2") (d #t) (k 0)))) (h "1kwcssb1vkhpzs6x9g7wxf9327ww7rlrd9l6cq8scvaas05h5ylh")))

(define-public crate-ockam_vault_test_suite-0.1.4 (c (n "ockam_vault_test_suite") (v "0.1.4") (d (list (d (n "ockam_core") (r "^0.12.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.6.0") (d #t) (k 0)))) (h "08vcw5plfv814rnm14aj7f8qy8cs4ri8vfxwnzqjkbxh48pyib2s")))

(define-public crate-ockam_vault_test_suite-0.2.0 (c (n "ockam_vault_test_suite") (v "0.2.0") (d (list (d (n "ockam_core") (r "^0.13.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.7.0") (d #t) (k 0)))) (h "1drk1axym107srm5mmig2lq3vp2w9y6n0sav4l7q9nqgkq138azc")))

(define-public crate-ockam_vault_test_suite-0.3.0 (c (n "ockam_vault_test_suite") (v "0.3.0") (d (list (d (n "ockam_core") (r "^0.14.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.8.0") (d #t) (k 0)))) (h "1pahdyi4b6ryx6d4cwpm7v4kjrrx640imkszj90269xmpyd9f0j7")))

(define-public crate-ockam_vault_test_suite-0.4.0 (c (n "ockam_vault_test_suite") (v "0.4.0") (d (list (d (n "ockam_core") (r "^0.15.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.9.0") (d #t) (k 0)))) (h "160655fpwi9c4kvgp76i2g3ihgrhm0j2mjwbqp0969ls8hdkz6q8")))

(define-public crate-ockam_vault_test_suite-0.5.0 (c (n "ockam_vault_test_suite") (v "0.5.0") (d (list (d (n "ockam_core") (r "^0.16.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.10.0") (d #t) (k 0)))) (h "1gz3afg8ixvgb2nsa8l5w6d2axg4w4wz09zgfyvn7lzl0mpaw1ai")))

(define-public crate-ockam_vault_test_suite-0.6.0 (c (n "ockam_vault_test_suite") (v "0.6.0") (d (list (d (n "ockam_core") (r "^0.17.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.11.0") (d #t) (k 0)))) (h "0hbblcqy9wpflqbans9baj51j8jawsyviv7yhcbp47c571zmz830")))

(define-public crate-ockam_vault_test_suite-0.7.0 (c (n "ockam_vault_test_suite") (v "0.7.0") (d (list (d (n "ockam_core") (r "^0.18.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.12.0") (d #t) (k 0)))) (h "08c31lyilfcmdryyysv9brfhpz6dkv522n1jlqj5m14q5mdzm6pj")))

(define-public crate-ockam_vault_test_suite-0.8.0 (c (n "ockam_vault_test_suite") (v "0.8.0") (d (list (d (n "ockam_core") (r "^0.19.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.13.0") (d #t) (k 0)))) (h "0slahvxsgyckbhjk9rd2rx03kra2049wsxa1j898qjj7vlkx2b79")))

(define-public crate-ockam_vault_test_suite-0.9.0 (c (n "ockam_vault_test_suite") (v "0.9.0") (d (list (d (n "ockam_core") (r "^0.20.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.14.0") (d #t) (k 0)))) (h "0b1fc1a4jaxmh9acsb3blr5wnabf243yfx8r0kcvq1b1w7y6qgsp")))

(define-public crate-ockam_vault_test_suite-0.10.0 (c (n "ockam_vault_test_suite") (v "0.10.0") (d (list (d (n "ockam_core") (r "^0.21.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.15.0") (d #t) (k 0)))) (h "00w4l2ljw0mz51n2afn4yijdjvwa85nsqckqls5zx0xdyhxa9s7l")))

(define-public crate-ockam_vault_test_suite-0.11.0 (c (n "ockam_vault_test_suite") (v "0.11.0") (d (list (d (n "ockam_core") (r "^0.22.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.16.0") (d #t) (k 0)))) (h "0m76c193xg1xpb665sx38pb95123ycfl92lwk3vjg2n4bgnnj0gh")))

(define-public crate-ockam_vault_test_suite-0.12.0 (c (n "ockam_vault_test_suite") (v "0.12.0") (d (list (d (n "ockam_core") (r "^0.23.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.17.0") (d #t) (k 0)))) (h "08a7lkxrnwq7wz6k9h71kh99fm5zkwf0w400db6r2fpdlsjdsf32")))

(define-public crate-ockam_vault_test_suite-0.13.0 (c (n "ockam_vault_test_suite") (v "0.13.0") (d (list (d (n "ockam_core") (r "^0.24.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.18.0") (d #t) (k 0)))) (h "0s6gh0k9v02bqwwbg9q6w2w40s89mklsvafhikw8b0zz2g1k9yvz")))

(define-public crate-ockam_vault_test_suite-0.14.0 (c (n "ockam_vault_test_suite") (v "0.14.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.25.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.19.0") (d #t) (k 0)))) (h "0j2jbnwml7rmwg3ykhgl0jb8fxwk2smm1wyv9927bwf898ljxhld")))

(define-public crate-ockam_vault_test_suite-0.15.0 (c (n "ockam_vault_test_suite") (v "0.15.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.26.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.20.0") (d #t) (k 0)))) (h "0vv5j2i8m0jdm61sbqmmzfm2d1jn5ja66c4gy666md36ca7h581p")))

(define-public crate-ockam_vault_test_suite-0.16.0 (c (n "ockam_vault_test_suite") (v "0.16.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.27.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.21.0") (d #t) (k 0)))) (h "1flx6dd6npckgxgkncpi4s93a4r76wb09dq92mha6c18x408gw4w")))

(define-public crate-ockam_vault_test_suite-0.17.0 (c (n "ockam_vault_test_suite") (v "0.17.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.28.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.22.0") (d #t) (k 0)))) (h "1d0r6gnh10a8jh09lkl1r472wb2624bpk6icg1ss3m3c03hr2ixm")))

(define-public crate-ockam_vault_test_suite-0.18.0 (c (n "ockam_vault_test_suite") (v "0.18.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.29.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.23.0") (d #t) (k 0)))) (h "11m5cyrf2ckajx1024wjx3ng0hnjnn4x202vvsxw6abqk699qly6")))

(define-public crate-ockam_vault_test_suite-0.19.0 (c (n "ockam_vault_test_suite") (v "0.19.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.30.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.24.0") (d #t) (k 0)))) (h "1f4n7n2sxp9pknvam5v3qrw2jv148mk39aiicni0iy408293mk7h")))

(define-public crate-ockam_vault_test_suite-0.20.0 (c (n "ockam_vault_test_suite") (v "0.20.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.31.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.25.0") (d #t) (k 0)))) (h "172arfkd1psjlfq03ynd7ax5vh8xbz06imf7zrvch4rv7yd6jz2a")))

(define-public crate-ockam_vault_test_suite-0.21.0 (c (n "ockam_vault_test_suite") (v "0.21.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.32.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.26.0") (d #t) (k 0)))) (h "105vmc2jj1kkxyq23a9si7wmd86ahlflp814gbz5y66d16rl54aj")))

(define-public crate-ockam_vault_test_suite-0.22.0 (c (n "ockam_vault_test_suite") (v "0.22.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.33.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.27.0") (d #t) (k 0)))) (h "0lpk7f0b2h8sl6axj07f50wjivf9fgc63qbcrxy8r9kv48fdjxzx")))

(define-public crate-ockam_vault_test_suite-0.23.0 (c (n "ockam_vault_test_suite") (v "0.23.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.34.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.28.0") (d #t) (k 0)))) (h "18h6ar8l618pfx7xj4i4ikda773pfz2mzwmi4d0kln41wjzd5f1w")))

(define-public crate-ockam_vault_test_suite-0.24.0 (c (n "ockam_vault_test_suite") (v "0.24.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.35.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.29.0") (d #t) (k 0)))) (h "182vacmfbj1jjrd8pawvzpsiqrj98diyjd1ydykcpsxssjz6d4j0")))

(define-public crate-ockam_vault_test_suite-0.25.0 (c (n "ockam_vault_test_suite") (v "0.25.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.36.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.30.0") (d #t) (k 0)))) (h "16dzlmxm6wzkn3q90f8jhrbjbq6b743g68w0lylypzism549jdxs")))

(define-public crate-ockam_vault_test_suite-0.26.0 (c (n "ockam_vault_test_suite") (v "0.26.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.37.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.31.0") (d #t) (k 0)))) (h "0fwn6c7b1byvv843zl6hfsqfarrp1b96a16yncxdf7d96mddavi0")))

(define-public crate-ockam_vault_test_suite-0.27.0 (c (n "ockam_vault_test_suite") (v "0.27.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.38.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.32.0") (d #t) (k 0)))) (h "1yl1ww2wgmfjpvwa0ajqvgdglm6fh737982r54swsig7xsiy4kr9")))

(define-public crate-ockam_vault_test_suite-0.28.0 (c (n "ockam_vault_test_suite") (v "0.28.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.39.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.33.0") (d #t) (k 0)))) (h "1c8n30xg9gq2lm1db5vfdfshqhxxwgcpdz37q5719kk1zss1f8r4")))

(define-public crate-ockam_vault_test_suite-0.29.0 (c (n "ockam_vault_test_suite") (v "0.29.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.40.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.34.0") (d #t) (k 0)))) (h "062z2k22kd88s2nrwjr088msc4ldrs1kp373b7h403i9xaxrph0k")))

(define-public crate-ockam_vault_test_suite-0.30.0 (c (n "ockam_vault_test_suite") (v "0.30.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.41.0") (d #t) (k 0)) (d (n "ockam_vault_core") (r "^0.35.0") (d #t) (k 0)))) (h "1v3ji4n6r2ks7zcr0c9iv5ica4rdy0dh8sph753babhgasazcams")))

(define-public crate-ockam_vault_test_suite-0.31.0 (c (n "ockam_vault_test_suite") (v "0.31.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.42.0") (d #t) (k 0)))) (h "0zws5s2sd12alscsz01g22a16j6l4a4k8c68xzrnjd15z3q50dzq")))

(define-public crate-ockam_vault_test_suite-0.32.0 (c (n "ockam_vault_test_suite") (v "0.32.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.43.0") (d #t) (k 0)))) (h "167s8q1d93vi94vz3yhmsn4jd2m1sipqc7lrm59zyjbfkf20qqgp") (r "1.56.0")))

(define-public crate-ockam_vault_test_suite-0.33.0 (c (n "ockam_vault_test_suite") (v "0.33.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.44.0") (d #t) (k 0)))) (h "11fmgb7kh19w4vcmps827g2srg3a19nf6v97plf8jvdmxiw3hlfp") (r "1.56.0")))

(define-public crate-ockam_vault_test_suite-0.34.0 (c (n "ockam_vault_test_suite") (v "0.34.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.45.0") (d #t) (k 0)))) (h "0rln9ghqvka2lklnm9mamvds83pq6xjjr6nyq2c849mkjskp2dvd") (r "1.56.0")))

(define-public crate-ockam_vault_test_suite-0.35.0 (c (n "ockam_vault_test_suite") (v "0.35.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.46.0") (d #t) (k 0)))) (h "0wgp1c8s2r7fm64i11if77bq57c0ihabdpiyabvdcrx6jm889v3v") (r "1.56.0")))

(define-public crate-ockam_vault_test_suite-0.36.0 (c (n "ockam_vault_test_suite") (v "0.36.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.48.0") (d #t) (k 0)))) (h "0va4ipkigm8plp391k3svhyh4wy72kkb99b5p7qmk5kb43188xh4") (r "1.56.0")))

(define-public crate-ockam_vault_test_suite-0.37.0 (c (n "ockam_vault_test_suite") (v "0.37.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ockam_core") (r "^0.49.0") (d #t) (k 0)))) (h "1imd6hj5c93n35ynziq6fj2m32kxz10wqv7jgdqwjglndspwik2f") (r "1.56.0")))

