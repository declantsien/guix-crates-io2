(define-module (crates-io oc ka ockam_node_attribute) #:use-module (crates-io))

(define-public crate-ockam_node_attribute-0.1.0 (c (n "ockam_node_attribute") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xrzfrgxjia6ln8kddyw8kh2y9z0klcnllrvj9259987pcs7d7hc") (y #t)))

(define-public crate-ockam_node_attribute-0.1.1 (c (n "ockam_node_attribute") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18cq3fii8jn59d927rm7srfwvbwxh6c8i19iyix0ccaahr9x0mdj") (y #t)))

(define-public crate-ockam_node_attribute-0.1.2 (c (n "ockam_node_attribute") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qs9vhawadn3fb7ns4vdnkq5yr0ilm0b9ndlvwbc12wh0g8ax61n")))

(define-public crate-ockam_node_attribute-0.1.3 (c (n "ockam_node_attribute") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j3pdsx4pnc6mr0jn61y0and0vwnpiylag8ca7502g7gai28a9xk")))

(define-public crate-ockam_node_attribute-0.1.4 (c (n "ockam_node_attribute") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rrw738x9wlhcgipg9qschd292hv166n2ry8mg5l708ql8i06nyx")))

(define-public crate-ockam_node_attribute-0.1.5 (c (n "ockam_node_attribute") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15rbmqpl4hz30rqrhp2dbmfgisjgr87w3lxpbykgc09liih5srw9")))

(define-public crate-ockam_node_attribute-0.1.6 (c (n "ockam_node_attribute") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pv54kg7isyfyf4znrmg2h2l01z4qvkwylq88pz58pgwpj7wr68b")))

(define-public crate-ockam_node_attribute-0.1.7 (c (n "ockam_node_attribute") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pm42x2jsn2avva7wq4w4d4rp8z12p7mkjjizsf8lnysinrdlj4c")))

(define-public crate-ockam_node_attribute-0.1.8 (c (n "ockam_node_attribute") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hifjrg5m1hmyvk09fv7ink6sf1pc57w3j2mdyb148mch6cv1yzq")))

(define-public crate-ockam_node_attribute-0.2.0 (c (n "ockam_node_attribute") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "082pyzkcbs0if0skk6wff9h9hk0cxd8j1fl5p2zcz8bkliyv27b1")))

(define-public crate-ockam_node_attribute-0.3.0 (c (n "ockam_node_attribute") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fkmvij6m6x169r1bm9n6z8jwx2znfacl8d51zgqmvgb1xqc03z0")))

(define-public crate-ockam_node_attribute-0.4.0 (c (n "ockam_node_attribute") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qv2qm57pd5har6vzzq83vjkrhrchy8kd0w4rj4a8x72k196z3kv")))

(define-public crate-ockam_node_attribute-0.5.0 (c (n "ockam_node_attribute") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13jbr4myjpdbhbzcya1l5ndnny5571ql1ha3ivhdrqn8w0rql3sc")))

(define-public crate-ockam_node_attribute-0.6.0 (c (n "ockam_node_attribute") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1saycqkxgcbrz1xnsy70mnxr9jb92imkcvd8fqx74yg5xkmvwq94")))

(define-public crate-ockam_node_attribute-0.7.0 (c (n "ockam_node_attribute") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "022gs5mdj6lay00xvlqxdr3gn5msg3gcbnyslvfzwanzmqilzknc")))

(define-public crate-ockam_node_attribute-0.8.0 (c (n "ockam_node_attribute") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f2rdqq2h33s1ndnz2hmcsxa6fiwpy1cx2s647v5x8bklfwxjrjx")))

(define-public crate-ockam_node_attribute-0.9.0 (c (n "ockam_node_attribute") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mg23qw7kqfvaa9i5bp451csaz0jafyxn43vp21sl31ccxa64m7x")))

(define-public crate-ockam_node_attribute-0.10.0 (c (n "ockam_node_attribute") (v "0.10.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s5glg403llmawp26wz7i7fb5hzaji9qagajqpr8nxp986hp65i8")))

(define-public crate-ockam_node_attribute-0.11.0 (c (n "ockam_node_attribute") (v "0.11.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fzag6qzd6qzvaqmcz6kf69a2nd2gghdmcqx2964vwss9bk1fi9q")))

(define-public crate-ockam_node_attribute-0.12.0 (c (n "ockam_node_attribute") (v "0.12.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ci5y6w34irmiy6cy9kmbhzsr6yb1q8bja0qxn9j0gmwhyhxaqw2")))

(define-public crate-ockam_node_attribute-0.13.0 (c (n "ockam_node_attribute") (v "0.13.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18yfg9b4sd12xzw6agm9biskr1pziqcnmfqdiayhh31b011s2ndz")))

(define-public crate-ockam_node_attribute-0.14.0 (c (n "ockam_node_attribute") (v "0.14.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10iybkm53c1fqm9g66qp71158gq63b4ry10lxly2llxmhc2hn4v3")))

(define-public crate-ockam_node_attribute-0.15.0 (c (n "ockam_node_attribute") (v "0.15.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aszmkh4mg5dhp9r058gbwszyarvxq3ibv845cznjliba6apn1g6") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.16.0 (c (n "ockam_node_attribute") (v "0.16.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nll8kbarkiwicbqcvf435ibzxvxwzgrv8ivpc7nasq94m78ani0") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.17.0 (c (n "ockam_node_attribute") (v "0.17.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pyglvjr8fmfxc5672xvih40f637fd5k7vi33p83qvpjwb6rq6kh") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.18.0 (c (n "ockam_node_attribute") (v "0.18.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m54nh8smcp6k70vf8qxh0j06y6yx5an4vi257g733q81sj1x5qh") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.19.0 (c (n "ockam_node_attribute") (v "0.19.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rgnyxqg9cagjy672k9kjb8ch5ymrbhs516dfiqm99ck43z3h6a5") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.20.0 (c (n "ockam_node_attribute") (v "0.20.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w9cg0jgx6h28mi22nm3h1w1dda5w4z2w8jynbdayj02qvlmzbyq") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.21.0 (c (n "ockam_node_attribute") (v "0.21.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j7vgdlh15dij9bxil46jqxsx541zzlhavaksjzw2hs12b6gzbzm") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.22.0 (c (n "ockam_node_attribute") (v "0.22.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xnvaychah5h15inlgzy50ghml9yx8h8svry3vbvchjmn4akn541") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.23.0 (c (n "ockam_node_attribute") (v "0.23.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0id05dpykv4kyzwaqk7sb72m68vr5bfyhgpqq0j1fvs87709qfzy") (f (quote (("std") ("no_std") ("default" "std") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.24.0 (c (n "ockam_node_attribute") (v "0.24.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aas5n6hq0l7x7a15288n8yli45lx3hw49a5wa4m3nnlirw22bmy") (f (quote (("no_main") ("default"))))))

(define-public crate-ockam_node_attribute-0.25.0 (c (n "ockam_node_attribute") (v "0.25.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03r97ylmngg844y0c07hjbr01sblfs7c24ckazpi13d3471d327g") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.26.0 (c (n "ockam_node_attribute") (v "0.26.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "119qrsrrx8ijdvs3nlm889jj706qcl5h1ak5hcsri2yr989v6kcl") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

(define-public crate-ockam_node_attribute-0.27.0 (c (n "ockam_node_attribute") (v "0.27.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04ia634yx7m17wgndf28i8qdynr9f43lvxy0i9q1iwxna6dx84vi") (f (quote (("std" "alloc") ("no_std") ("no_main") ("default") ("alloc"))))))

