(define-module (crates-io oc ka ockam_signature_core) #:use-module (crates-io))

(define-public crate-ockam_signature_core-0.1.0 (c (n "ockam_signature_core") (v "0.1.0") (d (list (d (n "bls12_381_plus") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "ff") (r "^0.9") (d #t) (k 0)) (d (n "group") (r "^0.9") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "heapless") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)) (d (n "typenum") (r "^1.13") (d #t) (k 0)))) (h "0gyzmy9qccy9yx550phjlyx7pi56fh9wjx686jjf6yjhrv3prb2a") (y #t)))

