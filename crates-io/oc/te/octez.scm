(define-module (crates-io oc te octez) #:use-module (crates-io))

(define-public crate-octez-0.1.1 (c (n "octez") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "0k4svrh92xjrrq9y9yn7cn21yjdv3w044j52dh5jw8lmpm0mxqak") (y #t)))

(define-public crate-octez-0.1.0-alpha.0 (c (n "octez") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "12fwiipgwc75izizswcgpdpl347908bg0rh1bmwqpiqyzwl3hgpp")))

