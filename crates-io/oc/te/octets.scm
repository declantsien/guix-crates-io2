(define-module (crates-io oc te octets) #:use-module (crates-io))

(define-public crate-octets-0.1.0 (c (n "octets") (v "0.1.0") (h "03948fdngdinazf8gbqrr89sbjiiq6l2ydhs8b644r6phgn0da9z")))

(define-public crate-octets-0.2.0 (c (n "octets") (v "0.2.0") (h "002hs18023rsdpwwjrzg71hxpryl6scgh2hlcc53mm14lz6z4x1s")))

(define-public crate-octets-0.3.0 (c (n "octets") (v "0.3.0") (h "168xxcix9gxrx9n7rg3im3z25vn9znjinwvnc28yh7i7j6h8768h")))

