(define-module (crates-io oc tr octree) #:use-module (crates-io))

(define-public crate-octree-0.0.6 (c (n "octree") (v "0.0.6") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)) (d (n "vecfx") (r "^0.0.8") (d #t) (k 0)))) (h "1c35chys2q03lwjfzvfz2b1ihg4djfhralp1w7cmhcg62nm5kic5")))

(define-public crate-octree-0.1.0 (c (n "octree") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)) (d (n "vecfx") (r "^0.1") (d #t) (k 0)))) (h "0jdnks283ap2ran4z30r8lxyknkzwm0s9xk7ayhk79828j8srd5c")))

