(define-module (crates-io oc as ocastabare) #:use-module (crates-io))

(define-public crate-ocastabare-0.1.0 (c (n "ocastabare") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.6") (f (quote ("ws"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "memory-stats") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-webpki-roots"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20") (f (quote ("rustls-tls-webpki-roots"))) (d #t) (k 0)))) (h "0iyc5yj0v4rv2cpdf8w7flkd74fbjyk65blck36srl8rpgl4x420")))

