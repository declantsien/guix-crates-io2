(define-module (crates-io oc l- ocl-macros) #:use-module (crates-io))

(define-public crate-ocl-macros-0.0.1 (c (n "ocl-macros") (v "0.0.1") (d (list (d (n "ocl") (r "^0.19.7") (d #t) (k 0)))) (h "0ms07knykgk8rag9qs0bvhyv7ikkgp2ksysjnpmh6amlrciy4p4p")))

(define-public crate-ocl-macros-0.0.2 (c (n "ocl-macros") (v "0.0.2") (d (list (d (n "ocl") (r "^0.19.7") (d #t) (k 0)))) (h "14qcqm92h0gsbc3kaisr94fdjzq4ka4ij662pgqy66g6x2qiy71x")))

(define-public crate-ocl-macros-0.0.3 (c (n "ocl-macros") (v "0.0.3") (d (list (d (n "ocl") (r "^0.19.7") (d #t) (k 0)))) (h "1nls3ipj57517xrwcazlc8kai6z71g76j7aw2n9074qvnfp5mild")))

(define-public crate-ocl-macros-0.0.4 (c (n "ocl-macros") (v "0.0.4") (d (list (d (n "ocl") (r "^0.19.7") (d #t) (k 0)))) (h "1hz4pqmx1fk2k59wxxi6y5qbyvygdbyj2yy9yl3km15lvfklk480")))

(define-public crate-ocl-macros-0.0.5 (c (n "ocl-macros") (v "0.0.5") (d (list (d (n "ocl") (r "^0.19.7") (d #t) (k 0)))) (h "0by0xwazf7ajscciwysyspn4cz1k4jd5swm2554i9r2jbi08hscb")))

