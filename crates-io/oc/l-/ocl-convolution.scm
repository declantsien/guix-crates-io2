(define-module (crates-io oc l- ocl-convolution) #:use-module (crates-io))

(define-public crate-ocl-convolution-0.1.0 (c (n "ocl-convolution") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ocl") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 2)))) (h "16r5j7fnygsbfm2dqr8mdgjxnbfp1dapymfkkarcshjl4r7jkx80")))

(define-public crate-ocl-convolution-0.2.0 (c (n "ocl-convolution") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "0fqbyhl7w4735004zwm1p03ydvdzn6n66j26118zdcykcf1bgh9k")))

(define-public crate-ocl-convolution-0.3.0 (c (n "ocl-convolution") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0xw5mak1bckpmqbx57b0ky7dpzm5q41xbr5znmxhb2z9c9gx2x2g") (r "1.57")))

