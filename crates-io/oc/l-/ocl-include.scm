(define-module (crates-io oc l- ocl-include) #:use-module (crates-io))

(define-public crate-ocl-include-0.2.0 (c (n "ocl-include") (v "0.2.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1nyy3h2ylw29nrlf5n18l2223x1nn3cwiwsshqgynfrgwwj8rvgi")))

(define-public crate-ocl-include-0.3.0 (c (n "ocl-include") (v "0.3.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1df2xh1wjb3czvmm3s8l07qpp70sn47g37mdvdkdlmx53xb9r77d")))

(define-public crate-ocl-include-0.3.1 (c (n "ocl-include") (v "0.3.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fckkbp3b4ylg8vb6g8rhbq1wbqrwqsw451nylbbv7lf7hskv403")))

(define-public crate-ocl-include-0.3.2 (c (n "ocl-include") (v "0.3.2") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "111zp84c0yvpwwc14g66yf9d0nqgmwgj0zdlrcb2513ycmi93hrw")))

(define-public crate-ocl-include-0.3.3 (c (n "ocl-include") (v "0.3.3") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12zd7s8nrglllqidb5i2482db78f9m6bc12mmpydf87qxwb72sk9")))

(define-public crate-ocl-include-0.3.4 (c (n "ocl-include") (v "0.3.4") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pr8wjj56rzai2q0wwqnm7dkwff1vz178dvb623sjffng4fa60xh")))

(define-public crate-ocl-include-0.4.0 (c (n "ocl-include") (v "0.4.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qnj1j3m0ymfzz9ayfc6s65g4xils2jyrblvp1kbzyiv8cab266d")))

(define-public crate-ocl-include-0.4.1 (c (n "ocl-include") (v "0.4.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "013qij6r3n3shpf0ppgdrlg7rl4gy3abgfybwm3410z4130k8fjf")))

(define-public crate-ocl-include-0.5.1 (c (n "ocl-include") (v "0.5.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uni-path") (r "^1") (d #t) (k 0)))) (h "095l6c81jp8n2dxl6ylayzz1h8aprdirycp9njwkszj8myddy9ps")))

(define-public crate-ocl-include-0.6.0 (c (n "ocl-include") (v "0.6.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zvxjyqj4vg3accvr9jmbaxynl8955ilhwfczswciybivd6kwpna")))

