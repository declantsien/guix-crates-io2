(define-module (crates-io oc l- ocl-core-vector) #:use-module (crates-io))

(define-public crate-ocl-core-vector-0.1.0 (c (n "ocl-core-vector") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1zgyr3rw29q8m8d61w7zyxcbrzkhmx8yrvdbqppasn4rfch2j1xl")))

(define-public crate-ocl-core-vector-0.1.1 (c (n "ocl-core-vector") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0x40xg52aqpmvgdkil6riz5mdymr9ry6zbsyxnp618bc0jg2fqpm")))

