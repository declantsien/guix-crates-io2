(define-module (crates-io oc l- ocl-stream) #:use-module (crates-io))

(define-public crate-ocl-stream-0.1.0 (c (n "ocl-stream") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2.5") (d #t) (k 0)))) (h "0ixiziv19yqjan31ybh29s7r9jpdxdsq8k2pac0x819qnr6dflws")))

(define-public crate-ocl-stream-0.2.0 (c (n "ocl-stream") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "05wc8k42j7qgmr6bzkkal45gmj2bizw1xqis522xw10kvqy7sbm8")))

(define-public crate-ocl-stream-0.2.1 (c (n "ocl-stream") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0g0azpky33wqc1f2si7k11qwqrlk1sxwpj2hwhdb6a8ywxpcldhg")))

(define-public crate-ocl-stream-0.3.0 (c (n "ocl-stream") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0q69h5va5abdg4xi4sny6433r9yyp01789ryyayh65hfb3fc0wb5")))

(define-public crate-ocl-stream-0.3.1 (c (n "ocl-stream") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "18hy3dwxi44kmgczfadsyiak0g1x2x3abfg14i7svapwn66264qg")))

(define-public crate-ocl-stream-0.3.2 (c (n "ocl-stream") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0lc5sm5bai613sw6s0ra04xma8pggny9xj9jbly02ag1cm82c714")))

(define-public crate-ocl-stream-0.3.3 (c (n "ocl-stream") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0zz7m41kl48r3blzbx3b5jb7lsdrv41msd41lm1vdsh3p1fak5pf")))

(define-public crate-ocl-stream-0.3.4 (c (n "ocl-stream") (v "0.3.4") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1isalmhdwiffbjam3h3xr5387lc0a2h32hmx0sksm38sx7007h1c")))

(define-public crate-ocl-stream-0.3.5 (c (n "ocl-stream") (v "0.3.5") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "12xs3dnsn15lk9f20lp5fggpyfbqfcxzkqh496j95iri8lpi013q")))

