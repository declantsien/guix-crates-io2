(define-module (crates-io oc ho ocho-gato) #:use-module (crates-io))

(define-public crate-ocho-gato-0.1.0 (c (n "ocho-gato") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (k 1)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "typify") (r "^0.0.12") (d #t) (k 1)))) (h "1mn0ifi9vb3m7wklyicawnw2nr9b76kffx1pgjj7873zadvsva9y")))

