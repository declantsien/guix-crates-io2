(define-module (crates-io oc -w oc-wasm-sys) #:use-module (crates-io))

(define-public crate-oc-wasm-sys-0.1.0 (c (n "oc-wasm-sys") (v "0.1.0") (d (list (d (n "ordered-float") (r "^2.5") (k 0)))) (h "05aynibxbv5k90ycv2m17d1525xk6zbrsyms7z6bq40fj4g1jmxa")))

(define-public crate-oc-wasm-sys-0.2.0 (c (n "oc-wasm-sys") (v "0.2.0") (d (list (d (n "ordered-float") (r "^3") (k 0)))) (h "1x9j43c06sgf3r0cbx1xqxxvj7914358zskwczrs30y4smknbmzy")))

(define-public crate-oc-wasm-sys-0.2.1 (c (n "oc-wasm-sys") (v "0.2.1") (d (list (d (n "ordered-float") (r "^3") (k 0)))) (h "13qzbg8vx0f2567yqhh41v30i0c8da1lsrj837lkbykdhg2kfk6x")))

(define-public crate-oc-wasm-sys-0.12.0 (c (n "oc-wasm-sys") (v "0.12.0") (d (list (d (n "ordered-float") (r "^4") (k 0)))) (h "0lhjfx7n9qi2bl0k8hipmka4b3m2gszj5ilbwhcal0lxhgzsv8q7")))

