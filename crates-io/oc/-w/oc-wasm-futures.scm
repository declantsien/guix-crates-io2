(define-module (crates-io oc -w oc-wasm-futures) #:use-module (crates-io))

(define-public crate-oc-wasm-futures-0.1.0 (c (n "oc-wasm-futures") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc"))) (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)) (d (n "ordered-float") (r "^2.5") (k 0)))) (h "0bq8q3gws5arm0qic1wr9z4dvqx4bmcn6pxzr28h57bvw5g8ncpx") (f (quote (("proper-waker") ("default" "proper-waker"))))))

(define-public crate-oc-wasm-futures-0.2.0 (c (n "oc-wasm-futures") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc"))) (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)) (d (n "ordered-float") (r "^2.5") (k 0)))) (h "1sw29wwdpdqwk20y88cxga8h7kxw2gma12kvhzh8lx4ixqb0b3jz") (f (quote (("proper-waker") ("default" "proper-waker"))))))

(define-public crate-oc-wasm-futures-0.3.0 (c (n "oc-wasm-futures") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc"))) (k 0)) (d (n "oc-wasm-safe") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "13dc9xg8z0m5c5ymc2rwzip5pcvhig6frbl50ghjpp0r7bb1apw6") (f (quote (("proper-waker") ("default" "proper-waker"))))))

(define-public crate-oc-wasm-futures-0.4.0 (c (n "oc-wasm-futures") (v "0.4.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc"))) (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "0wy210yx53lc848s28nwjmmd97j12ck7b5dz2spz8dfwg6dznj6v") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc"))))))

(define-public crate-oc-wasm-futures-0.5.0 (c (n "oc-wasm-futures") (v "0.5.0") (d (list (d (n "minicbor") (r "^0.18") (k 0)) (d (n "oc-wasm-safe") (r "^0.3") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "03rgs6viynfrpy3hc2sv1bjjd9p235d4pcdgii4cmyzd64bhf98i") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc" "minicbor/alloc"))))))

(define-public crate-oc-wasm-futures-0.6.0 (c (n "oc-wasm-futures") (v "0.6.0") (d (list (d (n "minicbor") (r "^0.19") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "1i8g7c4mrk2jmm12rs5d4k0gii8lvlrykaxkckn8jb1li2zsq7yr") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc" "minicbor/alloc"))))))

(define-public crate-oc-wasm-futures-0.6.1 (c (n "oc-wasm-futures") (v "0.6.1") (d (list (d (n "minicbor") (r "^0.19") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "0d5bvgraxjaldicplq52yl0f8xnfqkb2gksn7pz0mcfspimb5265") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc" "minicbor/alloc"))))))

(define-public crate-oc-wasm-futures-0.7.0 (c (n "oc-wasm-futures") (v "0.7.0") (d (list (d (n "minicbor") (r "^0.20") (k 0)) (d (n "oc-wasm-safe") (r "^0.5") (k 0)) (d (n "ordered-float") (r "^3") (k 0)))) (h "19gnlh38a2d184m9rqprh9mrv9b3xddb5k5ngjd82174r83bhdnd") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc" "minicbor/alloc"))))))

(define-public crate-oc-wasm-futures-0.12.0 (c (n "oc-wasm-futures") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.21") (k 0)) (d (n "oc-wasm-safe") (r "^0.12") (k 0)) (d (n "ordered-float") (r "^4") (k 0)))) (h "1f1l3qk3awgmk8b8v8kasqivajbfv3gs4v0x3p3fj6gvzn5yqxzk") (f (quote (("proper-waker" "alloc") ("default" "alloc" "proper-waker") ("alloc" "minicbor/alloc"))))))

