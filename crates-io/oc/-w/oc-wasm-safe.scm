(define-module (crates-io oc -w oc-wasm-safe) #:use-module (crates-io))

(define-public crate-oc-wasm-safe-0.1.0 (c (n "oc-wasm-safe") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.9") (k 0)) (d (n "oc-wasm-sys") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^2.5") (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "0s5kw63as4ky37948s7sc003ld45b2fqggm7clll0xcs700xf15l") (f (quote (("std") ("panic") ("default" "panic" "std")))) (y #t)))

(define-public crate-oc-wasm-safe-0.1.1 (c (n "oc-wasm-safe") (v "0.1.1") (d (list (d (n "minicbor") (r "^0.9") (k 0)) (d (n "oc-wasm-sys") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^2.5") (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "0ibzsl5i6xg9mraiqlv0ynga7pwsicjhqbl469yv8czbi5niymp3") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.2.0 (c (n "oc-wasm-safe") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.15") (k 0)) (d (n "oc-wasm-sys") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "13jcgrcp96wjyididxvhd006c7zmbwn796wc0mg2nw5n5f4cyy1g") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.2.1 (c (n "oc-wasm-safe") (v "0.2.1") (d (list (d (n "minicbor") (r "^0.15") (k 0)) (d (n "oc-wasm-sys") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1ppjyzcdhi5hk3a2ijk9rc8p8slshhkmjlqp8q9ca6j2mpxzc9kd") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.3.0 (c (n "oc-wasm-safe") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.18") (k 0)) (d (n "oc-wasm-sys") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0qpa851gjh89r51navpdv7fq9pisc67354dfs4mr3m19wbc6wzc4") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.3.1 (c (n "oc-wasm-safe") (v "0.3.1") (d (list (d (n "minicbor") (r "^0.18") (k 0)) (d (n "oc-wasm-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1a5f1b3xawzaklyyj02d2ydl7qqhx9m2lpd52cddrdax2k5xyfx0") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.4.0 (c (n "oc-wasm-safe") (v "0.4.0") (d (list (d (n "minicbor") (r "^0.19") (k 0)) (d (n "oc-wasm-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1f36iqaxqzq2k74aav59lmkabd8pcv7iw2z2cijz0g6gajqdhvgl") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.5.0 (c (n "oc-wasm-safe") (v "0.5.0") (d (list (d (n "minicbor") (r "^0.20") (k 0)) (d (n "oc-wasm-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0pqgyb3cig8jjkcl34rikb6hp4f2w16nb555vsjp4i1qx8pkizld") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

(define-public crate-oc-wasm-safe-0.12.0 (c (n "oc-wasm-safe") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.21") (k 0)) (d (n "oc-wasm-sys") (r "^0.12") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1i6zwm0bsg0i7gsgzll1451d0rb91xzi8zknbwfixq8vm4h34w05") (f (quote (("std") ("panic") ("default" "panic" "std"))))))

