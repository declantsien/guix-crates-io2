(define-module (crates-io oc -w oc-wasm-opencomputers) #:use-module (crates-io))

(define-public crate-oc-wasm-opencomputers-0.1.0 (c (n "oc-wasm-opencomputers") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.1") (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)))) (h "0nqwijb1v88rl156csxs1q2mrn9sm5q1klsncff2z1942blxa84s") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.2.0 (c (n "oc-wasm-opencomputers") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.1") (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)))) (h "0fs8dbdjql4gqqswl8w1ibn9yhfmvbzgbz6lcf6qvgk4japmy272") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.3.0 (c (n "oc-wasm-opencomputers") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.1") (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)))) (h "0mncx9nd71a672140d215zall788y76zbmnfjr3znzcgiy55snnj") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.3.1 (c (n "oc-wasm-opencomputers") (v "0.3.1") (d (list (d (n "minicbor") (r "^0.9") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.2") (k 0)) (d (n "oc-wasm-safe") (r "^0.1") (k 0)))) (h "0h21cyzh4srq32ryily22dkl5lkkja6fzhmxlf9xw2llwb5w4z2r") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.4.0 (c (n "oc-wasm-opencomputers") (v "0.4.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.3") (k 0)) (d (n "oc-wasm-safe") (r "^0.2") (k 0)))) (h "0mw9nid00j2bzpbkp695as1khk369fifhsascqqbrwlg0vgipl3y") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.4.1 (c (n "oc-wasm-opencomputers") (v "0.4.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.3") (k 0)) (d (n "oc-wasm-safe") (r "^0.2") (k 0)))) (h "1cx7k18q82zyksjkmlk4dgzjph3g0919ha6p2zcvd78hd1iivznn") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.5.0 (c (n "oc-wasm-opencomputers") (v "0.5.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)))) (h "1rpasdg8cl6d2qjrqbfws3sc8j0g9czlk2z3wxd9x3v1ri33ch2i") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.5.1 (c (n "oc-wasm-opencomputers") (v "0.5.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)))) (h "0585lrfdqsv0z0f1dgs607325g3kl8qx5jr8yny0ji3z7kxajkf0") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.6.0 (c (n "oc-wasm-opencomputers") (v "0.6.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-helpers") (r "^0.1") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)))) (h "07g13mnb21q9wbj49pvcz6fjkr9g56a6chvdzyci8hmglbp68qys") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.7.0 (c (n "oc-wasm-opencomputers") (v "0.7.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-helpers") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)))) (h "0mwpcircfazxqdnig9fjyxbk571sgm339hiik5hc2zncqgrpw7x6") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.8.0 (c (n "oc-wasm-opencomputers") (v "0.8.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-helpers") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.2.1") (k 0)))) (h "1kl4c84x2b0dqwbg8qcfwgg6h6qq1si5n4vfyag7k5z7bdfv7c4l") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.9.0 (c (n "oc-wasm-opencomputers") (v "0.9.0") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.5") (k 0)) (d (n "oc-wasm-helpers") (r "^0.3") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.3") (k 0)))) (h "0b0nrpj3cyci18lgfr5317kjbj8jmpdhnpzvqdpfgcfgsq7gv0az") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.10.0 (c (n "oc-wasm-opencomputers") (v "0.10.0") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-helpers") (r "^0.5") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "100ngd21wmgimfgsr0vfxg4356c4d9rkn564qb6bzn5nqzq8jyic") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.10.1 (c (n "oc-wasm-opencomputers") (v "0.10.1") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-helpers") (r "^0.5") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "1b9r9w2dkp6jh5pv2qpbv9q78n0b042s6fv148dix33wyhc199gd") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.10.2 (c (n "oc-wasm-opencomputers") (v "0.10.2") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-helpers") (r "^0.5.1") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "0nyqk471mmcq7wfp9fiwniwkii8i5mc9b6y42k4w7rll07r3g97p") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.11.0 (c (n "oc-wasm-opencomputers") (v "0.11.0") (d (list (d (n "minicbor") (r "^0.20") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.7") (k 0)) (d (n "oc-wasm-helpers") (r "^0.6") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.5") (k 0)))) (h "0mf8078yyh6dg81lfwm1mqbwnhkns0r9vslgbcq7v3ps7inzx4qb") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-opencomputers-0.12.0 (c (n "oc-wasm-opencomputers") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.21") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.12") (k 0)) (d (n "oc-wasm-helpers") (r "^0.12") (d #t) (k 0)) (d (n "oc-wasm-safe") (r "^0.12") (k 0)))) (h "0md3zbrj1di2cz464l4qfrs5wmag1hpb6a4kk9znrrwfmw89qcz3") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

