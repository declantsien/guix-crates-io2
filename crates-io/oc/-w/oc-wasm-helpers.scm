(define-module (crates-io oc -w oc-wasm-helpers) #:use-module (crates-io))

(define-public crate-oc-wasm-helpers-0.1.0 (c (n "oc-wasm-helpers") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("derive"))) (k 0)))) (h "1k0rb977zfd6nc64vg1kl2rn4kvsqdszpgm1rlms5hwwpij2y9za")))

(define-public crate-oc-wasm-helpers-0.2.0 (c (n "oc-wasm-helpers") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "oc-wasm-safe") (r "^0.2") (k 0)))) (h "1q2ngm81rb5pd7ndldb2f972cpih1bmr1g0cpq9zfc12l0797srq") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.3.0 (c (n "oc-wasm-helpers") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.5") (k 0)) (d (n "oc-wasm-safe") (r "^0.3") (k 0)))) (h "1dvp9fgm96pnq38m2c0aixc3m03cq5ph7sdrkfws82277xcvgsdn") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.4.0 (c (n "oc-wasm-helpers") (v "0.4.0") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "0g658rpjiq2k0jys2pjhqiqms7ga2hil69r5j3jk2lrpfvs2c70s") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.5.0 (c (n "oc-wasm-helpers") (v "0.5.0") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "0vjasa3sfm85m4anx49xcp8dd9s2wl25dxb71dj2bd494gkl9n02") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.5.1 (c (n "oc-wasm-helpers") (v "0.5.1") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "0ac3l0yxlnrnwg48a6ya53796jyj9fv911zdi52a67p67bhvszhk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.6.0 (c (n "oc-wasm-helpers") (v "0.6.0") (d (list (d (n "minicbor") (r "^0.20") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.7") (k 0)) (d (n "oc-wasm-safe") (r "^0.5") (k 0)))) (h "0l62n54q2lira7r6p7g7yvyycq1105n398vd1blvisawm354wqid") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-oc-wasm-helpers-0.12.0 (c (n "oc-wasm-helpers") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.21") (f (quote ("derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.12") (k 0)) (d (n "oc-wasm-safe") (r "^0.12") (k 0)))) (h "1kmpggcjv5xgnn8sw0dsmbw5im88a8x6bk4v2xvrsxfs5agij8j2") (f (quote (("default" "alloc") ("alloc"))))))

