(define-module (crates-io oc -w oc-wasm-cassette) #:use-module (crates-io))

(define-public crate-oc-wasm-cassette-0.1.0 (c (n "oc-wasm-cassette") (v "0.1.0") (d (list (d (n "cassette") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-futures") (r "^0.4") (k 0)) (d (n "sync-unsafe-cell") (r "^0.1") (d #t) (k 0)))) (h "0s5p46psbx2gb0ng5a9lrw3nbwpi389r4ymh83sl3s9cqjifz7s3")))

(define-public crate-oc-wasm-cassette-0.2.0 (c (n "oc-wasm-cassette") (v "0.2.0") (d (list (d (n "cassette") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-futures") (r "^0.5") (k 0)) (d (n "sync-unsafe-cell") (r "^0.1") (d #t) (k 0)))) (h "0vfz39hira9q9ck5prfzh7cz4ljh9kvxkka5z15z4rfd0bz85wcw")))

(define-public crate-oc-wasm-cassette-0.3.0 (c (n "oc-wasm-cassette") (v "0.3.0") (d (list (d (n "cassette") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "sync-unsafe-cell") (r "^0.1") (d #t) (k 0)))) (h "1hsfh37ldcr1zy4pvn8zjh5a31n3158j0nwxan5hg4bz2067myiz")))

(define-public crate-oc-wasm-cassette-0.4.0 (c (n "oc-wasm-cassette") (v "0.4.0") (d (list (d (n "cassette") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-futures") (r "^0.7") (k 0)) (d (n "sync-unsafe-cell") (r "^0.1") (d #t) (k 0)))) (h "1hjhskpql1xlvcjnlj8sg6h9hqz53rzw709yf8sfp1b685k79qid")))

(define-public crate-oc-wasm-cassette-0.12.0 (c (n "oc-wasm-cassette") (v "0.12.0") (d (list (d (n "cassette") (r "^0.2") (d #t) (k 0)) (d (n "oc-wasm-futures") (r "^0.12") (k 0)) (d (n "sync-unsafe-cell") (r "^0.1") (d #t) (k 0)))) (h "1yry3ryrw58n4fiq1w3k8nv5hzlzijllivapbsa3jc4w5gmz827l")))

