(define-module (crates-io oc -w oc-wasm-applied-energistics) #:use-module (crates-io))

(define-public crate-oc-wasm-applied-energistics-0.1.0 (c (n "oc-wasm-applied-energistics") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.19") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.6") (k 0)) (d (n "oc-wasm-helpers") (r "^0.5.1") (k 0)) (d (n "oc-wasm-safe") (r "^0.4") (k 0)))) (h "10k0r5zybrx7sn3aj3n74jm2m4qlpxqj7haw32a8x6x2bwx6fpfb") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-applied-energistics-0.2.0 (c (n "oc-wasm-applied-energistics") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.20") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.7") (k 0)) (d (n "oc-wasm-helpers") (r "^0.6") (k 0)) (d (n "oc-wasm-safe") (r "^0.5") (k 0)))) (h "17qlirj499dqz381qq8l1rxdgvq72fgadvbrxbhmj071b57lmfki") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

(define-public crate-oc-wasm-applied-energistics-0.12.0 (c (n "oc-wasm-applied-energistics") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.21") (f (quote ("alloc" "derive"))) (k 0)) (d (n "oc-wasm-futures") (r "^0.12") (k 0)) (d (n "oc-wasm-helpers") (r "^0.12") (k 0)) (d (n "oc-wasm-safe") (r "^0.12") (k 0)))) (h "0pqwqdyajh4z4rrbq4y3p4mjcihadka6g47aq2sw2r9wpchvibfc") (f (quote (("std" "oc-wasm-safe/std") ("default" "std"))))))

