(define-module (crates-io oc hr ochre) #:use-module (crates-io))

(define-public crate-ochre-0.1.0 (c (n "ochre") (v "0.1.0") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)) (d (n "usvg") (r "^0.13.0") (d #t) (k 2)))) (h "0dqy9azzz17lvpknf0c02inrah8k6fk2n5f8qlc2ivad14n88ixs")))

(define-public crate-ochre-0.2.0 (c (n "ochre") (v "0.2.0") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)) (d (n "usvg") (r "^0.13.0") (d #t) (k 2)))) (h "1xhikql3njk3483ip4zlil2a2ywyl3cxkqfd16zahn2q07n57qj8")))

