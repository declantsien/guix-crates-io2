(define-module (crates-io oc ea oceansdb) #:use-module (crates-io))

(define-public crate-oceansdb-0.0.1 (c (n "oceansdb") (v "0.0.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "netcdf") (r "^0.8.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1s5d8g6wj3mx3lb367qmhmkyw7lnvrm37gs65aysl3r6lr0nlp1n") (r "1.73.0")))

