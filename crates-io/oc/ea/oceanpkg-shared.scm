(define-module (crates-io oc ea oceanpkg-shared) #:use-module (crates-io))

(define-public crate-oceanpkg-shared-0.1.0 (c (n "oceanpkg-shared") (v "0.1.0") (h "00jb51h5zb73qcxar33xgklmlwbqhzbd88vjnwjgp17xk44y2ygn")))

(define-public crate-oceanpkg-shared-0.1.1 (c (n "oceanpkg-shared") (v "0.1.1") (h "05gfsb3q2djqh20sw8a03sy3jl3v7nmakj43lcblmqmk6pa85m8n")))

