(define-module (crates-io oc cl occlum-pal) #:use-module (crates-io))

(define-public crate-occlum-pal-0.1.0 (c (n "occlum-pal") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "144bqhs7pkg6a5byg9zvr7fry150k237iyh4afl2gnj286bp7rbd") (l "occlum-pal")))

(define-public crate-occlum-pal-0.1.1 (c (n "occlum-pal") (v "0.1.1") (h "1792lhlrlv9d69yy3i9nfgia9by9cajlkgd9rgn0xnlmnq8x1kkp") (l "occlum-pal")))

(define-public crate-occlum-pal-0.1.2 (c (n "occlum-pal") (v "0.1.2") (h "08vfa4g62jic506ybdjy3546ca6m9ccq0mwcldc2zd6xbiki7w9w") (l "occlum-pal")))

(define-public crate-occlum-pal-0.1.3 (c (n "occlum-pal") (v "0.1.3") (h "0v64dxzd01gcs7dk97giym1nm8fvxyf6vdmglinh0pzdv62ivpgd") (l "occlum-pal")))

(define-public crate-occlum-pal-0.1.4 (c (n "occlum-pal") (v "0.1.4") (h "0dfwl0kmv12k64md8l7ia7vjbxas9c84fd8kyg22q0sf4mnp757s") (l "occlum-pal")))

(define-public crate-occlum-pal-0.1.5 (c (n "occlum-pal") (v "0.1.5") (h "1x7ay3fa88b1n8arn9gb0915albr8345whn8xjqg29n3mnd9y07h") (l "occlum-pal")))

