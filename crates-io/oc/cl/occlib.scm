(define-module (crates-io oc cl occlib) #:use-module (crates-io))

(define-public crate-occlib-0.1.0 (c (n "occlib") (v "0.1.0") (h "0d2gvyspqr4fwc898c0jw4r4bqv840mwgpzha15g5zmshydja6hd")))

(define-public crate-occlib-0.1.1 (c (n "occlib") (v "0.1.1") (h "1p7871lfzig4iybwcivag5xyjg3z76b22qa2g9a5mz9asx700zpk")))

(define-public crate-occlib-0.1.2 (c (n "occlib") (v "0.1.2") (h "19gfky3ir5fv5k4xmw41na8wvy8vjfbiim7fgf44pv095jm1r7qm")))

