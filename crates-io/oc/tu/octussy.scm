(define-module (crates-io oc tu octussy) #:use-module (crates-io))

(define-public crate-octussy-0.2.0 (c (n "octussy") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pk1ajwb85qci5m1lki9v8nc3alm8vla3lsv5n9q1wp64y3rncjv")))

(define-public crate-octussy-0.3.0 (c (n "octussy") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1xfwdgh1cfp93xp31wfg4hib584dhlic12y8vw45grrasddij8cl")))

(define-public crate-octussy-0.3.1 (c (n "octussy") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "06acj9qb9hpa2jfc2zg5k883x9wq0vyrv0b5fh482k2bwzjwl59w")))

(define-public crate-octussy-0.3.2 (c (n "octussy") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0brfxcv9mjj67i9f94nkj5j43hm0xzqi4qxrjr58cs3n7n92kagp")))

(define-public crate-octussy-1.0.0 (c (n "octussy") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "05vj28sf1vv85i3ks0jb7cxayjr13nirj83p5b13pbks1vk7dzq2")))

(define-public crate-octussy-1.0.1 (c (n "octussy") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1pd0897kg8li6gvijrdyqvr3zapq99rbl3sra6rvfhyyrnpgvph2")))

(define-public crate-octussy-1.0.2 (c (n "octussy") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1q86d7fbz42q6sy9fwqbz0k6kslzk5a0rxqflc0hm1yny54rpj1a")))

