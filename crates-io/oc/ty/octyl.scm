(define-module (crates-io oc ty octyl) #:use-module (crates-io))

(define-public crate-octyl-0.1.0 (c (n "octyl") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "1ph8gs910mcd2ck0mvamh2xln2c8mrngxspshc5acx47qn3r3qz1")))

