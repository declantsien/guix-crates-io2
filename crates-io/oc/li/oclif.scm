(define-module (crates-io oc li oclif) #:use-module (crates-io))

(define-public crate-oclif-0.0.1 (c (n "oclif") (v "0.0.1") (h "14c8ksm638nmmbfs0fkl7v1aswggd9fz4mcdqa3z2rfc3l9q22yw")))

(define-public crate-oclif-0.1.0 (c (n "oclif") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1jjphv9c5vrdifrcbirx29w8bilnhkmc1ad2mcqv6dh4q05yiqsw")))

(define-public crate-oclif-0.2.0 (c (n "oclif") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "12xbyvsxbarpvds90jwbpfn2mgsvrqpfwrjkbvsvk9gl0jhdrl69")))

(define-public crate-oclif-0.2.1 (c (n "oclif") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1xywc377r37321wkh3dgsqiwb7c7a14p481v6xgzymllm090g6z1")))

(define-public crate-oclif-0.3.0 (c (n "oclif") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0ymm641bkasrdqaqm7lnp0rlkrpy3hfya58lrfzkkha12a4kv3s1")))

(define-public crate-oclif-0.3.1 (c (n "oclif") (v "0.3.1") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0bk591qar19xaik94vdc8j7p8qfl7c49k396rrhmlbdwip9wvjgy") (y #t)))

(define-public crate-oclif-0.4.0 (c (n "oclif") (v "0.4.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1v46drqjpwmijkndgqy34ywl359p3k22amy67nqdxk5pkvqjgg8m")))

(define-public crate-oclif-0.5.0-alpha (c (n "oclif") (v "0.5.0-alpha") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0aqdi2dhi9gzpzlk5jvxhwlydnbpipiqjqlygz8v8q0yn0whk430") (y #t)))

(define-public crate-oclif-0.5.0-beta (c (n "oclif") (v "0.5.0-beta") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1xdxd4r5zh6rnsxmql2yv3540aj889f3cizl4mipwv4ixbd78n9p") (y #t)))

(define-public crate-oclif-0.6.0-rc.0 (c (n "oclif") (v "0.6.0-rc.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0pn0sg00fjb7miq1rka4gs6a75bf1jsp4s50yhhrgn9b7la4jrzj") (y #t)))

(define-public crate-oclif-0.6.0 (c (n "oclif") (v "0.6.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1skhxgbq2pnrhvgx64v4idrca17ydk5iz1yyjvwgrddd2yzjm9vy") (y #t)))

(define-public crate-oclif-0.5.1 (c (n "oclif") (v "0.5.1") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0v06q3axa3sz5rg2zl59qnkya7h1s8hvd6v1jz87mrr8s9msk0vw") (y #t)))

(define-public crate-oclif-0.5.0 (c (n "oclif") (v "0.5.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1haxzkncbjamlgxzfw64isq5im5l3mjrjfl5wmz8sdx4d4pmfbrn") (y #t)))

(define-public crate-oclif-0.7.0-alpha (c (n "oclif") (v "0.7.0-alpha") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "13csspvk6h572lmwqcxrqhxm43hi00y4v4grh58i8m00194qh4ab") (y #t)))

(define-public crate-oclif-0.7.0-rc.0 (c (n "oclif") (v "0.7.0-rc.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0mi1d31ybchr057rzsz59vk8ki4xrn88mhvq0rld05vdslj1l09n") (y #t)))

(define-public crate-oclif-0.7.1 (c (n "oclif") (v "0.7.1") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1hlcyj96inwdqxdz7jgpvpxyih1mgrpkasa12iprrkbmzfqmqk46") (y #t)))

(define-public crate-oclif-0.7.3 (c (n "oclif") (v "0.7.3") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1rd9h7sqw405ibd7fb7wkjzxra8ydgsdxv8mfaqvy6b5d7494z15") (y #t)))

(define-public crate-oclif-0.7.5-alpha (c (n "oclif") (v "0.7.5-alpha") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "0qnr2rdjqaj9m3jk2gapvy1zfxshcqfacdy428wdxznrkcnsnhi7") (y #t)))

(define-public crate-oclif-0.7.5-beta (c (n "oclif") (v "0.7.5-beta") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "10pac16d50bg15b2p4hrmzd080f9wgp39a3nb6w90x8m4scni0ss") (y #t)))

(define-public crate-oclif-0.7.6-alpha (c (n "oclif") (v "0.7.6-alpha") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "16s47j6z8332kwp136xznsj0807f8xrn8sp6nvrymbgp4a3hbzkk") (y #t)))

(define-public crate-oclif-0.7.6 (c (n "oclif") (v "0.7.6") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1w869xxsxbd7c1amahyqk1g4pwjdlfkjs50l4is087ilnahnhbkv") (y #t)))

