(define-module (crates-io oc li ocli) #:use-module (crates-io))

(define-public crate-ocli-0.1.0 (c (n "ocli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0y18f076wrzx3pf10d5q59z0sbqr2llri3mxlsy1wkd01gmyrlpz")))

(define-public crate-ocli-0.1.1 (c (n "ocli") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-verbosity-flag") (r "^2.1.2") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "17km1ka847p8aafpf4vicqk613p3k2sxa50xbdsn6vy9khdfx8hr")))

(define-public crate-ocli-0.1.2 (c (n "ocli") (v "0.1.2") (d (list (d (n "anstyle") (r "^1.0.6") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-verbosity-flag") (r "^2.1.2") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1gi30fcigq6iq8y2bm00bl53ih06sdxjvijqa8sgi5qilimiiq6h")))

