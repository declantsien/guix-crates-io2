(define-module (crates-io oc li oclis) #:use-module (crates-io))

(define-public crate-oclis-0.1.0 (c (n "oclis") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nickel-lang-core") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1crlh5p1a3vbd6sggicl3vx2gqiwrlhxadc0gcvzgglwc811jc6n")))

(define-public crate-oclis-0.2.0 (c (n "oclis") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nickel-lang-core") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0fl33i4dzzc50ykn69fbzyzrrvmfvkwzmi8j3xm3135la0lpyh97")))

(define-public crate-oclis-0.3.0 (c (n "oclis") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nickel-lang-core") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0i66rrhal5nppf29vj9gcrpghzykrp7bl0f9in1bfnqdas6pjzwh")))

