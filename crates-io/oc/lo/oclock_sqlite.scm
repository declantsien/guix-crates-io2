(define-module (crates-io oc lo oclock_sqlite) #:use-module (crates-io))

(define-public crate-oclock_sqlite-0.1.5 (c (n "oclock_sqlite") (v "0.1.5") (d (list (d (n "diesel") (r "^2.1") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.1") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z3r20bgiwly2x7jh56cwzmmr85zic4qpkrbigav2b47ga4vjg4f")))

