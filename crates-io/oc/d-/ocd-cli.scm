(define-module (crates-io oc d- ocd-cli) #:use-module (crates-io))

(define-public crate-ocd-cli-0.0.1 (c (n "ocd-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "04wxvq4j9afsj4mqica28dzgs2k62nxcplsz91hkl6vl136gz06x")))

