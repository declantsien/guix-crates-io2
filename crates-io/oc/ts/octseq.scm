(define-module (crates-io oc ts octseq) #:use-module (crates-io))

(define-public crate-octseq-0.1.0 (c (n "octseq") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1j43d2iyl4p6y437hxhvf6ir881pj7mjl7l9q3m82p7pjnp5vpwv") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-octseq-0.2.0 (c (n "octseq") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0pd6id3rhrc13cw0i86b25hjn1vbl54v0l5byginwi1q5x4f742i") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-octseq-0.3.0 (c (n "octseq") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1jjhrph2fz7ya8kmndabgrcc2byxiqf9dk2xvbzz9jf1m3zbfcyj") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-octseq-0.3.1 (c (n "octseq") (v "0.3.1") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1fy3pj3vqp2a60zghcn0zfa4nsg1wa1dnrfgcsdmv3i9ik8s62fd") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-octseq-0.3.2 (c (n "octseq") (v "0.3.2") (d (list (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0pac091hdixv0vzfqba2g20ndrqn69ql2f08p0czdb5vmaj3hayr") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "bytes?/std")))) (r "1.65")))

(define-public crate-octseq-0.4.0 (c (n "octseq") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0rlbh2vzlwj4kk8011kbkjivbf7mpqn8zr0ybzg2z9wgs629m3iy") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "bytes?/std")))) (r "1.65")))

(define-public crate-octseq-0.5.0 (c (n "octseq") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0fd5c3zyiyg2dm523whdvikyihy3zbhz04s0601gcylyyfpv8j1l") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "bytes?/std")))) (r "1.65")))

(define-public crate-octseq-0.5.1 (c (n "octseq") (v "0.5.1") (d (list (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "165yd35mzbz6wx713nml5xgl9n9rs0zq9p8mqv0wr61d8pnfmlif") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "bytes?/std")))) (r "1.65")))

