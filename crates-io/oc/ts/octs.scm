(define-module (crates-io oc ts octs) #:use-module (crates-io))

(define-public crate-octs-0.1.0 (c (n "octs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (k 0)))) (h "0znqryd9q2gwkkga1jkc41vqq27pdn62r1l5b9ai9q7bkadlc228") (f (quote (("std" "bytes/std") ("i128") ("default" "std")))) (r "1.76.0")))

(define-public crate-octs-0.2.0 (c (n "octs") (v "0.2.0") (d (list (d (n "bytes") (r "^1.6.0") (k 0)))) (h "19b00qbgsbrvpw4027592dhhbj3xgyd0vii7kagvgf8yhj5w1wf5") (f (quote (("std" "bytes/std") ("i128") ("default" "std")))) (r "1.76.0")))

(define-public crate-octs-0.3.0 (c (n "octs") (v "0.3.0") (d (list (d (n "bytes") (r "^1.6.0") (k 0)))) (h "14s6vb70ci88rnyqrcywsk7r0scq2f69wwzvswnx87r2vx2s0qwz") (f (quote (("std" "bytes/std") ("i128") ("default" "std")))) (r "1.76.0")))

