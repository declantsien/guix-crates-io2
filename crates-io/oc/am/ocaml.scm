(define-module (crates-io oc am ocaml) #:use-module (crates-io))

(define-public crate-ocaml-0.1.0 (c (n "ocaml") (v "0.1.0") (h "1ajxc20w7y1bg45vqpjwb6rv5dlcw5zs7baafkaqficd7r2wl7mp")))

(define-public crate-ocaml-0.1.1 (c (n "ocaml") (v "0.1.1") (h "0w3ls8aing4mfanqj3hhp6cm3x39h6iw5c0g00ll8jrqycar5hpg")))

(define-public crate-ocaml-0.1.3 (c (n "ocaml") (v "0.1.3") (h "0gxs0qpadk471jbddvr4bq2y2z9ypjja8rp5wa5r0jjl3yq9aac8")))

(define-public crate-ocaml-0.2.0 (c (n "ocaml") (v "0.2.0") (h "01576xz123hkwjinfnmkcm884p3zgkjkl7h6m4k1587qbhs86aky")))

(define-public crate-ocaml-0.3.0 (c (n "ocaml") (v "0.3.0") (h "1fz7h6fynfl2vpcdlb0khm4lcnvh5lydjv9f9lmfsdf2c1d4whlm")))

(define-public crate-ocaml-0.3.1 (c (n "ocaml") (v "0.3.1") (h "1f0hg3aayfas2jpg8q3hm97cw0bv70affr7wc7r543arqh4p3p6b")))

(define-public crate-ocaml-0.3.2 (c (n "ocaml") (v "0.3.2") (h "02s9hfkr1nbsvpa6j29pmy6njj9hrsf9lkyyx9x4a3djdw7zfi3v")))

(define-public crate-ocaml-0.3.3 (c (n "ocaml") (v "0.3.3") (h "13281b1zymbyllzdzq0pw26w4k87kbf20f3qp4ii90y49r6k44zs")))

(define-public crate-ocaml-0.3.4 (c (n "ocaml") (v "0.3.4") (h "1kk1vqnmn0bqjwwd5k90mmlz5hy148ayx9b5a9h1m95bq9kbsgif")))

(define-public crate-ocaml-0.3.5 (c (n "ocaml") (v "0.3.5") (h "082a4vhbhkgnjb7yj5zk5704i674has3jka1d0gxl53359bdh6p4")))

(define-public crate-ocaml-0.3.6 (c (n "ocaml") (v "0.3.6") (h "0fvvw987hpy8nyrag9pzxpc3vl7v3zry0swir2q2g1kmjrvxknyp")))

(define-public crate-ocaml-0.3.7 (c (n "ocaml") (v "0.3.7") (h "1rw8nnn76d1648d82sfd51n45gcc1nr276pdxq5w793f3wsl7bn4")))

(define-public crate-ocaml-0.3.8 (c (n "ocaml") (v "0.3.8") (h "1g1l2kx6cvcni0wzkshm70b70sv8cc70whbgcd35ckjfy5ji7q3i")))

(define-public crate-ocaml-0.3.9 (c (n "ocaml") (v "0.3.9") (h "1145yhwqp48kj2hwd62w6kkfhrrz7k5cd94bifchkqhga33q8c0r")))

(define-public crate-ocaml-0.4.0 (c (n "ocaml") (v "0.4.0") (h "091b7qmran1am1kffkczvp27msfhfm7wm2g5akpzfbh46pv4qp0k")))

(define-public crate-ocaml-0.4.1 (c (n "ocaml") (v "0.4.1") (h "1jg3xzxfc2wygrpk6vgj90r5gvlk3a560gcgni53g0filp217r11")))

(define-public crate-ocaml-0.4.2 (c (n "ocaml") (v "0.4.2") (h "1ad8vdzlizqrw4z65a6v6s0vn79znljpb6whjdskf4c4pkgh7j68")))

(define-public crate-ocaml-0.4.3 (c (n "ocaml") (v "0.4.3") (h "0f97pwh2mwylcqimrm49rakmrnwc2pc1kym83p4yw0hj7b1pj37r")))

(define-public crate-ocaml-0.5.0 (c (n "ocaml") (v "0.5.0") (h "1qlp0z00jpyhallmmybmmhp5x3adb2zsjxfjcwrfjgb2kzrc80f6")))

(define-public crate-ocaml-0.5.1 (c (n "ocaml") (v "0.5.1") (h "0yfv05042fkq8axh99gsx5ay69h893zs6kmjysjzdl6mfn3wi5q8")))

(define-public crate-ocaml-0.6.0 (c (n "ocaml") (v "0.6.0") (h "0r11q3l598bfndj8cavsml6sd2n9s85cg0sas6p13a9dz8ks3g0z")))

(define-public crate-ocaml-0.6.1 (c (n "ocaml") (v "0.6.1") (h "1k1c78cw1dhgrxs34ay76jxy54lgbhj4l1j6wl09xynwff5x8yws")))

(define-public crate-ocaml-0.7.0-beta.0 (c (n "ocaml") (v "0.7.0-beta.0") (h "1ih6qvrzd7fyhbrdrq9dqfky13vfnvafv76b4yjp64x8agfsn6x0")))

(define-public crate-ocaml-0.7.0 (c (n "ocaml") (v "0.7.0") (h "020w0jl13r49f1b5rp7ilmpmwxxqfwhmpb9zibg4fam6ypcj0m5k")))

(define-public crate-ocaml-0.8.0 (c (n "ocaml") (v "0.8.0") (h "0rll5i11jwcl4mxsz7l9f32rbi9rm20jfq391ifkqmg3lz5irsxl")))

(define-public crate-ocaml-0.8.1 (c (n "ocaml") (v "0.8.1") (h "0alxbcrz8n4a1s9y3ylfqgx225v7n1g4ys1xg036dyp5hqbli5xl")))

(define-public crate-ocaml-0.9.0 (c (n "ocaml") (v "0.9.0") (h "1czmanprwr8lyzrzxg6d9nldkcmc7z1f9g41msv9d4p1v63dz6sm") (y #t)))

(define-public crate-ocaml-0.9.1 (c (n "ocaml") (v "0.9.1") (h "0g2iwrv49bihnyaa8s4diy9brd44nbp80iqzb20vi86x17px6aj1")))

(define-public crate-ocaml-0.9.2 (c (n "ocaml") (v "0.9.2") (h "14vhxr6v000i4hjs6442n4mnfmxfssa3spw2nqc16cq3qj1z5qbc")))

(define-public crate-ocaml-0.9.3 (c (n "ocaml") (v "0.9.3") (h "0y9ip3krblfdnhm56yb8dwb1pjaimyh59ii12105mvc2dm70y0xn")))

(define-public crate-ocaml-0.10.0 (c (n "ocaml") (v "0.10.0") (d (list (d (n "ocaml-derive") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.10") (d #t) (k 0)))) (h "0bkjmih80g8mymyfa6mryaljvsgkzimcrff8qdf7vqvjxdc7bkil") (f (quote (("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("deep-clone"))))))

(define-public crate-ocaml-0.10.1 (c (n "ocaml") (v "0.10.1") (d (list (d (n "ocaml-derive") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.10") (d #t) (k 0)))) (h "0pw3g35cy5shkriy9kfsnwcwf9b2jmnslkpgxmgh26paj73hycfv") (f (quote (("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("deep-clone"))))))

(define-public crate-ocaml-0.11.0 (c (n "ocaml") (v "0.11.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.11") (d #t) (k 0)))) (h "1m8rjdzg4h1r4nlgj864bz1cb22q48biyiq9nidm4xylqx4s6p26") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.11.1 (c (n "ocaml") (v "0.11.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.11") (d #t) (k 0)))) (h "1bxw7c3fzbx4pngghdc97s1cvq2n45b3m39fiw3qj63rmp88g6s9") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.11.2 (c (n "ocaml") (v "0.11.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.11") (d #t) (k 0)))) (h "1ka4k96j0wqb77984mzinz22xg3cf54q46qnlxf1rlbvpmhnpmp1") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.11.3 (c (n "ocaml") (v "0.11.3") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.11") (d #t) (k 0)))) (h "063i073gx61b3ayylhmbvrcji4nw1qwfx7jcwyb6fksiizyn6kk5") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.12.0 (c (n "ocaml") (v "0.12.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.12") (d #t) (k 0)))) (h "18lfxwrcp0rr2wgnjqirgb6andz0vcv5s3c1lykm737hxaq06696") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.12.1 (c (n "ocaml") (v "0.12.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.12") (d #t) (k 0)))) (h "01x84q55ghy6wcyyxbnagcgcxs570n881l9qg4ar5l9gi4vpv1pp") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.12.2 (c (n "ocaml") (v "0.12.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.12.2") (d #t) (k 0)))) (h "1zzv18hv703yhp0rl7qqz8vbgaxphbm67pc9aa4dgd28dgwsfp9g") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.12.3 (c (n "ocaml") (v "0.12.3") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.12.2") (d #t) (k 0)))) (h "03iiipzdv91kln6gzrvjrmr75d3ca4p5hzngq3pv2b0ixpin5fxw") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.12.4 (c (n "ocaml") (v "0.12.4") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.12.2") (d #t) (k 0)))) (h "08d00jjyylpv5nv3arr53f39xhrs4pkd0xasxnwqdn42cmgxl056") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.0 (c (n "ocaml") (v "0.13.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13") (d #t) (k 0)))) (h "15zs7bxbjwp80niygdlwrw02axwmy2659nxyaqwxz7bwl9kb00b7") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.1 (c (n "ocaml") (v "0.13.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13") (d #t) (k 0)))) (h "01brry6lfcxdm9kfwamkfn8rfxkj15dh2m8szbs7qfhb905aivkd") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.2 (c (n "ocaml") (v "0.13.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13") (d #t) (k 0)))) (h "0qabzzabg6z5hb67zq8xbala9iljg86iijxl95rds84ayy3d1xqk") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.3 (c (n "ocaml") (v "0.13.3") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13") (d #t) (k 0)))) (h "1d66146bsppggminfb6mrj2mb4njz3zyclm9i1vxjrskc8lahw3l") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.4 (c (n "ocaml") (v "0.13.4") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13.3") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13") (d #t) (k 0)))) (h "0hvf1g1rhmzv6ipl25r8a8c4i94rzbf07kgnmzkl8m0w3vxyvx5r") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.5 (c (n "ocaml") (v "0.13.5") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13.3") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13.1") (d #t) (k 0)))) (h "05yqznpbm6imfqzwadmqlqq7y0kprx3vsm64xlk3138n28vmhhc9") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.13.6 (c (n "ocaml") (v "0.13.6") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.13.3") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.13.1") (d #t) (k 0)))) (h "00ixpdihmar2i02mrmd62bn1lna40a50img771bxy1w6gsdvki2c") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.14.0 (c (n "ocaml") (v "0.14.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.14") (d #t) (k 0)))) (h "0hziwva6rpl5p58vq7pswd18vwgx2g6713f0znzzp7xz47g2q877") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.14.1 (c (n "ocaml") (v "0.14.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.14") (d #t) (k 0)))) (h "1xfzj4pcvmy1w00sdxcn4gvc0rrf38ylizkss9ak90h94dklgc7i") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.14.2 (c (n "ocaml") (v "0.14.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.14") (d #t) (k 0)))) (h "07fn2hn0p2qzi9hlrzc695phpn91g3cp0p4xxg1pnki2ipzg0kv8") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive"))))))

(define-public crate-ocaml-0.15.0 (c (n "ocaml") (v "0.15.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.15") (d #t) (k 0)))) (h "0lhfvpnvg51hcn66ibjvs1sifrwdijjikd92544jyjxxj8znxqj9") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.15.1 (c (n "ocaml") (v "0.15.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.15") (d #t) (k 0)))) (h "11rw3cb6smfwp3vf2bmqn68bhhmdm4xfpwkg873byam8vynnsxrk") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.16.0 (c (n "ocaml") (v "0.16.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.16") (d #t) (k 0)))) (h "1ll3iwsc12jpqg3i4pcd2i9405pn2n3w1iq31l6fl9jsxflh7qmr") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.17.0 (c (n "ocaml") (v "0.17.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.17") (d #t) (k 0)))) (h "1w98bxxar8dw2qiasmdmsihis2mwdhv5rr5hid0aixf49vlppk8l") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.18.0 (c (n "ocaml") (v "0.18.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.18") (d #t) (k 0)))) (h "1jgd1zrnh871nvk16irgzbj1qjc6hlivcnbwhsvbdl0kzrk34y8c") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.18.1 (c (n "ocaml") (v "0.18.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "0ks5jpr0iqqlxlc3zn2dzh7bx3xmhi06wjw7r32vq88185if7gcr") (f (quote (("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("docs-rs" "ocaml-sys/docs-rs") ("derive" "ocaml-derive") ("default" "derive") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.19.0 (c (n "ocaml") (v "0.19.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.19.0") (d #t) (k 0)))) (h "1m0imzfahip5ynyhpxgyq7ssk89v5sawylscqj345cc69jwi7clq") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.19.1 (c (n "ocaml") (v "0.19.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.19") (d #t) (k 0)))) (h "0x68nxiljgly62439mjaggni2mjhyy0k89b240w7hknikswbpk0b") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.20.0 (c (n "ocaml") (v "0.20.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.5") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20") (d #t) (k 0)))) (h "0d65m6514p2nw1fhrpv9bsxcwzhzrqnlccs254fpsw5zdl8c5lix") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.20.1 (c (n "ocaml") (v "0.20.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.5") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20") (d #t) (k 0)))) (h "1b27vi9da2z1zlnm58dy9bzqz6s6n1n5fxmgh4a8d1jw1ljf7b5k") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.21.0 (c (n "ocaml") (v "0.21.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.21") (d #t) (k 0)))) (h "1nyjsx85x2bv9vhcr8jmvjvk1vkkiqf9ayns7kkaa1kpwf2m231z") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.22.0 (c (n "ocaml") (v "0.22.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)))) (h "1fiwdx5dv8nm20lrd2gggyxd63rc73yrbd9wmigfs3s0rgndplzx") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.22.1 (c (n "ocaml") (v "0.22.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.5") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)))) (h "122cm541z2i3mwmcyfbkzjkwawshaycj8918qyb9bqliwn64ddn6") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.22.2 (c (n "ocaml") (v "0.22.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.5") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)))) (h "12q3v6zwwlamd436sb8m93c5wlf91272xbjd35iky42i05iaszqs") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.22.3 (c (n "ocaml") (v "0.22.3") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)))) (h "0arifvd2ma8jrsa48hpiw0rmy6vzk1fcvm1lvd30s8656z7z408r") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-0.22.4 (c (n "ocaml") (v "0.22.4") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)))) (h "00hkm9n1gmgk9hs183bvvg7zfyvkjgn2ixk8x9ddk8r4571lb2x4") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.0 (c (n "ocaml") (v "1.0.0-beta.0") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "1x46phg0dh1ymbk49sk8rzz7mas2cxrd5pj0plal1wz2p2v5aj68") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.1 (c (n "ocaml") (v "1.0.0-beta.1") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "1lyjpv4ki4d39yapxfl6apzphjac3kqgbw8z502rh1p6jgmp01ck") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.2 (c (n "ocaml") (v "1.0.0-beta.2") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "0cv0aff77qqcpxjg3ars3nq6l45iygg224f6h167bq2108xq6fv8") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.3 (c (n "ocaml") (v "1.0.0-beta.3") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "0s66lv8qwf6dnrmkkks2ca109h802hrv04991smyg6c5jn33q7ab") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.4 (c (n "ocaml") (v "1.0.0-beta.4") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "0fh2zz6qyq40ps1wbr5kfyp3am69vnrw3qllqwr8hnv57k7gyyfp") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc" "no-panic-hook") ("no-panic-hook") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

(define-public crate-ocaml-1.0.0-beta.5 (c (n "ocaml") (v "1.0.0-beta.5") (d (list (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (d #t) (k 0)) (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-derive") (r "^1.0.0-beta.5") (o #t) (d #t) (k 0)) (d (n "ocaml-interop") (r "^0.8.8") (d #t) (k 0)) (d (n "ocaml-sys") (r ">=0.23") (d #t) (k 0)))) (h "00hczr7mh0i90z6j891qsp68l3dr698nmcn8m3fm24y0za4sa7c7") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-interop/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-std" "cstr_core/alloc" "no-panic-hook") ("no-panic-hook") ("no-caml-startup" "ocaml-interop/no-caml-startup") ("link" "ocaml-sys/link") ("derive" "ocaml-derive") ("default" "derive") ("caml-state" "ocaml-sys/caml-state") ("bigarray-ext" "ndarray"))))))

