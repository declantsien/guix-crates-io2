(define-module (crates-io oc am ocaml-boxroot-sys) #:use-module (crates-io))

(define-public crate-ocaml-boxroot-sys-0.1.0 (c (n "ocaml-boxroot-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1yxxzilnids95d5ws9sifl2c9h185y90w0mw1nnz5wv3zf6wl9cq") (f (quote (("link-ocaml-runtime-and-dummy-program"))))))

(define-public crate-ocaml-boxroot-sys-0.2.0 (c (n "ocaml-boxroot-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mgy8mw3vmm0r34czqsplp8c31bynypvx0mvbfyf4k7fzcxkk1ji") (f (quote (("without-ocamlopt") ("link-ocaml-runtime-and-dummy-program"))))))

(define-public crate-ocaml-boxroot-sys-0.3.0-rc.1 (c (n "ocaml-boxroot-sys") (v "0.3.0-rc.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08rnv2h5dcsybl65l5f57c2k64877wyrcl3x4jpkv7dz6fy7bzxy") (f (quote (("link-ocaml-runtime-and-dummy-program") ("default" "bundle-boxroot") ("bundle-boxroot")))) (l "ocaml-boxroot")))

(define-public crate-ocaml-boxroot-sys-0.3.0 (c (n "ocaml-boxroot-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1syiy504fam6y39rmp4dw5x97rxds909xxqbizdvnxrakldh0l87") (f (quote (("link-ocaml-runtime-and-dummy-program") ("default" "bundle-boxroot") ("bundle-boxroot")))) (l "ocaml-boxroot")))

(define-public crate-ocaml-boxroot-sys-0.3.1 (c (n "ocaml-boxroot-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0232sbv22d383574zg25chapp00cl266y38f3gxmpalk2094q6sl") (f (quote (("link-ocaml-runtime-and-dummy-program") ("default" "bundle-boxroot") ("bundle-boxroot")))) (l "ocaml-boxroot")))

