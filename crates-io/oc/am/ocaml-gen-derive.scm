(define-module (crates-io oc am ocaml-gen-derive) #:use-module (crates-io))

(define-public crate-ocaml-gen-derive-0.1.0 (c (n "ocaml-gen-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "17fvkz8d0m5dygz3fnlhpp586mzxv2cimii9djgkxv0asx1zy911")))

(define-public crate-ocaml-gen-derive-0.1.1 (c (n "ocaml-gen-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1zra3dwc119z5snv89fb7vygldmfsvvc5a7f0kb722g2mj08bn7g")))

(define-public crate-ocaml-gen-derive-0.1.2 (c (n "ocaml-gen-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1qxd4aym5qgmv2awi5aspd2hkmlansgpkk2h360vmrpl8s7bb6ga")))

(define-public crate-ocaml-gen-derive-0.1.3 (c (n "ocaml-gen-derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3q67yrm98d3fhwf8gaifsnc6nnmdrj506qsn9pmi1gcy937xr8")))

(define-public crate-ocaml-gen-derive-0.1.4 (c (n "ocaml-gen-derive") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0dww0fvlqcbb2djxbmz4id6ggwxwfw437s1qx4ffb9rk0yzg4vn4")))

(define-public crate-ocaml-gen-derive-0.1.5 (c (n "ocaml-gen-derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1p6qv62mwwfiv52cw9d6w655rsqndnqmsprcjcbkvn69ypgfz50q")))

