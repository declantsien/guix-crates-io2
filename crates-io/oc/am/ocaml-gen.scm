(define-module (crates-io oc am ocaml-gen) #:use-module (crates-io))

(define-public crate-ocaml-gen-0.1.0 (c (n "ocaml-gen") (v "0.1.0") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.2") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0486n7j0dps53m7rjn2cz00kaca6knd3vafmxgkrqd5hyljsdss2")))

(define-public crate-ocaml-gen-0.1.1 (c (n "ocaml-gen") (v "0.1.1") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1a5ka4c30ada5avysywlf8k1vilkncnr60msb6b3a5dy0ad7fa1l")))

(define-public crate-ocaml-gen-0.1.2 (c (n "ocaml-gen") (v "0.1.2") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0sr4sz432gxigh2s36vqg1bsldpvg882h9ijvh8g5kmk0flq2k97")))

(define-public crate-ocaml-gen-0.1.3 (c (n "ocaml-gen") (v "0.1.3") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0n3p70m60y9azjxdrspmz74dqrrb4qmsadpwnvvx482md5r0y8rw")))

(define-public crate-ocaml-gen-0.1.4 (c (n "ocaml-gen") (v "0.1.4") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0gh0x2m9p9s6xmpvh8kb2cqz88p5qvc5kbhhr2vsmsrngdlrvp98")))

(define-public crate-ocaml-gen-0.1.5 (c (n "ocaml-gen") (v "0.1.5") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)) (d (n "ocaml-gen-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "19zq2110iymckfbxkycp4w8il97inradd7z0c9lgghg00x6970js") (f (quote (("without-ocamlopt" "ocaml/without-ocamlopt"))))))

