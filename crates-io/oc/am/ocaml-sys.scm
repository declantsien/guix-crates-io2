(define-module (crates-io oc am ocaml-sys) #:use-module (crates-io))

(define-public crate-ocaml-sys-0.10.0 (c (n "ocaml-sys") (v "0.10.0") (h "1na62dgkw76g0yfmlaq23zpj2fmiv1q5r91s5jy09nz8130wssq9") (f (quote (("link"))))))

(define-public crate-ocaml-sys-0.10.1 (c (n "ocaml-sys") (v "0.10.1") (h "0bz7fhmf937ylg094zzgy8d3lx5nrvyzhxjsbyqxdji91r40bljx") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.11.0 (c (n "ocaml-sys") (v "0.11.0") (h "15503nj6pz61z06iysi9b761asznf385ixfx7rfy2lzjfw8wd9ix") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.12.0 (c (n "ocaml-sys") (v "0.12.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0x3qmqxd4issnwjz0h45sdmx6k1lxw3iaxkp4s1r4r02z5s5m3xv") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.12.1 (c (n "ocaml-sys") (v "0.12.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0r08vp412dj3vq79zkn0ncx1ajxmxs63d4i1z4r49zh0dzw5yfm6") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.12.2 (c (n "ocaml-sys") (v "0.12.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1as2myb8affw8695h2jn4y3i22cc9wkpq0fz735xpfb7c4f70zql") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.13.0 (c (n "ocaml-sys") (v "0.13.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0gkbsphzhnnpmb2svsr9971qq2z2g2rhhab0y1ml923yf1f8cyax") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.13.1 (c (n "ocaml-sys") (v "0.13.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "09r0w38abvvc4dnr4d8r7wk5fwzjrfvfx6hayss3i4kr837cly65") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.14.0 (c (n "ocaml-sys") (v "0.14.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0z9divyh1sr2zaijairbnbf9v2bq53i08spg4i16l5hzmbksvbmr") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.15.0 (c (n "ocaml-sys") (v "0.15.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "01vc4xlkpmhd02257d2m33i3vqk8aa90ap451zj2snprlav5pw7d") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.16.0 (c (n "ocaml-sys") (v "0.16.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1znxsw6vvl36fkcm7rhg8ah1snisi9qqvg7vkx2x0s3xwx05h8hr") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.17.0 (c (n "ocaml-sys") (v "0.17.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1a3w21q4p7g8y5j3dlnczhvfrnj2024ahwlfljzvagb6fqf6cnnz") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.18.0 (c (n "ocaml-sys") (v "0.18.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1zr2yn2wdxx3mkg1gd2mhkpnfrhsba4hmwy85cxwzbaiccpzfb2d") (f (quote (("link") ("docs-rs")))) (y #t)))

(define-public crate-ocaml-sys-0.18.1 (c (n "ocaml-sys") (v "0.18.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0cdb7bl022ysjnmnzbygwk781kijl2nzh6zlpjf5cd8mapn8kc39") (f (quote (("link") ("docs-rs"))))))

(define-public crate-ocaml-sys-0.19.0 (c (n "ocaml-sys") (v "0.19.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "06ix8q37f3wzl1gi25zswp5ji6jahjib7xfws14kprywh04v9g1y") (f (quote (("without-ocamlopt") ("link") ("caml-state"))))))

(define-public crate-ocaml-sys-0.19.1 (c (n "ocaml-sys") (v "0.19.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0c7200nfmc34ahplzc76yqllw5kfzhgm330c375hfhn4p1p0i6sf") (f (quote (("without-ocamlopt") ("link") ("caml-state"))))))

(define-public crate-ocaml-sys-0.20.0 (c (n "ocaml-sys") (v "0.20.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0axp2g12w4bmsz5b7wc3gd83gndvyml1jg2ga8w1hrx5rl1q9y7w") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.20.1 (c (n "ocaml-sys") (v "0.20.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0675dji1paxgvigpy4afs0rshidb8nwgmqpw53qnl3k9g50ywzby") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.21.0 (c (n "ocaml-sys") (v "0.21.0") (d (list (d (n "chlorine") (r "^1.0") (d #t) (k 0)))) (h "11ssj1grrhl1888avcrwszx0z9bv16fy25k6d0j8g6sxvs0404w0") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.22.0 (c (n "ocaml-sys") (v "0.22.0") (d (list (d (n "chlorine") (r "^1.0") (d #t) (k 0)))) (h "0jk5chsx9kx85d8z5d2sxinnqcaz4idk4jnkizg9zvyv6k01x39f") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.22.1 (c (n "ocaml-sys") (v "0.22.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1bhrg10ls163lfir810p4477pcb0vmyalhlm5mchz34ymsjbs36g") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.22.2 (c (n "ocaml-sys") (v "0.22.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "175bpb2mnl8f3g7y6mhrzwmk2c6lb7sakq7xg935jf7cwwijxa63") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.22.3 (c (n "ocaml-sys") (v "0.22.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1kx3k9kiy6k73lbn19g88bb3zs3i8s7znnj34wk48n0lsjknrv3k") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

(define-public crate-ocaml-sys-0.23.0 (c (n "ocaml-sys") (v "0.23.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "118jrkggv1ai36p09k1mwbbkqq2wn1z049z0qzqz1rad15k4g0fw") (f (quote (("without-ocamlopt") ("link") ("default") ("caml-state"))))))

