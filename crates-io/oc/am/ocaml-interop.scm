(define-module (crates-io oc am ocaml-interop) #:use-module (crates-io))

(define-public crate-ocaml-interop-0.2.0 (c (n "ocaml-interop") (v "0.2.0") (h "195b1nccxi004fs1p4f3w786a5kxv0y27qqpf7qlb94wrx1wk2f9")))

(define-public crate-ocaml-interop-0.2.1 (c (n "ocaml-interop") (v "0.2.1") (h "0hbwgb1p8rg677ljqcjjnhlrn33mzj6q44nrgvw7ivmq2dh5sdhm")))

(define-public crate-ocaml-interop-0.2.2 (c (n "ocaml-interop") (v "0.2.2") (h "1vbl6vfd3lx6hgnpif2cyv9jwvl3z8mj9rqnzn7x3k58pvip6756")))

(define-public crate-ocaml-interop-0.2.3 (c (n "ocaml-interop") (v "0.2.3") (h "1pns0s21bzfdjr0smj9llg4kj7ir2hq812fsd4ca210rr4k6v7v2")))

(define-public crate-ocaml-interop-0.2.4 (c (n "ocaml-interop") (v "0.2.4") (h "0f8b4hxqps5ypmyb09zkga0x6z5b4ndnfksbpzqdrbr0w7dss5wc")))

(define-public crate-ocaml-interop-0.3.0 (c (n "ocaml-interop") (v "0.3.0") (h "0lwd5vgkc9bqdgdmzypxgwr6ck9lwpyi0i9b1dmcpyqg20aaisr8")))

(define-public crate-ocaml-interop-0.4.0 (c (n "ocaml-interop") (v "0.4.0") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "0hga62bmqw03gg6jhry5m5i0g16r2g22r29gbi84qd8xna5iz4q7")))

(define-public crate-ocaml-interop-0.4.1 (c (n "ocaml-interop") (v "0.4.1") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "1hm6habh4nybvpl1wz9h752004nsfaydykqp2170jm0hwmqa8cgl") (f (quote (("docs-rs" "ocaml-sys/docs-rs"))))))

(define-public crate-ocaml-interop-0.4.2 (c (n "ocaml-interop") (v "0.4.2") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "0hcsgrarya8qbrc9yaq7ar9wc9gq54w1b7xh83hbisnpwrhfgb4b") (f (quote (("docs-rs" "ocaml-sys/docs-rs"))))))

(define-public crate-ocaml-interop-0.4.3 (c (n "ocaml-interop") (v "0.4.3") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "1hx8w1rsvrvqk1f8dds2yd18k3lgqilmhcrilz0qjbqjihxwgg3c") (f (quote (("docs-rs" "ocaml-sys/docs-rs"))))))

(define-public crate-ocaml-interop-0.4.4 (c (n "ocaml-interop") (v "0.4.4") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "0a669hbc5qliibd952f5gqqsi70z7iihgi7yyzmfr7f4g09ypxkk") (f (quote (("docs-rs" "ocaml-sys/docs-rs"))))))

(define-public crate-ocaml-interop-0.5.0 (c (n "ocaml-interop") (v "0.5.0") (d (list (d (n "ocaml-sys") (r "^0.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lgvwk92x0cfg12bc7k5wa6nlz40rs6nrijy3nr43p3sng0qmsdl") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.5.1 (c (n "ocaml-interop") (v "0.5.1") (d (list (d (n "ocaml-sys") (r "^0.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0w4kq9caw9m2z2zhbdvrm0d1w9dkr08v4s92yrjyghsbxkd1gc41") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.5.2 (c (n "ocaml-interop") (v "0.5.2") (d (list (d (n "ocaml-sys") (r "^0.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0mq456qwrg5c2ikzvh49v3mdjsn33zrjab3z1v783cjnrdkhwzf4") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.5.3 (c (n "ocaml-interop") (v "0.5.3") (d (list (d (n "ocaml-sys") (r "^0.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0wjsygkmsar1b4mngwy4420bqbfmiqvnxgk7wdxhm3bqlmbsvb8b") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.4.5 (c (n "ocaml-interop") (v "0.4.5") (d (list (d (n "ocaml-sys") (r "^0.18.1") (d #t) (k 0)))) (h "1hci8gp3xcicw5w49i1g4vqwn191ajxb7r50ln7grgmzqv9r9l4h") (f (quote (("docs-rs" "ocaml-sys/docs-rs"))))))

(define-public crate-ocaml-interop-0.6.0 (c (n "ocaml-interop") (v "0.6.0") (d (list (d (n "ocaml-boxroot-sys") (r "^0.1") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0rpnhwmpbz27xhaz22c00qs732d9jf4ly3rzmlhqav6k5w6d5ci6") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.7.0 (c (n "ocaml-interop") (v "0.7.0") (d (list (d (n "ocaml-boxroot-sys") (r "^0.1") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1y1kb6jwv8sllq54v7nzgs8zz648ljzw2g1y1pcdzqswgapzwmfl") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.7.1 (c (n "ocaml-interop") (v "0.7.1") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1jh1bignsdaj55ikql2i0jhpr986icq96gippmkf7h6lzvb6y42y") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.7.2 (c (n "ocaml-interop") (v "0.7.2") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1x2fh39fabyyyca8v2dzialyn5zhl1h54wq43vd8xjidgl8l83ja") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt"))))))

(define-public crate-ocaml-interop-0.8.0 (c (n "ocaml-interop") (v "0.8.0") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0fqvjkac9mjjc4cbskh1vvzdl0v7hbkmx0mh19p579zppwd82j9s") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.1 (c (n "ocaml-interop") (v "0.8.1") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1549q99pfcg9gamcwfr3bz1sbh8k3k79kccb6pa24circga0mwl1") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.2 (c (n "ocaml-interop") (v "0.8.2") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0h7798fd5xs5d040mxzg7p6i1212cvdm5ryal8fqksg7j4fljkvx") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.3 (c (n "ocaml-interop") (v "0.8.3") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "05jyqk5nia0fl737zsfzxszjbc3hfsgbyq1caq5ay9y2hjw4scn1") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.4 (c (n "ocaml-interop") (v "0.8.4") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.20.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lvp8bidy19s1rb41c43hyshfnq8bb1mzvgfvhiq714cn4ji9vgy") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.5 (c (n "ocaml-interop") (v "0.8.5") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0m6c5cxjzzn5sqsrmjhk16qsa4r8ysxkkiv0wyf4qdnyishb990a") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.6 (c (n "ocaml-interop") (v "0.8.6") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1laqvlq5l3bk7ar0iqj0ygsab0v5bl1n53143qqbmi3whliwd95d") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.7 (c (n "ocaml-interop") (v "0.8.7") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "03dnkycj2kb5a7sgzagc7mzh1wyzrzrd3w468mxcss7dxyx9cdbg") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.8.8 (c (n "ocaml-interop") (v "0.8.8") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0qyhms3by7c9cfy77riwhpmawq18kbj2lp921alp5q572a2f009f") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.9.0 (c (n "ocaml-interop") (v "0.9.0") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "16rjmwgn0b1rsiglpf7g9y7jamdcx7mj4sm1313djhg2372bzagx") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.9.1 (c (n "ocaml-interop") (v "0.9.1") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1dci72r0vmplj80j633xr9vri8d90y6s3sg1khiz0blzj4792lzj") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.9.2 (c (n "ocaml-interop") (v "0.9.2") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.22") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "17isa51cih5pzmmi451mgxs0a7w697w57a32gkp0fygyikndzrhw") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

(define-public crate-ocaml-interop-0.10.0 (c (n "ocaml-interop") (v "0.10.0") (d (list (d (n "ocaml-boxroot-sys") (r "^0.2") (d #t) (k 0)) (d (n "ocaml-sys") (r "^0.23") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0z6via6k51g91g8apzwg5n4ihcfaaa089s74ld8p7rkkbpgsl4h4") (f (quote (("without-ocamlopt" "ocaml-sys/without-ocamlopt" "ocaml-boxroot-sys/without-ocamlopt") ("no-caml-startup") ("caml-state" "ocaml-sys/caml-state"))))))

