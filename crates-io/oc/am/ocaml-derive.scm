(define-module (crates-io oc am ocaml-derive) #:use-module (crates-io))

(define-public crate-ocaml-derive-0.10.0 (c (n "ocaml-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "12z3jk57v14k2c0ygd4g6qwziz7kg9k15swc668x6fhdssnp9k3j")))

(define-public crate-ocaml-derive-0.11.0 (c (n "ocaml-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1zq8f7khqhj902lg425a8d8cw1s7bwyk3qn0hpa5s9s6sda4w1h1")))

(define-public crate-ocaml-derive-0.12.0 (c (n "ocaml-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "09qfal875j4m9knqx72nx4cj0n3rd08x2c9kc3sa54jyk6ijbzkn")))

(define-public crate-ocaml-derive-0.12.1 (c (n "ocaml-derive") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0j442mwp6vy2sknlqgcvfwkv5w42ky4mn0905b0r0yrfgj28rykf")))

(define-public crate-ocaml-derive-0.13.0 (c (n "ocaml-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1avkwiw2rn7wxi2gqi4c25ws32dxh8d9m5xy43j06j3hf8a4gvcm")))

(define-public crate-ocaml-derive-0.13.1 (c (n "ocaml-derive") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "13cxy2fddky4n1c10vayapjpjglgcaa0yylzar08dnkn7817pp46")))

(define-public crate-ocaml-derive-0.13.2 (c (n "ocaml-derive") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0a1hxh5liyiz8aqrlj0n200mkmxfcwdxpzc4ay7spq97m09v3icc")))

(define-public crate-ocaml-derive-0.13.3 (c (n "ocaml-derive") (v "0.13.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ggqv7w13hh178ifr566g4mlvndap53nxgcq1d8i0hmz1h749m7g")))

(define-public crate-ocaml-derive-0.14.0 (c (n "ocaml-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ih4nfpcb46dshw4fz0zicn3mbsk84k0fd06k0g38vy7nld836l7")))

(define-public crate-ocaml-derive-0.15.0 (c (n "ocaml-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0x4l52jig6w6fmwr028d49hjw6wd45j1k32rx737gqry3jsghz05")))

(define-public crate-ocaml-derive-0.16.0 (c (n "ocaml-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1i3zc2v74a81xmg3w15npr223f6bgx7hrdxhrvgyq1nq1lknssgj")))

(define-public crate-ocaml-derive-0.17.0 (c (n "ocaml-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1gcc178c4y8aycc96mgmlslnw03h3cvqw2x7nwwsng3ijyfdgmp8")))

(define-public crate-ocaml-derive-0.18.0 (c (n "ocaml-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1gvcglzyba1spq5a3dnyvkl6zkkqp9whb10i8adg7bf3m3y12wm0")))

(define-public crate-ocaml-derive-0.19.0 (c (n "ocaml-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1z848m99brb8lc168z1s1vf4w94p1yy8yji85w0skcnp47rnps11")))

(define-public crate-ocaml-derive-0.20.0 (c (n "ocaml-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0qxc7rd0nh4596gavmhddij4yv7pk1lcw9k1s9ncdhh6jwsr8sbv")))

(define-public crate-ocaml-derive-0.21.0 (c (n "ocaml-derive") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "12wrpfsjqi4j2mln5mnmrygj0qcx9d582yfjpyvy4vgg6q8ffxxb")))

(define-public crate-ocaml-derive-0.22.0 (c (n "ocaml-derive") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "16594pmnyfr3535184pqxbmgq8fwj38yq544n5gbhs0j02daj2ml")))

(define-public crate-ocaml-derive-1.0.0-beta.0 (c (n "ocaml-derive") (v "1.0.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0sqkfq3r6jv5pif9zbg7zj83zl0b8il2rszgb9wnn4md8jlp4bnp")))

(define-public crate-ocaml-derive-1.0.0-beta.1 (c (n "ocaml-derive") (v "1.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "05fiklr2kfzzwr8wrxpjjc5w7dyvxvqa0h3k5in0adb148mlf9im")))

(define-public crate-ocaml-derive-1.0.0-beta.2 (c (n "ocaml-derive") (v "1.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1y84hcvnnagvj3p64y8nk0fxic33xlcpd1m1x7mi1f1nz1jj7arm")))

(define-public crate-ocaml-derive-1.0.0-beta.3 (c (n "ocaml-derive") (v "1.0.0-beta.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "18w31qwvhr5d9x3y6mj2mkgnxpjngxjymvf1fq2mb9b4j0m9sy4v")))

(define-public crate-ocaml-derive-1.0.0-beta.4 (c (n "ocaml-derive") (v "1.0.0-beta.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0yg5wxqrfchxb5kpfycsdga7frixgaf9zvl25rhfprn1h25s3f21")))

(define-public crate-ocaml-derive-1.0.0-beta.5 (c (n "ocaml-derive") (v "1.0.0-beta.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05qxa9s690r3ip0m17xz64wm4y717dnaq24na196qd2y7qwzvpsw")))

