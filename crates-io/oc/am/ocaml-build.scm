(define-module (crates-io oc am ocaml-build) #:use-module (crates-io))

(define-public crate-ocaml-build-1.0.0-beta.0 (c (n "ocaml-build") (v "1.0.0-beta.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c530fha6xf8q8n1zag0nrjpll4yk3x7pf14lqk9gk6j19jnr5dr") (f (quote (("dune" "cc"))))))

(define-public crate-ocaml-build-1.0.0-beta.1 (c (n "ocaml-build") (v "1.0.0-beta.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07kxrky322y9dwz67h8ph9apjiw7bipg75jzqg0gzvwpy59683wl") (f (quote (("dune" "cc"))))))

(define-public crate-ocaml-build-1.0.0-beta.2 (c (n "ocaml-build") (v "1.0.0-beta.2") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nf36avqsj668ar5zrj87p6bic9hshb22alb7aff39fq6dv56658") (f (quote (("dune" "cc"))))))

(define-public crate-ocaml-build-1.0.0-beta.3 (c (n "ocaml-build") (v "1.0.0-beta.3") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "166x0h2nwpbymbyrwxji5i43a1xxjb9pwygnaxzlspnhmalzhl80") (f (quote (("dune" "cc"))))))

(define-public crate-ocaml-build-1.0.0-beta.4 (c (n "ocaml-build") (v "1.0.0-beta.4") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "183asai1zklbbnai0z7ax59dh1fphizg9c8wwzwz7hl1b8ahnaxf") (f (quote (("dune" "cc"))))))

