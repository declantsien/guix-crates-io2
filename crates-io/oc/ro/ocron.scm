(define-module (crates-io oc ro ocron) #:use-module (crates-io))

(define-public crate-ocron-0.1.3 (c (n "ocron") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "07m5v6ynkgs68z0dz51l8cvvh10yyjrjxd2ssvhab24jbiyhjvb9")))

(define-public crate-ocron-0.2.0 (c (n "ocron") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1pnmi2yck05f5d5ajlxrmpl6c6ai2k7x4l28znmyf2syll0d206b")))

