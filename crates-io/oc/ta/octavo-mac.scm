(define-module (crates-io oc ta octavo-mac) #:use-module (crates-io))

(define-public crate-octavo-mac-0.1.0 (c (n "octavo-mac") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "octavo-digest") (r "^0.1.0") (d #t) (k 0)))) (h "19a8yvzwawjaa1ms7scsbiivzvdf7sk2g0z32x3x5l5dg6k05cgb") (f (quote (("unstable"))))))

(define-public crate-octavo-mac-0.1.1 (c (n "octavo-mac") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "octavo-digest") (r "^0.1.0") (d #t) (k 0)))) (h "0xwvkp8c1kkc93b27a8m63lrdqn7s4n7hmhrqf3z2icsfs8jvkqk") (f (quote (("unstable"))))))

