(define-module (crates-io oc ta octarine) #:use-module (crates-io))

(define-public crate-octarine-0.1.0 (c (n "octarine") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1a17l6h5dsrrm35k8fxkcf5rqdarqnwwam4nhw7n1392wmbijyy1") (y #t)))

(define-public crate-octarine-0.2.0 (c (n "octarine") (v "0.2.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0i0imzynqj69sb55hyidrkmrqs62889ww5rc5a5azfs67hd01pi7")))

(define-public crate-octarine-0.3.0 (c (n "octarine") (v "0.3.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "18zpv66i4vxkxi2fjyrg46crw32fbb7dkdjzf3a25ckryx941ba0")))

(define-public crate-octarine-0.3.1 (c (n "octarine") (v "0.3.1") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1b0hcwcba960y4jpd65jfpf3sv4glbk7dvs1s5754gxwcvclcbwq")))

(define-public crate-octarine-0.3.2 (c (n "octarine") (v "0.3.2") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0604dlhp058dy3qq75inkpzidww5mm6pb8b4d32bcgwb0pwj2ivj") (y #t)))

(define-public crate-octarine-0.3.3 (c (n "octarine") (v "0.3.3") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0kllhv9fi1jbkvmb217zxnmibgngf0rxlq3r2fay2sdqg0ydabn5")))

