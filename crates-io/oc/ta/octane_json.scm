(define-module (crates-io oc ta octane_json) #:use-module (crates-io))

(define-public crate-octane_json-0.1.1 (c (n "octane_json") (v "0.1.1") (d (list (d (n "octane_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1v0sd5p90sy7giar3s1ba848zbp99x8j7vfpdgid89s0fpmkmlxg")))

(define-public crate-octane_json-0.1.2 (c (n "octane_json") (v "0.1.2") (d (list (d (n "octane_macros") (r "^0.1.2") (d #t) (k 0)))) (h "104jxby9b1fivngkni5w51bx3narmg5dkplilqc600adxy4bqb4v")))

(define-public crate-octane_json-0.2.0 (c (n "octane_json") (v "0.2.0") (d (list (d (n "octane_macros") (r "^0.1.2") (d #t) (k 0)))) (h "115jv8mgi6viqqrgzdxcadf3dvn6561xz7m2x1r88ahbq6wrsi8f")))

