(define-module (crates-io oc ta octane_macros) #:use-module (crates-io))

(define-public crate-octane_macros-0.1.0 (c (n "octane_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)))) (h "1n64x1dgf005cfqk7s1a01wrsfjws5dgmz8a15z3zsijflpadn22")))

(define-public crate-octane_macros-0.1.1 (c (n "octane_macros") (v "0.1.1") (h "1sssq71f851kd4xbqmgfc2kilvyi82s893z3gv28ld6dxy4i5w86")))

(define-public crate-octane_macros-0.1.2 (c (n "octane_macros") (v "0.1.2") (h "1054daiakamyawc4nx8qw012ga8hyvyk6cwgyq2vgibqq0f95xjr")))

