(define-module (crates-io oc ta octagon) #:use-module (crates-io))

(define-public crate-octagon-0.1.0 (c (n "octagon") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "siesta") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1qiwcdd89p00ddl0i77di656i1j62x02dnqvb7qmp8vvfmbgmcgf")))

(define-public crate-octagon-0.1.1 (c (n "octagon") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "siesta") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1vjys4196y4192kiykchzl0fd1had9ibzjrg18crsz4q7bnqhhdd")))

