(define-module (crates-io oc ta octavo-crypto) #:use-module (crates-io))

(define-public crate-octavo-crypto-0.1.0 (c (n "octavo-crypto") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "typenum") (r "^1.2.0") (d #t) (k 0)))) (h "0q7395xgb18admi24wk4x94gq894shnm8fiwp4vvc5qfydwdyfdv") (f (quote (("unstable"))))))

(define-public crate-octavo-crypto-0.1.1 (c (n "octavo-crypto") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "typenum") (r "^1.2.0") (d #t) (k 0)))) (h "1f4wfif58z4fys3qirlpmpxzrvliwfmv71vy799advddnaglp9fi") (f (quote (("unstable"))))))

