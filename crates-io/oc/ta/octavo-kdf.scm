(define-module (crates-io oc ta octavo-kdf) #:use-module (crates-io))

(define-public crate-octavo-kdf-0.1.0 (c (n "octavo-kdf") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "octavo-crypto") (r "^0.1.0") (d #t) (k 0)))) (h "10cl76mx781bv7p3fmqzkvi319ilwzxgknf6isjhnbfz5ca1ifxc") (f (quote (("unstable"))))))

(define-public crate-octavo-kdf-0.1.1 (c (n "octavo-kdf") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "octavo-crypto") (r "^0.1.0") (d #t) (k 0)))) (h "1wjc41ya90z3igrxqd5kbdid0azs3lfm670kmf7j71435qz3rdxg") (f (quote (("unstable"))))))

