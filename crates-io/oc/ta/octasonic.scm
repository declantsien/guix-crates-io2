(define-module (crates-io oc ta octasonic) #:use-module (crates-io))

(define-public crate-octasonic-0.1.0 (c (n "octasonic") (v "0.1.0") (d (list (d (n "spidev") (r "^0.2.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (k 0)))) (h "06zhpcrqfbixzm0b0lf9kpjlldv7jf5ni4hxs0z7clad5pr0q938")))

