(define-module (crates-io oc s- ocs-learning-rust) #:use-module (crates-io))

(define-public crate-ocs-learning-rust-0.1.0 (c (n "ocs-learning-rust") (v "0.1.0") (h "03hzpn17p4xgilax6ghvvs4jywq2j858ajniq5zfhr8bl7q6f3l5")))

(define-public crate-ocs-learning-rust-0.1.1 (c (n "ocs-learning-rust") (v "0.1.1") (h "14p5ga9b30p5d1s2k80qh9xyjcw7fq50qyx7kq7w8v0ws2cjzi1a")))

(define-public crate-ocs-learning-rust-0.1.2 (c (n "ocs-learning-rust") (v "0.1.2") (h "1k1zxhqf6v6d1qqqyk4gnkchwhjfaqa8gbbwj7jpd8prra46n8fi")))

(define-public crate-ocs-learning-rust-0.1.3 (c (n "ocs-learning-rust") (v "0.1.3") (h "0amg47326wsyvyh36s578adhjz14w9lxdg22haqpifizridfrxjm")))

(define-public crate-ocs-learning-rust-0.1.4 (c (n "ocs-learning-rust") (v "0.1.4") (h "1p46p4r3yg327qb0vr76rddz4w2i607b29zwl7i9hmanmd3phlw5")))

