(define-module (crates-io oc i- oci-annotations) #:use-module (crates-io))

(define-public crate-oci-annotations-0.1.0-v1.0 (c (n "oci-annotations") (v "0.1.0-v1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10fp04v3d5k4krwrz8f7md30lkznanqa4ha5npasg7iq6wa91cln") (y #t)))

(define-public crate-oci-annotations-1.0.0 (c (n "oci-annotations") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0niw8xbfppd4y1ixmv06yh419hhfqky4c75r0x326qndppwj9i4k")))

