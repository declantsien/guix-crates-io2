(define-module (crates-io oc i- oci-runtime-spec) #:use-module (crates-io))

(define-public crate-oci-runtime-spec-0.0.1 (c (n "oci-runtime-spec") (v "0.0.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0hlpbyzdhxrq5nfpagshx5jx3h8piaf997jrqz7sj4afv4y6hyi1")))

(define-public crate-oci-runtime-spec-0.0.2 (c (n "oci-runtime-spec") (v "0.0.2") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "1jvlgd9qcxnvrpq1avpqhhp21jv0b9phfinda3nvy0k3ibp1cd20")))

(define-public crate-oci-runtime-spec-0.0.3 (c (n "oci-runtime-spec") (v "0.0.3") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "02kgx65hmk970djidamkwb936hjn2sw2skzqhlji5wlz15da7bvq")))

(define-public crate-oci-runtime-spec-0.0.4 (c (n "oci-runtime-spec") (v "0.0.4") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "1770m5zv4r77wj67ln9avs4jmkc242mvzj3aby8694fkzck65aq9")))

