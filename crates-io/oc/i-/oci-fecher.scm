(define-module (crates-io oc i- oci-fecher) #:use-module (crates-io))

(define-public crate-oci-fecher-0.1.0 (c (n "oci-fecher") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0hi10n4cqpfl0kcyiwn19i0kb214afcf6adl64rwfqw44827rly0")))

