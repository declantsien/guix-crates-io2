(define-module (crates-io oc i- oci-tar-builder) #:use-module (crates-io))

(define-public crate-oci-tar-builder-0.4.0 (c (n "oci-tar-builder") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oci-spec") (r "^0.6.4") (f (quote ("runtime" "runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "13jylvc4lmysv2zhl9zl7q145qfcmi5znkcjvld5a0yh4y7yjxvv")))

