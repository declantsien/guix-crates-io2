(define-module (crates-io oc i- oci-r2-uploader) #:use-module (crates-io))

(define-public crate-oci-r2-uploader-0.1.0 (c (n "oci-r2-uploader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.48.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (d #t) (k 0)))) (h "0ilnnzgi32m7rih2cd21iyrri4hr2p4n6k1fp7ph79nzsh37a1kw")))

(define-public crate-oci-r2-uploader-0.1.1 (c (n "oci-r2-uploader") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.48.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (d #t) (k 0)))) (h "0dfxbkys5wn9hqnzx41imrblpy27niwrvw36803z7wfss5pc7xcm")))

(define-public crate-oci-r2-uploader-0.1.2 (c (n "oci-r2-uploader") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.48.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (d #t) (k 0)))) (h "0iicl0dndxavw5a6aby33xnxsm09wydky24sfqj0x9vn4hp06j8m")))

