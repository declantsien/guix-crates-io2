(define-module (crates-io oc i- oci-fetcher) #:use-module (crates-io))

(define-public crate-oci-fetcher-0.1.1 (c (n "oci-fetcher") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1pyxxslpj6f5kh1klbn4nld2dbpvq7i5kzpzv28f6mn372g5r925")))

(define-public crate-oci-fetcher-0.1.2 (c (n "oci-fetcher") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1rbkxcf76msnn89y5m0rmms8a50dzk67ns81a4kpi63h8wm6gkz3")))

