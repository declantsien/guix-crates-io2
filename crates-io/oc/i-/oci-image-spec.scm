(define-module (crates-io oc i- oci-image-spec) #:use-module (crates-io))

(define-public crate-oci-image-spec-0.1.0 (c (n "oci-image-spec") (v "0.1.0") (d (list (d (n "blake3") (r "~1.2") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "digest") (r "~0.9") (d #t) (k 0)) (d (n "hex") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "sha2") (r "~0.9") (d #t) (k 0)))) (h "189h59ya62yy9v6v5rr90ci101df3picng3vrhrhyvyxnvrh9h4p")))

