(define-module (crates-io oc o_ oco_ref) #:use-module (crates-io))

(define-public crate-oco_ref-0.1.0 (c (n "oco_ref") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sxcf4qfmfl4ha0kgixsg6ijpp7c8lrjqdzlrysbpmwsjj02ghf8") (r "1.75")))

(define-public crate-oco_ref-0.1.1 (c (n "oco_ref") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0217bj4r4hdsyk87dwgg04dms8g4i8n565d1pvhabfghnbpvq7n5") (r "1.75")))

(define-public crate-oco_ref-0.2.0 (c (n "oco_ref") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "165rmfvk4h33jykn1ndsvgdcxwj9g1xg2zzn3ib63a1rzs14kfb4") (r "1.75")))

