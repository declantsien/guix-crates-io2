(define-module (crates-io oc a_ oca_zip_resolver) #:use-module (crates-io))

(define-public crate-oca_zip_resolver-0.1.0 (c (n "oca_zip_resolver") (v "0.1.0") (d (list (d (n "oca-rust") (r "^0.1.44") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1c6qyxc67vy5z5v02xsizwb38chpmjxlzi9bxglhrxza3sqmxjqv")))

(define-public crate-oca_zip_resolver-0.2.1 (c (n "oca_zip_resolver") (v "0.2.1") (d (list (d (n "oca-rust") (r "^0.1.45") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0w9q8v646dd116qbv6xsd9c42ia1mlsxv7w44fdq3r0j95g6hvs0")))

(define-public crate-oca_zip_resolver-0.2.2 (c (n "oca_zip_resolver") (v "0.2.2") (d (list (d (n "oca-rust") (r "^0.1.45") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1b89l19daqjxd04v4fdxkkkrksabgs8sr0pbhwssyh0xf9n2xm05")))

(define-public crate-oca_zip_resolver-0.2.3 (c (n "oca_zip_resolver") (v "0.2.3") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0q2i2l5cc0mrjjpj1qzimaxs30xj8dpwrm6jabbp15v7m3jkd4n1")))

(define-public crate-oca_zip_resolver-0.2.4 (c (n "oca_zip_resolver") (v "0.2.4") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0rryz8bizwaqjp8m4g7hlwv0mwyjyfzzrnl49jpazz0qin13hjnn")))

(define-public crate-oca_zip_resolver-0.2.5 (c (n "oca_zip_resolver") (v "0.2.5") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0vhryb5hxywl5nbn06fwr9rdaq9sbqz43x63i9vx0l5psbynnwwm")))

(define-public crate-oca_zip_resolver-0.2.6 (c (n "oca_zip_resolver") (v "0.2.6") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0k7m0s6dvwbaan91ym09qxsfph6jr4lvdz0l4dv6kmi0lbqnmapg")))

(define-public crate-oca_zip_resolver-0.2.7 (c (n "oca_zip_resolver") (v "0.2.7") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "06s0kfzqbjpmxghp9anhncwj5h7wiqfw10lrfz4ql9924045in4i")))

(define-public crate-oca_zip_resolver-0.2.8 (c (n "oca_zip_resolver") (v "0.2.8") (d (list (d (n "oca-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1lrib5z93fmphhky6b175w2nd5q1nklnzy8djk5lkpkb9yrps9kx")))

(define-public crate-oca_zip_resolver-0.2.10 (c (n "oca_zip_resolver") (v "0.2.10") (d (list (d (n "oca-rust") (r "^0.2.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1g14a497fr5b4wb88lkps4pvgbsrxsh53hqq2zsmi0qp102hbnri")))

(define-public crate-oca_zip_resolver-0.2.11 (c (n "oca_zip_resolver") (v "0.2.11") (d (list (d (n "oca-rust") (r "^0.2.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0fs8kky1l48k5agrrh6aycjcczylgy5irhwd7qyfgbpk0s6pdss1")))

(define-public crate-oca_zip_resolver-0.2.12 (c (n "oca_zip_resolver") (v "0.2.12") (d (list (d (n "oca-rs") (r "^0.2.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "13vs96g0kv29zz1bymbcgjsksjnil74aiiqkiah26l9mbrm82ds5")))

