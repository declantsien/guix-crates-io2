(define-module (crates-io oc r_ ocr_b_checksum) #:use-module (crates-io))

(define-public crate-ocr_b_checksum-0.1.0 (c (n "ocr_b_checksum") (v "0.1.0") (h "0w3xskr02jdprsli8zdhnbs45bdf71svyrkiq9k54i4pmxghbz1l")))

(define-public crate-ocr_b_checksum-0.1.1 (c (n "ocr_b_checksum") (v "0.1.1") (h "1ckdcs58qa696xnsl1pg9m8q37y1zgrpyf6cy8p8ch09v8d1f2qk")))

