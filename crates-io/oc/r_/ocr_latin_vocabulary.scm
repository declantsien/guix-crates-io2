(define-module (crates-io oc r_ ocr_latin_vocabulary) #:use-module (crates-io))

(define-public crate-ocr_latin_vocabulary-0.1.0 (c (n "ocr_latin_vocabulary") (v "0.1.0") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.4") (d #t) (k 0)))) (h "164f6n09gfa7rr5vhhna13sch8j4v40s991b2lpx4aab38vra4yz")))

