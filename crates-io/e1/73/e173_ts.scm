(define-module (crates-io e1 #{73}# e173_ts) #:use-module (crates-io))

(define-public crate-e173_ts-0.1.0-alpha.1 (c (n "e173_ts") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "unicode-ident") (r "^1.0.12") (d #t) (k 0)))) (h "1i1vqif6k3ibh03kan72bidg8wqik6z2c340byykp87w7kk65as4") (f (quote (("wasm-bindgen") ("json") ("js"))))))

