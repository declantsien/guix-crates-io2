(define-module (crates-io ld wm ldwm) #:use-module (crates-io))

(define-public crate-ldwm-0.1.0 (c (n "ldwm") (v "0.1.0") (h "14n3pn8ng664q4j52jry1ab84i94pj6xv1grg3v8vmm5hfib1rrl")))

(define-public crate-ldwm-0.2.0 (c (n "ldwm") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "1dl4x08cwr0g60pbpw4vi2nsm72ns0q62q084km4y5bd0vyn4djz") (f (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2.1 (c (n "ldwm") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "0jx1h63dd7ya1g3j66ah0ry6z7pcn6jx1chqkvppmbnzkgi28rck") (f (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2.2 (c (n "ldwm") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "04cy3k8xxhwgfi7g7wi18gnxziqc1nmhja5ic285mpbz1rx4gc59") (f (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2.3 (c (n "ldwm") (v "0.2.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "0bjcryihjwabcqxs5h6f84x382ckds8s31p6zqjk8sm2hg9w5gcm") (f (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2.4 (c (n "ldwm") (v "0.2.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "0pm5v6s3n4dawln2szy1g79k4n3iy6c305xn1ywphnkz1n598ryn") (f (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.3.0 (c (n "ldwm") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "1k3vvdsxs4j7mld17qq959khalfd5w0i488j81wz4b847sglwhw6") (f (quote (("verify") ("sign" "std") ("default" "verify" "sign" "rayon")))) (s 2) (e (quote (("std" "dep:rand") ("rayon" "dep:rayon"))))))

