(define-module (crates-io ld ra ldrawy) #:use-module (crates-io))

(define-public crate-ldrawy-0.1.0 (c (n "ldrawy") (v "0.1.0") (d (list (d (n "glam") (r "^0.20.5") (d #t) (k 0)) (d (n "glium") (r "^0.31.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1lzv6gb0zv084hiqxg2zn0hbcp0xjwxvyw1vgvrppj84ifh2jz92") (y #t)))

