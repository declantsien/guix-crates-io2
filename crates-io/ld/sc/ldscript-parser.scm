(define-module (crates-io ld sc ldscript-parser) #:use-module (crates-io))

(define-public crate-ldscript-parser-0.1.0 (c (n "ldscript-parser") (v "0.1.0") (d (list (d (n "nom") (r "^2.2") (d #t) (k 0)))) (h "03dwlayazg3mlgli40x54cp94d0a14zndmq2s9d59j3q405bgwvz")))

(define-public crate-ldscript-parser-0.2.0 (c (n "ldscript-parser") (v "0.2.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "0pb07h3adh1b66x424xw4c8yd4kfnya08s3fq61gh7sizv65wvxs")))

(define-public crate-ldscript-parser-0.3.0 (c (n "ldscript-parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1023xk8bhjqbv6gnn1w3fxjsn84b4c5q9ac7wpbvamfgm60gzkm6")))

