(define-module (crates-io ld #{06}# ld06-embed) #:use-module (crates-io))

(define-public crate-ld06-embed-0.1.0 (c (n "ld06-embed") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "pid") (r "^3.0.0") (d #t) (k 0)))) (h "17f73xh8lzkgb49h7rxkzi4lxa1iih1arqn90n0ipah1cnafkpyl")))

