(define-module (crates-io ld _p ld_preload_helpers) #:use-module (crates-io))

(define-public crate-ld_preload_helpers-0.1.0 (c (n "ld_preload_helpers") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qf5rr2bknngljnqkf24w7sgljv33cbm6i907nqq34firr0lh41y") (f (quote (("std") ("default" "std"))))))

(define-public crate-ld_preload_helpers-0.1.1 (c (n "ld_preload_helpers") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jnlgirgjiia608nw8x63w4d032vi35wzqan5szd49545w36ppiy") (f (quote (("std") ("default" "std"))))))

