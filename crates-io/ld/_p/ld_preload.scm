(define-module (crates-io ld _p ld_preload) #:use-module (crates-io))

(define-public crate-ld_preload-0.1.0 (c (n "ld_preload") (v "0.1.0") (h "0d475n1aas5v45fwdb97binbcchiq18v7wzsmiab1xbd51i3z198")))

(define-public crate-ld_preload-0.1.1 (c (n "ld_preload") (v "0.1.1") (h "1zjmb929jsvddr9vqjpsmrqbbfndiqasha5a27727649ca5vq3lj")))

(define-public crate-ld_preload-0.1.2 (c (n "ld_preload") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1wj3aa2k0v23ihg7ms5s9fl2bfzxhgynrn6xyzwhprqp2rl6faqz")))

