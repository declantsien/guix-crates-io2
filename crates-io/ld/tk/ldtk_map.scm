(define-module (crates-io ld tk ldtk_map) #:use-module (crates-io))

(define-public crate-ldtk_map-0.1.0 (c (n "ldtk_map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ki214fk58l02p5wb20x2fz8whdd43ym7hnw0qd4hsls1bhaggzm")))

(define-public crate-ldtk_map-0.2.0 (c (n "ldtk_map") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xaf1zb0wf77s38jy69qh22hmm7v9646c9pmbidi5c1pi4lj6mkv")))

(define-public crate-ldtk_map-0.3.0 (c (n "ldtk_map") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02bqbsxvl57220lwmxbrwlnifqx3dxgsjvifcxivb18gw80vv9bk")))

(define-public crate-ldtk_map-0.3.1 (c (n "ldtk_map") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x45kb3cdmjhzdyapvyyxn5nv4f0shgpzg99nbaqxiaracvj2w3h")))

