(define-module (crates-io ld tk ldtk_deser_json) #:use-module (crates-io))

(define-public crate-ldtk_deser_json-0.1.0 (c (n "ldtk_deser_json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1ykfvz9w8xyfad65278nll6jdn09x9gmpdfm7j9pwa5c67k2g387")))

(define-public crate-ldtk_deser_json-0.1.1 (c (n "ldtk_deser_json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "101r8j1xl3bidqaz422s783k6r2ir5x267bkpmv45wpr1ds02xld")))

(define-public crate-ldtk_deser_json-0.1.2 (c (n "ldtk_deser_json") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "09l908kdfz1mb26rkrnijksf5g82ysxn2k25wg5b6xfb64np19np")))

