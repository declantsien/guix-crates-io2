(define-module (crates-io ld tk ldtk_easy) #:use-module (crates-io))

(define-public crate-ldtk_easy-0.1.0 (c (n "ldtk_easy") (v "0.1.0") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "0c3mrv1633r5ygw5m8hcfm1wbi8pf7wxkcc3q2g2pl432cghhkzb")))

(define-public crate-ldtk_easy-0.1.1 (c (n "ldtk_easy") (v "0.1.1") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "1sgdc2n6qhyadl31m5rypzqax859zsl60ll5sxikrhb23s57yih8")))

(define-public crate-ldtk_easy-0.1.2 (c (n "ldtk_easy") (v "0.1.2") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "0ccqn8zmvw1699cnpqzjs6zlsdjriyb70bjim3mnq1cnjy7q0krb")))

(define-public crate-ldtk_easy-0.1.3 (c (n "ldtk_easy") (v "0.1.3") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "15kyy9zrzcvcj8m47bd2xmc4x42a8rl3k785kr8dfpnxipzczlqh")))

(define-public crate-ldtk_easy-0.1.4 (c (n "ldtk_easy") (v "0.1.4") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "05fkl47m7xaxkqyfskv5hr5ka4pvd0j18wz987d5nrh46gaj0ibv")))

(define-public crate-ldtk_easy-0.1.5 (c (n "ldtk_easy") (v "0.1.5") (d (list (d (n "ldtk_deser_json") (r "^0.1.2") (d #t) (k 0)))) (h "0kckf6r6ljh55lfxmhqyfnnwyhjjcxd7cinjdxlwfhbhgribbak3")))

