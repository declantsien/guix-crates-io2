(define-module (crates-io ld tk ldtk_rust) #:use-module (crates-io))

(define-public crate-ldtk_rust-0.1.0 (c (n "ldtk_rust") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i865x3r0wlcjd1vn8vwbwh0l42csiqxr6hklpsv296fb5wmvwir")))

(define-public crate-ldtk_rust-0.2.0 (c (n "ldtk_rust") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k304cvbgh33yx4syhpxhq2ni3vd88w40d3l7zpb8wlm5pi6gipy")))

(define-public crate-ldtk_rust-0.3.0 (c (n "ldtk_rust") (v "0.3.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k2y8q0c0f5y9vzswri2f5i8l5y6ads41f6d2d2p75qfijp0nyhi")))

(define-public crate-ldtk_rust-0.4.0 (c (n "ldtk_rust") (v "0.4.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jn3c1kwvzp0w50dqrdl8bs88316b9g75i17kgda06r1dajwi5vn")))

(define-public crate-ldtk_rust-0.4.1 (c (n "ldtk_rust") (v "0.4.1") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07i44rbqrl44cjirlpbkl67j4x78q4jrnghsvxr86301qlhwklbz")))

(define-public crate-ldtk_rust-0.5.0 (c (n "ldtk_rust") (v "0.5.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sbmzjqaqdp82z4zb5hrhm1wiafgzxpxg0xibir22cm068vpzjbv")))

(define-public crate-ldtk_rust-0.5.1 (c (n "ldtk_rust") (v "0.5.1") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wylqy5g3vb4rgrwn2km2ki45jla7mgx8s06xx1494sm0pqgx2vw")))

(define-public crate-ldtk_rust-0.5.2 (c (n "ldtk_rust") (v "0.5.2") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1740qchwlr919np7fyac1lda8a0qlmazbam2626wsg2dyd7hjdbi")))

(define-public crate-ldtk_rust-0.5.3 (c (n "ldtk_rust") (v "0.5.3") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04kpklcysdm1ffhf5gyi42pg986f17w64fp458zwy1az6420w6z5")))

(define-public crate-ldtk_rust-0.6.0 (c (n "ldtk_rust") (v "0.6.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gmf6xizr9vbr3bil7pa455bhyn0ad1i2wylr1gjdr798p8wjvky")))

