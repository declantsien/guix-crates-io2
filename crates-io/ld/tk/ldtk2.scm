(define-module (crates-io ld tk ldtk2) #:use-module (crates-io))

(define-public crate-ldtk2-0.1.0 (c (n "ldtk2") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w3jbi9c2biw0z7xg61bwfzwmhbp56s4vgza12yhg854bbbj2lax")))

(define-public crate-ldtk2-0.1.1 (c (n "ldtk2") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mcpmqxps3b8j8ki868in8rh3spaz7qg6zbk5qxc6v3y08bn2qi6")))

(define-public crate-ldtk2-0.1.2 (c (n "ldtk2") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mfi4m6zchf1n4z2mzc1zqm459hd82r8i82z876fd40mg1jk3dmz")))

(define-public crate-ldtk2-0.1.3 (c (n "ldtk2") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1008rfq09dmxcn0vn4z7avdvncvs0hwb8r7d54642xqkf0p78cpj")))

(define-public crate-ldtk2-0.1.4 (c (n "ldtk2") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1llyzp78j8llwgd84kmh2a7al2wwx77rf0avrjp9yf364gsjgzc7")))

(define-public crate-ldtk2-0.1.5 (c (n "ldtk2") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mfr9zha9x34rayqdf5kdav223bic4ld0b2jf44kyy1jh0in4xvb")))

(define-public crate-ldtk2-0.2.0 (c (n "ldtk2") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13r4zlw2pj0arafrz84vja6dhclkyhshpk8lhgapz75sx6y2y346")))

(define-public crate-ldtk2-0.2.1 (c (n "ldtk2") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hnaimxy9riymxx00z46jqq6zq0w9zn9qv4dq29y0wwfk2xqs526")))

(define-public crate-ldtk2-0.2.2 (c (n "ldtk2") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rgjs6pp3dq5gsn62gvm0q3xglaa64abgpd162fapz9gmmsjfm4d")))

(define-public crate-ldtk2-0.3.0 (c (n "ldtk2") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rwnvlhzw6c6nvg98s7bafgnirgdlgnss5ygvlsbyjzir0v5ghz2")))

(define-public crate-ldtk2-0.4.0 (c (n "ldtk2") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v6s0b7xln3wls7pxg2wj53dcd4143bwzp584j9p3dzgg7vldfqg")))

(define-public crate-ldtk2-0.5.0 (c (n "ldtk2") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dxsw7hwlqmvy06yq3l1a1jd2r1913j1fj398ixxq165gywjjccc")))

(define-public crate-ldtk2-0.5.1 (c (n "ldtk2") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i9g4c1ja5s367f30mp8ws0slggnxrnmbmjp0jv8gzkswipxc00m")))

(define-public crate-ldtk2-0.6.0 (c (n "ldtk2") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kb8gn212lkcpryvrdvkkf0zrp7gn5nmxgrnbnhy3gyvmafdrl24")))

(define-public crate-ldtk2-0.7.0 (c (n "ldtk2") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "071yj009clcskr3bazdr519w6afz1k48841ygdz5488wq9ajvz4l")))

(define-public crate-ldtk2-0.8.0 (c (n "ldtk2") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ld07wb19sb1kf3kq15h2dqlzaqlxmj36ngxh1lcv4m0iarbc0s7")))

