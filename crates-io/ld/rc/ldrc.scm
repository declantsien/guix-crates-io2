(define-module (crates-io ld rc ldrc) #:use-module (crates-io))

(define-public crate-ldrc-0.1.0 (c (n "ldrc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)))) (h "038sfmqh98wd17rs4mbx8l30mpcp1z6v3xpj65z8fpqf7zi7s70l")))

(define-public crate-ldrc-0.1.1 (c (n "ldrc") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)))) (h "1420bz6jqz00xngmjr1a9ynqj01xr4jcza3jfhihgigdxmy0kll1")))

