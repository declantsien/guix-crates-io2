(define-module (crates-io ld s- lds-color) #:use-module (crates-io))

(define-public crate-lds-color-0.1.0 (c (n "lds-color") (v "0.1.0") (d (list (d (n "lds-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "03pl9zya749y6l0x4grnap2qv98lhiip3b19nh0w9k2h9x5kq615")))

