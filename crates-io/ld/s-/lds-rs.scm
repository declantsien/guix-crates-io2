(define-module (crates-io ld s- lds-rs) #:use-module (crates-io))

(define-public crate-lds-rs-0.1.0 (c (n "lds-rs") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)))) (h "09zi7jdrsnvc3j56gbz3yj739kj3q3z937jyn7mlk0qm1pprji60")))

(define-public crate-lds-rs-0.1.1 (c (n "lds-rs") (v "0.1.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)))) (h "0jk877fw545ycl7p5sbsh918rdd1rnn1c85k7sx0di2cy8h818gs")))

