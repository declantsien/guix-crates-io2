(define-module (crates-io ld -m ld-memory-cli) #:use-module (crates-io))

(define-public crate-ld-memory-cli-0.2.5 (c (n "ld-memory-cli") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "evalexpr") (r "^11.0.0") (d #t) (k 0)) (d (n "ld-memory") (r "^0.2.5") (d #t) (k 0)))) (h "114jkwi9qi9pmk9fza8n7vjzc1lwkwwyx9m901h019gf4pmph54m")))

(define-public crate-ld-memory-cli-0.2.6 (c (n "ld-memory-cli") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "evalexpr") (r "^11.0.0") (d #t) (k 0)) (d (n "ld-memory") (r "^0.2.5") (d #t) (k 0)))) (h "0x9vgf2qlink88zhjbgdlicdknp1qr83s8sfk7j51pzwq7dfixpr")))

(define-public crate-ld-memory-cli-0.2.7 (c (n "ld-memory-cli") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "evalexpr") (r "^11.0.0") (d #t) (k 0)) (d (n "ld-memory") (r "^0.2.7") (d #t) (k 0)))) (h "0hcbj9p3y4p834jaii2arkji1vm1v5dn29dh04n3xcgs2g9nvjjq")))

(define-public crate-ld-memory-cli-0.2.8 (c (n "ld-memory-cli") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "evalexpr") (r "^11.0.0") (d #t) (k 0)) (d (n "ld-memory") (r "^0.2.9") (d #t) (k 0)))) (h "013rj8hhi09wjvq9p6qd9np8i5cijnslsr62riidfa45i891ym4p")))

