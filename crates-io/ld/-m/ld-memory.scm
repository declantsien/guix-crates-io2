(define-module (crates-io ld -m ld-memory) #:use-module (crates-io))

(define-public crate-ld-memory-0.1.0 (c (n "ld-memory") (v "0.1.0") (h "0y8rl1rpakkdjfh3b2937fnkz8njinsji14l32qk9nd0b1liqnx1")))

(define-public crate-ld-memory-0.2.0 (c (n "ld-memory") (v "0.2.0") (h "1b1ra7lgih10r3frll6kk4hdkf88hslq6v5nnx7r1y5a0np5b0ap")))

(define-public crate-ld-memory-0.2.1 (c (n "ld-memory") (v "0.2.1") (h "0i51jddxkd6lgkn8y52pbnsifwi8h4p7gpk1ygdxiflkyw5hzg04")))

(define-public crate-ld-memory-0.2.2 (c (n "ld-memory") (v "0.2.2") (h "12f0rl1a61vibzf6lmjx0j7di4pib0bia5zy0lsbjp2qnxk4sraq")))

(define-public crate-ld-memory-0.2.3 (c (n "ld-memory") (v "0.2.3") (h "0cm4cg7pyzb6mbqzr9crv8wsvi8nkmsrh9plsxmg53n9i7h928j2")))

(define-public crate-ld-memory-0.2.4 (c (n "ld-memory") (v "0.2.4") (h "0g3nrgfr1mlp7kpgcaysplxskpbvljqf74zz211mfx2yivd8aizr")))

(define-public crate-ld-memory-0.2.5 (c (n "ld-memory") (v "0.2.5") (h "05agfq06w8ck8xd812xdr5xz4vr76ri3l26qg00wajmq10lrv71w")))

(define-public crate-ld-memory-0.2.6 (c (n "ld-memory") (v "0.2.6") (h "1hba1lzm5l0flbkmx7aadxs63hnig1q00nlfdzx98y48ddzjkxrb")))

(define-public crate-ld-memory-0.2.7 (c (n "ld-memory") (v "0.2.7") (h "0z72mgrndk2qac8s0hwnjsg7x09pc0yjxm4wcr0crn6f7pnnkmy9")))

(define-public crate-ld-memory-0.2.9 (c (n "ld-memory") (v "0.2.9") (h "034z7qwygk755xckz9zskir6bc9j4693x7v83aa3635ga53pq60n") (f (quote (("build-rs"))))))

