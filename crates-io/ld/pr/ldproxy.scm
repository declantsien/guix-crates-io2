(define-module (crates-io ld pr ldproxy) #:use-module (crates-io))

(define-public crate-ldproxy-0.1.0 (c (n "ldproxy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hvkakzmg0ssga3m8z4ap8bsb5md8w7zanhgbr1c0f2xqalfajwy")))

(define-public crate-ldproxy-0.1.1 (c (n "ldproxy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.23.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07cd9b82v7w1rxgjzqc7rw26vx2d89z9lnr6vdlpi5918h1h392r")))

(define-public crate-ldproxy-0.1.2 (c (n "ldproxy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.23.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hfq26drdrag3xij9ryhqyvkzg96apqlmb11bil8x31hiz79f5qj")))

(define-public crate-ldproxy-0.1.3 (c (n "ldproxy") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.23.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pq4rpf2dwr284hwwv0w42bax4hp66klyf0k718zd9bypp2mh4kw")))

(define-public crate-ldproxy-0.2.0 (c (n "ldproxy") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nhaf5xxnwd3wxwl1l6b1lww49hzh55qifsfhrhyj80clgfgvh3m")))

(define-public crate-ldproxy-0.2.1 (c (n "ldproxy") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13vypgrainb7920srngj1i0gz38ywr76y18w7da0f96vg9r7mz3p")))

(define-public crate-ldproxy-0.3.0 (c (n "ldproxy") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ngg79hx00ymj838zbmy8i8lnrvc6zrl2zab55xwaa8lq4l72jlh")))

(define-public crate-ldproxy-0.3.1 (c (n "ldproxy") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.29") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zzrc9hlrwhpz4j4085ch89p63x0mmb88h5v2w0z1j1845mc3ic9")))

(define-public crate-ldproxy-0.3.2 (c (n "ldproxy") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.29") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s34zhbwdbr062mql8iv6plgl0nb6ylr5ncfh13j01hj1bp8x9c0")))

(define-public crate-ldproxy-0.3.3 (c (n "ldproxy") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "embuild") (r "^0.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19dv5jn106paj07frsrxw83ghz80bqdrcsfagp03l5k9f7g14g3q")))

