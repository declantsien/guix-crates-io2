(define-module (crates-io ld pf ldpfuse) #:use-module (crates-io))

(define-public crate-ldpfuse-0.1.0 (c (n "ldpfuse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)))) (h "14cix2bi33llczxi6avz432f051v9d0p7rl5lf9nvj8pkjw4pwn5")))

