(define-module (crates-io ld ap ldap-parser) #:use-module (crates-io))

(define-public crate-ldap-parser-0.1.0 (c (n "ldap-parser") (v "0.1.0") (d (list (d (n "der-parser") (r ">=5.0.0-beta1, <6.0.0") (d #t) (k 0)) (d (n "hex-literal") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "rusticata-macros") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1nr6xx9fc7w85yn0lklzwa1h3gvkii1h4id6c2aw2jm09irc6fzh")))

(define-public crate-ldap-parser-0.1.1 (c (n "ldap-parser") (v "0.1.1") (d (list (d (n "der-parser") (r ">=5.0.0-beta2, <6.0.0") (d #t) (k 0)) (d (n "hex-literal") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "rusticata-macros") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1md7y9la01khahk8s358ssv0f02wbzaw5cpf56r8sxq7pjmzsvnr")))

(define-public crate-ldap-parser-0.1.2 (c (n "ldap-parser") (v "0.1.2") (d (list (d (n "der-parser") (r "^5.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c0z9p0gf7f30w0w9b2h4q8629fkvgpfki9bmz6bdq87qkhf10bk")))

(define-public crate-ldap-parser-0.1.3 (c (n "ldap-parser") (v "0.1.3") (d (list (d (n "der-parser") (r "^5.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n460xcj5i33sdj3q7in8alz7l7n27jrcryzydirzgiay3sdh4fq")))

(define-public crate-ldap-parser-0.2.0 (c (n "ldap-parser") (v "0.2.0") (d (list (d (n "der-parser") (r "^6.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rcn96j0nlhg9hwz1wfxpj30hbxxp7rjmb7gyqc4isjkgj4ggvng")))

(define-public crate-ldap-parser-0.3.0 (c (n "ldap-parser") (v "0.3.0") (d (list (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zrmn66m7dv7l605a37phlq96dg39lqbdl2d4nyb9f72bydqhjfp")))

(define-public crate-ldap-parser-0.4.0 (c (n "ldap-parser") (v "0.4.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jbm8mkk14kf4gdkvihx63whk5wl94wwgwa0p3k29divg7w8p3vr")))

