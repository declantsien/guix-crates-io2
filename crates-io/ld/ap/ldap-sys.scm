(define-module (crates-io ld ap ldap-sys) #:use-module (crates-io))

(define-public crate-ldap-sys-0.1.0 (c (n "ldap-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0miqiny68c5p89ldyxm6kzam0y3a9wk5x1ilihlrf5vwfmnrzvry") (f (quote (("extern_fns") ("deprecated") ("default" "deprecated"))))))

(define-public crate-ldap-sys-0.1.1 (c (n "ldap-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1nvcf72nf7hag38b7ldg8r4zbp0svi14df9pxmp2kqfhxxwiv3dz") (f (quote (("extern_fns") ("deprecated") ("default" "deprecated"))))))

(define-public crate-ldap-sys-0.1.2 (c (n "ldap-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "039zgm2am4slz7610ggw703d7yy5yz8anwc9g82a0vnq09gjl4lg") (f (quote (("deprecated") ("default" "deprecated"))))))

