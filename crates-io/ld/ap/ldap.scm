(define-module (crates-io ld ap ldap) #:use-module (crates-io))

(define-public crate-ldap-0.0.1 (c (n "ldap") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "0ibmr3m68bkvx39gwjpia1zi1pg7q9mfsvyrvabsc16gdl7wld8a")))

(define-public crate-ldap-0.0.2 (c (n "ldap") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "0d0gihvfsjck5avaimlx77zwl7h605ixx3c1r5lzfvvphb3f60h9")))

(define-public crate-ldap-0.0.3 (c (n "ldap") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "07515l9psdid4sra3bl1l5sg0vcqkjfw12gc217hnh00kpbphcnh")))

(define-public crate-ldap-0.0.4 (c (n "ldap") (v "0.0.4") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "1yv3rvgzl4f84irspbbig2lqbyrkpszcgz9bin8p7sxsfjdn4v84")))

(define-public crate-ldap-0.0.5 (c (n "ldap") (v "0.0.5") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "1kahklm9h77imb3pl9qmq6gy2l7wnr9qkvwqhlv6wlrs7yqaslv6")))

(define-public crate-ldap-0.2.0 (c (n "ldap") (v "0.2.0") (d (list (d (n "asnom") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "1pjdsbhch44ypfih5j0k3fr6rf6avrcv3m7wz3mb31n8yfryw1gn")))

