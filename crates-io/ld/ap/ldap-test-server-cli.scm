(define-module (crates-io ld ap ldap-test-server-cli) #:use-module (crates-io))

(define-public crate-ldap-test-server-cli-0.0.2 (c (n "ldap-test-server-cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ldap-test-server") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread" "macros" "signal" "fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0g1rzdwlp6am3qsq91h4hcp58386vsrcrzqb9zxgsqbixl19rzwf") (r "1.56")))

(define-public crate-ldap-test-server-cli-0.0.3 (c (n "ldap-test-server-cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ldap-test-server") (r "^0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("rt-multi-thread" "macros" "signal" "fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "17wzkb3yk160i8mwcdmqxnky8bz1mhx6qxkgd0kkg1zy4b7hsv30") (r "1.56")))

