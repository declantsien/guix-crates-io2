(define-module (crates-io ld ap ldapico) #:use-module (crates-io))

(define-public crate-ldapico-0.1.0 (c (n "ldapico") (v "0.1.0") (d (list (d (n "ldap3") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1a0hc38jsmsbgkncig2ajrdsrhlfp80ch0n8ffv42d3a3knvvhxa") (r "1.59")))

