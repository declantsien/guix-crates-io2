(define-module (crates-io ld ap ldapquery) #:use-module (crates-io))

(define-public crate-ldapquery-0.0.1 (c (n "ldapquery") (v "0.0.1") (d (list (d (n "ldap3") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0v6madfdqx4nxya13q6w24x38w712hnzkfj8jnh0bm6z80b8jam6") (f (quote (("ldap3_helper" "ldap3"))))))

(define-public crate-ldapquery-0.0.2 (c (n "ldapquery") (v "0.0.2") (d (list (d (n "ldap3") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0v744vid954s6glkrxr23190iqknyiyy6jx4f8m5m3nh13ps9lya") (s 2) (e (quote (("ldap3" "dep:ldap3"))))))

(define-public crate-ldapquery-0.0.3 (c (n "ldapquery") (v "0.0.3") (d (list (d (n "ldap3") (r "^0.11") (o #t) (d #t) (k 0)))) (h "13f37syzkj7wyxcz4a8rga9xalrbrbxdqr9n27yxgc5c8fbv02v2") (s 2) (e (quote (("ldap3" "dep:ldap3"))))))

(define-public crate-ldapquery-0.0.6 (c (n "ldapquery") (v "0.0.6") (d (list (d (n "ldap3") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0pvn03kdrjybrcl8lbk2njvzdrrfps7ivxmwjn0ax05n2qlhlv8n") (s 2) (e (quote (("ldap3" "dep:ldap3"))))))

