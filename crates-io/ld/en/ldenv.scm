(define-module (crates-io ld en ldenv) #:use-module (crates-io))

(define-public crate-ldenv-0.0.1 (c (n "ldenv") (v "0.0.1") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "17wmsk5s5jicv6n5cz0d2pfg908w82y3554iq9cg94sxi5f8a757")))

(define-public crate-ldenv-0.0.2 (c (n "ldenv") (v "0.0.2") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1mmbn49fcfwgaajk1hgd8gfgx7bnx3lr0apsa11ga11108z3wpwk")))

(define-public crate-ldenv-0.0.3 (c (n "ldenv") (v "0.0.3") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "131i1ns58qhlh73hgh0x3bg3m70imq4gd5xd0x90q9h9s40ybzhq")))

(define-public crate-ldenv-0.1.0 (c (n "ldenv") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "0pmza01blr2jg36c6992ibwfszlhcrcxyc6v5ic0p2hwl7zzm5nb")))

(define-public crate-ldenv-0.1.1 (c (n "ldenv") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "02lknccl6brqvmlwzhm5cdh6bxzf6wm13nmvwvk105p1f07qrf1b")))

(define-public crate-ldenv-0.1.2 (c (n "ldenv") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1bbck5nf17za00z98k78cnc2yx54f7d3z2sqkylfpq2a0m5xx2cr")))

(define-public crate-ldenv-0.1.3 (c (n "ldenv") (v "0.1.3") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1ys9gzga2hc6mj8nc8n8jjxql5zzbf98mv5kpz11yb4224cwnnkw")))

(define-public crate-ldenv-0.2.0 (c (n "ldenv") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1c9n8ycsfy5n2ql4vp6ps60f2z25nr0m1zdcqyf3qpq96wf2p2i3")))

(define-public crate-ldenv-0.3.0 (c (n "ldenv") (v "0.3.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1a4061d3snxn6jmmyq0qh06bzk1aaqvag35q5as5imqwxj32ghlc")))

