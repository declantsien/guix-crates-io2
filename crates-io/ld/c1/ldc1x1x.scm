(define-module (crates-io ld c1 ldc1x1x) #:use-module (crates-io))

(define-public crate-ldc1x1x-0.1.0 (c (n "ldc1x1x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "freebsd-embedded-hal") (r "^0") (d #t) (t "cfg(target_os = \"freebsd\")") (k 2)))) (h "0q2szjn6qh9wpnzn9260vsry7g04j3dkij9nigh4jgnqk1bkrp86")))

(define-public crate-ldc1x1x-0.1.1 (c (n "ldc1x1x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "freebsd-embedded-hal") (r "^0") (d #t) (t "cfg(target_os = \"freebsd\")") (k 2)))) (h "1xsbimqfp611lzblbqhn77n217lkcn34cps6rng7vvlscq280hha")))

(define-public crate-ldc1x1x-0.1.2 (c (n "ldc1x1x") (v "0.1.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "freebsd-embedded-hal") (r "^0") (d #t) (t "cfg(target_os = \"freebsd\")") (k 2)))) (h "1c918mnilighd04yx4i3gmqjv6cwkjrln75mb5rg3abv02jadqx1")))

