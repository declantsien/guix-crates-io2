(define-module (crates-io ld s_ lds_simple_view) #:use-module (crates-io))

(define-public crate-lds_simple_view-0.1.0 (c (n "lds_simple_view") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy_diamond_square") (r "^0.1") (d #t) (k 0)))) (h "12b4gxkx2hhbja787i65q1sy0610kjyirdd70byb7f7f2sr31vlf")))

(define-public crate-lds_simple_view-0.1.0-1 (c (n "lds_simple_view") (v "0.1.0-1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy_diamond_square") (r "^0.1") (d #t) (k 0)))) (h "04dz2iak1i6fqy8r2nvcrfm2x1p5q9ryhs3y0xdxzs621ivb9adi") (y #t)))

(define-public crate-lds_simple_view-0.1.1 (c (n "lds_simple_view") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy_diamond_square") (r "^0.1") (d #t) (k 0)))) (h "00mcwxsxdhvik7lwfal6sw18qspinlsckl8la7zc8shizfmqfp9a")))

