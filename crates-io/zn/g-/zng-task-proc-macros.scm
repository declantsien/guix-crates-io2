(define-module (crates-io zn g- zng-task-proc-macros) #:use-module (crates-io))

(define-public crate-zng-task-proc-macros-0.2.0 (c (n "zng-task-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1r0pd3f62vkvmbxmw2nx5rbcqqacn99h7mv4hbzjvkgvzivhjcap")))

(define-public crate-zng-task-proc-macros-0.2.1 (c (n "zng-task-proc-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19za9qz6hrgp588r9imszfiw8m63b0rymyz66f6fw292fx8v296f")))

(define-public crate-zng-task-proc-macros-0.2.2 (c (n "zng-task-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1k7cvd15v9a14fwbzrzc7wbpqszb1vl4zdi4k0zf1dm55nlqbirv")))

(define-public crate-zng-task-proc-macros-0.2.3 (c (n "zng-task-proc-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wx562ajds2cd66bdb9j1cz8l6216kb5am312h9vd2waysqp4as1")))

(define-public crate-zng-task-proc-macros-0.2.4 (c (n "zng-task-proc-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "01nqpm5h447jaww6xjpsvzf48dg9ggdgq8lc519gs12lfr6z3hp0")))

(define-public crate-zng-task-proc-macros-0.2.5 (c (n "zng-task-proc-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "133rmprx9mm78lir2jiw8j2ij4yxllmvfkzaqdypqnj8i6xj3f3p")))

