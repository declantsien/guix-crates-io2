(define-module (crates-io zn g- zng-var-proc-macros) #:use-module (crates-io))

(define-public crate-zng-var-proc-macros-0.2.0 (c (n "zng-var-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1ds0v050g1c104b51m8clg7psq0h7qvi19dzgwj099my219yrj1p")))

(define-public crate-zng-var-proc-macros-0.2.1 (c (n "zng-var-proc-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0qx83fnlaf1j678hf6j4c7f8h01jrv3y1jmlh11pwa0b93an7c11")))

(define-public crate-zng-var-proc-macros-0.2.2 (c (n "zng-var-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1i7ra98r0jlc2v4ifkgxf5ra0a11mn6y5gdysf8315h60974xnnw")))

(define-public crate-zng-var-proc-macros-0.2.3 (c (n "zng-var-proc-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0r3zx69z2dgqvpw3i7nwyiy00y01w1qq9bx3xh7ap38v0dwsvv7m")))

(define-public crate-zng-var-proc-macros-0.2.4 (c (n "zng-var-proc-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "15g1yf9zrihgas78kw98x9qhpgniybpa4g6xf6v9wkh4ix4chfbh")))

(define-public crate-zng-var-proc-macros-0.2.5 (c (n "zng-var-proc-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "17yn2wzh0riqf94dxzw9byn38zji1f6344k3rxqq4g9j544w6n8q")))

(define-public crate-zng-var-proc-macros-0.2.6 (c (n "zng-var-proc-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0l5sfj9mkklyzb6g4c7nrwc1jx4wrqd3qvfkxkh4q5640z9gnmfl")))

