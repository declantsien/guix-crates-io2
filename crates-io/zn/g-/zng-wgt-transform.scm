(define-module (crates-io zn g- zng-wgt-transform) #:use-module (crates-io))

(define-public crate-zng-wgt-transform-0.2.1 (c (n "zng-wgt-transform") (v "0.2.1") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "0nl1fd6ic10z6piga48lpwh2m38kvgbnbi4kihcd65fggfs7rm58")))

(define-public crate-zng-wgt-transform-0.2.2 (c (n "zng-wgt-transform") (v "0.2.2") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "0n896zypx1v18vfaa6pbw7px7lmfqnv8m5dfx2g1ydbpjic127qp")))

(define-public crate-zng-wgt-transform-0.2.3 (c (n "zng-wgt-transform") (v "0.2.3") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "1i71yjn5wpgrj2xzhbhm7br4yrwy6m380pxyhy0056nv4gyx2i35")))

(define-public crate-zng-wgt-transform-0.2.4 (c (n "zng-wgt-transform") (v "0.2.4") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "1ky7vkpn51hfg6np6kjrb6mgncvqygv2rsinnjc0k7zx2lhvi684")))

(define-public crate-zng-wgt-transform-0.2.6 (c (n "zng-wgt-transform") (v "0.2.6") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0amm9bbv4f76aas16dcqpf5ffn50k6y3n7y0wgdpv8si0cj2725v")))

(define-public crate-zng-wgt-transform-0.2.7 (c (n "zng-wgt-transform") (v "0.2.7") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "0sx1gdc8s59imixww6yjijhpbvb6icakyyfrkvc25lyv8898hb7f")))

(define-public crate-zng-wgt-transform-0.2.8 (c (n "zng-wgt-transform") (v "0.2.8") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "0vdp0zj8ij32i62ymwvhkx1vvmnfqlaqmfldh6pzfypf9py158qw")))

(define-public crate-zng-wgt-transform-0.2.9 (c (n "zng-wgt-transform") (v "0.2.9") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "1gadr8d5z4zkm2md5dh4x47ybjswcpnf9v7zjkdgjlcdnbj0fgj1")))

(define-public crate-zng-wgt-transform-0.2.10 (c (n "zng-wgt-transform") (v "0.2.10") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "1ddnyi209gwmmbvwv7d2lrkzkhg8dfxqd9bzlq1avw9sxy5yr371")))

(define-public crate-zng-wgt-transform-0.2.11 (c (n "zng-wgt-transform") (v "0.2.11") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "0p6a8dp38rm21flhpzgljm24z2a7i8lxf3cjflvq7vy3g2dv4lx2")))

(define-public crate-zng-wgt-transform-0.2.12 (c (n "zng-wgt-transform") (v "0.2.12") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "05ykcsibjgbgn581ihqzkigcaj0p2rlbgx5g823b3wmz38hhcdj7")))

(define-public crate-zng-wgt-transform-0.2.13 (c (n "zng-wgt-transform") (v "0.2.13") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "11ygk3h8b28x9kznrnnwpa0fpcdmwr31il8yj69ram2z8nkb6abp")))

