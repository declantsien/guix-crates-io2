(define-module (crates-io zn g- zng-handle) #:use-module (crates-io))

(define-public crate-zng-handle-0.2.0 (c (n "zng-handle") (v "0.2.0") (h "137rvyi06wzj40ny57c1jaj9pj7a4ivpaqhv4hrn39a9ffgpbvjb")))

(define-public crate-zng-handle-0.2.1 (c (n "zng-handle") (v "0.2.1") (h "1cmxcvq65cglnq26z2mlm1g4afh3z07mbl3fdxl65cpvjphj6ks4")))

(define-public crate-zng-handle-0.2.2 (c (n "zng-handle") (v "0.2.2") (h "1x9cira02w9gjniz200lwv33dkk6slgmlzk1swhrb9pr99kbas54")))

(define-public crate-zng-handle-0.2.3 (c (n "zng-handle") (v "0.2.3") (h "1h5k90c9b30djif8bp53xibnvbmz3xl9x4an7034djg4nzzmzcyz")))

(define-public crate-zng-handle-0.2.4 (c (n "zng-handle") (v "0.2.4") (h "1sz84rmzayn3h75c76rgc1dwvmmz1y7ss06r9wn1fphyzx9sx8fp")))

