(define-module (crates-io zn g- zng-view-prebuilt) #:use-module (crates-io))

(define-public crate-zng-view-prebuilt-0.2.0 (c (n "zng-view-prebuilt") (v "0.2.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1") (d #t) (k 1)) (d (n "home") (r "^0") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "04vgxfi4zn6zv9chw529bxd1b5a502ncsra9869bilas6g7l3fcc") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.2.1 (c (n "zng-view-prebuilt") (v "0.2.1") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1") (d #t) (k 1)) (d (n "home") (r "^0") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1vbsjbask7id84h52z7k06plhg7x6xv8bd3yl8wybr12vx4p57xq") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.2.2 (c (n "zng-view-prebuilt") (v "0.2.2") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1") (d #t) (k 1)) (d (n "home") (r "^0") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "133j7nfkbkvi5jrxq9ja2mv8wy9dqqij2jbm8pbr5r12pwkkqb38") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.2.3 (c (n "zng-view-prebuilt") (v "0.2.3") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1") (d #t) (k 1)) (d (n "home") (r "^0") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1gn981bpd4q6l41nj27h3017ryqlr87s2s0jbms35x8yz6q2n839") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.2.4 (c (n "zng-view-prebuilt") (v "0.2.4") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1") (d #t) (k 1)) (d (n "home") (r "^0") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "08md8jailrri075qjdp79yk2z9pindxbvx1vh7z24bd8daw2iyxl") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.2.5 (c (n "zng-view-prebuilt") (v "0.2.5") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "120afawllbix004by74k4agl3sxzv08v8ms8352hg2cyb67ahk8x") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.3.0 (c (n "zng-view-prebuilt") (v "0.3.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0xh5ncgjp6cqk65w88i42g1iiwimy4c4z7cilix5w2zf6vdcmia3") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.3.1 (c (n "zng-view-prebuilt") (v "0.3.1") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "09hb2p0fvi7mq3qfnrbh65a6aijz6spdics17kqkz8rmxcdcwn8f") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.3.2 (c (n "zng-view-prebuilt") (v "0.3.2") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0qwpcrqkpgi761wgikfss98lwi3gac45n6nn9my5c4v47rxdmcwd") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.3.3 (c (n "zng-view-prebuilt") (v "0.3.3") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "12d8772skppnsj5h178az16r2gcrzyz6f5h9z71lmk0skz793hf2") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.3.4 (c (n "zng-view-prebuilt") (v "0.3.4") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1xbk40hap0crrbh11r501y6yj0d7yyd8aag3fpxqhl1jf4vgfdxx") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.4.0 (c (n "zng-view-prebuilt") (v "0.4.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "15y4qsh640awhv9j6b9h5m00x93gcs12yzfypp2lgf08xf4yqmhp") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.5.0 (c (n "zng-view-prebuilt") (v "0.5.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0x9j8laa7jkk65z976i3n0xwkil0f6nix3b9x44ds1k26rbmblpx") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.5.1 (c (n "zng-view-prebuilt") (v "0.5.1") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0kdir0p5rf1shci2gd1an33dfd6yv6c8svnw69pxhmhc5a2k0bdy") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.6.0 (c (n "zng-view-prebuilt") (v "0.6.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "18dk3lcw327n5ac194sl0qvrr1p2mjrbimxh913ag2r7mn31np38") (f (quote (("embedded") ("default" "embedded"))))))

(define-public crate-zng-view-prebuilt-0.6.1 (c (n "zng-view-prebuilt") (v "0.6.1") (d (list (d (n "base64") (r "^0.22") (d #t) (k 1)) (d (n "hashers") (r "^1.0") (d #t) (k 1)) (d (n "home") (r "^0.5") (d #t) (k 1)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1g4ghp1macm8p02wii3kljfinm1lkrb2dmaamiidq25bgmmh06av") (f (quote (("embedded") ("default" "embedded"))))))

