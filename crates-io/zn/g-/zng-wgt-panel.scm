(define-module (crates-io zn g- zng-wgt-panel) #:use-module (crates-io))

(define-public crate-zng-wgt-panel-0.2.2 (c (n "zng-wgt-panel") (v "0.2.2") (d (list (d (n "zng-app") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.2") (d #t) (k 0)))) (h "17ynxf05www85czl410z7xfkzkk65c2sz94gmzwbar6g1jvsc2px")))

(define-public crate-zng-wgt-panel-0.2.3 (c (n "zng-wgt-panel") (v "0.2.3") (d (list (d (n "zng-app") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.3") (d #t) (k 0)))) (h "1az53rg11bp4xq86nx49w1vphs28c4lhkk8vi5id8v885r5ijjjj")))

(define-public crate-zng-wgt-panel-0.2.4 (c (n "zng-wgt-panel") (v "0.2.4") (d (list (d (n "zng-app") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.4") (d #t) (k 0)))) (h "010kahqkjxfqd630n3hd9ijvkl1qghqzgmvgszl429xd3d8m0c8q")))

(define-public crate-zng-wgt-panel-0.2.5 (c (n "zng-wgt-panel") (v "0.2.5") (d (list (d (n "zng-app") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.5") (d #t) (k 0)))) (h "107qv56ldcp6iwrwnic3qm98g8w5s0sfpi5sbx7yzinjk93qgmzp")))

(define-public crate-zng-wgt-panel-0.2.7 (c (n "zng-wgt-panel") (v "0.2.7") (d (list (d (n "zng-app") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.7") (d #t) (k 0)))) (h "15j02x83m20563cr1xvhn9i6s79pxz98jdsivnadj328fpz4h04b")))

(define-public crate-zng-wgt-panel-0.2.8 (c (n "zng-wgt-panel") (v "0.2.8") (d (list (d (n "zng-app") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.8") (d #t) (k 0)))) (h "0hwirk97fn8cs9c3rv4cqsjcw3wcgzigqphl6ncnl18raawgjhb4")))

(define-public crate-zng-wgt-panel-0.2.9 (c (n "zng-wgt-panel") (v "0.2.9") (d (list (d (n "zng-app") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.9") (d #t) (k 0)))) (h "09xp97057zkkpy8pxx9nwdmpj488r8lgdac9g36w2a3kj6j4la4b")))

(define-public crate-zng-wgt-panel-0.2.10 (c (n "zng-wgt-panel") (v "0.2.10") (d (list (d (n "zng-app") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.2.10") (d #t) (k 0)))) (h "1hysggzjfhza196246nml168h26qsqqf1jkap5k9wxzn96bd3da8")))

(define-public crate-zng-wgt-panel-0.3.0 (c (n "zng-wgt-panel") (v "0.3.0") (d (list (d (n "zng-app") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.3.0") (d #t) (k 0)))) (h "18ar3ki9z9c9xhr08xh598lpn60xcrfnbcxdgkv6svqfv1dlgyig")))

(define-public crate-zng-wgt-panel-0.3.1 (c (n "zng-wgt-panel") (v "0.3.1") (d (list (d (n "zng-app") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.3.1") (d #t) (k 0)))) (h "0xdyinnicchk2wn7l2g8zrv6qxd26mpv9ghrhga27rjbkkhz815m")))

(define-public crate-zng-wgt-panel-0.3.2 (c (n "zng-wgt-panel") (v "0.3.2") (d (list (d (n "zng-app") (r "^0.5.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.3.2") (d #t) (k 0)))) (h "14i9a2y0ycjmv04rkcx503726ygkav26wp7hfhv5pbaa2pdhw4vi")))

(define-public crate-zng-wgt-panel-0.3.3 (c (n "zng-wgt-panel") (v "0.3.3") (d (list (d (n "zng-app") (r "^0.5.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt-wrap") (r "^0.3.3") (d #t) (k 0)))) (h "16ns04py9rrgpyirm9344nq2s8qi1h35snnpdy6rrhp3nwl1ibss")))

