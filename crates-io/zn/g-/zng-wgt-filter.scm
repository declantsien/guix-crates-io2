(define-module (crates-io zn g- zng-wgt-filter) #:use-module (crates-io))

(define-public crate-zng-wgt-filter-0.2.1 (c (n "zng-wgt-filter") (v "0.2.1") (d (list (d (n "zng-color") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "1p0d9bc8jxl7mav0c104506rk46glcn6mw5kpw4gwhqa4c489l7q")))

(define-public crate-zng-wgt-filter-0.2.2 (c (n "zng-wgt-filter") (v "0.2.2") (d (list (d (n "zng-color") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "0lp8znmsibgnc4kcs2z9l77jd5s4v4v56sqgq7gyiyar6drnqwds")))

(define-public crate-zng-wgt-filter-0.2.3 (c (n "zng-wgt-filter") (v "0.2.3") (d (list (d (n "zng-color") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "0s9m10h2hgh5sp11qw6dsjqyg8gbqsh6wd4mgfls3aiwxxxfxgvd")))

(define-public crate-zng-wgt-filter-0.2.4 (c (n "zng-wgt-filter") (v "0.2.4") (d (list (d (n "zng-color") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "001wshz2c7g2cnjvd7k30l1jixziwa6iabc1yq5mnm00k8cvsr4b")))

(define-public crate-zng-wgt-filter-0.2.6 (c (n "zng-wgt-filter") (v "0.2.6") (d (list (d (n "zng-color") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0qdg7jmyg8v5252qbwqfkx5fcsisp5j3i7jg32ds0a7p5hvc1mjj")))

(define-public crate-zng-wgt-filter-0.2.7 (c (n "zng-wgt-filter") (v "0.2.7") (d (list (d (n "zng-color") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "1j8h647bgc2s05p2ww1skhqqcay3xx6xqf6d0flagqlj9ampldvg")))

(define-public crate-zng-wgt-filter-0.2.8 (c (n "zng-wgt-filter") (v "0.2.8") (d (list (d (n "zng-color") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "0kxib2yw0q2ffxcaj97cww56827pvvj611bdwfsqkd1dgcdhm4y8")))

(define-public crate-zng-wgt-filter-0.2.9 (c (n "zng-wgt-filter") (v "0.2.9") (d (list (d (n "zng-color") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "08ip6a55v8bl1b0jcfwpcsfzjswkgbyfab8bfb971d2dj7k6ci7s")))

(define-public crate-zng-wgt-filter-0.2.10 (c (n "zng-wgt-filter") (v "0.2.10") (d (list (d (n "zng-color") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "14b4s220lap60jp1qf9dr5kwwwizwdlh8bqnawwf7kl7yshddky8")))

(define-public crate-zng-wgt-filter-0.2.11 (c (n "zng-wgt-filter") (v "0.2.11") (d (list (d (n "zng-color") (r "^0.2.11") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "09vzdik9jy0hj7ci9x4nszlqdyabx7s0x7svw04lk6nhlnd6h24x")))

(define-public crate-zng-wgt-filter-0.2.12 (c (n "zng-wgt-filter") (v "0.2.12") (d (list (d (n "zng-color") (r "^0.2.12") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "09wpmcrk7llp5j2wqhqwb1r1skxpwm3d79kxlywp65v7v1jrbk7c")))

(define-public crate-zng-wgt-filter-0.2.13 (c (n "zng-wgt-filter") (v "0.2.13") (d (list (d (n "zng-color") (r "^0.2.13") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0rhrpb0cvsjwdd6fs86364xfaqry1464w8ifwwlbdc36i5syla63")))

