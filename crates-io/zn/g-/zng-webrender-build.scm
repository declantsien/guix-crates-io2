(define-module (crates-io zn g- zng-webrender-build) #:use-module (crates-io))

(define-public crate-zng-webrender-build-0.0.2 (c (n "zng-webrender-build") (v "0.0.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0n252wznwmf89a1b11p9y8wrmjybw10nq9gl7lbrxfs99yi4zwii") (f (quote (("serialize_program" "serde"))))))

(define-public crate-zng-webrender-build-0.0.3 (c (n "zng-webrender-build") (v "0.0.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0jhbv9a59qf4cmcd57mds57l571iydf6syynfw004n9h4k1avfnq") (f (quote (("serialize_program" "serde"))))))

