(define-module (crates-io zn g- zng-wgt-undo) #:use-module (crates-io))

(define-public crate-zng-wgt-undo-0.2.1 (c (n "zng-wgt-undo") (v "0.2.1") (d (list (d (n "zng-ext-undo") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "0njnx3lir0cssi57hlrq75pfc8glz0hpbppy6ri48nvkjzwsgwpj")))

(define-public crate-zng-wgt-undo-0.2.2 (c (n "zng-wgt-undo") (v "0.2.2") (d (list (d (n "zng-ext-undo") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "13dm0iiinmd29c9pb17awp8bly9ywc9x9jgimlhbc9nzlhr1cpbp")))

(define-public crate-zng-wgt-undo-0.2.3 (c (n "zng-wgt-undo") (v "0.2.3") (d (list (d (n "zng-ext-undo") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "1fp202kf3zysl469jp85gd4arwm9w4jpr2r5zrgavki0ybyrasad")))

(define-public crate-zng-wgt-undo-0.2.4 (c (n "zng-wgt-undo") (v "0.2.4") (d (list (d (n "zng-ext-undo") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "12w4nhfn064b1xljywhchm4n4hxgls6jjnwvp6m4qr0rgpx6bsn2")))

(define-public crate-zng-wgt-undo-0.2.6 (c (n "zng-wgt-undo") (v "0.2.6") (d (list (d (n "zng-ext-undo") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0jl2c6g7qyx74f5xaz243s87m23n6fq1mv2zbgrh0dkkpnx3cdr1")))

(define-public crate-zng-wgt-undo-0.2.7 (c (n "zng-wgt-undo") (v "0.2.7") (d (list (d (n "zng-ext-undo") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "1ngr3whzmw7cp5fr8h0cpwpskh1p2nydm1kfflgqfd4crhdnc65l")))

(define-public crate-zng-wgt-undo-0.2.8 (c (n "zng-wgt-undo") (v "0.2.8") (d (list (d (n "zng-ext-undo") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "1a33nzr25j26ky26863gfczg1i1w1676hmniwl6w8zhfjykh54jq")))

(define-public crate-zng-wgt-undo-0.2.9 (c (n "zng-wgt-undo") (v "0.2.9") (d (list (d (n "zng-ext-undo") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "1fjfhwmpf6iy7wzvzlvy7yj6c0qk02byfsi0f7llpdrs3x923smm")))

(define-public crate-zng-wgt-undo-0.2.10 (c (n "zng-wgt-undo") (v "0.2.10") (d (list (d (n "zng-ext-undo") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "0qvf8dbqss4hmrc5j8mbdrcyf8sc0rzw2n7i1828czxxbr8p9jjy")))

(define-public crate-zng-wgt-undo-0.2.11 (c (n "zng-wgt-undo") (v "0.2.11") (d (list (d (n "zng-ext-undo") (r "^0.2.11") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "164vjb9f3wgs8m82w0fjkrvya682nlkg4s7nbxl7qnkwdpswfkrx")))

(define-public crate-zng-wgt-undo-0.2.12 (c (n "zng-wgt-undo") (v "0.2.12") (d (list (d (n "zng-ext-undo") (r "^0.2.12") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "0ybsymas4vdy6f9px7qqyf4iyl8q18nfnmdd0pgqfb8fvqpnrvg2")))

(define-public crate-zng-wgt-undo-0.2.13 (c (n "zng-wgt-undo") (v "0.2.13") (d (list (d (n "zng-ext-undo") (r "^0.2.13") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0nds92i2rc1bn9grl65l97gqzdqv1s4zz0vnxnmgm4phr30ff67s")))

