(define-module (crates-io zn g- zng-peek-poke) #:use-module (crates-io))

(define-public crate-zng-peek-poke-0.3.0 (c (n "zng-peek-poke") (v "0.3.0") (d (list (d (n "euclid") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "peek-poke-derive") (r "^0.3") (o #t) (d #t) (k 0) (p "zng-peek-poke-derive")))) (h "075qq9gbyxcgp1r70y5zaqaahg0ar70x7i7cc5z6fldmf1czx4jr") (f (quote (("extras" "derive" "euclid") ("derive" "peek-poke-derive") ("default" "derive"))))))

(define-public crate-zng-peek-poke-0.3.1 (c (n "zng-peek-poke") (v "0.3.1") (d (list (d (n "euclid") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "peek-poke-derive") (r "^0.3") (o #t) (d #t) (k 0) (p "zng-peek-poke-derive")))) (h "089lb4dvprv04bkv5fh1y0biwz5bl2l5vw082ynyr1x5x1ywcy12") (f (quote (("extras" "derive" "euclid") ("derive" "peek-poke-derive") ("default" "derive"))))))

