(define-module (crates-io zn g- zng-unit) #:use-module (crates-io))

(define-public crate-zng-unit-0.2.0 (c (n "zng-unit") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y3133ahj4z0gshs2lc1ymhxhnlb59p8dqg4kzj4h34b3a3z3jn9")))

(define-public crate-zng-unit-0.2.1 (c (n "zng-unit") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cg2qmv1h2lzjfqgi5v0ki0saxhzlnbx1jwdxhs0xryjfjsr251j")))

(define-public crate-zng-unit-0.2.2 (c (n "zng-unit") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x1kr57kpllsh0s6s0d9wifdq781dm1hp4qhs161fkawl82fgc7g")))

(define-public crate-zng-unit-0.2.3 (c (n "zng-unit") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0algph96zlp9m6cfkmc8ps4097cnczg3p5cdjbbr6s62jpzf0hwc")))

(define-public crate-zng-unit-0.2.4 (c (n "zng-unit") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f9g41rllmxsbcglln1h47gl44c6nn64fzsm3yq3f077jjnkwcsi")))

(define-public crate-zng-unit-0.2.5 (c (n "zng-unit") (v "0.2.5") (d (list (d (n "bytemuck") (r "^1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00dh0sd6vz4462q0g4rrxcza3z58z279v6yv467f09hm08znnw51")))

(define-public crate-zng-unit-0.2.6 (c (n "zng-unit") (v "0.2.6") (d (list (d (n "bytemuck") (r "^1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bw61akh0x5n4s4hq5paiqik8ya80fb1avc26n1s13gdnyxk8rsz")))

