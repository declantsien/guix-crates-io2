(define-module (crates-io zn g- zng-wgt-data-view) #:use-module (crates-io))

(define-public crate-zng-wgt-data-view-0.2.1 (c (n "zng-wgt-data-view") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.1") (d #t) (k 0)))) (h "0zfs4zwl1d4b2f6agf2rf5l19vqpkzwd35as7zxwgc82gs6gamig")))

(define-public crate-zng-wgt-data-view-0.2.2 (c (n "zng-wgt-data-view") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.2") (d #t) (k 0)))) (h "0c2j47mncfsqdzj8bi4yjygdz53vi0sdf5yqwnl523f4g525vyji")))

(define-public crate-zng-wgt-data-view-0.2.3 (c (n "zng-wgt-data-view") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.3") (d #t) (k 0)))) (h "182q7sfr6a6ac6byn2rva5gpqw0wipqw48fwgqdvzj8ma8wjz2aj")))

(define-public crate-zng-wgt-data-view-0.2.4 (c (n "zng-wgt-data-view") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.4") (d #t) (k 0)))) (h "1i1mpl4ph2a9pld8aik52nfm6q4kxd09gjw8zlzsjklabhda28kz")))

(define-public crate-zng-wgt-data-view-0.2.6 (c (n "zng-wgt-data-view") (v "0.2.6") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.6") (d #t) (k 0)))) (h "1kzggi1x6a0zx21r250ff2xk99vyd54406yba0q34smg1i4gjav5")))

(define-public crate-zng-wgt-data-view-0.2.7 (c (n "zng-wgt-data-view") (v "0.2.7") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.7") (d #t) (k 0)))) (h "0xx5z9inrl8f6j1dpslrkc6bpginlfl3x5f3m0dc9m22f1k9hl77")))

(define-public crate-zng-wgt-data-view-0.2.8 (c (n "zng-wgt-data-view") (v "0.2.8") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.8") (d #t) (k 0)))) (h "0ssi0w3yzfqjqfp9dyas5ds5zy6x2qbmihym9680bz4lpwy45vya")))

(define-public crate-zng-wgt-data-view-0.2.9 (c (n "zng-wgt-data-view") (v "0.2.9") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.9") (d #t) (k 0)))) (h "0yn1xayiniid21yl11sx7vybrkjf90728pqkh88qlig1p09zz675")))

(define-public crate-zng-wgt-data-view-0.3.0 (c (n "zng-wgt-data-view") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.10") (d #t) (k 0)))) (h "0djk63cspv10bj3qfp3rbcq27lvb9dy3bn1wq63kxr4mg498li4f")))

(define-public crate-zng-wgt-data-view-0.3.1 (c (n "zng-wgt-data-view") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.11") (d #t) (k 0)))) (h "1kbr60p1sb1ykw64pbc0r9jip8ggd6l9mdwz1pk1k3pi6l7mgvdg")))

(define-public crate-zng-wgt-data-view-0.3.2 (c (n "zng-wgt-data-view") (v "0.3.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.12") (d #t) (k 0)))) (h "0kk6mwi9hrrxibpfapf2p0x1ypmlw1sx94w113jl6j6ma8wp9wqk")))

(define-public crate-zng-wgt-data-view-0.3.3 (c (n "zng-wgt-data-view") (v "0.3.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt-container") (r "^0.2.13") (d #t) (k 0)))) (h "064ybygmig0jrfsji0czlb4dx86l8y21v0q4haks1pyhxdfpg1pa")))

