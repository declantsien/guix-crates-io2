(define-module (crates-io zn g- zng-wgt-checkerboard) #:use-module (crates-io))

(define-public crate-zng-wgt-checkerboard-0.2.1 (c (n "zng-wgt-checkerboard") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "03f9jw3v0bpdvx6f2zxmwspviy0z36f07myqfy5mpz65w4c1bp97")))

(define-public crate-zng-wgt-checkerboard-0.2.2 (c (n "zng-wgt-checkerboard") (v "0.2.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "1qigrcqmry5l1zam7fnhl5ng91srrcan6727ay2m1sp2j5ck5w25")))

(define-public crate-zng-wgt-checkerboard-0.2.3 (c (n "zng-wgt-checkerboard") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "0pswkih6riz3nhcpfzackyc4hf98fr5qvps07m5kf42n7cmh8isx")))

(define-public crate-zng-wgt-checkerboard-0.2.4 (c (n "zng-wgt-checkerboard") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "0650i7fyxsw1sgxsrxs642nk2cb9fv4i1x0bzzwh9v69afp4d9dw")))

(define-public crate-zng-wgt-checkerboard-0.2.6 (c (n "zng-wgt-checkerboard") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "14fcp7xzkmpdkr087lvhgyv1n6m0qmc1mrlyyla9k8ncjydjjz9s")))

(define-public crate-zng-wgt-checkerboard-0.2.7 (c (n "zng-wgt-checkerboard") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "1s7vlisy9mnsarc1j99n0mbklfnmm3v4h4yj164ay32kvdw1qv9r")))

(define-public crate-zng-wgt-checkerboard-0.2.8 (c (n "zng-wgt-checkerboard") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "1apzfmznc96dpahldbsmbnzdpngnlfdcv39k0ggzcrfav1zlspz6")))

(define-public crate-zng-wgt-checkerboard-0.2.9 (c (n "zng-wgt-checkerboard") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "0ms2nn55x4zzsmis6x23hqvma7g5hhbjxn9ls14vd0c3s6b4zkbd")))

(define-public crate-zng-wgt-checkerboard-0.3.0 (c (n "zng-wgt-checkerboard") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "1p8zv76jdsnagw0s801hi214bwbvmzvd9djgvjqnqwr3a35pbry4")))

(define-public crate-zng-wgt-checkerboard-0.3.1 (c (n "zng-wgt-checkerboard") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.11") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "1dxja6dvmkw8a4f36ynrm84jk6q3rhnnbc29w1nbm3qwfsad6mh3")))

(define-public crate-zng-wgt-checkerboard-0.3.2 (c (n "zng-wgt-checkerboard") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.12") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "09fphn1xvvmf2x1hr1aczcr0fqj8dmkvhqxyan3y00rfllkncpqd")))

(define-public crate-zng-wgt-checkerboard-0.3.3 (c (n "zng-wgt-checkerboard") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zng-color") (r "^0.2.13") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0ijqgl99ilrg78v1j943cmkpx5wsb2ssp1j2smgafylpbyxcsshx")))

