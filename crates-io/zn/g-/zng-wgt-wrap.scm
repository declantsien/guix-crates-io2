(define-module (crates-io zn g- zng-wgt-wrap) #:use-module (crates-io))

(define-public crate-zng-wgt-wrap-0.2.2 (c (n "zng-wgt-wrap") (v "0.2.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.2") (d #t) (k 0)))) (h "1s8ydfydrbv59lyw0qsx9inkdys0f2kdaqjy7lxd3sc23al319ax")))

(define-public crate-zng-wgt-wrap-0.2.3 (c (n "zng-wgt-wrap") (v "0.2.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.3") (d #t) (k 0)))) (h "1zh16ngz326f74a563ipf004iqwwyd066r5fxwfzi7nvz1rbng0q")))

(define-public crate-zng-wgt-wrap-0.2.4 (c (n "zng-wgt-wrap") (v "0.2.4") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.4") (d #t) (k 0)))) (h "1bf9cy8m37d2ayncgsflfdfxm0821x2lf4ha250fkhhv3dzwbxsv")))

(define-public crate-zng-wgt-wrap-0.2.5 (c (n "zng-wgt-wrap") (v "0.2.5") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.5") (d #t) (k 0)))) (h "194qhkljz07g6fn804qzv0bigm4jcbkjijlpny125lb35cki4ak2")))

(define-public crate-zng-wgt-wrap-0.2.7 (c (n "zng-wgt-wrap") (v "0.2.7") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.7") (d #t) (k 0)))) (h "1v280bx20ldyw80d27793p6vhyzx1nmy7jbf0m0lfbzgi4xx4wl0")))

(define-public crate-zng-wgt-wrap-0.2.8 (c (n "zng-wgt-wrap") (v "0.2.8") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.8") (d #t) (k 0)))) (h "08h3bh96f3wb4pfxr2807m84ygr7myb47wr792gys60flkjd35a5")))

(define-public crate-zng-wgt-wrap-0.2.9 (c (n "zng-wgt-wrap") (v "0.2.9") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.9") (d #t) (k 0)))) (h "0r47wacscqb420avmgl8xj4jhjvin0f28kfgihwf99zqid4hm970")))

(define-public crate-zng-wgt-wrap-0.2.10 (c (n "zng-wgt-wrap") (v "0.2.10") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.2.10") (d #t) (k 0)))) (h "0nrhrh9acb3grwgd51ir6icnr0yq0fqxd4ink7fj31lw35v4ic8i")))

(define-public crate-zng-wgt-wrap-0.3.0 (c (n "zng-wgt-wrap") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.6") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.3.0") (d #t) (k 0)))) (h "0ff0vgwc8hs2cwsw379xy4xylgh4rji9whk7p56vh5wcw9cjck73")))

(define-public crate-zng-wgt-wrap-0.3.1 (c (n "zng-wgt-wrap") (v "0.3.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.7") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.3.1") (d #t) (k 0)))) (h "1jig8ww8mvj0jlkxm9wi68kzv5fbw49x89smm0hmp6xn97fy79gk")))

(define-public crate-zng-wgt-wrap-0.3.2 (c (n "zng-wgt-wrap") (v "0.3.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.5.0") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.8") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.3.2") (d #t) (k 0)))) (h "18qglvbzm8lsyyjlv9g9m5b3a4vf69h9szkv6g923026mk6gg8xy")))

(define-public crate-zng-wgt-wrap-0.3.3 (c (n "zng-wgt-wrap") (v "0.3.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.5.1") (d #t) (k 0)) (d (n "zng-ext-font") (r "^0.3.9") (d #t) (k 0)) (d (n "zng-layout") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt-text") (r "^0.3.3") (d #t) (k 0)))) (h "18rfsmnmyziql405nd2z7idm2q5k0vx78hrnv1cflww4518cs9x8")))

