(define-module (crates-io zn g- zng-glsl-to-cxx) #:use-module (crates-io))

(define-public crate-zng-glsl-to-cxx-0.1.0 (c (n "zng-glsl-to-cxx") (v "0.1.0") (d (list (d (n "glsl") (r "^6.0") (d #t) (k 0)))) (h "0m0k7dayc8wf076zzkwynrxik9q173iv9vh1ngrzs3yn95jbg4wd")))

(define-public crate-zng-glsl-to-cxx-0.1.1 (c (n "zng-glsl-to-cxx") (v "0.1.1") (d (list (d (n "glsl") (r "^6.0") (d #t) (k 0)))) (h "16dsqgqir70nqx5v84f1h8hwr9mg5ksj7r9rlryrp2g3h0rxa9wn")))

