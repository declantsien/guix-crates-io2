(define-module (crates-io zn g- zng-swgl) #:use-module (crates-io))

(define-public crate-zng-swgl-0.1.0 (c (n "zng-swgl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "gleam") (r "^0.15") (d #t) (k 0)) (d (n "glsl-to-cxx") (r "^0.1.0") (d #t) (k 1) (p "zng-glsl-to-cxx")) (d (n "webrender-build") (r "^0.0.2") (d #t) (k 1) (p "zng-webrender-build")))) (h "0rbswzgwj6n818bwisnxqlqfspa50pzjf4xd9lhjin4c7z5wmw6r")))

(define-public crate-zng-swgl-0.1.1 (c (n "zng-swgl") (v "0.1.1") (d (list (d (n "cc") (r "=1.0.90") (d #t) (k 1)) (d (n "gleam") (r "^0.15") (d #t) (k 0)) (d (n "glsl-to-cxx") (r "^0.1.0") (d #t) (k 1) (p "zng-glsl-to-cxx")) (d (n "webrender-build") (r "^0.0.2") (d #t) (k 1) (p "zng-webrender-build")))) (h "10lwdycyjczb2j7x5388cp1jbibmh5pxxy5xrs26dl8373gq0xya")))

(define-public crate-zng-swgl-0.1.2 (c (n "zng-swgl") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "gleam") (r "^0.15") (d #t) (k 0)) (d (n "glsl-to-cxx") (r "^0.1.1") (d #t) (k 1) (p "zng-glsl-to-cxx")) (d (n "webrender-build") (r "^0.0.3") (d #t) (k 1) (p "zng-webrender-build")))) (h "0sj5znrnbgc7amfw958qi222nd5mqx4nqlzfpw013xiiac487msk")))

