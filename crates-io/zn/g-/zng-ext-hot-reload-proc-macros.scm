(define-module (crates-io zn g- zng-ext-hot-reload-proc-macros) #:use-module (crates-io))

(define-public crate-zng-ext-hot-reload-proc-macros-0.1.1 (c (n "zng-ext-hot-reload-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nishjrmvnl40cxnygrb7hhd28g2kmqhbr9fdq45awn7vg4gac2z")))

