(define-module (crates-io zn g- zng-txt) #:use-module (crates-io))

(define-public crate-zng-txt-0.2.0 (c (n "zng-txt") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1gqfdanw4r5zikk8vnv7fplf49f22srd9xnk7m2whrgdgh6kmgbm")))

(define-public crate-zng-txt-0.2.1 (c (n "zng-txt") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0mammn26py2g1c31vkwq99ng5gvr0cp2fsw924mihijqmxr6xpp0")))

(define-public crate-zng-txt-0.2.2 (c (n "zng-txt") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0w8bj3k3l9yfni7d4p49hgkp5zhrzjyr1gh78dnc0zg41074gv8v")))

(define-public crate-zng-txt-0.2.3 (c (n "zng-txt") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1mfkwjnd96b04csvbyn0d5j1cy4j25ik645ms2liqvxzxhk4hjq5")))

(define-public crate-zng-txt-0.2.4 (c (n "zng-txt") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0kh93nl9hl4wy0f1xflm2qz381m84bqpvxkrlsbymr0nz2ina6vf")))

(define-public crate-zng-txt-0.2.5 (c (n "zng-txt") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1a9lq8sjj1q6gbikaixhyrvzyv87ky02nhzb1fbgs26ln8ds5m6v")))

(define-public crate-zng-txt-0.2.6 (c (n "zng-txt") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "159xdvzvml7xpidw71m5y97k2a7xbrka86p8xqwgzcm0kd2fwzf0")))

