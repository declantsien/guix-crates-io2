(define-module (crates-io zn g- zng-wgt-container) #:use-module (crates-io))

(define-public crate-zng-wgt-container-0.2.1 (c (n "zng-wgt-container") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "070f7diq9awmn6l26d9ai9341a85izj8q94jfza19ky1207pg7l0")))

(define-public crate-zng-wgt-container-0.2.2 (c (n "zng-wgt-container") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "0xwdfz6948dg0n21fqra9bflqjs9kj2kjhp5ys1gs9mbd71xs9ma")))

(define-public crate-zng-wgt-container-0.2.3 (c (n "zng-wgt-container") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "0vn5rdkww623fkzswz8pncxi204x5caqr4p9p3yaj6glrs1pp0kv")))

(define-public crate-zng-wgt-container-0.2.4 (c (n "zng-wgt-container") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "164vjdzfvlz7j9y3d2yq6znknn0hhyvzg9m37xrc00z31phy0l7y")))

(define-public crate-zng-wgt-container-0.2.6 (c (n "zng-wgt-container") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "1ybwbhx53vavinz04kppnlc2prbmmxrds104rpk0kb834bjfysjg")))

(define-public crate-zng-wgt-container-0.2.7 (c (n "zng-wgt-container") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "0yl8nw198kbhc285bq94d5416mikchw74iaid2apfb50m8d96h0r")))

(define-public crate-zng-wgt-container-0.2.8 (c (n "zng-wgt-container") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "0xn3xjpc46ib74n91hf8zann51d26av1g6rnxgbg2578ps6ma9w2")))

(define-public crate-zng-wgt-container-0.2.9 (c (n "zng-wgt-container") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "0yxg9x89ngxybirp34li41c84jpi4snp94ss5i9dnk7sw4qmxg32")))

(define-public crate-zng-wgt-container-0.2.10 (c (n "zng-wgt-container") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "153pcprnbhwpwbfb4pxsli4x12k315cxyl27zwzyvliy2ldf8ss0")))

(define-public crate-zng-wgt-container-0.2.11 (c (n "zng-wgt-container") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "1jyfw0bdlg4291aqq567yavnwa5mg52gh91f5vxm0fpighw27l2m")))

(define-public crate-zng-wgt-container-0.2.12 (c (n "zng-wgt-container") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.5.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "0jknfplxnqvynld1cv8f4fcdlaqaixykf2gg5j542i10zk7b1b98")))

(define-public crate-zng-wgt-container-0.2.13 (c (n "zng-wgt-container") (v "0.2.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-app") (r "^0.5.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0babyrmm775z8plzjvcnzkd4amhnidvx9gzsf9c877f9065m8wia")))

