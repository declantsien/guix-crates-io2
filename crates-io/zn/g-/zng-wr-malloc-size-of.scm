(define-module (crates-io zn g- zng-wr-malloc-size-of) #:use-module (crates-io))

(define-public crate-zng-wr-malloc-size-of-0.0.2 (c (n "zng-wr-malloc-size-of") (v "0.0.2") (d (list (d (n "app_units") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)))) (h "0rbnh7q8hkch5k7arpwkqw19qmwqld3xjagq4g8cj5f5z1fpms2s")))

(define-public crate-zng-wr-malloc-size-of-0.0.3 (c (n "zng-wr-malloc-size-of") (v "0.0.3") (d (list (d (n "app_units") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)))) (h "1r7943aw61617dlvly4hkas9m2906jszdbrrv51am3152962vm5b")))

