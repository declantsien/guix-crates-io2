(define-module (crates-io zn g- zng-clone-move) #:use-module (crates-io))

(define-public crate-zng-clone-move-0.2.0 (c (n "zng-clone-move") (v "0.2.0") (h "1n98w9kvwfvfa7rn6smg319l1njb8hibfclmpqnmznr5jz98dchb")))

(define-public crate-zng-clone-move-0.2.1 (c (n "zng-clone-move") (v "0.2.1") (h "0awy32pzaj1xaaa0flrqfc4p4hihcg6dljazglfj15rqcjyfdcwl")))

(define-public crate-zng-clone-move-0.2.2 (c (n "zng-clone-move") (v "0.2.2") (h "0rx6ycnr7w9as1x3n318z30dypq1qvrlxhygn058ljk95sq4s20x")))

(define-public crate-zng-clone-move-0.2.3 (c (n "zng-clone-move") (v "0.2.3") (h "0n77v6qi4jg76ax8hz6wxi9ndvyllrxpmlkq3dhcy3cbwgri36ff")))

(define-public crate-zng-clone-move-0.2.4 (c (n "zng-clone-move") (v "0.2.4") (h "1h8a5xm5sh3l3gdpl27kjgw5np48mmk693yrr4b25nmh9363wg53")))

