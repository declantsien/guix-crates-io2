(define-module (crates-io zn g- zng-state-map) #:use-module (crates-io))

(define-public crate-zng-state-map-0.2.0 (c (n "zng-state-map") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.0") (d #t) (k 0)))) (h "1mgp93v3kgfqs5464jlr34w4j819p8hxi4aw1d87bqlx84kpd625")))

(define-public crate-zng-state-map-0.2.1 (c (n "zng-state-map") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.1") (d #t) (k 0)))) (h "1bsi5894qydg392y0js8nxsvb8w4g0p22094dsrz4m6lx8sq4a4h")))

(define-public crate-zng-state-map-0.2.2 (c (n "zng-state-map") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.2") (d #t) (k 0)))) (h "0w911lydj8af9mnxq0dq3zv3sz4vdr5nqdfhb8ifya4nc4hy0n05")))

(define-public crate-zng-state-map-0.2.3 (c (n "zng-state-map") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.3") (d #t) (k 0)))) (h "1nc25zdj509p1ik4wxz4h575k60nhkqvqxvg51vpg0smmpjx6b5j")))

(define-public crate-zng-state-map-0.2.4 (c (n "zng-state-map") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.4") (d #t) (k 0)))) (h "1q7iy156k1xmg02zrp0sk8kcp9dcrzicim1n3v49ibfd9kxnhszm")))

(define-public crate-zng-state-map-0.2.5 (c (n "zng-state-map") (v "0.2.5") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.2.5") (d #t) (k 0)))) (h "0p8ix3vz6k43ray3j2nygz17dyb5h9wzx4cgs5wcpkpvqr6q0jr3")))

(define-public crate-zng-state-map-0.2.6 (c (n "zng-state-map") (v "0.2.6") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.3.0") (d #t) (k 0)))) (h "0zig37ibfhhhl3zydqxix1nzwjwxvzyz8p59j4cm531cfndxs6gn")))

(define-public crate-zng-state-map-0.2.7 (c (n "zng-state-map") (v "0.2.7") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.3.1") (d #t) (k 0)))) (h "1s9i3ag1km21z27ih416b38nidjdh2a74fnv3gqbgjx4688mnyw9")))

(define-public crate-zng-state-map-0.3.0 (c (n "zng-state-map") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.15") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0") (d #t) (k 0)) (d (n "zng-unique-id") (r "^0.4.0") (d #t) (k 0)))) (h "178ffpf0065nw28ragbhl4ghcnxcnm7pbxbz4v4w055sqs0nvn8n")))

