(define-module (crates-io zn g- zng-wgt-style) #:use-module (crates-io))

(define-public crate-zng-wgt-style-0.2.1 (c (n "zng-wgt-style") (v "0.2.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "0642rmbklpi6y93ghwb5xvky1xcxm0xki5szivwnss51rqc9mcs8")))

(define-public crate-zng-wgt-style-0.2.2 (c (n "zng-wgt-style") (v "0.2.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "0k9z2c5jdjh4bn9mmzdgw6268akn9yfn7fs97j0vqkfiiv9mgli8")))

(define-public crate-zng-wgt-style-0.2.3 (c (n "zng-wgt-style") (v "0.2.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "031yfw53d228c8z92w1c0vnab3gsrh29dxhi7ylpbp02ibgm5l3j")))

(define-public crate-zng-wgt-style-0.2.4 (c (n "zng-wgt-style") (v "0.2.4") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "004mva0d90zlxwk56r8y60m7xkllnxk1aglzyk68gqi2l6i1v1j1")))

(define-public crate-zng-wgt-style-0.2.6 (c (n "zng-wgt-style") (v "0.2.6") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0j9i8rqbhc1954s0sf18fifja40b04r0ly8zfm6rb1d2ldnpygww")))

(define-public crate-zng-wgt-style-0.2.7 (c (n "zng-wgt-style") (v "0.2.7") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "1qk6lhyidh3pk5k81z66cdzkldq3rj79h0and70m9d6llxcp70wj")))

(define-public crate-zng-wgt-style-0.2.8 (c (n "zng-wgt-style") (v "0.2.8") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "1yk2n6b5ihx1hscsa191scq4r6dm7vxrrsk4q39v19id2za9in61")))

(define-public crate-zng-wgt-style-0.2.9 (c (n "zng-wgt-style") (v "0.2.9") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "199xav3a0kmja33jk9whm8qz6hw2dxmjxl5q2wm78as2vfkj5zxm")))

(define-public crate-zng-wgt-style-0.3.0 (c (n "zng-wgt-style") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "1l6nnxcb66dadbr90zjk9ggx4s181hqah9hr68bc0y67qd5sp6fc")))

(define-public crate-zng-wgt-style-0.3.1 (c (n "zng-wgt-style") (v "0.3.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "1apgxxy4sv1a3frc0iihrd7pdgvvyvd4fg7z9a15psvmi55kwrzl")))

(define-public crate-zng-wgt-style-0.3.2 (c (n "zng-wgt-style") (v "0.3.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.5.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "0ak867m8rrg9k70vncb2fs269077cw7xsqmqsg8645j6dr3jji3g") (f (quote (("trace_widget" "zng-app/trace_widget"))))))

(define-public crate-zng-wgt-style-0.3.3 (c (n "zng-wgt-style") (v "0.3.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zng-app") (r "^0.5.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "01xkh36wif054iv4fg0pdp3dj6csm18n7dkw2gsj500bfass7sz2") (f (quote (("trace_widget" "zng-app/trace_widget"))))))

