(define-module (crates-io zn g- zng-color-proc-macros) #:use-module (crates-io))

(define-public crate-zng-color-proc-macros-0.2.0 (c (n "zng-color-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pbch6m2msanz0ax83ckcnknn24yd2wcz5igmj1c1ygc0rcyp3gc")))

(define-public crate-zng-color-proc-macros-0.2.1 (c (n "zng-color-proc-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1a50l01ma8px6airvxs7ynb1ridkx92s2fcysas0z9gljqnl6fyn")))

(define-public crate-zng-color-proc-macros-0.2.2 (c (n "zng-color-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xhwdq0qw06khz6p23m3xcnp21x240pkfw52fv7wh5smvm1pjip3")))

(define-public crate-zng-color-proc-macros-0.2.3 (c (n "zng-color-proc-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lqamq6ch73kp2fa08gkq3jqyrf2d3nvg2yzag0hgp63kl3l97ks")))

(define-public crate-zng-color-proc-macros-0.2.4 (c (n "zng-color-proc-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14i2sb4l73jyfdiyy8k955hn2bna3vpqz40p26z4a82qx4ya8m5k")))

(define-public crate-zng-color-proc-macros-0.2.5 (c (n "zng-color-proc-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06kwn99i6g4bsfg0hh7kiwrf5d5wshgx2ixgzg38jl776xifyx3d")))

