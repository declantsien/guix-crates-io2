(define-module (crates-io zn g- zng-wgt-size-offset) #:use-module (crates-io))

(define-public crate-zng-wgt-size-offset-0.2.1 (c (n "zng-wgt-size-offset") (v "0.2.1") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "1gqg94hfhzkb2bqc4pk81g1k562j5ahy2m1l3pk082rbldlq68p2")))

(define-public crate-zng-wgt-size-offset-0.2.2 (c (n "zng-wgt-size-offset") (v "0.2.2") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "15xjxmcqs5xgaisi5km5x1kyjlv7pvawm2ix7wv8dshv1g4615j2")))

(define-public crate-zng-wgt-size-offset-0.2.3 (c (n "zng-wgt-size-offset") (v "0.2.3") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "0spccxk3zfbynz35ra1hnbj9p60zzs2hadmjl7768nk71pc0q2z7")))

(define-public crate-zng-wgt-size-offset-0.2.4 (c (n "zng-wgt-size-offset") (v "0.2.4") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "02dj98bdkv5v0vrb1pz816fv8d88d6jaf1i66vxy2pxwlw9srb1f")))

(define-public crate-zng-wgt-size-offset-0.2.6 (c (n "zng-wgt-size-offset") (v "0.2.6") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0ia5nsr8n1vqwbkkywka2nf2ls6g5jqz9jh4ahwscmnk0pyl818b")))

(define-public crate-zng-wgt-size-offset-0.2.7 (c (n "zng-wgt-size-offset") (v "0.2.7") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "0lpbf7dighm41674qsr5xdjmivz8445r14k28mb2yw1ksal11y3j")))

(define-public crate-zng-wgt-size-offset-0.2.8 (c (n "zng-wgt-size-offset") (v "0.2.8") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "0m7bw6s252yznjfg21qlrhpqbb657dnwp3i41v3l1plz8r4h9bk9")))

(define-public crate-zng-wgt-size-offset-0.2.9 (c (n "zng-wgt-size-offset") (v "0.2.9") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "1w047jy9a5jhzzn6mm8pv9j2d6ixk8c63jwlizizin4jaiq5536b")))

(define-public crate-zng-wgt-size-offset-0.2.10 (c (n "zng-wgt-size-offset") (v "0.2.10") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "05j271qm3x7gq2f82v8biqs3rw3ii8a4rpjachqn1x741c1j6kzy")))

(define-public crate-zng-wgt-size-offset-0.2.11 (c (n "zng-wgt-size-offset") (v "0.2.11") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "1qw28fpr6fhvxlxg164aarsdc1ns5rhh2k68nhwn77z2cswi2jxz")))

(define-public crate-zng-wgt-size-offset-0.2.12 (c (n "zng-wgt-size-offset") (v "0.2.12") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "0sq3kg68r76j4b83fx6ycm589d923p43350xiwainzfgjavbxqkq")))

(define-public crate-zng-wgt-size-offset-0.2.13 (c (n "zng-wgt-size-offset") (v "0.2.13") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0h6mhicmx6yi0xiic4qmxa143hpqd07hwnzx9i0jjlaw12fyhi24")))

