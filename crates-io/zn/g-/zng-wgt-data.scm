(define-module (crates-io zn g- zng-wgt-data) #:use-module (crates-io))

(define-public crate-zng-wgt-data-0.2.1 (c (n "zng-wgt-data") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "12j7crakybdqz8z492fw857j3s2v71g40x1wvyfdnq12b72gj453")))

(define-public crate-zng-wgt-data-0.2.2 (c (n "zng-wgt-data") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "06mhkbqf14mnw7pl1a7ws9apd48c8vi3xsgs6ijz786lwga3zyid")))

(define-public crate-zng-wgt-data-0.2.3 (c (n "zng-wgt-data") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "1f4226zz3h109qlcwkqs333lv9wcybd0zkpf7mrfn12scz7qx8q8")))

(define-public crate-zng-wgt-data-0.2.4 (c (n "zng-wgt-data") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "0b5ncnhs37xig5aqxfnm957iazv6n5hz4r0xgbmlbkfx2jsv5csy")))

(define-public crate-zng-wgt-data-0.2.6 (c (n "zng-wgt-data") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "1narcix9vcdxfq60wdq8zfbd15yw2q9yhiqn72nlv7a1aarxf8qd")))

(define-public crate-zng-wgt-data-0.2.7 (c (n "zng-wgt-data") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.5") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "06xam7k4474l7cm50ldzkqvmlgnb23j9ag6h2blnj2nhppxkd7cx")))

(define-public crate-zng-wgt-data-0.2.8 (c (n "zng-wgt-data") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.8") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "0fa9rhb1q5f7zf5s2h6slnxrm1ly8s6lixdwdgg075x80r25k11q")))

(define-public crate-zng-wgt-data-0.2.9 (c (n "zng-wgt-data") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.9") (d #t) (k 0)) (d (n "zng-var") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "1z5jq355i4vmrw671pb8jjbm1bdw0qy253dp5ihs8xaas7wk06gm")))

(define-public crate-zng-wgt-data-0.2.10 (c (n "zng-wgt-data") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.10") (d #t) (k 0)) (d (n "zng-var") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "0m4nyzs708s54gxkv8zf2bhc6j2p83jmzzgc4jzxj1knlm1vf3j1")))

(define-public crate-zng-wgt-data-0.2.11 (c (n "zng-wgt-data") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.11") (d #t) (k 0)) (d (n "zng-var") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "00rzvn5ps6b33jqlsg649xgsyn3wg1myn430jwn8mvxgfp1bzcs2")))

(define-public crate-zng-wgt-data-0.2.12 (c (n "zng-wgt-data") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.12") (d #t) (k 0)) (d (n "zng-var") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "12kf4m1bcmy8ns8k5lzyz4clc5if4lsp7jgibsqx6q3b8rdmwj6r")))

(define-public crate-zng-wgt-data-0.2.13 (c (n "zng-wgt-data") (v "0.2.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zng-color") (r "^0.2.13") (d #t) (k 0)) (d (n "zng-var") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0siv0vnxk42alh0ch9z70gakiiizzrnn5hyfzvpbzlgj9pv6s60m")))

