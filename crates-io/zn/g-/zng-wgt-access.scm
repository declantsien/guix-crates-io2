(define-module (crates-io zn g- zng-wgt-access) #:use-module (crates-io))

(define-public crate-zng-wgt-access-0.2.1 (c (n "zng-wgt-access") (v "0.2.1") (d (list (d (n "zng-app") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.1") (d #t) (k 0)))) (h "0h0fzasn6kbyfvzfyi1cxlmhmbmgy4a3xp32p92fclbp9i5gkqg3")))

(define-public crate-zng-wgt-access-0.2.2 (c (n "zng-wgt-access") (v "0.2.2") (d (list (d (n "zng-app") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.2") (d #t) (k 0)))) (h "1drszfzgxdds75rc2lngy0rcg2m6kgm2pz987nwfbd86ampr5fzp")))

(define-public crate-zng-wgt-access-0.2.3 (c (n "zng-wgt-access") (v "0.2.3") (d (list (d (n "zng-app") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.3") (d #t) (k 0)))) (h "0gmygk66442zlr6452mgi19s80bl76mwn5bqw9jpidh0laic1aik")))

(define-public crate-zng-wgt-access-0.2.4 (c (n "zng-wgt-access") (v "0.2.4") (d (list (d (n "zng-app") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.4") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.4") (d #t) (k 0)))) (h "1yzmb930idlvy2svzzbqdd46nlml390dzr6498fcsz27i48gx702")))

(define-public crate-zng-wgt-access-0.2.6 (c (n "zng-wgt-access") (v "0.2.6") (d (list (d (n "zng-app") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.2") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.6") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.6") (d #t) (k 0)))) (h "0gsy72cp1qlzq9ndjph0pmhgwxxfwrd0cv523laag092i3ja1ysr")))

(define-public crate-zng-wgt-access-0.2.7 (c (n "zng-wgt-access") (v "0.2.7") (d (list (d (n "zng-app") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.3") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.2.7") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.7") (d #t) (k 0)))) (h "19v274l6srs2jwjnhhm1hk8gggphmxiw9p1yr6psf57710lq8vi5")))

(define-public crate-zng-wgt-access-0.2.8 (c (n "zng-wgt-access") (v "0.2.8") (d (list (d (n "zng-app") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.4") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.3.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.8") (d #t) (k 0)))) (h "01y9fi0lzngydg0zbha636n6jfg717l0ib2iy8c9gdr889pi991r")))

(define-public crate-zng-wgt-access-0.2.9 (c (n "zng-wgt-access") (v "0.2.9") (d (list (d (n "zng-app") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.5") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.3.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.2.9") (d #t) (k 0)))) (h "0bgsj4q6hfc8brwcb5aq2vzjdf8jbfx1fb6rsjpgmpvsd84nayhr")))

(define-public crate-zng-wgt-access-0.2.10 (c (n "zng-wgt-access") (v "0.2.10") (d (list (d (n "zng-app") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.6") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.4.0") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.0") (d #t) (k 0)))) (h "0hsl43wj1avn45l2la6a9yn6fm0hyvrr1idn92a1nqxjwzmqmp6b")))

(define-public crate-zng-wgt-access-0.2.11 (c (n "zng-wgt-access") (v "0.2.11") (d (list (d (n "zng-app") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.7") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.4.1") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.1") (d #t) (k 0)))) (h "084h4n23idg7ankvrpk392j3r3pwpxvv5avb385s7x5ssh4n6qvy")))

(define-public crate-zng-wgt-access-0.2.12 (c (n "zng-wgt-access") (v "0.2.12") (d (list (d (n "zng-app") (r "^0.5.0") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.8") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.4.2") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.2") (d #t) (k 0)))) (h "0rh87zdshlrnyfxqfkwlm19drkh3x36ma3p10mrjcz8dxgmb6ywd")))

(define-public crate-zng-wgt-access-0.2.13 (c (n "zng-wgt-access") (v "0.2.13") (d (list (d (n "zng-app") (r "^0.5.1") (d #t) (k 0)) (d (n "zng-ext-l10n") (r "^0.3.9") (d #t) (k 0)) (d (n "zng-view-api") (r "^0.4.3") (d #t) (k 0)) (d (n "zng-wgt") (r "^0.3.3") (d #t) (k 0)))) (h "0qbcmrqx57cxxii497iyx505jv6jvg8mha2rsmcibj1wizxwr0m9")))

