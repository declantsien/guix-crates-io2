(define-module (crates-io zn g- zng-peek-poke-derive) #:use-module (crates-io))

(define-public crate-zng-peek-poke-derive-0.3.0 (c (n "zng-peek-poke-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "02y2s8545q1nkvxkdylia9brv4akn9l20bd989lcs2kv1fnfra6b")))

(define-public crate-zng-peek-poke-derive-0.3.1 (c (n "zng-peek-poke-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0by26n05811zpcncyd1d174w8rdd0y2ggd40vcnq5pvzr2an6fxp")))

