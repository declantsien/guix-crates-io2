(define-module (crates-io zn g- zng-l10n-scraper) #:use-module (crates-io))

(define-public crate-zng-l10n-scraper-0.2.0 (c (n "zng-l10n-scraper") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "0n1qyvb5i8hqsng1y0ph0dywr4c9dwkzlgvagksm6f5myljwb7hf")))

(define-public crate-zng-l10n-scraper-0.2.1 (c (n "zng-l10n-scraper") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "1yz41xdk7x6z0cq0hy0rzjd890ilwis8nc5xla4xwyklc4cnm4zk")))

(define-public crate-zng-l10n-scraper-0.2.2 (c (n "zng-l10n-scraper") (v "0.2.2") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "174qdpvqql84s59bsnbkiwccfisk051y24a2i556fllgf4fj73d3")))

(define-public crate-zng-l10n-scraper-0.2.3 (c (n "zng-l10n-scraper") (v "0.2.3") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "0qlc57m2mgm1n6sdr3jns8kvfw2m2csa6q97xm17gkx0irvsmdgn")))

(define-public crate-zng-l10n-scraper-0.2.4 (c (n "zng-l10n-scraper") (v "0.2.4") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "0dcsrcyshg9swwm7y3hz8rqnv3b4k0sg46mbjx1v70n26fjk8wp2")))

(define-public crate-zng-l10n-scraper-0.2.5 (c (n "zng-l10n-scraper") (v "0.2.5") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "1g3skqss85vm0cbypp57j8pjwhw5x8w3rap34594kzj3wy3wasbj")))

(define-public crate-zng-l10n-scraper-0.3.0 (c (n "zng-l10n-scraper") (v "0.3.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "rustc_lexer") (r "^0.1") (d #t) (k 0)))) (h "1znswr6wnsd2rghm2ask2h5b3acfqjkhc8zc8w7rakgybgjx3ls8")))

