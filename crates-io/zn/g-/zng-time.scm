(define-module (crates-io zn g- zng-time) #:use-module (crates-io))

(define-public crate-zng-time-0.2.0 (c (n "zng-time") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.2.0") (d #t) (k 0)))) (h "003176zyja03scavi278ms86ws5cca0ms4jhnj3fvaq3lkhdiy4g")))

(define-public crate-zng-time-0.2.1 (c (n "zng-time") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.2.1") (d #t) (k 0)))) (h "1la4baw55s0r65cvs9vynvj32ka6kzypgns7h6jaj0ygmsm5zprk")))

(define-public crate-zng-time-0.2.2 (c (n "zng-time") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.2.2") (d #t) (k 0)))) (h "1z3l960bg261yrl2n3nvf3aiscrdry2xmbv24vms5aywlp0gdn2k")))

(define-public crate-zng-time-0.2.3 (c (n "zng-time") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.3.0") (d #t) (k 0)))) (h "0kxvqz08rkxnkpn9nkgahwbgrf9x291b3qryy8wz299f2wwgjgp7")))

(define-public crate-zng-time-0.2.4 (c (n "zng-time") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.3.1") (d #t) (k 0)))) (h "0b7nxnyr6bg9rxm585ci4wxgc2g0kqq31f82ffaj1cl0sjqj0j5x")))

(define-public crate-zng-time-0.2.5 (c (n "zng-time") (v "0.2.5") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.3.2") (d #t) (k 0)))) (h "1k0vnxlb1vj5j09bkhgv2blcy76xj8mv7gm05snx295961ba72sy")))

(define-public crate-zng-time-0.2.6 (c (n "zng-time") (v "0.2.6") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.3.3") (d #t) (k 0)))) (h "061jg25f2mr4xchmljm83jl07v0hki059aw7x12affy471qq5qi8")))

(define-public crate-zng-time-0.2.7 (c (n "zng-time") (v "0.2.7") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.3.4") (d #t) (k 0)))) (h "14ankd7dr4vkl3lgmi8qd5kby8i3wnd5f9mlq0iydi0ibpyhw2ph")))

(define-public crate-zng-time-0.2.8 (c (n "zng-time") (v "0.2.8") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "zng-app-context") (r "^0.4.0") (d #t) (k 0)))) (h "1npg4j6m4nixnb97lmrrcv2yf4nnv27ihwhszfcv00vs59zp6b54")))

