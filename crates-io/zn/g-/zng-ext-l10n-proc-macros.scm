(define-module (crates-io zn g- zng-ext-l10n-proc-macros) #:use-module (crates-io))

(define-public crate-zng-ext-l10n-proc-macros-0.2.0 (c (n "zng-ext-l10n-proc-macros") (v "0.2.0") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0af0j5wjyri3am5al7xybqxrdcbl4qjr622pzqr5jdslhjnjngzm")))

(define-public crate-zng-ext-l10n-proc-macros-0.2.1 (c (n "zng-ext-l10n-proc-macros") (v "0.2.1") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "04j14nd3yafbybwab9ii624y909jnx099f4l5jn3nwcvyj13dpz6")))

(define-public crate-zng-ext-l10n-proc-macros-0.2.2 (c (n "zng-ext-l10n-proc-macros") (v "0.2.2") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1n3rpnvirmalzyxrghsfxsnz61sgzl9sbkfkhgyvf66hkszjzbhz")))

(define-public crate-zng-ext-l10n-proc-macros-0.2.3 (c (n "zng-ext-l10n-proc-macros") (v "0.2.3") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "05k32iljfnkim6q1xzidiqxvz3inasz2r25mrm8klcqfl8qh4rn8")))

(define-public crate-zng-ext-l10n-proc-macros-0.2.4 (c (n "zng-ext-l10n-proc-macros") (v "0.2.4") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1vz2g7sbixzrv8v478f6bjvgsmvj24izvanzsplpzvpfm8w2ixqv")))

(define-public crate-zng-ext-l10n-proc-macros-0.2.5 (c (n "zng-ext-l10n-proc-macros") (v "0.2.5") (d (list (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "extra-traits" "derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0g3bnl8mj6zb5031rq4pf6r27j7pmlqs265ss365ksn22v9hj9lr")))

