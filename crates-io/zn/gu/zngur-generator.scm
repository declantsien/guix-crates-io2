(define-module (crates-io zn gu zngur-generator) #:use-module (crates-io))

(define-public crate-zngur-generator-0.0.3 (c (n "zngur-generator") (v "0.0.3") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "^0.0.3") (d #t) (k 0)) (d (n "zngur-parser") (r "^0.0.3") (d #t) (k 0)))) (h "0iwnn2kq67m4yyk4ryvzr5k73r22528vnpxxy6wzljhfri7zvwsj")))

(define-public crate-zngur-generator-0.2.1 (c (n "zngur-generator") (v "0.2.1") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.2.1") (d #t) (k 0)) (d (n "zngur-parser") (r "=0.2.1") (d #t) (k 0)))) (h "0gkqfx30r9fshm3jpmrz3hs74q6pi2snfpdlinspnj2gf5nhz9xv")))

(define-public crate-zngur-generator-0.2.2 (c (n "zngur-generator") (v "0.2.2") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.2.2") (d #t) (k 0)) (d (n "zngur-parser") (r "=0.2.2") (d #t) (k 0)))) (h "0grpw08rxbzf7lyncd18s0dx8dwhhwdqv0iiv976rj4jp3vkdd4r")))

(define-public crate-zngur-generator-0.3.0 (c (n "zngur-generator") (v "0.3.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.3.0") (d #t) (k 0)) (d (n "zngur-parser") (r "=0.3.0") (d #t) (k 0)))) (h "0kjd6q3yir3qsjjj3x9nv4k2dvgmfn76idhfd342mi7icf6kfzz0")))

(define-public crate-zngur-generator-0.4.0 (c (n "zngur-generator") (v "0.4.0") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.4.0") (d #t) (k 0)) (d (n "zngur-parser") (r "=0.4.0") (d #t) (k 0)))) (h "05vl0bkp4pap2qnhw4f005rx0vwsirpmm5fpp5pj8fw3xrm6bqds")))

