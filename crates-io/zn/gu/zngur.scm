(define-module (crates-io zn gu zngur) #:use-module (crates-io))

(define-public crate-zngur-0.0.3 (c (n "zngur") (v "0.0.3") (d (list (d (n "zngur-generator") (r "^0.0.3") (d #t) (k 0)))) (h "1v93k67ysjla9cqxp98pqx1z1g05dx0pb9w09w8xjy82b2fdzlgl")))

(define-public crate-zngur-0.2.1 (c (n "zngur") (v "0.2.1") (d (list (d (n "zngur-generator") (r "=0.2.1") (d #t) (k 0)))) (h "1srihg77s2nhf5hm0zv5ch37rqjhmhgxgiay7miwngp03hvfjaqh")))

(define-public crate-zngur-0.2.2 (c (n "zngur") (v "0.2.2") (d (list (d (n "zngur-generator") (r "=0.2.2") (d #t) (k 0)))) (h "16h2zfj26nxkdgscvas6g42aiqzbk612jf5m9fia0rsw16n9nwmz")))

(define-public crate-zngur-0.3.0 (c (n "zngur") (v "0.3.0") (d (list (d (n "zngur-generator") (r "=0.3.0") (d #t) (k 0)))) (h "034paibw22bbg2qqrdaqh12qa43w58plyq1656s97kscnjajknb3")))

(define-public crate-zngur-0.4.0 (c (n "zngur") (v "0.4.0") (d (list (d (n "zngur-generator") (r "=0.4.0") (d #t) (k 0)))) (h "1g49gjqg12ishvlydk59lbcxfrf09zgzs2q4p9kxqz0grfj810lm")))

