(define-module (crates-io zn gu zngur-parser) #:use-module (crates-io))

(define-public crate-zngur-parser-0.0.3 (c (n "zngur-parser") (v "0.0.3") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "^0.0.3") (d #t) (k 0)))) (h "0zcj6fl223k3yl3vv0a570fa2vx0vfk93qa24bfws548i3nsjs4x")))

(define-public crate-zngur-parser-0.2.1 (c (n "zngur-parser") (v "0.2.1") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.2.1") (d #t) (k 0)))) (h "1gp88kb7jlypwvff1dzdjg1292ssf3p3r7hybp7srd688zy1jpzf")))

(define-public crate-zngur-parser-0.2.2 (c (n "zngur-parser") (v "0.2.2") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "zngur-def") (r "=0.2.2") (d #t) (k 0)))) (h "1w3i6gczb2r6q5dj3a97qm2dn53ryrpjck6256qyn87ri5qc3v8h")))

(define-public crate-zngur-parser-0.3.0 (c (n "zngur-parser") (v "0.3.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)) (d (n "zngur-def") (r "=0.3.0") (d #t) (k 0)))) (h "01gj1f27p6maghbkbrcal3jvmgq7nhkjcdn5ap8s13mf4hw0jn8s")))

(define-public crate-zngur-parser-0.4.0 (c (n "zngur-parser") (v "0.4.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "chumsky") (r "=1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)) (d (n "zngur-def") (r "=0.4.0") (d #t) (k 0)))) (h "0slvbsw9p9wq0l9jzwxzi4rj05f669fcsylf3gj1fharg8wlxyk8")))

