(define-module (crates-io zn gu zngur-cli) #:use-module (crates-io))

(define-public crate-zngur-cli-0.0.3 (c (n "zngur-cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zngur") (r "^0.0.3") (d #t) (k 0)))) (h "1pprmwivks6mha98dqhdwmgyjcl1kz5ss0nrcwqkjj9wd66yh8qj")))

(define-public crate-zngur-cli-0.2.1 (c (n "zngur-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zngur") (r "=0.2.1") (d #t) (k 0)))) (h "1xbvdqwa5mc7p1s1gcp9pk5i1sv3wq858lav3r3li7qmg0wwq8jf")))

(define-public crate-zngur-cli-0.2.2 (c (n "zngur-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zngur") (r "=0.2.2") (d #t) (k 0)))) (h "11fsnyfyv5wv9r1yrfxvlwjvwx9nb0n597r9mg3g74g0jn6fqjrn")))

(define-public crate-zngur-cli-0.3.0 (c (n "zngur-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zngur") (r "=0.3.0") (d #t) (k 0)))) (h "0brmxlh9gi0vy2cw6v6wdm4hg24hq7g0i96icima3q9l44q54jab")))

(define-public crate-zngur-cli-0.4.0 (c (n "zngur-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zngur") (r "=0.4.0") (d #t) (k 0)))) (h "0kcppsw5bsnnkyvxq7szzf7fa8da16caajvc8s4rdff4hzjgg4f9")))

