(define-module (crates-io zn gu zngur-def) #:use-module (crates-io))

(define-public crate-zngur-def-0.0.3 (c (n "zngur-def") (v "0.0.3") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "1ic33pl7srprz2lwynf91v8h85bdj5kvich3wzjn44nz6djsrag4")))

(define-public crate-zngur-def-0.1.1 (c (n "zngur-def") (v "0.1.1") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "1wsp85g76krih0grf48sym07zacwky4rzd66azdb95mqvgmny3mp")))

(define-public crate-zngur-def-0.2.1 (c (n "zngur-def") (v "0.2.1") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "02820hdqh8ldhg49q6sni0j2g4m0792qhd17fqiiqc5qddjxwc20")))

(define-public crate-zngur-def-0.2.2 (c (n "zngur-def") (v "0.2.2") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "0zycqrpwv3scyhvdyr5cisp8fpj60b1hpmynf4crwb8f43cp0x90")))

(define-public crate-zngur-def-0.3.0 (c (n "zngur-def") (v "0.3.0") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "01ygsyqnjrv8ibx40is828z7i4yc0l4vwlp4xrndmsc0hjkm0528")))

(define-public crate-zngur-def-0.4.0 (c (n "zngur-def") (v "0.4.0") (d (list (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "1h8sazp0fs5x2hghz8gqnf9hmpl8hsjw7csr46flpf4vbsb2nw4k")))

