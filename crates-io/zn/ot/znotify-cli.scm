(define-module (crates-io zn ot znotify-cli) #:use-module (crates-io))

(define-public crate-znotify-cli-0.1.0 (c (n "znotify-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo" "std"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "znotify") (r "^0.0.3") (d #t) (k 0)))) (h "17pymz64dqjcf33jh32las4qfgnzwjcm1aj7zmn52ivirbxgxjbp")))

(define-public crate-znotify-cli-0.1.1 (c (n "znotify-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo" "std"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "znotify") (r "^0.1.0") (d #t) (k 0)))) (h "116w9slcs1s915y4lh0vc69m0m1ix5aczzid1qxvl9p7q1vp4b88")))

