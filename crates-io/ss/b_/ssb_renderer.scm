(define-module (crates-io ss b_ ssb_renderer) #:use-module (crates-io))

(define-public crate-ssb_renderer-0.0.0 (c (n "ssb_renderer") (v "0.0.0") (d (list (d (n "cgmath") (r "~0.17.0") (d #t) (k 0)) (d (n "font-loader") (r "~0.8.0") (d #t) (k 0)) (d (n "gl32") (r "^0.1.0") (d #t) (k 0)) (d (n "glutin") (r "~0.19.0") (d #t) (k 0)) (d (n "glyph_brush") (r "~0.4.1") (d #t) (k 0)) (d (n "image") (r "~0.21.0") (d #t) (k 0)) (d (n "log") (r "~0.4.6") (d #t) (k 0)) (d (n "lyon") (r "~0.13.0") (d #t) (k 0)) (d (n "rusttype") (r "~0.7.5") (d #t) (k 0)) (d (n "ssb_parser") (r "^0.0.0") (d #t) (k 0)))) (h "0aj5yf2f08rhsca6l1zsba4kl39ji9h20l34mbgvimvg539sz1cs")))

