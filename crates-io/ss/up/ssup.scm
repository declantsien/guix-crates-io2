(define-module (crates-io ss up ssup) #:use-module (crates-io))

(define-public crate-ssup-0.3.3 (c (n "ssup") (v "0.3.3") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notifica") (r "^3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "shlex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c3zg4f6z4ni571z366ia2xxhbhm7k9j2g1k3rq6q46ws79mq5q4")))

(define-public crate-ssup-0.3.5 (c (n "ssup") (v "0.3.5") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notifica") (r "^3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "shlex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dl72708m80krzyjw8dyzp7h1xnvywh3ycyfp0vkmr432i861say")))

