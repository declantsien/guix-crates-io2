(define-module (crates-io ss z_ ssz_rs_derive) #:use-module (crates-io))

(define-public crate-ssz_rs_derive-0.1.0 (c (n "ssz_rs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "033vx7hdi0fsl3zyfkw9i3nw9bh0544142dzb04spijnyy6r53nw")))

(define-public crate-ssz_rs_derive-0.3.0 (c (n "ssz_rs_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qhyd18d0rpjkpk87x3n231ls9kgffdf5rjzkjsw48k9a22q8chr")))

(define-public crate-ssz_rs_derive-0.4.0 (c (n "ssz_rs_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f3w99v3k7scx0yybw1r7q5859kx42jxh2217vfff47mngl0chdm")))

(define-public crate-ssz_rs_derive-0.5.0 (c (n "ssz_rs_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06h1p0pm7qvw8zlcrpwwlgkvmqa568pbbvm286mykhslnb9kadfd")))

(define-public crate-ssz_rs_derive-0.6.0 (c (n "ssz_rs_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c0k2ss00pdih8ksn6iq7r0yd7k6rjlz2zs2786nwr6029s8p1rl")))

(define-public crate-ssz_rs_derive-0.7.0 (c (n "ssz_rs_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xxvrl5rw4g4hynzfbsir8hcd94qiwb4rsp4fry3f8lqirq9ysxf")))

(define-public crate-ssz_rs_derive-0.8.0 (c (n "ssz_rs_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rdmi1sipdz07rxyycdlj6kkbzhvlys556c3bfg0zsyfq7cynyf6")))

(define-public crate-ssz_rs_derive-0.9.0 (c (n "ssz_rs_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h51581dlq348rqmnhqi29pmz8axb58vlm9v6vmi65qss3258zgh")))

