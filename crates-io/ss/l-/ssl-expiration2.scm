(define-module (crates-io ss l- ssl-expiration2) #:use-module (crates-io))

(define-public crate-ssl-expiration2-0.2.0 (c (n "ssl-expiration2") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.58") (d #t) (k 0)))) (h "03vvjx53l7c8vx7dxbxzqf0hjg0wxh0c0v08l09xgqdz2l0ws2dp")))

(define-public crate-ssl-expiration2-0.2.1 (c (n "ssl-expiration2") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.58") (d #t) (k 0)))) (h "0379z89smlr7s9liqy9k82ys1m87b46wpa6hizsl3f1byvswqil6")))

(define-public crate-ssl-expiration2-0.2.2 (c (n "ssl-expiration2") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.58") (d #t) (k 0)))) (h "0gszh1i6fnsmkcdfpw95dg571gss8xz6vd1y2lv8pljjjajm65a4")))

(define-public crate-ssl-expiration2-0.2.3 (c (n "ssl-expiration2") (v "0.2.3") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.60") (d #t) (k 0)))) (h "0db84g285a6csrssgxv6w7xqg5s72zi4rsmkv95rg61rjvzbxdip")))

(define-public crate-ssl-expiration2-0.3.0 (c (n "ssl-expiration2") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.60") (d #t) (k 0)))) (h "0mbckpm1gqn11s57lrbj3fc8y3rz06zcxzagi23bzwq947aqa38d")))

(define-public crate-ssl-expiration2-0.4.0 (c (n "ssl-expiration2") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.34") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.63") (d #t) (k 0)))) (h "1gg4ihw5mv651496ddhfz1q4gx4nbggxvixmi3v8mbrz48hrba72")))

