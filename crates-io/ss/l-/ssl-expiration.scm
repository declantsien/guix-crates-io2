(define-module (crates-io ss l- ssl-expiration) #:use-module (crates-io))

(define-public crate-ssl-expiration-0.1.0 (c (n "ssl-expiration") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0y31bf4ikypfwsk8bxqmqzpgczxn1jkc1mspgx0sgiakwr753inj")))

(define-public crate-ssl-expiration-0.1.1 (c (n "ssl-expiration") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0iz7h2riqyw6c60vi62jlvnnf97mnppan59my9z428hsf5bdp12z")))

(define-public crate-ssl-expiration-0.1.2 (c (n "ssl-expiration") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "foreign-types-shared") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "122z18fvdyvmipdmc7dlsq01y85hchdhwxy2cl7kkba1yxwlm2rc")))

