(define-module (crates-io ss b- ssb-common) #:use-module (crates-io))

(define-public crate-ssb-common-0.1.0 (c (n "ssb-common") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)))) (h "1l11pwfzrdgp96jalg3b8jc6mk860iss13jfdqa8pvyygw0mfmqm")))

(define-public crate-ssb-common-0.1.1 (c (n "ssb-common") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0g2pinq6pkmz6qsm2q7sv4zy80bwk2s29bfad06594wpg0ccgf43")))

(define-public crate-ssb-common-0.2.0 (c (n "ssb-common") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0329s2qhrlnkhdphbgs39pq71d1006cbbzzl9a6jf90318zm41mv")))

(define-public crate-ssb-common-0.2.1 (c (n "ssb-common") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0zkxh4nla4kwr8g86f95zv6yf0lq0dhv124dgaazwfcxg79727s3")))

(define-public crate-ssb-common-0.2.2 (c (n "ssb-common") (v "0.2.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "1chkdaf3rfh0v3iz5ah281k9dqlb21jy24yfiwzzsp7plw20z43b")))

(define-public crate-ssb-common-0.2.3 (c (n "ssb-common") (v "0.2.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0v95r42861wdlw192nx9d6m7paj17xhz9i8kxy4ckaa730ihw1vg")))

(define-public crate-ssb-common-0.2.4 (c (n "ssb-common") (v "0.2.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0y60k1yji5ldnfv2qr3wddqvrjb1gzzzy3lvzwn175kw1jg4vzgy")))

(define-public crate-ssb-common-0.2.5 (c (n "ssb-common") (v "0.2.5") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "secret_handshake") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0rw10f03adlf9fs6r2wjyfh34r26hhfnvxirzrwy3xn0n8zivrbb")))

(define-public crate-ssb-common-0.3.0 (c (n "ssb-common") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "secret_handshake") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "098g70sjn5w62v7lnkgvxfr61wyj3bgkkz6y2x1yf6fps5w8xfgg")))

