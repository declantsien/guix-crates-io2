(define-module (crates-io ss b- ssb-json-msg-data) #:use-module (crates-io))

(define-public crate-ssb-json-msg-data-0.1.2 (c (n "ssb-json-msg-data") (v "0.1.2") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "ryu-ecmascript") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)))) (h "0sxq0xp59y4baz57mp1ym1jvy2q1qvygfwd4zxx069advb6db7r6")))

