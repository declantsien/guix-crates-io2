(define-module (crates-io ss b- ssb-slp-encoding) #:use-module (crates-io))

(define-public crate-ssb-slp-encoding-0.1.0 (c (n "ssb-slp-encoding") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)))) (h "1pxffh6gficm60fmcpq23q0367qvnlcs8ag17h8v7nmk4kwm5hkx")))

