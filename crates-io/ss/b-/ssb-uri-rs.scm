(define-module (crates-io ss b- ssb-uri-rs) #:use-module (crates-io))

(define-public crate-ssb-uri-rs-0.1.0 (c (n "ssb-uri-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "04sjjs2mfmsd3kqr5h2qf8mx3mwxf6zrm9q72rhwcyih2m2nikgc")))

(define-public crate-ssb-uri-rs-0.1.1 (c (n "ssb-uri-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0gaxaz75yynwkagj90453sy7451jcaq2vl1rn5svk4k3x6mdhkaq")))

(define-public crate-ssb-uri-rs-0.1.2 (c (n "ssb-uri-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "186q3m8nhfgxlzc63x14qa2vmamdwd1lgx706pjx3f0730wi1v77")))

(define-public crate-ssb-uri-rs-0.1.3 (c (n "ssb-uri-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1jc2qy19iq6c0m3y07889wrpgqksdn0ygrliv5krw331648d61xa")))

(define-public crate-ssb-uri-rs-0.1.4 (c (n "ssb-uri-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0qsfam40w9kdm8mpwinv8wgnffhp29m9kivxckkxzwy65sl6a9gj")))

(define-public crate-ssb-uri-rs-0.2.0 (c (n "ssb-uri-rs") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0hsvd41bwhbm35yjra7czn3p4z4kmc3wcag11n2gn6c0gzqa98jm")))

