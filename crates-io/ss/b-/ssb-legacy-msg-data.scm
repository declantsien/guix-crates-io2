(define-module (crates-io ss b- ssb-legacy-msg-data) #:use-module (crates-io))

(define-public crate-ssb-legacy-msg-data-0.1.2 (c (n "ssb-legacy-msg-data") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "ryu-ecmascript") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "strtod2") (r "^0.0.1") (d #t) (k 0)))) (h "0l9jnhz6vwldgxnmi35kr2hl375wdzdgfnii3qbaxbkl3grnk4gf")))

(define-public crate-ssb-legacy-msg-data-0.1.3 (c (n "ssb-legacy-msg-data") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "ryu-ecmascript") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "strtod2") (r "^0.0.1") (d #t) (k 0)))) (h "0c6yjx0dllbwhc01a8l4729qw2qg1ba1xc08gdpy72niiqhsyr0f")))

(define-public crate-ssb-legacy-msg-data-0.1.4 (c (n "ssb-legacy-msg-data") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "ryu-ecmascript") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "strtod2") (r "^0.0.1") (d #t) (k 0)))) (h "1n2q0vvarxnaf3d303pllxs75n4i4mx8x73pj7vznay3cccii13r")))

