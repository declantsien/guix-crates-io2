(define-module (crates-io ss b- ssb-multiformats) #:use-module (crates-io))

(define-public crate-ssb-multiformats-0.1.0 (c (n "ssb-multiformats") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1.4") (d #t) (k 0)))) (h "0n63mym9zmlqda1fbh48ifyb5wgg689b788hnjsxl9n8jr7bw8mp")))

(define-public crate-ssb-multiformats-0.2.1 (c (n "ssb-multiformats") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1.4") (d #t) (k 0)))) (h "0cwdfxqn814z7lbklpdihiazlpijh63dg97kl31gf1hdnxaxsx97")))

(define-public crate-ssb-multiformats-0.3.0 (c (n "ssb-multiformats") (v "0.3.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1.4") (d #t) (k 0)))) (h "1p8idab7y41i03cidrj1qjdpz45g6lp7m50k8x59cb1jay37h267")))

(define-public crate-ssb-multiformats-0.4.0 (c (n "ssb-multiformats") (v "0.4.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.2.1") (k 0)))) (h "0lf7mymbsn5cgbyzlhnbf0771p3mbnj7vnnvpgsggg2bylv5pw3z") (f (quote (("sodium" "ssb-crypto/sodium" "ssb-crypto/alloc") ("default" "dalek") ("dalek" "ssb-crypto/default"))))))

(define-public crate-ssb-multiformats-0.4.1 (c (n "ssb-multiformats") (v "0.4.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.2.2") (k 0)))) (h "0mv9lx8yfvbnsps25h383lvjq6fq31m0r5vanhxz7zai3pyylr0n") (f (quote (("sodium" "ssb-crypto/sodium" "ssb-crypto/alloc") ("default" "dalek") ("dalek" "ssb-crypto/default"))))))

(define-public crate-ssb-multiformats-0.4.2 (c (n "ssb-multiformats") (v "0.4.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.2.3") (k 0)))) (h "1b2wflppha1dnxh3pddw9ixcydax9lpl13iba80mprxlsa7qamhz") (f (quote (("sodium" "ssb-crypto/sodium" "ssb-crypto/alloc") ("default" "dalek") ("dalek" "ssb-crypto/default"))))))

