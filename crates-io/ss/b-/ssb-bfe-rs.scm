(define-module (crates-io ss b- ssb-bfe-rs) #:use-module (crates-io))

(define-public crate-ssb-bfe-rs-0.2.0 (c (n "ssb-bfe-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "ssb-uri-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1lmcfyr5yg0dk2ig1jpsjnmqln6pwc3y0m9x7lz91hnd55161xdw")))

(define-public crate-ssb-bfe-rs-0.2.1 (c (n "ssb-bfe-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "ssb-uri-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1f82z7yvxa7747wfvz51aw2904dygp40x9ni94wjmwjxvanbhvci")))

