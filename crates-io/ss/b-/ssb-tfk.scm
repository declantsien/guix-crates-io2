(define-module (crates-io ss b- ssb-tfk) #:use-module (crates-io))

(define-public crate-ssb-tfk-0.1.0 (c (n "ssb-tfk") (v "0.1.0") (d (list (d (n "snafu") (r "^0.6.9") (d #t) (k 0)) (d (n "ssb-multiformats") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1wd6in58wi14ny7q6854jicaqh8kaqldazq7l3cvf9fj0nxjyl9z") (f (quote (("multiformats" "ssb-multiformats") ("default"))))))

(define-public crate-ssb-tfk-0.1.1 (c (n "ssb-tfk") (v "0.1.1") (d (list (d (n "snafu") (r "^0.6.9") (d #t) (k 0)) (d (n "ssb-multiformats") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1s00p2bkasyis26jzkjh99w7avjxyp5wmapx7lwl6qiv8i4xqk3g") (f (quote (("multiformats" "ssb-multiformats") ("default"))))))

