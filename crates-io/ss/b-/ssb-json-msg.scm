(define-module (crates-io ss b- ssb-json-msg) #:use-module (crates-io))

(define-public crate-ssb-json-msg-0.1.3 (c (n "ssb-json-msg") (v "0.1.3") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ssb-json-msg-data") (r "^0.1.2") (d #t) (k 0)) (d (n "ssb-multiformats") (r "^0.2.1") (d #t) (k 0)))) (h "00lyqac855nv0z4nsivd213h2vwx6yszvvzqnph13zy0abhdvgp5")))

