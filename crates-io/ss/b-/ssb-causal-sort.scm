(define-module (crates-io ss b- ssb-causal-sort) #:use-module (crates-io))

(define-public crate-ssb-causal-sort-0.1.1 (c (n "ssb-causal-sort") (v "0.1.1") (d (list (d (n "daggy") (r "^0.6.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ssb-multiformats") (r "^0.4") (d #t) (k 0)))) (h "0pb942w5zyy0cqswy3h7js8ji1mdfzrqx2m5bsaawivkg810a4gq")))

