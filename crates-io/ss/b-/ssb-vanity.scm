(define-module (crates-io ss b- ssb-vanity) #:use-module (crates-io))

(define-public crate-ssb-vanity-0.1.0 (c (n "ssb-vanity") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1nzca7j26ps666vkcajqsjsdh7c5hq9b4p7drygsz481lpxslv00")))

