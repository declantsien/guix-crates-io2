(define-module (crates-io ss b- ssb-box-stream) #:use-module (crates-io))

(define-public crate-ssb-box-stream-0.2.0 (c (n "ssb-box-stream") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "duplexify") (r "^1.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.5") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "test-strategy") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nz9di8cfl8xrc9d55rggrv67ncdh1vcrg73clql71cglq4nqj30")))

