(define-module (crates-io ss b- ssb-packetstream) #:use-module (crates-io))

(define-public crate-ssb-packetstream-0.1.0 (c (n "ssb-packetstream") (v "0.1.0") (d (list (d (n "async-ringbuffer") (r "^0.5.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "04xzcpnjlykph7kbgb5qw6lxnsf07i7kf94nah42li8s0hgpmx5s")))

(define-public crate-ssb-packetstream-0.2.0 (c (n "ssb-packetstream") (v "0.2.0") (d (list (d (n "async-ringbuffer") (r "~0.5.2") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (f (quote ("futures"))) (d #t) (k 0)))) (h "06wgkcwf05b0va0d0ls7fkg36daq8j54sbwx3631ph1n47909siz")))

(define-public crate-ssb-packetstream-0.2.1 (c (n "ssb-packetstream") (v "0.2.1") (d (list (d (n "async-ringbuffer") (r "~0.5.2") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (f (quote ("futures"))) (d #t) (k 0)))) (h "01bh7dnwqjhbckj183sqhg2mzfx7sr7rl43cc80jnclcj3pi5li7")))

