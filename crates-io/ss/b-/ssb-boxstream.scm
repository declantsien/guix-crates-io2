(define-module (crates-io ss b- ssb-boxstream) #:use-module (crates-io))

(define-public crate-ssb-boxstream-0.1.0 (c (n "ssb-boxstream") (v "0.1.0") (d (list (d (n "async-ringbuffer") (r "^0.5.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "ssb-crypto") (r "~0.1.3") (d #t) (k 0)))) (h "0qlqx21w354x4zypl7jg7wpbaw92f0w56hjrhs6q2iqwxy4ym7mv")))

(define-public crate-ssb-boxstream-0.2.0 (c (n "ssb-boxstream") (v "0.2.0") (d (list (d (n "async-ringbuffer") (r "~0.5.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "ssb-crypto") (r "~0.1.3") (d #t) (k 0)))) (h "0137s4r8hmf3syfk0ybq3g78h91shhyvyqr307ddia8lzgxc92fl")))

(define-public crate-ssb-boxstream-0.2.1 (c (n "ssb-boxstream") (v "0.2.1") (d (list (d (n "async-ringbuffer") (r "~0.5.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "ssb-crypto") (r "~0.1.3") (d #t) (k 0)))) (h "1kmf4b3y1r2vqm3cm9d1rwfrgrhx2jg80m4xjlp7n5z2cn3d8rg7")))

(define-public crate-ssb-boxstream-0.2.2 (c (n "ssb-boxstream") (v "0.2.2") (d (list (d (n "async-ringbuffer") (r "~0.5.4") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (f (quote ("io"))) (d #t) (k 2)) (d (n "ssb-crypto") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "0d9lwp9vriijfqwvf7cwk59qpf7hsk649sg5w9xfp9j4j3pz4p7k")))

