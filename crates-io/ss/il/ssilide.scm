(define-module (crates-io ss il ssilide) #:use-module (crates-io))

(define-public crate-ssilide-0.1.0 (c (n "ssilide") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "bevy_ecs_macros") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0x0bkj4n2g1nbis5xli5q1khr328v19n78n594hn437kfszj0f75") (f (quote (("bevy" "bevy_ecs" "bevy_ecs_macros"))))))

(define-public crate-ssilide-0.2.0 (c (n "ssilide") (v "0.2.0") (h "10ibmbq1yn1vcxz6fmw8b0hm5llz1ma4jmpxw18p3v1irg765z1a")))

