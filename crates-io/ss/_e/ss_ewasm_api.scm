(define-module (crates-io ss _e ss_ewasm_api) #:use-module (crates-io))

(define-public crate-ss_ewasm_api-0.11.0 (c (n "ss_ewasm_api") (v "0.11.0") (d (list (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "qimalloc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "1wfprdqvlkhiaafy9xaa64va4ibkpfsxf9r1nwrw7va6py16r3ar") (f (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

