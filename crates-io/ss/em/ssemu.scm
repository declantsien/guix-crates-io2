(define-module (crates-io ss em ssemu) #:use-module (crates-io))

(define-public crate-ssemu-0.1.0 (c (n "ssemu") (v "0.1.0") (d (list (d (n "baby-emulator") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0szaislk3z6avyb9l250v7j6yb3snx8jl4py0fpl91i680ch5x04")))

(define-public crate-ssemu-0.1.1 (c (n "ssemu") (v "0.1.1") (d (list (d (n "baby-emulator") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0jaglzhzcsi64z5k8n2gmqnzzibq8xlsz5rmmbhlp2ps9bnpzmr5")))

(define-public crate-ssemu-0.1.2 (c (n "ssemu") (v "0.1.2") (d (list (d (n "baby-emulator") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1z7sg76s7pkylasqj07c4j934fng5ncj0823qw0a885m10nvasgf")))

(define-public crate-ssemu-0.1.3 (c (n "ssemu") (v "0.1.3") (d (list (d (n "baby-emulator") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1rp355ha5ndvz7qv05y3pyjlzyn7frvsm980zjra588v5qjp06sj")))

(define-public crate-ssemu-0.1.4 (c (n "ssemu") (v "0.1.4") (d (list (d (n "baby-emulator") (r "^0.1.7") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0mz57a4614md2svh0liji32jkkpdwc70gr91vw8wn4dxdll5lpvm")))

