(define-module (crates-io ss e- sse-client) #:use-module (crates-io))

(define-public crate-sse-client-0.1.0 (c (n "sse-client") (v "0.1.0") (d (list (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "05kss0q17rl2j54803ifhmvmxf89wn0p9c5kdgixwy2nm543pbx7")))

(define-public crate-sse-client-0.1.1 (c (n "sse-client") (v "0.1.1") (d (list (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "04gxz975pyip8gpyf8s9019c9b3mi3slia5a7c0nb57gpci2y8j0")))

(define-public crate-sse-client-1.0.0 (c (n "sse-client") (v "1.0.0") (d (list (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "10kxyplnsw7s1gqp8fmd3p1rwfiil1vvgqi0mq2rlbpwd1zalz2v")))

(define-public crate-sse-client-1.0.1 (c (n "sse-client") (v "1.0.1") (d (list (d (n "http-test-server") (r "^1.0.0") (d #t) (k 2)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "0pw2kpirjxadd2zq96yw9z0zi474p0p6cr5hrnhgh6vpnvqb501r")))

(define-public crate-sse-client-1.0.2 (c (n "sse-client") (v "1.0.2") (d (list (d (n "http-test-server") (r "^1.0.0") (d #t) (k 2)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "01fcqmfpa89f1b8bwn4k5iazpyada2q1vm707b40062h6c9z64lx")))

(define-public crate-sse-client-1.1.0 (c (n "sse-client") (v "1.1.0") (d (list (d (n "http-test-server") (r "^1.0.0") (d #t) (k 2)) (d (n "native-tls-crate") (r "^0.2.3") (o #t) (d #t) (k 0) (p "native-tls")) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "11cqgqd8yy8z9g96nm7dgabdsp03dg63ydz4dn9z8h7n5b2f91hz") (f (quote (("native-tls-vendored" "native-tls" "native-tls-crate/vendored") ("native-tls" "native-tls-crate") ("default"))))))

(define-public crate-sse-client-1.1.1 (c (n "sse-client") (v "1.1.1") (d (list (d (n "http-test-server") (r "^1.0.0") (d #t) (k 2)) (d (n "native-tls-crate") (r "^0.2.3") (o #t) (d #t) (k 0) (p "native-tls")) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "10njkh3gswl5ww8nlpa3qh0542ps31jfvacsc1717320jzmain36") (f (quote (("native-tls-vendored" "native-tls" "native-tls-crate/vendored") ("native-tls" "native-tls-crate") ("default"))))))

