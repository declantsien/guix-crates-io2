(define-module (crates-io ss e- sse-codec) #:use-module (crates-io))

(define-public crate-sse-codec-0.1.0 (c (n "sse-codec") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures_codec") (r "= 0.3.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "1rd8i4yi3n61jf66rb6cidgdr15a3srdnd19pbn80b2aq8064ysx")))

(define-public crate-sse-codec-0.2.0 (c (n "sse-codec") (v "0.2.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures_codec") (r "= 0.3.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "0yfs7dwch7d4q46i13677wdf0n41ccsz6fzxwhhx0pkw3hw476cy")))

(define-public crate-sse-codec-0.3.0 (c (n "sse-codec") (v "0.3.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures_codec") (r "= 0.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "1mjqa0mg7p54z6ajn624jgwl85n9c3jsgzww5iby3hshcbgk8sdx")))

(define-public crate-sse-codec-0.3.1 (c (n "sse-codec") (v "0.3.1") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures_codec") (r "= 0.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "1812q7pl49hr7lw7n7pcfipm0sr64hf05af6jddw48hz0ixbbwlw")))

(define-public crate-sse-codec-0.3.2 (c (n "sse-codec") (v "0.3.2") (d (list (d (n "arbitrary") (r "^0.4.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures_codec") (r "=0.4.1") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "surf") (r "=2.0.0-alpha.4") (f (quote ("native-client"))) (k 2)))) (h "0nh8b1y2k5lsvcva15da4by935bavirfpavs0d54pi2h2f0rz9c4")))

