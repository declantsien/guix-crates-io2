(define-module (crates-io ss e- sse-actix-web) #:use-module (crates-io))

(define-public crate-sse-actix-web-0.0.1 (c (n "sse-actix-web") (v "0.0.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1y2db0ix9bz0jhq9jwn65zbdmd9mmwmyr7mif6rcx4r32s7lz0h2")))

(define-public crate-sse-actix-web-0.1.0 (c (n "sse-actix-web") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0b5bw6q0qf21kazv97cinisazs4xp4v77zb7z7y53ybjvfx8lhf4")))

(define-public crate-sse-actix-web-0.2.0 (c (n "sse-actix-web") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1vvikzl05b78lmdgd40qv9h66bl4rr58gsi83f6vackmck4pngzf")))

(define-public crate-sse-actix-web-0.3.0 (c (n "sse-actix-web") (v "0.3.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0i6xfr8f4sg041id33z6h03x5n10vjvpqqhdvbabglzy4k2a4rzg")))

(define-public crate-sse-actix-web-0.4.0 (c (n "sse-actix-web") (v "0.4.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "17b48b8dldxnjfhiad8d7z6l694nzqnh788v2ncqhzvl8xwms9s4")))

(define-public crate-sse-actix-web-0.4.1 (c (n "sse-actix-web") (v "0.4.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "08mj12cvs4r8sz33zdf5lp51r3k77vq63x22and72m1gilci1ypz")))

(define-public crate-sse-actix-web-0.4.2 (c (n "sse-actix-web") (v "0.4.2") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1f429y8gqjbwvr6g6mpspyycrzrjy8zg6lsl3kdk1h96yfpmm3v3")))

(define-public crate-sse-actix-web-0.4.3 (c (n "sse-actix-web") (v "0.4.3") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0bqn2i7xczcc9pwq8wp0qb9jyycqh4kkm3gkcakkhyrkqm2j0n0v")))

(define-public crate-sse-actix-web-0.4.4 (c (n "sse-actix-web") (v "0.4.4") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1qk9w5gb8ww9fhs2b6dq0z6h1n1wixgrgm06pns8k89az7z1d31n")))

(define-public crate-sse-actix-web-0.4.5 (c (n "sse-actix-web") (v "0.4.5") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "18bjk5k5zn6zqjfxh8dcvjzzpv5fj99gpgjp5g4zqb3cjdm58809")))

(define-public crate-sse-actix-web-0.4.6 (c (n "sse-actix-web") (v "0.4.6") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0ksmz455xgyawvppk52n2q6sd0g1c3b2b46kkm98w1y2if0pxl32")))

(define-public crate-sse-actix-web-0.5.0 (c (n "sse-actix-web") (v "0.5.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1fv129w45g5was24y7v3jhb529bzpidpb5lhkggqg1zbdy9nsbmc")))

(define-public crate-sse-actix-web-0.5.1 (c (n "sse-actix-web") (v "0.5.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0hqdl9kqjbpf7bmbxbd6938vja5p3is8y5ac66pcc3jprl94iwhq")))

(define-public crate-sse-actix-web-0.5.2 (c (n "sse-actix-web") (v "0.5.2") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "02489hcj6md501azv7ikki2x29m0yn7dfwjsk3nh4af1mmrpzcil")))

(define-public crate-sse-actix-web-0.5.3 (c (n "sse-actix-web") (v "0.5.3") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-rc") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1cn34z08z84r3c3zpp58c9q5fi0kjp0krayw29blcvizmn0y6mhr")))

(define-public crate-sse-actix-web-0.6.0 (c (n "sse-actix-web") (v "0.6.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1vy37cj5g13aihns33wicjd624j991666rfv6b4ag308x7d76yw6")))

(define-public crate-sse-actix-web-0.6.1 (c (n "sse-actix-web") (v "0.6.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1b2c37bhbck8q6r0ajip677cad328ljfng5mrspwq6f5375gi78a")))

(define-public crate-sse-actix-web-0.7.0 (c (n "sse-actix-web") (v "0.7.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0fxhrvngjh7n432q8abcv0y26dvir95q73ikpvf2dxz4555jahmh")))

(define-public crate-sse-actix-web-0.7.1 (c (n "sse-actix-web") (v "0.7.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1jvzm1r1lqbgnyg1cqrnf4hcznzn439n1sx0n03bj0knqp20972y")))

(define-public crate-sse-actix-web-0.7.2 (c (n "sse-actix-web") (v "0.7.2") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "0zfiz1wcmyyshzi5wj5x9l2fkg55krm9xjmmxj6bnhhfl3qp8fw5")))

(define-public crate-sse-actix-web-0.8.0 (c (n "sse-actix-web") (v "0.8.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "1hs75s0h8w18kxd2v7qd9709mb9j1qwjibwzzb8ya8h0ci4n1k7d")))

(define-public crate-sse-actix-web-0.8.1 (c (n "sse-actix-web") (v "0.8.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (d #t) (k 0)))) (h "10nai0py2kjil1156wpr3jwmr6s3z4wphxbw9qkj46wv2n2pskgp")))

