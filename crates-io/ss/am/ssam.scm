(define-module (crates-io ss am ssam) #:use-module (crates-io))

(define-public crate-ssam-0.1.0 (c (n "ssam") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "039hzci3mxdr3adx1bni114qm6kmqws6l62fxmxympkprxxvw889")))

(define-public crate-ssam-0.1.1 (c (n "ssam") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "17k5ysmvjn8kq5jfvca3whzy4g3bq4iw5r6g26rzq7iqqbnadxfn")))

(define-public crate-ssam-0.1.2 (c (n "ssam") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "0sngklcc0n7d1287cd0kjk51mk5lix10nryzfqhmwmfcfnjz01q5")))

(define-public crate-ssam-0.2.0 (c (n "ssam") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "0dvqvn1fy6r08k7jg2k4vybac3v0p83ljclwb5s53zivp5bam8x1")))

