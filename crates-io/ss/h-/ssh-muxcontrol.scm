(define-module (crates-io ss h- ssh-muxcontrol) #:use-module (crates-io))

(define-public crate-ssh-muxcontrol-0.1.0 (c (n "ssh-muxcontrol") (v "0.1.0") (d (list (d (n "bytes") (r ">=1.1.0") (d #t) (k 0)) (d (n "sendfd") (r ">=0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "tokio") (r ">=1.0") (f (quote ("io-util" "macros" "net"))) (d #t) (k 0)) (d (n "tokio") (r ">=1.0") (f (quote ("rt" "time"))) (d #t) (k 2)) (d (n "tokio-pipe") (r ">=0.2.1") (d #t) (k 0)))) (h "0zcrhi3hr1k408g1vwrg332c8isvgbqxizx29ymljshjsxgz3dzi")))

