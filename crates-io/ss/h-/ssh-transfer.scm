(define-module (crates-io ss h- ssh-transfer) #:use-module (crates-io))

(define-public crate-ssh-transfer-0.1.0 (c (n "ssh-transfer") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.1") (d #t) (k 0)))) (h "1w8cvgzf185kd8hgf30llyzz7780kjrz68x09a4093bpr1akphz9")))

(define-public crate-ssh-transfer-0.1.1 (c (n "ssh-transfer") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.1") (d #t) (k 0)))) (h "1kbqb2wwdy6a9742448vihxjaj4v9yv2hvym25jrr6drm2cqi5ss")))

(define-public crate-ssh-transfer-0.2.0 (c (n "ssh-transfer") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.1") (d #t) (k 0)))) (h "1z08l3w6capl4ia4vphgbznv9y34f6ggvpybbhgwy66sfkn9nn0q")))

(define-public crate-ssh-transfer-0.3.0 (c (n "ssh-transfer") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.1") (d #t) (k 0)))) (h "0skbz6zjwyqbjy1822f1wqw466jw9bq8piy2lqqm7hgr37zh4w7d")))

