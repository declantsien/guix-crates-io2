(define-module (crates-io ss h- ssh-known-hosts) #:use-module (crates-io))

(define-public crate-ssh-known-hosts-1.0.0 (c (n "ssh-known-hosts") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0irbzcnwr4k3gzq3wgmw01p1hf5icy89583qcwfpibqzgiqvvm5x")))

