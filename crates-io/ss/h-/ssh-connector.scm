(define-module (crates-io ss h- ssh-connector) #:use-module (crates-io))

(define-public crate-ssh-connector-0.1.0 (c (n "ssh-connector") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("process"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1xb7pfb03qslz9qq18qb9m1h8sp6ypvmpa2x4bzpbqjra5wkmvxq")))

