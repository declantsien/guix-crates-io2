(define-module (crates-io ss h- ssh-agency) #:use-module (crates-io))

(define-public crate-ssh-agency-0.1.0 (c (n "ssh-agency") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "0mh7zch2vkbwp39ig6182la59k78xw88vmk3y4qh858jfg4py6zw")))

