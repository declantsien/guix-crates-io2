(define-module (crates-io ss h- ssh-packet) #:use-module (crates-io))

(define-public crate-ssh-packet-0.0.1 (c (n "ssh-packet") (v "0.0.1") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "0ry7cql7gv7sk4nbgzl29ncbgrq689svk2i7ij3fbqm9706q0c1a") (y #t)))

(define-public crate-ssh-packet-0.0.2 (c (n "ssh-packet") (v "0.0.2") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "18k44bc8gmv1gaz2zrdcxrinf5xzzhpqns9zxh88glhg7pxywibk") (y #t)))

(define-public crate-ssh-packet-0.0.3 (c (n "ssh-packet") (v "0.0.3") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "0wirv67j7183mr9701jib4y3n5ms9s635ckm2fc1b7w0fgpfbasd") (y #t)))

(define-public crate-ssh-packet-0.0.4 (c (n "ssh-packet") (v "0.0.4") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "150zbzgyh10ibgff5dbigc27arg69zfb5irivmmphr7cgpi9xahi") (y #t)))

(define-public crate-ssh-packet-0.0.5 (c (n "ssh-packet") (v "0.0.5") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "1ksdqsrl0xcf7yfiin0llq2lyh4r9a98zpkxpda7iswzr0ks82kc") (y #t)))

(define-public crate-ssh-packet-0.0.6 (c (n "ssh-packet") (v "0.0.6") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "06r598yf15k9kqp4y0yxjz08d1gr2yvcqlyvk8nggyqzzrm7c2pw") (y #t)))

(define-public crate-ssh-packet-0.0.7 (c (n "ssh-packet") (v "0.0.7") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "0xkrczj4hag5cacvvfckq3srv323xkhdsf87f6jwkixqw8cj4sv7") (y #t)))

(define-public crate-ssh-packet-0.0.8 (c (n "ssh-packet") (v "0.0.8") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "19z5wxlphk7y9kzm7m2lfbk9a6nw5djbcfv32zamr3gfb474yzlp") (y #t)))

(define-public crate-ssh-packet-0.0.9 (c (n "ssh-packet") (v "0.0.9") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)))) (h "0fn6599z2lg7avrrq0kizgv4162i32cwnya8yj4wlamlcpmizv24") (y #t)))

(define-public crate-ssh-packet-0.0.10 (c (n "ssh-packet") (v "0.0.10") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1prbk6yq9h2api8izs97gdpfp01an1jw0gwvyybfypqz1x1dakd1")))

(define-public crate-ssh-packet-0.1.0 (c (n "ssh-packet") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1z9vbjh2zpi4zkd6xmqqj161n1iwlz5jy8qgj941bd46d2gsd9gl")))

(define-public crate-ssh-packet-0.1.1 (c (n "ssh-packet") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0wdrin0g52gak2pa19k2rxhih6jg0im3gsl50c0sbgslpy8a46jw")))

(define-public crate-ssh-packet-0.1.2 (c (n "ssh-packet") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "02102zc2wisy4667xyrxali7mf240d5q5qzbblvllnigranfd3aq")))

(define-public crate-ssh-packet-0.1.3 (c (n "ssh-packet") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1krpc651dni8pmchaqnfy5mwhxsiw8gh2rdniiv64f7scw3z74nx")))

(define-public crate-ssh-packet-0.1.4 (c (n "ssh-packet") (v "0.1.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "106vbwcmhyvkzp5r8bj6kdh8ylifs1nyp7vpz9nr4zd4ldhhy7bp")))

(define-public crate-ssh-packet-0.1.5 (c (n "ssh-packet") (v "0.1.5") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "01jk94l085wx1bfnwjmzcqsxc1qksfxfdb9l3vhlji3zwi7wa7cv")))

(define-public crate-ssh-packet-0.1.6 (c (n "ssh-packet") (v "0.1.6") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0k81prdr6vclm37w13h5p8gisr0mgsyrdasg3f02wskarkcrqh7m")))

(define-public crate-ssh-packet-0.1.7 (c (n "ssh-packet") (v "0.1.7") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0x14z8yk6ix930dfd5g4c2gv4392xh9snp9am1ai878c995i2hyk")))

(define-public crate-ssh-packet-0.1.8 (c (n "ssh-packet") (v "0.1.8") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "089g1845mg0acpkwhxjfgmdxpf34afzlcqm9f2bjp5irxbcrpfps")))

(define-public crate-ssh-packet-0.1.9 (c (n "ssh-packet") (v "0.1.9") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0cxja380nrgbhmjfs8f07rxn4mc8fpacdkigahdcf4ikx341gq5y")))

(define-public crate-ssh-packet-0.1.10 (c (n "ssh-packet") (v "0.1.10") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1n5fbvbjqv7y2mpyk3jv9lpll1kzz15r99mplsp7k35b9046xncy")))

(define-public crate-ssh-packet-0.1.11 (c (n "ssh-packet") (v "0.1.11") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "09zgxq74wgy4nwgvhdbnjvh1kpqlqyk987hzara8lsx3nmianqja")))

(define-public crate-ssh-packet-0.1.12 (c (n "ssh-packet") (v "0.1.12") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1xb20sm2rddcfp1ccbj3zgn45fy8nmg8ll005nw2hsxiz0hgdc1n")))

(define-public crate-ssh-packet-0.1.13 (c (n "ssh-packet") (v "0.1.13") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "11cr1kpcrpdvhf351c1savrw9jn13jz77qjgrmrdar12rgphk388")))

(define-public crate-ssh-packet-0.1.14 (c (n "ssh-packet") (v "0.1.14") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "14l5lz9cn3fl67v8dd4szjigs54jci24cadzg771ncjx6abdlfva")))

(define-public crate-ssh-packet-0.1.15 (c (n "ssh-packet") (v "0.1.15") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ppi9krhqfwvbvacdnfj38cpnhn6bdnn2bdhpyvd87b13vp4x8n0")))

(define-public crate-ssh-packet-0.1.16 (c (n "ssh-packet") (v "0.1.16") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0m2bw129mw82bycx4nb9hignp6njv7ly6pxr8ikryzqaw8wrsv3m")))

(define-public crate-ssh-packet-0.1.17 (c (n "ssh-packet") (v "0.1.17") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07almn0s6m26f444r4jdfhffi8j4867x9vwwpykjgx1m3a1s73pl")))

(define-public crate-ssh-packet-0.1.18 (c (n "ssh-packet") (v "0.1.18") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1w7rqfc8w7rixyqar6qvfvzmmwpmza68ix4qvcxfh22lir84a19m")))

(define-public crate-ssh-packet-0.1.19 (c (n "ssh-packet") (v "0.1.19") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xs75ama790rglbixjlhbbkk010p90iadrfcazw9rifnnvsmzpz8")))

(define-public crate-ssh-packet-0.1.20 (c (n "ssh-packet") (v "0.1.20") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "16xj7b25ikxk77lgk4aif2rl46zad0s6jyrspc4r8rz1gd9jgqgk")))

(define-public crate-ssh-packet-0.2.0 (c (n "ssh-packet") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "04ndsxhm61zmzfvjmrvvr8sz4ibnjgyf47w9l1lj0cgfg142j7mk")))

(define-public crate-ssh-packet-0.2.1 (c (n "ssh-packet") (v "0.2.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0n7rjzrw3b9k9dh7q7wr2924lz6y5305x145grm5swbhr0riyrph")))

(define-public crate-ssh-packet-0.2.2 (c (n "ssh-packet") (v "0.2.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0qxlilndp3942x4s0mxnka05sxv7mvhq5b82cxhfq61g7p5cdw7q") (y #t)))

(define-public crate-ssh-packet-0.2.3 (c (n "ssh-packet") (v "0.2.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xfh8gjmcdxbinc04whm479fmqi13v7lk72khvzwz7qsz3rr7p03")))

(define-public crate-ssh-packet-0.2.4 (c (n "ssh-packet") (v "0.2.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0irn9865k4vpcxwr0j8ajijc48gp1v97lqjp8klwb77qmq5crxzx")))

(define-public crate-ssh-packet-0.3.0 (c (n "ssh-packet") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0y5sx6ixzz82dhvnxn9bmmlzr7ni112359xfwd9cwb3ahc3c2q16")))

(define-public crate-ssh-packet-0.4.0-alpha.1 (c (n "ssh-packet") (v "0.4.0-alpha.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "17ci12hp8avgbxm7mb51j7sxcnyyg8m5wqi36l8jjj9q2x63x418")))

(define-public crate-ssh-packet-0.4.0 (c (n "ssh-packet") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1p0j29s3q77x459378kiyran0rq5f012awir6vnza9kv6raj050v")))

(define-public crate-ssh-packet-0.4.1 (c (n "ssh-packet") (v "0.4.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1dvqhxzql2vv4r2cfzvqh7jjya66j6jwnp4zdizh5whnbc85k1ci")))

(define-public crate-ssh-packet-0.4.2 (c (n "ssh-packet") (v "0.4.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "11kz068fw91pbp65lpd6r56nx4sh6kvrknkc3fan8yiachxmj55m")))

(define-public crate-ssh-packet-0.4.3 (c (n "ssh-packet") (v "0.4.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0bw1kaymw1qrzzbsidf9m8r7yssnbc78z8iv6xmpg8ns272xp75g")))

(define-public crate-ssh-packet-0.4.4 (c (n "ssh-packet") (v "0.4.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "signature") (r "^2.2.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1dl1g9wzqqijan6kn9qc7vp3bqld7vxn0ms7sd544822qqfvnyhd")))

