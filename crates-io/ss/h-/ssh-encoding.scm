(define-module (crates-io ss h- ssh-encoding) #:use-module (crates-io))

(define-public crate-ssh-encoding-0.0.0 (c (n "ssh-encoding") (v "0.0.0") (h "1dp1y8wwwbjd1faiynnkfcacni6w650hm9h17mzz726kskzn8fkc") (y #t)))

(define-public crate-ssh-encoding-0.1.0 (c (n "ssh-encoding") (v "0.1.0") (d (list (d (n "base64") (r "^1.4") (o #t) (d #t) (k 0) (p "base64ct")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "pem") (r "^0.6") (o #t) (d #t) (k 0) (p "pem-rfc7468")) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "1qcia7s3if2fr0rh4q4lb8hbi17pzd2g6hfz7q8n540rw0rdrkqr") (s 2) (e (quote (("std" "alloc" "base64?/std" "pem?/std" "sha2?/std") ("pem" "base64" "dep:pem") ("alloc" "base64?/alloc" "pem?/alloc")))) (r "1.60")))

(define-public crate-ssh-encoding-0.2.0-pre (c (n "ssh-encoding") (v "0.2.0-pre") (d (list (d (n "base64") (r "^1.4") (o #t) (d #t) (k 0) (p "base64ct")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "pem") (r "^0.7") (o #t) (d #t) (k 0) (p "pem-rfc7468")) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "1m6b5p8bqxqm29kin7657c7c9vhllk6yk6pgbqq0xfakawf7cdia") (s 2) (e (quote (("std" "alloc" "base64?/std" "pem?/std" "sha2?/std") ("pem" "base64" "dep:pem") ("alloc" "base64?/alloc" "pem?/alloc")))) (r "1.60")))

(define-public crate-ssh-encoding-0.2.0-pre.0 (c (n "ssh-encoding") (v "0.2.0-pre.0") (d (list (d (n "base64") (r "^1.4") (o #t) (d #t) (k 0) (p "base64ct")) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "pem") (r "^0.7") (o #t) (d #t) (k 0) (p "pem-rfc7468")) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "1py4ns5f7kva7nfqzdas9kqp25ah4f5k6m112mb6q487bbahkrzx") (s 2) (e (quote (("std" "alloc" "base64?/std" "pem?/std" "sha2?/std") ("pem" "base64" "dep:pem") ("alloc" "base64?/alloc" "pem?/alloc")))) (r "1.60")))

(define-public crate-ssh-encoding-0.2.0 (c (n "ssh-encoding") (v "0.2.0") (d (list (d (n "base64") (r "^1.4") (o #t) (d #t) (k 0) (p "base64ct")) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "pem") (r "^0.7") (o #t) (d #t) (k 0) (p "pem-rfc7468")) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "05aavlhk68vm60vbw8lcgx1p5wry367ck8niij7af221xywl54pb") (s 2) (e (quote (("std" "alloc" "base64?/std" "pem?/std" "sha2?/std") ("pem" "base64" "dep:pem") ("bytes" "alloc" "dep:bytes") ("alloc" "base64?/alloc" "pem?/alloc")))) (r "1.60")))

