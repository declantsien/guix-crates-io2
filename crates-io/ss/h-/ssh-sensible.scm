(define-module (crates-io ss h- ssh-sensible) #:use-module (crates-io))

(define-public crate-ssh-sensible-1.0.0 (c (n "ssh-sensible") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0i0lyscg1lfwwh5jlqwr79qj3xndybg28a9j70759aka87mqqdnn")))

