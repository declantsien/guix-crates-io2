(define-module (crates-io ss h- ssh-keys) #:use-module (crates-io))

(define-public crate-ssh-keys-0.1.0 (c (n "ssh-keys") (v "0.1.0") (d (list (d (n "base64") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "151q4rswvc8yg1ca2w9v1bmrkcgz4s5agcpkp3br7cww2g0c95pk")))

(define-public crate-ssh-keys-0.1.1 (c (n "ssh-keys") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "0j2dpn6psxnhrigy4zmn61rpa4lnw735ydrfp9gjkdjxdq8qcq6w")))

(define-public crate-ssh-keys-0.1.2 (c (n "ssh-keys") (v "0.1.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "0fjqgylbnbc16m788ykl7mrgbjgqjkh48bi8h237bm4fpnhny3n8")))

(define-public crate-ssh-keys-0.1.3 (c (n "ssh-keys") (v "0.1.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "11jw6570l1y34dvjm710kpq8l46h85h2hzkcshdfw6i31xlpdpkd")))

(define-public crate-ssh-keys-0.1.4 (c (n "ssh-keys") (v "0.1.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "15cf5488iyys25x0xm10pwkiba6nrdvvxf00h74n3fg3iy2zjm95")))

