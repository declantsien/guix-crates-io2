(define-module (crates-io ss h- ssh-automation) #:use-module (crates-io))

(define-public crate-ssh-automation-0.0.0 (c (n "ssh-automation") (v "0.0.0") (h "1gpgc1vdn6aq2wl5ic5cbwypihr0j804sjl3fd8b3amwflr0v3az") (y #t)))

(define-public crate-ssh-automation-0.1.0 (c (n "ssh-automation") (v "0.1.0") (d (list (d (n "ssh-key") (r "^0.4") (d #t) (k 0)))) (h "0pyiizvb0jrgm82nwwmhmzpggkwhvrgc9dvif054ik470gqvc1q7") (y #t)))

