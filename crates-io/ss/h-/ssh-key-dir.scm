(define-module (crates-io ss h- ssh-key-dir) #:use-module (crates-io))

(define-public crate-ssh-key-dir-0.1.0 (c (n "ssh-key-dir") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "03fjvh3cq7phql7y41ik4r61rs1sdxg6q7j7ikicg8nrkmm2nwyp")))

(define-public crate-ssh-key-dir-0.1.1 (c (n "ssh-key-dir") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0mai5mkdil7f3hpdymflgj9jzzznx8g77cx9rph7xh3sva0305fd")))

(define-public crate-ssh-key-dir-0.1.2 (c (n "ssh-key-dir") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0h481nnbcslsxcs2cvbd1xp0b0y9c2pmsqy1631bcx5lap6raxdk")))

(define-public crate-ssh-key-dir-0.1.3 (c (n "ssh-key-dir") (v "0.1.3") (d (list (d (n "anyhow") (r ">=1.0.38, <2") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("std" "cargo"))) (k 0)) (d (n "nix") (r ">=0.17, <0.24") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "users") (r ">=0.10, <0.12") (d #t) (k 0)))) (h "1q31yj64h1f4gizgyx4ncvscmjk3w4ch03w3fzl8fzddg53kyykc")))

(define-public crate-ssh-key-dir-0.1.4 (c (n "ssh-key-dir") (v "0.1.4") (d (list (d (n "anyhow") (r ">=1.0.38, <2") (d #t) (k 0)) (d (n "clap") (r ">=3.1, <4") (f (quote ("std" "cargo"))) (k 0)) (d (n "nix") (r ">=0.17, <0.26") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "users") (r ">=0.10, <0.12") (d #t) (k 0)))) (h "1swh6nycfvi13gmfq5jr801ps12cy7ysgwhizd93i1jby09qmwz0") (r "1.58.0")))

