(define-module (crates-io ss h- ssh-certifier) #:use-module (crates-io))

(define-public crate-ssh-certifier-0.0.0 (c (n "ssh-certifier") (v "0.0.0") (h "0ga79jisn9a4vxjsd47sppfljlyg2gd37xpybvgsqmsf6pwz9025") (y #t)))

(define-public crate-ssh-certifier-0.1.0 (c (n "ssh-certifier") (v "0.1.0") (d (list (d (n "ssh-key") (r "^0.4") (d #t) (k 0)))) (h "16r30m3i9layhkly0rpyk1xp830xwii5akgjfc3rbgxndbk2w9ng") (y #t)))

