(define-module (crates-io ss h- ssh-keydump) #:use-module (crates-io))

(define-public crate-ssh-keydump-0.1.0 (c (n "ssh-keydump") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sec1") (r "^0.3") (d #t) (k 0)) (d (n "ssh-key") (r "^0.5") (f (quote ("rsa" "dsa" "std" "ecdsa" "ed25519" "encryption"))) (d #t) (k 0)))) (h "1wnfbwv2a5zafxi97i1pzw81lr8539ih9c7xr8nh28f1q9a585zi")))

