(define-module (crates-io ss h- ssh-jail-dto) #:use-module (crates-io))

(define-public crate-ssh-jail-dto-0.1.0 (c (n "ssh-jail-dto") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0wc9fwmxm83k67wkbgj41wnxy2jkrmjcjzr47y9946s4g2zvksr3")))

(define-public crate-ssh-jail-dto-0.1.1 (c (n "ssh-jail-dto") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0mqxb32l92qbadvn65dhcik5ylpbbnbjbisls4sbkbafp9vl4ak5")))

(define-public crate-ssh-jail-dto-0.2.0 (c (n "ssh-jail-dto") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1wiylj1brj47lr4pw40h0rh0b5m8z98vdk3n7mvvrxa0lh72zn9f") (y #t)))

(define-public crate-ssh-jail-dto-0.2.1 (c (n "ssh-jail-dto") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "021n0jcyqs5c16kq7mklpg4zqnkz0j45mkibchx4qxjfhiy3qrx1") (y #t)))

(define-public crate-ssh-jail-dto-0.3.0 (c (n "ssh-jail-dto") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1iakhf4q1x88has6q74541v608f5qqizprfybp6v909lpkjzwkmk") (y #t)))

(define-public crate-ssh-jail-dto-0.4.0 (c (n "ssh-jail-dto") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "03k9mn2glxqfzzdz0p3b4m9gci7xxyhv3nklzqq0l1wj1zfn63vf") (y #t)))

(define-public crate-ssh-jail-dto-0.4.1 (c (n "ssh-jail-dto") (v "0.4.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "11iqxmx6p5bli5fxrb56szq5sahbffqbwii77lvspg9v49b7l64r") (y #t)))

