(define-module (crates-io ss ss ssss) #:use-module (crates-io))

(define-public crate-ssss-0.1.0 (c (n "ssss") (v "0.1.0") (d (list (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "169ydziqri6g86q6q5y7mz724z8fz33binjnzjzia07adpb9vccv")))

(define-public crate-ssss-0.1.1 (c (n "ssss") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 1)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bxlg4pkqvb71y1j67y5nh55hprd0af20cm6b4cq8c0bjcsb62y8") (f (quote (("unstable") ("fuzz" "arbitrary"))))))

(define-public crate-ssss-0.2.0 (c (n "ssss") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.12") (d #t) (k 1)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "11l40z009ysxc0bl14g8aclwnddcf1az1s4dr14ywdq572apk3yw") (f (quote (("unstable") ("fuzz" "arbitrary"))))))

