(define-module (crates-io ss ss sssstd) #:use-module (crates-io))

(define-public crate-sssstd-0.1.0 (c (n "sssstd") (v "0.1.0") (h "02lzr8vrlj9jpmij5yviffi3kjizpmv6rpan0kvnn15n8jmm868p")))

(define-public crate-sssstd-0.1.1 (c (n "sssstd") (v "0.1.1") (h "1khhgk7sgrp0yp2gx7567xx11in9f5v138w2lj8dy97vf5gqyzlj")))

(define-public crate-sssstd-0.1.2 (c (n "sssstd") (v "0.1.2") (h "1xkdy01abazxfcbw2sygw45bb1rmqbfpli9948x6asmn2lh0bz4y")))

