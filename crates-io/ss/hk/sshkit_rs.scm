(define-module (crates-io ss hk sshkit_rs) #:use-module (crates-io))

(define-public crate-sshkit_rs-0.1.0 (c (n "sshkit_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxf35sjm7ga4h6plndbm48qxhljiv79rw7k9a876cnd2wjpvvcc")))

