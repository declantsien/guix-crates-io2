(define-module (crates-io ss hk sshkeys) #:use-module (crates-io))

(define-public crate-sshkeys-0.1.0 (c (n "sshkeys") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)))) (h "08z1qapii0158gz38c80iwmypqfjl272h9bjrsv3hv802wskldpj")))

(define-public crate-sshkeys-0.1.1 (c (n "sshkeys") (v "0.1.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)))) (h "0rrsmk6y2n5qjfgwsjf7a0w6h7i6hlp0fw5xslfg524a33gg6dqv")))

(define-public crate-sshkeys-0.2.0 (c (n "sshkeys") (v "0.2.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)))) (h "1imib9fmpgy8qw4jkxkzx280295rq4ckh6gg02xcp6ymlcysk6z1")))

(define-public crate-sshkeys-0.3.0 (c (n "sshkeys") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0gwz3gjhhx4vy876jzrpm9zrb5g1l5i6xzig0dkhk6dc2gr2j6xj")))

(define-public crate-sshkeys-0.3.1 (c (n "sshkeys") (v "0.3.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1j0gr0584b4iq2z008g5xxks93sw8ij6zh3c84ss9j98lsfvys8c")))

(define-public crate-sshkeys-0.3.2 (c (n "sshkeys") (v "0.3.2") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0z33nqyi8m6ca48klyg68gywk0pa6l0sm1isl5s495kpd80cn9n9")))

(define-public crate-sshkeys-0.3.3 (c (n "sshkeys") (v "0.3.3") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1g85ayqcarrs1hblj1a3v5nc2904mnbz1byipagavxsbs9rp8a25")))

