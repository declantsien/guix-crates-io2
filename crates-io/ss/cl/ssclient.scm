(define-module (crates-io ss cl ssclient) #:use-module (crates-io))

(define-public crate-ssclient-0.99.0 (c (n "ssclient") (v "0.99.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (k 0)) (d (n "getch") (r "^0.2") (d #t) (k 0)) (d (n "securestore") (r "^0.99") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bbgzgxrp769mddcmx9bkp6v6ygh2fbzhc7l2pwxpbn0g8z5akyz") (f (quote (("openssl-vendored" "securestore/openssl-vendored"))))))

(define-public crate-ssclient-0.99.1 (c (n "ssclient") (v "0.99.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (k 0)) (d (n "getch") (r "^0.2") (d #t) (k 0)) (d (n "securestore") (r "^0.99.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "046anxh39fk19cdpj8fgqhbs2yqn59r02w5qdqz6j8yvwa78pq0c") (f (quote (("openssl-vendored" "securestore/openssl-vendored"))))))

(define-public crate-ssclient-0.100.0 (c (n "ssclient") (v "0.100.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("std"))) (k 0)) (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "securestore") (r "^0.100.0") (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1x1gckjfwa3y20w99gr38x707gj725m7z55rw1azjlzmg53f8v6p") (f (quote (("openssl-vendored" "securestore/openssl-vendored"))))))

