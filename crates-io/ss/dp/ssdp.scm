(define-module (crates-io ss dp ssdp) #:use-module (crates-io))

(define-public crate-ssdp-0.1.0 (c (n "ssdp") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "13lk4fnqrzi8qzi2xj1k9r91b52i3hpc3479p08w1n040n3rrlra")))

(define-public crate-ssdp-0.1.1 (c (n "ssdp") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "14w62hgbi4r223ndykc0aa4zvnbgvw88g1m3pgnr03azyrxqyws9")))

(define-public crate-ssdp-0.1.2 (c (n "ssdp") (v "0.1.2") (d (list (d (n "hyper") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1f73bql8sn819cdf1xy3bs8b5nhcy55b2907gablv6sxh546rdy5")))

(define-public crate-ssdp-0.1.3 (c (n "ssdp") (v "0.1.3") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "03if7530swbjcrk5x1ki4ip3gj5gnwnscik972lldrmrpchsrcd0") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.1.4 (c (n "ssdp") (v "0.1.4") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "16iww3qlaab9wx470jqfmiya6vh227jykcnabfw4nz4piwa0vyp4") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.1.5 (c (n "ssdp") (v "0.1.5") (d (list (d (n "hyper") (r "^0.9.0") (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0fqkalg6xir4728lq65fzn5y65ms2wrk9ndhahkag6vhl65d3crc") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.1.6 (c (n "ssdp") (v "0.1.6") (d (list (d (n "hyper") (r "^0.9.0") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0dz2s235vq1lhlb7k63glhm2w0qxnglk0dg5gj42r97qzb223m0n") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.2.6 (c (n "ssdp") (v "0.2.6") (d (list (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1axs38ikmg8a63nb4hs5wzb4cv6bc27ry8s8ls8fjfhsb7a53b4y") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.3.0 (c (n "ssdp") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0isjnshbks058p6inyhkgqr323371j9r3zzpvbkpl7ignm5ngam4") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.3.1 (c (n "ssdp") (v "0.3.1") (d (list (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1nj5lhzw74xz7p9iy3f8pxnr19a5g6zpr4jgn9zyw2xgxpr9in73") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.3.2 (c (n "ssdp") (v "0.3.2") (d (list (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "10rajy1x0h72myr3sygkpydgk0jgsi5r9fcq108w5837n2rn21r6") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.4.0 (c (n "ssdp") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0hw0z9ygkmcqjjkmcng8xi0ncpdf9gyia0yv0nklf0fslvakbp4l") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.5.0 (c (n "ssdp") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.9.3") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0yqy2yv0amhsghs9gz29a989ch4v6gk0lrpd13rll4cv2361n86v") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.6.0 (c (n "ssdp") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "hyper") (r "^0.10.4") (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0acdfxxjsmr6gq4c4hrr7jc2fs4svimwz0g5j25k3nx9khd4zc4k") (f (quote (("unstable"))))))

(define-public crate-ssdp-0.7.0 (c (n "ssdp") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10.4") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1yj1j051pfq3a3zm3gcbz6sb7lf817ad92dn2rq42avq8vd0w0lc") (f (quote (("unstable"))))))

