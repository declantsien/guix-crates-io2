(define-module (crates-io ss dp ssdp-probe) #:use-module (crates-io))

(define-public crate-ssdp-probe-0.1.0 (c (n "ssdp-probe") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14x324nd7r33x49jmyw91m1fb7p9l20vjpkkmnx0qhgdbschs1q5") (f (quote (("envlogger" "pretty_env_logger") ("default"))))))

(define-public crate-ssdp-probe-0.2.0 (c (n "ssdp-probe") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18lnrdcm1rdids84q4nfyps22p04yrcz0ls1qsx8316ab07bzlff") (f (quote (("envlogger" "pretty_env_logger") ("default"))))))

(define-public crate-ssdp-probe-0.2.1 (c (n "ssdp-probe") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "161i1q6r4wsyqpgw2j5if1bgr3jjc7f1axscva3mjy3aa1106jr2") (f (quote (("envlogger" "pretty_env_logger") ("default"))))))

