(define-module (crates-io ss s_ sss_fft) #:use-module (crates-io))

(define-public crate-sss_fft-0.1.0 (c (n "sss_fft") (v "0.1.0") (d (list (d (n "array_par_map") (r "^0.1.0") (d #t) (k 0)) (d (n "iter_spread") (r "^0.1.0") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1v0rhks7l2s2rzbz43v91jzdwcxdyaps098a8ni390mdsfrfiq5b")))

(define-public crate-sss_fft-0.1.1 (c (n "sss_fft") (v "0.1.1") (d (list (d (n "array_par_map") (r "^0.1.0") (d #t) (k 0)) (d (n "iter_spread") (r "^0.1.0") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1wpi55bpb20dxk8bkz1yvs3q4by5dkxdg234n95g03rfvna2jp1v")))

(define-public crate-sss_fft-0.1.2 (c (n "sss_fft") (v "0.1.2") (d (list (d (n "array_math") (r "^0.2.9") (d #t) (k 0)) (d (n "array_par_map") (r "^0.1.0") (d #t) (k 0)) (d (n "iter_spread") (r "^0.1.0") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0v7j88pl9ak4dcm8n3mdp76b1wf397kp2585a2hn57dcnpcdmxwx")))

