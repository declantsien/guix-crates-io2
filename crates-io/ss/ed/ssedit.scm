(define-module (crates-io ss ed ssedit) #:use-module (crates-io))

(define-public crate-ssedit-0.1.0 (c (n "ssedit") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 0)))) (h "1mzkw0c9xwpsd52h61vc2fj3xz103lf2pal71vrwliby508shvnf")))

(define-public crate-ssedit-0.2.0 (c (n "ssedit") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 0)))) (h "0xnxc7j3grk5aznpbnxjq236g0zl1fxqayjv3bxyiaql3d419w1k")))

