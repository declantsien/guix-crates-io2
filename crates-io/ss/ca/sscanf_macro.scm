(define-module (crates-io ss ca sscanf_macro) #:use-module (crates-io))

(define-public crate-sscanf_macro-0.1.0 (c (n "sscanf_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.22") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0dxns24xidn5y130j77r623lrlw9rhvja38xlf8awm9yn30f92rh")))

(define-public crate-sscanf_macro-0.1.1 (c (n "sscanf_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.22") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0khl01k27n11aymsw8f66xmmxqsnxrnsxs0pais3ffa499ixlgcz")))

(define-public crate-sscanf_macro-0.1.2 (c (n "sscanf_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.22") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0i2h7kmvb31bnnf829ylj9nv1jfxyzbviq2sdqnwnk083fijkqs2")))

(define-public crate-sscanf_macro-0.1.3 (c (n "sscanf_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.22") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0rccp64mrq5ql6vkmr95mf4kcbk25qk8vd301g44rn9s28s20imp")))

(define-public crate-sscanf_macro-0.1.4 (c (n "sscanf_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1b7fd0rbr1zxznw67k1k9gafs7ngnbdzcmgf611x7zxgcr4875yn")))

(define-public crate-sscanf_macro-0.2.0 (c (n "sscanf_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1bdzf8aw2ff1sry5h66ka7sfyw7lzlc7v6ia9309wahn2xdc609r")))

(define-public crate-sscanf_macro-0.2.1 (c (n "sscanf_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1hwi1hck7v39afsbbg22w9yigpsbhvgnjxlca4msawkdhm6kc78d")))

(define-public crate-sscanf_macro-0.2.2 (c (n "sscanf_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0pgcn735jym0sp5886l3kzlyvwr7hfswymdr6lfy5dhx60sq7lcm")))

(define-public crate-sscanf_macro-0.3.0 (c (n "sscanf_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1smpnx4p6sxnjim9gbin9ccn8gb2k5ciabk5h158c6lvxj3nf1yn")))

(define-public crate-sscanf_macro-0.3.1 (c (n "sscanf_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1vbb82q573ih1dzdbhdb3kg7f4qa8a4hsf4zzz0j1wnaap90j8zy")))

(define-public crate-sscanf_macro-0.4.0-alpha (c (n "sscanf_macro") (v "0.4.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.50") (f (quote ("parsing" "derive" "full"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "14s0k4kd2wbwhdjic0m4q0kwvi251s3xpyc7ms782p4xn2bck64y")))

(define-public crate-sscanf_macro-0.4.0 (c (n "sscanf_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.50") (f (quote ("parsing" "derive" "full"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1s0f5j7ncv094r0154xys77sv37d7cf7p616i8cc6330d16cs95i")))

(define-public crate-sscanf_macro-0.4.1 (c (n "sscanf_macro") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("parsing" "derive" "full"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "07b1s6svhgbbxar0njhry0ivj2kzmw8vw1wasls5hzhm9akmm5c4") (r "1.56.0")))

