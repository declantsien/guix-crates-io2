(define-module (crates-io ss d1 ssd1307fb) #:use-module (crates-io))

(define-public crate-ssd1307fb-0.1.0 (c (n "ssd1307fb") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3.1") (d #t) (k 0)) (d (n "tinybmp") (r "^0.5.0") (d #t) (k 2)))) (h "1fyxslxf76qb9bp0wp50619dds87xrcgdcgfx8bvvr12rdaiwiz9")))

