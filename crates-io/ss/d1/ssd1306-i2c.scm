(define-module (crates-io ss d1 ssd1306-i2c) #:use-module (crates-io))

(define-public crate-ssd1306-i2c-0.1.0 (c (n "ssd1306-i2c") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)))) (h "0mvzb1506qrp3ambq2zhcj00y4ramj9mvjyf8rgcd0wzh8b5yfb0") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ssd1306-i2c-0.1.1 (c (n "ssd1306-i2c") (v "0.1.1") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)))) (h "0f76jb7q8jla6r3p9s1d186l446v6dmnij9nmzgblib1h9r6zp6b") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ssd1306-i2c-0.1.2 (c (n "ssd1306-i2c") (v "0.1.2") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)))) (h "04wxj27k6vb3zl4cshc600xnys01bf3brrc9gqy7likg4xidc9kw") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ssd1306-i2c-0.1.3 (c (n "ssd1306-i2c") (v "0.1.3") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)))) (h "0m1ad4bgmq1fd348zikv7wgdj1j7xl5k52n4irbcxyyqgqf9gl60") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ssd1306-i2c-0.1.4 (c (n "ssd1306-i2c") (v "0.1.4") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)))) (h "04zw6kfnhsqh2hc7x9mjzln8xb33v0vxx8jhz7wnwq7g6xg9gg80") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

