(define-module (crates-io ss d1 ssd1327) #:use-module (crates-io))

(define-public crate-ssd1327-0.1.0 (c (n "ssd1327") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "105sfiqzz65ajwkm01p5cl4pi02nhi7zvibrgs9avb5rr6afgxmy")))

