(define-module (crates-io ss d1 ssd1327-i2c) #:use-module (crates-io))

(define-public crate-ssd1327-i2c-0.2.0 (c (n "ssd1327-i2c") (v "0.2.0") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "esp-backtrace") (r "^0.9.0") (f (quote ("esp32" "panic-handler" "exception-handler" "print-uart"))) (d #t) (k 2)) (d (n "esp-println") (r "^0.7.0") (f (quote ("esp32"))) (d #t) (k 2)))) (h "0sdhabpgmrrlw97km11aicbhnnpj1kvh2mlhlphfssg3kyljgfgd") (f (quote (("default" "graphics")))) (s 2) (e (quote (("graphics" "dep:embedded-graphics-core"))))))

