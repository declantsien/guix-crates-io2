(define-module (crates-io ss d1 ssd1315) #:use-module (crates-io))

(define-public crate-ssd1315-0.0.0 (c (n "ssd1315") (v "0.0.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "11k6vc06zdkss8ck4mgzdaw6ib7hb8kilqgjndvabd041cp5q7q7") (y #t)))

(define-public crate-ssd1315-0.0.1 (c (n "ssd1315") (v "0.0.1") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0zpmcy7ihxkh27637jkxhd0aaax9hlv3hmx6ryqiykjiiwfim1an") (y #t)))

(define-public crate-ssd1315-0.0.2 (c (n "ssd1315") (v "0.0.2") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0cvq9c07p7qxh08r5w4s7ck3f3xgl36dw4fw0qbq66qldfqk0dp1") (y #t)))

(define-public crate-ssd1315-0.1.0 (c (n "ssd1315") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1fwqqg87k5d62hxxl3xp2hkflrh20qlxgai1wzhznb2sjv9bpgb9")))

(define-public crate-ssd1315-0.1.1 (c (n "ssd1315") (v "0.1.1") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "096vgnxbnds0qbc8rhcq7javjbd7m0gxcb32zwg9invky35ygbii")))

(define-public crate-ssd1315-0.1.2 (c (n "ssd1315") (v "0.1.2") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1a53v6dyq6rrgwkdfqfg5fbxfr3fg3ffhphg0q7jf2wr2fk7x1gq")))

(define-public crate-ssd1315-0.1.3 (c (n "ssd1315") (v "0.1.3") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0rqqjl554505lvjh30g2ln2ichhhj68ac6w12ldn2n4ykjhcjw3l")))

