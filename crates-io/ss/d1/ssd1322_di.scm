(define-module (crates-io ss d1 ssd1322_di) #:use-module (crates-io))

(define-public crate-ssd1322_di-0.1.0 (c (n "ssd1322_di") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.4") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "058mrhnsry8m3v7l4f226mzwr3509b1aim6z39r5h3m1cdq85rg4")))

(define-public crate-ssd1322_di-0.2.0 (c (n "ssd1322_di") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.4") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1gwjyy07cs7b4dqv7rjcsnlcwfac18g67c1lxm9a48nx6ydbpakh")))

