(define-module (crates-io ss d1 ssd1681) #:use-module (crates-io))

(define-public crate-ssd1681-0.1.0 (c (n "ssd1681") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "15kd37vl0kiyw6xfnd27h16b0kqghcxwp3qkxa9ib1llzz1dypal") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

