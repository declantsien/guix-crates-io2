(define-module (crates-io ss d1 ssd1680) #:use-module (crates-io))

(define-public crate-ssd1680-0.1.0 (c (n "ssd1680") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0wh21xgfywr6vi9dn2jmq8bjjjwmdhrg11jmsksl36lfm6q91wda") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

