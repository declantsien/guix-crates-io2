(define-module (crates-io ss d1 ssd1322) #:use-module (crates-io))

(define-public crate-ssd1322-0.1.0 (c (n "ssd1322") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "01zh9fhvzr2d8vyy7yx078mv4islc3yq3av59rg8f0ii03lkhmfc") (f (quote (("std") ("default" "std"))))))

(define-public crate-ssd1322-0.2.0 (c (n "ssd1322") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1ai89jk1101bxgvi9scq9kzfik0cibxqbrvxklfpj4yc9j46zyh5") (f (quote (("std") ("default" "std"))))))

(define-public crate-ssd1322-0.2.1 (c (n "ssd1322") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0zlhs20lqnx5f94642ym913dp8zxcawh5idzyhig5qi725alc4i7") (f (quote (("std") ("default" "std"))))))

(define-public crate-ssd1322-0.2.2 (c (n "ssd1322") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1li99wyzhpxz31f6psm1hf5l9s91sg91bnaz3c2igd7b21m4hj2f") (f (quote (("std") ("default" "std"))))))

(define-public crate-ssd1322-0.3.0 (c (n "ssd1322") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0s9wzhq9aghv03xqzd553z0bkq247cg0lai6khvn8v5r4zv1v0ay") (f (quote (("std") ("default" "std"))))))

