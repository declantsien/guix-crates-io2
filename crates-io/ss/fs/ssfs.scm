(define-module (crates-io ss fs ssfs) #:use-module (crates-io))

(define-public crate-ssfs-0.1.0 (c (n "ssfs") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.0") (d #t) (k 0)))) (h "1rrw5rrg75ayb46yi0gjx94bdk7qvssnikjd1x7fbxrrb2kwdziz")))

