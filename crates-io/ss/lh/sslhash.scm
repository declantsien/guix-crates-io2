(define-module (crates-io ss lh sslhash) #:use-module (crates-io))

(define-public crate-sslhash-0.1.0 (c (n "sslhash") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.2") (d #t) (k 0)))) (h "0jjbwdda1rn3dbf8f94vlvv57dchbbsha7pif9csn42dwjz6kvrw")))

(define-public crate-sslhash-0.1.1 (c (n "sslhash") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.2") (d #t) (k 0)))) (h "1z4wvbd650z79wi6m6x59p1ig5a6k25cm2aak1bskbzp4x9ry35w")))

