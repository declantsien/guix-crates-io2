(define-module (crates-io ss zb sszb_derive) #:use-module (crates-io))

(define-public crate-sszb_derive-0.3.0 (c (n "sszb_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0md4c541nqgqd46kji8972qr70dmgzhwrrllbbm731666pzzgnfy")))

