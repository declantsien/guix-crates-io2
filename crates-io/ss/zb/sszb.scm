(define-module (crates-io ss zb sszb) #:use-module (crates-io))

(define-public crate-sszb-0.4.0 (c (n "sszb") (v "0.4.0") (d (list (d (n "ethereum-types") (r "^0.12.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "sszb_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1qihrkjc06d73qil0rmn0faqx16gprdh7fq31cdkp6m5sv6zpfk2") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

