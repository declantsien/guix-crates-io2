(define-module (crates-io ss bc ssbc) #:use-module (crates-io))

(define-public crate-ssbc-0.1.0 (c (n "ssbc") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "010a7yfrarpyiazz9vwadkhc657r44gnkc227v5wq9kxhyyfqd0w") (f (quote (("signedmagnitude_sub"))))))

