(define-module (crates-io ss -t ss-trees) #:use-module (crates-io))

(define-public crate-ss-trees-0.1.0 (c (n "ss-trees") (v "0.1.0") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "ss-web-utils") (r "^0.1.0") (d #t) (k 0)))) (h "14a7v1l13sy6p6wp6bvfwdm4nxxk4izpaqp03iaqcfqwsjqyrxv1") (y #t)))

(define-public crate-ss-trees-0.1.1 (c (n "ss-trees") (v "0.1.1") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "ss-web-utils") (r "^0.1.0") (d #t) (k 0)))) (h "00igik7y49x19la22vksn2lw56bmf961spih8csvh71mx7pnxgfp") (y #t)))

