(define-module (crates-io ss -c ss-css-types) #:use-module (crates-io))

(define-public crate-ss-css-types-0.1.0 (c (n "ss-css-types") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "03gpklq5jqj0bgb7j4kcz8h6y87mlr95491355djq46r9jy7a3an") (y #t)))

(define-public crate-ss-css-types-0.1.1 (c (n "ss-css-types") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "00j7fqq1qinx8j70q6k317zpil1hfpaaxa3x771v7nk1krsl2xgh") (y #t)))

