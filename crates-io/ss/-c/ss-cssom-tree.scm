(define-module (crates-io ss -c ss-cssom-tree) #:use-module (crates-io))

(define-public crate-ss-cssom-tree-0.1.0 (c (n "ss-cssom-tree") (v "0.1.0") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "ss-css-types") (r "^0.1.0") (d #t) (k 0)))) (h "0x7q7lhf037qglnpdn7fpmb0v1hxxc159775llfw8bk3wmvkv277") (y #t)))

(define-public crate-ss-cssom-tree-0.1.1 (c (n "ss-cssom-tree") (v "0.1.1") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "ss-css-types") (r "^0.1.0") (d #t) (k 0)))) (h "03qk54jvhg2mk9sqviz87bdvvkpgzbrgn09296wxwzx6rl4n962p") (y #t)))

