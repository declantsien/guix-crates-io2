(define-module (crates-io ss ta sstable) #:use-module (crates-io))

(define-public crate-sstable-0.1.0 (c (n "sstable") (v "0.1.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1nr9jnr6v9rh9nvja44fcxdbxmq76nd00gawk91vjpgvqxv94pjh")))

(define-public crate-sstable-0.2.0 (c (n "sstable") (v "0.2.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "0pm0dvr8q9ji2szgapix2ziqw1bkbj0kfixl441q660kpnnm718v")))

(define-public crate-sstable-0.2.1 (c (n "sstable") (v "0.2.1") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "0pr7p9zvb12079ir2zzw2cs2zp4sd9s6x5rbny1yqphnd5ayi90m")))

(define-public crate-sstable-0.3.0 (c (n "sstable") (v "0.3.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1g5p6k9p2bqlqbn3h5rc3fraasagg5z31jv3xxq91c2qjla5jn9x")))

(define-public crate-sstable-0.3.1 (c (n "sstable") (v "0.3.1") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "0yhiawa6cfvmbl3yvj7jmibakfddx5pr7yv8393dm1zcvhs0fkgd")))

(define-public crate-sstable-0.3.2 (c (n "sstable") (v "0.3.2") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1nz7jk2m4hvmflmy6x3qcnm56q4ha8vhw89gx030y10a2xja8kqh")))

(define-public crate-sstable-0.4.0 (c (n "sstable") (v "0.4.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1q0cl3rvnsf0v355xr0wwk8d8zjgfvh8b9k7v5g67xzkckbg7xii")))

(define-public crate-sstable-0.4.1 (c (n "sstable") (v "0.4.1") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1zd4n5rjscxmppdx7xarjgi2f9sw8arn59lrwvdzlqdqjhvrw19x")))

(define-public crate-sstable-0.4.2 (c (n "sstable") (v "0.4.2") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "1a47gflnn1x5yxhbfmf0kibn7b4xyhhmh8lpc371n2wgfv5xi2s9")))

(define-public crate-sstable-0.4.3 (c (n "sstable") (v "0.4.3") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "0lb8hyxqmd14r8j344bkd1biznl5hl8bmsx1y2ij0yxvc91rdygi")))

(define-public crate-sstable-0.4.4 (c (n "sstable") (v "0.4.4") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)))) (h "0zjnapdfh3i6fw9lajbmmpay246xn80ykym7ivfkdwx1m9gx5wka")))

(define-public crate-sstable-0.5.0 (c (n "sstable") (v "0.5.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1fihf546y0q847lyvl9ksn1a699z1whfpi2ak5cy6d2i8cimpc1w")))

(define-public crate-sstable-0.5.1 (c (n "sstable") (v "0.5.1") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1rdjzxx57gwks4bv2l0k5lri1kzkkkkwdz1hyg4k9rm1n5awwl1x")))

(define-public crate-sstable-0.6.0 (c (n "sstable") (v "0.6.0") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0j0ffibpdasbbanv946lj3a8wwh6xycza174gf2610ld8ilxf5mg")))

(define-public crate-sstable-0.6.1 (c (n "sstable") (v "0.6.1") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0wklaq28ycmya23k6jix2pzaxd8hk4vlgd02r8rf5gmhkm2ix9wj")))

(define-public crate-sstable-0.6.2 (c (n "sstable") (v "0.6.2") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0x3d4001hbm2waqw7zfgy7p50qpqq2ybwkx1s20xcav1cx1dhsjd")))

(define-public crate-sstable-0.6.3 (c (n "sstable") (v "0.6.3") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1i9yxccm8dsi3q42g2lnxs16z64czdyi02msz44y4yp5hxgg2jgz")))

(define-public crate-sstable-0.6.4 (c (n "sstable") (v "0.6.4") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1kvf0gcjs1hwqiijs0cn9jqz98wfdgjyk7w8miv3j416c0swybq0")))

(define-public crate-sstable-0.6.5 (c (n "sstable") (v "0.6.5") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0q1z8dljychiy6amxhvc96613sl75c5v0z2ailk6x3jcnmgxmghd")))

(define-public crate-sstable-0.6.6 (c (n "sstable") (v "0.6.6") (d (list (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "03616aylp7hp9vnsbm35xcx49ica82lf2l0y4xx449jjxyrrj6i6")))

(define-public crate-sstable-0.6.7 (c (n "sstable") (v "0.6.7") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0g644gcziazsbnlqby62yf0a169byssa1s1pd10jlahnbzi19j3g")))

(define-public crate-sstable-0.7.0 (c (n "sstable") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0xx75j57frdahq3084ddap0wxgnqxav11ia72rf1wd3vdyrf6csl")))

(define-public crate-sstable-0.8.0 (c (n "sstable") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "155f26qy2gv9wnac6cqmsfzdd9bd3b9lkkrnqx2zc4yrkm313b2x")))

(define-public crate-sstable-0.8.1 (c (n "sstable") (v "0.8.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1kfzmh4bsp9dwg08p7c8y37da1srii758a19c0dv03cwpchzza8x")))

(define-public crate-sstable-0.8.2 (c (n "sstable") (v "0.8.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "1lrxwl6pp1j643zdyhnmjaxq62xb580q8czwy0i7aljycfvicgw5")))

(define-public crate-sstable-0.9.0 (c (n "sstable") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "17ksh4q8dmkz06wwpiszp60wzp78b3l48p0hksc159fmn237snnx")))

(define-public crate-sstable-0.10.0 (c (n "sstable") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0wb4qs7bab3nhfywnlqiin12fxn4wxfslsngbhj3ndq7zcj4s9zj")))

(define-public crate-sstable-0.11.0 (c (n "sstable") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0a2r4glzrsygd5y8yhapny9mlpysg12vfh4zfldbm2v05xx7apf7")))

(define-public crate-sstable-0.11.1 (c (n "sstable") (v "0.11.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)))) (h "0pp0n1sn1ik7saq10ngf7zbk7nkg2x3dyj85nh2v3h1z014zagvy")))

