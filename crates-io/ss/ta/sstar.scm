(define-module (crates-io ss ta sstar) #:use-module (crates-io))

(define-public crate-sstar-0.1.0 (c (n "sstar") (v "0.1.0") (h "1g08g0fv6w2lzgwhcipcsldg5129czw823k0gakfs0aq070hvi8g") (y #t)))

(define-public crate-sstar-0.1.1 (c (n "sstar") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1nzxz3gz046aq296n13n30vsgnnq19avrvk248arz7dmxqijbfcz") (y #t)))

(define-public crate-sstar-0.1.2 (c (n "sstar") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1mz6w2nplhbh6w1010p5f5lmqy2bklg3pa2pswcr3l12rlc5azs8")))

(define-public crate-sstar-0.1.3 (c (n "sstar") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1xqlajjinl1b8ckwjp8v9dgm7q7wqchka6wby8d8ksanmkmgs5sh")))

(define-public crate-sstar-0.1.4 (c (n "sstar") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "13xn5hpfi8w7g0d6xphd93bv98yxf7g7lbjj417jh6amjnlmp7jn")))

(define-public crate-sstar-0.1.5 (c (n "sstar") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "shaderc") (r "^0.8") (d #t) (k 1)))) (h "1527malfkz3nm9rncbdzmnl84lwhn68z6qvxf4jlik2bjxp0z1rd") (f (quote (("with-default-shaders") ("default")))) (y #t)))

(define-public crate-sstar-0.1.6 (c (n "sstar") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0vanyq9af1rf9klx9b3cv8bpq7617pm1jw77bjg5alcgsjk07wd3") (f (quote (("with-default-shaders") ("default"))))))

(define-public crate-sstar-0.1.7 (c (n "sstar") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0zv0dp1vv4l9hbn54j73zah3bm773ikwrcfp2zf5vljnw4pa1bq3") (f (quote (("with-default-shaders") ("default"))))))

(define-public crate-sstar-0.1.8 (c (n "sstar") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "12za6wxqfsznzvqqfgdsw2ywr8ff063p2kdl0i4jzk4l8lpyfh8p") (f (quote (("with-default-shaders") ("default"))))))

