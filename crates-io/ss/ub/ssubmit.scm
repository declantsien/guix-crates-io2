(define-module (crates-io ss ub ssubmit) #:use-module (crates-io))

(define-public crate-ssubmit-0.1.0 (c (n "ssubmit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "068nki3dlwb78yd346lmmnp48w122fckjmg5cl78qah096s214ck") (r "1.58")))

(define-public crate-ssubmit-0.1.1 (c (n "ssubmit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06cv8mzgaivg6k340mi54vj8pm02x8drgqf46d83y04mrc3rll2s") (r "1.58")))

(define-public crate-ssubmit-0.2.0 (c (n "ssubmit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pp21xims7v206kqy5v18a1z5dx3rs4pqchp0cwp5za83f1m276y") (r "1.58")))

(define-public crate-ssubmit-0.3.0 (c (n "ssubmit") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.7.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "07a6vr1la5q745ac96frg1wg361b8rwqpypbnfr2z7z22q6bly1q") (r "1.58")))

