(define-module (crates-io ss ag ssage) #:use-module (crates-io))

(define-public crate-ssage-0.1.0-alpha1 (c (n "ssage") (v "0.1.0-alpha1") (d (list (d (n "iter_tools") (r "^0.1") (k 0)) (d (n "priority-queue") (r "^1.2") (k 0)) (d (n "unicase") (r "^2.6") (k 0)))) (h "06ym934ygmap59h6gwpgg5hkmkh8m4laqhqbpbfsy2hn4dmv6x77")))

