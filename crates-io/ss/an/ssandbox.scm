(define-module (crates-io ss an ssandbox) #:use-module (crates-io))

(define-public crate-ssandbox-0.1.0 (c (n "ssandbox") (v "0.1.0") (d (list (d (n "caps") (r "^0.5.1") (d #t) (k 0)) (d (n "cgroups-rs") (r "^0.2.3") (d #t) (k 0)) (d (n "libscmp") (r "^0.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0gr70c5c3yf89fj0wwnpbxsjzhk7fbbrci3020i9fkiqqh04rdqj")))

