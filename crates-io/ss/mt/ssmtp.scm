(define-module (crates-io ss mt ssmtp) #:use-module (crates-io))

(define-public crate-ssmtp-1.0.0 (c (n "ssmtp") (v "1.0.0") (h "0scvmg5vmmkgw436i8h0p3g8x4159knjcsmg03fpv84w7rn01y0i") (y #t)))

(define-public crate-ssmtp-1.0.1 (c (n "ssmtp") (v "1.0.1") (h "1pqcm3x76r9hh4qm6f1hvrk1nxvky2w5i7rnminmaq3znj5d9sqw")))

