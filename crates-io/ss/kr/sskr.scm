(define-module (crates-io ss kr sskr) #:use-module (crates-io))

(define-public crate-sskr-0.1.0 (c (n "sskr") (v "0.1.0") (d (list (d (n "bc-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0dbkdpc80yv5gfdmdvbfisk9g8fgw8hk9z0a68ic7fv1nj1a3b6s")))

(define-public crate-sskr-0.1.1 (c (n "sskr") (v "0.1.1") (d (list (d (n "bc-crypto") (r "^0.1.1") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11cvdgl4scvzksyvds0vgsw4yjm2xswzbczsvh00zgwqidqz3qmi")))

(define-public crate-sskr-0.1.2 (c (n "sskr") (v "0.1.2") (d (list (d (n "bc-crypto") (r "^0.1.2") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1mn7dikz4lnp6lfhc65kkwlscwwx6lsrwvxdz1y8qyk252p0zrl9")))

(define-public crate-sskr-0.2.0 (c (n "sskr") (v "0.2.0") (d (list (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0900g80yk7hz7s46s2fixxp8rkyvcyxsflp92zmvs6zmaxsidyb8")))

(define-public crate-sskr-0.3.0 (c (n "sskr") (v "0.3.0") (d (list (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0p1pkzaddha5by5ls7h00csv27kh66l0bxg0blwkrxm7amlk3jrr")))

(define-public crate-sskr-0.3.1 (c (n "sskr") (v "0.3.1") (d (list (d (n "bc-rand") (r "^0.1") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "02ydccl18ca3m2ag4lp3cfidcl7pjx8qdrprf7xmpm7vs3xd076d")))

(define-public crate-sskr-0.3.2 (c (n "sskr") (v "0.3.2") (d (list (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "bc-shamir") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0mjgni5qbd56g73dyc8xqp9q2q5w1lllafhhw26csi07gjpx275y")))

