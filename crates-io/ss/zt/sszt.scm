(define-module (crates-io ss zt sszt) #:use-module (crates-io))

(define-public crate-sszt-0.1.1 (c (n "sszt") (v "0.1.1") (d (list (d (n "ethereum-types") (r "^0.6.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "ssz") (r "^0.1.2") (d #t) (k 0)) (d (n "ssz-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0gr8pky9l1adfnql2zsqmwf56fs1wnha0jadnbcp7kkd4qk55lfg")))

(define-public crate-sszt-0.1.2 (c (n "sszt") (v "0.1.2") (d (list (d (n "ethereum-types") (r "^0.6.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "ssz") (r "^0.1.2") (d #t) (k 0)) (d (n "ssz-derive") (r "^0.1.2") (d #t) (k 2)))) (h "0rh3xlrq8kyjshrisgs5imzl8aax16ljqxki045yhgnjwfl1iciv")))

