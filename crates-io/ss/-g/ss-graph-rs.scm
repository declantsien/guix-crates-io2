(define-module (crates-io ss -g ss-graph-rs) #:use-module (crates-io))

(define-public crate-ss-graph-rs-0.1.7 (c (n "ss-graph-rs") (v "0.1.7") (h "056nwqladddykx0viq5hiz2i4gjx8z40fc7najljcwnf5i6g0q66")))

(define-public crate-ss-graph-rs-0.1.8 (c (n "ss-graph-rs") (v "0.1.8") (h "1qmkzphzaidkwa1hzl3zfi6wymc65dzp9m7ivrlc4wnj2mkljk3z")))

