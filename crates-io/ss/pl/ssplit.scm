(define-module (crates-io ss pl ssplit) #:use-module (crates-io))

(define-public crate-ssplit-1.0.0 (c (n "ssplit") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "13c68ih3plnwdsd5dn2nmaa3c5k6nh85rp2q30gj7f1302isc4q7")))

