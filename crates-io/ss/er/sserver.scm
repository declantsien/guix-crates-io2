(define-module (crates-io ss er sserver) #:use-module (crates-io))

(define-public crate-sserver-1.0.0 (c (n "sserver") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0g5a1jkswky37d2898pgv05x4pl5grshwi80qg07wx73did799sz")))

(define-public crate-sserver-1.0.1 (c (n "sserver") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1m0s40783sdm91v21a0099j9m2x12f89i5q4m7xkf2fj224l2fa2")))

(define-public crate-sserver-1.0.2 (c (n "sserver") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "comrak") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "02lb6rqd3dfdqwm6hi541xngz06qs775ll1b9gspl8f19j2azda8")))

(define-public crate-sserver-1.0.3 (c (n "sserver") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0wjiq2sml7arcjxg15bxkrjbw2qnf182i5gf7zw3x5q7xip7gz9h")))

