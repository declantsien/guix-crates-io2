(define-module (crates-io ss to sstop) #:use-module (crates-io))

(define-public crate-sstop-0.1.0 (c (n "sstop") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0m2a858130a2zns6lcf3byy5wyqiksvvzb70n7qw84br4h50fp5k")))

(define-public crate-sstop-0.1.1 (c (n "sstop") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0rfgjl0x21mlcpi6l21bdf8spq4p0iajm95x0b9rc07n0ly8k1ki")))

(define-public crate-sstop-0.1.2 (c (n "sstop") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "03xpxbs8kh6njrynl53110kz3d4v6jrrf9rl2hflvb8pl5jkqkd7")))

(define-public crate-sstop-0.1.3 (c (n "sstop") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1lq7sifqbkb0amhzidi3glwmilqscr2frj69jqrylbscsq1jygfn")))

(define-public crate-sstop-0.1.4 (c (n "sstop") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "10n0665656d8xs7zj7m5lbalf3v9vcihvwixc4d2dlqmkdz1ydj7")))

(define-public crate-sstop-0.1.5 (c (n "sstop") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "19amgjd35x7xjr2sk24lnc2w9kfvy72l2h071y0zpj96wfzsaiws")))

