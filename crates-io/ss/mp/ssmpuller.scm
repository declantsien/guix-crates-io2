(define-module (crates-io ss mp ssmpuller) #:use-module (crates-io))

(define-public crate-ssmpuller-0.1.0 (c (n "ssmpuller") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.10") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.10") (d #t) (k 0)) (d (n "aws-types") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n6bvw7q83acvki11q957k0ybnk31m9lc8bs2w5a4qfg0cmlzk1m")))

