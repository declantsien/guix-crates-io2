(define-module (crates-io ss ip ssip-common) #:use-module (crates-io))

(define-public crate-ssip-common-0.1.0 (c (n "ssip-common") (v "0.1.0") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1r778fjgkf01w6fdv1m4qgmc2s1dfcj04ndzb61765w4y7nyk4vi")))

