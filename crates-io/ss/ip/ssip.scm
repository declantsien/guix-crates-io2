(define-module (crates-io ss ip ssip) #:use-module (crates-io))

(define-public crate-ssip-0.1.0 (c (n "ssip") (v "0.1.0") (d (list (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "14r55n2b152ai3kg1addphvbmikms0ibynw3hhvbp3wp4gfhkijz")))

