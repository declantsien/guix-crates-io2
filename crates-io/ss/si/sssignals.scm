(define-module (crates-io ss si sssignals) #:use-module (crates-io))

(define-public crate-sssignals-0.1.0 (c (n "sssignals") (v "0.1.0") (h "1ddgdjwka8scvxx075s15sz32hca73c2fr8rz653n9nk8b7cyvjd")))

(define-public crate-sssignals-0.2.0 (c (n "sssignals") (v "0.2.0") (h "1i65nzfk51f4sk953lpnp1jhaqdb4sngycand39i2dcjdjrfl4r2")))

(define-public crate-sssignals-0.2.1 (c (n "sssignals") (v "0.2.1") (h "1m3k6dgjcgn407xy9p7rw6kizl98iyqy6yx0l8w0k5205cfz5q7z")))

