(define-module (crates-io ss hx sshx-core) #:use-module (crates-io))

(define-public crate-sshx-core-0.1.0 (c (n "sshx-core") (v "0.1.0") (h "03rm6gq364vg5jlqm32fqz34140h96rf0yc7h00g9nca5ngn7p0y")))

(define-public crate-sshx-core-0.2.0 (c (n "sshx-core") (v "0.2.0") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (f (quote ("tls" "tls-webpki-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (d #t) (k 1)))) (h "0wla9yxa28966rpa3bmf2frfpx0x2qwa6zpxla9z2yjfv99l6wav")))

(define-public crate-sshx-core-0.2.1 (c (n "sshx-core") (v "0.2.1") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (f (quote ("tls" "tls-webpki-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (d #t) (k 1)))) (h "0qqqj3pb09wyx67x4z8qjccw4vsx32zdnaw81lvdbz7xniphpfj7")))

(define-public crate-sshx-core-0.2.2 (c (n "sshx-core") (v "0.2.2") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (f (quote ("tls" "tls-webpki-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (d #t) (k 1)))) (h "15jhdzzazap7q4ys37bqg05ssyn18m63kppvjkp888jdd5czx4dl")))

(define-public crate-sshx-core-0.2.3 (c (n "sshx-core") (v "0.2.3") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (f (quote ("tls" "tls-webpki-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (d #t) (k 1)))) (h "05z9wr2lg7fxsgyj2hc5jhbfzdp2fmfzlhvfwl5agspmv8gsfgkq")))

