(define-module (crates-io ss h_ ssh_jumper_model) #:use-module (crates-io))

(define-public crate-ssh_jumper_model-0.1.0 (c (n "ssh_jumper_model") (v "0.1.0") (d (list (d (n "async-ssh2-lite") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std-resolver") (r "^0.20.0") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1qwjb76718nrv1b1vflmhn8zmy35qzr4c3ygsmsgdkr7c3fqajh9")))

(define-public crate-ssh_jumper_model-0.1.1 (c (n "ssh_jumper_model") (v "0.1.1") (d (list (d (n "async-ssh2-lite") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std-resolver") (r "^0.20.1") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1004cd0jrqgx51ynkm7pzp8rrvsdlpfiq5sg2y17dzwa6w81mq6p")))

(define-public crate-ssh_jumper_model-0.2.0 (c (n "ssh_jumper_model") (v "0.2.0") (d (list (d (n "async-ssh2-lite") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std-resolver") (r "^0.20.3") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt"))) (d #t) (k 0)))) (h "16881ynxkw3m1x05qccjpam40ph7p882flsnnqm318y25sfpbnwv")))

(define-public crate-ssh_jumper_model-0.3.0 (c (n "ssh_jumper_model") (v "0.3.0") (d (list (d (n "async-ssh2-lite") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std-resolver") (r "^0.20.3") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0wgzq1l94d5dn9zyxzgqa6adfl6gwrvyd65fssqrqw13yfqi500j")))

(define-public crate-ssh_jumper_model-0.4.0 (c (n "ssh_jumper_model") (v "0.4.0") (d (list (d (n "async-ssh2-lite") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std-resolver") (r "^0.20.3") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1gkvx2m04kinrv77ncgnkq6mni6k26mhd7qq3wlvjvbbapgfawp2")))

