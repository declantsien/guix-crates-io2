(define-module (crates-io ss h_ ssh_cfg2) #:use-module (crates-io))

(define-public crate-ssh_cfg2-0.1.1 (c (n "ssh_cfg2") (v "0.1.1") (d (list (d (n "async-fs") (r "^1.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "004k3nvwshq58xis2n1nfpn5vp3i8y8m59a0vdh3djw4q53wlbal")))

