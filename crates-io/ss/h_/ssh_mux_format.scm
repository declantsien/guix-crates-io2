(define-module (crates-io ss h_ ssh_mux_format) #:use-module (crates-io))

(define-public crate-ssh_mux_format-0.1.0 (c (n "ssh_mux_format") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "191axnx2jmpsaqi81088i76v9383fxyji4ggsy57nirvqam5czph") (y #t)))

(define-public crate-ssh_mux_format-0.2.0 (c (n "ssh_mux_format") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0q0y6kx09yxc9s8zmcn96zl2wlhw3il2038cqnmlxg1p7hmqmdmx") (y #t)))

(define-public crate-ssh_mux_format-0.2.1 (c (n "ssh_mux_format") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "09679z98jvcx9jmf2112j0yb76x8f80lplvl2dmrmzcrcd6ihjnq") (y #t)))

(define-public crate-ssh_mux_format-0.3.0 (c (n "ssh_mux_format") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1id9k9dmwahf003g9b8wwphila6k1pky1vr1h58nfw8zmsl5arym") (y #t)))

(define-public crate-ssh_mux_format-0.3.1 (c (n "ssh_mux_format") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zzd1wjq5k7xi2zb00rw77kxpgpbamjigcjr3v4b8aapflbz6pz2") (y #t)))

(define-public crate-ssh_mux_format-0.3.2 (c (n "ssh_mux_format") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0637vyayhbmfrn49hj4sfpmlzihcgrz0hbi3m70mq6543yvnlkjx") (y #t)))

(define-public crate-ssh_mux_format-0.3.4 (c (n "ssh_mux_format") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0a3247fclijbqb4idhz3khc0ramjgwvmkcf3f24h57f0xfjwpwc0") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_mux_format-0.4.0 (c (n "ssh_mux_format") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "13q67nrfkgv3i4zv8xqn5px18gxm9ff8p0k7893qq6db55l5p7wz") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_mux_format-0.4.1 (c (n "ssh_mux_format") (v "0.4.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1lckkl0wxycy7hagsx8a3rhkzx2ghljvwrymvajl101iyfh3lhjg") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_mux_format-0.4.2 (c (n "ssh_mux_format") (v "0.4.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "09hwin2cd07llivibwxz57b4m0gnvxbk3nipih65d7dwwkqbv5j8") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_mux_format-0.4.3 (c (n "ssh_mux_format") (v "0.4.3") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mfgvc6nhqf5ba2nm2c1k0xy3ipfz1y4qr1v2kihrddhy2sad725") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_mux_format-0.4.4 (c (n "ssh_mux_format") (v "0.4.4") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1i6zdgpl5zcb2r6nmdg9866qp47l3i1h1lr0jxd0cv985r0jzdkf") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_mux_format-0.4.5 (c (n "ssh_mux_format") (v "0.4.5") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vw7rvdfa3bhicgznf7jaf8kqd8r0fnys0s6wmbwyma4j4l48sjv") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_mux_format-0.4.6 (c (n "ssh_mux_format") (v "0.4.6") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0qvislj25bqf2019k0b66556wa3p1qdwlhsv0jwlszgmkz6zpdxv") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_mux_format-0.4.7 (c (n "ssh_mux_format") (v "0.4.7") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "13142py3xswpmi7198v22n921dvca5639vvqw4n84p506c9gvzpx") (f (quote (("is_human_readable"))))))

