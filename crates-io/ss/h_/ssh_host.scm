(define-module (crates-io ss h_ ssh_host) #:use-module (crates-io))

(define-public crate-ssh_host-0.1.0 (c (n "ssh_host") (v "0.1.0") (d (list (d (n "whoami") (r "^0.6.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0hsra5q9lx4vkgij37w0gsrby9mh4aca7kh97ash3dgjhzw65d32")))

(define-public crate-ssh_host-0.1.1 (c (n "ssh_host") (v "0.1.1") (h "1sgbl030pyd8lld2ldz6zp6qf4a2wz5r2s5b0nqj734hy2kbig5m")))

(define-public crate-ssh_host-1.0.0 (c (n "ssh_host") (v "1.0.0") (d (list (d (n "whoami") (r "^0.6.0") (d #t) (k 0)))) (h "0m0a1rywhd6w23wab2shfwdqlzvwsys98lyzw8zvx2fja5s0q6f2")))

(define-public crate-ssh_host-1.0.1 (c (n "ssh_host") (v "1.0.1") (d (list (d (n "whoami") (r "^0.6.0") (d #t) (k 0)))) (h "10xx07a0i0rgl3m6clgajyzpvmf8zp61394hqp784kk5c9qmspga")))

