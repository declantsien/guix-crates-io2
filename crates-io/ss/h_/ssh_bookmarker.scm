(define-module (crates-io ss h_ ssh_bookmarker) #:use-module (crates-io))

(define-public crate-ssh_bookmarker-1.0.0 (c (n "ssh_bookmarker") (v "1.0.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0l2p8vb9bx8paa4kq801ang19aswv44bhdd4z0jbzds51brz8qn7")))

