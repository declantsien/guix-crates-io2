(define-module (crates-io ss h_ ssh_cfg) #:use-module (crates-io))

(define-public crate-ssh_cfg-0.1.0 (c (n "ssh_cfg") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "1dg947p0ck569in2g5jcqcncb4a24z2q1v0n648aa2dwifaqfq9b")))

(define-public crate-ssh_cfg-0.1.1 (c (n "ssh_cfg") (v "0.1.1") (d (list (d (n "async-fs") (r "^1.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "1wmf20np99xb1s6sahay97h5cap16ivqqcczq9bmxfp9dfx1bvb7")))

(define-public crate-ssh_cfg-0.2.0 (c (n "ssh_cfg") (v "0.2.0") (d (list (d (n "async-fs") (r "^1.5.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "146s2wxjaqmqcrqjlnsny4lp4xsd2fqvn0x307v0xd9bra807wvx")))

(define-public crate-ssh_cfg-0.3.0 (c (n "ssh_cfg") (v "0.3.0") (d (list (d (n "async-fs") (r "^1.5.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "plain_path") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "0k01g495rhdmsa1xqm8srigq1bddwhkp2zvgbwpq5rb1i9z0w4rk")))

