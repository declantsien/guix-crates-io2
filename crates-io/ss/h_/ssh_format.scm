(define-module (crates-io ss h_ ssh_format) #:use-module (crates-io))

(define-public crate-ssh_format-0.4.7 (c (n "ssh_format") (v "0.4.7") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1z91by76dsv4jbv9vrrpr5x4km8r1bdj8pn4whsjcyg3r8pjp6gn") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.5.0 (c (n "ssh_format") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vyp04sgx1gdrphig6kkkbmk9xb0ffr95fakz7di2vwa6f4rf02f") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.5.1 (c (n "ssh_format") (v "0.5.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1jjznzqxp7561i7pmy9w7ib97wpf2mhhy6j1wnl3m4c4vx0kjhhy") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.6.0 (c (n "ssh_format") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "03517wcd80q2mrcb45wxprbjdvgwmr5yyfn8hdnnmk1zn9q2k8dv") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.6.1 (c (n "ssh_format") (v "0.6.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1s68893qwnydabd70vyiq6v52bm50vqc96yjw5fads8bk3dni05k") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.6.2 (c (n "ssh_format") (v "0.6.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1bi32gl2lb6gpbgj83jfcbdcg7xjymdvdsivds7d23ws77ms408p") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.7.0 (c (n "ssh_format") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0cq8ccrqfxg9pv904q7mxyqmb8831xqnkl5mp20gsi92yi289rp0") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.8.0 (c (n "ssh_format") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0b6d56h53y0rxmqc6acm6zn5xc4qxfqc5xayckk7ibl3wr5xnakb") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.9.0 (c (n "ssh_format") (v "0.9.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "128b0mi4zsa32q54700x4y7ndp5yq35cnwp3lwi9x4ysa8cbxf9d") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.9.1 (c (n "ssh_format") (v "0.9.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0in8czl2b292iwsnjr0nb1nzjzy8cqid38977qgf89rqrgvp6cip") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.9.2 (c (n "ssh_format") (v "0.9.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1pnqnbhwkzf8g6v0ikhm2m248zq6sd3yhr45lanlas68056q7z3c") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.10.0 (c (n "ssh_format") (v "0.10.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1amnqhfm61qppf24jpphd3755jlr0zyyx928knzxwh7mmwwrzr5d") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.11.0 (c (n "ssh_format") (v "0.11.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "034g8cagham3csbwq0y721l8nl2fg0lmq4433kx9drkj4wmxcfhw") (f (quote (("is_human_readable")))) (y #t)))

(define-public crate-ssh_format-0.12.0 (c (n "ssh_format") (v "0.12.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "10mlkw30f1d78izsippscj0b80phyw7md6jzi8xx8ri7hwwi4w5q") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.0 (c (n "ssh_format") (v "0.13.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "06c97s6gym91nrh7j0mq59l6lsbq5cgzbkqw1i0nw3wqz6kl5sw0") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.1 (c (n "ssh_format") (v "0.13.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0fpa41223rdvqlypbjg1d318p7l208fgj0ilr8vm2b256z21gk5l") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.2 (c (n "ssh_format") (v "0.13.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "08xz6lli00vh0cgz9rry819ikxwxq02xqwbrqbd6qq1qdwsc85jk") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.3 (c (n "ssh_format") (v "0.13.3") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1h8rqa63n64wil3hdq5ck66akaxxznr8kaijknwra30blwldkv5c") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.4 (c (n "ssh_format") (v "0.13.4") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1xxs1664cbhyiglszyjsaz72j2nrk8syxslxf8sa9h5k7mxbam14") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.13.5 (c (n "ssh_format") (v "0.13.5") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "10wi8psxzaz7drbms2xl6rxkjb08bpjvcl2ss9zb2i7p6gpqmi81") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.14.0 (c (n "ssh_format") (v "0.14.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "generator") (r "^0.7") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ssh_format_error") (r "^0.1") (d #t) (k 0)))) (h "142vd1fwqizsig1chsim96zhjbgdczby6nc7pkf1bfi227l72jcd") (f (quote (("is_human_readable"))))))

(define-public crate-ssh_format-0.14.1 (c (n "ssh_format") (v "0.14.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "generator") (r "^0.7") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ssh_format_error") (r "^0.1") (d #t) (k 0)))) (h "011k3zq5b13j9ppw2rmqyr5gnpybb045a8zc4z1rg40w3l433ar4") (f (quote (("is_human_readable"))))))

