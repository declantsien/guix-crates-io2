(define-module (crates-io ss hq sshq) #:use-module (crates-io))

(define-public crate-sshq-0.0.1 (c (n "sshq") (v "0.0.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0137avg82mg1mvcymq2appwlc3bpcdfylf7ylnx2arjwav1l2fgi")))

(define-public crate-sshq-0.0.2 (c (n "sshq") (v "0.0.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbx4zqynaxwiz4fpvkgz1nrqcqf96fdk1fii0li0rcr8car3ay8")))

(define-public crate-sshq-0.0.3 (c (n "sshq") (v "0.0.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "05rqjc1mxkppvfj2qzch4gwcaxszw6p2lcdr84a2626sc8bkq6hm")))

(define-public crate-sshq-0.1.0 (c (n "sshq") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "02lik9gi1mz92izk8zgy9fi6s9ifalahp0n2z3i43c75c4d1j2mi")))

(define-public crate-sshq-0.1.1 (c (n "sshq") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfnwdc2365kkjwjwhlzyi0iwarv2lqpz8a1v1ccg1q9h8b5dqa5")))

(define-public crate-sshq-0.1.2 (c (n "sshq") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "06qvpj0j07l1gp8ls8m0fk5a8dfbdcnddmk6aj9d499jnsjlh691")))

(define-public crate-sshq-0.1.3 (c (n "sshq") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "0mdwyj1c8kf3qn9h65l9ahic3q0jjwm6vv791hmh2rn98zswyd31")))

(define-public crate-sshq-0.1.4 (c (n "sshq") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "06h562jc6l9a8b4x8c5j27cqljdprrf38b3ygwrs3361dg2j7gmk")))

(define-public crate-sshq-0.1.5 (c (n "sshq") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "ssh_cfg") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "120q5179f869zky4y377nvj1sgx0hg7kq2365xnk6c4l7y5mvnaj")))

