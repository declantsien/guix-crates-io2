(define-module (crates-io ss hi sshive) #:use-module (crates-io))

(define-public crate-sshive-0.1.0-alpha.1 (c (n "sshive") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1m9isi0garxc1xahzs4n74qrrr62ci9c08wnv353fb510pdx505v") (y #t)))

(define-public crate-sshive-0.1.0-alpha.2 (c (n "sshive") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1d4l16xi06nm2bl50kg1x4hxx35zdbjvakxx4kvjr2qybpy1c0a2") (y #t)))

(define-public crate-sshive-0.1.0-alpha.3 (c (n "sshive") (v "0.1.0-alpha.3") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1alfi16iwmxfnlj7hyysa6z4nzaw8lx5y96cvwjvqdy4kr0krrhj") (y #t)))

(define-public crate-sshive-0.1.0-alpha.4 (c (n "sshive") (v "0.1.0-alpha.4") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1g02jd8r0hq6n7j4ybmypw0g7y5l85kc631rqqssx4jwps92h8h1") (y #t)))

