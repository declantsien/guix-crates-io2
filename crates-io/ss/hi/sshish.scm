(define-module (crates-io ss hi sshish) #:use-module (crates-io))

(define-public crate-sshish-0.1.0 (c (n "sshish") (v "0.1.0") (d (list (d (n "ssh2") (r "^0.4.0") (d #t) (k 0)))) (h "0sc895clq2gaffpbba5v7ypi69qvql321v2sb89d9ynmqr9yxqb4")))

(define-public crate-sshish-0.1.1 (c (n "sshish") (v "0.1.1") (d (list (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)))) (h "1y9752w6fsdh80jys7sjz9vfmsmcb2fzs9cihicx55j5k9spmjs7")))

