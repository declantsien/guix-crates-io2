(define-module (crates-io ss ml ssml) #:use-module (crates-io))

(define-public crate-ssml-0.0.1 (c (n "ssml") (v "0.0.1") (h "1bxdifbjhggzaqdnxikynbffzrlwxw60j0w0m35i1b6kh1j0vkfp")))

(define-public crate-ssml-0.0.2 (c (n "ssml") (v "0.0.2") (h "0xpgkpsv7smv53mky6y1v4hajdm0p9pii1n56m0ax58sj3hs17gj")))

(define-public crate-ssml-0.0.3 (c (n "ssml") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0nv73gkbm7pdam3kklbzhd4mbr1y9f8vb2c5r45m89mpmpdn0rjy")))

(define-public crate-ssml-0.0.4 (c (n "ssml") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "10adldr94i5x61b0rzpf6mjn7n7r47cqccyw0ramii6b28ckh45b")))

(define-public crate-ssml-0.0.5 (c (n "ssml") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "02jqq0rycqhrk6kbs33b4049b4s02s83vdjlh4sf76mmyhm8wcsc")))

(define-public crate-ssml-0.1.0 (c (n "ssml") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "0v7zbz1dfvgvw7rqnb2k03p1inhcwysi6yj0b2jvjzwhrksiqhyy")))

