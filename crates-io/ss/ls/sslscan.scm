(define-module (crates-io ss ls sslscan) #:use-module (crates-io))

(define-public crate-sslscan-1.0.0 (c (n "sslscan") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "19fihq0xm0xgf2s9xcvz78bsvhi6dbncv1aakwlzw6147nzb4rf0")))

