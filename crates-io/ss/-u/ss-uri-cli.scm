(define-module (crates-io ss -u ss-uri-cli) #:use-module (crates-io))

(define-public crate-ss-uri-cli-0.1.0 (c (n "ss-uri-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ss-uri") (r "^0.3.0") (d #t) (k 0)))) (h "11bj2i5cn13ky6lkvxmfisl82bmrkjgynawd4lj2c8zmjbpdh0mm")))

