(define-module (crates-io ss -u ss-uri) #:use-module (crates-io))

(define-public crate-ss-uri-0.1.0 (c (n "ss-uri") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1q3jjky57887am4kl9pq9bc7fcav55zpy72frzii2q01wg3ayqmv")))

(define-public crate-ss-uri-0.2.0 (c (n "ss-uri") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0y8ln7xbi4aqjscg6zkpy6r7h72ckxqbl9pj4lvrijanmqry81ni")))

(define-public crate-ss-uri-0.3.0 (c (n "ss-uri") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "13h5fbkybinypqi5v6cgmkhrykgsh3ln9zaw6nr9cdmrr0qd87f8")))

