(define-module (crates-io ss ea ssearch) #:use-module (crates-io))

(define-public crate-ssearch-0.1.0 (c (n "ssearch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1rk9ji3255lkmxfkylwqi4mwdhgqb5gmz5hhdj66sgfcdmns5jxm") (y #t)))

(define-public crate-ssearch-0.2.0 (c (n "ssearch") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "09zh15a792yhk5hzmafi1p13ab7cvfjhdrbr3ca6ljz34z6y7720")))

