(define-module (crates-io ss vm ssvm_tensorflow_interface) #:use-module (crates-io))

(define-public crate-ssvm_tensorflow_interface-0.1.0 (c (n "ssvm_tensorflow_interface") (v "0.1.0") (h "0nymc0z1rkigrd9rm6i9bl414i8m53dk6040pjqsyx9k95nqwdwa")))

(define-public crate-ssvm_tensorflow_interface-0.1.1 (c (n "ssvm_tensorflow_interface") (v "0.1.1") (h "1xf3sxnifibwmqzw9qd7qh9ga6bqd8z45w21rnf8zd9i5qigkms2")))

(define-public crate-ssvm_tensorflow_interface-0.1.2 (c (n "ssvm_tensorflow_interface") (v "0.1.2") (h "0cfzaxfmvy0s21xwja8h9xn4jbf23azfxjizk75dbqpa4jra2fal")))

(define-public crate-ssvm_tensorflow_interface-0.1.3 (c (n "ssvm_tensorflow_interface") (v "0.1.3") (h "1x0gyb8yfvaqklqjxpc1mvaxgz44z7f3k3ixgjiwbfp3syhmjbbr")))

(define-public crate-ssvm_tensorflow_interface-0.1.4 (c (n "ssvm_tensorflow_interface") (v "0.1.4") (h "14rgzmhypzsw1fzig32y027innd4f7a41m4m7xn78v5sxky328fd")))

