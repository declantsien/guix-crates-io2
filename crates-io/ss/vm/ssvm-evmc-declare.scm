(define-module (crates-io ss vm ssvm-evmc-declare) #:use-module (crates-io))

(define-public crate-ssvm-evmc-declare-6.3.1-rc3 (c (n "ssvm-evmc-declare") (v "6.3.1-rc3") (d (list (d (n "evmc-vm") (r "^6.3.1-rc3") (d #t) (k 0) (p "ssvm-evmc-vm")) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "12h1xzw00s4hcylxa9j84f5hmif7a6dqbayd4b23q6arbp3ki238")))

