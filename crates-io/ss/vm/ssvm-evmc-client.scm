(define-module (crates-io ss vm ssvm-evmc-client) #:use-module (crates-io))

(define-public crate-ssvm-evmc-client-7.4.0 (c (n "ssvm-evmc-client") (v "7.4.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "evmc-sys") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "evmc-vm") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-vm")) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0y9460dwrqa9h2djhynl0i4khy91bhj3x5ydvffzy5f89n5dz9gn") (y #t)))

(define-public crate-ssvm-evmc-client-7.4.1 (c (n "ssvm-evmc-client") (v "7.4.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "evmc-sys") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "evmc-vm") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-vm")) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0sn65jvcq7b6dkaan6gf047r8a0a7yyhmykqpl0asfwcg1wylxpd") (y #t)))

(define-public crate-ssvm-evmc-client-6.3.1 (c (n "ssvm-evmc-client") (v "6.3.1") (d (list (d (n "evmc-sys") (r "^6.3.1") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "12zyvy34x8dwvq09krabshnwpgi41q94i0zrc9qv8k2y37d4k175") (y #t)))

(define-public crate-ssvm-evmc-client-6.3.1-rc1 (c (n "ssvm-evmc-client") (v "6.3.1-rc1") (d (list (d (n "evmc-sys") (r "^6.3.1-rc1") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0sda63cj4zccf78g7qyhjzkdrqgcairfbpv1ax7qihk0jxrgwr6j") (y #t)))

(define-public crate-ssvm-evmc-client-7.4.0-rc1 (c (n "ssvm-evmc-client") (v "7.4.0-rc1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "evmc-sys") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "evmc-vm") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-vm")) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "01za6dc51d8f62my6kp9bl3cl1k9v2k89v6kl2dkpyi8s2xn4ylb") (y #t)))

(define-public crate-ssvm-evmc-client-6.3.1-rc3 (c (n "ssvm-evmc-client") (v "6.3.1-rc3") (d (list (d (n "evmc-sys") (r "^6.3.1-rc3") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0c3773adxc7qvi2cnlwlhl7v65izivhdhk33fbx87mrs9p65w2dz") (y #t)))

(define-public crate-ssvm-evmc-client-7.4.0-rc2 (c (n "ssvm-evmc-client") (v "7.4.0-rc2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "evmc-sys") (r "^7.4.0-rc2") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "evmc-vm") (r "^7.4.0-rc2") (d #t) (k 0) (p "ssvm-evmc-vm")) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0kw7whf6disbymp1w9slw3hfm6rimj49kl0c9v8d7f2j9whw92n0")))

(define-public crate-ssvm-evmc-client-6.3.1-rc4 (c (n "ssvm-evmc-client") (v "6.3.1-rc4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "evmc-sys") (r "^6.3.1-rc4") (d #t) (k 0) (p "ssvm-evmc-sys")) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1n5km1dixb95myjyhn20fsjksvjykarna314ka22c71rfmrnpcjv")))

