(define-module (crates-io ss vm ssvm_storage_interface) #:use-module (crates-io))

(define-public crate-ssvm_storage_interface-0.1.29 (c (n "ssvm_storage_interface") (v "0.1.29") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "03diq6p4a7v8ysy0hrc9n5f2cf45c1zgdz6r9p0c0wqqvxfhxa2v")))

(define-public crate-ssvm_storage_interface-0.1.30 (c (n "ssvm_storage_interface") (v "0.1.30") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "150kgn6d0hw3mvxqy8jw5rlh731mh4gwlvppp33wkq1amj2wnamf")))

