(define-module (crates-io ss vm ssvm_container) #:use-module (crates-io))

(define-public crate-ssvm_container-0.1.0 (c (n "ssvm_container") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "leveldb") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1aldczl14x5b8rlw39107qyq3b8la09aj1wqz9l9513p3g272g35")))

(define-public crate-ssvm_container-0.1.1 (c (n "ssvm_container") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "leveldb") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "16sy6b4q2gjjs70p96k1qbgvgrpqiiq9154am74s516ls4yn5im8")))

(define-public crate-ssvm_container-0.1.2 (c (n "ssvm_container") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "leveldb") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vwwnnc69g1kl9ahhy1b5xijp2zcfhmrwyizlwr0m5ryn4lq5jam")))

(define-public crate-ssvm_container-0.1.3 (c (n "ssvm_container") (v "0.1.3") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cmq88fhclmjfgczm8ldbz90ks0c4j78841cjchibqjkm4syyjcd")))

