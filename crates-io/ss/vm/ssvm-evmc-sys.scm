(define-module (crates-io ss vm ssvm-evmc-sys) #:use-module (crates-io))

(define-public crate-ssvm-evmc-sys-7.4.0 (c (n "ssvm-evmc-sys") (v "7.4.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "00sv4hp0hzanhi93yx00v40qx8cwnrck1qp3l5k2jbl34pqwppzf") (y #t)))

(define-public crate-ssvm-evmc-sys-6.3.1 (c (n "ssvm-evmc-sys") (v "6.3.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "1q42m31vz49d0aqc0sy7mizkr5i61vzzd05fiy2imwlrwr8nph45") (y #t)))

(define-public crate-ssvm-evmc-sys-6.3.1-rc1 (c (n "ssvm-evmc-sys") (v "6.3.1-rc1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "0lppls4pflriv4b8h1hlkzfxvh2syy9v7h06qai2haxy425q3pq2") (y #t)))

(define-public crate-ssvm-evmc-sys-6.3.1-rc3 (c (n "ssvm-evmc-sys") (v "6.3.1-rc3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "0jihbkswpq2s8chs0v2dnjd00jkhdjzmlzmyh8dss20wwgn43mfb") (y #t)))

(define-public crate-ssvm-evmc-sys-6.3.1-rc4 (c (n "ssvm-evmc-sys") (v "6.3.1-rc4") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "05ax9cd6xmy1awydi5r7rnyc7ck2c88y9wfjs1cikn8g326k5r6f")))

(define-public crate-ssvm-evmc-sys-7.4.0-rc2 (c (n "ssvm-evmc-sys") (v "7.4.0-rc2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "1shw8cz8wxlyhnxa0afg1kkb55iw5d2ycl5i7507pzjj8li5c8ig")))

