(define-module (crates-io ss vm ssvm-evmc-vm) #:use-module (crates-io))

(define-public crate-ssvm-evmc-vm-7.4.0 (c (n "ssvm-evmc-vm") (v "7.4.0") (d (list (d (n "evmc-sys") (r "^7.4.0") (d #t) (k 0) (p "ssvm-evmc-sys")))) (h "1hi94n1j0fjnaa561pj4rgny25qqjckclsrb2bqfrbhqq4giqspy") (y #t)))

(define-public crate-ssvm-evmc-vm-6.3.1-rc3 (c (n "ssvm-evmc-vm") (v "6.3.1-rc3") (d (list (d (n "evmc-sys") (r "^6.3.1-rc3") (d #t) (k 0) (p "ssvm-evmc-sys")))) (h "0a5kx0nla1irckliz4bn1qqazk01ppa9ywyxfbqdm8f7sp4y1ivn") (y #t)))

