(define-module (crates-io ss vm ssvm-wasi-helper) #:use-module (crates-io))

(define-public crate-ssvm-wasi-helper-0.1.0 (c (n "ssvm-wasi-helper") (v "0.1.0") (h "0zwnbm32vx99xd2xjwfw920gnawfvf8hd16p1zdnh0ifpgii1mck")))

(define-public crate-ssvm-wasi-helper-0.1.1 (c (n "ssvm-wasi-helper") (v "0.1.1") (h "10zky01xy1jc38j8fgrzcq7j5g17gvgj1bq6klcsgys9659pxzzd")))

(define-public crate-ssvm-wasi-helper-0.1.2 (c (n "ssvm-wasi-helper") (v "0.1.2") (h "0fgfkgwx64zabryc5d74cjmlqjg09sm1rj9f8zgs4rbgmnk8qz8y")))

(define-public crate-ssvm-wasi-helper-0.1.3 (c (n "ssvm-wasi-helper") (v "0.1.3") (h "0ql0sms93kklasyymkpz1y0qmaqgf1g3v5vx7fwr52kc3w2l76jf")))

