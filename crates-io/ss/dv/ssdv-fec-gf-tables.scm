(define-module (crates-io ss dv ssdv-fec-gf-tables) #:use-module (crates-io))

(define-public crate-ssdv-fec-gf-tables-0.1.0 (c (n "ssdv-fec-gf-tables") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h84k7ra4yrwzm2lki7vm3g3i91f4rai0vlc433qaafbj570hvbg")))

