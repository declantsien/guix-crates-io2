(define-module (crates-io ss dv ssdv-fec) #:use-module (crates-io))

(define-public crate-ssdv-fec-0.1.0 (c (n "ssdv-fec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ssdv-fec-gf-tables") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0rhaigds1xcrx2wpn712p254bb6flxhdz3sy5yd1sa0f3zhq4ymn") (f (quote (("std" "thiserror") ("default" "cli" "std") ("cli" "anyhow" "clap" "std"))))))

