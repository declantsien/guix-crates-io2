(define-module (crates-io ss de ssdeep) #:use-module (crates-io))

(define-public crate-ssdeep-0.1.0 (c (n "ssdeep") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.1.0") (d #t) (k 0)))) (h "09jyzhfc167d7h7vdwmadm8s93jy15nfpl7kcgjmw7w2c0jcz489")))

(define-public crate-ssdeep-0.2.0 (c (n "ssdeep") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.2.0") (d #t) (k 0)))) (h "16arrd350llfv66k7wpgsnxp4n2f3i75zxln9paip89dsl9337vv")))

(define-public crate-ssdeep-0.3.0 (c (n "ssdeep") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0igd8yak3wr3fdxfsblw77rpa8zs19n8c3xqg2k8z9xbaccla60d")))

(define-public crate-ssdeep-0.4.0 (c (n "ssdeep") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1h33iiyyhhpgzpw40pdfhjx7wiyis57nwhsmnm1k31vzc08qjb4p")))

(define-public crate-ssdeep-0.5.0 (c (n "ssdeep") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0p4jsxcy03wih738hwizjss6gi41i0xh28fqxyyschm66c4k65qp")))

(define-public crate-ssdeep-0.6.0 (c (n "ssdeep") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzy-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0ll7k3hd85akxxp8y70rmjrjlxxh5yj6pbw6dl0q7dmaaz9jj45w")))

