(define-module (crates-io ss hc sshc) #:use-module (crates-io))

(define-public crate-sshc-1.0.0 (c (n "sshc") (v "1.0.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "cursive") (r "^0.6") (f (quote ("termion-backend"))) (k 0)) (d (n "either") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "136d9bg1fnwzhrgsj0hdsq98x3ly7h0nfibrhny0rgsdzrz4c4xm")))

