(define-module (crates-io ss sd sssd) #:use-module (crates-io))

(define-public crate-sssd-1.0.0 (c (n "sssd") (v "1.0.0") (d (list (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "1ck7mcy1gb1wdim262gxq7bdfcv8nv9jg1ldz4f3c4j3c1c63m7l")))

(define-public crate-sssd-1.0.1 (c (n "sssd") (v "1.0.1") (d (list (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "07m9gyagjcjn90618lfvh345xyjj4m09f5fhqbr773a5bl5w5q27")))

