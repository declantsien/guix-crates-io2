(define-module (crates-io ss h2 ssh2) #:use-module (crates-io))

(define-public crate-ssh2-0.0.1 (c (n "ssh2") (v "0.0.1") (d (list (d (n "libssh2-sys") (r "*") (d #t) (k 0)))) (h "17vdhvw8rwgp397kpy0ay1vbg4d1may8kmhfwc8nhc8cmghybmjn") (y #t)))

(define-public crate-ssh2-0.0.2 (c (n "ssh2") (v "0.0.2") (d (list (d (n "libssh2-sys") (r "*") (d #t) (k 0)))) (h "0xi6czk6k8p5lwyig4q2mg0kvl9ansiv70wkwrprsz0xx1mccpys") (y #t)))

(define-public crate-ssh2-0.1.0 (c (n "ssh2") (v "0.1.0") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "03pwp870jjkk59amrblzp7dp0yj1q7158vnmdz0qa9wcbhjcrcf8")))

(define-public crate-ssh2-0.1.1 (c (n "ssh2") (v "0.1.1") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0wmwcp9bibqjasv9ajn6a1kld2jnks56qkngv4f26290cdzhbw4s")))

(define-public crate-ssh2-0.1.2 (c (n "ssh2") (v "0.1.2") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1xcppfivk710pf0px05s5apfrkkwx8hpcadgz1ip43bzwsfqnx22")))

(define-public crate-ssh2-0.1.3 (c (n "ssh2") (v "0.1.3") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1wpvysksxw54g3npjk38wnrp6y3ihygd3n0c50csgja8p00ylph5")))

(define-public crate-ssh2-0.1.4 (c (n "ssh2") (v "0.1.4") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0hvc11rspqn1a7fsmn2813fl2k33ppj35ifcwy6yp6kkfarzb07l")))

(define-public crate-ssh2-0.1.5 (c (n "ssh2") (v "0.1.5") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1fbyshg5ri7xvjysvcw9i22gvvnxw5rv5pnkkh44lfaq360n6p65")))

(define-public crate-ssh2-0.1.6 (c (n "ssh2") (v "0.1.6") (d (list (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1093qbhl9lmwrs2gijh2v7l729fknxh0bcimyfmal18yy2r91wdx")))

(define-public crate-ssh2-0.1.7 (c (n "ssh2") (v "0.1.7") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0ignly6p5q20l7qhzx5fhalk7zc0bcfm7fz7xw6w2nb5mslbngcz")))

(define-public crate-ssh2-0.1.8 (c (n "ssh2") (v "0.1.8") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1vrdj7b8dphfqjdzygh2yqnlbd7jaljj052jbla4ywqqb2277bxn")))

(define-public crate-ssh2-0.1.9 (c (n "ssh2") (v "0.1.9") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "18f5wimncm30w29vi209xygzhxc8wx0ym9f8x7j8w034i9nm4xn2")))

(define-public crate-ssh2-0.1.10 (c (n "ssh2") (v "0.1.10") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1993jrmnpilk2nqj9jmq7czvczqhravi5bz9zbwc2fcpq8f1sy41")))

(define-public crate-ssh2-0.1.11 (c (n "ssh2") (v "0.1.11") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "10868dva8wd2gc5dyp96ywgdzxfbp749xnxdjy57afdgqmx8d2b3")))

(define-public crate-ssh2-0.2.0 (c (n "ssh2") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1zi94yv1f4raz6ninb3h12lfb9qcmgh40fnpjsqd22wljf256zn1")))

(define-public crate-ssh2-0.2.1 (c (n "ssh2") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1wm11sjwzqmhf7wv63an0gjhjzhn02w8c9mbzr097h644ngl8irf")))

(define-public crate-ssh2-0.2.2 (c (n "ssh2") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0s7469ll0mfpjbb2l9rcdsw31dp16ss6vnib41zp7hd2qm5l987k")))

(define-public crate-ssh2-0.2.3 (c (n "ssh2") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1938jcdbhpybbwxhvnk4ad5xya9scv11cb9s92qr61s9apbils59")))

(define-public crate-ssh2-0.2.4 (c (n "ssh2") (v "0.2.4") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1pb4bdz00y1052qqyv0mxdgacyxkz61r0lzrag0i1xi4znk1lm1j")))

(define-public crate-ssh2-0.2.5 (c (n "ssh2") (v "0.2.5") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1k82d9xyy7kaxr55wbgbgnms6w3gmbcz3fs9mi0z6sqpw6q2psfg")))

(define-public crate-ssh2-0.2.6 (c (n "ssh2") (v "0.2.6") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0j3hijibnnhzg39ip3bsn8mhkia1jxj8zx1f4fdh6m7wv1ca63gi")))

(define-public crate-ssh2-0.2.7 (c (n "ssh2") (v "0.2.7") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0xzbx3ddvq1xw79xjg08qxy1chnaxk44gliqqmfshpihsddp16vl")))

(define-public crate-ssh2-0.2.8 (c (n "ssh2") (v "0.2.8") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0z2rr1p1cdw3411kn0xgkylx7vv6jqg3mwgnni1dvj22q7givdcz")))

(define-public crate-ssh2-0.2.9 (c (n "ssh2") (v "0.2.9") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15zlyfslm139n1qkax1z3n5izqgyjzclp8jzipdq1blxz6vqhcb5")))

(define-public crate-ssh2-0.2.10 (c (n "ssh2") (v "0.2.10") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "173wzz7j41762gd6siy61h05nxflkvw9k7hrxbhm55s6s18ncmiy")))

(define-public crate-ssh2-0.2.11 (c (n "ssh2") (v "0.2.11") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rs5pk3azayzqhnkj2wxc87sij1sriii610ar7q7ff64prc122cf")))

(define-public crate-ssh2-0.2.12 (c (n "ssh2") (v "0.2.12") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1bnh698jkpn2fad226651i1vd1whvz35rx4cny3hl09wp2m90vf1")))

(define-public crate-ssh2-0.2.13 (c (n "ssh2") (v "0.2.13") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.38") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0wglxvmjhlgpqm3z0ds3lw9fr8lakfc7gc28bcnsibqscsv6glf1")))

(define-public crate-ssh2-0.2.14 (c (n "ssh2") (v "0.2.14") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.40") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0lals9rdk7dx1sf5vlrs88jfpkn2s6cd5jn6k1ln1mjhxfirvgdv") (y #t)))

(define-public crate-ssh2-0.2.15 (c (n "ssh2") (v "0.2.15") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.1.41") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1jb8i92ninz6bvccrywmw9zsllndmvw4dqh6cl3rlq8g5m74s2xq")))

(define-public crate-ssh2-0.3.0 (c (n "ssh2") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1nlx46glhp4mkbbd1ckdwv3c87ydb9zjhmjpzfl7ik4b2cpnssb5")))

(define-public crate-ssh2-0.3.1 (c (n "ssh2") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nk884vl50pfmws2zs9rkq0cq40zbqbhck4yaziwm2w5y1yrmqzs")))

(define-public crate-ssh2-0.3.2 (c (n "ssh2") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16rd0cfbwpfb3jgmmsfqnn7rvv9hdh10w3ygzwij7jr9zcx1imc5")))

(define-public crate-ssh2-0.3.3 (c (n "ssh2") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "031s0fpg87dn8zgv5ki4x7r7rk1f4wgr7dg39n6gj05737b25s6y")))

(define-public crate-ssh2-0.4.0 (c (n "ssh2") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ap86akspb6czpfy99p0qnvvyblwvn8l2zr4d8q9w121zi7wfpw7") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.5.0 (c (n "ssh2") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.13") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0fr5vm4q56lcy9ydfqgpdyv82ph28vxrsyhh1glyp8kig47cqghx") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.6.0 (c (n "ssh2") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.13") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ar2a3hzh8bfyvpnppv7vpsjhhy4cjpxaiqji4aczir68mcmxqx5") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.7.0 (c (n "ssh2") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12qag673nk63b8nd0kln6i8czcarfg3dgykl6qm66kq8if18p7wb") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.7.1 (c (n "ssh2") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0g1y4pl8pcxj32cdgjmq7hjdfqn97v51nxvjnwgpk60izy0ighr6") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.8.0 (c (n "ssh2") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.15") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "10mlfynkk73f9ygal9904va4kayxrw4pjz7nh3ms27vynkghn6pb") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.8.1 (c (n "ssh2") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1aarjhqckqqxni4qv0nwbx6saq5h3rc6zgikzl2ddkzsbpxq6cfx") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.8.2 (c (n "ssh2") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0q247qhlhxjx9wcfj0z2g8djxslggzjzpsrin6y9b8mrv90xfmms") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.8.3 (c (n "ssh2") (v "0.8.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1mbaprp5n3i9w4h4x12dzcf12a8l3rn40671lvdyakdqpfb6jlcc") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.9.0 (c (n "ssh2") (v "0.9.0") (d (list (d (n "bitflags") (r ">=1.2.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "libssh2-sys") (r ">=0.2.20, <0.3.0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.10.0, <0.11.0") (d #t) (k 0)) (d (n "tempdir") (r ">=0.3.0, <0.4.0") (d #t) (k 2)))) (h "11c94jfgi51z6cbgnv1ax8bmb734lpkladqa13ilz576lph4s0pd") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.9.1 (c (n "ssh2") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.20") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ms4g0jpplm699q96dkd8hd807s6b4vwazizsi2j5gvbgzax8xnq") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.9.2 (c (n "ssh2") (v "0.9.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.22") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16pi6xqgqqqqpan1q5bgk91kwiwj947cdj28g1qpky80l30cndgs") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl"))))))

(define-public crate-ssh2-0.9.3 (c (n "ssh2") (v "0.9.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.2.23") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cgfwarj5k8s9fbf1pcnp27ifk30xk2f7q3sjca7l1ih8kk474r6") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl") ("openssl-on-win32" "libssh2-sys/openssl-on-win32"))))))

(define-public crate-ssh2-0.9.4 (c (n "ssh2") (v "0.9.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0malilb9lbzlh3i24nb6s61lmz5as801swy30ib6v7sm20cldzp7") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl") ("openssl-on-win32" "libssh2-sys/openssl-on-win32"))))))

