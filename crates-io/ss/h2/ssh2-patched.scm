(define-module (crates-io ss h2 ssh2-patched) #:use-module (crates-io))

(define-public crate-ssh2-patched-0.9.4-p1 (c (n "ssh2-patched") (v "0.9.4-p1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh2-sys") (r "^0.3.0-p1") (d #t) (k 0) (p "libssh2-sys-patched")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1wq4f2b2r2dvzz53rcdq7xw00zdicsr1l4ka52v84c220dpz4z8f") (f (quote (("vendored-openssl" "libssh2-sys/vendored-openssl") ("openssl-on-win32" "libssh2-sys/openssl-on-win32"))))))

