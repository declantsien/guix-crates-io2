(define-module (crates-io ss z- ssz-derive) #:use-module (crates-io))

(define-public crate-ssz-derive-0.1.0 (c (n "ssz-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "ssz") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "12v0mn3804jfphb68gaxmvjqb0l0czwf0pfnz19s2ckbsn4q6b6g")))

(define-public crate-ssz-derive-0.1.1 (c (n "ssz-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "ssz") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zjfwlidf8074m7z9qhiqm1zm27wp12259nla4x7f9fa1yrd5vny")))

(define-public crate-ssz-derive-0.1.2 (c (n "ssz-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "ssz") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "13bflhh5jkj7sp8fr2y99xlm81ki8nqhxsk9ah64prlgys1cbsbx")))

(define-public crate-ssz-derive-0.2.0 (c (n "ssz-derive") (v "0.2.0") (d (list (d (n "deriving") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "ssz") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 2)))) (h "1iywylns4favgjq01acd95jvw3yv2adalvyygaqaryqhbdx6avp9")))

