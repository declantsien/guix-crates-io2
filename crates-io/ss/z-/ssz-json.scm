(define-module (crates-io ss z- ssz-json) #:use-module (crates-io))

(define-public crate-ssz-json-0.0.2 (c (n "ssz-json") (v "0.0.2") (d (list (d (n "ethereum-types") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "ssz") (r "^0.1.2") (d #t) (k 0)) (d (n "ssz-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1jzfrxizv1k0555n3b234plzafk5d7n8z19wvfbzdxbia9ahm94d")))

