(define-module (crates-io ss z- ssz-cli) #:use-module (crates-io))

(define-public crate-ssz-cli-0.1.0 (c (n "ssz-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ssz") (r "^0.1.2") (d #t) (k 0)))) (h "06fkkxggfpq4j9la52qhjb535dswvj3d1cm6p4jfbl23w61cmkm3")))

(define-public crate-ssz-cli-0.1.1 (c (n "ssz-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ssz") (r "^0.1.2") (d #t) (k 0)))) (h "17bhxw808c2p77m7857zxi8jm1whgmzpyd2bhx9i4wj29vfr371h")))

(define-public crate-ssz-cli-0.2.0 (c (n "ssz-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ssz-json") (r "^0.0.2") (d #t) (k 0)))) (h "0k04bh4akycxlj4wcrh09h83rn81grmn6b7svzfy4jzqv5lvw31y")))

