(define-module (crates-io ss ah ssahay-test-crate) #:use-module (crates-io))

(define-public crate-ssahay-test-crate-0.1.0 (c (n "ssahay-test-crate") (v "0.1.0") (h "1r8yw7kyra36h8l3vxsh9vw5k00xynmmbg0bi7143x4vjf4bzmc3")))

(define-public crate-ssahay-test-crate-0.1.1 (c (n "ssahay-test-crate") (v "0.1.1") (h "0jnzfynkkqg9nfsiyjv76l5ai87cfpb9zlkidhg09w7xqw1612hh")))

(define-public crate-ssahay-test-crate-0.1.9 (c (n "ssahay-test-crate") (v "0.1.9") (h "0jilzjp1s09b6w59aalsrb9fvakkq2n4bgf1i9f9g2viqjl65qy0")))

(define-public crate-ssahay-test-crate-0.1.5 (c (n "ssahay-test-crate") (v "0.1.5") (h "12prsf4vyxpq5v484w6krb1d6ibsfqsfw3h05gncz97q34xz0ss2")))

