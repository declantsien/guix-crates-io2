(define-module (crates-io ss hh sshh) #:use-module (crates-io))

(define-public crate-sshh-0.1.0 (c (n "sshh") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0n2zfbx503kx78g2y67gdzypq1ndia1gfvzn5ss5sar1wk64xnlh")))

(define-public crate-sshh-0.1.2 (c (n "sshh") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0qp4s2k1kfvi4gkwadp0ndbmgghwfh4j7y5x7kqxp4sydycf4yl0")))

(define-public crate-sshh-0.1.3 (c (n "sshh") (v "0.1.3") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03hjdq3k84dsn6c63ppxnqv238613kgb46hfk4812kfnxbvfr7j6")))

