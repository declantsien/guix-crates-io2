(define-module (crates-io ss i- ssi-tzkey) #:use-module (crates-io))

(define-public crate-ssi-tzkey-0.1.0 (c (n "ssi-tzkey") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ssi-jwk") (r "^0.1") (f (quote ("tezos"))) (k 0)) (d (n "ssi-jws") (r "^0.1") (f (quote ("tezos"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08x84ymyr2w2kjh9jkckci9mbzq87dgvr3cs78ji9li0ks8pvdf1")))

(define-public crate-ssi-tzkey-0.1.1 (c (n "ssi-tzkey") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ssi-jwk") (r "^0.1") (f (quote ("tezos"))) (k 0)) (d (n "ssi-jws") (r "^0.1") (f (quote ("tezos"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02s8znkicq2kj6qzblq9x2v0k867d7crjl701cab2j87lfgnm1zy")))

