(define-module (crates-io ss i- ssi-caips) #:use-module (crates-io))

(define-public crate-ssi-caips-0.1.0 (c (n "ssi-caips") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bech32") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "bs58") (r "^0.4") (f (quote ("check"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ssi-jwk") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07v6cfyd0in3rrpgbndf5i089mfpp4d5p6i7w3nfnnwmj2b4fb6s") (f (quote (("tezos" "ssi-jwk/tezos") ("ripemd-160" "ssi-jwk/ripemd-160") ("eip" "ssi-jwk/eip") ("default" "eip" "ripemd-160" "ssi-jwk/secp256k1") ("aleo" "ssi-jwk/aleo" "bech32"))))))

