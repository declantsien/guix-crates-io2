(define-module (crates-io ss i- ssi-contexts) #:use-module (crates-io))

(define-public crate-ssi-contexts-0.0.1 (c (n "ssi-contexts") (v "0.0.1") (h "1mdiskjhza3xamn3ys295kmlp5yb626wvc6cq2446a33nv4qvbs7")))

(define-public crate-ssi-contexts-0.0.2 (c (n "ssi-contexts") (v "0.0.2") (h "1xpir85q2qg2b6rajad36nih1ssfad98k044cflgl9n9prhm7xrq")))

(define-public crate-ssi-contexts-0.1.0 (c (n "ssi-contexts") (v "0.1.0") (h "1yhl15mvv09wf5cgkhjsw385k8bij469h0bbza37kdz064y0g4pf")))

(define-public crate-ssi-contexts-0.1.1 (c (n "ssi-contexts") (v "0.1.1") (h "0avk76sdymyw96zcgf6q3cf00rj8hhz8khyn67ic1308zjrsm82b")))

(define-public crate-ssi-contexts-0.1.2 (c (n "ssi-contexts") (v "0.1.2") (h "137r6k1m3mciw321pkf7fpfaykcq4yv01c6adxrqc6jp5fbf7niv")))

(define-public crate-ssi-contexts-0.1.3 (c (n "ssi-contexts") (v "0.1.3") (h "14xi176insl1y2i0ixq34k4i3j45lmlh7cq97kpdbz5iq92wriap")))

(define-public crate-ssi-contexts-0.1.4 (c (n "ssi-contexts") (v "0.1.4") (h "10mq0bjjxn619m25h9idmia8v4vpxm68fscdz946kdiagqhy4v98")))

(define-public crate-ssi-contexts-0.1.5 (c (n "ssi-contexts") (v "0.1.5") (h "0r3igz2s7ykzj739lddh7rks2c1zzfshyihmdykqhgdfrf19n07k")))

