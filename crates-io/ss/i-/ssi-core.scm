(define-module (crates-io ss i- ssi-core) #:use-module (crates-io))

(define-public crate-ssi-core-0.1.0 (c (n "ssi-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j1ha6rrfd2396j4z3mkjzzbppmk5nrd5xz8xbjw63dq2qhg8hsy")))

