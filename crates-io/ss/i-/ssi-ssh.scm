(define-module (crates-io ss i- ssi-ssh) #:use-module (crates-io))

(define-public crate-ssi-ssh-0.1.0 (c (n "ssi-ssh") (v "0.1.0") (d (list (d (n "sshkeys") (r "^0.3") (d #t) (k 0)) (d (n "ssi-jwk") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0636sl38i183gifark3p1wja6vhv79hvzpms2cl7lig2h9w7r5i2") (f (quote (("secp256r1" "ssi-jwk/secp256r1") ("default" "secp256r1"))))))

