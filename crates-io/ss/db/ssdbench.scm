(define-module (crates-io ss db ssdbench) #:use-module (crates-io))

(define-public crate-ssdbench-0.1.0 (c (n "ssdbench") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "fstd") (r "^0.1.2") (d #t) (k 0)))) (h "0ba5l8dpfvj35v437vlydgr31g7bd6m2yw28gjm9cx19abgddqil")))

