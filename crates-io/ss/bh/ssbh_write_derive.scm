(define-module (crates-io ss bh ssbh_write_derive) #:use-module (crates-io))

(define-public crate-ssbh_write_derive-0.5.0 (c (n "ssbh_write_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z55x9hjzixbxdh453qpx2h763znzz3yv025dclb4pp458p2lfw7")))

(define-public crate-ssbh_write_derive-0.6.0 (c (n "ssbh_write_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pm5mmjb64xixmbwmmbl005jcg5m7pyhpxdvxqbd8zhw2rdy02zh")))

(define-public crate-ssbh_write_derive-0.7.0 (c (n "ssbh_write_derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01pnx6lvxppwpkv282w9pimdna4hwdrzb9vksxrnpxicbnag3awz")))

(define-public crate-ssbh_write_derive-0.10.1 (c (n "ssbh_write_derive") (v "0.10.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14ldfbvyyvxqas8myzan6q576fsa99215wbnlz6skpynb1c9lmjc")))

(define-public crate-ssbh_write_derive-0.11.0 (c (n "ssbh_write_derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0chx494lbix1whfqs88b0ja0njmgakfmk8l5md6407540nazammn")))

(define-public crate-ssbh_write_derive-0.12.0 (c (n "ssbh_write_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10pw4m4d367jppbld5nwwilcw3nyqy8s7b1mlcdjk3p118qfvjxg")))

(define-public crate-ssbh_write_derive-0.13.0 (c (n "ssbh_write_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05jpr928mcws4hp3bw7s4sk2lixw14dwrdwx65banrwcpgfblkaw")))

(define-public crate-ssbh_write_derive-0.14.0 (c (n "ssbh_write_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hsmp07nrh1zkh549qf4dvchgrz5d340vccfwr8kjdplwchhn18z")))

(define-public crate-ssbh_write_derive-0.15.0 (c (n "ssbh_write_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1aziy8fqj5xjwg7xm6lf6p7zjcqmv49jf5lbh21nhx5dvlzm9kwr")))

(define-public crate-ssbh_write_derive-0.16.0 (c (n "ssbh_write_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "145pnqy938mfb838azr6w4fzwhqkx3gjs46l0152vajv4pm4isrh")))

(define-public crate-ssbh_write_derive-0.17.0 (c (n "ssbh_write_derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01n1kwlzzsji9rspdmdllfphppj6al133snrr5ki1dmyj7ni9psx")))

(define-public crate-ssbh_write_derive-0.18.0 (c (n "ssbh_write_derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02vs48caa3lkdlmjkrl67l566zy0hd0553xf43qhgyv8mjma3zh4")))

