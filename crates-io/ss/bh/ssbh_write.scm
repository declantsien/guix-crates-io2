(define-module (crates-io ss bh ssbh_write) #:use-module (crates-io))

(define-public crate-ssbh_write-0.11.0 (c (n "ssbh_write") (v "0.11.0") (d (list (d (n "ssbh_write_derive") (r "^0.11") (d #t) (k 0)))) (h "0ngzd2siwhbx4cz87cg8l4bpgwy36lppr6hdf9js8k1lvprhr8lv")))

(define-public crate-ssbh_write-0.12.0 (c (n "ssbh_write") (v "0.12.0") (d (list (d (n "ssbh_write_derive") (r "^0.12") (d #t) (k 0)))) (h "0x2z01ibbz5npgvffd0ajyri0ljkj1q8r29d5150mvs6717jv41f")))

(define-public crate-ssbh_write-0.13.0 (c (n "ssbh_write") (v "0.13.0") (d (list (d (n "ssbh_write_derive") (r "^0.13") (d #t) (k 0)))) (h "03rp20236cazf1nrghmf16n47m1llwc3b6mvby9lfba94z7fg2pd")))

(define-public crate-ssbh_write-0.14.0 (c (n "ssbh_write") (v "0.14.0") (d (list (d (n "ssbh_write_derive") (r "^0.14") (d #t) (k 0)))) (h "18wmh7ips1skf5dswxybl0k57hnhfx2bsjwc972gl715g1bmhn0v")))

(define-public crate-ssbh_write-0.15.0 (c (n "ssbh_write") (v "0.15.0") (d (list (d (n "ssbh_write_derive") (r "^0.14") (d #t) (k 0)))) (h "1rd6cckfpqp54i8x6yckkzwr5s54rv0mmc4fpw9907npp91vxv6i")))

(define-public crate-ssbh_write-0.16.0 (c (n "ssbh_write") (v "0.16.0") (d (list (d (n "ssbh_write_derive") (r "^0.16") (d #t) (k 0)))) (h "1h8xr9wnk1n1fh6iqqmk29nbmb5a62pppvv3j1w07ckr9qfzw605")))

(define-public crate-ssbh_write-0.17.0 (c (n "ssbh_write") (v "0.17.0") (d (list (d (n "ssbh_write_derive") (r "^0.17") (d #t) (k 0)))) (h "0f69dvk00za0ipgd1kw5bwmpp5clv7n9yn0081scz5qd8v14q5si")))

(define-public crate-ssbh_write-0.18.0 (c (n "ssbh_write") (v "0.18.0") (d (list (d (n "ssbh_write_derive") (r "^0.18") (d #t) (k 0)))) (h "1zk74swmy54f0r9ggswra7x2906s5n6snfap05nk6rnkphapdbhd")))

