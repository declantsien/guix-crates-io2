(define-module (crates-io ss _u ss_uploader) #:use-module (crates-io))

(define-public crate-ss_uploader-0.1.0 (c (n "ss_uploader") (v "0.1.0") (d (list (d (n "candid") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "garcon") (r "^0.2.3") (d #t) (k 0)) (d (n "ic-agent") (r "^0.15.0") (d #t) (k 0)) (d (n "read-byte-slice") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0g2dg45h0n92r42pncbkpp6v39vd4xhzgkf56rs5y8g629r22ibc")))

