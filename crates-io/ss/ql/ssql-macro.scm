(define-module (crates-io ss ql ssql-macro) #:use-module (crates-io))

(define-public crate-ssql-macro-0.1.0 (c (n "ssql-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ghi8l1l1p6dr7r0w7l983rkwgj2cmyi80i1bc87lpawdspl49br") (f (quote (("polars"))))))

(define-public crate-ssql-macro-0.1.1 (c (n "ssql-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jiq27pjb2fa5b5swrhnpl0slxamcwqzy9q8ari0z1lf13gqsmc3") (f (quote (("polars"))))))

(define-public crate-ssql-macro-0.1.2 (c (n "ssql-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gfiygk7014k7i1dsq0wy5vd8c7blpbc3vf15sl7g3vx3zainwdg") (f (quote (("polars"))))))

(define-public crate-ssql-macro-0.1.3 (c (n "ssql-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wmyacax487xf960b91m71anx3icirdvwmz32vd3cqnfxw8p8yxq") (f (quote (("polars"))))))

(define-public crate-ssql-macro-0.1.4 (c (n "ssql-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0znpw84yk329xmy1ps429c13l2hxj109mnmwx5pjqhxi3i7vifq0") (f (quote (("polars"))))))

(define-public crate-ssql-macro-0.2.0 (c (n "ssql-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kjj8wwahmzfan5p96nlwcnvhswv1kf4d4ifqbwxav328bslac8m") (f (quote (("serde") ("polars"))))))

