(define-module (crates-io ss d- ssd-benchmark) #:use-module (crates-io))

(define-public crate-ssd-benchmark-1.0.0 (c (n "ssd-benchmark") (v "1.0.0") (h "1ifzbicskh886n6c4ad4qar0x7khgzgq4ijqi31f9g65vvki1hya")))

(define-public crate-ssd-benchmark-1.1.0 (c (n "ssd-benchmark") (v "1.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0gjgbir0znlw3k9xsklv08dnm87zni2ydx9dj948f09p7df8bk8d")))

(define-public crate-ssd-benchmark-1.1.1 (c (n "ssd-benchmark") (v "1.1.1") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1xb1gdcgq87mvvyxxkkyghinjx97fw0gnzhqkdf2bpwzbg5y9gqv")))

(define-public crate-ssd-benchmark-1.1.2 (c (n "ssd-benchmark") (v "1.1.2") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1jz1hwc9f336hkj15cnqb6gprpnjgd3c224hzjs4zrrcqi3am927")))

(define-public crate-ssd-benchmark-1.1.3 (c (n "ssd-benchmark") (v "1.1.3") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0z157i1jqacy3anafih67y0hyrzd5nza4f8k87wrfsb3bglf4lg7")))

(define-public crate-ssd-benchmark-1.1.4 (c (n "ssd-benchmark") (v "1.1.4") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ddydmf6jx1mqh2nkirgc4srcaahyv7g5r9p7p35cm30s4gw8f9q")))

(define-public crate-ssd-benchmark-1.1.5 (c (n "ssd-benchmark") (v "1.1.5") (d (list (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0fxja1gmx89mq932qwi21sjcvhn3cp4n77xcxlm7d6jhdm0y7cmb")))

(define-public crate-ssd-benchmark-1.1.6 (c (n "ssd-benchmark") (v "1.1.6") (d (list (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1m8z2g4s90agh3i8xv24xfyhza7xpwbs2ngx2a126cz6dvfw4hxh")))

(define-public crate-ssd-benchmark-1.1.7 (c (n "ssd-benchmark") (v "1.1.7") (d (list (d (n "fastrand") (r "^1.4.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)))) (h "069fw73hkaj2wkxs99v2b2i3f2k4ir4dc133mv6ad50clcm4zjpf")))

(define-public crate-ssd-benchmark-1.1.8 (c (n "ssd-benchmark") (v "1.1.8") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1ib0mzw49cx91q81bs3zs7yxw5cbg07angqcp807kad3q4mfhzb7")))

(define-public crate-ssd-benchmark-1.1.9 (c (n "ssd-benchmark") (v "1.1.9") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.4") (d #t) (k 0)))) (h "116fdfhf8zpmcjsrjf6m8rpb9lnrj73lil9w38wwym52jcmhz2aq")))

