(define-module (crates-io ss lr sslrelay) #:use-module (crates-io))

(define-public crate-sslrelay-0.4.2 (c (n "sslrelay") (v "0.4.2") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xzsyljxy5w25kam6nyn3zcq5y9wq1b4my8xdn1bg0mp08bbc7as")))

(define-public crate-sslrelay-0.4.3 (c (n "sslrelay") (v "0.4.3") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "03bbbxpqgqvrvkhjqw8yiqgc5mwqsmgdg1bmx9blbqwa73sn4k7v")))

(define-public crate-sslrelay-0.4.31 (c (n "sslrelay") (v "0.4.31") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1l83clc3c8706kzb8wm3393jzzjl0qp3155nb9fnql40bw5nlljz")))

(define-public crate-sslrelay-0.4.4 (c (n "sslrelay") (v "0.4.4") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0lqpv3k47jxac5jpwdq207kvvzialyy0kxgcn0i1i72sfxah5hd6")))

(define-public crate-sslrelay-0.5.0 (c (n "sslrelay") (v "0.5.0") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14la3sr3wmk42qj432axd9smshyllhb3gmq8ihkpyr5mnxn73nm9")))

(define-public crate-sslrelay-0.6.0 (c (n "sslrelay") (v "0.6.0") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "1lrpcdlk532qssixh4f4gdjhfqr4bw0m3n5mkrxnq1phrkx2hb5l")))

(define-public crate-sslrelay-0.6.1 (c (n "sslrelay") (v "0.6.1") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "1za7yqf843la4bb4bl7qd5dn45sd04ib2mdz47zk2mfbv932nkxg")))

(define-public crate-sslrelay-0.6.2 (c (n "sslrelay") (v "0.6.2") (d (list (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "02can66fmpr5z0cc3xprinjwy6b2dsd7hd8m1smqa6sgwsm7ypcl")))

