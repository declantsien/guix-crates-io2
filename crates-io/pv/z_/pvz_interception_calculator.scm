(define-module (crates-io pv z_ pvz_interception_calculator) #:use-module (crates-io))

(define-public crate-pvz_interception_calculator-0.1.0 (c (n "pvz_interception_calculator") (v "0.1.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "0dghldhnw87hhs601ck0lv6ybidyk0lha7z1l8vhialqk8pdh0xh") (y #t)))

(define-public crate-pvz_interception_calculator-2.0.5 (c (n "pvz_interception_calculator") (v "2.0.5") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "0wk9nvaa1zl0nama4yzmlsbbyzd8wf1vfinsxwc712h27iidxn86")))

(define-public crate-pvz_interception_calculator-2.0.6 (c (n "pvz_interception_calculator") (v "2.0.6") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "124hgrj18jg12sp0lja551xhcb0c0fp00v75yk0zcrpigfx7g94g")))

(define-public crate-pvz_interception_calculator-2.0.7 (c (n "pvz_interception_calculator") (v "2.0.7") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1av6zpny0d6wqbh6b6r7j3j5pr2mpsb681k7yzgp6c49yndh32rg") (f (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2.0.8 (c (n "pvz_interception_calculator") (v "2.0.8") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1429k0aacgyw5s756x8bdri7k6d9lv51j7lsgzrav88472f3k5s4") (f (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2.0.9 (c (n "pvz_interception_calculator") (v "2.0.9") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "00mz4znbskbk47rgam5aczziwfjpvipfln5vcggxsdxfxq58sylp") (f (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2.0.10 (c (n "pvz_interception_calculator") (v "2.0.10") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1ym1hmdppk52yk1ck6dgi2c2l73jfksanz5z784mif1hpf0513im") (f (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2.0.11 (c (n "pvz_interception_calculator") (v "2.0.11") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1dvlqysclv5hrqjxjz0fvqg0qr5ji56dqzirzqmmxw8xyp90fp4b") (f (quote (("zh") ("en"))))))

