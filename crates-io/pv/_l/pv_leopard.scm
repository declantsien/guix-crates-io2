(define-module (crates-io pv _l pv_leopard) #:use-module (crates-io))

(define-public crate-pv_leopard-0.9.0 (c (n "pv_leopard") (v "0.9.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0fbiq5m5b51lrbv9vkpk5i89zajkpryadfq8gv9a8fzi2v0gpxzr") (y #t)))

(define-public crate-pv_leopard-1.0.0 (c (n "pv_leopard") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "111k7rzmqganc8b19v38wlsnf02aclsyn5gwvk9xzz5h7lyjy0hk")))

(define-public crate-pv_leopard-1.0.1 (c (n "pv_leopard") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "02qy3k5cd2lxy03p83921ky7vvh9mb93a9j0c859bwlqfx9sqd5j")))

(define-public crate-pv_leopard-1.0.2 (c (n "pv_leopard") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0vv92cfrq4kvppzmwi2v3wmkqk1qyz9jdnxs064l5z4s138abzhv")))

(define-public crate-pv_leopard-1.1.0 (c (n "pv_leopard") (v "1.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)))) (h "181ax1fbqmvpmrzpj3q8y1wr7c4msjjcqjcmra8g2mk3iyrq7pag")))

(define-public crate-pv_leopard-1.1.1 (c (n "pv_leopard") (v "1.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)))) (h "1dq8b0qwb2l12snchnqyamb39a6rvxpkdmm9jg54hv76fc6xicik")))

(define-public crate-pv_leopard-1.2.0 (c (n "pv_leopard") (v "1.2.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1z5k606iy2kgzhc89v1qjvpph6ij018bijw303vj1kx7ppczq86r")))

(define-public crate-pv_leopard-2.0.0 (c (n "pv_leopard") (v "2.0.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mlapz93ik9jzcl1s8hqm18mk0m3f5kxlhb5dfj56knw1gv09pf6")))

(define-public crate-pv_leopard-2.0.1 (c (n "pv_leopard") (v "2.0.1") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08bblyq9gsfvk4sing3sdn84r9k0m7g56ki5iahk27h9nbphpr8l")))

(define-public crate-pv_leopard-2.0.2 (c (n "pv_leopard") (v "2.0.2") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04mh9yc7h0v2lisffn2gj0az538zw8qnzm830n42m9zm29g6i3a8")))

