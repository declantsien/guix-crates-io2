(define-module (crates-io pv _r pv_rhino) #:use-module (crates-io))

(define-public crate-pv_rhino-0.0.1 (c (n "pv_rhino") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "18ga894a326d01h4dd5hmi9fa5x89gjsywjf7rh2kf6mc11wlcrm")))

(define-public crate-pv_rhino-1.6.0 (c (n "pv_rhino") (v "1.6.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1dlx80dbicv306jg72rd5cwc4cpysr9345wa7l9zr6qmyyg9i987")))

(define-public crate-pv_rhino-2.0.0 (c (n "pv_rhino") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1qsz361099dqvqxr9y3krcgcdnlf1gb30c8qn7770gxllrpbbr69")))

(define-public crate-pv_rhino-2.0.1 (c (n "pv_rhino") (v "2.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "18viw3bsdh00zrkmvrc454q2d54xyk6075g612va82y6w5nc3v5a")))

(define-public crate-pv_rhino-2.1.0 (c (n "pv_rhino") (v "2.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0dgnw6rh2426yrw1zwlwk9bvcg91k6yfllry7a6ahz2gly5mabam")))

(define-public crate-pv_rhino-2.1.1 (c (n "pv_rhino") (v "2.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0p8c6097lh3lgm09ykf3zki19abbknvc9r0r405b60cyqs32w50r")))

(define-public crate-pv_rhino-2.1.2 (c (n "pv_rhino") (v "2.1.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0zak9ga9py3x5qybg3h9s8vasga4vrvqcpcqzq9r4r1594ljwh8v")))

(define-public crate-pv_rhino-2.1.3 (c (n "pv_rhino") (v "2.1.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0zp6jv12j5rk64d8pwvsrcw0wd0g6d2a8b7kldhcdlrm1x6xwf5x")))

(define-public crate-pv_rhino-2.1.4 (c (n "pv_rhino") (v "2.1.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1w8yavbn28mgcbc11axdzgg80is2svf44p0w9981d7dih5svkfgl")))

(define-public crate-pv_rhino-2.1.5 (c (n "pv_rhino") (v "2.1.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0dc76lv2gy263465z0kwp47qg5a1c018xbhqr23wck582fwkfbpd")))

(define-public crate-pv_rhino-2.1.6 (c (n "pv_rhino") (v "2.1.6") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0v52lv6mic21w6zcfvp9fadh2i4a77hqymxx1kii0avp6c836yrx")))

(define-public crate-pv_rhino-2.1.7 (c (n "pv_rhino") (v "2.1.7") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0dj676l9sx2b239kprmzcw1782bmra0mh2zm8dk00bgy5hmngn83")))

(define-public crate-pv_rhino-2.1.8 (c (n "pv_rhino") (v "2.1.8") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0rhbw5yi5b6a6frda2xr0vli5df362445ylsgs0smimfmfcpqsrr")))

(define-public crate-pv_rhino-2.2.0 (c (n "pv_rhino") (v "2.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0dyykbys5l06ika38kwygac8fjv94k7953r1bn9y98wpak8bj7lh")))

(define-public crate-pv_rhino-2.2.1 (c (n "pv_rhino") (v "2.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0lsc964s4hci6ysf34dxw33hb5bnlkvz55438birdvm4afrg0gf8")))

(define-public crate-pv_rhino-3.0.0 (c (n "pv_rhino") (v "3.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "09ifkn21m9qh2iy2d9p6hvv2h82llb00cw8w8yvyjmrk40x9c1pf")))

(define-public crate-pv_rhino-3.0.1 (c (n "pv_rhino") (v "3.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0njm55wnw22j9m99q20zfqcglsrh7rzy4yg51kwz08ka6w1smbmn")))

(define-public crate-pv_rhino-3.0.2 (c (n "pv_rhino") (v "3.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0l75686wwbc1kwxw98g3wyyvff7fmi7nd846cs1x44khqd3d8ddh")))

