(define-module (crates-io pv _r pv_recorder) #:use-module (crates-io))

(define-public crate-pv_recorder-1.0.0 (c (n "pv_recorder") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "120jr99wvs3w6qv5zl0zwsl7rkri3qr8d43bs1v0vc4my5yw8zfm")))

(define-public crate-pv_recorder-1.0.1 (c (n "pv_recorder") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bvxcsyh7mxcclsqydq9im38ackqv43w32zb9hhc1bnp0snlkxs3")))

(define-public crate-pv_recorder-1.0.2 (c (n "pv_recorder") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1clhy8l9clsa0iij4pb5lpl8330xbgvl4647fbq4jfkimvi18dj8")))

(define-public crate-pv_recorder-1.1.0 (c (n "pv_recorder") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1k0n5nfzd1fdrgpbqj7axb23hdkmqqjfk32wa42g6574k9v0n59j")))

(define-public crate-pv_recorder-1.1.1 (c (n "pv_recorder") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "06kcmgd25phk0kd7ngvc3sv5sdkyslvzxrkdwr1dmajqnndr95mf")))

(define-public crate-pv_recorder-1.1.2 (c (n "pv_recorder") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "09z9gn7k55xj3qgkgp8jirmxnh3s5jzvl9l6sl6sw60lwana8pv9")))

(define-public crate-pv_recorder-1.2.0 (c (n "pv_recorder") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0dcidmgrv965hlmkbq5aj17f19lh0nxdl2wr4v0sw5m4cyxm4r5z")))

(define-public crate-pv_recorder-1.2.1 (c (n "pv_recorder") (v "1.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0cfy8nzzvw2i4n7351k2zglr6bjdkm3qj1bj4fwx14zrpqaivgj0")))

(define-public crate-pv_recorder-1.2.2 (c (n "pv_recorder") (v "1.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0klsnziih4wxk6mqd6p8r3zmlpv55l2sdbmpbjwpsqs0qwc19jr8")))

