(define-module (crates-io pv _c pv_cobra) #:use-module (crates-io))

(define-public crate-pv_cobra-0.0.1 (c (n "pv_cobra") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "11c3v4sfic1zl3wrq7d9dqnvyhrsgdm6xgimb429a4gawjsxkdag") (y #t)))

(define-public crate-pv_cobra-0.0.2 (c (n "pv_cobra") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0bd64ck1pkkq7n0s3ji3asyc7rgvzwfpdrmj03a8nadqpycr0drb") (y #t)))

(define-public crate-pv_cobra-1.0.0 (c (n "pv_cobra") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1jq4j5pbm5kzwd0n94gcq6j96r7vwzsv5l36ijjr1g8gnzy3nq2c") (y #t)))

(define-public crate-pv_cobra-1.0.1 (c (n "pv_cobra") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0adpwcqhzblh0r29ax1s4v88gwbq7cklndhd6a447vf0gdaxjfnx") (y #t)))

(define-public crate-pv_cobra-1.0.2 (c (n "pv_cobra") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "120xmi1nx6sskmn89n8xdbblsr356a3qvpfr0sr3c7pla49rdrp5") (y #t)))

(define-public crate-pv_cobra-1.0.3 (c (n "pv_cobra") (v "1.0.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0sq48mbpnw17qpb00cfgn7fnycp3di75cspq20xgpq9vkcdqijpw") (y #t)))

(define-public crate-pv_cobra-1.0.4 (c (n "pv_cobra") (v "1.0.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "06afib0lvlxl8ghphy95hzbjgdid0g3vmw6hvq3386cgfc1ncmlc")))

(define-public crate-pv_cobra-1.0.5 (c (n "pv_cobra") (v "1.0.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1kn6dis153vj71w1fnx8gx6xwiq9s36zpfdnw1axqc24fjkx11x4")))

(define-public crate-pv_cobra-1.0.6 (c (n "pv_cobra") (v "1.0.6") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0n5bqm7ps41jl9n1ngwrwq3pzlkm9kgmwhyhh61ycgq4q8qxc468")))

(define-public crate-pv_cobra-1.1.0 (c (n "pv_cobra") (v "1.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0fp1xz6zqysz5nxdkqfpnwyfrgmy0qlwcxpvfrq5jqiacidv2i8v")))

(define-public crate-pv_cobra-1.1.1 (c (n "pv_cobra") (v "1.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "15g0gm2dcbgyh68cja5rrcxc9czcd50xyswaacpj34gy2v7kiz55")))

(define-public crate-pv_cobra-1.1.3 (c (n "pv_cobra") (v "1.1.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1faim623dvwysmp9b3a5pcj7c5smv2ki8zbp7jg1fn6w6pwlvym9")))

(define-public crate-pv_cobra-1.1.4 (c (n "pv_cobra") (v "1.1.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "02z00dq7p8l1k7g7c33cxwcl6zz4l5xkxzls3vbmj0kwjjib4rid")))

(define-public crate-pv_cobra-1.2.0 (c (n "pv_cobra") (v "1.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)))) (h "1y0kxym8fp4s35wxlp5ml0sqrkvcwimv7qvhbblfp5fz2kq2jad7")))

(define-public crate-pv_cobra-1.2.1 (c (n "pv_cobra") (v "1.2.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)))) (h "1rmjv0vyfqgiy0xrnxk8ygkrnibmpy7ryjwlq0n25bgqx83p7a2g")))

(define-public crate-pv_cobra-2.0.0 (c (n "pv_cobra") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)))) (h "19knrkdfklsncr1pnsk15z6sgaf4m02hdcpcd59p4kwv5rdh7p3z")))

(define-public crate-pv_cobra-2.0.1 (c (n "pv_cobra") (v "2.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)))) (h "11yfvhyp9gz77yhjc9flwwvlc62yfwnbfy49l84162qjngkxlwdj")))

(define-public crate-pv_cobra-2.0.2 (c (n "pv_cobra") (v "2.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)))) (h "17aidkmzbmf0alfcr75gwdxx9hh9yjh3jrzibwxhfpmqrzwhrsjq")))

