(define-module (crates-io pv _c pv_cheetah) #:use-module (crates-io))

(define-public crate-pv_cheetah-0.9.0 (c (n "pv_cheetah") (v "0.9.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "15j67ld13rhlx2nphfwz8lyfi9m73mw61f0iaz88y3p84i942yab") (y #t)))

(define-public crate-pv_cheetah-1.0.0 (c (n "pv_cheetah") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0zr3p3d2j2mx1sv7vzs8idyhfrxynqdxrcibcki6z1rbnqm8kb8j")))

(define-public crate-pv_cheetah-1.0.1 (c (n "pv_cheetah") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "13fxq2z08qac3hczxjzw426jagvm2c962v3h7haj28rr2qsw221v")))

(define-public crate-pv_cheetah-1.1.0 (c (n "pv_cheetah") (v "1.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)))) (h "1dgahbazplbhwl8qbp2z9hp3hjsi74zkazhkqqmz3d5sg3av8919")))

(define-public crate-pv_cheetah-2.0.0 (c (n "pv_cheetah") (v "2.0.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ga8mzlc9w7zcdyyc3yvl8qpsvwipndw11wlj5gbdzvl4pwrpg4d")))

(define-public crate-pv_cheetah-2.0.1 (c (n "pv_cheetah") (v "2.0.1") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d80r3m38k24760v9v200x9jwwlmj8gfj859pw6xjp5na48phzk9") (y #t)))

(define-public crate-pv_cheetah-2.0.2 (c (n "pv_cheetah") (v "2.0.2") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pn3gn78jq1lfzbd9hxrmdksfmchhpjkscjq0pzs604lw7g5aihv")))

