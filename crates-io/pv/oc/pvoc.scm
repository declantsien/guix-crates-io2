(define-module (crates-io pv oc pvoc) #:use-module (crates-io))

(define-public crate-pvoc-0.1.0 (c (n "pvoc") (v "0.1.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)))) (h "1v38hvm3fddp1f7hdn88gjjci9fkvaz9d5d83nc6x74b644xn3j8")))

(define-public crate-pvoc-0.1.1 (c (n "pvoc") (v "0.1.1") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)))) (h "1z1j4kay7pv5yfkq1gsq4ci7ljrnpzvwc5f4mxjr7w66x19lp4p7")))

(define-public crate-pvoc-0.1.2 (c (n "pvoc") (v "0.1.2") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)))) (h "05v7g707viwba971bn7527hshmb7yri775zycmrhg0ghl0g34yfc")))

(define-public crate-pvoc-0.1.3 (c (n "pvoc") (v "0.1.3") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)))) (h "1xh7d4hdf23ffxj1p53xaab0x10rigkalhrzrpqm4ch5jl3620k7")))

(define-public crate-pvoc-0.1.4 (c (n "pvoc") (v "0.1.4") (d (list (d (n "apodize") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)))) (h "1kyjdvsksgl3x48bzbk8sjh5kxs2iwm5mx5r0wlydzmj2fgm0fsn")))

(define-public crate-pvoc-0.1.5 (c (n "pvoc") (v "0.1.5") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "04ci1cpbi6ylxx2hvibbchggvnndkp7r3qr81mngynmiznyiblrp")))

(define-public crate-pvoc-0.1.6 (c (n "pvoc") (v "0.1.6") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "15y5ax7zzld7y25p3zajadvx2wfz4qi86mywh8m65ndaakx90mrj")))

(define-public crate-pvoc-0.1.7 (c (n "pvoc") (v "0.1.7") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "0paq84hzwa14q4vbv2rhr9ag32v0rqvxivs63lvvl40ns7p3mc8a")))

