(define-module (crates-io pv _p pv_porcupine) #:use-module (crates-io))

(define-public crate-pv_porcupine-0.0.1 (c (n "pv_porcupine") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0wkyi6qpz76c1x3vnfhvjvhqficyqfyaslk07cgpp25rp3gld7ja") (y #t)))

(define-public crate-pv_porcupine-0.0.2 (c (n "pv_porcupine") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0l6v17ch3qiim305xgaxpys8rf7r49kirxl4zmnmzkjcfyk17rbg")))

(define-public crate-pv_porcupine-1.9.0 (c (n "pv_porcupine") (v "1.9.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1zxp7rl7145qd21fhmyy70msbin9afilwn8lgjinvsrhz3xf68ly")))

(define-public crate-pv_porcupine-1.9.1 (c (n "pv_porcupine") (v "1.9.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "06ak7yvx4s18isrs23dals3sbiydyz59bv88qifjs5x30qd6fx87")))

(define-public crate-pv_porcupine-1.9.3 (c (n "pv_porcupine") (v "1.9.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1aglisir648rj8dsym1fhgxzz4pz5pzdfb60368b86z0rlpzb018")))

(define-public crate-pv_porcupine-2.0.0 (c (n "pv_porcupine") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0ldb113x2v3zmmzc0wi9kf2ix495v5gi8z3khjf62m4c14zpcgbc")))

(define-public crate-pv_porcupine-2.0.1 (c (n "pv_porcupine") (v "2.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1kyvgmdpqs5zzlcy6f2vzc8dk1c1hvqrr9s3p0yz0m6n6zb2jj6b")))

(define-public crate-pv_porcupine-2.1.0 (c (n "pv_porcupine") (v "2.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1yc0q1w1dc2kq2n8gl3cxm5vplc6nkhlh0mrfxqhcbijbj3392vv")))

(define-public crate-pv_porcupine-2.1.1 (c (n "pv_porcupine") (v "2.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1kfr5iqr453gjyj428shypjzpvz0xh3jb5ccsxscqyj5dwlpisv0")))

(define-public crate-pv_porcupine-2.1.2 (c (n "pv_porcupine") (v "2.1.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0d7wp3dxa7ls7i70ibqg3fsq1wkd9idpx35gyybyqngavkh0a5zq")))

(define-public crate-pv_porcupine-2.1.3 (c (n "pv_porcupine") (v "2.1.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0yq91w1myk33nd1i5x47qd62cjxxia7z05sg31v88y6qqxjbs0mz")))

(define-public crate-pv_porcupine-2.1.4 (c (n "pv_porcupine") (v "2.1.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0pkg2y45y3jam00vayq209b6n7fql4lnhfg7ni46fbd4009mxw14")))

(define-public crate-pv_porcupine-2.1.5 (c (n "pv_porcupine") (v "2.1.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0iwbffh3vlbq60mdlylk4bpiph69qc48l57fjb08lvwarjpirxv6")))

(define-public crate-pv_porcupine-2.1.6 (c (n "pv_porcupine") (v "2.1.6") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0alirfizwpl8mxrs2wb8f0syw43dbr2cf83xc4jskckfihlh0r2i")))

(define-public crate-pv_porcupine-2.1.7 (c (n "pv_porcupine") (v "2.1.7") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "0f1lww1n7bwqnqba4z5wc776hmpsdc8kv4r76p4i7br60978ilr6")))

(define-public crate-pv_porcupine-2.1.8 (c (n "pv_porcupine") (v "2.1.8") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)))) (h "1hkigmcy3q98z8800pwbrjwr59dqad51lyls658izd6phpw6lh47")))

(define-public crate-pv_porcupine-2.2.0 (c (n "pv_porcupine") (v "2.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0y7xha835imghvf06q3q0vmkz5822nbxnc06nd62m5dq4r0ra0xf")))

(define-public crate-pv_porcupine-2.2.1 (c (n "pv_porcupine") (v "2.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "00ni57s2fcapydwi03ssalb4vp14larv9x9mkwzw5aanp16z0ymf")))

(define-public crate-pv_porcupine-3.0.0 (c (n "pv_porcupine") (v "3.0.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g33vfkik8mrrljgpmzw6c9a7iks3rcm20kj9y268va6vil9m5ki")))

(define-public crate-pv_porcupine-3.0.1 (c (n "pv_porcupine") (v "3.0.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ij9j576mbhnmfqbprhpf47maxmdm2n2776s8l2ib5icax9kxmcj")))

(define-public crate-pv_porcupine-3.0.2 (c (n "pv_porcupine") (v "3.0.2") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qxc7kmq4fxs64xj1kncpz647cwydrb9i4q09bq9al9j3bzihqkf")))

