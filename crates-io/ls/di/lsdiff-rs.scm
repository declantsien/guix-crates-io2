(define-module (crates-io ls di lsdiff-rs) #:use-module (crates-io))

(define-public crate-lsdiff-rs-0.1.1 (c (n "lsdiff-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1wjs5q0npm87ix6xwmsfvah4lzsyxssy8y305q6z1b3bv1ccpjw7")))

(define-public crate-lsdiff-rs-0.1.2 (c (n "lsdiff-rs") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0k5dnh3a3xis9j92h1zinm6y8xiamb39fz0mmmkc0a3wxk7dsg3f")))

