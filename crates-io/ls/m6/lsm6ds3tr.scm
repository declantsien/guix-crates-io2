(define-module (crates-io ls m6 lsm6ds3tr) #:use-module (crates-io))

(define-public crate-lsm6ds3tr-0.0.0 (c (n "lsm6ds3tr") (v "0.0.0") (h "1aky5z0ik5baz6kbpgqilmrzvv98l0nq1bjxp2s2d8d978m6di2k")))

(define-public crate-lsm6ds3tr-0.1.0 (c (n "lsm6ds3tr") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0bvn9qjgcbwdk8z15aqnmnmgg7zm17ixjv9w9p9dkm9zmzap5fv2") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-lsm6ds3tr-0.1.1 (c (n "lsm6ds3tr") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "11il4h6awvpdibxw6xz9pqkp6m5j981pda47s7yi52x1q5wv24ny") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

