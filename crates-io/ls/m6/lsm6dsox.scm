(define-module (crates-io ls m6 lsm6dsox) #:use-module (crates-io))

(define-public crate-lsm6dsox-1.0.1 (c (n "lsm6dsox") (v "1.0.1") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.3") (d #t) (k 0)) (d (n "measurements") (r "^0.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1qwc4ki72xcd1gps34731aa1zzfdz7y0j1id8afs92dzlk0x45y4")))

