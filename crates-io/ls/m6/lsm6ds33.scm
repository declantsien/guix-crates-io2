(define-module (crates-io ls m6 lsm6ds33) #:use-module (crates-io))

(define-public crate-lsm6ds33-0.1.0 (c (n "lsm6ds33") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1059rmpr1g57ff35l98v21bf52cl548lrncvxfdzs2r2qd8mzsw0")))

(define-public crate-lsm6ds33-0.2.0 (c (n "lsm6ds33") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1k8pl265kmdsbkifdsn47ld2rg5lfmxfqa77lwj7fhj64s509y4n")))

(define-public crate-lsm6ds33-0.3.0 (c (n "lsm6ds33") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.1") (d #t) (k 0)))) (h "00f3wk4srvzh231y6qq5ppbbwvk7z5s5lyaf1n43vpfid1avbp90") (f (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6ds33-0.4.0 (c (n "lsm6ds33") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.1") (d #t) (k 0)))) (h "1kd3ly2y1a6m73090kx6462z4ibwrlnsxrpaqdpkjsg04rh0j2cz") (f (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6ds33-0.5.0 (c (n "lsm6ds33") (v "0.5.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.1") (d #t) (k 0)))) (h "0zr2d81alfsswb6zsjhdvrg2zd6gl200swffivrwymfq5vi1p7l8") (f (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

