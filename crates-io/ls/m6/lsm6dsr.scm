(define-module (crates-io ls m6 lsm6dsr) #:use-module (crates-io))

(define-public crate-lsm6dsr-0.1.0 (c (n "lsm6dsr") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)))) (h "08f3nq9rklbv6wmf6b14c17p4vsk4rn3zichdlwpk7hbv1i4dkyv")))

