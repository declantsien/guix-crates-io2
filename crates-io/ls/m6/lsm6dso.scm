(define-module (crates-io ls m6 lsm6dso) #:use-module (crates-io))

(define-public crate-lsm6dso-0.1.0 (c (n "lsm6dso") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.1") (d #t) (k 0)))) (h "0k0x5wgwn09k9axcjmdarlldz0hhjg8hkciv0pqdnf97f9hh8yk3") (f (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

