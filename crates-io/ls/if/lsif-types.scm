(define-module (crates-io ls if lsif-types) #:use-module (crates-io))

(define-public crate-lsif-types-0.1.0 (c (n "lsif-types") (v "0.1.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1faqird00mvv55zis6hnifs0aqxzihpxk559n9x3wh1qgqcw5pf6") (y #t)))

(define-public crate-lsif-types-0.2.0 (c (n "lsif-types") (v "0.2.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1n2zrsr1srdzkc54dzg2sah7ghiib1ajaixk97yamhnki3rcvqrm") (y #t)))

(define-public crate-lsif-types-0.3.0 (c (n "lsif-types") (v "0.3.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "14qhg47w2ycrska0slns6144hnq9sk5mlk5qdbp0pw4w6p51a9li") (y #t)))

(define-public crate-lsif-types-0.3.1 (c (n "lsif-types") (v "0.3.1") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0xcr9rr56i21gdxjlrjij10hnqfwdgz238r7s5gwnijjbvpjyyc4") (y #t)))

(define-public crate-lsif-types-0.4.0 (c (n "lsif-types") (v "0.4.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0ssrh3ia9vms2c2m79xpwqsa270dnihm4y6fjmmyhzl6vsdayain") (y #t)))

(define-public crate-lsif-types-0.5.0 (c (n "lsif-types") (v "0.5.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "193n4d45h6j3j34v2yf2c2c3bn82scsv6lqlsz9w1krcyk6zj7hq") (y #t)))

(define-public crate-lsif-types-0.6.0 (c (n "lsif-types") (v "0.6.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1hixgicd69kr3var9w9rypv7yhfpm7fkc454k8byama2lyhqxswg") (y #t)))

(define-public crate-lsif-types-0.6.1 (c (n "lsif-types") (v "0.6.1") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1iad7l2v7mnvxs2airydkwqjc3c4slrbzsd2073bvwzicyk5ggnf") (y #t)))

(define-public crate-lsif-types-0.6.2 (c (n "lsif-types") (v "0.6.2") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0xc1q21mki46dsg3nbzsswvcf0j8z7kfy4x7smbvx3alimllhg0s") (y #t)))

(define-public crate-lsif-types-0.7.0 (c (n "lsif-types") (v "0.7.0") (d (list (d (n "lsp-types") (r "^0.86.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0fmvsylvnyjjl2lba2bbd11j9vbg67ikfsnfvpqrpajc26x1lzvc") (y #t)))

(define-public crate-lsif-types-0.8.0 (c (n "lsif-types") (v "0.8.0") (d (list (d (n "lsp") (r "^0.86.0") (d #t) (k 0) (p "lsp-types")) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1rw9546210v53z7rl7mqxbp7b0vwvwrlpdzwhxamdr50d187ghda") (y #t)))

