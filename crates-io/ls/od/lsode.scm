(define-module (crates-io ls od lsode) #:use-module (crates-io))

(define-public crate-lsode-0.1.0 (c (n "lsode") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rkw02f6y32vd9ng9bn3rccpx1y6agp8vy22izx908d9xvxhrsik")))

(define-public crate-lsode-0.1.1 (c (n "lsode") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "062j4qckjm0bm2ingjc69jjr42rhcsp74alxmcchcncmyqm19zk0")))

(define-public crate-lsode-0.1.2 (c (n "lsode") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ligjcfyjf0b5gqzcyqsnv31ia8kdd5ql4lbck9lki9x8w8rbh54")))

(define-public crate-lsode-0.1.3 (c (n "lsode") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zpbicfvklhsvingdh2qdzzs1fcm0c6306rdismapkd5vgy5pjzl")))

(define-public crate-lsode-0.2.0 (c (n "lsode") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "1s3m1v2zxa08b8f3ynnf95ikhak14vanfjwqgcbb4v5irkakfjgf")))

(define-public crate-lsode-0.2.1 (c (n "lsode") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "0ywfhpg0d8i7m0skfvp2wvsaf6j52hif34fshgsfpvyvnwc835n5")))

(define-public crate-lsode-0.2.2 (c (n "lsode") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "08jz4yd6d7l8a5161fs4qya1xcbr5pwapiggynfmhvd192q35a88")))

(define-public crate-lsode-0.2.3 (c (n "lsode") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "0cbj0qyn5adzpvadnrxrhn3gaf2l2xk0jwmf7lrcln8czqvc9bq1")))

(define-public crate-lsode-0.2.4 (c (n "lsode") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "1zw9mlricgykz4yxhiki2h9kn7bh1adpclg9vcldgvsbcrqkqj97")))

(define-public crate-lsode-0.2.5 (c (n "lsode") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "14can1kbrdrx9bdpwvcmjx2kkwnckr5hqm1mnbrhf5z8zp87l3xh")))

(define-public crate-lsode-0.2.6 (c (n "lsode") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "0xc320syq01f0rsvlmcf57ax0qbphjkq57ya6ayyga9wf4gc2xsn")))

(define-public crate-lsode-0.2.7 (c (n "lsode") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "0nw70xpgklq3wblmm5rhxjffkmrkrg4m8pzfs50w197znl135cy5")))

(define-public crate-lsode-0.2.8 (c (n "lsode") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.9") (d #t) (k 0)))) (h "14rm5dhhfyh3bj1sznjiggkkfjp3ldvsnq210q0z67y302xi3syf") (y #t)))

