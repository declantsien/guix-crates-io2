(define-module (crates-io ls tr lstree) #:use-module (crates-io))

(define-public crate-lstree-0.1.0 (c (n "lstree") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z5ngjlpk8cg0ivrjpgfy1ilhr7396q8m235ixfp3bqz7vmjdl1s") (y #t)))

(define-public crate-lstree-0.1.1 (c (n "lstree") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0naxd0dki26y58nix16fb4vrj7lkwbd7i0zjid43f1q7fxc329pd") (y #t)))

(define-public crate-lstree-0.1.2 (c (n "lstree") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ravf7z38irak3i24yfi57ljgjq4zixxfp41h0mca7yyw1x8bhdy") (y #t)))

(define-public crate-lstree-0.1.3 (c (n "lstree") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yhl2n81zv1ii418qgg7mfs8zmp7bh7f0jd5bb1s4rxls3zfb2hp") (y #t)))

(define-public crate-lstree-0.1.4 (c (n "lstree") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gx1b07xa5r2gqhq7g6dshydlpgqx6g4ymb6ym56d9mbikq61zdp") (y #t)))

