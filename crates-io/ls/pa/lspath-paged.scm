(define-module (crates-io ls pa lspath-paged) #:use-module (crates-io))

(define-public crate-lspath-paged-0.1.0 (c (n "lspath-paged") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minus") (r "^5.1.0") (f (quote ("static_output" "search"))) (d #t) (k 0)))) (h "16f9p6y7x45483x1adjryfnncs1r4i1d2b8zrdnbydv7m56f3yaf") (r "1.65")))

(define-public crate-lspath-paged-0.1.1 (c (n "lspath-paged") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minus") (r "^5.1.0") (f (quote ("static_output" "search"))) (d #t) (k 0)))) (h "0kvzmgicym79vg0ngwdcsdc4jb53fsxs95isd5z07pxy7r49j97h") (r "1.65")))

