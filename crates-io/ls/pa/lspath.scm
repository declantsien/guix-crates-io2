(define-module (crates-io ls pa lspath) #:use-module (crates-io))

(define-public crate-lspath-0.1.0 (c (n "lspath") (v "0.1.0") (h "098vfsm208wsdwz8lnb9yx84dggdbxawwsj0xx1k4p18276hp791")))

(define-public crate-lspath-0.1.1 (c (n "lspath") (v "0.1.1") (h "180r6b0gc4c9mh6pc8kqvblyych8xksk2kh3xfg2ykzkn770kf10")))

(define-public crate-lspath-0.1.3 (c (n "lspath") (v "0.1.3") (h "1ac0wdq92vlhpaf7mcxzm8zk4ys3n74yv6rnpjn7nnj3vvxsxghz")))

