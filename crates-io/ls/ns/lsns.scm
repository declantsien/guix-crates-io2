(define-module (crates-io ls ns lsns) #:use-module (crates-io))

(define-public crate-lsns-0.0.1 (c (n "lsns") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "18c0ws91ixjp12c4qkkmmd946w7bda45vg41794829nx703cdqyk")))

(define-public crate-lsns-0.0.2 (c (n "lsns") (v "0.0.2") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "nsutils") (r "^0.0.2") (d #t) (k 0)) (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "1ldvhcmy5k8zcxdkjlyad7srxr4fhhsgqp63gq67f7nf04dnzbrl")))

(define-public crate-lsns-0.0.3 (c (n "lsns") (v "0.0.3") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "nsutils") (r "0.*") (d #t) (k 0)) (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "13ma8h7i5asx54d0sdj057dmckwdrx7nmqab85446i5bmz2xrlhz")))

