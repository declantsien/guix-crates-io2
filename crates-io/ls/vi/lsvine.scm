(define-module (crates-io ls vi lsvine) #:use-module (crates-io))

(define-public crate-lsvine-0.2.2 (c (n "lsvine") (v "0.2.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "12m4q9acchqvdxksxkw1gnnwgvsbnhjyvg9y7h3r9nm2i6hiz5xa")))

(define-public crate-lsvine-0.2.4 (c (n "lsvine") (v "0.2.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "0k90ny01d9m7nczqm6fh0lp4llh2iqn9jjnsfa79j1zc1x5hm8d5")))

(define-public crate-lsvine-0.2.5 (c (n "lsvine") (v "0.2.5") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1zf70licca0c4fb93bl1zffnw29a044l3yd2lxwhp1b3fzm5d96v")))

(define-public crate-lsvine-0.3.1 (c (n "lsvine") (v "0.3.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "05d7280xyk8l0snwkjg1kmdsgshxnyy7g2i9hzrych5s0xx2lkv9")))

