(define-module (crates-io ls p- lsp-text-document) #:use-module (crates-io))

(define-public crate-lsp-text-document-0.1.0 (c (n "lsp-text-document") (v "0.1.0") (d (list (d (n "lsp-types") (r ">=0.83.1, <0.84.0") (d #t) (k 0)))) (h "123mqphqm5p5l6dx6sicivxp60nnf583wf2nv0l72zrddx29p30c")))

(define-public crate-lsp-text-document-0.2.0 (c (n "lsp-text-document") (v "0.2.0") (d (list (d (n "lsp-types") (r "^0.84.0") (d #t) (k 0)))) (h "1dfbxmrq4zmzcd0x8hdshrwvz28lx3lsapm7m5g96800sbhrm71k")))

