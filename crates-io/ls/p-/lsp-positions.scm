(define-module (crates-io ls p- lsp-positions) #:use-module (crates-io))

(define-public crate-lsp-positions-0.1.0 (c (n "lsp-positions") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.19") (o #t) (d #t) (k 0)))) (h "0mg86lnn0g4zxrk00b2xx1ly256g9r83izgagyqysjwkcw1alyi7") (f (quote (("default" "tree-sitter"))))))

(define-public crate-lsp-positions-0.2.0 (c (n "lsp-positions") (v "0.2.0") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.19") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "1lijzn89ri09ldv1ysm1ichfl0xbbhpq2bd17ilizfvl6im5m2rl") (f (quote (("default" "tree-sitter"))))))

(define-public crate-lsp-positions-0.3.0 (c (n "lsp-positions") (v "0.3.0") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.19") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "0kda5pzrbbxh1qa3zyl16pk8wprpwqxp98qhkzn8ppy8sbhl9xx8") (f (quote (("default" "tree-sitter"))))))

(define-public crate-lsp-positions-0.3.1 (c (n "lsp-positions") (v "0.3.1") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.19") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "033zmib3pxgsby0m7s7qrsgic1my3a9pzbk75ga8wrn4wap5psbc") (f (quote (("default" "tree-sitter"))))))

(define-public crate-lsp-positions-0.3.2 (c (n "lsp-positions") (v "0.3.2") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.19") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "10wfq6p4p81r15gjkpbnv3vzsyz731c1sc1j37n99ifqf1ahl8k2") (f (quote (("default" "tree-sitter"))))))

(define-public crate-lsp-positions-0.3.3 (c (n "lsp-positions") (v "0.3.3") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "0dxyjzm3vi6hkxwp0xn8662nv6g9815hbfmv56lfn91xz2acwqda") (s 2) (e (quote (("tree-sitter" "dep:tree-sitter") ("bincode" "dep:bincode"))))))

