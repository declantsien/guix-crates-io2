(define-module (crates-io ls p- lsp-codec) #:use-module (crates-io))

(define-public crate-lsp-codec-0.1.0 (c (n "lsp-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "14dwpzjpx8ch7c0sqzssasp99n578n2jy2vb530i7vrgcg8y6acv")))

(define-public crate-lsp-codec-0.1.1 (c (n "lsp-codec") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1zyb9m8sp4dgylrnscf0qfxlj9d75vdk9jm12d33wklq3dj0ymzk")))

(define-public crate-lsp-codec-0.1.2 (c (n "lsp-codec") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "06nkn53xg46cfxr1arbn6x4qqmjl5w9dj10q5pcdvy4wv1x7778n")))

(define-public crate-lsp-codec-0.2.0 (c (n "lsp-codec") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (k 0)))) (h "1i2yrkm1mj5b0razzpj6qkjv5v4b4fr3v9b60iz1wnki43iq6g6k")))

(define-public crate-lsp-codec-0.3.0 (c (n "lsp-codec") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "io-util"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (k 0)))) (h "0myjv7kf1qrg3r9kf7c5kghcgs3cbhx45jvzzccmlsj7c85rv4xa")))

