(define-module (crates-io ls p- lsp-client-rs) #:use-module (crates-io))

(define-public crate-lsp-client-rs-0.1.0 (c (n "lsp-client-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0s03ahw6d618ia0q9apidm1v2xxcxgvspvswdws00ix6yfrvjkbi")))

