(define-module (crates-io ls p- lsp-ty) #:use-module (crates-io))

(define-public crate-lsp-ty-0.2.0 (c (n "lsp-ty") (v "0.2.0") (d (list (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0jqqmf6mim2c4i72679f0lb3305lps9c1mba9p5aa9b5adpkd5qm") (f (quote (("default") ("async"))))))

(define-public crate-lsp-ty-0.2.1 (c (n "lsp-ty") (v "0.2.1") (d (list (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0m6nnm6bfkmd0hadjrv06wnaw6mf04v19gia88agas7brsz9apxz") (f (quote (("default") ("async"))))))

(define-public crate-lsp-ty-0.2.2 (c (n "lsp-ty") (v "0.2.2") (d (list (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0rxjzf4m484lmcgw7880vj2n851wlq4xj1qqr335a3jn274bnwak") (f (quote (("default") ("async"))))))

