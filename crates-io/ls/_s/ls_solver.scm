(define-module (crates-io ls _s ls_solver) #:use-module (crates-io))

(define-public crate-ls_solver-0.1.0 (c (n "ls_solver") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.2") (f (quote ("io" "sparse" "rand" "libm-force"))) (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (f (quote ("io"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1jv96qsgsqv7rxsn34cwqysgyvhks4sp5wkfmyw72g6yxh1kv8kc")))

