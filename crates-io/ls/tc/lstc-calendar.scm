(define-module (crates-io ls tc lstc-calendar) #:use-module (crates-io))

(define-public crate-lstc-calendar-0.1.0 (c (n "lstc-calendar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "1wx96f6gz7cvx79z6lcp295jjapa02qrcnx9fi2gbz49vrhim7w5")))

(define-public crate-lstc-calendar-0.1.1 (c (n "lstc-calendar") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0897i35x4cbikfhpyn86y65c0b65v5wfyk6n3alqn4qspaa7mp8y")))

(define-public crate-lstc-calendar-0.2.0 (c (n "lstc-calendar") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "1klimiv9zbq8876r75vfwy2nw1g023pj7pifm4f1g3qnms3raksn")))

