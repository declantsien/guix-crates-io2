(define-module (crates-io ls #{01}# ls010b7dh01) #:use-module (crates-io))

(define-public crate-ls010b7dh01-0.1.0 (c (n "ls010b7dh01") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "104zxx1h78lr0m7a00m3b7rm22bcqzhx53hnv70v2aspfa8byx74")))

