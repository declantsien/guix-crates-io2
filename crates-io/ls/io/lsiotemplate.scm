(define-module (crates-io ls io lsiotemplate) #:use-module (crates-io))

(define-public crate-lsiotemplate-0.1.3 (c (n "lsiotemplate") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "handlebars") (r "^0") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "0rf51hvai2ws7pf6zlqyfa569jymjpncib3wzj9ncqzh3j55cxgh")))

(define-public crate-lsiotemplate-0.1.5 (c (n "lsiotemplate") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "handlebars") (r "^0") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "0zka053dxdfqyi2gjhmla46lrds8g6cw1lvc8a36fmg51v7487w6")))

(define-public crate-lsiotemplate-0.1.6 (c (n "lsiotemplate") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "handlebars") (r "^0") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "11zr2gxpsr48i6v22l15szzb863j3ac8l5bpwzwsfk32v1hs706y")))

