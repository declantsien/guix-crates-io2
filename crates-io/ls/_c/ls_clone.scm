(define-module (crates-io ls _c ls_clone) #:use-module (crates-io))

(define-public crate-ls_clone-0.1.0 (c (n "ls_clone") (v "0.1.0") (h "0zglfci5wijzjbz6720kfnvxmq63zzgd80n8rsd0s9mljbgajz0d")))

(define-public crate-ls_clone-0.1.1 (c (n "ls_clone") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0mjcp3wm67y55f965qzr9i0r1wp2skw73mfin2xjig5iz6b19566")))

