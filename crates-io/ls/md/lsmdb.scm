(define-module (crates-io ls md lsmdb) #:use-module (crates-io))

(define-public crate-lsmdb-0.2.0 (c (n "lsmdb") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0bvzjj4fafckpgqdszj6ds6k9mmnsb5bygsa6qyq2dqry3wj294v") (y #t)))

(define-public crate-lsmdb-0.1.0 (c (n "lsmdb") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1d9155c1jymn4wn6grhvnr3wis0i8hhhiv3ly7rjmasis30z4vvc") (y #t)))

(define-public crate-lsmdb-0.3.0 (c (n "lsmdb") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0mzwqcqcaajzhqs3jj57nsqa73fjlp9n4gjm6mii24b5mmprqcna")))

(define-public crate-lsmdb-0.4.0 (c (n "lsmdb") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "148q1qssc5dkcl8zi9ns8vc7n1ih30yqm4ww7h161jinki9vx2zm")))

(define-public crate-lsmdb-0.4.1 (c (n "lsmdb") (v "0.4.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0iy82vm1xsgkfm0328f2zf8jkgapdmai2llkzw9la36ggpnk53cj")))

