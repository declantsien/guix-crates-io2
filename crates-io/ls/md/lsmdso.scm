(define-module (crates-io ls md lsmdso) #:use-module (crates-io))

(define-public crate-lsmdso-0.1.0 (c (n "lsmdso") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.1") (d #t) (k 0)))) (h "19nydxmx7za79nmrskgqajvr5gi5m0ycdn9ajcajg7rvcgyhzl5s") (f (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async")))) (y #t)))

