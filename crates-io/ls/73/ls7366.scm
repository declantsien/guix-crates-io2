(define-module (crates-io ls #{73}# ls7366) #:use-module (crates-io))

(define-public crate-ls7366-0.1.0 (c (n "ls7366") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "1i297lzw0f1i2an04xj345l1c8gh5l1jcpvjcwcg59ddir8wz41j")))

(define-public crate-ls7366-0.2.0 (c (n "ls7366") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0wxbc19hn3mp1cib556gn8nflj03m1i110ssw0mfpb7cvyhirf8i")))

(define-public crate-ls7366-0.2.1 (c (n "ls7366") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "00halyy3k353p0ijnhgf8x6agkvsdz5k7sdkami6vwlyvimfx0ng")))

