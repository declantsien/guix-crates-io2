(define-module (crates-io ls -q ls-qpack) #:use-module (crates-io))

(define-public crate-ls-qpack-0.1.0-alpha (c (n "ls-qpack") (v "0.1.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "07f4a0a02yg4v2xyiiy837wifgkq8v1qf41qh5y0n3yqp6ac1i4x")))

(define-public crate-ls-qpack-0.1.1-alpha (c (n "ls-qpack") (v "0.1.1-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "16lbnaqy71yinbislrgb8xaijw23ifwqa18x40hg00xzd1njjcn3")))

(define-public crate-ls-qpack-0.1.1 (c (n "ls-qpack") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.1") (d #t) (k 0)))) (h "15bz586adxqib9mjj551kxr2x1izyanmydkas8rxq5j76rlrllrb")))

(define-public crate-ls-qpack-0.1.2 (c (n "ls-qpack") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1arb5l52rhdim3a0flsqj3lfn47i5ccqffm7psvxiw882ygc173w")))

(define-public crate-ls-qpack-0.1.3 (c (n "ls-qpack") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.3") (d #t) (k 0)))) (h "01m6lyhnr0sylzsfh0qy1bqqqjks6bpij3f5ss8ki1wszr3yk7c6")))

(define-public crate-ls-qpack-0.1.4 (c (n "ls-qpack") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "ls-qpack-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1j2249ppx8qhzjbzry11kh42fv1x95r9nwzysivba0k1i77h2wad")))

