(define-module (crates-io ls -q ls-qpack-sys) #:use-module (crates-io))

(define-public crate-ls-qpack-sys-0.1.0-alpha (c (n "ls-qpack-sys") (v "0.1.0-alpha") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "0p2x5w0fd05dqn2xcas0nn7gkfxzn0nhldxhyrks629vca9gf2h0")))

(define-public crate-ls-qpack-sys-0.1.1-alpha (c (n "ls-qpack-sys") (v "0.1.1-alpha") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "1dygsg5rmpf0m465aws2my1sl5zcn755pk498v9xgk0ysz7n0hw7")))

(define-public crate-ls-qpack-sys-0.1.1 (c (n "ls-qpack-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "11j14bxahhw42k1zzn1z6g2qylamrmqc7xigzx7qzkpqbk4725n6")))

(define-public crate-ls-qpack-sys-0.1.2 (c (n "ls-qpack-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0m9wbkxiqh7rzbji7m4dvw4nr22lf81v2jlmlznbr1n70dy36hdz")))

(define-public crate-ls-qpack-sys-0.1.3 (c (n "ls-qpack-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0v7253vr71l5vy6w66g7psglnlpz2fnd60x9nz5m6mg4blv877qj")))

(define-public crate-ls-qpack-sys-0.1.4 (c (n "ls-qpack-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "1rnzxmv92v3i3h4v8vacv8pjk06mvdsdxx67vm5kfmvhp8nbz60k")))

