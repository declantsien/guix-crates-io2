(define-module (crates-io ls ts lstsq) #:use-module (crates-io))

(define-public crate-lstsq-0.1.0 (c (n "lstsq") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (k 2)) (d (n "nalgebra") (r "^0.28") (k 0)))) (h "0j001nfs6jn9ll56b2118b679bpcp5wyq6ci7g68zcfb3knsyivi") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.2.0 (c (n "lstsq") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (k 2)) (d (n "nalgebra") (r "^0.29") (k 0)))) (h "19g3m9mkjbf5g9iif5ghgdxkdlg2n5as6jghfb3a7nrbarp9idnv") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.3.0 (c (n "lstsq") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (k 2)) (d (n "nalgebra") (r "^0.30") (k 0)))) (h "07fg9wfvzgf1nmw11vdn7yqphc58wwacgkybyw50m563fx2ki26a") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.4.0 (c (n "lstsq") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (k 2)) (d (n "nalgebra") (r "^0.31") (k 0)))) (h "1blapm4m1znx33gj0nci2gwikq211clyb3yaj23i29ga5jv84309") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.5.0 (c (n "lstsq") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32") (k 0)) (d (n "approx") (r "^0.5") (k 2)))) (h "1n3i8khygjnb8xw06b3bp8scas9b81xid5yxzf1l5dv9a32r29cb") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

