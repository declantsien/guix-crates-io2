(define-module (crates-io ls ph lsph) #:use-module (crates-io))

(define-public crate-lsph-0.1.0 (c (n "lsph") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0lh1jygkqbgaf50g8x67a55xf9vrvww4v8w4vlrnfc1i5bfxa0fk")))

(define-public crate-lsph-0.1.1 (c (n "lsph") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0gqpz161ssfajfrvqn70qf6k7r21vshj54kzc32vfvgii8am26wy")))

(define-public crate-lsph-0.1.2 (c (n "lsph") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "11m79sraxyw7px3wgibb5hq1906ldd9yg4kj6hycw92flcis410c")))

(define-public crate-lsph-0.1.3 (c (n "lsph") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1fz1nlldcybb29nk71fkha36xgzfwy5g079z12mjd8rmhixygkp6")))

(define-public crate-lsph-0.1.4 (c (n "lsph") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0q4q8qhains3m6i02nxfff2lppr8abwhsqvzybpgr0wzgw40nmvb")))

(define-public crate-lsph-0.1.5 (c (n "lsph") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "09js4jvc4yksgf05f79rjlpavmxzg3kdpzdlz367sinrdn472f4j")))

(define-public crate-lsph-0.1.6 (c (n "lsph") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "182arxyhbyhdsb26h6hk79a49qd9qnhg4r9p4m0x4pv9bpq7sqpi")))

(define-public crate-lsph-0.1.7 (c (n "lsph") (v "0.1.7") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "11bhz1alsrx69bybb6l1lmb4fcxd65rm1g76dzycp0qll099g0y1")))

(define-public crate-lsph-0.1.8 (c (n "lsph") (v "0.1.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_hc") (r "^0.3.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "0wfswr8g22qfy99a70riv964cnhz41kmjvwj6b2ks0gw6hpyiich")))

