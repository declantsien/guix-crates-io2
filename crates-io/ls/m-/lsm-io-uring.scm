(define-module (crates-io ls m- lsm-io-uring) #:use-module (crates-io))

(define-public crate-lsm-io-uring-0.1.0 (c (n "lsm-io-uring") (v "0.1.0") (h "05znirspkb3pmldxwpgcnbv3nws8vwv40n715lcibbr7jillhskl")))

(define-public crate-lsm-io-uring-0.2.0 (c (n "lsm-io-uring") (v "0.2.0") (h "1c72i3221sddj55427jyrpqyd7pax5mj3fpifzwa9mc6yzyy57y2") (y #t)))

