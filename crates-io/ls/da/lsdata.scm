(define-module (crates-io ls da lsdata) #:use-module (crates-io))

(define-public crate-lsdata-0.1.0 (c (n "lsdata") (v "0.1.0") (d (list (d (n "implicit-clone") (r "^0.4.8") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "utf8-chars") (r "^3.0.1") (d #t) (k 0)))) (h "03xq8i70jxx4s6sg84fr5wh4nm9al4a1p5h5xd3a887qidkkaqvx")))

