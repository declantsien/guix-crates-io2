(define-module (crates-io ls b_ lsb_text_png_steganography) #:use-module (crates-io))

(define-public crate-lsb_text_png_steganography-0.1.0 (c (n "lsb_text_png_steganography") (v "0.1.0") (d (list (d (n "image") (r "^0.20") (d #t) (k 0)))) (h "19wyjwsykwkqx76f8r5l1nzs3zd4bi65s5wk7xqsbm64r50ymkdy")))

(define-public crate-lsb_text_png_steganography-0.1.1 (c (n "lsb_text_png_steganography") (v "0.1.1") (d (list (d (n "image") (r "^0.20") (d #t) (k 0)))) (h "1f3frgkdqv49sqrr35k0azfv0pyhzcqix58bbwlv2lpa8zh8v7ip")))

(define-public crate-lsb_text_png_steganography-0.1.2 (c (n "lsb_text_png_steganography") (v "0.1.2") (d (list (d (n "image") (r "^0.20") (d #t) (k 0)))) (h "07vindi7h5hdfqdxsv7rxdhl8vggqsa4wdpzsi0z3ylqdm6wgbwf")))

