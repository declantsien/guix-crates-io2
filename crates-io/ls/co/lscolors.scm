(define-module (crates-io ls co lscolors) #:use-module (crates-io))

(define-public crate-lscolors-0.1.0 (c (n "lscolors") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)))) (h "11xcw0gb3893ics84d5692lwnjazswzdp3dc3n54970l8bi91siy") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.2.0 (c (n "lscolors") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)))) (h "11276hl8zy488hlb8mrs2kgwfyzl2vmb3hciq9y8vbvh5jw659d3") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.2.1 (c (n "lscolors") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1zlwlvi5pbslbpvr0x6gj0mb8smdh9pzqmihn5invi401a05qp9c") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.3.0 (c (n "lscolors") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)))) (h "100jm8wyvahph6h961q8m2kxkclddlj6vzignix2d1j9zs5mlb1r") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.4.0 (c (n "lscolors") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "183s112nhp6zldma8qq330j0d49298nghxh5j7z5pagf1vcm68bm") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.5.0 (c (n "lscolors") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ndmrc7fwhayl2fs498bv0r86g8gnk2wki1yyxa38fbrqgc8z4z9") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.6.0 (c (n "lscolors") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jxsgkn378kxkiqdshdjdclw5wwp2xaz45cqd3yw85fhn8a38fza") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.7.0 (c (n "lscolors") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zp489bfxz0cbldxg8a551wxy3a6ypjrylpd1nbar7hlcwi4axqz") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.7.1 (c (n "lscolors") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vn1824lagf0xdv5rxyl7m9fbrcylyjibmnd4634dnn98m68jjyj") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.8.0 (c (n "lscolors") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zb2r133ynmpdf2wq38wfz5gjx2wwssri4d9j51rlzy422ds82mx") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.8.1 (c (n "lscolors") (v "0.8.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0x4igldjj8fygj59vf9nb63x87vl36sicbr7snk5y0zk4y3qvmcx") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.9.0 (c (n "lscolors") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0qjdvbgfwf20k5qd9ldszlxja4z30nhkf65dvv9glkjxaarj74sf") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.10.0 (c (n "lscolors") (v "0.10.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ri0z6axs8nvjs3bv6npsvy6d2jnyjd5c4wfgrmd2v7cvr747r48") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.11.0 (c (n "lscolors") (v "0.11.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11mcgvdhhnm2jyqp9p4cknx10jy04cm44vrbg7jjgdvkzr702km2") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.11.1 (c (n "lscolors") (v "0.11.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0l55vv1jhkyhibbxrz5v53jm1x5hpfwmyb9vsbify7sk97lh2d9b") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.12.0 (c (n "lscolors") (v "0.12.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1w8h31db3ai10qslv7y6gzv233pq08hram79iy0jwbh9kmsgyjq7") (f (quote (("default" "ansi_term"))))))

(define-public crate-lscolors-0.13.0 (c (n "lscolors") (v "0.13.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wnxs5d004fx71apvh9124xqky0qjjmpibag24km7bvvss2xrpn2") (f (quote (("default" "ansi_term" "nu-ansi-term"))))))

(define-public crate-lscolors-0.14.0 (c (n "lscolors") (v "0.14.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.47") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gx81bj1kr6igh9ybfj2np3r4s6g7sdfpsa3l0pykndn3wfxza8q") (f (quote (("default")))) (r "1.62.1")))

(define-public crate-lscolors-0.15.0 (c (n "lscolors") (v "0.15.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.49") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "039jl8lwcw84cii5zpngs4i7jxhy2pnpjjvynympib8386h1aw5z") (f (quote (("default")))) (r "1.63.0")))

(define-public crate-lscolors-0.16.0 (c (n "lscolors") (v "0.16.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.49") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0yqfym0lfdkj0f7lpvd0a0brljs7cpknyh14c202frcpqfg202xb") (f (quote (("gnu_legacy" "nu-ansi-term/gnu_legacy") ("default" "nu-ansi-term")))) (r "1.63.0")))

(define-public crate-lscolors-0.17.0 (c (n "lscolors") (v "0.17.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.50") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08z5jslgigvnpc1gj2i8r9pi8yn4m0pf8dzf3rk9grdidbzlyc2k") (f (quote (("gnu_legacy" "nu-ansi-term/gnu_legacy") ("default" "nu-ansi-term")))) (r "1.63.0")))

