(define-module (crates-io ls ig lsig) #:use-module (crates-io))

(define-public crate-lsig-0.1.0 (c (n "lsig") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "to-binary") (r "=0.4.0") (d #t) (k 0)))) (h "0v5vagg361ar9s8gsgsp8xmnslsbihddvq15gkf8sqsigvy1jcac")))

(define-public crate-lsig-0.1.1 (c (n "lsig") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "to-binary") (r "=0.4.0") (d #t) (k 0)))) (h "04qvjwn6mqn19qrjmlvwrp026g0346n6gw6q6dn7ykwrfw9ggqih")))

(define-public crate-lsig-0.1.2 (c (n "lsig") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "0kmal2rqswyi8l6pqpnfvkzpivpncnyski5fajiwpmkk12aiakhf")))

(define-public crate-lsig-0.1.3 (c (n "lsig") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "1gic4sdxnghissl0yny2415v4yvf7ijabfil62g5832zi4piihvw")))

