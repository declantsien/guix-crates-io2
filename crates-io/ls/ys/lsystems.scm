(define-module (crates-io ls ys lsystems) #:use-module (crates-io))

(define-public crate-lsystems-0.1.0 (c (n "lsystems") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0d1gpsh7ahqv940ypd2cyx77ynm5ikj18mk1hbw131n33ylj9s0p") (y #t)))

(define-public crate-lsystems-0.2.0 (c (n "lsystems") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0h86236vwlqsm5i5q50qlhp059cxhclw3qk34rxwx4m012zdxljn")))

(define-public crate-lsystems-0.2.1 (c (n "lsystems") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10fjhg7whcrlfa8mpb0nyffj0z18sx4vc0s52k94w7a41frayf8r")))

