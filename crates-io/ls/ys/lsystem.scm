(define-module (crates-io ls ys lsystem) #:use-module (crates-io))

(define-public crate-lsystem-0.1.0 (c (n "lsystem") (v "0.1.0") (h "1hfzxpcbzapklibh31180sdmixxl8l76dyra5i9609x4pmv6x4pz")))

(define-public crate-lsystem-0.2.1 (c (n "lsystem") (v "0.2.1") (h "109afs0n9hk93hh4as5a2aqbqcrjdq14gf77fchaxxd9y8875i13")))

