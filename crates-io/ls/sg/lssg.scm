(define-module (crates-io ls sg lssg) #:use-module (crates-io))

(define-public crate-lssg-0.1.0 (c (n "lssg") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lssg-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)))) (h "1h6hhxp5nhn3zdqibq1fpg0nnflpkg9ivaaxnclvif498d11bmwq")))

