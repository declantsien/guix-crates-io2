(define-module (crates-io ls sg lssg-lib) #:use-module (crates-io))

(define-public crate-lssg-lib-0.1.0 (c (n "lssg-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-extensions") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "0jr0xcm9z8mrbmx7gw9i13gy6smsjik9djxgdy6wd9385jhcbm8y")))

