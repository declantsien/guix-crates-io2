(define-module (crates-io ls tt lstty) #:use-module (crates-io))

(define-public crate-lstty-0.1.0 (c (n "lstty") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "logging") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 0)))) (h "11c5rh254vf6ycxvd43jv3p7chb4h58c3k50876rdfck9qh4qml4")))

