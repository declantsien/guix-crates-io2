(define-module (crates-io ls or lsor-proc-macro) #:use-module (crates-io))

(define-public crate-lsor-proc-macro-0.1.0 (c (n "lsor-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx-macros-core") (r "^0.7") (f (quote ("chrono" "json" "postgres" "uuid"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5qrk7qk95gqji38555fp8xgbbn8rbv5w6cj28gw6b8v72mdhka")))

