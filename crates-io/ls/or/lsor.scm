(define-module (crates-io ls or lsor) #:use-module (crates-io))

(define-public crate-lsor-0.1.0 (c (n "lsor") (v "0.1.0") (d (list (d (n "async-graphql") (r "^7.0") (f (quote ("chrono" "url" "uuid"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "lsor-core") (r "^0.1.0") (d #t) (k 0)) (d (n "lsor-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sqlx") (r "^0.7") (f (quote ("chrono" "json" "macros" "postgres" "runtime-tokio-rustls" "uuid"))) (d #t) (k 2)) (d (n "uuid") (r "^1.7") (f (quote ("v4" "serde"))) (d #t) (k 2)))) (h "0ibbl1ky7xkfq6v99ljrvcs9nxd2zxi540qypss7ylkqzsrpjnfn")))

