(define-module (crates-io ls _r ls_rules) #:use-module (crates-io))

(define-public crate-ls_rules-0.1.0 (c (n "ls_rules") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vf569yd6b8n4a5y9vr6jg88kalx1x44mi4bgy20bgb8dkwjaidx") (y #t)))

(define-public crate-ls_rules-0.2.0 (c (n "ls_rules") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rl9ya3gxl1mvp48sgykj7glk2xjgsa42ni70l8yqdaw2qykxlr4") (y #t)))

(define-public crate-ls_rules-0.3.0 (c (n "ls_rules") (v "0.3.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ak8i6b8lqaq5srlsjpmm6fw29cg6wgqk2jca35gnzdl04s23cg7") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (y #t) (r "1.56")))

(define-public crate-ls_rules-0.3.1 (c (n "ls_rules") (v "0.3.1") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0vyhina9iazsjlic7nfbx454aj1gxpi18i7n3cicmqk0l8ys7wrn") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (y #t) (r "1.56")))

(define-public crate-ls_rules-0.4.0 (c (n "ls_rules") (v "0.4.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1za9achx98misayp3acb0ccccwzqadnd191a4fw9zm3fyp453nkd") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

(define-public crate-ls_rules-0.4.1 (c (n "ls_rules") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "008j2g0q9jxjhfxn5zj1lliy9m49ghl6w3b935mmz05g45k4hly5") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

