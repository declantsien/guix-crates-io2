(define-module (crates-io ls fp lsfp) #:use-module (crates-io))

(define-public crate-lsfp-0.4.0 (c (n "lsfp") (v "0.4.0") (h "1rkj6vcj0q7crm6v149zw4bmckv3n6yyagg25bhb98fjn5m7fmms")))

(define-public crate-lsfp-0.4.1 (c (n "lsfp") (v "0.4.1") (h "1f2q4pixclgmnykgc2rww8pp10dx4y8jffn9i89kwg38aiy9cv0q")))

(define-public crate-lsfp-0.4.2 (c (n "lsfp") (v "0.4.2") (h "1hqd64if2dgvp0130ppadc2r7x5zan85qybi4ifwmfyfnpilc1fi") (f (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

(define-public crate-lsfp-0.4.3 (c (n "lsfp") (v "0.4.3") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "05afyxyfr2iswihkg3kd5ld76w8jrdds1p6d4ydi5w601vvxlgdb") (f (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

(define-public crate-lsfp-0.5.0 (c (n "lsfp") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "11a4nq1z3x340sav6svnrhpkk9r9qxjpr8ggll7k876zjh1p19ni") (f (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

