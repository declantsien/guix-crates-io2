(define-module (crates-io ls l- lsl-sys) #:use-module (crates-io))

(define-public crate-lsl-sys-0.1.0 (c (n "lsl-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "16wwyw52g09hfjqjj8dkgklw8qk3ims6cbz16q6z8rwkz2ci2xpl") (l "lsl")))

(define-public crate-lsl-sys-0.1.1 (c (n "lsl-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "19px9c8swhi96s1a1z7pvvvfk411ns8x6cw0lnxd364pdsz2fskl") (l "lsl")))

