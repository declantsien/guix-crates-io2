(define-module (crates-io ls dj lsdj-tools) #:use-module (crates-io))

(define-public crate-lsdj-tools-0.1.0 (c (n "lsdj-tools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lsdj") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "0q0hn01adyfyxzbxxpbm2dc08z0m17lni77whfvsi5nfzdh1bscs")))

