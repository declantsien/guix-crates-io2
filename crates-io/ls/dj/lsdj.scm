(define-module (crates-io ls dj lsdj) #:use-module (crates-io))

(define-public crate-lsdj-0.1.0 (c (n "lsdj") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "system-interface") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ux") (r "^0.1.4") (d #t) (k 0)))) (h "0lv70c9jmg2lzgxr71jf4q0rb7fzp3ljhhv3bp7mr5rrp2h14aa5")))

