(define-module (crates-io ls ap lsap) #:use-module (crates-io))

(define-public crate-lsap-1.0.0 (c (n "lsap") (v "1.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "0p7ygijlfi4bipxfk5br3x1vh3v8vhd0hzs893vypn76kidjxyyp")))

(define-public crate-lsap-1.0.1 (c (n "lsap") (v "1.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "060vb3s1vz8a0m20arscknans8laixvxzllg5l0f6b12f9165mj5")))

(define-public crate-lsap-1.0.2 (c (n "lsap") (v "1.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "0x57czjnvbf54q9g2442ab329fipdryi82x4cqmg4y32q9hf5x38")))

