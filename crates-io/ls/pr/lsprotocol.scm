(define-module (crates-io ls pr lsprotocol) #:use-module (crates-io))

(define-public crate-lsprotocol-0.1.0-alpha.1 (c (n "lsprotocol") (v "0.1.0-alpha.1") (d (list (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0l32x84gxf4gxmx565bprdc9zzhfwzi2q7q42sr8a6j3l711vb2v") (f (quote (("proposed"))))))

(define-public crate-lsprotocol-0.1.0-alpha.2 (c (n "lsprotocol") (v "0.1.0-alpha.2") (d (list (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0ncv47w9dvkasn6nr5vz2vjr05gak5grmyyfn15yi4cjy6hm2bc2") (f (quote (("proposed"))))))

