(define-module (crates-io ls m3 lsm303agr) #:use-module (crates-io))

(define-public crate-lsm303agr-0.1.0 (c (n "lsm303agr") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "047akay8llwpf7sgp7kzrk1m2rpw6610x9hwxf198j18nl3ipxzi")))

(define-public crate-lsm303agr-0.1.1 (c (n "lsm303agr") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1594rnlhwh5s8bdlwn7g7vd5hfxvbq4ylwfdcn231z03y0pwi46x")))

(define-public crate-lsm303agr-0.2.0 (c (n "lsm303agr") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1qj1dq1vjx2ah4fdncb156igjbb75akxq5xsfz4b6qqnj2cs9332")))

(define-public crate-lsm303agr-0.2.1 (c (n "lsm303agr") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1n83iqrxjdjjbqgzx61v5s0l2cvqj21368bxxw4biy8dd0xqqwr3")))

(define-public crate-lsm303agr-0.2.2 (c (n "lsm303agr") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0kxc69s12q5knsgnw5vx408q37hkv9c88wwwyzlypzaxlz1vic5a")))

(define-public crate-lsm303agr-0.3.0 (c (n "lsm303agr") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1p70ccxrkzxrvqp28ih4zvizmian072l1zvgm173hd857x275ml2")))

(define-public crate-lsm303agr-1.0.0 (c (n "lsm303agr") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "16zysmimk856j87c0p6q2hcvl8rscxh5f858dflrqz6j076km86l")))

