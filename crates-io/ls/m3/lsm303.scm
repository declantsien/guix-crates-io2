(define-module (crates-io ls m3 lsm303) #:use-module (crates-io))

(define-public crate-lsm303-0.1.0 (c (n "lsm303") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "dimensioned") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3") (d #t) (k 0)))) (h "1lmdvkdwfmgqxjhagk4yav1iajf1q7mf6bnswplmc9srq4wdqx40")))

