(define-module (crates-io ls m3 lsm303c) #:use-module (crates-io))

(define-public crate-lsm303c-0.1.0 (c (n "lsm303c") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "04k98fmgzbi3yf15yfddqdnni9szj1z2i46fsaknq446qsyijfgw")))

(define-public crate-lsm303c-0.1.1 (c (n "lsm303c") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1d6p282jmd6p2f5j0qh9n48nrbz0p9g8sdn4zhj43ma9abl542zi")))

(define-public crate-lsm303c-0.1.2 (c (n "lsm303c") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "18nmxkmyqwk3s6ijw1zsswzbazcr2qyz5sjkm39h91pl1x6kx9yy")))

(define-public crate-lsm303c-0.1.3 (c (n "lsm303c") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1fpc8phn8bwyiy436lsnja4pwf458bapciyljxs6sg7d00l55ggc")))

(define-public crate-lsm303c-0.1.4 (c (n "lsm303c") (v "0.1.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "13w1xwkhb98359sy2fz89866l66lszy2hzjvkkcv4v9fylh081hs")))

(define-public crate-lsm303c-0.1.5 (c (n "lsm303c") (v "0.1.5") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1w03508wsy83imxvdv23axvmyanynm1jfz1qbf19il395bxwyi3a")))

(define-public crate-lsm303c-0.2.0 (c (n "lsm303c") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1qqy9l8q8bvmqyqgm0vx1qcvx96pkqfz6qm8nxy1fg0v9g7psiqp")))

