(define-module (crates-io ls m3 lsm303dlhc) #:use-module (crates-io))

(define-public crate-lsm303dlhc-0.1.0 (c (n "lsm303dlhc") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "1538d7irjvljhrpwyhw5k905byjrkv3zvfgfc66ig9xv6dzb9aqy")))

(define-public crate-lsm303dlhc-0.1.1 (c (n "lsm303dlhc") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "1k3476fdlv86zagxfmyxr80in2b13l9305d0r90z7d05byknfpj4")))

(define-public crate-lsm303dlhc-0.1.2 (c (n "lsm303dlhc") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "18hg30p6ph87wlqwi0zfjcaxxhn6pj3wxrzpjfbwdg2g0195q0qb")))

(define-public crate-lsm303dlhc-0.2.0 (c (n "lsm303dlhc") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "01kpwlm58ijigkka0d4ii3lpslr8v3nl0jhd3cfk4l8955f1lpcy")))

