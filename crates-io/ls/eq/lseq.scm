(define-module (crates-io ls eq lseq) #:use-module (crates-io))

(define-public crate-lseq-0.1.0 (c (n "lseq") (v "0.1.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "rug") (r "^1.0.1") (f (quote ("integer" "rand"))) (k 0)) (d (n "skiplist") (r "^0.2.10") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05pwp93fgphb1v90jrknc23gc8wwq81idl6f2hlz5wlb7m0aiyqw")))

