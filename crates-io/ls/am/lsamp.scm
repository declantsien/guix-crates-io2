(define-module (crates-io ls am lsamp) #:use-module (crates-io))

(define-public crate-lsamp-0.1.0 (c (n "lsamp") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.3.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time" "io-std" "io-util"))) (d #t) (k 0)))) (h "1nnqqccimqc7lnlar585vk82kasnx91rfswh5yl1m7qbnqkl8xdv")))

(define-public crate-lsamp-0.1.1 (c (n "lsamp") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duration-str") (r "^0.3.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time" "io-std" "io-util"))) (d #t) (k 0)))) (h "00bnkmgcgbvz8pp7v8kxp50zvsfzv0pyjcw5cvqb8rqymfsbn698")))

(define-public crate-lsamp-0.1.2 (c (n "lsamp") (v "0.1.2") (h "1ga3swwqywy8iwiyj5bxp52nv31laz7nvp8li2ksdmnaalbn7gyz")))

(define-public crate-lsamp-0.1.3 (c (n "lsamp") (v "0.1.3") (h "1a44w610716g5vp4q1m5bcl9iyrrnr7ylpyh2fcs7b806igw2kp2")))

(define-public crate-lsamp-0.1.4 (c (n "lsamp") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "035a2hhbr3w7kd47fhpllz3p6w8iha8g5vw97mdglsjqlbz9npsw")))

