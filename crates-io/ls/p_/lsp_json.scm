(define-module (crates-io ls p_ lsp_json) #:use-module (crates-io))

(define-public crate-lsp_json-0.1.0 (c (n "lsp_json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "119m4wbc31kz1kxdxm8fxghv1diawfs55mzsbyvfwgxc86p2v4vd")))

(define-public crate-lsp_json-0.1.1 (c (n "lsp_json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0a8a4bnfmsxf4xlayvx9nb2ykn0q690zi2rggq1nlmlzyz4lm6vg")))

