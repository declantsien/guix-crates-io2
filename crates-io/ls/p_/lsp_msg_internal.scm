(define-module (crates-io ls p_ lsp_msg_internal) #:use-module (crates-io))

(define-public crate-lsp_msg_internal-0.1.0 (c (n "lsp_msg_internal") (v "0.1.0") (d (list (d (n "lsp_msg_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)))) (h "06s0w1mbb2ylab1qw0wni1yc1m8wx8ljr1pffnx1m42jwrrb5wn7")))

(define-public crate-lsp_msg_internal-0.2.0 (c (n "lsp_msg_internal") (v "0.2.0") (d (list (d (n "lsp_msg_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.90") (d #t) (k 2)) (d (n "spec") (r "^0.2.0") (d #t) (k 0)))) (h "1h8i80607bhj2l02mx552zfzf0h92vqdqf0i4a8krkspj40rrs2f")))

(define-public crate-lsp_msg_internal-0.3.0 (c (n "lsp_msg_internal") (v "0.3.0") (d (list (d (n "lsp_msg_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.90") (d #t) (k 2)) (d (n "spec") (r "^0.2.0") (d #t) (k 0)))) (h "1mbw39m9bj5c61qycsnhb627l84jk4q3hilplq2zxvkm82rq7z8b")))

