(define-module (crates-io ls p_ lsp_msg_derive) #:use-module (crates-io))

(define-public crate-lsp_msg_derive-0.1.0 (c (n "lsp_msg_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "175zdl9jqkc1x6xjbp0fl12wr0lwss6f9rjyimjqlywcrrv030rp")))

(define-public crate-lsp_msg_derive-0.2.0 (c (n "lsp_msg_derive") (v "0.2.0") (d (list (d (n "lsp_msg_internal") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmxyz1l99h5qzcsj9yjv6mg8w30cwxmzssnla6rk316zssbc568")))

(define-public crate-lsp_msg_derive-0.3.0 (c (n "lsp_msg_derive") (v "0.3.0") (d (list (d (n "lsp_msg_internal") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "spec") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0b5b7723f39yi3amlqipwnhg5w7w454d2pjxllmf4iy9sv283fii")))

