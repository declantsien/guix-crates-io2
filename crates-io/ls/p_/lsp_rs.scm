(define-module (crates-io ls p_ lsp_rs) #:use-module (crates-io))

(define-public crate-lsp_rs-0.1.0 (c (n "lsp_rs") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1v6k527a9w74hp7bhwzafi9fmf49ial08myz854763wr2smbhzx7")))

