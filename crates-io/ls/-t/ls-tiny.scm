(define-module (crates-io ls -t ls-tiny) #:use-module (crates-io))

(define-public crate-ls-tiny-0.1.0 (c (n "ls-tiny") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "009rcpq03c0iygv3jvhqkq6jagv9lfpzvxl926s7jb7zfiih2f2b")))

(define-public crate-ls-tiny-0.1.1 (c (n "ls-tiny") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "03l9w0yd9vmnynhzhfirv1xyrysyn2js5kdv1zlmq3bdfd4r8pv5")))

(define-public crate-ls-tiny-0.1.2 (c (n "ls-tiny") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "1rv23wmsfmaqz8s5yhkby5bhl46kgplbs8kzjdb361jnyvwfjwal")))

(define-public crate-ls-tiny-0.1.3 (c (n "ls-tiny") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "1ybfxnasppvyhj8r959fk7mm18c8s7lyzp3amq7f9dnpq8hchv9p")))

(define-public crate-ls-tiny-0.1.4 (c (n "ls-tiny") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "0f6kzxd55i3hr791vpn7rclfnl0nspy5n4r109zhx50haj6bsqsz")))

(define-public crate-ls-tiny-0.1.5 (c (n "ls-tiny") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "1wlzaspsz0pgc09r8n1x8v3vym3r2fflzrrw883vdih89bdhx96b")))

(define-public crate-ls-tiny-0.1.6 (c (n "ls-tiny") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "03ay0fpn2xfkjwj7750sfklnbvvcwg2qpb7khm7ym0hw6xbz9mlg")))

(define-public crate-ls-tiny-0.1.7 (c (n "ls-tiny") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "0cx2jzsl1pdhr9dnz6s575lz7y0v8skp28ykbb40w7ypd9g8w6qi")))

