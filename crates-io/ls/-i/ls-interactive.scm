(define-module (crates-io ls #{-i}# ls-interactive) #:use-module (crates-io))

(define-public crate-ls-interactive-1.7.0 (c (n "ls-interactive") (v "1.7.0") (d (list (d (n "cached") (r "^0.42.0") (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "human-panic") (r "^1.1.3") (d #t) (k 0)) (d (n "lnk") (r "^0.5.1") (d #t) (k 0)) (d (n "open") (r "^4.0.1") (d #t) (k 0)) (d (n "tiny_update_notifier") (r "^2.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "windows") (r "^0.47.0") (f (quote ("Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0w0hcspmlg4bvplz9ww2d53j391h13rmwz93sphkh2skcw00jhp0")))

