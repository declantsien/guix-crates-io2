(define-module (crates-io ls yn lsynth) #:use-module (crates-io))

(define-public crate-lsynth-1.0.0 (c (n "lsynth") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a71jhqc45ircbirz5d5synfn6am6z9cpda3xrwkflyxll433i15")))

(define-public crate-lsynth-1.0.1 (c (n "lsynth") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1bj1va6mprnghb14k3szjqjk95rlvx2vp1ag0bfhxzz1sn07d0")))

