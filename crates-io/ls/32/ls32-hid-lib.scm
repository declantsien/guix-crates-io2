(define-module (crates-io ls #{32}# ls32-hid-lib) #:use-module (crates-io))

(define-public crate-ls32-hid-lib-0.1.0 (c (n "ls32-hid-lib") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "hidapi") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18yai0xj4r2irfbk5mv85qqqfibw8a13s3inqxhy25rhn0ihrvg9")))

(define-public crate-ls32-hid-lib-0.2.0 (c (n "ls32-hid-lib") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "hidapi") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13aa570xpcjx748j6g6641y3vwl910x1wrxsrxcqy4iqcv443v0n")))

(define-public crate-ls32-hid-lib-0.3.0 (c (n "ls32-hid-lib") (v "0.3.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "hidapi") (r "^1.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qc1cqnw6pqvclxi5n3qaiz17vhh7lh4m2clnlzkgrhjc08sm6q4")))

