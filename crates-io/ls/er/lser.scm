(define-module (crates-io ls er lser) #:use-module (crates-io))

(define-public crate-lser-0.1.0 (c (n "lser") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2.2") (d #t) (k 0)))) (h "0ywidm8x7s38gm7hlyyh05ag6gbxingg05k1jlw14pil409hq2sc")))

