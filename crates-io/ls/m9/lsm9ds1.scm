(define-module (crates-io ls m9 lsm9ds1) #:use-module (crates-io))

(define-public crate-lsm9ds1-0.1.0 (c (n "lsm9ds1") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1xx5d4k4z82pxfs0lwlbjkgz3zjk98w3pz493sk3g4gs2fbj400l")))

