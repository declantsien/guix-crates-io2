(define-module (crates-io uf wp ufwprofile) #:use-module (crates-io))

(define-public crate-ufwprofile-0.1.0 (c (n "ufwprofile") (v "0.1.0") (h "1acp4p49ws93p0n4aag2j1q8hfv13d2a93pp6wzgdkrqfdpy3kzv")))

(define-public crate-ufwprofile-0.1.1 (c (n "ufwprofile") (v "0.1.1") (h "1im1cr5nfg1sm4nlnpfiwwmcza1ribb0w3886aciag0wphq1x4vs")))

(define-public crate-ufwprofile-0.1.2 (c (n "ufwprofile") (v "0.1.2") (h "1f9a29g3yswhqfn822aad1ds903zlsnpzrzybbpy9lahysxh1ljb")))

(define-public crate-ufwprofile-0.2.2 (c (n "ufwprofile") (v "0.2.2") (h "1ln9h5x8a7160g5sd0a73anwkdfr2rc11nc1068rb820pzfvlqbc")))

(define-public crate-ufwprofile-0.2.3 (c (n "ufwprofile") (v "0.2.3") (h "058nlr1flwbybisyixbiq3m03xb7q7kgw0csfiaydbnxhk3ya5gb")))

(define-public crate-ufwprofile-0.2.4 (c (n "ufwprofile") (v "0.2.4") (h "03jdiabfmhy2lhcz7r4kj90jmh3257b97vabyz8lz19icxx7r9ac")))

(define-public crate-ufwprofile-1.3.4 (c (n "ufwprofile") (v "1.3.4") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "18za6apvv37kihvvjhqyym3bd6f4pdir4k91zadfadvxr1miz4df")))

(define-public crate-ufwprofile-1.4.5 (c (n "ufwprofile") (v "1.4.5") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "0k0608g0qsf12ijgpjyn3fnqprc054fk6lplnl6d2mx5dignlm4d")))

(define-public crate-ufwprofile-1.4.6 (c (n "ufwprofile") (v "1.4.6") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1zwbmh63g9lqcvmhv8vvla78h0bjsnkjrzn421k3flhnf2k360qn")))

(define-public crate-ufwprofile-1.4.7 (c (n "ufwprofile") (v "1.4.7") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1zj6s2qlnplwjrkcjmfqzwkv0fda9b6xpva8j1kvnb2wl2f402gc")))

