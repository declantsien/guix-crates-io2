(define-module (crates-io uf #{2_}# uf2_block) #:use-module (crates-io))

(define-public crate-uf2_block-0.1.0 (c (n "uf2_block") (v "0.1.0") (d (list (d (n "bitmask") (r "^0.5.0") (k 0)) (d (n "packing") (r "^0.1.0") (d #t) (k 0)))) (h "0jrsbyc5n91rspvgb7b6d4gbzwydirwq99gilx4yzkbblly7kpv8")))

