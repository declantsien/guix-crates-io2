(define-module (crates-io uf #{2c}# uf2conv) #:use-module (crates-io))

(define-public crate-uf2conv-0.1.0 (c (n "uf2conv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "uf2") (r "^0.1.0") (d #t) (k 0)))) (h "1qbcr3y7g8dfrxv7jmws3574gqcqp4m9c1cjyv6xbg82774d410a")))

