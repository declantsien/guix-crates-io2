(define-module (crates-io uf ix ufix) #:use-module (crates-io))

(define-public crate-ufix-0.1.0 (c (n "ufix") (v "0.1.0") (d (list (d (n "hash32") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "184x46dnzknffcij6yx568vlv6vf4k6kwav9p6zcr3rq4g4lln9v") (f (quote (("word8") ("word16") ("i128" "typenum/i128") ("default"))))))

