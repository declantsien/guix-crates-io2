(define-module (crates-io uf tw uftwo) #:use-module (crates-io))

(define-public crate-uftwo-0.1.0 (c (n "uftwo") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "125f373wpl0pvijld50f1ckfc5yk9c826jc3c14k5hwx6i4xm7hh") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-uftwo-0.1.1 (c (n "uftwo") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m9i6hxvh46wc3czih5yxrrxb03p74nwk9xd7im3xdwh95862bfz") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-uftwo-0.1.2 (c (n "uftwo") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "148kql8j8f5wdcm7gpw1fs3njn594snlii7yxrl02vid7lmhqi46") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-uftwo-0.1.3 (c (n "uftwo") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "clap-num") (r "^1.1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p64mdxbgfcgin4d7vd7jp8vq4pp3ar5vd45ajr0ym8cyhd16j8r") (s 2) (e (quote (("defmt-03" "dep:defmt") ("cli" "dep:clap" "dep:anyhow"))))))

