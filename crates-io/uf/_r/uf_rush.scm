(define-module (crates-io uf _r uf_rush) #:use-module (crates-io))

(define-public crate-uf_rush-0.1.0 (c (n "uf_rush") (v "0.1.0") (h "0hy7jdqmwwfpxc5fznhg3jqpvzwikasxv2qlkwkpjbly6ch1lk43")))

(define-public crate-uf_rush-0.1.1 (c (n "uf_rush") (v "0.1.1") (h "18sx8lvkhnx5gx131y4pqyl4xmdvcqwv8vb2vspfczifnrgxydw3")))

(define-public crate-uf_rush-0.2.0 (c (n "uf_rush") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "00mf46501935jggw51yrkixq7wk8z91y4bb0mwwynhsdmgfafggk")))

(define-public crate-uf_rush-0.2.1 (c (n "uf_rush") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "081d1mkik0qk9ihsdflxfifgpm0a4jqbnpbgbzida0ra04ygrqxx")))

