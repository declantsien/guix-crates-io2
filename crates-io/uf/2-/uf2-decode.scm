(define-module (crates-io uf #{2-}# uf2-decode) #:use-module (crates-io))

(define-public crate-uf2-decode-0.1.0 (c (n "uf2-decode") (v "0.1.0") (h "1ib3aw9lv16v11isvjkq3q59vgvvm7zijdcx4qvg0w3x784km4ld")))

(define-public crate-uf2-decode-0.2.0 (c (n "uf2-decode") (v "0.2.0") (h "08q6n1xd9n418sshdngf6a36s35qg5ngjhr0yifs8gvyn8dd8xya")))

