(define-module (crates-io uf ut ufut) #:use-module (crates-io))

(define-public crate-ufut-0.0.0 (c (n "ufut") (v "0.0.0") (h "0pzp9wf7llcgc4z1slrh7rv5h0s3h1rk8q7b22m0pbh4p7ajsiy7")))

(define-public crate-ufut-0.2.0 (c (n "ufut") (v "0.2.0") (d (list (d (n "futures-micro") (r "=0.2.0") (d #t) (k 0)))) (h "0y9dc8j4gfmbvfkqqjz6mmkapl3iw84gingvpglzgagmpa4x4w6i")))

(define-public crate-ufut-0.3.0 (c (n "ufut") (v "0.3.0") (d (list (d (n "futures-micro") (r "=0.3.0") (d #t) (k 0)))) (h "0jqpy50jgf5kjvc0qgj9xisqnhxgz7139qg89yzm3i7b63d1ng03")))

(define-public crate-ufut-0.3.1 (c (n "ufut") (v "0.3.1") (d (list (d (n "futures-micro") (r "=0.3.1") (d #t) (k 0)))) (h "1pprr0mn2aj69r4v2yra9zpsmc0vdq5q42hf9h9vp38x2izxn8f3")))

