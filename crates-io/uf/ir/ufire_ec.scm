(define-module (crates-io uf ir ufire_ec) #:use-module (crates-io))

(define-public crate-ufire_ec-0.9.0 (c (n "ufire_ec") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "1p03rlglqqnm5a4yqka3ygi8apag892na8pp4j68px20hgrn0hq5")))

(define-public crate-ufire_ec-0.9.1 (c (n "ufire_ec") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "0339ij052kypa867h8qsx6lx5zfc0jbd5i4k07ya8qii6lcjbrgs")))

(define-public crate-ufire_ec-0.9.2 (c (n "ufire_ec") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "01mb0psxc9knlw4dvdyrzr0zckxg4s4n4naamz0j20p0nvxylmac")))

(define-public crate-ufire_ec-2.0.0 (c (n "ufire_ec") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "09ry6pn0zhps29jhcgiig1s8swjg7i956mndb9zv14slm3sllcs0")))

