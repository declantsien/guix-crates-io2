(define-module (crates-io uf ir ufire_iso_ise) #:use-module (crates-io))

(define-public crate-ufire_iso_ise-0.9.0 (c (n "ufire_iso_ise") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "0v9amrg2gnl8py2l967f4rp70678zy1sqhrjklkx3hwzjn9b8yv6")))

