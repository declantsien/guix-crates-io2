(define-module (crates-io uf ir ufire_iso_ec) #:use-module (crates-io))

(define-public crate-ufire_iso_ec-0.9.0 (c (n "ufire_iso_ec") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "1aj79g176qqnshcv01mg1b6qs512s3jm3bz8qyy8xjsa1lx1138y")))

(define-public crate-ufire_iso_ec-0.9.1 (c (n "ufire_iso_ec") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "1zhiz313m1qxnfyw9i1y8lcqrhgwppbxad4s4hm3lvwjkjldwhzq")))

(define-public crate-ufire_iso_ec-0.9.2 (c (n "ufire_iso_ec") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "1gala4477bmd0kah6mhf0nc3vm9mn8299pwm64jm2f9bak25jwg0")))

