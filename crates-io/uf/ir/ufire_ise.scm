(define-module (crates-io uf ir ufire_ise) #:use-module (crates-io))

(define-public crate-ufire_ise-0.9.0 (c (n "ufire_ise") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "1vzjywrw1ccns5j331nqfx95fyrx9b5k3j2vp4f8hppvypyi41jg")))

(define-public crate-ufire_ise-0.9.1 (c (n "ufire_ise") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "16zjp0n2fqr5prm3ngy5smk5j945yh0abmnddch358c3x8bd2dcd")))

(define-public crate-ufire_ise-0.9.2 (c (n "ufire_ise") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "1pdqn5wlk1rikqh1zjanh0vskr2raif54bw1r113vwiclhy6vazd")))

(define-public crate-ufire_ise-2.0.0 (c (n "ufire_ise") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "1g53gj8ib0cm6ymjgwdj2drv6m6rkrhksjz625ma8qcv3nkl81kl")))

