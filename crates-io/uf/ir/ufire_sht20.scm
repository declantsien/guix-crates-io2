(define-module (crates-io uf ir ufire_sht20) #:use-module (crates-io))

(define-public crate-ufire_sht20-0.9.0 (c (n "ufire_sht20") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "1z8ax8pc9nw6gyd262g7i61i68bd5vz676sql4z92mbzr8z5swqq")))

(define-public crate-ufire_sht20-0.9.1 (c (n "ufire_sht20") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 0)))) (h "1zhdyrlbi2kcrpza5yb2l3619why3gg9l6w4xyays1nyh5vqz3sh")))

