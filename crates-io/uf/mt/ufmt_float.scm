(define-module (crates-io uf mt ufmt_float) #:use-module (crates-io))

(define-public crate-ufmt_float-0.1.0 (c (n "ufmt_float") (v "0.1.0") (d (list (d (n "micromath") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "1dislfp9qc4zzfyjqy0hkdq8fmj9afp83wk45wliawqb6cxq97x5") (f (quote (("std" "ufmt/std") ("maths" "micromath") ("default" "maths"))))))

(define-public crate-ufmt_float-0.2.0 (c (n "ufmt_float") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("ufmt-impl"))) (d #t) (k 2)) (d (n "micromath") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)))) (h "1d4ps0w8gl8yha0pqh2qclmd96qwkkl8daxrwci7h7ysp1lvv0bi") (f (quote (("std" "ufmt/std") ("maths" "micromath") ("default" "maths"))))))

