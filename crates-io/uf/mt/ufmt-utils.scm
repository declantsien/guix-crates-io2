(define-module (crates-io uf mt ufmt-utils) #:use-module (crates-io))

(define-public crate-ufmt-utils-0.1.0-alpha.1 (c (n "ufmt-utils") (v "0.1.0-alpha.1") (d (list (d (n "heapless") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "1qinas1slwx04vf3xflzs9nc668s5i48xsa2m5r75sq70wd7mlbq") (y #t)))

(define-public crate-ufmt-utils-0.1.0 (c (n "ufmt-utils") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1g5jjzl9dgh8j2ksl828l4vinibhvr8bgh7941bg0jj9r332r80r")))

(define-public crate-ufmt-utils-0.1.1 (c (n "ufmt-utils") (v "0.1.1") (d (list (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 2)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1y9dxv8k09riyc1famy6dzgiv3rzkwdq67cfc67c9m974jxmd3cn")))

(define-public crate-ufmt-utils-0.2.0 (c (n "ufmt-utils") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 2)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "00n9vwzxcvlhgm8xa3xvj18zlxm2klm09zvn3rz19pam8yhvdvhw")))

