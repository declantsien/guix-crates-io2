(define-module (crates-io uf mt ufmt) #:use-module (crates-io))

(define-public crate-ufmt-0.1.0-beta.1 (c (n "ufmt") (v "0.1.0-beta.1") (d (list (d (n "ufmt-macros") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "02hcq2j59m1pqp77yfsvz5p65mvi06nwp08ig099zl1pd9rdcras") (f (quote (("std" "ufmt-write/std")))) (y #t)))

(define-public crate-ufmt-0.1.0-beta.2 (c (n "ufmt") (v "0.1.0-beta.2") (d (list (d (n "ufmt-macros") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "1ac9ik0nmjdqxdkihwb1c196jpchgixwy333anwmh44ykcv3l4xy") (f (quote (("std" "ufmt-write/std")))) (y #t)))

(define-public crate-ufmt-0.1.0-beta.3 (c (n "ufmt") (v "0.1.0-beta.3") (d (list (d (n "ufmt-macros") (r "^0.1.0-beta.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "1w8a817s141hf45yva2pdzr7x9svr77s9g5dgz51r8dfzji8fi3k") (f (quote (("std" "ufmt-write/std")))) (y #t)))

(define-public crate-ufmt-0.1.0-beta.4 (c (n "ufmt") (v "0.1.0-beta.4") (d (list (d (n "ufmt-macros") (r "^0.1.0-beta.3") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "1na690lkxr37sxyci8kdd5xww05sxzfmznxss1bliygnh4znzza9") (f (quote (("std" "ufmt-write/std")))) (y #t)))

(define-public crate-ufmt-0.1.0-beta.5 (c (n "ufmt") (v "0.1.0-beta.5") (d (list (d (n "ufmt-macros") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "0fqpyxk1kj97s0zbc0klz9h84z7w9w4h6sq6ghv08avvx7kg1g16") (f (quote (("std" "ufmt-write/std")))) (y #t)))

(define-public crate-ufmt-0.1.0 (c (n "ufmt") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "ufmt-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1844qwbmc4m69nfi6xmdcdf4fmjjvypi9rpfg3wgilvrxykwwzif") (f (quote (("std" "ufmt-write/std"))))))

(define-public crate-ufmt-0.1.1 (c (n "ufmt") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "ufmt-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1ljjkdwn50hhs46rzknjwj3d23w1rx3ffb77zirnsl857mqm4sx0") (f (quote (("std" "ufmt-write/std"))))))

(define-public crate-ufmt-0.1.2 (c (n "ufmt") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "ufmt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1pbc0gcfi7m21ly00x460pv1k2h12zb184f7wpcckpqj6g3c1lri") (f (quote (("std" "ufmt-write/std"))))))

(define-public crate-ufmt-0.2.0 (c (n "ufmt") (v "0.2.0") (d (list (d (n "ufmt-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "17dbpq5r09mcpalra2hmpgb2ly282s6xjsb4il8fjmrbq1p88r0s") (f (quote (("std" "ufmt-write/std"))))))

