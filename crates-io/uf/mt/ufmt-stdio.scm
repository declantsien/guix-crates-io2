(define-module (crates-io uf mt ufmt-stdio) #:use-module (crates-io))

(define-public crate-ufmt-stdio-0.1.0 (c (n "ufmt-stdio") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "1nchza6800mw7bmihxg6f023kwyp61gs0j8awyfb09937dgkp31l")))

(define-public crate-ufmt-stdio-0.1.1 (c (n "ufmt-stdio") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)))) (h "10v6swx42dljpjh0zdg9jdb77fyhs68q9ag6hkwcrkl0ggyzdi5y")))

(define-public crate-ufmt-stdio-0.1.2 (c (n "ufmt-stdio") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(all(target_arch = \"wasm32\", target_os = \"unknown\")))") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)))) (h "0lsv6cs44c78fz84p3llzw8b2b8fjv6fa07fwdk055pp9rnhc5fg")))

(define-public crate-ufmt-stdio-0.1.3 (c (n "ufmt-stdio") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(all(target_arch = \"wasm32\", target_os = \"unknown\")))") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)))) (h "1izpq80kp9pc22fqgsfniqd6l8xraxfsba8dz4r2c6zsy6pv97a1")))

(define-public crate-ufmt-stdio-0.2.0 (c (n "ufmt-stdio") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(all(target_arch = \"wasm32\", target_os = \"unknown\")))") (k 0)) (d (n "ufmt") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)))) (h "1kgr6kl6gv8xyf5kykhsqgz0q8x0ks9w16aizdasbjlgjmz35757")))

(define-public crate-ufmt-stdio-0.3.0 (c (n "ufmt-stdio") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(all(target_arch = \"wasm32\", target_os = \"unknown\")))") (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)))) (h "1vblbz9hcl4y3dvkkgw2hbcmi2gyfj1c8zd7m2phnxn3bwarkmh4")))

(define-public crate-ufmt-stdio-0.4.0 (c (n "ufmt-stdio") (v "0.4.0") (d (list (d (n "critical-section") (r "^1") (o #t) (d #t) (t "cfg(target_arch = \"riscv32\")") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(not(any(target_arch = \"riscv32\", all(target_arch = \"wasm32\", target_os = \"unknown\"), target_arch = \"mos\")))") (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)))) (h "1hynrl2iz641kbhjmlbfh5ca40dg7ai865nlqmdpgk22i0hdp5mj") (f (quote (("esp-uart" "critical-section") ("esp-jtag" "critical-section"))))))

