(define-module (crates-io uf mt ufmt-write) #:use-module (crates-io))

(define-public crate-ufmt-write-0.1.0-beta.1 (c (n "ufmt-write") (v "0.1.0-beta.1") (h "0xpnlfawvw01z8kzgx80vril9p7hks2nc6ja10hryv3b2kvmcjy0") (f (quote (("std")))) (y #t)))

(define-public crate-ufmt-write-0.1.0 (c (n "ufmt-write") (v "0.1.0") (h "0sdx0r6ah9xr3nydrqxj01v25sb956c0jk5rqf6f5i9fnkb2wyp8") (f (quote (("std"))))))

