(define-module (crates-io uf mt ufmt-macros) #:use-module (crates-io))

(define-public crate-ufmt-macros-0.1.0-beta.1 (c (n "ufmt-macros") (v "0.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1cfz0r4p6bmd1zf6vzkma00cr56plcf1bapfj9spk2bzl4v4rqgs") (y #t)))

(define-public crate-ufmt-macros-0.1.0-beta.2 (c (n "ufmt-macros") (v "0.1.0-beta.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1x2ycszmpalkb5q42mv1yai262hdf40sw0a4wvsmmaysz1bb0zkn") (y #t)))

(define-public crate-ufmt-macros-0.1.0-beta.3 (c (n "ufmt-macros") (v "0.1.0-beta.3") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6x9hkpkidk4kg8pi0ibnlqm3b3fcn4lk6g6k2gif62j32cshbl") (y #t)))

(define-public crate-ufmt-macros-0.1.0-beta.4 (c (n "ufmt-macros") (v "0.1.0-beta.4") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2yzmvxwn22xz59m3a5zihpiwqjrypmlqaa7ynynj05fp9zr0qs") (y #t)))

(define-public crate-ufmt-macros-0.1.0 (c (n "ufmt-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02w9bzqygahlsys8s6j8fy1r0q1c2w31xj3w09c3x1wsra7r1qap")))

(define-public crate-ufmt-macros-0.1.1 (c (n "ufmt-macros") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sf0z9f6kjw5h15xd1hlj46dgri59lqwin1fxrcdradzl8s3x0gd")))

(define-public crate-ufmt-macros-0.1.2 (c (n "ufmt-macros") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ay6m0k1nbi7g1r01y193a37a2sy37lb1f9ayn14gm6jv0jaq1yy")))

(define-public crate-ufmt-macros-0.2.0 (c (n "ufmt-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nfpgpimg04n08cxi0mcsz803kh3kzpmllmxm2a6768cyf96raz4")))

(define-public crate-ufmt-macros-0.3.0 (c (n "ufmt-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05ipcslk5bcpkw3j8405hhzxibr9wkn8sg33nif1cjblc6zd6dyk")))

