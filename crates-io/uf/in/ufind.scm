(define-module (crates-io uf in ufind) #:use-module (crates-io))

(define-public crate-ufind-0.1.0 (c (n "ufind") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "066wkfik2k2idyxwvix26wh5v3wjp5m8xsnj39g6168rzb9nfn72")))

(define-public crate-ufind-0.2.0 (c (n "ufind") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 1)))) (h "1vp3l0gy5sqzp2iwv19wdrhypjb5zs31vlwzz4zr7p3xbs5znqcn")))

(define-public crate-ufind-0.3.0 (c (n "ufind") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 1)))) (h "0qjvvh606945cv616vdyvylaxlgc0h3xi70k1r27qxj1xyrpg2d5")))

(define-public crate-ufind-0.3.1 (c (n "ufind") (v "0.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 1)))) (h "12z995h6lgdxlf74jshw5gm6c9308ph2738gq3vlzwhry14sxmnl")))

