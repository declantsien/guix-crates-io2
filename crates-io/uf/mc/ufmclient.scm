(define-module (crates-io uf mc ufmclient) #:use-module (crates-io))

(define-public crate-ufmclient-0.1.0 (c (n "ufmclient") (v "0.1.0") (h "0swnvrp4lywzvn32c6anq278gxn067j2na1glfcqpbh0j3c25ypa")))

(define-public crate-ufmclient-0.1.1 (c (n "ufmclient") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r41kdyg73bimfzh7rb7lc6s9828wr7vp2sjlwmhg03jsz7jh0j9")))

(define-public crate-ufmclient-0.1.2 (c (n "ufmclient") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cylxxg9vglfmsjkw6si5lqxmi9ps9lajc9nac1rnklspk7j2zi0")))

