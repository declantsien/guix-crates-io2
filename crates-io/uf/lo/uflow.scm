(define-module (crates-io uf lo uflow) #:use-module (crates-io))

(define-public crate-uflow-0.3.0 (c (n "uflow") (v "0.3.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0z153ginnygw71l3k1ddxn4dkcvad705i9yfy2rbykp02rv001b3") (y #t)))

(define-public crate-uflow-0.3.1 (c (n "uflow") (v "0.3.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0cj2mjflrhqyccjpb72id1r1647a1apv7dy7bhajlnzjm4dh08hk") (y #t)))

(define-public crate-uflow-0.3.2 (c (n "uflow") (v "0.3.2") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1jn6z7g9q8hp4n05hdhz5ib4bd2hkc7bl1gaaxclyc2mvmlygcqi")))

(define-public crate-uflow-0.4.0 (c (n "uflow") (v "0.4.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "13xzhm52s1r23y2bdgxpgz8dsc7k3rcd9r940jjn3774bwx06ajs")))

(define-public crate-uflow-0.5.0 (c (n "uflow") (v "0.5.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ki8k3jk844g33q2zy3hj2b5x23c64l1dikbiw9m71nzvfvf5gqn") (y #t)))

(define-public crate-uflow-0.5.1 (c (n "uflow") (v "0.5.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "10ai8wf0d27hz7x8fcv56fchz8zs0a1iyhdz3x8jk2avpmqczs2j")))

(define-public crate-uflow-0.6.0 (c (n "uflow") (v "0.6.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1syliflcndg49zxxqldjs9rb4snjv9vdfj6cp034smbayqylc3ys")))

(define-public crate-uflow-0.6.1 (c (n "uflow") (v "0.6.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0d1fag3yyg0jigddj5mavw39pkjxxyjnhl77xsw00fcx9sflf98w")))

(define-public crate-uflow-0.7.0 (c (n "uflow") (v "0.7.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "02isvnzk6dwyp8bv36vjs7n1959z2sdab4mzninf6cm671lmsyg8")))

(define-public crate-uflow-0.7.1 (c (n "uflow") (v "0.7.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1dj6pr2gbxyd0m2fi3xrl8b0n7jjsj5w5his6fq5fshh3hfdgr1v")))

