(define-module (crates-io uf bx ufbx) #:use-module (crates-io))

(define-public crate-ufbx-0.1.0 (c (n "ufbx") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0v4179zvv1vwqxgksv94yqh3krm4zyp7y7mzr5bjbbl3w14ywwgm")))

(define-public crate-ufbx-0.1.1 (c (n "ufbx") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hvp5h7mlpkjwc5dgjddf6n67r7dxdf9nwsnbba4bdlhzarcshxf")))

(define-public crate-ufbx-0.1.2 (c (n "ufbx") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1y4claj4darr2ypyyyv36iywvl1cfgi5sxd9f8sv6fygvh8phpdp")))

(define-public crate-ufbx-0.1.3 (c (n "ufbx") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0a3l1nc39avj49cag8kkxhr556205561widhjd6fkvvbiik1pmsm")))

(define-public crate-ufbx-0.2.0 (c (n "ufbx") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "147fs4pkh4hh287hkdswayyjf3cfnzzv2jlf1acjs39vq41xyjjj")))

(define-public crate-ufbx-0.3.0 (c (n "ufbx") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ly4f2y7yy11hg9x42vf54m1x7xq6cylxwpjg94lvncjjwav6ahk")))

(define-public crate-ufbx-0.4.0 (c (n "ufbx") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "0744fhshsf39q4v1ay3k11hxy7cfv556h73031nq1biwjqw1kgn5") (f (quote (("nightly"))))))

(define-public crate-ufbx-0.5.0-integration (c (n "ufbx") (v "0.5.0-integration") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "01m66l3r5ybjcsf7y78ai2rlzcxfqsxcwdzwhkjp9wvccja9bbas") (f (quote (("nightly"))))))

(define-public crate-ufbx-0.5.0 (c (n "ufbx") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "1bajcx9rhwhs4ws4jiigh2d85spl6vl391fgxlvg3hj613cf757q") (f (quote (("nightly"))))))

(define-public crate-ufbx-0.6.0-integration (c (n "ufbx") (v "0.6.0-integration") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "0mxk2913nlri6xb879qsngnzp9bb9klmz10b3s69rryq6h54fm68") (f (quote (("nightly"))))))

(define-public crate-ufbx-0.6.0 (c (n "ufbx") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "05388478qp382hlf8lciqiv4q6mxw0sihz383k9vmxb6886kz6ki") (f (quote (("nightly"))))))

(define-public crate-ufbx-0.6.1 (c (n "ufbx") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (o #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)))) (h "17miw59m5s14714n6ibs9a8kfmlghzazj95pi4lgp8z3awpd827v") (f (quote (("nightly"))))))

