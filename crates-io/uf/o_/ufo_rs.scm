(define-module (crates-io uf o_ ufo_rs) #:use-module (crates-io))

(define-public crate-ufo_rs-0.1.0 (c (n "ufo_rs") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)))) (h "1cac532042f35gswkjrzyb0ycrvr9gg4xhrjq5788x097fgvf75a")))

(define-public crate-ufo_rs-0.1.1 (c (n "ufo_rs") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)))) (h "0lhdx7rz1pl0nfc62cx9clls8vpblnlxzw95aq2hwl53ncszv970")))

