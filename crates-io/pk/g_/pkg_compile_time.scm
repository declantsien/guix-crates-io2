(define-module (crates-io pk g_ pkg_compile_time) #:use-module (crates-io))

(define-public crate-pkg_compile_time-0.1.1 (c (n "pkg_compile_time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "15lj46k64va32y39j57dfmmp5kcmgrihgjnqlvc70ap6rvb984h8")))

(define-public crate-pkg_compile_time-0.1.2 (c (n "pkg_compile_time") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "1i12ws6baycsfv5xaf20skw3jqavc3psgzcxap4kszkpmqqgs3nd")))

(define-public crate-pkg_compile_time-0.1.3 (c (n "pkg_compile_time") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "time") (r "^0.2.22") (d #t) (k 0)))) (h "14zrrb3j2bl8sh6i933yml0hw3qc3v1arcm3pwh46dqkzryhx5z8")))

