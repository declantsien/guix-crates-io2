(define-module (crates-io pk g_ pkg_manager) #:use-module (crates-io))

(define-public crate-pkg_manager-0.1.0 (c (n "pkg_manager") (v "0.1.0") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0lxc33h30kfjv1lqr0gm3npr9vv91k6zrgj5f6ak3405rax9nlv3")))

(define-public crate-pkg_manager-0.1.1 (c (n "pkg_manager") (v "0.1.1") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "1vilcaca6mfj1mszzdd2p6w1a5nnvn7jir98x7spx2ccy3hlfjwx")))

