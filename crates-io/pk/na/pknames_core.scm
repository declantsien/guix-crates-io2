(define-module (crates-io pk na pknames_core) #:use-module (crates-io))

(define-public crate-pknames_core-0.1.0 (c (n "pknames_core") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "burn") (r "^0.10.0") (f (quote ("ndarray" "train-minimal" "autodiff"))) (d #t) (k 0)) (d (n "pkarr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "zbase32") (r "^0.1.2") (d #t) (k 0)))) (h "0wmh69cxanxzyph03bzdarpmqm8iz9ad1w5lmls0lb349amhh0k9")))

(define-public crate-pknames_core-0.1.1 (c (n "pknames_core") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "burn") (r "^0.10.0") (f (quote ("ndarray" "train-minimal" "autodiff"))) (d #t) (k 0)) (d (n "pkarr") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "zbase32") (r "^0.1.2") (d #t) (k 0)))) (h "0hs0v4ik991kyp794c4zkbsky2dfdiq0cq38a4rxr3li94dp70nh")))

