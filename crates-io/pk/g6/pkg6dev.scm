(define-module (crates-io pk g6 pkg6dev) #:use-module (crates-io))

(define-public crate-pkg6dev-0.0.1-placeholder (c (n "pkg6dev") (v "0.0.1-placeholder") (h "0rxn45wnbj11383jn4hdhdbz88dva4hdm352hdwnwc9z25didcqb")))

(define-public crate-pkg6dev-0.1.0 (c (n "pkg6dev") (v "0.1.0") (d (list (d (n "clap") (r "~3.0.0-beta.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "libips") (r "^0.1.0") (d #t) (k 0)))) (h "0gdfa8lcsa1qn11p8fvj5i316zy3ir95bnp9wg1kw8768fiaymi7")))

(define-public crate-pkg6dev-0.1.1 (c (n "pkg6dev") (v "0.1.1") (d (list (d (n "clap") (r "~3.0.0-beta.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "libips") (r "^0.1.1") (d #t) (k 0)) (d (n "userland") (r "^0.1.1") (d #t) (k 0)))) (h "1anmxdcqwg4g96nxn5miz2j6fk5vrcslj83hninhrdq7wi27hb4w")))

