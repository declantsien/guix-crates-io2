(define-module (crates-io pk gb pkgbuild) #:use-module (crates-io))

(define-public crate-pkgbuild-0.1.0 (c (n "pkgbuild") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pkgspec") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3") (d #t) (k 2)))) (h "0kmms8klw2ihw0lz7c3mscf85pdg9lk8idc7ki46cb8lfn2jsl2d") (y #t)))

(define-public crate-pkgbuild-0.1.1 (c (n "pkgbuild") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pkgspec") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3") (d #t) (k 2)))) (h "04h5ik9slhsd71m45jabq1zmbwzydlma7g7zablcw1jshqp2rq53") (y #t)))

