(define-module (crates-io pk il pkill) #:use-module (crates-io))

(define-public crate-pkill-1.0.0 (c (n "pkill") (v "1.0.0") (d (list (d (n "heim") (r "^0.1.0-alpha.1") (f (quote ("process" "runtime-tokio"))) (k 0)) (d (n "skim") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full" "macros"))) (d #t) (k 0)))) (h "1p7102jsd78waxmcp6m5v85dd3lnarkwkb6mdhzqd1vmvrfvsdha") (y #t)))

