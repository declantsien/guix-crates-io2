(define-module (crates-io pk ga pkgar-repo) #:use-module (crates-io))

(define-public crate-pkgar-repo-0.1.0 (c (n "pkgar-repo") (v "0.1.0") (d (list (d (n "pkgar") (r "^0.1.7") (d #t) (k 0)) (d (n "pkgar-core") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "sodiumoxide") (r "=0.2.6") (k 0)))) (h "01n34zf6xc29fasvj270i2i7ba99c4nnjwxvyd8cq544rdaiv0fb")))

(define-public crate-pkgar-repo-0.1.11 (c (n "pkgar-repo") (v "0.1.11") (d (list (d (n "pkgar") (r "^0.1.11") (d #t) (k 0)) (d (n "pkgar-core") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (k 0)))) (h "1fxjl409smsam00nqkdpzwhr8zs0csym5lp9hm34ykcfa10lw71x")))

(define-public crate-pkgar-repo-0.1.12 (c (n "pkgar-repo") (v "0.1.12") (d (list (d (n "pkgar") (r "^0.1.12") (d #t) (k 0)) (d (n "pkgar-core") (r "^0.1.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (k 0)))) (h "0f8bskf8660f504dx7631nsh6bbi5jvq5058fz3zhw5kxnwxn940")))

(define-public crate-pkgar-repo-0.1.13 (c (n "pkgar-repo") (v "0.1.13") (d (list (d (n "pkgar") (r "^0.1.13") (d #t) (k 0)) (d (n "pkgar-core") (r "^0.1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (k 0)))) (h "0m2kzgbz343gi5ff0x1d8yky0rnm100g6pl2lgyiai4b78alwkql")))

