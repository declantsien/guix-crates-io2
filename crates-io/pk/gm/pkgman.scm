(define-module (crates-io pk gm pkgman) #:use-module (crates-io))

(define-public crate-pkgman-0.2.0 (c (n "pkgman") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "07zpisdl9s1ykyrngvwqs9yhgcwysn816sr4vzwi912i34fpn4h4") (y #t)))

(define-public crate-pkgman-0.2.1 (c (n "pkgman") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "04mv7g6m97g5w6nzjd7ppsaizc549ayp95nihhgj1r07fmyc1f20") (y #t)))

(define-public crate-pkgman-0.2.2 (c (n "pkgman") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0kx23widhsycwp6w6xycb5vnj6ifamj7y7mfxmbdxw55y6rs5xfy") (y #t)))

