(define-module (crates-io pk g- pkg-utils) #:use-module (crates-io))

(define-public crate-pkg-utils-0.1.0 (c (n "pkg-utils") (v "0.1.0") (d (list (d (n "display_derive") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "0wfh83qswsliy7wqg67ds8gx335qfj1mj22wzhk1mjc4swwj2x6j")))

