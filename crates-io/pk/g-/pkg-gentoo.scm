(define-module (crates-io pk g- pkg-gentoo) #:use-module (crates-io))

(define-public crate-pkg-gentoo-0.0.1 (c (n "pkg-gentoo") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "03kw9gzh0dafikb1bnir6mij54nrjaz6c7cw4ah5dx3sy75v8rs1")))

(define-public crate-pkg-gentoo-0.0.2 (c (n "pkg-gentoo") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0n14sxkcvn5sccnzfr9x4lj4ckzz3g27xw4jalrmb8z78r34a7yf")))

(define-public crate-pkg-gentoo-0.0.3 (c (n "pkg-gentoo") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)))) (h "1m54z3avbh8sxi4a789cyxglfws3x585m7y6xj88638ac12n1fzk")))

(define-public crate-pkg-gentoo-0.0.4 (c (n "pkg-gentoo") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)))) (h "0lgi5hnql3x0pldxchdi8qj1rbk5x88qs1mfs5kj2xq8ry4r6c80")))

(define-public crate-pkg-gentoo-0.0.5 (c (n "pkg-gentoo") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0651v9sqkrc9h04vkcl24ir23mplc6b1lbal9mbq2qf8mf21d5r4")))

(define-public crate-pkg-gentoo-0.0.7 (c (n "pkg-gentoo") (v "0.0.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "05lfzr52d77530a8acm15dh31k7kwafgqwy2xz7xidalds8ixgyv") (f (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.0.8 (c (n "pkg-gentoo") (v "0.0.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "1ggxbyvhgvyc2jzfisn83d93ajg6zjdazfmk18rqvjk0w3hnpgbl") (f (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.0.9 (c (n "pkg-gentoo") (v "0.0.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0i92fcys1im47djs6s14wn4s7zs0pdld3n9b8macjw2jyg6ydj6f") (f (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.1.0 (c (n "pkg-gentoo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.4") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0yyzv8dzdwpixm3rz2nh9yyb7sxw5ph6vhk46226whp1mcqnzgqp") (f (quote (("gentoolkit"))))))

