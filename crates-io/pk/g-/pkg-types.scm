(define-module (crates-io pk g- pkg-types) #:use-module (crates-io))

(define-public crate-pkg-types-0.1.0 (c (n "pkg-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1h7xjzrf34fbk3bznr9qlqfryawslbymg6s1wyb8zcpmq44n6yla")))

