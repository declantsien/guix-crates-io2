(define-module (crates-io pk g- pkg-config) #:use-module (crates-io))

(define-public crate-pkg-config-0.0.1 (c (n "pkg-config") (v "0.0.1") (h "0bfhjb74cbcjfpmczqslybdswzz39mr71p6ynzkaag659q2994xj")))

(define-public crate-pkg-config-0.1.0 (c (n "pkg-config") (v "0.1.0") (h "0b4k8dlwhn1rrf08fh4fq12hf7f4w2ns0n7pz4mbs6p0w4dpzj97")))

(define-public crate-pkg-config-0.1.1 (c (n "pkg-config") (v "0.1.1") (h "13za5amy913dqxbzb9zmlg591xwcifxpw4vssn7gxcx20q46isnq")))

(define-public crate-pkg-config-0.1.2 (c (n "pkg-config") (v "0.1.2") (h "1y17hbpq6vdfj70jjzhykqiff54znn5qbrb8rrsw39c97kf9anf2")))

(define-public crate-pkg-config-0.1.3 (c (n "pkg-config") (v "0.1.3") (h "11lvddxmhppww1dbyjvbscm77nd3v153p8qj0x1nlk9ljkc3yd89")))

(define-public crate-pkg-config-0.1.4 (c (n "pkg-config") (v "0.1.4") (h "0kb529ad14nvl0n2ddliwqwp4fr1h778z3fz3r9qqyb55yj7jqr2")))

(define-public crate-pkg-config-0.1.5 (c (n "pkg-config") (v "0.1.5") (h "084g2kdnvnbb871qrgxabqh5fnh0pm06iygw9iciy08jq2v3x4r5")))

(define-public crate-pkg-config-0.1.6 (c (n "pkg-config") (v "0.1.6") (h "1dy3l4asjilppbfigykdnyh4dfwm026y2qh5cg5kp0c3bs9s4yyj")))

(define-public crate-pkg-config-0.1.7 (c (n "pkg-config") (v "0.1.7") (h "07q644m1r5wxm83flpg9y8lwczzk4sn61v4mhrxd386n6pvw177b")))

(define-public crate-pkg-config-0.2.0 (c (n "pkg-config") (v "0.2.0") (h "0mhz9w9lm6m7qpmnmyiqlamzc15k6gm1i42b5gx7dqggb0gfw479")))

(define-public crate-pkg-config-0.2.1 (c (n "pkg-config") (v "0.2.1") (h "0y2l9agsv34jgq4xgy16jpw6dvj9mhnx7ds2h8606wzsn66di3p3")))

(define-public crate-pkg-config-0.2.2 (c (n "pkg-config") (v "0.2.2") (h "0i5vdfkv0yxymj54b2f4arx2hvigrkyhv60sm3rdnjdryy1sjmqd")))

(define-public crate-pkg-config-0.3.0 (c (n "pkg-config") (v "0.3.0") (h "0fqivyv0hkbw6snbs1avs3gk93jj7v8i901kyv6havsjq22msl0r")))

(define-public crate-pkg-config-0.3.1 (c (n "pkg-config") (v "0.3.1") (h "095504zj76dxcddlv8xfbg6x68x1f4ynay2laqk6w1vvp6ba1rca")))

(define-public crate-pkg-config-0.3.2 (c (n "pkg-config") (v "0.3.2") (h "1mlz6qb3j875siya4as2waharlmz3p265f2d69j3nzvsfqxmdx3d")))

(define-public crate-pkg-config-0.3.3 (c (n "pkg-config") (v "0.3.3") (h "0qc3yj7n3yawrjfink9fzxvpb6rsyb8csirxhnaa5ljqiknchynr")))

(define-public crate-pkg-config-0.3.4 (c (n "pkg-config") (v "0.3.4") (h "11dlhx4xbzisrahpiivp93asg2mxsrq7djdqm5akjg88v22md7qb")))

(define-public crate-pkg-config-0.3.5 (c (n "pkg-config") (v "0.3.5") (h "15f45i57nsn1kbsvzyxmsag0arksgb3zxawjniizqckixh38413i")))

(define-public crate-pkg-config-0.3.6 (c (n "pkg-config") (v "0.3.6") (h "0byv7s8al3hwyrawj5vj02ps1w5cv7m3c47wlyiig92cwayq09p2")))

(define-public crate-pkg-config-0.3.7 (c (n "pkg-config") (v "0.3.7") (h "1spb4a9l23csnzgkza2nsih5i3xkrcnxg6ammr2v8ag0ixdmi8an")))

(define-public crate-pkg-config-0.3.8 (c (n "pkg-config") (v "0.3.8") (h "1ypj4nj2z9z27qg06v3g40jyhw685i3l2wi098d21bvyri781vlc")))

(define-public crate-pkg-config-0.3.9 (c (n "pkg-config") (v "0.3.9") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "00x9vc7667m4r8sn8idgpmj9yf1ih6bj1cdrshf1mkb5h5mlr2rs")))

(define-public crate-pkg-config-0.3.10 (c (n "pkg-config") (v "0.3.10") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0f85girr3pqa7abkidwwfym0r6v997jgzwv3n6ahknva8sbyj9q5")))

(define-public crate-pkg-config-0.3.11 (c (n "pkg-config") (v "0.3.11") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0bs5a8zysyhynmvbqz09jxw9kk4bcvjpycllcbsp7frxb7imw38i")))

(define-public crate-pkg-config-0.3.12 (c (n "pkg-config") (v "0.3.12") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "05mdsmd4wa9bd98hlzzslrydh1ky283sp1740zp0ai9mr3dy8lka")))

(define-public crate-pkg-config-0.3.13 (c (n "pkg-config") (v "0.3.13") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0id7xbza5a799jz66l2qqgd2216b1xih61yvfsy3q8c33jm30ihh")))

(define-public crate-pkg-config-0.3.14 (c (n "pkg-config") (v "0.3.14") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "135ia995lqzr0gxpk85h0bjxf82kj6hbxdx924sh9jdln6r8wvk7")))

(define-public crate-pkg-config-0.3.15 (c (n "pkg-config") (v "0.3.15") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1byjfivxlpbh549scss9kp893pzwfig93w14bwxxn557lp7x5hd7")))

(define-public crate-pkg-config-0.3.16 (c (n "pkg-config") (v "0.3.16") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1slikl3p0qbxy37crxynz7zznaf5gzl7ag9w0fyp17zlj06kgmbj")))

(define-public crate-pkg-config-0.3.17 (c (n "pkg-config") (v "0.3.17") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0xynnaxdv0gzadlw4h79j855k0q7rj4zb9xb1vk00nc6ss559nh5")))

(define-public crate-pkg-config-0.3.18 (c (n "pkg-config") (v "0.3.18") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0cxc4yd9qb40944a2svgci41bws68f1hqvyljhrldwbadda94r6k")))

(define-public crate-pkg-config-0.3.19 (c (n "pkg-config") (v "0.3.19") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0k4860955riciibxr8bhnklp79jydp4xfylwdn5v9kj96hxlac9q")))

(define-public crate-pkg-config-0.3.20 (c (n "pkg-config") (v "0.3.20") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1jz6qlr0k0nz5bxijdba6bznll98zkfnqx763swr6y1qni0i16vw")))

(define-public crate-pkg-config-0.3.21 (c (n "pkg-config") (v "0.3.21") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0vg6ysfj5bxaf3c5dfaqmy1laf2d231qd8s0w3469gpccjxzrqhh")))

(define-public crate-pkg-config-0.3.22 (c (n "pkg-config") (v "0.3.22") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "07vy6mn0q6k2adrs7min3rpy999q7kprph0vb1414iwlybs5sa8j")))

(define-public crate-pkg-config-0.3.23 (c (n "pkg-config") (v "0.3.23") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0ginjbmkks5avg4cmv566rzm9ahr108bz5yg2bjz7wfp1m7ym8yi")))

(define-public crate-pkg-config-0.3.24 (c (n "pkg-config") (v "0.3.24") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1ghcyjp5537r7qigmgl3dj62j01arlpddaq93a3i414v3iskz2aq")))

(define-public crate-pkg-config-0.3.25 (c (n "pkg-config") (v "0.3.25") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1bh3vij79cshj884py4can1f8rvk52niaii1vwxya9q69gnc9y0x")))

(define-public crate-pkg-config-0.3.26 (c (n "pkg-config") (v "0.3.26") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0q2i61dhqvawc51zfzl3jich57w0cjgfa894hn6ings7ffgsbjba")))

(define-public crate-pkg-config-0.3.27 (c (n "pkg-config") (v "0.3.27") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0r39ryh1magcq4cz5g9x88jllsnxnhcqr753islvyk4jp9h2h1r6") (r "1.30")))

(define-public crate-pkg-config-0.3.28 (c (n "pkg-config") (v "0.3.28") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "16kgffwncx5hsppsdf54z6jnjkhwywqy601cxk3rqncyi9zmilv9") (r "1.30")))

(define-public crate-pkg-config-0.3.29 (c (n "pkg-config") (v "0.3.29") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1jy6158v1316khkpmq2sjj1vgbnbnw51wffx7p0k0l9h9vlys019") (r "1.30")))

(define-public crate-pkg-config-0.3.30 (c (n "pkg-config") (v "0.3.30") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1v07557dj1sa0aly9c90wsygc0i8xv5vnmyv0g94lpkvj8qb4cfj") (r "1.30")))

