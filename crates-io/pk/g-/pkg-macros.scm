(define-module (crates-io pk g- pkg-macros) #:use-module (crates-io))

(define-public crate-pkg-macros-3.0.0 (c (n "pkg-macros") (v "3.0.0") (d (list (d (n "git2") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "1xmspi4jvlawn41773mvzw9xbkp5dz8hnch3q1vc98r3294jdv6h") (f (quote (("git" "build" "git2") ("default") ("build"))))))

