(define-module (crates-io pk ce pkce) #:use-module (crates-io))

(define-public crate-pkce-0.1.0 (c (n "pkce") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0d2zb65809mjbz8hsqb7cky6ryag5ysifn5lz4pvaqkjx2wg7laf")))

(define-public crate-pkce-0.1.1 (c (n "pkce") (v "0.1.1") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0wpp66kplxc5lca9fsxgiwn7am9ksrlqqwk2wagwlik8j63nczpz")))

(define-public crate-pkce-0.2.0 (c (n "pkce") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1ly2352pkq0f0yf2pmg8j98ysw1idp9jizhlr44xx0mxmvidna72") (s 2) (e (quote (("js" "dep:getrandom"))))))

