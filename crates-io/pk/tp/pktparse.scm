(define-module (crates-io pk tp pktparse) #:use-module (crates-io))

(define-public crate-pktparse-0.1.0 (c (n "pktparse") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "1szfazaxn7pkr2yy82dhcn4cl8sl1zd41ddwrkibrs3rj4b6zdc7")))

(define-public crate-pktparse-0.2.0 (c (n "pktparse") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^2.2") (d #t) (k 0)))) (h "0x7d8bzzyf86angvxww6xvlrzr49iqwrb85nab43sz4p7bvmf7lx")))

(define-public crate-pktparse-0.2.1 (c (n "pktparse") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0biba0kdjnhvc3d3khfg0kn4fjc42lgzfczkladw65d92m7vcjv2")))

(define-public crate-pktparse-0.2.2 (c (n "pktparse") (v "0.2.2") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bq35b0vd3kq9b573rfr5ggxzfz32cz420vhy443bs9c603igdhi") (f (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.2.3 (c (n "pktparse") (v "0.2.3") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "142b2fml8sdk23n4kr0kjcqnagzl6ycb7vdbsla57v3mp94b8mjp") (f (quote (("derive" "serde" "serde_derive")))) (y #t)))

(define-public crate-pktparse-0.3.0 (c (n "pktparse") (v "0.3.0") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "096y97j32ylhcc099iz1kay0p16cdl3l01mwsr371l6k4q22qw3h") (f (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.4.0 (c (n "pktparse") (v "0.4.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fb04kscjccd0zvqrigfizig7vpyg7n0hlrdq8k3amyfml6x4ghz") (f (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.4.1 (c (n "pktparse") (v "0.4.1") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qif4556q7zx17cs5smh3zr7h9pjlq8l4qsz8yhxyri370yirj0r") (f (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.5.0 (c (n "pktparse") (v "0.5.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17a78c29l070mlwjdr3bdph785c9v5rps0if3v0s21w4nwaybhck")))

(define-public crate-pktparse-0.6.0 (c (n "pktparse") (v "0.6.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1barfz96pp7xh65d6vvhq2rd9vjcqdqkhzx9afs2gdylij923sxa")))

(define-public crate-pktparse-0.6.1 (c (n "pktparse") (v "0.6.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iqaap6ql02q103papms150sn8kilbanh12qvp8rg24935w04sl1")))

(define-public crate-pktparse-0.7.0 (c (n "pktparse") (v "0.7.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ri1glnajfw0a7n6wdcabcj6l93rk58f9bbsnz7xgx7n1h3x6vgv")))

(define-public crate-pktparse-0.7.1 (c (n "pktparse") (v "0.7.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06sy7lwnhwmkyqfdbi4cs11z55rihipxafbdspnd5zg76pnbgbm8")))

