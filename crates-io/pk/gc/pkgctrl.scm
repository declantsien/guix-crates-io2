(define-module (crates-io pk gc pkgctrl) #:use-module (crates-io))

(define-public crate-pkgctrl-0.1.0 (c (n "pkgctrl") (v "0.1.0") (d (list (d (n "alpm") (r "~2") (f (quote ("git"))) (d #t) (k 0)) (d (n "alpm-utils") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "colored") (r "~2") (d #t) (k 0)) (d (n "dialoguer") (r "~0.8") (d #t) (k 0)) (d (n "handlebars") (r "~4") (d #t) (k 0)) (d (n "human-panic") (r "~1") (d #t) (k 0)) (d (n "pacmanconf") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "sha2") (r "~0.9") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)))) (h "0nlx0p6mz5q1vpwnffnxxqvralyjqqb1n76wrp1yzzyi0q57xc4g")))

