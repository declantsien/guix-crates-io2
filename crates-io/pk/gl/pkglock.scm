(define-module (crates-io pk gl pkglock) #:use-module (crates-io))

(define-public crate-pkglock-0.1.0 (c (n "pkglock") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0qx0729jwlwv2xk73h1sd5i6daf75cs86yz59sjh7ry851wb1b3x")))

