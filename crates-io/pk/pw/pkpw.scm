(define-module (crates-io pk pw pkpw) #:use-module (crates-io))

(define-public crate-pkpw-0.1.0 (c (n "pkpw") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n191h163kby776jnyp70dhys2cpg88m4np1fzwxqk14gkvp52s1")))

(define-public crate-pkpw-0.1.1 (c (n "pkpw") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cq5d5da074zc986wwmiqjws1yjcvndr809w47dflzjqmkmvc60r")))

(define-public crate-pkpw-0.2.0 (c (n "pkpw") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07k9jkk50zbvli0qbrhg67n3rcd72c5vrvvfhg9kqsacji7snqmj")))

(define-public crate-pkpw-0.3.0 (c (n "pkpw") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j075h2g777sx472fwrqa9zqf8jcwx1a9px97l4cyrswxw57ndxj")))

(define-public crate-pkpw-0.4.0 (c (n "pkpw") (v "0.4.0") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nl7ysfrmb9cc2cq5z6imr87d3wnfzjmk5vsqz9lqbfqk302jl90")))

(define-public crate-pkpw-0.4.1 (c (n "pkpw") (v "0.4.1") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wk23pnz46gw3h5rcibpa43a21wiabcwab1fngm22ck81s1vvckz")))

(define-public crate-pkpw-0.5.0 (c (n "pkpw") (v "0.5.0") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pcancryq9idlbl2ainqhwfp689qn3a2xy14llg9lfh4r3z4nabc")))

(define-public crate-pkpw-1.0.0 (c (n "pkpw") (v "1.0.0") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "130l556h568riy0pnw63y80r5yzlqr2yirilzb7j14j1kr88yqv4")))

(define-public crate-pkpw-1.0.1 (c (n "pkpw") (v "1.0.1") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j8hmmv4pq4dk1gv4wfhzrfs2vi1dbypgm5j1k1xrdkf93553mcy")))

(define-public crate-pkpw-1.0.2 (c (n "pkpw") (v "1.0.2") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rm19byp040b5vi9xl1y59k9acb4sfi51hrq1gvy5fcbl4jq4xp3")))

(define-public crate-pkpw-1.1.0 (c (n "pkpw") (v "1.1.0") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d70jbcj3vaaczx1r8bj910nl2sx1df0jhrgxa1vgcnf5qsafx6c")))

(define-public crate-pkpw-1.1.1 (c (n "pkpw") (v "1.1.1") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jnmjrmlpfnrhi5r74dxrxay97plnw59x8fd4q9sfgf6hjq1pssz")))

(define-public crate-pkpw-1.1.2 (c (n "pkpw") (v "1.1.2") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m3irwbimjv21lim69raqkvs0dwgxms6nkdnpmsk1mm2pprbil4b")))

(define-public crate-pkpw-1.1.3 (c (n "pkpw") (v "1.1.3") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jhab2h2zllwp3hyvb77pbaz55i6nl5mplw5f5cqw1vvrxypsvv7")))

(define-public crate-pkpw-1.1.4 (c (n "pkpw") (v "1.1.4") (d (list (d (n "arboard") (r "^2.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10d1l5d9nbsw25cr9d41ggvnhw52rgp12nfsjg4c9mhs2ccq0xiq")))

(define-public crate-pkpw-1.1.5 (c (n "pkpw") (v "1.1.5") (d (list (d (n "arboard") (r "^3.1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17i1drm7jdcr8x24hccjhq3z2fgz8cas6hp1x1jalxbcvhihcnw1")))

(define-public crate-pkpw-1.1.6 (c (n "pkpw") (v "1.1.6") (d (list (d (n "arboard") (r "^3.1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qd7y4pgcrd2fmawn8rsdjkkjqv5asj1ans8sm7adciv29s3lkpz")))

(define-public crate-pkpw-1.2.0 (c (n "pkpw") (v "1.2.0") (d (list (d (n "arboard") (r "^3.1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07vwkpznhdzqmd94mv343dng4f963bgjj3vwrcpb9c7m1fzyjbvi")))

