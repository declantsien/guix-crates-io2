(define-module (crates-io pk dx pkdx) #:use-module (crates-io))

(define-public crate-pkdx-0.1.0 (c (n "pkdx") (v "0.1.0") (h "0cgi15rk635dzraww8nmbr0qpckc4s7lvdi86qh1cb186lp2hvxp")))

(define-public crate-pkdx-0.1.1 (c (n "pkdx") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)) (d (n "tui-logger") (r "^0.8") (d #t) (k 0)))) (h "09irma79cz9936hghfi9cdfishmlfbnhhm55xsjrk6rnhimbsqnx")))

