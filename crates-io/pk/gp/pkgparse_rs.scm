(define-module (crates-io pk gp pkgparse_rs) #:use-module (crates-io))

(define-public crate-pkgparse_rs-0.1.0 (c (n "pkgparse_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0qszrsq09flcy8x3gqkb5aihvd1yy042ckw02kmlq37wrwmpfgla")))

(define-public crate-pkgparse_rs-0.2.0 (c (n "pkgparse_rs") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)))) (h "1lbnyhx2n39hgiijwl4viyz8ljgv74n4xjmrazlxgwggzf712r6k")))

(define-public crate-pkgparse_rs-0.2.1 (c (n "pkgparse_rs") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)))) (h "0lkk1idczjfmfg20a1rg5pmp9z90p88slvs9sjlx8wnrfxgg1643") (y #t)))

(define-public crate-pkgparse_rs-0.2.11 (c (n "pkgparse_rs") (v "0.2.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)))) (h "147i4lgjrhs2aj75z9qwspsnvmp3lilabpfs24sjb73zznzmzd48") (y #t)))

(define-public crate-pkgparse_rs-0.2.111 (c (n "pkgparse_rs") (v "0.2.111") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)))) (h "1jnyplh9zfk2hv3dg255sfhhgxjff75f0wsw68hgydjajkq8d9zz")))

