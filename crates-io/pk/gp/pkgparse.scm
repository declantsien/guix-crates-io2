(define-module (crates-io pk gp pkgparse) #:use-module (crates-io))

(define-public crate-pkgparse-0.1.0 (c (n "pkgparse") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1m8sr8lrk2qn90ll47xnnqwm8vyyjri4wws9g5c0a319w21jq3vi") (y #t)))

