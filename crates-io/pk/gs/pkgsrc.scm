(define-module (crates-io pk gs pkgsrc) #:use-module (crates-io))

(define-public crate-pkgsrc-0.1.0 (c (n "pkgsrc") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1fz4idjg590kxbzjfic89apkc1xii0xk55fwc0cvzgh4spr8jv4b")))

(define-public crate-pkgsrc-0.1.1 (c (n "pkgsrc") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "05x3w2895zdcsxwyxbjbq12vb56kakccammkk7yx02z8f9hsq2rc")))

(define-public crate-pkgsrc-0.1.2 (c (n "pkgsrc") (v "0.1.2") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0paf44pp4bsprma251s668p9wzv3f8r7y85swnxzbzzl3ggdm5lf")))

(define-public crate-pkgsrc-0.1.3 (c (n "pkgsrc") (v "0.1.3") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0ipn6ll80lvzj7yf6nkm2l986azl9c5vnz1nmaii061i9d8znc06")))

(define-public crate-pkgsrc-0.1.4 (c (n "pkgsrc") (v "0.1.4") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0jg41xwxyypjf8k3yjyi5f6j64rlgipaavfbqfrmpjid4frkvsw2")))

(define-public crate-pkgsrc-0.1.5 (c (n "pkgsrc") (v "0.1.5") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1jysp78jcjmka8rnzz8midm57nx6b6aaswrf174kd0g3h3ws611p")))

(define-public crate-pkgsrc-0.1.6 (c (n "pkgsrc") (v "0.1.6") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1amvmmgrdbl726fimhd0gziwspq8c8rz65j0fs6jiiw31z32j8vr")))

(define-public crate-pkgsrc-0.1.7 (c (n "pkgsrc") (v "0.1.7") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1sq68bwpl27c9k5gbg1sml3p0rfaw8dk5xm63kdrgj7vw8svrchq")))

(define-public crate-pkgsrc-0.1.8 (c (n "pkgsrc") (v "0.1.8") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0k5imnigci7px8shd4r2q154s2zy0s60q9pwdmr0v76x3szmvkpn")))

(define-public crate-pkgsrc-0.1.9 (c (n "pkgsrc") (v "0.1.9") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0l9ycpsy29cmc9619i8zd8k6y19iy4pqlp6cwjd4mrdhr9c0l0k1")))

(define-public crate-pkgsrc-0.1.10 (c (n "pkgsrc") (v "0.1.10") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1g1b0ccnb0k1h4d7c1v82vrh0d3zjq8w4m7iqp2rnhgbrbl4q608")))

(define-public crate-pkgsrc-0.1.12 (c (n "pkgsrc") (v "0.1.12") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1n1vzlppnrl6w6z0zy9bh1qs675gpfvklrpzf68ic6rj873rg5pd")))

(define-public crate-pkgsrc-0.1.13 (c (n "pkgsrc") (v "0.1.13") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1xgp58iswhkgkfs8z7j689zdhkdcd0m69716dsk73nz772ll1dih")))

(define-public crate-pkgsrc-0.1.14 (c (n "pkgsrc") (v "0.1.14") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "12pakkx3fkk2wl2y54qiz9x2037dslns6li9lw0c9rnd3sry1f68")))

(define-public crate-pkgsrc-0.1.15 (c (n "pkgsrc") (v "0.1.15") (d (list (d (n "ar") (r "^0.7.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0pn0jqvdhxkpxpcvksq6wjz63qjbd620a8zdvk3izxqclmbl7wxi")))

