(define-module (crates-io pk gs pkgspec) #:use-module (crates-io))

(define-public crate-pkgspec-0.1.0 (c (n "pkgspec") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yxg2chy7x3y3ki9hg6zzx9yn017q8qjywwzv2hd2lnqlr5a31mr") (y #t)))

(define-public crate-pkgspec-0.1.1 (c (n "pkgspec") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qg12il3c127pk1ypc8ldvz9i99h7q81z15q5b1ic20k6qjm05f1") (y #t)))

(define-public crate-pkgspec-0.2.0 (c (n "pkgspec") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0l6gz2xwz2smbkkjkq0kb9vs6iggsxfan08ka0fn65w56kbzmqhr") (y #t)))

