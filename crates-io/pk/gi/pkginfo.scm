(define-module (crates-io pk gi pkginfo) #:use-module (crates-io))

(define-public crate-pkginfo-0.1.0 (c (n "pkginfo") (v "0.1.0") (d (list (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.5.3") (d #t) (k 0)))) (h "1vjdq9m3kkc3q6zl3iyiip9v7zwdfzac2zx2701vj8r7g359nmw0")))

(define-public crate-pkginfo-0.1.1 (c (n "pkginfo") (v "0.1.1") (d (list (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.5.3") (d #t) (k 0)))) (h "0pzx71n30wak59hfhmychl3ss9b1vdb227a1f97wpgm9xbgghnva")))

(define-public crate-pkginfo-0.1.2 (c (n "pkginfo") (v "0.1.2") (d (list (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.5.3") (d #t) (k 0)))) (h "0ql6bhng1lz0pdgl0lgnziv77sd9abdc9ay3xyqz3sc5rqa386ms")))

(define-public crate-pkginfo-0.1.3 (c (n "pkginfo") (v "0.1.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.5.3") (d #t) (k 0)))) (h "0nmcnzfi3wryqr055i8n44vn8d3lpa6v1anbn5wsyzwrhkv00mw5")))

