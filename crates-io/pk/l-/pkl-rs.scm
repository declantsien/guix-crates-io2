(define-module (crates-io pk l- pkl-rs) #:use-module (crates-io))

(define-public crate-pkl-rs-0.1.0 (c (n "pkl-rs") (v "0.1.0") (h "1n17vl7iah3sbbmymvzn13syfqymgp09ksawgihsfrxdngb66rxs") (y #t)))

(define-public crate-pkl-rs-0.0.0 (c (n "pkl-rs") (v "0.0.0") (h "19fk9lssgbqb1ysnjw50zxiarfavc69mdqxlhjd989g5ijj6g80s") (y #t)))

