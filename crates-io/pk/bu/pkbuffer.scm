(define-module (crates-io pk bu pkbuffer) #:use-module (crates-io))

(define-public crate-pkbuffer-0.1.0 (c (n "pkbuffer") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "147jnbh0chns40d4j8389jk49jr9c5z0lc9vcx2vj5y2q8llqx6n")))

(define-public crate-pkbuffer-0.2.0 (c (n "pkbuffer") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1hph7qzl8r83flknpch69z75a5x3pz6h0z3gvfrkwwm7c8fgnr35")))

(define-public crate-pkbuffer-0.3.0 (c (n "pkbuffer") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1iyvpbbdar2fm6d20aqpghm1bfw8i9pdjfpxj3vyfy5d28nwgqhq")))

(define-public crate-pkbuffer-0.4.0 (c (n "pkbuffer") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pkbuffer_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1rmfxjr2xrr30n4rmhlmpdgdaw6j5ph8xyh0inni2244i7y746cb")))

(define-public crate-pkbuffer-0.4.1 (c (n "pkbuffer") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pkbuffer_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1vzdaz6q73ph26vyll9wxc9b9mnbd3swbil2syqpjrqrvn67aqqw")))

(define-public crate-pkbuffer-0.4.2 (c (n "pkbuffer") (v "0.4.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "pkbuffer_derive") (r "^0.1.0") (d #t) (k 0)))) (h "151dsvv4q7hg0ycsy4lvajh98wb4lc79d5pnln74kc5swrqx3hmz")))

