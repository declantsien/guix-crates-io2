(define-module (crates-io pk bu pkbuffer_derive) #:use-module (crates-io))

(define-public crate-pkbuffer_derive-0.1.0 (c (n "pkbuffer_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "0mfif5xzzflhxkbvscsigaw8zwxs8if6x7xpchb9qp3ip4kav1kw")))

