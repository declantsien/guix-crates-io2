(define-module (crates-io pk _c pk_compiler) #:use-module (crates-io))

(define-public crate-pk_compiler-0.0.1-dev.0 (c (n "pk_compiler") (v "0.0.1-dev.0") (d (list (d (n "pk_parser") (r "^0.0.1-dev.0") (d #t) (k 0)))) (h "1md8jw2pzy9f5acn1yz5ywz3mcwzwancclv0ma5k0ch7m3wj35nc")))

(define-public crate-pk_compiler-0.0.2-dev.0 (c (n "pk_compiler") (v "0.0.2-dev.0") (d (list (d (n "pk_parser") (r "^0.0.1-dev.0") (d #t) (k 0)))) (h "0zi36z67pf05kj98z9628ifslbw4wa80gslfnlx426j5z6ffgs6w")))

(define-public crate-pk_compiler-0.0.3-dev.0 (c (n "pk_compiler") (v "0.0.3-dev.0") (d (list (d (n "pk_parser") (r "^0.0.3-dev.0") (d #t) (k 0)))) (h "0zv3hfxhag51i3mmsdmnny99ygbqns77599fqd3j10xd56vfm9lp")))

(define-public crate-pk_compiler-0.0.4-dev (c (n "pk_compiler") (v "0.0.4-dev") (d (list (d (n "bytecoding") (r "^0.1.0") (d #t) (k 0)) (d (n "pk_parser") (r "^0.0.4-dev") (d #t) (k 0)))) (h "0h4gvfgqy66pzgbnl4bwjilmflky9l4k3fryj15glm9cq64qn6gh")))

(define-public crate-pk_compiler-0.0.5-dev (c (n "pk_compiler") (v "0.0.5-dev") (d (list (d (n "bytecoding") (r "^0.1.0") (d #t) (k 0)) (d (n "pk_parser") (r "^0.0.5-dev") (d #t) (k 0)))) (h "1520qnvgimd80paxxm5sdnqs1r50a14j5qavcsypmqmyib76xxl6")))

