(define-module (crates-io pk l_ pkl_fast) #:use-module (crates-io))

(define-public crate-pkl_fast-0.1.0 (c (n "pkl_fast") (v "0.1.0") (h "099wqgf15g16jllwqwsdv9a1za45bw2fanx85kd0jpw7pl22r5dp")))

(define-public crate-pkl_fast-0.1.1 (c (n "pkl_fast") (v "0.1.1") (d (list (d (n "logos") (r "^0.14.0") (d #t) (k 0)))) (h "09fs75niz7pvdz30s6nx3l9axidc57zdibx3if47lb7byddc105x")))

