(define-module (crates-io pk ey pkey_mprotect) #:use-module (crates-io))

(define-public crate-pkey_mprotect-0.1.0 (c (n "pkey_mprotect") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nmqmhjm9wmx12n0733lc97kg646ia153xlrk2ymy09zwxc12khw") (r "1.57")))

(define-public crate-pkey_mprotect-0.1.1 (c (n "pkey_mprotect") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkey-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bz5wnczwjrh07ykw6z7vwi0g1j7d6bfkyqpjhcl9v76zfzl7jz0") (r "1.57")))

(define-public crate-pkey_mprotect-0.1.2 (c (n "pkey_mprotect") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0639x46j2dikyb1qyndsnhxqc59zx37vxp8d16n63wz3nwcp1sbn") (l "pkey-sys") (r "1.57")))

(define-public crate-pkey_mprotect-0.1.3 (c (n "pkey_mprotect") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cmigndpkrg6w5naqbw6a25a6gl0ys0r0m97r9j6i48kppmlr1n6") (l "pkey-sys") (r "1.57")))

(define-public crate-pkey_mprotect-0.2.0 (c (n "pkey_mprotect") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00w5p5dw2xaacaqvlay6fms8azd19d02b5xnfkf3hdcvjd9py2m8") (r "1.59")))

(define-public crate-pkey_mprotect-0.2.1 (c (n "pkey_mprotect") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lyb0ab5pgkmajzzbknhwfg9npn8ixas1hvdwnxj6kzazbnfvwjj") (r "1.59")))

(define-public crate-pkey_mprotect-0.2.2 (c (n "pkey_mprotect") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xbsqs168jkry7rxfmq874rd8lxqxvw16swrd0q61iizq6qxagm4") (r "1.59")))

(define-public crate-pkey_mprotect-0.2.3 (c (n "pkey_mprotect") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "160k33x8qqxr8i0zgpzs61jf1h2b33mawdn3dfy365123bi03xqh") (r "1.59")))

