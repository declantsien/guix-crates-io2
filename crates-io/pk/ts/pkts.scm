(define-module (crates-io pk ts pkts) #:use-module (crates-io))

(define-public crate-pkts-0.1.0 (c (n "pkts") (v "0.1.0") (d (list (d (n "pkts-macros") (r "^0.1") (d #t) (k 0)))) (h "1i61kkyjjmylgz0p81y0sakjawrz1whqyc40kfn6kq5yndf3vpi8") (f (quote (("std" "alloc") ("default" "std" "custom_layer_selection") ("custom_layer_selection" "pkts-macros/custom_layer_selection") ("alloc")))) (r "1.65")))

(define-public crate-pkts-0.1.1 (c (n "pkts") (v "0.1.1") (d (list (d (n "pkts-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0jf2c8w4rjfkgxq9yl60ah65qjnw2p9r4iz2hi5smckrp5ir3isc") (f (quote (("std" "alloc") ("default" "std" "custom_layer_selection") ("custom_layer_selection" "pkts-macros/custom_layer_selection") ("alloc")))) (r "1.65")))

(define-public crate-pkts-0.1.2 (c (n "pkts") (v "0.1.2") (d (list (d (n "pkts-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0zl02wq5f9qsa4njxj1n1shw1sgv36g1fi8ih9dwcar79arc4mma") (f (quote (("std" "alloc") ("default" "std" "custom_layer_selection") ("custom_layer_selection" "pkts-macros/custom_layer_selection") ("alloc")))) (r "1.65")))

(define-public crate-pkts-0.1.3 (c (n "pkts") (v "0.1.3") (d (list (d (n "pkts-macros") (r "^0.1.3") (d #t) (k 0)))) (h "1ml9y7rwfxqid0s9whz5bpfs5lasma9s5y42bd79klrvmsj545y0") (f (quote (("std" "alloc") ("default" "std" "custom_layer_selection") ("custom_layer_selection" "pkts-macros/custom_layer_selection") ("alloc")))) (r "1.65")))

