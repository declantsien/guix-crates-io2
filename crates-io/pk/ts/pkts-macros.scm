(define-module (crates-io pk ts pkts-macros) #:use-module (crates-io))

(define-public crate-pkts-macros-0.1.0 (c (n "pkts-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19pqjsm2g3cvvwh013irryg76g83x9pn5pzrm9a2q6ifzs5v8pdr") (f (quote (("custom_layer_selection"))))))

(define-public crate-pkts-macros-0.1.1 (c (n "pkts-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fpj74n6dsvs1sf8smqb8sf2ak4m5y15npd87npld6qamln0279m") (f (quote (("custom_layer_selection"))))))

(define-public crate-pkts-macros-0.1.2 (c (n "pkts-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1258vdggdgmdj720s4cg09bp98qidy34radwxiwg92mx56p62zpb") (f (quote (("custom_layer_selection"))))))

(define-public crate-pkts-macros-0.1.3 (c (n "pkts-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d25fsqf4zsar3ixywnqxlarc9wzja5rw4vkib6gjsjxddmqh641") (f (quote (("custom_layer_selection"))))))

