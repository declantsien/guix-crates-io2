(define-module (crates-io pk cs pkcs10) #:use-module (crates-io))

(define-public crate-pkcs10-0.0.0 (c (n "pkcs10") (v "0.0.0") (h "0jz66k486paxvnqz5v6h8zrz022pk65p1fkhj8hir6972awyksyg") (y #t)))

(define-public crate-pkcs10-0.1.0 (c (n "pkcs10") (v "0.1.0") (d (list (d (n "der") (r "^0.6") (f (quote ("oid" "derive" "alloc"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.6") (d #t) (k 0)) (d (n "x509-cert") (r "^0.1.0") (d #t) (k 2)))) (h "1cmf5iyrccfh9k9cb4sj28nimxyg6w0nyi2fsmdlbwdvl9mnrhdv") (f (quote (("pem" "der/pem" "spki/pem")))) (y #t) (r "1.56")))

(define-public crate-pkcs10-0.2.0 (c (n "pkcs10") (v "0.2.0") (d (list (d (n "der") (r "^0.6") (f (quote ("oid" "derive" "alloc"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.6") (d #t) (k 0)) (d (n "x509-cert") (r "^0.1.0") (d #t) (k 2)))) (h "031md31rllkp734qs94q3saby40cn420ay81vd0nrsv9nfvj40c0") (f (quote (("pem" "der/pem" "spki/pem")))) (y #t) (r "1.56")))

