(define-module (crates-io pk cs pkcs7) #:use-module (crates-io))

(define-public crate-pkcs7-0.1.0 (c (n "pkcs7") (v "0.1.0") (h "06xyfand85y4mdv8mhwzmzz3a2syq52wys4gnqj3ziivmlkgmkpb") (y #t)))

(define-public crate-pkcs7-0.2.0 (c (n "pkcs7") (v "0.2.0") (h "0cfxfmb4xk7hhpf86l57jqlrm6c1ybdsfb9jr5rnssr2wvl2m828") (y #t)))

(define-public crate-pkcs7-1.0.0 (c (n "pkcs7") (v "1.0.0") (h "1llgh89ip65d9081x2c1dbrxrayv5awrxjpchn7jix8yza2w1xjh") (y #t)))

(define-public crate-pkcs7-0.3.0 (c (n "pkcs7") (v "0.3.0") (d (list (d (n "der") (r "^0.5") (f (quote ("oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.5") (d #t) (k 0)))) (h "16v1s73s990ln0h0d9lrbxqn83hysyaj610yx4ylfdp2s3k68wqz") (r "1.56")))

(define-public crate-pkcs7-0.4.0-pre.0 (c (n "pkcs7") (v "0.4.0-pre.0") (d (list (d (n "der") (r "^0.6") (f (quote ("oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.6") (d #t) (k 0)))) (h "000mhsyg2bibjm39x9bwr12m9gz8lyjn9vkri1h7wxxf7rdml063") (r "1.57")))

(define-public crate-pkcs7-0.4.0-pre.1 (c (n "pkcs7") (v "0.4.0-pre.1") (d (list (d (n "der") (r "^0.7") (f (quote ("oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "=0.2.0-pre.0") (k 0)))) (h "1ihd4rrmrp4yvr5gl124lhaia1dc5ddyhkdwrrw5nmf47bbcy3p0") (r "1.65")))

(define-public crate-pkcs7-0.4.0 (c (n "pkcs7") (v "0.4.0") (d (list (d (n "der") (r "^0.7") (f (quote ("oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (k 0)))) (h "1yhi6r3pmwp0ajr94mwfgnb0nmzmy97fwm341xnj6iw2m5wq2aj7") (r "1.65")))

(define-public crate-pkcs7-0.4.1 (c (n "pkcs7") (v "0.4.1") (d (list (d (n "der") (r "^0.7") (f (quote ("oid"))) (d #t) (k 0)) (d (n "der") (r "^0.7") (f (quote ("oid" "pem"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (k 0)) (d (n "x509-cert") (r "^0.2") (f (quote ("pem"))) (k 2)))) (h "1rvp9gm7vzcbbzz6vr6xz6ri2szgxm35j0zk5dhf01b40sz7i4fp") (r "1.65")))

