(define-module (crates-io pk cs pkcs12) #:use-module (crates-io))

(define-public crate-pkcs12-0.0.0 (c (n "pkcs12") (v "0.0.0") (h "0i37715cx7fby9hnf4y6zasgw73gra0g83605fbg2h4as1zwhyg0") (y #t)))

(define-public crate-pkcs12-0.1.0 (c (n "pkcs12") (v "0.1.0") (d (list (d (n "cms") (r "^0.2.1") (d #t) (k 0)) (d (n "const-oid") (r "^0.9") (f (quote ("db"))) (d #t) (k 0)) (d (n "der") (r "^0.7.8") (f (quote ("alloc" "derive" "oid" "pem"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (f (quote ("alloc"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "pkcs5") (r "^0.7.1") (f (quote ("pbes2" "3des"))) (d #t) (k 2)) (d (n "pkcs8") (r "^0.10.2") (f (quote ("pkcs5" "getrandom"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (d #t) (k 2)) (d (n "whirlpool") (r "^0.10.4") (d #t) (k 2)) (d (n "x509-cert") (r "^0.2.3") (f (quote ("pem"))) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1hpgr43xvcgaa6wrb6vszb2yz6xpnqsmw8vh5pqia46csgrksnv9") (s 2) (e (quote (("kdf" "dep:digest")))) (r "1.65")))

