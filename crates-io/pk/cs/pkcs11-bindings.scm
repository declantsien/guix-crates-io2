(define-module (crates-io pk cs pkcs11-bindings) #:use-module (crates-io))

(define-public crate-pkcs11-bindings-0.1.0 (c (n "pkcs11-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)))) (h "081giqbjnxqcf17ga7908b1ndyldav7vnf1gvfcwzdsv8gdnakq5")))

(define-public crate-pkcs11-bindings-0.1.1 (c (n "pkcs11-bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)))) (h "14mm7rlm1xiq2b1hl7x035lg8ajzpbabzgxma1yzh8s8h4dgf2vq")))

(define-public crate-pkcs11-bindings-0.1.2 (c (n "pkcs11-bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)))) (h "1031xxp8gl5dasf887bh4a54fmv5ghrs9zl63sfjiarnr2k8f19r")))

(define-public crate-pkcs11-bindings-0.1.3 (c (n "pkcs11-bindings") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)))) (h "04xv7n7xsbcdwlg1nmb8j5hwcnvbwg36v0lklh8zi3r14bvsycqj")))

(define-public crate-pkcs11-bindings-0.1.4 (c (n "pkcs11-bindings") (v "0.1.4") (d (list (d (n "bindgen") (r ">=0.59.2") (f (quote ("runtime"))) (k 1)))) (h "0lvw8ygvlcfqkhnnay4rnn4wgjk14awh78cf7chxgxb4bgg5c1f2")))

(define-public crate-pkcs11-bindings-0.1.5 (c (n "pkcs11-bindings") (v "0.1.5") (d (list (d (n "bindgen") (r ">=0.59.2") (f (quote ("runtime"))) (k 1)))) (h "1shqm9m5caln2hj6zb7fx83jkl3k79drk46sac3q48jbwvdvpynh")))

