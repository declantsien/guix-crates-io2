(define-module (crates-io pk cs pkcs11) #:use-module (crates-io))

(define-public crate-pkcs11-0.1.0 (c (n "pkcs11") (v "0.1.0") (d (list (d (n "libloading") (r "^0.4.2") (d #t) (k 0)))) (h "1ilhl2h4hm8bj8ab5b1nilr0jawi0z925isd9ksfindl3fqhb6s2")))

(define-public crate-pkcs11-0.2.0 (c (n "pkcs11") (v "0.2.0") (d (list (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "1z1wh1m7idwjq137d29ipavq04kkw50x3pl66aw2w7v3n0s00973")))

(define-public crate-pkcs11-0.3.0 (c (n "pkcs11") (v "0.3.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "0v958la7j6bl7lms4iz78cww5mpidn0cn6h6kpv3bbgyj35yccxq")))

(define-public crate-pkcs11-0.3.1 (c (n "pkcs11") (v "0.3.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "10s7mbiz55azg7lmpqqfwkz484g6c2p7j74i655k7cwxrm0gcqw7")))

(define-public crate-pkcs11-0.4.0 (c (n "pkcs11") (v "0.4.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "1hy19ip3q5sz4a7y7zmpgpqxqzq0gvkj48afbxwzqf2ns6q3lh7z")))

(define-public crate-pkcs11-0.4.1 (c (n "pkcs11") (v "0.4.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)) (d (n "serial_test") (r "~0.1") (d #t) (k 2)) (d (n "serial_test_derive") (r "~0.1") (d #t) (k 2)))) (h "180m83ljzkrzg7cg5i9ssmm1s0ingx90345mwb5ni070kylr61f2")))

(define-public crate-pkcs11-0.4.2 (c (n "pkcs11") (v "0.4.2") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)) (d (n "serial_test") (r "~0.1") (d #t) (k 2)) (d (n "serial_test_derive") (r "~0.1") (d #t) (k 2)))) (h "1gznq4yr2dnp7qbfpmvd2gq8vlvlcmcy3gpnf5ja5nd9sdr24wa5")))

(define-public crate-pkcs11-0.5.0 (c (n "pkcs11") (v "0.5.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)) (d (n "serial_test") (r "~0.1") (d #t) (k 2)) (d (n "serial_test_derive") (r "~0.1") (d #t) (k 2)))) (h "1bqhdd4w0ymvv6zzc090vy2mywq07lix16am8pz3nqf8wiknvjis")))

