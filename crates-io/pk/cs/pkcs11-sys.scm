(define-module (crates-io pk cs pkcs11-sys) #:use-module (crates-io))

(define-public crate-pkcs11-sys-0.0.0 (c (n "pkcs11-sys") (v "0.0.0") (h "06lfdl35dygf1kp4b8x2xg9cm1sxs1zlsx3jxpgkpb974wra276n")))

(define-public crate-pkcs11-sys-0.2.0 (c (n "pkcs11-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "03rylbcsaw4jxfykh6mlflj9vpqz2v33jmz02fssd8nbhh5qknym") (r "1.63")))

(define-public crate-pkcs11-sys-0.2.1 (c (n "pkcs11-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "17s4150q1wvzr29xqrsbx720jz0cyjrjxz21g0mq10lkybnvc0f0") (r "1.63")))

(define-public crate-pkcs11-sys-0.2.2 (c (n "pkcs11-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)))) (h "1zlk9d1pxggm48hbxgkfwnw6fxbjiimq7fmbk21xyscy4ypbfxrh") (r "1.63")))

(define-public crate-pkcs11-sys-0.2.6 (c (n "pkcs11-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)))) (h "1ffcl5y8r2bmmzn0klg51b3g7pv184n4nrvsqm879i7h4jidzz05") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.7 (c (n "pkcs11-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)))) (h "00325a4q7xs0kajcbs46m18smiqqg30n78hphzp1mmvhs77wpzz8") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.8 (c (n "pkcs11-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)))) (h "1sfhs896z6a7f8fw4vvk7lim3gwyy11g3rbvy5ap7wj95jb1kd8s") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.11 (c (n "pkcs11-sys") (v "0.2.11") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)))) (h "1xy2wvfyydcipjf68kjswymvqxwpw02k8cna8dn9pd96an2szndq") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.12 (c (n "pkcs11-sys") (v "0.2.12") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)))) (h "0qxzl8j8vkqcddlnwhi72rn7nd4g21n2p2sbj1cjk4jc2z50pzs0") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.13 (c (n "pkcs11-sys") (v "0.2.13") (d (list (d (n "bindgen") (r "^0.68.1") (o #t) (d #t) (k 1)))) (h "0kmv8mpaq9xw5ak6icxc4ddz6dv0sya7w9rzy5kr83ghhvy31miv") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.15 (c (n "pkcs11-sys") (v "0.2.15") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)))) (h "1rrv2qs3nnbinrxlq6h852d1y2y2w49vx64zrvqfr22jjpmqyd8a") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.16 (c (n "pkcs11-sys") (v "0.2.16") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)))) (h "0l8nd706jkandksjcvaax3w0qqrbqdhx3qsanqpqvc3pq3ny4cga") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.17 (c (n "pkcs11-sys") (v "0.2.17") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)))) (h "060mzwkwswdwmw0sa81128i41j6hmxp41hyqxg24f36azfrz1x1v") (r "1.65")))

(define-public crate-pkcs11-sys-0.2.18 (c (n "pkcs11-sys") (v "0.2.18") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)))) (h "0vs4f3673n2z5va80zh3w26kcd1zhigw3771hxxd5ab28hfxqyxr") (r "1.65")))

