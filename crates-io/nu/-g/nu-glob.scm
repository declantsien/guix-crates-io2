(define-module (crates-io nu -g nu-glob) #:use-module (crates-io))

(define-public crate-nu-glob-0.60.0 (c (n "nu-glob") (v "0.60.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "10wabk6ldgn5cbk0s18l7g8j68fgblplrag4zpvi0bvncd19qfcz")))

(define-public crate-nu-glob-0.61.0 (c (n "nu-glob") (v "0.61.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0w544dcfl7aqalhfhxd3hxwvpfa6fkh17y7g8hd6j4snjh0cz759")))

(define-public crate-nu-glob-0.62.0 (c (n "nu-glob") (v "0.62.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0q5y4br8nar4gb9pddmpkmfw0igyrccdxk6crjgsznd9hk0v0hhb")))

(define-public crate-nu-glob-0.63.0 (c (n "nu-glob") (v "0.63.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0kw9wxznv9yq5ham3x8in25gsipi79sk6mgvy0gj1hjpwalv1v2v")))

(define-public crate-nu-glob-0.64.0 (c (n "nu-glob") (v "0.64.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06y0kl8w85g8yi4sv01mcnink75w92yivzfr6563l43crjx06vva")))

(define-public crate-nu-glob-0.65.0 (c (n "nu-glob") (v "0.65.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ppdqn0q6c0c5m6m1anrfp9j0vykhgp2j0q7kbdnsw531iaxm96g")))

(define-public crate-nu-glob-0.66.0 (c (n "nu-glob") (v "0.66.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "05iwnzggy1c584cbkqqp9yn2q38gx0b8z3kyqrylw1wnbi7450sn")))

(define-public crate-nu-glob-0.66.1 (c (n "nu-glob") (v "0.66.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "09cfqwh6ynhxsp712rkjpr9wd2m1w5pyd1l1f7j5bdrlkfz34w1q")))

(define-public crate-nu-glob-0.66.2 (c (n "nu-glob") (v "0.66.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1v0anj71v43ab5558mkljim5ilp3xxb93bq0dq64cdb7j2hrkmgb")))

(define-public crate-nu-glob-0.67.0 (c (n "nu-glob") (v "0.67.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rk0vl3090y6n3i9rlwsqg346p2vixw16b1bhicn6sc6mvx4x4gj")))

(define-public crate-nu-glob-0.68.0 (c (n "nu-glob") (v "0.68.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16835v82p2sdxiw3kviysgrmg60vh4318dlx30n8222pl4dwbchf")))

(define-public crate-nu-glob-0.68.1 (c (n "nu-glob") (v "0.68.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qi9bnh016255vbs38jshpmcdyhcqf1blgbljzm10f9smy352axh")))

(define-public crate-nu-glob-0.69.0 (c (n "nu-glob") (v "0.69.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1pwqgyg1yaas5piir30a653v7jbq103d9k05ds8im9xdwkra1rj9")))

(define-public crate-nu-glob-0.69.1 (c (n "nu-glob") (v "0.69.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "11k7827za2a39slp6svsax5nbhcn7r2b6q4i3c9hgkn1xlvgz9qk")))

(define-public crate-nu-glob-0.70.0 (c (n "nu-glob") (v "0.70.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vwh89lk6xvjpn0p6i5vfbqdxii59dyrgwgg8cxwkklppfbzza1f")))

(define-public crate-nu-glob-0.71.0 (c (n "nu-glob") (v "0.71.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1phfziwcclnihck4nzcjzpcn1fvlv2y64q55ska3h0ksmnfhq7by")))

(define-public crate-nu-glob-0.72.0 (c (n "nu-glob") (v "0.72.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1bdybzqwbb28qlj78c6dlv1vcalyqls7av5vk6g49grcwfv0383i")))

(define-public crate-nu-glob-0.73.0 (c (n "nu-glob") (v "0.73.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0kb719lfalqp0744p5461q5iqn0rp1l56h8jzyg197qkribzs7k1")))

(define-public crate-nu-glob-0.74.0 (c (n "nu-glob") (v "0.74.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1d3bvbk0xkvy5ql28fpn50izxv89bigmhk74rxlsqq5i6akk30ip")))

(define-public crate-nu-glob-0.75.0 (c (n "nu-glob") (v "0.75.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0a694bbwrx20spvpkbv0rm89d1ppgqbk23p259zxpk2yn15i5cid")))

(define-public crate-nu-glob-0.76.0 (c (n "nu-glob") (v "0.76.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1ikk5s9sznq8074n1xi15c004pb37rxaiqqp8mvb2lbvaak8vwz5")))

(define-public crate-nu-glob-0.77.0 (c (n "nu-glob") (v "0.77.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "109x32jm1b7bw2jxizpvii8azbccxlk2v5d2ch317x3bqsg1fhjp")))

(define-public crate-nu-glob-0.77.1 (c (n "nu-glob") (v "0.77.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0csap74sxi447vdfcfi8jl1xp2s6azsbmj2382w5iddi8a2iqvwb")))

(define-public crate-nu-glob-0.78.0 (c (n "nu-glob") (v "0.78.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1lhzhwww6wnvbi5yhk3fq85mpipd0myfn23vf0q2k2r6ikdcdfbz")))

(define-public crate-nu-glob-0.79.0 (c (n "nu-glob") (v "0.79.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0j4143ff9m95jlhi65ink596xhjg8bc58pgl2xc4kdw3qi0nqjz3")))

(define-public crate-nu-glob-0.80.0 (c (n "nu-glob") (v "0.80.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "07xk6nq7qdiyicr341ks872li5kr8zfxpj8vsjhmlz9q0gmj0awj")))

(define-public crate-nu-glob-0.81.0 (c (n "nu-glob") (v "0.81.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "13g277mqhs3xjcyvzrplkls43jd4mzjg5nvzghva41ry6a0k4hba")))

(define-public crate-nu-glob-0.82.0 (c (n "nu-glob") (v "0.82.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1n99fzmr3rq8narmk5400v5f3b0k615kmgddcqc05wl059gfsfm7")))

(define-public crate-nu-glob-0.83.0 (c (n "nu-glob") (v "0.83.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1h07f19vzkwz97yy2f6zgg78kii1phv2a1fljjsk5cmsswj8qp3c")))

(define-public crate-nu-glob-0.83.1 (c (n "nu-glob") (v "0.83.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0nr7lnq1ynigw3g0nqad2inqkwbyh0b2an5as8w5ma1inqb7943l")))

(define-public crate-nu-glob-0.84.0 (c (n "nu-glob") (v "0.84.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "18b9grskd3pj81vz87znnb822fb97xv35r5nv3mads0a7r3pjgdc")))

(define-public crate-nu-glob-0.85.0 (c (n "nu-glob") (v "0.85.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0fr5qxyvdsa6yvps8bvshxhx9x15iq2xf9xb9pw6kpqghwxx81g7")))

(define-public crate-nu-glob-0.86.0 (c (n "nu-glob") (v "0.86.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "18fi029r2fqid59dd8pd5gfdgshaqfprkwzhm1fixjgkv47m6f15")))

(define-public crate-nu-glob-0.87.0 (c (n "nu-glob") (v "0.87.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "047hlyfb6bcc3jid7id2whvrqkf87viwx628ls6s3xrkd2p62590")))

(define-public crate-nu-glob-0.87.1 (c (n "nu-glob") (v "0.87.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0zsvl26y8ivvi52ybdpl123iv2c1jnfvgc781q7vz5wq1p52bb89")))

(define-public crate-nu-glob-0.88.0 (c (n "nu-glob") (v "0.88.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0ay6hnp9qbmrc0xa0i6bgzdklpbi8l3k8lx7j13m3zi9csn40l2v")))

(define-public crate-nu-glob-0.88.1 (c (n "nu-glob") (v "0.88.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1zb22s6d1x56h09jybwv2n5hfb9q7g39ngsy11s3a39yxqbf5ilg")))

(define-public crate-nu-glob-0.89.0 (c (n "nu-glob") (v "0.89.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "19srjc324svbakjr1p335z7labbimzqbqfyz5h31r7cl53ax1356")))

(define-public crate-nu-glob-0.90.0 (c (n "nu-glob") (v "0.90.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0wvi5ymdji6r2n5in2r8nb1gvb6gl704rfvk7idjr0kqxzbcfnd8")))

(define-public crate-nu-glob-0.90.1 (c (n "nu-glob") (v "0.90.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "001id9zj6hfvmnx25kjifb09ikwp9spjzhgwbs4xffa9mfjmas2h")))

(define-public crate-nu-glob-0.91.0 (c (n "nu-glob") (v "0.91.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "094jkfb7rlcl0dxs5gnw8x30zv75s372l72zsg1wmv8lblzbfybx")))

(define-public crate-nu-glob-0.92.0 (c (n "nu-glob") (v "0.92.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0qnz811ydgf083binqapk1cpwfmgmngx7my3r01j91w2y4525ap9")))

(define-public crate-nu-glob-0.92.1 (c (n "nu-glob") (v "0.92.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0x8rdd1vswb096c2iz9ypm37dkblpy9v06q0jwdgnzw1869ix41n")))

(define-public crate-nu-glob-0.92.2 (c (n "nu-glob") (v "0.92.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0a7a9f7w43iy8y73h9938pp0c8wrc1g8vsj4fqip7rnf2d9lp77r")))

(define-public crate-nu-glob-0.93.0 (c (n "nu-glob") (v "0.93.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0k339hvav8jfykry6kfnm60zk842a32qkmwyxvzvrvg0280ffg0w")))

(define-public crate-nu-glob-0.94.0 (c (n "nu-glob") (v "0.94.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "10dc4rppiinhvllvydrvvj7jhgrgd02rlm8x7adcpnaf9i4g3na5")))

