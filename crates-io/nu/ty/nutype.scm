(define-module (crates-io nu ty nutype) #:use-module (crates-io))

(define-public crate-nutype-0.1.0 (c (n "nutype") (v "0.1.0") (h "07ikhaxif0ccyhavv33xvls07gvd00sxaygyz72lxmghiblv6l1v")))

(define-public crate-nutype-0.1.1 (c (n "nutype") (v "0.1.1") (d (list (d (n "nutype_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0b08f8qbvbqc4ihipl0licqf67hapv8zp7m2zhz7vmqzzx07579a") (f (quote (("serde1" "nutype_macros/serde1"))))))

(define-public crate-nutype-0.2.0 (c (n "nutype") (v "0.2.0") (d (list (d (n "nutype_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1dfwivwxnlzxzsr0w7rdl8vc76lcv2fzps3m6c3cg1dfvgd7y2h6") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.3.0 (c (n "nutype") (v "0.3.0") (d (list (d (n "nutype_macros") (r "^0.3.0") (d #t) (k 0)))) (h "09n1b3rbr2icfgfhbvz7c50varl7hbipx77g0zhg8si6lcii04s9") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.3.1 (c (n "nutype") (v "0.3.1") (d (list (d (n "nutype_macros") (r "^0.3.1") (d #t) (k 0)))) (h "149scjd48mx796ab1jjvpahqx6k9p9d27p32x8yhjhy5jilw5q8a") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.4.0-beta.1 (c (n "nutype") (v "0.4.0-beta.1") (d (list (d (n "nutype_macros") (r "^0.3.1") (d #t) (k 0)))) (h "1sjbvpky9hs5p7a89x49xghcj0mav34bxqy7k6ns1lbkbgsfxy43") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.4.0-beta.2 (c (n "nutype") (v "0.4.0-beta.2") (d (list (d (n "nutype_macros") (r "^0.4.0-beta.2") (d #t) (k 0)))) (h "0xpi1qi7d5nc0yxxvpxgd7anh07idf5457vf49w7v20n4n4bqbii") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.4.0 (c (n "nutype") (v "0.4.0") (d (list (d (n "nutype_macros") (r "^0.4.0") (d #t) (k 0)))) (h "01yh052dk9y3if551qik1sgn101vx262ykdzxhy6lnr73ljkiz9q") (f (quote (("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked"))))))

(define-public crate-nutype-0.4.1-beta.1 (c (n "nutype") (v "0.4.1-beta.1") (d (list (d (n "nutype_macros") (r "^0.4.1-beta.1") (d #t) (k 0)))) (h "0vgz87wna2wz2yys5qlwmmb8m2c61d4yi75pgq0zdfqswgkfz5g6") (f (quote (("std" "nutype_macros/std") ("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked") ("default" "std") ("arbitrary" "nutype_macros/arbitrary"))))))

(define-public crate-nutype-0.4.1 (c (n "nutype") (v "0.4.1") (d (list (d (n "nutype_macros") (r "^0.4.1") (d #t) (k 0)))) (h "1mj4v7z9p6j5djjlglhvjmjnik110pphl4nqxjkf1nxl2lm2dn9w") (f (quote (("std" "nutype_macros/std") ("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked") ("default" "std") ("arbitrary" "nutype_macros/arbitrary"))))))

(define-public crate-nutype-0.4.2 (c (n "nutype") (v "0.4.2") (d (list (d (n "nutype_macros") (r "^0.4.2") (d #t) (k 0)))) (h "0bizp10jcxhb0s9j55271zv986vvb8rzrxghv93xn0rgxva8f4c0") (f (quote (("std" "nutype_macros/std") ("serde" "nutype_macros/serde") ("schemars08" "nutype_macros/schemars08") ("regex" "nutype_macros/regex") ("new_unchecked" "nutype_macros/new_unchecked") ("default" "std") ("arbitrary" "nutype_macros/arbitrary"))))))

