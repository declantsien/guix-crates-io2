(define-module (crates-io nu mx numx) #:use-module (crates-io))

(define-public crate-numX-0.2.0 (c (n "numX") (v "0.2.0") (d (list (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (o #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)))) (h "18lbpakim0as3wd1xawin1vsdaz6n8mszvzzrdrbn64p2g56xr7x") (f (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("default" "std")))) (y #t)))

