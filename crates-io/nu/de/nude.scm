(define-module (crates-io nu de nude) #:use-module (crates-io))

(define-public crate-nude-0.1.0 (c (n "nude") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1rbc2wbjmjshvn0zjkrblf60wmgfwmnarrxr57pv5x4s6bpk4a20")))

(define-public crate-nude-0.1.1 (c (n "nude") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "084fyc7n38a18h2pvhqqcx642rjb7dyc82gk0a3qrkrg0ifn7l8a")))

(define-public crate-nude-0.2.0 (c (n "nude") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "05ymw6a97hywnfva30ghmzbk19415r15rg416qirccvy97lczaj0")))

(define-public crate-nude-0.3.0 (c (n "nude") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1qyv1bn5vxpn27mi9f38xk6hzvay7kmwcwc30m4blrgp657zi6v2")))

