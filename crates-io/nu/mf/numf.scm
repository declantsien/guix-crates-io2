(define-module (crates-io nu mf numf) #:use-module (crates-io))

(define-public crate-numf-0.1.0 (c (n "numf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.1.1") (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.0") (f (quote ("bintols"))) (d #t) (k 0)))) (h "1qys3rzdrrmqkk02dk1vqxx0fxxdaw2q1qmyam6scnrjznxp24xy")))

(define-public crate-numf-0.2.0 (c (n "numf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.1.1") (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.0") (f (quote ("bintols"))) (d #t) (k 0)))) (h "1nnlyncw8yxap8gy45igbpdfh339chmch4im5fbqjvbnvhharf5a")))

(define-public crate-numf-0.3.0 (c (n "numf") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.1.1") (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.1") (f (quote ("bintols"))) (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "0nmaxgc43mrf4fyvqfz02a9k9ia6zg7g30scwlw0wpwkdhvnkml5") (y #t)))

(define-public crate-numf-0.3.1 (c (n "numf") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.1.1") (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.1") (f (quote ("bintols"))) (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "0hhibcz0c3wham9dwa08i3kfb37i7fp4i02payx0r28aj28sxqyf")))

(define-public crate-numf-0.3.2 (c (n "numf") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.1") (f (quote ("bintols"))) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "01lca5nglk0nklhv2r9pqvwby3264b08c0vbpj1p15i5gwv04bjv")))

(define-public crate-numf-0.3.3 (c (n "numf") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fast32") (r "^1.0.2") (d #t) (k 0)) (d (n "libpt") (r "^0.5.1") (f (quote ("bintols"))) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1s1pz5a5ka148h884xgffx1lq6lhxh36saapvcadqbw1cvz1hp9q")))

