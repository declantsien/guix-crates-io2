(define-module (crates-io nu mf numfmt) #:use-module (crates-io))

(define-public crate-numfmt-1.0.0 (c (n "numfmt") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "dtoa") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)))) (h "0i4fc6slrdbpzqxhlb11nmdbgm1c76q6k3msqs6n5c8g8s7qwicr")))

(define-public crate-numfmt-1.1.0 (c (n "numfmt") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "dtoa") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)))) (h "0npbcnq0xkfxmkx6srh5ysrsjiihlr7jhzx9drvkk4ggz7ryzx9m")))

(define-public crate-numfmt-1.1.1 (c (n "numfmt") (v "1.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "dtoa") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)))) (h "0880pp6l958ig1i6z93jy1cn7f7h3i6c6zglnfjnxyz9gpj6fx6v")))

