(define-module (crates-io nu ki nuki-sys) #:use-module (crates-io))

(define-public crate-nuki-sys-4.1.5 (c (n "nuki-sys") (v "4.1.5") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1x1cx0jbap0in3b7sxli2zhpdzd81m6xqqfixwyx8k1sxhr9yvy2") (y #t) (l "nuklear")))

(define-public crate-nuki-sys-4.1.5+1 (c (n "nuki-sys") (v "4.1.5+1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ljnr5n4cdmb4jmfnki7i0ahy7vx5191shhlmc6n1d41d89vvafz") (l "nuklear")))

(define-public crate-nuki-sys-4.3.1+1 (c (n "nuki-sys") (v "4.3.1+1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lzysw6phdpwj385f2y9yyccaz2bx8wlwi1zyrmzppcyjr18k10c") (l "nuklear")))

(define-public crate-nuki-sys-4.3.2 (c (n "nuki-sys") (v "4.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r2242hxfypp9cby1ficjizxqkwgdfrr390fyp2l20sgg3q82dfm") (l "nuklear")))

(define-public crate-nuki-sys-4.3.3 (c (n "nuki-sys") (v "4.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "084qw8y2dpdhdv3620jm362h7rllmdswh1mm1h2r4qf3f1mdrws8") (l "nuklear")))

(define-public crate-nuki-sys-4.3.4 (c (n "nuki-sys") (v "4.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06nw51zmgq85iclya54kdcxykdcvirbqx1mg7cj1028w3ig4k1zy") (l "nuklear")))

(define-public crate-nuki-sys-4.6.1 (c (n "nuki-sys") (v "4.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gznzwmfwxbrvgg691bk3368mbjqr12sza2bwvgf9ychmprw0ifx") (l "nuklear")))

