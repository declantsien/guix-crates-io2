(define-module (crates-io nu ki nuki) #:use-module (crates-io))

(define-public crate-nuki-0.6.3 (c (n "nuki") (v "0.6.3") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuki-sys") (r "~4.1") (d #t) (k 0)))) (h "0h6y7inxfgbs6v40rizl5ylzprf2fv042nkhya2hbyxx273r3zyk") (f (quote (("rust_allocator") ("default")))) (y #t)))

(define-public crate-nuki-0.1.0 (c (n "nuki") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.1") (d #t) (k 0)))) (h "002cmgfbgv4gj7z4ifkcsf14l462dgzw3yid49n55inlspd5j9gp") (f (quote (("default"))))))

(define-public crate-nuki-0.2.0 (c (n "nuki") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.1") (d #t) (k 0)))) (h "0zhy63rz3k6p1z7d4f3krsv20dh1crgsnsbw65m557d5vmpi59bi") (f (quote (("default")))) (y #t)))

(define-public crate-nuki-0.2.1 (c (n "nuki") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.1") (d #t) (k 0)))) (h "12had7wxdsddgdslwfbf525bif98lc1d1iwdz5yag57g9ga4qvy7") (f (quote (("default")))) (y #t)))

(define-public crate-nuki-0.2.2 (c (n "nuki") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "0znlp4l9l3728x3bv2gv7kmpl2x5yhfl3nd60k10wacy754i154b") (f (quote (("default"))))))

(define-public crate-nuki-0.2.3 (c (n "nuki") (v "0.2.3") (d (list (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "0v0f4m6x8gb6m9qh76w50rh03l8ji3d10ifnlsq7kqv94kx3bl6g") (f (quote (("default"))))))

(define-public crate-nuki-0.2.4 (c (n "nuki") (v "0.2.4") (d (list (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "03wzdl2dl6220rnbncnj5snmmphgqpwjxbvbiw6lxir44g3cyydq") (f (quote (("default"))))))

(define-public crate-nuki-0.2.5 (c (n "nuki") (v "0.2.5") (d (list (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "1dl064072lvycpxnn7wwfjg24havjng4bc9j62fgi9iyjk1vir4k") (f (quote (("default"))))))

(define-public crate-nuki-0.2.6 (c (n "nuki") (v "0.2.6") (d (list (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "0w8z1bchs5ql5497iwhxagrmgsrv8kpii026kaxs5gzqpaml8q5h") (f (quote (("default"))))))

(define-public crate-nuki-0.3.0 (c (n "nuki") (v "0.3.0") (d (list (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "0cna8n1x2vsd60lr275zrwzdwviyqqvmyl21zx0i34vilnwsx54d") (f (quote (("default"))))))

(define-public crate-nuki-0.3.1 (c (n "nuki") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "03pjf1k49a8z6p4q6b6i6jy7k5a4l19vc5rbs3amkwnz59810pr6") (f (quote (("default"))))))

(define-public crate-nuki-0.3.2 (c (n "nuki") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "input-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nuki-sys") (r "^4.3") (d #t) (k 0)))) (h "0jiqdj9rmi0jmkvgy05ilgxz97xq6wx68kv1q92mlqzan4j0hnq0") (f (quote (("default"))))))

