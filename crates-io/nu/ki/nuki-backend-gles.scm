(define-module (crates-io nu ki nuki-backend-gles) #:use-module (crates-io))

(define-public crate-nuki-backend-gles-0.1.0 (c (n "nuki-backend-gles") (v "0.1.0") (d (list (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.1") (d #t) (k 0)))) (h "0qm408nw5fjdcw9c6v7jmg8j4cc68yb85zp9lg793dqkwhdm4pks") (y #t)))

(define-public crate-nuki-backend-gles-0.1.1 (c (n "nuki-backend-gles") (v "0.1.1") (d (list (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.1") (d #t) (k 0)))) (h "1ss96sxrwp8i9f5gmnla1ynfv4fc32pp8phc3nil3lgrzbvh3ql6")))

(define-public crate-nuki-backend-gles-0.1.2 (c (n "nuki-backend-gles") (v "0.1.2") (d (list (d (n "egls") (r "^0.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.2") (d #t) (k 0)))) (h "0h15mshnrv8ivq9vkzvpkxml4zs7kai7fqh86gp1asrfzzibhv0s")))

(define-public crate-nuki-backend-gles-0.1.3 (c (n "nuki-backend-gles") (v "0.1.3") (d (list (d (n "egls") (r "^0.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.2") (d #t) (k 0)))) (h "0ikxwbgvjfhc0yjrprls0cqlvrwbvw8zn6qbq36mxdn7mvjhsmlb")))

(define-public crate-nuki-backend-gles-0.1.4 (c (n "nuki-backend-gles") (v "0.1.4") (d (list (d (n "egls") (r "^0.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.2") (d #t) (k 0)))) (h "06clnjiwbzv9y81i1vbgqhv1b7m4c6mfpkmgqrlmpifnkj1rvxzq")))

(define-public crate-nuki-backend-gles-0.2.0 (c (n "nuki-backend-gles") (v "0.2.0") (d (list (d (n "egls") (r "^0.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "gls") (r "^0.1") (d #t) (k 0)) (d (n "nuki") (r "^0.3") (d #t) (k 0)))) (h "1ryhbhyq8kbb6h9bxr16kyqmam0d8k29wy4xnigfc26nxd03zasd")))

