(define-module (crates-io nu bl nubls) #:use-module (crates-io))

(define-public crate-nubls-0.1.0 (c (n "nubls") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.3.1") (f (quote ("nightly" "endo"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0b5gj81wk34xn9586h2vdpyy0fbsda90bav6vbmyf7wdpdi8faba")))

