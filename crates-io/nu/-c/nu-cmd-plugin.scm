(define-module (crates-io nu -c nu-cmd-plugin) #:use-module (crates-io))

(define-public crate-nu-cmd-plugin-0.93.0 (c (n "nu-cmd-plugin") (v "0.93.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nu-engine") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-path") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-plugin-engine") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "16va465gygrf96f0zp7zc9f6kf7wp950108h3x7f3sdqsfm39h58")))

(define-public crate-nu-cmd-plugin-0.94.0 (c (n "nu-cmd-plugin") (v "0.94.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nu-engine") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-path") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-plugin-engine") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "03falbr4r1hwgf599ayil220p0jkxcgv2aam6jsxdxa99s53fxxr")))

