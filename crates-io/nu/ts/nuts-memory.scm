(define-module (crates-io nu ts nuts-memory) #:use-module (crates-io))

(define-public crate-nuts-memory-0.4.0 (c (n "nuts-memory") (v "0.4.0") (d (list (d (n "nuts-backend") (r "=0.4.0") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.50") (d #t) (k 0)))) (h "11qsj6fp30xj47xxw856nhlsga4jk4gcm5mawsh4a51pjdplgs1z")))

(define-public crate-nuts-memory-0.4.1 (c (n "nuts-memory") (v "0.4.1") (d (list (d (n "nuts-backend") (r "=0.4.1") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "0br6z0bz6nzx5qbmmjsfrkiicavzmqvc0lfg2vxmy0nfhrwxcqav")))

(define-public crate-nuts-memory-0.4.2 (c (n "nuts-memory") (v "0.4.2") (d (list (d (n "nuts-backend") (r "=0.4.2") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "09vvid1q5bic6a1v2qiiny8yqvarwk5yi2n796nzyjjxzgm05g6f")))

(define-public crate-nuts-memory-0.4.3 (c (n "nuts-memory") (v "0.4.3") (d (list (d (n "nuts-backend") (r "=0.4.3") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1prmp2b3b9bnsjfw073h9kk1c19hcywq5dd41jxi7qrqr57ywhqm")))

(define-public crate-nuts-memory-0.5.0 (c (n "nuts-memory") (v "0.5.0") (d (list (d (n "nuts-backend") (r "=0.5.0") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1gpv6vini4azrpxxkyx77vk6d1qzjir46v6j5vk7vzygdgfqgmn4")))

(define-public crate-nuts-memory-0.6.0 (c (n "nuts-memory") (v "0.6.0") (d (list (d (n "nuts-backend") (r "=0.6.0") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.6.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "13n3m0f594mqj1bqwyb6ps5x7d9928smznmxby4b03fzqsy65wzg")))

(define-public crate-nuts-memory-0.6.1 (c (n "nuts-memory") (v "0.6.1") (d (list (d (n "nuts-backend") (r "=0.6.1") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.6.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1w8i1zg5diijdmlfbl65dlj5l3vdgywb18rm7v7b67qsla548lg9")))

(define-public crate-nuts-memory-0.6.2 (c (n "nuts-memory") (v "0.6.2") (d (list (d (n "nuts-backend") (r "=0.6.2") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.6.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0glpr4d4xp4468vl8kmfwqbllhnaa8i6lmqg4ghzg638pbx2a8k0")))

