(define-module (crates-io nu ts nuts-bytes) #:use-module (crates-io))

(define-public crate-nuts-bytes-0.1.0 (c (n "nuts-bytes") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "1546xw7ch3c3qi29zcy3rwrf2n7apjarplvb5xcs6s76f7rabj8b")))

(define-public crate-nuts-bytes-0.1.1 (c (n "nuts-bytes") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)))) (h "06hvd72dh8dhfz4k8gdfyz2gxscm72rlg3va42i1062a6an5l3jw")))

(define-public crate-nuts-bytes-0.2.0 (c (n "nuts-bytes") (v "0.2.0") (d (list (d (n "nuts-bytes-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1wa6jvj7rjpvvz1sjpf3vbn2j1fg1kgw2k4y2svmfjvkgsmlfz2z") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.2.1 (c (n "nuts-bytes") (v "0.2.1") (d (list (d (n "nuts-bytes-derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ivvpxr8dld0990pisdf4mbhycsjqlmxa2q5dl75xxg6s8fd635y") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.2.2 (c (n "nuts-bytes") (v "0.2.2") (d (list (d (n "nuts-bytes-derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "00mx7hyd8mln33gx7ffsngnwl92c33lrf4qdxygwv627iwpcz1p1") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.4.0 (c (n "nuts-bytes") (v "0.4.0") (d (list (d (n "nuts-bytes-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.50") (d #t) (k 0)))) (h "0v2hilig9f96214cx3qqw8gigcb1jz6c27wf8npwjx15274q4jlc") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.4.1 (c (n "nuts-bytes") (v "0.4.1") (d (list (d (n "nuts-bytes-derive") (r "=0.4.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1v3diq4vidwv3lvc8066bz6sx9k7sw3rc0nzvwafcm1lk6irmd6v") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.4.2 (c (n "nuts-bytes") (v "0.4.2") (d (list (d (n "nuts-bytes-derive") (r "=0.4.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1hfjq0dj9g35ybk5w74qq1zwfmhj8xd3jw3sw64sywkmjkpg3x40") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.4.3 (c (n "nuts-bytes") (v "0.4.3") (d (list (d (n "nuts-bytes-derive") (r "=0.4.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "1djpwj2451a03jxrlx2l9kvzjjg0maygkmdydncb3a24a272i9k7") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.5.0 (c (n "nuts-bytes") (v "0.5.0") (d (list (d (n "nuts-bytes-derive") (r "=0.5.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "18iz5wfzchwzn3h9vh2a1gwfp9waqlzlbfsc3cvnyy9gb1p77rpb") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.6.0 (c (n "nuts-bytes") (v "0.6.0") (d (list (d (n "nuts-bytes-derive") (r "=0.6.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "13q2gfy8zk3ix1c66hjgkgay6d7lmf8gnn6al9nra55x7n3knb8q") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.6.1 (c (n "nuts-bytes") (v "0.6.1") (d (list (d (n "nuts-bytes-derive") (r "=0.6.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.56") (d #t) (k 0)))) (h "0w92snspr5pbrsdcssp9bj4axz1acpq5z6ic8z3rpd45k3n88zbv") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

(define-public crate-nuts-bytes-0.6.2 (c (n "nuts-bytes") (v "0.6.2") (d (list (d (n "nuts-bytes-derive") (r "=0.6.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0x4hjr8pw7k68x5xw7n722janvm3ik2xd7bbr19xlkzshgyfjxmp") (s 2) (e (quote (("derive" "dep:nuts-bytes-derive"))))))

