(define-module (crates-io nu ts nuts-backend) #:use-module (crates-io))

(define-public crate-nuts-backend-0.4.0 (c (n "nuts-backend") (v "0.4.0") (d (list (d (n "nuts-bytes") (r "=0.4.0") (d #t) (k 0)))) (h "16i9x2x8y5dm6lza4d37flgnvw5j3zidjin2965mh3w36w3snbrk")))

(define-public crate-nuts-backend-0.4.1 (c (n "nuts-backend") (v "0.4.1") (d (list (d (n "nuts-bytes") (r "=0.4.1") (d #t) (k 0)))) (h "11d9zwhy60b0mvdyk8pcshymch76lrighjbn5cqjxvid2a969x4s")))

(define-public crate-nuts-backend-0.4.2 (c (n "nuts-backend") (v "0.4.2") (d (list (d (n "nuts-bytes") (r "=0.4.2") (d #t) (k 0)))) (h "057mml25l9b5gk0hxwgwlqlcz5lspjfl4if0ang2flljdpwyd4n6")))

(define-public crate-nuts-backend-0.4.3 (c (n "nuts-backend") (v "0.4.3") (d (list (d (n "nuts-bytes") (r "=0.4.3") (d #t) (k 0)))) (h "1f8wqkjpg4k1xl01jrq2zlirij13mj4i1g229a778nlqrdm4k2yg")))

(define-public crate-nuts-backend-0.5.0 (c (n "nuts-backend") (v "0.5.0") (d (list (d (n "nuts-bytes") (r "=0.5.0") (d #t) (k 0)))) (h "0igwmhfqvvs2z0p3pbp7kwrwa2agfjf8wmlg43li66rajcjb1zl4")))

(define-public crate-nuts-backend-0.6.0 (c (n "nuts-backend") (v "0.6.0") (h "0giwbmskfxcy9qc2hdc8ly8gmd5mmfw1vfiq2ds5nzirchya6p71")))

(define-public crate-nuts-backend-0.6.1 (c (n "nuts-backend") (v "0.6.1") (h "07hvaiig6lsvzhq0hp52cdz55daqbl3scxwl26nx12jll2whxnzx")))

(define-public crate-nuts-backend-0.6.2 (c (n "nuts-backend") (v "0.6.2") (h "1bia79ajabahk7j6k9yzwl8hnfg3xkmgimb1afsfn30li7imp7wj")))

