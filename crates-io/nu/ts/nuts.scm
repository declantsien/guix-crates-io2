(define-module (crates-io nu ts nuts) #:use-module (crates-io))

(define-public crate-nuts-0.1.0 (c (n "nuts") (v "0.1.0") (d (list (d (n "stdweb") (r "^0.4.20") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0394sl7jkw2aj7x063hdsmaw5bm6w282iapjzh680sfwcvcxmlp3")))

(define-public crate-nuts-0.1.1 (c (n "nuts") (v "0.1.1") (h "04kq3qmrp6vx56c9rf5784ig0m41zv62yysqdv9jcr8hvwj90fpw")))

(define-public crate-nuts-0.2.0 (c (n "nuts") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "1np2xi21qhw7b28csz2vrzfc94xy6lf8rnjp126ys5yanv5vj55r") (f (quote (("web-debug" "web-sys") ("verbose-debug-log"))))))

(define-public crate-nuts-0.2.1 (c (n "nuts") (v "0.2.1") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "1nfwpbgbvw9fzxdbyyabvdk2j2g7ll9wgfk74dar1qa3aa2hy56y") (f (quote (("web-debug" "web-sys") ("verbose-debug-log"))))))

