(define-module (crates-io nu ts nuts-bytes-derive) #:use-module (crates-io))

(define-public crate-nuts-bytes-derive-0.2.0 (c (n "nuts-bytes-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1nh0wswi9s2jihbhm46wl7r4ip0dxsqc544lhva89n2phm50ji0i")))

(define-public crate-nuts-bytes-derive-0.2.1 (c (n "nuts-bytes-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "119l2dxiavqi2ii6kyw3r87ap8d81g7r1ahpf3ncp63f380jvsvh")))

(define-public crate-nuts-bytes-derive-0.2.2 (c (n "nuts-bytes-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1alqwmgfrcb58qngrd3y04wlxiz13fhrxyxdyspfynjlv4rbi01j")))

(define-public crate-nuts-bytes-derive-0.4.0 (c (n "nuts-bytes-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0b44w97ck507cg2nqcn2gpjnfjn1iz87is1xnxybf11wijjsia3x")))

(define-public crate-nuts-bytes-derive-0.4.1 (c (n "nuts-bytes-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jhcp0nwhzay0x9anwi4cs6l9bjai9psd3qjgxkzhgk2v1ra7rfq")))

(define-public crate-nuts-bytes-derive-0.4.2 (c (n "nuts-bytes-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1iyzl8j0x4a3hf80ciwas9619qc9aqpdpc5pycs261sb34znb9j4")))

(define-public crate-nuts-bytes-derive-0.4.3 (c (n "nuts-bytes-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00lw47yr9n3ln56f2i9b2g73mdpzkkigwnzmpr5x5kj9921vn68k")))

(define-public crate-nuts-bytes-derive-0.5.0 (c (n "nuts-bytes-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qfkscn9zpw6hhpx06vkwi1qlqq06ffa9mbp9451sz49hix3gryd")))

(define-public crate-nuts-bytes-derive-0.6.0 (c (n "nuts-bytes-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0knb3vp2acgbq3am9p8ba9qkqbgj62am2bf9gv6qw187sgym6n01")))

(define-public crate-nuts-bytes-derive-0.6.1 (c (n "nuts-bytes-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0s2pr5gkdii48vlhqxz5hbva7dz107sl6639ddfxfd4001cxabap")))

(define-public crate-nuts-bytes-derive-0.6.2 (c (n "nuts-bytes-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1al613vsbvxg6jgy7xiyg2sl5549ds5sqk7vkwwhy2czhndbkbx2")))

