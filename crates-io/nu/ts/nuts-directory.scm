(define-module (crates-io nu ts nuts-directory) #:use-module (crates-io))

(define-public crate-nuts-directory-0.1.0 (c (n "nuts-directory") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nuts-container") (r "^0.1.0") (f (quote ("backend"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rapvqimn724cqzdnhxw3jgp1rysl0b8zph1b5baxv1rvyl17lil")))

(define-public crate-nuts-directory-0.1.1 (c (n "nuts-directory") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.1.1") (d #t) (k 0)) (d (n "nuts-container") (r "^0.1.1") (f (quote ("backend"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q4qp3gjr1h24zkyazf21kspmj58bhgr1lasj585z4s58arvdk9j")))

(define-public crate-nuts-directory-0.2.0 (c (n "nuts-directory") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.1.1") (d #t) (k 0)) (d (n "nuts-container") (r "^0.2.0") (f (quote ("backend"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h72ngxlh233daw7qjwqamjczi7s56fn4g0y811pyjkkvi8c4l7a")))

(define-public crate-nuts-directory-0.3.0 (c (n "nuts-directory") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.1.1") (d #t) (k 0)) (d (n "nuts-container") (r "^0.2.0") (f (quote ("backend"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d2mfw266z714bc92pvlrmqfr6b8yj1mm79vhbj8412i5nayamgy")))

(define-public crate-nuts-directory-0.3.1 (c (n "nuts-directory") (v "0.3.1") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.1.1") (d #t) (k 0)) (d (n "nuts-container") (r "^0.2.0") (f (quote ("backend"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "10p2bhzfcqfckq86b0hplc49v2z15vm0z0q97ldzkayx08nlqxzg")))

(define-public crate-nuts-directory-0.3.2 (c (n "nuts-directory") (v "0.3.2") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nuts-bytes") (r "^0.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nuts-container") (r "^0.2.2") (f (quote ("backend"))) (k 0)))) (h "0hsha7i3spc0xgda567156mf7i1d3cfdif978lkn4hwzz9ap4y9i")))

(define-public crate-nuts-directory-0.4.0 (c (n "nuts-directory") (v "0.4.0") (d (list (d (n "getrandom") (r "=0.2.10") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.4.0") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r5w492sz3iblfs195cnzypalscga3v2765hpnhhyv1vn33v8fq9")))

(define-public crate-nuts-directory-0.4.1 (c (n "nuts-directory") (v "0.4.1") (d (list (d (n "getrandom") (r "=0.2.12") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.4.1") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14qiwd9pzn9y77i9pry2608jaf4y6y8dnb4sggc1cp7q0nyfp5s0")))

(define-public crate-nuts-directory-0.4.2 (c (n "nuts-directory") (v "0.4.2") (d (list (d (n "getrandom") (r "=0.2.12") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.4.2") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hqld3w42vxcap9xnahd3hvrdhaa4920vmqvmqinl7ls2lkhp7la")))

(define-public crate-nuts-directory-0.4.3 (c (n "nuts-directory") (v "0.4.3") (d (list (d (n "getrandom") (r "=0.2.12") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.4.3") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2rl9ghpiyli0y3yla86k26bl8fnipyhqbwa7qc5gh7ry5p9239")))

(define-public crate-nuts-directory-0.5.0 (c (n "nuts-directory") (v "0.5.0") (d (list (d (n "getrandom") (r "=0.2.12") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.5.0") (d #t) (k 0)) (d (n "nuts-bytes") (r "=0.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06gz1dgvdsj0lax3vprbbid0lnkxi7z6m13hibs9gx6ykz8a9q7s")))

(define-public crate-nuts-directory-0.6.0 (c (n "nuts-directory") (v "0.6.0") (d (list (d (n "getrandom") (r "=0.2.12") (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.6.0") (d #t) (k 0)) (d (n "nuts-tool-api") (r "=0.6.0") (o #t) (d #t) (k 0)))) (h "1s7v8kvv07m2qv6ra6ynyg4nxdqf4aqdg83r90fvnz664xnl949b") (s 2) (e (quote (("plugin" "dep:nuts-tool-api"))))))

(define-public crate-nuts-directory-0.6.1 (c (n "nuts-directory") (v "0.6.1") (d (list (d (n "getrandom") (r "=0.2.12") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "=0.4.20") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.6.1") (d #t) (k 0)) (d (n "nuts-tool-api") (r "=0.6.1") (o #t) (d #t) (k 0)))) (h "11hlb4l6sfdcqx65n25b7iy55papyryywlzcis89rr60dqviw066") (s 2) (e (quote (("plugin" "dep:nuts-tool-api"))))))

(define-public crate-nuts-directory-0.6.2 (c (n "nuts-directory") (v "0.6.2") (d (list (d (n "getrandom") (r "^0.2.15") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "nuts-backend") (r "=0.6.2") (d #t) (k 0)) (d (n "nuts-tool-api") (r "=0.6.2") (o #t) (d #t) (k 0)))) (h "00qz0r5m8hlzrfxs31ry9ggzsd5ychmlx702fy0rg3zgjrifi0x6") (s 2) (e (quote (("plugin" "dep:nuts-tool-api"))))))

