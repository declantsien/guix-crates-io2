(define-module (crates-io nu mn numnums) #:use-module (crates-io))

(define-public crate-numnums-0.1.0 (c (n "numnums") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0r00vmb9qcsfh1vb49qdakknhslk4fkli5m051ihnqirjymsj7yd")))

(define-public crate-numnums-0.1.1 (c (n "numnums") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "05qsxiajw2i3flhrjqx5cavdkm18vgkv1nlrx35kk7kaq5y9j037")))

