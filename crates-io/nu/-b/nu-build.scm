(define-module (crates-io nu -b nu-build) #:use-module (crates-io))

(define-public crate-nu-build-0.7.0 (c (n "nu-build") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "11y2hgsv7blwwvpcg0gl0sfvn7jymd45nn1mgiirdsj40ljravbz")))

(define-public crate-nu-build-0.8.0 (c (n "nu-build") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0i7fdk73bhva3sydb91w08xx1p70acw5y3xxdllgczcrgl75wrjz")))

(define-public crate-nu-build-0.9.0 (c (n "nu-build") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "02yfgd48sllifbg1l8mdzwm2zwrs3dv41whmqh1lmpw8a9nhivds")))

(define-public crate-nu-build-0.10.0 (c (n "nu-build") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0r7yksxxavdqnw7ndkyq10f0rv37hc3kdf1ypnna2ksj49ky36ql")))

(define-public crate-nu-build-0.11.0 (c (n "nu-build") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "070ndr9mj13scz0amxkpqdrv3p40qja9hwnkx4i6294lgr5h3i79")))

(define-public crate-nu-build-0.12.0 (c (n "nu-build") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1wsn9qzk7hswp1p60mdwd14yx3lvymrnyag7nszkmdiz3lzri82l")))

(define-public crate-nu-build-0.13.0 (c (n "nu-build") (v "0.13.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1n09jgqz5k7zif5g72hyyhbfqikarvjyc6qw8ayd2kgwg89wnldj")))

(define-public crate-nu-build-0.14.0 (c (n "nu-build") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "141v13dpvwr23fm7bddb1lznji8clr7naag24sxyc5v6b6379ps3")))

(define-public crate-nu-build-0.15.0 (c (n "nu-build") (v "0.15.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1bi8xxys6kgzqs3vpxprxi9jsqj671kza2vn90pf1svbf036l0ph")))

(define-public crate-nu-build-0.16.0 (c (n "nu-build") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1jzbrcy1jnzgxfvzviicsfnx4klzbiirlacsb75wbk0x1sgkkr12")))

(define-public crate-nu-build-0.17.0 (c (n "nu-build") (v "0.17.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1v37s33pzxrilxgicq1j34ij8g6f0g8q3y47ig6a15nnkj1zrw4a")))

