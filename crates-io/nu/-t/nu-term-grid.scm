(define-module (crates-io nu -t nu-term-grid) #:use-module (crates-io))

(define-public crate-nu-term-grid-0.60.0 (c (n "nu-term-grid") (v "0.60.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "01nmyzfac0jizgwyadjpfw53gxwh2q6ylr71rd1q1ldprvbapv47")))

(define-public crate-nu-term-grid-0.61.0 (c (n "nu-term-grid") (v "0.61.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1kpr1rrbr5lm2arpbzzxs3dk5z6h1sslaaj4lvb2vbmh0na0gzdw")))

(define-public crate-nu-term-grid-0.62.0 (c (n "nu-term-grid") (v "0.62.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0pcib2aa5632mnjgjvj05b1gdw427n8dv1q02d9mcng9xjznq4hm")))

(define-public crate-nu-term-grid-0.63.0 (c (n "nu-term-grid") (v "0.63.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1r381p9rdc372rykm6qk1nvgjxqs5wjfk1a2an2zl1gk7mz55jv9")))

(define-public crate-nu-term-grid-0.64.0 (c (n "nu-term-grid") (v "0.64.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "18cgy7ijiaisppil9hi23r8nzq4i14mfxg66vnvgy7s5fsidzdmj")))

(define-public crate-nu-term-grid-0.65.0 (c (n "nu-term-grid") (v "0.65.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "028gi1xnm0zsf49ibj5kgg0ss0plas0m2dgzyy7cr8kdra85xbvr")))

(define-public crate-nu-term-grid-0.66.0 (c (n "nu-term-grid") (v "0.66.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0gk9f2q3iwgisg09v0a3gzsi5q9snjyzv9wflcq6wsj9m7nfcfc3")))

(define-public crate-nu-term-grid-0.66.1 (c (n "nu-term-grid") (v "0.66.1") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0i4hxnk6f8hjr92jga9dxsqvdbkys26139i75g43yw65wy8l9ms6")))

(define-public crate-nu-term-grid-0.66.2 (c (n "nu-term-grid") (v "0.66.2") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1244g2wb7hz7hs26akqmmhi3dw0bpllc07r03izpsyz9jid57b7g")))

(define-public crate-nu-term-grid-0.67.0 (c (n "nu-term-grid") (v "0.67.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0n4v6r1g9b3959ksq83ia0klkinh32qd9s9ag6lgkzy32c44cpb3")))

(define-public crate-nu-term-grid-0.68.0 (c (n "nu-term-grid") (v "0.68.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "056zahds9g1l0cpy7faklzf2zyj08vv48v74ppcjc08h9lkb2cww")))

(define-public crate-nu-term-grid-0.68.1 (c (n "nu-term-grid") (v "0.68.1") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1cm0yf03g49qpaiwh3cjngk45zh465aav24mpvhi38j9ls62va2b")))

(define-public crate-nu-term-grid-0.69.0 (c (n "nu-term-grid") (v "0.69.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1xf7ml6yx8hd2dpbn4g2wyzr9qg1nkl7zzrw9bl1d96nplwl43jp")))

(define-public crate-nu-term-grid-0.69.1 (c (n "nu-term-grid") (v "0.69.1") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1ycczbplpqf6376blf7d4s5w4c2jadvjh029nwm3i17xzvi9zr08")))

(define-public crate-nu-term-grid-0.70.0 (c (n "nu-term-grid") (v "0.70.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0if22ys7bv975hphxa6cjk26jfqgpnh68mpql0w7k78r5r88m68c")))

(define-public crate-nu-term-grid-0.71.0 (c (n "nu-term-grid") (v "0.71.0") (d (list (d (n "nu-utils") (r "^0.71.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1nw3a2b76yb4v4sz93dn3y000svb2534kx5qcncvms5dl76ll5qy")))

(define-public crate-nu-term-grid-0.72.0 (c (n "nu-term-grid") (v "0.72.0") (d (list (d (n "nu-utils") (r "^0.72.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0l6v11y84g1v5j1h3xlaxqpvbqlcj06pankh420rwb3x4ys9angl")))

(define-public crate-nu-term-grid-0.73.0 (c (n "nu-term-grid") (v "0.73.0") (d (list (d (n "nu-utils") (r "^0.73.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1lr5vqb5ylgyz846qjm3bhvfwc1jyam29gb0n6471xkcbab2rvlb")))

(define-public crate-nu-term-grid-0.74.0 (c (n "nu-term-grid") (v "0.74.0") (d (list (d (n "nu-utils") (r "^0.74.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1hahffmhlankc41xmdn606rpvqvj9gb2d4i96zr315y4ik261149")))

(define-public crate-nu-term-grid-0.75.0 (c (n "nu-term-grid") (v "0.75.0") (d (list (d (n "nu-utils") (r "^0.75.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0bs431ldcr8hh6180y9mfd18p9i9nj0x9p30bbiwjma25ki6ahri")))

(define-public crate-nu-term-grid-0.76.0 (c (n "nu-term-grid") (v "0.76.0") (d (list (d (n "nu-utils") (r "^0.76.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0ic8difrfd5rsv4i03la4316zfadib5brj1x5pqiqpgfk6qj6f4v")))

(define-public crate-nu-term-grid-0.77.0 (c (n "nu-term-grid") (v "0.77.0") (d (list (d (n "nu-utils") (r "^0.77.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1iqh8kc1c9x66x27v48da6hcwzp198yxpjwx4sj8m9bmm0a966cg")))

(define-public crate-nu-term-grid-0.77.1 (c (n "nu-term-grid") (v "0.77.1") (d (list (d (n "nu-utils") (r "^0.77.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0n08kxlmpy5mjg872318phas5s4rzp5rdrjjhpmfaja0sgpdm088")))

(define-public crate-nu-term-grid-0.78.0 (c (n "nu-term-grid") (v "0.78.0") (d (list (d (n "nu-utils") (r "^0.78.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "16kv1rkjf825cgwlnr6lr87jy98wn1cnxwn4ywgkh0j0p9b2kwsj")))

(define-public crate-nu-term-grid-0.79.0 (c (n "nu-term-grid") (v "0.79.0") (d (list (d (n "nu-utils") (r "^0.79.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "043cb5hjgicl6gi7c1wm0s54chd3qbyzfd50p8lb6dhf1q3z7kdp")))

(define-public crate-nu-term-grid-0.80.0 (c (n "nu-term-grid") (v "0.80.0") (d (list (d (n "nu-utils") (r "^0.80.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "09p50rdvrp5lj8rd9zf2a8a4dk551wf6d7gb4cyp8ywxqx2i6vzj")))

(define-public crate-nu-term-grid-0.81.0 (c (n "nu-term-grid") (v "0.81.0") (d (list (d (n "nu-utils") (r "^0.81.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "13an0cibz2gbsrpskyz7lyhd0b4gq5jnvnf8w7w2xwy3q9bfr755")))

(define-public crate-nu-term-grid-0.82.0 (c (n "nu-term-grid") (v "0.82.0") (d (list (d (n "nu-utils") (r "^0.82.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "18fqyffhrxlnxsjnp2gih5y7mrhr6aq6z7l1nrbkwb2yil3c959y")))

(define-public crate-nu-term-grid-0.83.0 (c (n "nu-term-grid") (v "0.83.0") (d (list (d (n "nu-utils") (r "^0.83.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1lpyi0ns8ig50l1r6hd0n4y5kcg65r6d5d499nwpjgd1a4y5nxjj")))

(define-public crate-nu-term-grid-0.83.1 (c (n "nu-term-grid") (v "0.83.1") (d (list (d (n "nu-utils") (r "^0.83.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1pjkcyn33h0h0wl26h51dz2imnlrbw58vcr3038gysbhq8zccpks")))

(define-public crate-nu-term-grid-0.84.0 (c (n "nu-term-grid") (v "0.84.0") (d (list (d (n "nu-utils") (r "^0.84.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0f9yax3pcswfxf4hmrfwcdd6w3p1wc4vgc5yh8xzgd18qnlg92xh")))

(define-public crate-nu-term-grid-0.85.0 (c (n "nu-term-grid") (v "0.85.0") (d (list (d (n "nu-utils") (r "^0.85.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1z4vl1af8xi6h9yc6ylqz9ry48lh0mfd2n17wwqnh2gsarcd9sqz")))

(define-public crate-nu-term-grid-0.86.0 (c (n "nu-term-grid") (v "0.86.0") (d (list (d (n "nu-utils") (r "^0.86.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1rijc4cb5qshf519ya5fmzfg9f4szwwwhqh9ygj9fc0wpz1ygdjb")))

(define-public crate-nu-term-grid-0.87.0 (c (n "nu-term-grid") (v "0.87.0") (d (list (d (n "nu-utils") (r "^0.87.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0mivj7nzc39lh38w4d4an8p910dv3bh3hsx32fds27rhbr3z3x4a")))

(define-public crate-nu-term-grid-0.87.1 (c (n "nu-term-grid") (v "0.87.1") (d (list (d (n "nu-utils") (r "^0.87.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "01mqniah666sdl8lb26fbvkrv1zdnhrb981b3angsfrbvgv6k7r1")))

(define-public crate-nu-term-grid-0.88.0 (c (n "nu-term-grid") (v "0.88.0") (d (list (d (n "nu-utils") (r "^0.88.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "08zjb1mz067x9yar4m269f285g0832hyissjzf7c9wchdwrjyc3l")))

(define-public crate-nu-term-grid-0.88.1 (c (n "nu-term-grid") (v "0.88.1") (d (list (d (n "nu-utils") (r "^0.88.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1b40z6wz8gbs4gpw29ig48yw94b6xsbwlb3fqj2z6cl8fc3h8ldw")))

(define-public crate-nu-term-grid-0.89.0 (c (n "nu-term-grid") (v "0.89.0") (d (list (d (n "nu-utils") (r "^0.89.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "025vbyxsgjxcdz0gw9g28w927yjgg2qsabkqgh08h7g0alcdqgyh")))

(define-public crate-nu-term-grid-0.90.0 (c (n "nu-term-grid") (v "0.90.0") (d (list (d (n "nu-utils") (r "^0.90.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "15wp3j8r5x8axf69s07ncdkvkjjr051ghhqb18ba3rz091ksxnlw")))

(define-public crate-nu-term-grid-0.90.1 (c (n "nu-term-grid") (v "0.90.1") (d (list (d (n "nu-utils") (r "^0.90.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1as6fcwn3j0ahf8l6880cvxnb4drbk8yz44fmx4w71nj7x5dydjn")))

(define-public crate-nu-term-grid-0.91.0 (c (n "nu-term-grid") (v "0.91.0") (d (list (d (n "nu-utils") (r "^0.91.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "12f0i7m6flpkf1valkjfg6chalifpb65cknq91p22sii4dx0x89r")))

(define-public crate-nu-term-grid-0.92.0 (c (n "nu-term-grid") (v "0.92.0") (d (list (d (n "nu-utils") (r "^0.92.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "191lsb8zk4psd0l7bky65qz4mlcp8wvbcsnpi77cr12gc4qzv7ff")))

(define-public crate-nu-term-grid-0.92.1 (c (n "nu-term-grid") (v "0.92.1") (d (list (d (n "nu-utils") (r "^0.92.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1lhjrdsmyzgvpisp3qp0pip1ss2kzgdpmhl3jjsq15g0kpwchyv4")))

(define-public crate-nu-term-grid-0.92.2 (c (n "nu-term-grid") (v "0.92.2") (d (list (d (n "nu-utils") (r "^0.92.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "05wglad15hhl8cvc0923rx8ngs2gyr9gyiwl1kwbqlkrfirdl22h")))

(define-public crate-nu-term-grid-0.93.0 (c (n "nu-term-grid") (v "0.93.0") (d (list (d (n "nu-utils") (r "^0.93.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0qqlnz9p00jr82wcwdagb3brmz80279vg15lq928p6g8bcqakjp5")))

(define-public crate-nu-term-grid-0.94.0 (c (n "nu-term-grid") (v "0.94.0") (d (list (d (n "nu-utils") (r "^0.94.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0ga9ljmdi1xbkx25345mkwc53xkdny88zqwnnvbmz731gl35s4lj")))

