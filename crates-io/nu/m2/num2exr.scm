(define-module (crates-io nu m2 num2exr) #:use-module (crates-io))

(define-public crate-num2exr-0.1.0 (c (n "num2exr") (v "0.1.0") (d (list (d (n "exr") (r "^1.72") (k 0)))) (h "07ga4b933cx00bh1ihxdx8n2hq0k5wf9nizx31ss3ds9pcx3xx77")))

(define-public crate-num2exr-0.1.1 (c (n "num2exr") (v "0.1.1") (d (list (d (n "exr") (r "^1.72") (k 0)))) (h "05cgqi66zavsam921w08404pmz5lfshn2r3znvyx1kl4h16zw8ys")))

