(define-module (crates-io nu m2 num2words) #:use-module (crates-io))

(define-public crate-num2words-0.1.0 (c (n "num2words") (v "0.1.0") (h "025cfla8gml5r027iz8rl40f9b96vj1hxnrvgb7c7jzb3c4yj32d")))

(define-public crate-num2words-0.2.0 (c (n "num2words") (v "0.2.0") (h "0843i4c7ygsl1yqzhsbqafq768l6aab2fg3m6iqv22flm6x51b3z")))

(define-public crate-num2words-0.3.0 (c (n "num2words") (v "0.3.0") (h "06wq4wgz3f8cn5j6701f7z8zlriai3flcma9zpsr7nf6g3g6kjd7")))

(define-public crate-num2words-0.4.0 (c (n "num2words") (v "0.4.0") (h "1in45mm22ljf019dqw13hprmh7cc5hxjh9jvi0s5pndqcssmnsfh")))

(define-public crate-num2words-0.4.1 (c (n "num2words") (v "0.4.1") (h "1zzgpa21nlqgyysr59v03xxxgd8b0360cl9blraakdhj0lf7gyd1")))

(define-public crate-num2words-1.0.0 (c (n "num2words") (v "1.0.0") (d (list (d (n "num-bigfloat") (r "^1.6.2") (k 0)))) (h "0zhvcmm41p37j9mk0265dd341h6hxn7xsfjwp670z6wffqxj57c1")))

(define-public crate-num2words-1.0.1 (c (n "num2words") (v "1.0.1") (d (list (d (n "num-bigfloat") (r "^1.6.2") (k 0)))) (h "0i37xwlgisk3r231iffm5adg2743kl82f3x1i2wdzwwip69l8zr0")))

(define-public crate-num2words-1.1.0 (c (n "num2words") (v "1.1.0") (d (list (d (n "num-bigfloat") (r "^1.6.2") (k 0)))) (h "06jx24k98caczdd3qpskv9pfffmmbizf9vripw49nab0pqjyiyrk")))

(define-public crate-num2words-1.2.0 (c (n "num2words") (v "1.2.0") (d (list (d (n "num-bigfloat") (r "^1.7.1") (k 0)))) (h "01np8l5jsx0yjrz2503pshbn2sfwr7qzfhx4d7w6q7hfgac8pdra")))

