(define-module (crates-io nu mq numquant) #:use-module (crates-io))

(define-public crate-numquant-0.1.0 (c (n "numquant") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00brwz6m6rdqyp9bp00226q1z5jwlsdvpgn9sf4iqdsqfy0qakv2")))

(define-public crate-numquant-0.2.0 (c (n "numquant") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16advb1id8yrdkphrfajw7r9dp9gysnwc0qd5i1k59cssx1rx02l")))

