(define-module (crates-io nu mw numwit) #:use-module (crates-io))

(define-public crate-numwit-0.1.0 (c (n "numwit") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pxy5xq78idf6s8icgskkhracmnaq5shhsjpmjlzi864my2vi4kg")))

