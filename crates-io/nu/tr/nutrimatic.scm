(define-module (crates-io nu tr nutrimatic) #:use-module (crates-io))

(define-public crate-nutrimatic-0.1.0 (c (n "nutrimatic") (v "0.1.0") (d (list (d (n "any_ascii") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "184hf7bh1d1zjvl287rfvnj3vk971npc7zlm65r7i4vhnq2vdy3y")))

(define-public crate-nutrimatic-0.1.1 (c (n "nutrimatic") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "11lhr1qzip6hp240vmk57rky94ypgr8kgbpjypg05y1jb4nqwhp5")))

