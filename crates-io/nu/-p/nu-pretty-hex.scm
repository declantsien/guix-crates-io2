(define-module (crates-io nu -p nu-pretty-hex) #:use-module (crates-io))

(define-public crate-nu-pretty-hex-0.31.0 (c (n "nu-pretty-hex") (v "0.31.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.29.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1crsmihbil68p3qb345d33m5p6xa7dkbdxrbxfxh7km4hflc2pq4")))

(define-public crate-nu-pretty-hex-0.32.0 (c (n "nu-pretty-hex") (v "0.32.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0c076j68lv6vakgp0579nn1qrcrxpk6g6lvnczqsvkp3gzl15msl")))

(define-public crate-nu-pretty-hex-0.33.0 (c (n "nu-pretty-hex") (v "0.33.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0mh2b67mglvswmx51mfdjx0k2rqr8g4bgx9fdnwhwk7vdwvrhvxz")))

(define-public crate-nu-pretty-hex-0.34.0 (c (n "nu-pretty-hex") (v "0.34.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0zc33npid3fkl1nj4k2pq3rak4ws89n2c781vwwiyrnp4525imif")))

(define-public crate-nu-pretty-hex-0.35.0 (c (n "nu-pretty-hex") (v "0.35.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.35.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0mg1xdrss9jvc7kmn65b14abm2yhjgdx7yzpz1iry9907hfvkwvc")))

(define-public crate-nu-pretty-hex-0.36.0 (c (n "nu-pretty-hex") (v "0.36.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.36.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1d4qvry8xknlsqr1dmqjsfwz3l2avxwwa5jg9p3rrbwcqxppvsq1")))

(define-public crate-nu-pretty-hex-0.37.0 (c (n "nu-pretty-hex") (v "0.37.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.37.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0zz171iqf0q1cjzvxi52ipnjlgw6jbq58m9cd9difbf560fqk0mq")))

(define-public crate-nu-pretty-hex-0.38.0 (c (n "nu-pretty-hex") (v "0.38.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.38.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0x2x1l0bghdzmq2cz4x549mcbabm29jxdc3fnsinjrrnxqhx6sj8")))

(define-public crate-nu-pretty-hex-0.39.0 (c (n "nu-pretty-hex") (v "0.39.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 2)) (d (n "nu-ansi-term") (r "^0.39.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0fyg0nhfrfr3cn07r8giqg45az2sc0dqy1jpp13mm29qqwjfz934")))

(define-public crate-nu-pretty-hex-0.40.0 (c (n "nu-pretty-hex") (v "0.40.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.40.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1nd8j5cpcd6f727r42cpgj2f3510sjysvvgm7w48fj51ywv3dd7j")))

(define-public crate-nu-pretty-hex-0.41.0 (c (n "nu-pretty-hex") (v "0.41.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.41.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "17p1y52h4k3gi370dn0lwm26hwlwbm5sc96fg4sbcsq98xfqx2l6")))

(define-public crate-nu-pretty-hex-0.42.0 (c (n "nu-pretty-hex") (v "0.42.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.42.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1bxmcwqzfbp9p7ihdx9hblwfz17s47346fkz75n8n80xm4zml30s")))

(define-public crate-nu-pretty-hex-0.43.0 (c (n "nu-pretty-hex") (v "0.43.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.43.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "04zqqpadx55782s85xh32cb9iss0q8q3alk5n17ic49pxlk7c3jz")))

(define-public crate-nu-pretty-hex-0.44.0 (c (n "nu-pretty-hex") (v "0.44.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.44.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1dizakbxm2c6vpjy5brqbjhk8m7lr7wammibb1vr8nq85aazrj5r")))

(define-public crate-nu-pretty-hex-0.60.0 (c (n "nu-pretty-hex") (v "0.60.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.45.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1zf52pvrlyaak7zaalhj7hckyihmqxmnvha6k6lsdgyhlma8vx0w")))

(define-public crate-nu-pretty-hex-0.61.0 (c (n "nu-pretty-hex") (v "0.61.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.45.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1hlb0pzknx0lk75f7w9m0lc00kk2qj3mlv6ipdgbva506pda5ccv")))

(define-public crate-nu-pretty-hex-0.62.0 (c (n "nu-pretty-hex") (v "0.62.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.45.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1dqld62iy7hi2fg2l87w2430gz6vplip426wy46ki57y7pfn606p")))

(define-public crate-nu-pretty-hex-0.63.0 (c (n "nu-pretty-hex") (v "0.63.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.45.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "03lam05q0wnm3wqz7v147wmnfmvr2ifw3016sj1f6nnfxwjmfjg9")))

(define-public crate-nu-pretty-hex-0.64.0 (c (n "nu-pretty-hex") (v "0.64.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0cwa69w3f4xaajslc6ikr4987rcxk6a9pxsz7zy2x2jqhrcz27dz")))

(define-public crate-nu-pretty-hex-0.65.0 (c (n "nu-pretty-hex") (v "0.65.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1ysapmizs2grj9q51hgl91mxisrhlvvpmjnx09dh41x54djhwvp1")))

(define-public crate-nu-pretty-hex-0.66.0 (c (n "nu-pretty-hex") (v "0.66.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0ac1cbslks3b28qbi29d28hrnagfr0i7x306blkgzmri9lkwsrf9")))

(define-public crate-nu-pretty-hex-0.66.1 (c (n "nu-pretty-hex") (v "0.66.1") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "130wx1wqslg7q6rr7vdfklvpplq55bafyc3v0yzckbs2nwg8cxqd")))

(define-public crate-nu-pretty-hex-0.66.2 (c (n "nu-pretty-hex") (v "0.66.2") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07gx2cjzrcfbwmr8wr0w3krzksi7km3qvrqsgyhn6dlbhnwb2dd5")))

(define-public crate-nu-pretty-hex-0.67.0 (c (n "nu-pretty-hex") (v "0.67.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1yqgw99c5wzp29lwr273fc5x5fg5a2k9kgxss0fjxarfam8k7hm1")))

(define-public crate-nu-pretty-hex-0.68.0 (c (n "nu-pretty-hex") (v "0.68.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1f52pz7gpwal0kw9fk0zir9n3r29aka3a4wlxh75585ayqyi699y")))

(define-public crate-nu-pretty-hex-0.68.1 (c (n "nu-pretty-hex") (v "0.68.1") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07z4wp1ksdxkh2pnpmpr4fc3lpwhq3vd1ys85gzxr16iaxrm4kzi")))

(define-public crate-nu-pretty-hex-0.69.0 (c (n "nu-pretty-hex") (v "0.69.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1qdixjc21iypah2yybs0d726x5f16m1n1cfppw3z0lpr3yhpbd5w")))

(define-public crate-nu-pretty-hex-0.69.1 (c (n "nu-pretty-hex") (v "0.69.1") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0mwx6rnbaalbybv0m9qzzqnhd7xif22slb0rbwdf04yzdmz6dacz")))

(define-public crate-nu-pretty-hex-0.70.0 (c (n "nu-pretty-hex") (v "0.70.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "10xrarav8ih70wn7sqp4in1xnj84f5r4vfcgiijypshnn0mc3rph")))

(define-public crate-nu-pretty-hex-0.71.0 (c (n "nu-pretty-hex") (v "0.71.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0252s4q2287n3f83mw1c4zw1ry2v7985q2rqdz2hywmwxyzbfy61")))

(define-public crate-nu-pretty-hex-0.72.0 (c (n "nu-pretty-hex") (v "0.72.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0wwvg46ibfc556x1zjr0fkx2nqzwq6wccmkgfyh7qa8mjy8zw7hm")))

(define-public crate-nu-pretty-hex-0.73.0 (c (n "nu-pretty-hex") (v "0.73.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "07gdljkr54kb2glai0m6lzc46jgi7nkqgljhvf6fpsdd8q8yblnc")))

(define-public crate-nu-pretty-hex-0.74.0 (c (n "nu-pretty-hex") (v "0.74.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "14i224hhs12l6ixxfmik0s6cy57s0lf13iy0llqnci2l1s7w2hs0")))

(define-public crate-nu-pretty-hex-0.75.0 (c (n "nu-pretty-hex") (v "0.75.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "19w5m0svlr7rxqc2z6l9dk7nkagqg036w7f1v90a4z8gk59ay88m")))

(define-public crate-nu-pretty-hex-0.76.0 (c (n "nu-pretty-hex") (v "0.76.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ix9sbfyq63h0qlgzfs9ajqf5ndncfjfxwwyflifbii9j96xnnwg")))

(define-public crate-nu-pretty-hex-0.77.0 (c (n "nu-pretty-hex") (v "0.77.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "09cpkxlmjd78jdh92m85p0zj8zaqm3yp4908la4d75jlsj8q39v6")))

(define-public crate-nu-pretty-hex-0.77.1 (c (n "nu-pretty-hex") (v "0.77.1") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0j2k12y1pg1d5sjnh0dab746450n8dlmigy5wca3nkxbpfdv5gww")))

(define-public crate-nu-pretty-hex-0.78.0 (c (n "nu-pretty-hex") (v "0.78.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1pcsiv07pibfp4h2wff0r6nv03rypbjmzhwq7h7h5b9r4d28bmd4")))

(define-public crate-nu-pretty-hex-0.79.0 (c (n "nu-pretty-hex") (v "0.79.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1bvcrxzw4nv4ahinjahfxfap2427k0l6qpqdi8svkwy6bs3pks72")))

(define-public crate-nu-pretty-hex-0.80.0 (c (n "nu-pretty-hex") (v "0.80.0") (d (list (d (n "heapless") (r "^0.7.8") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1lksmnqd9cf9hznm0wp6rx4qz6dw6m21njd9b544a9hvsisfmzp6")))

(define-public crate-nu-pretty-hex-0.81.0 (c (n "nu-pretty-hex") (v "0.81.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1vwwvszry4gq8r1yiyj02mlypparq3blw55zwyyg7b4lwx1bnmfc")))

(define-public crate-nu-pretty-hex-0.82.0 (c (n "nu-pretty-hex") (v "0.82.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.47.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1c0dbc4k48z5vrnjc1h24blr3mikrzz1137qgz49s4dkbh7fzzpx")))

(define-public crate-nu-pretty-hex-0.83.0 (c (n "nu-pretty-hex") (v "0.83.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pi7km688k68drvbqsisagrzv1pdgg56s17af2b0cxxk06455s1f")))

(define-public crate-nu-pretty-hex-0.83.1 (c (n "nu-pretty-hex") (v "0.83.1") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xk78gjvfc6cy3vlv94y7a9bnqrcvdzwgrxplhypk3lqr75sii34")))

(define-public crate-nu-pretty-hex-0.84.0 (c (n "nu-pretty-hex") (v "0.84.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0zzk1rmn0hp5m3671f7i7s39mgxj6jlqi9d0w5xmpg597r916xsv")))

(define-public crate-nu-pretty-hex-0.85.0 (c (n "nu-pretty-hex") (v "0.85.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1897c0rg1pd217fgqpd7ilg5mkq8aazdwy1s6y43vc38mlv5ww9d")))

(define-public crate-nu-pretty-hex-0.86.0 (c (n "nu-pretty-hex") (v "0.86.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fr7mzff4vavqqs47amzw7i8xf8a9d1vfdq8v0ji91c0vh12rm0g")))

(define-public crate-nu-pretty-hex-0.87.0 (c (n "nu-pretty-hex") (v "0.87.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jmcayrchgn9bgi5w7ml50j3arnm56fh3agbqm29qc8w44xdan8y")))

(define-public crate-nu-pretty-hex-0.87.1 (c (n "nu-pretty-hex") (v "0.87.1") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qm6kk1fbqs1pxn66y8fm3lxfsvcrpbhzp2jmpfrncgcaynljj4k")))

(define-public crate-nu-pretty-hex-0.88.0 (c (n "nu-pretty-hex") (v "0.88.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0svg827l3fhibiz3zrwzs73p9wpdkpk6clr8sji8wxyvgpzf2lp1")))

(define-public crate-nu-pretty-hex-0.88.1 (c (n "nu-pretty-hex") (v "0.88.1") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "12i534984736spygc21n9qn0mmparmpjlprpf9wgxrn9blncmjb0")))

(define-public crate-nu-pretty-hex-0.89.0 (c (n "nu-pretty-hex") (v "0.89.0") (d (list (d (n "heapless") (r "^0.7") (k 2)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1lgh9cc6qdifbr5b1ar9lkkpg86a13k4mayi3wia58clkxibb5qg")))

(define-public crate-nu-pretty-hex-0.90.0 (c (n "nu-pretty-hex") (v "0.90.0") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ij34b8pkp9xk32730ia5q0rlwirxzpssriwv0x3cx2jbng9zczn")))

(define-public crate-nu-pretty-hex-0.90.1 (c (n "nu-pretty-hex") (v "0.90.1") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1bad64kn6v12jjdy6ikigr9iqb4zjcpq39fwfnxjfk4zcdxbv7ca")))

(define-public crate-nu-pretty-hex-0.91.0 (c (n "nu-pretty-hex") (v "0.91.0") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1iq8amp5hqf2xxp5n74l5sgqv2bj204zwbjcnarhy88ijzjicrl6")))

(define-public crate-nu-pretty-hex-0.92.0 (c (n "nu-pretty-hex") (v "0.92.0") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "097zg83wp8wjvvr0v42zzmwg17qls12a6kbv749mh3lrzcj5i0lp")))

(define-public crate-nu-pretty-hex-0.92.1 (c (n "nu-pretty-hex") (v "0.92.1") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1vz61invla8kqm9gqpi9qz7xp4rmsqvbwml75ivmzn5a2h4611sy")))

(define-public crate-nu-pretty-hex-0.92.2 (c (n "nu-pretty-hex") (v "0.92.2") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1gzgqmlp8qr3i34b9pdxl26mk6bvdiy3gm58vkr8fx3rypp631ph")))

(define-public crate-nu-pretty-hex-0.93.0 (c (n "nu-pretty-hex") (v "0.93.0") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1032adhqpchs9m0jb7g6kvybqlwxyai0q5p51z36m554ag530qr8")))

(define-public crate-nu-pretty-hex-0.94.0 (c (n "nu-pretty-hex") (v "0.94.0") (d (list (d (n "heapless") (r "^0.8") (k 2)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0fi5qf4qng5q95cqpqgfp1jasmwc7193xb04l6al4ph18f1i42br")))

