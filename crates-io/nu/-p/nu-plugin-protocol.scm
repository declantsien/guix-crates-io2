(define-module (crates-io nu -p nu-plugin-protocol) #:use-module (crates-io))

(define-public crate-nu-plugin-protocol-0.93.0 (c (n "nu-plugin-protocol") (v "0.93.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "nu-utils") (r "^0.93.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "0vvlvgjy795vhi129s148hgkml82x9adwb887jfwm3ay7r0hj3hw") (f (quote (("local-socket") ("default" "local-socket"))))))

(define-public crate-nu-plugin-protocol-0.94.0 (c (n "nu-plugin-protocol") (v "0.94.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "nu-utils") (r "^0.94.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "16lgyq4ncaxb945g98x8zli2zmbskxwg3b5di6sy2c869mn3dsl2") (f (quote (("local-socket") ("default" "local-socket"))))))

