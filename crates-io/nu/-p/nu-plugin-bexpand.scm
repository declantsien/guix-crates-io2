(define-module (crates-io nu -p nu-plugin-bexpand) #:use-module (crates-io))

(define-public crate-nu-plugin-bexpand-1.0.0 (c (n "nu-plugin-bexpand") (v "1.0.0") (d (list (d (n "bexpand") (r "^1.0.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1fgdbrblmid2p6m0z036f18cbmbbmrvayyr51lal7s8hdasbd77x")))

(define-public crate-nu-plugin-bexpand-1.0.1 (c (n "nu-plugin-bexpand") (v "1.0.1") (d (list (d (n "bexpand") (r "^1.0.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "005fq5j27npfsym7kpmsv20c86fx0nvrblia9yl22nkbc1d0h7h0")))

(define-public crate-nu-plugin-bexpand-1.0.2 (c (n "nu-plugin-bexpand") (v "1.0.2") (d (list (d (n "bexpand") (r "^1.0.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0krhbqzilkq102vy9qjyq3095zfgxr5bnb6gil98fy0x8iawapy8")))

(define-public crate-nu-plugin-bexpand-1.0.3 (c (n "nu-plugin-bexpand") (v "1.0.3") (d (list (d (n "bexpand") (r "^1.0.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0sniwd50x233iprx4015q1a83nlyqzjj1yvsygff5hjqy1gp4v40")))

(define-public crate-nu-plugin-bexpand-1.1.1 (c (n "nu-plugin-bexpand") (v "1.1.1") (d (list (d (n "bexpand") (r "^1.1.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1mp8qjq4vwr5v1h249kz89vswmanck0gb0x8jcagdzqngbk6m1ld")))

(define-public crate-nu-plugin-bexpand-1.1.2 (c (n "nu-plugin-bexpand") (v "1.1.2") (d (list (d (n "bexpand") (r "^1.1.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "02aak087pihrphg6cmrmc40rm8rxdwfzyad3jfpp7zlnysrakasi")))

(define-public crate-nu-plugin-bexpand-1.1.3 (c (n "nu-plugin-bexpand") (v "1.1.3") (d (list (d (n "bexpand") (r "^1.1.3") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "18lcrbhnw2jyq4gyfv5wan3a9x90a0bjrngw4i3lp5fm1qqmmx9i")))

(define-public crate-nu-plugin-bexpand-1.1.4 (c (n "nu-plugin-bexpand") (v "1.1.4") (d (list (d (n "bexpand") (r "^1.1.3") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "081gdhwipjj0821irhkq9jhah8cx67gda4mv40b159ggdwxdfjmf")))

(define-public crate-nu-plugin-bexpand-1.1.5 (c (n "nu-plugin-bexpand") (v "1.1.5") (d (list (d (n "bexpand") (r "^1.1.3") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "02ryscvx98sixmybwh7xynzc0ig9498x4f9m7slmmvbbwgbi6c1i")))

(define-public crate-nu-plugin-bexpand-1.2.0 (c (n "nu-plugin-bexpand") (v "1.2.0") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "16l8cs00z1dn8gpa4lcqr3q5wmsklg3gw4008flx71npx7viachc")))

(define-public crate-nu-plugin-bexpand-1.3.0 (c (n "nu-plugin-bexpand") (v "1.3.0") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1dmdn4m3xhkhvxj7li16sz8hqnwlnhfv12vaax9n472flrjc7y0c")))

(define-public crate-nu-plugin-bexpand-1.3.1 (c (n "nu-plugin-bexpand") (v "1.3.1") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "12a77a0rj2larn6w1s8znn794cdv3iaqn72l2n11bcsna1w7023x")))

(define-public crate-nu-plugin-bexpand-1.3.2 (c (n "nu-plugin-bexpand") (v "1.3.2") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0pkqqf17xifz7vza8qg29i8bl3z2whj7syzw1bii1jfvdlvs0pqw")))

(define-public crate-nu-plugin-bexpand-1.3.3 (c (n "nu-plugin-bexpand") (v "1.3.3") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0vb6zb7pcp8givqry3nfknzvsyf34gclyvwljih1nbqlc1ala7d8")))

(define-public crate-nu-plugin-bexpand-1.3.4 (c (n "nu-plugin-bexpand") (v "1.3.4") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.91") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91") (f (quote ("plugin"))) (d #t) (k 0)))) (h "09859s8knb5d872la7ciz4jg1xwzki05yw1564rll59nf6lmfvac")))

(define-public crate-nu-plugin-bexpand-1.3.5 (c (n "nu-plugin-bexpand") (v "1.3.5") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92") (f (quote ("plugin"))) (d #t) (k 0)))) (h "13hqlghjjdly7rsgp7xbzx65h2invwbwmm4yg63b68nczqmbp6qv")))

(define-public crate-nu-plugin-bexpand-1.3.6 (c (n "nu-plugin-bexpand") (v "1.3.6") (d (list (d (n "bexpand") (r "^1.2.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.93") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1l5r1s4nbqj3ga0ppndnk0dm2qb68h5rgyfjm06y5cqfg7fia6q0")))

