(define-module (crates-io nu -p nu-path) #:use-module (crates-io))

(define-public crate-nu-path-0.33.0 (c (n "nu-path") (v "0.33.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "0qjmh55l5w2s8xh5rs9rc353awyivkqrlgj7h6i80g676fd7ciak")))

(define-public crate-nu-path-0.34.0 (c (n "nu-path") (v "0.34.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1by7y09ldiqzj0h9n30k351rxhv3ycs7z72d4gsw2g7c33gzaqsc")))

(define-public crate-nu-path-0.35.0 (c (n "nu-path") (v "0.35.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "0dckmj1agrlpxbwkdqdn1msslm4xdsyd8b5ypfz4mxzmhwwxcfnh")))

(define-public crate-nu-path-0.36.0 (c (n "nu-path") (v "0.36.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1pjx76zblqmpllkv3p6hrf5my85xf9jwds0v067a9d3nfpm2c46g")))

(define-public crate-nu-path-0.37.0 (c (n "nu-path") (v "0.37.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "0l5ha47cayjncbw93y1qji0f4inpg2g1kzs9rfwsd938pwgjg98z")))

(define-public crate-nu-path-0.38.0 (c (n "nu-path") (v "0.38.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1wyws5282gwgaiwwlfjdb3bqmgqf4ykjhnn1dd60yjpa1wkynq46")))

(define-public crate-nu-path-0.39.0 (c (n "nu-path") (v "0.39.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "0naljlrhi7i3naxk2cqm54ky2n8y35cdlrc0c1nqzsc0kjz5667k")))

(define-public crate-nu-path-0.40.0 (c (n "nu-path") (v "0.40.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1jd472rahdhc4zjfgxg5gj1ivr5y6ijyc1rg5pfraldf640pzghf")))

(define-public crate-nu-path-0.41.0 (c (n "nu-path") (v "0.41.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1ssgcy4xxlw297im849afqhrkcck0b4lkkpjsky6ilvc5z6qi89i")))

(define-public crate-nu-path-0.42.0 (c (n "nu-path") (v "0.42.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "083czgx9wbx6kr3q223bghjpq0wrcy916n2gp011zl9hvja32aj0")))

(define-public crate-nu-path-0.43.0 (c (n "nu-path") (v "0.43.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "0girfz9yihrkps3zpzc1jgnwmfxqbnxpq0i6vxqrwyj7ipglc8z7")))

(define-public crate-nu-path-0.44.0 (c (n "nu-path") (v "0.44.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "069df6ikkaimcr1pjiycfr3ma6sjmb4ajmyxqdckckaqx4nj5wqk")))

(define-public crate-nu-path-0.60.0 (c (n "nu-path") (v "0.60.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "1gdk6gm18ak5mnhgi4kd7pmbwc16ba3rmrsrk4wypmq0a946wrf2")))

(define-public crate-nu-path-0.61.0 (c (n "nu-path") (v "0.61.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)))) (h "04s74s1p461dk5vwzc5bp4k7z03x0g40jyb18farj311hn2qi6am")))

(define-public crate-nu-path-0.62.0 (c (n "nu-path") (v "0.62.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0ncldzsdyxx07157fa7sywl1s1535ikjddd6qf1cpzdqx14c846f")))

(define-public crate-nu-path-0.63.0 (c (n "nu-path") (v "0.63.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0kx6m0dh0wgndsvdwmgyx2fgq2lann9hfsrg77fm060vywzc90xk")))

(define-public crate-nu-path-0.64.0 (c (n "nu-path") (v "0.64.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "02lywpz0jziqykpy4bjchc7wlc6dfxddpbi56gl5x7j5xnhzix0f")))

(define-public crate-nu-path-0.65.0 (c (n "nu-path") (v "0.65.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0iwg4r3qhyya1jrvzv0jgvgpmgq5lc3aiyr6vc2wvrddqvqgn5ky")))

(define-public crate-nu-path-0.66.0 (c (n "nu-path") (v "0.66.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1ird5fn63k60ygdi7n72dd2fjsallhq8xz3kpbfjjkaax2app8c2")))

(define-public crate-nu-path-0.66.1 (c (n "nu-path") (v "0.66.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1kkldzq9cv55nz1phz4lfif80hnbbvdikh80lfcyab9p70n0c0yq")))

(define-public crate-nu-path-0.66.2 (c (n "nu-path") (v "0.66.2") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "133l9rm42rzi4gfaq6vdpc898abwh27hq85m4hz2wfcxmh98461b")))

(define-public crate-nu-path-0.67.0 (c (n "nu-path") (v "0.67.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0n083agl9i0yxb6c5n92qq0kylmhjv274mcvzybzfklk7gay204v")))

(define-public crate-nu-path-0.68.0 (c (n "nu-path") (v "0.68.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1pyw9bydh5pp6vzv42abindy7m8s5mv6frizw8nqcy3384gnflc3")))

(define-public crate-nu-path-0.68.1 (c (n "nu-path") (v "0.68.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0ydhixcwy2g4hc8xpgrwkrd24j9r9zg7fnkhw8kbszwya5r0nc4z")))

(define-public crate-nu-path-0.69.0 (c (n "nu-path") (v "0.69.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0gwc7nd9pn54f76hpsjxqqkmj5k3hxvj0g668jjc5dybqni5bqhc")))

(define-public crate-nu-path-0.69.1 (c (n "nu-path") (v "0.69.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "137ccz9djl36002c6pccj786072p128m782iriwmlnsr2pw5h020")))

(define-public crate-nu-path-0.70.0 (c (n "nu-path") (v "0.70.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0x9nbs52gjr1yar4n9dg68nakhixr11gac3g3xrci50xwcgkzyvs")))

(define-public crate-nu-path-0.71.0 (c (n "nu-path") (v "0.71.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1zg3x7xxynwwgi2h65ndi484zwhkqsf3b0hjqfwaz2m7rq406f90")))

(define-public crate-nu-path-0.72.0 (c (n "nu-path") (v "0.72.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0li9szy0gm6wbhw7hmhj2v353d43hvdb88spvydvlf4qw2h8jcvh")))

(define-public crate-nu-path-0.73.0 (c (n "nu-path") (v "0.73.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "06q9mx4gnw2cbi49c1lh2pl1sh153nrz7k10xcrwkn3bizgnkwzd")))

(define-public crate-nu-path-0.74.0 (c (n "nu-path") (v "0.74.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0303qmrhx26hqaa2k2a4518ah6wxs0mpwlz5jykh844ix83f1dw9")))

(define-public crate-nu-path-0.75.0 (c (n "nu-path") (v "0.75.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0svs5b914dcfmbzk9xz3i911z2nssld0y5j0wzfcgsr7r9dr3mml")))

(define-public crate-nu-path-0.76.0 (c (n "nu-path") (v "0.76.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "13yg14napq18hlqkvw3wb1mma8ss9bqj53g7200mkpj54kh61yv2")))

(define-public crate-nu-path-0.77.0 (c (n "nu-path") (v "0.77.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "08wm60k3xwpp6jmhi0ir8wf56cbf7l618w8in6ifb94xla812ri3")))

(define-public crate-nu-path-0.77.1 (c (n "nu-path") (v "0.77.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0p58bydkan4jzmv9z3027ivscsfrk6zylknvw2s2d95gawzn695r")))

(define-public crate-nu-path-0.78.0 (c (n "nu-path") (v "0.78.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0rmmpc5ig65mqc9mhfq372524b43q5g72s8mflbndis19hwsf47y")))

(define-public crate-nu-path-0.79.0 (c (n "nu-path") (v "0.79.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "105n4ny466224i8vs8f3b3p38s4231pfgkmxclja9qm15inrdzgj")))

(define-public crate-nu-path-0.80.0 (c (n "nu-path") (v "0.80.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3.1") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0wwp9rww6xgpw2bcpi953z30wpxg6gbaffpn355i66hzdjqkr6qh")))

(define-public crate-nu-path-0.81.0 (c (n "nu-path") (v "0.81.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1c4ahq3jn6qw8r6n969c6g1ic8w1x8qrs8jbi2ancj14775zy1i9")))

(define-public crate-nu-path-0.82.0 (c (n "nu-path") (v "0.82.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1jhz8zqwqxhr6vc5qz9ppyjzgzflvv4qaxwagnacxax248b56bhd")))

(define-public crate-nu-path-0.83.0 (c (n "nu-path") (v "0.83.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "17vl4181ln7q14k8bfrkrbkziyj73b6kpb25dxp3qi0cqqpza2h4")))

(define-public crate-nu-path-0.83.1 (c (n "nu-path") (v "0.83.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "04q4w1lryhszgdrvr7ky8pl5nilv2ahwy5fn9f83gzk0cska6m8f")))

(define-public crate-nu-path-0.84.0 (c (n "nu-path") (v "0.84.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1ywca5wd65i88qq6sigw4kzngfbil8cvdhhv9y4cmkjjlz8p6zc3")))

(define-public crate-nu-path-0.85.0 (c (n "nu-path") (v "0.85.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1bzkz4dlns8rvhk70c1a72d0mq69nz44wgr2ih0zmw2viqhf0dwx")))

(define-public crate-nu-path-0.86.0 (c (n "nu-path") (v "0.86.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1fkynjaw9b8q3dr16aq699hz2xw2cgs7ph3kpblymrspa2g9yhqc")))

(define-public crate-nu-path-0.87.0 (c (n "nu-path") (v "0.87.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "0vncij02y6drv3k9n63bs5xasz34vpm8px6vy5r1kqc9ynbxp833")))

(define-public crate-nu-path-0.87.1 (c (n "nu-path") (v "0.87.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "049id2zndlrf49g5rl53m8nscklykpw7i5yzm0mr9blwrw7lvhf1")))

(define-public crate-nu-path-0.88.0 (c (n "nu-path") (v "0.88.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1bgj7p56sba8xm3bcz33yhbqg2sqcxgwjxjv6fj19scbwhvdrs0f")))

(define-public crate-nu-path-0.88.1 (c (n "nu-path") (v "0.88.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "0agarl4ywszlwsiipqspd4yp7la6fsls0fw9zdfwjdr6vzird6xq")))

(define-public crate-nu-path-0.89.0 (c (n "nu-path") (v "0.89.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1i9h9si270zapc9g0zl8q4dxx7gxzq4laxcgr4xyp3vxkg021bh2")))

(define-public crate-nu-path-0.90.0 (c (n "nu-path") (v "0.90.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1l9dyk5hamikc2vrclfvyjj8qrfh9clvvw33xsah615r1zwhgdy5")))

(define-public crate-nu-path-0.90.1 (c (n "nu-path") (v "0.90.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1ac2g166qv2h25y1xm6gm0i3qcbps57a4xj4hi1amnjq8wkvqkaa")))

(define-public crate-nu-path-0.91.0 (c (n "nu-path") (v "0.91.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "146lm48vna9w5kr46dclqmzl1cbz5k7j1zz6jl8i6d83np4nn1sa")))

(define-public crate-nu-path-0.92.0 (c (n "nu-path") (v "0.92.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1bl2nlzv0sip0fggr2dv5g2y0r8ja759j6cxkg81j39xp3dkq3xp")))

(define-public crate-nu-path-0.92.1 (c (n "nu-path") (v "0.92.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "0aivlw9wdlzp2r93wm001kn43qxgdprml1s7khbrwc9rndi274jx")))

(define-public crate-nu-path-0.92.2 (c (n "nu-path") (v "0.92.2") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1vhkp1p9a1hpq7wqi8bba1rr6qkwqv66nrwiq5py0m5lryl3cggc")))

(define-public crate-nu-path-0.93.0 (c (n "nu-path") (v "0.93.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "0kx4cy84a50z8722p4k867xagm4sik1zg664gkxhfagldanfbvvr")))

(define-public crate-nu-path-0.94.0 (c (n "nu-path") (v "0.94.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "omnipath") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "pwd") (r "^1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\"), not(target_os = \"android\")))") (k 0)))) (h "1wxfkjfrkhwnk5gnfjd9kwa1ywzk012kcxzg0mpdg5q4h964738z")))

