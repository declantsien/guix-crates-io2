(define-module (crates-io nu tm nutmeg) #:use-module (crates-io))

(define-public crate-nutmeg-0.0.0 (c (n "nutmeg") (v "0.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0c0rj568rs1wi4vnz8h32010ga78pwq76gxx6klfz87b1p17ms8n")))

(define-public crate-nutmeg-0.0.1 (c (n "nutmeg") (v "0.0.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "11rf8vv4wzb0h1wfi5pfq6572va0byankyxqv394ghvjkynyzjgx")))

(define-public crate-nutmeg-0.0.2 (c (n "nutmeg") (v "0.0.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0nry4i1nsgilldlx1ifqlykfmqlp5hs0mfszn9m231mkd2b0a6s3")))

(define-public crate-nutmeg-0.1.0 (c (n "nutmeg") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0vhlnxny92400vd42xp6xmdfdb5k0afllb91ccp1agqaiv2dfxnv")))

(define-public crate-nutmeg-0.1.1 (c (n "nutmeg") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0mky3dcp9a4w7csd77lz3i04507yivs3gz967r9bm1qxyn5qxxfh")))

(define-public crate-nutmeg-0.1.2 (c (n "nutmeg") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1sh0ah8f876qd3c0skasi7dx24kf1g1hmk7cv3gp6f4kac31zky4")))

(define-public crate-nutmeg-0.1.3 (c (n "nutmeg") (v "0.1.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "14s461d6zcnpsdzhb6h9kww6sddz6c338r98i1h219xn5zamhvlm")))

(define-public crate-nutmeg-0.1.4 (c (n "nutmeg") (v "0.1.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "12w43vawzzll2b9sxph1i4r8c3hcf7rjx8sg4qpkf74hlqzkc2r1")))

