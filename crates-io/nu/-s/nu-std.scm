(define-module (crates-io nu -s nu-std) #:use-module (crates-io))

(define-public crate-nu-std-0.79.0 (c (n "nu-std") (v "0.79.0") (d (list (d (n "miette") (r "^5.6.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-parser") (r "^0.79.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.79.0") (d #t) (k 0)))) (h "0lsmghzbqjwp0nahx4slnd8gml4sxli0gvwkn57g99ld9d6558cw")))

(define-public crate-nu-std-0.80.0 (c (n "nu-std") (v "0.80.0") (d (list (d (n "miette") (r "^5.6.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-parser") (r "^0.80.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.80.0") (d #t) (k 0)))) (h "0x4lvd4jbyk571a0kifskccfwbjf7nls6cij57m4l0mw9kkcirmz")))

(define-public crate-nu-std-0.81.0 (c (n "nu-std") (v "0.81.0") (d (list (d (n "miette") (r "^5.9") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (d #t) (k 0)))) (h "1vykh5d4x0i9lw3yyhpk12lqlh6yly3k7wlwgqybycpwn3mr9552")))

(define-public crate-nu-std-0.82.0 (c (n "nu-std") (v "0.82.0") (d (list (d (n "miette") (r "^5.9") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.82.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.82.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.82.0") (d #t) (k 0)))) (h "0gw7jz2zyhygc955nhbzjir6zzbqr1kxbbbi839gxnjkjn3dmvrx")))

(define-public crate-nu-std-0.83.0 (c (n "nu-std") (v "0.83.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.83.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.83.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.0") (d #t) (k 0)))) (h "18fvxjrmg1bblsn2prk3igb2afi8nq0qdcfnrylyd56ry23lhgqn")))

(define-public crate-nu-std-0.83.1 (c (n "nu-std") (v "0.83.1") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-parser") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)))) (h "03c4na8rlwsfnpnw3b65sr0hfwby759r9id5d3a4x84isx8crzzp")))

(define-public crate-nu-std-0.84.0 (c (n "nu-std") (v "0.84.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.84.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.84.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.84.0") (d #t) (k 0)))) (h "17ljhs04ybs48ajiims6swl15cmfy1k5pp58qbkryfsql8dl8pq5")))

(define-public crate-nu-std-0.85.0 (c (n "nu-std") (v "0.85.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (d #t) (k 0)))) (h "1jq4603x37zakpykvwpkdsv72szhwnzna3z2xki0s5hg431z6cf2")))

(define-public crate-nu-std-0.86.0 (c (n "nu-std") (v "0.86.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (d #t) (k 0)))) (h "1c2cn9jy0l86kjcwy9zlmgifwjp52iah75b2lbjd5vl72qm7ngng")))

(define-public crate-nu-std-0.87.0 (c (n "nu-std") (v "0.87.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (d #t) (k 0)))) (h "1sljqbhvliwraz83i1zby9z7wymysv3jq1q3s9kdyadk3qasb3fd")))

(define-public crate-nu-std-0.87.1 (c (n "nu-std") (v "0.87.1") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-parser") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (d #t) (k 0)))) (h "0zk7x8fq6x5gxf12k04sr4c133281736f30b8n7f7n7av5zbarx8")))

(define-public crate-nu-std-0.88.0 (c (n "nu-std") (v "0.88.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.88.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.88.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.0") (d #t) (k 0)))) (h "11v3a2vgs8avp6r0ipa8xic94f61gss8mcm2vfx5sqps26z9hsi5")))

(define-public crate-nu-std-0.88.1 (c (n "nu-std") (v "0.88.1") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.88.1") (d #t) (k 0)) (d (n "nu-parser") (r "^0.88.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.1") (d #t) (k 0)))) (h "0k7zg57v5ia52b415l85ygs6lhqfj7f1l4765cva1l9p7ww6acrq")))

(define-public crate-nu-std-0.89.0 (c (n "nu-std") (v "0.89.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (d #t) (k 0)))) (h "179w3ps544rdghj0w3cwp60qivcqhs611kcw33p94wdbnvnn2y7g")))

(define-public crate-nu-std-0.90.1 (c (n "nu-std") (v "0.90.1") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-parser") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (d #t) (k 0)))) (h "0z28r4zcdj0z18zms41hw6nnnd36ci6ps49c0fbf0hlb66nv3gag")))

(define-public crate-nu-std-0.91.0 (c (n "nu-std") (v "0.91.0") (d (list (d (n "miette") (r "^7.1") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (d #t) (k 0)))) (h "1igdid80qbfgqdmcg6szq2rsi7i5qlyhplw74byh81vkqsn5z74w")))

(define-public crate-nu-std-0.92.0 (c (n "nu-std") (v "0.92.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miette") (r "^7.2") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.0") (d #t) (k 0)))) (h "1gx4kxj3i2i5kv24jh0wpa3av84d91lwl6d7v74nmpan5fn1pcfm")))

(define-public crate-nu-std-0.92.1 (c (n "nu-std") (v "0.92.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miette") (r "^7.2") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-parser") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.1") (d #t) (k 0)))) (h "03iiggcn5msbk8vnyflz5db4vw6wfq3a2zrx82whpvb965xaiarc")))

(define-public crate-nu-std-0.92.2 (c (n "nu-std") (v "0.92.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miette") (r "^7.2") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.92.2") (d #t) (k 0)) (d (n "nu-parser") (r "^0.92.2") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.2") (d #t) (k 0)))) (h "16qyvydb8axj4cxkflh39rsvhf8s480q052ykg1qprjcrg662nn3")))

(define-public crate-nu-std-0.93.0 (c (n "nu-std") (v "0.93.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miette") (r "^7.2") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)))) (h "1z99anpb18qxmyifnzik4gb0i9c8fzg4h086pwi2dqpsy1240mn2")))

(define-public crate-nu-std-0.94.0 (c (n "nu-std") (v "0.94.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miette") (r "^7.2") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "nu-engine") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (d #t) (k 0)))) (h "0c2y34jgr56jgvy5s09b6yykafvy6g887l8dwshzab0952n9gai3")))

