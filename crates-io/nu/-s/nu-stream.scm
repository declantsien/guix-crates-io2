(define-module (crates-io nu -s nu-stream) #:use-module (crates-io))

(define-public crate-nu-stream-0.25.0 (c (n "nu-stream") (v "0.25.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.0") (d #t) (k 0)))) (h "1i2p13rprj4mn5nmvkx7wb2p3gfdzv58rzs6g86151wwvjfmf9r6") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.25.1 (c (n "nu-stream") (v "0.25.1") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.1") (d #t) (k 0)))) (h "1d6mj3fyyhcj50rvapfl2rmmsw6fbh7kx7nvxii059j0ppa4b6h2") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.26.0 (c (n "nu-stream") (v "0.26.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.26.0") (d #t) (k 0)))) (h "1y7f28zxfrlhi9qrpbk6dpzb3wr6f59yhx3yim001796c1x6xlcs") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.27.0 (c (n "nu-stream") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.0") (d #t) (k 0)))) (h "1hz9n380rnvvg86qi9bfika6c702kn4b9ljsgcwrr984mm3vbb2n") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.27.1 (c (n "nu-stream") (v "0.27.1") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.1") (d #t) (k 0)))) (h "09xm93mk0kyi4nd6f1x89yfk3139ahamirs2zwnzr97f7xng216v") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.28.0 (c (n "nu-stream") (v "0.28.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.28.0") (d #t) (k 0)))) (h "02dl4d9gzl2jhg71hfc04x9j4p29iwwcyvm6m9pihwfpwb3q2bhj") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.29.0 (c (n "nu-stream") (v "0.29.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.29.0") (d #t) (k 0)))) (h "15zx16s4wb23316ih4wqnnpajh1qz5vckpzk3k41qqkmar1w4sah") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.30.0 (c (n "nu-stream") (v "0.30.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.30.0") (d #t) (k 0)))) (h "1wb2njwfxf1mmb9yc07gmchamg8z8p5paifnn5sbrhbrd25xvpmg") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.31.0 (c (n "nu-stream") (v "0.31.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.31.0") (d #t) (k 0)))) (h "0ylmfqhhj4aaspwq51yb5wmsn7zhh19035yb0mr28ajygf38s1g7") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.32.0 (c (n "nu-stream") (v "0.32.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.32.0") (d #t) (k 0)))) (h "188p0a1kr4x1ha11znjgh42q0b7f256mvy4cwsgqxh5bijyzb2df") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.33.0 (c (n "nu-stream") (v "0.33.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.33.0") (d #t) (k 0)))) (h "170z0niss6f883d08gwsjy392h2g1p434i9nvmg02dba839da5g0") (f (quote (("trace") ("stable"))))))

(define-public crate-nu-stream-0.34.0 (c (n "nu-stream") (v "0.34.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.34.0") (d #t) (k 0)))) (h "075rfkl1bhr08g9pn0r5ji7dnbxc92ggrf454x0cfnjvjsxiy6n7") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.35.0 (c (n "nu-stream") (v "0.35.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.35.0") (d #t) (k 0)))) (h "1l1lc4p3jylfb6x56527vjphqjr3a5yh8ci1rgysargh3jnzj990") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.36.0 (c (n "nu-stream") (v "0.36.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.36.0") (d #t) (k 0)))) (h "0siqhyxdbl9zpyxkdlysrd1369lrvikp72awx7pii5dzvgv2aaaj") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.37.0 (c (n "nu-stream") (v "0.37.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.37.0") (d #t) (k 0)))) (h "0z0zqddfc3ydi2x6yhjnhn3fnilvg7x9hspd2l3mni4q2v70y4mr") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.38.0 (c (n "nu-stream") (v "0.38.0") (d (list (d (n "nu-errors") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.38.0") (d #t) (k 0)))) (h "1q06h6klmdwclx4pp1k6pc2f93w5d75n4bm08h8rw3w1x9m75h31") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.39.0 (c (n "nu-stream") (v "0.39.0") (d (list (d (n "nu-errors") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.39.0") (d #t) (k 0)))) (h "0gq6bb3c26r46j92syr69nkw220k57ldjf4rw8ykbly7lgj0v84l") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.40.0 (c (n "nu-stream") (v "0.40.0") (d (list (d (n "nu-errors") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.40.0") (d #t) (k 0)))) (h "02chzpc9pncjc1fdwrhxmpibdqpxkbdqwpq19dhz2vsxak8maklp") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.41.0 (c (n "nu-stream") (v "0.41.0") (d (list (d (n "nu-errors") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.41.0") (d #t) (k 0)))) (h "0gfaji9wcps9kv43cqzdmz6akl8c1iyvp8fbjcj74a89srmw9x2j") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.42.0 (c (n "nu-stream") (v "0.42.0") (d (list (d (n "nu-errors") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.42.0") (d #t) (k 0)))) (h "0zmjslv20j2793a79qda7wl4p2nyq1mxirlrn1qy5fnnr0g34z5p") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.43.0 (c (n "nu-stream") (v "0.43.0") (d (list (d (n "nu-errors") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.43.0") (d #t) (k 0)))) (h "11v103gnwp4jxvwmk6dj93yrr6dqxl3r3yhfzlb0q416ai0x4h3k") (f (quote (("stable"))))))

(define-public crate-nu-stream-0.44.0 (c (n "nu-stream") (v "0.44.0") (d (list (d (n "nu-errors") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.44.0") (d #t) (k 0)))) (h "14fs2dqa4papd48zil35901z4sc0p15dga96jbrsbhimk85g0gba") (f (quote (("stable"))))))

