(define-module (crates-io nu tt nuttx-embedded-hal) #:use-module (crates-io))

(define-public crate-nuttx-embedded-hal-1.0.0 (c (n "nuttx-embedded-hal") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)))) (h "07db0k9lviqsjsclyrpw4m82xbvqvfikmxn59250wnv3qwi6q9bi") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.1 (c (n "nuttx-embedded-hal") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)))) (h "0mivw3r6gpvl8bvs9rc9qyrsa0klx2kw8gpjzilvw7rnv9gis7pl") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.2 (c (n "nuttx-embedded-hal") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)))) (h "04adrik9mbaw7rxiawp2dc12pq389axfd6vbfdqc3h4q99mpri4x") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.3 (c (n "nuttx-embedded-hal") (v "1.0.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)))) (h "0z3sph3m2wzmd9xpq32b7qszjzcwswb9sp80w37sich85gkshqkv") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.4 (c (n "nuttx-embedded-hal") (v "1.0.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0ad0wlzscmhxaapc6sb2sgk0cb2m259qxl6r9k783jpnf4z34mzl") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.5 (c (n "nuttx-embedded-hal") (v "1.0.5") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0ih5c6adwrwaa8dzdw89n675x9jmsl4c3gvkhamy9wy67sk61cga") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.6 (c (n "nuttx-embedded-hal") (v "1.0.6") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0y09y0sw99bias2196fmmr4ndd9vnill5xxxd1l560cvgj7pa5mr") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.7 (c (n "nuttx-embedded-hal") (v "1.0.7") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "07px9qkdd84fc5wb39g10dfzqkd2xjjrhp1iq1nqv6jx5qlgjcmb") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.8 (c (n "nuttx-embedded-hal") (v "1.0.8") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0xl5905lz4dcw5m6l5jqzypnymba7qc56gbkl27f3j0b6a0yg8nz") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.9 (c (n "nuttx-embedded-hal") (v "1.0.9") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0zs1m5xm6lgwgfsaxbywq7rdiv3gcwfsw28j986bcfjskvlij85f") (f (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1.0.10 (c (n "nuttx-embedded-hal") (v "1.0.10") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1s5nci0dj3bg0adj6bc49waacwxgi3nynsd92c213ypj69j237y6") (f (quote (("default"))))))

