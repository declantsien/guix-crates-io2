(define-module (crates-io nu mt numtest) #:use-module (crates-io))

(define-public crate-numtest-0.1.0 (c (n "numtest") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0x00qbw9x7ims4hx90qpgm3sfgzw71a8qywf3wix731ahnhl3k82")))

(define-public crate-numtest-0.1.1 (c (n "numtest") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0la0mr53sxd5h167mmcfldzhb2hnfn605ia7d3nxlabwfimndf4a")))

(define-public crate-numtest-0.1.2 (c (n "numtest") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0yyyj4y3hmf1d1yv8lxxk4sih60sf9wvcrafp6pky6hkshcdkh4b")))

(define-public crate-numtest-0.1.3 (c (n "numtest") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1vrlc27lfy8warkka4q7svsgvzkwhvgdkhbwf005vpl0m9r34wgg")))

