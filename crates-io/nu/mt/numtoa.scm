(define-module (crates-io nu mt numtoa) #:use-module (crates-io))

(define-public crate-numtoa-0.0.1 (c (n "numtoa") (v "0.0.1") (h "1si45jb0zf0p5rsy9pccvhyycb06czqpric0cc7vnng9jgrygljl") (y #t)))

(define-public crate-numtoa-0.0.2 (c (n "numtoa") (v "0.0.2") (h "0rsv5rnq6jhz9wvrzsbhy8mgcnl3jfvhkr5wygmbnxcqb2pbwqjw") (y #t)))

(define-public crate-numtoa-0.0.3 (c (n "numtoa") (v "0.0.3") (h "1yrgddn23dgh4ff05kw9cfmf9fcndz84ai1bnfbqd1kvsm5x9i25") (y #t)))

(define-public crate-numtoa-0.0.4 (c (n "numtoa") (v "0.0.4") (h "19pw6wqcg3lbyrw4p18zvf1q3acg6yn7dsymm6f5rwlkx5magdg1") (y #t)))

(define-public crate-numtoa-0.0.5 (c (n "numtoa") (v "0.0.5") (h "1f6xccg0dhl0gi4pzb2asdcqxsb1gc1rffvkwxj74sq4q4jb5dy9")))

(define-public crate-numtoa-0.0.6 (c (n "numtoa") (v "0.0.6") (h "1slcg67pm5ka61r3q5h4z4mlbrh2f7wpbaxkzzrl70wlxmnwdx24")))

(define-public crate-numtoa-0.0.7 (c (n "numtoa") (v "0.0.7") (h "13wg769b3crrq0x2vk6s3zww8fpkqfqbsl7wih92vfhyvw4wp3sw")))

(define-public crate-numtoa-0.1.0 (c (n "numtoa") (v "0.1.0") (h "1vs9rhggqbql1p26x8nkha1j06wawwgb2jp5fs88b5gi7prvvy5q") (f (quote (("std"))))))

(define-public crate-numtoa-0.2.0 (c (n "numtoa") (v "0.2.0") (h "0sndjpkgcxk7414bc2g8wdpcvhic6y0j5hy4378gkg5fk41xbfzj") (f (quote (("std")))) (y #t)))

(define-public crate-numtoa-0.2.1 (c (n "numtoa") (v "0.2.1") (h "1iprnwins1z3pb1nrlj7mj15ffxf5ab6bl0rs62kqi6hfgbrf2d2") (y #t)))

(define-public crate-numtoa-0.2.2 (c (n "numtoa") (v "0.2.2") (h "15nf5sjky1kadjymjzpavwpmrfry35yj3za9cf9mzzxb5grl63w5") (y #t)))

(define-public crate-numtoa-0.2.3 (c (n "numtoa") (v "0.2.3") (h "1c10ndd0wk48blhljqhlwrpni23246jzkg9abpxc3cm0xynvc8g5")))

(define-public crate-numtoa-0.2.4 (c (n "numtoa") (v "0.2.4") (h "03yhkhjb3d1zx22m3pgcbpk8baj0zzvaxqc25c584sdq77jw98ka")))

