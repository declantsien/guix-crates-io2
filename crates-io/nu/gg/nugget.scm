(define-module (crates-io nu gg nugget) #:use-module (crates-io))

(define-public crate-nugget-0.0.1 (c (n "nugget") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "0cxj3jrw67h8wkwldkbrqxq9zzsd1798w3f53yysr8nkmk44vhfd")))

(define-public crate-nugget-0.0.2 (c (n "nugget") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "125lqfl7iqnlqb5qskn50khwf1bih2ffx64wsf4ksw64pv1p26lp")))

