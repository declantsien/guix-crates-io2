(define-module (crates-io nu -h nu-hist) #:use-module (crates-io))

(define-public crate-nu-hist-0.1.0 (c (n "nu-hist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "inline_colorization") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)))) (h "1z43dg76bzsrxyvkp78sxx7kdla6a3ycpdxhfxbbp66g8c76v2v3")))

