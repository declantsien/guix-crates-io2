(define-module (crates-io nu kl nuklear-rust) #:use-module (crates-io))

(define-public crate-nuklear-rust-0.1.0 (c (n "nuklear-rust") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nuklear-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0qdwkwhcvcbvd2arlwbvwcn9gdzcbvncy1zpq3liqn92x0b9k5xj") (f (quote (("default"))))))

(define-public crate-nuklear-rust-0.1.1 (c (n "nuklear-rust") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nuklear-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1fxm80b7g3n1j410k6489ha1sxjmlyn5la6sf6b4rvn3diwf12l8") (f (quote (("default"))))))

(define-public crate-nuklear-rust-0.1.2 (c (n "nuklear-rust") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nuklear-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1h17wydh3ijg3lz4ci7zh9f04iqcr0j47azmwl23yi0z93vgzf1l") (f (quote (("default"))))))

(define-public crate-nuklear-rust-0.1.3 (c (n "nuklear-rust") (v "0.1.3") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0zizjcv9jp30czn8iqd8aqpz1y7xidd0vymcrd2ipjg17cdirab9") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.1.4 (c (n "nuklear-rust") (v "0.1.4") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^0.1.3") (d #t) (k 0)))) (h "106ir857mi88cm4zzk5bq4171jp036kv594w9s4wqbr6v0b8w0fc") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.1.5 (c (n "nuklear-rust") (v "0.1.5") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.156.0") (d #t) (k 0)))) (h "082aq3ary6xmndrdz13i1dv9jz3yiq4lmy7151miwhwa54ddvlrw") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.1.6 (c (n "nuklear-rust") (v "0.1.6") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.156.0") (d #t) (k 0)))) (h "0nzpw1dmxf2hhpd1g0qgxlsvv56nh2s63rc44iqm6n756a6dq87k") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.1.7 (c (n "nuklear-rust") (v "0.1.7") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.156.0") (d #t) (k 0)))) (h "06cgwn1sxchvlkz4g8clcscwv4dx5slsl9kjd1dzqxrpmddl8ibg") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.1.8 (c (n "nuklear-rust") (v "0.1.8") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.156.0") (d #t) (k 0)))) (h "12ym3364kdy88ywlbplk74fib0v6pryphb6rihbmy1mj941jbsi7") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.2.0 (c (n "nuklear-rust") (v "0.2.0") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.33.0") (d #t) (k 0)))) (h "168jl0fb34kk9m51v4vxpjyrilf637lgx4wyc6pxvicbr5gxb8yx") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.2.1 (c (n "nuklear-rust") (v "0.2.1") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.33.0") (d #t) (k 0)))) (h "0lipks196pqflypfzvkq1rrk8ing03cs677vqlj7d8fmnm34k48b") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.3.0 (c (n "nuklear-rust") (v "0.3.0") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.33.0") (d #t) (k 0)))) (h "0ngifib3cb3xdwhrljy8l2r7blh4v9spl18bw273nfsb3hqx8dzk") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.3.1 (c (n "nuklear-rust") (v "0.3.1") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "^1.33.0") (d #t) (k 0)))) (h "16vysaw6grcwqv603vvxjx5ahpwjxngwpxqwd98niv2v0vcmyvbx") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.3.2 (c (n "nuklear-rust") (v "0.3.2") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~1.33") (d #t) (k 0)))) (h "046g25i7cj6113j9zc69cq4wspjnjdvw1hndlsagz3si6qm56bk7") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.4.0 (c (n "nuklear-rust") (v "0.4.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~1.33") (d #t) (k 0)))) (h "0kf8x55ixijsqynn1rywyd9sgx5qqxb5mdcmqb80qg7v2n61yw9s") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.5.0 (c (n "nuklear-rust") (v "0.5.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~1.33") (d #t) (k 0)))) (h "1sgqz0ngak0wbdmr68jmkfjyq5ps6j0b676akrq9xpi5j3qz386q") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.5.1 (c (n "nuklear-rust") (v "0.5.1") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~1.33") (d #t) (k 0)))) (h "0j0qa6v9c9ql34y5896bz1pjinhlsjnrzy1p4ky42531a21amp76") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.6.0 (c (n "nuklear-rust") (v "0.6.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~4.0") (d #t) (k 0)))) (h "1nywmdy0k1fpmpbyrq6lc5x9rpyazl31sispvyzz7wd44cbacdnq") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.6.1 (c (n "nuklear-rust") (v "0.6.1") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~4.0") (d #t) (k 0)))) (h "1xihh5qjkxpdr77v52q49a759sgwbql5nx22cvxd5ymq8cd6mrf7") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.6.2 (c (n "nuklear-rust") (v "0.6.2") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-sys") (r "~4.0") (d #t) (k 0)))) (h "18c2y20kbxjm1j9672k0sgl25bgbrjpqwmadxyx9z89chz9v3b7d") (f (quote (("rust_allocator") ("default"))))))

(define-public crate-nuklear-rust-0.6.3 (c (n "nuklear-rust") (v "0.6.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "nuklear-sys") (r "^4.0.5") (d #t) (k 0)))) (h "0531ziha8hwy1v0rrp0rgky8zm33wfhcf8m9495g4l9nxfkq873m") (f (quote (("rust_allocator") ("default"))))))

