(define-module (crates-io nu kl nuklear-sys) #:use-module (crates-io))

(define-public crate-nuklear-sys-0.1.0 (c (n "nuklear-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0hnfyn7nqkxlw6inx161bk5kifm44p3f6wgr66r6f2hcl8sdz69c")))

(define-public crate-nuklear-sys-0.1.1 (c (n "nuklear-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "08d7swsdp7dalnpa6cb6rv79jfx06r91s3vc5dqx1qzz0yk7gfbg")))

(define-public crate-nuklear-sys-0.1.2 (c (n "nuklear-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0myw9xb2ncb32bjj5snfhqqrlisbshg9gl9r4hbb4908b5w5j2vn")))

(define-public crate-nuklear-sys-0.1.3 (c (n "nuklear-sys") (v "0.1.3") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1zf0z08s8z6fp3rkciqla4cl3izknpvgkak6ccimr04s4xbc8lwh")))

(define-public crate-nuklear-sys-1.156.0 (c (n "nuklear-sys") (v "1.156.0") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0cjkqdq79nhknq75phzl6xk73mrwlssz5grywjk5zva9mgkc0yf8") (y #t)))

(define-public crate-nuklear-sys-1.33.0 (c (n "nuklear-sys") (v "1.33.0") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0sbnma94izfifpl333jmv8ka3ir44mdcql2mm55k5lnfbi06ap7w")))

(define-public crate-nuklear-sys-1.33.1 (c (n "nuklear-sys") (v "1.33.1") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "047qivm58lr6gjvbpinz7a0p91n74xnxcqjnjngdwhwr70ygznqp")))

(define-public crate-nuklear-sys-1.33.2 (c (n "nuklear-sys") (v "1.33.2") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0sf8nf8r5jjqpl0njbjfvksyjcay3py328fzii8dym021vlmksgg")))

(define-public crate-nuklear-sys-4.0.2 (c (n "nuklear-sys") (v "4.0.2") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "19chwwj04dicm2gskkpr667im8zmqyvygl41vs13spqinsaah1yh") (l "nuklear")))

(define-public crate-nuklear-sys-4.0.3 (c (n "nuklear-sys") (v "4.0.3") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "16bvsbpm9g31ppc0hh338h0fyl1vf13qr025dc70g3nbp25hb2c3") (l "nuklear")))

(define-public crate-nuklear-sys-4.0.4 (c (n "nuklear-sys") (v "4.0.4") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "031nlc1kzhhzqrqpp1s08dz3sm3qpz5gxmwnxa22cvv4m1y2jqma") (l "nuklear")))

(define-public crate-nuklear-sys-4.0.5 (c (n "nuklear-sys") (v "4.0.5") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "11c6q7lzyb8x9mwrbk0xsxkdyz7gl6dnqjc0vaxn1fxjdrkq1ksj") (l "nuklear")))

