(define-module (crates-io nu kl nuklear-backend-glium) #:use-module (crates-io))

(define-public crate-nuklear-backend-glium-0.1.0 (c (n "nuklear-backend-glium") (v "0.1.0") (d (list (d (n "glium") (r "~0.16") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "1j46vmfg3phwkmycxqiwgd69a71ysmh1m5gdr0pa4bmnv9v7bh2b")))

(define-public crate-nuklear-backend-glium-0.2.0 (c (n "nuklear-backend-glium") (v "0.2.0") (d (list (d (n "glium") (r "~0.16") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.3") (d #t) (k 0)))) (h "1sqq1daxjbi3i8c7kzk4mj627aadj4kkj8jrkvnykrw8415v5as0")))

(define-public crate-nuklear-backend-glium-0.3.0 (c (n "nuklear-backend-glium") (v "0.3.0") (d (list (d (n "glium") (r "~0.22") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.4") (d #t) (k 0)))) (h "1cw0vp2pr7wilmjnsky9fnhirhi6vsm604pgrs7nph266ils4zpr")))

(define-public crate-nuklear-backend-glium-0.3.1 (c (n "nuklear-backend-glium") (v "0.3.1") (d (list (d (n "glium") (r "~0.22") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.5") (d #t) (k 0)))) (h "003ylyp0win8a93b741ad57dx5633qv85wczsa6fky3apabrknxw")))

(define-public crate-nuklear-backend-glium-0.4.0 (c (n "nuklear-backend-glium") (v "0.4.0") (d (list (d (n "glium") (r "~0.24") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.6") (d #t) (k 0)))) (h "1y4n70psxxlla25gs9vwnzc0522231xssmn5sb6kl3iwc9gqyjy8")))

(define-public crate-nuklear-backend-glium-0.5.0 (c (n "nuklear-backend-glium") (v "0.5.0") (d (list (d (n "glium") (r "~0.30") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.6") (d #t) (k 0)))) (h "06r6qcwn1m2b7fdsq010jazpbgklibqxi08mz34nsm2c00iv2w3q")))

