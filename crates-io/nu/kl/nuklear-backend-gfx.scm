(define-module (crates-io nu kl nuklear-backend-gfx) #:use-module (crates-io))

(define-public crate-nuklear-backend-gfx-0.1.1 (c (n "nuklear-backend-gfx") (v "0.1.1") (d (list (d (n "gfx") (r "^0.12.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.11.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nuklear-rust") (r "^0.1.1") (d #t) (k 0)))) (h "0f1w16f7q6811lqm8avlk0pk3wrr72k48wm1vl7964qkk2rs529r")))

(define-public crate-nuklear-backend-gfx-0.1.2 (c (n "nuklear-backend-gfx") (v "0.1.2") (d (list (d (n "gfx") (r "^0.12.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.11.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nuklear-rust") (r "^0.1.2") (d #t) (k 0)))) (h "1l5d5mxs12fyv0g6dgg3hdnnkxhppdik7px25pr5y4ysc07ry0a3")))

(define-public crate-nuklear-backend-gfx-0.1.3 (c (n "nuklear-backend-gfx") (v "0.1.3") (d (list (d (n "gfx") (r "~0.12") (d #t) (k 0)) (d (n "gfx_device_gl") (r "~0.11") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "~0.12") (d #t) (k 0)) (d (n "glutin") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "15h4mb386i8y6aa5i4fjmg9j316z1lp6hmrx44zjlrrpj9wjdnwl")))

(define-public crate-nuklear-backend-gfx-0.1.4 (c (n "nuklear-backend-gfx") (v "0.1.4") (d (list (d (n "gfx") (r "~0.12") (d #t) (k 0)) (d (n "gfx_device_gl") (r "~0.11") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "~0.12") (d #t) (k 0)) (d (n "glutin") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "1nfizz4gdjs5phz0ppwc257is6j451wjlsxa0m5qapynk6idrcx1")))

(define-public crate-nuklear-backend-gfx-0.1.5 (c (n "nuklear-backend-gfx") (v "0.1.5") (d (list (d (n "gfx") (r "~0.12") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "1w96cs52r9m6qfmsy56fj4symgaqkgp984zqz793h7q7r0d6dyhx")))

(define-public crate-nuklear-backend-gfx-0.2.0 (c (n "nuklear-backend-gfx") (v "0.2.0") (d (list (d (n "gfx") (r "~0.14") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "0mh6s8cds5awdzkzd4mx1g48g50gqr8j0x0ci38ib22f3w7p80ld")))

(define-public crate-nuklear-backend-gfx-0.2.1 (c (n "nuklear-backend-gfx") (v "0.2.1") (d (list (d (n "gfx") (r "~0.14") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.1") (d #t) (k 0)))) (h "0nv1hz4ipdgzcb8kjgqdlaz0641z7kxw6hq79bkcwvj4pvx9a0bg")))

(define-public crate-nuklear-backend-gfx-0.2.2 (c (n "nuklear-backend-gfx") (v "0.2.2") (d (list (d (n "gfx") (r "~0.14") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.2") (d #t) (k 0)))) (h "1xirhdz0mrdn8azi80v4yr4k1zr9q2922w0dskr9130fk720xs7p")))

(define-public crate-nuklear-backend-gfx-0.2.3 (c (n "nuklear-backend-gfx") (v "0.2.3") (d (list (d (n "gfx") (r "~0.15") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.2") (d #t) (k 0)))) (h "0s1xiwallc5zabh5ik7b5rww53g8jwx8bh3pbs49lmw4wc4v9v92")))

(define-public crate-nuklear-backend-gfx-0.3.0 (c (n "nuklear-backend-gfx") (v "0.3.0") (d (list (d (n "gfx") (r "~0.15") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.2") (d #t) (k 0)))) (h "1f20w7vr1f1nk8kdn7v4ckpz635azr9flb2jsj00dfdlmyrlfnwr")))

(define-public crate-nuklear-backend-gfx-0.4.0 (c (n "nuklear-backend-gfx") (v "0.4.0") (d (list (d (n "gfx") (r "~0.15") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.3") (d #t) (k 0)))) (h "1qrfja2rbdpz2krwypamc9x9sl5pkn0bgjkylpm283q7spgcbkqj")))

(define-public crate-nuklear-backend-gfx-0.5.0 (c (n "nuklear-backend-gfx") (v "0.5.0") (d (list (d (n "gfx") (r "~0.16") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.3") (d #t) (k 0)))) (h "1sgfqgaq4qvn818v0a11cw33bws5cwpr0s5j7c6vyc70qdx44gpw")))

(define-public crate-nuklear-backend-gfx-0.6.0 (c (n "nuklear-backend-gfx") (v "0.6.0") (d (list (d (n "gfx") (r "~0.16") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.3") (d #t) (k 0)))) (h "0s1biry5sh4mh0a35cny2ka2hc5m51qbqrh4fipa35c4nkq03v43")))

(define-public crate-nuklear-backend-gfx-0.6.1 (c (n "nuklear-backend-gfx") (v "0.6.1") (d (list (d (n "gfx") (r "~0.17") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.3") (d #t) (k 0)))) (h "15pw4lajx0c3154k9xfzlmskkwjrzs7jrkzwln6rs1hbxf725axq")))

(define-public crate-nuklear-backend-gfx-0.7.0 (c (n "nuklear-backend-gfx") (v "0.7.0") (d (list (d (n "gfx") (r "~0.17") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.4") (d #t) (k 0)))) (h "03qk902qnshgxdy1y15l5f13s7p876z3d77xzxcdjs2w1z7525da")))

(define-public crate-nuklear-backend-gfx-0.7.1 (c (n "nuklear-backend-gfx") (v "0.7.1") (d (list (d (n "gfx") (r "~0.17") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.5") (d #t) (k 0)))) (h "028qxzr2nbycrzix8xvydmhajh0f9p2afdicy3nav5jg3qpyhknk")))

(define-public crate-nuklear-backend-gfx-0.8.0 (c (n "nuklear-backend-gfx") (v "0.8.0") (d (list (d (n "gfx") (r "~0.17") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.6") (d #t) (k 0)))) (h "01wirnr2i63pnf68zqwvxgavz69wxhhjff6hyqfxclk8c8nm9q2g")))

(define-public crate-nuklear-backend-gfx-0.9.0 (c (n "nuklear-backend-gfx") (v "0.9.0") (d (list (d (n "gfx") (r "~0.18") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nuklear-rust") (r "~0.6") (d #t) (k 0)))) (h "0rzqb1m81c9mcr68aqf7ppifpbmd4sd23rcnpsfzbvipc4azrmpk")))

