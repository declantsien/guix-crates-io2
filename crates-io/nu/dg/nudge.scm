(define-module (crates-io nu dg nudge) #:use-module (crates-io))

(define-public crate-nudge-0.1.0 (c (n "nudge") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)))) (h "10858qjcdgk0cd2xr682d47y9rf2y5y9l6xv6x7lyx3vd7cqm18m") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2.0 (c (n "nudge") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)))) (h "12mbl2axmz8yrx3nm5fccs3zvhgif4r2n4dxf833sb9kchbv6scs") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2.1 (c (n "nudge") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)))) (h "0ygphx57dscnxsr417k9wz3ga8fxj485znawzf7n6a5bmwz9qdsz") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2.2 (c (n "nudge") (v "0.2.2") (h "0qgq4rbbi8894s5vqidqd85f60sll0licc9iggydxpprppn6h73q") (f (quote (("std") ("nightly") ("default" "std"))))))

