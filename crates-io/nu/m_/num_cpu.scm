(define-module (crates-io nu m_ num_cpu) #:use-module (crates-io))

(define-public crate-num_cpu-1.13.0 (c (n "num_cpu") (v "1.13.0") (h "00j3hw9670zcp0xzmlj7jyd2kba6qykxqcric3awq5xinr5xf1qz") (y #t)))

(define-public crate-num_cpu-1.13.1 (c (n "num_cpu") (v "1.13.1") (h "0fcfll8bfs6j3r5529lr0m239c7rxdsfhahz1xmqdnvmalcxmys9") (y #t)))

(define-public crate-num_cpu-1.13.2 (c (n "num_cpu") (v "1.13.2") (h "1cadgvm3f4z20nhyp32qifhw4zdw0gplj2vf0dmjsl9hf9i62fnb") (y #t)))

(define-public crate-num_cpu-1.14.0 (c (n "num_cpu") (v "1.14.0") (h "1kyp1z16l4cm07b0dbmv5p5ixq8k4c28ajgz5jnfdj3j54fqjmkc")))

