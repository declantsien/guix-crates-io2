(define-module (crates-io nu m_ num_stream) #:use-module (crates-io))

(define-public crate-num_stream-0.1.0 (c (n "num_stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "03m44dxzz3y20dyyizpp9wg3yqwk6yzvb6abci6mm3z1ni80piv6")))

(define-public crate-num_stream-0.1.1 (c (n "num_stream") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1mbzv8b6h6jydw9k4lflv6i9yb3mkyhz70pvryb0r891500a746k")))

(define-public crate-num_stream-0.1.2 (c (n "num_stream") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0y2gnixcrpq7jmr15mc66hhqbka0spbvj6jwplaywkvqnp5nkd89")))

