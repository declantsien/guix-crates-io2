(define-module (crates-io nu m_ num_reader) #:use-module (crates-io))

(define-public crate-num_reader-0.1.0 (c (n "num_reader") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fl6adyrw724wfvfi7g36npmsxp6i2amwn93ipld7l7q0v13a4ik")))

(define-public crate-num_reader-0.1.1 (c (n "num_reader") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0cwb4px36dipgawn7yfsa67n4wcbfw7b79cg32kyymicghdv8plx")))

(define-public crate-num_reader-0.1.2 (c (n "num_reader") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1pgg9pid4a8v90rk2ycc56c5cz7js57wxl8cysap2j9mwsra7f20")))

