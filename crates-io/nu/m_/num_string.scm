(define-module (crates-io nu m_ num_string) #:use-module (crates-io))

(define-public crate-num_string-0.1.0 (c (n "num_string") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "0hllslnczwzbfw2ivg65icg1nrs2q5psfgnry17dxybwnx1fzpij")))

