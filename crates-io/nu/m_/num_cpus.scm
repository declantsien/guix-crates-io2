(define-module (crates-io nu m_ num_cpus) #:use-module (crates-io))

(define-public crate-num_cpus-0.1.0 (c (n "num_cpus") (v "0.1.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0rwkkr3pmyvdm07pd2q5p6s3gabjgc85l7k70nx7zkciwsx8ili2")))

(define-public crate-num_cpus-0.2.0 (c (n "num_cpus") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1y45p6ixbrbz89jknvdaa34z59984qm4bj8799nkp146m5d5m2gi")))

(define-public crate-num_cpus-0.2.1 (c (n "num_cpus") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1cimaa811r6dhsppcd0gqdxgkx4i6mhbw5jnp2smnpakzsmr4051")))

(define-public crate-num_cpus-0.2.2 (c (n "num_cpus") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "19xp0l91n2hrlvpjirmv52zc7wjy7v9mpnnfwln5rlxcnp6r8z02")))

(define-public crate-num_cpus-0.2.3 (c (n "num_cpus") (v "0.2.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fv979gwyyz0lglmrjqr8x0szy4d7mkv7slhvva7c69ls1shppgq")))

(define-public crate-num_cpus-0.2.4 (c (n "num_cpus") (v "0.2.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0v4876qq8z0xc748340r46a3xc9a59hk108mxinj04fld9kcd12l")))

(define-public crate-num_cpus-0.2.5 (c (n "num_cpus") (v "0.2.5") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0l7mf4hd1402hmm0dy6438g1wxc0hlzgkihvxl8spywvrnj8ql8c")))

(define-public crate-num_cpus-0.2.6 (c (n "num_cpus") (v "0.2.6") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0c8f6h6zjm07rd3ylcp8r0hbbli0b1zsfdkddhdiv0ain33a1mxf")))

(define-public crate-num_cpus-0.2.7 (c (n "num_cpus") (v "0.2.7") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "16xrj16fnxlfhrprqwq5v0p86a5gw9cjl17fk9nc2yf6sbj8rnpx")))

(define-public crate-num_cpus-0.2.8 (c (n "num_cpus") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fyvizfqnd6np29slxfqssjsif411dclnxwv470zphf4g2rjvmsp")))

(define-public crate-num_cpus-0.2.9 (c (n "num_cpus") (v "0.2.9") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1vdd4y6vzxdk4n3b4bkrsk2allkgjfcs0rcrp612kv42m4fic03l")))

(define-public crate-num_cpus-0.2.10 (c (n "num_cpus") (v "0.2.10") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0mv559jhc06zc4v1ykyzkq43hhgcxx259mlrw2y956d08k3jnrnq")))

(define-public crate-num_cpus-0.2.11 (c (n "num_cpus") (v "0.2.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02gddq6yga9rwxpmqz6nyjw6szm30nvpl0gy28v3bxq5gblxmzji")))

(define-public crate-num_cpus-0.2.12 (c (n "num_cpus") (v "0.2.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nyyja0avgd52zhig6bzm111jvqri238gabjr2h1fav5pls4z9gz")))

(define-public crate-num_cpus-0.2.13 (c (n "num_cpus") (v "0.2.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qx6s549dc2m7bs830wzz5w41gxpkmn8v3srvndy3ygkas0yiryf")))

(define-public crate-num_cpus-1.0.0 (c (n "num_cpus") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pwxq2g4nc4p3zdqi73ld35lsdpgqsid31sbrqgflw3spwf08nd8")))

(define-public crate-num_cpus-1.1.0 (c (n "num_cpus") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bfwcn3yhwa31rinjw9yr7b6gvn6c06hnwnjz06pvm938w4fd448")))

(define-public crate-num_cpus-1.2.0 (c (n "num_cpus") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wx4iikpcjmm2cq23sd9q6f5ik07bcgymwn0wkja4wb25m7bzajm")))

(define-public crate-num_cpus-1.2.1 (c (n "num_cpus") (v "1.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fcdd3imfhicwhg1h991l4jyiijn13q4k3pq4jf5jrvmf7id29d2")))

(define-public crate-num_cpus-1.3.0 (c (n "num_cpus") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qj4jxbfpmzppyr8wjgbxnd76gn9h0k4lhb9ffw51720cqj3k351")))

(define-public crate-num_cpus-1.4.0 (c (n "num_cpus") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ry1jf7265j2xksh0gff9b4bb70x3flrzklazq6kxv67c8c3ycfa")))

(define-public crate-num_cpus-1.5.0 (c (n "num_cpus") (v "1.5.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "1g27hmbg3bn1vv38yhzl1wchk72bdfgq2kh9wriy4gawyg3m1s7n")))

(define-public crate-num_cpus-1.5.1 (c (n "num_cpus") (v "1.5.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0flqwmgnzlv081zqbqpga8rzf504imm586fbk3rkzfx44yhnnhbf")))

(define-public crate-num_cpus-1.6.0 (c (n "num_cpus") (v "1.6.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "1s9dnzvdlw84dll18ylf5f4917pq68k53bdy6rzzins7zngmdpw3")))

(define-public crate-num_cpus-1.6.1 (c (n "num_cpus") (v "1.6.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0nl8g1dc36syhwkj5cw3hch0426gvzf514l4j3fwdjqscbil68qm") (y #t)))

(define-public crate-num_cpus-1.6.2 (c (n "num_cpus") (v "1.6.2") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "1165l3bhsynbdppfk3j680bk0bvng0aclb6klmf7q96hy8s3ridf")))

(define-public crate-num_cpus-1.7.0 (c (n "num_cpus") (v "1.7.0") (d (list (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "03djaga53swrvhnsxq81va8zpqsgnrqsc33843rkzrabwrrhsksi")))

(define-public crate-num_cpus-1.8.0 (c (n "num_cpus") (v "1.8.0") (d (list (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "0c0am9q3mxwdfyi59h4hah04v4y6mc18l5csml9d5adwwhi366n5")))

(define-public crate-num_cpus-1.9.0 (c (n "num_cpus") (v "1.9.0") (d (list (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "0f4jz85575lp7fj5l0jlr3ln9plfayclwi18yspsl4y2pmjd8sas")))

(define-public crate-num_cpus-1.10.0 (c (n "num_cpus") (v "1.10.0") (d (list (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "1fpvhmikngmg2mwdilbq631c79xdhwnisfz8qyhbljm563nz08qs")))

(define-public crate-num_cpus-1.10.1 (c (n "num_cpus") (v "1.10.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "0wrj3zvj6h3q26sqj9zxpd59frjb54n7jhjwf307clq31ic47vxw")))

(define-public crate-num_cpus-1.11.0 (c (n "num_cpus") (v "1.11.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "1x8sjhsn7zwih6q6hx6ajfjv89nj6awvy9fs960z1pfd4kwr8lqm")))

(define-public crate-num_cpus-1.11.1 (c (n "num_cpus") (v "1.11.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "0wlxs00cpg16z09fwchj1gdz1jxnf5dgg1cbidvq0sc75bnwbnkn")))

(define-public crate-num_cpus-1.12.0 (c (n "num_cpus") (v "1.12.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "1riw641hsmp2vwb9wz7d26dsycrjbw3zf4nd6p18kzw5y1a3a826")))

(define-public crate-num_cpus-1.13.0 (c (n "num_cpus") (v "1.13.0") (d (list (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "1cv6yxhz2zbnwn8pn1yn8grg7zsnd523947fby41a737aqvryj85")))

(define-public crate-num_cpus-1.13.1 (c (n "num_cpus") (v "1.13.1") (d (list (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (t "cfg(not(windows))") (k 0)))) (h "18apx62z4j4lajj2fi6r1i8slr9rs2d0xrbj2ls85qfyxck4brhr")))

(define-public crate-num_cpus-1.14.0 (c (n "num_cpus") (v "1.14.0") (d (list (d (n "hermit-abi") (r "^0.1.3") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1xfyz6qfmcgnlgzk4889hdndp1l2pkry8l5iq8my0wac69j8w1gn")))

(define-public crate-num_cpus-1.15.0 (c (n "num_cpus") (v "1.15.0") (d (list (d (n "hermit-abi") (r "^0.2.6") (d #t) (t "cfg(all(any(target_arch = \"x86_64\", target_arch = \"aarch64\"), target_os = \"hermit\"))") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0fsrjy3arnbcl41vz0gppya8d7d24cpkjgfflr3v8pivl4nrxb0g")))

(define-public crate-num_cpus-1.16.0 (c (n "num_cpus") (v "1.16.0") (d (list (d (n "hermit-abi") (r "^0.3.0") (d #t) (t "cfg(target_os = \"hermit\")") (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0hra6ihpnh06dvfvz9ipscys0xfqa9ca9hzp384d5m02ssvgqqa1")))

