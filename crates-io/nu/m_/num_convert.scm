(define-module (crates-io nu m_ num_convert) #:use-module (crates-io))

(define-public crate-num_convert-0.3.2 (c (n "num_convert") (v "0.3.2") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "19pyb8pr1smmpsd0yhc48kgiczyykk2iifx6ahzrla2y697h5s0j")))

(define-public crate-num_convert-0.4.0 (c (n "num_convert") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "07b5cl953r2w0jl07nv9rxvxy0n6gn0fj54cmsy5hd9ml2vypl5w")))

(define-public crate-num_convert-0.4.1 (c (n "num_convert") (v "0.4.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1mcb802ycisg17jfrw505mjh592jsnfjy686ymmrdypmkx9vg6ja")))

(define-public crate-num_convert-0.4.2 (c (n "num_convert") (v "0.4.2") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0m86vaxfiwldcswm36k1wcnrfxbf2gqx1fg6q9aym4lklkndzvxl")))

(define-public crate-num_convert-0.4.3 (c (n "num_convert") (v "0.4.3") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1x3yw5papydfz1hvq84qqdbdxw3dj1q1hyj86903wzrvc89cdi29") (f (quote (("default" "bounds" "bits") ("bounds") ("bits"))))))

(define-public crate-num_convert-0.4.4 (c (n "num_convert") (v "0.4.4") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0547vrnl6wdgc75v0riy4asa4zxfl3drnljhw9irbcpqqh8bcbb8") (f (quote (("default" "bounds" "bits") ("bounds") ("bits"))))))

(define-public crate-num_convert-0.4.5 (c (n "num_convert") (v "0.4.5") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1x3mhdwl9krckzrh74zhv9gcbm1mn62nnknmv7xr9cxk7qqpkf9x") (f (quote (("default" "bounds" "bits") ("cast-into") ("cast-from") ("bounds") ("bits"))))))

(define-public crate-num_convert-0.4.6 (c (n "num_convert") (v "0.4.6") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "00ri51pmg5rhsz0n7nysngif4wivqpssza08cq77qkbk88n6fknr") (f (quote (("default" "bounds" "bits" "cast-from" "cast-into") ("cast-into") ("cast-from") ("bounds") ("bits"))))))

(define-public crate-num_convert-0.4.7 (c (n "num_convert") (v "0.4.7") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "07ic6vqwlj8ki1z7pr6xpjhc050ycq3bwi9j4j4sbvk9y5lxfvpd") (f (quote (("val-type-info") ("type-info") ("default" "bounds" "bits") ("cast-into") ("cast-from") ("bounds") ("bits"))))))

(define-public crate-num_convert-0.5.0 (c (n "num_convert") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1dgva24c3kcxrmgbcr2v70zg6xfw56n2rrz0bqh3xnmplcac0m0q") (f (quote (("val_type_info") ("type_info") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.5.1 (c (n "num_convert") (v "0.5.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "118xqknfcb1lppk295am011pmggll7blwakyd8pksj9na9jbnsgr") (f (quote (("val_type_info") ("type_info") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.5.2 (c (n "num_convert") (v "0.5.2") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "04v6cs6yd352ql0h2j4v8s4b0mb2k68cizqb6s19qbnvigvlpvi3") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.5.3 (c (n "num_convert") (v "0.5.3") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "058dg4k19hm4plhqv6kpw2hi5a72zg6457d7lxwapfbcbh6jbfpv") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.5.4 (c (n "num_convert") (v "0.5.4") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0agi13cisljl1r2blnjkw207m53n3w1icg3imnmmmc7lkldbxzif") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.5.5 (c (n "num_convert") (v "0.5.5") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1mxrpza2xw760nrnmr90zbqjafxmqz8h8dq86prxx4za9xpkb8ss") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.6.0 (c (n "num_convert") (v "0.6.0") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "try_tup_to_arr_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1cv9jxgrxn13qrm4j9plgkrvp9frqpsjxg868imfybacajgh2sxm") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("try_tup_to_arr8" "try_tup_to_arr_macro/try_tup_to_arr_8") ("try_tup_to_arr16" "try_tup_to_arr_macro/try_tup_to_arr_16") ("try_from_int_str") ("to_zero") ("to_min") ("to_max") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.7.0 (c (n "num_convert") (v "0.7.0") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "try_tup_to_arr_macro") (r "^0.1.0") (d #t) (k 0)))) (h "09aynrsylry8bxhvdd2zb4d8qzgj9kpb2nbv3p0l9y6fi1sf2znm") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("try_tup_to_arr8" "try_tup_to_arr_macro/try_tup_to_arr_8") ("try_tup_to_arr16" "try_tup_to_arr_macro/try_tup_to_arr_16") ("try_from_int_str") ("to_zero") ("to_min") ("to_max") ("get_rem") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.7.1 (c (n "num_convert") (v "0.7.1") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "try_tup_to_arr_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0jsjczrwdfgrdcqjqx8g37y2ixz9iw1h8avhrn9fj72sr4a1k9w7") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("try_tup_to_arr8" "try_tup_to_arr_macro/try_tup_to_arr_8") ("try_tup_to_arr16" "try_tup_to_arr_macro/try_tup_to_arr_16") ("try_from_int_str") ("to_zero") ("to_min") ("to_max") ("get_rem") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

(define-public crate-num_convert-0.7.2 (c (n "num_convert") (v "0.7.2") (d (list (d (n "from_tup_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "try_tup_to_arr_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1zxiz25mygc03i71gcsn7mbbqhjhxxq4s06iksmaxdv917vdqlva") (f (quote (("val_type_info") ("type_info") ("tup8" "from_tup_macro/from_tup_8") ("tup16" "from_tup_macro/from_tup_16") ("try_tup_to_arr8" "try_tup_to_arr_macro/try_tup_to_arr_8") ("try_tup_to_arr16" "try_tup_to_arr_macro/try_tup_to_arr_16") ("try_from_int_str") ("to_zero") ("to_min") ("to_max") ("get_rem") ("default" "to_min" "to_zero" "to_max" "bits") ("cast_into_as") ("cast_from_as") ("bits"))))))

