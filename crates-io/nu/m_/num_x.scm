(define-module (crates-io nu m_ num_x) #:use-module (crates-io))

(define-public crate-num_x-0.2.2 (c (n "num_x") (v "0.2.2") (d (list (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (o #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)))) (h "16ik4s6ih4phjrazf0v15qaxlxc9ha0isl7d4kzdhy07lry55dk1") (f (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("default" "std"))))))

