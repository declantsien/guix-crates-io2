(define-module (crates-io nu m_ num_bound) #:use-module (crates-io))

(define-public crate-num_bound-0.1.0 (c (n "num_bound") (v "0.1.0") (h "0hsmlg1301j7igy5syn33x9jvrprhd5wiqgfnyzwdxfmxrnlch5v")))

(define-public crate-num_bound-0.1.1 (c (n "num_bound") (v "0.1.1") (h "06qq5674la29f1pks6sd3vz9875dp3k2w0d744ngiasyplnxkza6")))

(define-public crate-num_bound-0.1.2 (c (n "num_bound") (v "0.1.2") (h "07gpd52lifffqr0fmr107kybzg6mnd7ly7pjar5dgmbd3zdza2xq")))

(define-public crate-num_bound-1.0.0 (c (n "num_bound") (v "1.0.0") (h "1myn6sgcz7cr4ksbda4lkcihpy7gqnkzbnl61hc9hr786lx82lsd")))

