(define-module (crates-io nu m_ num_base) #:use-module (crates-io))

(define-public crate-num_base-0.1.0 (c (n "num_base") (v "0.1.0") (h "193h39n9af1yv8ixsg297dpv4s4plj93lxsg6g1qvvbzfwv4din5")))

(define-public crate-num_base-0.1.1 (c (n "num_base") (v "0.1.1") (h "0q36c8zdx271876m3gj92w813yk5pwzqnfhi9vvf4vw08p3c6dps")))

(define-public crate-num_base-0.1.2 (c (n "num_base") (v "0.1.2") (h "1v18yrriljkkjr0sb9cgh28dg044gbmxhdjhi27y367s99nfd2zj") (f (quote (("ops"))))))

(define-public crate-num_base-0.2.0 (c (n "num_base") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rbq22chlxqddg54sc9rwwaf3a0hqdfqndcqahcp0h4zrbmb2c6n") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.0 (c (n "num_base") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1d23ps8jxn4y49ph5wi2clqi9bnb968m61a114vjnlqqdm7fwlq2") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.1 (c (n "num_base") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1ps0r7b18k65sgpakfpj7nigv435xy2m2k6shd24fnx8clq4qxlw") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.2 (c (n "num_base") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "10c2sxi7jmdnpg6i2cpyk0hlsral15rs8l4bri52zl1n72ddzczq") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.3 (c (n "num_base") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "0y0asgh86kdwxcpkflc55dqw0xplsxlbn5xg6glv62l0rkqhnb17") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.4 (c (n "num_base") (v "0.3.4") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "07xfvggm8hwa1nz9q9hwp9azdx3fl7qbf6zdr2704lc6hkpvrrq6") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.3.5 (c (n "num_base") (v "0.3.5") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "0lwqwps87b6m77vfagw9lpbmg46vhh30m5b0j4fl8mab94kbfnqv") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.4.0 (c (n "num_base") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hxa9mcrih3wzlys6jb0bj34q0i21vrnwaz72caz3adn17mc67qq") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.4.1 (c (n "num_base") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08sbcngdm5lzfgg429l60j1zdrdrajlxw6nl44yp3qn47vj7jdvd") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-num_base-0.4.2 (c (n "num_base") (v "0.4.2") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wqmxs9jk6w5isvagz8ybhy2086skrfw5j6kmm682adpqyk6a65w") (f (quote (("ops")))) (s 2) (e (quote (("cli" "dep:clap"))))))

