(define-module (crates-io nu m_ num_alias) #:use-module (crates-io))

(define-public crate-num_alias-0.1.0 (c (n "num_alias") (v "0.1.0") (h "03y0vhjcjhndg00kwzdqa0bq2y2p5qw9xcc7xidks9r9006mw72f")))

(define-public crate-num_alias-0.1.1 (c (n "num_alias") (v "0.1.1") (h "1i3wnms9a2p230kp1xfw5ivmy93l2gj4wig11fxs45cb49kinign")))

(define-public crate-num_alias-0.1.2 (c (n "num_alias") (v "0.1.2") (h "1w2v2dxiipn8jbfdj0348vk4aavkm41rqhimrd88rikrwk0zrm5y")))

(define-public crate-num_alias-0.1.3 (c (n "num_alias") (v "0.1.3") (h "1n7ysvzkdks8cnrmxhfkaldggx6nsbrnimq1v81fwas9irgmzrl0")))

(define-public crate-num_alias-0.1.4 (c (n "num_alias") (v "0.1.4") (h "07i6qmwz0lsv6akirw7lylalgjj0gsgl93rr9ix468574l2p5wvs")))

(define-public crate-num_alias-0.1.5 (c (n "num_alias") (v "0.1.5") (h "0igvzcgsgsflx6n552lklihivnr7khcp8pjs5pdzvihv326kbvc9")))

(define-public crate-num_alias-0.1.6 (c (n "num_alias") (v "0.1.6") (h "0xiqdfgd17jvwrg89nkpv5s0h07hq191nvmnmbbaa6vgnm9s0978")))

(define-public crate-num_alias-0.1.7 (c (n "num_alias") (v "0.1.7") (h "1595hn6fgg65x1dwj9vx2g1z08vfv72ism3y8m2jdiml45f835yr")))

