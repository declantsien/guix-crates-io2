(define-module (crates-io nu m_ num_parser) #:use-module (crates-io))

(define-public crate-num_parser-1.0.0 (c (n "num_parser") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuple-conv") (r "^1.0.1") (d #t) (k 0)))) (h "1swv3dqh0d5nxm09y5yhn04bmwd98hmxklv4h9z6jv0sg7r609kn") (f (quote (("serde_support" "serde"))))))

(define-public crate-num_parser-1.0.1 (c (n "num_parser") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuple-conv") (r "^1.0.1") (d #t) (k 0)))) (h "0inscggqvnp6g0j2h518xddw8lh6b075g5v2l2b2qrh754x454a9") (f (quote (("serde_support" "serde"))))))

(define-public crate-num_parser-1.0.2 (c (n "num_parser") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuple-conv") (r "^1.0.1") (d #t) (k 0)))) (h "0l0qnynfgykvjyqvxzrhssz0slnmllq13h6n9qnq0i4vpkn1jfvy") (f (quote (("serde_support" "serde"))))))

