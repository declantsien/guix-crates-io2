(define-module (crates-io nu mb number_prefix) #:use-module (crates-io))

(define-public crate-number_prefix-0.1.0 (c (n "number_prefix") (v "0.1.0") (h "09qcyvm6pmix1m627pbh2pc6as5phis54xylda9wjk10g5q08k2s")))

(define-public crate-number_prefix-0.2.0 (c (n "number_prefix") (v "0.2.0") (h "1vc8ch008zxrh0yccpaya8kg1g58n6yilmgk6gv6y638d8k0d8w5")))

(define-public crate-number_prefix-0.2.1 (c (n "number_prefix") (v "0.2.1") (h "0nkf37yll93sb4m5nafn17g6pf6pl6hy2hc5975b8wgbxbirnq78")))

(define-public crate-number_prefix-0.2.2 (c (n "number_prefix") (v "0.2.2") (h "1rm7v4q5j163vh2b51ns50wrmwx5fhrddzppkysadssg632fyg5v")))

(define-public crate-number_prefix-0.2.3 (c (n "number_prefix") (v "0.2.3") (h "0ljz62w86283ap2hg253siysdfslx1xl4dri8zmn4sdlmjs1p911")))

(define-public crate-number_prefix-0.2.4 (c (n "number_prefix") (v "0.2.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1kqw8s227bk6qnkl0xj56zw6fjf3dlz6lnhpj7v8a6zxpxra3zps")))

(define-public crate-number_prefix-0.2.5 (c (n "number_prefix") (v "0.2.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0xydjx5yyflxxvjhcpslx56qym6z22j43sdxr6d1lqk0pzs0ak88")))

(define-public crate-number_prefix-0.2.6 (c (n "number_prefix") (v "0.2.6") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "18zhsn8vd8pi48i7y528k2ldlb5idz9p0mmrw60ri0ai6wi7r7xh")))

(define-public crate-number_prefix-0.2.7 (c (n "number_prefix") (v "0.2.7") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)))) (h "00j944md0fqf0k9wpf3i9ns896hyyjcsqddd5dh9rjqiqbllp8ar")))

(define-public crate-number_prefix-0.2.8 (c (n "number_prefix") (v "0.2.8") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vjgb8sdz0r17qlfyszwrcgvzrp92hlp3hm1ib0f7568b4z9kyfv")))

(define-public crate-number_prefix-0.3.0 (c (n "number_prefix") (v "0.3.0") (h "0slm4mqmpgs6hvz22ycny9lvyvl9ivs80a1lncslp7lszz02zc0p") (f (quote (("std") ("default" "std"))))))

(define-public crate-number_prefix-0.4.0 (c (n "number_prefix") (v "0.4.0") (h "1wvh13wvlajqxkb1filsfzbrnq0vrmrw298v2j3sy82z1rm282w3") (f (quote (("std") ("default" "std"))))))

