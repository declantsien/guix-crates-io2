(define-module (crates-io nu mb number-based) #:use-module (crates-io))

(define-public crate-number-based-0.1.0 (c (n "number-based") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "10zpf0d78sp7f9mqbcj8q1g304ky15zd3v3c599xqd73nzsgm7s7")))

(define-public crate-number-based-0.1.1 (c (n "number-based") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0vxhsw7jr94v736m74d6yp5wc8h49rqkvpwq6z833pfa8j20wqm1")))

(define-public crate-number-based-0.2.0 (c (n "number-based") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "022g4c07ra8gdhnj0irzh5j6x7lav90dk052gya38bpl8i75bhsl")))

(define-public crate-number-based-0.2.1 (c (n "number-based") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "02pis794aikahxabjk93iibz0wc4lmvsgvz7hk0cfvbnzagwq0zj") (f (quote (("non_default_graphemes"))))))

(define-public crate-number-based-0.2.2 (c (n "number-based") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "015bbd7sd29q6r22q9bwgsc72kddn25rqn8x0halxywny644fyy8") (f (quote (("custom_graphemes"))))))

(define-public crate-number-based-0.2.3 (c (n "number-based") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "01aarh6yx9gy1xmyizcpq67sb9sfy8f5zh81wk7xi6w46ix1a2hk") (f (quote (("custom_graphemes"))))))

