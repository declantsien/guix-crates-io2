(define-module (crates-io nu mb number_easing) #:use-module (crates-io))

(define-public crate-number_easing-0.1.0 (c (n "number_easing") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "1zpappfl9r09b8hj3a4c5qym2m7h82rjs67vx8sz3jvwd81kyylh")))

(define-public crate-number_easing-0.1.1 (c (n "number_easing") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "0qbjga0mlw9hw6s7bn0i9mn0wdmaq9pn87f6prvjdx3ykwajcn1h")))

