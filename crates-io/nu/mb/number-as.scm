(define-module (crates-io nu mb number-as) #:use-module (crates-io))

(define-public crate-number-as-1.0.0 (c (n "number-as") (v "1.0.0") (h "0ka9rz3832abwyvra5p6xfpxdzr75y6la7fbaswi68mvghkgsh9j")))

(define-public crate-number-as-1.0.1 (c (n "number-as") (v "1.0.1") (h "11lvkf3qx9sk7hylsn6kmnzb7apq9h9qgq9c9abbcqybj02cb9lf")))

(define-public crate-number-as-1.0.2 (c (n "number-as") (v "1.0.2") (h "1izn37g67265f1nr3slzchgxym2fys034gb6k83x017bf7xizy0j")))

(define-public crate-number-as-1.0.3 (c (n "number-as") (v "1.0.3") (h "01cr4dqvf2yys02wk5jz0bbq4d1slxac1bhxbzssiknnmml4pqdl")))

(define-public crate-number-as-1.0.4 (c (n "number-as") (v "1.0.4") (h "0dy3g1im4vz1lcc5vzlw3ark8wb8xic4cdy87y5bfdyvmg54g0aa")))

(define-public crate-number-as-1.0.5 (c (n "number-as") (v "1.0.5") (h "1v8haaj8wbp3hnai8jpq2x2kan39044k2hl9rrmjx7zyflcs7mcj")))

(define-public crate-number-as-1.0.6 (c (n "number-as") (v "1.0.6") (h "1lp576kgkjs2j422b5lzgxm15gv306z9qgb5w8fhncr6ks4qg877")))

(define-public crate-number-as-1.0.7 (c (n "number-as") (v "1.0.7") (h "02g4dlh0v2mlib1s0f16f81dih9wrhyj8kzd6m69p665svnlq3p3")))

(define-public crate-number-as-1.0.9 (c (n "number-as") (v "1.0.9") (h "1ncd9jhbja6a60s5davw29alx0mmn80bmq2qfql5xhskh3z0ij48")))

(define-public crate-number-as-1.0.10 (c (n "number-as") (v "1.0.10") (h "0zlslwfwas11fi0f2cya0prvl9xwq86xkr1vzxj6qv13r0xsjlcg")))

