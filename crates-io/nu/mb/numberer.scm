(define-module (crates-io nu mb numberer) #:use-module (crates-io))

(define-public crate-numberer-0.1.0 (c (n "numberer") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0r67z1903jpq1q9p8k5xbd17s9gf29xb7ali098g0ndj7gy2x026")))

(define-public crate-numberer-0.1.1 (c (n "numberer") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1bzkrd2ni8shpra1v1zhci4whzb035rzai0zxnx52l095ayvid28")))

(define-public crate-numberer-0.2.0 (c (n "numberer") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0a96wj1fdp72ghsa46x3l3i5p3kdv0pa76k7jms0djnq45p43qy8")))

(define-public crate-numberer-0.2.1 (c (n "numberer") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "10m28sgg58svvs841xcvbcxi0h9b6sfark5qk2cfdl6jp39s09ji")))

