(define-module (crates-io nu mb number2name) #:use-module (crates-io))

(define-public crate-number2name-1.0.0 (c (n "number2name") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12xzhw4mn20zjplzx6950p67yqk6q4x3i8409mn9892h80ql4cq2")))

(define-public crate-number2name-1.0.1 (c (n "number2name") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13f6f3m126xfi478sv16s9rq2dwf288hlxs476hz408nldls6pk1")))

(define-public crate-number2name-1.1.0 (c (n "number2name") (v "1.1.0") (d (list (d (n "criterion") (r ">=0.3.3, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (o #t) (d #t) (k 0)))) (h "0qbvwm0478dyg70s89ncqjw7fqsdm4qqh4n6k8s6jhrskxscsrq6") (f (quote (("default" "builtin-charsets" "cli") ("cli" "structopt") ("builtin-charsets" "lazy_static"))))))

(define-public crate-number2name-1.1.1 (c (n "number2name") (v "1.1.1") (d (list (d (n "criterion") (r ">=0.3.3, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (o #t) (d #t) (k 0)))) (h "0b0wdd7pqn27b48lhrfjy4wla66nixnhgjz025ybmzw8fs62wk0m") (f (quote (("default" "builtin-charsets" "cli") ("cli" "structopt") ("builtin-charsets" "lazy_static"))))))

(define-public crate-number2name-1.1.2 (c (n "number2name") (v "1.1.2") (d (list (d (n "criterion") (r ">=0.3.3, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (o #t) (d #t) (k 0)))) (h "09hsm2ri0ilc4rn6lsnhd7093qriycy7pm751lad06hgbsaldbkw") (f (quote (("default" "builtin-charsets" "cli") ("cli" "structopt") ("builtin-charsets" "lazy_static"))))))

