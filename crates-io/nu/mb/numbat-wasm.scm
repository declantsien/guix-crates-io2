(define-module (crates-io nu mb numbat-wasm) #:use-module (crates-io))

(define-public crate-numbat-wasm-0.0.0 (c (n "numbat-wasm") (v "0.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "numbat-codec") (r "^0.0.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "14my7dxhhl5sp5ma6r598pgvhvdjmccqj1rr47xphqwmg27ys32b")))

