(define-module (crates-io nu mb numb_rs) #:use-module (crates-io))

(define-public crate-numb_rs-0.0.0 (c (n "numb_rs") (v "0.0.0") (h "0h99i9vs6dii1xmq025yf782ap3fdmcg3k561n5dvvfi8nkcaxms")))

(define-public crate-numb_rs-0.0.1 (c (n "numb_rs") (v "0.0.1") (h "1qv2zx7mkkdd8mnp9hf9i63xqa5p3csxm5k5rx4i530ays73vp08")))

(define-public crate-numb_rs-0.0.2 (c (n "numb_rs") (v "0.0.2") (h "08jywiakj247qz0ds60y7xwr7sa580l4kr56cyadqrg4rziqzw8l")))

(define-public crate-numb_rs-0.0.3 (c (n "numb_rs") (v "0.0.3") (h "0bb13kb6g82rbxna13b72wjwfr20i6qyb9bxazhr7sziyvdh9ik5")))

(define-public crate-numb_rs-0.0.4 (c (n "numb_rs") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "1klh7k13ylplrzpiyg0gs3mn4k2mgcn85lsgm5w6yi9rzr7bmxfc")))

