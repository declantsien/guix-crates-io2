(define-module (crates-io nu mb number_to_words) #:use-module (crates-io))

(define-public crate-number_to_words-0.1.0 (c (n "number_to_words") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "12rzsbwg2q4xhgwh229h441ll5jfb3w4i6a4r3qlvlv9rha65z9h")))

(define-public crate-number_to_words-0.1.1 (c (n "number_to_words") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "18iai3p1mgdygmbdijfyschmdcgd4j6zh35jcb7rs81xf6zx2mdh")))

