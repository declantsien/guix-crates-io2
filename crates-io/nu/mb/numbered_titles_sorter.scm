(define-module (crates-io nu mb numbered_titles_sorter) #:use-module (crates-io))

(define-public crate-numbered_titles_sorter-0.1.0 (c (n "numbered_titles_sorter") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qgwdz62a9swsk1jq78xwdr3s4yi1c2h1kxvkfl23gnlkp7924ap")))

(define-public crate-numbered_titles_sorter-0.1.1 (c (n "numbered_titles_sorter") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01kwv83yqkn0l4lliyshi26zaybp3ck1wazp16jv0sdpms4935y7")))

(define-public crate-numbered_titles_sorter-0.1.2 (c (n "numbered_titles_sorter") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wbbn33z8sz0gk5ylfa0b6v67r6gq3idplkhb5fv3kpr5qyjhhf1")))

