(define-module (crates-io nu mb number-utils) #:use-module (crates-io))

(define-public crate-number-utils-0.1.0 (c (n "number-utils") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "num-iter") (r "^0.1.43") (o #t) (d #t) (k 0)))) (h "121filp8ns4xmhf82ip2328hf68s3zvsia7jm175lwymsqbyas6i") (f (quote (("default" "num-bigint")))) (s 2) (e (quote (("num-bigint" "dep:num-bigint" "dep:num-iter"))))))

