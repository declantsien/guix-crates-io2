(define-module (crates-io nu mb number-theory) #:use-module (crates-io))

(define-public crate-number-theory-0.0.0 (c (n "number-theory") (v "0.0.0") (h "12fgc7sl0aqka97fhrrzw3kz8xw0gja7pqshrvwh2pdfj3hqskjv")))

(define-public crate-number-theory-0.0.1 (c (n "number-theory") (v "0.0.1") (h "1fgpmgmgb3z6hnzy0yca4dzrl26lcpm9mjyyjm7954fzdfapbmpp")))

(define-public crate-number-theory-0.0.2 (c (n "number-theory") (v "0.0.2") (h "1dxs5d0ni9n9wssq9lk69xyiia21mx1bvz32a5qg8m9b8yg8znms")))

(define-public crate-number-theory-0.0.3 (c (n "number-theory") (v "0.0.3") (h "0x7riab3q2rc0zjrb5z849wldr652c2zy8ci88gxkc0d3l3n387s")))

(define-public crate-number-theory-0.0.4 (c (n "number-theory") (v "0.0.4") (h "0gzzmi244s76xhpns7c220mph9wa59aa7b6kvwcmwm12mvsnw7h1")))

(define-public crate-number-theory-0.0.5 (c (n "number-theory") (v "0.0.5") (h "1md54rgp4200iim82n17r29xpjslsl57v8g9c8aszjf4828abs7j")))

(define-public crate-number-theory-0.0.6 (c (n "number-theory") (v "0.0.6") (h "0r9f91kpi0l57q60bi6zz83zwdg5204dw598fsx064jmys8lzg2p")))

(define-public crate-number-theory-0.0.7 (c (n "number-theory") (v "0.0.7") (h "1z2sg6pvysdrdnyy70xzg3k8npsasxikaz4jw2z13nw7kbrjrpjm") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.8 (c (n "number-theory") (v "0.0.8") (h "0qnykggy29i36w2l2pp4abgmwqdcyn5cp7vrzbd6hv4px4dkqmsk") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.9 (c (n "number-theory") (v "0.0.9") (h "09hjr59s1vz2b7iy34k1r1kv5f5cmvr71l5fzycgv37m0xvf5vvq") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.10 (c (n "number-theory") (v "0.0.10") (h "08sfiry8zaf7l5g5jbnmsrjg80pxxv57sbwa08av5z3yn9hh973l") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.11 (c (n "number-theory") (v "0.0.11") (h "1638w6h5k46aylpncys88ankmplczamxlkz3cfl8qsqzvs2z9jqj") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.12 (c (n "number-theory") (v "0.0.12") (h "12qn5gvnpj027vfh250jjcc2ds3rab05zwnrl04qs27chqjwq8dn") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.13 (c (n "number-theory") (v "0.0.13") (h "18ldr1wazr6n8h5m8mibzcqm8iykwqgfzb4fypnxnh0y2asscpn3") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.14 (c (n "number-theory") (v "0.0.14") (h "18hn35br3rxv6ivrm7lgrmsysmdg0vlq9k0q9vl7rw1njmk4qd67") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.15 (c (n "number-theory") (v "0.0.15") (h "1s3yflzb1rsr7m4vmk50ajfqlh88qpcg394920l10js6nkbicfn1") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.16 (c (n "number-theory") (v "0.0.16") (h "0bs4g0yg3xga0zdydcsymg34dgm4lxrfp0nhc8g6k3wcw4z3gqyq") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.17 (c (n "number-theory") (v "0.0.17") (h "17ja9084721qbdn4yfwrknhn3hld3231g64nxxpw47j36fi0ij1s") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.18 (c (n "number-theory") (v "0.0.18") (h "0mplh1ygrm3s6s73pzrs7lbpav2p4p1nix870cq0al0qrwinl2jy") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.19 (c (n "number-theory") (v "0.0.19") (h "0npk98fgry3h3kw0w9j6gic1l02mjlxszg5cdxy937a3f794i0m2") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.20 (c (n "number-theory") (v "0.0.20") (h "1kw1c9sag17m3gzfrq08wjlng24p78k18dyidqfvc58p0n6zsshm") (f (quote (("parallel")))) (y #t)))

(define-public crate-number-theory-0.0.21 (c (n "number-theory") (v "0.0.21") (h "09sffwxjn2gchazcpz21j8q8zdb04msj68mz1jp5inxa47dlciax") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.22 (c (n "number-theory") (v "0.0.22") (h "1j7rnap7pj45frny6gdrjz9knz8sbrl0as8krkfn14xb7yazrppy") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.23 (c (n "number-theory") (v "0.0.23") (h "1cw25341d503xya8x1dybfvkvgsc8mssphp8fzhv8bk66zjhx85l") (f (quote (("parallel"))))))

(define-public crate-number-theory-0.0.24 (c (n "number-theory") (v "0.0.24") (d (list (d (n "machine-prime") (r "^1.2.2") (d #t) (k 0)))) (h "0xj7pww9pm47pg70snh79qjps1xzq3w7s30iid0h2yxxmk1xx1b3") (f (quote (("parallel"))))))

