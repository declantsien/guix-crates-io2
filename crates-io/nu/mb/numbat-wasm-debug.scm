(define-module (crates-io nu mb numbat-wasm-debug) #:use-module (crates-io))

(define-public crate-numbat-wasm-debug-0.0.0 (c (n "numbat-wasm-debug") (v "0.0.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "numbat-wasm") (r "^0.0.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "068za0vnbqq03lqw2v1l13xscnvah6r1rv4hxhj541r2jxzfi0g0")))

