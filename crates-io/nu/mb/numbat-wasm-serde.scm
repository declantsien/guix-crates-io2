(define-module (crates-io nu mb numbat-wasm-serde) #:use-module (crates-io))

(define-public crate-numbat-wasm-serde-0.0.0 (c (n "numbat-wasm-serde") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "06gpnk986j26cw19iyrv9yzsqlbi5yqkq9lnbpkbnvqvy01mlgl1")))

