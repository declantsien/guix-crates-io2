(define-module (crates-io nu mb number_traits) #:use-module (crates-io))

(define-public crate-number_traits-0.1.0 (c (n "number_traits") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0i8fva0qz2ipi8w6kx9ax7s9c1w9242kplxsvggbh1mw1i586ndf")))

(define-public crate-number_traits-0.1.1 (c (n "number_traits") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0q2pj0aakmggkc6rylalimllj06vkb04fvdkk2nn475nqdmjkw4z")))

(define-public crate-number_traits-0.1.2 (c (n "number_traits") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "18251h2617dncamphc953bb5nb8xh4kgannjrzcpcgqcrgqyp2ss")))

(define-public crate-number_traits-0.1.3 (c (n "number_traits") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1k61nfwlwvly4ksr5hkjj03ydhh88piwhxqhzdcjlczd2gici78n")))

(define-public crate-number_traits-0.1.4 (c (n "number_traits") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "07afbr0janwcx6i0x3j0j70kg642nhiy98v1ydhd93mgg3wxr9mj")))

(define-public crate-number_traits-0.1.5 (c (n "number_traits") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1wb8ldbdylf1bw00flcxw037zzb4f71pwa3z9xpgkka2z1abvh72")))

(define-public crate-number_traits-0.1.6 (c (n "number_traits") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0czb80rw8fpqfnw42knrg9w31pqyjj972qx73xgbd37zk0w0hvjn")))

(define-public crate-number_traits-0.2.0 (c (n "number_traits") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "11jf63g1znmi4krry7l4rxb4qhiwa4l60ir0qr43p6ipin5w13lf")))

(define-public crate-number_traits-0.2.1 (c (n "number_traits") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "070vghadp0a9amil89iijndb2hcsiw1mz1jpdh75mg87g1rm7rb6")))

(define-public crate-number_traits-0.2.2 (c (n "number_traits") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0s881jj8rz7kg519ypvgp31jwc5dlrysr7d6scnny0d6gp0hxrbp")))

(define-public crate-number_traits-0.2.3 (c (n "number_traits") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0q7cac63rklqmh2wwdv92nckiv6msl0hz8xaaxxp7nbllwwlvkr1")))

(define-public crate-number_traits-0.2.4 (c (n "number_traits") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0xq981vdffqrnnzfsbdbwpdkiy1fg6jrzq7qspbx0ipnhsxmvyc3")))

