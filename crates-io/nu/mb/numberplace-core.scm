(define-module (crates-io nu mb numberplace-core) #:use-module (crates-io))

(define-public crate-numberplace-core-0.1.0 (c (n "numberplace-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "slice-group-by") (r "^0.2.6") (d #t) (k 0)))) (h "1qd1ipqkkvazwvv29kg7h2pycmpf69i2b1sn886i9cic1nqfys04")))

(define-public crate-numberplace-core-0.1.1-1 (c (n "numberplace-core") (v "0.1.1-1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "slice-group-by") (r "^0.2.6") (d #t) (k 0)))) (h "1plgp0b88m1bdp9nkvr2nn0wwqprsskbc82siabf70d8jrvq4g8c")))

