(define-module (crates-io nu mb number_words) #:use-module (crates-io))

(define-public crate-number_words-0.0.1 (c (n "number_words") (v "0.0.1") (h "1cmgg4iagvynjqwad85lbnq32vvnyriqqpwh1r7n819dayrvhm4d")))

(define-public crate-number_words-0.0.2 (c (n "number_words") (v "0.0.2") (h "15gaw8r92d91vywgvny65rr7jf3d774bw8mlk5yji5pw2cvljfnd")))

(define-public crate-number_words-0.0.3 (c (n "number_words") (v "0.0.3") (h "094gcg9py51ic49n9zkk5vhlf43wzgjspj777dksp4llvlp0khwp")))

