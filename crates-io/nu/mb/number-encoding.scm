(define-module (crates-io nu mb number-encoding) #:use-module (crates-io))

(define-public crate-number-encoding-0.1.0 (c (n "number-encoding") (v "0.1.0") (h "0mh8zaj9p7lwrqcngp1nr7i227mf1m25yqc3nwp9rcwdjvx469sn")))

(define-public crate-number-encoding-0.2.0 (c (n "number-encoding") (v "0.2.0") (h "1x1irlfhw2p97f2ncm47fql5pbj8qnzmr8zw8sradx3mgncli094") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-number-encoding-0.2.1 (c (n "number-encoding") (v "0.2.1") (h "1vxl7052yiv59xnkbn8p67aj6cszlydgl0csv7mb33s4q5x81n1i") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

