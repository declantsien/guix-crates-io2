(define-module (crates-io nu mb numbat-codec) #:use-module (crates-io))

(define-public crate-numbat-codec-0.0.0 (c (n "numbat-codec") (v "0.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1vlk53nqi3svqiqyvcfll2yz3drz8f67lcaavrkns7s5gyp4qyz4")))

