(define-module (crates-io nu mb number-names) #:use-module (crates-io))

(define-public crate-number-names-0.1.1 (c (n "number-names") (v "0.1.1") (h "16ahnl4l6q66lnclqaijdkxrp9zv2yrl9wiknwwkmvybdn20yygp")))

(define-public crate-number-names-0.1.2 (c (n "number-names") (v "0.1.2") (h "0ixzlnns7bj1m6f9wimzxxwd3c185yia0421ihjhd8hpp53mn044")))

(define-public crate-number-names-0.2.0 (c (n "number-names") (v "0.2.0") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "1mw6s01a71vhx0199axqpvmlx1mf1wrxfkl84y8zvgjjrr534545")))

