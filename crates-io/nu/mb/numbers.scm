(define-module (crates-io nu mb numbers) #:use-module (crates-io))

(define-public crate-numbers-0.1.0 (c (n "numbers") (v "0.1.0") (d (list (d (n "chinese-numbers") (r "^1.0") (d #t) (k 0)) (d (n "english-numbers") (r "^0.3") (d #t) (k 0)) (d (n "french-numbers") (r "^0.1") (d #t) (k 0)))) (h "1hyg9qn2g567dvmdgi56fknj0q5vgp8n9f8g8vx8bl30x98n6z1p")))

(define-public crate-numbers-0.1.1 (c (n "numbers") (v "0.1.1") (d (list (d (n "chinese-numbers") (r "^1.0") (d #t) (k 0)) (d (n "english-numbers") (r "^0.3") (d #t) (k 0)) (d (n "french-numbers") (r "^0.1") (d #t) (k 0)))) (h "19mc6vy7pwds7xpjlaa9aa9p9s5xysdccg22h5n0jz6k0jqfrb5v")))

(define-public crate-numbers-0.1.2 (c (n "numbers") (v "0.1.2") (d (list (d (n "chinese-numbers") (r "^1.0") (d #t) (k 0)) (d (n "english-numbers") (r "^0.3") (d #t) (k 0)) (d (n "french-numbers") (r "^0.1") (d #t) (k 0)) (d (n "isolang") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hif1i6mz1lkiz54l2hjb7azc7rz4m4qzhzr6b816vvh2hrnhdlk")))

