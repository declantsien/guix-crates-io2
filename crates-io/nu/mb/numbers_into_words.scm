(define-module (crates-io nu mb numbers_into_words) #:use-module (crates-io))

(define-public crate-numbers_into_words-0.1.0 (c (n "numbers_into_words") (v "0.1.0") (h "0ssbbj6fy347kbhg9v636qi477appa7zq8rab52fr1m2dqp4l0q7")))

(define-public crate-numbers_into_words-0.1.1 (c (n "numbers_into_words") (v "0.1.1") (h "11q1wv2nfcj4nb6v1khy2hljpbpkj6js7zjnmzizpdj9nlwvsngp")))

(define-public crate-numbers_into_words-0.1.2 (c (n "numbers_into_words") (v "0.1.2") (h "1hlhhrmbw6n1vxcy0lbparkd3sz9q1s8zd9jvc9qzi440z5192zn")))

