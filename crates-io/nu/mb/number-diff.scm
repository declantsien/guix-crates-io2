(define-module (crates-io nu mb number-diff) #:use-module (crates-io))

(define-public crate-number-diff-0.1.0 (c (n "number-diff") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1nv0q828hbkxbsnqrvf4rlsbwr2i0cbll8nhyrmvqw17xfrs0ss0") (f (quote (("nightly") ("default"))))))

(define-public crate-number-diff-0.1.1 (c (n "number-diff") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14n43k1pls4kj6as8v89zbwhlpma8pjzibfr1sm3ajmyidj7m3c8") (f (quote (("nightly") ("default")))) (s 2) (e (quote (("serialize" "dep:serde"))))))

(define-public crate-number-diff-0.1.2 (c (n "number-diff") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s5jpm8za6zjv6kya723nbwj5afy09f7ijxnl5im6bphq5bl82xw") (f (quote (("nightly") ("default")))) (y #t) (s 2) (e (quote (("serialize" "dep:serde"))))))

(define-public crate-number-diff-0.1.3 (c (n "number-diff") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nywcyg30cph9spq5kqwjrn6vjscn8flb6hvrj25yc2957njzny9") (f (quote (("nightly") ("default")))) (s 2) (e (quote (("serialize" "dep:serde"))))))

(define-public crate-number-diff-0.1.4 (c (n "number-diff") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bpw1bbj0d9gf8nxspzpp1nqp6190qn792w83aw4xks7s3vz86p6") (f (quote (("nightly") ("default")))) (s 2) (e (quote (("serialize" "dep:serde"))))))

