(define-module (crates-io nu mb numbers_rus) #:use-module (crates-io))

(define-public crate-numbers_rus-0.1.0 (c (n "numbers_rus") (v "0.1.0") (h "1mc2w2c6vvw3r8asas2v4m2jlkkwr2253zva12rkr0jr6a6rz9k2")))

(define-public crate-numbers_rus-0.1.1 (c (n "numbers_rus") (v "0.1.1") (h "1zphsrsab99c1akhggy77yxqx3s8d823c47q27bcir9yy5hh8lfz")))

(define-public crate-numbers_rus-0.1.2 (c (n "numbers_rus") (v "0.1.2") (h "0jjak8z6f9kqqjy4snhcv4mhpkam6szq8gqakgkkcs29f36zz1zs")))

(define-public crate-numbers_rus-0.1.3 (c (n "numbers_rus") (v "0.1.3") (h "06spqwp46ky8pbbviyyqp5w54pkvfdxdxm2w3bpdg7d735ns1pvk")))

(define-public crate-numbers_rus-0.1.4 (c (n "numbers_rus") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)))) (h "0ji282ljhbc7l3rq1qiikvscig01q1xab465csfyd6acd8vlzrx4")))

(define-public crate-numbers_rus-0.1.5 (c (n "numbers_rus") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)))) (h "0c1c6lmzdzl71cs8fca38g4vdvgkxbs22ca7c0jvh6k38m4cfzwc")))

(define-public crate-numbers_rus-0.1.6 (c (n "numbers_rus") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)))) (h "0wqcq4zyps1mjsabfmrm2610gy3a2ax39n1388zrncnxnjrb0ibv")))

(define-public crate-numbers_rus-0.1.7 (c (n "numbers_rus") (v "0.1.7") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)))) (h "1ld7f82pgm6f2jcaphwr7dvgznrrdspfg7whydh0i16954hydprp")))

(define-public crate-numbers_rus-0.1.8 (c (n "numbers_rus") (v "0.1.8") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)))) (h "113gzmz1h089f7brdgqhxrzclzrlxrx4s2mgb4nqw8cq95wnyzqi")))

(define-public crate-numbers_rus-0.1.9 (c (n "numbers_rus") (v "0.1.9") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vw0b6f471fnd3cawh31anf8lrm9j0qp05z74krp92sksbn6j079")))

(define-public crate-numbers_rus-0.2.0 (c (n "numbers_rus") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1nsx6apw1iyn6y276yhjx4qcgy3qs8w217ip3fmdqgib74d7rrck")))

(define-public crate-numbers_rus-0.2.1 (c (n "numbers_rus") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0pd0lmhcyq9q6ygzbvf9hwv7zmn80zcmvvrzlzm1p8na0lsh5ljk")))

