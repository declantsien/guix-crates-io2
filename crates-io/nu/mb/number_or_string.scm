(define-module (crates-io nu mb number_or_string) #:use-module (crates-io))

(define-public crate-number_or_string-0.1.0 (c (n "number_or_string") (v "0.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0camh8dm7ra3ihznaqazz5x0i45nmlk2206a9fmfd230pdzyyh52") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-number_or_string-0.1.1 (c (n "number_or_string") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0pl1al7394d99x1pvvlghn8k8maypg1w0q407p6bz8bz3bddnhax")))

(define-public crate-number_or_string-0.2.0 (c (n "number_or_string") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0jny2l2w19jq7wp5mhayxacdvgawxxqg0k2mlkma2jc3d6rkd7dp")))

