(define-module (crates-io nu mb numbat-exchange-rates) #:use-module (crates-io))

(define-public crate-numbat-exchange-rates-0.1.0 (c (n "numbat-exchange-rates") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.29.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1q9si73gp6n053v514ka26a00hk19xz73g8h44qhzd5ygj5wrys4")))

(define-public crate-numbat-exchange-rates-0.2.0 (c (n "numbat-exchange-rates") (v "0.2.0") (d (list (d (n "attohttpc") (r "^0.26.0") (f (quote ("tls-rustls-webpki-roots"))) (k 0)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "1w3d6wb8dby9vkwwfnjmgx77qasyn301118my0abp9d2d4cmi9ax") (r "1.70")))

(define-public crate-numbat-exchange-rates-0.3.0 (c (n "numbat-exchange-rates") (v "0.3.0") (d (list (d (n "attohttpc") (r "^0.26.0") (f (quote ("tls-rustls-webpki-roots"))) (o #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "0hzp2sws6qkf1m6ky3dw4n9law1kn68dd58ph5awmg2a9zqgzkar") (f (quote (("default" "attohttpc")))) (r "1.70")))

(define-public crate-numbat-exchange-rates-0.4.0 (c (n "numbat-exchange-rates") (v "0.4.0") (d (list (d (n "attohttpc") (r "^0.26.0") (f (quote ("tls-rustls-webpki-roots"))) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)))) (h "02rlls0pfjda96dspa5xyy1byb4hnjx76blqmahbl8f2a02b1ij5") (r "1.70")))

(define-public crate-numbat-exchange-rates-0.5.0 (c (n "numbat-exchange-rates") (v "0.5.0") (d (list (d (n "attohttpc") (r "^0.27.0") (f (quote ("tls-rustls-webpki-roots"))) (o #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)))) (h "063pvwjirkgdvsars0m90aihcrag340kyhfbrpbx08lz9wz3q7px") (s 2) (e (quote (("fetch-exchangerates" "dep:attohttpc")))) (r "1.70")))

