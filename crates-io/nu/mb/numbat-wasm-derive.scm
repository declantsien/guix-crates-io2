(define-module (crates-io nu mb numbat-wasm-derive) #:use-module (crates-io))

(define-public crate-numbat-wasm-derive-0.0.0 (c (n "numbat-wasm-derive") (v "0.0.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m08r5mzrvaq7kz9ai0s0bjnwnihn1rsbxx5wmf51cx1axkdypf9") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

