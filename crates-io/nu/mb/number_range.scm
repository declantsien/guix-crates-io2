(define-module (crates-io nu mb number_range) #:use-module (crates-io))

(define-public crate-number_range-0.1.0 (c (n "number_range") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1w8hrs00x1wc5lflzpad8qp14vlhxxb9k8d20sn578cdnk115frj")))

(define-public crate-number_range-0.1.1 (c (n "number_range") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1ys3lnkm3z6bxjfl7ckg8d32m9s2m4p3cn9zdpzx74q6l4a898kq")))

(define-public crate-number_range-0.1.2 (c (n "number_range") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "03bnjw3lq4ccxsx1ci03lmbnwzsv8x8rr495kv76z1gn1s4wnrn2")))

(define-public crate-number_range-0.1.3 (c (n "number_range") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "10l19615nhi71db36kshain3n32nawhbcaqdwkj6sjlhxgpf1j8r")))

(define-public crate-number_range-0.1.4 (c (n "number_range") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1yr5l72dym3na72j57wd889nmcn6bvhmzd1lkhyavy6fbjkaamzq")))

(define-public crate-number_range-0.2.0 (c (n "number_range") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0lfxsjy2a92w8p1bpw2lrhg82ix9fc1wrxq3dg8w8pq4pjgsv9p8")))

(define-public crate-number_range-0.3.0 (c (n "number_range") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0g2l4y7vi6y82hsgwh4ssvn841mm9lwqp4z9b4brfmjbzpzgm1rf")))

(define-public crate-number_range-0.3.1 (c (n "number_range") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0grx10calq1yn6g3ha0abyi04b8zhpmvd3yl2dyl043j8w8mxq89")))

(define-public crate-number_range-0.3.2 (c (n "number_range") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1fz1n9rhkr0p934fmxliyq9nndrnc5lbxch1p380m9acrnn0y230")))

