(define-module (crates-io nu _p nu_plugin_hcl) #:use-module (crates-io))

(define-public crate-nu_plugin_hcl-0.1.0 (c (n "nu_plugin_hcl") (v "0.1.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.79.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.79.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0m3jbhp7hddw5alb27ihrmf1p1aadz5cfyg060n74313b0vrfvan")))

(define-public crate-nu_plugin_hcl-0.80.0 (c (n "nu_plugin_hcl") (v "0.80.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.80.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.80.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0cw3c6jiddnnd3fn4xz8wws7gs3awi3ghd4cxkcpwk4sysyhzw5w")))

(define-public crate-nu_plugin_hcl-0.81.0 (c (n "nu_plugin_hcl") (v "0.81.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "054bav0h2lcbwhyknq395lpyrpbrklp1c9rihfz5yicy8577zvhq")))

(define-public crate-nu_plugin_hcl-0.82.0 (c (n "nu_plugin_hcl") (v "0.82.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.82.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.82.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1xfjyv564kxvalbhs4ypqqk0qgfzxl816a7pgzy198iyqf0sgjpz")))

(define-public crate-nu_plugin_hcl-0.83.0 (c (n "nu_plugin_hcl") (v "0.83.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0g016zkwbncr5552dxmfygk28mxar5hbjp7n569bqkghr9bcr4i5")))

(define-public crate-nu_plugin_hcl-0.87.1 (c (n "nu_plugin_hcl") (v "0.87.1") (d (list (d (n "hcl-rs") (r "^0.16.6") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1jx4p9ybbsbar5kgh175ivqy4s1fn9fjs9yhfjf1nsl1vi3nywgm")))

(define-public crate-nu_plugin_hcl-0.90.1 (c (n "nu_plugin_hcl") (v "0.90.1") (d (list (d (n "hcl-rs") (r "^0.16.6") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "002daw4cwnmmq8mdndah6iqkjr0sxk4lvcbplcs1bk6wnh2b7dp1")))

