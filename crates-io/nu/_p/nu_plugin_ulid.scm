(define-module (crates-io nu _p nu_plugin_ulid) #:use-module (crates-io))

(define-public crate-nu_plugin_ulid-0.1.0 (c (n "nu_plugin_ulid") (v "0.1.0") (d (list (d (n "nu-plugin") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "ulid") (r "^1.1.2") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "0kcr9dzx2p6gdbcj79hmyq7myqg48sai6i0n844nyknh42qn3d40")))

(define-public crate-nu_plugin_ulid-0.2.0 (c (n "nu_plugin_ulid") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "ulid") (r "^1.1.2") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "0z4qw4dj734k2g145gz5r3ybq646azhpf11fxfinxh564i7i070y")))

(define-public crate-nu_plugin_ulid-0.3.0 (c (n "nu_plugin_ulid") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "ulid") (r "^1.1.2") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "06k5y21f3c1r38m7hlylk3qj81wbkijyxqa0qxrd8ca09l0p9jc9")))

(define-public crate-nu_plugin_ulid-0.4.0 (c (n "nu_plugin_ulid") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "ulid") (r "^1.1.2") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "17k7q5vggvvg8nsabsvf6p3v5x9q58bl3y8cx2pii59pw6bbpxnc")))

