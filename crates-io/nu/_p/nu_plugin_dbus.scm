(define-module (crates-io nu _p nu_plugin_dbus) #:use-module (crates-io))

(define-public crate-nu_plugin_dbus-0.1.0 (c (n "nu_plugin_dbus") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0qa9sza7ggp4a21ixrs2vvilwx6lhn4mf0wv0srzf0qb3pbzsfmg")))

(define-public crate-nu_plugin_dbus-0.1.1 (c (n "nu_plugin_dbus") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "01lxx3cvdfqil7r7bfm2rg4h0crixf3994d7wxjb460zn5jss855")))

(define-public crate-nu_plugin_dbus-0.2.0 (c (n "nu_plugin_dbus") (v "0.2.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "10imwabxrp0h76g8ynq16hs9wkhpz7g91jhziiz1zkpmpf0x62zy") (y #t)))

(define-public crate-nu_plugin_dbus-0.2.1 (c (n "nu_plugin_dbus") (v "0.2.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1d7nmdid9dfs01yv72vvx6z6xv7l8c39badjy0g45az30iisk7kw")))

(define-public crate-nu_plugin_dbus-0.2.2 (c (n "nu_plugin_dbus") (v "0.2.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1xhairb9m6h9f006116kp6mg383wybl7j6nqbzxh7sxdlm9qihi7")))

(define-public crate-nu_plugin_dbus-0.3.0 (c (n "nu_plugin_dbus") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0wn37pvjrncgkwlkkkly2djwv1794d5499bgd35vgzvh4r6y7xz8")))

(define-public crate-nu_plugin_dbus-0.4.0 (c (n "nu_plugin_dbus") (v "0.4.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "103mmfhnmws8cpsrs3bzqb18w0ap8j9fwzvrxd9q0pflgijy7cnq")))

(define-public crate-nu_plugin_dbus-0.5.0 (c (n "nu_plugin_dbus") (v "0.5.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1qr9s36sjxicrsk7ls9dzd1fqcjnfa6a38r4xqhl729vs0xkf3gp")))

(define-public crate-nu_plugin_dbus-0.6.0 (c (n "nu_plugin_dbus") (v "0.6.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0phyiwl33rph6c2bj3d9j6vk14mv6irn36h14bjrpqsjsh9lhpym")))

(define-public crate-nu_plugin_dbus-0.6.1 (c (n "nu_plugin_dbus") (v "0.6.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0838mlkm612y6bfyxkwpv7ijijm76gn4hkz6jn115z466gcwy900")))

