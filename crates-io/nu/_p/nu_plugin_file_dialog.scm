(define-module (crates-io nu _p nu_plugin_file_dialog) #:use-module (crates-io))

(define-public crate-nu_plugin_file_dialog-0.1.0 (c (n "nu_plugin_file_dialog") (v "0.1.0") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.7.0") (f (quote ("windows_dpi_awareness" "windows_visual_styles"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)))) (h "0sx2jkdixxnqrhnqj6gqhccmxjlqnn9dmmwgxbgza226wyk6lp1j")))

(define-public crate-nu_plugin_file_dialog-0.2.0 (c (n "nu_plugin_file_dialog") (v "0.2.0") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.7.0") (f (quote ("windows_dpi_awareness" "windows_visual_styles"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)))) (h "11p74lq9i7dilp16zaa99y4d32r5max5q7r17m5cr22iqmq07r55")))

(define-public crate-nu_plugin_file_dialog-0.3.0 (c (n "nu_plugin_file_dialog") (v "0.3.0") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.7.0") (f (quote ("windows_dpi_awareness" "windows_visual_styles"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)))) (h "0afjzfcaimimxxr1aaxfnv33fn2kfr1snl49qxhfyi81zrzan6bg")))

