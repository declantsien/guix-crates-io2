(define-module (crates-io nu _p nu_plugin_from_sse) #:use-module (crates-io))

(define-public crate-nu_plugin_from_sse-0.1.0 (c (n "nu_plugin_from_sse") (v "0.1.0") (d (list (d (n "nu-plugin") (r "^0.91") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1rh88wql27s9hl3jpm8zpdzr0rgkzv92kghmw2q15dvk6dbysjlg")))

(define-public crate-nu_plugin_from_sse-0.2.0 (c (n "nu_plugin_from_sse") (v "0.2.0") (d (list (d (n "nu-plugin") (r "^0.92") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92") (f (quote ("plugin"))) (d #t) (k 0)))) (h "15l83ys3n6yy1c7r4yakvrn906p0xs7q1x3l06zlzmmgap5pklb9")))

(define-public crate-nu_plugin_from_sse-0.3.0 (c (n "nu_plugin_from_sse") (v "0.3.0") (d (list (d (n "nu-plugin") (r "^0.94") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0rjhl5q11lfawjh5qwzvzjp54yalcqwfaixnxck9n5y704gipada")))

