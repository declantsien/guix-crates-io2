(define-module (crates-io nu _p nu_plugin_clipboard) #:use-module (crates-io))

(define-public crate-nu_plugin_clipboard-0.2.0 (c (n "nu_plugin_clipboard") (v "0.2.0") (d (list (d (n "arboard") (r "^3.2.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0ljicf70ipdv94fsfjrcs371chfhkapj7pypqybrgxxnc1nk4nzb") (f (quote (("use-wayland" "arboard/wayland-data-control"))))))

(define-public crate-nu_plugin_clipboard-0.3.0 (c (n "nu_plugin_clipboard") (v "0.3.0") (d (list (d (n "arboard") (r "^3.2.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "18n2bmhj231hkq6gdcgdfbglm5kg374b5y7k9va6j8prl9qqkj54") (f (quote (("use-wayland" "arboard/wayland-data-control") ("enforce-daemon") ("default"))))))

(define-public crate-nu_plugin_clipboard-0.3.1 (c (n "nu_plugin_clipboard") (v "0.3.1") (d (list (d (n "arboard") (r "^3.3.0") (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0fr68kl2v659n1vd715r1aha3n2jk8pi45clb5wggjrdj1mn5r3a") (f (quote (("use-wayland" "arboard/wayland-data-control") ("enforce-daemon") ("default"))))))

(define-public crate-nu_plugin_clipboard-0.3.4 (c (n "nu_plugin_clipboard") (v "0.3.4") (d (list (d (n "arboard") (r "^3.3.0") (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1p0as1c0sh549s4zfvcbwxihjwws7q7ff96hb9zyshc1jwki56kx") (f (quote (("use-wayland" "arboard/wayland-data-control") ("enforce-daemon") ("default"))))))

(define-public crate-nu_plugin_clipboard-0.91.0 (c (n "nu_plugin_clipboard") (v "0.91.0") (d (list (d (n "arboard") (r "^3.3.0") (k 0)) (d (n "nu-plugin") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0mcv1j2gf32sd43b7zqpm4j6nf2490w3r2w1sk0j0z71naghhns4") (f (quote (("use-wayland" "arboard/wayland-data-control") ("enforce-daemon") ("default"))))))

