(define-module (crates-io nu _p nu_plugin_qr) #:use-module (crates-io))

(define-public crate-nu_plugin_qr-0.1.0 (c (n "nu_plugin_qr") (v "0.1.0") (d (list (d (n "fast_qr") (r "^0.9.0") (f (quote ("svg" "image"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)) (d (n "quircs") (r "^0.10.1") (d #t) (k 0)))) (h "04qsydcb83w95dzvp7k9gkfi9lkg2zjgw99x87basmvwa6aml6gh")))

