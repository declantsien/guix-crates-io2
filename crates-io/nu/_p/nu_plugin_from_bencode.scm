(define-module (crates-io nu _p nu_plugin_from_bencode) #:use-module (crates-io))

(define-public crate-nu_plugin_from_bencode-0.2.0 (c (n "nu_plugin_from_bencode") (v "0.2.0") (d (list (d (n "bt_bencode") (r "^0.6.0") (f (quote ("alloc"))) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.62") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.62") (d #t) (k 0)))) (h "0ix17hsiclqrk8svgxwqkmbj6yikv9xq4w28wi45q78ww13m3hb0") (y #t)))

(define-public crate-nu_plugin_from_bencode-0.4.0 (c (n "nu_plugin_from_bencode") (v "0.4.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.77.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.77.1") (d #t) (k 0)))) (h "1ghafx8bldank1kx4v0z8k1rzl32ldrv1mdnak7743wr7v44kx72") (y #t)))

(define-public crate-nu_plugin_from_bencode-0.5.0 (c (n "nu_plugin_from_bencode") (v "0.5.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.84") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.84") (d #t) (k 0)))) (h "17cp8w6i8vv19nf58cgdgbhcyygp9x6gx5ijnkyc3zhv2pv06482") (y #t) (r "1.65.0")))

(define-public crate-nu_plugin_from_bencode-0.6.0 (c (n "nu_plugin_from_bencode") (v "0.6.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (d #t) (k 0)))) (h "1lw2bnf6a2bv4xlci64b3bi2pyvc3cssyjbrslc1ygavcx0wnjbb") (y #t) (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.7.0 (c (n "nu_plugin_from_bencode") (v "0.7.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (d #t) (k 0)))) (h "1b7x3gl3d0672faz8p92zd09nyasfpfcy86v12fb7sa3d7l5wva9") (y #t) (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.8.0 (c (n "nu_plugin_from_bencode") (v "0.8.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (d #t) (k 0)))) (h "0p63yvw7mribp0s2vb4si9kw0s0qhj2afsglq9vmygjk7i0xx1jw") (y #t) (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.8.1 (c (n "nu_plugin_from_bencode") (v "0.8.1") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (d #t) (k 0)))) (h "185jm393x7d0nmfw6qxw30zzm76ha44cpzir77mfpg1f40kc1imd") (y #t) (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.9.0 (c (n "nu_plugin_from_bencode") (v "0.9.0") (d (list (d (n "bt_bencode") (r "^0.7") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.88.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.1") (d #t) (k 0)))) (h "1v9lk12mk3wsdrfl2h42r6namihp3nv6nmpbfbp5jb2rhgjilhy8") (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.10.0 (c (n "nu_plugin_from_bencode") (v "0.10.0") (d (list (d (n "bt_bencode") (r "^0.8") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.90") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90") (d #t) (k 0)))) (h "15asfblgpf73wajqf4an5lx5z2gcqa5xnwhiiaf8grq2z46dyqwb") (r "1.70.0")))

(define-public crate-nu_plugin_from_bencode-0.11.0 (c (n "nu_plugin_from_bencode") (v "0.11.0") (d (list (d (n "bt_bencode") (r "^0.8") (f (quote ("alloc"))) (k 0)) (d (n "nu-plugin") (r "^0.93") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93") (d #t) (k 0)))) (h "0sln9qbczvn7v1hz3xkazz0q2nlqwidifl8z2k3p8sjvdbcqigms") (r "1.72.1")))

