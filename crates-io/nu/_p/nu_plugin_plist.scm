(define-module (crates-io nu _p nu_plugin_plist) #:use-module (crates-io))

(define-public crate-nu_plugin_plist-0.1.1 (c (n "nu_plugin_plist") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.74") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "03jdji12g5ff6gmr8fbpxm6gpjqvdsppvmcsm3nsxykb2qh0i9rs")))

(define-public crate-nu_plugin_plist-0.1.2 (c (n "nu_plugin_plist") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.76") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.76.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "18nq6m74s1c0v57g0hi244a2f2m3wcm23zjqjb4yc13g8s2xmra7")))

(define-public crate-nu_plugin_plist-0.1.4 (c (n "nu_plugin_plist") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.80") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.80") (d #t) (k 0)) (d (n "plist") (r "^1.4") (d #t) (k 0)))) (h "0m7axphqjss1xincch9r6cylv90mgl2wiazfp73njkm3a59wjy0n")))

(define-public crate-nu_plugin_plist-0.1.5 (c (n "nu_plugin_plist") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83") (d #t) (k 0)) (d (n "plist") (r "^1.4") (d #t) (k 0)))) (h "0k0g1c4ggbc08rdmrizn71ckab07d6rvw50w0h49y10ncvv1wr1j")))

(define-public crate-nu_plugin_plist-0.1.6 (c (n "nu_plugin_plist") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83") (d #t) (k 0)) (d (n "plist") (r "^1.4") (d #t) (k 0)))) (h "11nk22nw62j8w3hggh7bzxyr0kn00rw9ddkxmsqzz5n7gbrs9bxb")))

(define-public crate-nu_plugin_plist-0.89.0 (c (n "nu_plugin_plist") (v "0.89.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "1qlxgar1d094lcgxv3bn6d3whnw9qikdgy3l4lqydhj8g8axfsp5")))

(define-public crate-nu_plugin_plist-0.89.1 (c (n "nu_plugin_plist") (v "0.89.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "00y1ysswpgmyky2sk9dxjan6bbhq2zr6zzsz3hpzc425cnwn0sn5")))

(define-public crate-nu_plugin_plist-0.89.2 (c (n "nu_plugin_plist") (v "0.89.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "1d17gxh3zzajsp2y7i6wg56z94kyy47a6bicam51fyh05302k0h8")))

(define-public crate-nu_plugin_plist-0.89.3 (c (n "nu_plugin_plist") (v "0.89.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "0rr7chnpz144rmm3kpsy5dp4qc6pd26l99irb99fxl1bpjxdsr8f")))

(define-public crate-nu_plugin_plist-0.91.0 (c (n "nu_plugin_plist") (v "0.91.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.91") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "0byh0rfyv0j6ij6z6xb7x8jlwxlh11fh5f824fzvj0xib0v0gk9l")))

(define-public crate-nu_plugin_plist-0.92.1 (c (n "nu_plugin_plist") (v "0.92.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.1") (d #t) (k 0)) (d (n "plist") (r "^1.6") (d #t) (k 0)))) (h "0q40v939ky1j0lkl891cb8m536rr6sj4is41i37922zn27sn2ays")))

