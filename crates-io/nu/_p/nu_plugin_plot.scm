(define-module (crates-io nu _p nu_plugin_plot) #:use-module (crates-io))

(define-public crate-nu_plugin_plot-0.74.0 (c (n "nu_plugin_plot") (v "0.74.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0n9s3z8cjvr42iyyaaxmbd03jfklax5alsgjd92a325sybf67csg")))

