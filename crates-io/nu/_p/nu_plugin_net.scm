(define-module (crates-io nu _p nu_plugin_net) #:use-module (crates-io))

(define-public crate-nu_plugin_net-1.0.0 (c (n "nu_plugin_net") (v "1.0.0") (d (list (d (n "nu-plugin") (r "^0.66.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "pnet") (r "^0.31") (f (quote ("std"))) (d #t) (k 0)))) (h "09djbzva0nhw9k0dymkbl550aydslny3nb33vnhbcrbcyd0njs7l")))

(define-public crate-nu_plugin_net-1.1.0 (c (n "nu_plugin_net") (v "1.1.0") (d (list (d (n "nu-plugin") (r "^0.66.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "pnet") (r "^0.31") (f (quote ("std"))) (d #t) (k 0)))) (h "1q15jihqrqqvrs0iq2mffjpnbv44rgav5889frjcgdzzzy2fl5wc")))

(define-public crate-nu_plugin_net-1.2.0 (c (n "nu_plugin_net") (v "1.2.0") (d (list (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "pnet") (r "^0.31") (f (quote ("std"))) (d #t) (k 0)))) (h "0r5bi4j3jkmdrk1q0h14xmy308648xg1mrk9hfk16kzppx4fhw0d")))

(define-public crate-nu_plugin_net-1.3.0 (c (n "nu_plugin_net") (v "1.3.0") (d (list (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34") (f (quote ("std"))) (d #t) (k 0)))) (h "1v9f9y5xwmbwvzd6q3kbkwdln6lxrzdki4ssg8k4a3ynx092whr1")))

(define-public crate-nu_plugin_net-1.4.0 (c (n "nu_plugin_net") (v "1.4.0") (d (list (d (n "nu-plugin") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34") (f (quote ("std"))) (d #t) (k 0)))) (h "0dmpl6xvqqxhq9l1jn6s8kklnqy4yg980rix8zj38iwkisyrxqms")))

