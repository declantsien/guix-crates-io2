(define-module (crates-io nu _p nu_plugin_dialog) #:use-module (crates-io))

(define-public crate-nu_plugin_dialog-0.1.0 (c (n "nu_plugin_dialog") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.4") (f (quote ("fuzzy-select" "completion"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.78.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.78.0") (d #t) (k 0)))) (h "006qw2ifg3qznf5pvsb4fx64izh1n7yh0fp02wrval1np8gyp21v")))

(define-public crate-nu_plugin_dialog-0.2.0 (c (n "nu_plugin_dialog") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.10.4") (f (quote ("fuzzy-select" "completion"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.78.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.78.0") (d #t) (k 0)))) (h "1l3q7f1f16jij6knly1ir03cblkn35iixzvsrz1lqyy0vn49gv5x")))

