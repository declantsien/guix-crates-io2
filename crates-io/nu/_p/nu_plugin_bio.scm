(define-module (crates-io nu _p nu_plugin_bio) #:use-module (crates-io))

(define-public crate-nu_plugin_bio-0.70.0 (c (n "nu_plugin_bio") (v "0.70.0") (d (list (d (n "noodles") (r "^0.27.0") (f (quote ("fasta"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.70.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.70.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1mivs9bmkhclfd4qfc8zm3vynqf8gjay4x1q932rnkxpjbgvjkf1")))

(define-public crate-nu_plugin_bio-0.74.1 (c (n "nu_plugin_bio") (v "0.74.1") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "gfa") (r "^0.10.1") (d #t) (k 0)) (d (n "noodles") (r "^0.31.1") (f (quote ("fasta" "fastq" "bam" "sam" "cram" "bcf" "vcf" "gff" "bgzf" "bed"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "10zcq3i1chily3w79faz56dy3zql6n88ix3d5w7g7h879gmg5zyd")))

(define-public crate-nu_plugin_bio-0.74.2 (c (n "nu_plugin_bio") (v "0.74.2") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "gfa") (r "^0.10.1") (d #t) (k 0)) (d (n "noodles") (r "^0.31.1") (f (quote ("fasta" "fastq" "bam" "sam" "cram" "bcf" "vcf" "gff" "bgzf" "bed"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0br68fg2jabvmvnx76n2lvdz2ng9iaa3bqmdw5h6mlwvxhl1hc4r")))

(define-public crate-nu_plugin_bio-0.76.0 (c (n "nu_plugin_bio") (v "0.76.0") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "gfa") (r "^0.10.1") (d #t) (k 0)) (d (n "noodles") (r "^0.32.0") (f (quote ("fasta" "fastq" "bam" "sam" "cram" "bcf" "vcf" "gff" "bgzf" "bed"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.76.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.76.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0kfnfbac7051m3a87c764c19pa7ii48vdyb955da2ck3r1nqhass")))

(define-public crate-nu_plugin_bio-0.85.0 (c (n "nu_plugin_bio") (v "0.85.0") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "gfa") (r "^0.10.1") (d #t) (k 0)) (d (n "noodles") (r "^0.53.0") (f (quote ("fasta" "fastq" "bam" "sam" "cram" "bcf" "vcf" "gff" "bgzf" "bed"))) (d #t) (k 0)) (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "05mfzy4dkd7gqw926xkmcxn6nqqm29r5sj0yjfk38iclks8qh1dc")))

