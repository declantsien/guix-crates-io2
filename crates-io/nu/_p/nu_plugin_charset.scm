(define-module (crates-io nu _p nu_plugin_charset) #:use-module (crates-io))

(define-public crate-nu_plugin_charset-0.1.0 (c (n "nu_plugin_charset") (v "0.1.0") (d (list (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)))) (h "1q7gwzxaxl75x3np803z39zfbh61aif4gp3mr87syy4bcy1zaqdl")))

(define-public crate-nu_plugin_charset-0.1.1 (c (n "nu_plugin_charset") (v "0.1.1") (d (list (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)))) (h "0hbf786xxyd1g3b6q2qc5cswjgh647mb2a2a0dv876ii5r2khlq6")))

(define-public crate-nu_plugin_charset-0.1.2 (c (n "nu_plugin_charset") (v "0.1.2") (d (list (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)))) (h "1nzz1m5vv1d66g4jwnn0b0zvmbp2da2wsvszgsc4p2k3xasbd1nk")))

