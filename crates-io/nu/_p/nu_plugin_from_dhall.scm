(define-module (crates-io nu _p nu_plugin_from_dhall) #:use-module (crates-io))

(define-public crate-nu_plugin_from_dhall-0.0.1 (c (n "nu_plugin_from_dhall") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.6.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "nu-errors") (r "~0") (d #t) (k 0)) (d (n "nu-plugin") (r "~0") (d #t) (k 0)) (d (n "nu-protocol") (r "~0") (d #t) (k 0)) (d (n "nu-source") (r "~0") (d #t) (k 0)) (d (n "nu-value-ext") (r "~0") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)))) (h "1swhccq4qbwfqhw7r4apd3q0wl9ffcwaqjdds63n3dbnciwxsr5i")))

