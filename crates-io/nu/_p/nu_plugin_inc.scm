(define-module (crates-io nu _p nu_plugin_inc) #:use-module (crates-io))

(define-public crate-nu_plugin_inc-0.7.0 (c (n "nu_plugin_inc") (v "0.7.0") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "nu-build") (r "^0.7.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.7.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1mq3wn2njbmbq56na212qncspns9hh9ygarfjcx0q0x62vzlyak2")))

(define-public crate-nu_plugin_inc-0.8.0 (c (n "nu_plugin_inc") (v "0.8.0") (d (list (d (n "nu-build") (r "^0.8.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0jnwrkz2jgpcg7w1nk7z3brz9wr8ainvak0yhi6mfxfg7xbr8gn5")))

(define-public crate-nu_plugin_inc-0.9.0 (c (n "nu_plugin_inc") (v "0.9.0") (d (list (d (n "nu-build") (r "^0.9.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.9.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "07hjrki1k60sgi56a58vkzgk4szgix028l7mlbdxfby4iijprrya")))

(define-public crate-nu_plugin_inc-0.10.0 (c (n "nu_plugin_inc") (v "0.10.0") (d (list (d (n "nu-build") (r "^0.10.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.10.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0mxvcz2ccr8c1v45jvxc6056nssap0kz1xglhx9xlrk8wfhq7amb")))

(define-public crate-nu_plugin_inc-0.11.0 (c (n "nu_plugin_inc") (v "0.11.0") (d (list (d (n "nu-build") (r "^0.11.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.11.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0ffmiwayjfgr4qch1ywy2r66p784ymidlvh6j5i19g8wwcnpv9p8")))

(define-public crate-nu_plugin_inc-0.12.0 (c (n "nu_plugin_inc") (v "0.12.0") (d (list (d (n "nu-build") (r "^0.12.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.12.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1d4319p6rrwv19klyg7gh59py6lzvlj04vdz9migff4w28j26bh5")))

(define-public crate-nu_plugin_inc-0.13.0 (c (n "nu_plugin_inc") (v "0.13.0") (d (list (d (n "nu-build") (r "^0.13.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.13.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0wnkh6fpb07ig9svnim4rzcrb3s970fxrj76pbs9l2b9syriz6y3")))

(define-public crate-nu_plugin_inc-0.14.0 (c (n "nu_plugin_inc") (v "0.14.0") (d (list (d (n "nu-build") (r "^0.14.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.14.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1zs1bbwnxndqqxibiy60c93cm94qn3d4gs48b6xvn2mmvv913pz8")))

(define-public crate-nu_plugin_inc-0.15.0 (c (n "nu_plugin_inc") (v "0.15.0") (d (list (d (n "nu-build") (r "^0.15.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.15.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "143awjmfsx2syr4qixwzyzix0q9h0vip2m26akvrwlrljvm6f7m5")))

(define-public crate-nu_plugin_inc-0.16.0 (c (n "nu_plugin_inc") (v "0.16.0") (d (list (d (n "nu-build") (r "^0.16.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.16.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "0g2426h0xfdgq14zpwmmdm3vdkhazjl2w66j0ww8d5xzmf5idq2n")))

(define-public crate-nu_plugin_inc-0.17.0 (c (n "nu_plugin_inc") (v "0.17.0") (d (list (d (n "nu-build") (r "^0.17.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.17.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "0r8gy5bkff2h9mdq1i8yhmwqcvkyf2a2ll9ziz2xv08wyxif3kkx")))

(define-public crate-nu_plugin_inc-0.18.0 (c (n "nu_plugin_inc") (v "0.18.0") (d (list (d (n "nu-errors") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.18.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1ilv0vsmlqva8lsyqvilwx04s4sx4088pw035y3q4zzjid21a3hx")))

(define-public crate-nu_plugin_inc-0.18.1 (c (n "nu_plugin_inc") (v "0.18.1") (d (list (d (n "nu-errors") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.18.1") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1ydyb911dhk1vhyaa0v080p1w5z7h1nwl2i0jdsyc207kr68gxhr")))

(define-public crate-nu_plugin_inc-0.19.0 (c (n "nu_plugin_inc") (v "0.19.0") (d (list (d (n "nu-errors") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.19.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1759c97mwpb8z76cvzjlv3m9rc10im4qp778ki96n3xl6pb0awcr")))

(define-public crate-nu_plugin_inc-0.20.0 (c (n "nu_plugin_inc") (v "0.20.0") (d (list (d (n "nu-errors") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.20.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1jdprihnaaikb8j5xcrbp0pjn25p8li47a1dnnfy1khjlqqzsjfq")))

(define-public crate-nu_plugin_inc-0.21.0 (c (n "nu_plugin_inc") (v "0.21.0") (d (list (d (n "nu-errors") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.21.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "0cdm39a1gy0dclzzp1wvsablv0zzsds1199kpd23jxjfsizs1v22")))

(define-public crate-nu_plugin_inc-0.22.0 (c (n "nu_plugin_inc") (v "0.22.0") (d (list (d (n "nu-errors") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.22.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1g11vgdn371y90qx6ad96hd6j7jqkgj6j9wlpf0qa6b78pw2kb86")))

(define-public crate-nu_plugin_inc-0.23.0 (c (n "nu_plugin_inc") (v "0.23.0") (d (list (d (n "nu-errors") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-plugin") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-protocol") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-source") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-test-support") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-value-ext") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "semver") (r ">=0.10.0, <0.11.0") (d #t) (k 0)))) (h "1mq2fnmgahb9wp2lcgqiqkw80g7rnmh5sajpgmcks299z231dbjh")))

(define-public crate-nu_plugin_inc-0.24.0 (c (n "nu_plugin_inc") (v "0.24.0") (d (list (d (n "nu-errors") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.24.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1vmq6brcr31wi2dr8hm3m81d730m2c0610df5xiiwxx7krknd5h9")))

(define-public crate-nu_plugin_inc-0.24.1 (c (n "nu_plugin_inc") (v "0.24.1") (d (list (d (n "nu-errors") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.24.1") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1pb40w7swfq904zqs1p7cpgqm00yngssda40qgc6rnvkvgnzping")))

(define-public crate-nu_plugin_inc-0.25.0 (c (n "nu_plugin_inc") (v "0.25.0") (d (list (d (n "nu-errors") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.25.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "0d1ncvbshxq679vdz9ns3zc68pda0gsya40wkzp3x8pfa0ihxl94")))

(define-public crate-nu_plugin_inc-0.25.1 (c (n "nu_plugin_inc") (v "0.25.1") (d (list (d (n "nu-errors") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.25.1") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "00g6p6p84kxa2692bpnaypbq66zy98czn0yb3283xhh80pk5333y")))

(define-public crate-nu_plugin_inc-0.26.0 (c (n "nu_plugin_inc") (v "0.26.0") (d (list (d (n "nu-errors") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.26.0") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1v72zaqhzf37x3sba43c6pry75v4aacqsk4mw7fnmx6mfzbpjdn6")))

(define-public crate-nu_plugin_inc-0.27.0 (c (n "nu_plugin_inc") (v "0.27.0") (d (list (d (n "nu-errors") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.27.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0rpgbxk79wgg3wks9w1nz0pala2arq9kxrqm971ph10gl398dah0")))

(define-public crate-nu_plugin_inc-0.27.1 (c (n "nu_plugin_inc") (v "0.27.1") (d (list (d (n "nu-errors") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.27.1") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1ykrfixf642zv0h6349mxsidm9m7j933imnw978v96ymzp7hdalk")))

(define-public crate-nu_plugin_inc-0.28.0 (c (n "nu_plugin_inc") (v "0.28.0") (d (list (d (n "nu-errors") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.28.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1dbayv8gcjpz46xgvh9b1pka2ygyhyalk1xzh43cvaccqn4qafrb")))

(define-public crate-nu_plugin_inc-0.29.0 (c (n "nu_plugin_inc") (v "0.29.0") (d (list (d (n "nu-errors") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.29.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0w0dmqa6rv12whpmsvli5nb7pnazrhdp08x2fzrabz60rq1qsfx9")))

(define-public crate-nu_plugin_inc-0.30.0 (c (n "nu_plugin_inc") (v "0.30.0") (d (list (d (n "nu-errors") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.30.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1jf3gmih7li67v4gr4jy6y2xmsyk9msq8fr715cabx8k8v222k8j")))

(define-public crate-nu_plugin_inc-0.31.0 (c (n "nu_plugin_inc") (v "0.31.0") (d (list (d (n "nu-errors") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.31.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0qfi48rh813vbmlaj0idkkcjy6dkx0jz7k023dwdf47v0m6c34hq")))

(define-public crate-nu_plugin_inc-0.32.0 (c (n "nu_plugin_inc") (v "0.32.0") (d (list (d (n "nu-errors") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1pla6640qm9bj2hya5gg43g5h990hfbgjbrbk93sqghi70r32yd7")))

(define-public crate-nu_plugin_inc-0.33.0 (c (n "nu_plugin_inc") (v "0.33.0") (d (list (d (n "nu-errors") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.33.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1x8p09kgj8sxchgakgzafpzlxz7ig1gmz6b6cg7h0grk56p0k9cj")))

(define-public crate-nu_plugin_inc-0.34.0 (c (n "nu_plugin_inc") (v "0.34.0") (d (list (d (n "nu-errors") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.34.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0ms32kk0h74gz3h1d61sc1sbg490r3lcfpl4vy5gww2kl9qbgl9w")))

(define-public crate-nu_plugin_inc-0.35.0 (c (n "nu_plugin_inc") (v "0.35.0") (d (list (d (n "nu-errors") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.35.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0414skx1qffmddp81kaacjd6bfcf9843xdq65kkaxc4lp1zs03i5")))

(define-public crate-nu_plugin_inc-0.36.0 (c (n "nu_plugin_inc") (v "0.36.0") (d (list (d (n "nu-errors") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.36.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1vrvrdyhb9rvrbdaq25b1b9ibpwr52hz3p60kl3xn0zs437vqhzs")))

(define-public crate-nu_plugin_inc-0.37.0 (c (n "nu_plugin_inc") (v "0.37.0") (d (list (d (n "nu-errors") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.37.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.37.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0s6yl3xiww6qbjxb5ap1zhqz6jfni59byphlz4fq36pha128h81y")))

(define-public crate-nu_plugin_inc-0.38.0 (c (n "nu_plugin_inc") (v "0.38.0") (d (list (d (n "nu-errors") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.38.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.38.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "16m4fy1vh1nfyacyvll9h16gq772mxf3dy6ac5i8hxmvh8kk218i")))

(define-public crate-nu_plugin_inc-0.39.0 (c (n "nu_plugin_inc") (v "0.39.0") (d (list (d (n "nu-errors") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.39.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.39.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1rm28z8bivkr2filycp1w11qc9s4rsdxgmq7pm5vs9bjfslh4rgh")))

(define-public crate-nu_plugin_inc-0.40.0 (c (n "nu_plugin_inc") (v "0.40.0") (d (list (d (n "nu-errors") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.40.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.40.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1vy90lx27qcqp7pdsfma3yf024mslw4zinfh77gkp8zi547789z2")))

(define-public crate-nu_plugin_inc-0.41.0 (c (n "nu_plugin_inc") (v "0.41.0") (d (list (d (n "nu-errors") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.41.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.41.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1n6vbm3sc7hnvxzxgfgxjv5iincdajq85f78r5bqrh6qgmpx9n7k")))

(define-public crate-nu_plugin_inc-0.42.0 (c (n "nu_plugin_inc") (v "0.42.0") (d (list (d (n "nu-errors") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.42.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.42.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0r9di4mz6g6gqsgdzxxgpz6la7a18ib3dars9qkadn1bqavsb371")))

(define-public crate-nu_plugin_inc-0.43.0 (c (n "nu_plugin_inc") (v "0.43.0") (d (list (d (n "nu-errors") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.43.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.43.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "12d2rrw3z0791vg44h87xprkcxksqrhjk6jx23f8hpxfh3mg4llg")))

(define-public crate-nu_plugin_inc-0.44.0 (c (n "nu_plugin_inc") (v "0.44.0") (d (list (d (n "nu-errors") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-test-support") (r "^0.44.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.44.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0gyj54ip1nl68rav3h78sjljh826cp71khmjsbsvyphhgdm1kbhw")))

(define-public crate-nu_plugin_inc-0.60.0 (c (n "nu_plugin_inc") (v "0.60.0") (d (list (d (n "nu-plugin") (r "^0.60.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.60.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "09r6ig0484wz92f465rfdyxsk6ih63pd74wq4y0vnpdcaj71x9gz")))

(define-public crate-nu_plugin_inc-0.61.0 (c (n "nu_plugin_inc") (v "0.61.0") (d (list (d (n "nu-plugin") (r "^0.61.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.61.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1ayvvg1ar1rv3gk0c8k8cpxz4biz10zx0sqbvh1488h32lpjm3ij")))

(define-public crate-nu_plugin_inc-0.62.0 (c (n "nu_plugin_inc") (v "0.62.0") (d (list (d (n "nu-plugin") (r "^0.62.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.62.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1kvyxvpjxxy15jca4d1zml69jc2f7qh1wpblihv4xpwy72yd20nf")))

(define-public crate-nu_plugin_inc-0.63.0 (c (n "nu_plugin_inc") (v "0.63.0") (d (list (d (n "nu-plugin") (r "^0.63.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.63.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1z0b3gl4bhf2xbvpii2blibx1cq739h257q4gvjb79bmb7ad1bjp")))

(define-public crate-nu_plugin_inc-0.64.0 (c (n "nu_plugin_inc") (v "0.64.0") (d (list (d (n "nu-plugin") (r "^0.64.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.64.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0iaaq557yf0s9q51xlg70r92h3ghci8vb11l3jx9q83il546fjz1")))

(define-public crate-nu_plugin_inc-0.65.0 (c (n "nu_plugin_inc") (v "0.65.0") (d (list (d (n "nu-plugin") (r "^0.65.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.65.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1wxw4j05w2nzd120gi0m2kzj5af2bhbgrdjfgc61a8mlkqr8kx53")))

(define-public crate-nu_plugin_inc-0.66.0 (c (n "nu_plugin_inc") (v "0.66.0") (d (list (d (n "nu-plugin") (r "^0.66.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0kh03rwqii21g9nvdg5qgs36ywcg7dd781rhs8lmsd00ssa66shh")))

(define-public crate-nu_plugin_inc-0.66.1 (c (n "nu_plugin_inc") (v "0.66.1") (d (list (d (n "nu-plugin") (r "^0.66.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1vpvxabk18g3hg19k1vm65wmvg028mapnpvsgk6hjmn8lgkzhfjm")))

(define-public crate-nu_plugin_inc-0.66.2 (c (n "nu_plugin_inc") (v "0.66.2") (d (list (d (n "nu-plugin") (r "^0.66.2") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.2") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0ykdwg5fb7dkdmxsb4z72kf1wg48ap9jblf0gl67zfmh1dhi0594")))

(define-public crate-nu_plugin_inc-0.67.0 (c (n "nu_plugin_inc") (v "0.67.0") (d (list (d (n "nu-plugin") (r "^0.67.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.67.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1p51ama4mz7zgq2xq2fb5glilgzcib5aabpb1kc0vp180lkqfncg")))

(define-public crate-nu_plugin_inc-0.68.0 (c (n "nu_plugin_inc") (v "0.68.0") (d (list (d (n "nu-plugin") (r "^0.68.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.68.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "01rzq3xjp4ws2596r466chff6fj5mvzrfb7cmllzmybvdg6wfzvw")))

(define-public crate-nu_plugin_inc-0.68.1 (c (n "nu_plugin_inc") (v "0.68.1") (d (list (d (n "nu-plugin") (r "^0.68.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.68.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "19wi8kwx9blrbcdycrkmizvy7ggqzvkk4m4d3yfhd2c2ig3gs8ys")))

(define-public crate-nu_plugin_inc-0.69.0 (c (n "nu_plugin_inc") (v "0.69.0") (d (list (d (n "nu-plugin") (r "^0.69.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.69.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0aq26ghm71wka89gr5xp3vsdq5cip6g2miwykgbyq9wmvlgsadvm")))

(define-public crate-nu_plugin_inc-0.69.1 (c (n "nu_plugin_inc") (v "0.69.1") (d (list (d (n "nu-plugin") (r "^0.69.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.69.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "01wd6fhcnjhpvd1mxnqn91kmqgwymk6chvb48rn0ddcrhq91698x")))

(define-public crate-nu_plugin_inc-0.70.0 (c (n "nu_plugin_inc") (v "0.70.0") (d (list (d (n "nu-plugin") (r "^0.70.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.70.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "03x02azrvdnfygwf526662v2x6f50bmnlnxxq3mx8zd83y6vl3qn")))

(define-public crate-nu_plugin_inc-0.71.0 (c (n "nu_plugin_inc") (v "0.71.0") (d (list (d (n "nu-plugin") (r "^0.71.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.71.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0v18jyl761ghp9vn6cfv8yvrgh316lkvxsx7yhr5l6dj21n2np6d")))

(define-public crate-nu_plugin_inc-0.72.0 (c (n "nu_plugin_inc") (v "0.72.0") (d (list (d (n "nu-plugin") (r "^0.72.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.72.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0fd0dgdnz9kgbky0prmfqpf1r16x78d1j0inx9irds5gs10vqnxw")))

(define-public crate-nu_plugin_inc-0.73.0 (c (n "nu_plugin_inc") (v "0.73.0") (d (list (d (n "nu-plugin") (r "^0.73.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.73.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1bk2cpf40y0n880ihgaap6jcrfkw16p3q9pr5idv24y7lr46j5s3")))

(define-public crate-nu_plugin_inc-0.74.0 (c (n "nu_plugin_inc") (v "0.74.0") (d (list (d (n "nu-plugin") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "17p2nj9128gbbaz17dzfjsfhph99fway12sbjx9frzckwv797sga")))

(define-public crate-nu_plugin_inc-0.75.0 (c (n "nu_plugin_inc") (v "0.75.0") (d (list (d (n "nu-plugin") (r "^0.75.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.75.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "0yp5k1zx65k5xn5xpwpxpxl3svkva4qkbzmzx2m80mqxbdcqp0id")))

(define-public crate-nu_plugin_inc-0.76.0 (c (n "nu_plugin_inc") (v "0.76.0") (d (list (d (n "nu-plugin") (r "^0.76.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.76.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "1i1p2ga6xsvigmzdgibsp9ibljs47xfrwcj7d4swy9d5vdxxn2iq")))

(define-public crate-nu_plugin_inc-0.77.0 (c (n "nu_plugin_inc") (v "0.77.0") (d (list (d (n "nu-plugin") (r "^0.77.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.77.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "0nvl3li6w4ndm7080sbzjwdm9g2dpbimkjvqbl6yzlphiblmby71")))

(define-public crate-nu_plugin_inc-0.77.1 (c (n "nu_plugin_inc") (v "0.77.1") (d (list (d (n "nu-plugin") (r "^0.77.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.77.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "147nfq7b4rri3b02s5dz6ac57iac4hnac95rbc7h3mj8yyqyy09z")))

(define-public crate-nu_plugin_inc-0.78.0 (c (n "nu_plugin_inc") (v "0.78.0") (d (list (d (n "nu-plugin") (r "^0.78.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.78.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "1g2l2mcsn3jjhm9xp6r3lpz9wbf5l40fisdkq1z0g4adnhf37gai")))

(define-public crate-nu_plugin_inc-0.79.0 (c (n "nu_plugin_inc") (v "0.79.0") (d (list (d (n "nu-plugin") (r "^0.79.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.79.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "0cn4wfi048ix4mihz3dajxy0swq34ixl03wff8qxhjkm2kzbf296")))

(define-public crate-nu_plugin_inc-0.80.0 (c (n "nu_plugin_inc") (v "0.80.0") (d (list (d (n "nu-plugin") (r "^0.80.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.80.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)))) (h "1j8ka715mgpd7vkbmbk5pz8sppcn068l29mph3x3hg2007531yfa")))

(define-public crate-nu_plugin_inc-0.81.0 (c (n "nu_plugin_inc") (v "0.81.0") (d (list (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0fn3qj9zd5zp0gh7rvgh9y01x3nbm3fjcg0a4lz00c1ynk6q0hrb")))

(define-public crate-nu_plugin_inc-0.82.0 (c (n "nu_plugin_inc") (v "0.82.0") (d (list (d (n "nu-plugin") (r "^0.82.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.82.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "00ld7cyilws631vzm8y7gx54nyd2ib3yrzxpjqlgfc09z45nry6a")))

(define-public crate-nu_plugin_inc-0.83.0 (c (n "nu_plugin_inc") (v "0.83.0") (d (list (d (n "nu-plugin") (r "^0.83.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0bci99iwnf5yhy1czajhn9zx98dhcalgm7yhq0qasf0zv1nkzg2w")))

(define-public crate-nu_plugin_inc-0.83.1 (c (n "nu_plugin_inc") (v "0.83.1") (d (list (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1wsbi2fdk8mmswkkm6ngfs6gyb46wxrdr6daswjy4pml7k32lyfp")))

(define-public crate-nu_plugin_inc-0.84.0 (c (n "nu_plugin_inc") (v "0.84.0") (d (list (d (n "nu-plugin") (r "^0.84.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.84.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0d750z90rzj2ly5qsjxfwgl3jdf53c170xggmwry9kldviihhq7f")))

(define-public crate-nu_plugin_inc-0.85.0 (c (n "nu_plugin_inc") (v "0.85.0") (d (list (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0hw70g60mch1lhli791g2h2ajf0lgh2mg8w99snkzwiqa4kjab3h")))

(define-public crate-nu_plugin_inc-0.86.0 (c (n "nu_plugin_inc") (v "0.86.0") (d (list (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1mhj9wdvpj9z6jmzmybp85z9pjdb50gb2rz1ghkmygyc4fg1imw9")))

(define-public crate-nu_plugin_inc-0.87.0 (c (n "nu_plugin_inc") (v "0.87.0") (d (list (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1rn83dmc159igg4f8w43ag83dbajl50i1ih7k1lw3mc2whs5334g")))

(define-public crate-nu_plugin_inc-0.87.1 (c (n "nu_plugin_inc") (v "0.87.1") (d (list (d (n "nu-plugin") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0qx6ygr3hb0dpcipmkadg5z7hm3723dprm99qvhcl6fkj1hr3y5k")))

(define-public crate-nu_plugin_inc-0.88.0 (c (n "nu_plugin_inc") (v "0.88.0") (d (list (d (n "nu-plugin") (r "^0.88.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1g045mmb6pgj8p6v9palr1szzi8jrd0gx4mfmpq9hqwari3mp6wk")))

(define-public crate-nu_plugin_inc-0.88.1 (c (n "nu_plugin_inc") (v "0.88.1") (d (list (d (n "nu-plugin") (r "^0.88.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1j0caaq9vbvcsfk7fhx8mq92qzfdrvsr6s67plisb6v4j6qh4ndj")))

(define-public crate-nu_plugin_inc-0.89.0 (c (n "nu_plugin_inc") (v "0.89.0") (d (list (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0syc2xjjr15jmxpspf2s7ks9mjz7visvjjdv6d5khf314aj3hbwr")))

(define-public crate-nu_plugin_inc-0.90.1 (c (n "nu_plugin_inc") (v "0.90.1") (d (list (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0rar5mbfzxdwy398gbc94xpirgab1ck9l3jgax6apb4fwh6cmv84")))

(define-public crate-nu_plugin_inc-0.91.0 (c (n "nu_plugin_inc") (v "0.91.0") (d (list (d (n "nu-plugin") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "178j4v84x6av3dv87gcrdh9ms6s54whmjd127a61n7lg3pa2yybz")))

(define-public crate-nu_plugin_inc-0.92.0 (c (n "nu_plugin_inc") (v "0.92.0") (d (list (d (n "nu-plugin") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0rsjsv3j1m06x8sapbab9207pxpmkzghzsz4bvad8qvy5lms92a7")))

(define-public crate-nu_plugin_inc-0.92.1 (c (n "nu_plugin_inc") (v "0.92.1") (d (list (d (n "nu-plugin") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0jqg5v6bvrhx2y280vpbdcm90jb4r5risansgld813cx870b9dri")))

(define-public crate-nu_plugin_inc-0.92.2 (c (n "nu_plugin_inc") (v "0.92.2") (d (list (d (n "nu-plugin") (r "^0.92.2") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.2") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0ykgdxzbs8pl6lv4frda60z4ir246lbbr5927qavdj6xi3zhbymq")))

(define-public crate-nu_plugin_inc-0.93.0 (c (n "nu_plugin_inc") (v "0.93.0") (d (list (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1bkzlsqqwgswv2gizpki9nak3pgf6r6kz678d4d8sv5lngxqq3y3")))

(define-public crate-nu_plugin_inc-0.94.0 (c (n "nu_plugin_inc") (v "0.94.0") (d (list (d (n "nu-plugin") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "18mylz12y3xz1720xgmfkk9p0q5ssyq7gf8xg7dsaqg08ah251zj")))

