(define-module (crates-io nu _p nu_plugin_shuffle) #:use-module (crates-io))

(define-public crate-nu_plugin_shuffle-0.1.0 (c (n "nu_plugin_shuffle") (v "0.1.0") (d (list (d (n "nu-errors") (r "~0") (d #t) (k 0)) (d (n "nu-plugin") (r "~0") (d #t) (k 0)) (d (n "nu-protocol") (r "~0") (d #t) (k 0)) (d (n "nu-source") (r "~0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "02xhn1aq6apv49x4v335i8b1mmxm3ipxsxxfvjbpv4kla3zgv6yc") (y #t)))

(define-public crate-nu_plugin_shuffle-0.1.1 (c (n "nu_plugin_shuffle") (v "0.1.1") (d (list (d (n "nu-errors") (r "~0") (d #t) (k 0)) (d (n "nu-plugin") (r "~0") (d #t) (k 0)) (d (n "nu-protocol") (r "~0") (d #t) (k 0)) (d (n "nu-source") (r "~0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "118x331lazxqnmcb4h2z945i826676mpaqnixzimwwsl2i13p7pm") (y #t)))

(define-public crate-nu_plugin_shuffle-0.1.2 (c (n "nu_plugin_shuffle") (v "0.1.2") (d (list (d (n "nu-errors") (r "~0") (d #t) (k 0)) (d (n "nu-plugin") (r "~0") (d #t) (k 0)) (d (n "nu-protocol") (r "~0") (d #t) (k 0)) (d (n "nu-source") (r "~0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "13s9nkq0lf9id5ya0im4apdfx3yiyfv0rd1iwvgsn2zwpndisfjb") (y #t)))

