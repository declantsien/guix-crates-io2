(define-module (crates-io nu _p nu_plugin_from_beancount) #:use-module (crates-io))

(define-public crate-nu_plugin_from_beancount-1.0.0 (c (n "nu_plugin_from_beancount") (v "1.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "beancount-parser") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nu-plugin") (r "^0.75") (k 0)) (d (n "nu-protocol") (r "^0.75") (f (quote ("plugin"))) (k 0)) (d (n "rstest") (r "^0.16") (k 2)))) (h "1rq2liq7w3nypwffpqhkab3snsb7lq7xjgkm2nj08x0pgxahhg8k") (r "1.62")))

(define-public crate-nu_plugin_from_beancount-1.1.0 (c (n "nu_plugin_from_beancount") (v "1.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "beancount-parser") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nu-plugin") (r "^0.75") (k 0)) (d (n "nu-protocol") (r "^0.75") (f (quote ("plugin"))) (k 0)) (d (n "rstest") (r "^0.16") (k 2)))) (h "1mv603qlqn3bfcfjc13i41d4ma881i6y4pvbrjlbgay0z6vljaqc") (r "1.62")))

(define-public crate-nu_plugin_from_beancount-2.0.0 (c (n "nu_plugin_from_beancount") (v "2.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "beancount-parser") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nu-plugin") (r "^0.76") (k 0)) (d (n "nu-protocol") (r "^0.76") (f (quote ("plugin"))) (k 0)) (d (n "rstest") (r "^0.16") (k 2)))) (h "1ml7mg7cpyi1778cfz4fkkfkbrbpjdrmhyy182k114kj4p9g2dc1") (r "1.62")))

