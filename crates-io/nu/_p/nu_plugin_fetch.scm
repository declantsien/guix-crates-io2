(define-module (crates-io nu _p nu_plugin_fetch) #:use-module (crates-io))

(define-public crate-nu_plugin_fetch-0.7.0 (c (n "nu_plugin_fetch") (v "0.7.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.7.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.7.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0j95fcylbp03vwl8xky5gm0zb1x3l5cmpdka0xpaq44mx137r8vn")))

(define-public crate-nu_plugin_fetch-0.8.0 (c (n "nu_plugin_fetch") (v "0.8.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.8.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.8.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "06j854r83vh7flz93dxfrd38icz92s0rpd50n3jql0x87lp25ng5")))

(define-public crate-nu_plugin_fetch-0.9.0 (c (n "nu_plugin_fetch") (v "0.9.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.9.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.9.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1027hql40qiyzkbjnpv1dhn6q43khzwqzczc34522b7mlv33fsk0")))

(define-public crate-nu_plugin_fetch-0.10.0 (c (n "nu_plugin_fetch") (v "0.10.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.10.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.10.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "01pzx1bd9zll5qx2vifcs448j6ip1k6pdc7vc195s2bry5mhxb0g")))

(define-public crate-nu_plugin_fetch-0.11.0 (c (n "nu_plugin_fetch") (v "0.11.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.11.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.11.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1d65c1gid9lzgvan64j9kgk5whma8iqgbym8rymcihys3j2i0v91")))

(define-public crate-nu_plugin_fetch-0.12.0 (c (n "nu_plugin_fetch") (v "0.12.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.12.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.12.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0xdpazc6y0000896llm03a94ykh2kjkz8bxfzdx74ww0lcn31521")))

(define-public crate-nu_plugin_fetch-0.13.0 (c (n "nu_plugin_fetch") (v "0.13.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.13.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.13.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "09faxkl0xqk8sn9vffp1s01bdmjbf9l0lm0nwcyr67nm6xzpa552")))

(define-public crate-nu_plugin_fetch-0.14.0 (c (n "nu_plugin_fetch") (v "0.14.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.14.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.14.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "09vkpx8qsgx969mk1y3dvg4l2svp7swca4pilvj21nbqxfvazqyp")))

(define-public crate-nu_plugin_fetch-0.15.0 (c (n "nu_plugin_fetch") (v "0.15.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.15.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.15.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "062v2c4wl0qdhk9v8lja8lpyxj3rig2ykqg04n81aqdrhl4h95nb")))

(define-public crate-nu_plugin_fetch-0.16.0 (c (n "nu_plugin_fetch") (v "0.16.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.16.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.16.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "03jx78w6h5710jzi7s1mk2ilg4a5yrfk7fj95wpl4d82j496ai8f")))

(define-public crate-nu_plugin_fetch-0.17.0 (c (n "nu_plugin_fetch") (v "0.17.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-build") (r "^0.17.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.17.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0qpqqvq2gp10hzkw42gn0mn1wbw5jq88bqmlp244qmdw3j5284nd")))

(define-public crate-nu_plugin_fetch-0.18.0 (c (n "nu_plugin_fetch") (v "0.18.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.18.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0vly2ppvgh3w80h064sw8qhl0l5mw70y9qaaxpibxnjp6f5shc4s")))

(define-public crate-nu_plugin_fetch-0.18.1 (c (n "nu_plugin_fetch") (v "0.18.1") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.18.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.18.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0n2969kia4fajx1rjxx49rhpjqvv6y2k6pi6yw9r4k5d4dhrs7gi")))

(define-public crate-nu_plugin_fetch-0.19.0 (c (n "nu_plugin_fetch") (v "0.19.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.19.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.19.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1yrvwyhznnbjyj7p7pg7619fdj70zjs05rw95sal0d9ypj4pa1l2")))

(define-public crate-nu_plugin_fetch-0.20.0 (c (n "nu_plugin_fetch") (v "0.20.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.20.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.20.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0i7nc1xskadsrcb65g4lsqsj3ky8shf77pmd514kgjzp98nvlnjf")))

(define-public crate-nu_plugin_fetch-0.21.0 (c (n "nu_plugin_fetch") (v "0.21.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.21.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.21.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "01d7isfaj2ckd2vs19f7c6hk1skr5zhvzas06lsax4n7vymkym01")))

(define-public crate-nu_plugin_fetch-0.22.0 (c (n "nu_plugin_fetch") (v "0.22.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.22.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.22.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0y5y470l7chmmmv70l09imrk7g215laalgpl4n8a1xw7dngslnv9")))

(define-public crate-nu_plugin_fetch-0.23.0 (c (n "nu_plugin_fetch") (v "0.23.0") (d (list (d (n "base64") (r ">=0.12.3, <0.13.0") (d #t) (k 0)) (d (n "futures") (r ">=0.3.5, <0.4.0") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-plugin") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-protocol") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "nu-source") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "surf") (r ">=1.0.3, <2.0.0") (d #t) (k 0)) (d (n "url") (r ">=2.1.1, <3.0.0") (d #t) (k 0)))) (h "0wl40af0r4lc96cipk68d9dp8fqrjgijscr27h0grq5nyh30x2za")))

(define-public crate-nu_plugin_fetch-0.24.0 (c (n "nu_plugin_fetch") (v "0.24.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.24.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.24.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0yn2hfs2rla9ysfqdrnk2mrs2cv52hgza83lrpa33ndpb0r26zkg")))

(define-public crate-nu_plugin_fetch-0.24.1 (c (n "nu_plugin_fetch") (v "0.24.1") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.24.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.24.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1x7ysyj7myg93dlk93144ayidhmrjgnzlsg29s5i1pap22ms03mp")))

(define-public crate-nu_plugin_fetch-0.25.0 (c (n "nu_plugin_fetch") (v "0.25.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0rhqj70sk7q2s7ckaz6wjgn5rm51jsl1s07j2q6yi9vgp3clzqza")))

(define-public crate-nu_plugin_fetch-0.25.1 (c (n "nu_plugin_fetch") (v "0.25.1") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.25.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.25.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0pzz6vwchl3h5j7isyn2la7v5r31gzvxaz27720nswz06hfpp7y9")))

(define-public crate-nu_plugin_fetch-0.26.0 (c (n "nu_plugin_fetch") (v "0.26.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.26.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0f61f3n5wdzqwfi94ym8j60sd1wah1z1gdyhyg60fbyygd9scm4r")))

(define-public crate-nu_plugin_fetch-0.27.0 (c (n "nu_plugin_fetch") (v "0.27.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0cyn462vpb1njhcdmad63c672xc3si2l4kvgbjf7z57zsg4ldf1i")))

(define-public crate-nu_plugin_fetch-0.27.1 (c (n "nu_plugin_fetch") (v "0.27.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "nu-errors") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.27.1") (d #t) (k 0)) (d (n "nu-source") (r "^0.27.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "06mgd7vis7g3br3acswq24v4ffi15zq5fi0i7v65jz418vapvmzp")))

(define-public crate-nu_plugin_fetch-0.28.0 (c (n "nu_plugin_fetch") (v "0.28.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.28.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.28.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("h1-client-rustls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1v7pc58m8gi5zscvyviy8j5zbn30hxqlb37c0grgda7635vqhq31")))

(define-public crate-nu_plugin_fetch-0.29.0 (c (n "nu_plugin_fetch") (v "0.29.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.29.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.29.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "086z2a2fmi4v95kg6bgzp1ylilbbflxnf242vnkmw6ys5gs8g4dy")))

(define-public crate-nu_plugin_fetch-0.30.0 (c (n "nu_plugin_fetch") (v "0.30.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.30.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.30.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "06mlh5y51r4yi37xwic16w879qn54pcmnhsqaj45j1zj0gf6j54i")))

(define-public crate-nu_plugin_fetch-0.31.0 (c (n "nu_plugin_fetch") (v "0.31.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.31.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.31.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0w6739dznxympqv775621r5mgfi5f7i56s6irgpmfpzkvl4xvk53")))

(define-public crate-nu_plugin_fetch-0.32.0 (c (n "nu_plugin_fetch") (v "0.32.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.32.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.32.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0p2cmliwlz1wqr40276qvbdhgiq3dqwijhc3m30cdaarj2lg3s7b")))

(define-public crate-nu_plugin_fetch-0.33.0 (c (n "nu_plugin_fetch") (v "0.33.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.33.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.33.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "12vmavcgk3pimnhv3lqkvyfrxangy21xph89xpimlj4kmrywkf45")))

(define-public crate-nu_plugin_fetch-0.34.0 (c (n "nu_plugin_fetch") (v "0.34.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.34.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.34.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0cf4j9l74zsdbkh4g86jvn1db8s8rxwxmnqnazdcgvy8fgj8v2gh")))

(define-public crate-nu_plugin_fetch-0.35.0 (c (n "nu_plugin_fetch") (v "0.35.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.35.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.35.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1wsns5dh9727z1v88y1hcjw45h6bvrh0a4izx5p7b1ygfl0j8db5")))

(define-public crate-nu_plugin_fetch-0.36.0 (c (n "nu_plugin_fetch") (v "0.36.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "nu-errors") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.36.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.36.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("hyper-client"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1zi86kcsj7ys103w9q9f59p7nmjpssbdrkmj6dh60n2nbwcc9i49")))

