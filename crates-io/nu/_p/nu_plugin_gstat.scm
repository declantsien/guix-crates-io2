(define-module (crates-io nu _p nu_plugin_gstat) #:use-module (crates-io))

(define-public crate-nu_plugin_gstat-0.60.0 (c (n "nu_plugin_gstat") (v "0.60.0") (d (list (d (n "git2") (r "^0.13.24") (d #t) (k 0)) (d (n "nu-engine") (r "^0.60.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.60.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.60.0") (d #t) (k 0)))) (h "07l0r0fccn013j05v62717pyci7jmf8n6kzk6rkgq4a2d2608p3y")))

(define-public crate-nu_plugin_gstat-0.61.0 (c (n "nu_plugin_gstat") (v "0.61.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.61.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.61.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.61.0") (d #t) (k 0)))) (h "0anbsxzdsld86dikc5kc2kzhdfrz9drz9xw7blpgvbg4gfhcnjxm")))

(define-public crate-nu_plugin_gstat-0.62.0 (c (n "nu_plugin_gstat") (v "0.62.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.62.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.62.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.62.0") (d #t) (k 0)))) (h "1ndirr398xp88lhblg9swxmk48hr7q7h3h0wr62wn8iawjg0jasd")))

(define-public crate-nu_plugin_gstat-0.63.0 (c (n "nu_plugin_gstat") (v "0.63.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.63.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.63.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.63.0") (d #t) (k 0)))) (h "1m5xssnxrkgwi9hq4vqrcr782fbb7jqmvbhahf63l9mww633njm5")))

(define-public crate-nu_plugin_gstat-0.64.0 (c (n "nu_plugin_gstat") (v "0.64.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.64.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.64.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.64.0") (d #t) (k 0)))) (h "044y25vi8yc9aw784l6wxgh4v83igz078nsk4nnin1p3f4r3vn9z")))

(define-public crate-nu_plugin_gstat-0.65.0 (c (n "nu_plugin_gstat") (v "0.65.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.65.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.65.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.65.0") (d #t) (k 0)))) (h "0s0g07s8ha05ab1zdlgmgnf8mghj9ynajbmi74s2vi18zi34na8j")))

(define-public crate-nu_plugin_gstat-0.66.0 (c (n "nu_plugin_gstat") (v "0.66.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.66.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.66.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.0") (d #t) (k 0)))) (h "0i92s18r1dvvcnfy89saaf3wqx69gr238mkka4yk8zcbmhgsa98f")))

(define-public crate-nu_plugin_gstat-0.66.1 (c (n "nu_plugin_gstat") (v "0.66.1") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.66.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.66.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.1") (d #t) (k 0)))) (h "0zv21ld559jb7wzmld1p1cqwglcq99n57rqnipr3ysv72h65ynyb")))

(define-public crate-nu_plugin_gstat-0.66.2 (c (n "nu_plugin_gstat") (v "0.66.2") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "nu-engine") (r "^0.66.2") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.66.2") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.66.2") (d #t) (k 0)))) (h "0qk2bgrianbpa7fnqd6rpvq6jnk9m2lrnm5ac4j0qsmrxcpflgs4")))

(define-public crate-nu_plugin_gstat-0.67.0 (c (n "nu_plugin_gstat") (v "0.67.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.67.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.67.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.67.0") (d #t) (k 0)))) (h "0v5w3gr0y2nc41f9l9dwqwji66hqidy0j83l42kp51dlah2zz67w")))

(define-public crate-nu_plugin_gstat-0.68.0 (c (n "nu_plugin_gstat") (v "0.68.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.68.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.68.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.68.0") (d #t) (k 0)))) (h "062bym8zw4m5zgjji7bw61vllzqq4n4yz6dp18ds6pjha835yy1m")))

(define-public crate-nu_plugin_gstat-0.68.1 (c (n "nu_plugin_gstat") (v "0.68.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.68.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.68.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.68.1") (d #t) (k 0)))) (h "05z7qr2jp8vxmhdp41s4k0p7cc83rlvyh087g6ynnf30d35jkqk0")))

(define-public crate-nu_plugin_gstat-0.69.0 (c (n "nu_plugin_gstat") (v "0.69.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.69.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.69.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.69.0") (d #t) (k 0)))) (h "048r9zkhvlvdy6g6dlzbxj6gr52b5b67jkbvba6y5ksisaknbh5d")))

(define-public crate-nu_plugin_gstat-0.69.1 (c (n "nu_plugin_gstat") (v "0.69.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.69.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.69.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.69.1") (d #t) (k 0)))) (h "0niadi6jvnfh074imysz3dwx1cc8ghb0k1ii5sf2qj8r0b0jmckx")))

(define-public crate-nu_plugin_gstat-0.70.0 (c (n "nu_plugin_gstat") (v "0.70.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.70.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.70.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.70.0") (d #t) (k 0)))) (h "08m3fwd2yhs8sz0pwdm0a083b1ggyw2vvhkdg2f7xr44jk0p3hk3")))

(define-public crate-nu_plugin_gstat-0.71.0 (c (n "nu_plugin_gstat") (v "0.71.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.71.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.71.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.71.0") (d #t) (k 0)))) (h "09fj2isn20133vfbhlpfb1q91ih0hqs40dsx00r81cnjy0szyaxp")))

(define-public crate-nu_plugin_gstat-0.72.0 (c (n "nu_plugin_gstat") (v "0.72.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.72.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.72.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.72.0") (d #t) (k 0)))) (h "1asd9399rk875mkgb7kjcbbm65wdq2mi6lnnilx4hwbzincj3s3v")))

(define-public crate-nu_plugin_gstat-0.73.0 (c (n "nu_plugin_gstat") (v "0.73.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.73.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.73.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.73.0") (d #t) (k 0)))) (h "1wdlrffi22a7vmfmbw9wd1qgylzbial7cny3gls3z4jzx7zp19si")))

(define-public crate-nu_plugin_gstat-0.74.0 (c (n "nu_plugin_gstat") (v "0.74.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.74.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.74.0") (d #t) (k 0)))) (h "01jr9fxvb7ijyyj70jy8x1k32fdp7x0f9whrgn7cffvwanlwz577")))

(define-public crate-nu_plugin_gstat-0.75.0 (c (n "nu_plugin_gstat") (v "0.75.0") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "nu-engine") (r "^0.75.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.75.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.75.0") (d #t) (k 0)))) (h "10bg2hbxm7h4qf7wvxr5fnq3isa3s0ppasffvl53lva2sbzdw702")))

(define-public crate-nu_plugin_gstat-0.76.0 (c (n "nu_plugin_gstat") (v "0.76.0") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "nu-engine") (r "^0.76.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.76.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.76.0") (d #t) (k 0)))) (h "07kyq91qznkgabh9bgsn11bcfmlwpi8zxkp012qwxlqpzwr0cd7b")))

(define-public crate-nu_plugin_gstat-0.77.0 (c (n "nu_plugin_gstat") (v "0.77.0") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "nu-engine") (r "^0.77.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.77.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.77.0") (d #t) (k 0)))) (h "0pbn3b79qwf7y5gqcpq85i0zkxx21zh4n61856b55jsaz99a7j8g")))

(define-public crate-nu_plugin_gstat-0.77.1 (c (n "nu_plugin_gstat") (v "0.77.1") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "nu-engine") (r "^0.77.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.77.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.77.1") (d #t) (k 0)))) (h "0sfmwd6d1dja3g1wh4ncf4s26kqbl803ld51f107s3vhwwqrfpdk")))

(define-public crate-nu_plugin_gstat-0.78.0 (c (n "nu_plugin_gstat") (v "0.78.0") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "nu-engine") (r "^0.78.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.78.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.78.0") (d #t) (k 0)))) (h "1zyj7w2dd1s63650hbinnndi3014brsk7vkal810bfj986jh3cdh")))

(define-public crate-nu_plugin_gstat-0.79.0 (c (n "nu_plugin_gstat") (v "0.79.0") (d (list (d (n "git2") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.79.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.79.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.79.0") (d #t) (k 0)))) (h "1d1q7y941hm9377gj3v677s198fm6vzqn2x65x2skfbnphmm1ixn")))

(define-public crate-nu_plugin_gstat-0.80.0 (c (n "nu_plugin_gstat") (v "0.80.0") (d (list (d (n "git2") (r "^0.17.0") (d #t) (k 0)) (d (n "nu-engine") (r "^0.80.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.80.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.80.0") (d #t) (k 0)))) (h "06s8gnn4qmshv8310bsj5lh6ny4m5kqnb74nrqszfwd0z3rgld2g")))

(define-public crate-nu_plugin_gstat-0.81.0 (c (n "nu_plugin_gstat") (v "0.81.0") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.81.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.81.0") (d #t) (k 0)))) (h "17xpk8wsbj5j22flmn3cpf9qzqilb1l2aijisl5ndcqlzilj3v2w")))

(define-public crate-nu_plugin_gstat-0.82.0 (c (n "nu_plugin_gstat") (v "0.82.0") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.82.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.82.0") (d #t) (k 0)))) (h "0ffl6qwdlf05k2vxvfa3yzwlxzhmxspzx97pi55ssbksy6897a22")))

(define-public crate-nu_plugin_gstat-0.83.0 (c (n "nu_plugin_gstat") (v "0.83.0") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.0") (d #t) (k 0)))) (h "14rk3cqqajvcp05d50n2mmxzyipfr2fxrs6j6s2lgh65djn6ypbs")))

(define-public crate-nu_plugin_gstat-0.83.1 (c (n "nu_plugin_gstat") (v "0.83.1") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.83.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.83.1") (d #t) (k 0)))) (h "1fszljq4rjnvk0rrry32hmpkpx4gdg08fhk4rvma9lc9dyfjplzb")))

(define-public crate-nu_plugin_gstat-0.84.0 (c (n "nu_plugin_gstat") (v "0.84.0") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.84.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.84.0") (d #t) (k 0)))) (h "1bqav6h6wkyam0pz49mb34nfpyfi6b301qhl40xrljrd1a0nkgkx")))

(define-public crate-nu_plugin_gstat-0.85.0 (c (n "nu_plugin_gstat") (v "0.85.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (d #t) (k 0)))) (h "1k4sw5sis6bs4ryj53299ffw5xk7n8kz0pc1v1kbvwhi2792f5x7")))

(define-public crate-nu_plugin_gstat-0.86.0 (c (n "nu_plugin_gstat") (v "0.86.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (d #t) (k 0)))) (h "0pjpisq81j6aj1aq3cr2zv7i8j16b2cy0iwycgigfiliacdjhlx7")))

(define-public crate-nu_plugin_gstat-0.87.0 (c (n "nu_plugin_gstat") (v "0.87.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (d #t) (k 0)))) (h "0xkypam4m6y8bcyx4yjqnp323hfni1xm79f3k3k52hw4zgvw52kp")))

(define-public crate-nu_plugin_gstat-0.87.1 (c (n "nu_plugin_gstat") (v "0.87.1") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (d #t) (k 0)))) (h "0wwd8689dvwy9vhflf6vqkjc3z8sh8av192j8abz4mwvjqvrrzxk")))

(define-public crate-nu_plugin_gstat-0.88.0 (c (n "nu_plugin_gstat") (v "0.88.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.88.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.0") (d #t) (k 0)))) (h "0ivqxvkd2q3jdl2nqnklmffsj4vsbw45vbhpfz9n4g870l5ix7sb")))

(define-public crate-nu_plugin_gstat-0.88.1 (c (n "nu_plugin_gstat") (v "0.88.1") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.88.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.88.1") (d #t) (k 0)))) (h "0bypssqnkmaxj545jna5b628wpill9ysiv6jjg6zccbc73pvwxz6")))

(define-public crate-nu_plugin_gstat-0.89.0 (c (n "nu_plugin_gstat") (v "0.89.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.89.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.89.0") (d #t) (k 0)))) (h "0cr4qdyq56binhc64hm58pkqpmfq61b3s1y04s47afwjzfms8lp1")))

(define-public crate-nu_plugin_gstat-0.90.1 (c (n "nu_plugin_gstat") (v "0.90.1") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (d #t) (k 0)))) (h "1h645wsf5nla20zzkl06870k5s5j5qnmixg3rvy5mnnbk7ygn5cr")))

(define-public crate-nu_plugin_gstat-0.91.0 (c (n "nu_plugin_gstat") (v "0.91.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.91.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.91.0") (d #t) (k 0)))) (h "10jmrbyrs8r9k3m68c902laz0scjfzzdg3pj9cxc5p88m61qs603")))

(define-public crate-nu_plugin_gstat-0.92.0 (c (n "nu_plugin_gstat") (v "0.92.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.0") (d #t) (k 0)))) (h "1xibkhslpw9fwbn2jkklhgrzhhc46wdrzshhb73az1lpb35g4kif")))

(define-public crate-nu_plugin_gstat-0.92.1 (c (n "nu_plugin_gstat") (v "0.92.1") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.1") (d #t) (k 0)))) (h "0971gnldk4xwh8bz635hrak80vpsnd0wqm8vpghbzfip6yqjhrx3")))

(define-public crate-nu_plugin_gstat-0.92.2 (c (n "nu_plugin_gstat") (v "0.92.2") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.92.2") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.92.2") (d #t) (k 0)))) (h "09gwk56s17r2cy1bdmnn2cy2zr388w05xx4wn5xhy81s0bxs030v")))

(define-public crate-nu_plugin_gstat-0.93.0 (c (n "nu_plugin_gstat") (v "0.93.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)))) (h "1c8b4aa3shszllbk2c1i3g9bvd6k6rldzxwldbw9aygragr7bqdi")))

(define-public crate-nu_plugin_gstat-0.94.0 (c (n "nu_plugin_gstat") (v "0.94.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (d #t) (k 0)))) (h "0nwx6njwx6jcb9vgjc4g2ynrjv8a2vkqcxrzqirnnlbxd8y3i59y")))

