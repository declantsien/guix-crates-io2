(define-module (crates-io nu _p nu_plugin_id3) #:use-module (crates-io))

(define-public crate-nu_plugin_id3-0.1.0 (c (n "nu_plugin_id3") (v "0.1.0") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "nu-errors") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.10.0") (d #t) (k 0)))) (h "1493cxmwcygxf9l0qagmrmxy9grpfx36n1rakma0qpg9c59jh3j6")))

(define-public crate-nu_plugin_id3-0.1.1 (c (n "nu_plugin_id3") (v "0.1.1") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "nu-errors") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.16.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.16.0") (d #t) (k 0)))) (h "0wz6d1nkfq1pvjq1kyr2mfai02s6r1swamh1nmm4rqhmvgvdpwlx")))

(define-public crate-nu_plugin_id3-0.2.0 (c (n "nu_plugin_id3") (v "0.2.0") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "nu-errors") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.18.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03cws6i3lblb3nv8djxdly1x9gr4yb7nh17akyiw1px9jlqcpa2g")))

(define-public crate-nu_plugin_id3-0.3.0 (c (n "nu_plugin_id3") (v "0.3.0") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "nu-errors") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.26.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1iriac7bapsw9yshbf7zri15f10x4adx0q172bw0nlwk301agb6n")))

