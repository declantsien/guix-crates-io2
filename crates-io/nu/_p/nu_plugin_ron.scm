(define-module (crates-io nu _p nu_plugin_ron) #:use-module (crates-io))

(define-public crate-nu_plugin_ron-0.1.0 (c (n "nu_plugin_ron") (v "0.1.0") (d (list (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1ky79bvgjfr62blzk0x5bkvsvsnv4iaj7hznq6h4csrwpsac88hh")))

(define-public crate-nu_plugin_ron-0.1.1 (c (n "nu_plugin_ron") (v "0.1.1") (d (list (d (n "nu-plugin") (r "^0.85.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.85.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0dm12a2ca2cam29xqc7q3wx9al41wwzghk7drza5n22hwfmd7yv5")))

