(define-module (crates-io nu _p nu_plugin_port_list) #:use-module (crates-io))

(define-public crate-nu_plugin_port_list-1.0.0 (c (n "nu_plugin_port_list") (v "1.0.0") (d (list (d (n "netstat") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "118likgnfp8vf85vrqp0zz8nbj5xww05z1l8rcw189rf93d1kkqi")))

(define-public crate-nu_plugin_port_list-1.0.1 (c (n "nu_plugin_port_list") (v "1.0.1") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1hgpccdfibp2lk1ijdr3h3c3z2myss7dppdz5n6n5709y6gbn2yy")))

(define-public crate-nu_plugin_port_list-1.0.2 (c (n "nu_plugin_port_list") (v "1.0.2") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0nazp4dicw8x5n688hkhq1pfnwmz85wj4mqm1iilxmma3cmqz0xr")))

(define-public crate-nu_plugin_port_list-1.1.0 (c (n "nu_plugin_port_list") (v "1.1.0") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0vcsapds1qm44wyqa1g49bxs260bhq6y4d1f93di5mnwc2hs4csh")))

(define-public crate-nu_plugin_port_list-1.1.1 (c (n "nu_plugin_port_list") (v "1.1.1") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "1m7yn1rxb75wv48jxhfsc284a4p1rb599am8zp5c4afjhpx8bzq7")))

(define-public crate-nu_plugin_port_list-1.2.1 (c (n "nu_plugin_port_list") (v "1.2.1") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "1bps87da9znh88zzy7mrwz0acg7gdvry5yylx46i36q3pznq4k0l")))

(define-public crate-nu_plugin_port_list-1.2.2 (c (n "nu_plugin_port_list") (v "1.2.2") (d (list (d (n "netstat2") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0pxx0l2r72a99zim6dblmqavydhsckpy6axar8hp69ivpka9b0hi")))

(define-public crate-nu_plugin_port_list-1.2.10 (c (n "nu_plugin_port_list") (v "1.2.10") (d (list (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "04mm2a4pz8x466my6sgv43565kpkzn9qrm2sdhmxddkq9xbxgqfy")))

