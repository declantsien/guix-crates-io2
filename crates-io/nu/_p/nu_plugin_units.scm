(define-module (crates-io nu _p nu_plugin_units) #:use-module (crates-io))

(define-public crate-nu_plugin_units-0.1.0 (c (n "nu_plugin_units") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.13") (d #t) (k 0)))) (h "1hnpxvycmxv4pq2m0is4wjjfw1xlvci7d690sb2k6cya7kz5z3yx")))

