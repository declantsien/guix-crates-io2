(define-module (crates-io nu _p nu_plugin_sum) #:use-module (crates-io))

(define-public crate-nu_plugin_sum-0.7.0 (c (n "nu_plugin_sum") (v "0.7.0") (d (list (d (n "nu-build") (r "^0.7.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.7.0") (d #t) (k 0)))) (h "1a9cnnsr3wjgfy9c68mqs8rxnq96664k28q8alqxappx9ppx414y")))

(define-public crate-nu_plugin_sum-0.8.0 (c (n "nu_plugin_sum") (v "0.8.0") (d (list (d (n "nu-build") (r "^0.8.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.8.0") (d #t) (k 0)))) (h "0vajamyyv4s9ppnin2lp8d5q5kg8mhzya1gdbiv88ghmrs8d3rn7")))

(define-public crate-nu_plugin_sum-0.9.0 (c (n "nu_plugin_sum") (v "0.9.0") (d (list (d (n "nu-build") (r "^0.9.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.9.0") (d #t) (k 0)))) (h "1s5xc0f0fp6pqg6j90q6zdlh0zc255xyf6yl4g3plrzslc5v498n")))

(define-public crate-nu_plugin_sum-0.10.0 (c (n "nu_plugin_sum") (v "0.10.0") (d (list (d (n "nu-build") (r "^0.10.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.10.0") (d #t) (k 0)))) (h "1splk3p71gd18hl5x10pf3lhylwa3i35miaam2fjh45h82wfz0mr")))

(define-public crate-nu_plugin_sum-0.11.0 (c (n "nu_plugin_sum") (v "0.11.0") (d (list (d (n "nu-build") (r "^0.11.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.11.0") (d #t) (k 0)))) (h "0a0mzd2k7rmhwgzivqc3544arvmg1mvrrky7wws1hvwiybl0w1bq")))

