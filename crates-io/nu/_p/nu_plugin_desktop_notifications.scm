(define-module (crates-io nu _p nu_plugin_desktop_notifications) #:use-module (crates-io))

(define-public crate-nu_plugin_desktop_notifications-0.1.0 (c (n "nu_plugin_desktop_notifications") (v "0.1.0") (d (list (d (n "notify-rust") (r "^4.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1izj9fzvmgx6havvxxk3n2cchbqgrqg7qa8l7my4dzzcbjdb1i96")))

(define-public crate-nu_plugin_desktop_notifications-0.1.1 (c (n "nu_plugin_desktop_notifications") (v "0.1.1") (d (list (d (n "notify-rust") (r "^4.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1qyndwq8rdb0a7jspwbsw6cp2w7dqrdjhf87wyk90krz7vqaj1p4")))

(define-public crate-nu_plugin_desktop_notifications-1.0.0 (c (n "nu_plugin_desktop_notifications") (v "1.0.0") (d (list (d (n "notify-rust") (r "^4.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1yplmxq1y474jarfsr1145wdrb6g23g74b2gclcxryr2lgzdswhm")))

(define-public crate-nu_plugin_desktop_notifications-1.0.1 (c (n "nu_plugin_desktop_notifications") (v "1.0.1") (d (list (d (n "notify-rust") (r "^4.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0g5b3vamdi86isari9xrxgh8y2lm9s9wz4rsnw3l4jwpm5d7w9dh")))

(define-public crate-nu_plugin_desktop_notifications-1.0.8 (c (n "nu_plugin_desktop_notifications") (v "1.0.8") (d (list (d (n "notify-rust") (r "^4.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)))) (h "091y9gw1hpwilbi3h66qnla3230cjawyz4b4v0285bmlrqls5agq")))

