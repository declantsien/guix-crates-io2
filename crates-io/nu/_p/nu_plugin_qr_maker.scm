(define-module (crates-io nu _p nu_plugin_qr_maker) #:use-module (crates-io))

(define-public crate-nu_plugin_qr_maker-1.0.0 (c (n "nu_plugin_qr_maker") (v "1.0.0") (d (list (d (n "nu-plugin") (r "^0.87.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "0j4i8fq734c16czz5i17y78aw2kpak7x4q0w3g4ha6ia47m411y5")))

(define-public crate-nu_plugin_qr_maker-1.0.3 (c (n "nu_plugin_qr_maker") (v "1.0.3") (d (list (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "1d793m5g0rx8jzkhy9mpc1xllb5xfz6j2p018sjr47b9gfclb930")))

