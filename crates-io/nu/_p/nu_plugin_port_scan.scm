(define-module (crates-io nu _p nu_plugin_port_scan) #:use-module (crates-io))

(define-public crate-nu_plugin_port_scan-1.0.0 (c (n "nu_plugin_port_scan") (v "1.0.0") (d (list (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0j60hsaqhg0xcvyb9q123v39mvfbp752z3qir5dvg0ab8s7hq4jj")))

(define-public crate-nu_plugin_port_scan-1.0.1 (c (n "nu_plugin_port_scan") (v "1.0.1") (d (list (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0amqbs7v0ys1sra9k7x0a5ffx8y1lqk6za2jlnhcs94wfrnsf2km")))

(define-public crate-nu_plugin_port_scan-1.0.2 (c (n "nu_plugin_port_scan") (v "1.0.2") (d (list (d (n "nu-plugin") (r "^0.86.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.86.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "18p5jkwb5nzcy47cnp88fq6njjyk4q6pzhk1rw94pv5vrmd5105i")))

(define-public crate-nu_plugin_port_scan-1.0.3 (c (n "nu_plugin_port_scan") (v "1.0.3") (d (list (d (n "nu-plugin") (r "^0.87.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.87.0") (f (quote ("plugin"))) (d #t) (k 0)))) (h "1xasmzssif99kzfsvcywrra24shphqgm5wbdq58kzf2jp35fnph2")))

(define-public crate-nu_plugin_port_scan-1.0.10 (c (n "nu_plugin_port_scan") (v "1.0.10") (d (list (d (n "nu-plugin") (r "^0.90.1") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.90.1") (f (quote ("plugin"))) (d #t) (k 0)))) (h "0h8rpm0mdccj3d88qlbif3ha26nc54zc9mkfh2af1zi39p4iw1s0")))

