(define-module (crates-io nu _p nu_plugin_average) #:use-module (crates-io))

(define-public crate-nu_plugin_average-0.7.0 (c (n "nu_plugin_average") (v "0.7.0") (d (list (d (n "nu-build") (r "^0.7.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.7.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.7.0") (d #t) (k 0)))) (h "0fnxb5xh6vvvc34wdmfrkqs5asqbndapdrf1k45hs52cmln6sjh0")))

(define-public crate-nu_plugin_average-0.8.0 (c (n "nu_plugin_average") (v "0.8.0") (d (list (d (n "nu-build") (r "^0.8.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.8.0") (d #t) (k 0)))) (h "1jk1fmnbifq1m6ww1qk6jmgjzm3zky0yw04fwp7jr60l2warxn03")))

(define-public crate-nu_plugin_average-0.9.0 (c (n "nu_plugin_average") (v "0.9.0") (d (list (d (n "nu-build") (r "^0.9.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.9.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.9.0") (d #t) (k 0)))) (h "0r1xl5igvjjjwb5dbxzf93yk8jbjw0acj64y6mysvfj93wg5kp1s")))

(define-public crate-nu_plugin_average-0.10.0 (c (n "nu_plugin_average") (v "0.10.0") (d (list (d (n "nu-build") (r "^0.10.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.10.0") (d #t) (k 0)))) (h "0jbn5jby24prjz82vphighj0zc0dx39jdri4sa4qr6w8g7gv8bxk")))

(define-public crate-nu_plugin_average-0.11.0 (c (n "nu_plugin_average") (v "0.11.0") (d (list (d (n "nu-build") (r "^0.11.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.11.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.11.0") (d #t) (k 0)))) (h "1an4946hqlny5gqpphx2yf3h3ycxnlwqzdrykadcbdlym0a99vjb")))

(define-public crate-nu_plugin_average-0.12.0 (c (n "nu_plugin_average") (v "0.12.0") (d (list (d (n "nu-build") (r "^0.12.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.12.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.12.0") (d #t) (k 0)))) (h "1icvs7q45bl64i53yps7pcbsiq8i80n89c2bd0mxab83cia5g61a")))

(define-public crate-nu_plugin_average-0.13.0 (c (n "nu_plugin_average") (v "0.13.0") (d (list (d (n "nu-build") (r "^0.13.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.13.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.13.0") (d #t) (k 0)))) (h "122d93wgaq6a2jagca7xgli24g917xp1bx0qij87g2ikl6f9l8dy")))

(define-public crate-nu_plugin_average-0.14.0 (c (n "nu_plugin_average") (v "0.14.0") (d (list (d (n "nu-build") (r "^0.14.0") (d #t) (k 1)) (d (n "nu-errors") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.14.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.14.0") (d #t) (k 0)))) (h "14iiamaad4sbmx9csfwqnvbf2n5m939nlzn7k95198x4a02jzahj")))

