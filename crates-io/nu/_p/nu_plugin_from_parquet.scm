(define-module (crates-io nu _p nu_plugin_from_parquet) #:use-module (crates-io))

(define-public crate-nu_plugin_from_parquet-0.0.0 (c (n "nu_plugin_from_parquet") (v "0.0.0") (d (list (d (n "bigdecimal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "nu-errors") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-plugin") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-source") (r "^0.26.0") (d #t) (k 0)) (d (n "nu-value-ext") (r "^0.26.0") (d #t) (k 0)) (d (n "parquet") (r "^3.0.0") (d #t) (k 0)))) (h "1qd6hvzfw461m4ad9qh56k2kamkwxnl10v544bh94xq7r1526scd")))

