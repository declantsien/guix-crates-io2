(define-module (crates-io nu s3 nus3audio) #:use-module (crates-io))

(define-public crate-nus3audio-1.0.0 (c (n "nus3audio") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "03z0frzcw3axnlkz446z99na2h8z71xg3rccxc57jsrjsh6pzva9")))

(define-public crate-nus3audio-1.0.1 (c (n "nus3audio") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0gfy9imhmpy2f7m9vwmham5217p4iiqmccj8vji5il9r5qbgkfbj")))

(define-public crate-nus3audio-1.0.2 (c (n "nus3audio") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0vnzxzp302wq725p10gj0yzmxblyil10fzjsd9hkb9dbmpjwpal3")))

(define-public crate-nus3audio-1.0.3 (c (n "nus3audio") (v "1.0.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0j7lpsn52abnmzhnx35dxvw8l014gcgagb0hqivlns1biyglba8j")))

(define-public crate-nus3audio-1.0.4 (c (n "nus3audio") (v "1.0.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1q3j4vgcfwjqhf256nbrnlclg18rkyavnhhy6pndfvwhlgkw9l7a")))

(define-public crate-nus3audio-1.0.5 (c (n "nus3audio") (v "1.0.5") (d (list (d (n "binwrite") (r ">= 0.1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0p84598hhyj0rv8g02xkar084bwdq51bx1pwm3m502klb9r9g7lx")))

(define-public crate-nus3audio-1.0.6 (c (n "nus3audio") (v "1.0.6") (d (list (d (n "binwrite") (r ">= 0.1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m1dfmbcpllfkln442y7b94k2jlvzy2swk8pvipv6iw0zxll6vk0")))

(define-public crate-nus3audio-1.0.7 (c (n "nus3audio") (v "1.0.7") (d (list (d (n "binwrite") (r ">= 0.1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rngp0z32yxvnfb3r0b0h9viswwgpgd2lkb5n83v0s34xgzn4q57")))

(define-public crate-nus3audio-1.1.0 (c (n "nus3audio") (v "1.1.0") (d (list (d (n "binwrite") (r ">=0.1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z208qwaysqzdmrgksnsqw694h1mvmdzcdyl3idb2xsrc80kc2y2")))

(define-public crate-nus3audio-1.2.0 (c (n "nus3audio") (v "1.2.0") (d (list (d (n "binwrite") (r ">=0.1.3") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mrhvfhdb8k4r3jgdifwyaw73rd5wkx076gligxmik696fyh0cyx")))

