(define-module (crates-io nu ll nullable-result) #:use-module (crates-io))

(define-public crate-nullable-result-0.1.0 (c (n "nullable-result") (v "0.1.0") (h "1ybhin59c4h24mwjrlyr5ng5knfd0k56x4s02ypqrbp7kf22rjiv") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.2.0 (c (n "nullable-result") (v "0.2.0") (h "0gpp2sigvl4a3pxqj0kmjsrmff3i0102rm5x7axwn67q0f28f2ar") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.3.0 (c (n "nullable-result") (v "0.3.0") (h "1vn1gx07pdvd0fg87hdlinyrca8kl844qz9wxw2zhm4l437rvf6n") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.4.0 (c (n "nullable-result") (v "0.4.0") (h "1llx0n7agy74rhrnwm5w850fssjlqxgppi9li0j253x98nhhjl2c") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.5.0 (c (n "nullable-result") (v "0.5.0") (h "0zs7bik1hpkwj8v4wlpqpbi84babgmn5dnl2qh8fbrc5314b4bdj") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.6.0 (c (n "nullable-result") (v "0.6.0") (h "1b2flx39maiq6m77fn2xlfx4vxzap76cbjdh3c16b71pf7p03zb9") (f (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.7.0 (c (n "nullable-result") (v "0.7.0") (h "16dr8vlya6xsc86m7dlsr6j7jd1v68bxds7g1d9dm7by98srwp35") (f (quote (("try_trait" "__unstable") ("std") ("default" "std") ("__unstable"))))))

