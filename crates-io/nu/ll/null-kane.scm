(define-module (crates-io nu ll null-kane) #:use-module (crates-io))

(define-public crate-null-kane-0.1.0 (c (n "null-kane") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jgmdxcv8xjfmagbdjj6zph9j3z0x5wb0ls3ry6jbjr5vzxl6php") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1.1 (c (n "null-kane") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "063lqmmqvacszy04wm4csqkfr8d9n6pd6jn4mgpnqjsixyi3qzym") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1.2 (c (n "null-kane") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00jg2kidyjhyy365gd5aydw8p093y692gzn4d63d35b13v9d07rm") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1.3 (c (n "null-kane") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rlwr1qx1axpgwag8c2s3b1c2zfnghcngz2ym9j6lnl4z683qnl3") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1.4 (c (n "null-kane") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jz3q3djh8iaclnhrvvfqy7vfwkg2b7imfyf2i2j2g9vg8q77f57") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.2.0 (c (n "null-kane") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hxd13dia1spkih1hg8nvxqv230f4jk7ni3jnwcszb8c3wq79plw") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.2.1 (c (n "null-kane") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08w3ljhxrhzh95ashna1hbgzap25nlnb21xp3s4hncdidrprb2ia") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.3.0 (c (n "null-kane") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zjlf8ygqn6qiirmbf1bwnibzg190b8my448gc6r0aqw8arpbwx0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1.0.0 (c (n "null-kane") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rfbcvmyhw3v8ka2likx9gp0pvpypgnw0g9aklxsjhacvz6l1svy") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1.0.1 (c (n "null-kane") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zl3l64izmmfpg1gl8k59f5iwmlh4dj2f9linniqr46am2v8mf4m") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1.0.2 (c (n "null-kane") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fimg0pi5vyijphan8gbvpag9q17m31q20fr61hsxkrbb96fxp44") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2.0.0 (c (n "null-kane") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c0yjwsxm3jx8hjgvj5if24nxka9ifnzvzjkbg5mga96l831v0fh") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2.0.1 (c (n "null-kane") (v "2.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xwj518ckl66m5fljxkh7n3vv4yzpcj6r97v9jcwck4gjyw166i1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2.0.2 (c (n "null-kane") (v "2.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05qj7v4zi8gzssfa6dfg9m3yr73lnc4cgry7z7ksxyifmx5rbpr0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2.0.3 (c (n "null-kane") (v "2.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n4anh7rcvpxaw1y5fzdiv7r3qahvw07sk57x7kibvyphx6gdyq5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2.0.4 (c (n "null-kane") (v "2.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0is5swmh1yaij5hh9kiswmdknbg8wjmbc2zwrpk0sivr1c96lyi7") (s 2) (e (quote (("serde" "dep:serde"))))))

