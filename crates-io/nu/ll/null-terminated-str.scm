(define-module (crates-io nu ll null-terminated-str) #:use-module (crates-io))

(define-public crate-null-terminated-str-0.1.0 (c (n "null-terminated-str") (v "0.1.0") (h "0c9prw94wsc2aa740snp90phvps4jjwykd1l8z47x39dhqfv4svl") (r "1.63")))

(define-public crate-null-terminated-str-0.1.1 (c (n "null-terminated-str") (v "0.1.1") (h "0zsddq2rm3plq70xjbfggwfzm7hqr1y29j7dq7qbmhajqdbqj41f") (r "1.63")))

(define-public crate-null-terminated-str-0.1.2 (c (n "null-terminated-str") (v "0.1.2") (h "0n4pxq57zawfqir6w49i4x5ql0m8zwdbl9r0kchylcxp37fh2jyi") (r "1.63")))

(define-public crate-null-terminated-str-0.1.3 (c (n "null-terminated-str") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "11iccixc9vlp0vz47q74lm9d18wg9pw7qx6dcc8p51iam4ap2928") (r "1.63")))

(define-public crate-null-terminated-str-0.1.4 (c (n "null-terminated-str") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "1gahyaf26z2mb3gy3700nkn52n19v4g2m7gz4k7rpvw9nncc0kjr") (r "1.63")))

