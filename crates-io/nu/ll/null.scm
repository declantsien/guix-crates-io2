(define-module (crates-io nu ll null) #:use-module (crates-io))

(define-public crate-NULL-1.0.0 (c (n "NULL") (v "1.0.0") (d (list (d (n "num") (r "^0.1.36") (f (quote ("bigint"))) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)))) (h "06d9r7akkd11f2cvysfwpf2zw4a95vjzbs0jij27yjrrabnkn5m1")))

(define-public crate-NULL-1.0.1 (c (n "NULL") (v "1.0.1") (d (list (d (n "num-bigint") (r "^0.1.35") (k 0)) (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)))) (h "17kmfvlywkznf45bzvas6b1f400ai50fwgwxgrnp62nj9kr4lsg8")))

(define-public crate-NULL-1.1.0 (c (n "NULL") (v "1.1.0") (d (list (d (n "num-bigint") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "num-integer") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3.1") (d #t) (k 0)))) (h "1bhgjn4q7nxxp5b35v843p3i5z1bvr35fdh60cymy0f09qap6xiv")))

(define-public crate-NULL-1.2.0 (c (n "NULL") (v "1.2.0") (d (list (d (n "num-bigint") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "num-integer") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3.1") (d #t) (k 0)))) (h "1l1rlm6iz3s8xjj36qkl716pdpsrc92bcqib4b087y0l2j7r1kpc")))

