(define-module (crates-io nu ll null-pointer) #:use-module (crates-io))

(define-public crate-null-pointer-0.0.1 (c (n "null-pointer") (v "0.0.1") (h "0ikv392701svc0kaxpcw4ia9ijmmnz7qiz8qp1pyk1yg0y9k3swc") (y #t)))

(define-public crate-null-pointer-0.1.0 (c (n "null-pointer") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("std" "error-context" "help" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "0wx3l8z6qfbx6id711fwnrm6jdvhhi02lgkxamcz55za5d2n3f68")))

(define-public crate-null-pointer-0.1.1 (c (n "null-pointer") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("std" "error-context" "help" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "15nlgrvyz2wdc5glgzh2qhwa7ysfzipwval5q1m3bnkrrn5xj03z")))

(define-public crate-null-pointer-0.1.2 (c (n "null-pointer") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("std" "error-context" "help" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "0ia5p0g0jpqyljslcjqh0nn72nflnvyfc0y0ikgnva63g6ic0wmg") (y #t)))

(define-public crate-null-pointer-0.2.0 (c (n "null-pointer") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("std" "error-context" "help" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "18rbfr0hmsn1v0vh5pz3cricqxj8ix44knkyn9ng0bk04gwq3dyg")))

(define-public crate-null-pointer-0.2.1 (c (n "null-pointer") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.17") (f (quote ("std" "error-context" "help" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "10ak0zn11sm0flwg72igyhnjjb7mfmc5554anr7ncsbxg7q9kbk0")))

(define-public crate-null-pointer-0.2.2 (c (n "null-pointer") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("std" "error-context" "help" "usage" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "193ajdjr95bpxr2mi32ymc7k4sbgm38a2ps6abdzihrzrp8ncrh9")))

(define-public crate-null-pointer-0.2.3 (c (n "null-pointer") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("std" "error-context" "help" "usage" "derive"))) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https"))) (d #t) (k 0)))) (h "1qm3s7ashlj76265dizbkmzxmq6jqiklvx8g13m4li9h0y64rpza")))

