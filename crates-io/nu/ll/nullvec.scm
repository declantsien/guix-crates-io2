(define-module (crates-io nu ll nullvec) #:use-module (crates-io))

(define-public crate-nullvec-0.1.0 (c (n "nullvec") (v "0.1.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "1j4w5sckipv4ss8axjpxal81wp0hzsym9w13fy9f1sb2h1wz54n7")))

(define-public crate-nullvec-0.1.1 (c (n "nullvec") (v "0.1.1") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "01dz607msawzws817a92a45f4jif8jdxhs6577fnqyhzjslifa8f")))

(define-public crate-nullvec-0.1.2 (c (n "nullvec") (v "0.1.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0y2bl9zxvpwfnad0s25nyckgiacwgchvqqmpl9akg98p03xq360c")))

(define-public crate-nullvec-0.1.3 (c (n "nullvec") (v "0.1.3") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "18pslx0m2qrdf9y302c192xh0z28xvx8gcn5s0s1ymkz7mcwqqlv")))

(define-public crate-nullvec-0.1.4 (c (n "nullvec") (v "0.1.4") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "04fszm093vydys76pssf2cqxz5qbwa0wd6j64zdqmwx2klwfcg2w")))

(define-public crate-nullvec-0.1.5 (c (n "nullvec") (v "0.1.5") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0k19p08cc5rh2ii1m5krcpz63zx9wvcj20hr50pdd78hyv36qrmf")))

(define-public crate-nullvec-0.2.0 (c (n "nullvec") (v "0.2.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "1g3ymmwv441pzcyrrgkkr9is60qn7sq6bd8yr26dprfl95w4fr93")))

