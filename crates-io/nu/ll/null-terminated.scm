(define-module (crates-io nu ll null-terminated) #:use-module (crates-io))

(define-public crate-null-terminated-0.1.0 (c (n "null-terminated") (v "0.1.0") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3") (d #t) (k 0)))) (h "1kwp1wb9p87mfql26sldd3i1h0zgqnl05fp29v059bsfy6xhg97r")))

(define-public crate-null-terminated-0.2.0 (c (n "null-terminated") (v "0.2.0") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3") (d #t) (k 0)))) (h "1ny3lzcrivc9rb6qi9sqhladhalncjddvvsll9yixcs85mkn7d82")))

(define-public crate-null-terminated-0.2.1 (c (n "null-terminated") (v "0.2.1") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3.1") (d #t) (k 0)))) (h "1im9gwz3axlsgirj3sz6yb415dkrfi3vhbpdf4s2fzg4cg1fnz0n") (y #t)))

(define-public crate-null-terminated-0.2.2 (c (n "null-terminated") (v "0.2.2") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3.1") (d #t) (k 0)))) (h "0c801agxkq4i46pm9zg5nkm2gz8rk2zr4k8dqjvdm12dx0czhg8y") (y #t)))

(define-public crate-null-terminated-0.2.3 (c (n "null-terminated") (v "0.2.3") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3.1") (d #t) (k 0)))) (h "1aln9hhqd1hxjxlzlz4hvcvqc0gkqx3l0vknapyqr5bxkzsvdi01") (y #t)))

(define-public crate-null-terminated-0.2.4 (c (n "null-terminated") (v "0.2.4") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.4.0") (d #t) (k 0)))) (h "0f322rqc395fng234bdi5whvr2nnz8qg3av7w1h3m99v8p8laj0p") (y #t)))

(define-public crate-null-terminated-0.2.5 (c (n "null-terminated") (v "0.2.5") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)))) (h "0c9h5mls1a08srj1lg191rszdlwv1b23kk2inkrq47sc91sp8j0a") (y #t)))

(define-public crate-null-terminated-0.2.6 (c (n "null-terminated") (v "0.2.6") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)))) (h "11mdkn2m0fbm5m6yha9g6fdrlbq3i7rgskn4ivsdxp5iw26y6262")))

(define-public crate-null-terminated-0.2.7 (c (n "null-terminated") (v "0.2.7") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)))) (h "03vzrjwc9d0i319p33i4f6ll682g0k4adhdc4h399c7arch9g2aw")))

(define-public crate-null-terminated-0.2.8 (c (n "null-terminated") (v "0.2.8") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "19301hmj8w27s1s9g8bg6h2zahkzc8lfb8qdwjn7zl94iyxba98k")))

(define-public crate-null-terminated-0.2.9 (c (n "null-terminated") (v "0.2.9") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "1m3aaynbgll88lxjy1ynavj4f9cv4x6z570qkf5m8l932ylca7db")))

(define-public crate-null-terminated-0.2.10 (c (n "null-terminated") (v "0.2.10") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "0d64gsshk2ihp5l8y34av93pqpapwx56vqb4z5hds7x71iwda2rm")))

(define-public crate-null-terminated-0.3.0 (c (n "null-terminated") (v "0.3.0") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "1njv0yycnx09wk84i24nkzdrxpi9bxpxazc1py0lxccdaf3v6syd")))

(define-public crate-null-terminated-0.2.11 (c (n "null-terminated") (v "0.2.11") (d (list (d (n "null-terminated") (r "^0.3") (d #t) (k 0)))) (h "182myqc57w82drnfxgqzan97gkgpv6iqmnzwk084p98g14x91dfy")))

(define-public crate-null-terminated-0.3.1 (c (n "null-terminated") (v "0.3.1") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "0q2jxnqlqzh60yw2cwxfba3hiqiy40bq3sda6b9g67lvq895gv2y")))

(define-public crate-null-terminated-0.3.2 (c (n "null-terminated") (v "0.3.2") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "05zalacgm4pwf2frbmbc100ck1lq6y4wcm01w789s3kax9r1zbbn")))

(define-public crate-null-terminated-0.3.3 (c (n "null-terminated") (v "0.3.3") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1q3kmkr25bkinq19bc3fgywrl00yv3kdvpzs5zpdaxyg7fqhp43s") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.4 (c (n "null-terminated") (v "0.3.4") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ln1aqy5lmcmsw1qss06md7sr3726wlk7qdv42gnp2qr6bds53n2") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.5 (c (n "null-terminated") (v "0.3.5") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06y2vq4v9fs7wnl5g7k9q94asxrsn574wj4n5islk7pmbb9vx73j") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.6 (c (n "null-terminated") (v "0.3.6") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "05hxyhb2sl3spl9cc33sm4pr5a63s79fhqn5rd12dlsqrkgyngc9") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.7 (c (n "null-terminated") (v "0.3.7") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1kacksbplz4fp04wya7y15qdkmq9llp23q14wdam0v45b08n34wg") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.8 (c (n "null-terminated") (v "0.3.8") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0h0pm9r7d4803xdkdfz3yxppdcv9yjzgxds4r0fr9mb77nw66ikg") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.9 (c (n "null-terminated") (v "0.3.9") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "04qw00731lfc2jn37364kah44anp1xrpyr0rbjnbzqw5m5hy1ff3") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.10 (c (n "null-terminated") (v "0.3.10") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1k5x4cclcpr0qwxw4h4cvg5r4dflww7vwc3b3q8kl2ips8vck1m0") (f (quote (("default" "utf")))) (y #t)))

(define-public crate-null-terminated-0.3.11 (c (n "null-terminated") (v "0.3.11") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1xr5wskb6rrngw3y8hlyg44mnpk9zdbl571b74c2mfvmaxpxj30s") (f (quote (("default" "utf")))) (y #t)))

(define-public crate-null-terminated-0.3.12 (c (n "null-terminated") (v "0.3.12") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hfa6f1wzfzm8ivmgr6ssw8vl2fnjm7x07p11994wf36mq9h732i") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.13 (c (n "null-terminated") (v "0.3.13") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0b6wbspkykhk1dnz9cr674rfd2jwhpcxffxxqd258vrhnvcrdlpi") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.14 (c (n "null-terminated") (v "0.3.14") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0bdnw3z3sv22a0mhgr6vxrngwrkryz95brbc8lzwqdnwvbfsgisc") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.15 (c (n "null-terminated") (v "0.3.15") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0gmgj9bcf7grmq6jws6scjzyk8259fg0x0j5jxffb3kigbna83z8") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.16 (c (n "null-terminated") (v "0.3.16") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1xg67g16a9glk9mfzpjz102gis52adqm0pncsrabxxlbkdswzfdg") (f (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3.17 (c (n "null-terminated") (v "0.3.17") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "system-call") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "utf") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1grh9vhqs6kwnk065z4yr1p4bd0q8cy9yndkf4ax4sxa4p3b4fnv") (f (quote (("default" "utf"))))))

