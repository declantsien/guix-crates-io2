(define-module (crates-io nu ll null-engine) #:use-module (crates-io))

(define-public crate-null-engine-0.0.0 (c (n "null-engine") (v "0.0.0") (h "0givdzwkaq46lwi6jm2wl31i64c5wpgxyb44768vpjnznxj39lzn")))

(define-public crate-null-engine-0.1.0 (c (n "null-engine") (v "0.1.0") (d (list (d (n "block-reward") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (d #t) (k 0)) (d (n "mashina") (r "^0.1.0") (d #t) (k 0)) (d (n "vapjson") (r "^0.1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1mq0nyf9rzd891ib88syaks6v823k3xys12jvljxlrlrrrljkx2i")))

