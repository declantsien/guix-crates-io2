(define-module (crates-io nu ll null_fn) #:use-module (crates-io))

(define-public crate-null_fn-0.1.0 (c (n "null_fn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "171v4h3q4r8m6ph1fz449qsjwb9xfmnapwpddb21a14gp1iyqd29")))

(define-public crate-null_fn-0.1.1 (c (n "null_fn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hbnk61744gg4zdxppwa7ag6vabc0ibfl6n2xykqs39dnp7ijn9l")))

