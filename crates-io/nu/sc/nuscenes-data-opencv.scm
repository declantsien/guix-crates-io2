(define-module (crates-io nu sc nuscenes-data-opencv) #:use-module (crates-io))

(define-public crate-nuscenes-data-opencv-0.1.0 (c (n "nuscenes-data-opencv") (v "0.1.0") (d (list (d (n "nuscenes-data") (r "^0.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.82.1") (f (quote ("imgcodecs"))) (k 0)))) (h "14zfdwqbic5sj3znm44vgmn8d82jsvs963yak3j5jcrc1rl161al")))

