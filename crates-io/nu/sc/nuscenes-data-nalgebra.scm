(define-module (crates-io nu sc nuscenes-data-nalgebra) #:use-module (crates-io))

(define-public crate-nuscenes-data-nalgebra-0.1.0 (c (n "nuscenes-data-nalgebra") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "nuscenes-data") (r "^0.4.0") (d #t) (k 0)))) (h "0532bv50492bkynasknd79y09l72flx8f1vdhc53kkxy385mya63")))

