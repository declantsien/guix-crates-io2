(define-module (crates-io nu sc nuscenes-data-pcd) #:use-module (crates-io))

(define-public crate-nuscenes-data-pcd-0.1.0 (c (n "nuscenes-data-pcd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 2)) (d (n "nuscenes-data") (r "^0.4.0") (d #t) (k 0)) (d (n "pcd-rs") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "raw-parts") (r "^2.0.0") (d #t) (k 0)))) (h "05c59h0a671mll53j4wkj2ija0wnkrvj97drfg683wqqdspyc36a")))

