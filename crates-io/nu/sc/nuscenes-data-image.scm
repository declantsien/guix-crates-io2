(define-module (crates-io nu sc nuscenes-data-image) #:use-module (crates-io))

(define-public crate-nuscenes-data-image-0.1.0 (c (n "nuscenes-data-image") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nuscenes-data") (r "^0.4.0") (d #t) (k 0)) (d (n "show-image") (r "^0.13.1") (f (quote ("image"))) (d #t) (k 2)))) (h "1xlgdrmhxj3n62mb3sw7yj1qn7pajwvjfy2nb6c2pwdf982ijq8i")))

