(define-module (crates-io nu co nucomcore) #:use-module (crates-io))

(define-public crate-nucomcore-0.1.0 (c (n "nucomcore") (v "0.1.0") (d (list (d (n "nuidl") (r "^0.1.0") (d #t) (k 0)) (d (n "nuidl") (r "^0.1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1jn9naiap2f5igrmxm6c63rry83cwayf20lnzalvmbzzqfvm6qc4")))

(define-public crate-nucomcore-0.1.1 (c (n "nucomcore") (v "0.1.1") (d (list (d (n "nuidl") (r "^0.1.1") (d #t) (k 0)) (d (n "nuidl") (r "^0.1.1") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "183bcrg6n79dvpz7hzy1nrf5cqif6q2zzwyqni67qhh5s34kvczf")))

