(define-module (crates-io nu -m nu-macros) #:use-module (crates-io))

(define-public crate-nu-macros-0.7.0 (c (n "nu-macros") (v "0.7.0") (d (list (d (n "nu-protocol") (r "^0.7.0") (d #t) (k 0)))) (h "1sjv4ayv0760vklvjzmkvqg0fdcpikz253b3d93w53yz3ipmqbvv")))

(define-public crate-nu-macros-0.8.0 (c (n "nu-macros") (v "0.8.0") (d (list (d (n "nu-protocol") (r "^0.8.0") (d #t) (k 0)))) (h "13jrlbbx4f5bviz3awacmq8nqld2rwpv0n2daarmcaqh6k447jjd")))

(define-public crate-nu-macros-0.9.0 (c (n "nu-macros") (v "0.9.0") (d (list (d (n "nu-protocol") (r "^0.9.0") (d #t) (k 0)))) (h "1i3pvglqbc34vl7yn0kv4az2092izjd7c025rxi3ff48lh9dc1h4")))

(define-public crate-nu-macros-0.10.0 (c (n "nu-macros") (v "0.10.0") (d (list (d (n "nu-protocol") (r "^0.10.0") (d #t) (k 0)))) (h "0mrwzcwlqml9yjcf9sk04i5cwkqrpl3xd3rmqcqf9dp83wbr9mz6")))

(define-public crate-nu-macros-0.11.0 (c (n "nu-macros") (v "0.11.0") (d (list (d (n "nu-protocol") (r "^0.11.0") (d #t) (k 0)))) (h "0k2bbshrhxg3w43s86kwyqf10xiibx3hlcwxdz3g06xfq0kxah0n")))

(define-public crate-nu-macros-0.12.0 (c (n "nu-macros") (v "0.12.0") (d (list (d (n "nu-protocol") (r "^0.12.0") (d #t) (k 0)))) (h "1y060sf8y05ic25d46aginkkw5wqbjjqx1hm9gn4iblsif53sa02")))

