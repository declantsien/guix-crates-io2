(define-module (crates-io nu l- nul-terminated) #:use-module (crates-io))

(define-public crate-nul-terminated-0.1.0 (c (n "nul-terminated") (v "0.1.0") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "idem") (r "^0.3") (d #t) (k 0)))) (h "03a3sllngn8b74fn77v91xp97pa57vphwpj8vshrfys00iiiqny8")))

