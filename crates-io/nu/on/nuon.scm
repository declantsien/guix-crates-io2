(define-module (crates-io nu on nuon) #:use-module (crates-io))

(define-public crate-nuon-0.93.0 (c (n "nuon") (v "0.93.0") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "nu-engine") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.93.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.93.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0khv6ykxdpbvwq4lvxnfrvg7ir0z9wy9caydswca008h6z7bj2b5")))

(define-public crate-nuon-0.94.0 (c (n "nuon") (v "0.94.0") (d (list (d (n "chrono") (r "^0.4.34") (k 2)) (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "nu-engine") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-parser") (r "^0.94.0") (d #t) (k 0)) (d (n "nu-protocol") (r "^0.94.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0rmlw6d0qyialimznd4kxifydrch7r1fh40ms0br8l71rig4jw5s")))

