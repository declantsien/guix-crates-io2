(define-module (crates-io nu id nuid) #:use-module (crates-io))

(define-public crate-nuid-0.1.0 (c (n "nuid") (v "0.1.0") (d (list (d (n "lazy_static") (r "~1.0") (d #t) (k 0)) (d (n "rand") (r "~0.4") (d #t) (k 0)))) (h "10phm8f5gidjpfv26h3bxdvn1p4iq3ikjr40zisllvv44y45fy4q")))

(define-public crate-nuid-0.2.0 (c (n "nuid") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "19crkg1vvxklv5lwil09khlbc5xw495sp21j32lnjnpg72dv6h8b")))

(define-public crate-nuid-0.2.1 (c (n "nuid") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "15df35j21pdwnf7vkmqgkkz954php8hvvpiivqb5m7k0j6msbw24")))

(define-public crate-nuid-0.2.2 (c (n "nuid") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0y8rnrbmlbxwi1ixwlq9jp3y9ypjzhxf0bir3agi1p3n5z2vwqc0")))

(define-public crate-nuid-0.3.0 (c (n "nuid") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1lrbk9ab0w01k0b4r4whhsad476hyxmhig4a7sj4np2l5cwwj03h")))

(define-public crate-nuid-0.3.2 (c (n "nuid") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 0)))) (h "0f2gpgzmkkzxq29vf3xq59dv96hdnbgazwbb614d663731jvph90")))

(define-public crate-nuid-0.4.0 (c (n "nuid") (v "0.4.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 0)))) (h "12cfvccvkabb4b2v9lpi6gf21iwh0bz0fk7qg2g00jp3fzybg391")))

(define-public crate-nuid-0.4.1 (c (n "nuid") (v "0.4.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 0)))) (h "1bpn1zvz64mrpnivpak7mlphxzka1k3bc83vlvk87r1j0iqv2q8b")))

(define-public crate-nuid-0.5.0 (c (n "nuid") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "10vy5gjpjnvsmbavjlczhgfby9ldlwk0r8ha78b2kyanb3wmm2gw") (r "1.63")))

