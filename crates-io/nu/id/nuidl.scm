(define-module (crates-io nu id nuidl) #:use-module (crates-io))

(define-public crate-nuidl-0.1.0 (c (n "nuidl") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (d #t) (k 0)) (d (n "nuidl-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "nuidl-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1pcif3jnnpxfjxmynx7y2qm5qc5qnxmwzqjz1d27xzwam9dq2q2x") (f (quote (("default" "cargo" "macro") ("cargo" "nuidl-lib/cargo")))) (s 2) (e (quote (("macro" "dep:nuidl-macro"))))))

(define-public crate-nuidl-0.1.1 (c (n "nuidl") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (d #t) (k 0)) (d (n "nuidl-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "nuidl-macro") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1jl4mszx83x4x2jaz5cjpbp46np1d44wnjiv0z2yvq00hgr32lg9") (f (quote (("default" "cargo" "macro") ("cargo" "nuidl-lib/cargo")))) (s 2) (e (quote (("macro" "dep:nuidl-macro"))))))

