(define-module (crates-io nu id nuidl-macro) #:use-module (crates-io))

(define-public crate-nuidl-macro-0.1.0 (c (n "nuidl-macro") (v "0.1.0") (d (list (d (n "nuidl-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)))) (h "00bczq757c1hy8442k9ifn98ylnmb6hgv20sh1hhwfs0vhwdiagj")))

(define-public crate-nuidl-macro-0.1.1 (c (n "nuidl-macro") (v "0.1.1") (d (list (d (n "nuidl-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)))) (h "0n9p9ibajlnrw6kw7r8wf7sfs0ak53vsmifxcy6ib9vfyyn2n7sz")))

