(define-module (crates-io nu cl nucleic-acid) #:use-module (crates-io))

(define-public crate-nucleic-acid-0.1.0 (c (n "nucleic-acid") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1m7r4rbb68bj8hmkjprq59p07b50mpnlsj0ca0jj5s8z0y0m2fv4")))

(define-public crate-nucleic-acid-0.1.1 (c (n "nucleic-acid") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0vxd5591x6h364qpy9ccxcdlblzvc850bq2kkhih6dkd2sgfzlbc")))

