(define-module (crates-io nu cl nucleob) #:use-module (crates-io))

(define-public crate-nucleob-0.0.0 (c (n "nucleob") (v "0.0.0") (h "10nygysrw0vx320q6k13v4ml81a655444j63g6a1qbwxyk2yzigs") (y #t)))

(define-public crate-nucleob-1.0.0-alpha.1 (c (n "nucleob") (v "1.0.0-alpha.1") (h "1y0fkmg5jmchaybcmy1c9lzlmzfq6445vn00idysdvxl6m6is81h") (y #t)))

(define-public crate-nucleob-1.0.0 (c (n "nucleob") (v "1.0.0") (h "0ds04dh6xrmc2rhx4gap92g2q4xza85s1rxcjh381bb9v5x9fcg9")))

(define-public crate-nucleob-1.1.0 (c (n "nucleob") (v "1.1.0") (h "0jqhhsxa31wc1n0s3x5anpq586k4wyw0chk5g99abxf5l5xsdk6n")))

(define-public crate-nucleob-1.1.1 (c (n "nucleob") (v "1.1.1") (h "1haysbaxsi99v56wrsirj2y7sn8rxk78d31hw0giqv453inzqbxz")))

