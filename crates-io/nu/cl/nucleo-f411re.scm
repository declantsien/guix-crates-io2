(define-module (crates-io nu cl nucleo-f411re) #:use-module (crates-io))

(define-public crate-nucleo-f411re-0.1.0 (c (n "nucleo-f411re") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.7.0") (f (quote ("stm32f411" "rt"))) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 2)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 2)))) (h "05cdmx3rzq3i6dfsqgf0861d22m0508apphj7dlrlslg7frfpsy2") (f (quote (("rt") ("default" "rt"))))))

