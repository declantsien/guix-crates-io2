(define-module (crates-io nu cl nucleo) #:use-module (crates-io))

(define-public crate-nucleo-0.0.0 (c (n "nucleo") (v "0.0.0") (d (list (d (n "nucleo-matcher") (r "^0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1js7hg711205a7d4i97fh9v2vpi6ijcnigaf3whknmp74w3xiaph")))

(define-public crate-nucleo-0.1.0 (c (n "nucleo") (v "0.1.0") (d (list (d (n "nucleo-matcher") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1kvpmp0q72n3nrwc6sv0129lbaimgw9kaiyqxh46f5q8brv5xnaw")))

(define-public crate-nucleo-0.1.1 (c (n "nucleo") (v "0.1.1") (d (list (d (n "nucleo-matcher") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0ampnscdbfmw1hbcz6rshs5r07v5abqvz5bkcix1ka3bijvdyjrl")))

(define-public crate-nucleo-0.2.0 (c (n "nucleo") (v "0.2.0") (d (list (d (n "nucleo-matcher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1dhikdizb5sl4qis6vjnad0zfn6kk16r8c0lncdjgbf8y8vbkjhc")))

(define-public crate-nucleo-0.2.1 (c (n "nucleo") (v "0.2.1") (d (list (d (n "nucleo-matcher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1wzx32bz4n68dcd3yw57195sz49hdhv9b75jikr5qiyfpks32lxf")))

(define-public crate-nucleo-0.3.0 (c (n "nucleo") (v "0.3.0") (d (list (d (n "nucleo-matcher") (r "^0.3.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0b6n7x2ikz975z11qki9m4rxvwf83id1qrcray8ihjaivwb87hnr")))

(define-public crate-nucleo-0.4.0 (c (n "nucleo") (v "0.4.0") (d (list (d (n "nucleo-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0kplnk50cagdpjgfmbm82h8dyhkja2d2a4zj1y9l1gslsa859vky")))

(define-public crate-nucleo-0.4.1 (c (n "nucleo") (v "0.4.1") (d (list (d (n "nucleo-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1sckfnrhbjf5f17x2wck15xd369rh7vcp4vm7995h1l6v0wa2l33")))

(define-public crate-nucleo-0.5.0 (c (n "nucleo") (v "0.5.0") (d (list (d (n "nucleo-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard" "arc_lock"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1m1rq0cp02hk31z7jsn2inqcpy9a1j8gfvxcqm32c74jji6ayqjj")))

