(define-module (crates-io nu cl nuclear-router) #:use-module (crates-io))

(define-public crate-nuclear-router-0.0.1 (c (n "nuclear-router") (v "0.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c0wzcx37izsbjdvicc6pxkzr60p1ksq7ndmf50bps2frx7b3d59") (y #t)))

(define-public crate-nuclear-router-0.1.0 (c (n "nuclear-router") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros"))) (d #t) (k 2)))) (h "0i9zp2iz4b7g6vn9abb4wwqxca9wkbfz8akah701fmrbsd3x222a") (f (quote (("hyper-service" "http-router" "hyper") ("http-router" "http") ("default" "http-router" "hyper-service"))))))

