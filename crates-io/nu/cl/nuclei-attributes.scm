(define-module (crates-io nu cl nuclei-attributes) #:use-module (crates-io))

(define-public crate-nuclei-attributes-0.1.0 (c (n "nuclei-attributes") (v "0.1.0") (d (list (d (n "nuclei") (r "^0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l97phl4yw2h0scxx0gn4dwmngnm2xfd3r4xyz6gkpdhllg1qc46")))

