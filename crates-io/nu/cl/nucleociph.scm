(define-module (crates-io nu cl nucleociph) #:use-module (crates-io))

(define-public crate-nucleociph-1.0.0 (c (n "nucleociph") (v "1.0.0") (h "05bfkfgjfj3zxg0n2ryf9yy3irahw3shqky0jk0v0zl56d5m1x42")))

(define-public crate-nucleociph-1.0.1 (c (n "nucleociph") (v "1.0.1") (h "18skyy8wn12wjggnjwwa0cal9hc6q9mcn7qcmjk06i13x8ri06z6")))

