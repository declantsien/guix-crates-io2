(define-module (crates-io nu cl nucleus) #:use-module (crates-io))

(define-public crate-nucleus-0.1.0 (c (n "nucleus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1sfmpcq5z13qn7fzgxr78fiyzb1h8sg9a98i88vpdla8m3fszlcc")))

(define-public crate-nucleus-0.1.1 (c (n "nucleus") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0f3zw220n6gprpnqqcs07m62dk1spgh6yp3xn8cqrx4j074g6y74")))

