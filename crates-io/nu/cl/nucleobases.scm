(define-module (crates-io nu cl nucleobases) #:use-module (crates-io))

(define-public crate-nucleobases-0.1.0 (c (n "nucleobases") (v "0.1.0") (h "16nh21s82d8zq4znaq9ylpv1grfjhm99pxhmgm1akbl8wqnmhiyf")))

(define-public crate-nucleobases-1.1.0 (c (n "nucleobases") (v "1.1.0") (h "112lr60niw40391nk8dfacm9g2i13g93rfllxxplaa8mkwx79q5c")))

