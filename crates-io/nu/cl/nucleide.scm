(define-module (crates-io nu cl nucleide) #:use-module (crates-io))

(define-public crate-nucleide-0.0.1 (c (n "nucleide") (v "0.0.1") (d (list (d (n "parity-wasm") (r "^0.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w6zmrj4i8ffw7x4bcd84fx44l17pkqk1pb2j1ibpx6x27pbm5kb")))

(define-public crate-nucleide-0.1.0 (c (n "nucleide") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.6") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45") (k 0)))) (h "06jrx0hjqkp6llv6p3vjdriib61vcxa7slkas083z14irmp9xhwc") (r "1.70")))

