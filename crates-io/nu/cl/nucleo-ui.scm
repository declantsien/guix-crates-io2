(define-module (crates-io nu cl nucleo-ui) #:use-module (crates-io))

(define-public crate-nucleo-ui-0.1.0 (c (n "nucleo-ui") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "12gqgxlny25vq1g8q1sqs53aag33b1cl0vkvvjfgd2b4av7489nd")))

(define-public crate-nucleo-ui-0.1.1 (c (n "nucleo-ui") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1qqxqca6vs0iwlj69bw21w9sbcm3ailydskjdm7l19abc54b5a11")))

(define-public crate-nucleo-ui-0.1.2 (c (n "nucleo-ui") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "026clr44xhrjsp17mf8zj5ipk7091bjanbx8bmxvck0hl093s06a")))

(define-public crate-nucleo-ui-0.1.3 (c (n "nucleo-ui") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1xb4slw1qvvx9sbw0ld7q4d2glmnkdl59gqa7nqba0p3jrsvqa2k")))

(define-public crate-nucleo-ui-0.1.4 (c (n "nucleo-ui") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1qcgjmf78niryx7jf9psjcxcr6380vcy02sgf6irlqfl5vjj8scn")))

(define-public crate-nucleo-ui-0.1.5 (c (n "nucleo-ui") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1xddr0202bi7r6pymvj2ixx8ziw7a27rpkrj98bzf6p1f21lyw2k")))

(define-public crate-nucleo-ui-0.1.6 (c (n "nucleo-ui") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0p3ba7z9dx0jmcf5ijw2jzqavif2rx7djd356pmj101s81l0jzk2")))

