(define-module (crates-io nu cl nucleon) #:use-module (crates-io))

(define-public crate-nucleon-0.1.0 (c (n "nucleon") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "nucleus-rpc") (r "^0.1.0") (d #t) (k 0)))) (h "17950ds5d52hgr8ckvnmqf6l2hp7my9f9a05b0y4658nxc7p8rs1")))

(define-public crate-nucleon-0.1.1 (c (n "nucleon") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "nucleus-rpc") (r "^0.1.1") (d #t) (k 0)))) (h "0zns17l1cxc09c29dzk857cckq59fwmpk77s2vpfvicpw8nfpfr1")))

