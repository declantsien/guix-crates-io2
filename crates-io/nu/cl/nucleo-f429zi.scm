(define-module (crates-io nu cl nucleo-f429zi) #:use-module (crates-io))

(define-public crate-nucleo-f429zi-0.0.1 (c (n "nucleo-f429zi") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.1") (d #t) (k 0)) (d (n "smoltcp") (r "^0.5.0") (f (quote ("log" "proto-ipv4" "socket-udp" "socket-tcp"))) (k 0)) (d (n "stm32f429") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "stm32f429-hal") (r "^0.1.1") (d #t) (k 0)))) (h "0hp1x5y4mn3h53lnqw9zbc4ai3wc1m67p7jb14n3h00d1yhp40im")))

