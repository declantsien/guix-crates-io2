(define-module (crates-io nu cl nuclide) #:use-module (crates-io))

(define-public crate-Nuclide-0.1.0 (c (n "Nuclide") (v "0.1.0") (h "04k5kzpv3l3b7jjsn0nhdl63vagpnqq3fr91vshppm4mjrgn5inm")))

(define-public crate-Nuclide-0.1.1 (c (n "Nuclide") (v "0.1.1") (h "1i05b4qxllk5iybmbls30972zhlsg78a9rprkafzwzhxlwmayacs")))

(define-public crate-Nuclide-0.1.2 (c (n "Nuclide") (v "0.1.2") (h "06b653nzpm54xpzxxkqkbzv5lhd1cxnm91pr9xg2129632vm04ih")))

(define-public crate-Nuclide-0.1.3 (c (n "Nuclide") (v "0.1.3") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "0jlcx5sq83c0hcx54jxrpk5q64lrjm3vw2svyfm57y9mdd0vqbvl")))

(define-public crate-Nuclide-0.1.4 (c (n "Nuclide") (v "0.1.4") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "163bd9q2k0zr34nv0h0mxw26hyi3d7xnzzv22xzpvh2wkl9564j7")))

(define-public crate-Nuclide-0.1.5 (c (n "Nuclide") (v "0.1.5") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "1mzdwmm5jvb7q5nmsjdhf9q2gaakzqi2xilh8hj1927lxxiv58vg")))

(define-public crate-Nuclide-0.1.6 (c (n "Nuclide") (v "0.1.6") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "1pzlg4rp4lf89ih4vaxwkyr7mgh64i2vazj6k5hxf2wv8ilj9rf5")))

(define-public crate-Nuclide-0.1.7 (c (n "Nuclide") (v "0.1.7") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "1ag05y9ak2cypc8mxg43bw56bqhadq5486krlqd7nfk1i1iplg7g")))

(define-public crate-Nuclide-0.1.8 (c (n "Nuclide") (v "0.1.8") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "0q3g2xnhzb5m993spw7bpqfh3c1kk0p2yqb3rflipr5hb8gz1xj3")))

(define-public crate-Nuclide-0.1.9 (c (n "Nuclide") (v "0.1.9") (d (list (d (n "Pion") (r "^0.0.1") (d #t) (k 0)))) (h "0kck9gkazgj3wqlwrq54ymmjyzg33lbcwnawbqjq32n5zzdjc633")))

(define-public crate-Nuclide-0.2.0 (c (n "Nuclide") (v "0.2.0") (d (list (d (n "Pion") (r "^0.0.2") (d #t) (k 0)))) (h "1r27akcas704xf3mpg493ld4n0lsbvh6pl0zym4l4zs3hdmpm47w")))

(define-public crate-Nuclide-0.2.1 (c (n "Nuclide") (v "0.2.1") (d (list (d (n "Pion") (r "^0.0.2") (d #t) (k 0)))) (h "0pn61jyzhsg48mqnz714fn79bn68fbhdqkr1k21lb6dpdiymvn1c")))

