(define-module (crates-io nu cl nucleo-l031k6-bsp) #:use-module (crates-io))

(define-public crate-nucleo-l031k6-bsp-0.1.1 (c (n "nucleo-l031k6-bsp") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "stm32l0x1") (r "^0.13.1") (d #t) (k 0)) (d (n "stm32l0x1-hal") (r "^0.7") (f (quote ("STM32L031x6" "rt"))) (d #t) (k 0)))) (h "04ffm4snfcwzvgazr9gw189abp9v03pnimsdkbrg488l0x9ji83m")))

(define-public crate-nucleo-l031k6-bsp-0.2.0 (c (n "nucleo-l031k6-bsp") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.11.0") (f (quote ("stm32l0x1" "rt"))) (d #t) (k 0)) (d (n "stm32l0x1-hal") (r "^0.9") (f (quote ("STM32L031x6" "rt"))) (d #t) (k 0)))) (h "1x4lc68mpxd127lrc4jdjq0qr342jq930xqz8lw3lmc1mlzdf0s6")))

(define-public crate-nucleo-l031k6-bsp-0.3.0 (c (n "nucleo-l031k6-bsp") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.15.1") (f (quote ("stm32l0x1" "rt"))) (d #t) (k 0)) (d (n "stm32l0x1-hal") (r "^0.11") (f (quote ("STM32L031x6" "rt"))) (d #t) (k 0)))) (h "1wvrikfby88yxd8kxvs5r8amb4xmhpyvsqxz5fkancdjl6v21hmj")))

