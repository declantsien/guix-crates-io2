(define-module (crates-io nu cl nucleus-tauri-utils) #:use-module (crates-io))

(define-public crate-nucleus-tauri-utils-1.0.0 (c (n "nucleus-tauri-utils") (v "1.0.0") (d (list (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "1m9d4xsvkk8qcxm4272cckl0gfgh4qzb6d6mvxg2hx321pg2c5zk")))

