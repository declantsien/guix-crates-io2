(define-module (crates-io nu cl nucleo-matcher) #:use-module (crates-io))

(define-public crate-nucleo-matcher-0.0.0 (c (n "nucleo-matcher") (v "0.0.0") (d (list (d (n "cov-mark") (r "^1.1.0") (k 0)) (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1wbhg7hgn7w09wj2rfw1c03mcy2k1gssjag7dmvh7c0469njplg9")))

(define-public crate-nucleo-matcher-0.1.0 (c (n "nucleo-matcher") (v "0.1.0") (d (list (d (n "cov-mark") (r "^1.1.0") (k 0)) (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1g1zgkq97s0ys9xhcrlpvwv3sih0bnajc6hf05dc646s8pvll0bn")))

(define-public crate-nucleo-matcher-0.2.0 (c (n "nucleo-matcher") (v "0.2.0") (d (list (d (n "cov-mark") (r "^1.1.0") (k 0)) (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (o #t) (d #t) (k 0)))) (h "0mxxz58acszkgxha4wy459fkcx6f8sh55d803wnid1p25x02nw0v") (f (quote (("unicode-normalization") ("unicode-casefold") ("default" "unicode-normalization" "unicode-casefold" "unicode-segmentation")))) (s 2) (e (quote (("unicode-segmentation" "dep:unicode-segmentation"))))))

(define-public crate-nucleo-matcher-0.3.0 (c (n "nucleo-matcher") (v "0.3.0") (d (list (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (o #t) (d #t) (k 0)))) (h "0kb5ya14zzikr5l9mba4j4z09a7mjcj0p6chjvlwbih3m8l0c115") (f (quote (("unicode-normalization") ("unicode-casefold") ("default" "unicode-normalization" "unicode-casefold" "unicode-segmentation")))) (s 2) (e (quote (("unicode-segmentation" "dep:unicode-segmentation"))))))

(define-public crate-nucleo-matcher-0.3.1 (c (n "nucleo-matcher") (v "0.3.1") (d (list (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (o #t) (d #t) (k 0)))) (h "11dc5kfin1n561qdcg0x9aflvw876a8vldmqjhs5l6ixfcwgacxz") (f (quote (("unicode-normalization") ("unicode-casefold") ("default" "unicode-normalization" "unicode-casefold" "unicode-segmentation")))) (s 2) (e (quote (("unicode-segmentation" "dep:unicode-segmentation"))))))

