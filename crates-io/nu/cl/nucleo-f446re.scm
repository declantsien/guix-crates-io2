(define-module (crates-io nu cl nucleo-f446re) #:use-module (crates-io))

(define-public crate-nucleo-f446re-0.1.0 (c (n "nucleo-f446re") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "panic-probe") (r "^0.3.0") (f (quote ("print-rtt"))) (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.13.2") (f (quote ("stm32f446"))) (d #t) (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "04b4wp6gv5wzb05crxj4ixxqm54ynxkknvngryjkslkgabg86i2p")))

