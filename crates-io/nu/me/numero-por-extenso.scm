(define-module (crates-io nu me numero-por-extenso) #:use-module (crates-io))

(define-public crate-numero-por-extenso-0.1.0 (c (n "numero-por-extenso") (v "0.1.0") (h "1anqbqg2m0c6mxx9hkz1ckp8v3dwi2l920s5wmi0g49kkknng1kb")))

(define-public crate-numero-por-extenso-0.1.1 (c (n "numero-por-extenso") (v "0.1.1") (h "18f1lfkwnb6pfy7f8kcqw580kaddcmjqba5806cyrzsqgb8c88l9")))

(define-public crate-numero-por-extenso-0.1.2 (c (n "numero-por-extenso") (v "0.1.2") (h "1qvqh9127ik7ja5nkajhzypblw55rprhqjvj8cc9kc6jvz6xgavw")))

(define-public crate-numero-por-extenso-0.2.0 (c (n "numero-por-extenso") (v "0.2.0") (h "0crk7mkq74xpqna3fial776fddv48b2hfm0mlyss0fbfi6i9dz5c")))

(define-public crate-numero-por-extenso-0.3.0 (c (n "numero-por-extenso") (v "0.3.0") (h "0nba9m9jiwrrmcfdc2jz4hzywxxm3jw3j1ishxr94944xc96h6r6")))

(define-public crate-numero-por-extenso-0.3.1 (c (n "numero-por-extenso") (v "0.3.1") (h "131gh11brgvc3y4rdz3j4wqqmikq0vn7d3fdw1d2svpas2flc1yk")))

