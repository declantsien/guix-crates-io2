(define-module (crates-io nu me numeral) #:use-module (crates-io))

(define-public crate-numeral-0.1.0 (c (n "numeral") (v "0.1.0") (h "1avh804ryfazv5fh984srxxn9ryvnfafc2y7lzp2xi9ry6jvg32j") (y #t)))

(define-public crate-numeral-0.1.1 (c (n "numeral") (v "0.1.1") (h "13wqa9kb5y3v7iai9mn2p07f9vp9xyb59zppshwsj1r5b4rkjm76")))

(define-public crate-numeral-0.1.2 (c (n "numeral") (v "0.1.2") (h "0730rra77p663pkkg63fk10zamjcw05631r79zg9hfaaaxniwhsb")))

(define-public crate-numeral-0.2.0 (c (n "numeral") (v "0.2.0") (h "1awz9dcwx9xgdrcfw6va5zvf62zdb8ykci4bcshxb4399vrdga95")))

(define-public crate-numeral-0.3.0 (c (n "numeral") (v "0.3.0") (h "0z3daqby8sdz83ba6qxcjgsyf9fiayby3zcm2gavzs9d3i1s1pff")))

(define-public crate-numeral-0.3.1 (c (n "numeral") (v "0.3.1") (h "1a5012igxdbjg231vmkwirrp35hflzwk7rcxxx69byfcpykf3ygp")))

(define-public crate-numeral-1.0.0 (c (n "numeral") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)))) (h "08p7giin625q0j53lv2g35qyx32l963wabym7rkyjscgbai51684")))

