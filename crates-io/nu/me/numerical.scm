(define-module (crates-io nu me numerical) #:use-module (crates-io))

(define-public crate-numerical-0.1.0 (c (n "numerical") (v "0.1.0") (h "1dyxl9q9wzwiaq1n65lvwdb3iw7nzinscxr5s581nmdvz1j95k0c")))

(define-public crate-numerical-0.1.1 (c (n "numerical") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b867rzw3ihql3ayq3ldjx01awpifwf2s80pikpy6vjdfwp6akzq")))

(define-public crate-numerical-0.1.2 (c (n "numerical") (v "0.1.2") (d (list (d (n "core_float") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1136ymwmc846qy3x194q1h7my7p2sg32m4y2pwn0g6bhxw6wjscp")))

(define-public crate-numerical-0.1.3 (c (n "numerical") (v "0.1.3") (d (list (d (n "core_float") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vf7pyg8vcj9aw8wbpbp443bg6ay8vxpdzi4zlm2f92njf9nvyv1")))

(define-public crate-numerical-0.1.4 (c (n "numerical") (v "0.1.4") (d (list (d (n "core_float") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fnj29k59ka7g9a2gmdiipwwvn71kr79nph2k6471ns4jzw4f15m")))

(define-public crate-numerical-0.1.5 (c (n "numerical") (v "0.1.5") (d (list (d (n "core_float") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x11g41r2l6cczr1kssj1dy10621hh1xzsdv3v0jxwd3j8jcybih")))

