(define-module (crates-io nu me numeric_constant_traits) #:use-module (crates-io))

(define-public crate-numeric_constant_traits-0.1.0 (c (n "numeric_constant_traits") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "10zfaqr2rr3p7y9rww2gv4im61v5bb063p16yvcyrlvnrma0k3ff")))

(define-public crate-numeric_constant_traits-0.1.1 (c (n "numeric_constant_traits") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0njj86zwcjwq0fxjlg4681w41dcd2j6mm76gn44vgcjmpqsak1vz")))

