(define-module (crates-io nu me numeric_literals) #:use-module (crates-io))

(define-public crate-numeric_literals-0.1.0 (c (n "numeric_literals") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut" "printing" "full" "parsing" "proc-macro" "clone-impls"))) (k 0)))) (h "1nrzrhph26zm7mbqka3ai6bhh2rs3c3rf40078zpszji97kqbb7k")))

(define-public crate-numeric_literals-0.1.1 (c (n "numeric_literals") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut" "printing" "full" "parsing" "proc-macro" "clone-impls"))) (k 0)))) (h "1l91qsbyxxma3x37vvdgjd9agryf1hcyzy0f0mqdggnnpxjphb70")))

(define-public crate-numeric_literals-0.2.0 (c (n "numeric_literals") (v "0.2.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "printing" "full" "parsing" "proc-macro" "clone-impls"))) (k 0)))) (h "12pr7fw946sklpy1l8ccvib1vddx0rqz3x4qd5s8284z1dxscnh9")))

