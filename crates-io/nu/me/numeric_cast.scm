(define-module (crates-io nu me numeric_cast) #:use-module (crates-io))

(define-public crate-numeric_cast-0.1.0 (c (n "numeric_cast") (v "0.1.0") (h "07yxqyikq6xsq98k3n2b30vrxv63piv2slmg8c6y8467ydb0qvbh") (y #t)))

(define-public crate-numeric_cast-0.1.1 (c (n "numeric_cast") (v "0.1.1") (h "139d288lah56sp45giq8qff1wdc8xb181gwlgs8l5279j7ig856r") (y #t)))

(define-public crate-numeric_cast-0.1.2 (c (n "numeric_cast") (v "0.1.2") (h "1hrawcfkcx9z13bjf2m1npw92ja68gq7hxzz198x9g43mq96zhc9")))

(define-public crate-numeric_cast-0.2.0 (c (n "numeric_cast") (v "0.2.0") (h "0jnplmqbics21rrfc9v0pfrmx3xx301inyhw3qdfql9wfpfal99j") (r "1.56.0")))

(define-public crate-numeric_cast-0.2.1 (c (n "numeric_cast") (v "0.2.1") (h "19bkj77dsdrjn6fl6a11z4mip463dvwzin90dj1x2dqpkcnyww6g") (r "1.56.0")))

