(define-module (crates-io nu me numeric-algs) #:use-module (crates-io))

(define-public crate-numeric-algs-0.1.0 (c (n "numeric-algs") (v "0.1.0") (h "08llf0yz217m8nl0m0dxpinbcd7hgmnha1syyw10p4g8rc0qa7xg")))

(define-public crate-numeric-algs-0.2.0 (c (n "numeric-algs") (v "0.2.0") (h "0dk32kj8q4b1xrv1phq7xhcvm3vl7m118p18ip21vn9bgyvcnlan")))

(define-public crate-numeric-algs-0.2.1 (c (n "numeric-algs") (v "0.2.1") (h "06d2f2rrk688g2f3pp4mgihcr159lrb1w84vj8020c8zkipkdq8s")))

(define-public crate-numeric-algs-0.3.0 (c (n "numeric-algs") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1l2x408fxinbx5b99g92az70cb2zcgqw5845fd3qhbdr7fniwb6a")))

(define-public crate-numeric-algs-0.4.0 (c (n "numeric-algs") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.21") (o #t) (d #t) (k 0)))) (h "05ymndp209c7da6aw84p4rm444ph4q18c5ydksz3migc2h1vlqk2")))

(define-public crate-numeric-algs-0.4.1 (c (n "numeric-algs") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.21") (o #t) (d #t) (k 0)))) (h "054grkn221drgn1lkk9x3pk1280m2kxg2dgxwhnqwcj93g1kyw3c")))

(define-public crate-numeric-algs-0.5.0 (c (n "numeric-algs") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1a25fp1782g2yfcm1437l1mpdh39qv7vi27rnawmxq3znyk9py46")))

(define-public crate-numeric-algs-0.5.1 (c (n "numeric-algs") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.27") (o #t) (d #t) (k 0)))) (h "0z3z1lv0clhb55yri4hcn80pkc65v6cdxvq4a13ifzdr6p7rnp5n")))

