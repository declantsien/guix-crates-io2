(define-module (crates-io nu me numerilib) #:use-module (crates-io))

(define-public crate-numerilib-0.1.0 (c (n "numerilib") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0j0qrr73apijxif7gx1almz9fnqz13xvwxbv7qpqmqymrbn0r681")))

(define-public crate-numerilib-0.1.1 (c (n "numerilib") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1sb23j1alnj5n3cb0cy16902i9v6lmgzvgdr30q4wkh8c25gq90w")))

(define-public crate-numerilib-0.1.2 (c (n "numerilib") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1zphchw5v2lxacbby8i6b7l2pp005b5sja6ca09x0s3471ds8c72")))

