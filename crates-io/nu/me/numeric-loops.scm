(define-module (crates-io nu me numeric-loops) #:use-module (crates-io))

(define-public crate-numeric-loops-0.1.0 (c (n "numeric-loops") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "1lish7r86jjs5831y5sv990k6qgpb96blmhnrc0lm3xic1d7a00h") (f (quote (("use_nightly"))))))

(define-public crate-numeric-loops-0.1.1 (c (n "numeric-loops") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "17pvhnwjsxxprh7x2crgdcibn3gcfhl2vy5jwk9kzqhcymn5rgnc") (f (quote (("use_nightly"))))))

