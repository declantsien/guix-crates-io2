(define-module (crates-io nu me numeronym) #:use-module (crates-io))

(define-public crate-numeronym-1.2.0 (c (n "numeronym") (v "1.2.0") (h "1y8bik79r5s4dvvdwify1syihn91mlakrgjvgz3bj5pws07dbvda")))

(define-public crate-numeronym-1.3.0 (c (n "numeronym") (v "1.3.0") (h "1ii5nymg9wya4fgzqi5qhmabxkpb1nnyyp5nf5xnl17wpd91kv9y")))

(define-public crate-numeronym-1.3.1 (c (n "numeronym") (v "1.3.1") (h "1c8b9db5h6fv1lywpixickv176jv6m4yvh82kryg0aydi5hn0bk0")))

(define-public crate-numeronym-1.4.1 (c (n "numeronym") (v "1.4.1") (h "0np5qh39siaw5hssmzqr04g8rc61wsszp3iwjp3mnwh12qlqd8bl")))

(define-public crate-numeronym-1.4.2 (c (n "numeronym") (v "1.4.2") (h "1sa976rbvfv6icp5ll3bwz9qsjndkhcanwzd9204rzg555p7iqvr")))

