(define-module (crates-io nu me numeric-sort) #:use-module (crates-io))

(define-public crate-numeric-sort-0.1.0 (c (n "numeric-sort") (v "0.1.0") (d (list (d (n "no-std-compat") (r "^0.4.1") (d #t) (k 0)))) (h "0dwfcxg4sdv170a4g11wdlqb0frmjqh67qk024bjijckynnw9wm5") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-numeric-sort-0.1.1 (c (n "numeric-sort") (v "0.1.1") (h "06rkri77hxlglnd03kq82qlkq9xs1a0y64rqqq6i1riabmbyv02i") (f (quote (("default" "alloc") ("alloc"))))))

