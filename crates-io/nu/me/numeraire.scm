(define-module (crates-io nu me numeraire) #:use-module (crates-io))

(define-public crate-numeraire-1.3.0 (c (n "numeraire") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "04dqxnybf9a71nc37v7137nacf5l7dsjy9xvgn8hz6jizmw4qw9j")))

(define-public crate-numeraire-1.3.1 (c (n "numeraire") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0msbphqwd3nk369zmxqvi34r2n2q2sv50y9vxl0cfkf2sb1s1sz2")))

