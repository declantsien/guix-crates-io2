(define-module (crates-io nu me numext-constructor) #:use-module (crates-io))

(define-public crate-numext-constructor-0.1.0 (c (n "numext-constructor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16x7cb58fz0m3r28gij7aw3696am32inkcr9rdn36pgwrvhz16fz")))

(define-public crate-numext-constructor-0.1.1 (c (n "numext-constructor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "064xca2fps9x231bgxxhyscdsd7fmbyrwhl5m3rf4win30nbv0xc") (y #t)))

(define-public crate-numext-constructor-0.1.2 (c (n "numext-constructor") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08wfpsjay3zai41rnvn6d6p1y95pldmq8xr29sn1413wmnnqv4qf")))

(define-public crate-numext-constructor-0.1.3 (c (n "numext-constructor") (v "0.1.3") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xg75p23z7r2hll2wlpa8zy52h89hvg3qs679l27c89zrp18j1zf")))

(define-public crate-numext-constructor-0.1.4 (c (n "numext-constructor") (v "0.1.4") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08kmrjzswx5sfvddzq79z89ambbz7maslvskp7ama3y57sqq6fcq")))

(define-public crate-numext-constructor-0.1.5 (c (n "numext-constructor") (v "0.1.5") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "071xcvy6fczcvzigxlhw5w94ylcz8daz3cjcslwbi0r3lsd0q1dn")))

(define-public crate-numext-constructor-0.1.6 (c (n "numext-constructor") (v "0.1.6") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qc6z4m35hdz1qw3hsjgrq1xh32yixzdgk8md06837vj8kqf07v2")))

