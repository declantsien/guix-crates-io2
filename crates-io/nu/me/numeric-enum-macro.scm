(define-module (crates-io nu me numeric-enum-macro) #:use-module (crates-io))

(define-public crate-numeric-enum-macro-0.1.0 (c (n "numeric-enum-macro") (v "0.1.0") (h "0a13zbvwin0x0bn1xdc4ln6w3cdfmz35pkmnk5l8avqs6b66b4zi") (y #t)))

(define-public crate-numeric-enum-macro-0.1.1 (c (n "numeric-enum-macro") (v "0.1.1") (h "1im3k7a9j200fwmjimd8xdsx0kn1wkm76fzcp5344n9qxw7dm15s")))

(define-public crate-numeric-enum-macro-0.2.0 (c (n "numeric-enum-macro") (v "0.2.0") (h "051pbb8snh404lmp5vm0ys8n8ad44kps23khisa95da6dgdln3ih")))

