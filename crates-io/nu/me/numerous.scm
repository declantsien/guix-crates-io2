(define-module (crates-io nu me numerous) #:use-module (crates-io))

(define-public crate-numerous-0.1.0 (c (n "numerous") (v "0.1.0") (d (list (d (n "gcd") (r "^2.3.0") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "prime_factorization") (r "^1.0.3") (d #t) (k 0)))) (h "1j6xzx3zrx0irxl2v43b9crwxbq8z5b0q0nx4g2qrjhgpqa8v68m")))

