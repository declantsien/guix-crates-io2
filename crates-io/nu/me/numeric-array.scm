(define-module (crates-io nu me numeric-array) #:use-module (crates-io))

(define-public crate-numeric-array-0.1.0 (c (n "numeric-array") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.8.2") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "11xvfh0ivrnjvyk7a1siamn2fvrhp9mdllfm841dw4axzl8axhph")))

(define-public crate-numeric-array-0.1.1 (c (n "numeric-array") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.8.2") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0w6ppk4d19qz9p3sl2p5jjqjnwb85r7rd1ispsqlp3hn947aqi0i")))

(define-public crate-numeric-array-0.1.2 (c (n "numeric-array") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.8.2") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1mzxq8ynxxmrlh8k94x4llql1ii27r2f2ccc1aabl70znv1jnh3b")))

(define-public crate-numeric-array-0.1.3 (c (n "numeric-array") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.8.2") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1v3pxq54iljb6w5vprm7ab9i51sizf90scj6z542jbl99pp1sbz6")))

(define-public crate-numeric-array-0.1.4 (c (n "numeric-array") (v "0.1.4") (d (list (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "18gcl6zjrks4yh666mp69a3cg7bwnj4n68i0jz9wsbis874m07jl")))

(define-public crate-numeric-array-0.1.5 (c (n "numeric-array") (v "0.1.5") (d (list (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0wzdcs8d5dryg3rz2mhzdcbgf1vpjn71bmqy79ycnqf4sc0wjwvb")))

(define-public crate-numeric-array-0.1.6 (c (n "numeric-array") (v "0.1.6") (d (list (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "09a0gyxpq6aya362vwrs86x6kdhrd1kyka9bwyh2lbpnm0qbh111")))

(define-public crate-numeric-array-0.2.0 (c (n "numeric-array") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1nsylvwm4c6c3fh6cdrnmyx5xg6pqd96ppbvdxqzg0l5r2x3g37y")))

(define-public crate-numeric-array-0.2.1 (c (n "numeric-array") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "12yfp74133cc82rffidq71hv5klw3wc8dl2dnv208x8drjss6427")))

(define-public crate-numeric-array-0.2.2 (c (n "numeric-array") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "13sms5gcnl2clzql11vy0y07q5cshh7bm8bbyqwmslvp88jq1w03")))

(define-public crate-numeric-array-0.2.3 (c (n "numeric-array") (v "0.2.3") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "08xsairvq7adlppjgvyp69ply9w8bpr0xzl56m4js6k14llh4qry")))

(define-public crate-numeric-array-0.2.4 (c (n "numeric-array") (v "0.2.4") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0242jdbqkh83wajgzwaiafdcnzgwwaps6cjxhsx8fbv456h3n0na") (y #t)))

(define-public crate-numeric-array-0.2.5 (c (n "numeric-array") (v "0.2.5") (d (list (d (n "generic-array") (r "^0.11.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "08ldqp3b0m626mbr6aj4y0cpvldv7pkygrr1sjkxbli09mjnh84m")))

(define-public crate-numeric-array-0.3.0 (c (n "numeric-array") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "18mw0r207ybgg35b6h3kkd59q3hhzzqh910b7rh15ivlj0k8d91z")))

(define-public crate-numeric-array-0.3.1 (c (n "numeric-array") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1ki8p0l601cc65vmdv0bgxa1d8w6nd095qrvkvj9i5r6ni2xskv6") (f (quote (("serde1" "serde" "generic-array/serde"))))))

(define-public crate-numeric-array-0.4.1 (c (n "numeric-array") (v "0.4.1") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "16xn4yclgd5m5b8bivkxymlhy95v75pbd8jx9fcklpv6nwlf22bc") (f (quote (("serde1" "serde" "generic-array/serde"))))))

(define-public crate-numeric-array-0.4.2 (c (n "numeric-array") (v "0.4.2") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "10a1x77hcq7lgd4vn6yp5f8ca3d9rfqx2vijl4zjvc5pc1h516ns") (f (quote (("serde1" "serde" "generic-array/serde"))))))

(define-public crate-numeric-array-0.5.0 (c (n "numeric-array") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0dk93fi9h9iyjgws6wrizvxkq66jnrw0xgrfc57qsn3asv3rvcsf") (f (quote (("serde1" "serde" "generic-array/serde"))))))

(define-public crate-numeric-array-0.5.1 (c (n "numeric-array") (v "0.5.1") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y9026wjq274yzcclnl38h81bwv90g7r12nsyph7zs0rcxsn96bp") (f (quote (("serde1" "serde" "generic-array/serde"))))))

(define-public crate-numeric-array-0.5.2 (c (n "numeric-array") (v "0.5.2") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kscwsp85g07mvx60mx5ry5lw9sqxmfxx82av1g6hfli4yrajn9c") (f (quote (("serde1" "serde" "generic-array/serde"))))))

