(define-module (crates-io nu me numeric-lut) #:use-module (crates-io))

(define-public crate-numeric-lut-0.1.0 (c (n "numeric-lut") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1z71r8hr0q3v9nx7335jvfmwyxwc7li7mnap7pw7izaf3qknh6da")))

