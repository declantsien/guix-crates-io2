(define-module (crates-io nu me numerals) #:use-module (crates-io))

(define-public crate-numerals-0.1.0 (c (n "numerals") (v "0.1.0") (h "0rd3hlgy2nh4d632n2vqyinrm3m6607y5fn6n22f7cw4chb06i46")))

(define-public crate-numerals-0.1.1 (c (n "numerals") (v "0.1.1") (h "19d60r48n68qwg180k2wf9qf4m5ybg5n6dzzy7fmcvgxiq2cfdk0")))

(define-public crate-numerals-0.1.2 (c (n "numerals") (v "0.1.2") (h "13nf06jpja92j0c08zlzp0h80fqarl4gcjibjnfdhlr7dv85kwl7")))

(define-public crate-numerals-0.1.3 (c (n "numerals") (v "0.1.3") (h "06z4gbc0q6djqkmwrq2b6ad9nsi45hzpdhfjc8lb69xa20rf9bh1")))

(define-public crate-numerals-0.1.4 (c (n "numerals") (v "0.1.4") (h "0cdx6yf5zcx2nvmzavr4qk9m35ha6i2rhy5fjxgx2wm7fq9y4nz2")))

