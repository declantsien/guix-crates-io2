(define-module (crates-io nu ur nuuro_build) #:use-module (crates-io))

(define-public crate-nuuro_build-0.0.1 (c (n "nuuro_build") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1kjfpyjq83ng3w7815cclr5vbgi4m1i0d1a94pwv0gjmvvm8qn8x")))

(define-public crate-nuuro_build-0.0.3 (c (n "nuuro_build") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "190z4p0dw8ydxfqhn48sja8j4qiwa9w4c2gd4v527yzj8gmykyf8")))

(define-public crate-nuuro_build-0.0.4 (c (n "nuuro_build") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1hljrcpy3vbd93jaaw6nn63a3xkghzvqy3rm6qqvsy2lkva5hlkd")))

(define-public crate-nuuro_build-0.0.5 (c (n "nuuro_build") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1rm9q0787sz94q059ngbfm7vlsdn9kkf31zfgwhklxs3zbrhc8ag")))

(define-public crate-nuuro_build-0.1.0 (c (n "nuuro_build") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "03lbykkafajccm345ag7dvhxyfrdg24ni68zxfqa7dbndb131ic0")))

(define-public crate-nuuro_build-0.1.1 (c (n "nuuro_build") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1nq6f5kmn4k4rij1f59dy4hh1y8lyiif8np17n6i9j2hjgk0xnsv")))

(define-public crate-nuuro_build-0.1.2 (c (n "nuuro_build") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0gpjmhp40cx2734d15zb80rdw5lvgk1hif0wmqj8z561r1n7rq6x")))

(define-public crate-nuuro_build-0.1.4 (c (n "nuuro_build") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "06ll5n13hmksx6l5pkq8hk5p3cffw9rrvpkf7srpwn38xhh154cl")))

(define-public crate-nuuro_build-0.1.5 (c (n "nuuro_build") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1cy32982xa401qgja5j6yj32ij3llbjdggyq2vzlhc3rnq21g412")))

