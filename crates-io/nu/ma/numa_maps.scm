(define-module (crates-io nu ma numa_maps) #:use-module (crates-io))

(define-public crate-numa_maps-0.0.0 (c (n "numa_maps") (v "0.0.0") (h "00prxzb1skmkqz76m2iwglh65dzb0bmcv4p7rqvc35isf9al3lq0") (r "1.72")))

(define-public crate-numa_maps-0.1.0 (c (n "numa_maps") (v "0.1.0") (h "17wivha6blyskyk0k7mk8j0afxq37ykfbqz38h9djdgn0dpg56li") (r "1.72")))

