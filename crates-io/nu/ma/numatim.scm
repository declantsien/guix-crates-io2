(define-module (crates-io nu ma numatim) #:use-module (crates-io))

(define-public crate-numatim-0.2.1 (c (n "numatim") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bri4yxjb5ngc3bdy2fgl2dna6aslra21669awqx8k0dv52fmlzp")))

(define-public crate-numatim-0.2.2 (c (n "numatim") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d4xgrjwxj6knxivrlr8avdzrqy6m44mz5wa3mrkqnbzyibd8byr")))

