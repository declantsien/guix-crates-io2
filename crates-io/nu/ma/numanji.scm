(define-module (crates-io nu ma numanji) #:use-module (crates-io))

(define-public crate-numanji-0.1.2 (c (n "numanji") (v "0.1.2") (d (list (d (n "allocator-suite") (r "^0.1") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "175p8w1z2dx3ic8bpk2fgprdldb9xsd2k6pk4lvhivwm06y6rna1")))

(define-public crate-numanji-0.1.3 (c (n "numanji") (v "0.1.3") (d (list (d (n "allocator-suite") (r "^0.1.6") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "14ba9im2pm4www2xmryg0rs1148cww0f6sh2yim83h9zdjkvdndd")))

(define-public crate-numanji-0.1.4 (c (n "numanji") (v "0.1.4") (d (list (d (n "allocator-suite") (r "^0.1.6") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1lb3zj2hlfiz4qdgaxnzjk1lz5hjyp7s8ggj8r1zgvvkazypaknh")))

(define-public crate-numanji-0.1.5 (c (n "numanji") (v "0.1.5") (d (list (d (n "allocator-suite") (r "^0.1.6") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0dzwxigs9na5zjvny0p8scz4w1qglphgxb97smcm2ddrry88a0n0")))

