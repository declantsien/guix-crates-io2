(define-module (crates-io nu ma numas) #:use-module (crates-io))

(define-public crate-numas-0.1.0 (c (n "numas") (v "0.1.0") (h "17k5p90g2nzbpx10qa37zb22lldic8yg1jqi0fv0a6q7k5icwizy")))

(define-public crate-numas-0.1.1 (c (n "numas") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "068lqy69k706hn90n4v09lmazjwhraj79bq2cywc71hs9vady68d")))

(define-public crate-numas-0.1.2 (c (n "numas") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1wyyh0rm2j0wj1vrck5lxy9pyhv5d17851az2fab8ncd80pzr92j")))

(define-public crate-numas-0.1.3 (c (n "numas") (v "0.1.3") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1fc9dm8rawzaws9xzpm2mskwci9pijjryhl5mb3g9zhrzfzk70sb")))

(define-public crate-numas-0.1.4 (c (n "numas") (v "0.1.4") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "14z7qq4i59n80fl17awid38gilbqnyndxxw7dnhcrhmmvhip7smh")))

(define-public crate-numas-0.1.5 (c (n "numas") (v "0.1.5") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1clmx2z4kza4x8agrjhqx0qgqbvj4pp6dq4375nbc12mls8zgzbc")))

(define-public crate-numas-0.1.6 (c (n "numas") (v "0.1.6") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "159icj0n7zxn49dsrgz00kljg9b2fckx4dh1adi7c6b8jjs3smh7")))

(define-public crate-numas-0.1.7 (c (n "numas") (v "0.1.7") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0i2xzlkk8mra6zqv2axw5j0ppx7gpwi3xwfzpzsscl8ycq8fjnvv")))

(define-public crate-numas-0.1.8 (c (n "numas") (v "0.1.8") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0kq0167aq90z01b09zqhiv9pibrwfycsgd44x8p6q9n6zw8vh3ix")))

(define-public crate-numas-0.1.9 (c (n "numas") (v "0.1.9") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "18z3s3l9as7xkk2s8bapnlgqn2r2cw11psiyddmrb2ngxn2rqrrv")))

(define-public crate-numas-0.2.0 (c (n "numas") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0qy11s9mwvdml9ah5qk4zmkirdz8h17km9nwl5fb0hxzzv8d1pvm")))

(define-public crate-numas-0.2.1 (c (n "numas") (v "0.2.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0za0wz1565ampap9cpplhf0va1amwjx0xzr51cmvnbad2xzm59fx")))

(define-public crate-numas-0.2.2 (c (n "numas") (v "0.2.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0inxzbckvj126x2njns312fnbgraqw78nplzzwg7sikmb5ml5y3r")))

(define-public crate-numas-0.2.3 (c (n "numas") (v "0.2.3") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1g0q811vn9x2f6di278dpjgaxms57imm1b9x4nazpqfyhn6lfri2")))

(define-public crate-numas-0.2.4 (c (n "numas") (v "0.2.4") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0nvvgf8c4i7c2m0qvy2lmgfrp9scj7787k1z2pd8zgddsj496x3d")))

(define-public crate-numas-0.2.5 (c (n "numas") (v "0.2.5") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1yrqjgsv9g0dfz05zw95qqsma3v7614pyc0xrnd2hijamg41r4i0")))

(define-public crate-numas-0.2.7 (c (n "numas") (v "0.2.7") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "01dvlz6g96w631gh1qwkrdac6ky34mg10fz7l2kxlw20ph4ry7n2")))

