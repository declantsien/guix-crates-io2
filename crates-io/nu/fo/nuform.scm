(define-module (crates-io nu fo nuform) #:use-module (crates-io))

(define-public crate-nuform-0.1.0 (c (n "nuform") (v "0.1.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (f (quote ("serde"))) (k 0)))) (h "0arw88l3yy1ffiamrcmajsfgv3yjlv6vahxmd9irvf1lic17xy5v")))

(define-public crate-nuform-0.1.2 (c (n "nuform") (v "0.1.2") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.2") (f (quote ("serde"))) (k 0)) (d (n "toml") (r "^0.3") (k 1)))) (h "1cqv7pwprxpppq4cd6x5g3xq09pgf0xbg5k7a0npkvswspsdmq1q")))

