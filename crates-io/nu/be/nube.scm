(define-module (crates-io nu be nube) #:use-module (crates-io))

(define-public crate-nube-0.0.1 (c (n "nube") (v "0.0.1") (d (list (d (n "bls12_381") (r "^0.6") (d #t) (k 0)) (d (n "ff") (r "^0.11") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1zz7xww9zn4h0i4y350zzwy4igwkidmifakg05ls7nbfbxz02jbq")))

