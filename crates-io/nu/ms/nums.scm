(define-module (crates-io nu ms nums) #:use-module (crates-io))

(define-public crate-nums-0.1.0 (c (n "nums") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00avpznwb12fb62rhhwy1yvq61lawky2lbq2m14zr37v4pwp8g6g")))

(define-public crate-nums-1.0.0 (c (n "nums") (v "1.0.0") (d (list (d (n "num-bigint") (r "^0.4.5") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-forest") (r "^0.1.6") (f (quote ("ansi" "smallvec"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("std" "env-filter"))) (d #t) (k 0)))) (h "02yan34dc2sd6m2b9myxd54drxf21dl2mzpqqi0ww444m104nanp")))

