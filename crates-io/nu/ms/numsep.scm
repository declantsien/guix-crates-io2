(define-module (crates-io nu ms numsep) #:use-module (crates-io))

(define-public crate-numsep-0.1.0 (c (n "numsep") (v "0.1.0") (d (list (d (n "slicestring") (r "^0.1.3") (d #t) (k 0)))) (h "124vv6ii8civxv6zh9r1f41rxkrl562b6x5710hvjqav4xykvcc6")))

(define-public crate-numsep-0.1.1 (c (n "numsep") (v "0.1.1") (d (list (d (n "slicestring") (r "^0.1.3") (d #t) (k 0)))) (h "1d2337cbjpiqjsj811ks7ihyywva33ji309al4h75vx5dyajd552")))

(define-public crate-numsep-0.1.12 (c (n "numsep") (v "0.1.12") (d (list (d (n "slicestring") (r "^0.1.3") (d #t) (k 0)))) (h "08nsjxg3qcdyn9m969pfb76psr8v0cva3js33zxlwc9cw71ljp5d")))

