(define-module (crates-io nu mr numrs) #:use-module (crates-io))

(define-public crate-numrs-0.1.0 (c (n "numrs") (v "0.1.0") (h "01chk4bql8kkapxxv47f0mq139jfhc8lnwsjll69xp5l17nqn2f1")))

(define-public crate-numrs-0.2.0 (c (n "numrs") (v "0.2.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1cp4b6cgi6286g3jwm3y90dra39i76bnfp5dawb3ksdvxj75gxkv")))

