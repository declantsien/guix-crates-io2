(define-module (crates-io nu mr numrepr) #:use-module (crates-io))

(define-public crate-numrepr-0.1.0 (c (n "numrepr") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xqkw27sr4cgnylw8bndk2xxh8nbiypw57yqzhhadhn7dajp5wh0")))

