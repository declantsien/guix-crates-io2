(define-module (crates-io nu e- nue-macros) #:use-module (crates-io))

(define-public crate-nue-macros-0.0.2 (c (n "nue-macros") (v "0.0.2") (d (list (d (n "aster") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 2)) (d (n "pod") (r "^0.0.3") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "quasi") (r "^0.3") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.3") (d #t) (k 0)))) (h "02qznrspkaxvf91n9lgs8hfrxwkyhsdf7p8b2sks1ndv368nxx4k")))

(define-public crate-nue-macros-0.0.3 (c (n "nue-macros") (v "0.0.3") (d (list (d (n "aster") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 2)) (d (n "pod") (r "^0.0.3") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "quasi") (r "^0.3") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.3") (d #t) (k 0)))) (h "08ml8z67nmqv261k3y82q1k4rj6g8fpqfj83n3prazwvkw44y4fh")))

(define-public crate-nue-macros-0.0.4 (c (n "nue-macros") (v "0.0.4") (d (list (d (n "aster") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 2)) (d (n "pod") (r "^0.0.3") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "quasi") (r "^0.3") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.3") (d #t) (k 0)))) (h "1sy6s79nyfnmpcmj8b9wnpk754g28p3ikgh8jj7cqqqvlccm2vg5")))

(define-public crate-nue-macros-0.0.5 (c (n "nue-macros") (v "0.0.5") (d (list (d (n "aster") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 2)) (d (n "pod") (r "^0.0.3") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "quasi") (r "^0.3") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.3") (d #t) (k 0)))) (h "0pnxjwzxg9x3kfilgvfm835k9h6cfbd84abwr6shzb7f4d49l3sn")))

(define-public crate-nue-macros-0.1.0 (c (n "nue-macros") (v "0.1.0") (d (list (d (n "nue-codegen") (r "^0.1") (f (quote ("unstable"))) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 2)) (d (n "pod") (r "^0.1") (f (quote ("unstable"))) (d #t) (k 2)))) (h "18ajzpnpqprsp6l1ad4psyv9v0am4x40v25wp8nz9rd763gnscwv")))

(define-public crate-nue-macros-0.2.0 (c (n "nue-macros") (v "0.2.0") (d (list (d (n "nue") (r "^0.2") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "nue-codegen") (r "^0.2") (f (quote ("unstable"))) (k 0)) (d (n "pod") (r "^0.2") (f (quote ("unstable"))) (d #t) (k 2)))) (h "1640f4cpryrjzs3fcjxs49xr14vgf21hhrim9cc721ghm8gr4jqz")))

(define-public crate-nue-macros-0.3.0 (c (n "nue-macros") (v "0.3.0") (d (list (d (n "nue") (r "^0.3") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "nue-codegen") (r "^0.3") (f (quote ("unstable"))) (k 0)) (d (n "pod") (r "^0.3") (f (quote ("unstable"))) (d #t) (k 2)))) (h "1639x6yj7kymdivlrr924znbig4b963q6gn3j4bksqvziqhqc6z8")))

