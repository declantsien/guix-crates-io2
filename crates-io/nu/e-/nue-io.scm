(define-module (crates-io nu e- nue-io) #:use-module (crates-io))

(define-public crate-nue-io-0.0.1 (c (n "nue-io") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)))) (h "0ml5nyr3wc77wnx16hclhjzb7aq31ch6x33mqp6izxq2z1y3vh7m")))

(define-public crate-nue-io-0.1.0 (c (n "nue-io") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)))) (h "09hbfi1wxg2yfqm5a0cwhk19x06qpygmmfj35bll9lbqvn3dmak0")))

(define-public crate-nue-io-0.2.0 (c (n "nue-io") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "0z99pi2ilwb1sk9ldbjqhphmi2kp7adbihmq77qm1xikbmfizyg2")))

(define-public crate-nue-io-0.3.0 (c (n "nue-io") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "16jv69k45gy4q9v4h6h0wng9q5xxxmd473ng22yvfgillcffzmax")))

