(define-module (crates-io nu mg numguessrust) #:use-module (crates-io))

(define-public crate-NumGuessRust-0.1.9 (c (n "NumGuessRust") (v "0.1.9") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "01gs9q9rz14bzzn2xg62yhq31wvgbl70bvp7h8y0ap95yn5fn3lz")))

(define-public crate-NumGuessRust-0.1.10 (c (n "NumGuessRust") (v "0.1.10") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "1lm704lplxv39ccvfv14fdg6flidf97qim8wh224rbv735v67r0x")))

(define-public crate-NumGuessRust-0.1.11 (c (n "NumGuessRust") (v "0.1.11") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "089zhk7ga21cw051vafy1k49a7zb4rf8lb9hrniv97666p906py1")))

(define-public crate-NumGuessRust-0.1.12 (c (n "NumGuessRust") (v "0.1.12") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "1q2d1d619f63dxii25q1b4ixdsi5j76dy7a6dwrw5qsackvnxnxv")))

(define-public crate-NumGuessRust-0.1.13 (c (n "NumGuessRust") (v "0.1.13") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "0ldf7jvfmw7zfwalbsk91kbc7r38d9925k55ics541vr7nda4c2p")))

(define-public crate-NumGuessRust-0.1.16 (c (n "NumGuessRust") (v "0.1.16") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "0i5vqf2mayypx461bxgcf5h2hv8y9xl29q6v9xdm8nl2v9r9g77b")))

(define-public crate-NumGuessRust-0.1.17 (c (n "NumGuessRust") (v "0.1.17") (d (list (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "19w8glz9l1ihq1qlcrhc2dvqbybz2c4jhnjb4nnj5shlmz9k5xks")))

