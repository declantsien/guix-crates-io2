(define-module (crates-io nu st nustify) #:use-module (crates-io))

(define-public crate-nustify-0.2.1 (c (n "nustify") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "05rxd7qhqai62rdphnz12v7409j7rv558fp8spqfzfdl5n0wf4gf") (f (quote (("imgur" "reqwest/multipart"))))))

(define-public crate-nustify-0.2.2 (c (n "nustify") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0fpz35nvqvs36rs4imj2shk8a6iz3s1x1fip4azqd1mb7qlmw3cw") (f (quote (("imgur" "reqwest/multipart"))))))

