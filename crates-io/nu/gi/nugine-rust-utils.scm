(define-module (crates-io nu gi nugine-rust-utils) #:use-module (crates-io))

(define-public crate-nugine-rust-utils-0.1.0 (c (n "nugine-rust-utils") (v "0.1.0") (h "1lqp3ispkq4b3i1qlwg0jr4n186czb94hjl2fxsrzs2zjkqds47s") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t)))

(define-public crate-nugine-rust-utils-0.1.1 (c (n "nugine-rust-utils") (v "0.1.1") (h "1c4503j5x35zx0zgq16wbsv1kf6sashavczykmw7v9g3hzwcr7nm") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t)))

(define-public crate-nugine-rust-utils-0.1.2 (c (n "nugine-rust-utils") (v "0.1.2") (d (list (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "15k4ayphsccnj99x2p7saj2q6q3z1d24r00ycqfr5pgdz9y6g8cr") (f (quote (("default" "std") ("ascii") ("alloc")))) (y #t) (s 2) (e (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.1.3 (c (n "nugine-rust-utils") (v "0.1.3") (d (list (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0vgp6czj9a2c0xcbyx0lwddgmcirmrbfiidhkz7zwygshf3az22k") (f (quote (("default" "std") ("ascii") ("alloc")))) (y #t) (s 2) (e (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.1.4 (c (n "nugine-rust-utils") (v "0.1.4") (d (list (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "05zlg8pnm15c7dmwxxkjaqa7i96n1s316kgbz8azqjnp2ih3x91x") (f (quote (("default" "std") ("ascii") ("alloc")))) (y #t) (s 2) (e (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.2.0 (c (n "nugine-rust-utils") (v "0.2.0") (d (list (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0m254ayxbr8fw9y9zj1lzs5h4h98fasy6y4md5js371wvffdkl5f") (f (quote (("default" "std") ("ascii") ("alloc")))) (y #t) (s 2) (e (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.2.1 (c (n "nugine-rust-utils") (v "0.2.1") (d (list (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1ha99dzfxz01ikqzyg80h9gpmjzpmifq5wqlnwkbwrf7451574hb") (f (quote (("default" "std") ("ascii") ("alloc")))) (s 2) (e (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.3.0 (c (n "nugine-rust-utils") (v "0.3.0") (d (list (d (n "simdutf8") (r "^0.1.4") (k 0)))) (h "01161jsn9pjphc0jxaq7wvazlz7l426xkird973n120x3451507w") (f (quote (("std" "alloc" "simdutf8/std") ("default" "std") ("alloc"))))))

(define-public crate-nugine-rust-utils-0.3.1 (c (n "nugine-rust-utils") (v "0.3.1") (d (list (d (n "simdutf8") (r "^0.1.4") (k 0)))) (h "18mf0a47q6qqka6f02psw5zdgsf46c3y0w86rb3sjii2m77xkp04") (f (quote (("std" "alloc" "simdutf8/std") ("default" "std") ("alloc"))))))

