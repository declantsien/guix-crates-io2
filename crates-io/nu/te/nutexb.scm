(define-module (crates-io nu te nutexb) #:use-module (crates-io))

(define-public crate-nutexb-0.0.1 (c (n "nutexb") (v "0.0.1") (d (list (d (n "binwrite") (r "^0.1.5") (d #t) (k 0)) (d (n "ddsfile") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0zmb9pr027pjmvxp8pal1jzkd9znkls7s6dycj5ffihxd47ykgx0")))

(define-public crate-nutexb-0.0.2 (c (n "nutexb") (v "0.0.2") (d (list (d (n "binwrite") (r "^0.1.5") (d #t) (k 0)) (d (n "ddsfile") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1bw22083slmyhsdyaang06j853nabbbja2s9pvhm2sa0982l8vrr")))

(define-public crate-nutexb-0.0.3 (c (n "nutexb") (v "0.0.3") (d (list (d (n "binwrite") (r "^0.1.5") (d #t) (k 0)) (d (n "ddsfile") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "086g7970f7yk37pqdpkdi7j0x5w01x37ah9qadgy7lc8q4l7c2nb")))

(define-public crate-nutexb-0.0.4 (c (n "nutexb") (v "0.0.4") (d (list (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.1.5") (d #t) (k 0)) (d (n "ddsfile") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "1bs6q3ic81ffvr45hk5wliqxzs4ahp5gkjmf6jrmlnn3ws9b3njk")))

(define-public crate-nutexb-0.0.5 (c (n "nutexb") (v "0.0.5") (d (list (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.1.5") (d #t) (k 0)) (d (n "ddsfile") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "0lzq2dsfcjhya63666kw3fzlppvmylyb5d7612yxkws36av8p3zf")))

(define-public crate-nutexb-0.6.0 (c (n "nutexb") (v "0.6.0") (d (list (d (n "binrw") (r "^0.8.4") (d #t) (k 0)) (d (n "ddsfile") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.10") (o #t) (d #t) (k 0)) (d (n "tegra_swizzle") (r "^0.2.0") (d #t) (k 0)))) (h "0x5y5g5igcj0ymysk3xzkj4j2mkp1s99flpl11vj5m927qf9qs85")))

(define-public crate-nutexb-0.7.0 (c (n "nutexb") (v "0.7.0") (d (list (d (n "binrw") (r "^0.13") (d #t) (k 0)) (d (n "ddsfile") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (o #t) (d #t) (k 0)) (d (n "tegra_swizzle") (r "^0.3.0") (d #t) (k 0)))) (h "0qh878d7f9h05xzv1fzmrbi3wkcq27ccsbndk73lxiwblhmg7cfl")))

