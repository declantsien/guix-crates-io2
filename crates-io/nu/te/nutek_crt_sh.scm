(define-module (crates-io nu te nutek_crt_sh) #:use-module (crates-io))

(define-public crate-nutek_crt_sh-0.1.2 (c (n "nutek_crt_sh") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11qdj8kclv7faq5ppkp4kgm86yz3p076z5lbg8p6lp1k2f3c2zlk")))

