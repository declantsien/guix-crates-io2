(define-module (crates-io nu te nutek) #:use-module (crates-io))

(define-public crate-nutek-0.1.0 (c (n "nutek") (v "0.1.0") (h "1lyb01p40081i1pdi7yp2l6mgisps7d7dywardwhwn1i2ph8hl4d")))

(define-public crate-nutek-0.1.1 (c (n "nutek") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.2.6") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "run_shell") (r "^0.1.6") (d #t) (k 0)) (d (n "shiplift") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10010422qb0pn0i7yq9hrqcn3z1krsdz21srgy2a2jbvl808xl5i")))

(define-public crate-nutek-0.2.0 (c (n "nutek") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.6") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "run_shell") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4n41ms6i7b6f51gjfhbrjpb030xn5yhbz277ni2y3sqd447qip")))

(define-public crate-nutek-0.2.1 (c (n "nutek") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.6") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "run_shell") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a73m39b0mqns7lvbs39hfcrvdh8ig0wkl160aj7qmg6l0l2r6d6")))

(define-public crate-nutek-0.2.2 (c (n "nutek") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.6") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "run_shell") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xn30ad8k05vgrgbbjwplb4as1qwjzmdb8bcffyv98fbliailskr")))

