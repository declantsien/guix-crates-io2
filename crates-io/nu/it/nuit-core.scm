(define-module (crates-io nu it nuit-core) #:use-module (crates-io))

(define-public crate-nuit-core-0.0.1 (c (n "nuit-core") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s1j0dqmkjk71bysanxgk43v60qw6l90pqgaxcdxknyk9gzsvhpr")))

(define-public crate-nuit-core-0.0.2 (c (n "nuit-core") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wds0bwixw3lwi33c9xfw31j3pd0l7w6klgkzqszn37f8hygbxlv")))

(define-public crate-nuit-core-0.0.3 (c (n "nuit-core") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n37fcn8rry87gnz8hkwcadhh8fgj46bzr2pwg42wyya4lfxa6b7")))

(define-public crate-nuit-core-0.0.4 (c (n "nuit-core") (v "0.0.4") (d (list (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pyrmh2wd98r1hrwapc7r6g7i7sm9m5i7azyl61mrj1gqqq9yzjl")))

