(define-module (crates-io nu it nuit) #:use-module (crates-io))

(define-public crate-nuit-0.0.3 (c (n "nuit") (v "0.0.3") (d (list (d (n "nuit-bridge-swiftui") (r "^0.0.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nuit-core") (r "^0.0.3") (d #t) (k 0)) (d (n "nuit-derive") (r "^0.0.3") (d #t) (k 0)))) (h "0yd4jqvmgbianvnl7zj2ayh85fmdzhn1dr8ags0bbvb55r9npb9k")))

(define-public crate-nuit-0.0.4 (c (n "nuit") (v "0.0.4") (d (list (d (n "nuit-bridge-swiftui") (r "^0.0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nuit-core") (r "^0.0.4") (d #t) (k 0)) (d (n "nuit-derive") (r "^0.0.4") (d #t) (k 0)))) (h "0wk3gxacl7701la8b3k3jn53rq0lw3kcr8zbg9qcgqrr0n4hc98g")))

