(define-module (crates-io nu it nuit-bridge-swiftui) #:use-module (crates-io))

(define-public crate-nuit-bridge-swiftui-0.0.1 (c (n "nuit-bridge-swiftui") (v "0.0.1") (d (list (d (n "nuit-core") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0m8z3jl70m4m3qal1hzkkbjhghfdkb34gyxyz3050wv61q5l2awj")))

(define-public crate-nuit-bridge-swiftui-0.0.2 (c (n "nuit-bridge-swiftui") (v "0.0.2") (d (list (d (n "nuit-core") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0fzh1wdk1j9mgya2i5v2pk04dkza8akgmh46rv0lp6bk66f27riw")))

(define-public crate-nuit-bridge-swiftui-0.0.3 (c (n "nuit-bridge-swiftui") (v "0.0.3") (d (list (d (n "nuit-core") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "16qna8r4lfh3hxgg37cijdmplps76iz9nr0qbvbdhmg5c4i4pbkd")))

(define-public crate-nuit-bridge-swiftui-0.0.4 (c (n "nuit-bridge-swiftui") (v "0.0.4") (d (list (d (n "nuit-core") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1y0ah67zqrp77mqywq1l3z1lh6ijx98bqhcs3f7paaaqfs3p91k5")))

