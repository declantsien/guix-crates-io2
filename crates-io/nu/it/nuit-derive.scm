(define-module (crates-io nu it nuit-derive) #:use-module (crates-io))

(define-public crate-nuit-derive-0.0.3 (c (n "nuit-derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dfndznjl6gr5jyvprilipxnj8j4b49kzg0x8jl6n3w6fx0574rs")))

(define-public crate-nuit-derive-0.0.4 (c (n "nuit-derive") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1qa3r7f9fg3l387m99r2hag62a3wlbm9nzxg5m8njy8qrr7vh3nk")))

