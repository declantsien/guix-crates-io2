(define-module (crates-io nu ba nuban) #:use-module (crates-io))

(define-public crate-nuban-1.0.0 (c (n "nuban") (v "1.0.0") (h "0bkin8wd87ph2y17b1ad7pqj49v4vh4f2jgj9d421way0q337na3")))

(define-public crate-nuban-1.1.0 (c (n "nuban") (v "1.1.0") (h "0y8vihvwz48vyk0dhg7zk9ccamilh1bvz8qawhgs65hvyksjggxd")))

