(define-module (crates-io nu ba nuban_rust) #:use-module (crates-io))

(define-public crate-nuban_rust-0.1.0 (c (n "nuban_rust") (v "0.1.0") (h "1xqcrv4c1x585lkzs66wii25l59v4lrhrns5br5qh68d7za4z0wa")))

(define-public crate-nuban_rust-0.1.1 (c (n "nuban_rust") (v "0.1.1") (h "1aiwmx8xdka7dgma046kavri8bqmqq3a5bsgwsws58837z9ks05q")))

