(define-module (crates-io nu mo numopt) #:use-module (crates-io))

(define-public crate-numopt-0.1.0 (c (n "numopt") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1cm3cwfdgszjfhy9xc0pgnmy5gs2683a2hzg07la735kgj8cvfx8") (f (quote (("ipopt"))))))

(define-public crate-numopt-0.1.1 (c (n "numopt") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0dmm4qzm5f2qx88sfpqya0qay1kwlhfg7s1v9mqyhpmd3caqya71") (f (quote (("ipopt"))))))

(define-public crate-numopt-0.2.1 (c (n "numopt") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0vxkqkj11mwwfq75mhgr7918y5958x238gj63x8m0f5r30jd0czs") (f (quote (("ipopt"))))))

