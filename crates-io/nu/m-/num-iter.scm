(define-module (crates-io nu m- num-iter) #:use-module (crates-io))

(define-public crate-num-iter-0.1.32 (c (n "num-iter") (v "0.1.32") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0p74nj5c1mc33h9lx4wpmlmggmn5lnkhxv1225g0aix8d6ciqyi8")))

(define-public crate-num-iter-0.1.33 (c (n "num-iter") (v "0.1.33") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "07m4q41d3m998kdfjj5i31dbi868c4vl1ldp94iz2dmrswdqklgp")))

(define-public crate-num-iter-0.1.34 (c (n "num-iter") (v "0.1.34") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "00gyvw9qn24wpigfcfjq1vb2gkw1j8c18ysjxb8frd459z4gr1bl")))

(define-public crate-num-iter-0.1.35 (c (n "num-iter") (v "0.1.35") (d (list (d (n "num-integer") (r "^0.1.36") (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "14ymi6v12921983xhcyfvy2v74bdjbxzlmyxd4svcnas5kqns8jb") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-iter-0.1.36 (c (n "num-iter") (v "0.1.36") (d (list (d (n "num-integer") (r "^0.1.37") (k 0)) (d (n "num-traits") (r "^0.2.3") (k 0)))) (h "1i90gnx5acb5lxvdsjv03sggjnfhxbjq3vhizr5v2lg1p7d850pv") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.37 (c (n "num-iter") (v "0.1.37") (d (list (d (n "num-integer") (r "^0.1.38") (k 0)) (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "09115d12q1bcgig3ivnnbs8vz9kwqc78c0vvqm6ld9ci6aydngxg") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.38 (c (n "num-iter") (v "0.1.38") (d (list (d (n "autocfg") (r "^0.1.2") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.38") (k 0)) (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "096iysn15z8a71402nksvqlzsay9f8cq69qrawlpiwh97ybwzdsh") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.39 (c (n "num-iter") (v "0.1.39") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.38") (k 0)) (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "0bhk2qbr3261r6zvfc58lz4spfqjhvdripxgz5mks5rd85r55gbn") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.40 (c (n "num-iter") (v "0.1.40") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "005wif3bk23b5jdg7l0cprzqzyc4jg0xjyzyykciv2ci08581c6z") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.41 (c (n "num-iter") (v "0.1.41") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "17sb142lhmpsq17cf9wrffjh8vjk901axxf55565r6cgfiy6nvks") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.42 (c (n "num-iter") (v "0.1.42") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0ndd9wb9qar50fdr16xm3i1zk6h2g9br56nml2n22kd56y1iq0mj") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.43 (c (n "num-iter") (v "0.1.43") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0lp22isvzmmnidbq9n5kbdh8gj0zm3yhxv1ddsn5rp65530fc0vx") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128" "num-integer/i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-iter-0.1.44 (c (n "num-iter") (v "0.1.44") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (f (quote ("i128"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)))) (h "1aamy25jyys9rh6qmadmr9f3hd9qz7qr407xcd0jhmf4q0fc0sfq") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128") ("default" "std")))) (r "1.31")))

(define-public crate-num-iter-0.1.45 (c (n "num-iter") (v "0.1.45") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.46") (f (quote ("i128"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)))) (h "1gzm7vc5g9qsjjl3bqk9rz1h6raxhygbrcpbfl04swlh0i506a8l") (f (quote (("std" "num-integer/std" "num-traits/std") ("i128") ("default" "std")))) (r "1.31")))

