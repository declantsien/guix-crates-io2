(define-module (crates-io nu m- num-derive) #:use-module (crates-io))

(define-public crate-num-derive-0.1.36 (c (n "num-derive") (v "0.1.36") (d (list (d (n "compiletest_rs") (r "^0.2.2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "1166rrgnkmiqjlsgqx80269m57knn4fgwj1659jy9ql7l69j0vgi")))

(define-public crate-num-derive-0.1.37 (c (n "num-derive") (v "0.1.37") (d (list (d (n "compiletest_rs") (r "^0.2.2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "1n48dm0wc574kd0hqrgr01a0jkqpj9rbcmqb2jynsfwnjqx0n7i5")))

(define-public crate-num-derive-0.1.38 (c (n "num-derive") (v "0.1.38") (d (list (d (n "compiletest_rs") (r "^0.2.2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "03dl35gghxh6m4jh8ipm8b14m35cs8r8bs980j8bbmvfraxc9gl8")))

(define-public crate-num-derive-0.1.39 (c (n "num-derive") (v "0.1.39") (d (list (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "19pag5fxmnqhd1f58h8821133j8n17vykj3d1bjnpx5sa0jc7gf8")))

(define-public crate-num-derive-0.1.40 (c (n "num-derive") (v "0.1.40") (d (list (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "0m97d1vcnnqlqy1hs30079micsdsibqk32pygawni4g98nl4icjr")))

(define-public crate-num-derive-0.1.41 (c (n "num-derive") (v "0.1.41") (d (list (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.7.0") (d #t) (k 0)))) (h "15za63m8yi8rbw7vyic02rrw7npq48dkbp2nyb8l75s275k55s32")))

(define-public crate-num-derive-0.1.42 (c (n "num-derive") (v "0.1.42") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.7") (d #t) (k 0)))) (h "12ydwq3cyg1kaai67gl8z6xrl8ppriwlsx9aaa774kibjycxsl6w") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.1.43 (c (n "num-derive") (v "0.1.43") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.7") (d #t) (k 0)))) (h "0j2rnkrmaql5i7l98wrrp7v49psw2nmxr87cs43rlww8lv2p3l02") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.1.44 (c (n "num-derive") (v "0.1.44") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.7") (d #t) (k 0)))) (h "0mxvs0hlin1qkhdgh3f5sqgccr1rfbddr2mrf8vl83mkc6ffaiwq") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.0 (c (n "num-derive") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.7") (d #t) (k 0)))) (h "05xk21jlbqlxznffhf3h6351gmkslzyzxchi0q2mz7zjyj0alv23") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.1 (c (n "num-derive") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.8") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.7") (d #t) (k 0)))) (h "1wxwzzqcczhjiydwk4yfln2ix2j9v0lrpkazaxlrqd95xw022wrg") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.2 (c (n "num-derive") (v "0.2.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1rsxc4c0x6qg4nxbgmgxr8mg14yb2c2xf4vs1k9r7a9nbjvk2b0d") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.3 (c (n "num-derive") (v "0.2.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0z8yzrwdgigqy8y1dgp5gximqf5lpcjznwnmpzbh8bvwj1y89wca") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.4 (c (n "num-derive") (v "0.2.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0v9i3nn6251pcwkx8yifih67x084wmda3kx1x1ya71hvzp58zznr") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.2.5 (c (n "num-derive") (v "0.2.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1wnv7776fh4i40r3zfxcxcmm0dh029skx7gp4sjknz2kqm2hpzga") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.3.0 (c (n "num-derive") (v "0.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0imprwv8cs01k46g56ajlvc97dp8kz51y2vn6cp9jkw1c6r1b2qc") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.3.1 (c (n "num-derive") (v "0.3.1") (d (list (d (n "num") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ra66s2mym3x2h0zp1qgwzvgg1cil2lspxjzy3ihlnrdzcrn4fg0") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.3.2 (c (n "num-derive") (v "0.3.2") (d (list (d (n "num") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1czs5215ypgbwg0qgy2i515xj3vfcgm8fw7gi4gmwsyv3a2bj2bg") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.3.3 (c (n "num-derive") (v "0.3.3") (d (list (d (n "num") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gbl94ckzqjdzy4j8b1p55mz01g6n1l9bckllqvaj0wfz7zm6sl7") (f (quote (("full-syntax" "syn/full"))))))

(define-public crate-num-derive-0.4.0 (c (n "num-derive") (v "0.4.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0klzicgzfk7xx6ci9drfnqz9957j4p4idk46j2yhgirpyza0ysly") (r "1.56.0")))

(define-public crate-num-derive-0.4.1 (c (n "num-derive") (v "0.4.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "04j7mndk9p6nzl9j6zrf49r2cq3250h4ldcx40jv3y48mxwpddyg") (r "1.56.0")))

(define-public crate-num-derive-0.4.2 (c (n "num-derive") (v "0.4.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "00p2am9ma8jgd2v6xpsz621wc7wbn1yqi71g15gc3h67m7qmafgd") (r "1.56.0")))

