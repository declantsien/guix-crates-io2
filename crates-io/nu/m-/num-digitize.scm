(define-module (crates-io nu m- num-digitize) #:use-module (crates-io))

(define-public crate-num-digitize-0.1.0 (c (n "num-digitize") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0bjph55xab44b05cq8djq8xv7yjgnj0vsf7jb9y6bnmylbv4l947")))

(define-public crate-num-digitize-0.1.1 (c (n "num-digitize") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1cg45cj7cxgl298iw3zkxi4mlcpry5fhnxysmcpcdjb7210nqk43")))

(define-public crate-num-digitize-0.2.0 (c (n "num-digitize") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1iq28pqny1l537k8vz82knisj305bwicayif865hrsl7jmnr4xqf")))

(define-public crate-num-digitize-0.2.1 (c (n "num-digitize") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0ccl3qs7lyra0i8h77i5agb48dhx4fgp1spgr77cmw2a536kzg4n")))

(define-public crate-num-digitize-0.2.2 (c (n "num-digitize") (v "0.2.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0nyc433wm5zjj4wq7zdlis1scw8r5v1ggzhd2zzk9i6z6x7m5bq4")))

(define-public crate-num-digitize-0.3.0 (c (n "num-digitize") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1pkcxq6mwkk2ip379jjnb4azmf204kc82nlmgpqfwnvwjnz86lkr")))

(define-public crate-num-digitize-0.4.0 (c (n "num-digitize") (v "0.4.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0hj54zvmnydwk4rz75iz6pqs339vvcscwshpwx356vvk1az6rsy2")))

(define-public crate-num-digitize-0.4.1 (c (n "num-digitize") (v "0.4.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1qcs4jngzj1aqk1x3zg3xb29iab3b5crdgakf9rkicifsy6912am")))

(define-public crate-num-digitize-0.4.2 (c (n "num-digitize") (v "0.4.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1q4anq1xd6b8p81lw0gb4xlimp9nw6jv8yy8cnyxmn7yz5h46b37")))

