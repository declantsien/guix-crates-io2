(define-module (crates-io nu m- num-sign) #:use-module (crates-io))

(define-public crate-num-sign-0.1.0 (c (n "num-sign") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wnghx3xdpymwgdcpkiiaz70qhf7471krzd5czfcf33zj399rxwj")))

(define-public crate-num-sign-0.1.1 (c (n "num-sign") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06qbkwsvxvryxk9lxfajjgaj874br9dr3bg67qw56lw84rmgpx3k")))

(define-public crate-num-sign-0.1.2 (c (n "num-sign") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gcy1ilnjb5rk0fgv4f1w6r12xrkqj7072sw2fd1f5lk918fyd2l")))

