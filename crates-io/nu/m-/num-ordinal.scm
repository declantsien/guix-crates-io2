(define-module (crates-io nu m- num-ordinal) #:use-module (crates-io))

(define-public crate-num-ordinal-0.1.0 (c (n "num-ordinal") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1wa8i9fhxknk8rljnb77j9zq4r9l0k2nwafndxlxz5lbw78338p9")))

(define-public crate-num-ordinal-0.2.0 (c (n "num-ordinal") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1dpa30jamh9r2s0xw5yc0ywm5y31c28nyaymmc001wsgz9jly90b")))

