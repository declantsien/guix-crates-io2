(define-module (crates-io nu m- num-integer) #:use-module (crates-io))

(define-public crate-num-integer-0.1.32 (c (n "num-integer") (v "0.1.32") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "14pvaaawl0pgdcgh4dfdd67lz58yxlfl95bry86h28pjnfzxj97v")))

(define-public crate-num-integer-0.1.33 (c (n "num-integer") (v "0.1.33") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1df8r4jhi3hrmvy91cjr2na69arzihbrriphgv99gmyik08dzr11")))

(define-public crate-num-integer-0.1.34 (c (n "num-integer") (v "0.1.34") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0dxaz2jnqp5nl0kx16n6ssxaw4fdra9chk4v79waajhpz7v4n6pg")))

(define-public crate-num-integer-0.1.35 (c (n "num-integer") (v "0.1.35") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1fhg9m8m679k4yvc121jlvp9121bv6qhpfvf1rzs0j740s5jwifi")))

(define-public crate-num-integer-0.1.36 (c (n "num-integer") (v "0.1.36") (d (list (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1zp2qc5p9l1nmsi89lw5yx0lkalrmwf1v3y7hm4nfigv36invlpq") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-integer-0.1.37 (c (n "num-integer") (v "0.1.37") (d (list (d (n "num-traits") (r "^0.2.3") (k 0)))) (h "1627rcbcdikwva5cqfnkzgap2cvfznvl5n3zpsiwc0mmbf0idvrg") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.38 (c (n "num-integer") (v "0.1.38") (d (list (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "0igfj7ccyz2jn3xzgp5s374xcn4kpsrk3038nzbdk2aasrcfmh3a") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.39 (c (n "num-integer") (v "0.1.39") (d (list (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "1shc9hfykcywgd86h2w6939d436gpmx2pbqbay653w3p4s6m4gg8") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.40 (c (n "num-integer") (v "0.1.40") (d (list (d (n "autocfg") (r "^0.1.2") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "0x767krr52bjkxivgz3hg69i7jw46vwhyhwv876phh0qlg5gi2lb") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.41 (c (n "num-integer") (v "0.1.41") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.4") (k 0)))) (h "02dwjjpfbi16c71fq689s4sw3ih52cvfzr5z5gs6qpr5z0g58pmq") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.42 (c (n "num-integer") (v "0.1.42") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "1fpw8yr9xwsf3qrh91rm7mzqaiwlc2dmnalsxv9pr9w1klpacviz") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.43 (c (n "num-integer") (v "0.1.43") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0nw79ynfvw8br6yncv27pw65y2vw2z7m3kv9g2hinm1dcrz4ancd") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.44 (c (n "num-integer") (v "0.1.44") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "1nq152y3304as1iai95hqz8prqnc94lks1s7q05sfjdmcf56kk6j") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.45 (c (n "num-integer") (v "0.1.45") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "1ncwavvwdmsqzxnn65phv6c6nn72pnv9xhpmjd6a429mzf4k6p92") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-integer-0.1.46 (c (n "num-integer") (v "0.1.46") (d (list (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)))) (h "13w5g54a9184cqlbsq80rnxw4jj4s0d8wv75jsq5r2lms8gncsbr") (f (quote (("std" "num-traits/std") ("i128") ("default" "std")))) (r "1.31")))

