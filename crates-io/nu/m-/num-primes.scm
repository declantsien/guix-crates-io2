(define-module (crates-io nu m- num-primes) #:use-module (crates-io))

(define-public crate-num-primes-0.1.0 (c (n "num-primes") (v "0.1.0") (d (list (d (n "num") (r "^0.2.1") (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0z130w13a02gm0s3l6s62qzk39nijnq882s06p8wkddgjlmpfrwz")))

(define-public crate-num-primes-0.1.1 (c (n "num-primes") (v "0.1.1") (d (list (d (n "num") (r "^0.2.1") (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1sk41wdiai5mlb2rgvms4c16vrxndbxjbdbpi3al32iagcg3izfy")))

(define-public crate-num-primes-0.1.2 (c (n "num-primes") (v "0.1.2") (d (list (d (n "num") (r "^0.2.1") (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1cbrbfmmpgcjnwkr4sfbllbzavwndjpnr84sa3hpbr3bf669v9yw")))

(define-public crate-num-primes-0.2.0 (c (n "num-primes") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "1r47wq6lc02k9vwangkr1bzsqc902rfq3b9bnkh84pms82iwglc5")))

(define-public crate-num-primes-0.2.1 (c (n "num-primes") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "1n59n95p5y3n18lyns2syn0x0i7xmxwqdzdx5pbz3xins0w9kii6")))

(define-public crate-num-primes-0.3.0 (c (n "num-primes") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "02ihmdl25jvqbvwxv0wzyajjkvmvbaqdhcdgnlv0n40nlrcm9wiq")))

