(define-module (crates-io nu m- num-complex) #:use-module (crates-io))

(define-public crate-num-complex-0.1.32 (c (n "num-complex") (v "0.1.32") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "045qilhmjs3sqvqzhwbfqvvb3aqxfvvyyw4d9x3rgb4kz2x6gb6c") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.33 (c (n "num-complex") (v "0.1.33") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0mg5q3bnlwg2cslv6wska1b1y5xzpq7fw7ql2n2cwm8qkq3vrawa") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.35 (c (n "num-complex") (v "0.1.35") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0bzrjfppnnzf9vmkpklhp2dw9sb1lqzydb8r6k83z76i9l2qxizh") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.36 (c (n "num-complex") (v "0.1.36") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "157zc1yxr3wqm3516wi6bz2xrv67mgmg9hlz5z0icsqzia6qjd1m") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.37 (c (n "num-complex") (v "0.1.37") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0xs6lsqdrz0159lk16zqa25p6c9my7yiywqq6j2k08kpr8jb73hl") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.38 (c (n "num-complex") (v "0.1.38") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "1bxn7zzwr329ayrf42i8y4i4awmzvlw4wmrclak9lmsn7hagqba1") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.39 (c (n "num-complex") (v "0.1.39") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0yd19zy98l4wvs6gj8vvfw7kx5jw5yw64zszzrfpmrh435zxn97b") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.40 (c (n "num-complex") (v "0.1.40") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "1rh4wxjrig8799wzf46gdcpgw3p4bgh82ak6rxkjsjf50n26cgjh") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.41 (c (n "num-complex") (v "0.1.41") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0z5l51sgvq7rrk5k0y8h7g8askl6i7a9fmsscg5vwpfgyx5pppjq") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.42 (c (n "num-complex") (v "0.1.42") (d (list (d (n "num-traits") (r "^0.2.0") (f (quote ("std"))) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0xq09dp41gsaqimhwypmyhlym2d3q8w53qwmnpv8q7czmgg8xzr6") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.1.43 (c (n "num-complex") (v "0.1.43") (d (list (d (n "num-traits") (r "^0.2.0") (f (quote ("std"))) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (o #t) (d #t) (k 0)))) (h "0mln3h018lar511hadjwfkbyq1561s8kdzfg8aagbakqg0fn725j") (f (quote (("unstable") ("default" "rustc-serialize"))))))

(define-public crate-num-complex-0.2.0 (c (n "num-complex") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "rand") (r "^0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "13j03xhgi6x8bb6xxkqcng9vzy1wx1dh68x97zdgpq49hxbq7pk8") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-complex-0.2.1 (c (n "num-complex") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "rand") (r "^0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1y5sv21a9f05crr78ih3gwi7hi8in1svcxw2d0q1jj6jdkl9nyqh") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-complex-0.2.2 (c (n "num-complex") (v "0.2.2") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.7") (k 0)) (d (n "rand") (r "^0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0k2qk58jj48gjjv43sanx7z42nnjw9hisvhibrqmy1mgz3zm963s") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-complex-0.2.3 (c (n "num-complex") (v "0.2.3") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.7") (k 0)) (d (n "rand") (r "^0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1z6zjdzx1g1hj4y132ddy83d3p3zvw06igbf59npxxrzzcqwzc7w") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-complex-0.2.4 (c (n "num-complex") (v "0.2.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "rand") (r "^0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "15dwaksw729r3v14sgzc9723s3fnfixiir8jzwx7b7kim48r9cdn") (f (quote (("std" "num-traits/std") ("i128" "num-traits/i128") ("default" "std"))))))

(define-public crate-num-complex-0.3.0 (c (n "num-complex") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1dczd81f2xb092dhb0brbdbf19pyfn0v9xmkf6qm0w4pv1dx0nmh") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.3.1 (c (n "num-complex") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1igjwm5kk2df9mxmpb260q6p40xfnkrq4smymgdqg2sm1hn66zbl") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.0 (c (n "num-complex") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "11ahq51ibf7x30rsabgp3a29zw6d6bfilz53sj152z5vpdkkd1r6") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.1 (c (n "num-complex") (v "0.4.1") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1457nvfqy6hw9jdw2kfk1la4mqb962gjk4wlwglxbvzgmy3w7ywp") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.2 (c (n "num-complex") (v "0.4.2") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "06ac87jsgdh845q4czdj5bypll83by9aj9y781zvspxwr1497qvs") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.3 (c (n "num-complex") (v "0.4.3") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0zgiry1dq278j4ig6qhcxb1yhwb640s1br5153qxca68al9d5q02") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.4 (c (n "num-complex") (v "0.4.4") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "051j73vvdj07kdlpqv056s3a50ragsx3183cbpl1shc51355g88v") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.5 (c (n "num-complex") (v "0.4.5") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "19k89q0cd2jacd79z8ka95mmg0sx0fd1kpz01ycpr9clv8pn1ii3") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-num-complex-0.4.6 (c (n "num-complex") (v "0.4.6") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "15cla16mnw12xzf5g041nxbjjm9m85hdgadd5dl5d0b30w9qmy3k") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde") ("rkyv" "dep:rkyv") ("rand" "dep:rand") ("bytemuck" "dep:bytemuck") ("bytecheck" "dep:bytecheck")))) (r "1.60")))

