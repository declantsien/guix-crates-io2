(define-module (crates-io nu m- num-macros) #:use-module (crates-io))

(define-public crate-num-macros-0.1.22 (c (n "num-macros") (v "0.1.22") (h "07627shjp65pjxi4xlsyanj7s01g1qax2xnsk64g631i6b02r1bz")))

(define-public crate-num-macros-0.1.23 (c (n "num-macros") (v "0.1.23") (h "04ilbpgnqzlf5pjqkpdbb3fpnh7ak649k3jsx1p085z6zm5z9pcn")))

(define-public crate-num-macros-0.1.24 (c (n "num-macros") (v "0.1.24") (h "1g3xy6ig5dplb8fjd6j0h16wyxdlryfzvq273c390z6n2n7yfriv")))

(define-public crate-num-macros-0.1.25 (c (n "num-macros") (v "0.1.25") (d (list (d (n "num") (r "*") (d #t) (k 2)))) (h "01dpd1c313gd0sd9db9bs91b6igj8wkpdgl3n02rkyvn7j3wi3fp")))

(define-public crate-num-macros-0.1.26 (c (n "num-macros") (v "0.1.26") (d (list (d (n "num") (r "*") (d #t) (k 2)))) (h "15qr1k5h11wl72v8c9gl45zz9g9fxnvpmiym7yd7jw4d97acjsm5")))

(define-public crate-num-macros-0.1.27 (c (n "num-macros") (v "0.1.27") (d (list (d (n "num") (r "*") (d #t) (k 2)))) (h "08xlx3w54swdmwpddmff5rmvvha8ylbh2c49zb78zy22415s8b0m")))

(define-public crate-num-macros-0.1.28 (c (n "num-macros") (v "0.1.28") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "05zrnlh9jkq7hq5dyf8x7qkr1imsx2jwa8fsmdgavhxxhbh90mqm")))

(define-public crate-num-macros-0.1.29 (c (n "num-macros") (v "0.1.29") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0mxd0gncqg7xdl6dba509qcm9z1my09maj3l2y19wa1mmgdrkj46")))

(define-public crate-num-macros-0.1.30 (c (n "num-macros") (v "0.1.30") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0mhn4zsg4lppkp65fx5jlwbbhz2wscw0cnnj5vg5w8plkp16ih3w")))

(define-public crate-num-macros-0.1.31 (c (n "num-macros") (v "0.1.31") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0pwm7n71p6lwgbkic4cb7qq59v0rkxgqw716v8h0pyyzqvj86w41")))

(define-public crate-num-macros-0.1.32 (c (n "num-macros") (v "0.1.32") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "1zfwhfphfh5692z3siaxkddn9607yf4v9lhbv5lzlrl11z95hnnl")))

(define-public crate-num-macros-0.1.33 (c (n "num-macros") (v "0.1.33") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "1ckvqx70hgs8lka76v2f5dypgd14ca664dsgf18psrcw7p8dxkjs")))

(define-public crate-num-macros-0.1.36 (c (n "num-macros") (v "0.1.36") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "02ngx7dbfdgznrbbd0qmxx9l3zakbxda8mpkc11hvfz6wz8463r6")))

(define-public crate-num-macros-0.1.37 (c (n "num-macros") (v "0.1.37") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0wbyq8wprgzvb7vfah29ahalq9nr4wqn35a8i8mpdsyiq4smbga5")))

(define-public crate-num-macros-0.1.38 (c (n "num-macros") (v "0.1.38") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "11cki08bbsk031gqvhy6k8r8adxiyp2cmp8ap55pyhwn8pdjfxd4")))

(define-public crate-num-macros-0.1.39 (c (n "num-macros") (v "0.1.39") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "0ih1hy7qqnx8rcfg7hiw8mzpdfzpxn5ly0i6srjv1i7phclihfms")))

(define-public crate-num-macros-0.1.40 (c (n "num-macros") (v "0.1.40") (d (list (d (n "num") (r "^0.1") (d #t) (k 2)))) (h "06gzadq2hkb8bwhbwdlvvh23i7bq5p66kfhbn7b81qkyvwpjmwn8")))

