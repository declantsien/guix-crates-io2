(define-module (crates-io nu m- num-modular) #:use-module (crates-io))

(define-public crate-num-modular-0.0.1 (c (n "num-modular") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17zkdz3yldlsf0m9bw22mdvp3jk7sqmalbgp46qg0dx7sgjm7kcs")))

(define-public crate-num-modular-0.0.2 (c (n "num-modular") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0n0qah6r7gvpdn54qyjms1r1568nmycdjvpz7yz3slcgr4jxmcw9")))

(define-public crate-num-modular-0.0.3 (c (n "num-modular") (v "0.0.3") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "138lgcfl56iqyz1h2jnnvqj56g575sxbmlzi572jcx55sxlkw5kg")))

(define-public crate-num-modular-0.1.0 (c (n "num-modular") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "06q9dv45bj8drhy5j1js3k03vdxbgifi52ayjm13w3ci4vm45nn9")))

(define-public crate-num-modular-0.1.1 (c (n "num-modular") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0aznxna3x0iacp5vh5981zc3872izblyw6a4zq44d0j7giwp463r")))

(define-public crate-num-modular-0.2.0 (c (n "num-modular") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "07k1lwr034rh4rdw71qqbkm5lmi20gm8fxncd0bhjpv9hg7xp2kl") (f (quote (("std"))))))

(define-public crate-num-modular-0.2.1 (c (n "num-modular") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0mqcwzmkif6m8a6s5al0bm8j5s1rfxrx24rlzpd53l055df6zkhj") (f (quote (("std"))))))

(define-public crate-num-modular-0.2.2 (c (n "num-modular") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0x27p7whw49fxqyq69ipim382r706r64has24w4x9mv0a63qy599") (f (quote (("std"))))))

(define-public crate-num-modular-0.3.0 (c (n "num-modular") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1l77ychsb44n5lfx8y8rslkz73w5k35dip9k6lm61m0bbphlimly") (f (quote (("std"))))))

(define-public crate-num-modular-0.4.0 (c (n "num-modular") (v "0.4.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "07bpxg91nr47zcdyympd8vsd1pf270b2ifpvmjzja5hl6nqkvjxd") (f (quote (("std"))))))

(define-public crate-num-modular-0.4.1 (c (n "num-modular") (v "0.4.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1rqkwgd2vnbx50ly0wjw8igzj77x0m6ad0b5qwfnfbsbvz5364bq") (f (quote (("std"))))))

(define-public crate-num-modular-0.4.2 (c (n "num-modular") (v "0.4.2") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1b9wzrdh9ljn6hm5xjx2d3mwdcz0dmjxrpw3ma7szz42ndp2558z") (f (quote (("std"))))))

(define-public crate-num-modular-0.4.3 (c (n "num-modular") (v "0.4.3") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1pcsdkanw2g78vjg06pspfdh6ggj3xyb3ial6vmkwqkzjn8as4ld") (f (quote (("std"))))))

(define-public crate-num-modular-0.5.0 (c (n "num-modular") (v "0.5.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1881npk54dgfzcspjc9yarhpgv7y2g2099jizs8h4mlmk5n73fgi") (f (quote (("std"))))))

(define-public crate-num-modular-0.5.1 (c (n "num-modular") (v "0.5.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0681zckrmg2pv3sk9w3gby561adgjjqihnx9yg6knp0ksh8zx9b4") (f (quote (("std"))))))

(define-public crate-num-modular-0.6.0 (c (n "num-modular") (v "0.6.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "03p0ipl1my11524rhf8bcw4hd2xhilp6zcv118q1sssjad4732p4") (f (quote (("std"))))))

(define-public crate-num-modular-0.6.1 (c (n "num-modular") (v "0.6.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0zv4miws3q1i93a0bd9wgc4njrr5j5786kr99hzxi9vgycdjdfqp") (f (quote (("std"))))))

