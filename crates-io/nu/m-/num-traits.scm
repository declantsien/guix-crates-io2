(define-module (crates-io nu m- num-traits) #:use-module (crates-io))

(define-public crate-num-traits-0.1.32 (c (n "num-traits") (v "0.1.32") (h "0fcwkrmyzf543silsm7gjarvk264dxiwxkpqjp9gmbkiy54b3sji")))

(define-public crate-num-traits-0.1.33 (c (n "num-traits") (v "0.1.33") (h "11qd2hxzx40apxyxikb37xwm6392wjxnm7hggklnhfc7wfkf81lx")))

(define-public crate-num-traits-0.1.34 (c (n "num-traits") (v "0.1.34") (h "13r72s02jvpwwqw4hjsbmafcjbdam66qlk0kmcqaqsjr6jn8xrcm")))

(define-public crate-num-traits-0.1.35 (c (n "num-traits") (v "0.1.35") (h "09r51zfzqh4mwfy5hnifhxa6zc186w0hpfdmb2lky9agk54flnc3")))

(define-public crate-num-traits-0.1.36 (c (n "num-traits") (v "0.1.36") (h "07688sp4z40p14lh5ywvrpm4zq8kcxzhjks8sg33jsr5da2l4sm1")))

(define-public crate-num-traits-0.1.37 (c (n "num-traits") (v "0.1.37") (h "16ffdq29yfglbhrm4k3nm8g4jbbdl19fs6rj0pf77zpkh4vzmjz1")))

(define-public crate-num-traits-0.1.38 (c (n "num-traits") (v "0.1.38") (h "05zfgj6pknwlh21q9bjik8wg2lxfp9371jylj6kj6r21m2xdg5cl") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-num-traits-0.1.39 (c (n "num-traits") (v "0.1.39") (h "1xkad92q9s070zgrzr2jdvq7l44svczcydnrz998pa82hric020p")))

(define-public crate-num-traits-0.1.40 (c (n "num-traits") (v "0.1.40") (h "1c5r473nl1xyiar83shhcs7swhmvqhrpx8837cqv9n38dn2kr14r")))

(define-public crate-num-traits-0.1.41 (c (n "num-traits") (v "0.1.41") (h "0w00n3j5lpv34lhg79gf3yfckvn5l98np2f7s3khw9a8xfswmkya")))

(define-public crate-num-traits-0.1.42 (c (n "num-traits") (v "0.1.42") (h "05w0ml0fhni3prnci0rrn3pr0cij01cndarqsfrair0gqxn06dlr")))

(define-public crate-num-traits-0.2.0 (c (n "num-traits") (v "0.2.0") (h "046g0jjnlxmxmk1li6whf7g9mzc12vvxi3pf8l6957fv8vqj1pp7") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-traits-0.1.43 (c (n "num-traits") (v "0.1.43") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0c9whknf2dm74a3cqirafy6gj83a76gl56g4v3g19k6lkwz13rcj")))

(define-public crate-num-traits-0.2.1 (c (n "num-traits") (v "0.2.1") (h "1lrwmxncpjkjysryv9jxx7cn5p1lf7rwjqxpavllh7njp7cjng0b") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-traits-0.2.2 (c (n "num-traits") (v "0.2.2") (h "0r3k9kjfrf5wd4zcpwcyshvr4mgzxcfx58bxvl2fwnkjvzy95q6y") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-traits-0.2.3 (c (n "num-traits") (v "0.2.3") (h "1h8g1ppz99v8ih7ky0d0nkp8ydjjb19dniqyqxjz4jnbayhj0by2") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.4 (c (n "num-traits") (v "0.2.4") (h "0a2yjmkvmgna637hik0daxa9f561ifxnjvar0i82yk95hpi96lvp") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.5 (c (n "num-traits") (v "0.2.5") (h "1zp9f1y8c1gr0wpjnjqgvs8gxjw3y20kpqxpg3fhr7f7bkpy23b3") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.6 (c (n "num-traits") (v "0.2.6") (h "1qdym9m6sbzna1pq3s21cbjgyjakyjds33xwp7c30vbxr5y5sfhb") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.7 (c (n "num-traits") (v "0.2.7") (d (list (d (n "autocfg") (r "^0.1.2") (d #t) (k 1)))) (h "1pa9fmqjyzjzby9wswnfwv03kqkcwr75q87yl129y4sa5aarriyr") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.8 (c (n "num-traits") (v "0.2.8") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)))) (h "0clvrm34rrqc8p6gq5ps5fcgws3kgq5knh7nlqxf2ayarwks9abb") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.9 (c (n "num-traits") (v "0.2.9") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1iisd4qj46jdv6i7zn51sx54fy6v8hwqkn4rljzzq7akqfrm6g24") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.10 (c (n "num-traits") (v "0.2.10") (d (list (d (n "autocfg") (r "^0.1.3") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1r079jbmrnrbvsz7dc5mcghijx7bhpfikjspfqrgl4n227y1zj6l") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.11 (c (n "num-traits") (v "0.2.11") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "15khrlm1bra50nd48ijl1vln13m9xg4fxzghf28jp16ic5zf8ay6") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.12 (c (n "num-traits") (v "0.2.12") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "04fnzwlnn6fcy09jjbi9l7bj5dvg657x5c2sjgwfb3pl0z67n9mc") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.13 (c (n "num-traits") (v "0.2.13") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1fny8wc3ag5drsnmlb7m79vf6qkwk36ww83rbb2v3z4l5n6yn8c3") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.14 (c (n "num-traits") (v "0.2.14") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "144j176s2p76azy2ngk2vkdzgwdc0bc8c93jhki8c9fsbknb2r4s") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.15 (c (n "num-traits") (v "0.2.15") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1kfdqqw2ndz0wx2j75v9nbjx7d3mh3150zs4p5595y02rwsdx3jp") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-num-traits-0.2.16 (c (n "num-traits") (v "0.2.16") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1hp6x4gayrib34y14gpcfx60hbqsmh7i8whjrbzy5rrvfayhl2zk") (f (quote (("std") ("i128") ("default" "std")))) (r "1.31")))

(define-public crate-num-traits-0.2.17 (c (n "num-traits") (v "0.2.17") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0z16bi5zwgfysz6765v3rd6whfbjpihx3mhsn4dg8dzj2c221qrr") (f (quote (("std") ("i128") ("default" "std")))) (r "1.31")))

(define-public crate-num-traits-0.2.18 (c (n "num-traits") (v "0.2.18") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0yjib8p2p9kzmaz48xwhs69w5dh1wipph9jgnillzd2x33jz03fs") (f (quote (("std") ("i128") ("default" "std")))) (r "1.31")))

(define-public crate-num-traits-0.2.19 (c (n "num-traits") (v "0.2.19") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0h984rhdkkqd4ny9cif7y2azl3xdfb7768hb9irhpsch4q3gq787") (f (quote (("std") ("i128") ("default" "std")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.60")))

