(define-module (crates-io nu m- num-chrono-duration) #:use-module (crates-io))

(define-public crate-num-chrono-duration-0.1.0 (c (n "num-chrono-duration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0iy01627sspci3l2h76jpm0ghy24l4prcmxhisi0qasl7mqy2197")))

