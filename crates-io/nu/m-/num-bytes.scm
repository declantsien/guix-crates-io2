(define-module (crates-io nu m- num-bytes) #:use-module (crates-io))

(define-public crate-num-bytes-0.1.0 (c (n "num-bytes") (v "0.1.0") (h "0vf3z45v29p6fs8vadx5b2c0qbigvhr44bn3hfd8mi9pq6s83w8d")))

(define-public crate-num-bytes-0.2.0 (c (n "num-bytes") (v "0.2.0") (h "1bi09g4hgv29b0h67k2fifhqnxpl2pb5as4haxi87x72k67c6ch4")))

(define-public crate-num-bytes-0.3.0 (c (n "num-bytes") (v "0.3.0") (h "17dlixf5p6r52rd5skqh32csd16zmd0kap4wgfqnwzp3hgj17xbw")))

(define-public crate-num-bytes-0.4.0-alpha (c (n "num-bytes") (v "0.4.0-alpha") (h "1if88qf15bng5cir815ivxfhrng064i3iw88m5jmdpzm3895lmsi") (y #t)))

(define-public crate-num-bytes-0.4.0-beta (c (n "num-bytes") (v "0.4.0-beta") (h "1lxixrjd1q11fszhj9mvpy7plgbs3ww6zqab0y460wb49fdcrgck") (y #t)))

(define-public crate-num-bytes-0.4.0 (c (n "num-bytes") (v "0.4.0") (h "1ra044lcb2wkki71951njfkvh1gg3b23w1q281a9yldg2m7v2zck")))

