(define-module (crates-io nu m- num-parse) #:use-module (crates-io))

(define-public crate-num-parse-0.1.0 (c (n "num-parse") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "18ci95cbhfbvf1j2qmcsci5043jxq6gwl3i9f4pizdgy5is3c32v")))

(define-public crate-num-parse-0.1.1 (c (n "num-parse") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1vhixckqb762jf3fz8ijgpgb7i7svwk1fc59gxms30s495r3l83l")))

(define-public crate-num-parse-0.1.2 (c (n "num-parse") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "153a74n7n4ms39aldxdcmaggcwfdgjhjwq64q8jsab5jyknlwy9c")))

