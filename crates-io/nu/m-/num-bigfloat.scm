(define-module (crates-io nu m- num-bigfloat) #:use-module (crates-io))

(define-public crate-num-bigfloat-0.1.0 (c (n "num-bigfloat") (v "0.1.0") (h "0xz48654lnh45bfv8k8qpxqzggj9gyhrb5jm9f55ix8mfjgdkmnn") (y #t)))

(define-public crate-num-bigfloat-0.1.1 (c (n "num-bigfloat") (v "0.1.1") (h "0rc5k1v65n7rzblqbij98m7i3rgpd07pci278h219zcqs4z4w300") (y #t)))

(define-public crate-num-bigfloat-0.1.2 (c (n "num-bigfloat") (v "0.1.2") (h "18kxign6djikdig6w6smral3pyhp8vm317a01am412w6z1sszpvz")))

(define-public crate-num-bigfloat-0.1.3 (c (n "num-bigfloat") (v "0.1.3") (h "17sz5f5zmjb8zj2jc1wjl34rkyvr9klgr3qsbpcizafbwv4p0cpz")))

(define-public crate-num-bigfloat-0.1.4 (c (n "num-bigfloat") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10w4nhr6hxxmzy6n5ff1i0qgjd4szr2h84anw7gfrg76ingbz3kn")))

(define-public crate-num-bigfloat-0.1.5 (c (n "num-bigfloat") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cwmkhfy94rpihqil788n81n6zvb2ik4vxy48vhxn3kf3sqaf8kw")))

(define-public crate-num-bigfloat-0.1.6 (c (n "num-bigfloat") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qnsg8pkdipjx2am8pgf550yfxkdl7m69fxq9bxr75vf2fsvx6pr")))

(define-public crate-num-bigfloat-1.0.0 (c (n "num-bigfloat") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1g667dngnnpfzdhxb0v5zv2ak5by2naaic67ilmlngn37hb20kii") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.1.0 (c (n "num-bigfloat") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hb4lv6fkpsqgka26yl7ys0dw7gpqhgxm8jikjg2kgqakwyj32q1") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.1.1 (c (n "num-bigfloat") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cwvmv7hz5mdlajngvrjssc022zbpb4s615q78c6xw2wwacjz8s7") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.2.0 (c (n "num-bigfloat") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kj6n9yrzn0n9s4a596hjkbri7iin469f02nsh9c24y1hn5yqr57") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.3.0 (c (n "num-bigfloat") (v "1.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nc967km0g19xpkf1ggq7rjvzir3qcxhqh7dck8cisqj4ck6xkpp") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.3.1 (c (n "num-bigfloat") (v "1.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1qqac1bv9inliimz8qhcbbmhn965mcclin4g5a2dbkx81kc2i3pv") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.4.1 (c (n "num-bigfloat") (v "1.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10ncr1i0fzjd0csbw9kvhihpdkjhbmixcy8akv2ra0sa0my0la2f") (f (quote (("std") ("default" "std"))))))

(define-public crate-num-bigfloat-1.5.0 (c (n "num-bigfloat") (v "1.5.0") (d (list (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.147") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ny0pm1cjfq8pj2a5psrnaazp3a7dd5f5mcix5xcvkzk9y4kd4x0") (f (quote (("std" "serde/std" "rand/std" "rand/std_rng") ("default" "std" "serde" "rand")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std"))))))

(define-public crate-num-bigfloat-1.6.0 (c (n "num-bigfloat") (v "1.6.0") (d (list (d (n "num-traits") (r "~0.2.15") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.147") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1x55f4df3xqzhs9yyk4zy6kgvby6ixq8b5knaz3gz8f7z50rf1pm") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

(define-public crate-num-bigfloat-1.6.1 (c (n "num-bigfloat") (v "1.6.1") (d (list (d (n "num-traits") (r "~0.2.15") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.147") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1i8sdlwdvbwchj9i8x0lx4hfbwdqq7shyk95y3jim4q4ga9zqrfs") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

(define-public crate-num-bigfloat-1.6.2 (c (n "num-bigfloat") (v "1.6.2") (d (list (d (n "num-traits") (r "~0.2.15") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.147") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10xqb70r9jdi94s1acgv2d5fbi7k0pjlanc7bxzq5rc43lrm2sm5") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

(define-public crate-num-bigfloat-1.6.3 (c (n "num-bigfloat") (v "1.6.3") (d (list (d (n "num-traits") (r "~0.2.15") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.147") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ivhggqirwyx02bnsmf50s26zd0g1n95g4z2wj01llb69f4xh61y") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

(define-public crate-num-bigfloat-1.7.0 (c (n "num-bigfloat") (v "1.7.0") (d (list (d (n "num-traits") (r "~0.2.16") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fa9qbkbwkglnjanas44ppwima9bd51jf0hyf0gbi9p0h7pyp0sp") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

(define-public crate-num-bigfloat-1.7.1 (c (n "num-bigfloat") (v "1.7.1") (d (list (d (n "num-traits") (r "~0.2.16") (o #t) (k 0)) (d (n "rand") (r "~0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "~1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1r777glycspjlbkllrxda2bvj9vgkls9xfqyffb23k3qp5hixvnm") (f (quote (("std" "serde/std" "num-traits/std" "rand/std" "rand/std_rng") ("default" "std" "rand" "serde" "num-traits")))) (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand" "std") ("num-traits" "dep:num-traits"))))))

