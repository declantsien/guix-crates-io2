(define-module (crates-io nu m- num-quaternion) #:use-module (crates-io))

(define-public crate-num-quaternion-0.1.0 (c (n "num-quaternion") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1ppfsn9j61042vfv4q4mphymaa9aj4b5zqynya73nl2w1q3dc6az") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (r "1.60.0")))

(define-public crate-num-quaternion-0.1.1 (c (n "num-quaternion") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1wlli4i747yb0l9ddqpav8z1m1gjc1j9q547kf57cx2gmp6hhnma") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (y #t) (r "1.60.0")))

(define-public crate-num-quaternion-0.1.2 (c (n "num-quaternion") (v "0.1.2") (d (list (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11g91c17ppd0l68lb9s19l25mj62q2ssqqpax358l0ydbzwhrvas") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (r "1.60.0")))

