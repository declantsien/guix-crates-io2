(define-module (crates-io nu m- num-to-words) #:use-module (crates-io))

(define-public crate-num-to-words-0.1.0 (c (n "num-to-words") (v "0.1.0") (h "01dy5hcz447lnivyl9a0b8007nz618xlda90w0lcnj13cla9n6dm") (f (quote (("uk_ua") ("en_us") ("default" "en_us"))))))

(define-public crate-num-to-words-0.1.1 (c (n "num-to-words") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0m1wam03cxq9l6rn6h7lqlf8jcxbnxqi5xwbp9kp7q4cimpckj3h") (f (quote (("uk_ua") ("en_us") ("default" "en_us"))))))

