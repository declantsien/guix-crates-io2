(define-module (crates-io nu m- num-runtime-fmt) #:use-module (crates-io))

(define-public crate-num-runtime-fmt-0.1.0 (c (n "num-runtime-fmt") (v "0.1.0") (d (list (d (n "iterext") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0nzjqmf7jr1868pgfizhbc9qykiam5lv7m0zllyi3n53946dmq7y")))

(define-public crate-num-runtime-fmt-0.1.1 (c (n "num-runtime-fmt") (v "0.1.1") (d (list (d (n "iterext") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0r79d8v7jzq25vwvk6qn681rqq52hhg1q9w4d6snlg8z92f1jdvz")))

(define-public crate-num-runtime-fmt-0.1.2 (c (n "num-runtime-fmt") (v "0.1.2") (d (list (d (n "iterext") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0kil47sv6kan2w3xxyn2i2ni5c5snkxdq6fbmycala475j4sm8v8")))

