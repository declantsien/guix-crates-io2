(define-module (crates-io nu m- num-variants) #:use-module (crates-io))

(define-public crate-num-variants-0.1.0 (c (n "num-variants") (v "0.1.0") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0zpdhpsvy65b3hywzyyn33amzs9pdh1v6lj5m4phyy0ik5mn5d06") (y #t)))

(define-public crate-num-variants-0.1.1 (c (n "num-variants") (v "0.1.1") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1cm042aljrngx17a3p4s9cagnajgcpm7cxbkxvc8yd8hwz6z4lhp") (y #t)))

(define-public crate-num-variants-0.1.2 (c (n "num-variants") (v "0.1.2") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "03h3m4rhiq6pxnm2wp0pi538d3p5f1zvy98p94mf275f8qrn772b") (y #t)))

(define-public crate-num-variants-0.1.3 (c (n "num-variants") (v "0.1.3") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "07q2k7crqx5gbn02rrw7a81n88n6a01vn3arjhdx8gbdk2d7y69k") (y #t)))

(define-public crate-num-variants-0.1.4 (c (n "num-variants") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0s2r6y3hfnl74z6n5qdk7imhhv7w12k25pscn1j82w4iq5pkjx4q") (y #t)))

(define-public crate-num-variants-0.1.5 (c (n "num-variants") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0k1l2iqb7km75jgbylj4msdhi100gv5jsfsgf4zk1gv6x6wsk78p") (y #t)))

(define-public crate-num-variants-0.1.6 (c (n "num-variants") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1rjgdb5p4ifvs06agr56f0mvrq2r75ivsa3n5mhaffprdz33kjaa") (y #t)))

(define-public crate-num-variants-0.1.7 (c (n "num-variants") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0lvnvh2rk1k45pxw4yf4rraaxjl23q2zxij9b9fdhg5psqnw1f33") (y #t)))

(define-public crate-num-variants-0.2.1 (c (n "num-variants") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0mm2azcjhfjzi8gwpwdwfd4x42r55s1wgapdvdn3drjb0nrl4f4s") (y #t)))

(define-public crate-num-variants-0.2.2 (c (n "num-variants") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0vi75n95y2f0k3d5s9q7a3ga086w4k2pq7vga852fc45bqb13mwa") (y #t)))

(define-public crate-num-variants-0.2.6 (c (n "num-variants") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0j56sjpnvwq76q5cxd9s2rxl1rb6vynvg4qym8qyvnp7zcxx3wwi") (y #t)))

(define-public crate-num-variants-0.2.7 (c (n "num-variants") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "18pp4svijjjs9nrfp34l202amrdb05m1r0gbw3m2ndp2fr9zj561") (y #t)))

