(define-module (crates-io nu m- num-huarongdao) #:use-module (crates-io))

(define-public crate-num-huarongdao-0.1.0 (c (n "num-huarongdao") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1qr5k3a0jgaxlhhmhl8klvvj9ssckzf8vrqn2f5bhisxz8dap3ra")))

(define-public crate-num-huarongdao-0.1.1 (c (n "num-huarongdao") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0mq67snzlrif1104ncs4sms0zmg416rpr2b8pk1nw52p651kir26")))

(define-public crate-num-huarongdao-0.1.2 (c (n "num-huarongdao") (v "0.1.2") (h "1am4fqrmf0j1jmgd8ivdwiv6z5gyw9vr4xijia62s4j9s7gy5cq8")))

(define-public crate-num-huarongdao-0.2.0 (c (n "num-huarongdao") (v "0.2.0") (d (list (d (n "oorandom") (r "^11.1") (d #t) (k 0)))) (h "1wnsi3zq16i6j353a5c2dik3zi466z774ghjcn76skn3qq2vf7b2")))

(define-public crate-num-huarongdao-1.0.1 (c (n "num-huarongdao") (v "1.0.1") (h "15zaf7zi7sdlvj8i7g6klai6mk717hb47mpf2sg49vsxkvyxwdw6")))

