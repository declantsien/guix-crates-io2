(define-module (crates-io nu m- num-format-windows) #:use-module (crates-io))

(define-public crate-num-format-windows-0.3.0 (c (n "num-format-windows") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)))) (h "1sy5jxrbhv6s28c51ibzi34s8qcjm8b21nf7biray7v1qi89h5sf")))

(define-public crate-num-format-windows-0.4.2 (c (n "num-format-windows") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0q3ay3ds0xm1c1qdycyfdva503b866pgz2cy4j38056vjiszb91x")))

(define-public crate-num-format-windows-0.4.3 (c (n "num-format-windows") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1w8mjrypvdzv66srv0ls5w15glqyj4wvylrp9c33psg0nr2vr1l9")))

(define-public crate-num-format-windows-0.4.4 (c (n "num-format-windows") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1yic2lcv3gngyshzmnqjhaq1z736j8jgghhn3n8yph95fbv0f7lv")))

