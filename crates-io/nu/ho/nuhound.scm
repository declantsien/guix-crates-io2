(define-module (crates-io nu ho nuhound) #:use-module (crates-io))

(define-public crate-nuhound-0.1.0 (c (n "nuhound") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "02d6lzq061xfjh45giq4vfz57dk7a6r377gjwl9xps4i8h0pps38") (f (quote (("disclose"))))))

(define-public crate-nuhound-0.1.1 (c (n "nuhound") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1nd8z2r1i1rj1n45zqlrswqr8980xw48izjvy16hdnd1hbfps4q0") (f (quote (("disclose"))))))

(define-public crate-nuhound-0.1.2 (c (n "nuhound") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "19sklvbwqar1piqy2m5kfg4ja5k0jr76npl02y60pmvzvypqb9m2") (f (quote (("disclose"))))))

