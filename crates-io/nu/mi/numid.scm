(define-module (crates-io nu mi numid) #:use-module (crates-io))

(define-public crate-numid-0.1.0 (c (n "numid") (v "0.1.0") (h "188gk4y2538w0vq3yb2nnf2izbjh7q0bf9xbr23xdiwys1vampx5")))

(define-public crate-numid-0.1.1 (c (n "numid") (v "0.1.1") (h "1b2fpzzafiqglxinbjd21kjm16azbc7d6w5j8fwlj198cvnzpm0l") (f (quote (("example") ("default"))))))

(define-public crate-numid-0.1.2 (c (n "numid") (v "0.1.2") (h "0rpf341v2f73fv32hah02x3lcwgkm2lxzgh4sjvmyfligriffn5d") (f (quote (("std") ("example") ("default" "std")))) (y #t)))

(define-public crate-numid-0.2.0 (c (n "numid") (v "0.2.0") (h "1vkbjqqfzqhziva81clwpz33jh16m0rg8yr65fzlbvkg6xsqkvjm") (f (quote (("example") ("default"))))))

(define-public crate-numid-0.2.1 (c (n "numid") (v "0.2.1") (h "18v45g1hjyg7r2wmhg4m00p9zw69xhczbrwg9jm1p08m3hg53d16") (f (quote (("example") ("default"))))))

(define-public crate-numid-0.2.2 (c (n "numid") (v "0.2.2") (h "0cr88v6lkbm1q0g0ng35cf952y5j3vqpg623bvxsrca2j7vbgr8m") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.3 (c (n "numid") (v "0.2.3") (h "1xww97h15cp1s6zgn1wwffkv0530020h4vjpx24hbr1ydc1vyl8b") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.4 (c (n "numid") (v "0.2.4") (h "1lbhs9f8p85cqslxb28ixfb7synadhavfrv068859bqnlmfvfq2s") (f (quote (("example") ("display") ("default" "display")))) (y #t)))

(define-public crate-numid-0.2.5 (c (n "numid") (v "0.2.5") (h "03xmg5c55cmfs4fhgz02bqddp4sbjjw7qn2q6aipx3l2mdfx7wxc") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.6 (c (n "numid") (v "0.2.6") (h "1ssgp9nyjc9bz3dp124fk4v9yi7szhdfyr17yxs03cqnfc8sqn2m") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.7 (c (n "numid") (v "0.2.7") (h "03ay49v59s8xz9i3b05a3617fmspa00nipp9ij3j7vn75y9g3sxn") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.8 (c (n "numid") (v "0.2.8") (d (list (d (n "const_fn_assert") (r "^0.1") (d #t) (k 0)))) (h "0v68sz2swblxgq1f7liigcmis2gdxxy2i670ks7wnbz8h0g157ym") (f (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2.9 (c (n "numid") (v "0.2.9") (d (list (d (n "const_fn_assert") (r "^0.1") (d #t) (k 0)))) (h "1vgamqcc7wzla5qq28qpf9j8g0dnvvmbj2j4vgxf6ig428gwqyz0") (f (quote (("example") ("display") ("default" "display"))))))

