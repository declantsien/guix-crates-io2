(define-module (crates-io nu #{-i}# nu-isp) #:use-module (crates-io))

(define-public crate-nu-isp-0.5.0 (c (n "nu-isp") (v "0.5.0") (d (list (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1qbviwxrm86wczcmp3ybddv14dxlnclvgnrardcygz8g8rbd9yhh")))

(define-public crate-nu-isp-0.6.0 (c (n "nu-isp") (v "0.6.0") (d (list (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1fq27ff7y0zhk6q5vx75xl759hzhlnxiy1bilbw10brp99757ljj")))

(define-public crate-nu-isp-0.7.0 (c (n "nu-isp") (v "0.7.0") (d (list (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0ibsinl5mwhsfq2bdkz65g96dc467nxh8ifsgw2cp9zj245a5wc8")))

(define-public crate-nu-isp-0.7.1 (c (n "nu-isp") (v "0.7.1") (d (list (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0warajgmg9yhnmbnals4k8gcjyzy3ik5wri6ypg15jqsnxhjw2j4")))

