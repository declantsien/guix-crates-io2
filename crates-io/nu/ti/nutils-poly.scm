(define-module (crates-io nu ti nutils-poly) #:use-module (crates-io))

(define-public crate-nutils-poly-1.0.0 (c (n "nutils-poly") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sqnc") (r "^1") (f (quote ("ndarray"))) (d #t) (k 0)))) (h "1mnlijqshd1g3c6g791lf2izamzz4bkv6fqc6ja3mg4qlcgqr6hv") (f (quote (("bench"))))))

