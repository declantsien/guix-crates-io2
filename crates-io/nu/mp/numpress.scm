(define-module (crates-io nu mp numpress) #:use-module (crates-io))

(define-public crate-numpress-1.0.0 (c (n "numpress") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1aw8bqqaym1s4c0yxi9k97dvc024xfdvaz50xf9g1hmxr64gf1hv")))

(define-public crate-numpress-1.0.1 (c (n "numpress") (v "1.0.1") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "09pvmq7yg5hc8ak4f5x58d9gzfm90fh5ym0d6kjlkhgy5iqhvbby")))

(define-public crate-numpress-1.0.3 (c (n "numpress") (v "1.0.3") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0lz24zyvq17511l6im2mv55caspc8abwddwlw8bkkp3z12i6c1lw")))

(define-public crate-numpress-1.0.4 (c (n "numpress") (v "1.0.4") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ia7arwzm34bb67r2dm8447jrn4mixiii8d4g7bqlzbaz9xpr0zl") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.5 (c (n "numpress") (v "1.0.5") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1msy0dxwknjcfkw7zi9ai0yyfysl3c7ga01iplv2lwffhc7mjks1") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.6 (c (n "numpress") (v "1.0.6") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00p6ib7l5riym7i5lc0v76h51f8wrvhj5ql06g2gm8wdrw339gk0") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.7 (c (n "numpress") (v "1.0.7") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "02p4yq89i6zyspfp2waivdfzwmk8ph82lcd8xlxhda2byb896izx") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.8 (c (n "numpress") (v "1.0.8") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "12jx478vgfdjbn80bx9kzzyy7shaw5020nmzjz5g74803adwbqi3") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.9 (c (n "numpress") (v "1.0.9") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0vk189hx4bxndyx9vdfraq0r3v50aa4hjr3y2z9mvxgf8708gqry") (f (quote (("std") ("nightly") ("default" "std") ("alloc" "wee_alloc"))))))

(define-public crate-numpress-1.0.10 (c (n "numpress") (v "1.0.10") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0dzcyxrq7yzc7j7z9nlq16c9bx7apibp7jwh15fanxzg8psvnsgi") (f (quote (("std") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-numpress-1.1.0 (c (n "numpress") (v "1.1.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0lhfdrrqckbxfbqas8jgwa9n9ajx5qnplnv2z71c37bqr8kw1j65") (f (quote (("std") ("default" "std"))))))

