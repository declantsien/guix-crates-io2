(define-module (crates-io nu mc numcore) #:use-module (crates-io))

(define-public crate-numcore-1.0.0 (c (n "numcore") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuple-conv") (r "^1.0.1") (d #t) (k 0)))) (h "0hqlxddj8fhhvl62ihi8kdlh7pqx6p5lv43v3hl5hw2yk9wvhgmc") (f (quote (("serde_support" "serde")))) (y #t)))

