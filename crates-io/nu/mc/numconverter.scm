(define-module (crates-io nu mc numconverter) #:use-module (crates-io))

(define-public crate-numconverter-1.1.0 (c (n "numconverter") (v "1.1.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "nix") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "x11-clipboard") (r "^0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1b6kabch5h90d1k3lgbr8mpdr92cybx2130zc6yis1ay86qgv9r5") (f (quote (("fail-on-warnings"))))))

