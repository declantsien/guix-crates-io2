(define-module (crates-io nu mm nummap) #:use-module (crates-io))

(define-public crate-nummap-0.1.0 (c (n "nummap") (v "0.1.0") (h "0p53cb4fr9m00m45z713s5f8srrsvag1d5qr9gnx7p1wlc2054d9") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.1.1 (c (n "nummap") (v "0.1.1") (h "0p576n6xl3lsnhva6jl3ffx7g17vfvvr3jvvqmf02b4r7jilrjjh") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2.0 (c (n "nummap") (v "0.2.0") (h "09549z7zpnilgx113nam5x3jdwjpn36sz46vvi64wvi104hwb057") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2.1 (c (n "nummap") (v "0.2.1") (h "1zsbnclh2kmhf608i6r19z3pz85q3mahilg1yz1xvh9jxf3pb8rr") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2.2 (c (n "nummap") (v "0.2.2") (h "1v0sccw804sqh5ggzdy8mdvmizap99b19p0gris4gpfpf2p2sx9a") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2.3 (c (n "nummap") (v "0.2.3") (h "0n7039pgic7f3k8aci70cygjw9l91vw8wd93cyq574j5d0dyxw1b") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2.4 (c (n "nummap") (v "0.2.4") (h "1v2w3nplwdzz2nkjspk1000jkkqw3ql6fvy0vmwi3cvbpskyk85h") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3.0 (c (n "nummap") (v "0.3.0") (h "1yrwccfxlr3g9f73lfmj5b73dz2mbh7sba935b9gw4cjimf8138f") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3.1 (c (n "nummap") (v "0.3.1") (h "09x938sr7mcvk7faypi1iljz34rgp7js7jh25h6vnihm595w1888") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3.2 (c (n "nummap") (v "0.3.2") (h "19bq78q41d9g7c73xbymhk55z2shjf7ai9z93vjvz1arggxqair6") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3.3 (c (n "nummap") (v "0.3.3") (h "19xrs2j29prn4xpg4gmk2n489firhgl7ry443f3gl2zrymhnf97r") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3.4 (c (n "nummap") (v "0.3.4") (h "1wpzkyz8zdlc4ssi86f0nhx4cvn2pqrg1ss4xs3gg3zv2hcdykm5") (f (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.4.0 (c (n "nummap") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1bvdxzivs7ws76sb65f7limj0namv4hlpfng9hckpgxk6lsdbklr") (f (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.4.1 (c (n "nummap") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)))) (h "1kdm0gj5m48jn9vkf1gv7qx7j5yl6ba5vfnd2ma14c0pk7nb53gv") (f (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.4.2 (c (n "nummap") (v "0.4.2") (d (list (d (n "hashbrown") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)))) (h "0xpfcpsb2jdi4ckk7a9awiy01n9d3d6p171hyg1sa8bmqipscrmp") (f (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.5.0 (c (n "nummap") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)))) (h "0ld1r22xbngv7nlv15h214z1pn7yyvskkydqn2wm6dp20lp34nwk") (f (quote (("no_std" "hashbrown") ("map_get_key_value") ("default" "no_std"))))))

(define-public crate-nummap-0.5.1 (c (n "nummap") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)))) (h "0lphs4i73d7814k929qdyi0sbkkd3vc0mvgycnck5vvpkcgld8sl") (f (quote (("no_std" "hashbrown") ("map_get_key_value") ("default" "no_std"))))))

