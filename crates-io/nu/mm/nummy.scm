(define-module (crates-io nu mm nummy) #:use-module (crates-io))

(define-public crate-nummy-0.1.0 (c (n "nummy") (v "0.1.0") (d (list (d (n "decimal") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "fructose") (r "^0.3.3") (d #t) (k 0)))) (h "1lms4i0hw7vxbj5ff7qnrw3m4w8d4mlqg68fjriyjp0pj8j79p0v") (f (quote (("default"))))))

