(define-module (crates-io nu mm nummer) #:use-module (crates-io))

(define-public crate-nummer-0.1.0 (c (n "nummer") (v "0.1.0") (h "0r63y1zv0cv0csrm05173pxzmh43b10k1x1p489bx1n5g6n2dh65")))

(define-public crate-nummer-0.2.0 (c (n "nummer") (v "0.2.0") (h "1acgphpryg4a6ccqg72w49hf47ahlki6qi46shlbnqz0jg3qa4gm")))

(define-public crate-nummer-0.2.1 (c (n "nummer") (v "0.2.1") (h "0r8zhmz6w9b6nlyllg4c1k2359fw45iqix5m30ymir19vr0mvfj7")))

