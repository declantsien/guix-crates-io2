(define-module (crates-io x4 #{48}# x448) #:use-module (crates-io))

(define-public crate-x448-0.1.0 (c (n "x448") (v "0.1.0") (d (list (d (n "ed448-goldilocks") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1vis6y1dmw2nv44kldqhsw08cndpm05kjsii9lyvmsmkw5gfhn1r")))

(define-public crate-x448-0.1.1 (c (n "x448") (v "0.1.1") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "0xr90cx2kwxwxk857nm2anw0mmdlhrj683qyssyrfnb987ikh0qp")))

(define-public crate-x448-0.2.0 (c (n "x448") (v "0.2.0") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1cknphrnvbk292v6cqibszigyqswpla7d9papvm6a36fjjdhmjqi")))

(define-public crate-x448-0.3.0 (c (n "x448") (v "0.3.0") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "07fw61mfp1f4pr42vhgn7xa1w016gaaxj8gfbbg52zmxj0wxdm07")))

(define-public crate-x448-0.4.0 (c (n "x448") (v "0.4.0") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "08bq20976pc9wg4ffnkq6ivk1g9f96mf32shb9sjzsn3dgy3kh3l")))

(define-public crate-x448-0.4.1 (c (n "x448") (v "0.4.1") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "0mp4x7n6l62z2nl5gs3kh2qvdbh5aya7851whn82w5l9rn9944w0")))

(define-public crate-x448-0.5.0 (c (n "x448") (v "0.5.0") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1fgpvzkq8sbqgjbmp8fhbyhirmlldzdap8jywviqa3pflghm30av")))

(define-public crate-x448-0.6.0 (c (n "x448") (v "0.6.0") (d (list (d (n "ed448-goldilocks") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1fzy7xhcldasvnjajp0jc1vwwbfmgh3zgb5wkl40g7p2zba0gkf4")))

