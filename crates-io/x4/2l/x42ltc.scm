(define-module (crates-io x4 #{2l}# x42ltc) #:use-module (crates-io))

(define-public crate-x42ltc-0.0.5 (c (n "x42ltc") (v "0.0.5") (d (list (d (n "x42ltc-sys") (r "^0.0.5") (d #t) (k 0)))) (h "126jp4i9hgg2kz7gbvbynl4fa08v2dsps9m1rz9fpgmjqwqqr7af")))

(define-public crate-x42ltc-0.0.6 (c (n "x42ltc") (v "0.0.6") (d (list (d (n "x42ltc-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0jypyw31nh3z71xxl58hdrfxkp10z98dvxlr1zwb2jhplwnhyms1")))

