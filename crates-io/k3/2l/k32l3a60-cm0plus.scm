(define-module (crates-io k3 #{2l}# k32l3a60-cm0plus) #:use-module (crates-io))

(define-public crate-k32l3a60-cm0plus-0.1.0 (c (n "k32l3a60-cm0plus") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bjy5wp47sizy5yi68vgaqfz45dhjpa7n1z6b2aqgapzr49f0hia") (s 2) (e (quote (("rt" "dep:cortex-m-rt"))))))

