(define-module (crates-io k3 -w k3-wasm-macros) #:use-module (crates-io))

(define-public crate-k3-wasm-macros-0.1.0 (c (n "k3-wasm-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "01vjdxxvd7wflky4qbm42y89vw78ziyccps7ny07grd94p3z40kg") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.1 (c (n "k3-wasm-macros") (v "0.1.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "11ia31qhwyhms2bcqd2k774cin3sl0113239l317ysqy8hjbzd78") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.2 (c (n "k3-wasm-macros") (v "0.1.2") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0pg890asm3df9zgnsavhpf0vj4wp2mz3hy86fnndgqnmhvldjj7c") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.3 (c (n "k3-wasm-macros") (v "0.1.3") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqgkifnip8jkqx577k4zix132r8sckr9lgb2zy63njcwgij2bgg") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.4 (c (n "k3-wasm-macros") (v "0.1.4") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4dn1009viizaf397wsvd6g8wz6xsp9q40wy5xacb75lq4i7z6p") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.5 (c (n "k3-wasm-macros") (v "0.1.5") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "16djnp95rx6jd2hdhw528w2x5fnkiixzvrhk50sz20rcmgm2yy7z") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.6 (c (n "k3-wasm-macros") (v "0.1.6") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0fbivqj4hm34726ragd6ij8vnfp0m7qgdpxkpx9ffrv0qm53hx9l") (f (quote (("nightly") ("default"))))))

(define-public crate-k3-wasm-macros-0.1.7 (c (n "k3-wasm-macros") (v "0.1.7") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1rxnvcspwkmh6144p0z30rjv52rd630vgihw12c9syc52khp76a3") (f (quote (("nightly") ("default"))))))

