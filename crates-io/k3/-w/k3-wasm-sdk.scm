(define-module (crates-io k3 -w k3-wasm-sdk) #:use-module (crates-io))

(define-public crate-k3-wasm-sdk-0.1.0 (c (n "k3-wasm-sdk") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "14km0010akg12gm7llbbxwhjdkqm4sjza1lwvm3pgddp4f519f1q")))

(define-public crate-k3-wasm-sdk-0.1.1 (c (n "k3-wasm-sdk") (v "0.1.1") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "13s6z3fnrhwgn1f2i7yfnca2zqzmsiih707vj89b3fdbmzhda2qy")))

(define-public crate-k3-wasm-sdk-0.1.2 (c (n "k3-wasm-sdk") (v "0.1.2") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "01dhc0ksid6m6awhx8j9xdl8n5zik3m5rjv96q2d0av3d29szrkf")))

(define-public crate-k3-wasm-sdk-0.1.3 (c (n "k3-wasm-sdk") (v "0.1.3") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "0y3gwb7l83cd0vkf2pj24yi7044l0wz60ywig0p25p86yck5fb7h")))

(define-public crate-k3-wasm-sdk-0.1.4 (c (n "k3-wasm-sdk") (v "0.1.4") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "0hvzcz8jkqx66ih82r4zglq9wzdlrndsk0q4l0chsl7sqa79bv31")))

(define-public crate-k3-wasm-sdk-0.1.5 (c (n "k3-wasm-sdk") (v "0.1.5") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "05dba7ikyp69ppnwlm2vh3m7gzjcj9n9d3k5r4zdm7n887jv9l3x")))

(define-public crate-k3-wasm-sdk-0.1.6 (c (n "k3-wasm-sdk") (v "0.1.6") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "0wwga4gsas752b00vvzvmficl6gr9mh63wxck9bjwm6cgxkdx86a")))

(define-public crate-k3-wasm-sdk-0.1.7 (c (n "k3-wasm-sdk") (v "0.1.7") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (f (quote ("wasm"))) (k 0)))) (h "1njyd1mn3p2ss84vmav040m7h7j1g3md9k61k3k2sk3bhzqsqvq9")))

(define-public crate-k3-wasm-sdk-0.1.8 (c (n "k3-wasm-sdk") (v "0.1.8") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (d #t) (k 0)))) (h "1mpiw2n1flqaayf7fy06iamr965sigq4dy45g0kjny4f10cp8d1w")))

(define-public crate-k3-wasm-sdk-0.1.9 (c (n "k3-wasm-sdk") (v "0.1.9") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (f (quote ("wasm"))) (k 0)))) (h "1422scsknvndni9qsg0avicmgw2lriy01h2fssy6d9wsz9a37v2g")))

(define-public crate-k3-wasm-sdk-0.1.10 (c (n "k3-wasm-sdk") (v "0.1.10") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (f (quote ("wasm"))) (k 0)))) (h "1vyl2afz0r36n5qdisgd8nd3kkll14x6xpa43gj6i45sbq0v4cz4")))

(define-public crate-k3-wasm-sdk-0.1.11 (c (n "k3-wasm-sdk") (v "0.1.11") (d (list (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (f (quote ("wasm"))) (k 0)))) (h "1lgjcbkpvppa5mqg6zbr9jv6v8703ry5n4km9422qzk7x82vfj89")))

