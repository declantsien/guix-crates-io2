(define-module (crates-io mp oo mpool) #:use-module (crates-io))

(define-public crate-mpool-0.1.0 (c (n "mpool") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "10zzi38iwggbcqi3yaqaqrpiaij13m4wzrrrlcacyrcvhz39xxvj")))

(define-public crate-mpool-0.1.1 (c (n "mpool") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "160w7yf6vldzs3h33dmk6ymnzyz85dkdcd0y5696312gx924cnax")))

(define-public crate-mpool-0.1.2 (c (n "mpool") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1v08a20ffz9xgphfw6xfkggk4xiggw6axpknbkhwj1g0pyd20dyc")))

(define-public crate-mpool-0.1.3 (c (n "mpool") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "11sjj7a7gg4v5ggzanw46rdbk0drl95zbhmyf6r2cp7gxf6ahg47")))

(define-public crate-mpool-0.1.4 (c (n "mpool") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1nwk6b6a323441n6xwdsj9p8g372q86cc4i5x9h24fxpx43qk9gr")))

