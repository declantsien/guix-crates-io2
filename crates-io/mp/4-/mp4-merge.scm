(define-module (crates-io mp #{4-}# mp4-merge) #:use-module (crates-io))

(define-public crate-mp4-merge-0.1.0 (c (n "mp4-merge") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0xa2wh73z7m00nlbapd8xd888if8kcyc9af0qs6h4f7w3spwhsad")))

(define-public crate-mp4-merge-0.1.1 (c (n "mp4-merge") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0d2xxj920fz2bds19r73gv4s9r2yc6gzc0pwniv98rsgb20bylpf")))

(define-public crate-mp4-merge-0.1.2 (c (n "mp4-merge") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "02q7bqpbygag2s82vds03ffmi4p9qv2vmr3gbgwlg5ym6c2xqw4a")))

(define-public crate-mp4-merge-0.1.3 (c (n "mp4-merge") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1agnaa72bzpmn0657wxvgnjbd6k0gf1v8f289r93yw195rqv00q0")))

(define-public crate-mp4-merge-0.1.5 (c (n "mp4-merge") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1g4mbipdw61rjbvyymj2a0rfar53vbn350ssv86w62fds73dxkds")))

(define-public crate-mp4-merge-0.1.6 (c (n "mp4-merge") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dsd18fc34k0ndmjyf49jqy5libx9cqibsmshz76y48mkbcxb1jc")))

(define-public crate-mp4-merge-0.1.7 (c (n "mp4-merge") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "filetime_creation") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1y0z51rfqq55iy490pxnbhahhqx2my2fh3hqk2whc583hr7cs902")))

(define-public crate-mp4-merge-0.1.8 (c (n "mp4-merge") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "filetime_creation") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k1cnn8azm10p2gqr2ifnvpw17h4yp3b4wn7f52ijaxl57nbn5dj")))

