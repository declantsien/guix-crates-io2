(define-module (crates-io mp #{4-}# mp4-stream) #:use-module (crates-io))

(define-public crate-mp4-stream-0.1.0 (c (n "mp4-stream") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bmff") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5") (f (quote ("no_wrapper"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "x264") (r "^0.5") (d #t) (k 0)))) (h "1abbvwhw3466xz8xivj01nm7nqmlhvx59bj9a80crzs2j0bi837d") (r "1.66")))

