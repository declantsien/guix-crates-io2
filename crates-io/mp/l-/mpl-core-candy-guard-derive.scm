(define-module (crates-io mp l- mpl-core-candy-guard-derive) #:use-module (crates-io))

(define-public crate-mpl-core-candy-guard-derive-0.2.1 (c (n "mpl-core-candy-guard-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0aw61q5hnsmwd1ic1jrkmlyknf89f52imgzqhzmfyaaa8dp5kqgw")))

