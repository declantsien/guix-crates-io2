(define-module (crates-io mp l- mpl-gumdrop) #:use-module (crates-io))

(define-public crate-mpl-gumdrop-0.1.1 (c (n "mpl-gumdrop") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "=1.2.7") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.18") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "=1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (d #t) (k 0)))) (h "0r5b76l6zfpswck5l9mj40sfs5i4hkffkwigi8gw84x1b0575f18") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

