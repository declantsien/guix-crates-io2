(define-module (crates-io mp l- mpl-token-metadata-context-derive) #:use-module (crates-io))

(define-public crate-mpl-token-metadata-context-derive-0.0.1 (c (n "mpl-token-metadata-context-derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1z40mbnb802cy2wsq1cpgk22y3ymj76mi2r74byqif7zf6vjvmzl")))

(define-public crate-mpl-token-metadata-context-derive-0.1.0 (c (n "mpl-token-metadata-context-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cks15cbf6981hx7y2y0ni1bm5an0wkcz9m7rw509shlr1bfz0q7")))

(define-public crate-mpl-token-metadata-context-derive-0.1.1 (c (n "mpl-token-metadata-context-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ay4109j6p0shm8b3skc45l36xrlgb66x38lsms75ls1k7p88vsf")))

(define-public crate-mpl-token-metadata-context-derive-0.2.0 (c (n "mpl-token-metadata-context-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11sp20hazp6wdph1zsir7jvbpysj1ff13l3lirmp45bhcn25rg4a")))

(define-public crate-mpl-token-metadata-context-derive-0.2.1 (c (n "mpl-token-metadata-context-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hzzbdyk5smh7q6932hr5rvrr7s7640i6ma8jj8yxc0maz29p60j")))

(define-public crate-mpl-token-metadata-context-derive-0.3.0 (c (n "mpl-token-metadata-context-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06nh193dvvzn5x249sb1b6rpjz5ii88gxxaflrhkdn8ikq0kk9xm")))

(define-public crate-mpl-token-metadata-context-derive-1.13.2-beta.1 (c (n "mpl-token-metadata-context-derive") (v "1.13.2-beta.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0354yq9na0hckjc1ldaahn5gmzlg3y3vk4xzg4fn3qc759ivzypw") (y #t)))

