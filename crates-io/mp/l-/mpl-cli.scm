(define-module (crates-io mp l- mpl-cli) #:use-module (crates-io))

(define-public crate-mpl-cli-0.1.0-alpha.1 (c (n "mpl-cli") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 0)) (d (n "hard-xml") (r "^1.27.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1jmgdchc4kc07jxgxw1r5bvbhl0jd41whaz838vzdzq5il63c7hn")))

