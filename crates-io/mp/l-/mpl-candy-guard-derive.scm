(define-module (crates-io mp l- mpl-candy-guard-derive) #:use-module (crates-io))

(define-public crate-mpl-candy-guard-derive-0.1.0 (c (n "mpl-candy-guard-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "101dlccmbi1p32asl5hf2blhk4fjzqzhigqrinmwvnwgajv97jp2")))

(define-public crate-mpl-candy-guard-derive-0.2.0 (c (n "mpl-candy-guard-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g8p24vbdyqhz2rx8jx2jxr6hsh0gbwgm6474d5fk0d85q0d7r0q")))

