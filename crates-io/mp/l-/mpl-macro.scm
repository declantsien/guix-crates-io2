(define-module (crates-io mp l- mpl-macro) #:use-module (crates-io))

(define-public crate-mpl-macro-0.1.0 (c (n "mpl-macro") (v "0.1.0") (d (list (d (n "mpl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s3qx3g1k0hkpvx6if8hbrqw9m54cd5vr4s6fy4vija0dhbndzyi")))

(define-public crate-mpl-macro-0.1.1 (c (n "mpl-macro") (v "0.1.1") (d (list (d (n "mpl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ibrwl4km4czsg5fs53a61whwf4y46939fi7lha6lhqsjxg6i95y")))

(define-public crate-mpl-macro-0.1.2 (c (n "mpl-macro") (v "0.1.2") (d (list (d (n "mpl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j2yga1cr7p0h7fs9vgmv27hljyrdnq7f2s1mfz71vvvjgnd14gy")))

