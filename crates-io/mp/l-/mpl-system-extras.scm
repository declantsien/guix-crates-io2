(define-module (crates-io mp l- mpl-system-extras) #:use-module (crates-io))

(define-public crate-mpl-system-extras-0.1.0 (c (n "mpl-system-extras") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r ">=1.14.13, <1.18") (d #t) (k 0)) (d (n "solana-program-test") (r ">=1.14.13, <1.18") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.14.13, <1.18") (d #t) (k 2)) (d (n "spl-token") (r "=3.5.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1jflxl952wg2c5yg51182mwdqxwm0lmcgzm06xjvrbfk3sh5qhna") (f (quote (("test-bpf"))))))

