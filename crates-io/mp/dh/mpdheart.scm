(define-module (crates-io mp dh mpdheart) #:use-module (crates-io))

(define-public crate-mpdheart-0.1.0 (c (n "mpdheart") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (f (quote ("json"))) (d #t) (k 0)))) (h "0aanpy61apq95whzjx03qi4lfybhhkvicrh57zfwnz9i2gf2rwaj")))

