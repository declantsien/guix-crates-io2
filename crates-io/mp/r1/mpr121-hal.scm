(define-module (crates-io mp r1 mpr121-hal) #:use-module (crates-io))

(define-public crate-mpr121-hal-0.1.0 (c (n "mpr121-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "14y3v3flcnq3zxjdih8wypmn749vf34jmry5yzy5193fjpwvs3qi")))

(define-public crate-mpr121-hal-0.2.0 (c (n "mpr121-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1361y8mpqi96b5szyyfawkpdqa7vdfcmxl0f1cfm00fhl4x5j0md")))

(define-public crate-mpr121-hal-0.3.0 (c (n "mpr121-hal") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "07zp5kmv8g7832wv9byrijk3hc0pqwijrv1zkp4kxpgi0276kr49")))

