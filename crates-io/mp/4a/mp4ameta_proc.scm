(define-module (crates-io mp #{4a}# mp4ameta_proc) #:use-module (crates-io))

(define-public crate-mp4ameta_proc-0.1.0 (c (n "mp4ameta_proc") (v "0.1.0") (h "15zc4aazrrsrm71fa03ygn63j7k9lm2qd6jzx6iz1si202lcbmbh")))

(define-public crate-mp4ameta_proc-0.1.1 (c (n "mp4ameta_proc") (v "0.1.1") (h "1i82asxzdl4cxrb9y50s1y5j40qgx4c5ca89y2vy6rvb3x287bk4")))

(define-public crate-mp4ameta_proc-0.2.0 (c (n "mp4ameta_proc") (v "0.2.0") (h "1c83r3nvf1s2dpizr96aybx5f6afjxbbvkq27lv12xlv4h57cy4v")))

(define-public crate-mp4ameta_proc-0.2.1 (c (n "mp4ameta_proc") (v "0.2.1") (h "0ay02yjv0d1h3fpsizkxa8jfncjvxwv16icsl95rbd030r15c4yz")))

(define-public crate-mp4ameta_proc-0.3.0 (c (n "mp4ameta_proc") (v "0.3.0") (h "04jybm1b8q7559pbdr8m520nkg4jyrdf77qfyb9mb4nb2jcjm1m4")))

(define-public crate-mp4ameta_proc-0.4.0 (c (n "mp4ameta_proc") (v "0.4.0") (h "12jws7mqmhkps5kacs1nx6k5069xnnvqfgc0kyl9d7nn7hhcwxa9")))

(define-public crate-mp4ameta_proc-0.5.0 (c (n "mp4ameta_proc") (v "0.5.0") (h "152was15fy9h193zbxsmww6l4vc4aiq9qhlbd4l4qwg7b89k535f")))

(define-public crate-mp4ameta_proc-0.6.0 (c (n "mp4ameta_proc") (v "0.6.0") (h "01i8snk5kjd27zly5k1y6arvv80d6q1lh43pbxk0l33ls49wmp07")))

