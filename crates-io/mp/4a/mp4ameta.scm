(define-module (crates-io mp #{4a}# mp4ameta) #:use-module (crates-io))

(define-public crate-mp4ameta-0.1.0 (c (n "mp4ameta") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "1yiayvnsry9nr2am0h6fr17nnllrwhqz6mcy2zi53naxfy123d5l")))

(define-public crate-mp4ameta-0.1.1 (c (n "mp4ameta") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "1vi2q37vjkp3q5z2r6nwyz22gi89dkv3grf01s20m5m7215wm0c7")))

(define-public crate-mp4ameta-0.2.0 (c (n "mp4ameta") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0ag8aq1pzivvnp1sjamf7sfpxbhpx78mnzv7ncjvrbykslkz6f1m")))

(define-public crate-mp4ameta-0.2.1 (c (n "mp4ameta") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0gk2ilgxfg3bn1as2r24h10gqrickazy5l8z6ja4zcsvnf23f70c")))

(define-public crate-mp4ameta-0.2.2 (c (n "mp4ameta") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0y2q2c6xd17ms67r00yy92vx2vl65xqkdqi7q41y61b61r4ff39b")))

(define-public crate-mp4ameta-0.3.0 (c (n "mp4ameta") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0zsj4w9y5h3akraa4f5qpp5hazhkh39k8vlzjzi6r64n6x30fhqv")))

(define-public crate-mp4ameta-0.4.0 (c (n "mp4ameta") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0zg5q8xk7hb1a7x6409iwnjb8sjwiq6f2l5dlbm7mzd2h7iik7fq")))

(define-public crate-mp4ameta-0.4.1 (c (n "mp4ameta") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.1.0") (d #t) (k 0)))) (h "0y0gh40kzq7590lgmzwdxwcjf0c610amyk86b579647idy528p54")))

(define-public crate-mp4ameta-0.5.0 (c (n "mp4ameta") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1fsxxa8j29f1ifwpwfldhmrd6r3r1lx4dlzx9yajzhgvr2mlp2ia")))

(define-public crate-mp4ameta-0.5.1 (c (n "mp4ameta") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1930m9lbyf1sw0qnp16kh9fa35n0g67c3hcifdk81b9pp9lvds8m")))

(define-public crate-mp4ameta-0.6.0 (c (n "mp4ameta") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1nwbv5769f815ksvqlh7pzi3n01cs5q93kwh4by7lw0wknilkmb9")))

(define-public crate-mp4ameta-0.6.1 (c (n "mp4ameta") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1ch1jsp6vilkxlb8ds0cxwkq9l3jfifnx6r0w9xrxapya6xa5xqy")))

(define-public crate-mp4ameta-0.7.0 (c (n "mp4ameta") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "13n8r5mhvadg0ygwg9x9migxrcayi98lcb038nxkzhkgn61l2283")))

(define-public crate-mp4ameta-0.7.1 (c (n "mp4ameta") (v "0.7.1") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "walkdir") (r ">=2.3.1, <3.0.0") (d #t) (k 2)))) (h "0cdf963v561sws5sfmwcjr2jnngvshn8awv436hs9d3j988x8hqd")))

(define-public crate-mp4ameta-0.7.2 (c (n "mp4ameta") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1zazrwv5jda7p9cyr2mfc5bdlqsi9ddw8vjzvhaa34wjjmwcx6n9") (y #t)))

(define-public crate-mp4ameta-0.7.3 (c (n "mp4ameta") (v "0.7.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1i5j3n9a1bjp7zf22x3milll7s7l77m77nzw35qpjg9m77hlc176")))

(define-public crate-mp4ameta-0.8.0 (c (n "mp4ameta") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1cvl5lxh115xfdglzsc97c9q52as69ksg67zrq5rlaz3xwmxl36q")))

(define-public crate-mp4ameta-0.9.0 (c (n "mp4ameta") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "04j8mamcwq3bmvab9bvnpv4bdv129y8akbqzm9awlmyph5g0bcy6")))

(define-public crate-mp4ameta-0.9.1 (c (n "mp4ameta") (v "0.9.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0rjfkdivgqgc0nxf0irjp7wabwhskj5wrljk3jnyz446dgl6z7v1")))

(define-public crate-mp4ameta-0.10.0 (c (n "mp4ameta") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0qjz7ixma56b7lgzgs0845yb4vzy8826i8dqk8nbgd1wyzbvhj84")))

(define-public crate-mp4ameta-0.10.1 (c (n "mp4ameta") (v "0.10.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0c0yms1343v4xpm9cz4l731p2h8g5n8q4abmcdpayj3m7c2izajn")))

(define-public crate-mp4ameta-0.10.2 (c (n "mp4ameta") (v "0.10.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1k2awwz8q7g4pb5k6lppwidirp55z1rzxx9r96d247bgaj1x3qa3")))

(define-public crate-mp4ameta-0.11.0 (c (n "mp4ameta") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mp4ameta_proc") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "10by0ms6ml5wi4k9axls4gvcisk9jbm70z35g4zrladmiqpdc8zb")))

