(define-module (crates-io mp l3 mpl3115) #:use-module (crates-io))

(define-public crate-mpl3115-0.1.0 (c (n "mpl3115") (v "0.1.0") (d (list (d (n "cast") (r "^0.2") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "18vblv4kzc9pm3d84rqy2vpk40748f7vjncidgljkkrnkpg77iy6")))

