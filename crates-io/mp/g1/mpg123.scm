(define-module (crates-io mp g1 mpg123) #:use-module (crates-io))

(define-public crate-mpg123-0.1.0 (c (n "mpg123") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1r0mcxa8qh2prqnmlkj3adwyp17cyf4gw9xi3snw3frryqvbng4r")))

(define-public crate-mpg123-0.1.1 (c (n "mpg123") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1bvyp0nq5lar6n37rmw2iliv4iqipyzivpf5di25r1ym5kriainv")))

(define-public crate-mpg123-0.1.2 (c (n "mpg123") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.6.0") (d #t) (k 0)))) (h "09102084awq9w3jq3blxnd07qgpgjidllpflshgd7q309xfx77aq")))

