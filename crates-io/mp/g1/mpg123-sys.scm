(define-module (crates-io mp g1 mpg123-sys) #:use-module (crates-io))

(define-public crate-mpg123-sys-0.0.1 (c (n "mpg123-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a5xbncjjs6gs0h5ybfa0j5aprih90xkxkqq2js87w2fman4vay8")))

(define-public crate-mpg123-sys-0.0.2 (c (n "mpg123-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q0k5prv8mwj8ccv6i00n03d8mvli2k1m2siylyngwg16f7knh9r")))

(define-public crate-mpg123-sys-0.0.3 (c (n "mpg123-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g97pn36rmx1qrq1jm3j5vzdjzv04h09h6l521n772mhmbw262vq")))

(define-public crate-mpg123-sys-0.1.0 (c (n "mpg123-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ci46k2qaaz25973w5nwndki3hp3x613rmj0ssyb8glwggwdanih")))

(define-public crate-mpg123-sys-0.2.0 (c (n "mpg123-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m616vl87svz9dmgf7cf9zs70bj15gxj9v2qaf72dxk0fwcs4yym")))

(define-public crate-mpg123-sys-0.3.0 (c (n "mpg123-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vallqfkb1d6d5xrpbr750c0gvv3g9i746qs0a61j3939dlsz6pw") (f (quote (("static"))))))

(define-public crate-mpg123-sys-0.3.1 (c (n "mpg123-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ibj3d31fdz1gv5n7fkrc8j1gb8aasbw31mcm0ln2z56l80prr29") (f (quote (("static"))))))

(define-public crate-mpg123-sys-0.4.0 (c (n "mpg123-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "057rh2wcibpvnn8xic8sjgd3b622hjilfh15jbzgvc0bar7c5nnl") (f (quote (("static"))))))

(define-public crate-mpg123-sys-0.4.1 (c (n "mpg123-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16pjmb80v5i8sn1sl65f0wys9smhrkbv19i3dvnjh10nrlsn3zm5") (f (quote (("static"))))))

(define-public crate-mpg123-sys-0.5.0 (c (n "mpg123-sys") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09qva7mvdqx10byivbfs9zsr2my15lwgwcfc4v44l068bzykfxq9") (f (quote (("static"))))))

(define-public crate-mpg123-sys-0.6.0 (c (n "mpg123-sys") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "043nj0bp44srx6q1ih3daxlciangpgdgq2f1j4q45hc5klijcp57") (f (quote (("static"))))))

