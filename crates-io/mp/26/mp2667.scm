(define-module (crates-io mp #{26}# mp2667) #:use-module (crates-io))

(define-public crate-mp2667-0.0.0 (c (n "mp2667") (v "0.0.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "17q9w2qvb6f468v2hxffci78b2n00xc6clldp2j79dzh5vvx8l8p")))

(define-public crate-mp2667-0.0.1 (c (n "mp2667") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1m4gvhvawziaivhwjvrlbixssv1nmj7fqqfnhapgqylf8r3mhdi3")))

(define-public crate-mp2667-0.0.2 (c (n "mp2667") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "0czl8w0xiz6anzsdl10z9d1hzn8ikbd6niwnhjk600m6g9d8ajmk")))

(define-public crate-mp2667-0.0.3 (c (n "mp2667") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "0mrvz9yq3nkza86c34wv7aa2hc3jglg2gm6d9w9x84yb9r75qsa2")))

(define-public crate-mp2667-0.0.4 (c (n "mp2667") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1bk731xk7l2l84gbpc1rs387jb5m98nmi846xj28i3h4dbixpmps")))

(define-public crate-mp2667-0.0.5 (c (n "mp2667") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "17hqvqy54a2vz4jd4fckmzps7ndsbh1mk1nkb6jdnkyjmz8w4sjb")))

