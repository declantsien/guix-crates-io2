(define-module (crates-io mp #{4p}# mp4parse) #:use-module (crates-io))

(define-public crate-mp4parse-0.0.2 (c (n "mp4parse") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "1xa2spa6w5621yrpz21v9grcczaija39nmj86rr56bh0gw2hyx1r")))

(define-public crate-mp4parse-0.0.3 (c (n "mp4parse") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0hdgc16l0g0r5piv7nb5s5d56cgvnwxgg8ddzyfbbfk99jag0mng")))

(define-public crate-mp4parse-0.0.4 (c (n "mp4parse") (v "0.0.4") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "10ypyavgd15shn9jjlifcgk68n29fs2cqyq849giccvi03anylms")))

(define-public crate-mp4parse-0.0.5 (c (n "mp4parse") (v "0.0.5") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "13xazhhkf92cy6ax6bxpnqbwr3ddspmg3xy27njfw17bjk3rcgxv")))

(define-public crate-mp4parse-0.0.6 (c (n "mp4parse") (v "0.0.6") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "1r8dls8iwzpiz3rvxnmlac3wdhywh2sshkzs2xzxrjwy6bxf4kaq")))

(define-public crate-mp4parse-0.0.7 (c (n "mp4parse") (v "0.0.7") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "1kby6sjqyr249sxkb3gljwxky7rhpw3cijr24dvz5gmr0hfngava")))

(define-public crate-mp4parse-0.0.8 (c (n "mp4parse") (v "0.0.8") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0wh47vh4iai4csx43qxrlzimpxi7dxc6bnqpwv543iam0aa0116j")))

(define-public crate-mp4parse-0.0.9 (c (n "mp4parse") (v "0.0.9") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0jr7r5zd3z5mmvznpamr20pyx0lx4fkj9cxapwc1r92kky1p7qwl")))

(define-public crate-mp4parse-0.1.0 (c (n "mp4parse") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0hdvryc550qdfzvhkk4rk60jys753p5ic7bphb6fyr8yjzi6ar24") (y #t)))

(define-public crate-mp4parse-0.1.1 (c (n "mp4parse") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0j9cg4m06ylylsd47vvrxwsnn5lvrhjrszs9acsiy85qp8l6c5d9") (y #t)))

(define-public crate-mp4parse-0.1.2 (c (n "mp4parse") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "1rzdfmqzq6f4f4dl08a7kbv49h8rpa09jix62cnmylpc6cm203mp")))

(define-public crate-mp4parse-0.1.3 (c (n "mp4parse") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0zwpnkv0h162kmhhv6r3nasa0d882af3lnslxihi7fi6svacn873")))

(define-public crate-mp4parse-0.1.4 (c (n "mp4parse") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "18g4gj9p4hhgmwvdwkin431bnyc9451j51mmpmy9hdi8qmrmii33")))

(define-public crate-mp4parse-0.1.5 (c (n "mp4parse") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.3.8") (d #t) (k 0)))) (h "0qjh926rkw7bz36lj5z61swns5q4zfq1mbwlyh1ar71cz7r64j95")))

(define-public crate-mp4parse-0.1.6 (c (n "mp4parse") (v "0.1.6") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.0") (d #t) (k 2)))) (h "1fcncb2cmxp0bi7ix9b550p1myyyf7zxrwi730p2ypp7i56ks3jv")))

(define-public crate-mp4parse-0.2.0 (c (n "mp4parse") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.0") (d #t) (k 2)))) (h "05xfld4162793lyig6r01amgal3skxw4xb7xydrf9hcia677ykq8")))

(define-public crate-mp4parse-0.2.1 (c (n "mp4parse") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.0") (d #t) (k 2)))) (h "1r1bh3yimi7gvxvmck63m6s1ilgvci953ff9kf9rczmd6m0qn0cb")))

(define-public crate-mp4parse-0.3.0 (c (n "mp4parse") (v "0.3.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3.2") (d #t) (k 1)) (d (n "test-assembler") (r "^0.1.1") (d #t) (k 2)))) (h "1dp6iwghrkf7dgmb9i4248z0nbazwbfalvyxrbz8c78b8ki727zy") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.4.0 (c (n "mp4parse") (v "0.4.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3.2") (d #t) (k 1)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "04hdk7bzx5a8kz447xinshsyyzl7a1m5amacwm2kzhmxypgv7si8") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.4.1 (c (n "mp4parse") (v "0.4.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3.2") (d #t) (k 1)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "171ajm1xmqf9y3a7mmqw7yq1jb9x7c4q4lpszygp0nq206hab4qd") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.5.0 (c (n "mp4parse") (v "0.5.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "02iraaqzra9g3hga9z0bwjvqwshqqyr2qbd90z9skqaff1wlng8y") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.5.1 (c (n "mp4parse") (v "0.5.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "012mlb9x9jibhm1h50cscskmpgvrrhd1ba5m519cpa83cz83c5qy") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.6.0 (c (n "mp4parse") (v "0.6.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "1dsj4jr3kv9k9a1hp082dykdgrabpdxhprsvhrkbzfqiyn7piwj0") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.7.0 (c (n "mp4parse") (v "0.7.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "17n6h3k4bqhhjs04ylqszk1llh4y1fnqp8i6fa09lamnbqr9g29g") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.7.1 (c (n "mp4parse") (v "0.7.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "0ynai8ywwydwzfyx6pbnqj58c2p53aspwk5mh69dalyykqgnb0bv") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.8.0 (c (n "mp4parse") (v "0.8.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "1pqsv1zm5x9nnkjrv25qv2yg6ba4dny6bsy6cfdzrdm8kwg2r54r") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.9.0 (c (n "mp4parse") (v "0.9.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "afl-plugin") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "mp4parse_fallible") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "030w6r18zdc9rxym9xg73rb4f79sgqgag2nmy94hgd8fq96qazms") (f (quote (("fuzz" "afl" "afl-plugin" "abort_on_panic"))))))

(define-public crate-mp4parse-0.9.1 (c (n "mp4parse") (v "0.9.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "mp4parse_fallible") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "1851m66c81g1mlfmkba5kyigpdg1gvx05yv1p3cidzf0kdwy68gq") (f (quote (("fuzz" "afl" "abort_on_panic"))))))

(define-public crate-mp4parse-0.10.0 (c (n "mp4parse") (v "0.10.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mp4parse_fallible") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "1xqva7mnwvsg9whfmvx1n97vralj6agf10js8gw0xvwlgl721jxj") (f (quote (("fuzz" "afl" "abort_on_panic"))))))

(define-public crate-mp4parse-0.10.1 (c (n "mp4parse") (v "0.10.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mp4parse_fallible") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "0m7jy8fxx4yfyfplsds6ry89ckl6awrdwpc079wyags4cj2745kk") (f (quote (("fuzz" "afl" "abort_on_panic"))))))

(define-public crate-mp4parse-0.11.2 (c (n "mp4parse") (v "0.11.2") (d (list (d (n "abort_on_panic") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "afl") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitreader") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mp4parse_fallible") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)))) (h "0054chdiy19yg3bmslc130mk38skna31q7nrb8sqg0jq4r21q9r3") (f (quote (("fuzz" "afl" "abort_on_panic"))))))

(define-public crate-mp4parse-0.11.4 (c (n "mp4parse") (v "0.11.4") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0kx8l0iz5gghwnhdz67ykkh6ghk64vwndizb9mnnl8d49vmrxqxq") (f (quote (("mp4parse_fallible")))) (y #t)))

(define-public crate-mp4parse-0.11.5 (c (n "mp4parse") (v "0.11.5") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.3") (f (quote ("std_io"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1wwkkjrf2p9qy58sa5pa9wqlfhd36zcp8h0lgrz9l5pgkr89b6yk")))

(define-public crate-mp4parse-0.11.6 (c (n "mp4parse") (v "0.11.6") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4") (f (quote ("std_io"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0rd35ml4mm4xmz3n95wn5b4sgrj80q9i300759hzx1wiy2rrgsk0") (f (quote (("unstable-api") ("mp4v") ("missing-pixi-permitted") ("meta-xml") ("3gpp")))) (y #t)))

(define-public crate-mp4parse-0.12.0 (c (n "mp4parse") (v "0.12.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4") (f (quote ("std_io"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1ppqv60qiyrnbb996gb1sik08c0j2i317llv3rrcwb1cjg3bdlk7") (f (quote (("unstable-api") ("mp4v") ("missing-pixi-permitted") ("meta-xml") ("3gpp"))))))

(define-public crate-mp4parse-0.12.1 (c (n "mp4parse") (v "0.12.1") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4") (f (quote ("std_io"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1scynvlmiy6xv2rrzzpijd812amh6a863na8i0xrcw5d9d08kl8h") (f (quote (("unstable-api") ("mp4v") ("missing-pixi-permitted") ("meta-xml") ("3gpp"))))))

(define-public crate-mp4parse-0.17.0 (c (n "mp4parse") (v "0.17.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fallible_collections") (r "^0.4") (f (quote ("std_io"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0w654hv04w1zi2m9b1kji2610mrfrc554xqw4par5kn6sc1m58v3") (f (quote (("unstable-api") ("mp4v") ("missing-pixi-permitted") ("meta-xml") ("3gpp"))))))

