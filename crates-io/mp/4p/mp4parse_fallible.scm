(define-module (crates-io mp #{4p}# mp4parse_fallible) #:use-module (crates-io))

(define-public crate-mp4parse_fallible-0.0.1 (c (n "mp4parse_fallible") (v "0.0.1") (h "0c6d8pqca3w1qm308whlw4993znkhdc4fbphxs2gkf3fyypc49k6")))

(define-public crate-mp4parse_fallible-0.0.2 (c (n "mp4parse_fallible") (v "0.0.2") (h "18yfi0xfpw347szcggl3wqpyi0dx2vnrd9h1ljmy5500ahi0p6qq")))

(define-public crate-mp4parse_fallible-0.0.3 (c (n "mp4parse_fallible") (v "0.0.3") (h "02mi37jj8m6si1v73dy9369v2cj9329xysvv88872gmcf4s7fkvh")))

