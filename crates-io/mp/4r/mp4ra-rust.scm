(define-module (crates-io mp #{4r}# mp4ra-rust) #:use-module (crates-io))

(define-public crate-mp4ra-rust-0.1.0 (c (n "mp4ra-rust") (v "0.1.0") (d (list (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)))) (h "09kns5klmw1qh21n0k3pb1mf84gl82x64z4lc8lq9wrvnh1sz7dy")))

(define-public crate-mp4ra-rust-0.2.0 (c (n "mp4ra-rust") (v "0.2.0") (d (list (d (n "four-cc") (r "^0.3.0") (d #t) (k 0)))) (h "1i80yrblqg447c0g7wi1sbsywm0y1vrfrpps8sp6i4nyj6ahvkzi")))

(define-public crate-mp4ra-rust-0.3.0 (c (n "mp4ra-rust") (v "0.3.0") (d (list (d (n "four-cc") (r "^0.4.0") (d #t) (k 0)))) (h "1drnlm24isb9fanwjg8s8lc5lb6xydk2wj3hcan6cp88cww3vg7x")))

