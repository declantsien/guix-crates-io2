(define-module (crates-io mp ss mpsse) #:use-module (crates-io))

(define-public crate-mpsse-0.1.0 (c (n "mpsse") (v "0.1.0") (h "1fidmc3kjbbsks6w44v95fsqnk4d1bdd7430rycl05snxs0mxwvi")))

(define-public crate-mpsse-0.1.1 (c (n "mpsse") (v "0.1.1") (h "0h8kf6i2byvh60r2w4gyr6wh5w20qfv9v8d5ikgsnsd288z5dalj")))

(define-public crate-mpsse-0.1.2 (c (n "mpsse") (v "0.1.2") (h "0lx151fxld8ywi3a5rw596xlvg807cr06r5c784zchnbdip8v561")))

(define-public crate-mpsse-0.2.0 (c (n "mpsse") (v "0.2.0") (h "0m67384rx05zbzjs43qjx71g8agf29fw6q82ljzw252jg0zvbab5")))

(define-public crate-mpsse-0.3.0 (c (n "mpsse") (v "0.3.0") (h "0r693jiixyd7kihwjm5xz40npy0j4almd0vv13c8pfr5xld82rid")))

