(define-module (crates-io mp rs mprs) #:use-module (crates-io))

(define-public crate-mprs-0.1.8 (c (n "mprs") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)))) (h "1mqi5azzr7mn4h46gmzpm26w4p37d9n9qf2i9xgmf35dbai7ryxa") (y #t)))

(define-public crate-mprs-0.1.9 (c (n "mprs") (v "0.1.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "configr") (r "^0.8.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0966r968y5ngzpk2j4zn5rilbdwkan882c3q75pw1g2gz7i1q483") (y #t)))

(define-public crate-mprs-0.1.10 (c (n "mprs") (v "0.1.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "configr") (r "^0.8.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0r6rsz1skzliy1qbjfbkhkvf8zc7kgh34jg3l0kisn452zagdi7c") (y #t)))

(define-public crate-mprs-0.2.0-beta (c (n "mprs") (v "0.2.0-beta") (d (list (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1h466g4jdvrh3dny66751anrw0hzs733qzv6vyan9wrzmcd82052")))

(define-public crate-mprs-0.2.0 (c (n "mprs") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9.0") (d #t) (k 0)))) (h "0rn3nwhn173hb3kkcl82s0rl04r1isd02yj0vpzv96cihdnp57if")))

