(define-module (crates-io mp d- mpd-easy) #:use-module (crates-io))

(define-public crate-mpd-easy-0.1.4 (c (n "mpd-easy") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0cvl1zn15l277z4mlzch8zzi4sx59d3k1bsfbwm8jm3gi6nwdayw")))

(define-public crate-mpd-easy-0.1.5 (c (n "mpd-easy") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1c4vwpj92q8lh6y8hq36gr99pz40jpwa2ik3rq7rgkgrzxx5lhic")))

(define-public crate-mpd-easy-0.1.6 (c (n "mpd-easy") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1ifwfjqizn7442hzyyhiw1anvvf6rq5nikdl9jh96299m34irjxj")))

(define-public crate-mpd-easy-0.1.7 (c (n "mpd-easy") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1kny2w74xkrn6j2nyxf67jcfz3ffsb3crpfrpl6bwhbbjpcx1007")))

(define-public crate-mpd-easy-0.2.0 (c (n "mpd-easy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1d0n4c6kd03mn84iiwgy4p22fsdbgr43pfn189kgslp7xzrpzydj")))

(define-public crate-mpd-easy-0.2.1 (c (n "mpd-easy") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0zvw6zpd5hy36r0wk1fp5cddkd92w8ixqckcqq27pxv84fzq8v9m")))

