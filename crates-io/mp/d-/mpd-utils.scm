(define-module (crates-io mp d- mpd-utils) #:use-module (crates-io))

(define-public crate-mpd-utils-0.1.0 (c (n "mpd-utils") (v "0.1.0") (d (list (d (n "async-once-cell") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "mpd_client") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1mxwpbx5i8rf1bpghp711xmvf9vk3pv024l1y8iwn24z8v7fk3qd")))

(define-public crate-mpd-utils-0.2.0 (c (n "mpd-utils") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "mpd_client") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "197qmfx689izffmkd50xh32a8c8zpkz6c08vcy58c15675nsi0bh")))

(define-public crate-mpd-utils-0.2.1 (c (n "mpd-utils") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "mpd_client") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1ljlybw28l21f55s1ymg5c3xqlq9lsd595jyz7plcsi0ndqhxrcg")))

