(define-module (crates-io mp mf mpmfnum) #:use-module (crates-io))

(define-public crate-mpmfnum-0.1.0 (c (n "mpmfnum") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "~1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (f (quote ("num-traits"))) (d #t) (k 0)))) (h "0r86gyf76ragyi7vmf1lkkkkxhbyb7rq9dl1cpylld71ldzdpqm6")))

(define-public crate-mpmfnum-0.2.0 (c (n "mpmfnum") (v "0.2.0") (d (list (d (n "gmp-mpfr-sys") (r "~1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (f (quote ("num-traits"))) (d #t) (k 0)))) (h "01mn7s31bgx05yw5nzdz8xjgmnndshin76c0bq25j7xia6g7vzaf")))

