(define-module (crates-io mp st mpst-seq-proc) #:use-module (crates-io))

(define-public crate-mpst-seq-proc-0.0.2 (c (n "mpst-seq-proc") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "06nwxy3xg3dk765h6g2qcyvpcq5ck815pbrki3n47phxfrwlkm31")))

(define-public crate-mpst-seq-proc-0.0.3 (c (n "mpst-seq-proc") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0jikkqd83i2nvidb8hv093vyxaffnmplcq8axf0y9jn9kpfzf5cy")))

(define-public crate-mpst-seq-proc-0.0.4 (c (n "mpst-seq-proc") (v "0.0.4") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1f0y1b4x2325j23ragf7dppam5b39w0sq25vjnxkvrfllh9s0cli")))

(define-public crate-mpst-seq-proc-0.0.5 (c (n "mpst-seq-proc") (v "0.0.5") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "05sd3m47i4i6a0smj8i2r7s9l17k8lkrw88qkxa7bq6fvg6w05cx")))

(define-public crate-mpst-seq-proc-0.0.6 (c (n "mpst-seq-proc") (v "0.0.6") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0l7nrs7ppnaqsr1y0l0972syw8dlcziqxybbpvqvkwfy13ww3779")))

(define-public crate-mpst-seq-proc-0.0.7 (c (n "mpst-seq-proc") (v "0.0.7") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0iwrmahxs7qvpi6n9hqnvd1c3cingldzhydh1jn1f1g2z1q3lrh1")))

(define-public crate-mpst-seq-proc-0.0.8 (c (n "mpst-seq-proc") (v "0.0.8") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "151dpkkg9wswr3c1rqibdf48hpz9rdkbm1cry0mpa49klcpjnqil")))

(define-public crate-mpst-seq-proc-0.0.9 (c (n "mpst-seq-proc") (v "0.0.9") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1bnd7c6bvxrkidmy1xlvy1xv43c1bdlhgzyjdv17pbl76q2fhmi9")))

(define-public crate-mpst-seq-proc-0.0.10 (c (n "mpst-seq-proc") (v "0.0.10") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1zlw84jr755dlp4ib40231in0rls8j9qmi7rk2j7hmwvcjj8csnz")))

(define-public crate-mpst-seq-proc-0.0.11 (c (n "mpst-seq-proc") (v "0.0.11") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0gcgkd730sr5i706fxnk25vskh4ffcvzmn29k1y7b84s4lwkczw7")))

(define-public crate-mpst-seq-proc-0.1.12 (c (n "mpst-seq-proc") (v "0.1.12") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0c3bhw0c4qabj00py6qrd3iwdmjfxcdal9hwspgms0pm2zjsc2km")))

(define-public crate-mpst-seq-proc-0.1.13 (c (n "mpst-seq-proc") (v "0.1.13") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "167csykik6ir3652c5ql586y3kr0xnfni95jv87iq4a83qh2p31w")))

(define-public crate-mpst-seq-proc-0.1.14 (c (n "mpst-seq-proc") (v "0.1.14") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "0qswkzqgq5zpgyk3rw82ddaf5678mqbbnrb5b7afkvs84hf5ih72") (r "1.77")))

