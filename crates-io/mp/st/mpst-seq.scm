(define-module (crates-io mp st mpst-seq) #:use-module (crates-io))

(define-public crate-mpst-seq-0.0.2 (c (n "mpst-seq") (v "0.0.2") (d (list (d (n "mpst-seq-proc") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "1s145y6g5mw4jgv38j3nbzhnzr9jmm9c4k56x5vf9czx8r3fc01s")))

(define-public crate-mpst-seq-0.0.3 (c (n "mpst-seq") (v "0.0.3") (d (list (d (n "mpst-seq-proc") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "1g4aq9pjam1bdlvc3nbshn1s41dvwpfrxl4q5cp8g966jjldm3dr")))

(define-public crate-mpst-seq-0.0.4 (c (n "mpst-seq") (v "0.0.4") (d (list (d (n "mpst-seq-proc") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "0cb71cz0jkngdi34i81rj1f07ywq76d3hnqw13jgq8i94ikxw90k")))

(define-public crate-mpst-seq-0.0.5 (c (n "mpst-seq") (v "0.0.5") (d (list (d (n "mpst-seq-proc") (r "^0.0.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "011cdx7v6w56n4xn61c3xpnmb57rx7ah9xvphl0qqz1kcmbw2zzf")))

(define-public crate-mpst-seq-0.0.6 (c (n "mpst-seq") (v "0.0.6") (d (list (d (n "mpst-seq-proc") (r "^0.0.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "05v3xgvbsxp4jdhwv7ws15x5pnq9amiwjwrv20lgzkhsq8h0i6zk")))

(define-public crate-mpst-seq-0.0.7 (c (n "mpst-seq") (v "0.0.7") (d (list (d (n "mpst-seq-proc") (r "^0.0.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1js3sq4wf96n220knj7b1xfi54qbsk43z9nicqwql037kpw8vj3z")))

(define-public crate-mpst-seq-0.0.8 (c (n "mpst-seq") (v "0.0.8") (d (list (d (n "mpst-seq-proc") (r "^0.0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)))) (h "1vhsvlgwa5zfyp20p3i4mmqrxcbnm9zc6kil6xq2pmdxfdq590c6")))

(define-public crate-mpst-seq-0.0.9 (c (n "mpst-seq") (v "0.0.9") (d (list (d (n "mpst-seq-proc") (r "^0.0.9") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)))) (h "1l7mxqibbd956d09jlyifh2sckfk6lmi9slw2kbahcbm4hyyg689")))

(define-public crate-mpst-seq-0.0.10 (c (n "mpst-seq") (v "0.0.10") (d (list (d (n "mpst-seq-proc") (r "^0.0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)))) (h "1cr6ggpjy7fh4dnqijd6ha7y7kkx7hrl9k1d05892wsp1868dzid")))

(define-public crate-mpst-seq-0.0.11 (c (n "mpst-seq") (v "0.0.11") (d (list (d (n "mpst-seq-proc") (r "^0.0.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)))) (h "15gblqi4f0dh92p576fsi3y0l26hi2n5jrk81zyqh94vcc1c6xv1")))

(define-public crate-mpst-seq-0.1.15 (c (n "mpst-seq") (v "0.1.15") (d (list (d (n "mpst-seq-proc") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1ziq7r8i4279ip4cwsrpgrjac501yskmz9p4g2np2pmwxnwp9v7a")))

(define-public crate-mpst-seq-0.1.16 (c (n "mpst-seq") (v "0.1.16") (d (list (d (n "mpst-seq-proc") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "05yczcyq8wvvn37n7az554zgwdjfk2yfrl5k0svrq1bbwrdlx0vc")))

(define-public crate-mpst-seq-0.1.17 (c (n "mpst-seq") (v "0.1.17") (h "0z2g3ihmbfa1175mmpksmvwy1mjycgpwyqfdxcngibl7sgx10j39")))

(define-public crate-mpst-seq-0.1.18 (c (n "mpst-seq") (v "0.1.18") (h "1vaiy369285q7jhidla97rp99y0jwkpvd4aidr9l9kbghag8yiyf")))

