(define-module (crates-io mp es mpesa_derive) #:use-module (crates-io))

(define-public crate-mpesa_derive-0.1.0 (c (n "mpesa_derive") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vp4q0j597y1rwj1zjfy79j8rxv6dxsnllywdji3izbgs4zmgmyz")))

(define-public crate-mpesa_derive-0.1.1 (c (n "mpesa_derive") (v "0.1.1") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11mhlkbcr8bhs56q2sn6w6f4jf7a80j4csva5cxi0n485gkb4yz3")))

(define-public crate-mpesa_derive-0.2.0 (c (n "mpesa_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cjqml05054fyr2ljv9pklxi624b0qbl44znrvs0h54yi1sy4ip4")))

(define-public crate-mpesa_derive-0.2.1 (c (n "mpesa_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0msmkbygvjb2nlglzk1q28p0fqchs2ryvwmb6182wx19f3ds7z3w")))

(define-public crate-mpesa_derive-0.3.0 (c (n "mpesa_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kwrjymicwk4h5wdr8sy8c5fzq6hbqyl05f4zk08i2z1svh3cbsr")))

