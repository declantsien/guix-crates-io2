(define-module (crates-io mp ar mparsed) #:use-module (crates-io))

(define-public crate-mparsed-0.1.0 (c (n "mparsed") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "envy") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ga7zsp4l4x0q9s7a25vd1l7cc0lwj26dadrl66ickprwqh8kg9n")))

(define-public crate-mparsed-0.2.0 (c (n "mparsed") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "envy") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "03dwdhq5s1vjy2yy569g3s981z7m8b7hqpzzdhrldi0xvqml90j5")))

