(define-module (crates-io mp c- mpc-ecdsa-getrandom-hack) #:use-module (crates-io))

(define-public crate-mpc-ecdsa-getrandom-hack-1.0.0 (c (n "mpc-ecdsa-getrandom-hack") (v "1.0.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)))) (h "0fwmzahnxs1mcfc4zx4i7fs7b5gbj9yx0s7fvghmrgnpcqsq5fdk")))

