(define-module (crates-io mp c- mpc-bench) #:use-module (crates-io))

(define-public crate-mpc-bench-0.0.1 (c (n "mpc-bench") (v "0.0.1") (h "1z2x3vrhaz7ak7lfnbkradligd4916kq365fqm743p6nkc2w7880")))

(define-public crate-mpc-bench-0.1.0 (c (n "mpc-bench") (v "0.1.0") (h "1vcxz9zvn0f681jpws65bmx8jx0jadwdplapxarff50kls0va3ky")))

(define-public crate-mpc-bench-0.2.0 (c (n "mpc-bench") (v "0.2.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)))) (h "0acgg7a4xs46212fj30zrgkv6sv5xfkm916skj17ckk2gaql3q0i")))

(define-public crate-mpc-bench-0.3.0 (c (n "mpc-bench") (v "0.3.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)))) (h "0q51k7wgrlsqdhygvgzigsqizi77j1ph8i22b0f595dh5sjilzwa")))

(define-public crate-mpc-bench-0.4.0 (c (n "mpc-bench") (v "0.4.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)))) (h "1mbjhv6bdsbvvn9qz16j9393kz2lyjl7crj71rzqnj5ikcmlfgr4")))

(define-public crate-mpc-bench-0.5.0 (c (n "mpc-bench") (v "0.5.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)))) (h "1bfqaahwi50sm7hw376shlrp4zc75sqsiixr7817iaszjsbra3pw") (y #t)))

(define-public crate-mpc-bench-0.5.1 (c (n "mpc-bench") (v "0.5.1") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)))) (h "0ddsfzd9qfpb4b1bzp5fa25b9413y1i3x1chcz43jizmr4iaavxs")))

(define-public crate-mpc-bench-0.6.0 (c (n "mpc-bench") (v "0.6.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "1pcp8gikhnnbsl85zjkam47prhy9bxnb41z5yqvcr38smp7q5ifc")))

(define-public crate-mpc-bench-0.6.1 (c (n "mpc-bench") (v "0.6.1") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "1wzpn6j70iwhni2b0w6l8c088dqgajalzrspx94s3b5sx64f17qd")))

(define-public crate-mpc-bench-0.7.0 (c (n "mpc-bench") (v "0.7.0") (d (list (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "0bdb6lx6n07qnclxbd8r4j3r2sg21xiynh5amd5rx2z13k2fnzxn")))

(define-public crate-mpc-bench-0.8.0 (c (n "mpc-bench") (v "0.8.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "0jc8sbkgxhpzxan57jfbmx00b2gj9pwj7gq63986l98c3q6l4bfn")))

(define-public crate-mpc-bench-0.8.1 (c (n "mpc-bench") (v "0.8.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "1h91vfwzzawr62dyjgwsdf3si5qmzzpj2287cyr6ia3dsmnsqs79")))

(define-public crate-mpc-bench-0.8.2 (c (n "mpc-bench") (v "0.8.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "04zpw5zppk2hrsgsmqd593wk3nm2wpxghcf1mrpjsn369xd7qdyl") (f (quote (("verbose"))))))

