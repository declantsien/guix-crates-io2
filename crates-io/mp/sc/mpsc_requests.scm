(define-module (crates-io mp sc mpsc_requests) #:use-module (crates-io))

(define-public crate-mpsc_requests-0.1.0 (c (n "mpsc_requests") (v "0.1.0") (h "1zdgym7dnmfqxir5ch9sl1wbq769s0gpw5wzl7ww7f3818r6v90g")))

(define-public crate-mpsc_requests-0.2.0 (c (n "mpsc_requests") (v "0.2.0") (h "0fyarz5p3sz5x9ark44wrrnk1a008y7nv1blcrm8yrba51j6dzk0")))

(define-public crate-mpsc_requests-0.3.1 (c (n "mpsc_requests") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "11fd12zxsca1p4h3s8zn6jy6xcrc07q25swfhx8qc9rhmq0yxpn9")))

(define-public crate-mpsc_requests-0.3.2 (c (n "mpsc_requests") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)))) (h "0kd8mx4n6c0ki077vr4vb9ki287kfhwaycvxsg5rvadh3xm4a42p")))

(define-public crate-mpsc_requests-0.3.3 (c (n "mpsc_requests") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)))) (h "0p78gy58w60lgi24zwf95nbg70xlf7jy9gcb7k9dhcbvif58nxsx")))

