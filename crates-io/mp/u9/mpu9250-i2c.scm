(define-module (crates-io mp u9 mpu9250-i2c) #:use-module (crates-io))

(define-public crate-mpu9250-i2c-0.5.0 (c (n "mpu9250-i2c") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1hcgk6pl6flziflyb0i5dshhjwzv0n3qhj8582rxv6zwbpx47a5q")))

(define-public crate-mpu9250-i2c-0.6.0 (c (n "mpu9250-i2c") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 0)))) (h "1aabp2dn673a2g9z9byjb28yzh4w6bbir5lhmywpjj3ff5awxs6a")))

(define-public crate-mpu9250-i2c-0.6.1 (c (n "mpu9250-i2c") (v "0.6.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 0)))) (h "12sfcc10mi848vhmaz4v42pc23cnwsin31lv4i6zv623whwz9vnc")))

(define-public crate-mpu9250-i2c-0.6.2 (c (n "mpu9250-i2c") (v "0.6.2") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 0)))) (h "08j9v53z78hadk54lzmf0i4rw354gy1q8r1i0g2x39fjifa6s5sj")))

(define-public crate-mpu9250-i2c-0.6.3 (c (n "mpu9250-i2c") (v "0.6.3") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 0)))) (h "12pg5k6v8xn12q06cn7czrysrdg172h4nyyvzfns2ahwv3rl9aj8")))

(define-public crate-mpu9250-i2c-0.6.4 (c (n "mpu9250-i2c") (v "0.6.4") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 0)))) (h "0sbzvbf20gab11qdmjjildhc80k7msbnjg0lxnm1mrj2a79izcdh")))

(define-public crate-mpu9250-i2c-0.6.5 (c (n "mpu9250-i2c") (v "0.6.5") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 2)))) (h "0n7fw26igqdxv5bwgljzxz3wmkwjkphaamb6ng9n1yfllhl2xnxb")))

(define-public crate-mpu9250-i2c-0.6.6 (c (n "mpu9250-i2c") (v "0.6.6") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 2)))) (h "19s36q56gsrbcahnsgzga64m7a1zs6lgpfqllkzb01225a5avc8j")))

(define-public crate-mpu9250-i2c-0.6.7 (c (n "mpu9250-i2c") (v "0.6.7") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 2)))) (h "0vlvccm0j69gs6vrc2fya2jwakgdmgh2bbizz3nfyr17y72mrq1f")))

(define-public crate-mpu9250-i2c-0.6.8 (c (n "mpu9250-i2c") (v "0.6.8") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "madgwick") (r "^0.1.1") (d #t) (k 2)))) (h "1iggcaxdnyvsb6mfqy19ynfbc4zifhfqdbrypcykcn6b6bk7xgs8")))

