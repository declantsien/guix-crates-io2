(define-module (crates-io mp u9 mpu9250-dmp) #:use-module (crates-io))

(define-public crate-mpu9250-dmp-1.0.0 (c (n "mpu9250-dmp") (v "1.0.0") (d (list (d (n "ak8963") (r "^1.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 2)))) (h "12ip70v5k55idz7lycc4m476vph8y4xddxc55822pppizn98k5ll")))

