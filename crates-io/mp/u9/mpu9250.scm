(define-module (crates-io mp u9 mpu9250) #:use-module (crates-io))

(define-public crate-mpu9250-0.2.2 (c (n "mpu9250") (v "0.2.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "1nfbw3jxl3di8vmak2mq6r7lkf74ywj0zpqg0d1vj355385c6aw6")))

(define-public crate-mpu9250-0.2.4 (c (n "mpu9250") (v "0.2.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "0dl275c0y7k6srny2z2jf03w7czsbrl54wvs14arj1jgzsngi1zi")))

(define-public crate-mpu9250-0.3.0 (c (n "mpu9250") (v "0.3.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "0q1iaxqyry9aixxznw02b3csgr2bcv9yfvxklgsy3ib2qh02mf2r")))

(define-public crate-mpu9250-0.4.0 (c (n "mpu9250") (v "0.4.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "1p4rv77x2hkl9v3n52yc1gpdr9ixp43gwl20l31aylbfj1d1yd9v")))

(define-public crate-mpu9250-0.4.1 (c (n "mpu9250") (v "0.4.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "1w6sjszn96mmhm2jim212k708fy0dv04jq6wz06kyqc80jbv86qk")))

(define-public crate-mpu9250-0.4.2 (c (n "mpu9250") (v "0.4.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "1v916iqr13hh36lyz1wrrsmb9x5s35wmw8a58n7ix4734mdw1nrw")))

(define-public crate-mpu9250-0.5.0 (c (n "mpu9250") (v "0.5.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "1hvhr8v4fa0z7a37q114nl4qa2kwcr0h66j59br2mqsq74r819sz")))

(define-public crate-mpu9250-0.6.0 (c (n "mpu9250") (v "0.6.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "05ma4cmz3qywr6vc1hf5a73pb0vn14mnhya94x2c7dj1h4gdbnph")))

(define-public crate-mpu9250-0.6.2 (c (n "mpu9250") (v "0.6.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "0rs5mp6vsk87xhf5mrzxm6br6q60s33fn3zjmk2q6cc52l42hm07")))

(define-public crate-mpu9250-0.6.3 (c (n "mpu9250") (v "0.6.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (k 0)))) (h "0wggb4myln2hg682xa1xn6sdmxph7953grq751a13nk2fi82r7nw")))

(define-public crate-mpu9250-0.6.4 (c (n "mpu9250") (v "0.6.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "000saizixdyv46dsc0z003mmkvlgh6plp69m47v30kc1hvmq7i79")))

(define-public crate-mpu9250-0.7.0 (c (n "mpu9250") (v "0.7.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0as8qhwiwci55nqh23bkhsymi36043bp4jmihjp2rzvdvi24gzs6")))

(define-public crate-mpu9250-0.7.1 (c (n "mpu9250") (v "0.7.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0wysbbhw7yrbj9vipr7zppdn5pa860ink4rfzfg2b4qr8mg1vi5z")))

(define-public crate-mpu9250-0.7.2 (c (n "mpu9250") (v "0.7.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1l2w2k5llpvrbm9brr1h4k0lqhqhgnk8f46nj706l7b5p743yx5d")))

(define-public crate-mpu9250-0.7.3 (c (n "mpu9250") (v "0.7.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "13r1w80dcr3pg3z8733y8whss66l6siqn1xpa5z5m4hkpwb41jgx")))

(define-public crate-mpu9250-0.7.4 (c (n "mpu9250") (v "0.7.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1mlr428g0pi7cf105z3qm8qvmbwyqs9bmxc70sj06hnz9v408pnn")))

(define-public crate-mpu9250-0.7.5 (c (n "mpu9250") (v "0.7.5") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0r289r71bisdalclfh59cissm0cbvm0l3z1zax46qijingrm3xb5")))

(define-public crate-mpu9250-0.8.0 (c (n "mpu9250") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0vqpfcvm8zxkxl5hvyinygn1brlb03m06wg8r3b8zc1kiidl5835")))

(define-public crate-mpu9250-0.9.0 (c (n "mpu9250") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1q87c9s9lcl43xw1zxc3am10r997vcali48l90194a6sfyxpf4a7") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.9.1 (c (n "mpu9250") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0qf4ir1vb870vy4y11ii5dcz9zvpd1243z757gsz7gvijvqxw19s") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.9.2 (c (n "mpu9250") (v "0.9.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0bvpa5hmvsanqxv983v171cz10fvarxhxvg6jyd5811pxfb8gc1w") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.9.3 (c (n "mpu9250") (v "0.9.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "1ladlgj04qx756sgvi0lrwfyv729klciahx2c48a3zzxnm3sqy8n") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.9.4 (c (n "mpu9250") (v "0.9.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0acri3xs88zcb3pzgp0h8vacc7khfydky90c1zm8cnb6c3jcinvq") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.9.5 (c (n "mpu9250") (v "0.9.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0kh22jh0jc4yiyws387jxn25kc9khpszxabp9nnlmcainbnrsdli") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.10.0 (c (n "mpu9250") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "0zn2ij3mavvmdkfnnqb1h6k2wkvpdyl4w4av8s02pvkpvfyvhr3z") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.10.1 (c (n "mpu9250") (v "0.10.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.4") (k 0)))) (h "00xdnhqpm0nydzpkx64xbvils1f3lqpfraaijvdkh4xsczs288wl") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.11.0 (c (n "mpu9250") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)))) (h "0jjl1n0ifa4pkr4yfgx325kwa22xxspw00rgzzwclj0iq6071m9b") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.12.0 (c (n "mpu9250") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)))) (h "1kaxb7fjbcr9z3vg90fmzj7vv8d0k8c224dvpz10szgq7b5r83mp") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.12.1 (c (n "mpu9250") (v "0.12.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)))) (h "0j2mbppzfd3varndmx0c82r3vw9hblayvllkk62fzli1jd9asf5a") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.12.2 (c (n "mpu9250") (v "0.12.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)))) (h "12002hxk7z367jaz6g3xncdbr81s2idkz19jslkgfdac0z9n20dy") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.20.0 (c (n "mpu9250") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "00kzsmwmmwdhcw4370jza9hqwy1xva8fpc164d7kz4sn800rhzsq") (f (quote (("i2c"))))))

(define-public crate-mpu9250-0.22.0 (c (n "mpu9250") (v "0.22.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0gd7m9n6c811v5lp8x2810v98jlnb84szygimly1pk3j09cxl9rm") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.22.1 (c (n "mpu9250") (v "0.22.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1sf20ibzmxijij45l602c5hkxp3b71i0kdy8yz32bals9sknkjq7") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.22.2 (c (n "mpu9250") (v "0.22.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0l621c5ibh5wacbk7cc9b7amkavma8xr3zmx9asnkn5yvhh7b4gq") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.24.0 (c (n "mpu9250") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0b829844j7pnca5amcraidghjkndz30pfb02y6gi1wd3qncqsqvm") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.24.1 (c (n "mpu9250") (v "0.24.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1n01kkcrnngk6av8fpwz5fz3ri6wzb9wazpg95j57by9bf6ya3wk") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.24.2 (c (n "mpu9250") (v "0.24.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1hdyc4vnnrj44qrzkckql8hskr2p0ds1ml9gvjcqmsgb4d17nvbm") (f (quote (("i2c") ("dmp" "libm"))))))

(define-public crate-mpu9250-0.25.0 (c (n "mpu9250") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0hvafq6cc915f4vyzhf5jdmjsfp169bmi2c5mx97kg99asi2h9q9") (f (quote (("i2c") ("dmp" "libm"))))))

