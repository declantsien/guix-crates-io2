(define-module (crates-io mp i- mpi-sys) #:use-module (crates-io))

(define-public crate-mpi-sys-0.1.0 (c (n "mpi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1kiahls1kd4ac46jzncijvapchc24zyjvdvmxw44kkm57bl9ingf")))

(define-public crate-mpi-sys-0.1.1 (c (n "mpi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0vvlfq0q30mswhmpbvq7bv8n9dy1z1zwm657x685y9npna0drx8z")))

(define-public crate-mpi-sys-0.1.2 (c (n "mpi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0ia7ikyf3rakafz78nkjf935yqx41z70fcggrl50nl73bx35jq2x")))

(define-public crate-mpi-sys-0.2.0 (c (n "mpi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0f373fh7wq54d32mgmk4dyw2nv97sh9pdffjsqv2n0xigj5aqqw9") (r "1.54")))

(define-public crate-mpi-sys-0.2.1 (c (n "mpi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1.3") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "05mr6hngygx4hawik074x1f1pywyhsbzip219xxiqb1zlqsxkd9c") (r "1.65")))

(define-public crate-mpi-sys-0.2.2 (c (n "mpi-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)))) (h "1phh1n79xwdf3k0l4vagcblisqv5h1ci5380bx8mk2dkppbzsd8g") (l "mpi") (r "1.65")))

