(define-module (crates-io mp i- mpi-derive) #:use-module (crates-io))

(define-public crate-mpi-derive-0.1.0 (c (n "mpi-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pb3xhzxmgzmm4p6n1z74sd5jwc9829164sj7s3ssm5xpxgxpkgg") (r "1.54")))

(define-public crate-mpi-derive-0.1.1 (c (n "mpi-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mz6apq8b0vw6p2wslq4l4hvmg1mp3y85305d12sdba9qwphbb5g") (r "1.65")))

(define-public crate-mpi-derive-0.1.2 (c (n "mpi-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1wqg32042sr4s99q38qxjwb38dk0ac3zk3nmrg9qsi0igicnfy9z") (r "1.65")))

