(define-module (crates-io mp cl mpcli) #:use-module (crates-io))

(define-public crate-mpcli-0.0.0 (c (n "mpcli") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "codec") (r "^1.1.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")))) (h "0qlx43a2hksg2mj6smzg14q1s03wlbb6mw512j9hpnshk929aw8f") (y #t)))

