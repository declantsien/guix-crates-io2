(define-module (crates-io mp #{3-}# mp3-folder-rename) #:use-module (crates-io))

(define-public crate-mp3-folder-rename-0.1.0 (c (n "mp3-folder-rename") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clogger") (r "^0.1.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)))) (h "11gwyn15y6dnchwki3ndbi7jacv9bqrjzibm9ww57izz7mxfxszp")))

