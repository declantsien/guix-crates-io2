(define-module (crates-io mp #{3-}# mp3-metadata) #:use-module (crates-io))

(define-public crate-mp3-metadata-0.1.0 (c (n "mp3-metadata") (v "0.1.0") (h "1nsca034bqy6wgdwhpir06zzrlzn7xivhpvbjyk1iakvaqxdmp3n")))

(define-public crate-mp3-metadata-0.1.1 (c (n "mp3-metadata") (v "0.1.1") (h "1xq84xalv0fhc5lqa3d3gzvwm1gckmv2aw9wsb8m660ykajqp729")))

(define-public crate-mp3-metadata-0.2.0 (c (n "mp3-metadata") (v "0.2.0") (h "0d2q1paxqzb9hzfa30wrnw9amnqix3hbjahppv4srbw4l363p90z")))

(define-public crate-mp3-metadata-0.2.1 (c (n "mp3-metadata") (v "0.2.1") (h "1ap7h76lxcyqnl25wmf7wipwzgczpp061rb0y1l7h6a3cl0hzswc")))

(define-public crate-mp3-metadata-0.2.2 (c (n "mp3-metadata") (v "0.2.2") (h "0frpaqiyb5ayb43x2kcjkw3g4vxwsr04q5slba8fbw73psagwwaf")))

(define-public crate-mp3-metadata-0.2.3 (c (n "mp3-metadata") (v "0.2.3") (h "1sqgrq7mr8jchb5r1qbwkb2wmdnf1g35l9asl61yqg7wywrcyq9g")))

(define-public crate-mp3-metadata-0.2.4 (c (n "mp3-metadata") (v "0.2.4") (h "066afa0gm66zwj69xkpbnrm93gbs848p95adjskrj0c7hja2k9nw")))

(define-public crate-mp3-metadata-0.2.5 (c (n "mp3-metadata") (v "0.2.5") (h "06a2acffv7xscsm9lr7g3067vmdwy8lm1h9q2w23wjxih2pk4iyx")))

(define-public crate-mp3-metadata-0.3.0 (c (n "mp3-metadata") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 2)) (d (n "simplemad") (r "^0.8") (d #t) (k 2)))) (h "1pn78bzpl4w8c4bkbdjmy0k4gm0wlpjhq86i101451imd79g3daa")))

(define-public crate-mp3-metadata-0.3.1 (c (n "mp3-metadata") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 2)) (d (n "simplemad") (r "^0.8") (d #t) (k 2)))) (h "1h3f5w2wr8rfdyal41wriycv48i7p5azxz33mnp1wbhiwckiyjnl")))

(define-public crate-mp3-metadata-0.3.2 (c (n "mp3-metadata") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 2)) (d (n "simplemad") (r "^0.8") (d #t) (k 2)))) (h "1spidy6qrkwr3ybfnv63vc0qhcc8nsbbbbl35w84q0f2rlrznyaw")))

(define-public crate-mp3-metadata-0.3.3 (c (n "mp3-metadata") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 2)) (d (n "simplemad") (r "^0.8") (d #t) (k 2)))) (h "16cn64br6c5q8zhb73bzx3ww0v5ay5q5v393c2zv09ipaz1rp5pb")))

(define-public crate-mp3-metadata-0.3.4 (c (n "mp3-metadata") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "simplemad") (r "^0.9") (d #t) (k 2)))) (h "1i4w9i3ljk79i5914a905walj3ril3iqrf39sw2q08fw52ia7sp4")))

