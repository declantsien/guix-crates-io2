(define-module (crates-io mp #{3-}# mp3-duration-sum) #:use-module (crates-io))

(define-public crate-mp3-duration-sum-0.1.0 (c (n "mp3-duration-sum") (v "0.1.0") (d (list (d (n "mp3-duration") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "09lr4p9y642l1zbkkr6dq7ygfpx21rdxq5yr73czyxqyldz3iq5k")))

(define-public crate-mp3-duration-sum-0.1.1 (c (n "mp3-duration-sum") (v "0.1.1") (d (list (d (n "mp3-duration") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "171ciyvgmn43rkhqfjg9iy4wvsbmy6fgqz8i88sqqw065cpjw7k0")))

