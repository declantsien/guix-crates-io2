(define-module (crates-io mp #{3-}# mp3-duration) #:use-module (crates-io))

(define-public crate-mp3-duration-0.1.0 (c (n "mp3-duration") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1j5sxpgz5jqj543izhg8whhndilsmg849nqz7f83646zywyrwk1s")))

(define-public crate-mp3-duration-0.1.1 (c (n "mp3-duration") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "13ia6svabf79yg5866gzvhdgc0pjwxd4h6is1fhga93sknndq3b3")))

(define-public crate-mp3-duration-0.1.2 (c (n "mp3-duration") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0pm7k2dwyld78ryr4j5bqnbw89l8v3ldhs8afgzvflph3a36mxp2")))

(define-public crate-mp3-duration-0.1.3 (c (n "mp3-duration") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "09nfdzlznpfpfzi8yjzy5hm8adsacqm0dgvfpw9k58j5a2knn093")))

(define-public crate-mp3-duration-0.1.4 (c (n "mp3-duration") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1kf5qfp7fwaafffakvkc1kn4gf1yqs00i1nzszhd2fdbjn96wryq")))

(define-public crate-mp3-duration-0.1.5 (c (n "mp3-duration") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1js3mv70bfik9pj0a6fs6sbzafqza8fz963z41wanv5ipijjzyfn")))

(define-public crate-mp3-duration-0.1.6 (c (n "mp3-duration") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1wq63vl6nmr90zgq0zjfa4vw1mn6pv1pcrng39gqrknln13sja90")))

(define-public crate-mp3-duration-0.1.7 (c (n "mp3-duration") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0kv0rm2k4kgqsspqcpdn9bzbwgymvz8m8ckk0mjshj8xpppc6z4v")))

(define-public crate-mp3-duration-0.1.8 (c (n "mp3-duration") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0sbp827ixlz26vn4wjm3cglzq3chw2jgkk8wfa0ac2l6zplvfz42")))

(define-public crate-mp3-duration-0.1.9 (c (n "mp3-duration") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0d9dgk8p620v7jlh3q94ks365h3xhccjg6pazixjrkb6qak1p9pd")))

(define-public crate-mp3-duration-0.1.10 (c (n "mp3-duration") (v "0.1.10") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03qaz7yrh9ia4pd9pvs4nx1xhg3i2n448z5mwl0hhbsh01rxr2rl")))

