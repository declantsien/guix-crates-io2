(define-module (crates-io mp w- mpw-rs) #:use-module (crates-io))

(define-public crate-mpw-rs-2.4.0 (c (n "mpw-rs") (v "2.4.0") (d (list (d (n "argon2rs") (r "^0.2") (d #t) (k 0)) (d (n "bcrypt") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "ring-pwhash") (r "^0.2") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 0)))) (h "1slzwc3p118c931g7kgcwaz9iiq22ssdkx8njxwd0wbcvmmrvidn")))

(define-public crate-mpw-rs-2.4.1 (c (n "mpw-rs") (v "2.4.1") (d (list (d (n "argon2rs") (r "^0.2") (d #t) (k 0)) (d (n "bcrypt") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "ring-pwhash") (r "^0.2") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 0)))) (h "1bbyxbvdjzp0fzhs2986a4vpwz57si89li5dygbr9m06vgvxqc1d")))

(define-public crate-mpw-rs-2.4.2 (c (n "mpw-rs") (v "2.4.2") (d (list (d (n "argon2rs") (r "^0.2") (d #t) (k 0)) (d (n "bcrypt") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "ring-pwhash") (r "^0.2") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 0)))) (h "0lwbyk11vc3065w224zq04dh89gcnmj2srmsbcnx1z4x7ih2qnjr")))

(define-public crate-mpw-rs-2.4.3 (c (n "mpw-rs") (v "2.4.3") (d (list (d (n "argon2rs") (r "^0.2") (d #t) (k 0)) (d (n "bcrypt") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "ring-pwhash") (r "^0.11") (d #t) (k 0)) (d (n "rpassword") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 0)))) (h "1c52s8ib812hfx4p72wl57ayr4gqfq4x60mvz5hv4jjxwvksz961")))

