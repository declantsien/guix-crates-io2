(define-module (crates-io mp -r mp-rs) #:use-module (crates-io))

(define-public crate-mp-rs-0.1.0 (c (n "mp-rs") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.10") (d #t) (k 0)))) (h "1azk0g6382zhfqdq7s8k11i7dagm1azgwwp94f72l806v2mwd08v") (y #t)))

(define-public crate-mp-rs-0.1.1 (c (n "mp-rs") (v "0.1.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.10") (d #t) (k 0)))) (h "14szzjijhdzm6zsignwng4zi5ssyi8wyvd6r2mv3fp4wvm8sw961") (y #t)))

(define-public crate-mp-rs-0.1.2 (c (n "mp-rs") (v "0.1.2") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.10") (d #t) (k 0)))) (h "15vwr0a1qhzvgpyiy4ha5p55ipcc53z1ryl0cgs6jm1hva7sdy0c") (y #t)))

