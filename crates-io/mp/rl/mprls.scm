(define-module (crates-io mp rl mprls) #:use-module (crates-io))

(define-public crate-mprls-0.1.0 (c (n "mprls") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 0)))) (h "1k3y0f9d0jx3d2g559zmch8jnwxqn7z736hn6aj5nycng0nj7w14")))

