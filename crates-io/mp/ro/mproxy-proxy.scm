(define-module (crates-io mp ro mproxy-proxy) #:use-module (crates-io))

(define-public crate-mproxy-proxy-0.1.0 (c (n "mproxy-proxy") (v "0.1.0") (d (list (d (n "mproxy-client") (r "^0.1.0") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.0") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0pgqiqclykqkpr0xjm85xx062ha27imn92i915smgkwdir368y4j") (y #t) (s 2) (e (quote (("tls" "dep:rustls" "dep:webpki-roots"))))))

