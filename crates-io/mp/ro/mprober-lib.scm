(define-module (crates-io mp ro mprober-lib) #:use-module (crates-io))

(define-public crate-mprober-lib-0.1.0 (c (n "mprober-lib") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "1ljrkma4j3si8ar98cx525pi7yy8mfsqwvpgy3zv6j4dgv40qxry") (y #t)))

(define-public crate-mprober-lib-0.1.1 (c (n "mprober-lib") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "1zs8q0cdjyb4p8isfq5aqz69v54paprigv8y5c6gycm45k2aj2ka")))

(define-public crate-mprober-lib-0.1.2 (c (n "mprober-lib") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0qp8mp3b1zkmzh4y2j5i17nbd0wi1vhx05k5qscn0yq53wz42nmz")))

(define-public crate-mprober-lib-0.1.3 (c (n "mprober-lib") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "052v0l0ky1cmd98g3z0v88z2wqsh1w7hci0c4a6lmcsns02a6fjv")))

(define-public crate-mprober-lib-0.1.4 (c (n "mprober-lib") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0k46pxzyag0yswf6jpz5ywv2yp9911ciq41s041lp2wcmqbc4krr")))

(define-public crate-mprober-lib-0.1.5 (c (n "mprober-lib") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "14gc9gn9cga29rdlzl7laa4lhiqh9dk6yxvna2r1mwvx2bfmd39q")))

(define-public crate-mprober-lib-0.1.6 (c (n "mprober-lib") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0x1bzsig4j5wmfhsffzkay50g32ymyf3v2s2d43lisnib1msn0m2")))

(define-public crate-mprober-lib-0.1.7 (c (n "mprober-lib") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "15m052lrh0f02bygaq1bfal9l2vys6arfbd8idp2s123ckdn9zly")))

(define-public crate-mprober-lib-0.1.8 (c (n "mprober-lib") (v "0.1.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "1szqiymd0qmrns8bm5f4f5gzwzj57fihrbgk4wgcqwcgirysnka7")))

(define-public crate-mprober-lib-0.1.9 (c (n "mprober-lib") (v "0.1.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0rk9amr7dd9lvdk3wj3ivsggk3m5jb23hz2v5wh5a3qz6w6mng8w") (y #t)))

(define-public crate-mprober-lib-0.1.10 (c (n "mprober-lib") (v "0.1.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "11y7ciz0fl4zpmmn6pf2bn0n5yk7qvsq59abjj6cgddf2sn4nvbi")))

(define-public crate-mprober-lib-0.1.11 (c (n "mprober-lib") (v "0.1.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0s19pkxxzlgrh8n6nrj963bl3sj3i1rmxk0zy8gynyi0qlgazyzq") (r "1.56")))

(define-public crate-mprober-lib-0.1.12 (c (n "mprober-lib") (v "0.1.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "1rwqmxbwzlmr8i6h7zs6wyddjq09pcg4c88avczjpnbn4zfisyd7") (r "1.61")))

(define-public crate-mprober-lib-0.1.13 (c (n "mprober-lib") (v "0.1.13") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "1pj4a0j0bh6sgglymyjxghhp0b6mrglvqaqfza453g77d5mbb2d3") (r "1.61")))

(define-public crate-mprober-lib-0.1.14 (c (n "mprober-lib") (v "0.1.14") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "118qxyf5wb9qhqpi0z4c7ripkj2qb9qs1558ybk1mwmq6bsmn941") (r "1.65")))

(define-public crate-mprober-lib-0.1.15 (c (n "mprober-lib") (v "0.1.15") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "page_size") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.8") (d #t) (k 0)))) (h "0sblrya2ik67231yhz4kcb58n7id5w8cw7bz4n0zflvgn0k2dsli") (r "1.65")))

