(define-module (crates-io mp ro mproxy-reverse) #:use-module (crates-io))

(define-public crate-mproxy-reverse-0.1.1 (c (n "mproxy-reverse") (v "0.1.1") (d (list (d (n "mproxy-client") (r "^0.1.1") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.1") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.1") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "07wss73ph0w1axjnwcs84408w5y3a4vvfz3sa7ghlqlr8h5rs53m")))

(define-public crate-mproxy-reverse-0.1.2 (c (n "mproxy-reverse") (v "0.1.2") (d (list (d (n "mproxy-client") (r "^0.1.2") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.2") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.2") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0c9x0hi6wps59isbwarvnm254zra2sngi7f5v5988k2r18bmjy1y")))

(define-public crate-mproxy-reverse-0.1.3 (c (n "mproxy-reverse") (v "0.1.3") (d (list (d (n "mproxy-client") (r "^0.1.3") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.3") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.3") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0zg3vymilh7vvvmllf9cr23gf6ijcappijiz0nj4kvqal8bqf5gg")))

(define-public crate-mproxy-reverse-0.1.4 (c (n "mproxy-reverse") (v "0.1.4") (d (list (d (n "mproxy-client") (r "^0.1.4") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.4") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.4") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0zr63g5nzn2pqz46b6zkz02dhgna9lykrr7j1pahl4dfgyqywvjx")))

(define-public crate-mproxy-reverse-0.1.5 (c (n "mproxy-reverse") (v "0.1.5") (d (list (d (n "mproxy-client") (r "^0.1.5") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.5") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.5") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1ki49j9phgvcjl5qjck2800i6lba7j4cnjl747zrhz01x6lha79x")))

(define-public crate-mproxy-reverse-0.1.6 (c (n "mproxy-reverse") (v "0.1.6") (d (list (d (n "mproxy-client") (r "^0.1.6") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.6") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.6") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.6") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "119s1dgq142rf8wyr03i2anlqqnvl9rf4cfsic6wrjx1lm3i3awv")))

(define-public crate-mproxy-reverse-0.1.7 (c (n "mproxy-reverse") (v "0.1.7") (d (list (d (n "mproxy-client") (r "^0.1.7") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.7") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.7") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "17pd3z5z2kvbfipfl7qab9rc7c3cdgqmwsizcbn43635gn0cqkig")))

(define-public crate-mproxy-reverse-0.1.8 (c (n "mproxy-reverse") (v "0.1.8") (d (list (d (n "mproxy-client") (r "^0.1.8") (d #t) (k 0)) (d (n "mproxy-forward") (r "^0.1.8") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.8") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "04kqi7mznqprygs7a7i923ljlb62mxvw1kqm4sw9x2p366wmrjn0")))

