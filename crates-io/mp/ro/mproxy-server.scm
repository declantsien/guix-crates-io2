(define-module (crates-io mp ro mproxy-server) #:use-module (crates-io))

(define-public crate-mproxy-server-0.1.0 (c (n "mproxy-server") (v "0.1.0") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0gss2f5zrg787pzqgy1kzgiz7gmwgdxya90gz066nzricbswpxln")))

(define-public crate-mproxy-server-0.1.1 (c (n "mproxy-server") (v "0.1.1") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0118zh8hpz8d56g20avcmgi3lnd12l1r17d2z7yya8x8kn656c1q")))

(define-public crate-mproxy-server-0.1.2 (c (n "mproxy-server") (v "0.1.2") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1nb03406b4km9wyfqxdnzbgyj3i3393ixshq055qrgsim45r3qxw")))

(define-public crate-mproxy-server-0.1.3 (c (n "mproxy-server") (v "0.1.3") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0sw6m5f96ip90hv4ij7m9qbqm32bk56f8g8m854a01vxgcfklk1l")))

(define-public crate-mproxy-server-0.1.4 (c (n "mproxy-server") (v "0.1.4") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1vxcn3nnyrrx83ayn4rn8kypz6v0ljari6ic6bv64dpfpzi18abg")))

(define-public crate-mproxy-server-0.1.5 (c (n "mproxy-server") (v "0.1.5") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0ynf46hiddl720j4n4qh6gc44ix71gi49a6n4id70lx40s9cg9ls")))

(define-public crate-mproxy-server-0.1.6 (c (n "mproxy-server") (v "0.1.6") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.6") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1daqwwlw8awx88dgwnnpgaiz2cylxg3jz808gyvf9f6qk60g5d3z")))

(define-public crate-mproxy-server-0.1.7 (c (n "mproxy-server") (v "0.1.7") (d (list (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0mq3mcn261dqk72j8bd88w7ymjkwkrrl52zcji4x8wdzsf1wndxp")))

(define-public crate-mproxy-server-0.1.8 (c (n "mproxy-server") (v "0.1.8") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1xh2x8lgj9y5qllqfwzhzpz4ag53h3fhh93ykkp9vpxj95g1m8qk")))

