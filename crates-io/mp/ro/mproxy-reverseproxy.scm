(define-module (crates-io mp ro mproxy-reverseproxy) #:use-module (crates-io))

(define-public crate-mproxy-reverseproxy-0.1.0 (c (n "mproxy-reverseproxy") (v "0.1.0") (d (list (d (n "mproxy-client") (r "^0.1.0") (d #t) (k 0)) (d (n "mproxy-proxy") (r "^0.1.0") (d #t) (k 0)) (d (n "mproxy-server") (r "^0.1.0") (d #t) (k 0)) (d (n "mproxy-socket_dispatch") (r "^0.1.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "021y89d3pbii74cl4fr18fssx6a2bpzzq0b0a02dfcplviv533bb") (y #t)))

