(define-module (crates-io mp ro mproxy-client) #:use-module (crates-io))

(define-public crate-mproxy-client-0.1.0 (c (n "mproxy-client") (v "0.1.0") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1xrs5ihr4qfbnzvhdfc8z0rc8ipqvvy3rxk8zyz5ijq78zhrpry5")))

(define-public crate-mproxy-client-0.1.1 (c (n "mproxy-client") (v "0.1.1") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "15wmpaz72lb1ifmz74wsy287bydl97mbjxrqfc6hyvsnmmidlp40")))

(define-public crate-mproxy-client-0.1.2 (c (n "mproxy-client") (v "0.1.2") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1f49ilnfwxkf5v4gmx40vn9ziwbs5sr69hndr02436bh35rf4ssx")))

(define-public crate-mproxy-client-0.1.3 (c (n "mproxy-client") (v "0.1.3") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "06cb4qh93qhr6qa8ci23rdzjd2zbk7m6vpb9v8pnn5fj5ja8sh7l")))

(define-public crate-mproxy-client-0.1.4 (c (n "mproxy-client") (v "0.1.4") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1chx11rbk9i6xrapmx96jrbqaah90xw1rycy04yhxq05daa77r18")))

(define-public crate-mproxy-client-0.1.5 (c (n "mproxy-client") (v "0.1.5") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "1xw51pnfr2b9ggx6lp89rmvn91mlwkkgkbcsx19s98gg8svvq8hy")))

(define-public crate-mproxy-client-0.1.6 (c (n "mproxy-client") (v "0.1.6") (d (list (d (n "mproxy-socket_dispatch") (r "^0.1.6") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "18dkzkm3s4acf0b7nmc30miixlrp5ifmnw3a9rfpykhdl7ip809p")))

(define-public crate-mproxy-client-0.1.7 (c (n "mproxy-client") (v "0.1.7") (d (list (d (n "default-net") (r "^0.14") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "05bpklfp0d791p1sccgzix2c4l46nscbp4bgvwanfs9pmv225fvs")))

(define-public crate-mproxy-client-0.1.8 (c (n "mproxy-client") (v "0.1.8") (d (list (d (n "default-net") (r "^0.14") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)))) (h "0rm99w008gzbqqaids1rhdcyb8vd087m8vi9dy3y4k9fq5qdcss8")))

