(define-module (crates-io mp ro mprovision) #:use-module (crates-io))

(define-public crate-mprovision-0.1.0 (c (n "mprovision") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "expectest") (r "^0.12") (d #t) (k 2)) (d (n "memmem") (r "^0.1") (d #t) (k 0)) (d (n "plist") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1b1s3wi5g0z858n6sl3il3vcd1j9xmsxzcawvy9mbr77qzjibbww")))

(define-public crate-mprovision-0.1.1 (c (n "mprovision") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "expectest") (r "^0.12") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "plist") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "07cfbm6y8lmdnb5lqvaxaldfcbz3w9jkgrhmrknxgwhv92amqpxg")))

