(define-module (crates-io mp dr mpdrs) #:use-module (crates-io))

(define-public crate-mpdrs-0.1.0 (c (n "mpdrs") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 2)))) (h "11i60g95n1w37l7hj8j39a986gm3fppbqb65fyph52wnx1z1pj6y")))

