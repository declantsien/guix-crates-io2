(define-module (crates-io mp ig mpigdb) #:use-module (crates-io))

(define-public crate-mpigdb-0.1.0 (c (n "mpigdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "00n2mdr27wnzsrn48p9z0k6zv1bk8irq85vzhfji2xspizrc25jg")))

(define-public crate-mpigdb-0.2.0 (c (n "mpigdb") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "14ppk10a0gj7ri4r7cm56g9hgqwxacd0756kl9qvws80mbkcvn78")))

(define-public crate-mpigdb-0.4.0 (c (n "mpigdb") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "1zgj34x3qbsr5d7gzd0r3wvgracr67fdpdjqv1d12nmi4hdxpqk2")))

(define-public crate-mpigdb-0.4.1 (c (n "mpigdb") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "1hg3kavngkknb3k7g43m7glgjydz7z2blqkad35lxs7i30jchy9d")))

(define-public crate-mpigdb-0.5.0 (c (n "mpigdb") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "00kwa840q1g7f836bycv0fqdvczw8g2zxrxmbcfmhdg2fcqf5v70")))

