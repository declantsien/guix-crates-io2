(define-module (crates-io mp id mpid_messaging) #:use-module (crates-io))

(define-public crate-mpid_messaging-0.0.1 (c (n "mpid_messaging") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "xor_name") (r "~0.0.1") (d #t) (k 0)))) (h "15gklw61rxhflzwil06bavkwp85ykdi16p15i5chrrjh9hfq81nc")))

(define-public crate-mpid_messaging-0.0.2 (c (n "mpid_messaging") (v "0.0.2") (d (list (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)) (d (n "xor_name") (r "~0.0.1") (d #t) (k 0)))) (h "0d0avdfjcysl4zca3ipp0xrmyjni55a1ib6f492633rifizlblaj")))

(define-public crate-mpid_messaging-0.0.3 (c (n "mpid_messaging") (v "0.0.3") (d (list (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)) (d (n "xor_name") (r "~0.0.1") (d #t) (k 0)))) (h "08gcixfg1k824207aa5c7r9hp497jpd93yxmjdjswjrl2b1529hw")))

(define-public crate-mpid_messaging-0.0.4 (c (n "mpid_messaging") (v "0.0.4") (d (list (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)) (d (n "xor_name") (r "~0.0.1") (d #t) (k 0)))) (h "002l60pjdr2p8mnyhy8lm1z1fhmpk38c7d305z2ygl3k7w0ch04c")))

(define-public crate-mpid_messaging-0.1.0 (c (n "mpid_messaging") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.37") (o #t) (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)) (d (n "xor_name") (r "~0.0.3") (d #t) (k 0)))) (h "0anarpn570pfcdppd0x1fw9znjb269vqdqpxg25xvzrfqk99lnw5") (f (quote (("default"))))))

(define-public crate-mpid_messaging-0.2.0 (c (n "mpid_messaging") (v "0.2.0") (d (list (d (n "clippy") (r "~0.0.46") (o #t) (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)) (d (n "xor_name") (r "~0.1.0") (d #t) (k 0)))) (h "0sljn7xwfr2wqs7fhvfqqf4434cbd1sx4572l2r9gpl037r37zx4") (y #t)))

