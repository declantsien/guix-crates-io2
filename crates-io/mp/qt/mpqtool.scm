(define-module (crates-io mp qt mpqtool) #:use-module (crates-io))

(define-public crate-mpqtool-0.1.0 (c (n "mpqtool") (v "0.1.0") (d (list (d (n "ceres-mpq") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0xf4n4wcdj1p3as8h9qnxb78cnib35hv79ym6wxzwk8vfxzryr9m")))

(define-public crate-mpqtool-0.1.1 (c (n "mpqtool") (v "0.1.1") (d (list (d (n "ceres-mpq") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0ga0ym43n1mzs6w9b0pbqrzg3c7lx5ws0s3isb09ycnzid62g6cc")))

(define-public crate-mpqtool-0.1.2 (c (n "mpqtool") (v "0.1.2") (d (list (d (n "ceres-mpq") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0xa2gnq0193n2vy2f02qh1msw5zszpzf276zm564b26pfvqqq2zd")))

(define-public crate-mpqtool-0.1.3 (c (n "mpqtool") (v "0.1.3") (d (list (d (n "ceres-mpq") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "027nkv1zx7b759hsk0q8s82xn00bd2wh0vh67qd5y0vx23i7f9r8")))

(define-public crate-mpqtool-0.1.4 (c (n "mpqtool") (v "0.1.4") (d (list (d (n "ceres-mpq") (r "^0.1.8") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1dir8kh8iyf2sjipkd69d182g65kx3gmdjbsy3x2vg54wiji343d")))

