(define-module (crates-io mp r- mpr-sys) #:use-module (crates-io))

(define-public crate-mpr-sys-0.0.1 (c (n "mpr-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0f349c4pvwlxc7lwml5cqlq8z211khx9vmvkwj5x41rwdwajzz06")))

(define-public crate-mpr-sys-0.1.0 (c (n "mpr-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "19a5whylkf3ddnnfblwsklignczv684rg8bmdcvwrywzy33shqaq")))

(define-public crate-mpr-sys-0.1.1 (c (n "mpr-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1sd5v6jx0fg50y008pb87ddv5b7acjczij0njlpvlmw57n3cn2s7")))

