(define-module (crates-io mp ne mpnet-rs) #:use-module (crates-io))

(define-public crate-mpnet-rs-0.1.0 (c (n "mpnet-rs") (v "0.1.0") (d (list (d (n "candle-core") (r "^0.3.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.1") (d #t) (k 0)))) (h "1hw1bsja3sahz3raxz65bsjfp84bb5zkckazn6glwl0q2dnddjhk") (f (quote (("exclude_from_ci"))))))

(define-public crate-mpnet-rs-0.1.1 (c (n "mpnet-rs") (v "0.1.1") (d (list (d (n "candle-core") (r "^0.3.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.1") (d #t) (k 0)))) (h "0m91r26szalhm8glkqy68b5miiyqyc95h30g41s9kqvq30pxh83m") (f (quote (("exclude_from_ci"))))))

(define-public crate-mpnet-rs-0.1.2 (c (n "mpnet-rs") (v "0.1.2") (d (list (d (n "candle-core") (r "^0.4.1") (d #t) (k 0)) (d (n "candle-nn") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.1") (d #t) (k 0)))) (h "0zh8lq4i8z6nd246hrgnzj51pzlqdjyc44cm836vs09xx0sgmd20") (f (quote (("exclude_from_ci"))))))

(define-public crate-mpnet-rs-0.1.3 (c (n "mpnet-rs") (v "0.1.3") (d (list (d (n "candle-core") (r "^0.4.1") (d #t) (k 0)) (d (n "candle-nn") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.1") (d #t) (k 0)))) (h "0d1sy4hphvxg842nal2r1bmb2hsn8rf9snyqfbvwg1q4sgy47vbd") (f (quote (("exclude_from_ci"))))))

