(define-module (crates-io mp vi mpvi) #:use-module (crates-io))

(define-public crate-mpvi-0.1.0 (c (n "mpvi") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serial_test") (r "^0") (d #t) (k 0)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "0zsp4256f663w4fshzxqyygqdcrmibq6732d9mr2s4wkw0xmh0l4")))

