(define-module (crates-io mp vi mpvipc) #:use-module (crates-io))

(define-public crate-mpvipc-1.0.0 (c (n "mpvipc") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0qj92jbxj32dwr6k7zxvc3hnjc448w3802yj84lh4idsyk21hr8m")))

(define-public crate-mpvipc-1.1.0 (c (n "mpvipc") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1d3axafp92mdkb2wv2klyrly317ca7nzcc4paijp03rpsk3hiq4k")))

(define-public crate-mpvipc-1.1.1 (c (n "mpvipc") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0j5ry6sdvap3jys1vdsbmiyqq835c0q26hx9mcpgzbln65rbih55")))

(define-public crate-mpvipc-1.1.2 (c (n "mpvipc") (v "1.1.2") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0p57xq46yppkx48q984ydq74zcvwrmmclvr4hccqgxny29z3kk62")))

(define-public crate-mpvipc-1.1.3 (c (n "mpvipc") (v "1.1.3") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1ilz5l2j3j1q3kjk5z0xj4ygcz9f8lim0bq74c479dflim33lpkb")))

(define-public crate-mpvipc-1.1.4 (c (n "mpvipc") (v "1.1.4") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1575jbicagca5akzqri8dxa51k59m7k9zrcbl9ffmsyfdp8f44vb")))

(define-public crate-mpvipc-1.1.5 (c (n "mpvipc") (v "1.1.5") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0wi2zyal0g19fj5s7h6769h18spsjin1fzl78fsfz7p1zvmhl912")))

(define-public crate-mpvipc-1.1.6 (c (n "mpvipc") (v "1.1.6") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0amr13grvcyf1is5kl02yghrlqffmjvzwcf6wy91lmcnj6yinsa8")))

(define-public crate-mpvipc-1.1.7 (c (n "mpvipc") (v "1.1.7") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "11qmnlvgai62hykj00qvyw5d13q12lnxda6kqp73r7r7vi9778c1")))

(define-public crate-mpvipc-1.1.8 (c (n "mpvipc") (v "1.1.8") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1d9akbqkph0yimlygmw6mwcjy2aafhiwwdwybyyj58hd756wwc6p")))

(define-public crate-mpvipc-1.1.9 (c (n "mpvipc") (v "1.1.9") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1famqjzgsb5kh41zk1p6wl8c7hp4y5gcx2zxwds5941i8yabvnn4")))

(define-public crate-mpvipc-1.2.0 (c (n "mpvipc") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "134r3fzi1dwd2c1iw43nysk3z2wjipahl6jm2g2c83gwzhghgpv4")))

(define-public crate-mpvipc-1.2.1 (c (n "mpvipc") (v "1.2.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0i4qabwznxhqsyhzkb99mz8rjximmbqfl9p5yqr0bxviv40i7zcb")))

(define-public crate-mpvipc-1.2.2 (c (n "mpvipc") (v "1.2.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "18v1a0zc4xa917mvj9x6vffb0jv81icpv9j4k8a8n7g8z7yba368")))

(define-public crate-mpvipc-1.3.0 (c (n "mpvipc") (v "1.3.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1gg2s7nk34pr3rc9s0ppkx4j8ldfmf713fd34gqm9fz571li64if")))

