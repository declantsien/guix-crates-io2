(define-module (crates-io mp ch mpchash) #:use-module (crates-io))

(define-public crate-mpchash-0.1.0 (c (n "mpchash") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "10r1icj5wgc9hjm8s6f1bzpqflqhv74ijpz396wi2gaapyyvcmf9")))

(define-public crate-mpchash-1.1.0 (c (n "mpchash") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1ps8534ryzw5kgpqn76alzjnz18sifahcibl0jrfbsbdscgb55cp")))

(define-public crate-mpchash-1.2.0 (c (n "mpchash") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1axx08k7hly431hkz28m5rbj7nb9mcg1qxhzld1bv4qz73c3anmr")))

(define-public crate-mpchash-1.2.1 (c (n "mpchash") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "06dcsc6b68nmll8k50a06idmmq9my8f10qwgaz9q59i2b5lfxi6f")))

(define-public crate-mpchash-1.2.2 (c (n "mpchash") (v "1.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "0vx926wyrsp1m495261bxpz1dgq4hcib8grq11vrh4w8q58x28qf")))

(define-public crate-mpchash-1.2.3 (c (n "mpchash") (v "1.2.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1abiv0shhk6798k1wkqlbgvs65j2r8zkiswa5wi1hlv4magikn5x")))

