(define-module (crates-io mp v- mpv-client-dyn) #:use-module (crates-io))

(define-public crate-mpv-client-dyn-0.5.0 (c (n "mpv-client-dyn") (v "0.5.0") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "068kp4xg8vyh9k8yvcmxb805qd0v89dgr9zs5xzdrbck8m2y03af")))

