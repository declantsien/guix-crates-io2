(define-module (crates-io mp v- mpv-audio) #:use-module (crates-io))

(define-public crate-mpv-audio-0.1.0 (c (n "mpv-audio") (v "0.1.0") (h "1ip60nj2vfglmgbsxkdkk9ac4m0558il5z901xsx3yssr93048ac")))

(define-public crate-mpv-audio-0.1.1 (c (n "mpv-audio") (v "0.1.1") (h "1b12yini9ziq5xmh2561wbf64vzrbiws64s0xqgx3ra86wfvqp36")))

