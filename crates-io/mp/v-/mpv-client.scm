(define-module (crates-io mp v- mpv-client) #:use-module (crates-io))

(define-public crate-mpv-client-0.1.0 (c (n "mpv-client") (v "0.1.0") (h "1k8dsf1pkkgdz94lz6k37dk1fb83cf8g9g0zhdq0w4ic0dhky16d") (y #t)))

(define-public crate-mpv-client-0.1.1 (c (n "mpv-client") (v "0.1.1") (h "0l7n43ff7ci6mjpihhm7lr5k80k81fj861w7rc5l7dpv38id2c59") (y #t)))

(define-public crate-mpv-client-0.1.2 (c (n "mpv-client") (v "0.1.2") (h "0aaxg4q8bfha5x8anc4rx70fjfzk129z4yps33pbbl9h87mpd0li") (y #t)))

(define-public crate-mpv-client-0.1.3 (c (n "mpv-client") (v "0.1.3") (h "160h6mm0gd87hmbvdsz05xdw5gm44cwja9fky49y48gq457dk7nm") (y #t)))

(define-public crate-mpv-client-0.1.4 (c (n "mpv-client") (v "0.1.4") (h "0h9ka4azpq5i1b4xbhppyr0783wzaxc277ia7d9khmp8pja3c5dk") (y #t)))

(define-public crate-mpv-client-0.2.0 (c (n "mpv-client") (v "0.2.0") (h "1jwkbd8y4q0rm7jcgh1qm3w4i15g6ldxdl7b9g098gkys70vgvgl") (y #t)))

(define-public crate-mpv-client-0.3.0 (c (n "mpv-client") (v "0.3.0") (h "1vkdv2cr5z95jxfnp6sjcwzmd30l8wbjq31lgwc5ppwzgw797l1r") (y #t)))

(define-public crate-mpv-client-0.3.1 (c (n "mpv-client") (v "0.3.1") (h "1da8nr40vafj27b0xa750ida2fh3cr6d8m5qd1jihrg5ib2zd8k6") (y #t)))

(define-public crate-mpv-client-0.3.2 (c (n "mpv-client") (v "0.3.2") (h "1c1zf2r9a52s0j2y1lm5gi3nll0b6qprgbj1zqvm4as9mzlnchm7")))

(define-public crate-mpv-client-0.3.3 (c (n "mpv-client") (v "0.3.3") (h "1yx1s96jb4klrxg0i9qylkjybj27qxf5jlbjvry3bv1y7lcdx6mp")))

(define-public crate-mpv-client-0.4.0 (c (n "mpv-client") (v "0.4.0") (h "1gj5jpp62xhlhl7vbljpi81zg76vw612y1q9gz6mgasnbk61ryr0")))

(define-public crate-mpv-client-0.4.1 (c (n "mpv-client") (v "0.4.1") (h "04arm8inmhqxva95adf0wnxm875iw9361axzg0jlbi3bhw79b4a2")))

(define-public crate-mpv-client-0.5.0 (c (n "mpv-client") (v "0.5.0") (h "113yy6942w9kb441zmqwm2hmdvxaa70f01aiz25mmv0jlm759pmp")))

(define-public crate-mpv-client-0.6.0 (c (n "mpv-client") (v "0.6.0") (h "12mvwsarldiw5yy7ma7w4x9k2q76rv13qg4va7kac1kp4w8hdxc9")))

(define-public crate-mpv-client-0.6.1 (c (n "mpv-client") (v "0.6.1") (h "16dc4w4zbh9jwr9p08ij6ni94i74k4ilsrg0807dn22j2qb8xq29")))

(define-public crate-mpv-client-0.6.2 (c (n "mpv-client") (v "0.6.2") (h "1vvz3z7vzkvdnbw7h1cnk9psmkaycq9ladlykp2vpindk0ih086c")))

