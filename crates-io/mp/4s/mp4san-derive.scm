(define-module (crates-io mp #{4s}# mp4san-derive) #:use-module (crates-io))

(define-public crate-mp4san-derive-0.4.0 (c (n "mp4san-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (d #t) (k 0)))) (h "13jmi0vwgz990i7hn3qx4nma5i2315aj3mny1khbzpchyr7r27xi") (r "1.61.0")))

(define-public crate-mp4san-derive-0.5.0 (c (n "mp4san-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (d #t) (k 0)))) (h "0zfj8aymyv3clhsiwi4ijlllccj75378mgzwvpxb9qs18l1xrd12") (r "1.61.0")))

(define-public crate-mp4san-derive-0.5.1 (c (n "mp4san-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (d #t) (k 0)))) (h "0ypgr9xjig3i9n83m931fn4ibdpsbzl74qfm5y1wzwg3i1p45ixq") (r "1.61.0")))

