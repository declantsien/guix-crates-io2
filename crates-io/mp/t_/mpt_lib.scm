(define-module (crates-io mp t_ mpt_lib) #:use-module (crates-io))

(define-public crate-mpt_lib-0.0.1 (c (n "mpt_lib") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1iz7m0ldmwsvldhzdda731kligr7fl1vjl2bq38dgyh94fcag323")))

(define-public crate-mpt_lib-0.0.2 (c (n "mpt_lib") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "1g36iv5jh3gqpqk8rwxk4llhs2y3rnfnwjjjjj990qj3hrs51kri")))

