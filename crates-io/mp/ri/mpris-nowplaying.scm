(define-module (crates-io mp ri mpris-nowplaying) #:use-module (crates-io))

(define-public crate-mpris-nowplaying-0.1.0 (c (n "mpris-nowplaying") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mpris") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.0") (d #t) (k 0)))) (h "1jnngpv2z42l5bm0y95igww4bv20z4hnpy15b6ij4yfnf64x7n8n")))

(define-public crate-mpris-nowplaying-0.1.1 (c (n "mpris-nowplaying") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mpris") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.0") (d #t) (k 0)))) (h "06zm7n1rh6n3fn89f7s6597n4yys8bwns6xxz8zajm0ncky5xa8y")))

