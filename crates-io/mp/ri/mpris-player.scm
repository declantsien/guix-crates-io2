(define-module (crates-io mp ri mpris-player) #:use-module (crates-io))

(define-public crate-mpris-player-0.1.0 (c (n "mpris-player") (v "0.1.0") (d (list (d (n "dbus") (r "^0.6.2") (d #t) (k 0)) (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)))) (h "0hszxfr26h84jy502la5l4j4wc47hrp9rdpl2v76qaclpx8qdg7g")))

(define-public crate-mpris-player-0.3.0 (c (n "mpris-player") (v "0.3.0") (d (list (d (n "dbus") (r "^0.6.4") (d #t) (k 0)) (d (n "glib") (r "^0.7.0") (d #t) (k 0)))) (h "1fwi6755k4q4vdfa66n6cc5fhd5p4qic7hvqhpq9jrxmnjq0mda1")))

(define-public crate-mpris-player-0.4.0 (c (n "mpris-player") (v "0.4.0") (d (list (d (n "dbus") (r "^0.6.4") (d #t) (k 0)) (d (n "glib") (r "^0.8.0") (d #t) (k 0)))) (h "0k8mshskya3az1mn226rrwnyjijk0660b91d428387gjg4857sza")))

(define-public crate-mpris-player-0.5.0 (c (n "mpris-player") (v "0.5.0") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.9.0") (d #t) (k 0)))) (h "00mzlnyd9q1rh12zqab2r3v91jpynxnls57v7ypdpxlcx8wyw3yr")))

(define-public crate-mpris-player-0.6.0 (c (n "mpris-player") (v "0.6.0") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.10.1") (d #t) (k 0)))) (h "0s7nnir1g4fas7navsbiz78f8z4xg886zzwn9xva7pw0vkcksz0g")))

(define-public crate-mpris-player-0.6.1 (c (n "mpris-player") (v "0.6.1") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.10.3") (d #t) (k 0)))) (h "01xvdzac9vwzc0fxpa7qwnn3n62bngrmr5z2n9pf86z3xgbasssg")))

(define-public crate-mpris-player-0.6.2 (c (n "mpris-player") (v "0.6.2") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.15.10") (d #t) (k 0)))) (h "167h7jynv472hr8v4jqqrpmad6qjyi9baayh14vg9nhz2z4jx0xy")))

(define-public crate-mpris-player-0.6.3 (c (n "mpris-player") (v "0.6.3") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.15.10") (d #t) (k 0)))) (h "1lci0cyql47blbw9k7k1ahdd386p0df5l0x7gknasmip4rmxjqfn")))

