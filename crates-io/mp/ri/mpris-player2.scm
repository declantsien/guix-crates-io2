(define-module (crates-io mp ri mpris-player2) #:use-module (crates-io))

(define-public crate-mpris-player2-0.6.1 (c (n "mpris-player2") (v "0.6.1") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "glib") (r "^0.18.2") (d #t) (k 0)))) (h "1bn64xsf9vl9lhpj1xlv0fqc8z4hc27mklaml78xbprfkfgrf8x6")))

