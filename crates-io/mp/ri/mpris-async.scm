(define-module (crates-io mp ri mpris-async) #:use-module (crates-io))

(define-public crate-mpris-async-0.1.0 (c (n "mpris-async") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "mpris") (r "^2.0.1") (d #t) (k 0)))) (h "18qrphjr8blpmgdlk5ha2zqnada1jkw1h3zfk84gajk8gzhy5wnc")))

