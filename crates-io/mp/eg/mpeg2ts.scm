(define-module (crates-io mp eg mpeg2ts) #:use-module (crates-io))

(define-public crate-mpeg2ts-0.1.0 (c (n "mpeg2ts") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1kifw5m766xv6yhb2arzrrhm3k77rcl5mgvgy0ivr8zijg0b0jzf")))

(define-public crate-mpeg2ts-0.1.1 (c (n "mpeg2ts") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1cpknzy0nrmf33a7mglqkj4c1qvmsvr95hqmv4pgc58dpcmwjf3s")))

(define-public crate-mpeg2ts-0.2.0 (c (n "mpeg2ts") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0ayg4qyspznkqjg2f9sslg0hvf1jmf2abg6my0nw8952viikpf1f")))

(define-public crate-mpeg2ts-0.3.0 (c (n "mpeg2ts") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "1r92iixsnmbcr1q9ivpcn2xm0hvk1wqxp2nsxgbqgdwpkg863mnd")))

(define-public crate-mpeg2ts-0.3.1 (c (n "mpeg2ts") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0g84446iycm46vjapp4qawvkakqv6dcflgyy2b4aip76ffmgwdb4")))

