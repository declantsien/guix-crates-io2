(define-module (crates-io mp eg mpeg_encoder) #:use-module (crates-io))

(define-public crate-mpeg_encoder-0.1.0 (c (n "mpeg_encoder") (v "0.1.0") (d (list (d (n "ffmpeg-sys") (r "2.8.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1z2qln4al2599kyiix5vrksvrqhz0x8g52ypzlsyvnpqv8fg1s4s")))

(define-public crate-mpeg_encoder-0.2.0 (c (n "mpeg_encoder") (v "0.2.0") (d (list (d (n "ffmpeg-sys") (r "2.8.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "14g6408wbc4jbrrjc3q63r1yi0srvyzc6n4f0drzxvwlb2lx8109")))

(define-public crate-mpeg_encoder-0.2.1 (c (n "mpeg_encoder") (v "0.2.1") (d (list (d (n "ffmpeg-sys") (r "2.8.*") (f (quote ("avformat" "swscale"))) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0pzh7i9d7hynrrbjxpr3hldr9k6v420akq5s84y7adsrv45rfq5p")))

