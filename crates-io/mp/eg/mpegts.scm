(define-module (crates-io mp eg mpegts) #:use-module (crates-io))

(define-public crate-mpegts-0.1.0 (c (n "mpegts") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^0.6.1") (d #t) (k 0)) (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "0kwkq5v3vjj0vh4hdw9hnvygi2v31h1grzl92a8vn8a5hhqgn98f")))

(define-public crate-mpegts-0.1.1 (c (n "mpegts") (v "0.1.1") (d (list (d (n "bitstream-io") (r "^0.6.1") (d #t) (k 0)) (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "1cppr1wndh1vz38r18c3gv8yjxpqcvsc3cm9ranx9jczbd5wf7w3")))

(define-public crate-mpegts-0.1.2 (c (n "mpegts") (v "0.1.2") (d (list (d (n "bitstream-io") (r "^0.6.1") (d #t) (k 0)) (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "1z05nqk9igc3g90y446b57jw5191zz3kcillwdgj884llpbq20hv")))

