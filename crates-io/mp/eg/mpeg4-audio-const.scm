(define-module (crates-io mp eg mpeg4-audio-const) #:use-module (crates-io))

(define-public crate-mpeg4-audio-const-0.1.0 (c (n "mpeg4-audio-const") (v "0.1.0") (h "00l9zhdgiyf50rk78jy2p2r46yjmx32x4iq7m2acf16jzv9lvfg9")))

(define-public crate-mpeg4-audio-const-0.2.0 (c (n "mpeg4-audio-const") (v "0.2.0") (h "062rwli1lw2hiy5h6xnjfy5kkfixlfj0mj6jvvx932dnfligx8cn")))

