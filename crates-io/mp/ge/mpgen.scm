(define-module (crates-io mp ge mpgen) #:use-module (crates-io))

(define-public crate-mpgen-0.1.0 (c (n "mpgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ci0ri8q9d96qdd7q4p9wa9mbkr79x3igr30lfbg7v1pyh85gdca")))

(define-public crate-mpgen-0.1.1 (c (n "mpgen") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "16yl2gb1mci2nsa8v7yssq2dfkjb6b9sw5rxw817nm8rmd080h4h")))

(define-public crate-mpgen-0.1.2 (c (n "mpgen") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1zm8c9iknijvldyxhbn89gbw71y2bzmsqb6zwqyv8v1gmb1rdaac")))

