(define-module (crates-io mp mc mpmc-ringbuf) #:use-module (crates-io))

(define-public crate-mpmc-ringbuf-0.1.0 (c (n "mpmc-ringbuf") (v "0.1.0") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (d #t) (k 0)))) (h "190x801dsa553jadqx7kp8kkn14z7f3f64imb44lw696hyadxrrq")))

(define-public crate-mpmc-ringbuf-0.1.1 (c (n "mpmc-ringbuf") (v "0.1.1") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1blw5cd36pi6gg1968h1098ggag4d5np68xbxwhg938nfhxkxcsa")))

(define-public crate-mpmc-ringbuf-0.1.2 (c (n "mpmc-ringbuf") (v "0.1.2") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "06wifznadz246rkh5ahj1hmq0w8ngm2zxxajad0c8mv0bq5qj0fc")))

(define-public crate-mpmc-ringbuf-0.1.3 (c (n "mpmc-ringbuf") (v "0.1.3") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "00q2hw4n8dvj5va48is9h5l9fbxk7pafag9yzqqwxpxx678xzy4b")))

(define-public crate-mpmc-ringbuf-0.1.4 (c (n "mpmc-ringbuf") (v "0.1.4") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "09jhz974m1y0gzsgl1v4z02nf7x7lvh3fsvww16kdymx9k6fbv72")))

