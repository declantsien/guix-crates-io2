(define-module (crates-io mp mc mpmc-async) #:use-module (crates-io))

(define-public crate-mpmc-async-0.1.0 (c (n "mpmc-async") (v "0.1.0") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "04zhm3r728hvfrcc8cmihm8mw6w6015jmk39y7hk4hiqm42dh1zj")))

(define-public crate-mpmc-async-0.1.1 (c (n "mpmc-async") (v "0.1.1") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "161pxf5j5vp87dq7rg8y3k4651zz4ma6hr5g3jh79x380ljmhav0")))

(define-public crate-mpmc-async-0.1.2 (c (n "mpmc-async") (v "0.1.2") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "0yf7jg2gzxkqj0p7wnprf7wqwla48qv85g8wwzbcsxn6j7djx75l")))

(define-public crate-mpmc-async-0.1.3 (c (n "mpmc-async") (v "0.1.3") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1pqghbxwljfkjq195wh351d6r379bdmzp3dcp2dgqkaxsj08pbcc")))

(define-public crate-mpmc-async-0.1.4 (c (n "mpmc-async") (v "0.1.4") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "0i5i1fzvqg33mpgkr9lki8prasssjcmc0brhjpy09xj9pxhqj43b")))

(define-public crate-mpmc-async-0.1.5 (c (n "mpmc-async") (v "0.1.5") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1678p9wvj2wi30hm5bh2z8p5p4ihy56l7688ggynm71ypglsznyr")))

(define-public crate-mpmc-async-0.1.6 (c (n "mpmc-async") (v "0.1.6") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1v65v1q2yhz744igg6y4xgns5yh49236h9qj1ab3q63a9wrckh6w")))

(define-public crate-mpmc-async-0.1.7 (c (n "mpmc-async") (v "0.1.7") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "0bjj7sj7d59h27b81wfcl5ny5z4jb9zqg30kqfi37h28san8ri1f")))

