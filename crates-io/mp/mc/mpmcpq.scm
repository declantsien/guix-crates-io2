(define-module (crates-io mp mc mpmcpq) #:use-module (crates-io))

(define-public crate-mpmcpq-0.1.0 (c (n "mpmcpq") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1524bb8fhdkpip0wynlczddq6c2yfl3k96rc0kbpdq6rvr6ql73k")))

(define-public crate-mpmcpq-0.1.1 (c (n "mpmcpq") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "09ka4jdz20hkz50wdswkndhd3p6k7py5nvp45mgyqw31gbgr8s5v")))

(define-public crate-mpmcpq-0.2.0 (c (n "mpmcpq") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0c2z3yc8pgfamis36z6lrxk1kkrp2l9mxl7hpgdmiviln6gqqjzf")))

(define-public crate-mpmcpq-0.3.0 (c (n "mpmcpq") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1cfk81ybjkdd8f381rpyy75cgmsh1haw4gjwr8si0nw3bv9sj3kc")))

(define-public crate-mpmcpq-0.4.0 (c (n "mpmcpq") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "13132p2ainhj17dzvrhlvzavns6kzzny9dsbf9gwwj7hd7sbmfrv")))

(define-public crate-mpmcpq-0.5.0 (c (n "mpmcpq") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "03w3kn891rmr838jnbwm07lnxx4fz19346kiapzwm390arw2yriw")))

(define-public crate-mpmcpq-0.6.0 (c (n "mpmcpq") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1syi3vqa1qhg4bbkllcq9lxy0ppj53i4av99sbm9rfx29lxjj9w9")))

(define-public crate-mpmcpq-0.7.0 (c (n "mpmcpq") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0ng290y3jnkay3ym3hq678ghl5hrd0kkrzvs1rxkddinc48bs9j9")))

(define-public crate-mpmcpq-0.8.0 (c (n "mpmcpq") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0kfjabfyc8b421y6plh51bn0d9aynxwmycbr1svhmyqjl5wi0r20")))

(define-public crate-mpmcpq-0.9.1 (c (n "mpmcpq") (v "0.9.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "1dcibz0vgj3bj6qh8gzwj1bg561gic1rw8krz1xr02w786s3fs23")))

(define-public crate-mpmcpq-0.9.2 (c (n "mpmcpq") (v "0.9.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0qsizcwhg93bixbcxigsj2m8q2k06przfwcsxq3snw5vhc5ggrra")))

(define-public crate-mpmcpq-0.9.3 (c (n "mpmcpq") (v "0.9.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0m3vd5vrjl62hlznw7xxgyhiwkxsv4n3j0vdja44pmz0hgphjb07")))

