(define-module (crates-io mp mc mpmc-scheduler) #:use-module (crates-io))

(define-public crate-mpmc-scheduler-0.1.0 (c (n "mpmc-scheduler") (v "0.1.0") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "07v3bk5rz6ip5l7a90in04clilsmk2fvyggsrlilphlgx0cckyrs")))

(define-public crate-mpmc-scheduler-0.1.1 (c (n "mpmc-scheduler") (v "0.1.1") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1d2n8vrsllvz3r5zkrc6xha7av6jvvcapbc97vng221yid2859sv")))

(define-public crate-mpmc-scheduler-0.1.2 (c (n "mpmc-scheduler") (v "0.1.2") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1f5r51wswbv3pfv988gzqjqinr97wgicms2vmgfjrvg64qq0967a")))

(define-public crate-mpmc-scheduler-0.2.0 (c (n "mpmc-scheduler") (v "0.2.0") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0g11s1ilnbpaaw8qh5x9zlckc1nmg3hl0qzg4vmmk4kd8qchlja7")))

(define-public crate-mpmc-scheduler-0.2.1 (c (n "mpmc-scheduler") (v "0.2.1") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1pzz0rvxplx4654g419307f1b2fg6zmxb0j851vnszzmi6ck3ihc")))

(define-public crate-mpmc-scheduler-0.2.2 (c (n "mpmc-scheduler") (v "0.2.2") (d (list (d (n "bus") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "02iisylf9kj9gsl0bd3sxzwk3ny0f94kiqf2q08rkm2hvrmswv1a")))

(define-public crate-mpmc-scheduler-0.3.0 (c (n "mpmc-scheduler") (v "0.3.0") (d (list (d (n "bus") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "07rcw4vpnxa4llkym2vvcjr1v9m5p23qvg3913wg5gjq6yy05wyd")))

(define-public crate-mpmc-scheduler-0.3.1 (c (n "mpmc-scheduler") (v "0.3.1") (d (list (d (n "bus") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "06gd66jqa1cqsxzb8qsv8p5hdxdz2908gx79irhip1hrzzlxc3jq")))

