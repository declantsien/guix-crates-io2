(define-module (crates-io mp mc mpmc) #:use-module (crates-io))

(define-public crate-mpmc-0.1.2 (c (n "mpmc") (v "0.1.2") (h "0sfs3lbjg09jjz7calr4agy4lhlyriigs86ib5mwxaw9da06qgp9")))

(define-public crate-mpmc-0.1.3 (c (n "mpmc") (v "0.1.3") (h "19fhxbhj1xs1zkq9nrswrbnj2ww06k6sw43ir03dv58v0wb6ccjw")))

(define-public crate-mpmc-0.1.4 (c (n "mpmc") (v "0.1.4") (h "0hap9g21rzqa91cr7m3xc9i3lwri3jmjypfdmnk5m577gqmjp073")))

(define-public crate-mpmc-0.1.5 (c (n "mpmc") (v "0.1.5") (h "0fygw43hb5whrq14azakg1hrdssvc6q6jwnrn7392hkqimlpr56b")))

(define-public crate-mpmc-0.1.6 (c (n "mpmc") (v "0.1.6") (h "1vfwd5xd5r9m72dqjydplb05b07wzn7yvy5m05pfjfwm58jb2y5z")))

