(define-module (crates-io mp u6 mpu6050) #:use-module (crates-io))

(define-public crate-mpu6050-0.1.0 (c (n "mpu6050") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.1") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "070q3gihi7xfs1h7bkfa9f77yjf5rh418bf9iksxn9ar8nhzkj6s")))

(define-public crate-mpu6050-0.1.1 (c (n "mpu6050") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "17dyqsp108l61b4xbs54g0gxzszd5767mkfrvilv370clnqpcp0g")))

(define-public crate-mpu6050-0.1.2 (c (n "mpu6050") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1jcd1q8hqndfc94l8g1m2yxsrivk6y84zppm62r2clxvm5yz2956")))

(define-public crate-mpu6050-0.1.3 (c (n "mpu6050") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18.0") (k 0)))) (h "0h8zafhm29lzwb2a6vvd08w4bk7qw7a128akcmpccp6g4mjzynzh")))

(define-public crate-mpu6050-0.1.4 (c (n "mpu6050") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24.1") (k 0)))) (h "0f5y8j949jqxk6433abbarcyv99hazhsx7l5z5ds4pd9gfijfx27")))

(define-public crate-mpu6050-0.1.5 (c (n "mpu6050") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24.1") (k 0)))) (h "1p4920ws0lpag6h4vc6nagk48shrjimv08wfxp0m0qzryvzcw9fh")))

(define-public crate-mpu6050-0.1.6 (c (n "mpu6050") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.31.2") (k 0)))) (h "1wmrwgv66ikjw87mqhwfb7x6bh4m0zaqkp8lma1cmg24n997immb")))

