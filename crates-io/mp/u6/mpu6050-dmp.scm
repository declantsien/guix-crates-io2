(define-module (crates-io mp u6 mpu6050-dmp) #:use-module (crates-io))

(define-public crate-mpu6050-dmp-0.2.0 (c (n "mpu6050-dmp") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "09a798xrbhfd5hmx2qbwm5qlf9prc5i4zrlns25qpzvighpw79bx")))

(define-public crate-mpu6050-dmp-0.3.0 (c (n "mpu6050-dmp") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "1r2i8n20pv9da7am2fybi5g7qsirhfk8jw5v1a2pwzpi8aqrvbbq")))

(define-public crate-mpu6050-dmp-0.4.0 (c (n "mpu6050-dmp") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1cwyrfchkwr20c2lnjbimvqn2p8wi4a9fl3fi2kl4ih8rv2ph1rq")))

