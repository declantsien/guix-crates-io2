(define-module (crates-io mp u6 mpu6886) #:use-module (crates-io))

(define-public crate-mpu6886-0.1.0 (c (n "mpu6886") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "esp-println") (r "^0.3.1") (f (quote ("esp32"))) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.2") (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0l6a6d0wdqimby399n8vm0cv9mrdglm2wn5rzv6p5dphaw17ik1w")))

