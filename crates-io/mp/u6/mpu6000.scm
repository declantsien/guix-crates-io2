(define-module (crates-io mp u6 mpu6000) #:use-module (crates-io))

(define-public crate-mpu6000-0.0.1 (c (n "mpu6000") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "108lnmk6vpvypajyk4dxi8k8ciz02q8hzw17mvcfhbmyzxqs77g4")))

(define-public crate-mpu6000-0.0.2 (c (n "mpu6000") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "0x4iqwrnnisdg9bbkm4aw7m6b1a2crkr2vp0m0gk0mbvci78y395")))

(define-public crate-mpu6000-0.0.3 (c (n "mpu6000") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.21") (k 0)))) (h "0a7pmwvby8g5cg86vvfkp1vmmai7f8qs66izs7y1lp242xzjrgzi")))

(define-public crate-mpu6000-0.0.4 (c (n "mpu6000") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.21") (k 0)))) (h "0cjspkyybsi07snrsh663i5y8dmx267wdrh8zjqky0q9zr93licn")))

(define-public crate-mpu6000-0.0.5 (c (n "mpu6000") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1wxnvklvf1n05m7n2zyx5x9l4kc5gidsz0q1xrbk7904j69djz58")))

(define-public crate-mpu6000-0.1.0 (c (n "mpu6000") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1bbgsfp23yxiy2dhasml5hwnfc9wqgzzv6g2scighh3i323kzgas")))

(define-public crate-mpu6000-0.1.2 (c (n "mpu6000") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "090jinblx2pf8m85ix39nwf5691an9mmjbyqz58ms8jn7d7h0q97")))

(define-public crate-mpu6000-0.1.3 (c (n "mpu6000") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "19g7gcd0xdz9s77lc5j53ihrai69w3nnpjywm36smacaxnfqm6n9")))

(define-public crate-mpu6000-0.1.4 (c (n "mpu6000") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "13p5dd6m55i3fn0aggflwn577kc0zcbhvsr9rlh8b279bc0ngk1c")))

(define-public crate-mpu6000-0.1.5 (c (n "mpu6000") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1905m6kg1ls2flfvd4a9sh6fmym02mrwn5abmj4bywm3invz2nps")))

(define-public crate-mpu6000-0.1.6 (c (n "mpu6000") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1b8pkvq7yb55wzkm81hx7hnf3i2s0vw3samc7qs7qbv3kzj71ps6") (y #t)))

(define-public crate-mpu6000-0.1.7 (c (n "mpu6000") (v "0.1.7") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1ywf558cx1r4y0h98g9rcljfk00kgfxzqgfc1j895cn2d3lwnjxq")))

(define-public crate-mpu6000-0.1.8 (c (n "mpu6000") (v "0.1.8") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (k 0)))) (h "1275hj5z1862l2xv3kkcjxcr5m6xzz029dqwax30whaavw90zhmw")))

(define-public crate-mpu6000-0.1.9 (c (n "mpu6000") (v "0.1.9") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "03zza73gki4fmfa5qlhbsx6xjibs19mpqhk1pi2wm4gk6ggk7y35")))

(define-public crate-mpu6000-0.2.0 (c (n "mpu6000") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1y7chq9bz58rq7qhnfj8nvh3bmdpswh8vi34mm8spkx7lbyz9l5c")))

(define-public crate-mpu6000-0.2.1 (c (n "mpu6000") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "00jjikrky6j03szfxlbvvcpbbzp6vl6bjrna4c5rpv0vv15q4b5w")))

(define-public crate-mpu6000-0.3.0 (c (n "mpu6000") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "fixed-point") (r "^1.0") (k 0)))) (h "1c3m89p83gcnq46alpi9vj6dnhzdzac6q3jrgbnrsgj9ynvd31w7")))

