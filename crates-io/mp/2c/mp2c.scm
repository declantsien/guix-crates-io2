(define-module (crates-io mp #{2c}# mp2c) #:use-module (crates-io))

(define-public crate-mp2c-0.1.0 (c (n "mp2c") (v "0.1.0") (h "06bzb0hz8kgqjja728567bqs8i4yj19l265r8j5g72kc0a35n1bp")))

(define-public crate-mp2c-0.1.1 (c (n "mp2c") (v "0.1.1") (h "1ck7v38aznl4djf13m4b715hj9lrf9gdcbh6vb82xg9yp9pnbswm")))

(define-public crate-mp2c-0.1.2 (c (n "mp2c") (v "0.1.2") (h "064485yaf97ys97fhn79a1wb9f62vckczzq9jvgqga6x68clgpw8")))

