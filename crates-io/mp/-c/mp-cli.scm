(define-module (crates-io mp -c mp-cli) #:use-module (crates-io))

(define-public crate-mp-cli-0.1.0 (c (n "mp-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0rm9yvli3q4ai1ssvqnrzaf3bqskxwh075j4xnfj8nhqz5h47syc")))

(define-public crate-mp-cli-0.1.1 (c (n "mp-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1qr28pi1c33aqmy7cdaap9n5ryh1i68y31lk6zxfj943d94ghhch")))

(define-public crate-mp-cli-0.1.2 (c (n "mp-cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0gz2kizhs9kxbwav8c6i590bgdl50080rx2a8anh86b64f43gr9v")))

(define-public crate-mp-cli-0.1.3 (c (n "mp-cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1q9fg66jhg4pkka78jpszyaa8ipd30m5qmxh34fjciyxv2y1iim9")))

(define-public crate-mp-cli-0.1.4 (c (n "mp-cli") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0s2g6p3s2dc8qs96lshkdy0ad85kjhg6vp076kyk6hsba9w1hrm0")))

(define-public crate-mp-cli-0.1.6 (c (n "mp-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mpd-easy") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "068qc1hsh3bnashiczsxwnj7dh8nxynid7ffbfb2yfxpkplv21vp")))

(define-public crate-mp-cli-0.1.7 (c (n "mp-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mpd-easy") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yqlczxyyi8rjdp26qqa0fp7kzc2qlmrm52lljvm2br7h903vskl")))

(define-public crate-mp-cli-0.2.0 (c (n "mp-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mpd-easy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zq2yycpnn83651rxn5g0ilw4wk3hnv1pnms36bnxrrx2szlwba3")))

(define-public crate-mp-cli-0.2.1 (c (n "mp-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mpd-easy") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cnkkpv2144sqajsyi8m9pirv3vsg7cacd3fgpgms9v08bxkf1lf")))

