(define-module (crates-io mp fr mpfr-sys) #:use-module (crates-io))

(define-public crate-mpfr-sys-0.0.1 (c (n "mpfr-sys") (v "0.0.1") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "0fxj72hxg5pnbjp37avjaimgk009l016q6yykgbzssgcr458f8p3")))

(define-public crate-mpfr-sys-0.0.2 (c (n "mpfr-sys") (v "0.0.2") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "14pppvnb9nw5il7smv28224zy31mm2bscm6m1m2d5wa933krghmi")))

(define-public crate-mpfr-sys-0.0.3 (c (n "mpfr-sys") (v "0.0.3") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "1jw04ml5arfn5k6wqhv978940bvzbw3rlav1z40dhbn9p2jylrs9")))

(define-public crate-mpfr-sys-0.0.4 (c (n "mpfr-sys") (v "0.0.4") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "12nzxhcypxwn3bih2knilp2pf97ksza1kv6gnvp44wzzj4sjnqxm")))

(define-public crate-mpfr-sys-0.0.5 (c (n "mpfr-sys") (v "0.0.5") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "1dm0a1qj0n0qdgqq81sfvgly70w5ha18a0scjdfvk4hq4y4pdv4w")))

(define-public crate-mpfr-sys-0.0.6 (c (n "mpfr-sys") (v "0.0.6") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "19bdxbm90fibpyn3cikzg7q591a0dqh3r68h794kyjhh54573qn5")))

(define-public crate-mpfr-sys-0.0.7 (c (n "mpfr-sys") (v "0.0.7") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "1n70ga4bw98irahawfr8sznyx8wz2bxy1xxms5fgaq2zjpy57k6q")))

(define-public crate-mpfr-sys-0.0.8 (c (n "mpfr-sys") (v "0.0.8") (d (list (d (n "gmp-sys") (r "*") (d #t) (k 0)))) (h "0zsj9p23y9ixqj42m272m4ncx38dy4m7i4d153rp6a0qiy9pli1r")))

