(define-module (crates-io mp fr mpfr) #:use-module (crates-io))

(define-public crate-mpfr-0.0.1 (c (n "mpfr") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)))) (h "138bhmv71dzbg259b5xhfjf9j82bpdjnkd6w4avjbpjh3v0y0vra")))

(define-public crate-mpfr-0.0.2 (c (n "mpfr") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)))) (h "0xdjpsw7qr8zj8n8rfkkrmbkvixvhpgpz15vd94m93admpn38991")))

(define-public crate-mpfr-0.0.3 (c (n "mpfr") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)))) (h "15iwgqrk6m8knq7ydgwfjxs1nbx3wywjjiii5wsa5lv5dx3pq40z")))

(define-public crate-mpfr-0.0.4 (c (n "mpfr") (v "0.0.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)))) (h "0synd9gzzm7m56ghgd3dznkwiwj1yj8m354ggdjzbl49yamag3hc")))

(define-public crate-mpfr-0.0.5 (c (n "mpfr") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "16wrqryddkmnpxsbgrzcacsfn896sa57yg9acfbrz52w19gqps19")))

(define-public crate-mpfr-0.0.6 (c (n "mpfr") (v "0.0.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "16nhprx60m6hp5pqmm3qwk5rx8aqbrm688gcbnbbdmhkc3mh9kay")))

(define-public crate-mpfr-0.0.7 (c (n "mpfr") (v "0.0.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0qbachj1grndh2hp7bsm5fq28ggh00qpjj005zzayvcm2biv25lj")))

(define-public crate-mpfr-0.0.8 (c (n "mpfr") (v "0.0.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "mpfr-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1m28fnxlvakn3grwxp676fmh1acl32hn4ckbva321m7wlfj6imxa")))

