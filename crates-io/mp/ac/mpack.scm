(define-module (crates-io mp ac mpack) #:use-module (crates-io))

(define-public crate-mpack-0.0.1 (c (n "mpack") (v "0.0.1") (h "188aj08v06ypg2x3vmzdpfvar9b2mlrkdda26i9h3q0rxf0yii0j")))

(define-public crate-mpack-0.0.2 (c (n "mpack") (v "0.0.2") (h "0a9989cil3bi66n6hr2d3172mws52l9jl25mi0x0w0zcv2cicnq0")))

(define-public crate-mpack-0.0.3 (c (n "mpack") (v "0.0.3") (h "0fkm54pvcicg6ldgzrnpjq7p8xrl0zrx1fcd7sxrsbhc3dajsybs")))

(define-public crate-mpack-0.0.4 (c (n "mpack") (v "0.0.4") (h "147n1pk9a3lfvzv3q21yi58q1w1nghj3k02mh2b5xw4zhb32q983")))

(define-public crate-mpack-0.0.5 (c (n "mpack") (v "0.0.5") (d (list (d (n "rand") (r "^0.2.0") (d #t) (k 0)))) (h "1c27wzxi9i3n65p8jmm4axfwxd8s3jy59g80z9phks44c56y09rf")))

(define-public crate-mpack-0.0.6 (c (n "mpack") (v "0.0.6") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0mn2gr7a2nqw4wvr7jzh9x3dm3xbg7w88ldns0m14xqgcf0byghp")))

(define-public crate-mpack-0.1.0 (c (n "mpack") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "08bldmcx6cwpddsqdkip0nxhhj8392w8x2jihymf0mj3clq9wzpi")))

(define-public crate-mpack-0.1.1 (c (n "mpack") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "19r1s3gdldgalxv1cs1mk6aw741clqs6vd2sjfqq2hkcxs8mkyai")))

(define-public crate-mpack-0.1.2 (c (n "mpack") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1d5dswf7537s315bvvfd3rv32rj8aniw1hl8f51zzl17nv4khj76")))

