(define-module (crates-io mp #{3l}# mp3lame-encoder) #:use-module (crates-io))

(define-public crate-mp3lame-encoder-0.1.0 (c (n "mp3lame-encoder") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.2") (k 0)) (d (n "symphonia") (r "^0.5.1") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "0b97q20m1dhfz7cg2i9m1m05l6298gial4hyrgxricawhh7wxx52") (f (quote (("std")))) (y #t)))

(define-public crate-mp3lame-encoder-0.1.1 (c (n "mp3lame-encoder") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.4") (k 0)) (d (n "symphonia") (r "^0.5.2") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "02qg47bsagn53k69hbnbk8cczk7p3xl30kgpizcwimxy07y9q4wy") (f (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1.2 (c (n "mp3lame-encoder") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.4") (k 0)) (d (n "symphonia") (r "^0.5.2") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "0v1da3dk19n46dhazqq4ri9mphbfl6wljw2w4lr7jl28r8rj6bc1") (f (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1.3 (c (n "mp3lame-encoder") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.4") (k 0)) (d (n "symphonia") (r "^0.5.2") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "01zv389qlwv6b1hs5nd1bpqvhralm1k4zlvm7z1m67f24m6lma61") (f (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1.4 (c (n "mp3lame-encoder") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.4") (k 0)) (d (n "symphonia") (r "^0.5.2") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "04qc8fawjyvn5p8h8mdfn6ngvfz8gsprqm7n9npyssj2n11ckrna") (f (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1.5 (c (n "mp3lame-encoder") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mp3lame-sys") (r "^0.1.4") (k 0)) (d (n "symphonia") (r "^0.5.2") (f (quote ("ogg" "vorbis"))) (k 2)))) (h "1iw9jsa9204i5waz9whmznh1yc5hv0xzb9lpxqpjmvprw9j2wqk4") (f (quote (("std"))))))

