(define-module (crates-io mp #{3l}# mp3lame-sys) #:use-module (crates-io))

(define-public crate-mp3lame-sys-0.1.0 (c (n "mp3lame-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (t "cfg(unix)") (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (t "cfg(windows)") (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1azkqdzz159bin6l7hsxr84rfhn67a0b9ldvmkgv6rfps0lxb52a") (f (quote (("decoder")))) (y #t)))

(define-public crate-mp3lame-sys-0.1.1 (c (n "mp3lame-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (t "cfg(unix)") (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (t "cfg(windows)") (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "16n0wzq6zb77c1qx86h4n1kn0lh19nxb0wl19hl5j4xfxs2kmvc0") (f (quote (("decoder"))))))

(define-public crate-mp3lame-sys-0.1.2 (c (n "mp3lame-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (t "cfg(unix)") (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (t "cfg(windows)") (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "03llsqg303rf43gqiqjflijxd0z7fwnj30zgq8g1i8k8nrkh2nn7") (f (quote (("decoder"))))))

(define-public crate-mp3lame-sys-0.1.3 (c (n "mp3lame-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "autotools") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (t "cfg(windows)") (k 1)))) (h "1r884hnjb8cag3rm99xis8vhmvwwlwnbnqf3225dxq3zqdp87zyn") (f (quote (("decoder")))) (y #t)))

(define-public crate-mp3lame-sys-0.1.4 (c (n "mp3lame-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "autotools") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (t "cfg(windows)") (k 1)))) (h "01ki39nspdzan8h6kk1mi0shyxwfglg4lpsc2ry9b3nm7va6z2cr") (f (quote (("decoder"))))))

