(define-module (crates-io rf w- rfw-scene) #:use-module (crates-io))

(define-public crate-rfw-scene-0.2.0 (c (n "rfw-scene") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "l3d") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rfw-backend") (r "^0.2") (d #t) (k 0)) (d (n "rfw-math") (r "^0.2") (d #t) (k 0)) (d (n "rfw-utils") (r "^0.2") (d #t) (k 0)) (d (n "rtbvh") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tobj") (r "^3.0") (d #t) (k 0)))) (h "19s307365madlyb4zlfzadw9ff147b0kr8pz3dp86cp7rgbgc097")))

