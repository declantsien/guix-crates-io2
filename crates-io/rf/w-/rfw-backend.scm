(define-module (crates-io rf w- rfw-backend) #:use-module (crates-io))

(define-public crate-rfw-backend-0.2.0 (c (n "rfw-backend") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "l3d") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rfw-math") (r "^0.2") (d #t) (k 0)) (d (n "rtbvh") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dpqcd0n8nmq5qplq1rx8dgmfdgmb87niyfbxzkq2iv5n0acsif5")))

