(define-module (crates-io rf w- rfw-math) #:use-module (crates-io))

(define-public crate-rfw-math-0.2.0 (c (n "rfw-math") (v "0.2.0") (d (list (d (n "glam") (r ">=0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10xx73lnhaba90fmw1jj40fm8k0jqcbsss9cv63mq4b5gq7aax3w")))

