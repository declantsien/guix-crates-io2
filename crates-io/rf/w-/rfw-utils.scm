(define-module (crates-io rf w- rfw-utils) #:use-module (crates-io))

(define-public crate-rfw-utils-0.2.0 (c (n "rfw-utils") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "1q5znb4bkdlpgvd7k238y8gyxbl1adm2ls33jj1igapffqa0r1rg")))

