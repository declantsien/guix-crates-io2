(define-module (crates-io rf w- rfw-font) #:use-module (crates-io))

(define-public crate-rfw-font-0.2.0 (c (n "rfw-font") (v "0.2.0") (d (list (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "rfw") (r "^0.2") (d #t) (k 0)))) (h "0l0hp49glrzr4bqp16z95b3i5fqzdqg059dp2ff767p3iycdw8in")))

