(define-module (crates-io rf c1 rfc1939) #:use-module (crates-io))

(define-public crate-rfc1939-0.1.0 (c (n "rfc1939") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1w07mwn74csallvj2595mckvicaq2qra7p1bi223r4zgvb3gvg3w")))

(define-public crate-rfc1939-1.0.0 (c (n "rfc1939") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1ff4y5b78lc2sprdgpn2ckdag7hh49hwfm8hnnqy339nr95rz2g0")))

