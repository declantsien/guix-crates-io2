(define-module (crates-io rf c1 rfc1700) #:use-module (crates-io))

(define-public crate-rfc1700-0.1.0 (c (n "rfc1700") (v "0.1.0") (h "0712qpzmmvpkmf9lq2bfvn8xn7y2ihkl9rncjij8vhl6ijvrzwjk")))

(define-public crate-rfc1700-0.1.1 (c (n "rfc1700") (v "0.1.1") (h "0qnx4kb3fsrin1yl3q2b4w8a4816akndqghb50mawjw6n0w4yc4k")))

(define-public crate-rfc1700-0.1.2 (c (n "rfc1700") (v "0.1.2") (h "0m59vgxz5i7iagdjpxacmbp9cqbx70c5g2rhzrs4kfba9h4b0hs3")))

(define-public crate-rfc1700-1.0.0 (c (n "rfc1700") (v "1.0.0") (h "0i943v2l3yfwhn23dy4z00w0fwa59d6vppnn41p0s99jfscabbhq")))

