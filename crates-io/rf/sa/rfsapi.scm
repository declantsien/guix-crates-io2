(define-module (crates-io rf sa rfsapi) #:use-module (crates-io))

(define-public crate-rfsapi-0.1.0 (c (n "rfsapi") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jfd6z8h1826kz2d7m8y0zg7avbnakjrdnra4n09yi80kl8vqvqv")))

(define-public crate-rfsapi-0.2.0 (c (n "rfsapi") (v "0.2.0") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0vvmm996ngm5wdli67l6c4vqhshzpc8w8kjgs2lkywjy7dpr9bvj")))

