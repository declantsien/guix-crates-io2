(define-module (crates-io rf ir rfirebird) #:use-module (crates-io))

(define-public crate-rfirebird-0.1.0 (c (n "rfirebird") (v "0.1.0") (d (list (d (n "argopt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0v1dh78x2j91dhaqvqzhp47x8knk7mqpz4rlbw1qp22g6dyyzxbr") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:argopt" "dep:tabled"))))))

