(define-module (crates-io rf le rflex) #:use-module (crates-io))

(define-public crate-rflex-0.1.0 (c (n "rflex") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "07jwhpl3h87274pmwk7q64kb4mf8xcqsvg897i2k327fr8wqg1li")))

(define-public crate-rflex-0.2.0 (c (n "rflex") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "0cy3h4q1cv6sb1mkzx4kasr3is8sprs0jyfrz9i92mxnjay5jq0h")))

(define-public crate-rflex-0.2.1 (c (n "rflex") (v "0.2.1") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "0n3r34b4q2z56vgcjxplhs9whi1f2lpjhny5wvic36hkqazdq6f1")))

(define-public crate-rflex-0.3.0 (c (n "rflex") (v "0.3.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "14lysy9p3lrya0krsqyzgifpg8rrbqh31vmflzbkh7xi3axj5q2m")))

(define-public crate-rflex-0.4.0 (c (n "rflex") (v "0.4.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "0r5nh8zrjc4lvgcp1zkx3yqnv8qmragicfk25hsgpxn94fcm34nj")))

(define-public crate-rflex-0.5.0 (c (n "rflex") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "0vihsdwcd2wha1xzgmrmyz4khj83pj0ys6cr9jrijmnbrs203z1l")))

(define-public crate-rflex-0.6.0 (c (n "rflex") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "1qfdnazc5dvm605kgn5lii753drpwhwr9aikdh9j5kimzi7i0wba")))

(define-public crate-rflex-0.7.0 (c (n "rflex") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "1aj7vnrir7q53iwlczcp5285xzn9qvxgkg44b7iq6r2bh76fdkl8")))

(define-public crate-rflex-0.8.0 (c (n "rflex") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "1rjvzdx7jqc9bik6f0wvknqvphjp7p3vzl9380zivd2qr6cbdxw7")))

(define-public crate-rflex-0.8.1 (c (n "rflex") (v "0.8.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "liquid") (r "^0.17") (d #t) (k 0)))) (h "1v41q8ffdl8017gsk3hk7jzysg5sj177d6idllgp9b8ciimyc1ir")))

