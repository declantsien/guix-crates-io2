(define-module (crates-io rf s_ rfs_tester) #:use-module (crates-io))

(define-public crate-rfs_tester-0.1.0 (c (n "rfs_tester") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1bb92m6zaq580qpj817xkfmcggyhwg0fg2nwjd22plsig21rl02f") (y #t)))

(define-public crate-rfs_tester-0.1.1 (c (n "rfs_tester") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "105lzf3dspg2a54lz2yp5wy78gh43kz9i377qq37wrhq0jk9rqk3")))

(define-public crate-rfs_tester-0.1.2 (c (n "rfs_tester") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1yyjyn3awf315a6s4ln0z6rl76bzf0xswz02lncn7fix0fzfxrrf") (y #t)))

(define-public crate-rfs_tester-0.1.3 (c (n "rfs_tester") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "079mh6xfiwmg6r6ghknlmh0j7snwv2x3alimb22bsrzrc1zj0924") (y #t)))

(define-public crate-rfs_tester-0.1.4 (c (n "rfs_tester") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0ny650l0p4y0nj82g8z570nhvhk2a325mjv3jdrc2aikj0fmzh7w")))

(define-public crate-rfs_tester-0.1.5 (c (n "rfs_tester") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1zhqkcbl5dn5zx8cxmqmb33w4byj5qa11l8r1vhiz0vbpga06ws5")))

(define-public crate-rfs_tester-0.1.6 (c (n "rfs_tester") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1zd32w0axil8fjfd43dx6yg169rl9d16kgqiqld01p8y6r0dd58k")))

(define-public crate-rfs_tester-0.2.0 (c (n "rfs_tester") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1rzy6nkj06a6vjq4a2wcx94bwhp995b60hrndig6ba7s58figz4a")))

(define-public crate-rfs_tester-0.2.1 (c (n "rfs_tester") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0m9n0nmz4vpl0bxd4mbq5n7q1aavifafh39ky5wsvdfx04qr8xkc")))

