(define-module (crates-io rf c7 rfc7239) #:use-module (crates-io))

(define-public crate-rfc7239-0.1.0 (c (n "rfc7239") (v "0.1.0") (d (list (d (n "uncased") (r "^0.9.3") (d #t) (k 0)))) (h "0ixsyn8y2jfhfqnhwivgil3cvdr4jdr5s0nr7gqq3d3yryrifwq8")))

(define-public crate-rfc7239-0.1.1 (c (n "rfc7239") (v "0.1.1") (d (list (d (n "uncased") (r "^0.9.10") (d #t) (k 0)))) (h "0qqhdbxriyv6k0irmsxizga62a7raywfl83adp8kc0svxdgah1mi")))

