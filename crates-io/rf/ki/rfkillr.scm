(define-module (crates-io rf ki rfkillr) #:use-module (crates-io))

(define-public crate-rfkillr-0.1.3 (c (n "rfkillr") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zvariant") (r "^3.12.0") (o #t) (d #t) (k 0)))) (h "18m9i50l7nyjcd8l6lr2089mfnjmn7zgwss2hlxf9fsipa86x8aq") (f (quote (("serialization" "serde" "zvariant"))))))

