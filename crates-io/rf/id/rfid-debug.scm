(define-module (crates-io rf id rfid-debug) #:use-module (crates-io))

(define-public crate-rfid-debug-0.0.1 (c (n "rfid-debug") (v "0.0.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0hb2qab121pnyvsdyw9bqp4zc8cwhypjs7dj1rzq973cdl0f9ycq") (y #t)))

(define-public crate-rfid-debug-0.0.2 (c (n "rfid-debug") (v "0.0.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1pfh4dzq0y9flf9wmvxg743yv4lsr8zwhfbcx2cs1ac41c7nnz8s")))

(define-public crate-rfid-debug-0.0.3 (c (n "rfid-debug") (v "0.0.3") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0czxg7ci44c6nan0f8zc9h9qz003kx556v48iry4pfi4jyzbmka7")))

(define-public crate-rfid-debug-0.0.4 (c (n "rfid-debug") (v "0.0.4") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0xs6m2aksqh5adbavfjy7x03d6wzh4qlv88622xgps85i412jdcr")))

(define-public crate-rfid-debug-0.0.5 (c (n "rfid-debug") (v "0.0.5") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1i0jqjfk7567nl42cqqcb6jv3s41vnwj8mkyfs7x0ln1km3cfw2a")))

(define-public crate-rfid-debug-0.0.6 (c (n "rfid-debug") (v "0.0.6") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "15vq9q70nc0viwsfj46l5lsjadz9lfpp55c5s76s545787viwqw3")))

(define-public crate-rfid-debug-0.0.7 (c (n "rfid-debug") (v "0.0.7") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "11yk868dn6a2474c8gv0y9xnrq6jfx6smg7fbgh94wjs4dydrl3i")))

