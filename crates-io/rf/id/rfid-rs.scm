(define-module (crates-io rf id rfid-rs) #:use-module (crates-io))

(define-public crate-rfid-rs-0.1.0 (c (n "rfid-rs") (v "0.1.0") (d (list (d (n "spidev") (r "^0.4") (d #t) (k 0)))) (h "0cwrm7dwsb1l585vdw3cjjah8r5ksknnmzcpaw50l95888xs0xqf")))

(define-public crate-rfid-rs-0.1.1 (c (n "rfid-rs") (v "0.1.1") (d (list (d (n "spidev") (r "^0.4") (d #t) (k 0)))) (h "12yjnd7sa2a4cqdhvk0rvkhlywfq8sld4j82byjyc06s41ssq0c8")))

(define-public crate-rfid-rs-0.1.2 (c (n "rfid-rs") (v "0.1.2") (d (list (d (n "spidev") (r "^0.4") (d #t) (k 0)))) (h "1dig2rj6922jfv7f03shp4na4ga4ag62012w8bvbhlvlydbx7gnn")))

(define-public crate-rfid-rs-0.1.3 (c (n "rfid-rs") (v "0.1.3") (d (list (d (n "spidev") (r "^0.4") (d #t) (k 0)))) (h "0m4758yk6lkd22ly4llhhr9nz95z1s00jwk9n6kfry0hky2l282f")))

(define-public crate-rfid-rs-0.2.0 (c (n "rfid-rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "1hyadznakiv594dgf02rlw81akm7yxibphaim7nhvi2qahlwa57h")))

(define-public crate-rfid-rs-0.2.1 (c (n "rfid-rs") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0v30c1kyk2iq1x9ypk4s7mmi5nvampgvsi84m5gv9llq196200b1")))

