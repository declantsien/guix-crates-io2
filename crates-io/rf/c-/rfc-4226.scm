(define-module (crates-io rf c- rfc-4226) #:use-module (crates-io))

(define-public crate-rfc-4226-0.1.0 (c (n "rfc-4226") (v "0.1.0") (d (list (d (n "ring") (r "^0.14.6") (d #t) (k 0)))) (h "0nzx1knrpn2x9xhmv5i0486idxv571dgj1bfgbqkgf0khndjb4df")))

(define-public crate-rfc-4226-0.2.0 (c (n "rfc-4226") (v "0.2.0") (d (list (d (n "ring") (r "^0.16.20") (k 0)))) (h "08rxlzs4jichflc8xrr5nvc1ngyp5k5p7x6diyigcy8d128sfys1")))

