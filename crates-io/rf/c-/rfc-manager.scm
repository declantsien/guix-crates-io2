(define-module (crates-io rf c- rfc-manager) #:use-module (crates-io))

(define-public crate-rfc-manager-0.0.1 (c (n "rfc-manager") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)))) (h "13xq5lmgdpk5qn4fm0w8msqs2xf1z0xlfx3yx0shqh81s4xvr9xs") (r "1.62")))

