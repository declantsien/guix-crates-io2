(define-module (crates-io rf cp rfcp) #:use-module (crates-io))

(define-public crate-rfcp-0.1.0 (c (n "rfcp") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "059mq61rlga0mzw6xhypf8b87p0i698jqfh43q5mxl7sz3ddykw9") (y #t)))

(define-public crate-rfcp-0.1.1 (c (n "rfcp") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1qkb77c4x3khc1abv1g6q146ggg5ay4brgs96pj8mbrvb30rdpw2") (y #t)))

(define-public crate-rfcp-0.2.0 (c (n "rfcp") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "14fkcs6hmnik5ad7cymj3r21xjpggv0m64jr8vkhprcw5gyj4g1c") (y #t)))

(define-public crate-rfcp-0.2.1 (c (n "rfcp") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1nlj1wj3nwr8c84rjdsa3g88s1phjygcm5vnvx0nfrns7q19f2ps") (y #t)))

(define-public crate-rfcp-0.2.2 (c (n "rfcp") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1rw810zl80543idykm5466a6b774i2s4r3xvj76br34dqalxd597") (y #t)))

(define-public crate-rfcp-1.0.0 (c (n "rfcp") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1p3896b0rivphibnx0q645mi6vrlv07wxx1p1q89kydgi81wdjhp")))

(define-public crate-rfcp-2.0.0 (c (n "rfcp") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("ansi-parsing"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1nkfag2sh19kzblzd5030rwklnzv8prac7jnjsvzcdix6k9i7f36")))

