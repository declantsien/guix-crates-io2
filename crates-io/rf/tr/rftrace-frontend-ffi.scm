(define-module (crates-io rf tr rftrace-frontend-ffi) #:use-module (crates-io))

(define-public crate-rftrace-frontend-ffi-0.1.0 (c (n "rftrace-frontend-ffi") (v "0.1.0") (d (list (d (n "rftrace-frontend") (r "^0.1") (d #t) (k 0)))) (h "0iddrp3qpkxw8yljs1s38sdifgw3ksykcdl0yrzng7mji680ky8h")))

(define-public crate-rftrace-frontend-ffi-0.2.0 (c (n "rftrace-frontend-ffi") (v "0.2.0") (d (list (d (n "rftrace-frontend") (r "^0.2") (d #t) (k 0)))) (h "1p41cj9wnzbw9hayr869j7wrds8m1h3zn6v4b2z8cxrchhhac8q5")))

(define-public crate-rftrace-frontend-ffi-0.2.1 (c (n "rftrace-frontend-ffi") (v "0.2.1") (d (list (d (n "rftrace-frontend") (r "^0.2") (d #t) (k 0)))) (h "197hrs86xqxpvfxcn3mhl4p87kami366sbx828zqvic2xym7w6zs")))

