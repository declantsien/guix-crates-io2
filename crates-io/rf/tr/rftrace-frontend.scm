(define-module (crates-io rf tr rftrace-frontend) #:use-module (crates-io))

(define-public crate-rftrace-frontend-0.1.0 (c (n "rftrace-frontend") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "16k3530yqrjcxpp2553zk2qagp182q3d8mkk9q3n30k8dlkmm6j1")))

(define-public crate-rftrace-frontend-0.2.0 (c (n "rftrace-frontend") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1nagns6f1d2wgnf6zsljcf65yn5v7q6qhpfdcm9i1pa0zb0r768j")))

(define-public crate-rftrace-frontend-0.2.1 (c (n "rftrace-frontend") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0a40rc2z3wb595y5ab9xc3fdpkwagnw9mjxz4cjk2knc28x1vvym")))

