(define-module (crates-io rf tr rftrace) #:use-module (crates-io))

(define-public crate-rftrace-0.1.0 (c (n "rftrace") (v "0.1.0") (h "1pdy8q1yz72blfdcjdx3mw15rixa11wmnsjpnj799jwpgvby4bma") (f (quote (("interruptsafe") ("default" "interruptsafe") ("buildcore") ("autokernel"))))))

(define-public crate-rftrace-0.2.0 (c (n "rftrace") (v "0.2.0") (d (list (d (n "llvm-tools") (r "^0.1") (d #t) (k 1)))) (h "1rji4jincwr227scrq0n2dv9pg43ffvq6wm9wg4g2bwpyy0k9y0s") (f (quote (("staticlib") ("interruptsafe") ("default"))))))

(define-public crate-rftrace-0.2.1 (c (n "rftrace") (v "0.2.1") (d (list (d (n "llvm-tools") (r "^0.1") (d #t) (k 1)))) (h "17xw0pm97bli4y5h4mak0rzx018rl1apmlbzcb82zf3mm8la6kdp") (f (quote (("staticlib") ("interruptsafe") ("default"))))))

