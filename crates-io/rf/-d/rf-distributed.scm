(define-module (crates-io rf -d rf-distributed) #:use-module (crates-io))

(define-public crate-rf-distributed-0.1.0 (c (n "rf-distributed") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rf-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rumqttc") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1synky720636mmjbdkwfkm9icz2alj57i4wfprny5g1jxxipqrjj")))

(define-public crate-rf-distributed-0.2.0 (c (n "rf-distributed") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rf-core") (r "^0.3.1") (d #t) (k 0)) (d (n "rumqttc") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j0w253x0vrpsqvqng7pqr4pgf3wb56vmlxpki3sx10368ashclq")))

(define-public crate-rf-distributed-0.4.0 (c (n "rf-distributed") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rf-core") (r "^0.4.0") (d #t) (k 0)) (d (n "rumqttc") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2apgvkf2i20l2di61sp0qlpi7fvjbp7x2gszkjizfbiiaq8psx")))

(define-public crate-rf-distributed-0.4.1 (c (n "rf-distributed") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jm13nvazdkzp9cqwqcyfv3bxhhwj4xbwj8lfkiaixplsxv3pkw7") (y #t)))

(define-public crate-rf-distributed-0.4.2 (c (n "rf-distributed") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00pdihg1imwyggbhfv7xhb138gpmjv2sbsp4gb3ibxgd4x5jwlw5")))

(define-public crate-rf-distributed-0.4.3 (c (n "rf-distributed") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1p9vcjkpar1myi0ff4kdr14587afifx5nw52bkx518zsd4f6smzc")))

(define-public crate-rf-distributed-0.5.0 (c (n "rf-distributed") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0w6yxzrzkrhd2mps5xfsjdg3qga2nirkyfafl8xqsd7br9x0bsqc")))

(define-public crate-rf-distributed-0.5.1 (c (n "rf-distributed") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01lqfrxmci367gg6qa10439p2rfhynkbxpr1wj4qd0i0ds46wi5l")))

(define-public crate-rf-distributed-0.5.2 (c (n "rf-distributed") (v "0.5.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1lh66a1q5pfbq3wpvvwj4y5fii1shifrymmdha8chz2pjqic90lp")))

(define-public crate-rf-distributed-0.6.0 (c (n "rf-distributed") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "rf-core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "17wcvwk3ashj77qfxnhm8kvqq9k7bihizk5lym6jrjwnqpc73nvl")))

