(define-module (crates-io rf du rfdups) #:use-module (crates-io))

(define-public crate-rfdups-0.0.1 (c (n "rfdups") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)))) (h "0hj8kplqszxz3gjfrirzz7hyg5pag398ibc0qx399ia6a14sqy33")))

