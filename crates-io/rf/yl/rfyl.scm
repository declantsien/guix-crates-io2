(define-module (crates-io rf yl rfyl) #:use-module (crates-io))

(define-public crate-rfyl-0.1.0 (c (n "rfyl") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xwac4sh4lf56kvmx1aayp89qhvrzqfna80xaj3ziiv3d22xybrb")))

(define-public crate-rfyl-0.1.1 (c (n "rfyl") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "106dzqq55yv7lp41p770lp7g4ibvh79cg8xchja5rz31y6ad1fks")))

(define-public crate-rfyl-0.2.0 (c (n "rfyl") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "15x6imxmks5vfnl58xqjbcch9yfnf5pca58hy9r20zgbdcvb2wzw")))

(define-public crate-rfyl-0.3.0 (c (n "rfyl") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0g2s17xgzayamg8pbjpmfl3wmjfihv6h9sbii9xlryqrj9f2m62l")))

(define-public crate-rfyl-0.3.1 (c (n "rfyl") (v "0.3.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1wk991jhsj4qmfqjq6s68vr4jfg4j1vvafyhgzghxl41z0zjxvpa")))

