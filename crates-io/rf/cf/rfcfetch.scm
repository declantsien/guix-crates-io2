(define-module (crates-io rf cf rfcfetch) #:use-module (crates-io))

(define-public crate-rfcfetch-0.1.0 (c (n "rfcfetch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8byg65ak1c623cl9lamzszngbmmcjj22jzhr6cfh3nqsvv0fpb")))

