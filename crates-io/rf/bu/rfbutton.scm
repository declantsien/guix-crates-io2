(define-module (crates-io rf bu rfbutton) #:use-module (crates-io))

(define-public crate-rfbutton-0.1.0 (c (n "rfbutton") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wi9wasdnjpgd51zzqcc6kgx71bccdf4q0jy65ml5anwgljiazsj")))

(define-public crate-rfbutton-0.1.1 (c (n "rfbutton") (v "0.1.1") (d (list (d (n "cc1101") (r "^0.1.3") (f (quote ("std"))) (d #t) (k 2)) (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "embedded-hal-bus") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 2)) (d (n "eyre") (r "^0.6.9") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "rppal") (r "^0.17.1") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.176") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xg4820i0dwd54wqsdfkhrdvz90ai4niaz7wrdf3m55ja6s89kqv") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

