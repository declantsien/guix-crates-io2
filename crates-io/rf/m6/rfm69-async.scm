(define-module (crates-io rf m6 rfm69-async) #:use-module (crates-io))

(define-public crate-rfm69-async-0.0.1 (c (n "rfm69-async") (v "0.0.1") (d (list (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-async") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16fs77167fjf9wim4gf1wjd5c29fvxx2scq2n5vsg0qzmb4w3nhw")))

(define-public crate-rfm69-async-0.0.2 (c (n "rfm69-async") (v "0.0.2") (d (list (d (n "embassy-time") (r "^0.1.0") (f (quote ("defmt" "defmt-timestamp-uptime" "unstable-traits" "nightly"))) (o #t) (d #t) (k 0)) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-async") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fm2dz3myfhil10px1k7r2j2adfpi43w96hipwydbqs2a1wx70m1") (s 2) (e (quote (("embassy" "dep:embassy-time"))))))

