(define-module (crates-io rf m6 rfm69) #:use-module (crates-io))

(define-public crate-rfm69-0.1.0 (c (n "rfm69") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1sxng8i6fqb8snzkaf5yj2jx7y4cqsjmfbw9aj6bqgkcgyax39l5") (y #t)))

(define-public crate-rfm69-0.1.1 (c (n "rfm69") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "029mihjfjgsgg694ip52pfkv1089p0xspgil2gsha1faqlph87nr")))

(define-public crate-rfm69-0.2.0 (c (n "rfm69") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0swyaaij3y5z77pyzkgk2hcgap8rb9lh7ck8ryjw4dnhvjyszzr3")))

(define-public crate-rfm69-0.2.1 (c (n "rfm69") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0cv5r99i55m5x4klkb1a0krhki7w3vnx46l06lij1mwylwp96zrc")))

(define-public crate-rfm69-0.3.0 (c (n "rfm69") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1y97m8p3rh9fyg8xi9mnrp1jj2caz04867xgmpcmgwlkbzrlnn7h")))

(define-public crate-rfm69-0.3.1 (c (n "rfm69") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0plila0x327f0md9fj72l9ry76sz1ssi57bpifsfavdy9412w6kq")))

(define-public crate-rfm69-0.4.0 (c (n "rfm69") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0la3499xj190179p67cq4dinx2jd0ymbshv259gvvbbgcc9zwkps")))

(define-public crate-rfm69-0.4.1 (c (n "rfm69") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.1") (d #t) (k 2)))) (h "1qk6qmxs6vrkpmdfv570n87ask53qnjkcribhwypq8mbp4jcagpx")))

(define-public crate-rfm69-0.5.0 (c (n "rfm69") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0w2xagv6w1mw6dlwqxqw5s85df276pa3k0ihnlx1clab81q5ahbv")))

(define-public crate-rfm69-0.6.0 (c (n "rfm69") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-bus") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)))) (h "1i20hpa54334d8aa2279lc7lxyx6j7wfffjzdvvg4cllg5cw3j5j")))

