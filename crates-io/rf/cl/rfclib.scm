(define-module (crates-io rf cl rfclib) #:use-module (crates-io))

(define-public crate-rfclib-0.1.0 (c (n "rfclib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "01yybikg6wkd8c1q2kg6ibyvnpgrwd32w2zd7k70dnzq0jazv88z")))

