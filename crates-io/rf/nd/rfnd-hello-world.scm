(define-module (crates-io rf nd rfnd-hello-world) #:use-module (crates-io))

(define-public crate-rfnd-hello-world-1.0.0 (c (n "rfnd-hello-world") (v "1.0.0") (h "0jwnjl7izdglkg1qnn0f1h1xa1br16my0m2l28xhfc5yhpmdb9ay")))

(define-public crate-rfnd-hello-world-1.0.1 (c (n "rfnd-hello-world") (v "1.0.1") (h "1jab680xxj95v35sy21sbj6yv2p1l7mwh9br6aq9hi583i8c18zk")))

