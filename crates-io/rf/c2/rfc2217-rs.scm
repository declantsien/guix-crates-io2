(define-module (crates-io rf c2 rfc2217-rs) #:use-module (crates-io))

(define-public crate-rfc2217-rs-0.1.0 (c (n "rfc2217-rs") (v "0.1.0") (d (list (d (n "serialport") (r "^4.2.0") (o #t) (k 0)))) (h "13hawbbk6fbsrcy0v62rhvd189v5n4m87llm0l7q62jc1rqhzjjn") (f (quote (("std" "serialport") ("default" "std"))))))

