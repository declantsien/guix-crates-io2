(define-module (crates-io rf c2 rfc2047-decoder) #:use-module (crates-io))

(define-public crate-rfc2047-decoder-0.1.0 (c (n "rfc2047-decoder") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "10jr2rkxx9dvf02g1s38h1pl2dvmvyd56fy6i2nd448sflp825i3")))

(define-public crate-rfc2047-decoder-0.1.1 (c (n "rfc2047-decoder") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "12pfi2nk8h59w85ry0fhdwb02rxw81k7lh885s3hc6w7fxnimglc")))

(define-public crate-rfc2047-decoder-0.1.2 (c (n "rfc2047-decoder") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "0xwxl6ykyfkp476cjfnigm1f36kyfaxsm5k7w9an2i3z72xg5v47")))

(define-public crate-rfc2047-decoder-0.1.3 (c (n "rfc2047-decoder") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jrdvagyf9p2yhxi2g99ii8dfjvbsgxadpndwk87zmm716x5pk62")))

(define-public crate-rfc2047-decoder-0.2.0 (c (n "rfc2047-decoder") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "048f0f3pwwf7b62rrh71w9hv2wghgg1r34dgg8v1skp3980psd0i")))

(define-public crate-rfc2047-decoder-0.2.1 (c (n "rfc2047-decoder") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10alfppkdlgqywzijnfffmsd2pr0va618h4qfz7332gbklyga3bf")))

(define-public crate-rfc2047-decoder-0.2.2 (c (n "rfc2047-decoder") (v "0.2.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0bpc2k7dp3nxc3pnsvz6zd3vc58j8q29nzibn4q3wz49a974pz31")))

(define-public crate-rfc2047-decoder-1.0.0 (c (n "rfc2047-decoder") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "045iv00nbqkkvzlllwksfxvhj4yyvqrg21a8229qpdmfr4icbh27")))

(define-public crate-rfc2047-decoder-1.0.1 (c (n "rfc2047-decoder") (v "1.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qzpmcq62nhg1iv91s0i9w2pcbz3n0pdvxg2yn2731mghxjs9aad")))

(define-public crate-rfc2047-decoder-1.0.2 (c (n "rfc2047-decoder") (v "1.0.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "charset") (r "^0.1") (d #t) (k 0)) (d (n "chumsky") (r "^0.9") (d #t) (k 0)) (d (n "memchr") (r "^2.5") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mbr3z593z9rkhya2sm81ddiwnyamzxw3c2jj0gigiazy49jcdwy")))

(define-public crate-rfc2047-decoder-1.0.5 (c (n "rfc2047-decoder") (v "1.0.5") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "charset") (r "^0.1") (d #t) (k 0)) (d (n "chumsky") (r "^0.9") (d #t) (k 0)) (d (n "memchr") (r "^2.5") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wyc2w6bp91vvq37mbzm971i5n2kighq665f30qjqh9w8s66c2p9")))

