(define-module (crates-io rf c2 rfc2580) #:use-module (crates-io))

(define-public crate-rfc2580-0.1.0 (c (n "rfc2580") (v "0.1.0") (h "0bdamb38pffnw2chk4zvm7xzlch3jz7ya13vq1rnnq3nkm59v0ch")))

(define-public crate-rfc2580-0.1.1 (c (n "rfc2580") (v "0.1.1") (h "1vfv18gbvgj1n0naga5fwyikgnki1s7h6j5mlqw8347m6wg82zpr")))

(define-public crate-rfc2580-0.2.0 (c (n "rfc2580") (v "0.2.0") (h "108ny4l4wywn71dzw005h6vq0jfy9d4z1x0xzn20gwiflg4gy7q2")))

(define-public crate-rfc2580-0.3.0 (c (n "rfc2580") (v "0.3.0") (h "1nkpa9jhkbyyj0ahacd74ci4ws60zbq23farnngdb9svk51i2r7p")))

