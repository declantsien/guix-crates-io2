(define-module (crates-io rf c2 rfc2047) #:use-module (crates-io))

(define-public crate-rfc2047-0.1.0 (c (n "rfc2047") (v "0.1.0") (h "00zzvnn9kx6bpmnfzr0f18phb58sn7rd43pk9c28z8wng7zdla7y")))

(define-public crate-rfc2047-0.1.1 (c (n "rfc2047") (v "0.1.1") (h "05gfrn5w3p4j5yv5ahyz8ac6sagrbgsfvz9002sr43539zgbb1k9")))

