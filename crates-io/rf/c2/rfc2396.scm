(define-module (crates-io rf c2 rfc2396) #:use-module (crates-io))

(define-public crate-rfc2396-1.0.1 (c (n "rfc2396") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.36") (d #t) (k 1)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1xb03i6wfa7c2nj2963azagpnqpjv12x3pjx6plq7w1jgnxdk17r")))

(define-public crate-rfc2396-1.0.2 (c (n "rfc2396") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.36") (d #t) (k 1)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1b8sv6hz8d9gyl22dwb68hpq9myk83i9dsgc47xbnjhmlyw2ifc3")))

(define-public crate-rfc2396-1.0.3 (c (n "rfc2396") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.36") (d #t) (k 1)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1n0dn9hg7ydxz3a6m4l2s20y5a5wh9g10hj0mb0civc3axw0bx1j")))

(define-public crate-rfc2396-1.0.4 (c (n "rfc2396") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.37") (d #t) (k 1)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1sms65v37njncizqrfwjh0ildaf2ky7h7lsvz6bgyjd695m2anaq")))

(define-public crate-rfc2396-1.0.5 (c (n "rfc2396") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0446ij9p1sc128kzvb6p6qbm1281anbk1mly342z472yq5g0s6hj")))

(define-public crate-rfc2396-1.1.0 (c (n "rfc2396") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)))) (h "1yr7p5l5s4sl2g8kj4vp7x84az597ygnj0x57k8n43v9xnxn6hw5")))

