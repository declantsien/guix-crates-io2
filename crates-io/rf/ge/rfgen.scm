(define-module (crates-io rf ge rfgen) #:use-module (crates-io))

(define-public crate-rfgen-0.1.0 (c (n "rfgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "1hcdxlcf9x5yc79kwxl38r6if0n3f0996wsn8nlgyr9k7ql6mm6q")))

(define-public crate-rfgen-0.2.0 (c (n "rfgen") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "1s8h5xjjawddchapd93iqdlzqgxx2wxlp66p6ypfwzsmr78jb1ar")))

