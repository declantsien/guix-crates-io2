(define-module (crates-io rf -c rf-core) #:use-module (crates-io))

(define-public crate-rf-core-0.1.0 (c (n "rf-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "10v3h65cl98nmpkay7gaz31d9l10cllpgvd5bgcd55f50fmjvsqm")))

(define-public crate-rf-core-0.1.1 (c (n "rf-core") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1hwysyqga3cnirsj860rh5qnr14019zjpmf7f64v6f3j796zz5mi")))

(define-public crate-rf-core-0.1.2 (c (n "rf-core") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1x9pwp5h78mlrag3a99nh0kxc3n0rf3qpmikg49mly3a7lr8hci1")))

(define-public crate-rf-core-0.2.0 (c (n "rf-core") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1rzzdx15gxp8q55yql2cd2sclslvy6j2b550zkiqa9dg6bxxs920")))

(define-public crate-rf-core-0.2.1 (c (n "rf-core") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "03ih40fmwnhmg7xib13a5qqm9qk3mqighgdaaylnc678z8g09bb7")))

(define-public crate-rf-core-0.3.0 (c (n "rf-core") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0d777qkgbivf1vpims8xa6ym3vv83fhzxgmmskia18m21syfngmv")))

(define-public crate-rf-core-0.3.1 (c (n "rf-core") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0z5rnqaksf842i6k2p09dkmb1qg383w2fjl9z44k15q8wlh6iiry")))

(define-public crate-rf-core-0.4.0 (c (n "rf-core") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0cr7m83sssfg6sp5q2j5x2acxd1mq5wswq5jkj4jxvn16ybyqvz1")))

(define-public crate-rf-core-0.4.1 (c (n "rf-core") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0g0h3af42h0prlgsn6q8qmjcxfk8ml8avnj28q4kc4zqy6cjyn6h") (y #t)))

(define-public crate-rf-core-0.4.2 (c (n "rf-core") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1na2zj5ivqgii03j37m184z33lj0vvvbf0jyyk93g1j4n80vr44p")))

(define-public crate-rf-core-0.4.3 (c (n "rf-core") (v "0.4.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1gdqz4kylnh6qs9rzz01paiw0yaj2z8a1gcaid48vm3ama6x516k")))

(define-public crate-rf-core-0.5.0 (c (n "rf-core") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1gs1k69mvjq1hh9pqvfd87drvqghmvkxqx5b0bgyymw360cp6afw")))

(define-public crate-rf-core-0.5.1 (c (n "rf-core") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1sdic8x4f6vrj4rng4y0shp99v9f8c825s75124xgjk3zsq85vk4")))

(define-public crate-rf-core-0.5.2 (c (n "rf-core") (v "0.5.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0cdknwqdwjfpn6v1rbwxjwwbv4ak75zq388b8zbihxkfnqnxjrkm")))

(define-public crate-rf-core-0.6.0 (c (n "rf-core") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "16m6za69idz1ph9wwvv3jsqh1470chf5m496am808j49ba98v8c1")))

