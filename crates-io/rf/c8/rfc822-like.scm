(define-module (crates-io rf c8 rfc822-like) #:use-module (crates-io))

(define-public crate-rfc822-like-0.1.0 (c (n "rfc822-like") (v "0.1.0") (d (list (d (n "fmt2io") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0g5id264zc19zmm2i424qsfndy3jy286gcchjs22m7737bj5p55x") (f (quote (("live_test"))))))

(define-public crate-rfc822-like-0.1.1 (c (n "rfc822-like") (v "0.1.1") (d (list (d (n "fmt2io") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0i9j0554bf4pa8z4i9jkxz58x6qiafhrwcs0q418ip1qzy0dhrin") (f (quote (("live_test"))))))

(define-public crate-rfc822-like-0.2.0 (c (n "rfc822-like") (v "0.2.0") (d (list (d (n "fmt2io") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "02wa0d9fz5a3mmfxv74zln3qz41riq7afr7lz0zj9z8jm0n1amls") (f (quote (("live_test"))))))

(define-public crate-rfc822-like-0.2.1 (c (n "rfc822-like") (v "0.2.1") (d (list (d (n "fmt2io") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "126wd43i2p23ivssg89bd74c9hhapw52xqnkpq6899ysj9cm8wr5") (f (quote (("live_test"))))))

