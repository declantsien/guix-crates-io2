(define-module (crates-io rf c8 rfc822_sanitizer) #:use-module (crates-io))

(define-public crate-rfc822_sanitizer-0.1.0 (c (n "rfc822_sanitizer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "01w0ll1g1aymkanad3rjlb1p0jnv612yyq74qj0pfjgbii9sllsi") (y #t)))

(define-public crate-rfc822_sanitizer-0.1.1 (c (n "rfc822_sanitizer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0pwk5ila645gfahxx78mzzpcwhs6cmq3l5j2d455fn9vx5hwvbrg") (y #t)))

(define-public crate-rfc822_sanitizer-0.1.2 (c (n "rfc822_sanitizer") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "080gn5vfhk68b39bi2wpbpisc5nf19jk4q4i8y5npdclzrv4ghys")))

(define-public crate-rfc822_sanitizer-0.2.0 (c (n "rfc822_sanitizer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ls1qh8spd9sckg4gz004yclvljgxp3q08xg92fmbp1ia4d2q2sq")))

(define-public crate-rfc822_sanitizer-0.2.1 (c (n "rfc822_sanitizer") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zi70kk8mq8gqh7rxn9ll4zmdmsj4m4ixww0xh6w5dx38k8kh466")))

(define-public crate-rfc822_sanitizer-0.3.0 (c (n "rfc822_sanitizer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0jg4b355gm47hbpacmd04y8fbpfzxwzls429xw2gk9zlswg2jq5f")))

(define-public crate-rfc822_sanitizer-0.3.1 (c (n "rfc822_sanitizer") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "15hfd12qmmpk83ha5vdp7yzkxbzlk7d4qjf31gk5d5940xn35fp0")))

(define-public crate-rfc822_sanitizer-0.3.2 (c (n "rfc822_sanitizer") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zd6d2jrfp7271gs1jjwbqsxyb1h958rch5mnlxdpmf7q5nklhi8")))

(define-public crate-rfc822_sanitizer-0.3.3 (c (n "rfc822_sanitizer") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "05fqz6alc7503inw7jm3fp4piwj5w0j59v2bvhvgikg0q42q63k8")))

(define-public crate-rfc822_sanitizer-0.3.4 (c (n "rfc822_sanitizer") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1yf4ncb41gz20hx177h5afwqlvzwzx9r8wdsjjxd2hh8har7rr9s")))

(define-public crate-rfc822_sanitizer-0.3.6 (c (n "rfc822_sanitizer") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0i2x9x5wa7j89skj6wl9pdci3ks7czxc5pav0a0hd01mwv06lpnr")))

