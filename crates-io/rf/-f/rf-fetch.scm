(define-module (crates-io rf -f rf-fetch) #:use-module (crates-io))

(define-public crate-rf-fetch-0.1.0 (c (n "rf-fetch") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "passwd") (r "^0.0.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.0.0") (d #t) (k 0)))) (h "00cf9n4rrrwmljcdgvw5h5x49wbjkq3lbh002mwfjgs24nz2hrpk")))

(define-public crate-rf-fetch-0.1.1 (c (n "rf-fetch") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "passwd") (r "^0.0.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.0.0") (d #t) (k 0)))) (h "192s3bbap6n46w6dh0sg2c2467g7wyq3vr69cia9ppyz073hawrq")))

(define-public crate-rf-fetch-1.0.0 (c (n "rf-fetch") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "passwd") (r "^0.0.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "whoami") (r "^1.0.0") (d #t) (k 0)))) (h "0dbviyzz2ps4vcwpg0kiy097dz88af726a7dnb4xxnkmliqwm7q9")))

