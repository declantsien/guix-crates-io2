(define-module (crates-io rf mt rfmt) #:use-module (crates-io))

(define-public crate-rfmt-0.1.0 (c (n "rfmt") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "rsyntax") (r "^1.10.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1c3aa19v8bj35c4fnp3f833k0al0j35n51kw3mdidnzss9hvxylc")))

