(define-module (crates-io rf an rfang) #:use-module (crates-io))

(define-public crate-rfang-0.1.0 (c (n "rfang") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y872s5l493p8mp6jzq8pg0brg7mry1rmm003dfjfwslihj1hhl9")))

(define-public crate-rfang-0.1.1 (c (n "rfang") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jfih2gclang4qxm9b6wr9r4v59wi49xlz3b51bkf87kiy535aar")))

(define-public crate-rfang-0.1.2 (c (n "rfang") (v "0.1.2") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dq4n0zy4ppbs9g1yrr4bqgb8r679hrm9ghik9wiqy0c4l7hpnba")))

(define-public crate-rfang-0.1.3 (c (n "rfang") (v "0.1.3") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cz3dxcfpzgpxzrvgikbd4sgm02m6vfk3s49kx6d2ma39hh9wx5g")))

(define-public crate-rfang-0.1.4 (c (n "rfang") (v "0.1.4") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xlrx6211i0wsrspsc4v81jzdg9i9bxilzyq548xiq6zgccwhh9j")))

(define-public crate-rfang-0.1.5 (c (n "rfang") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rfykkvgwqc8g9rinqj5r2bv1gf42pjmv0wi7y2iyx1bjm8dp4kj")))

