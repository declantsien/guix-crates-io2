(define-module (crates-io rf m9 rfm9x) #:use-module (crates-io))

(define-public crate-rfm9x-0.1.0 (c (n "rfm9x") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1v6gc5y09psvn05lx0bmh5cs6ca06qik81plpnmkr6zy5r8jrvhl")))

(define-public crate-rfm9x-0.1.1 (c (n "rfm9x") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1rf5bvvn6mjs4vpc65lw0r4xk8lsb8nykiljwkmf2sxnsma9vz2g")))

(define-public crate-rfm9x-0.1.2 (c (n "rfm9x") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0xschajw89vg0dsmafr6jx456g66ykv8sk9nmqbdpclip7sfkdl5")))

