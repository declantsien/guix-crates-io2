(define-module (crates-io rf in rfind) #:use-module (crates-io))

(define-public crate-rfind-0.1.0 (c (n "rfind") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1xcbxghvm03x7079l2kzz84d1rnsdxy61vy8j46jzwrk57c5gj1f")))

(define-public crate-rfind-0.2.0 (c (n "rfind") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1qxjlmz1q6bk6by4nsrr4yxvwwkqn7d7fldlkbmbq65wkj5d8l1z")))

