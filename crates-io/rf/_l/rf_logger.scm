(define-module (crates-io rf _l rf_logger) #:use-module (crates-io))

(define-public crate-rf_logger-0.2.0 (c (n "rf_logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0qd4ad8bifmh2vwsd1041q2qmzmvmbcisvzlxd58348avn0p0vja")))

