(define-module (crates-io rf ca rfcalcs) #:use-module (crates-io))

(define-public crate-rfcalcs-0.1.0 (c (n "rfcalcs") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "01w2dnqgf30c55182nry0jwxd6hwf3b48zb9ya5y08f5pbp2cnq0")))

(define-public crate-rfcalcs-0.1.1 (c (n "rfcalcs") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0n16cpd1zxvliv95f0bmrracwglpj42hrdamwxxmspik9p0mqw34")))

