(define-module (crates-io rf ca rfcalc) #:use-module (crates-io))

(define-public crate-rfcalc-0.1.0 (c (n "rfcalc") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y0zflima984y5z8bhaffw2h7dvv4nyrwjxnrgcs7xiqcr1lvw0s")))

(define-public crate-rfcalc-0.1.1 (c (n "rfcalc") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l1vyqh02fmxmlz25kr1glhh0s0ixzdx5sbz9pgj48li9x1ib1zi")))

(define-public crate-rfcalc-0.2.0 (c (n "rfcalc") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wjl86j9j8ppfhwdrls773ja6iydygaq1781j9w7k87a9shds1l4")))

(define-public crate-rfcalc-0.2.1 (c (n "rfcalc") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "03iiwxv60xr99s3mi10pwb1f9ykr6imkfpajh6zg4jvd137gp7w3")))

(define-public crate-rfcalc-0.3.0 (c (n "rfcalc") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0wwfgli90c61ymzh4gcaakiq9kmwrcdhcnhx03b7ws5jq1pgfzvv")))

(define-public crate-rfcalc-0.3.1 (c (n "rfcalc") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1ihl5cd9idx4d27hpr1j3wsa6n7rr7j90n22qhnhcj4as75znb70")))

(define-public crate-rfcalc-0.3.2 (c (n "rfcalc") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0zfah7p6magza1sqi6v0a7k9lhfyargbiy5ddfkcq2rbk72n15z8")))

(define-public crate-rfcalc-0.3.3 (c (n "rfcalc") (v "0.3.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "18f68x02mkn1yz7z54gc7719p1ryzv431g9b5c29s22v6hgf89id")))

