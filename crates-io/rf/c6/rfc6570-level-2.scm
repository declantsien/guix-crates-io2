(define-module (crates-io rf c6 rfc6570-level-2) #:use-module (crates-io))

(define-public crate-rfc6570-level-2-0.1.0 (c (n "rfc6570-level-2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 1)))) (h "0zk3hp13c752zncxdfkvwgzzjd82cxma6kgxa0nmg25ygfpnq74z")))

(define-public crate-rfc6570-level-2-1.0.0 (c (n "rfc6570-level-2") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 1)))) (h "0sak5y8s8qjwaws14n542snjrg0rvpgbjlzlh46d2c9hk6l8jjj4")))

(define-public crate-rfc6570-level-2-1.1.0 (c (n "rfc6570-level-2") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 1)))) (h "15n8jg3v13p84iwlvn5a7w7qnk4mslx8mrf3cd6vdsw6p7yyrzp3")))

(define-public crate-rfc6570-level-2-1.2.0 (c (n "rfc6570-level-2") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1pbfsq6l13yz1jsr9m762nyb3f1pi8lq10k1w5029zvjnjck9gyn")))

