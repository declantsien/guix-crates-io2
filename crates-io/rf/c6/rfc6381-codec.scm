(define-module (crates-io rf c6 rfc6381-codec) #:use-module (crates-io))

(define-public crate-rfc6381-codec-0.1.0 (c (n "rfc6381-codec") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)) (d (n "mp4ra-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "mpeg4-audio-const") (r "^0.2.0") (d #t) (k 0)))) (h "0dw0dpg07h6d5s4rhfdbkjhq608ck8x1ydm5yrbprmghcxmg95a3")))

(define-public crate-rfc6381-codec-0.2.0 (c (n "rfc6381-codec") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "mp4ra-rust") (r "^0.3.0") (d #t) (k 0)) (d (n "mpeg4-audio-const") (r "^0.2.0") (d #t) (k 0)))) (h "1zcp0828j96pc5g6fmsg1mjxbiayqwyk32wrdnmjxj1ybh7w4m7d")))

