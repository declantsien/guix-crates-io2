(define-module (crates-io rf c6 rfc6979) #:use-module (crates-io))

(define-public crate-rfc6979-0.0.0 (c (n "rfc6979") (v "0.0.0") (h "19k6k52jb6dkhv5hral6nnzkkz5fd0x4jvkyic5g3izwi1fm5lnb")))

(define-public crate-rfc6979-0.1.0 (c (n "rfc6979") (v "0.1.0") (d (list (d (n "crypto-bigint") (r "^0.3") (f (quote ("generic-array" "zeroize"))) (k 0)) (d (n "hmac") (r "^0.11") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "09850my3yx9hshwhk4c3kb992r880068kxzcz692ffgnfn2n1vwn") (r "1.56")))

(define-public crate-rfc6979-0.2.0-pre.0 (c (n "rfc6979") (v "0.2.0-pre.0") (d (list (d (n "crypto-bigint") (r "^0.4.0-pre.0") (f (quote ("generic-array" "zeroize"))) (k 0)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1wkfs8w69l8pfl5sw09h0ng5358nzagvdk10k0wrazrnv8pccpvb") (r "1.57")))

(define-public crate-rfc6979-0.2.0 (c (n "rfc6979") (v "0.2.0") (d (list (d (n "crypto-bigint") (r "^0.4") (f (quote ("generic-array" "zeroize"))) (k 0)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1plmmpcazvn3l5ddzdpqcb4xrz64xfa3a7grkb217qaygm1qh1vc") (r "1.57")))

(define-public crate-rfc6979-0.3.0 (c (n "rfc6979") (v "0.3.0") (d (list (d (n "crypto-bigint") (r "^0.4") (f (quote ("generic-array" "zeroize"))) (k 0)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0cpy6b0rvjzajp4wgvxcisdl26x0jaq525c6yi90lhspy2065j48") (r "1.57")))

(define-public crate-rfc6979-0.3.1 (c (n "rfc6979") (v "0.3.1") (d (list (d (n "crypto-bigint") (r "^0.4") (f (quote ("generic-array" "zeroize"))) (k 0)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "zeroize") (r "^1") (k 0)))) (h "1fzsp705b5lhwd2r9il9grc3lj6rm3b2r89vh0xv181gy5xg2hvp") (r "1.57")))

(define-public crate-rfc6979-0.4.0-pre.0 (c (n "rfc6979") (v "0.4.0-pre.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1gghvrr55ibgqg0hahyqz5jrwn0nf38zid7x78vjqnlpip0rad8v") (r "1.61")))

(define-public crate-rfc6979-0.4.0 (c (n "rfc6979") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1chw95jgcfrysyzsq6a10b1j5qb7bagkx8h0wda4lv25in02mpgq") (r "1.61")))

(define-public crate-rfc6979-0.5.0-pre.0 (c (n "rfc6979") (v "0.5.0-pre.0") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.0") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "=0.11.0-pre.0") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1hia4ahzkjiq93kbcvpidh5rllxnbpvvgyyjc15pm699zs6kay5k") (r "1.71")))

(define-public crate-rfc6979-0.5.0-pre.1 (c (n "rfc6979") (v "0.5.0-pre.1") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.1") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "=0.11.0-pre.1") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "0kym55i80qq8nsd5dymiwjqdnm7vq7faacy6y7scyzav4ksy4kzp") (r "1.71")))

(define-public crate-rfc6979-0.5.0-pre.2 (c (n "rfc6979") (v "0.5.0-pre.2") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.2") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "=0.11.0-pre.2") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "0y9ysjb8d7jhy4wzp7q4rxj7jhrac2hj9ifag22m1v48vbv3792w") (r "1.72")))

(define-public crate-rfc6979-0.5.0-pre.3 (c (n "rfc6979") (v "0.5.0-pre.3") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.3") (f (quote ("reset"))) (k 0)) (d (n "sha2") (r "=0.11.0-pre.3") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "19kdx4kbgv9n1j40nw58nhaa4g5jz787yjw3ysi6g53byvr74n84") (r "1.72")))

