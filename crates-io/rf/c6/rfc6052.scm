(define-module (crates-io rf c6 rfc6052) #:use-module (crates-io))

(define-public crate-rfc6052-1.0.0 (c (n "rfc6052") (v "1.0.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0nqkjcky9h2lkpx4i5w4492fqr95f1zzkp6xqsws532r6yiskvsd")))

