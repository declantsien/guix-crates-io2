(define-module (crates-io xv c- xvc-test-helper) #:use-module (crates-io))

(define-public crate-xvc-test-helper-0.3.0 (c (n "xvc-test-helper") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.3") (d #t) (k 0)))) (h "1m5j1bv3x1akmwcnsih9ign7s3h2pl94pyq4gffmcq6cilxvj0i7")))

(define-public crate-xvc-test-helper-0.3.2 (c (n "xvc-test-helper") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.3") (d #t) (k 0)))) (h "07609z9zkbz8hc99m61f1xfjcx9njwmax5zpw87c4p11584jh94v")))

(define-public crate-xvc-test-helper-0.3.3 (c (n "xvc-test-helper") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.3") (d #t) (k 0)))) (h "1g4cg9jijbcgqg6zg2cfgzmf53mrfjxrnhxn0mwpfjb6ydr69dw9")))

(define-public crate-xvc-test-helper-0.4.0 (c (n "xvc-test-helper") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.4.0") (d #t) (k 0)))) (h "18ababsh5ra00f5mlzk8vigr72s6000hcnp48sy0zmm0z7qbp1hn")))

(define-public crate-xvc-test-helper-0.4.2-alpha.0 (c (n "xvc-test-helper") (v "0.4.2-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.4.0") (d #t) (k 0)))) (h "0db0ffx0kazkq9ynwgx6g3y73wcr4nyx36dx8qin3va3pizh5kaw")))

(define-public crate-xvc-test-helper-0.4.2-alpha.6 (c (n "xvc-test-helper") (v "0.4.2-alpha.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.4.2-alpha.6") (d #t) (k 0)))) (h "1msglykgpyq34d6acxghfkj8ajbkjqfb6f1cc4hf80bv3ibhqlhw")))

(define-public crate-xvc-test-helper-0.4.2 (c (n "xvc-test-helper") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.4.2-alpha.6") (d #t) (k 0)))) (h "1iv6hnc1hm4j1blix3mrbrdn4pkhy2pbqbrhkkf5m52jx817glmb")))

(define-public crate-xvc-test-helper-0.5.0 (c (n "xvc-test-helper") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.5.0") (d #t) (k 0)))) (h "0qql8sh4fyfi71xy16lbgarzlsr3h3rpx08ixzal60gm5bky8gql")))

(define-public crate-xvc-test-helper-0.5.2 (c (n "xvc-test-helper") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.5.0") (d #t) (k 0)))) (h "0xykxgrlfj2w7q0kvlxcvg1h8i6v006pj8yinpyx97a8js5riil2")))

(define-public crate-xvc-test-helper-0.6.0 (c (n "xvc-test-helper") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.0") (d #t) (k 0)))) (h "15ax64sx3cpzf5fcax9mxmhlihdzb80iizmbn0926wz7rby81754")))

(define-public crate-xvc-test-helper-0.6.1 (c (n "xvc-test-helper") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.0") (d #t) (k 0)))) (h "0ymci5x0f7vfh41gn2m13pvnxbd8jh3sxzc3v1473kwd7y87f7j4")))

(define-public crate-xvc-test-helper-0.6.3 (c (n "xvc-test-helper") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.0") (d #t) (k 0)))) (h "0bshcjd42z4d713lwf94drwv4g1wxhz953chj3gbwblga0ql06gs")))

(define-public crate-xvc-test-helper-0.6.4-alpha.1 (c (n "xvc-test-helper") (v "0.6.4-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.0") (d #t) (k 0)))) (h "005x9pnar554hniv7iaglbhybiy484n3gv5lvwind30hinzkx36f")))

(define-public crate-xvc-test-helper-0.6.4-alpha.5 (c (n "xvc-test-helper") (v "0.6.4-alpha.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6") (d #t) (k 0)))) (h "189gp0xna5zdxcabf8j7gn2rsdls907g0rybglkd250f1hq9y9qj")))

(define-public crate-xvc-test-helper-0.6.4 (c (n "xvc-test-helper") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6") (d #t) (k 0)))) (h "1la0m0ij2m1dk1kq20wbj86hqfhcgg07syvfvbmcqdw22vcsvcqk")))

(define-public crate-xvc-test-helper-0.6.5 (c (n "xvc-test-helper") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.5") (d #t) (k 0)))) (h "0wi50v17025fxlfzzkzn25qx9413nhqahfqqy0rc5gaqxrjx0qz7")))

(define-public crate-xvc-test-helper-0.6.6-alpha.0 (c (n "xvc-test-helper") (v "0.6.6-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.6-alpha.0") (d #t) (k 0)))) (h "1hm9v9mkmqka9qzi5pxmqcrjh4cnl1g4m8qzbwjl7ar31v27prqh")))

(define-public crate-xvc-test-helper-0.6.6 (c (n "xvc-test-helper") (v "0.6.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xvc-logging") (r "^0.6.6") (d #t) (k 0)))) (h "1pca5dx9zy9mi1h6pqz07hlhqxikmn2y807dp9hg99mssaif8m44")))

