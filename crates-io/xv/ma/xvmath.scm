(define-module (crates-io xv ma xvmath) #:use-module (crates-io))

(define-public crate-xvmath-0.1.0 (c (n "xvmath") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "157x3hn4msq8a6m5ybsjgd5bav1h3q4gy4ivzmlqw5gk1wc6j7gn") (y #t)))

(define-public crate-xvmath-0.1.0-beta.0 (c (n "xvmath") (v "0.1.0-beta.0") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "1fdgiz4wpip8lyyk06pi6s2yhyknxpj8hdq46mqyp60i6cvyij0l") (y #t)))

(define-public crate-xvmath-0.1.0-beta.1 (c (n "xvmath") (v "0.1.0-beta.1") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "1am5k4pyx12lgqpn92cv21z1yi134y32q0p6mq4pv64fkhkkqla9") (y #t)))

(define-public crate-xvmath-0.1.0-beta.2 (c (n "xvmath") (v "0.1.0-beta.2") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "00k28s763lrz1bdbsjaxgx1v969jg6p47nf91hxx5wy09s05yhj8")))

(define-public crate-xvmath-0.1.1 (c (n "xvmath") (v "0.1.1") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "1562b6f0a9mbbcnvk8xkgp6hs81kvann46dhg62cp8wkl3i0g9kk")))

(define-public crate-xvmath-0.1.2 (c (n "xvmath") (v "0.1.2") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "0kl2lc5wqbzfgd8wl9kvqlxd7v1lvhvzm8kr9gzgl17p8blcmfhs")))

(define-public crate-xvmath-0.1.3 (c (n "xvmath") (v "0.1.3") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "15vady9gbnbn70p9ml38xvcfnjhlzc30s7y3lj6cli7qw9ysg9fq")))

(define-public crate-xvmath-0.1.3-1 (c (n "xvmath") (v "0.1.3-1") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "1kz5i1c4yzkmkpwgifxdgyia5iszym2ldlwzrn77c8rnnls0sydm")))

(define-public crate-xvmath-0.1.4 (c (n "xvmath") (v "0.1.4") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)))) (h "0yzfni9ksschmxh1wq5f6ybqk818rnr9vcqjj4gc3m0wadazis94")))

