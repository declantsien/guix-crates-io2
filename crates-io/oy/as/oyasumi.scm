(define-module (crates-io oy as oyasumi) #:use-module (crates-io))

(define-public crate-oyasumi-0.1.0 (c (n "oyasumi") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pv6grg9qhsiqllbncap80xswl6grwh3bvd8pr79mrr7csx3a8hb")))

(define-public crate-oyasumi-0.1.1 (c (n "oyasumi") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q9rhdb5khv3bc7w6xxaam26f4gq5wpjnrfcvxji6bvcmjvjkk5v")))

