(define-module (crates-io oy st oysterpack_app_metadata_macros) #:use-module (crates-io))

(define-public crate-oysterpack_app_metadata_macros-0.1.0 (c (n "oysterpack_app_metadata_macros") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "time"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "oysterpack_app_metadata") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "03rn0faqrscisdnwvv2jlmk9b89pyck066ljmfyslkl9pp7mxbb4")))

