(define-module (crates-io oy st oysterpack_testing) #:use-module (crates-io))

(define-public crate-oysterpack_testing-0.1.0 (c (n "oysterpack_testing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "108v520cw6f3dad9a2ya5f31ff9dfrskn42zcr8gvxrkcw2lh0h9")))

(define-public crate-oysterpack_testing-0.1.1 (c (n "oysterpack_testing") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1wdapi0bwii4vq61ymrcfl3484mwli4k2pismacmzjk7sszx7iim")))

(define-public crate-oysterpack_testing-0.1.2 (c (n "oysterpack_testing") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0xnynphp7gsv5ssi1cvnxflgrw59c98pvw2f8mi35wq6sgy7bx4a")))

