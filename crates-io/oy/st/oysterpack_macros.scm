(define-module (crates-io oy st oysterpack_macros) #:use-module (crates-io))

(define-public crate-oysterpack_macros-0.1.0 (c (n "oysterpack_macros") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0sqrkji3afp7xx6hayn4kbcp4ha10jihlnm62a90f57i0ipvvzm9")))

