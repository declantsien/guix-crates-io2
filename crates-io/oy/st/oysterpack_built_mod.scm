(define-module (crates-io oy st oysterpack_built_mod) #:use-module (crates-io))

(define-public crate-oysterpack_built_mod-0.1.0 (c (n "oysterpack_built_mod") (v "0.1.0") (d (list (d (n "oysterpack_built") (r "^0.2") (d #t) (k 1)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "04ggmcnwcr4h0y3k502gshl3xqmc2ivqsig5qc9pm11rl9jpsjkc") (y #t)))

(define-public crate-oysterpack_built_mod-0.2.2 (c (n "oysterpack_built_mod") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "time"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "oysterpack_built") (r "^0.2") (d #t) (k 1)) (d (n "semver") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08dj4zkc7x59pxrvzkgp8yaki2hm39hk4zi5zhj4hzms4jkf7p6z") (y #t)))

(define-public crate-oysterpack_built_mod-0.2.3 (c (n "oysterpack_built_mod") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "time"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "oysterpack_built") (r "^0.2") (d #t) (k 1)) (d (n "semver") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ljkqnchpv40ln5fxgpd743sc9wr77apnx5ydqvciqjsy23q00gy") (y #t)))

