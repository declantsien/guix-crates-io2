(define-module (crates-io oy st oysterpack_log) #:use-module (crates-io))

(define-public crate-oysterpack_log-0.1.0 (c (n "oysterpack_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "oysterpack_app_metadata") (r "^0.3") (d #t) (k 0)) (d (n "oysterpack_app_metadata_macros") (r "^0.1") (d #t) (k 2)) (d (n "oysterpack_built") (r "^0.3") (d #t) (k 1)) (d (n "oysterpack_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0mgs9q2nxysg3izi0fvbfc7136cqlv42qdjmmhf2idvcs656hb93")))

