(define-module (crates-io ys cl yscl) #:use-module (crates-io))

(define-public crate-yscl-1.0.0 (c (n "yscl") (v "1.0.0") (h "0brjx9g1w8xxc78gg8lhabyijlypp66lndkwk5hcrh3g2ks1h1sd")))

(define-public crate-yscl-1.0.1 (c (n "yscl") (v "1.0.1") (h "1x0b5dig4vjii0x6xkp0sahrx9yfazvkc748iw9nbjgqmav8c0fx")))

(define-public crate-yscl-1.0.2 (c (n "yscl") (v "1.0.2") (h "065nih3mic0nfy65f3dbl275sbxvipmw9v32rfsr7pla46a6h2dw")))

(define-public crate-yscl-1.1.0 (c (n "yscl") (v "1.1.0") (h "0am4zn1ib4w38k3v94wysy6hw76vwg8zd98zcsnz7d9x9in90k32") (y #t)))

(define-public crate-yscl-1.2.0 (c (n "yscl") (v "1.2.0") (h "0snw8l3vhm7r4mfvwzm7az7dm96f1xn28qmgv2wmwl4wklylwq8a")))

