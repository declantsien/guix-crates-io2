(define-module (crates-io ys fe ysfed) #:use-module (crates-io))

(define-public crate-ysfed-0.1.0 (c (n "ysfed") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cocoon") (r "^0.4.1") (d #t) (k 0)))) (h "1z99wmvlm3qw1brf1974hi5k5pqxh37apsah0avcp99nwx1wx7n5")))

(define-public crate-ysfed-0.1.1 (c (n "ysfed") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cocoon") (r "^0.4.1") (d #t) (k 0)))) (h "0bflfxnmmc72nvwyha9lcriy76panw07jfpvpizjh5hkhna8lhgs")))

(define-public crate-ysfed-0.1.2 (c (n "ysfed") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cocoon") (r "^0.4.1") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "0q4kp15y3amacn45hb5rwggb73jnskakdjqh9s22kfrycsm2hn0w")))

