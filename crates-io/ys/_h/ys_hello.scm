(define-module (crates-io ys _h ys_hello) #:use-module (crates-io))

(define-public crate-ys_hello-0.0.3 (c (n "ys_hello") (v "0.0.3") (h "1kcwqg3hscwqpfp8zqnsfaxl26pjqxw90b6ailj4vim6p39kajrb")))

(define-public crate-ys_hello-0.0.4 (c (n "ys_hello") (v "0.0.4") (h "1767kxz3sjikx3v56c8b26fmy1gb2i4hjkn721kpz5byf4ang9n5")))

