(define-module (crates-io ve to vetomint) #:use-module (crates-io))

(define-public crate-vetomint-0.1.1 (c (n "vetomint") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0saxm0vnhpyll0yfhd79h175dbpqx22zjlapsqfm2rb3ygx4ljch")))

(define-public crate-vetomint-0.2.0 (c (n "vetomint") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hlrq41y3hqc5hrkql61m0wrwwwgx89ma00maiah8j9h8ghxs6bj")))

