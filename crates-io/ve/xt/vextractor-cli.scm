(define-module (crates-io ve xt vextractor-cli) #:use-module (crates-io))

(define-public crate-vextractor-cli-0.2.0 (c (n "vextractor-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "vextractor") (r "^0") (d #t) (k 0)))) (h "0ywwgkb3fcdszv9v8rkwh9zjhyzhd9bawga762p3djqscwrxp4zv")))

(define-public crate-vextractor-cli-0.2.1 (c (n "vextractor-cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "vextractor") (r "^0") (d #t) (k 0)))) (h "16vx6r3svvlvrvn6b98bkh704a0vfdbmpnicp2swncgy15v3icfg") (y #t)))

