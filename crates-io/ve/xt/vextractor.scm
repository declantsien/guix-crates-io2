(define-module (crates-io ve xt vextractor) #:use-module (crates-io))

(define-public crate-vextractor-0.1.0 (c (n "vextractor") (v "0.1.0") (h "0f95v1m7cwpa0n5sxdgjjadcwk2j0q94g3nj2qb2sq1qmyw583ia") (y #t)))

(define-public crate-vextractor-0.1.1 (c (n "vextractor") (v "0.1.1") (h "1m06a7qpmqwl6j6bv522z2q9jpikb292a7m0qabnhc5cr33sb2ig") (y #t)))

(define-public crate-vextractor-0.1.2 (c (n "vextractor") (v "0.1.2") (h "1zjnwdwbnglj3xg0gyhsnwcrrbdzkb68kasi2j7yvidwz6cd195c")))

(define-public crate-vextractor-0.2.0 (c (n "vextractor") (v "0.2.0") (h "0645kzcda365dyilz74i33x1vmjrf8jy9saq6263rxyhgpc9qsz9")))

(define-public crate-vextractor-0.2.1 (c (n "vextractor") (v "0.2.1") (h "1x55f3sgmabsym4h7kg0m9lx856w4jmgiavm9sqdhjgqwiwiks9h")))

(define-public crate-vextractor-0.2.2 (c (n "vextractor") (v "0.2.2") (h "07n0scisjpjvfk70x60jnqbbp7l5zi63ilc19wsvdrk1cv1mx51d")))

(define-public crate-vextractor-0.3.0 (c (n "vextractor") (v "0.3.0") (h "1wzdygys12dmiyc9r7zcxawaqf3qid49hc8q9xbwxdyynll4cmbw") (y #t)))

(define-public crate-vextractor-0.3.1 (c (n "vextractor") (v "0.3.1") (h "08ywqn19xy4ica0dm6ld1hyqia84v25r570rfql58vyc2kgkcd2m") (y #t)))

