(define-module (crates-io ve xt vext) #:use-module (crates-io))

(define-public crate-vext-0.1.0 (c (n "vext") (v "0.1.0") (h "1yy50ba5iaysl8gg2jbh255gf7kl85lrc1zigvxgpdp0f1rmarv2") (y #t)))

(define-public crate-vext-0.1.1 (c (n "vext") (v "0.1.1") (h "10q76jg6y8sv6g7gq8cxhx4ff3wdna05m159mpqnbnrvmj2779cb") (y #t)))

(define-public crate-vext-0.1.2 (c (n "vext") (v "0.1.2") (h "1xrpzl2zqa3szcmzbgl3qrl59074dyvk5arqa9v191yzwcqzz3p8") (y #t)))

(define-public crate-vext-0.1.3 (c (n "vext") (v "0.1.3") (h "06drgyphzsyx33dxhkvwikapg5jyjfz5v2g26723n0dxfw9ciaj0") (y #t)))

(define-public crate-vext-0.1.4 (c (n "vext") (v "0.1.4") (h "1igx9ba3xkrjlskv1c1j69jhw3izkmb5j5fzf1x9hl5xq8j9s8vw") (y #t)))

(define-public crate-vext-0.1.5 (c (n "vext") (v "0.1.5") (h "1cjm00wgqjav80zkcg9p678a14fy3yd1vlhpfkignmlmr9fpkhza") (y #t)))

(define-public crate-vext-0.1.6 (c (n "vext") (v "0.1.6") (h "0sy1lskillaqkrdidfx46hn704fsgi8y1f1j15sliilgj920chc6") (y #t)))

(define-public crate-vext-0.1.7 (c (n "vext") (v "0.1.7") (h "17ap50x24ys04ybgyn99m4lf7wkflig451c63gjixylz9v8m462p") (y #t)))

(define-public crate-vext-0.1.8 (c (n "vext") (v "0.1.8") (h "0rv6i5n9wkp7zq6j8jpp5z7nd9miyhjsfhs6c7yrvbi1j0hbvn0f") (y #t)))

(define-public crate-vext-0.1.9 (c (n "vext") (v "0.1.9") (h "0ijh80psba7v0j9r8fy9d79ixmflh1xs8f2cj8g8k3qfdp4gnvsa") (y #t)))

(define-public crate-vext-0.1.10 (c (n "vext") (v "0.1.10") (h "17wa17jn43y2ayg9nzln2nvjrlw31jdfk52v4w21w4iis86ka156") (y #t)))

(define-public crate-vext-0.1.11 (c (n "vext") (v "0.1.11") (h "1z9bxg3mmnzg2c80x3654kl0mdbm55gjka20rf31ab63k4nnfw2x") (y #t)))

(define-public crate-vext-0.1.12 (c (n "vext") (v "0.1.12") (h "1lsik14rrrn4s8phjlszarsr0jf6v0wk2qbxlbfcrmxq7b7m9f4c") (y #t)))

(define-public crate-vext-0.1.13 (c (n "vext") (v "0.1.13") (h "1d3p3a9mr5ss9qxifn2dlv6wdivlhp3pwf89swjprsc04cjn39nm") (y #t)))

(define-public crate-vext-0.1.14 (c (n "vext") (v "0.1.14") (h "0r4l1fglxsn0pkncfk9g7vxz9iqx00fig6ffiqhanygbha9c06gg") (y #t)))

(define-public crate-vext-0.1.15 (c (n "vext") (v "0.1.15") (h "1cx6znjpwpi38c2in53y9g5cqrwzpd0vzcgajzcnjd9185b3w4rw") (y #t)))

(define-public crate-vext-0.1.16 (c (n "vext") (v "0.1.16") (h "0z4imb0pnkxwsrcg2gqsh8hyr1zqg1pmmxhml70az0n5v817ad1v") (y #t)))

(define-public crate-vext-0.1.17 (c (n "vext") (v "0.1.17") (h "0pj4yzcs2cphlc34rpqv96sqwj0hrcs23z2332p60k9aja85xnvh") (y #t)))

(define-public crate-vext-0.1.18 (c (n "vext") (v "0.1.18") (h "0g2m7kl6wpzrrbg9hzfn95sm1cyf7gk7q2k7gkhnjd5374aadszc") (y #t)))

(define-public crate-vext-0.1.19 (c (n "vext") (v "0.1.19") (h "172i1vn8af5xynb04kliy972v55n7qbnrclzsb0pvddcbvcnbfa8") (y #t)))

(define-public crate-vext-0.1.20 (c (n "vext") (v "0.1.20") (h "17n4dfbdg2kpa3w8wfj40jslc9yknks1x6qjngaz8z76k89slx84") (y #t)))

