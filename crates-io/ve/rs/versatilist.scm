(define-module (crates-io ve rs versatilist) #:use-module (crates-io))

(define-public crate-versatilist-0.1.0 (c (n "versatilist") (v "0.1.0") (d (list (d (n "iced") (r "^0.3.0") (f (quote ("glow" "debug"))) (d #t) (k 0)) (d (n "iced_web") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0i6d1zc92225bh4c2jw03mrszqjv77kc56a263cfc15i1af443bf")))

