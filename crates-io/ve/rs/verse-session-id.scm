(define-module (crates-io ve rs verse-session-id) #:use-module (crates-io))

(define-public crate-verse-session-id-1.0.0 (c (n "verse-session-id") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (f (quote ("u64_backend"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jx567hmpzrsywsk1c9ymkkz9d0m2mcphxzxhiwcpfiym02ipkc6")))

(define-public crate-verse-session-id-1.0.1 (c (n "verse-session-id") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (f (quote ("u64_backend"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "012lapg1szpj6wj5s9ywivhqgq3jjrxw76r28zpxamhb5px8jh2b")))

