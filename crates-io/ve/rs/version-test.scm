(define-module (crates-io ve rs version-test) #:use-module (crates-io))

(define-public crate-version-test-0.1.0 (c (n "version-test") (v "0.1.0") (h "04hz7z3viz8wlyk71pm44fffih8l6z7l900mqgj1831mzd5pw13a")))

(define-public crate-version-test-0.1.1-alpha.1 (c (n "version-test") (v "0.1.1-alpha.1") (h "1xrhh0cn00fxfzywgq1fi38sh19lbsw7x216v70d7xjn0xa63k2m")))

