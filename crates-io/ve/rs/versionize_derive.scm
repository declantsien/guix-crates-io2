(define-module (crates-io ve rs versionize_derive) #:use-module (crates-io))

(define-public crate-versionize_derive-0.1.0 (c (n "versionize_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">= 1.0") (d #t) (k 0)) (d (n "quote") (r ">= 1.0") (d #t) (k 0)) (d (n "syn") (r ">= 1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14gcgggvqmyv3yjnnbjg1mq9di5vchjvgfxk1pm859xjgi2kj30w") (y #t)))

(define-public crate-versionize_derive-0.1.1 (c (n "versionize_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r ">= 1.0") (d #t) (k 0)) (d (n "quote") (r ">= 1.0") (d #t) (k 0)) (d (n "syn") (r ">= 1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nl1knf7rkr5n7yj36jh3m9720ranaxqzywhsy39cxdnpdvp70fp") (y #t)))

(define-public crate-versionize_derive-0.1.2 (c (n "versionize_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r ">=1.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lrkf9ziyhggdggz5gcmrdzkkfb2filam4g3jv54rkqndhxrnp2d") (y #t)))

(define-public crate-versionize_derive-0.1.3 (c (n "versionize_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r ">=1.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18n6w8gl81a446rml3kvnhr9dxbm38497gmgj5231bdgwqyjaz7n")))

(define-public crate-versionize_derive-0.1.4 (c (n "versionize_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r ">= 1.0") (d #t) (k 0)) (d (n "quote") (r ">= 1.0") (d #t) (k 0)) (d (n "syn") (r ">= 1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jkb70l1r2lgifqj7il5j83j8s87622hvjx11yjpwrlg57ysj2hl")))

(define-public crate-versionize_derive-0.1.5 (c (n "versionize_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0akxj31r0a00q68zv569lbzh335gw7zm7wgrr3xbhf25pydaxfy9")))

(define-public crate-versionize_derive-0.1.6 (c (n "versionize_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fp0jlqq8y0mn037g45jmqaax3pdm84ya8ij018ws27pgv072j8w")))

