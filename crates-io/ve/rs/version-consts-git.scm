(define-module (crates-io ve rs version-consts-git) #:use-module (crates-io))

(define-public crate-version-consts-git-0.1.0 (c (n "version-consts-git") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6") (k 0)))) (h "1bdi2dphcb16mfafyjc249xvn4192bq0g6w2jf8zkvwm7sppql7h")))

(define-public crate-version-consts-git-0.1.1 (c (n "version-consts-git") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6") (k 0)))) (h "1gahh0scldby6l5b2s5rh6ljj9i20v1bd8f8qw8jkp2bjg744paa")))

(define-public crate-version-consts-git-0.1.2 (c (n "version-consts-git") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6") (k 0)))) (h "1y8n2wpr9zqyzy0csz2xdka5z6ph9a6v7c4kc4pfh4v7ibvaqncl")))

(define-public crate-version-consts-git-0.1.3 (c (n "version-consts-git") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "git2") (r "^0.6") (k 0)))) (h "1yjnyh1703y372p1zmy08xyjxkmpm4avw35b2zwkikbfq50pv2n1")))

(define-public crate-version-consts-git-0.2.0 (c (n "version-consts-git") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "version-consts-git-impl") (r "^0.1") (d #t) (k 0)))) (h "14nyx5pv91jlfd8rp9fs97hkdgmj5vp36w7ymmzqjafb03n9y5ir")))

