(define-module (crates-io ve rs version_check) #:use-module (crates-io))

(define-public crate-version_check-0.1.0 (c (n "version_check") (v "0.1.0") (h "023cxda7iwsjjhdy4gyyc57x064gy8yw6y56f5vxjccg62ssvvgj")))

(define-public crate-version_check-0.1.1 (c (n "version_check") (v "0.1.1") (h "1xlvza5csz63m078yy0qvczdcpg65hjacrwlhd77qzlwabn4qp3w")))

(define-public crate-version_check-0.1.2 (c (n "version_check") (v "0.1.2") (h "1zlafgfqzigigidzgzmfh4c37p8rfrsg2gbjx9npjdlyy85rbcrb")))

(define-public crate-version_check-0.1.3 (c (n "version_check") (v "0.1.3") (h "1786vk8yyb8gqjngyv0acwm28x7xqlw286daq43ihmj7wcbj0xvb")))

(define-public crate-version_check-0.1.4 (c (n "version_check") (v "0.1.4") (h "0lb0pkcpz2wbmsz06j2wb8lpy9lg4iw120pq89apxs4fjr1c45kp")))

(define-public crate-version_check-0.1.5 (c (n "version_check") (v "0.1.5") (h "1pf91pvj8n6akh7w6j5ypka6aqz08b3qpzgs0ak2kjf4frkiljwi")))

(define-public crate-version_check-0.9.0 (c (n "version_check") (v "0.9.0") (h "1dz8say22nl81iyws2h79jns1va0srqh38j7f7zgy4wlzm9xbls5")))

(define-public crate-version_check-0.9.1 (c (n "version_check") (v "0.9.1") (h "1kikqlnggii1rvnxrbls55sc46lxvinz5k3giscgncjj4p87b1q7")))

(define-public crate-version_check-0.9.2 (c (n "version_check") (v "0.9.2") (h "1vbaqdf802qinsq8q20w8w0qn2pv0rkq5p73ijcblrwxcvjp5adm")))

(define-public crate-version_check-0.9.3 (c (n "version_check") (v "0.9.3") (h "1zmkcgj2m0pq0l4wnhrp1wl1lygf7x2h5p7pvjwc4719lnlxrv2z")))

(define-public crate-version_check-0.9.4 (c (n "version_check") (v "0.9.4") (h "0gs8grwdlgh0xq660d7wr80x14vxbizmd8dbp29p2pdncx8lp1s9")))

