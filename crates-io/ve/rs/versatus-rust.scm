(define-module (crates-io ve rs versatus-rust) #:use-module (crates-io))

(define-public crate-versatus-rust-0.1.0 (c (n "versatus-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "ethnum") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "1wjj091xzjabkyirn0wz2w6zz2hl2k22xm6z997phqzxlw0c5agd")))

