(define-module (crates-io ve rs versionisator) #:use-module (crates-io))

(define-public crate-versionisator-1.0.0 (c (n "versionisator") (v "1.0.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "13prha38gila9zirnxdvy6b862dd90fbi7m700qs3s6g3v82b5fv")))

(define-public crate-versionisator-1.0.1 (c (n "versionisator") (v "1.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0m8l4r8vr3s6grjm3ivdlrv2gixv3ls7qsh5yn1f0pcwv3yvgkkr")))

(define-public crate-versionisator-1.0.2 (c (n "versionisator") (v "1.0.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "platforms") (r "^0.2") (d #t) (k 0)))) (h "0zhf5dx7whv6vp5y2jnah1ngr56i6yy0f8c2lrlgvfh7zyjrvxv2")))

(define-public crate-versionisator-1.0.3 (c (n "versionisator") (v "1.0.3") (d (list (d (n "platforms") (r "^1.0") (d #t) (k 0)))) (h "0jgcbjdzz2bkbgk37p0352jhnnam3xlb6z3vh3fy07c99vac9wxw")))

(define-public crate-versionisator-1.1.0 (c (n "versionisator") (v "1.1.0") (d (list (d (n "platforms") (r "^1.0") (d #t) (k 0)))) (h "0lm3s168acbhpxc2jn2pb406lrapqfmschxm2crmp1c6l95v26x1")))

(define-public crate-versionisator-1.2.0 (c (n "versionisator") (v "1.2.0") (d (list (d (n "platforms") (r "^1.0") (d #t) (k 0)))) (h "1j5n88wj2xxjcbqsda531v8apvz9p5wqkgylgn97f2m3w1qrjh0r")))

