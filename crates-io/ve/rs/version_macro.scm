(define-module (crates-io ve rs version_macro) #:use-module (crates-io))

(define-public crate-version_macro-0.1.0 (c (n "version_macro") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)))) (h "1jsw1mqb15q0ivw7ldm7lp06m0pk6crnd3fi5xlhsa2zrjazljvb")))

