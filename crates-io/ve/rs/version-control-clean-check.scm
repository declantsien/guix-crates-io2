(define-module (crates-io ve rs version-control-clean-check) #:use-module (crates-io))

(define-public crate-version-control-clean-check-0.0.0 (c (n "version-control-clean-check") (v "0.0.0") (h "0fjag85shglvacrf367jxa8yysykzmc1cp1m0jimfpmffir4df28")))

(define-public crate-version-control-clean-check-0.1.0 (c (n "version-control-clean-check") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.6") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive" "strum_macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1dylmrgkckz06s3yyncfq7345jzngbqlkvsfz2rxzcykbfln9bnl")))

(define-public crate-version-control-clean-check-0.1.1 (c (n "version-control-clean-check") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.6") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "strum") (r "^0.26.1") (f (quote ("derive" "strum_macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0afdsfdavyqpfm8k067prqb8rh7rg27jalk68ch3pa98kbidfw8x")))

(define-public crate-version-control-clean-check-0.1.2 (c (n "version-control-clean-check") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.6") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "strum") (r "^0.26.1") (f (quote ("derive" "strum_macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0vc54sbsg7nk24lby9wngq03hgcpxwcgr89bayfzdzndy8cmspxn")))

