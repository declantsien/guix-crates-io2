(define-module (crates-io ve rs version-compare) #:use-module (crates-io))

(define-public crate-version-compare-0.0.1 (c (n "version-compare") (v "0.0.1") (h "0kwls64fsgrqxv2rn3qky7a15bdx8mqpvfjbklsfpcq1ip1vc0ma")))

(define-public crate-version-compare-0.0.2 (c (n "version-compare") (v "0.0.2") (h "0vnqsldqw9dfpi5gjckdir3hqrg6ngckhnlarhl7r1laqfd5scfx")))

(define-public crate-version-compare-0.0.3 (c (n "version-compare") (v "0.0.3") (h "1774a6h788smxqgwcd5gyimravxxr9rwmy215mdzz6r56bfn9sf7") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.4 (c (n "version-compare") (v "0.0.4") (h "0blrn4wjlw48rx70d6dv14f9da64kbzrvsx0n8w5wflzd3f6yxnb") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.5 (c (n "version-compare") (v "0.0.5") (h "0y736clzfjmw3m9rw0ligmzf2ppwkjz94b3hjskq0b01j899jcna") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.6 (c (n "version-compare") (v "0.0.6") (h "0drdjjxam0rzfd9i10swaa73zh745wc6fn7s2dyx7r7iigfql1kq") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.7 (c (n "version-compare") (v "0.0.7") (h "0dqi9fsadv68vcp924ny9lwjd40hwmiv3cdi8iclhrqxn8pah033") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.8 (c (n "version-compare") (v "0.0.8") (h "00vn8mh4ndf09cnhxsp6c4jh8860wzp0rd2d0ywp4bj05n7jpp0n") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.9 (c (n "version-compare") (v "0.0.9") (h "0zhc73kvvpqzf4iyg03gqbk4xsfg82czvciqwrdl51kpfvn2jlgb") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.10 (c (n "version-compare") (v "0.0.10") (h "18ack6rx18rp700h1dncljmpzchs3p2dfh76a8ds6vmfbfi5cdfn") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.11 (c (n "version-compare") (v "0.0.11") (h "06v688jg6gd00zvm3cp7qh2h3mz8cs2ngr09bnwxhyddxrcwh60w") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.12 (c (n "version-compare") (v "0.0.12") (h "0s3m0gxmq17wkqkm58xj6b872s6rly7ij23jjygxd6vg3dgrid75") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.0.13 (c (n "version-compare") (v "0.0.13") (h "05igxk04rzwnzj90697vppg10pbn667xvj9dddfc23iif95ziz03") (f (quote (("dev") ("default"))))))

(define-public crate-version-compare-0.1.0 (c (n "version-compare") (v "0.1.0") (h "0wyasmnqqngvm54x0gsxbwpxznvn747jkp0dx1nnppy1j9xj927y")))

(define-public crate-version-compare-0.1.1 (c (n "version-compare") (v "0.1.1") (h "0acg4pmjdbmclg0m7yhijn979mdy66z3k8qrcnvn634f1gy456jp")))

(define-public crate-version-compare-0.2.0 (c (n "version-compare") (v "0.2.0") (h "12y9262fhjm1wp0aj3mwhads7kv0jz8h168nn5fb8b43nwf9abl5") (r "1.56.0")))

