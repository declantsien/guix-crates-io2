(define-module (crates-io ve rs version-consts-git-impl) #:use-module (crates-io))

(define-public crate-version-consts-git-impl-0.1.0 (c (n "version-consts-git-impl") (v "0.1.0") (d (list (d (n "git2") (r "^0.9") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1v1k5gpypn1l8kd7nqnmp329sc7f1qigzhklpwm3m31s24j92a8x")))

(define-public crate-version-consts-git-impl-0.1.1 (c (n "version-consts-git-impl") (v "0.1.1") (d (list (d (n "git2") (r "^0.9") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1j7j98vik5d15h5sxibbx5pbnhp63yvjbhvvrgyj3a26g5jq25q9")))

(define-public crate-version-consts-git-impl-0.1.2 (c (n "version-consts-git-impl") (v "0.1.2") (d (list (d (n "git2") (r "^0.10") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0rbahl2rjlh17dkgf98mz34gahw11pwss3nwnqg37g07a8q4gqxi")))

(define-public crate-version-consts-git-impl-0.1.3 (c (n "version-consts-git-impl") (v "0.1.3") (d (list (d (n "git2") (r "^0.10") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1g25na6x7sqm7w22fm1hh42638f3czs0m3bqq80wrfrmfc02p710")))

