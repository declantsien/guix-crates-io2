(define-module (crates-io ve rs versatiles_derive) #:use-module (crates-io))

(define-public crate-versatiles_derive-0.11.1 (c (n "versatiles_derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0.84") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.36") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1l0jifiwjcrp94wzjl0i3wszl34h6s08iqv0xp5lapng09wb2qrm")))

(define-public crate-versatiles_derive-0.11.2 (c (n "versatiles_derive") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1.0.84") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.36") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0kra0zp91sp040znjxvx16xrq25vw4inqsaqs2kljwyxv1ww5f0k")))

(define-public crate-versatiles_derive-0.11.3 (c (n "versatiles_derive") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1.0.84") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.36") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1bfs9ydyp9f6zh3d7a7ikmrv127k93h90z07jj93rfkazrmvmvay")))

