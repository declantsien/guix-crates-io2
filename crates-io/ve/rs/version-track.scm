(define-module (crates-io ve rs version-track) #:use-module (crates-io))

(define-public crate-version-track-0.1.0 (c (n "version-track") (v "0.1.0") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0bz1b96iqh4z4r90xc0ya5gmywps072g5w6q1vrv2qsfdgnzz6kd")))

