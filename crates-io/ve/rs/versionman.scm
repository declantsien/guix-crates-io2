(define-module (crates-io ve rs versionman) #:use-module (crates-io))

(define-public crate-versionman-0.1.0 (c (n "versionman") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.13.4") (d #t) (k 0)))) (h "1vf3wv5n0xpmgg5bj3iiq1zy4xb4jy63zwdg20ly1nnyr7fa6895")))

(define-public crate-versionman-0.1.1 (c (n "versionman") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.13.4") (d #t) (k 0)))) (h "0fk27bn8hd021ba62vjrrgw80mfz9yqw8qmzw175f5lg5r7wk7xq")))

