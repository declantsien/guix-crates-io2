(define-module (crates-io ve rs version_info) #:use-module (crates-io))

(define-public crate-version_info-0.0.1 (c (n "version_info") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "0vyjz3fkfbkwhax9mmnckmyk0lvxwviyf3ksjdw1yq5vl0v3p46s") (y #t)))

(define-public crate-version_info-0.0.2 (c (n "version_info") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "1w0h36rbsi98mcgjqkmk087jafji5z2pbfzwn571f9a8n0fa00gw") (y #t)))

(define-public crate-version_info-0.0.3 (c (n "version_info") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "07m7300q9fglsfc37n31krch669390q13j41j25vk4wnm1h9hw93") (y #t)))

(define-public crate-version_info-0.0.4 (c (n "version_info") (v "0.0.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "0ws5i0msv50himrcqjsbfjpfzb0zb29rfvvpypnf02i04kar7v9r") (y #t)))

(define-public crate-version_info-0.0.5 (c (n "version_info") (v "0.0.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "1pgdx4f78734fb37j773p1yqdhlv51xx9m22m0l7bdx3hn7ngdvc") (y #t)))

(define-public crate-version_info-0.0.6 (c (n "version_info") (v "0.0.6") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winver"))) (d #t) (k 0)))) (h "16xab6wffz6f01y3wps4q8av8lkxsqp8js8vlk1jhwi1y91wiwqv")))

