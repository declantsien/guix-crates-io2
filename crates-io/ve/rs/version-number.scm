(define-module (crates-io ve rs version-number) #:use-module (crates-io))

(define-public crate-version-number-0.1.0 (c (n "version-number") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yare") (r "^1") (d #t) (k 2)))) (h "0iq17fq6r69k5393a2bdwmnh1bz4gy8z5i0qxkp8lmlpvq4kripv")))

(define-public crate-version-number-0.2.1 (c (n "version-number") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yare") (r "^1") (d #t) (k 2)))) (h "1qff4jg8q4i4nhl5gwqcccns5g3kqf0qwbszyr0bklqlfwbjr3li")))

(define-public crate-version-number-0.2.2 (c (n "version-number") (v "0.2.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yare") (r "^1") (d #t) (k 2)))) (h "003qq07w6wbyv8hli8i97a7j7hfnv94rgc1m36ggw5jm8j3zdkgx")))

(define-public crate-version-number-0.3.0 (c (n "version-number") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "semver") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yare") (r "^1") (d #t) (k 2)))) (h "07fzjlyymf0wx15hlcjb5ln3hffqnafm0dpvw66k5mnk2kdy4kcn")))

(define-public crate-version-number-0.4.0 (c (n "version-number") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "semver") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "yare") (r "^1") (d #t) (k 2)))) (h "0z7vlm5hk3a4v982chmjcc2klgd6dsc1wsrajbmqjxr3pjnayn53") (r "1.56")))

