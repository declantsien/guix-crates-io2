(define-module (crates-io ve rs vers-cli) #:use-module (crates-io))

(define-public crate-vers-cli-0.1.0 (c (n "vers-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_toml") (r "^0.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0knizbi7ifmd9p6jbngycsqwvbz1mdd68had4v7qxrzria89gy4v")))

