(define-module (crates-io ve rs verstau) #:use-module (crates-io))

(define-public crate-verstau-0.0.1 (c (n "verstau") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)))) (h "1pb5xpvq022mjadsm1slllczkam439ylhgqkjgy1zs0df2sisdzw")))

(define-public crate-verstau-0.0.2 (c (n "verstau") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)))) (h "0v01141idw4r8fgllj5q1iliiarrm0kyc7n3czjb30gj4lgya851")))

(define-public crate-verstau-0.0.3 (c (n "verstau") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)))) (h "1qkjrjgbx2n8yawlqnpajsh2la3zj359k9vysknya4cbx9bs5p7b")))

(define-public crate-verstau-0.0.4 (c (n "verstau") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.9") (d #t) (k 0)))) (h "16mx5lyr35qwklwyv9wdfw7cz6gc97h7fa4vm2wvsdqw8qrins8d")))

