(define-module (crates-io ve rs versioned-file) #:use-module (crates-io))

(define-public crate-versioned-file-0.1.0 (c (n "versioned-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 0)))) (h "145gq1y5v08jk8g1n12br55x22hhpw0xi4j18snm9lncgx1ifc8m")))

(define-public crate-versioned-file-0.1.1 (c (n "versioned-file") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 0)))) (h "13gahr48d3klpaldyz2khmqwnkpg4ln3cmi7l6lvik9g0qcf6wvj")))

