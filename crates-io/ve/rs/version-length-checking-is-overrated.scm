(define-module (crates-io ve rs version-length-checking-is-overrated) #:use-module (crates-io))

(define-public crate-version-length-checking-is-overrated-12345678901234567890.12345678901234567890.12345678901234567890 (c (n "version-length-checking-is-overrated") (v "12345678901234567890.12345678901234567890.12345678901234567890") (h "1hwmbk2gyqzfp26ddinx6i7wf8scl0bk90vxmpkxkdjwbx7vdlw9") (y #t)))

(define-public crate-version-length-checking-is-overrated-12345678901234567890.12345678901234567890.12345678901234567891 (c (n "version-length-checking-is-overrated") (v "12345678901234567890.12345678901234567890.12345678901234567891") (h "0gkdmrz9xg6dcwxhqp6qn5abmv0kz44bk0yr18xd7mnn9xjxr70s") (y #t)))

