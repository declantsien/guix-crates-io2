(define-module (crates-io ve rs versa_unstable_schema) #:use-module (crates-io))

(define-public crate-versa_unstable_schema-0.1.0 (c (n "versa_unstable_schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zx1y90a6844dfcxryms7h5wb7v61mvb5q4f8gb5dda4clq4flj5")))

(define-public crate-versa_unstable_schema-0.2.0 (c (n "versa_unstable_schema") (v "0.2.0") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1xd0ajvgpbz356ighj972w55lf4vzdzrxvzhd6bnlahzgblmwjvw")))

(define-public crate-versa_unstable_schema-0.2.1 (c (n "versa_unstable_schema") (v "0.2.1") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "17j3k0jirq3260hab9nviml9pfpmxqnwa2y33gzn57507fg4mmx3")))

(define-public crate-versa_unstable_schema-0.2.2 (c (n "versa_unstable_schema") (v "0.2.2") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0xvid07xcnsr3sqxp466d3kx6q4xiilfyxhn356bkkkxqq14r1qf")))

(define-public crate-versa_unstable_schema-0.2.3 (c (n "versa_unstable_schema") (v "0.2.3") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0d3cjcybfs3zlp1b70hfbigi296mj88hdkfgx0i75sid2ca26yq0")))

(define-public crate-versa_unstable_schema-0.2.4 (c (n "versa_unstable_schema") (v "0.2.4") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "05hhdvk2km9wbxbxif79w861fzydn5wk398nhdvc2mf4i92nws65")))

(define-public crate-versa_unstable_schema-0.2.5 (c (n "versa_unstable_schema") (v "0.2.5") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "18j6ad0wvq3rb5aiwidywika9p5nri8lq7j4vpdzjjqwh0ywsycl")))

