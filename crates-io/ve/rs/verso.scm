(define-module (crates-io ve rs verso) #:use-module (crates-io))

(define-public crate-verso-0.1.0 (c (n "verso") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11yiazbbplxdzapjzm88n0h1mka85hf3vjhx2i06fm6f1c0gl5z8")))

(define-public crate-verso-0.1.1 (c (n "verso") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bzbscfr87sn0zlmvwqhy5k3rm04w3sfi7pk86la6nlykazi7jlh")))

(define-public crate-verso-0.1.2 (c (n "verso") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11xmpwp8k837cb1dnqk8y0srazknpcp6413j611jql4xal29jp6c")))

(define-public crate-verso-0.2.0 (c (n "verso") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07n0vn52j8hqklv9vy0ximj89i11nhjcg2m3rl6z2i6cy2gdkzay")))

