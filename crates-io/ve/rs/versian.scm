(define-module (crates-io ve rs versian) #:use-module (crates-io))

(define-public crate-versian-0.1.0 (c (n "versian") (v "0.1.0") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "rust-apt") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1504qfs5a7fdr44bszkdlym5v88b575mdyvqc29s41bvg4gzh2wa") (f (quote (("cmp" "rust-apt"))))))

