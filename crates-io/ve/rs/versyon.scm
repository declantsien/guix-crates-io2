(define-module (crates-io ve rs versyon) #:use-module (crates-io))

(define-public crate-versyon-0.0.1 (c (n "versyon") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1ln9cww7ajnnrh6srvqhcdzcvfv46q6njyx41b27pd1pkpbdnxk2") (y #t)))

(define-public crate-versyon-0.0.3 (c (n "versyon") (v "0.0.3") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0r35q2sb1a33faqr6s1ir7l4rhlsjnq2sc4pphh082b9g8bqaqky")))

(define-public crate-versyon-0.0.4 (c (n "versyon") (v "0.0.4") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1i9rd8sq74a61v3j54yf7q04gkml92hd4i90c3yk29lwxdd8gmyc")))

(define-public crate-versyon-0.0.5 (c (n "versyon") (v "0.0.5") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0j94qfa2g05zkdawb1yd6dry19sbg7bar934hkmspmjd54fbsxqd")))

(define-public crate-versyon-0.0.13 (c (n "versyon") (v "0.0.13") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kkaap0w23j3xnjdlk1z2b8ijrkk470cm7nha8ycf2lb9karpdqy")))

(define-public crate-versyon-0.0.14 (c (n "versyon") (v "0.0.14") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0pg116b43bp1iv884prkrfvfklh6x0xx0mzw6c1izvhhlg8sal4v")))

