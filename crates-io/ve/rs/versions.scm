(define-module (crates-io ve rs versions) #:use-module (crates-io))

(define-public crate-versions-1.0.0 (c (n "versions") (v "1.0.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1bxhmlp3z6j8l60n2jj1phm0zan8ykrz484rwnz80fif3njndhsk")))

(define-public crate-versions-1.0.1 (c (n "versions") (v "1.0.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "031znr9992rzcr2kcz37ni0k1sxpq6y1yzdy17r99m7ncc5jbjwl")))

(define-public crate-versions-2.0.0 (c (n "versions") (v "2.0.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1lffpgqa61svfb5z96f5g1gdb07nv47jlv3rkwggfh7l30d4b2wp")))

(define-public crate-versions-2.0.1 (c (n "versions") (v "2.0.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0q472f4kzsmfs62pz1m3r1b1b9p2dr7vg4s7jq5sjbx3wsvd6c72")))

(define-public crate-versions-2.0.2 (c (n "versions") (v "2.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1499anxrmh4xaygxbnsk9p01faijdhq3lvni9ynhdjahldx98vxv")))

(define-public crate-versions-2.1.0 (c (n "versions") (v "2.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1p6vwfas4wmf1shw1bw2rmp4d3690ngwr1al5747pzh292mi9fag")))

(define-public crate-versions-3.0.0 (c (n "versions") (v "3.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ii65i3b2dkh0gri6cm9fnw1y73s94gy72rsb01q0rwr2rj46nvv")))

(define-public crate-versions-3.0.1 (c (n "versions") (v "3.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qdimx0gqmyybg2j2qpynq2zw202glv01zh0x4chdrhgwphayq4c")))

(define-public crate-versions-3.0.2 (c (n "versions") (v "3.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ffjw6mbsm3nrgg03b76jfc41lg44yz1pyqmv6zj37q88i6y4wri")))

(define-public crate-versions-3.0.3 (c (n "versions") (v "3.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1gica6drgqlcn5kx90i594zb93mvxksdz8xynspgbnj55jiagnaw")))

(define-public crate-versions-4.0.0 (c (n "versions") (v "4.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1m04qvig0av1fz36082kyaln5rlndzrj70g0az6f3pck2wanq9ym")))

(define-public crate-versions-4.1.0 (c (n "versions") (v "4.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1lv50vzdp8jfd4n6zmaasqyindj2ny8pd80j758zp4ymggcy35zf")))

(define-public crate-versions-5.0.0 (c (n "versions") (v "5.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "00idpf5f4zdlhj53jfja8d1k0q6vg2fwmqxjnvjyz5g4j7mv7zij")))

(define-public crate-versions-5.0.1 (c (n "versions") (v "5.0.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1p9ldgq2v55b5b671sw0ncbnnpr286gy74zfzd8ry0z38jy3cfn7")))

(define-public crate-versions-6.0.0 (c (n "versions") (v "6.0.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1hw245a0vcc1l3mgk3iqsdc99knlg79g1l69a0w8w9832p473hnp")))

(define-public crate-versions-6.1.0 (c (n "versions") (v "6.1.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1iprsrxnq6k8wjvh8rr3201dml5xq0kyp7nx95w79fimk64z8zzk")))

(define-public crate-versions-6.2.0 (c (n "versions") (v "6.2.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0nxcqiqh0b89394144kkl5gcyvg4kl5yf8300x468yqnilgr7a1q")))

