(define-module (crates-io ve rs versatiles_geometry) #:use-module (crates-io))

(define-public crate-versatiles_geometry-0.11.3 (c (n "versatiles_geometry") (v "0.11.3") (d (list (d (n "anyhow") (r "^1.0.86") (f (quote ("std"))) (k 0)) (d (n "byteorder") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "log") (r "^0.4.21") (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "tokio") (r "^1.37.0") (k 2)) (d (n "versatiles_core") (r "^0") (d #t) (k 0)))) (h "18mx8gdcp89s3q68s51n2pbby8jwfy0zrnbxqjg4yh6in1kpcvzz") (f (quote (("default"))))))

