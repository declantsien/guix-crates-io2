(define-module (crates-io ve rs version-rs) #:use-module (crates-io))

(define-public crate-version-rs-0.1.0 (c (n "version-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1aybw4g9k4ww9h8jmajw1dib88hbhlgvnacj9x5kxprysazzbzv2")))

(define-public crate-version-rs-0.1.1 (c (n "version-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0675knxf6kry5681dray3hlsqkz0x4yxq7ag8w5w0kvxs52f1839") (f (quote (("serde-derive" "serde/derive") ("default"))))))

(define-public crate-version-rs-0.2.0 (c (n "version-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1d1i4ihq2n878p02wi1427nqx32f4d1zgqms5jcwyh81j539qvm6") (f (quote (("serde-derive" "serde/derive") ("lossy") ("default"))))))

