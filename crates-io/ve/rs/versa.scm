(define-module (crates-io ve rs versa) #:use-module (crates-io))

(define-public crate-versa-0.1.0 (c (n "versa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "proptest") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zl493v37xzfb5lz8mh0r3s7m9mnzyj2p48s1lmarzzbiiss029n") (f (quote (("test_utils" "proptest") ("default")))) (r "1.64")))

