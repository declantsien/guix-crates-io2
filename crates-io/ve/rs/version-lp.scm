(define-module (crates-io ve rs version-lp) #:use-module (crates-io))

(define-public crate-version-lp-0.1.6 (c (n "version-lp") (v "0.1.6") (d (list (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 0)))) (h "021cha7zgvzflikaywrzf67rhnfc6avin2ib2l0q4sr4ijavlxc2")))

(define-public crate-version-lp-0.2.0 (c (n "version-lp") (v "0.2.0") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 0)))) (h "1n045sw7481qhfqy2gja399hbv2j7cc2c3l2wcl6gpyw1brg7s0p")))

(define-public crate-version-lp-0.2.1 (c (n "version-lp") (v "0.2.1") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 0)))) (h "16i50nw6sa4cbrc5yijncm01vnmr84ac0il5g2mx0771k3x9iq6r")))

