(define-module (crates-io ve rs version-bump) #:use-module (crates-io))

(define-public crate-version-bump-0.1.0 (c (n "version-bump") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0qrf1xrn36ppq6wqp418kgk7bhqdndpshgj3dhkd81ms1xlc00z7") (y #t)))

(define-public crate-version-bump-0.2.0 (c (n "version-bump") (v "0.2.0") (h "05rsk3kdhf2zlhmc08cxawf49sj783ixwh73zh7kw3kw5l3gvxhl")))

