(define-module (crates-io ve rs version) #:use-module (crates-io))

(define-public crate-version-1.0.0 (c (n "version") (v "1.0.0") (h "1qbin2fjv6x0nnphakkcfkfn6spx8pzaqi6a99r34rscvxv9cq42")))

(define-public crate-version-1.1.0 (c (n "version") (v "1.1.0") (h "0cyhi7pmad8l09aancv5ivg2s8n7ydwkba784prywb1y901nzdd7")))

(define-public crate-version-2.0.0 (c (n "version") (v "2.0.0") (h "17fdqsqs7k63x1igkvsfss1j4xbrhbw37ylb5xsghzby4j95d2r4")))

(define-public crate-version-2.0.1 (c (n "version") (v "2.0.1") (h "09pyh3z3qpml4adkn2f2gad70i2llicmrj7was0n7bwv90ai552c")))

(define-public crate-version-3.0.0 (c (n "version") (v "3.0.0") (h "0rg0ihhbwkn5j5mv12yjks1cixhh2mn3wsim071gq574zrj90i1s")))

