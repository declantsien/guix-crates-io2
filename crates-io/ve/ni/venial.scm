(define-module (crates-io ve ni venial) #:use-module (crates-io))

(define-public crate-venial-0.1.0 (c (n "venial") (v "0.1.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 2)))) (h "12kc5zga5y4xg875jvk258wqylxgvryh2ssirz7fa08n77zj6k1d")))

(define-public crate-venial-0.1.1 (c (n "venial") (v "0.1.1") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 2)))) (h "09rxsb8v30694a8qinqz1zpjgsxwbdih1xl60mlkv2gjppw947ng")))

(define-public crate-venial-0.2.1 (c (n "venial") (v "0.2.1") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 2)))) (h "1khcn9jgh7z4k7rqh79li3v7f8qs0am45sh18g8nbnvq0j1sz89r")))

(define-public crate-venial-0.3.0 (c (n "venial") (v "0.3.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "visit"))) (d #t) (t "cfg(fuzzing)") (k 2)))) (h "0mbirh51khbmvnhq7w1094yw41l3ii8xj0gfwdi30jmkcb5vnzlj")))

(define-public crate-venial-0.4.0 (c (n "venial") (v "0.4.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 2)))) (h "02fnyji0ls5vadnz241rhzr2s9wqaw4pcdy63fbj7xgbn3a27mzk")))

(define-public crate-venial-0.5.0 (c (n "venial") (v "0.5.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 2)))) (h "1cjbvzrpc8rdk7jy79ap20jkm12hjpmm5j7w4mdppy8nbcr4ln31")))

(define-public crate-venial-0.6.0 (c (n "venial") (v "0.6.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 2)))) (h "0gv12b86b4xrq3jdjfwj5xyiigy70s2dw16v78dxvy0bycrbq5k8")))

