(define-module (crates-io ve nt vent) #:use-module (crates-io))

(define-public crate-vent-0.1.0 (c (n "vent") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("serde" "v4" "v5"))) (d #t) (k 0)))) (h "1ym7787imvlp9rfdbczpy12va4fpdsgaw0s4zgr3054rpvpih2gs")))

