(define-module (crates-io ve nt vents) #:use-module (crates-io))

(define-public crate-vents-0.1.0 (c (n "vents") (v "0.1.0") (d (list (d (n "refs") (r "^0.1.3") (d #t) (k 0)))) (h "09fhmcjl15xdg2cwk02cb21fafd7fxjsqk9qcdnh9zf45552p47s")))

(define-public crate-vents-0.1.1 (c (n "vents") (v "0.1.1") (d (list (d (n "refs") (r "^0.1.3") (d #t) (k 0)))) (h "0ki4v0z6qn2vl57bd0v5q0jl5dgmfbq8v7n64yz2a42hcffyw7b6")))

(define-public crate-vents-0.1.2 (c (n "vents") (v "0.1.2") (d (list (d (n "refs") (r "^0.1.5") (d #t) (k 0)))) (h "0yn0lwh76p8waklr447fpd8irgqlv5ldc96sqyn3w6g6hs7is5ak")))

(define-public crate-vents-0.1.3 (c (n "vents") (v "0.1.3") (d (list (d (n "refs") (r "^0.1.5") (d #t) (k 0)))) (h "0qv2hx85x5b5fiwiw802bjcb7ax18hljyysx2jhclvx9nxf2wqix")))

(define-public crate-vents-0.1.4 (c (n "vents") (v "0.1.4") (d (list (d (n "refs") (r "^0.1.5") (d #t) (k 0)))) (h "04yywsxfkvn6vam5p4z5msjlsgwbjlsxaxdqids7y6i9rwvmhfmp")))

(define-public crate-vents-0.1.5 (c (n "vents") (v "0.1.5") (d (list (d (n "refs") (r "^0.1.12") (d #t) (k 0)))) (h "13wlgicgd8d0q2j6iykw9i9av72id3ypd07dkh43nygwlfs345p6")))

(define-public crate-vents-0.1.6 (c (n "vents") (v "0.1.6") (d (list (d (n "refs") (r "^0.1.25") (d #t) (k 0)))) (h "17na468wpdggp5g8ipbx4wcfd1vfvkbkf4ks0wys0lm07sf5paba")))

(define-public crate-vents-0.1.7 (c (n "vents") (v "0.1.7") (d (list (d (n "refs") (r "^0.1.28") (d #t) (k 0)))) (h "1sk1xswqfdwswm7vaxqfv0ynj69lcz095hmxfhsdx8wgaqisip5c")))

(define-public crate-vents-0.1.8 (c (n "vents") (v "0.1.8") (d (list (d (n "refs") (r "^0.1.29") (d #t) (k 0)))) (h "1ijvn1nvxvd1xsig5nkcpci7arklzap2cz5xf3bqhgfkhwrgzyh8")))

(define-public crate-vents-0.1.9 (c (n "vents") (v "0.1.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ph136cgqpgyg7rn17wka4rhp5s6qy9mghl29ckb3lxc8fab0gzr")))

(define-public crate-vents-0.1.10 (c (n "vents") (v "0.1.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "151zf9yw1kzqbm7pc79wy7r2jsa2ai0wrhgqlnfilrdimv365fd3")))

(define-public crate-vents-0.1.11 (c (n "vents") (v "0.1.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16plm2mikh4f671jr7r9fdrws6gqm34mzkzr4ymp824jfcp6wd2k")))

(define-public crate-vents-0.2.0 (c (n "vents") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0va1a88j4pyqkqzlhncadjn12v3xvlcpbm4734a4rj47x8mryidy")))

(define-public crate-vents-0.2.1 (c (n "vents") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dn8csxzm88fixv7qcapfhfw2ifxcmk5z2zcyjv7czpidlx9j2n0")))

(define-public crate-vents-0.3.0 (c (n "vents") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q1rvnij729zphzl7phpwm9afsh2y8i4ihgi0fz0dkmbnxkrvrmm")))

(define-public crate-vents-0.4.0 (c (n "vents") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kxdrvq0jdmqwhwi53bvp4n9lsfdnn92q8gmaz18v6780kr3xxia")))

(define-public crate-vents-0.5.0 (c (n "vents") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16r2fgmd67zp1gm389bzw05wz3n311lmd1v67d5rfj2l3mia1nl9")))

(define-public crate-vents-0.5.1 (c (n "vents") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1yqqblcnmnzabqjchjdn2d164x9fza1amz8cm2a8n6a6y7lm9j")))

