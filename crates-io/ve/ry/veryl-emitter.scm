(define-module (crates-io ve ry veryl-emitter) #:use-module (crates-io))

(define-public crate-veryl-emitter-0.1.0 (c (n "veryl-emitter") (v "0.1.0") (d (list (d (n "veryl-parser") (r "^0.1.0") (d #t) (k 0)))) (h "1h51gcn1jpkp9dy1cic3klymjszlzcfpwm07ps0dp4958brjidw0")))

(define-public crate-veryl-emitter-0.1.1 (c (n "veryl-emitter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.1") (d #t) (k 0)))) (h "1d39pj41bwhja1lkb83921ydfp59jkaymkp91hrisai6p49w8qr0")))

(define-public crate-veryl-emitter-0.1.2 (c (n "veryl-emitter") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.2") (d #t) (k 0)))) (h "0fkngr081izg4jqa2p1n7wwnzxr6bdirpr96p9kd4rrz42yvdccq")))

(define-public crate-veryl-emitter-0.1.3 (c (n "veryl-emitter") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.3") (d #t) (k 0)))) (h "09dg27pbbpzbmixh92kv74yznbwm2p5hdzyqnb38g2xm5py9qlzg")))

(define-public crate-veryl-emitter-0.1.6 (c (n "veryl-emitter") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.6") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.6") (d #t) (k 0)))) (h "0j37f4ivzgr0hvfvcrhc1g73rfw7wc0prcwk8asvc1bnlxnldx97")))

(define-public crate-veryl-emitter-0.1.7 (c (n "veryl-emitter") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.7") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.7") (d #t) (k 0)))) (h "1azpy92x3pj4i4rkwq1gqhjd6nqwkr760fms3r535xahdwcp6zn8")))

(define-public crate-veryl-emitter-0.1.8 (c (n "veryl-emitter") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.8") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.8") (d #t) (k 0)))) (h "1xidn9qjb165pxzzq71lrns0d3cz525p11nq9y2956rl5zq16smp")))

(define-public crate-veryl-emitter-0.1.9 (c (n "veryl-emitter") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.9") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.9") (d #t) (k 0)))) (h "0arx44v0n62gha8mx0h3a77xyb5khs44ssyh3wyvakz3bylnjyf9")))

(define-public crate-veryl-emitter-0.1.10 (c (n "veryl-emitter") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.10") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.10") (d #t) (k 0)))) (h "0jaf9mg30xqyiwj4hry78d2bbs5h58acbxcnm288c6imz8zwdk9l")))

(define-public crate-veryl-emitter-0.1.11 (c (n "veryl-emitter") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.11") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.11") (d #t) (k 0)))) (h "1hyad6xmb5qp4x8srapgad67fsz82mryni1hqy547jiqy9h4j7mv")))

(define-public crate-veryl-emitter-0.1.12 (c (n "veryl-emitter") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.12") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.12") (d #t) (k 0)))) (h "120qzm4b4l45k8mfi77zmfp6fsw9cvnq4nigzn22wswg6bq77czh")))

(define-public crate-veryl-emitter-0.1.13 (c (n "veryl-emitter") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.13") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.13") (d #t) (k 0)))) (h "0im5wlsh3ws67bfpbvbcp2dkwm1y1g4xl21dvximbmczy5518525")))

(define-public crate-veryl-emitter-0.1.14 (c (n "veryl-emitter") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.1.14") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.14") (d #t) (k 0)))) (h "013ziwsbca4207jmjix07bl6843l1m3asr1w1gw55nazd6vban1g")))

(define-public crate-veryl-emitter-0.2.0 (c (n "veryl-emitter") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.2.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.2.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.0") (d #t) (k 0)))) (h "1zbb5fm5rf50k4pl4hkvxvgvvnr0vf557c1xcd7cdi0dfd55bc14")))

(define-public crate-veryl-emitter-0.2.1 (c (n "veryl-emitter") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.2.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.2.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.1") (d #t) (k 0)))) (h "075y4kxmd1q6w4rnc4n4p8243mbqba2gr9ml2c1rj84prk6888kj")))

(define-public crate-veryl-emitter-0.2.2 (c (n "veryl-emitter") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.2.2") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.2.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.2") (d #t) (k 0)))) (h "0bmlkwxrkbv3a0andp3hlf53gf1lsahlh2rvdav1cb1pvdp5amd2")))

(define-public crate-veryl-emitter-0.3.0 (c (n "veryl-emitter") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.3.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.3.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.0") (d #t) (k 0)))) (h "0kja5s2paki3mj3nf0vjwkvk8ayfhzwg1z60yp8iy51svlzaq225")))

(define-public crate-veryl-emitter-0.3.1 (c (n "veryl-emitter") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.3.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.3.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.1") (d #t) (k 0)))) (h "0v6dvqas1viivwpv84bmrbi8v6067bas59zlg6jq83r7craik2jv")))

(define-public crate-veryl-emitter-0.3.2 (c (n "veryl-emitter") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.3.2") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.3.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.2") (d #t) (k 0)))) (h "1csksvbvflm46ppp78an09ywyfcxgbsg8dqf1v3hps076k5wsn1m")))

(define-public crate-veryl-emitter-0.3.3 (c (n "veryl-emitter") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.3.3") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.3.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.3") (d #t) (k 0)))) (h "0l2fqhzaya3j3ha3rizf04zkx4qx0xczs3kqr6w6826np812qd2i")))

(define-public crate-veryl-emitter-0.3.4 (c (n "veryl-emitter") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.3.4") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.3.4") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.4") (d #t) (k 0)))) (h "0rhm8w3kxyfcifm9ddnsxzvx6jq1yly74w5bdhac1qz223ml64sf")))

(define-public crate-veryl-emitter-0.4.0 (c (n "veryl-emitter") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.4.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.4.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.4.0") (d #t) (k 0)))) (h "0pn9ym36ysdxhbmd06jdcnkr7sp8mcc2zm5jzjalv6paznjpdxd8")))

(define-public crate-veryl-emitter-0.5.0 (c (n "veryl-emitter") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.0") (d #t) (k 0)))) (h "14chv885kvy0pyy8z5na5nzbiqg2fx2ga5qjr91nac8qj12j330y")))

(define-public crate-veryl-emitter-0.5.1 (c (n "veryl-emitter") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.1") (d #t) (k 0)))) (h "0598afk9pnr3snrx3lxgkdwk870sx5c60nhh1s31lcmav8m3qif7")))

(define-public crate-veryl-emitter-0.5.2 (c (n "veryl-emitter") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.2") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.2") (d #t) (k 0)))) (h "1xc8139qaxib8m9ypxssanrhia04nd2mr2zdfaxbd8hiik0i09fc")))

(define-public crate-veryl-emitter-0.5.3 (c (n "veryl-emitter") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.3") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.3") (d #t) (k 0)))) (h "1rrv7h20fkqygprl2jvps4lp2w4vs8pxj4g9bldfrmjckrbmp4mx")))

(define-public crate-veryl-emitter-0.5.4 (c (n "veryl-emitter") (v "0.5.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.4") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.4") (d #t) (k 0)))) (h "16sd6mk86amgr7r1gghkcmrxdd2pvvslg42rl6hxhcz3jbcw6sbl")))

(define-public crate-veryl-emitter-0.5.5 (c (n "veryl-emitter") (v "0.5.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.5") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.5") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.5") (d #t) (k 0)))) (h "1bhx2adhappw1mdgfffj08y3qjj2gma7sgcyszhbl2n6sj55db88")))

(define-public crate-veryl-emitter-0.5.6 (c (n "veryl-emitter") (v "0.5.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.5.6") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.5.6") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.6") (d #t) (k 0)))) (h "02gsd5rh2anp2b375dwvm1bg31whv54pvbalm2y4smzl6q7n8688")))

(define-public crate-veryl-emitter-0.6.0 (c (n "veryl-emitter") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.6.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.6.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1sg9fa39fi5h00nkar0xb0qhhshlxn255ibhng3vqs4gbysb0v5l")))

(define-public crate-veryl-emitter-0.7.0 (c (n "veryl-emitter") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.7.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.7.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.0") (d #t) (k 0)))) (h "08pjifd9lgfyfm0qligiwy6c2rvnwry3bfdxc8qdwislir6aqmp7")))

(define-public crate-veryl-emitter-0.7.1 (c (n "veryl-emitter") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.7.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.1") (d #t) (k 0)))) (h "1plwhijyy124gg9v4jz8j1qwjd891svrw00j4w4w43h88ryb5c0m")))

(define-public crate-veryl-emitter-0.7.2 (c (n "veryl-emitter") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.7.2") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.7.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.2") (d #t) (k 0)))) (h "1x0a7274a18zqkxv9y8p7v9p1f8pvlr41si3s7iqqq8bihd9imcq")))

(define-public crate-veryl-emitter-0.8.0 (c (n "veryl-emitter") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.8.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.0") (d #t) (k 0)))) (h "1827l0yhngy19b9gfx46imqryqiz4xmksnimwalpcm18f6kiz8d7")))

(define-public crate-veryl-emitter-0.8.1 (c (n "veryl-emitter") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.8.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.8.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.1") (d #t) (k 0)))) (h "179r8y9mmdz85b3w1sd282qci7n3gbclp1si9dpz4n2qaa30svsf")))

(define-public crate-veryl-emitter-0.8.2 (c (n "veryl-emitter") (v "0.8.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.8.2") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.2") (d #t) (k 0)))) (h "00lnqqkkh3azxsxa0lr46yjyb2bh9vpral1h9lyh7js5a280lwd2")))

(define-public crate-veryl-emitter-0.9.0 (c (n "veryl-emitter") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.9.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.9.0") (d #t) (k 0)))) (h "0g79l1m958p483nbdii8lqyr7207v0chwqqcp6sn3xd6y27dl14g")))

(define-public crate-veryl-emitter-0.10.0 (c (n "veryl-emitter") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.10.0") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.0") (d #t) (k 0)))) (h "0gifw0p9ayafvpa2psk23qq1y4yi7ialc1q0pff4ap5sk01g0lf9")))

(define-public crate-veryl-emitter-0.10.1 (c (n "veryl-emitter") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-analyzer") (r "^0.10.1") (d #t) (k 0)) (d (n "veryl-metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.1") (d #t) (k 0)))) (h "13m38wc4w4b1p22q3s7n6cfqbbxhwrhc3hbkb5qsj088grjwh317")))

