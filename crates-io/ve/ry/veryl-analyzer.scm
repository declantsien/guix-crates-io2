(define-module (crates-io ve ry veryl-analyzer) #:use-module (crates-io))

(define-public crate-veryl-analyzer-0.1.0 (c (n "veryl-analyzer") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.0") (d #t) (k 0)))) (h "14vxr7xhwbfdir5c8fdxp59r1c2kssip7sdxymdfrwq72z7w74d4")))

(define-public crate-veryl-analyzer-0.1.1 (c (n "veryl-analyzer") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.1") (d #t) (k 0)))) (h "0xk74vgwlf546xkw3j55ag5fipcybmwmc11irfwq3nkjclilby1i")))

(define-public crate-veryl-analyzer-0.1.2 (c (n "veryl-analyzer") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.2") (d #t) (k 0)))) (h "0f1havncv9g206llpg2bfcbvsqh3cyw2n8lgk2943mkb53d8s88h")))

(define-public crate-veryl-analyzer-0.1.3 (c (n "veryl-analyzer") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.3") (d #t) (k 0)))) (h "0fxgqk6h0x76ba11mgxxq7dz0b68fqipmi48my6pcw5aqnwrcik0")))

(define-public crate-veryl-analyzer-0.1.4 (c (n "veryl-analyzer") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.4") (d #t) (k 0)))) (h "13b32vy3rj5cw9r65q17ni8wvi3a0nai6dlh4pdgvv9l4h0bb5sa")))

(define-public crate-veryl-analyzer-0.1.6 (c (n "veryl-analyzer") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.6") (d #t) (k 0)))) (h "1lansfm52lydp0kyabfba6hg4dma372lnk2dk4aph7hchsl9s7ls")))

(define-public crate-veryl-analyzer-0.1.7 (c (n "veryl-analyzer") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.7") (d #t) (k 0)))) (h "1j11b8nhjnjx8hhnwd6agc4ny1c2rg83nspgq0qc64zj49g75kc6")))

(define-public crate-veryl-analyzer-0.1.8 (c (n "veryl-analyzer") (v "0.1.8") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.8") (d #t) (k 0)))) (h "0lcap4f82lzhvc24w4g4dp2bs1r29sl63qjy04jhdgf03v7v61av")))

(define-public crate-veryl-analyzer-0.1.9 (c (n "veryl-analyzer") (v "0.1.9") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.9") (d #t) (k 0)))) (h "0p13qm0b7iviz2xs4yz2rj07zvhdxwizzb662lpilypi69vfgz0c")))

(define-public crate-veryl-analyzer-0.1.10 (c (n "veryl-analyzer") (v "0.1.10") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.10") (d #t) (k 0)))) (h "15n05r2jpr1pkb13riqkg31j7k3ki2w5aaf8kgq34vb6iqghkzbh")))

(define-public crate-veryl-analyzer-0.1.11 (c (n "veryl-analyzer") (v "0.1.11") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.11") (d #t) (k 0)))) (h "17893s8wf4ihapnp4r8vqaygi1dw3kjd6nis3ciw7c2xlpj3gc8d")))

(define-public crate-veryl-analyzer-0.1.12 (c (n "veryl-analyzer") (v "0.1.12") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.12") (d #t) (k 0)))) (h "1ral1cfdww1gzbzdzjgmam6bffw30vgkg2hy6zkwgc7lj0i0hz6n")))

(define-public crate-veryl-analyzer-0.1.13 (c (n "veryl-analyzer") (v "0.1.13") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.13") (d #t) (k 0)))) (h "10haizajdmbz7jyaij3r8895nm7mzqpw17df6z0qlw01vq4r5ig7")))

(define-public crate-veryl-analyzer-0.1.14 (c (n "veryl-analyzer") (v "0.1.14") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.14") (d #t) (k 0)))) (h "0qwqkkdn745fh7cv95a0mb7zcqz2p676cwvyw9zk180cpg5d9v9a")))

(define-public crate-veryl-analyzer-0.2.0 (c (n "veryl-analyzer") (v "0.2.0") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0087l6yyf89ih9kh6bkckf8ckpdgmgkq647arhhr85d1sixs1xyc")))

(define-public crate-veryl-analyzer-0.2.1 (c (n "veryl-analyzer") (v "0.2.1") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.1") (d #t) (k 0)))) (h "08bbvixllw3ma3qfj7jvmwivlnsj9bd0b204j9xkzlla41n212c7")))

(define-public crate-veryl-analyzer-0.2.2 (c (n "veryl-analyzer") (v "0.2.2") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.2") (d #t) (k 0)))) (h "0fhm088cw2wivb3hazs2vqmcvk7wrbxrb0j529rsv5ailvxahx3a")))

(define-public crate-veryl-analyzer-0.3.0 (c (n "veryl-analyzer") (v "0.3.0") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.0") (d #t) (k 0)))) (h "0g2jpx5gfqfj30lxnfdi5qfws3lpw9mxgphhlracas3cfq2018il")))

(define-public crate-veryl-analyzer-0.3.1 (c (n "veryl-analyzer") (v "0.3.1") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.1") (d #t) (k 0)))) (h "1hx0ncyzval6mx5qh601m976vipd06yvx2m4d3i602qhp4xwxaj1")))

(define-public crate-veryl-analyzer-0.3.2 (c (n "veryl-analyzer") (v "0.3.2") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.2") (d #t) (k 0)))) (h "0rbcasjrwmix1s7bg3nyn52af78dg4gx2wdls1k0p0sd39djz3g9")))

(define-public crate-veryl-analyzer-0.3.3 (c (n "veryl-analyzer") (v "0.3.3") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.3") (d #t) (k 0)))) (h "0sa9pyzpah5jsi0kyx63yxz62xisfxrs1b28p1q61rj84ffi2w97")))

(define-public crate-veryl-analyzer-0.3.4 (c (n "veryl-analyzer") (v "0.3.4") (d (list (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.4") (d #t) (k 0)))) (h "1s9g0xry63364as5j7y3mwcii525kga0xig9ybx7mjaz7vdcaz9r")))

(define-public crate-veryl-analyzer-0.4.0 (c (n "veryl-analyzer") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.4.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.4.0") (d #t) (k 0)))) (h "1rdgmvgyphlcvmy86cwgvvn88jkzsdarb1b4qwlify6as8h42mqx")))

(define-public crate-veryl-analyzer-0.5.0 (c (n "veryl-analyzer") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.0") (d #t) (k 0)))) (h "133gvi3s099vh2wa4kxcjaasa5db969m0m04m2li58wf722k71a3")))

(define-public crate-veryl-analyzer-0.5.1 (c (n "veryl-analyzer") (v "0.5.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.1") (d #t) (k 0)))) (h "18pkkzyvjp7bpwlbryk3nf331ws7pgcz8siwb3n7djgja68g3cxl")))

(define-public crate-veryl-analyzer-0.5.2 (c (n "veryl-analyzer") (v "0.5.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.2") (d #t) (k 0)))) (h "1z3brrfzr7hii1viigh1796yy35g7s9cy58cc37pd35hlcxdynq4")))

(define-public crate-veryl-analyzer-0.5.3 (c (n "veryl-analyzer") (v "0.5.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.3") (d #t) (k 0)))) (h "0z1shlxlvvywyi02mr60nn7p4isly86kaj6cx1c5x6nsnknz5js8")))

(define-public crate-veryl-analyzer-0.5.4 (c (n "veryl-analyzer") (v "0.5.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.4") (d #t) (k 0)))) (h "0i6accna82n0yc1hjzz1l30rlrvxxw34kaar171hw535ib3f4lmz")))

(define-public crate-veryl-analyzer-0.5.5 (c (n "veryl-analyzer") (v "0.5.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "miette") (r "^5.5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.5") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.5") (d #t) (k 0)))) (h "10q1s5bmwwnrbghlcblnyhf37fcqf98x34ghzckj922vvrr407w5")))

(define-public crate-veryl-analyzer-0.5.6 (c (n "veryl-analyzer") (v "0.5.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.5.6") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.6") (d #t) (k 0)))) (h "0qbx3aqqv3nm33vg7z0826gzx0v5viccbr2ccllm73p5y465xrjz")))

(define-public crate-veryl-analyzer-0.6.0 (c (n "veryl-analyzer") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.6.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.6.0") (d #t) (k 0)))) (h "18ga1xvbqhqzxg176ahbbhh9b2qbqwpa2dc2hqgq9acp0w908m1p")))

(define-public crate-veryl-analyzer-0.7.0 (c (n "veryl-analyzer") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.7.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1nww3sxjj8k2l9904kir4y7bldw2ick0qyf0f8gbqs8psy8hzvq4")))

(define-public crate-veryl-analyzer-0.7.1 (c (n "veryl-analyzer") (v "0.7.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.1") (d #t) (k 0)))) (h "12lg44gd1jkqvj13bfrb1lyxfmpbs0z10gphwifrxvnchrp918sb")))

(define-public crate-veryl-analyzer-0.7.2 (c (n "veryl-analyzer") (v "0.7.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.7.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.2") (d #t) (k 0)))) (h "1ya723h6xkd23vngcd425vg0lyiii8jpq2n8xs1ia15msjdiink9")))

(define-public crate-veryl-analyzer-0.8.0 (c (n "veryl-analyzer") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.0") (d #t) (k 0)))) (h "1kbv44qym5hlrwffm9969jcfkr6msv539fa2p8wmz2291cmbmn7c")))

(define-public crate-veryl-analyzer-0.8.1 (c (n "veryl-analyzer") (v "0.8.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.8.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.1") (d #t) (k 0)))) (h "02l1qxnjrm2xqk2fns7cj4sm7lsw1xzcnw7r7snhgw0rmkrddzjp")))

(define-public crate-veryl-analyzer-0.8.2 (c (n "veryl-analyzer") (v "0.8.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.2") (d #t) (k 0)))) (h "1xs21c5fl8mchxms5fa74fdwpzb90m10ij6a4hfl6cpyn9jli7ah")))

(define-public crate-veryl-analyzer-0.9.0 (c (n "veryl-analyzer") (v "0.9.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.9.0") (d #t) (k 0)))) (h "0plncvidf4hgq9bkbzn7mwk4c5f2lfms7k74f649ja798zrpvs2j")))

(define-public crate-veryl-analyzer-0.10.0 (c (n "veryl-analyzer") (v "0.10.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.0") (d #t) (k 0)))) (h "0188sck0cv7vbpban3b66mdkwc7srjw2chl5zkwagsgs6ggnlqv8")))

(define-public crate-veryl-analyzer-0.10.1 (c (n "veryl-analyzer") (v "0.10.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strnum_bitwidth") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 2)) (d (n "veryl-metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.1") (d #t) (k 0)))) (h "17scxb035wdvhld3v72x967appkis1rh9frwdbbndqam6k5pv0a8")))

