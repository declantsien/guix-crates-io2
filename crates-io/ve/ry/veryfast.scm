(define-module (crates-io ve ry veryfast) #:use-module (crates-io))

(define-public crate-veryfast-0.1.0 (c (n "veryfast") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)))) (h "0xjlqjp69f105z2klj6nprr1p3rr3dbc7yn6x751vf25jfc500j7")))

(define-public crate-veryfast-0.1.1 (c (n "veryfast") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)))) (h "0qa1f6pgwx9014n4qzki8qrir5mrfbi6ybl3kacabwr2702py39m")))

(define-public crate-veryfast-0.1.2 (c (n "veryfast") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 2)))) (h "1x2kik3bcrg1y74dr1mazn64slwpadw42pcfh5mmc9hfmhn3i0hp")))

(define-public crate-veryfast-0.2.0 (c (n "veryfast") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 2)))) (h "1d7yy0zkqnwax9qrdbw2ywawb3h5hkmvbgpj4lrlikl8znq3p652")))

(define-public crate-veryfast-0.3.0 (c (n "veryfast") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.8") (d #t) (k 2)))) (h "1afcg4vwk6vs1j10jlwgky6xjpy0fr68i99jrdprvzy1bpbk6bdc")))

(define-public crate-veryfast-0.3.1 (c (n "veryfast") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.8") (d #t) (k 2)))) (h "07yqi8bmh0v508xqfy2m4ki26zv1w5k5diaic7y9lw7kpqg28fmn")))

