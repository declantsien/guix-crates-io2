(define-module (crates-io ve ry very_simple_2d_core) #:use-module (crates-io))

(define-public crate-very_simple_2d_core-0.1.0 (c (n "very_simple_2d_core") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0hg02jsx7zqzmsrydwjsf4qyilr3nal8rp7yaa1maclwnzwpdhbp")))

(define-public crate-very_simple_2d_core-0.2.0 (c (n "very_simple_2d_core") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "18jbxplzbq5qmhq48r9gqjbsd2sysla2fsrzz5gidr2rgxwyyll4")))

(define-public crate-very_simple_2d_core-0.2.1 (c (n "very_simple_2d_core") (v "0.2.1") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1r78ngaask5p4bpzs20ywr6xwpfma7ja1kry5qm00fjdav440zfq")))

(define-public crate-very_simple_2d_core-0.3.0 (c (n "very_simple_2d_core") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0vvgiv9p9pqc87kqmlppjcd4i0pb8yb3bdv76k5mrjrhga78hv8g")))

(define-public crate-very_simple_2d_core-0.3.1 (c (n "very_simple_2d_core") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "12h4lpxmkjq80lc4sxrzx5p29hl2irr4d90xg2hiaylkf6gzqain")))

(define-public crate-very_simple_2d_core-0.3.2 (c (n "very_simple_2d_core") (v "0.3.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0jcnlpiac6iig1c81jzd08x2gqxwq104cyi1j5ry69hfgs93rn0r")))

(define-public crate-very_simple_2d_core-0.4.0 (c (n "very_simple_2d_core") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1h13zvh49499n65isjisnvdbiz71244i74chmds8ra9dlaczmwj8")))

(define-public crate-very_simple_2d_core-0.4.1 (c (n "very_simple_2d_core") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "12kg8zv1rf00ih0ydbx84czfjlgl27qhxymqvdx5pvgds1d4kkg6")))

(define-public crate-very_simple_2d_core-0.5.0 (c (n "very_simple_2d_core") (v "0.5.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1bv9ryhannm6f9g8s9ggfn38wr78j69lalwqmbv6yilwhh74i0bg")))

(define-public crate-very_simple_2d_core-0.5.1 (c (n "very_simple_2d_core") (v "0.5.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0lal3iaf0f8by3xmp9nv1k7ij3ykdk10sd3g131qwkmc263f1b6h")))

(define-public crate-very_simple_2d_core-0.6.0 (c (n "very_simple_2d_core") (v "0.6.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0p52pr8b9664f8sqsa6irpx8545lj5rh295cywxaj2k0gnsp3304")))

