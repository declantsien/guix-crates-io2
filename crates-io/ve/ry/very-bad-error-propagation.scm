(define-module (crates-io ve ry very-bad-error-propagation) #:use-module (crates-io))

(define-public crate-very-bad-error-propagation-0.1.0 (c (n "very-bad-error-propagation") (v "0.1.0") (h "1k3z0937cxf33f883hbj0smmkwaj921lfppk2qv1fdclma658m9k")))

(define-public crate-very-bad-error-propagation-0.1.1 (c (n "very-bad-error-propagation") (v "0.1.1") (h "1vn3cp3jwbf6mfnw1p1icd262j06z17i3br7fxymmlj9ki9942cs")))

