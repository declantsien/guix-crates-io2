(define-module (crates-io ve ry veryfi) #:use-module (crates-io))

(define-public crate-veryfi-1.0.0 (c (n "veryfi") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.0.0") (d #t) (k 0)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.1") (d #t) (k 0)))) (h "1pwxm1vbrgpx6anjg74mqrrr8lck9hg4azg6l9g4mknlp48xwsnm")))

