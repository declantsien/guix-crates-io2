(define-module (crates-io ve ry very_simple_2d) #:use-module (crates-io))

(define-public crate-very_simple_2d-0.1.0 (c (n "very_simple_2d") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.1.0") (d #t) (k 0)))) (h "0sjylw6vskhnc0gnn23mfk64fxm80i9f78vbyjkj37nc347mb5b4")))

(define-public crate-very_simple_2d-0.1.1 (c (n "very_simple_2d") (v "0.1.1") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.1.0") (d #t) (k 0)))) (h "15gx4w30rj4ywj92d8jbzqvld009hy13vj7rgfb4vba5rmi25arx")))

(define-public crate-very_simple_2d-0.1.2 (c (n "very_simple_2d") (v "0.1.2") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.1.0") (d #t) (k 0)))) (h "06cyr17182d71gcc3lham5l1p406f22ff3vp991zmiif0016ycp1")))

(define-public crate-very_simple_2d-0.2.0 (c (n "very_simple_2d") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.1.0") (d #t) (k 0)))) (h "1qdf8iyg50mh93hgl93s8r2x7pizpjx3w9sshzkq9sm1nc50isdd")))

(define-public crate-very_simple_2d-0.2.1 (c (n "very_simple_2d") (v "0.2.1") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.2.0") (d #t) (k 0)))) (h "07n141dz6zyzxmbwxk3bvrld0fqa60lky5l9cx7rzm3am8c5zmbh")))

(define-public crate-very_simple_2d-0.2.2 (c (n "very_simple_2d") (v "0.2.2") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.2.1") (d #t) (k 0)))) (h "1krq474n49ppqr1sfi1nkm5mwcb13knq6qaybqy904cagr5i8142")))

(define-public crate-very_simple_2d-0.3.0 (c (n "very_simple_2d") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.3.0") (d #t) (k 0)))) (h "1flv647jdhmrm9z3aj58l9c32583ql08qar00as978j3g1ddgj94")))

(define-public crate-very_simple_2d-0.3.1 (c (n "very_simple_2d") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.3.1") (d #t) (k 0)))) (h "1lfb2sx5hpm3xb47zi0ki452g34pzxglr4yvjvkvimy5sj7pd5g0")))

(define-public crate-very_simple_2d-0.3.2 (c (n "very_simple_2d") (v "0.3.2") (d (list (d (n "axgeom") (r "^1.4") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.3.1") (d #t) (k 0)))) (h "0mqzyp00gaq5ag8xml9qcacfdbhlnk34jz6ibg72y2bjjdn6rsmb")))

(define-public crate-very_simple_2d-0.3.3 (c (n "very_simple_2d") (v "0.3.3") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.3.2") (d #t) (k 0)))) (h "1a2zi5sl458hd6lfmd8dd97n06l0m0d2mcanf4bcdx4skplv55kp")))

(define-public crate-very_simple_2d-0.4.0 (c (n "very_simple_2d") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.4.0") (d #t) (k 0)))) (h "08mav76hchf61ly4zmlcv52dlkax27bqrn2rnjgq2mnwig6pfzmn") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.4.1 (c (n "very_simple_2d") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.4.0") (d #t) (k 0)))) (h "0gk9z4igcxbq1bgv7ypqhmpsvlrfdvja857nfvi2nxz1cad561zp") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.4.2 (c (n "very_simple_2d") (v "0.4.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.4.0") (d #t) (k 0)))) (h "1bf8q10mvhpxry7wbrmcmgj30w0zwlbs354nxlggrg3acz9w64sq") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.4.3 (c (n "very_simple_2d") (v "0.4.3") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.4.1") (d #t) (k 0)))) (h "0g4xpqfmznf7kdvanmr8l6kxfhvwzxhyhpcgxgz65275sdxp9c81") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.5.0 (c (n "very_simple_2d") (v "0.5.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.5.0") (d #t) (k 0)))) (h "1c33r7vqka3rk1j4q16sgi2nc058mdlsbd7kkhyxhlhgd6bb7n3i") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.5.1 (c (n "very_simple_2d") (v "0.5.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.5.1") (d #t) (k 0)))) (h "0zc5w6h4j64wq2m2qpi0h632mn3pbjfq4ha9mzsj3619v2hq1r0l") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-0.6.0 (c (n "very_simple_2d") (v "0.6.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)) (d (n "very_simple_2d_core") (r "^0.6.0") (d #t) (k 0)))) (h "1xf20nc33744n6k6b03sv4jxxa6qsf9bxs8fcmkyk2z926pc8imw") (f (quote (("fullscreen"))))))

(define-public crate-very_simple_2d-1.0.0 (c (n "very_simple_2d") (v "1.0.0") (h "0yp5jlk46pnnssash34l30vd7j8b0rd8a0d9f5dk4k7w5s37br0y")))

