(define-module (crates-io ve ry veryl-formatter) #:use-module (crates-io))

(define-public crate-veryl-formatter-0.1.0 (c (n "veryl-formatter") (v "0.1.0") (d (list (d (n "veryl-parser") (r "^0.1.0") (d #t) (k 0)))) (h "1zh6rc6ma6cmjd3fxvx2ky4gm4w3mdrjn9hqvkl4nsj2qz1brs5h")))

(define-public crate-veryl-formatter-0.1.1 (c (n "veryl-formatter") (v "0.1.1") (d (list (d (n "veryl-metadata") (r "^0.1.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.1") (d #t) (k 0)))) (h "1a4vc518dx2vzic1jpvirn5zf0r1xm36yy5jjphx9xs3w2qaccj3")))

(define-public crate-veryl-formatter-0.1.2 (c (n "veryl-formatter") (v "0.1.2") (d (list (d (n "veryl-metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.2") (d #t) (k 0)))) (h "1kr21km0pzzk1nm6d503bmhv02d9prg5fril252s5460i33m02s9")))

(define-public crate-veryl-formatter-0.1.3 (c (n "veryl-formatter") (v "0.1.3") (d (list (d (n "veryl-metadata") (r "^0.1.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.3") (d #t) (k 0)))) (h "17bvr51w2jhnydkbp2z2xxh8rh6bq4zkj6a3iq5mvjm8yfvvs4b1")))

(define-public crate-veryl-formatter-0.1.6 (c (n "veryl-formatter") (v "0.1.6") (d (list (d (n "veryl-metadata") (r "^0.1.6") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.6") (d #t) (k 0)))) (h "11qyzy9bn7i9wm530c1qbisq1znxhg7575wkl4rc6rgfa5pnbh68")))

(define-public crate-veryl-formatter-0.1.7 (c (n "veryl-formatter") (v "0.1.7") (d (list (d (n "veryl-metadata") (r "^0.1.7") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.7") (d #t) (k 0)))) (h "0j3y8gk01r2vs1dhkidrfzijkipvpan1qx4i11v0hssad0ipfig8")))

(define-public crate-veryl-formatter-0.1.8 (c (n "veryl-formatter") (v "0.1.8") (d (list (d (n "veryl-metadata") (r "^0.1.8") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.8") (d #t) (k 0)))) (h "0cdfq2c28qcy84k6hvrm4limcafnvldlvpgb681h9fqjch9zl5xi")))

(define-public crate-veryl-formatter-0.1.9 (c (n "veryl-formatter") (v "0.1.9") (d (list (d (n "veryl-metadata") (r "^0.1.9") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.9") (d #t) (k 0)))) (h "1i22jccshnfzs91y7377zp6zzzk1yj6xqb0m2ndmgxr4jz2qqkq0")))

(define-public crate-veryl-formatter-0.1.10 (c (n "veryl-formatter") (v "0.1.10") (d (list (d (n "veryl-metadata") (r "^0.1.10") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.10") (d #t) (k 0)))) (h "1028zjdv6cdyyakpaaghk27zgc9rgvfwbn2sz2giyjvm0pv1y69c")))

(define-public crate-veryl-formatter-0.1.11 (c (n "veryl-formatter") (v "0.1.11") (d (list (d (n "veryl-metadata") (r "^0.1.11") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.11") (d #t) (k 0)))) (h "0i6lz5wf9a7r2j60jd7v6fhv538ykgscfvr3smqf5c8hhii3ak4c")))

(define-public crate-veryl-formatter-0.1.12 (c (n "veryl-formatter") (v "0.1.12") (d (list (d (n "veryl-metadata") (r "^0.1.12") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0vhly32jxpgkvlwcss4gfh64mrgk437jbvyaya7gj5qcbw2q7fkc")))

(define-public crate-veryl-formatter-0.1.13 (c (n "veryl-formatter") (v "0.1.13") (d (list (d (n "veryl-metadata") (r "^0.1.13") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.13") (d #t) (k 0)))) (h "1h9r7jx95lccbj9z2qdz9jbafimbny5iv0vm2mlcyzk4gr12206k")))

(define-public crate-veryl-formatter-0.1.14 (c (n "veryl-formatter") (v "0.1.14") (d (list (d (n "veryl-metadata") (r "^0.1.14") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.1.14") (d #t) (k 0)))) (h "07m51qyrwnwrmhvd6vwj0biqhc36p53km4jcx3xkrfjaxryyhmxv")))

(define-public crate-veryl-formatter-0.2.0 (c (n "veryl-formatter") (v "0.2.0") (d (list (d (n "veryl-metadata") (r "^0.2.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0jjs1w96fc7kx2p5gh9lw8zzb7j085krm91kk8dv0555rdc0q4xs")))

(define-public crate-veryl-formatter-0.2.1 (c (n "veryl-formatter") (v "0.2.1") (d (list (d (n "veryl-metadata") (r "^0.2.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.1") (d #t) (k 0)))) (h "1jyzjv6m3lb3cb3nnkb506lpfmfbjpvj1xw0f5y566a5xj94fbrj")))

(define-public crate-veryl-formatter-0.2.2 (c (n "veryl-formatter") (v "0.2.2") (d (list (d (n "veryl-metadata") (r "^0.2.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.2.2") (d #t) (k 0)))) (h "0k2m99rka8jqy3mwxyal3dsqs27r07v2r133gh5f96cr322r6jw0")))

(define-public crate-veryl-formatter-0.3.0 (c (n "veryl-formatter") (v "0.3.0") (d (list (d (n "veryl-metadata") (r "^0.3.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.0") (d #t) (k 0)))) (h "1w1wmhg6h8krvhi087g3prfa7xav2lmxjhmv9f5giiqbjv8k9vf0")))

(define-public crate-veryl-formatter-0.3.1 (c (n "veryl-formatter") (v "0.3.1") (d (list (d (n "veryl-metadata") (r "^0.3.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.1") (d #t) (k 0)))) (h "0d6dym289g3sgkvaiv3lxizm846bb9h2ssrzqrk7bc1v461nls70")))

(define-public crate-veryl-formatter-0.3.2 (c (n "veryl-formatter") (v "0.3.2") (d (list (d (n "veryl-metadata") (r "^0.3.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.2") (d #t) (k 0)))) (h "024gmqp9ayz7192biny6k2m4x7ql2lkvnlkzmxhv68ms1iswazkr")))

(define-public crate-veryl-formatter-0.3.3 (c (n "veryl-formatter") (v "0.3.3") (d (list (d (n "veryl-metadata") (r "^0.3.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.3") (d #t) (k 0)))) (h "0mdksvnca1dlxsglxysinsvpjbs02sq9x9k9ma1y5gaky8rbsyfq")))

(define-public crate-veryl-formatter-0.3.4 (c (n "veryl-formatter") (v "0.3.4") (d (list (d (n "veryl-metadata") (r "^0.3.4") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.3.4") (d #t) (k 0)))) (h "0jxcbwdw7acwpb9k6d7yycsvg0n2m72zw245n47pkviv3f7slwvq")))

(define-public crate-veryl-formatter-0.4.0 (c (n "veryl-formatter") (v "0.4.0") (d (list (d (n "veryl-metadata") (r "^0.4.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.4.0") (d #t) (k 0)))) (h "0bnzgq3ip27xvdkrbjna3lmr0spmz16z90k52cm1pcvki9gz1mb3")))

(define-public crate-veryl-formatter-0.5.0 (c (n "veryl-formatter") (v "0.5.0") (d (list (d (n "veryl-metadata") (r "^0.5.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.0") (d #t) (k 0)))) (h "1gnv5n16xmgrkl4d9x95yxlky84pr7yv2rny79wamiwzc1hsjq07")))

(define-public crate-veryl-formatter-0.5.1 (c (n "veryl-formatter") (v "0.5.1") (d (list (d (n "veryl-metadata") (r "^0.5.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.1") (d #t) (k 0)))) (h "0306iir0vpyxybymnl863ysr5dril10dkrw7ifz126ccgzrzi1kj")))

(define-public crate-veryl-formatter-0.5.2 (c (n "veryl-formatter") (v "0.5.2") (d (list (d (n "veryl-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.2") (d #t) (k 0)))) (h "14dxsn3janzfqvrr5aj2kic26fqg8g1lnw6d2xv9lrs6c9rkj2vr")))

(define-public crate-veryl-formatter-0.5.3 (c (n "veryl-formatter") (v "0.5.3") (d (list (d (n "veryl-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.3") (d #t) (k 0)))) (h "0imj968npbszrpdvw05d2wna98v6y69s9j8h41zklccp7jhmqrb0")))

(define-public crate-veryl-formatter-0.5.4 (c (n "veryl-formatter") (v "0.5.4") (d (list (d (n "veryl-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.4") (d #t) (k 0)))) (h "0q91zpyazcv3rk2jrn46qc5bsy9fraa5yss33nw6nzg83jf6rp11")))

(define-public crate-veryl-formatter-0.5.5 (c (n "veryl-formatter") (v "0.5.5") (d (list (d (n "veryl-metadata") (r "^0.5.5") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.5") (d #t) (k 0)))) (h "1wg0zc2kh7a5n2ap8m3g12a9i0ns4j7v09w7n60qq5fxjg06g3d3")))

(define-public crate-veryl-formatter-0.5.6 (c (n "veryl-formatter") (v "0.5.6") (d (list (d (n "veryl-metadata") (r "^0.5.6") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.5.6") (d #t) (k 0)))) (h "0gdrcpm4xhrrjhm66yldvz2y7qd5hy0gxrwjldn08z7dbfmpidk0")))

(define-public crate-veryl-formatter-0.6.0 (c (n "veryl-formatter") (v "0.6.0") (d (list (d (n "veryl-metadata") (r "^0.6.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1iw1rb6gsgn2266mr265ihchay4lycjdggi2i6gnc7v80r03xhdp")))

(define-public crate-veryl-formatter-0.7.0 (c (n "veryl-formatter") (v "0.7.0") (d (list (d (n "veryl-metadata") (r "^0.7.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.0") (d #t) (k 0)))) (h "0i1v7r5gdg81c525x0g6bdgr6lp4v5qpg6l53bb3mi3rrnvhpiw7")))

(define-public crate-veryl-formatter-0.7.1 (c (n "veryl-formatter") (v "0.7.1") (d (list (d (n "veryl-metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.1") (d #t) (k 0)))) (h "111z0c7qgi1lvs63basflkgkp6f6x7zbg8ih8c36p5rv8as7ppi3")))

(define-public crate-veryl-formatter-0.7.2 (c (n "veryl-formatter") (v "0.7.2") (d (list (d (n "veryl-metadata") (r "^0.7.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.7.2") (d #t) (k 0)))) (h "1ns51s8c5yrh33flf6l6s79hri6pk2f2i87sd2yr736nss3yr3vq")))

(define-public crate-veryl-formatter-0.8.0 (c (n "veryl-formatter") (v "0.8.0") (d (list (d (n "veryl-metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0pqq79fv2sjddssk7i8wi908lx4pfvhxdcpsa1fv4dk98488khhx")))

(define-public crate-veryl-formatter-0.8.1 (c (n "veryl-formatter") (v "0.8.1") (d (list (d (n "veryl-metadata") (r "^0.8.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.1") (d #t) (k 0)))) (h "1cs4rwkyadpb37cymdb4jq8z29gyxwhi5hx3i0z56z884iclx6lh")))

(define-public crate-veryl-formatter-0.8.2 (c (n "veryl-formatter") (v "0.8.2") (d (list (d (n "veryl-metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.8.2") (d #t) (k 0)))) (h "1il7i000j49r694z3mzrgw2kfwkjxsha55rffkic92xj9q3x40n7")))

(define-public crate-veryl-formatter-0.9.0 (c (n "veryl-formatter") (v "0.9.0") (d (list (d (n "veryl-metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.9.0") (d #t) (k 0)))) (h "15iyj8g2p1sb9x4vqm3zrbczsw4iscw67dqpbgmn16r3v77r00h4")))

(define-public crate-veryl-formatter-0.10.0 (c (n "veryl-formatter") (v "0.10.0") (d (list (d (n "veryl-metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.0") (d #t) (k 0)))) (h "18i0zc5ilczfqr0qdsinggr6rmshwlb7kacqrg71xi2pnfq17lxg")))

(define-public crate-veryl-formatter-0.10.1 (c (n "veryl-formatter") (v "0.10.1") (d (list (d (n "veryl-metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "veryl-parser") (r "^0.10.1") (d #t) (k 0)))) (h "1dr9k4lp2sva114m8adbka8qrsnylg9h1nwd5adf2wdfl2k5w1r5")))

