(define-module (crates-io ve nk venko) #:use-module (crates-io))

(define-public crate-venko-0.1.0 (c (n "venko") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "crate-token") (r "^0.4.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0c67rqbnvrw8b18dnbzg2sn9205ck61gd2j4i6s8zi4z9fznmsc7") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-venko-0.1.1 (c (n "venko") (v "0.1.1") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "crate-token") (r "^0.4.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0729w2p5rfg7rd8rqhj6jagg09b5gwvsmpx47fg8jpm9ps0msvgc") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

