(define-module (crates-io ve cg vecgrid) #:use-module (crates-io))

(define-public crate-vecgrid-0.1.0 (c (n "vecgrid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15ajcq0kkbxvpnpd3bgmhn56cx9gqyafn006v5zrx3qnljbxfpqg")))

(define-public crate-vecgrid-0.1.1 (c (n "vecgrid") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d570nybbjkziwgigkgsk3klnhn89q7q9c1xnixwq208gl8z0km7")))

(define-public crate-vecgrid-0.1.2 (c (n "vecgrid") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19xrm1ba7d702kxki6j50xjz8di3043i1mg5ib2k5sixpfis0za2")))

(define-public crate-vecgrid-0.1.3 (c (n "vecgrid") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p6i9nkcz7lnhiv5hxcviz96vmvxx1kdxz23llikb0vrjws7f6w9")))

(define-public crate-vecgrid-0.2.0 (c (n "vecgrid") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l25fa05n1vramp6haq32v83k0rlxz4dqr5y26i1d6mpabadmc3z")))

(define-public crate-vecgrid-0.2.1 (c (n "vecgrid") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ngpaf24j57la43mb58fw57pi74c50xmfszfbjsh08vhsp73nnrh")))

(define-public crate-vecgrid-0.2.2 (c (n "vecgrid") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cc7icl9qbfn6hzmxsf45jpnjpdnp5k6w6phipizc7x5xzddaiax")))

