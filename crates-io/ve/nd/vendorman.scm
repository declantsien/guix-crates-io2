(define-module (crates-io ve nd vendorman) #:use-module (crates-io))

(define-public crate-vendorman-0.1.0 (c (n "vendorman") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0955pqr9s9af1s6hwzvmj1nyy6b33zvxwy53aqqqy06vb5hyx3wi")))

