(define-module (crates-io ve re verex) #:use-module (crates-io))

(define-public crate-verex-0.1.0 (c (n "verex") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "184r8p37x24g1adrby65h4a5njjiknnf91a0klnyvy4yx7h138ml")))

(define-public crate-verex-0.1.1 (c (n "verex") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0vwr77ii16fv4xfndb7wdgpj20aqmgzzls2z42if04wwwy1nxh5s")))

(define-public crate-verex-0.2.0 (c (n "verex") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0k5baa4qibn71kk4j382mh9mx5f4papicc22khv6jfnwi5wmgpi2")))

(define-public crate-verex-0.2.1 (c (n "verex") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0ilwjqwwnxk25cy991sqippr1dyblf8bvv6bgxbky85n957j3z5l")))

(define-public crate-verex-0.2.2 (c (n "verex") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1cwpsqfw1qyrk68b6yv0i5kas4xri8g10swy64bpf5n000rpx4y3")))

