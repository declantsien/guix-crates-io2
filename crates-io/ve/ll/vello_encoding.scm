(define-module (crates-io ve ll vello_encoding) #:use-module (crates-io))

(define-public crate-vello_encoding-0.1.0 (c (n "vello_encoding") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "guillotiere") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "peniko") (r "^0.1.0") (d #t) (k 0)) (d (n "skrifa") (r "^0.15.5") (o #t) (d #t) (k 0)))) (h "08ylyzzzxwhi512bf66px8x3fnam3gpwr50q3vpbdj8kq5p6qnqc") (f (quote (("full" "skrifa" "guillotiere") ("default" "full")))) (r "1.75")))

