(define-module (crates-io ve ll vello) #:use-module (crates-io))

(define-public crate-vello-0.1.0 (c (n "vello") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "peniko") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6.0") (d #t) (k 0)) (d (n "skrifa") (r "^0.15.5") (d #t) (k 0)) (d (n "vello_encoding") (r "^0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-profiler") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "0ylb5rmmsz2q0zq3s10f3zvng9z3c1jl7d38g2kgyvkd5mmbk979") (f (quote (("hot_reload") ("default" "wgpu") ("buffer_labels")))) (r "1.75")))

