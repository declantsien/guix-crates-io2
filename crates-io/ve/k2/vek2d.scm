(define-module (crates-io ve k2 vek2d) #:use-module (crates-io))

(define-public crate-vek2d-1.0.0 (c (n "vek2d") (v "1.0.0") (h "0vhrxg9lkyb1qmpa153yqyspl35427hn56fdsxv1if5n51zgd6nj")))

(define-public crate-vek2d-1.0.1 (c (n "vek2d") (v "1.0.1") (h "1x1bk2y993qdi6ickf5pp4s81970ip9b6x9l7giqbdgaz224nlyn")))

