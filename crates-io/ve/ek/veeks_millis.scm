(define-module (crates-io ve ek veeks_millis) #:use-module (crates-io))

(define-public crate-veeks_millis-0.5.5 (c (n "veeks_millis") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0h43qzfg35wbvdh7kdxz76yyg36ckpfc228xiaxz62rxg7qiz91r") (y #t)))

(define-public crate-veeks_millis-1.0.2 (c (n "veeks_millis") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0cdn17585a6441n65y59idwnm5dslcjx71vxx7ckqdx7rfcsgaa8") (y #t)))

(define-public crate-veeks_millis-1.0.3 (c (n "veeks_millis") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1ydqy7cvfmz6d31rzkchcd6dy78y8f5xw20f4hdi3s0cs8b3yk47") (y #t)))

