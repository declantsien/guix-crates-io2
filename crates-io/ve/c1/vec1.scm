(define-module (crates-io ve c1 vec1) #:use-module (crates-io))

(define-public crate-vec1-1.0.0 (c (n "vec1") (v "1.0.0") (h "0lrcprrz7f5jqnpsrmn4051fpnj1dzd6y6rc942x8c79595dxvmi")))

(define-public crate-vec1-1.0.1 (c (n "vec1") (v "1.0.1") (h "0c6yx2lmcf29787nbzyisp1684nk58rbnj0w1n9iqydhqic6gbjr")))

(define-public crate-vec1-1.1.0 (c (n "vec1") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dcyjx5mgxsgwclvd3j4hmxb5xvbr030xcnbgfk23x4rk647hv9l")))

(define-public crate-vec1-1.2.0 (c (n "vec1") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wrvbrs1mkpvf13kqb40vn63rz2sf55j1sr80hvalg21j3fhm8yb") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.3.0 (c (n "vec1") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pcn0kmlcszby08b1ir41dfa4gcxig7gkwlrajx12zfrsg8gzclp") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.4.0 (c (n "vec1") (v "1.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qgrjrd2v33dln9hp396xlkffz0i9jk1zaq2zrhqc0nc8hw2mbfj") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.5.0 (c (n "vec1") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ldzil8vhz73hw6lgdpzszj0bp47ar4262v9pmqcy3vhdlhi4fm5") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.5.1 (c (n "vec1") (v "1.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1412rlbnmlx2hgymg9nv2nifib3l7hsjb4xhzvrl3fm3m7h3zm6z") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.6.0 (c (n "vec1") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15zy2p7i62as0c283xrqdp71dx3hvvpn9kjwc86dbn5ggc8zvzzj") (f (quote (("unstable-nightly-try-from-impl"))))))

(define-public crate-vec1-1.8.0 (c (n "vec1") (v "1.8.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (o #t) (d #t) (k 0) (p "smallvec")))) (h "0cv1b88k9fac0wlg3yzbkrwdxvyb8w9f14big5q9a3sgfwf67haz") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std"))))))

(define-public crate-vec1-1.9.0 (c (n "vec1") (v "1.9.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "0bjsb76ywnq8al929ss22vl6rh9kmsqldkygaax25r4fl2rb0kxl") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std"))))))

(define-public crate-vec1-1.10.0 (c (n "vec1") (v "1.10.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "1k1rils1q8fvxbydybid1ghqbb8w56cah8x8a87m3a83dsc45np5") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.57")))

(define-public crate-vec1-1.10.1 (c (n "vec1") (v "1.10.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "0p4xrrgqpzxlg7q74aqwxgsyfjsz0ppfgabqqahyj7rkr90prnib") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.57")))

(define-public crate-vec1-1.11.0 (c (n "vec1") (v "1.11.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "06m1s9vk3ximv4idvq6x6lqyi5q1r2wbrg7q74z36yfg80d5xnr9") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.57")))

(define-public crate-vec1-1.11.1 (c (n "vec1") (v "1.11.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "0x029sxy3x2jw6ips839dyr0m0pzh7xcngdhy7bzgb4jqhrga9jd") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.74")))

(define-public crate-vec1-1.12.0 (c (n "vec1") (v "1.12.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "0dk9qlly3n6b5g71p9rxnnfyx7v1d31364x8wbabz2f1zz7hvdpz") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.74")))

(define-public crate-vec1-1.12.1 (c (n "vec1") (v "1.12.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec_v1_") (r "^1.6.1") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0) (p "smallvec")))) (h "08k370srixpnqgvyxqjqvkzig4j9c5kapqzvzsqfys8ghib8pdpa") (f (quote (("unstable-nightly-try-from-impl") ("std") ("smallvec-v1-write" "std" "smallvec_v1_/write") ("smallvec-v1" "smallvec_v1_") ("default" "std")))) (r "1.71.1")))

