(define-module (crates-io ve _s ve_shader) #:use-module (crates-io))

(define-public crate-ve_shader-0.1.0 (c (n "ve_shader") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0h1c1m1553g82ks5lrm8rb5gk3gqbl58vikhf0dm29xandghqm7c")))

(define-public crate-ve_shader-0.1.1 (c (n "ve_shader") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1iig5pagnkv7qlfpk55zjc0qlbz9m9lk9zgvcfm6d16yph71pm09")))

(define-public crate-ve_shader-0.1.2 (c (n "ve_shader") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0h4p2mfh0ckfmhh4lp7z4xlsbbp6c01ll82cjk27j2npc4h2gxjg")))

