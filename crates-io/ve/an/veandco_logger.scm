(define-module (crates-io ve an veandco_logger) #:use-module (crates-io))

(define-public crate-veandco_logger-0.1.0 (c (n "veandco_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fpiabpbp6xg7y3r1vhd51q1v75hjiz26mm0qi2hsqsh2ps39d0n")))

(define-public crate-veandco_logger-0.2.0 (c (n "veandco_logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x77wv1k6qjaybl27v43lvq158ink0ddqhddwb4dfk3x2aqvysng")))

