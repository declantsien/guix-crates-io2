(define-module (crates-io ve c2 vec2) #:use-module (crates-io))

(define-public crate-vec2-0.1.0 (c (n "vec2") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "0lkhi7njyxgi73m33jk0x5biyq8kx0ybsrag5cxazqm3760sd4p1")))

(define-public crate-vec2-0.1.1 (c (n "vec2") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "0kgb08ia85dlj1piwm72bfzclkhmfx5y94z7vgqi6nc5xfc8g6f1")))

(define-public crate-vec2-0.1.2 (c (n "vec2") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "04hvkzpzl3ysh8mhjmjwsyi3mqnvy4x2415qzfsxvcdy8jynmfbs")))

(define-public crate-vec2-0.1.3 (c (n "vec2") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "1nmn0mq1a8b8j76wb5wpiphwzjxicsbnvygfvwagysrsv3vw9qp8")))

(define-public crate-vec2-0.1.4 (c (n "vec2") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "01r2hpm2gipccg1ayfm0464m8z921cjqiljji0d4mrb5brc95apm")))

(define-public crate-vec2-0.1.5 (c (n "vec2") (v "0.1.5") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "1gachs1wv3z68bibj0l4hnn700lj4bff4xvykf9kphjf6aa3n8dm")))

(define-public crate-vec2-0.2.0 (c (n "vec2") (v "0.2.0") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "1nhh0sb300m026qx4qck8rrqnqj4i6likyyg5my0qnnkfvfplwyj")))

(define-public crate-vec2-0.2.1 (c (n "vec2") (v "0.2.1") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "0gxv9hdl07imqsjqh2vrpckflf7k51qnjajmajn0yn0vacaq8k55")))

