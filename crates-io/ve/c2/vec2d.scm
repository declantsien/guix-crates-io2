(define-module (crates-io ve c2 vec2d) #:use-module (crates-io))

(define-public crate-vec2d-0.0.1 (c (n "vec2d") (v "0.0.1") (h "0dwvpqz4fmr4dirn432vr5fm6r7n4yk872a706zwmb7yqbvba2vx")))

(define-public crate-vec2d-0.0.2 (c (n "vec2d") (v "0.0.2") (h "17p76221p6k81xrd63m0c1f1l8wh0y4j00fbhz2rd3plwqvqmvcl")))

(define-public crate-vec2d-0.0.3 (c (n "vec2d") (v "0.0.3") (h "1jbngi2x4vfhkvcv6szvj5bb32zlmcgpqqrllwfjanraz6nkx2h4")))

(define-public crate-vec2d-0.0.4 (c (n "vec2d") (v "0.0.4") (h "0gp12c5jjmmwmnzy5i25dm54hm479dsg7w114cmdl3i9myg4p275")))

(define-public crate-vec2d-0.0.5 (c (n "vec2d") (v "0.0.5") (h "0ah0ryar88r0wm5bxbjgj2sd4nnqwqk7z4mzfy180cbd49liicxk")))

(define-public crate-vec2d-0.1.0 (c (n "vec2d") (v "0.1.0") (h "1gmcn8flddkmgw59aqvjygdharfisqyz6dgyg4ncfdq9vd42k2zl")))

(define-public crate-vec2d-0.1.1 (c (n "vec2d") (v "0.1.1") (h "1bm2cqympyi1dq9j539hhif6l0vjsy488lgnvwix71wwiymla8hj")))

(define-public crate-vec2d-0.3.0 (c (n "vec2d") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "09g4pvgbdiqszjada5sfgg1ysrrldl4sbxrwjayx4ma5qjix2y40") (f (quote (("serde_support" "serde"))))))

(define-public crate-vec2d-0.4.0 (c (n "vec2d") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 2)))) (h "1pwmqa89v3jbmm9nw738drmb1lzaic1y3va2mg58k39b5z6almnm") (f (quote (("serde_support" "serde"))))))

(define-public crate-vec2d-0.4.1 (c (n "vec2d") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 2)))) (h "0ccyirx2qd34lvl7vrv1r7q27a6mdqzqq4z5fgrmx3s38f5w19h5") (f (quote (("serde_support" "serde"))))))

