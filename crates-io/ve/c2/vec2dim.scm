(define-module (crates-io ve c2 vec2dim) #:use-module (crates-io))

(define-public crate-vec2dim-0.1.0 (c (n "vec2dim") (v "0.1.0") (h "1kh9xlxg3k86rppzmgf988zzh3947vfxacsmx3n561kl9gigx4ay")))

(define-public crate-vec2dim-0.1.1 (c (n "vec2dim") (v "0.1.1") (h "0yln28dh9zcmaqs4a1gkgy3ssbq5fayqzylxsfz7k0bsh17mrr10")))

(define-public crate-vec2dim-0.2.0 (c (n "vec2dim") (v "0.2.0") (h "03ahjgvcs62mzjwm9n6v3ffc1bsg5wslmxq72syxgbbk2s4ln741")))

(define-public crate-vec2dim-0.2.1 (c (n "vec2dim") (v "0.2.1") (h "1j2ymz81fbnr4gdxvz2jbvxcrkcsxgzqf6jz09smw65008byjigf")))

(define-public crate-vec2dim-0.2.2 (c (n "vec2dim") (v "0.2.2") (h "1n1q5bb4405rhcfv6hd3d0x50ynjag0hys6dqar5a2yaz4jc93c8")))

(define-public crate-vec2dim-0.3.1 (c (n "vec2dim") (v "0.3.1") (h "1cbai2bd2kix7yfx4cjbylkkdmbm3g0m0js826z6as2895y3ghnl")))

(define-public crate-vec2dim-0.3.2 (c (n "vec2dim") (v "0.3.2") (h "16cd9nzih4dwcf35jqrchbwyaqw3j87lxz2680ssjs1cyd4wdc7v")))

