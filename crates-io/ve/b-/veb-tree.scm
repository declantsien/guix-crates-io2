(define-module (crates-io ve b- veb-tree) #:use-module (crates-io))

(define-public crate-veb-tree-0.1.0 (c (n "veb-tree") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "pprof") (r "^0.11") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gbkzy4c0fp9yrwsh29v57107bl7hbq5wkqb1vkbhzn799cz88d2")))

