(define-module (crates-io ve ct vectorize) #:use-module (crates-io))

(define-public crate-vectorize-0.1.0 (c (n "vectorize") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "11iwv2kdwsw6gqf7jp0gfb0anqkkv717rjnx6zk8kahiwwj1400k")))

(define-public crate-vectorize-0.2.0 (c (n "vectorize") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0nrn1vwglcjak0vz3swpw2gxdb3ajdfb9cc3w46acknczpyvpqr5")))

