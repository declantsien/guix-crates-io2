(define-module (crates-io ve ct vectorscan) #:use-module (crates-io))

(define-public crate-vectorscan-0.1.0 (c (n "vectorscan") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1fr7mx2ncd64gs0cn6dcr7qzwygdzrfhy6vi1wcmni89xj9ix1xb") (f (quote (("gen" "bindgen"))))))

(define-public crate-vectorscan-0.1.1 (c (n "vectorscan") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ymxil3zb6mi2blhzg3bs2nfssrh9i5ijcyfqvdjk10mp8iwiv67") (f (quote (("gen" "bindgen")))) (y #t)))

(define-public crate-vectorscan-0.1.2 (c (n "vectorscan") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jl9ix06z3bljmj3yf9ml67hjzyys9f2gghv0333l24db476mzh9") (f (quote (("gen" "bindgen")))) (y #t)))

