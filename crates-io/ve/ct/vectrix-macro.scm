(define-module (crates-io ve ct vectrix-macro) #:use-module (crates-io))

(define-public crate-vectrix-macro-0.1.0 (c (n "vectrix-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a50rcnmlym2v7ls3fbkd6m1fz7f27i3k40ilan1mcvcfhdjqxvi")))

(define-public crate-vectrix-macro-0.1.1 (c (n "vectrix-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nrlllc8vigfppd3grlmci2rgff1z2dz772bbpdd4v7cy0dq43l0")))

(define-public crate-vectrix-macro-0.1.2 (c (n "vectrix-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w11nqw2phs3p43i5mp2naamrf2rcplx3gvrzwb6sbpl0529761y")))

(define-public crate-vectrix-macro-0.2.0 (c (n "vectrix-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d1f4hpazk253cd5xr4w5nb3mfbpc2sg5a2ir44wn4bl7alvg7d1")))

(define-public crate-vectrix-macro-0.3.0 (c (n "vectrix-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xcbpws840ayak9wivwrrz5hcw1hrdydbp9lb0jjca8nhc3pj6wn")))

