(define-module (crates-io ve ct vector) #:use-module (crates-io))

(define-public crate-vector-0.1.0 (c (n "vector") (v "0.1.0") (h "04gsy4pp9lah138bg4kj93kyir1ss8q92hx00arizc633qp6p232")))

(define-public crate-vector-0.2.0 (c (n "vector") (v "0.2.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "11j3gnvlpiypjv4my7pxihphcksk7nbqlvvwd6k9r81d92hr18z4")))

(define-public crate-vector-0.3.0 (c (n "vector") (v "0.3.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "0ms9h4kdf2hgjk0yyb2lf9aq5y5k86sqyv49fr2njwvj312rj80w")))

(define-public crate-vector-0.3.1 (c (n "vector") (v "0.3.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "1xppfwl8d9l6i8lwgcxsqxvjswkp3j1wdh7kpc9pz1qcj6d90gw9")))

(define-public crate-vector-0.4.0 (c (n "vector") (v "0.4.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "17c6gcvjmfh28d1qr233v236j7wlfz7rc7fi9nhlb52jazxxjs9p")))

(define-public crate-vector-0.4.1 (c (n "vector") (v "0.4.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "1l5giqzg29i8shnmm052w26gzx627y4z47nw1p5y0hr4as8fj2r8")))

