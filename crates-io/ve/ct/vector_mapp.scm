(define-module (crates-io ve ct vector_mapp) #:use-module (crates-io))

(define-public crate-vector_mapp-0.1.0 (c (n "vector_mapp") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)))) (h "1yk05z7paf7kp71pvyvwrylxczpnqr8p92gdaxza2y2zskv322r4") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.2.0 (c (n "vector_mapp") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)))) (h "04x1ffxpai4gnqix4p5hls7wiw1wv7wlk229khr4cgdqz1imlmyw") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.2.1 (c (n "vector_mapp") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "0r8ypl24pxdfal214jxv86la3w2f9miwwv265fmh9prmwrfk0z0j") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.2.2 (c (n "vector_mapp") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "1k5zih10skxrv2gcfbrj952qkh3102jrmkwmjpdknypjr94pmvka") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.3.0 (c (n "vector_mapp") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "1zrpk9ym2i1i6inlfvwrspj7dx9dhq7mxj05ca50cs3lvqsf45a7") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.3.1 (c (n "vector_mapp") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "0vhqrnj5gps4viavzpkd9f8sshxmyys1fnhawdpr658706ahhxy2") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.3.2 (c (n "vector_mapp") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "1h8x8a5qlmlxgid39wf50zalgqrj5v7b0xzccym0r74d2gmigcri") (f (quote (("alloc"))))))

(define-public crate-vector_mapp-0.3.3 (c (n "vector_mapp") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "03l3rbfirb5hwwsp36chb13p3jb7iwavi69lxx4gfcrjcr30g6ja") (f (quote (("alloc"))))))

