(define-module (crates-io ve ct vectorscan-rs) #:use-module (crates-io))

(define-public crate-vectorscan-rs-0.0.1 (c (n "vectorscan-rs") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vectorscan-rs-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1hyi9gavrydkymhknir4kk76mnfdxsjmkfc3w9liva0mimk4dhnb") (f (quote (("simd_specialization" "vectorscan-rs-sys/simd_specialization") ("fast_nonportable" "cpu_native" "simd_specialization") ("cpu_native" "vectorscan-rs-sys/cpu_native")))) (r "1.73")))

(define-public crate-vectorscan-rs-0.0.2 (c (n "vectorscan-rs") (v "0.0.2") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vectorscan-rs-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0qaf2bkafr11zgkd1m3d098xvchnl9l37fx36bksvsss44q76zq7") (f (quote (("unit_hyperscan" "vectorscan-rs-sys/unit_hyperscan") ("simd_specialization" "vectorscan-rs-sys/simd_specialization") ("fast_nonportable" "cpu_native" "simd_specialization") ("cpu_native" "vectorscan-rs-sys/cpu_native")))) (r "1.73")))

