(define-module (crates-io ve ct vector2) #:use-module (crates-io))

(define-public crate-vector2-0.1.0 (c (n "vector2") (v "0.1.0") (h "0mp1y8b22sr0kkfs2wx5qpb81yacjs0y6kqms833vqmk28gzm15n")))

(define-public crate-vector2-0.1.1 (c (n "vector2") (v "0.1.1") (h "13f442bqnr2jlwgp7p3ik33ryfaf6n1fljnws8v1m5kjbkcm90pi")))

(define-public crate-vector2-0.1.2 (c (n "vector2") (v "0.1.2") (h "1h6fl5iwg1i9hx4b27i1djb1kj4p0wx9jrd48w3gagggahr82q6m")))

(define-public crate-vector2-0.1.3 (c (n "vector2") (v "0.1.3") (h "0b1q27aw1lwgdp04liz9hysbc3m0wfihcnpx4d3j8dqrwcq9398l")))

(define-public crate-vector2-0.1.4 (c (n "vector2") (v "0.1.4") (h "0in8l9rcipkh0nr4vqpnsp4i7s01wmyn0dmrhlwxjz9kcz3vkyk0")))

(define-public crate-vector2-0.1.5 (c (n "vector2") (v "0.1.5") (h "016ym2b4sb2qn3wrp7rib3sraiwi2ayn66jf6xl7b1j19y6jymig")))

