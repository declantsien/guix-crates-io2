(define-module (crates-io ve ct vectortile) #:use-module (crates-io))

(define-public crate-vectortile-0.2.0 (c (n "vectortile") (v "0.2.0") (d (list (d (n "postgis") (r "^0.4.0") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.2") (d #t) (k 0)))) (h "02g7qbihw590rhxwcgjsi3bxssag3wi4475y97rkhp1mnmrx9b6f")))

(define-public crate-vectortile-0.2.1 (c (n "vectortile") (v "0.2.1") (d (list (d (n "postgis") (r "^0.4.0") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.2") (d #t) (k 0)))) (h "0gz32vzxk2fyh70yyg0c8fxpxkd3brnfgg826lay642n0098kh1f")))

(define-public crate-vectortile-0.2.2 (c (n "vectortile") (v "0.2.2") (d (list (d (n "postgis") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (d #t) (k 0)))) (h "0j3ybjcmjfr99wwykq50f9rrkxsdd93fjdrzgz440r6ghs6y9dzp")))

