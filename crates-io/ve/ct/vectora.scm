(define-module (crates-io ve ct vectora) #:use-module (crates-io))

(define-public crate-vectora-0.1.0 (c (n "vectora") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1kgnf95xn4zskbfxzh3czhnyd358p22mrav3qdmp9bnix8v3qf91")))

(define-public crate-vectora-0.1.1 (c (n "vectora") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0q44pbyi2ch7l6400xyjv2i3kpr00jfn0h2gwkhsi501029cg1lj")))

(define-public crate-vectora-0.1.2 (c (n "vectora") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0qwzhi0xiz080lawwn0mi3ii4jxz7fqzn217f750sxq2b498is8b")))

(define-public crate-vectora-0.1.3 (c (n "vectora") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "02y918spfm2zgbqp3m36yqpgdhkq7yzp9bdpnk9r3pnxkyjj4z97")))

(define-public crate-vectora-0.2.0 (c (n "vectora") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1r8nmlx11n9fzz5vhg33l3a08j3cj35d6dvv0z7z1pb3fivrjkln")))

(define-public crate-vectora-0.2.1 (c (n "vectora") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "05l9xgjqrfh4a8r7lzwjw1r9k4zp2qmwzddsabm1jl5xaak2anm9")))

(define-public crate-vectora-0.3.0 (c (n "vectora") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1cp8in2xdlcsx6zsl2cszdzkz6m6ycb5p4yk2m8zcvjbf8c7y9y0")))

(define-public crate-vectora-0.3.1 (c (n "vectora") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "01x2gc76283657xdrbglbrraa32zykanpny5mscrz3pl9gfwy18c")))

(define-public crate-vectora-0.4.0 (c (n "vectora") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0i7kng46zx802ckh4j831644in98kn05lf0ndxd72rf0w3dbc9jr")))

(define-public crate-vectora-0.5.1 (c (n "vectora") (v "0.5.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1a7cz4wgdwq7f625s3ay827lhk9q9rc6qpjvb7wwgvvnl18jbmg2")))

(define-public crate-vectora-0.6.0 (c (n "vectora") (v "0.6.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1fyqy5rxjfkpy9nj0s80ify7yjcbnylxjmf6904ps6ry5sv23y67")))

(define-public crate-vectora-0.7.0 (c (n "vectora") (v "0.7.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0w5hgk03jy36ivwjcv4v7ls160vmmi2hz8dd8f3k5dy8q93jsb4r")))

(define-public crate-vectora-0.8.0 (c (n "vectora") (v "0.8.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0aqcp36lm91yhg021piff549iwkg2q964a3xgagr5akzc5mdbq0v") (f (quote (("parallel" "rayon"))))))

(define-public crate-vectora-0.8.1 (c (n "vectora") (v "0.8.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1qy3z7bikwhdrayq1k6zldl6cffvjh39hna80aijignvs0dlimh6") (f (quote (("parallel" "rayon"))))))

