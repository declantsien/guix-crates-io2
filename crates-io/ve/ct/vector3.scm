(define-module (crates-io ve ct vector3) #:use-module (crates-io))

(define-public crate-vector3-1.1.0 (c (n "vector3") (v "1.1.0") (h "0j0yfq8nmcx4pmqpx7ndfm904l7nbkapfz3l3ns0h448cd6c6zjy")))

(define-public crate-vector3-1.1.1 (c (n "vector3") (v "1.1.1") (h "0mbr0agb6zpj8bcbsf6ralasah47ivk7p2vn79h1ann992cn51fy")))

(define-public crate-vector3-1.1.2 (c (n "vector3") (v "1.1.2") (h "111lsl5s7mn02dqb80sz5pwc65mxayyiq8k2pikilc2qcwhz4spa")))

(define-public crate-vector3-1.1.3 (c (n "vector3") (v "1.1.3") (h "1vdqkbrspzwgf021b7dvh5iqa2czkvlfgkx9g2nb97a4yciljxz2")))

(define-public crate-vector3-1.1.4 (c (n "vector3") (v "1.1.4") (h "0zl2s2fdadnpqywffbk3ms8dg0dvkcmsqp7y2i9903hp8v4wab08")))

