(define-module (crates-io ve ct vectorscan-sys) #:use-module (crates-io))

(define-public crate-vectorscan-sys-0.0.0 (c (n "vectorscan-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "eyre") (r "^0.6.8") (d #t) (k 1)) (d (n "spack") (r "^0.0.5") (d #t) (k 1) (p "spack-rs")) (d (n "tokio") (r "1.33.*") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 1)))) (h "0hm9n3qrjj7y68hj7z2ppqarhjcadbxjzihxyhz4rsx3r461rasm") (f (quote (("static") ("dynamic") ("default") ("compiler") ("chimera")))) (l "vectorscan")))

(define-public crate-vectorscan-sys-0.0.1 (c (n "vectorscan-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "eyre") (r "^0.6.8") (d #t) (k 1)) (d (n "spack") (r "^0.0.5") (d #t) (k 1) (p "spack-rs")) (d (n "tokio") (r "1.33.*") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 1)))) (h "0l2r48gbkjiv0c083wkgqgw6pi6mjngd72wk69pz6v8g97290s70") (f (quote (("static") ("dynamic") ("default") ("compiler") ("chimera")))) (l "vectorscan")))

