(define-module (crates-io ve ct vector-rust-library) #:use-module (crates-io))

(define-public crate-vector-rust-library-0.1.0 (c (n "vector-rust-library") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0lfqrfzqzdsd31bm7n6rprk2rgcxspjijj2liim523yvd58rp3n5")))

