(define-module (crates-io ve ct vector-expr) #:use-module (crates-io))

(define-public crate-vector-expr-0.2.0 (c (n "vector-expr") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0yg0b0ly97q0s8nw61ypqsjghysvbh9iplkzwabrjq34d503pl01")))

(define-public crate-vector-expr-0.4.0 (c (n "vector-expr") (v "0.4.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1lgh29rj4nxs5i3v4jrd6v16gif3239q3nmjn5ncdxr2rcqw9sy1")))

