(define-module (crates-io ve ct vector-space) #:use-module (crates-io))

(define-public crate-vector-space-0.1.0 (c (n "vector-space") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cmvzihbdg1sr1nh2wx1flxjk6fsx3yzymil9vc4sxvq97fhvp48")))

(define-public crate-vector-space-0.2.0 (c (n "vector-space") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j73f2gsjmsxhxk3pd2qm44pccqs78hzjbm5nnk0rnk5dzya8hv0")))

(define-public crate-vector-space-0.3.0 (c (n "vector-space") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1f5gqxvlgr59d4gmcky2l0lc19365s4633h55bqflabxk7bic343")))

(define-public crate-vector-space-0.3.1 (c (n "vector-space") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1njwlli31p6h85c8l60cm4a0rkn0si6jmasj9irmpf9d737h779q")))

