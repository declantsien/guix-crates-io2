(define-module (crates-io ve ct vector-array) #:use-module (crates-io))

(define-public crate-vector-array-0.1.0 (c (n "vector-array") (v "0.1.0") (h "082njdys66l4irrnvqy5wyx8nn8613fmy4czh813b2lk2n0d9pih")))

(define-public crate-vector-array-0.1.1 (c (n "vector-array") (v "0.1.1") (h "1agc6z5gfzrvyjncn7y8bvviqdf4i8flihavg795js9dhk2x7dwm")))

(define-public crate-vector-array-0.1.2 (c (n "vector-array") (v "0.1.2") (h "1dxh3vm3cyisp19avkxg25s2sllr6jy5n7akpi69d938v936r2ww")))

(define-public crate-vector-array-0.1.3 (c (n "vector-array") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qjj7rkan5qzyvq89h8vk6k6m7rsr0pcr21dm2vi10r5m3a68hfa")))

(define-public crate-vector-array-0.1.4 (c (n "vector-array") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10yfgspjh29cgs8bkcwhd2aminrhqc7wls8wb9k21bazf0sq2460")))

