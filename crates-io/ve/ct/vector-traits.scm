(define-module (crates-io ve ct vector-traits) #:use-module (crates-io))

(define-public crate-vector-traits-0.1.0 (c (n "vector-traits") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0i9rm781mfn9bkv9ihbkay6rwzr4rjd8v9bw2piramdswcryj7hk") (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath") ("approx" "dep:approx"))))))

(define-public crate-vector-traits-0.2.0 (c (n "vector-traits") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0f8rdvjkbxk41xy4g6hx6rb8avbkc2mzckkibb3443ncdx7lb9zd") (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

(define-public crate-vector-traits-0.3.0 (c (n "vector-traits") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0m9y7my8achg7vq3m1cd3il6slyc1j70bn56xc19qq7nzqz57paa") (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

(define-public crate-vector-traits-0.3.1 (c (n "vector-traits") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1wxhcv0xjis24nbq4vrznshxz72y2gxn34gsarnyx35cfdxza122") (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

(define-public crate-vector-traits-0.3.2 (c (n "vector-traits") (v "0.3.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "11kxsl6hssrfr5fykvxcmjg99c518lsnlvfgls1g9k66mb9ch03k") (f (quote (("glam-fast-math" "glam/fast-math") ("glam-core-simd" "glam/core-simd")))) (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

(define-public crate-vector-traits-0.3.3 (c (n "vector-traits") (v "0.3.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "051bbswy1hfkqrqkxd68b4z3k1w7ag8r643j37bn8j9ik33gc8s2") (f (quote (("glam-fast-math" "glam/fast-math") ("glam-core-simd" "glam/core-simd")))) (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

(define-public crate-vector-traits-0.3.4 (c (n "vector-traits") (v "0.3.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "15yzym8rp5cincgij295096n9p47g8kxdvrakpfdlbmmv12ri57p") (f (quote (("glam-fast-math" "glam/fast-math") ("glam-core-simd" "glam/core-simd")))) (s 2) (e (quote (("glam" "dep:glam") ("cgmath" "dep:cgmath"))))))

