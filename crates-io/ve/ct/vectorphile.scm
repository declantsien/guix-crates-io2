(define-module (crates-io ve ct vectorphile) #:use-module (crates-io))

(define-public crate-vectorphile-0.1.0 (c (n "vectorphile") (v "0.1.0") (h "0bb2hkravxpczjbydf62sg3whgw4czdm6iwpc7k9cadi2whq4s3z")))

(define-public crate-vectorphile-0.1.3 (c (n "vectorphile") (v "0.1.3") (h "1cr2m5iwm6jyz0hmcq8qw5fwwgn4dhga1vpiyv62ack4favbxhlb")))

(define-public crate-vectorphile-0.1.5 (c (n "vectorphile") (v "0.1.5") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "16qw2bqsd2c516jyfd8y63wb17rw3nara7kw2g2bh518dhmp6lsx")))

(define-public crate-vectorphile-0.1.6 (c (n "vectorphile") (v "0.1.6") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "0rm8zhv05306kpxm64wf7xlhziw23j2shx2n4ynxiplgzx73pqpi")))

(define-public crate-vectorphile-0.1.7 (c (n "vectorphile") (v "0.1.7") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "0giadva28b9ff0kcawjwpmnam9g2l95x0lzm08fl9vi5x2nb1gz9")))

(define-public crate-vectorphile-0.1.8 (c (n "vectorphile") (v "0.1.8") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "0nb47gq9n851rbgdaka8x8i421fyplfcmc5c5cnrspbx84yy0ld1")))

(define-public crate-vectorphile-0.1.9 (c (n "vectorphile") (v "0.1.9") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "1lylam31206ishj1cz88mlfjf3dac8ajcjz6cyw398jc03pwf2gi")))

(define-public crate-vectorphile-0.1.10 (c (n "vectorphile") (v "0.1.10") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "00kc1nprmz739lhbzgx4fa6lhfayvps9fsliksmbmfhczqacbn5s")))

(define-public crate-vectorphile-0.1.11 (c (n "vectorphile") (v "0.1.11") (d (list (d (n "euclid") (r "^0.16.0") (d #t) (k 0)))) (h "13394q6ycqbapxsxhabnnwfvd23k2wbpf7zjmdhd9q136xw4vz9w")))

