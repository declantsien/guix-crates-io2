(define-module (crates-io ve ct vectorlib) #:use-module (crates-io))

(define-public crate-vectorlib-0.1.0 (c (n "vectorlib") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09ynyv1w3fyr6sxd5xf84w875hc1ndh9jd7skdsm19rvnph8mbvb") (y #t)))

(define-public crate-vectorlib-0.1.1 (c (n "vectorlib") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12qi4nsqnqpl6sfsxgfmg8sl2d0n4nzlinrccqz8s4qagha5xnf2")))

(define-public crate-vectorlib-0.1.2 (c (n "vectorlib") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gvg5ricl6hpzs89rfbhs0db0hvm3f9hhk64mzdmd615s362f3yc")))

