(define-module (crates-io ve ct vectormath) #:use-module (crates-io))

(define-public crate-vectormath-0.1.0 (c (n "vectormath") (v "0.1.0") (h "1qhjmmsb9z5hczihdf5b57f11jr0pipri19if4195dn1xfksi7iv") (y #t)))

(define-public crate-vectormath-0.1.1 (c (n "vectormath") (v "0.1.1") (h "0v3mfkhi12w0kkwhkbcwclf23rn8clgpx37xi2kwji643xcmnn8v") (f (quote (("unstable") ("default")))) (y #t)))

