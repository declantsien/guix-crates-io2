(define-module (crates-io ve ct vector_base) #:use-module (crates-io))

(define-public crate-vector_base-0.1.0 (c (n "vector_base") (v "0.1.0") (d (list (d (n "annoy-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8r9ycv9il5wh87cvdvh2k9zj15wdqd8r80zbwlm7lafqdrn3ck")))

