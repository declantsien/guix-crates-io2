(define-module (crates-io ve ct vectorscan-rs-sys) #:use-module (crates-io))

(define-public crate-vectorscan-rs-sys-0.0.1 (c (n "vectorscan-rs-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1g4078y96yj71fkwrc119vl90cfgrnkmk48r9mc8il7j8lzn31a8") (f (quote (("simd_specialization") ("gen" "bindgen") ("cpu_native")))) (l "hs") (r "1.73")))

(define-public crate-vectorscan-rs-sys-0.0.2 (c (n "vectorscan-rs-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1qkxbf82h1i64fbz2a7kw1lrflyv1iarnffr8n8cf6jab36sawqk") (f (quote (("unit_hyperscan") ("simd_specialization") ("gen" "bindgen") ("cpu_native")))) (l "hs") (r "1.73")))

