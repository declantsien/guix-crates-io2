(define-module (crates-io ve ct vectorial) #:use-module (crates-io))

(define-public crate-vectorial-0.1.0 (c (n "vectorial") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (k 0)) (d (n "ext-ops") (r "^0.1.0") (o #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (k 0)))) (h "1fx629zd2dclp58v6qcx3imhqlr9y3gn3apjdi9x9n652fp9wzcm") (f (quote (("std") ("default" "std"))))))

(define-public crate-vectorial-0.2.0 (c (n "vectorial") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (k 0)) (d (n "ext-ops") (r "^0.1.0") (o #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fxw6d2y3rrv8c1fl09aa1injfw5njk2ciyh07ig2hh93w2xr8a4") (f (quote (("std") ("default" "std"))))))

