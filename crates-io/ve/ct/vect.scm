(define-module (crates-io ve ct vect) #:use-module (crates-io))

(define-public crate-vect-0.1.0 (c (n "vect") (v "0.1.0") (h "1dzvd1d0nid5akkk8cb9k4n4lbrwfdgvq3y3d99pnv0m5fs496bp")))

(define-public crate-vect-0.1.1 (c (n "vect") (v "0.1.1") (h "0si9c6skkf6qyirjk6lr69xbgk0i90g160knk7qzsrm2rg5hwr8s")))

(define-public crate-vect-0.2.0 (c (n "vect") (v "0.2.0") (h "022j4c7a5gq9ayfsx2riqjrqwsxj3c6capx48zf5gzrh80mbj9vn")))

(define-public crate-vect-1.0.0 (c (n "vect") (v "1.0.0") (h "0g3n7qym2n2gpjpq0rjm72rsvkjpgd0cw83hr64crz4bk41csypb")))

(define-public crate-vect-2.0.0 (c (n "vect") (v "2.0.0") (h "0m7l8aprqn7p63mxx5wh95gvxycnyq42w1n6klycajhq8sycywkr")))

