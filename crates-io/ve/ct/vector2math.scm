(define-module (crates-io ve ct vector2math) #:use-module (crates-io))

(define-public crate-vector2math-0.1.0 (c (n "vector2math") (v "0.1.0") (h "0sxlwbcbdil665kf6kk2sd1sq9h9xysx9g55xsf0f8jq2gc6a693")))

(define-public crate-vector2math-0.1.1 (c (n "vector2math") (v "0.1.1") (h "1zsbqyrv4f2v7x4gqzkqqm47hidzrxa2i17pp7434h5srzpprdd4")))

(define-public crate-vector2math-0.2.0 (c (n "vector2math") (v "0.2.0") (h "084n2z3qjfyqhrl1a9wbjzksnks9nrzb8whzz22gm1rfx9lv1d2x")))

(define-public crate-vector2math-0.3.0 (c (n "vector2math") (v "0.3.0") (h "0fhfvw2lw0bmdx91cps5qhyji66c75i7lyi3v5sbq5v6y94jdx9w")))

(define-public crate-vector2math-0.3.1 (c (n "vector2math") (v "0.3.1") (h "1bga3fgm47y3kanjk8ag9jf7cw0s2qgw86im4c1qzgxl9bv4alyp")))

(define-public crate-vector2math-0.3.2 (c (n "vector2math") (v "0.3.2") (h "0y29yg9igd64q06dnklwpxzsj50y3f10ag0s8b22h7byrd0bizw9")))

(define-public crate-vector2math-0.3.3 (c (n "vector2math") (v "0.3.3") (h "0wy0rgg9k6mms4xv8cihwnb0rld3rsrkjcavvjmxqah2dhwz75a1")))

(define-public crate-vector2math-0.3.4 (c (n "vector2math") (v "0.3.4") (h "18m2lkhamk1kcgbgqqv78g8j2kqnjg0snm4zacrdcnqlzvavnp97")))

(define-public crate-vector2math-0.3.5 (c (n "vector2math") (v "0.3.5") (h "09h0mmasl2irp9x835r9vqqwljvyp69rq2avgvrdwgimgalhicn7")))

(define-public crate-vector2math-0.4.0 (c (n "vector2math") (v "0.4.0") (h "022gy6vlmvgrz2b4l7qq42fijisd7q0yms8bg2f5px11b5qfs9v8")))

(define-public crate-vector2math-0.5.0 (c (n "vector2math") (v "0.5.0") (h "1yc7n5rwdmya7p0lynsr0v7660p86dprixxc4cbyyhrr5b4px4z0")))

(define-public crate-vector2math-0.6.0 (c (n "vector2math") (v "0.6.0") (h "0mfbxc6yndwc9125ayh5600i5gwmpgslj7iwgh58p375ip11acr4")))

(define-public crate-vector2math-0.6.1 (c (n "vector2math") (v "0.6.1") (h "0f1gjzyrivflcd7jgvv3gczm1zakqk8vaxclr09plifxzrfsxnny")))

(define-public crate-vector2math-0.7.0 (c (n "vector2math") (v "0.7.0") (h "1778b6s9647jnjh25aakrqgiksqqjswld10lbcg805lmwhpwqy9p")))

(define-public crate-vector2math-0.8.0 (c (n "vector2math") (v "0.8.0") (h "09bq4ri3ii98929zmnkpk6khb9mwi001483jaqlvgz2d5y0id96k")))

(define-public crate-vector2math-0.8.1 (c (n "vector2math") (v "0.8.1") (h "13kxaizh38n6079bq57f9d5s145kg59w2pwvyvljsf26hs6p08l7")))

(define-public crate-vector2math-0.9.0 (c (n "vector2math") (v "0.9.0") (h "12qnb92viqy15rmdrcbz3g83xjxwd36zx0vrbh6q61kz18s24i2n")))

(define-public crate-vector2math-0.10.0 (c (n "vector2math") (v "0.10.0") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "04b3cnmf75qqk6nhydy4sv1faqm97krvxbh7i44kwlbhf10zjk31") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.1 (c (n "vector2math") (v "0.10.1") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "07y0idw46cqad1v0dv7yd1dzb6mjgzq6m2ig2hdikaw777miz10j") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.2 (c (n "vector2math") (v "0.10.2") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "00fdgr1sr5dv9g0dld3ghmhqa2ki9163mgg7b1pigvv5p56y1dr5") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.3 (c (n "vector2math") (v "0.10.3") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "1bm8zd60qh6vpahc1vrdld3izyphd33cz0gdcx026032afb5a95c") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.4 (c (n "vector2math") (v "0.10.4") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "1785gykww1bjxnmbfa29irrj0z6a6byg80zc1f7ih1jgf154g8h2") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.5 (c (n "vector2math") (v "0.10.5") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "170r9qfnbzc8vzlmbj02li5z10vyjgmi10ciffhc7g1f7bv7zbn0") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.10.6 (c (n "vector2math") (v "0.10.6") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "039n4q4yr7v9xn1nwxqcj3627xv7bifz23a3nrlh9jhrr4ccf8xr") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.11.0 (c (n "vector2math") (v "0.11.0") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "0r5rkjrshdq2x1llcrzviwih4ic95cxzdw3jdfkg66dcm9qdskxy") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.11.1 (c (n "vector2math") (v "0.11.1") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "1q92ldhbskd0ad3f2y5adl8mzfvklir40i63mwcjwbmx004jhqhk") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.11.2 (c (n "vector2math") (v "0.11.2") (d (list (d (n "packed_simd") (r "^0.3.4") (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "1wq1i3dzp6dk784mfwh51af6fdixbqic0j896q8cjwjwqmc9vm99") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.12.0 (c (n "vector2math") (v "0.12.0") (d (list (d (n "packed_simd") (r "^0.3.4") (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "0vnwp78kyb0p6s5a8hypik3kjpligh9fz9kpwswkg1s8y5s4jbz6") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.13.0 (c (n "vector2math") (v "0.13.0") (d (list (d (n "packed_simd") (r "^0.3.4") (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "0871h58fzcb7r3jhn4xb39ljb10wrsg4xxap8rz9d9kb38w58q4y") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.13.1 (c (n "vector2math") (v "0.13.1") (d (list (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "0jyc1l964hh8smbzm22jajvdak4gryypdhc6v5pyah4lygnvjvly") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.14.0 (c (n "vector2math") (v "0.14.0") (d (list (d (n "packed_simd") (r "^0.3.9") (o #t) (d #t) (k 0)))) (h "1r56ag55fh55vj002b7l5rh5lgr8l3iyy7hr8l6h21s26bxz213r") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.14.1 (c (n "vector2math") (v "0.14.1") (d (list (d (n "packed_simd") (r "^0.3.9") (o #t) (d #t) (k 0)))) (h "0rd4gphsraay1rbsvxq30jkxhzm81jxcy9z3iz1c2a9g28fyar63") (f (quote (("simd" "packed_simd") ("default"))))))

(define-public crate-vector2math-0.14.2 (c (n "vector2math") (v "0.14.2") (d (list (d (n "packed_simd") (r "^0.3.9") (o #t) (d #t) (k 0)))) (h "0g8mcy4lqhw2cilw68frmq29y0kpjjj330dv8ljvq1jcjl2wc2kb") (f (quote (("simd" "packed_simd") ("default"))))))

