(define-module (crates-io ve ct vecto) #:use-module (crates-io))

(define-public crate-vecto-0.1.0 (c (n "vecto") (v "0.1.0") (d (list (d (n "umath") (r "^0.0.7") (d #t) (k 0)))) (h "0ryqv9x6ajj9fv24hx2ilwvj9bjcbb5cgnx8nvhvz2wk7m9lcs01")))

(define-public crate-vecto-0.1.1 (c (n "vecto") (v "0.1.1") (d (list (d (n "umath") (r "^0.0.7") (d #t) (k 0)))) (h "0pdpdbii7yrqafha5xammjzhcp9yn75xd9bylqy0v8m8g2ck7wf9")))

