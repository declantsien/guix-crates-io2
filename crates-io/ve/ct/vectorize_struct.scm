(define-module (crates-io ve ct vectorize_struct) #:use-module (crates-io))

(define-public crate-vectorize_struct-0.1.0 (c (n "vectorize_struct") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.5") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1by25qv6hx0nc284csbr0pzsi1288j68mha8i1amd298f4x9rg3g")))

(define-public crate-vectorize_struct-0.1.1 (c (n "vectorize_struct") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.5") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0f2qzyv1m9s11qnzyjb5p82ix5js40fh867pwxjq3p58pmj5gjrg")))

(define-public crate-vectorize_struct-0.1.2 (c (n "vectorize_struct") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.5") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ay8kgdibw8l2a63wpjpjlg5cl0qa4lc6s17323g4z4isa7cwla2")))

(define-public crate-vectorize_struct-0.1.3 (c (n "vectorize_struct") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.5") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03j9a0m4b1dps61p5nzwzkhx4za9gz5vd68d1r3fgbr8kbrrhwar")))

