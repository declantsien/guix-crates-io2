(define-module (crates-io ve ct vector-basis) #:use-module (crates-io))

(define-public crate-vector-basis-0.1.0 (c (n "vector-basis") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "03zv3s2744s9y5algr1r1l5973akmh0wjlx5d0h4m07pgjimb4gy")))

