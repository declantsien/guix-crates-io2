(define-module (crates-io ve ct vector2d) #:use-module (crates-io))

(define-public crate-vector2d-1.1.3 (c (n "vector2d") (v "1.1.3") (d (list (d (n "proc_vector2d") (r "^1.0.1") (d #t) (k 0)))) (h "1y93d9sx9bdhhswfna8p5silm13yj6p3ibygp2mx8iwk4fiwc08z")))

(define-public crate-vector2d-2.0.1 (c (n "vector2d") (v "2.0.1") (d (list (d (n "proc_vector2d") (r "^1.0.1") (d #t) (k 0)))) (h "06sa2c2pacf0qcdi6kypd1k6xnykvfx4dv938fnvgkvihc09nalf")))

(define-public crate-vector2d-2.2.0 (c (n "vector2d") (v "2.2.0") (d (list (d (n "proc_vector2d") (r "^1.0.2") (d #t) (k 0)))) (h "0n0kdr63p0h4vxxyc8825dhn2dc2kzd171n58ishpm9mmhg4iv5h")))

