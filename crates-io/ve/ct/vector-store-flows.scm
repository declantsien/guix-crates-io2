(define-module (crates-io ve ct vector-store-flows) #:use-module (crates-io))

(define-public crate-vector-store-flows-0.1.0 (c (n "vector-store-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "02g8s7rxqdkdjzrxxv6ckysaab18kclm1z6pzr1m6zglgi9az6bm")))

(define-public crate-vector-store-flows-0.1.1 (c (n "vector-store-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0vmpl1rxs2qymzbgsbcrkcx79va127b0h1n10jp7vi07ym7kcsaa")))

(define-public crate-vector-store-flows-0.1.2 (c (n "vector-store-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1wgwrndrzw6w94cpkn88nx416y2xk2cjnywnfbnsyc4l5qhv1650")))

