(define-module (crates-io ve ct vector-editor-core) #:use-module (crates-io))

(define-public crate-vector-editor-core-0.1.0 (c (n "vector-editor-core") (v "0.1.0") (d (list (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "touch-selection") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "044lkl7wh4bl3nxq3iybqm54w1zjdhzmyx77i672qgv4lsyap48f")))

(define-public crate-vector-editor-core-0.1.1 (c (n "vector-editor-core") (v "0.1.1") (d (list (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "touch-selection") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "0iwv0yp8qds1b0kxzqradni6yl3lnkqz8gi8n592wsgxsgc0pngq")))

