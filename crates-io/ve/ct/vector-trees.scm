(define-module (crates-io ve ct vector-trees) #:use-module (crates-io))

(define-public crate-vector-trees-0.1.0 (c (n "vector-trees") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.4.0") (f (quote ("collections"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "17bvk3rv993nilslq5in4n87ykhv4vv5jn8vd0mag0wvq9xds9g1") (f (quote (("default") ("avltree"))))))

