(define-module (crates-io ve ct vector3d) #:use-module (crates-io))

(define-public crate-vector3d-0.1.1 (c (n "vector3d") (v "0.1.1") (d (list (d (n "clapme") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1is5kpz6ijw44k6jaaa682xlca6pa08jqkjrhm83w8cyc1ak5q9i") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.2 (c (n "vector3d") (v "0.1.2") (d (list (d (n "clapme") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04ab0z3c1m55p7pxgnihbj82vzq757za60v5jv122qx7bzph2pc4") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.3 (c (n "vector3d") (v "0.1.3") (d (list (d (n "clapme") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04h3dd5vq26l7wwb97w6f0cch0wishd79g5bwnc2p5wz87aw3a8h") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.4 (c (n "vector3d") (v "0.1.4") (d (list (d (n "clapme") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rh6vf9asmim25mh536f6vgmdsadkrgqslpms7747hivv0y0k7y1") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.5 (c (n "vector3d") (v "0.1.5") (d (list (d (n "clapme") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0z464qfhpacdn83bqwxq8vhqwb83msjdklbsp3dnkxkm1ps75dql") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.6 (c (n "vector3d") (v "0.1.6") (d (list (d (n "clapme") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w5xm010yxx7jvsnxln046zb8mp37d3i7fjq2hsqwni68fs6fpn8") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.7 (c (n "vector3d") (v "0.1.7") (d (list (d (n "clapme") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00chf6kbcfbn61mb2ml4nr244vp1h8vrjianbzwjcr81cla146sh") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.8 (c (n "vector3d") (v "0.1.8") (d (list (d (n "clapme") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0j46ad1dni9rmmxygxyjmxd2pkd75ndq1c0z4ri179kb9773fcc2") (f (quote (("strict"))))))

(define-public crate-vector3d-0.1.9 (c (n "vector3d") (v "0.1.9") (d (list (d (n "auto-args") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "clapme") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mdb8g9jzg44qjxpx5n01wlc05pvaabfvyfz1m0g1af80hp5ycnm") (f (quote (("strict") ("serde1" "serde" "serde_derive") ("default" "serde1" "clapme") ("auto_args" "auto-args"))))))

(define-public crate-vector3d-0.2.0 (c (n "vector3d") (v "0.2.0") (d (list (d (n "auto-args") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "clapme") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16xwvyziynivbp2l0qwgw7wk1lg33601rp0fp105vk9xq80xcjnl") (f (quote (("strict") ("serde1" "serde" "serde_derive") ("default" "serde1"))))))

(define-public crate-vector3d-0.2.1 (c (n "vector3d") (v "0.2.1") (d (list (d (n "auto-args") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "clapme") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ql7gkq8gyk7pagfn4acby0l5q9943k8hwnhmg469zmhgd0fpmzv") (f (quote (("strict") ("serde1" "serde" "serde_derive") ("default" "serde1"))))))

