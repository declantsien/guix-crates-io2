(define-module (crates-io ve ct vector-map) #:use-module (crates-io))

(define-public crate-vector-map-0.1.0 (c (n "vector-map") (v "0.1.0") (h "1givih7grzsp691vzxxcbs5hyb1lqnkc8rc9mfk9pqbpwzj68043")))

(define-public crate-vector-map-0.2.0 (c (n "vector-map") (v "0.2.0") (h "1iami4f5zlf0jxjs076yrp7myfkyhn88a503nkrskny8g3v2yj9w")))

(define-public crate-vector-map-0.3.0 (c (n "vector-map") (v "0.3.0") (h "08fn0a4lh9390r9wccy32qzrcr3igw0dbx5l9fbrqvlmb9hmhamq")))

(define-public crate-vector-map-0.4.0 (c (n "vector-map") (v "0.4.0") (h "1qj2x2y6k374h5r6n8sfjssq8dk517xqgrmw1zwryy4vps7k9q5c")))

(define-public crate-vector-map-0.5.0 (c (n "vector-map") (v "0.5.0") (h "03b31sy2d3pmyl9d7gjy7gj9bc9dhfxppn472f6xr85fwf9la4vc") (f (quote (("nightly"))))))

(define-public crate-vector-map-0.5.1 (c (n "vector-map") (v "0.5.1") (h "0lr0n5nbkgns63r76gzwrnh55b9vixk9i6ynd1cd7gmnjawykjms") (f (quote (("nightly"))))))

(define-public crate-vector-map-0.5.2 (c (n "vector-map") (v "0.5.2") (h "02yhspgilhxyhjzj7rvqkrd9gwf0605kc6p59idb7s9vw261n2vz") (f (quote (("nightly"))))))

(define-public crate-vector-map-0.6.0 (c (n "vector-map") (v "0.6.0") (h "1xdw2clp9xacb3hrjnj8rlp2bx185hbz4awd1k454wlll1mf7h42") (f (quote (("nightly"))))))

(define-public crate-vector-map-1.0.0 (c (n "vector-map") (v "1.0.0") (d (list (d (n "contracts") (r "^0.4") (d #t) (k 0)) (d (n "linear-map") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "062k5qd6w17syzh4cfhckj2x9znr0lfsgnzwpapj20dxd82ab5id") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("enable_contracts") ("default" "contracts/disable_contracts"))))))

(define-public crate-vector-map-1.0.1 (c (n "vector-map") (v "1.0.1") (d (list (d (n "contracts") (r "^0.4") (d #t) (k 0)) (d (n "linear-map") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "136n1j5h86dd75947amxz5ghp7qpqkk0k1qq74hhwp54jjp743sm") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("enable_contracts") ("default" "contracts/disable_contracts"))))))

