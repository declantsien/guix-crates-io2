(define-module (crates-io ve ct vectrix) #:use-module (crates-io))

(define-public crate-vectrix-0.1.0 (c (n "vectrix") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "vectrix-macro") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "1zqnpvhb4nkc4q51bzrd18xdkh98scz0i60m91x0vzg0vw3fbkcq") (f (quote (("std") ("macro" "vectrix-macro") ("default" "macro" "std"))))))

(define-public crate-vectrix-0.1.1 (c (n "vectrix") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "vectrix-macro") (r "=0.1.1") (o #t) (d #t) (k 0)))) (h "0pwcq1x0539fjsgq4g235l5li71adjc7ax287akihl6zplb84ccw") (f (quote (("std") ("macro" "vectrix-macro") ("default" "macro" "std"))))))

(define-public crate-vectrix-0.1.2 (c (n "vectrix") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "vectrix-macro") (r "=0.1.2") (o #t) (d #t) (k 0)))) (h "1r9847skfch8si469i1265adr4ia646jwxkj1x47z62rv6bdh7cx") (f (quote (("std") ("macro" "vectrix-macro") ("default" "macro" "std"))))))

(define-public crate-vectrix-0.1.3 (c (n "vectrix") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "stride") (r "^0.1.1") (d #t) (k 0)) (d (n "vectrix-macro") (r "=0.1.2") (o #t) (d #t) (k 0)))) (h "1v14n1dk8hdjbh4kpxzfzckmn952ryx86101hyvrxvl24hgvgz07") (f (quote (("std") ("macro" "vectrix-macro") ("default" "macro" "std"))))))

(define-public crate-vectrix-0.2.0 (c (n "vectrix") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 2)) (d (n "stride") (r "=0.2.0") (d #t) (k 0)) (d (n "vectrix-macro") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "1m191klrl9shfba5rha45ckisa57ca0zkx1d58yj0kw4d6qhddig") (f (quote (("std") ("macro" "vectrix-macro") ("default" "macro" "std"))))))

(define-public crate-vectrix-0.3.0 (c (n "vectrix") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 2)) (d (n "stride") (r "^0.3.0") (d #t) (k 0)) (d (n "vectrix-macro") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1mpyavc84kcix60d36bn6fahfg54rw0qhc1w09bri47dll3ygnai") (f (quote (("std") ("default" "macro" "std")))) (s 2) (e (quote (("macro" "dep:vectrix-macro"))))))

