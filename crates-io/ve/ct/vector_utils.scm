(define-module (crates-io ve ct vector_utils) #:use-module (crates-io))

(define-public crate-vector_utils-0.1.0 (c (n "vector_utils") (v "0.1.0") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0qa592as0ajr98ijzj1k835wlprw096shygdr38hp4s3q4s0rdci")))

(define-public crate-vector_utils-0.1.1 (c (n "vector_utils") (v "0.1.1") (d (list (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "03pvyf1m1r1wl2a32l3p7gz9y1ck5iz5kcjzf8gjjwgkgzi2cq9w")))

(define-public crate-vector_utils-0.1.2 (c (n "vector_utils") (v "0.1.2") (d (list (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0fwd8w6xlw768gisbxx0mf0pyylika0riz1ss9a20aagmxnz9xf2")))

(define-public crate-vector_utils-0.1.3 (c (n "vector_utils") (v "0.1.3") (d (list (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0pvvf1nph4z1cqcxhvd9iys4x07ivc33g4qi1dsj2pcvz0hp040f")))

(define-public crate-vector_utils-0.1.5 (c (n "vector_utils") (v "0.1.5") (d (list (d (n "permutation") (r "^0.2") (d #t) (k 0)) (d (n "superslice") (r "^1") (d #t) (k 0)))) (h "0n9h1lhbm9yi31vv8595sa80lqb3n2xpmq0znhndvz8vw67cygid")))

