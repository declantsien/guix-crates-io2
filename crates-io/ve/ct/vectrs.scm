(define-module (crates-io ve ct vectrs) #:use-module (crates-io))

(define-public crate-vectrs-0.0.0 (c (n "vectrs") (v "0.0.0") (h "05240mw1yy7v2hcqiyxvw0s9wh42hz0kyq7iq8lakv1s2r0njf55")))

(define-public crate-vectrs-0.0.1 (c (n "vectrs") (v "0.0.1") (h "0p0ng745wxplqss4ys3ia7zqaf9lnqpxxv59n0h41b8wsab99732")))

(define-public crate-vectrs-0.0.2 (c (n "vectrs") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "11p63w1ggip6iwc55af3clvv74cs8s0xiipbcvaa6fwiyghl2bj9")))

(define-public crate-vectrs-0.0.3 (c (n "vectrs") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "112q2n46q9069rab7d2a4gg5rgmrvjd4zdjww8vlfrw64yppimpk")))

(define-public crate-vectrs-0.0.4 (c (n "vectrs") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1lrdp5lhh3sxyd6m2dw9q21wgcqx4862l12l77y3di0wvkyqk820")))

(define-public crate-vectrs-0.0.5 (c (n "vectrs") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "18p041a1mqfgdzak6dfysibiqv65gl9nwmagjkr2jy7lw1wr4661") (f (quote (("std") ("default" "std"))))))

(define-public crate-vectrs-0.0.6 (c (n "vectrs") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0n2x3k87w3k26a8lsqfgd7z4yjrad2nzgpn56grxni39lrqppdnh") (f (quote (("std") ("default" "std"))))))

(define-public crate-vectrs-0.0.7 (c (n "vectrs") (v "0.0.7") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0g0pgyp1mgppkjhpxnzr7i1x1bhfwa0mc0znfalkimwf64yl7lf8") (f (quote (("std") ("default" "std"))))))

