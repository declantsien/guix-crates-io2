(define-module (crates-io ve c3 vec3d) #:use-module (crates-io))

(define-public crate-vec3D-0.1.0 (c (n "vec3D") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)))) (h "0qriwhh3afkz2j2ixp9408k8ab7wv8bwi5si1s8klqzjar3qrak1")))

(define-public crate-vec3D-0.2.0 (c (n "vec3D") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)))) (h "0hjw4xblz62sfa9illc0xybv047g464c95xq41yrrs746f9wmx1w")))

(define-public crate-vec3D-0.3.0 (c (n "vec3D") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)))) (h "0v94w1vdxb488c3xh4jf4q8ypsvkfqavw2jswaz4ikqn7686jpyb")))

