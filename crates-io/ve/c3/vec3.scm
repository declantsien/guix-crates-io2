(define-module (crates-io ve c3 vec3) #:use-module (crates-io))

(define-public crate-vec3-0.1.0 (c (n "vec3") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "0q4vns7j2fwb3k40nip6x6smyiva89rnamkcdg9fqva6y0vsajw7")))

(define-public crate-vec3-0.1.1 (c (n "vec3") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "1qniqm87m4mxcv63wxlxjzcdzz0rickj0ijz3a6k7bfhy8jl1n7x")))

(define-public crate-vec3-0.1.2 (c (n "vec3") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "0pnc39qdyf8896dgbi1d86djj7qgsiwr08gfvfsxc4xzxrqja4n9")))

(define-public crate-vec3-0.1.3 (c (n "vec3") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1x3cfm67vbdcyg2drwz9hdsxyxhjp16k5jvnqsj2yl5i28vrw85s")))

(define-public crate-vec3-0.1.4 (c (n "vec3") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "0b17cfi3f2n9djl2ix4895nnhcn3ss7kiy54nqvk3pdx2qfaywg5") (y #t)))

(define-public crate-vec3-0.1.5 (c (n "vec3") (v "0.1.5") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1d1i86r4rgnx6hx9blsmv9i4akdpcjm426w73c1kncm02w6ra8fh")))

(define-public crate-vec3-0.1.6 (c (n "vec3") (v "0.1.6") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "17qsmz20hdzrwplr6g12q2s5bp5aymznz9fmj1rdpm7lm9zs00bf")))

(define-public crate-vec3-0.2.0 (c (n "vec3") (v "0.2.0") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0szmqz3v4z6dwb2apl126c1q422a2vxwjr54xgz165xg9pdnhi11")))

(define-public crate-vec3-0.2.1 (c (n "vec3") (v "0.2.1") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qa90j9x2dcs4lzs0m7sf69cxdh34b7gjg71d5kwhdmwpk7m5nba")))

