(define-module (crates-io ve c3 vec3-rs) #:use-module (crates-io))

(define-public crate-vec3-rs-0.1.0 (c (n "vec3-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1d7l54w6dlnz5m6h28syi6ghxrzdsnm92xjvh75s90f8l529flvf")))

(define-public crate-vec3-rs-0.1.1 (c (n "vec3-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0r4q0pq04pbz1bgfhv1w52rk9x4zy937p7r2zx4z31svspxiy1df")))

(define-public crate-vec3-rs-0.1.2 (c (n "vec3-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "069pr2f7724xa598ymv1wjwdzf0wlw6s1ll96lgjlz5mqy1mjajs")))

(define-public crate-vec3-rs-0.1.3 (c (n "vec3-rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0qgpqwfxbmvjdsr5dbzlbm5bcg9ny3x15qri4h9hzqv2zm033ikz")))

(define-public crate-vec3-rs-0.1.4 (c (n "vec3-rs") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0d460rvbj0h37ajsi3qsfnn1ahgw3v30ni5cpcwzanz5n2ydv91b")))

(define-public crate-vec3-rs-0.1.5 (c (n "vec3-rs") (v "0.1.5") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "05pnzgvdkjp25l2iyqfc8krkhn7y5fcn9m0sh7lrw7323hsc4kxb")))

(define-public crate-vec3-rs-0.1.6 (c (n "vec3-rs") (v "0.1.6") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "10hxjcnffs8a1f29p8vxx788grc35qn316b86mnnp39ar16nw9ms")))

