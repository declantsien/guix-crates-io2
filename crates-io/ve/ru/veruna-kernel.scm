(define-module (crates-io ve ru veruna-kernel) #:use-module (crates-io))

(define-public crate-veruna-kernel-1.1.0 (c (n "veruna-kernel") (v "1.1.0") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0d4lmrhdjb475kjx6hlpkws7008avpn9r33c15c3pfjgbw3h97nr") (y #t)))

(define-public crate-veruna-kernel-1.1.1 (c (n "veruna-kernel") (v "1.1.1") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0yhf7hs7ixdkz8xb7shb5javjrjqwdf3nmb0gm3fpvfjn88yrg0g") (y #t)))

(define-public crate-veruna-kernel-1.1.2 (c (n "veruna-kernel") (v "1.1.2") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "18gka5hs73f6jkwnkkk85a1wlh1fzam23hwdrwldr3fvxhpr77dh") (y #t)))

(define-public crate-veruna-kernel-1.1.3 (c (n "veruna-kernel") (v "1.1.3") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0c7nyn5sf15sksa0rxg4w31w2f3mfcfmi9a2gq8fcg45yq8z3cv5") (y #t)))

(define-public crate-veruna-kernel-1.1.4 (c (n "veruna-kernel") (v "1.1.4") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "118sd2gj59bv8iskphkgmi6mirjxrkf1aqa1hg1l0rk3cilq2m4b") (y #t)))

(define-public crate-veruna-kernel-1.1.5 (c (n "veruna-kernel") (v "1.1.5") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0hiik7k37h8qn48zn5yz69nc8xrsy8i3510h0wdksw16xi028zl8") (y #t)))

(define-public crate-veruna-kernel-1.1.6 (c (n "veruna-kernel") (v "1.1.6") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "18va2lk2zkvlnb4bljz084xm4m9njlhwal7ah0hv9bhx9hcbbks6") (y #t)))

(define-public crate-veruna-kernel-1.1.7 (c (n "veruna-kernel") (v "1.1.7") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1r3fysngwd7rbirrk3jv45p2bl8f4k63dhir6mpfvi31bcjk7n5f") (y #t)))

(define-public crate-veruna-kernel-1.1.8 (c (n "veruna-kernel") (v "1.1.8") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zfi351hzrr60pijmm85yaglz318sk6s7p43mxi1w1j94gd63qlv") (y #t)))

(define-public crate-veruna-kernel-1.1.9 (c (n "veruna-kernel") (v "1.1.9") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "021kcrh18z5rgjhz7b6dddvfxl9w4n5s5kbmbf3745ybcsdpl3kq") (y #t)))

(define-public crate-veruna-kernel-1.1.10 (c (n "veruna-kernel") (v "1.1.10") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0vk6a4bfwf9nzf3ma8xslnzb6glnd2qim7vhhq05dqljbx26jfp5") (y #t)))

(define-public crate-veruna-kernel-1.1.11 (c (n "veruna-kernel") (v "1.1.11") (d (list (d (n "shaku") (r "0.6.*") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0vr8gsf25hp6kq7d2ndiiqxyy7dv8ir7mwmsa5fnj191zq0hab51") (y #t)))

(define-public crate-veruna-kernel-1.1.12 (c (n "veruna-kernel") (v "1.1.12") (d (list (d (n "shaku") (r "0.6.*") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "12bjar1jzrb6558f8993lmxc018ghdnrf1kryx6aa6haypryw3d7") (y #t)))

(define-public crate-veruna-kernel-1.1.13 (c (n "veruna-kernel") (v "1.1.13") (d (list (d (n "shaku") (r "0.6.*") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1xqmzi5cnwbv253py3m2pwwm620vhpw3a57laxnvv4ywb6k78w7a") (y #t)))

(define-public crate-veruna-kernel-1.1.14 (c (n "veruna-kernel") (v "1.1.14") (d (list (d (n "shaku") (r "0.6.*") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0r402c925im0vw5pqhxyivy64fvm210qn8n5khip7ri5l67nw0l0") (y #t)))

(define-public crate-veruna-kernel-1.1.15 (c (n "veruna-kernel") (v "1.1.15") (d (list (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1fhh2wwj34rx6l9aahqmnr29pkiwb3zjlscgg380r8rd293sc7s1") (y #t)))

(define-public crate-veruna-kernel-1.1.16 (c (n "veruna-kernel") (v "1.1.16") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1gd8va82mc9bpwivnv457glck80izvljdyfa6cpijgdswjiripc9") (y #t)))

