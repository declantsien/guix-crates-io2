(define-module (crates-io ve ru verust) #:use-module (crates-io))

(define-public crate-verust-0.1.0 (c (n "verust") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1r8jy3zx1x87mhq1ardi8hngyhn3v06w5lfd0g0hfy66n3wpibas")))

(define-public crate-verust-0.2.4 (c (n "verust") (v "0.2.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0ip96j5k5flm79i1ygl6r3qa6f6klx96pbf28b80x7idm7pc3bqa")))

