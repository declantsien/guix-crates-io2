(define-module (crates-io ve ru veruna-data) #:use-module (crates-io))

(define-public crate-veruna-data-1.0.1 (c (n "veruna-data") (v "1.0.1") (d (list (d (n "sea-orm") (r "^0") (f (quote ("sqlx-sqlite" "runtime-tokio-native-tls" "macros" "debug-print"))) (k 0)) (d (n "veruna-kernel") (r "^1.1.15") (d #t) (k 0)))) (h "09rb362l9qhzszzhz6v6qyfhfwcjx7l26crapcyp8l3fd2wqw7w1") (y #t)))

