(define-module (crates-io ve ru veruna) #:use-module (crates-io))

(define-public crate-veruna-1.0.12 (c (n "veruna") (v "1.0.12") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "assert-str") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "veruna-data") (r "^1.0.1") (d #t) (k 0)) (d (n "veruna-kernel") (r "^1.1.15") (d #t) (k 0)))) (h "091mfzsdpld0fmpn0acsbgk2d2jhhxhy5j80wpc2xpk47da8nyjd") (y #t)))

(define-public crate-veruna-1.0.0 (c (n "veruna") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "assert-str") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "veruna-data") (r "^1.0.1") (d #t) (k 0)) (d (n "veruna-kernel") (r "^1.1.15") (d #t) (k 0)))) (h "1j81a9dgpckdhpnpgrdr1ybff9wirsjyq8q4fp12mi65qw8kbzf2") (y #t)))

