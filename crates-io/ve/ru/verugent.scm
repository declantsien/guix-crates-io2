(define-module (crates-io ve ru verugent) #:use-module (crates-io))

(define-public crate-verugent-0.1.0 (c (n "verugent") (v "0.1.0") (h "0cvs6ykw6n04wl64pfkda8pg3nz7df8mr5zw8dx7sn31j2vg9y8s") (y #t)))

(define-public crate-verugent-0.1.1 (c (n "verugent") (v "0.1.1") (h "1s7gb20w0ym1y4g2fic3s9f43ifk01hdi212q8i11pz9wr2rsfp3")))

(define-public crate-verugent-0.1.2 (c (n "verugent") (v "0.1.2") (h "1p41j2qh79lj8fv2xkd277ikyq5x4fiahxna2cjdi82kb8rzxbmx")))

(define-public crate-verugent-0.1.3 (c (n "verugent") (v "0.1.3") (h "032z01vz0vjb491y60phhq1za3wy4pan163n1ida48fq3744cxky")))

