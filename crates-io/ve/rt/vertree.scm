(define-module (crates-io ve rt vertree) #:use-module (crates-io))

(define-public crate-vertree-0.1.0 (c (n "vertree") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.7") (d #t) (k 0)))) (h "12jsc7gs92j583mp16chnpl933cl6wngwxnysyf17h8mjh08884v")))

(define-public crate-vertree-0.1.1 (c (n "vertree") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.7") (d #t) (k 0)))) (h "1drvdv3fplj9ammj1ingvfyf0j0xa0y63hvvh4wnzh2yq6j9vf38")))

(define-public crate-vertree-0.1.2 (c (n "vertree") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.7") (d #t) (k 0)))) (h "1nl0g89z5imgp1j5w4pnsj1q3diaywzri1a7m6ahj84s4plc0x1m")))

(define-public crate-vertree-0.2.0 (c (n "vertree") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "06jyxrjm6bpfkwvz74jq552zkfnimpmw8wp5r5qs97ln4d9if18l")))

(define-public crate-vertree-0.2.1 (c (n "vertree") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "1vav71inbh2sz6z8rgcv179s4xa98529j6dqs2111x16f3yvcdc1")))

(define-public crate-vertree-0.2.2 (c (n "vertree") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "1aylqnc1pc4r2qv71d6l11rkpx9a02r15pcv8kihc39q7nsa3j4y")))

