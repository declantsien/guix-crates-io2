(define-module (crates-io ve rt vert-macros) #:use-module (crates-io))

(define-public crate-vert-macros-0.1.1 (c (n "vert-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ldzfp5fw302chml7yr5bv8ia6ly22yi38yf05m38hardk0v9q9h")))

