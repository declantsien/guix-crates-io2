(define-module (crates-io ve rt vertx-tcp-eventbus-bridge-client-rust) #:use-module (crates-io))

(define-public crate-vertx-tcp-eventbus-bridge-client-rust-0.1.0 (c (n "vertx-tcp-eventbus-bridge-client-rust") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05k5g49114a7wkrxvmm8xr4n9dmns3c3mjv2sfinv2p42c3dl8g6")))

(define-public crate-vertx-tcp-eventbus-bridge-client-rust-0.2.0 (c (n "vertx-tcp-eventbus-bridge-client-rust") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0q6a171md5rskspd7gxbcs215s15l9v8wqrg27dvsf4wzdlbqnf8")))

