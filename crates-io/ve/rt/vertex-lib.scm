(define-module (crates-io ve rt vertex-lib) #:use-module (crates-io))

(define-public crate-vertex-lib-0.1.0 (c (n "vertex-lib") (v "0.1.0") (h "0k8l4ymv04fxa9ljziw4qdasfzxrq8gagc54730qb104wvf0sfca")))

(define-public crate-vertex-lib-0.1.1 (c (n "vertex-lib") (v "0.1.1") (h "0yz0pa636wfn62dmzsq3h06srz6i5x2344q6q5lxg00vq72w3kq6")))

(define-public crate-vertex-lib-0.1.2 (c (n "vertex-lib") (v "0.1.2") (h "194y1r7nlsljpix2fghclxgnc215d7jnq93q1dj12yzvlnl7cd84")))

(define-public crate-vertex-lib-0.1.3 (c (n "vertex-lib") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1xvy3kzhc30dab4jjimq28fabqpr0y5npfjjld1x87za5y1sqf49")))

(define-public crate-vertex-lib-0.1.4 (c (n "vertex-lib") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "05418yhyi7b2jgzmj6qsj167afz0lny2v97qxafw06fxwbpk53g7")))

