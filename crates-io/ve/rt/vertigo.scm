(define-module (crates-io ve rt vertigo) #:use-module (crates-io))

(define-public crate-vertigo-0.1.0-alpha.0 (c (n "vertigo") (v "0.1.0-alpha.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0sg3494yxrkr6js888f3kshy4x9znls2y8y6k3rmfxmr9j7zndjp")))

(define-public crate-vertigo-0.1.0-alpha.1 (c (n "vertigo") (v "0.1.0-alpha.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1yayv5glgi92fdd5kxvm38zg1vh07cvciz8rdz2sg7c2ny885ff0")))

(define-public crate-vertigo-0.1.0-alpha.2 (c (n "vertigo") (v "0.1.0-alpha.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "05hkbscghlv23yll6dajmgmcwrxdcsp9gdh488mhncpgh1bah3wd")))

(define-public crate-vertigo-0.1.0-alpha.3 (c (n "vertigo") (v "0.1.0-alpha.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "0q7p12x52vyvzfxznjfk1na82rwwip3857r0lx4pwnrw7d3grwq7") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0-beta.1 (c (n "vertigo") (v "0.1.0-beta.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "0h55yc1liy48p7kqyidgck8ygd360ij38s2j33n2m4psgp1iw7xd") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0-beta.2 (c (n "vertigo") (v "0.1.0-beta.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "1rii64pl1br33bi9p4khh5xd5d2za33g409ln9p33k3v627v83pm") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0-beta.3 (c (n "vertigo") (v "0.1.0-beta.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-beta.3") (d #t) (k 0)))) (h "08xf0fj0kf6vcihnv4jzfdrpcr34bh3nvfwjr0wg3z0pzv2pigzc") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0-beta.4 (c (n "vertigo") (v "0.1.0-beta.4") (d (list (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "1r9vx4q1sp1b5f7c5k0938rn5kgyx0ib78b4agln3qi4kfkk382v") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0-beta.5 (c (n "vertigo") (v "0.1.0-beta.5") (d (list (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0-beta.5") (d #t) (k 0)))) (h "1jmn5i6ncpbg694rndfw2zsdq45r6g5bs86hfjylbb5h1aipz99r") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.0 (c (n "vertigo") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1v43vhjr506di1cg24f7rmbyhchs2zwszkxh09mb85hnrngxq6ky") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.1.1 (c (n "vertigo") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0n297dgw3rhdv8xf0l9ayirdfxjl1hlfqp2ja0lqgj0pfxarl1kp") (f (quote (("serde_request" "serde_json"))))))

(define-public crate-vertigo-0.2.0-alpha (c (n "vertigo") (v "0.2.0-alpha") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1yiip9m1z93dkdqdm2kdm71259qlam70xdgqzzhgsyzvmi0h133b")))

(define-public crate-vertigo-0.2.0 (c (n "vertigo") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.2.0") (d #t) (k 0)))) (h "01avrsakhz7z6gdc7bqpl28hb6h1rc1i6xa3z6d5kyq764ac6wm9")))

(define-public crate-vertigo-0.3.0 (c (n "vertigo") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1dhyzjv7lp987x5gwksy6ikpngl72i9yyjbp3fwj1d1x06qvmim1")))

(define-public crate-vertigo-0.3.1 (c (n "vertigo") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.3.1") (d #t) (k 0)))) (h "02r9kr1pasznkq0jh72l7lb596fadhhsl3libqhniharqvpyfczi")))

(define-public crate-vertigo-0.3.2 (c (n "vertigo") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1dm11b83j4m6gl5all4aa6fwgfb226q13l0a50hzd368djwqmkw6")))

(define-public crate-vertigo-0.4.0 (c (n "vertigo") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.4.0") (d #t) (k 0)))) (h "1w2gybicysn5g280xcdpb61p9fr5clrbba7gx0rvp8i8jfm09isx")))

(define-public crate-vertigo-0.4.1 (c (n "vertigo") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.4.1") (d #t) (k 0)))) (h "1mydcfkj6wpbnxkg4878nsxrglgncgkypf56bs35jwa3dhkbnpaa")))

(define-public crate-vertigo-0.4.2 (c (n "vertigo") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.4.2") (d #t) (k 0)))) (h "03fgcg3ik4axmg2qwa8kmxid37gmzw2c6pk3a1wrywrmrfxxx36l")))

(define-public crate-vertigo-0.4.3 (c (n "vertigo") (v "0.4.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.4.3") (d #t) (k 0)))) (h "1cy1qs1kd9zvfn874ld7qn1iamchcr5yicykxq5rnrsgik3ikz6h")))

(define-public crate-vertigo-0.5.0 (c (n "vertigo") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "vertigo-macro") (r "^0.5.0") (d #t) (k 0)))) (h "12ajdkb6f28v85r150sc2r4xar08vqv8h0n3ckzfh4y3b8ys8hp2")))

