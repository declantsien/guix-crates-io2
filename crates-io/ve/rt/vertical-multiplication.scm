(define-module (crates-io ve rt vertical-multiplication) #:use-module (crates-io))

(define-public crate-vertical-multiplication-0.0.0 (c (n "vertical-multiplication") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0bbnvqfgdqizqc022fmlmya3fdiiwymbh9hkrswq6j1fizjm0ryd")))

(define-public crate-vertical-multiplication-0.1.0 (c (n "vertical-multiplication") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0zzwari9d4p7hkn7z8ygigqfbfphlxs5gra0qjpgq547vvg25wnx")))

