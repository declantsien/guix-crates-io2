(define-module (crates-io ve rt vertigo-html) #:use-module (crates-io))

(define-public crate-vertigo-html-0.1.0-alpha.1 (c (n "vertigo-html") (v "0.1.0-alpha.1") (d (list (d (n "vertigo") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "vertigo-html-macro") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0zma2v6qxa7fgck0hwfc46wrsdf8nc2hvsy8g2zcpcl6r4kdjzdj")))

(define-public crate-vertigo-html-0.1.0-alpha.2 (c (n "vertigo-html") (v "0.1.0-alpha.2") (d (list (d (n "vertigo") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "vertigo-html-macro") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "08xzgihh86bvn4fhcv8w9vkwsscvl0bdzpym3jf4z25jnv9azvpl")))

