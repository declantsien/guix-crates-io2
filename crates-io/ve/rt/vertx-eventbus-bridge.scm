(define-module (crates-io ve rt vertx-eventbus-bridge) #:use-module (crates-io))

(define-public crate-vertx-eventbus-bridge-0.0.1 (c (n "vertx-eventbus-bridge") (v "0.0.1") (d (list (d (n "buffered-reader") (r "^0.17.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "0vmymqjmv867chzvnrmxn1qmv83lzbf5z2vwkms6lnm2a6jlnkga")))

(define-public crate-vertx-eventbus-bridge-0.0.2 (c (n "vertx-eventbus-bridge") (v "0.0.2") (d (list (d (n "buffered-reader") (r "^0.17.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "testcontainers") (r "^0.9.1") (d #t) (k 2)))) (h "03lcp1bj3wlg7swjjabcrbg5dp491c1iq316lk2k7klwv459bcbg")))

(define-public crate-vertx-eventbus-bridge-0.0.3 (c (n "vertx-eventbus-bridge") (v "0.0.3") (d (list (d (n "buffered-reader") (r "^0.20.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "testcontainers") (r "^0.11.0") (d #t) (k 2)))) (h "0wll3lsv36vpa2xax2iwfwnswf353vg1c5ygksxrp3ckcv6p3iiz")))

(define-public crate-vertx-eventbus-bridge-0.0.4 (c (n "vertx-eventbus-bridge") (v "0.0.4") (d (list (d (n "buffered-reader") (r "^0.20.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "testcontainers") (r "^0.11.0") (d #t) (k 2)))) (h "1vdqs2y64d7y2dl4rb2a5frb9wczphv6h53f5wllxi5iy0l26a0d")))

