(define-module (crates-io ve cm vecmap-rs) #:use-module (crates-io))

(define-public crate-vecmap-rs-0.1.0 (c (n "vecmap-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "018zmx4cl4cnc4pqzk3ki81annz1is77lwv8w00x84d4q48ibvpm") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.1 (c (n "vecmap-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "001f5xdncqj8xj52q00ba0rzx8fl9q05xsjb7gs1sq1imxqhkfcr") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.2 (c (n "vecmap-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "0dpp099c90mhlwph6ijs7c2l887v7cw6mp5kix76kxg6bv5mb4g3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.3 (c (n "vecmap-rs") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "0gbhcsbm55v931d6j9y7bqy1z9iih3w4i70s4bx9yrdy7d7k7vqr") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.4 (c (n "vecmap-rs") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "0qwnn7hicqln3w346b4bingz0by8358qq2ds957i8h94pvwx8ni3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.5 (c (n "vecmap-rs") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "0s6l6qlk2zi9w23s6aa37qjx3yksfy8hhzckl9lg8j8ancad8rdl") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.6 (c (n "vecmap-rs") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (k 2)))) (h "07493v3h1lwjzjlhq2x14hbbwnx5d74sk4r7f664rghv375sdzvg") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.7 (c (n "vecmap-rs") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1if6380hs7k6fkbwyv8dp8a83c7wcpydxkz4ir6k2lliza766zrk") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.8 (c (n "vecmap-rs") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03fhms3m5maqb99rvw2mw4blhfxpf8nw2vg5ngppcv1965wqvc6q") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.9 (c (n "vecmap-rs") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1h2q3dcrvqdcnf8r6srfq59d72h5rzik91cizybc8a25kgkhhs70") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.10 (c (n "vecmap-rs") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1h0c6n8ra4mkvmz08khx4iw8byh7cnldizj44s175m4y4yq8rlfz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.11 (c (n "vecmap-rs") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03jy2llhmgqnw9zwmswv9hk98a34d93c60wrc2iq8vadp1zkikig") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.12 (c (n "vecmap-rs") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0mqx59grzqd1z0zin5dhnd1wl37ccn7spzd8f26izpd86gmn9mna") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.13 (c (n "vecmap-rs") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1livgim0fgc8gb1lbwl7jq0vigr2z4y77llqiqmzy9ibnk7mrvzd") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.14 (c (n "vecmap-rs") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1npc14qnrq64vkkx0gxdn56l7smn3sh8m2wk3iwwl7qr2dd7fp6d") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.1.15 (c (n "vecmap-rs") (v "0.1.15") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0hb5b3qh6h5r3s6ci9dwmgyw7hi49nn1193cc80wcxr3zm2y834s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.2.0 (c (n "vecmap-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0cxx9345qpzkydb5awpb4irpplch72x65lhgprb218mldl4vxsyq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecmap-rs-0.2.1 (c (n "vecmap-rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0zd99i8s64d30xxjkkkbb815wpmysff4lvqwq7g4g7bn5ipwgnb7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

