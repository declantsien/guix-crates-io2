(define-module (crates-io ve cm vecmap) #:use-module (crates-io))

(define-public crate-vecmap-0.1.0 (c (n "vecmap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0js2lnj9krbzvnjv3s74zsp7zqaszm7zc8lh6fdjnzw99wadmrik")))

