(define-module (crates-io ve cm vecmath) #:use-module (crates-io))

(define-public crate-vecmath-0.0.0 (c (n "vecmath") (v "0.0.0") (h "1dqmy89c3i0m0dxb9wwqkaa2vqbd35c36zjd48ryww3z72cynwms")))

(define-public crate-vecmath-0.0.1 (c (n "vecmath") (v "0.0.1") (h "0p3qd49c6srlvnixs8sjff8nqvqx50lsqs78shv9n1ixj5aclqz2")))

(define-public crate-vecmath-0.0.2 (c (n "vecmath") (v "0.0.2") (h "01hqk985jg371whpd1zqajj865dly662ga9dj46dig4c759zzxsf")))

(define-public crate-vecmath-0.0.3 (c (n "vecmath") (v "0.0.3") (h "0qfv93p2rdypfx0vqgn4ad3fhxilpfj03zsk9pfzv9dwgxdw2r6a")))

(define-public crate-vecmath-0.0.5 (c (n "vecmath") (v "0.0.5") (h "05jz9m60mg14hqgasy7mp0cmmrmppcxzmpzgifvad8qwngmzvh29")))

(define-public crate-vecmath-0.0.6 (c (n "vecmath") (v "0.0.6") (d (list (d (n "num") (r "^0.1.21") (d #t) (k 0)))) (h "1ns7pnzlhhr64zvxd7jxcysshsihklg7jxl22jnmwr491wl394qi")))

(define-public crate-vecmath-0.0.7 (c (n "vecmath") (v "0.0.7") (d (list (d (n "num") (r "^0.1.21") (d #t) (k 0)))) (h "1wbgxflnvlpvbncfg7x6s94ym1nc7jrjsyr2gg9jgkv4xpmznp9y")))

(define-public crate-vecmath-0.0.13 (c (n "vecmath") (v "0.0.13") (h "0x6kzzlvkc2lz4abspxppxmk1h44mb1d815qff782m1nmslryp9r")))

(define-public crate-vecmath-0.0.22 (c (n "vecmath") (v "0.0.22") (d (list (d (n "piston-float") (r "^0.0.1") (d #t) (k 0)))) (h "06x40klma0qrhdwv4jmz12fml6n34q4ig38q6p71schw3whavi1f")))

(define-public crate-vecmath-0.0.23 (c (n "vecmath") (v "0.0.23") (d (list (d (n "piston-float") (r "^0.0.2") (d #t) (k 0)))) (h "19bd7xxkdnl5wahnwqpf2k8sh8my1yygmz4nlqnvmm7kp8ap068s")))

(define-public crate-vecmath-0.1.0 (c (n "vecmath") (v "0.1.0") (d (list (d (n "piston-float") (r "^0.1.0") (d #t) (k 0)))) (h "0lk9pxpw9zk1hhdcbc6j7ay87baj9mv5nbaiwh7gshypy0j37jxz")))

(define-public crate-vecmath-0.1.1 (c (n "vecmath") (v "0.1.1") (d (list (d (n "piston-float") (r "^0.1.0") (d #t) (k 0)))) (h "18yafzf86n428153zd33sfh34cagv2axhmyxfwzfaf1mpxwrcsd0")))

(define-public crate-vecmath-0.2.0 (c (n "vecmath") (v "0.2.0") (d (list (d (n "piston-float") (r "^0.2.0") (d #t) (k 0)))) (h "1y1g6wzililn87xlgw3g4ifl8q7fc4zf7bn22r4l11j4j1vq3cli")))

(define-public crate-vecmath-0.3.0 (c (n "vecmath") (v "0.3.0") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "007z4llkxjc2lsygrnbjshqhwad031rlqml6vn2vrhv9skcjbgmy")))

(define-public crate-vecmath-0.3.1 (c (n "vecmath") (v "0.3.1") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "1zls8wmyv67z7fkzjfzl3k4gcxr7w4h42gjz9095w7lwxqs61p8v")))

(define-public crate-vecmath-1.0.0 (c (n "vecmath") (v "1.0.0") (d (list (d (n "piston-float") (r "^1.0.0") (d #t) (k 0)))) (h "0shmj76rj7rqv377vy365xwr5rx23kxqgkqxxrymdjjvv3hf2slm")))

