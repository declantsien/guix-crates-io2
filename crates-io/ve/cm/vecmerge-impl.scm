(define-module (crates-io ve cm vecmerge-impl) #:use-module (crates-io))

(define-public crate-vecmerge-impl-0.0.1 (c (n "vecmerge-impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1fm43jk02v1ammk6nz2h6h4xcmhb39p6s00zhbglsri3mb7sh899")))

(define-public crate-vecmerge-impl-0.0.2 (c (n "vecmerge-impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0lcmhjswkq3nb9n6spdb6h5wz0k69hki2shlfqnpwzwdzjw8z0y1")))

(define-public crate-vecmerge-impl-0.0.3 (c (n "vecmerge-impl") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "139bmy7bxiadc7fszzbh62iq4nk1n0xya71v639ck3bwgkfdd6bq")))

