(define-module (crates-io ve cm vecmerge) #:use-module (crates-io))

(define-public crate-vecmerge-0.0.1 (c (n "vecmerge") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)) (d (n "vecmerge-impl") (r "^0.0.1") (d #t) (k 0)))) (h "02j351zwdvmisrk4y3rqbfiv80cb1rxp19chb27737wqid659hy4")))

(define-public crate-vecmerge-0.0.2 (c (n "vecmerge") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)) (d (n "vecmerge-impl") (r "^0.0.2") (d #t) (k 0)))) (h "0fphdmgsbh9jnfa25k25plgbgy4xa4k30l0b1j7fw8qj7r99dcz3")))

(define-public crate-vecmerge-0.0.3 (c (n "vecmerge") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)) (d (n "vecmerge-impl") (r "^0.0.3") (d #t) (k 0)))) (h "1vhfd4wbbsmm3cbydkm4cgn751d1s6psv06l28a648prf8xagf0m")))

