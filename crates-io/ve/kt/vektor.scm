(define-module (crates-io ve kt vektor) #:use-module (crates-io))

(define-public crate-vektor-0.0.0 (c (n "vektor") (v "0.0.0") (h "1fyfnzgii81m9cp2p52yhmd8r7w64byvinsrhzxsvfdglvic5wq0")))

(define-public crate-vektor-0.2.0 (c (n "vektor") (v "0.2.0") (h "1kdnc1cw0jq26y9w0aqn286ddfa9bxdqpa3a3lzm0fnzja01f0hw")))

(define-public crate-vektor-0.2.1 (c (n "vektor") (v "0.2.1") (d (list (d (n "packed_simd") (r "^0.3") (d #t) (k 0)))) (h "1nikx62d4sqqsn7f21yp6a3dk6658wpdvrnv43bpspddx6z6zs22")))

(define-public crate-vektor-0.2.2 (c (n "vektor") (v "0.2.2") (d (list (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "1c0x3m6riz7qw3qy57xdwpaii44hqc3jzc7snckmxwbs1qirfv6y")))

