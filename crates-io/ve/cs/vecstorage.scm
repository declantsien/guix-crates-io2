(define-module (crates-io ve cs vecstorage) #:use-module (crates-io))

(define-public crate-vecstorage-0.1.0 (c (n "vecstorage") (v "0.1.0") (h "1d2hdxm6vl2hp18fpqvm9b40x51jx69gdrcb88mfjib89zambzc5")))

(define-public crate-vecstorage-0.1.1 (c (n "vecstorage") (v "0.1.1") (h "0m8dh18d5d1qpjv4bgyysryqrhjig5vsgny5d2n4c1fq0f5ai09v")))

(define-public crate-vecstorage-0.1.2 (c (n "vecstorage") (v "0.1.2") (h "0xaf57nqmkzcw6xzrr3c0m62s97r5iijn41skbm9d5ks3ixkvdz2")))

