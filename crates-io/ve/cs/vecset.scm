(define-module (crates-io ve cs vecset) #:use-module (crates-io))

(define-public crate-vecset-0.0.1 (c (n "vecset") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "19d6v7w8cybch2cchlil3id3dfs5a00nf5i9kqqbb5zb7a44lpd8") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vecset-0.0.2 (c (n "vecset") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1pag6gy7is4s7d8i7n6fhbcjcnhvvkb5zqz0cv8z132fi8p5rb3s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

