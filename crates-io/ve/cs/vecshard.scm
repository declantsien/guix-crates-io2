(define-module (crates-io ve cs vecshard) #:use-module (crates-io))

(define-public crate-vecshard-0.0.1 (c (n "vecshard") (v "0.0.1") (h "17g0xfzm5a0shwk232v9bk02a6cnv4bsyb6xzjx0rfmz41np9ry5")))

(define-public crate-vecshard-0.0.2 (c (n "vecshard") (v "0.0.2") (h "12b9r25hkmv4hidg5dyqf1l9sffxj1v9fbbbdq1a6y1y5radwwrx")))

(define-public crate-vecshard-0.1.1 (c (n "vecshard") (v "0.1.1") (h "0xgq4jzhq50ybxsnqvndfqz7pck1c9vwj2p3d67iqj1w0500qr15")))

(define-public crate-vecshard-0.2.0 (c (n "vecshard") (v "0.2.0") (h "19pkxpbvn2zm6qqbwgf5y12lm1n2xcmm7d4cg3r7k88nnjj0q689")))

(define-public crate-vecshard-0.2.1 (c (n "vecshard") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.90") (d #t) (k 2)))) (h "0r9bm5ckxrscp35vcwyzlks8xrayq9fmrfyncrbi0k71vzbamdrn")))

