(define-module (crates-io ve cs vecs) #:use-module (crates-io))

(define-public crate-vecs-0.1.0 (c (n "vecs") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1db2v01qrbaqv61hkfmz474npl2d58qhq83ckjra6g0z61ny69wh") (y #t)))

(define-public crate-vecs-0.1.1 (c (n "vecs") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0c279ifrm3zzwjhmkb54c752kjgsahzci2jdq8r4wy3p057mqmi6") (y #t)))

(define-public crate-vecs-0.1.2 (c (n "vecs") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1cjawysd9xjym3vgcbz34f6rf9wbx7bpyqas488g1f50id8sraa0") (y #t)))

(define-public crate-vecs-0.2.0 (c (n "vecs") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "02d72wgbvx8rdmxv7q408nwmzbg537vqyflr1ldj9qf92lg1gvj8")))

(define-public crate-vecs-0.2.1 (c (n "vecs") (v "0.2.1") (h "06b7hwjwnliysldhfxlik3zpsw5hx0d1g3c0mi5q7cmxh2nx35sa")))

(define-public crate-vecs-0.2.2 (c (n "vecs") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "15s94cawylmlzyx4q7bcaa5yk5yjqg44cqhvcd0rllai664f34wm") (y #t)))

(define-public crate-vecs-0.2.3 (c (n "vecs") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fsw5dj71jd9wdyjxh8s9zdk635f1d64asm21vm1d7gdys2kfnjh")))

