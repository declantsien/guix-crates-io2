(define-module (crates-io ve nn venn) #:use-module (crates-io))

(define-public crate-venn-0.0.0 (c (n "venn") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "projective") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d5qdqrlv90zj59isgbj89db932fffrbnlj3c3xrrd3vrjyssa0x") (f (quote (("default"))))))

