(define-module (crates-io ve nn venndb-macros) #:use-module (crates-io))

(define-public crate-venndb-macros-0.1.0 (c (n "venndb-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1amf4fh8hysicjllzhcfkgp1zsdsygm429859khn2pc38k44nf7y") (r "1.75.0")))

(define-public crate-venndb-macros-0.1.1 (c (n "venndb-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13m4xn3jmajhmg06mf6rxjnil67r2fyzf16fw35bp3y96yvzw445") (r "1.75.0")))

(define-public crate-venndb-macros-0.2.0 (c (n "venndb-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04zvdxx1n1vzda35mrzh07jsxpgv0s8ss9jx2rsgrlbi84afsjki") (r "1.75.0")))

(define-public crate-venndb-macros-0.2.1 (c (n "venndb-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "005dmsrijk3ghxgd1a3zy485c5b988ga0h73084q5kbny6r97686") (r "1.75.0")))

(define-public crate-venndb-macros-0.3.0 (c (n "venndb-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1m6g6kf5iiw2zh796yvg288rscspkkr28cwaj7n18b48a1ad5g5d") (r "1.75.0")))

(define-public crate-venndb-macros-0.4.0 (c (n "venndb-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "065si6vh4c3k9px31zk88ihv03w7l2c2r1lj6wgk449fq4sspjp4") (r "1.75.0")))

(define-public crate-venndb-macros-0.5.0 (c (n "venndb-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vsh3irkvsqc8yf0xzfq7z42whhdv5j619f2vwvfv8f5fkpnkzz3") (r "1.75.0")))

