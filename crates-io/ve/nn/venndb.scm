(define-module (crates-io ve nn venndb) #:use-module (crates-io))

(define-public crate-venndb-0.0.0 (c (n "venndb") (v "0.0.0") (h "0083r66blssqp21a5bm5vc1wchnyq23xn4dgg39c70jgvmlbbjrd")))

(define-public crate-venndb-0.1.0 (c (n "venndb") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1f5qj7r0wvnp1d3dglakbq5icsiphw926pmi3zf1h3ng492j2k4j") (r "1.75.0")))

(define-public crate-venndb-0.1.1 (c (n "venndb") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0l7izfci95brp6a74xyjjj6vkag2c8pbc5iqyybi3mkj4kgq2f2c") (r "1.75.0")))

(define-public crate-venndb-0.2.0 (c (n "venndb") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0l3d5927ykk8aasajv1hw5dh0kv78b0fl5p9ddpc6xxig7y7ycgp") (r "1.75.0")))

(define-public crate-venndb-0.2.1 (c (n "venndb") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.2.1") (d #t) (k 0)))) (h "0lyqpzcccg4n1wrclsw9ss8brkw2rgpbk2q2znmw1v7y2xswf0sp") (r "1.75.0")))

(define-public crate-venndb-0.3.0 (c (n "venndb") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1668hhrs8xxj8dr7vfh73dyjlvj1081cyzprhspg4107v1i2wkjq") (r "1.75.0")))

(define-public crate-venndb-0.4.0 (c (n "venndb") (v "0.4.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.4.0") (d #t) (k 0)))) (h "073j7qg7q0dsrn3wpv5rb7pd7ss1i0qfkz9aqj3jm18hkada9vvg") (r "1.75.0")))

(define-public crate-venndb-0.5.0 (c (n "venndb") (v "0.5.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sqlite") (r "^0.36.0") (d #t) (k 2)) (d (n "venndb-macros") (r "^0.5.0") (d #t) (k 0)))) (h "0qzsy01aihxwf0y3kql8y8645ibhd2lgdd9v15vix48pk4khvva2") (r "1.75.0")))

