(define-module (crates-io ve ld veldora) #:use-module (crates-io))

(define-public crate-veldora-0.1.0 (c (n "veldora") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "pdf") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1k2byhdjijqm5l1a8n55a9k63pirsc4y1yldd4krd3jxj2dl0scr")))

(define-public crate-veldora-0.1.1 (c (n "veldora") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "pdf") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1x7im9bfyaw24rc8vsh1ly7h943m8533jd00c3bnik2ga1l2vich")))

(define-public crate-veldora-0.1.2 (c (n "veldora") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "pdf") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1b0y8575yf3q805pzrrz23h0g60r95xbfiva8v39irll64gj1wc0")))

(define-public crate-veldora-0.2.0 (c (n "veldora") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "pdf") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0jkj3g42xcb0rcdysad7rlaqlid4b0a44qz7fwa9k80kfm0rvh54")))

(define-public crate-veldora-0.2.1 (c (n "veldora") (v "0.2.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "pdf") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tiger") (r "^0.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "01wnk2waknrq82frz36d2sdyzv01n6g1s2zninah9bnhm17clkwx")))

