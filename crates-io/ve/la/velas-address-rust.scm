(define-module (crates-io ve la velas-address-rust) #:use-module (crates-io))

(define-public crate-velas-address-rust-0.1.0 (c (n "velas-address-rust") (v "0.1.0") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0ab4zxy44rhd3rp12dz78q4hnnldbqr4w3hz0wsz66wy0s304jzs")))

(define-public crate-velas-address-rust-0.2.0 (c (n "velas-address-rust") (v "0.2.0") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0d38nrgn5i73mi881cf1c9k95n13r17619r2vmqapgyfacakgb1b")))

(define-public crate-velas-address-rust-0.2.1 (c (n "velas-address-rust") (v "0.2.1") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0fl4har9p63hprixll7f18jvsinh042gjlswxnxf0dxbmysgd2ar")))

(define-public crate-velas-address-rust-0.2.2 (c (n "velas-address-rust") (v "0.2.2") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "11sm5c927f3r5gpqw428bdw39jnv122wlgd70x7slsclcpps7khg")))

(define-public crate-velas-address-rust-0.2.3 (c (n "velas-address-rust") (v "0.2.3") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0ix9yp2b4sg3ar95qkd0zb09nlb7pa93irc3s4bs4mxi2fiss40y")))

