(define-module (crates-io ve x- vex-sdk) #:use-module (crates-io))

(define-public crate-vex-sdk-0.1.0 (c (n "vex-sdk") (v "0.1.0") (h "16zzpmhkdkz5bm5667psvkki6xs2wyjm714czk5f4frggh0ylxih")))

(define-public crate-vex-sdk-0.2.0 (c (n "vex-sdk") (v "0.2.0") (h "0x76h115rls6ibd73jm64jyid8sagl3qlk14ad6z3l2cp19appyw")))

(define-public crate-vex-sdk-0.3.0 (c (n "vex-sdk") (v "0.3.0") (h "08i1j0133slvc8izg5j3im0xbnmqnnkmskih813q046ay2zna0kd")))

(define-public crate-vex-sdk-0.4.0 (c (n "vex-sdk") (v "0.4.0") (h "0b9mayp7spm0k6cyxyjagnig8z8di2ms5i0rx52lqcjnm3rgnsjd")))

(define-public crate-vex-sdk-0.5.0 (c (n "vex-sdk") (v "0.5.0") (h "0mkwlvaxsjajw6ndwbnpjpdnjc5kh63inqyc7w26z9hrwgm4jpx7")))

(define-public crate-vex-sdk-0.6.0 (c (n "vex-sdk") (v "0.6.0") (h "09rbqfzld999y5rw5230sxkmaxjzxilaljv1pr2p3x4q3j71kx7m")))

(define-public crate-vex-sdk-0.7.0 (c (n "vex-sdk") (v "0.7.0") (h "1y19dph14v1ydsf81rsnymql3b75ww2saf8w1dcpxb2aqfiijz14")))

(define-public crate-vex-sdk-0.7.1 (c (n "vex-sdk") (v "0.7.1") (h "0bvdqssp5n4sk7f2ypa6jd21wnvb5mg9gxk4zvc6pqij1k5fsyvz")))

(define-public crate-vex-sdk-0.7.2 (c (n "vex-sdk") (v "0.7.2") (h "08aqpr51jknflbxny1357yavp2widjfcvprrr20lbjq2y21yfv0b")))

(define-public crate-vex-sdk-0.8.0 (c (n "vex-sdk") (v "0.8.0") (h "0hrhx8781fhn12jd05xyqcwhpmyxk2lmfrss36v6k11hy4hjzx2d")))

(define-public crate-vex-sdk-0.9.0 (c (n "vex-sdk") (v "0.9.0") (h "1qx7r8cc23v8skpm5qf2figl20ay9fdn9pdagi4axafappj9cw0z")))

(define-public crate-vex-sdk-0.9.1 (c (n "vex-sdk") (v "0.9.1") (h "1mwm4jl7jmmb9ag2cbqfcpwyz4jm9jxrgv5iyvlc10zvfgqzfx44")))

(define-public crate-vex-sdk-0.9.2 (c (n "vex-sdk") (v "0.9.2") (h "1xd443vr4bk0ddlqcmf183l2ixqlbqix8m3ycnrlr11sxzz5njy1")))

(define-public crate-vex-sdk-0.9.3 (c (n "vex-sdk") (v "0.9.3") (h "1w3qdv5cf7pi0bhp5dwiwx9z4h2y52b3pdlbal8ii9hj3270h6f6")))

(define-public crate-vex-sdk-0.10.0 (c (n "vex-sdk") (v "0.10.0") (h "0fnw6jv20q2x06ppl1sc8zh5hzyasbhw1ddgdih30spfl8r2qbgk")))

(define-public crate-vex-sdk-0.10.1 (c (n "vex-sdk") (v "0.10.1") (h "0b615f31lb4dm56maiwqd9cdqixkyy6gzwgslv671d4lwdvnhfy1")))

(define-public crate-vex-sdk-0.11.0 (c (n "vex-sdk") (v "0.11.0") (h "0cy1azckcwn2k0syjhgi07y969lnzrwi53f0bigb8h6zr88jwlm0")))

(define-public crate-vex-sdk-0.12.0 (c (n "vex-sdk") (v "0.12.0") (h "0ax1plfwz48081gyr3qjh6w9kqsl50x6sial24xp09rvpyy3xvvp")))

(define-public crate-vex-sdk-0.12.2 (c (n "vex-sdk") (v "0.12.2") (h "176pk1sjxxbrzcpkb4hq7xf6qqa5xrb90z7wflgv9aglldqpgin7")))

(define-public crate-vex-sdk-0.12.3 (c (n "vex-sdk") (v "0.12.3") (h "0jlx94pqmg6zl2pvwrpwi7rjdp0v1xyyz5k0ks3c94nmyg4mm5qb")))

(define-public crate-vex-sdk-0.13.0 (c (n "vex-sdk") (v "0.13.0") (h "0kgb0li8cjbnx0ym1sxl1phbs3iz0fm9md4liq9ng7qswn744wnh")))

(define-public crate-vex-sdk-0.14.0 (c (n "vex-sdk") (v "0.14.0") (h "02nnn5g3bvzy9z04l9khq6ms3agmpdqr0y9wxfc810p8id244q7r")))

(define-public crate-vex-sdk-0.15.0 (c (n "vex-sdk") (v "0.15.0") (h "1pq6iy9s08vyb8j0ayk87hqsrpsigmidjk7pmnnx83d5n6vabr3m")))

(define-public crate-vex-sdk-0.16.0 (c (n "vex-sdk") (v "0.16.0") (d (list (d (n "rustc-std-workspace-core") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0mqmmxf9qs6q4hmdwym7gn36zm58slb9ryydb9w9ygdrg8vkinfi") (f (quote (("rustc-dep-of-std" "rustc-std-workspace-core"))))))

(define-public crate-vex-sdk-0.16.1 (c (n "vex-sdk") (v "0.16.1") (d (list (d (n "compiler_builtins") (r "^0.1.109") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0agc1pm4w63nr72sc7i9y60y182nm52hziw413d7h24b92i4fdi4") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-vex-sdk-0.17.0 (c (n "vex-sdk") (v "0.17.0") (d (list (d (n "compiler_builtins") (r "^0.1.109") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1l6nlkdi87ab1sg1m2x509q1w4p9s9fkdgifwkxcgi3gp71bp1gj") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

