(define-module (crates-io ve x- vex-sys) #:use-module (crates-io))

(define-public crate-vex-sys-0.1.0 (c (n "vex-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "foreman") (r "^0.4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^0.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "047jx9g8hvybaan8rmixr98xg79zlhb73r1mrpr08d8ygc3kzd0z")))

(define-public crate-vex-sys-0.2.0 (c (n "vex-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "128qzbkgr9f7zg4qwlgw89x41996s3g40j860wmny594hnjp1nbd") (f (quote (("pic") ("default"))))))

(define-public crate-vex-sys-0.3.0 (c (n "vex-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "0n7h5h0777v9285axsksy5p79nq47fw6fapvkqv32fzlbdr50gs9") (f (quote (("pic") ("default"))))))

