(define-module (crates-io ve cc veccell) #:use-module (crates-io))

(define-public crate-veccell-0.1.0 (c (n "veccell") (v "0.1.0") (h "0bcpnzkc4a1vbk4rz6mnk66jnnq2qrqw4vyd1a5ag5xz2hn9znaa")))

(define-public crate-veccell-0.1.1 (c (n "veccell") (v "0.1.1") (h "1drnq25a1z52a2b8fz40hf3hsj7xgmzn2va107nar5b9ziz10rfh")))

(define-public crate-veccell-0.2.0 (c (n "veccell") (v "0.2.0") (h "0jpzy5b1mwm3w4fwjg6k3wx0csfxbicpwfyx1ch9jcl93jzm6a07")))

(define-public crate-veccell-0.2.1 (c (n "veccell") (v "0.2.1") (h "10dlf2ca2hag4c1bnx2wfpkc7pshq8zw4fikd1xbzvvbh85fhc8y")))

(define-public crate-veccell-0.3.0 (c (n "veccell") (v "0.3.0") (h "0f0l4fzsvim3zdvri0zr905kkb02sxf6mr5idgn12hllvyz95sik")))

(define-public crate-veccell-0.4.0 (c (n "veccell") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kipj004n3aqhvj1cvz7yfndsva89phynx70jym9z5agg1jmhk9p") (s 2) (e (quote (("serde" "dep:serde"))))))

