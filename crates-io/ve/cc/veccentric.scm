(define-module (crates-io ve cc veccentric) #:use-module (crates-io))

(define-public crate-veccentric-0.1.0 (c (n "veccentric") (v "0.1.0") (d (list (d (n "overload") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)))) (h "1643dskwlmlki6lpfr4spln53mdahz7vn3biraka86l6429ag1wl") (f (quote (("random" "rand"))))))

(define-public crate-veccentric-0.1.1 (c (n "veccentric") (v "0.1.1") (d (list (d (n "overload") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)))) (h "1mnr0bdl15syk0d3cvynv4n2vnyynv8cncsyf7svnfn1xfmzyqhj") (f (quote (("random" "rand") ("default"))))))

(define-public crate-veccentric-0.1.2 (c (n "veccentric") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "overload") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)))) (h "1cpp8j2fww6h6rbg050h3wzp9a8v8j6lxik4jyza4wkvp4ly10ql") (f (quote (("random" "rand") ("default"))))))

(define-public crate-veccentric-0.2.0 (c (n "veccentric") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "overload") (r "^0.1") (d #t) (k 0)) (d (n "pixels") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 2)))) (h "19414zb9bs3b0hk6gdd97dk5lvihjw5aag361a3v3cam8s650zya") (f (quote (("random" "rand") ("default") ("all" "random"))))))

(define-public crate-veccentric-0.3.0 (c (n "veccentric") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "overload") (r "^0.1") (d #t) (k 0)) (d (n "pixels") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 2)))) (h "0jsdfsrklgchxnrydhgpy82rndscxl6mp3cgn5l55lq310zp0rdb") (f (quote (("random" "rand") ("default") ("all" "random")))) (y #t)))

(define-public crate-veccentric-0.3.1 (c (n "veccentric") (v "0.3.1") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "pixels") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 2)))) (h "11l3laac88079jwfapxbahnl4kf3wsdpbx98rzfw405lfixh5wrn") (f (quote (("random" "rand") ("default") ("all" "random"))))))

