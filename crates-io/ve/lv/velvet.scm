(define-module (crates-io ve lv velvet) #:use-module (crates-io))

(define-public crate-velvet-0.1.0 (c (n "velvet") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.21.1") (d #t) (k 0)))) (h "1zmzh44vwzidhclv7xvv7jzzymbxz0k2a8gxqv5j5yv7zxaghz20")))

(define-public crate-velvet-0.2.0 (c (n "velvet") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.15") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "velvet-convert") (r "^0.1.0") (d #t) (k 0)) (d (n "velvet-core") (r "^0.2.1") (d #t) (k 0)))) (h "0yp921a8g002syhvcs8bf6dhx85qfga6h4yr5q3ay5sm1jidlixm")))

(define-public crate-velvet-0.2.1 (c (n "velvet") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.15") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "velvet-convert") (r "^0.1.0") (d #t) (k 0)) (d (n "velvet-core") (r "^0.2.1") (d #t) (k 0)))) (h "0s74jks962im2q7py4nf60f1nn8jgnx37v6858irg4anvzph0hjx")))

(define-public crate-velvet-0.3.0 (c (n "velvet") (v "0.3.0") (d (list (d (n "hdf5") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "velvet-convert") (r "^0.1.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.3.1") (d #t) (k 0)))) (h "1gr6s6wv5ndnliaf33hrsqs0xfdhdyy8ygmg38n1pddmnlx328gj")))

(define-public crate-velvet-0.3.1 (c (n "velvet") (v "0.3.1") (d (list (d (n "hdf5") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "velvet-convert") (r "^0.1.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.3.3") (d #t) (k 0)))) (h "0gz525g041afxif79pa04jnahpj5svnzdy0rzfsbkyvfscc94qqd")))

(define-public crate-velvet-0.3.2 (c (n "velvet") (v "0.3.2") (d (list (d (n "hdf5") (r "^0.7") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "velvet-convert") (r "^0.1.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.3.3") (d #t) (k 0)))) (h "1amq7wgwp2cyil17fkhnd85p62awzcijm6flizxdxvv42wk8carg")))

(define-public crate-velvet-0.4.0 (c (n "velvet") (v "0.4.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hdf5") (r "^0.7") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 2)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "velvet-core") (r "^0.4.0") (d #t) (k 0)) (d (n "velvet-external-data") (r "^0.1.0") (d #t) (k 0)))) (h "0n79322mb27z3sxg0fmjfyhhxbndnbizgpsrh8xiipldh1rgsq3b")))

(define-public crate-velvet-0.4.1 (c (n "velvet") (v "0.4.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hdf5") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 2)) (d (n "velvet-core") (r "^0.4.1") (d #t) (k 0)) (d (n "velvet-external-data") (r "^0.1.0") (d #t) (k 0)))) (h "0jjjfy143v6rnpy19bmfgpig1j2z3saj5kczmx6xji9ik9qd9115") (f (quote (("hdf5-output" "velvet-core/hdf5-output" "hdf5" "hdf5-sys") ("default"))))))

(define-public crate-velvet-0.4.2 (c (n "velvet") (v "0.4.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hdf5") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 2)) (d (n "velvet-core") (r "^0.4.2") (d #t) (k 0)) (d (n "velvet-external-data") (r "^0.1.0") (d #t) (k 0)))) (h "0ww932wwsz88xnm6qj856xx0sd9ny6by0bwxhq42djb6nr53vmf4") (f (quote (("rayon" "velvet-core/rayon") ("hdf5-output" "velvet-core/hdf5-output" "hdf5" "hdf5-sys") ("default"))))))

