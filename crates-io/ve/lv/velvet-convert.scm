(define-module (crates-io ve lv velvet-convert) #:use-module (crates-io))

(define-public crate-velvet-convert-0.1.0 (c (n "velvet-convert") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.2.1") (d #t) (k 0)))) (h "14ixb8vyz5kk20yj0imim0z06xi5hi5ldjskvq4pan7az4m2x3az") (y #t)))

(define-public crate-velvet-convert-0.1.1 (c (n "velvet-convert") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.2.1") (d #t) (k 0)))) (h "0g353fjmagjji7s1i4d2d7rm2hrg3cd1pk7zbybpj4mq38gb783i") (y #t)))

(define-public crate-velvet-convert-0.1.2 (c (n "velvet-convert") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.3.0") (d #t) (k 0)))) (h "0y35xjayf1wxyjgsk6aw7ihzijfb3spq330gs9lfylrdahfiv2k5") (y #t)))

