(define-module (crates-io ve lv velvet-external-data) #:use-module (crates-io))

(define-public crate-velvet-external-data-0.1.0 (c (n "velvet-external-data") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.4.0") (d #t) (k 0)))) (h "1y8in5l8lrfmq72ihsh3qw2gyj1hcjd6x73fa4sxf46psr1b5ckq")))

