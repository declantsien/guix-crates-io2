(define-module (crates-io ve lv velvet-cli) #:use-module (crates-io))

(define-public crate-velvet-cli-0.1.0 (c (n "velvet-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "velvet-convert") (r "^0.1.1") (d #t) (k 0)) (d (n "velvet-core") (r "^0.2.1") (d #t) (k 0)))) (h "0835nrmf2zsv05g3c892vlx49ijdnhiib25qsb34c1m1hzz6zmww")))

(define-public crate-velvet-cli-0.1.1 (c (n "velvet-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "velvet-convert") (r "^0.1.2") (d #t) (k 0)) (d (n "velvet-core") (r "^0.3.0") (d #t) (k 0)))) (h "1k42wnyb29mjgljmwp85x1kij6i5k7fw7ilz07vb0yc208gqk79h")))

(define-public crate-velvet-cli-0.1.2 (c (n "velvet-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "velvet-core") (r "^0.4.0") (d #t) (k 0)) (d (n "velvet-external-data") (r "^0.1.0") (d #t) (k 0)))) (h "1r7ir8zqixx28738idhk71nxs8xfggpkdmycv0112c2218l31rr0")))

