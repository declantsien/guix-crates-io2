(define-module (crates-io ve se vesema) #:use-module (crates-io))

(define-public crate-vesema-0.0.1 (c (n "vesema") (v "0.0.1") (h "1aqvmlsdg6fnvmfhsjp6gyzcnsaasf5x6k6plnxgbzqv1d2ns8hg")))

(define-public crate-vesema-0.0.2 (c (n "vesema") (v "0.0.2") (h "1vqkzgrnka2471rp95xbwkmbkqsjzagrxv2gbwny2h8pjj5qad7g")))

