(define-module (crates-io ve cl veclist) #:use-module (crates-io))

(define-public crate-veclist-0.1.0 (c (n "veclist") (v "0.1.0") (h "10da4ppakhk6xmz82x9giz1r9g2gwwfwpc168awy693lwxsjdf5l")))

(define-public crate-veclist-0.1.1 (c (n "veclist") (v "0.1.1") (h "1b5ns9xn10nki9936qgv4bsp0swk3anpm6wp4dnc00v10yn6lrd1")))

(define-public crate-veclist-0.1.2 (c (n "veclist") (v "0.1.2") (h "1s46hnkv02pnaxm3m8gxsx4xx6lp70j4x7nmvi3d3srj8idbqqpr")))

