(define-module (crates-io ve r- ver-cmp) #:use-module (crates-io))

(define-public crate-ver-cmp-0.1.1 (c (n "ver-cmp") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yqv07d01w66a52mqi1s81xxak655dd4gyg1m05w7rn0av5zfmjq")))

(define-public crate-ver-cmp-0.1.2 (c (n "ver-cmp") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mmjqsxk3w8lcds8gpnd3aa9g11xxmidn8z858pq74iqw4njc3hy") (f (quote (("build-binary" "clap"))))))

(define-public crate-ver-cmp-0.1.3 (c (n "ver-cmp") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xnh3jyp98nnm0d75n74h49x0w26i3xscs5rb7qncraczl16m8zz") (f (quote (("build-binary" "clap"))))))

(define-public crate-ver-cmp-0.1.4 (c (n "ver-cmp") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vspviwvpss0kdxmg5mvdf5hyj855df1pg96jm1clrx3dcfd3aqh") (f (quote (("build-binary" "clap"))))))

