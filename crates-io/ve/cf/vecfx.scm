(define-module (crates-io ve cf vecfx) #:use-module (crates-io))

(define-public crate-vecfx-0.0.8 (c (n "vecfx") (v "0.0.8") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0y7kqd77s9amvvjdb1pndzzh2217rs6hibrk7adc7r870wilvc3h") (f (quote (("default"))))))

(define-public crate-vecfx-0.1.0 (c (n "vecfx") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "16c5pah1dfpfmjcppg4193r56hvji7d3f7yavc9dvcjfhmgl2ins") (f (quote (("default"))))))

(define-public crate-vecfx-0.1.1 (c (n "vecfx") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0grmw9204plv067znjmz9zdshnq3j8mlap50y0zcnj533ifn5fpk") (f (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.2.0 (c (n "vecfx") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "1qwi5gf8fnxk80jkaccmrmsdi7ym50jb8iismxywgvfb2prw3amq") (f (quote (("default") ("adhoc")))) (y #t)))

(define-public crate-vecfx-0.1.2 (c (n "vecfx") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0h96awsbap6dq4qw0gr94gdg2n56ib1na8fipxn7lq589p8277bm") (f (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.1.5 (c (n "vecfx") (v "0.1.5") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^2.10") (d #t) (k 0)))) (h "0xwam4wxf5yxphkvm6shsi1wyghr5c9xhxazamzpkbs87lwk8s97") (f (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.1.6 (c (n "vecfx") (v "0.1.6") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^3") (f (quote ("serde" "rand"))) (d #t) (k 0)))) (h "1cr0kn6bfcyadyx7v5cx5x0c2k4iqplbzi20r7zprs68kabm3pr1") (f (quote (("default") ("adhoc"))))))

