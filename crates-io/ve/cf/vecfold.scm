(define-module (crates-io ve cf vecfold) #:use-module (crates-io))

(define-public crate-vecfold-0.1.0 (c (n "vecfold") (v "0.1.0") (h "0csrjpb1w3adchzvfbm3nm2p7jir1wfyj7s5w49i2d3v2zfpm8rd")))

(define-public crate-vecfold-0.1.1 (c (n "vecfold") (v "0.1.1") (h "08i0lwg9jwwar0ij7p5k1a4lrl4z237wjq0hypwfypwdqk2w3n5z")))

(define-public crate-vecfold-0.1.3 (c (n "vecfold") (v "0.1.3") (h "0223pa9iqkh5imj0crxl1c7cvbwf3gsih7drpncl9kl2g12rjbv0")))

(define-public crate-vecfold-0.1.4 (c (n "vecfold") (v "0.1.4") (h "1glzxhrf4wjk722hf5cnighmw65p6lplfdgmg4ffjknp4pxamjh9")))

(define-public crate-vecfold-0.1.5 (c (n "vecfold") (v "0.1.5") (h "1lf3pfda6wb25cm7r4s396m9qyjnbjvw45scwfizj117h54rzi68")))

(define-public crate-vecfold-0.1.6 (c (n "vecfold") (v "0.1.6") (h "14agxq8z2qhn8z18q9hhfwh5f5crp1jrjk58iz1v0sqlipm1244n")))

