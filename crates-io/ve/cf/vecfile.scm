(define-module (crates-io ve cf vecfile) #:use-module (crates-io))

(define-public crate-vecfile-0.1.0 (c (n "vecfile") (v "0.1.0") (d (list (d (n "desse") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1ri1hmp3g6r4ryjhqc8ypmaain2ybp44vdf4117irz7frb3ns240") (y #t)))

(define-public crate-vecfile-0.2.0 (c (n "vecfile") (v "0.2.0") (d (list (d (n "desse") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1d53g63kgz54i3dn03ki1zd3zra7kzfm8cmp1n7aipkg3kkmdcay") (y #t)))

(define-public crate-vecfile-0.3.0 (c (n "vecfile") (v "0.3.0") (d (list (d (n "desse") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1abzm4662pf8d2ljpikwicch0rp6z3l6yhw3mpa7j66avmjcc19v") (y #t)))

