(define-module (crates-io ve _d ve_direct) #:use-module (crates-io))

(define-public crate-ve_direct-0.1.0 (c (n "ve_direct") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ixmykx6xsmk2wli4g4skdygn9lxxav7hjd0j4k1av89k3q0kfdc")))

(define-public crate-ve_direct-0.1.1 (c (n "ve_direct") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1nxd9hmyvsypadzjqh47qb6909xilphidbf8xpi00rqgrmwdfipg")))

(define-public crate-ve_direct-0.1.2 (c (n "ve_direct") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "02vrdfv08jjkq5p6xvrlwvndjbwzhbrfdac1bh7pl7myqcrzimk5")))

(define-public crate-ve_direct-0.1.3 (c (n "ve_direct") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0vrqnniia4x7ryn7yy58gbz7gh0707cx6ls4ww96dndqliv6wkp5")))

(define-public crate-ve_direct-0.1.4 (c (n "ve_direct") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ziwizg6hkv1shcm0c1shk10lmvgcwr0i2ja4ysjxr0bg5mrvj7k")))

(define-public crate-ve_direct-0.1.5 (c (n "ve_direct") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "07alpxgs8qlql29qf5f7gq7s3j554pbj4yn0yzb7jlnqpbm18j4f")))

(define-public crate-ve_direct-0.1.6 (c (n "ve_direct") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "15sczn0psllchl65w95cs75jfcn3g21pr526pghw485f7hsgla5g")))

(define-public crate-ve_direct-0.1.7 (c (n "ve_direct") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ndwpfa5czxxxqi8a6jszky57vdlgg14vzi9gg731s9wsfikhssn")))

