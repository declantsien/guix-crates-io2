(define-module (crates-io ve sc vesc-comm) #:use-module (crates-io))

(define-public crate-vesc-comm-0.0.2 (c (n "vesc-comm") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 0)) (d (n "heapless") (r "^0.4.1") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1xkqfq1c7vsbih4yzxamcijrajr0wj3hm1v5ijrblc57i7al6n90")))

(define-public crate-vesc-comm-0.0.3 (c (n "vesc-comm") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 0)) (d (n "heapless") (r "^0.4.1") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "017hidq2hwg2si199d6v77cky3d5kiqdxxn5hjc1nh81b99qbcbm")))

(define-public crate-vesc-comm-0.1.0 (c (n "vesc-comm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 0)) (d (n "heapless") (r "^0.4.1") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "1v704g1ixdiz3k7nixjjwijfkncp08mipzac7iasgk9hyabp4sg9")))

(define-public crate-vesc-comm-0.1.1 (c (n "vesc-comm") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 0)) (d (n "heapless") (r "^0.4.1") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "0jlhi83jflannsyvjshkk4y1v9j7iskq71z23jyxyh9bv8c6az9a")))

