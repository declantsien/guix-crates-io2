(define-module (crates-io ve us veusz) #:use-module (crates-io))

(define-public crate-veusz-0.1.0 (c (n "veusz") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (d #t) (k 0)))) (h "1yb7z2pscqwwf98xch5ydmwd9f1k2q1162dz1cwxhw3njvs519dr")))

(define-public crate-veusz-0.1.1 (c (n "veusz") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (d #t) (k 0)))) (h "1dxyhh6ijaw25ybpa90sw0swnb5bx7i4a5cn1gbbb6hly44rf53m")))

(define-public crate-veusz-0.1.2 (c (n "veusz") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (d #t) (k 0)))) (h "1bm36f0lycf25dvfgbb6i9nnxkhhxd7i3fyzrahq1b1dnrb6dhpf")))

