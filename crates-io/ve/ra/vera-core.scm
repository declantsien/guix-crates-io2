(define-module (crates-io ve ra vera-core) #:use-module (crates-io))

(define-public crate-vera-core-0.3.0 (c (n "vera-core") (v "0.3.0") (d (list (d (n "vera") (r "^0.3.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-macros") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)))) (h "1rp4pnq5fyvq4561wha0nknzqsaiyqxwj5qpv0z0akrrqmkysgil")))

