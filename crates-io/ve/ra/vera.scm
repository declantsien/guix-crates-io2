(define-module (crates-io ve ra vera) #:use-module (crates-io))

(define-public crate-vera-0.1.0 (c (n "vera") (v "0.1.0") (d (list (d (n "ash") (r "^0.29.0") (d #t) (k 0)) (d (n "bitflags") (r ">=1.0.4") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "cocoa") (r "^0.18.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 0)) (d (n "metal") (r "^0.17.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tobj") (r "^0.1.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("windef" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "0na2kr3gng2a9652nsxdl4kfhiqlnbbcz59c5qsam516d0r4r63j")))

(define-public crate-vera-0.2.0 (c (n "vera") (v "0.2.0") (d (list (d (n "vulkano") (r "^0.33.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.33.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.33.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)))) (h "1lqswgjrzxwjvlsjs0pajb3s7g20fqq071wzhn9x7m6d1mgqcvkp")))

(define-public crate-vera-0.3.0 (c (n "vera") (v "0.3.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)))) (h "02c74wsjk133mzd7ragyxppqlli750wz6bbcajp0nkc7n6617p2g")))

