(define-module (crates-io ve lo velodyne) #:use-module (crates-io))

(define-public crate-velodyne-0.1.0 (c (n "velodyne") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "07jnlp8f8mv0fbfw7pqvwmv4d5cfx2nh7hwdpa2szmq99bf1sh1m") (f (quote (("xml" "xml-rs"))))))

(define-public crate-velodyne-0.1.1 (c (n "velodyne") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0gz7crzjy4s9fab9w1rz64ysx8c0y0sfnljslc99yypszwz80hjn") (f (quote (("xml" "xml-rs"))))))

