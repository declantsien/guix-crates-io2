(define-module (crates-io ve lo velocypack) #:use-module (crates-io))

(define-public crate-velocypack-0.1.0 (c (n "velocypack") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kw4k3wn0a28rp9rhlm3r693a5vcny22b0w21b7gfndcid89nwrk")))

(define-public crate-velocypack-0.1.1 (c (n "velocypack") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08191wjmxagrn71yv3x47lwng82w4h0yqa4ha4115p7qxgs1szcr")))

(define-public crate-velocypack-0.1.2 (c (n "velocypack") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x3inmb10jc2jfyx8jkiyl8civhzkyy5pcskv2f4h0ybgbql36zg")))

(define-public crate-velocypack-0.1.3 (c (n "velocypack") (v "0.1.3") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "175gfr7l9k5db3gnb0dinw2dpi82v6pjsx1l6dglri5mr7c7vsas")))

(define-public crate-velocypack-0.1.4 (c (n "velocypack") (v "0.1.4") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14210kpnzix7cm6xvighik8gm3j83md0xw54lz8jzyv7shfv07lm")))

