(define-module (crates-io ve lo velocity) #:use-module (crates-io))

(define-public crate-velocity-0.1.0 (c (n "velocity") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1a0139aqvg0v3hf1ib3rvbja2gh9qxzmn50a6klw728d9lrxab0a")))

(define-public crate-velocity-0.1.1 (c (n "velocity") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0kc48a5d8z921k18r4fvyyr9p7xszdkyk362fr7yjx63334k8xc9")))

(define-public crate-velocity-0.1.2 (c (n "velocity") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1pzsqy6g9gm66irqqyyflfj70h68xkg83v8iz79fykdljc94nam9")))

