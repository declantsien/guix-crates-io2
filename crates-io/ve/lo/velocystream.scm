(define-module (crates-io ve lo velocystream) #:use-module (crates-io))

(define-public crate-velocystream-0.1.0 (c (n "velocystream") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "velocypack") (r "^0.1") (d #t) (k 0)))) (h "1f0k5177gbjm27arw2jz4k1x0cch69z6fdlis43ifzgr88gvc0ii")))

