(define-module (crates-io ve lo veloci_levenshtein_automata) #:use-module (crates-io))

(define-public crate-veloci_levenshtein_automata-0.1.0 (c (n "veloci_levenshtein_automata") (v "0.1.0") (d (list (d (n "fst") (r "^0.4") (o #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 2)))) (h "0f17wppw844blqr1i7b7b5mf066p0x5dqsw3nvyb3a2cc70h0y86") (f (quote (("fst_automaton" "fst"))))))

