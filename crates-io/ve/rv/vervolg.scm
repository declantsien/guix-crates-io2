(define-module (crates-io ve rv vervolg) #:use-module (crates-io))

(define-public crate-vervolg-0.1.0 (c (n "vervolg") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)) (d (n "users") (r "^0.6.0") (d #t) (k 0)))) (h "0is184sm8qfrc54cxw6v2q68m2xl4v84ms7idhn4fk4am1jnb17c")))

