(define-module (crates-io ve rv verver) #:use-module (crates-io))

(define-public crate-verver-0.0.1 (c (n "verver") (v "0.0.1") (d (list (d (n "insta") (r "^1.35.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (d #t) (k 2)) (d (n "winnow") (r "^0.4.7") (d #t) (k 0)))) (h "0rcrrp0k2j1asxjc2vavbifs6aj4140ifnjmfx0pzwh4vkq5h6ry")))

