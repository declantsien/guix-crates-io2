(define-module (crates-io ve sy vesync) #:use-module (crates-io))

(define-public crate-vesync-0.1.0 (c (n "vesync") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.14.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0wvdn25y31r4c2s43avghpcw0nissdsg7w9mrgjk2cbmj32qhkgh")))

