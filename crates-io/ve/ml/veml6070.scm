(define-module (crates-io ve ml veml6070) #:use-module (crates-io))

(define-public crate-veml6070-0.1.0 (c (n "veml6070") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0pgjr9csri9hnb0ydvmivbdv73ag8zsc6gfffanhqjjwd7hamb4p")))

