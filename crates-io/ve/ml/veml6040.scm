(define-module (crates-io ve ml veml6040) #:use-module (crates-io))

(define-public crate-veml6040-0.1.0 (c (n "veml6040") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0dzyz3y2n2i5wkmjvcqpqwdzz2wilyf532ib1vyxph2yzamxv7i2")))

(define-public crate-veml6040-0.1.1 (c (n "veml6040") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1mh8mkiknlgavxgbdmiqpn09wqxd019srzi4002xv99d2crq91pm")))

