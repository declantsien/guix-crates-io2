(define-module (crates-io ve ml veml6075) #:use-module (crates-io))

(define-public crate-veml6075-0.1.0 (c (n "veml6075") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1pzqrm79cc1gjlgsf6lmblfpbabga4gbckr99zihwjxvlynb6qkx")))

(define-public crate-veml6075-0.2.0 (c (n "veml6075") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1slld3479cpjfifa6qqbyqa8kvrk52539avr3qdm9a2rhzjxx8p4")))

(define-public crate-veml6075-0.2.1 (c (n "veml6075") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1556fx3dd2bhf66x4zh1445iydp0348hi8akv5x1i7a3k62ahfkz")))

