(define-module (crates-io ve ml veml6030) #:use-module (crates-io))

(define-public crate-veml6030-0.1.0 (c (n "veml6030") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0adbsp6qrqzhn277lsdk0cxp2s2l3zzslqkvmhyd4b4fs9iybmkb")))

(define-public crate-veml6030-0.1.1 (c (n "veml6030") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0s4y350qi9hvk1nklm6vf6681rwja5iq65d62nr4gb1rc5jzxf6h")))

(define-public crate-veml6030-0.1.2 (c (n "veml6030") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "10z31d97v6pbcw1z2sfqji5lgn0wzxl4jwyzi3x79xw7zr05fr5b")))

