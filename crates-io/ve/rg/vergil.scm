(define-module (crates-io ve rg vergil) #:use-module (crates-io))

(define-public crate-vergil-0.1.0 (c (n "vergil") (v "0.1.0") (h "06568srikgx6yd4mc4drzz2j7l7lfkjkw6vx0pk1yqyl564r3f0v")))

(define-public crate-vergil-0.1.1 (c (n "vergil") (v "0.1.1") (h "044z75zj89zq4bbc82sxd4yrs8s4vj63n1nmvf9r3da0qkh02fdf")))

(define-public crate-vergil-0.1.2 (c (n "vergil") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0bn274hav9mjm4raqsqsgbd5v2wdygk14cc4w1v0lw1x3vwig657")))

(define-public crate-vergil-0.1.3 (c (n "vergil") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09kb1pgwsirdj822m2rgy9mw8vfll3p245vlgwfhgqm8zi4v6hyz")))

(define-public crate-vergil-0.1.4 (c (n "vergil") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1aaz5m7gd4n1nsqzgr70q1csi866c5d1giixs7j04dp8dby18wdk")))

(define-public crate-vergil-0.1.5 (c (n "vergil") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1cw7crngb68ic2qz8vi11jpgil2iqd0p7lgll7gydc5qnrdq2g09")))

(define-public crate-vergil-0.1.6 (c (n "vergil") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1rkkkgh7gdwr2l5labkw8lc6qzdwcmd9hdx1qn1c70hc616a0k11")))

(define-public crate-vergil-0.1.7 (c (n "vergil") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1rvfk4h95kj8w9c654823z9p81q21zvir9qsc70x4rdzwz0n4c8b")))

