(define-module (crates-io ve rg vergen-lib) #:use-module (crates-io))

(define-public crate-vergen-lib-0.1.0 (c (n "vergen-lib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13.0") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)))) (h "0g03zd4qz50nhi8hyy0g0i700wcy463377bk65zsahx4y6frj7pj") (f (quote (("unstable") ("si") ("rustc") ("git") ("default") ("cargo") ("build"))))))

(define-public crate-vergen-lib-0.1.1 (c (n "vergen-lib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13.0") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)))) (h "1lgn0y4rxxdr68rd4gxwlnpqh8cmcyl0wjd1a56yjv9nqrdx7wsj") (f (quote (("unstable") ("si") ("rustc") ("git") ("default") ("cargo") ("build"))))))

(define-public crate-vergen-lib-0.1.2 (c (n "vergen-lib") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13.0") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)))) (h "11sickcljxc5l375hhh8b5ja9d9m4d8s27pl4ngma1595fkzpsr6") (f (quote (("unstable") ("si") ("rustc") ("git") ("default") ("cargo") ("build"))))))

