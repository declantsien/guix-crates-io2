(define-module (crates-io ve rg vergit) #:use-module (crates-io))

(define-public crate-vergit-0.1.0 (c (n "vergit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0q3m48fd1wrsn1wqj99wg16qpgf946xl91c387jbg4ryl949z31y")))

(define-public crate-vergit-0.1.2 (c (n "vergit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0lckxi2wjfij89dw8mzwdf2zhcrqnbmj2yby19vrhr85axc2y9av")))

(define-public crate-vergit-0.2.1 (c (n "vergit") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1i0ddc6lv1x7048gr5acfyn6zznl9b1rypgf3w99q53b117l8bic")))

(define-public crate-vergit-0.2.2 (c (n "vergit") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1crdwkj6yp4ws1fqlzvrnmnahqhsnhki288lvv08ylmlkfd8bjg8")))

(define-public crate-vergit-0.2.3 (c (n "vergit") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "15zg24b3k4mk1c3nrbc26r5bf9h078f8ypghrm8yb2rrwzgq24nz")))

(define-public crate-vergit-0.2.4 (c (n "vergit") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1ikj7gfks2m7cim4l7l3qd3ik8iap6hwkqkfdcwg8s5h0l4mw8hx")))

(define-public crate-vergit-0.2.5 (c (n "vergit") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1gvmf2mcs85kbw2y3ryvasv5r49l4c1qiljshz53lnv7ym8fn9hp")))

(define-public crate-vergit-0.2.6 (c (n "vergit") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1rq5906887vapb0fjrzn7c197la36cbh3bg6gl1434y7l4q73nai")))

(define-public crate-vergit-0.2.7 (c (n "vergit") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1w5g42pqwg78b5hcsn3hsx0sy2f7zgrn55n8ma7kjn95w1jpwnm8")))

(define-public crate-vergit-0.2.8 (c (n "vergit") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1mma9wbai64d9x34m71jygji366v9i77bw9yg2imvv7kzqvpim01")))

(define-public crate-vergit-0.2.9 (c (n "vergit") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1fkrix3b0hncgklrqnnjc9r70lwbshyz253as08xp8ky3v7hlffc")))

(define-public crate-vergit-0.2.10 (c (n "vergit") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (d #t) (k 0)))) (h "1p7xvkx9lynaiscr04ccb9k6i0ljfdllaipivhgk7qydvv0x5y0q")))

(define-public crate-vergit-0.2.11 (c (n "vergit") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (d #t) (k 0)))) (h "1ldi2zijqfq5rbk997jrmh1j7dfdxvsh0vgsrlkc3fyp3ak4w047")))

(define-public crate-vergit-0.2.12 (c (n "vergit") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (d #t) (k 0)))) (h "1a6wrl2kyx115zcnx9g0dhlkc6ddmcm1h635mjp8wm8y5z5lmy95")))

