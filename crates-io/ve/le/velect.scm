(define-module (crates-io ve le velect) #:use-module (crates-io))

(define-public crate-velect-0.1.0 (c (n "velect") (v "0.1.0") (d (list (d (n "delegate") (r "^0.12.0") (d #t) (k 0)))) (h "05qmd0n1fwmikm6vbzc2dqrjk7mpjhmh1zn57f6d8vjjglwy42h8")))

(define-public crate-velect-0.2.0 (c (n "velect") (v "0.2.0") (d (list (d (n "delegate") (r "^0.12.0") (d #t) (k 0)))) (h "1sqjm4cc4kvba9fgkq1mj04xn816hxk1y5xhyz9wxrpk4n2aknvr")))

