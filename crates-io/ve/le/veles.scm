(define-module (crates-io ve le veles) #:use-module (crates-io))

(define-public crate-veles-0.1.0 (c (n "veles") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jppwhg4yf7x4kd69msfrpjw1z6ikvpn5z0gwqbcwk5ahff4mljf")))

