(define-module (crates-io ve le velen) #:use-module (crates-io))

(define-public crate-velen-0.1.0 (c (n "velen") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1gl9fnrhxibc4l37mld1zrapavd15x3q5jjhbhs3n4fqgg0jxnbn") (y #t)))

(define-public crate-velen-0.1.1 (c (n "velen") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "03kdfk5vm96b8x707iik4pwlqqm1l4vavlfrkffwxcjgxjz17h86") (y #t)))

(define-public crate-velen-0.1.2 (c (n "velen") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1qdy9hniyawcqy8igqbl2hzzwq847ammz4p5dlm4vyknps02wbnp") (y #t)))

(define-public crate-velen-0.1.3 (c (n "velen") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0163mv3hpqcchgwvm250gl0fhsqpi7kk2d3qf5mwq23h9wfw726c") (y #t)))

(define-public crate-velen-0.1.4 (c (n "velen") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "13dqlllqdsxzabbdh1a28qhyc67r60290afzaz71r2nhc14fk0m5") (y #t)))

(define-public crate-velen-0.1.5 (c (n "velen") (v "0.1.5") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "13kfrx1nz2csxfl0h77jf89m0whmkgglrlx33bb0pmr3zsa0wph8") (y #t)))

(define-public crate-velen-0.1.6 (c (n "velen") (v "0.1.6") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "101drjy78zwvnffjm7jg2wghiiw9fz6vnh3hpynfvy2s59qa1crb") (y #t)))

(define-public crate-velen-0.1.7 (c (n "velen") (v "0.1.7") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0pvyxjz2a3i8axfccrj421piy7bjv088vndxr5vwkn3lm1xmibfr") (y #t)))

(define-public crate-velen-0.1.8 (c (n "velen") (v "0.1.8") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "09zcfg8hmzfnw0hp1j1i6jrn6ib53jixhz06ixn4826xbs07vhj0") (y #t)))

(define-public crate-velen-0.1.9 (c (n "velen") (v "0.1.9") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1pi3vng485aad2qlblir364am8cm1jm051k0vnc7m7rcd62xa0kq")))

