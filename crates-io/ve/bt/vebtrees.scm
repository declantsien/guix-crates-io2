(define-module (crates-io ve bt vebtrees) #:use-module (crates-io))

(define-public crate-vebtrees-0.1.0 (c (n "vebtrees") (v "0.1.0") (h "15jjzk4ysn9nwwvnkg7f8i5nk5di4s6y1f40qw0jgicfb3l3lg42")))

(define-public crate-vebtrees-0.1.1 (c (n "vebtrees") (v "0.1.1") (h "1bzqvgchbdq6dyd0hiw8vjjr0yv8c26fzc3m9r0xccky19970jdv")))

(define-public crate-vebtrees-0.1.2 (c (n "vebtrees") (v "0.1.2") (h "0y7sz3p2sr84z3wbiqgyjs5nhzzrp3hcmqq5xqpcfm4pl6qn6gaz")))

(define-public crate-vebtrees-0.1.3 (c (n "vebtrees") (v "0.1.3") (h "1r6qf01sfp87kmpgm0nw7rnvgg6wyhxgn1v45qwmkmqrqp5bnc91")))

(define-public crate-vebtrees-0.1.4 (c (n "vebtrees") (v "0.1.4") (h "1h734093gjdlbdkqr3ll48n4sb3q26a5fzj628mjc2vf37vl268l")))

