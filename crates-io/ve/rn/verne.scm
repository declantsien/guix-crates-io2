(define-module (crates-io ve rn verne) #:use-module (crates-io))

(define-public crate-verne-0.1.0 (c (n "verne") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "virt") (r "^0.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0lkflhspvbvcv0pblfxn44hfmh22kpj27vnf3f79cyqjsvbvv0fd")))

