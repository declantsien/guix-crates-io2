(define-module (crates-io ve rn vern) #:use-module (crates-io))

(define-public crate-vern-0.1.0 (c (n "vern") (v "0.1.0") (h "18q60y1s22p8j99g169pxvh6dcrp9xzk8zp6rly2rbk75ggmrlw8")))

(define-public crate-vern-0.1.1 (c (n "vern") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0bdmax6g44685j189r9bqkkxzb8b27ssqhxg54xhzak0wxkpixxw")))

(define-public crate-vern-0.1.2 (c (n "vern") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1n04zk0ln5cdrdngq5kmmynrv7c3falbvv8kwh7bcransmll455b")))

(define-public crate-vern-0.1.3 (c (n "vern") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "14lw4d60fl6g3r1w8pv8x4ln0nfkwjbl17ra1m1zxv1s348h1hs9")))

