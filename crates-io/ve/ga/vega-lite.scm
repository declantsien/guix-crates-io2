(define-module (crates-io ve ga vega-lite) #:use-module (crates-io))

(define-public crate-vega-lite-0.0.1 (c (n "vega-lite") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rgl4yf3mkc5y55zxz5hjhp5ya7r2dfvr9ik6bldxz2wg6sia5rk")))

(define-public crate-vega-lite-0.0.2 (c (n "vega-lite") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06j4px3przay61dszg6d4jdjnlahx9isgij1hs18wcppvjv6jj06")))

