(define-module (crates-io ve ga vegas-lattice) #:use-module (crates-io))

(define-public crate-vegas-lattice-0.1.0 (c (n "vegas-lattice") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yc4jc4qiyd0l3xl1sv9vbm1n5592ahzm0b746vb4vqf7adqlg7s")))

(define-public crate-vegas-lattice-0.2.0 (c (n "vegas-lattice") (v "0.2.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p5jfa8x7i2qnqmzpnvs6lcpqxcwic8h9kjlgkj9lqxd8jdd5gwh")))

(define-public crate-vegas-lattice-0.3.0 (c (n "vegas-lattice") (v "0.3.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gjajnjsrp12vg8zwagq64siyvrx22fv5s2y46i4rnv28z6xvw62")))

(define-public crate-vegas-lattice-0.4.0 (c (n "vegas-lattice") (v "0.4.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v08gcmcwbmrwcl10pisnpjij69n9axjp0dgr7dmkv1121kmbilg")))

(define-public crate-vegas-lattice-0.5.0 (c (n "vegas-lattice") (v "0.5.0") (d (list (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hrxdp0b8aqm7j02gqhlkva7l00izfi31ggazwb9fc6mxx0miws")))

(define-public crate-vegas-lattice-0.5.1 (c (n "vegas-lattice") (v "0.5.1") (d (list (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vn4qkra4w7fzr5vdg1mkkszs64x76hp03d6fk31h41i59f0kd3v")))

