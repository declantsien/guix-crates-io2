(define-module (crates-io ve ro verona-rt) #:use-module (crates-io))

(define-public crate-verona-rt-0.0.1 (c (n "verona-rt") (v "0.0.1") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 2)) (d (n "verona-rt-sys") (r "=0.0.1") (d #t) (k 0)))) (h "0v13bywpb6wqc7ial4xi72n717b3xp84wffq2k41lhvirsa0rxm5") (f (quote (("systematic_testing" "verona-rt-sys/systematic_testing") ("flight_recorder" "verona-rt-sys/flight_recorder"))))))

(define-public crate-verona-rt-0.0.2 (c (n "verona-rt") (v "0.0.2") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 2)) (d (n "verona-rt-sys") (r "=0.0.2") (d #t) (k 0)))) (h "1vbxzyn0ypripf9zjga3y0njyadnghgz5bfjjvv11p46agrvqs52") (f (quote (("systematic_testing" "verona-rt-sys/systematic_testing") ("flight_recorder" "verona-rt-sys/flight_recorder"))))))

