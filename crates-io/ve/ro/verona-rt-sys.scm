(define-module (crates-io ve ro verona-rt-sys) #:use-module (crates-io))

(define-public crate-verona-rt-sys-0.0.1 (c (n "verona-rt-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 0)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1fvql4jn7wkj12hy3iq99sjvy8qs0skys5grav5w1rcfjp1b6bwd") (f (quote (("systematic_testing") ("flight_recorder"))))))

(define-public crate-verona-rt-sys-0.0.2 (c (n "verona-rt-sys") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 0)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1x8y9647w5ygfpycwyq2r5dkb8jv23yw7xmwjfclckr5kxd6y9mm") (f (quote (("systematic_testing") ("flight_recorder"))))))

