(define-module (crates-io ve dv vedvaring) #:use-module (crates-io))

(define-public crate-vedvaring-0.1.0 (c (n "vedvaring") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1af0irlsfs1fyaak9d6ypi3n6vkaax6d517pfjz3wngwcfcbjjy4")))

(define-public crate-vedvaring-0.1.1 (c (n "vedvaring") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "08b8piy6s7gczdkkpsmffm5hwsf35rlzwi14pjch66d9h552lc42")))

(define-public crate-vedvaring-0.1.2 (c (n "vedvaring") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1i26bqip3scvgymjp94ysn409vcylj1gd0n7r8r3prpdcbnlgisv")))

(define-public crate-vedvaring-0.1.4 (c (n "vedvaring") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1xpp6rh20f7435s8lc874kqnrcgzl6jb3dpxswwqw0z1fbakvyjl")))

(define-public crate-vedvaring-0.1.5 (c (n "vedvaring") (v "0.1.5") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1sakv62l8zz6a0qpaf6gxcmzkjhva7ll1c03dhvs0d2bha97xspc")))

(define-public crate-vedvaring-0.1.6 (c (n "vedvaring") (v "0.1.6") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1r2834aabvrla8h767274gb53wy366fpqf88ga5bgnld4y194aas")))

(define-public crate-vedvaring-0.1.7 (c (n "vedvaring") (v "0.1.7") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "12d2jcxk8myzy3jz3g7cyba7dd24j82lhzdxr8wnvgsc77q36s6k")))

(define-public crate-vedvaring-0.1.8 (c (n "vedvaring") (v "0.1.8") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "17s41vsxm09hsmqw3xn9fykqqd9zhnvv9a45rwz174bxviy3ijsg")))

(define-public crate-vedvaring-0.1.9 (c (n "vedvaring") (v "0.1.9") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "06pmmibs4kifj4r6yxglhis7l4rljwkw9m8c16mihn9m5agvdnl5")))

