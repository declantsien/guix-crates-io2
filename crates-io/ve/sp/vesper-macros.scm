(define-module (crates-io ve sp vesper-macros) #:use-module (crates-io))

(define-public crate-vesper-macros-0.0.0 (c (n "vesper-macros") (v "0.0.0") (h "1qvd891fba7lxiki4xcy945w32xr176i5q1kmz2ngmqx51ccqx0h")))

(define-public crate-vesper-macros-0.11.0 (c (n "vesper-macros") (v "0.11.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1r53k9m9k861psx16hcndmfpv6vf07qi6lp1vapp5jk28c7ay771")))

(define-public crate-vesper-macros-0.12.0 (c (n "vesper-macros") (v "0.12.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1gxsbm1xlqqxhijqjj8ysr32yw6qyank84v1njcw4w3ffdg34bm7")))

(define-public crate-vesper-macros-0.13.0 (c (n "vesper-macros") (v "0.13.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1gbr91y26hgjvkidg4c9fhbkzfw3wc4a0a906r5nydj6zyfnlkbr")))

