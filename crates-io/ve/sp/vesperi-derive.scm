(define-module (crates-io ve sp vesperi-derive) #:use-module (crates-io))

(define-public crate-vesperi-derive-0.1.0 (c (n "vesperi-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "07rqnvyzl5fm1b3jr5nk6iqjwfs638pwl66bak8sdrq53145jq9m")))

