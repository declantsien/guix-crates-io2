(define-module (crates-io ve sp vesper-lang) #:use-module (crates-io))

(define-public crate-vesper-lang-0.1.0 (c (n "vesper-lang") (v "0.1.0") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "strict_encoding") (r "^2.7.0-beta.1") (d #t) (k 0)))) (h "1c6j7nqqn8msfdpahkmxj84q5x50pz15hc5xwanfhvpi68xvsbpp") (f (quote (("default") ("all")))) (r "1.66")))

