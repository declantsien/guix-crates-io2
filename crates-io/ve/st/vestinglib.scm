(define-module (crates-io ve st vestinglib) #:use-module (crates-io))

(define-public crate-vestinglib-0.1.0 (c (n "vestinglib") (v "0.1.0") (h "02zg9h7fi3ys6kns38zxbs5xsd3j8k0xp3x2fbxldzvm9pvprx8i")))

(define-public crate-vestinglib-0.1.1 (c (n "vestinglib") (v "0.1.1") (h "09ba82iw6i6hnbj5a91bv8k2f4xdknvxmcgzvl3x3ffx40kkpdv2")))

(define-public crate-vestinglib-0.1.2 (c (n "vestinglib") (v "0.1.2") (h "1sa4aps5h9rcgszjczql3nlnml67jjm8f4mpcnpz28mfrp9l48b7")))

(define-public crate-vestinglib-0.1.3 (c (n "vestinglib") (v "0.1.3") (h "1rqrr3jbs6z48xq0nlx8bydmr5fm513spd4ng9hfa62ywyy0bjjd")))

(define-public crate-vestinglib-0.1.4 (c (n "vestinglib") (v "0.1.4") (h "189jq1d47hn7iqhgqv5xwap0ximlbk5v6zxylfwcsg7yf774np0z")))

