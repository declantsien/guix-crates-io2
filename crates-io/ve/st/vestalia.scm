(define-module (crates-io ve st vestalia) #:use-module (crates-io))

(define-public crate-vestalia-0.1.0 (c (n "vestalia") (v "0.1.0") (d (list (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "06c83qpff88pxa451jzhg3lmz40qn7gshmv0qwlm8vjdlixs573h")))

