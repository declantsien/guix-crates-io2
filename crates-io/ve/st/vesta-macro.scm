(define-module (crates-io ve st vesta-macro) #:use-module (crates-io))

(define-public crate-vesta-macro-0.1.0 (c (n "vesta-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vesta-syntax") (r "^0.1") (d #t) (k 0)))) (h "14sm05w2nwz2qk51ldbm3b6h22yjyfy3mk476adc718xhc17ig0i") (y #t)))

(define-public crate-vesta-macro-0.1.1 (c (n "vesta-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vesta") (r "^0.1") (d #t) (k 2)) (d (n "vesta-syntax") (r "^0.1") (d #t) (k 0)))) (h "0sscr9ydmkihi0fh714hcfyxh4xd36mrc4fi509zanapyy7sjpi0") (y #t)))

(define-public crate-vesta-macro-0.1.2 (c (n "vesta-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vesta") (r "^0.1") (d #t) (k 2)) (d (n "vesta-syntax") (r "^0.1") (d #t) (k 0)))) (h "0dbng2r2jzqs3ygx843ngd180hn5s2adiwg0sxgfa5qf35i5vz98")))

