(define-module (crates-io ve il veil) #:use-module (crates-io))

(define-public crate-veil-0.1.0 (c (n "veil") (v "0.1.0") (h "19gpq4dh0f93r2mcx9lbnr2gcnfmmpw00pf2x58qhwis93a5awla") (y #t)))

(define-public crate-veil-0.1.1 (c (n "veil") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "veil-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1gs7byrqg075i5i9jqy2irigwv7fx6makicwy3rf95zm5lr07r2d") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.2 (c (n "veil") (v "0.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "veil-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1bmbp6gqljxzymnnb4rymy4yhdg25m9g5m6wnc9mjmlj2ivn6cla") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.3 (c (n "veil") (v "0.1.3") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "veil-macros") (r "^0.1.3") (d #t) (k 0)))) (h "1ri44g6srjdcs1fa9274spfry2zp3yxbzsqjz0bpqm5k8dcncg1m") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.4 (c (n "veil") (v "0.1.4") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "veil-macros") (r "=0.1.4") (d #t) (k 0)))) (h "12c9r929hzvs4zmqshlbx1ddkqj2dg6fxf40ybl009hf95j02b9l") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.5 (c (n "veil") (v "0.1.5") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "veil-macros") (r "=0.1.5") (d #t) (k 0)))) (h "1mbgk64283lqmm9sn9y6vknq567pnn60h62mxfva4h0xz89ddjhs") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.6 (c (n "veil") (v "0.1.6") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "veil-macros") (r "=0.1.6") (d #t) (k 0)))) (h "103q119vhh6pwvdplbia00f7jifm2cgy3w7l83npqk1wg35453mv") (f (quote (("toggle"))))))

(define-public crate-veil-0.1.7 (c (n "veil") (v "0.1.7") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)) (d (n "veil-macros") (r "=0.1.7") (d #t) (k 0)))) (h "1123l1l6vs34l9vgpvadnj862802nsbshhq9i921r4i49fyb102p") (f (quote (("toggle"))))))

