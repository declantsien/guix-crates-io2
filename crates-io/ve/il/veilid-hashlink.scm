(define-module (crates-io ve il veilid-hashlink) #:use-module (crates-io))

(define-public crate-veilid-hashlink-0.1.0 (c (n "veilid-hashlink") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1cfp94hgdizxwc9sjiqb9pmz4vs146h1ianw6mk1gzicl2ysngba") (f (quote (("serde_impl" "serde"))))))

