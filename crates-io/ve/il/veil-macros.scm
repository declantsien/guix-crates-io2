(define-module (crates-io ve il veil-macros) #:use-module (crates-io))

(define-public crate-veil-macros-0.1.0 (c (n "veil-macros") (v "0.1.0") (h "1jjcjykzjfxlxq59rsxw9g9f4vgh3qdr8mhh3h61lxk8w9pccfhd") (y #t)))

(define-public crate-veil-macros-0.1.1 (c (n "veil-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s1652hfywvlqzmdyhhqvj14xs7yv3dcgrl4aqgwy5dg1fp0amnv")))

(define-public crate-veil-macros-0.1.2 (c (n "veil-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02f87n79pm6kw09bi84nldgalqbndjhp18vimha8ip6df74lrqjx")))

(define-public crate-veil-macros-0.1.3 (c (n "veil-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2casfnv915kjfyl73hj3zyzrn3lf1l567jb150gdmlnmx6xv31")))

(define-public crate-veil-macros-0.1.4 (c (n "veil-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s890y8bxmsh2x13dz8fy2zzj4apgzb6wx0fi3a160qmm024sg58")))

(define-public crate-veil-macros-0.1.5 (c (n "veil-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07xgcvw3chq6b6lvmgcs9hnlc7sb7hxl0017d55ax9vcx5mv53gm")))

(define-public crate-veil-macros-0.1.6 (c (n "veil-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03g679mb9lp7jxd0rgvwjmzxx52pfs7iyxbamg354q5s5f46pvqy")))

(define-public crate-veil-macros-0.1.7 (c (n "veil-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00ni3lf2ggqpavgw9w7x1hfmbhsnk9ljsf0y8iajbaridcf3iwng")))

