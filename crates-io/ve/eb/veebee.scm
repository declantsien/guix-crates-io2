(define-module (crates-io ve eb veebee) #:use-module (crates-io))

(define-public crate-veebee-1.0.0 (c (n "veebee") (v "1.0.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_dynamic_plugin" "bevy_gilrs" "bevy_gltf" "bevy_wgpu" "bevy_winit" "render" "png" "hdr" "mp3" "x11"))) (k 0)) (d (n "bevy_kira_audio") (r "^0.5.0") (f (quote ("flac" "mp3" "ogg" "wav"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14pxqcm4knvhf4h2kxj690nlx26hr7bnykcx4j5pql0qg5c9n3lg") (y #t)))

(define-public crate-veebee-1.1.1 (c (n "veebee") (v "1.1.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_dynamic_plugin" "bevy_gilrs" "bevy_gltf" "bevy_wgpu" "bevy_winit" "render" "png" "hdr" "mp3" "x11"))) (k 0)) (d (n "bevy_kira_audio") (r "^0.5.0") (f (quote ("flac" "mp3" "ogg" "wav"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "17iii7wjqzdp0ly0yg2dq9sblw5lg6hai5hsdkxz30kwz1d7qjcl") (y #t)))

