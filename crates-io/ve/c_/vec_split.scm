(define-module (crates-io ve c_ vec_split) #:use-module (crates-io))

(define-public crate-vec_split-0.1.0 (c (n "vec_split") (v "0.1.0") (h "0bazdqi0rv81zmp7gg1ddq2y5z46p47l5mwrp9d3xp1mx3w8ix9q")))

(define-public crate-vec_split-0.1.1 (c (n "vec_split") (v "0.1.1") (h "1b4ri0b1nqiacinala4305wcd91s2wsmkyapwm21xbbx9l8sa2qn")))

(define-public crate-vec_split-0.1.2 (c (n "vec_split") (v "0.1.2") (h "0z6pp05ik11r1nyyv20i66wxalvdh6hr661zzg40vam7lmzglqzx")))

(define-public crate-vec_split-0.1.3 (c (n "vec_split") (v "0.1.3") (h "00g36599ynbvs7921qb5fs5v370551mcdxfyxqsbmj5nsvj5fkpj")))

(define-public crate-vec_split-0.1.4 (c (n "vec_split") (v "0.1.4") (h "0fy9ryc7fbndgkizpwl02vchqyi72039rwd96qy1iq55ih1sdqg0")))

