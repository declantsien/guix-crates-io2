(define-module (crates-io ve c_ vec_mut_scan) #:use-module (crates-io))

(define-public crate-vec_mut_scan-0.1.0 (c (n "vec_mut_scan") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.3") (d #t) (k 2)))) (h "1xpr7y9m3vrms6a3wrkpngcv82kgs22ybsdkdvz8snh7664ndmdl")))

(define-public crate-vec_mut_scan-0.2.0 (c (n "vec_mut_scan") (v "0.2.0") (h "0zm7b91kmcqcssx3cggwq2im4as34lvl3xdr3zdm9a1ikc0vmf8g")))

(define-public crate-vec_mut_scan-0.3.0 (c (n "vec_mut_scan") (v "0.3.0") (h "1lkz66l8z13lvjll69s23vrca12inpyyh00kwg0djqsyil563vb8")))

(define-public crate-vec_mut_scan-0.4.0 (c (n "vec_mut_scan") (v "0.4.0") (h "086b90j4gvssxj5rz6gphdchnzzyyiknp0jh56p01s708k0khkx2")))

(define-public crate-vec_mut_scan-0.5.0 (c (n "vec_mut_scan") (v "0.5.0") (h "0vsqgcqzrcd04a1hkfgnv1aybgv2y40lfp3hb6fanz1z4cbg5c9g") (r "1.36.0")))

