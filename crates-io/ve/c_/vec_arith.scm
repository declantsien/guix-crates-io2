(define-module (crates-io ve c_ vec_arith) #:use-module (crates-io))

(define-public crate-vec_arith-1.0.0 (c (n "vec_arith") (v "1.0.0") (h "1csa3qinrsv6g0h5rb92ylc31vq0cq5vyjhrwkb35w9jv95i4l05") (y #t)))

(define-public crate-vec_arith-1.0.1 (c (n "vec_arith") (v "1.0.1") (h "1lh5rgk4gsgjh1qb12sx2qvcw4ih588aypxdn21wvmzgvnc7qz5z") (y #t)))

(define-public crate-vec_arith-1.0.2 (c (n "vec_arith") (v "1.0.2") (h "1qgmxncms9sm5dsbhsqxh48d7x4ksxc28qspn80cp6xafkkpm6wp") (y #t)))

(define-public crate-vec_arith-1.0.3 (c (n "vec_arith") (v "1.0.3") (h "1j7pbsz1dw7fcf5yr3xnhsg8vl0jg33r22j2y091y82nb6z5h51k")))

(define-public crate-vec_arith-1.0.4 (c (n "vec_arith") (v "1.0.4") (h "0mzlink3czq606rx45sqc9hpy7xx37ysgm7lr343x4q2l3yy23rf")))

