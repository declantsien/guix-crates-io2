(define-module (crates-io ve c_ vec_filter) #:use-module (crates-io))

(define-public crate-vec_filter-0.1.0 (c (n "vec_filter") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "vec_filter_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1gjhqh3kmrd3w2bcs1p56g2c9c95hk4h3f69p0s640fbbk4kyqlz")))

(define-public crate-vec_filter-0.2.0 (c (n "vec_filter") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "vec_filter_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "19s1pkwk8fd9hqmy0kqjsg6q8xki9xlpqs4nx2wkbl7sj6nzcv4q")))

(define-public crate-vec_filter-0.2.1 (c (n "vec_filter") (v "0.2.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "vec_filter_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "06wxxcsys23i0772j3dbrv32a1khjlxz9kdzk9a3icynffmx12qm")))

(define-public crate-vec_filter-0.2.2 (c (n "vec_filter") (v "0.2.2") (d (list (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "vec_filter_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "029vi3pp5plfgxi3prg9ksl03aybaaz450q7hxqx6jqcgl8rdqgg")))

