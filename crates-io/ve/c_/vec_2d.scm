(define-module (crates-io ve c_ vec_2d) #:use-module (crates-io))

(define-public crate-vec_2d-0.1.0 (c (n "vec_2d") (v "0.1.0") (h "0fxwk1gxcrz7ynq5dfhmyzvzwnz1q738ykmrq3ss3nyhaywmfzs8")))

(define-public crate-vec_2d-0.1.1 (c (n "vec_2d") (v "0.1.1") (h "0bbg6am9xvfn39wgjfpmsvw4v06pcwqlznidhyvvln5y6fgj70lk")))

(define-public crate-vec_2d-0.1.2 (c (n "vec_2d") (v "0.1.2") (h "1ipx65x6c823kwzvnqcfcs03pd5c7si2gxs51mw1c3672bg580wd")))

