(define-module (crates-io ve c_ vec_filter_derive) #:use-module (crates-io))

(define-public crate-vec_filter_derive-0.1.0 (c (n "vec_filter_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ispll233g7grwnjhnnp6wfj72r65bcxsn59ry3zyx6xidqxnmdp")))

(define-public crate-vec_filter_derive-0.1.1 (c (n "vec_filter_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1md4dsz26czg7j2xs6ri3k0l4k1h8jg789iwizrvbas6r0kgwdkw")))

(define-public crate-vec_filter_derive-0.1.2 (c (n "vec_filter_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "006098r5w9ni3fls4sxv1yrlkla0d1w6hspfrknsparijyfb1v5h")))

