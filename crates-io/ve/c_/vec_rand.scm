(define-module (crates-io ve c_ vec_rand) #:use-module (crates-io))

(define-public crate-vec_rand-0.1.0 (c (n "vec_rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "05pvvjsv39zdr9blgdxrwaxblci2dw7vdr15wbs3pmhx4q6wv3xz")))

(define-public crate-vec_rand-0.1.1 (c (n "vec_rand") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1b0fwwkm09d2nqfpgshrc7ldgjfxndajjla5va9knk6bymm3lvam")))

(define-public crate-vec_rand-0.1.2 (c (n "vec_rand") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1l8431fjrc11735zv0gp5vzx58c5a7gn5hzqwp7jfx32r7d5009z")))

