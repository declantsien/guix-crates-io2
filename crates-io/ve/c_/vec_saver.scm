(define-module (crates-io ve c_ vec_saver) #:use-module (crates-io))

(define-public crate-vec_saver-0.0.1 (c (n "vec_saver") (v "0.0.1") (h "0ccyvw342jsfc3q4dx83x09l9k7mrlhql03kaypz8jdjbfmaf219")))

(define-public crate-vec_saver-0.0.2 (c (n "vec_saver") (v "0.0.2") (h "19wlj2jnnnnpmbz6dqrr4k08nffh3c9d2gxxm43kxilsvpsik11s")))

