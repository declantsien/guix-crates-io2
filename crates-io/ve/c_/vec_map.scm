(define-module (crates-io ve c_ vec_map) #:use-module (crates-io))

(define-public crate-vec_map-0.0.1 (c (n "vec_map") (v "0.0.1") (h "1pfsh5jydhy2dp1l9gzh44hky84s8smscdnbxykvjyfcrr515fy1")))

(define-public crate-vec_map-0.1.0 (c (n "vec_map") (v "0.1.0") (h "10s7lkzyvn3fgaarxnbvlkg5wc67lxv7wlcfxidwa8glhbxb0y7f")))

(define-public crate-vec_map-0.2.0 (c (n "vec_map") (v "0.2.0") (h "085fzmvx3kyhwx2h6vrwvn715wdsxr16rb9riyd2z28ya9ggjyxy") (f (quote (("nightly"))))))

(define-public crate-vec_map-0.3.0 (c (n "vec_map") (v "0.3.0") (h "11by6bywlqfka6kyqy08dim8myagaywfw959x1mxigy61gvwiw5b") (f (quote (("nightly"))))))

(define-public crate-vec_map-0.4.0 (c (n "vec_map") (v "0.4.0") (h "00lgjk07wdggvq0b21y78a4yijcyai5i7j6rx4kx8wkw1n51218q") (f (quote (("nightly"))))))

(define-public crate-vec_map-0.6.0 (c (n "vec_map") (v "0.6.0") (d (list (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0pxhvwm6hp6n44dqd066vkv67vk2ql0wg0sgz314x88grgjyzifa") (f (quote (("eders" "serde" "serde_macros"))))))

(define-public crate-vec_map-0.7.0 (c (n "vec_map") (v "0.7.0") (d (list (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "15qq6q6kb5ic1gb1cbvnn535c4ppczkb4zrmfbc8w6fh7fwwikgq") (f (quote (("eders" "serde" "serde_derive"))))))

(define-public crate-vec_map-0.8.0 (c (n "vec_map") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "134q4kiznx47ynvd1y48cqcd704zhr6dv9xspcl1dl1a3iimnyw8") (f (quote (("eders" "serde" "serde_derive"))))))

(define-public crate-vec_map-0.8.1 (c (n "vec_map") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06n8hw4hlbcz328a3gbpvmy0ma46vg1lc0r5wf55900szf3qdiq5") (f (quote (("eders" "serde"))))))

(define-public crate-vec_map-0.8.2 (c (n "vec_map") (v "0.8.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1481w9g1dw9rxp3l6snkdqihzyrd2f8vispzqmwjwsdyhw8xzggi") (f (quote (("eders" "serde"))))))

