(define-module (crates-io ve c_ vec_security) #:use-module (crates-io))

(define-public crate-vec_security-0.1.0 (c (n "vec_security") (v "0.1.0") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0i77rqzgxdqlv4ikzfba8d1w02npz3ni9p52p587nnffqmgsb2rm")))

(define-public crate-vec_security-0.1.1 (c (n "vec_security") (v "0.1.1") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0fpzhfzn7f3njryh1sg7ynnf5i2azim0m781k1q2h8b345r2aba6")))

