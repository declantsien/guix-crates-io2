(define-module (crates-io ve c_ vec_file) #:use-module (crates-io))

(define-public crate-vec_file-0.1.0 (c (n "vec_file") (v "0.1.0") (d (list (d (n "desse") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "17cv6ryzv4y2yrwd30jnnyv7pcwgb4sv6s00mlabaffk82k85n0y") (y #t)))

(define-public crate-vec_file-0.1.1 (c (n "vec_file") (v "0.1.1") (d (list (d (n "desse") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "05aqwpyrp8xxjasqx6x048yp56sp8y5i2l3wzviq6kwmmyf43yac") (y #t)))

