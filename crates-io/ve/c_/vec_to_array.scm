(define-module (crates-io ve c_ vec_to_array) #:use-module (crates-io))

(define-public crate-vec_to_array-0.1.0 (c (n "vec_to_array") (v "0.1.0") (h "0hbyhx20d8ai2w5m6y8h7rb6nlgvrzggajswyxwxm9zm492xm3q9")))

(define-public crate-vec_to_array-0.2.0 (c (n "vec_to_array") (v "0.2.0") (h "0i5hkfhsf6s120vrpnp86x259d5phv41p3ppil846ppnh3r6hyqv")))

(define-public crate-vec_to_array-0.2.1 (c (n "vec_to_array") (v "0.2.1") (h "0dsr5ld710yijr3dd9szcywa0ds359266fhiidj4m2jmkv86kiq6") (r "1.73")))

(define-public crate-vec_to_array-0.2.2 (c (n "vec_to_array") (v "0.2.2") (h "1s9q3pdl6fbs8vi23r54a4kjlkwyv9ldnmpsjfdsry8mw7m5fl7h") (r "1.73")))

(define-public crate-vec_to_array-0.2.3 (c (n "vec_to_array") (v "0.2.3") (h "1x4xfflc39hr1gwc0pmppapnay83dd3c7h9hr5pxg4120iz29sbs") (r "1.73")))

