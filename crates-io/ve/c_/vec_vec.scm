(define-module (crates-io ve c_ vec_vec) #:use-module (crates-io))

(define-public crate-vec_vec-0.1.0 (c (n "vec_vec") (v "0.1.0") (d (list (d (n "stack-trait") (r "^0.2") (d #t) (k 0)))) (h "1jbmx7yvqp5xz8rsxj7d29jadh6mf72jixibxi2f4cy9z91c8y11")))

(define-public crate-vec_vec-0.2.0 (c (n "vec_vec") (v "0.2.0") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.2") (d #t) (k 0)))) (h "0qd5ppn4gqw45s1bjbihrbcc3v0z6q2dnwp9f3nd6myzby7cgrx6")))

(define-public crate-vec_vec-0.3.0 (c (n "vec_vec") (v "0.3.0") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.3") (d #t) (k 0)))) (h "0iawzgcn518ci5lprnz1f5k0kvk511z97cmrf15ycc43vlkv9mw9")))

(define-public crate-vec_vec-0.4.0 (c (n "vec_vec") (v "0.4.0") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.3") (d #t) (k 0)))) (h "17awpd2pcgp4sx3zcsabqh17f7m47l2rssywxsm50ig6y36dgqzj")))

(define-public crate-vec_vec-0.4.1 (c (n "vec_vec") (v "0.4.1") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.3") (d #t) (k 0)))) (h "17wzcp2w5vxykg248dsjwi4w1b3q6bq1k4syvim7sc888ckvx316")))

(define-public crate-vec_vec-0.5.0 (c (n "vec_vec") (v "0.5.0") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.3") (d #t) (k 0)))) (h "0ds1swz4b0507hkv00g5bi3yl2iq9k2z0aylyryv72rp98bk0j7f")))

(define-public crate-vec_vec-0.5.1 (c (n "vec_vec") (v "0.5.1") (d (list (d (n "lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "nougat") (r "^0.2") (d #t) (k 0)) (d (n "stack-trait") (r "^0.3") (d #t) (k 0)))) (h "0b241lb2imvrscdnd322rqxicl6fggsfd5f1a53kpj52l412np6g")))

