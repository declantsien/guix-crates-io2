(define-module (crates-io ve c_ vec_once) #:use-module (crates-io))

(define-public crate-vec_once-0.1.0 (c (n "vec_once") (v "0.1.0") (h "108a6d3zm34lzlpg1xz0hscfiivn5kj8b92cdkl6fg8nn8kck73g")))

(define-public crate-vec_once-0.1.1 (c (n "vec_once") (v "0.1.1") (h "08fl9hyfk3gvf5g6gh4ah1nrsrjq97viaci7rqa2df4qhag7xjwn")))

