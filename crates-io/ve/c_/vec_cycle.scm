(define-module (crates-io ve c_ vec_cycle) #:use-module (crates-io))

(define-public crate-vec_cycle-0.1.0 (c (n "vec_cycle") (v "0.1.0") (h "0a604d9gi3sbw6r2ncj3rqvhq0hhlvjq0y704jr2p0pg8160n78k")))

(define-public crate-vec_cycle-0.2.0 (c (n "vec_cycle") (v "0.2.0") (h "171wiimx7xi2118h8fihr91byqmsndn10krjz2whsbsq7m2bxa2g")))

