(define-module (crates-io ve da vedasynth) #:use-module (crates-io))

(define-public crate-vedasynth-0.0.1 (c (n "vedasynth") (v "0.0.1") (d (list (d (n "cpal") (r "^0.15") (f (quote ("jack"))) (d #t) (k 0)))) (h "0202mh83zkas7jgrhbn4gyx476lwwkr9afiqxbba8kfb7qaqxcad")))

(define-public crate-vedasynth-0.0.2 (c (n "vedasynth") (v "0.0.2") (d (list (d (n "cpal") (r "^0.15") (f (quote ("jack"))) (d #t) (k 0)))) (h "1zr1mb0mg237kaic18wd9n7280zzq2pv5faympk4kxmz1siday59")))

