(define-module (crates-io ve ge vegemite) #:use-module (crates-io))

(define-public crate-vegemite-0.1.0 (c (n "vegemite") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "0sl6jhm9b8jg2x5ycnr3jm2pq8mbq5nd6cg2d4w6w8ndz6spl9n8") (y #t)))

(define-public crate-vegemite-0.1.1 (c (n "vegemite") (v "0.1.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "0s4dv7dssa882nng7v9n7vc1vshivcy4m9gc011ydy5w0vi47q4c")))

(define-public crate-vegemite-0.1.2 (c (n "vegemite") (v "0.1.2") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "1rv3py75dp393nddr2x3kx7ssrigkc86jdvr7czibm4k6whcf3m0")))

(define-public crate-vegemite-0.2.0 (c (n "vegemite") (v "0.2.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "15xvywk5m7akgi0c6i9hpvj0wm2rc416m52a5r05ga8rl156509m") (r "1.65.0")))

(define-public crate-vegemite-0.2.1 (c (n "vegemite") (v "0.2.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "1v6vjv7qx0xgwx528vp9vncgldzc124ndda59ablbamji76b2xmq") (r "1.65.0")))

