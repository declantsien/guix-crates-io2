(define-module (crates-io ve ge vegetation-cli) #:use-module (crates-io))

(define-public crate-vegetation-cli-0.1.0 (c (n "vegetation-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "08x9ch218xn1mp61dad65aqcwykhxkd13l99xska8gyjzfbnxzbv")))

