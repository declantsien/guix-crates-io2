(define-module (crates-io ve lc velcro2) #:use-module (crates-io))

(define-public crate-velcro2-0.0.1 (c (n "velcro2") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elasticsearch") (r "^8.5.0-alpha.1") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12nfaagv9hjx023zkmq4jcm8ijgk8rqqv6vccijmazivqikwicqg")))

