(define-module (crates-io ve lc velcro_macros) #:use-module (crates-io))

(define-public crate-velcro_macros-0.2.0 (c (n "velcro_macros") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ay366pk9lb9plf2kqjhhb9sps1idh6y29kn20lwym48m9hhikmz")))

(define-public crate-velcro_macros-0.3.0 (c (n "velcro_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.3.0") (d #t) (k 0)))) (h "0xypcrg5k6121vsw7gqyfb7g1fahglwjqn6w9d8gnp3gmdrhj0lr")))

(define-public crate-velcro_macros-0.4.0 (c (n "velcro_macros") (v "0.4.0") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.4.0") (d #t) (k 0)))) (h "0f7ik6kvabrf8jj1zhmsj53z72bbcc918j7gli8188d8pzxpg9bw")))

(define-public crate-velcro_macros-0.4.1 (c (n "velcro_macros") (v "0.4.1") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.4.1") (d #t) (k 0)))) (h "1h68yvq6z9h7h1npmxd9x5rifxx6203xlx2baasgnjlggf1vl0qk")))

(define-public crate-velcro_macros-0.4.2 (c (n "velcro_macros") (v "0.4.2") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.4.2") (d #t) (k 0)))) (h "0539kgm7105dna892z9sn6w86sfqxw995ar7w95cvzw8wwk8y22c")))

(define-public crate-velcro_macros-0.5.0 (c (n "velcro_macros") (v "0.5.0") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.5.0") (d #t) (k 0)))) (h "02nbd3c9j6dspqgic53wp28faqbynmmqc05nwn9dg5d8hlbwsjld")))

(define-public crate-velcro_macros-0.5.1 (c (n "velcro_macros") (v "0.5.1") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.5.1") (d #t) (k 0)))) (h "0c6wci2cl77swipk1h9qr3kv7knvma4aqw2sxd4sxjb12g8i20fm")))

(define-public crate-velcro_macros-0.5.2 (c (n "velcro_macros") (v "0.5.2") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.5.2") (d #t) (k 0)))) (h "010r6kg1xfq9cy1yigrb7s5cqqii6wahbgrrbv3kzl01jqzcpmnl")))

(define-public crate-velcro_macros-0.5.4 (c (n "velcro_macros") (v "0.5.4") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "velcro_core") (r "=0.5.4") (d #t) (k 0)))) (h "1346p2srlw56y7ym5iji5dqapp01mhhd3rifw7k7g6dlsw3ch8vv")))

