(define-module (crates-io ve lc velcro) #:use-module (crates-io))

(define-public crate-velcro-0.1.0 (c (n "velcro") (v "0.1.0") (h "0cw42c9dw4380k5q4qywnmsn11j5p63bdzx6cpj7kyaimqs2kk1m")))

(define-public crate-velcro-0.1.1 (c (n "velcro") (v "0.1.1") (h "0iczb0y8i7bg58ygcddm3d6p9df1cjq9xz92wslz5c4xi5fwk6ky")))

(define-public crate-velcro-0.1.2 (c (n "velcro") (v "0.1.2") (h "1x3bgbcj8nzdakqsnviyxrj8bj8iazhvirdcxggspg7zr4hjds6j")))

(define-public crate-velcro-0.2.0 (c (n "velcro") (v "0.2.0") (d (list (d (n "velcro_macros") (r "=0.2.0") (d #t) (k 0)))) (h "1zkwshpyfy0sps8z6fxzf5qbva21hn6wdx7pl44za05djw8hplg3")))

(define-public crate-velcro-0.3.0 (c (n "velcro") (v "0.3.0") (d (list (d (n "velcro_macros") (r "=0.3.0") (d #t) (k 0)))) (h "13l6zqyladjsdh2502crx3k4ihaism4wv853dv12kb2m2q1maa3s")))

(define-public crate-velcro-0.4.0 (c (n "velcro") (v "0.4.0") (d (list (d (n "velcro_macros") (r "=0.4.0") (d #t) (k 0)))) (h "18r2m1yhs34kl07fvxgg799kwxl7nxp7grhys6b13xmcx3gg6id9")))

(define-public crate-velcro-0.4.2 (c (n "velcro") (v "0.4.2") (d (list (d (n "velcro_macros") (r "=0.4.0") (d #t) (k 0)))) (h "0mbpd18214bpzqpm7c4mmnjzwd97g99v02i6k16pr2lljnsi8dv1")))

(define-public crate-velcro-0.4.3 (c (n "velcro") (v "0.4.3") (d (list (d (n "velcro_macros") (r "=0.4.1") (d #t) (k 0)))) (h "1scb9jjzy40jkv2vp7pn4cfhk8iflszlrcf7g2d86bnm1fiqjhls")))

(define-public crate-velcro-0.4.4 (c (n "velcro") (v "0.4.4") (d (list (d (n "velcro_macros") (r "=0.4.2") (d #t) (k 0)))) (h "0l7m85xsd7kaygk3wh7kpfy2lg29c5wn2ibkq5m0lswabhmhwsn1")))

(define-public crate-velcro-0.5.0 (c (n "velcro") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 2)) (d (n "velcro_macros") (r "=0.5.0") (d #t) (k 0)))) (h "12c7kgdnpk4kg8jb4r9fyx2gvw5is4ajghr4kmnmzskbz3p08x8q")))

(define-public crate-velcro-0.5.1 (c (n "velcro") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 2)) (d (n "velcro_macros") (r "=0.5.1") (d #t) (k 0)))) (h "02m8c84mll17nr6nddizj14lpmy2hngj0yx4c9b408k4viadcy5q")))

(define-public crate-velcro-0.5.2 (c (n "velcro") (v "0.5.2") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 2)) (d (n "velcro_macros") (r "=0.5.1") (d #t) (k 0)))) (h "0hsj12fbnvdlm2xpq0ispyv9lcb76wx4g05czmpxn5g1yd9hqlbm")))

(define-public crate-velcro-0.5.3 (c (n "velcro") (v "0.5.3") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 2)) (d (n "velcro_macros") (r "=0.5.2") (d #t) (k 0)))) (h "1gq7gqxsabcm1i3n66zlrcqnbpg14p2wfcav6dmrc9vm9cm4wmc5")))

(define-public crate-velcro-0.5.4 (c (n "velcro") (v "0.5.4") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 2)) (d (n "velcro_macros") (r "=0.5.4") (d #t) (k 0)))) (h "1z4nc54ljgfij1ncw138ryg47mbchp26vq07fdsk845shccabiii")))

