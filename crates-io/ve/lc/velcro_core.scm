(define-module (crates-io ve lc velcro_core) #:use-module (crates-io))

(define-public crate-velcro_core-0.3.0 (c (n "velcro_core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jxbvdhyb9zbzp23fl2ql6yw055jvwwgn4n940x925dm9dg8q80l")))

(define-public crate-velcro_core-0.4.0 (c (n "velcro_core") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jply3viibsfl3k6rrd2ll2prrqd6lz42m7nn1xczi3rkv7j3vhl")))

(define-public crate-velcro_core-0.4.1 (c (n "velcro_core") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05gacllz9n0s2xbl3a7xfk14h8chjksfd3ndhfwps5awnkajr635")))

(define-public crate-velcro_core-0.4.2 (c (n "velcro_core") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15p3zxvm0d482w3ramj6hzpvkh23698y4gxavh4ndjzanawdfhk7")))

(define-public crate-velcro_core-0.5.0 (c (n "velcro_core") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c9a46yfqhk3yb6z2zgir15h9j4mdn82rlfbnqmcnqhkyazwwx8v")))

(define-public crate-velcro_core-0.5.1 (c (n "velcro_core") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "13hjhbp7xa0ngj6a0qx0xhavw5dc0pyw8pkgjafhz4yd684gja6b")))

(define-public crate-velcro_core-0.5.2 (c (n "velcro_core") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "166ln0ixy2l7mhr74gyhifchi1jy6chflz675cwarhqd3nxmq4lr")))

(define-public crate-velcro_core-0.5.4 (c (n "velcro_core") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0v4ixhkg7liqn8b75x41bb3r0f4pm013c23yhwa7d6wq0xfz8b3l")))

