(define-module (crates-io ve ca vecarray) #:use-module (crates-io))

(define-public crate-vecarray-0.1.0 (c (n "vecarray") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "161hzr3cb06dvzw428w3xy1h0pynhzd8awsm84dxblmb88mcni02") (f (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1.1 (c (n "vecarray") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "12r1ijlsz6ph9j0jcq08z0ghkaw1ij2a7cncw9gmagd14fr04f9a") (f (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1.2 (c (n "vecarray") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "14d0y75188nlfc71k0g8ksc046p3gbj8v60k3hsygg86z26ij7cn") (f (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1.3 (c (n "vecarray") (v "0.1.3") (d (list (d (n "parity-codec") (r "^4.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0gzqxks19xlq4789znibarkznad2zglhjq4bap351nfpnxrqmmnl") (f (quote (("std" "serde/std" "parity-codec/std") ("default" "std"))))))

