(define-module (crates-io ve rd verde-derive) #:use-module (crates-io))

(define-public crate-verde-derive-0.1.0 (c (n "verde-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iba282zy32q7bpnypxapjniwvvn04d1j7jlw6wqd5fmhlxsrlrj") (f (quote (("serde"))))))

