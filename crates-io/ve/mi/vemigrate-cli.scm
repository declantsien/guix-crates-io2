(define-module (crates-io ve mi vemigrate-cli) #:use-module (crates-io))

(define-public crate-vemigrate-cli-0.3.0 (c (n "vemigrate-cli") (v "0.3.0") (d (list (d (n "cdrs") (r "^2.1.0") (d #t) (k 0)) (d (n "cdrs_helpers_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vemigrate") (r "^0.3") (d #t) (k 0)))) (h "14sdd78qvprhh43d4xpyj5pckhg75gp2p1yf2pm4laz45bkaii2h")))

(define-public crate-vemigrate-cli-0.3.1 (c (n "vemigrate-cli") (v "0.3.1") (d (list (d (n "cdrs") (r "^2.1.0") (d #t) (k 0)) (d (n "cdrs_helpers_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vemigrate") (r "^0.3") (d #t) (k 0)))) (h "16hdpkvf9jcsx29a3q7j7rvdpd3mx3in1acxcihyp63hkmaf1xia")))

