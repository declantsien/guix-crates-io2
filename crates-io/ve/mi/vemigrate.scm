(define-module (crates-io ve mi vemigrate) #:use-module (crates-io))

(define-public crate-vemigrate-0.1.0 (c (n "vemigrate") (v "0.1.0") (h "18npcdy1rc0s50zdrcvv1ry1nggy60c5s2p8q5m6vhpxn9xxsh8v")))

(define-public crate-vemigrate-0.1.1 (c (n "vemigrate") (v "0.1.1") (h "0sav2yymyb2p95izhnd740f6457pxwwj64dkvm1r105y7ach6pdx")))

(define-public crate-vemigrate-0.2.0 (c (n "vemigrate") (v "0.2.0") (h "1fm2ldns3p6chrl3krjyvcxwi04ng6wwf6mr2czxrvwq99ppa305")))

(define-public crate-vemigrate-0.3.0 (c (n "vemigrate") (v "0.3.0") (h "13fd2r124dbcwzh745vh9s9c3s995xizq8r0n7ks53yzcbz1id4k")))

(define-public crate-vemigrate-0.3.1 (c (n "vemigrate") (v "0.3.1") (h "1r1dyqsb0wg7qrnyyvnrp9a7rh2pibmrv321gbymnmpx6pawkyy2")))

