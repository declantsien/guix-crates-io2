(define-module (crates-io ve rc vercel_lambda) #:use-module (crates-io))

(define-public crate-vercel_lambda-0.2.0 (c (n "vercel_lambda") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0mv22d5ry9r0gh84dq533f2dqdyfy112hcynb8mws3qz8sxsf7hw")))

