(define-module (crates-io ve rc vercel_runtime_macro) #:use-module (crates-io))

(define-public crate-vercel_runtime_macro-0.3.0 (c (n "vercel_runtime_macro") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^0.3.0") (d #t) (k 0)))) (h "14rpm307syz74xvyvkpsc5wssjz45q308hnfpkyxsbs360pqyll3") (y #t)))

(define-public crate-vercel_runtime_macro-0.3.1 (c (n "vercel_runtime_macro") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^0.3.1") (d #t) (k 0)))) (h "00ji58pygfflj5bqdrb4dqk1939qw25df1y34p52bfig3wqaqyhk") (y #t)))

(define-public crate-vercel_runtime_macro-0.3.2 (c (n "vercel_runtime_macro") (v "0.3.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^0.3.2") (d #t) (k 0)))) (h "08jxzzjvc4ak7g3sa7pr05nf1sy8hiag84arqasz4qxyah25b3j9") (y #t)))

(define-public crate-vercel_runtime_macro-0.3.3 (c (n "vercel_runtime_macro") (v "0.3.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^0.3.3") (d #t) (k 0)))) (h "1cxykca3d0sra7wmpwa2f20dy8gk08jhr9wz2z2lfcy4d7nqxnxv") (y #t)))

(define-public crate-vercel_runtime_macro-0.3.4 (c (n "vercel_runtime_macro") (v "0.3.4") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^0.3.4") (d #t) (k 0)))) (h "0vygdzgchxfskhmdh8bq9i4c1z149l6fjn2didg99gc2b1h5m8lx") (y #t)))

(define-public crate-vercel_runtime_macro-1.0.0 (c (n "vercel_runtime_macro") (v "1.0.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.0.0") (d #t) (k 0)))) (h "1ba61irhpipxllmfksxf8s8aicc93rbcxzvjp626c163vx806awj")))

(define-public crate-vercel_runtime_macro-1.0.1 (c (n "vercel_runtime_macro") (v "1.0.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.0.1") (d #t) (k 0)))) (h "1rpds7qy8qsllwbc73039sxn218qy7lb61ykpq23hm71dvg0rra3")))

(define-public crate-vercel_runtime_macro-1.0.2 (c (n "vercel_runtime_macro") (v "1.0.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.0.2") (d #t) (k 0)))) (h "09a33j81rdyzcd3imanz8rfp78ihapmqf31zysv7nb4wqx02mzb3")))

(define-public crate-vercel_runtime_macro-1.1.0 (c (n "vercel_runtime_macro") (v "1.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.1.0") (d #t) (k 0)))) (h "12zrcgfa2hy576vbjdm4n6zwsh7kpd216mjak38nszh1apcmz0pm")))

(define-public crate-vercel_runtime_macro-1.1.1 (c (n "vercel_runtime_macro") (v "1.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.1.1") (d #t) (k 0)))) (h "0v4m56c2b3401qsh2nfngh6w9v5libxyvcl2brjgd2njgsygbahd")))

(define-public crate-vercel_runtime_macro-1.1.2 (c (n "vercel_runtime_macro") (v "1.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.1.2") (d #t) (k 0)))) (h "1m3vnxb3zrkb5mz1sa6ig87ha1v5vq0q3vk9k4s0m6nrl7abda7b")))

(define-public crate-vercel_runtime_macro-1.1.3 (c (n "vercel_runtime_macro") (v "1.1.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vercel_runtime_router") (r "^1.1.3") (d #t) (k 0)))) (h "1404qfy3g5yp0kbqy6hpbjbmwa1vnj03qs9arnjd4hsc58w5vhcg")))

