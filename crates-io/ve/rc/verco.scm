(define-module (crates-io ve rc verco) #:use-module (crates-io))

(define-public crate-verco-1.0.0 (c (n "verco") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)))) (h "04jx1gq7nfa0dydra5cpfch3snwnh477m9zljzsnz3pfa0ama4g6")))

(define-public crate-verco-1.1.0 (c (n "verco") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)))) (h "0ndfsqp5dvjhh6dd0769ddzbzhc24gqyq81h3r62izyc4qn21b7j")))

(define-public crate-verco-1.2.0 (c (n "verco") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)))) (h "0ga4bcxwa88w8vzf2x0ymmq0m79lqm5z1kddfk3jkrv52xi051a5")))

(define-public crate-verco-1.3.0 (c (n "verco") (v "1.3.0") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)))) (h "10h600ixp5y003qy1wxhm229vcm9a8h6pj3x4r5vd9c0sawm825f")))

(define-public crate-verco-1.4.0 (c (n "verco") (v "1.4.0") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)))) (h "00rv1cnv21qn0l80zr9sxawvy8acsdd77wwq02qinmrm1fmnrxry")))

(define-public crate-verco-1.4.1 (c (n "verco") (v "1.4.1") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "1xazhg1vfh764wmzw7chhx2dqgyi0866fvmb0dny3jyxracac7i9")))

(define-public crate-verco-1.4.2 (c (n "verco") (v "1.4.2") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "0ak8p80agqgb0g3zb6c0dbwn6lw0120p06why8xqjyygx1riz7x3")))

(define-public crate-verco-1.4.3 (c (n "verco") (v "1.4.3") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "1i6139x3iwdxkwcpjmhv7l8fv4pmpwby4f3hf9k07cr614yk97y9")))

(define-public crate-verco-1.4.4 (c (n "verco") (v "1.4.4") (d (list (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "0pcazwn3ifgc8y74gg8w148x9hnn7rbc1zpprh3iiwqry3i9szy2")))

(define-public crate-verco-1.5.0 (c (n "verco") (v "1.5.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "1y33an03bv5g9rdrgmbjiikhvxq06pmvq378i9xkdwp2k1rk8jam")))

(define-public crate-verco-1.6.0 (c (n "verco") (v "1.6.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "18mbadsbcmy8d45b57lvws537m9gcms27ddmnah97rcs32r0i8d1")))

(define-public crate-verco-1.7.0 (c (n "verco") (v "1.7.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "1d8kr3chawm0qz32h7nyghlqqk47jcr8ks04sbd3qhprx5y6p8f7")))

(define-public crate-verco-2.0.0 (c (n "verco") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "1ya028w5w3x76ymwy225s8m34dqdcxmb63v4jg642w8kawlpsbgf")))

(define-public crate-verco-2.1.0 (c (n "verco") (v "2.1.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "0rkkcg9kldch0135kb6zhw7m5myc8bvfsss0qxzk4dw0xbmshb0g")))

(define-public crate-verco-2.2.0 (c (n "verco") (v "2.2.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "181wsanb3r8jmrhln7bjiv1150z4ad4s53zywgl5lj3z0idi29ig")))

(define-public crate-verco-2.3.0 (c (n "verco") (v "2.3.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "0i36qyni20kv30sk6sabd9ciz273x7jh5fm2j9izxyjiy0gpyiy9")))

(define-public crate-verco-2.3.1 (c (n "verco") (v "2.3.1") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "0w058jchjw5apvsrl4mv4vmraic6x6hxhffzyvf5a6vkdkwifgyy")))

(define-public crate-verco-3.0.1 (c (n "verco") (v "3.0.1") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "02imww5l1zh91zhf7k0128n8zzh3vf07a7r5wk3n7cv3605ljacl")))

(define-public crate-verco-3.1.0 (c (n "verco") (v "3.1.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "018hx9h8dc3fq954fhmqaq9l0bgljh21gfzpnbvw03qzizhm4kc6")))

(define-public crate-verco-3.2.0 (c (n "verco") (v "3.2.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (d #t) (k 0)))) (h "07jwyhx6pihqij0ynzbcxg2qcr2b66a1aw4js1s2wp6vgxp1dr2i")))

(define-public crate-verco-3.3.0 (c (n "verco") (v "3.3.0") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)))) (h "07p59xz8lnd9qzg7m2k4n4k05vxiwwhp8aiz9kx0vm4b1ykmz8a4")))

(define-public crate-verco-3.3.1 (c (n "verco") (v "3.3.1") (d (list (d (n "crossterm") (r "^0.10.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)))) (h "1ci2x2l0lzdnpr8xn8xgrd9yw8p0w77qa2fgchbxys4imhvcb9ms")))

(define-public crate-verco-4.0.0 (c (n "verco") (v "4.0.0") (d (list (d (n "crossterm") (r "^0.16") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)))) (h "16phyrm766fwj54nwjbl0axcqbap6qk19z9zf8v73kww8yg7nidq")))

(define-public crate-verco-4.2.0 (c (n "verco") (v "4.2.0") (d (list (d (n "crossterm") (r "^0.16") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)))) (h "19ldd793z7wy4xn22fkhrcw39lzjb9wdd4y9ycp2xm37yldr4vaw")))

(define-public crate-verco-4.10.0 (c (n "verco") (v "4.10.0") (d (list (d (n "crossterm") (r "^0.17.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "17sc5z36gmcki5jd437p1mypm3sapm5qdd5k9qh1k8h9jn2v99ks")))

(define-public crate-verco-4.12.0 (c (n "verco") (v "4.12.0") (d (list (d (n "crossterm") (r "^0.17.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "0zh2wbln8nbcfrwr6qjnvrhdl739l063m5h1j20577fkpgkssd2l")))

(define-public crate-verco-5.0.0 (c (n "verco") (v "5.0.0") (d (list (d (n "crossterm") (r "^0.17.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "0ilk75q9r2gqa854j2whqim63b98r2wwlg846nff66z1ggs84hqr")))

(define-public crate-verco-5.1.0 (c (n "verco") (v "5.1.0") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "1f556zqybpiva7xqzcdfq27j497ijalmvhhka1jd3f86i03qvjix")))

(define-public crate-verco-5.2.0 (c (n "verco") (v "5.2.0") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "07p0lh0ny63dcgj6vlkmp6sisg6gn46flhc9ms97aqha12q8r6jl")))

(define-public crate-verco-5.2.1 (c (n "verco") (v "5.2.1") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "09kvydz58y52wxhh0cn1k081ykhbmd1fan0lz39n7n53wwid7m1a")))

(define-public crate-verco-5.3.0 (c (n "verco") (v "5.3.0") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "06wbz25qrrw4hnvxf0xbdm48q1d6y655xl2mr2228if862fvikb4")))

(define-public crate-verco-5.4.1 (c (n "verco") (v "5.4.1") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "0pl3gihwvwj7njg542j5j9xb328n8813y4ypm63w2jzvhfr4qssr")))

(define-public crate-verco-5.4.2 (c (n "verco") (v "5.4.2") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "0aiskf9mph4ry68bhqcpx0d512mb4gssfyrb1z8hrikv5plcjqh0")))

(define-public crate-verco-5.5.1 (c (n "verco") (v "5.5.1") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "1rjpfr2s58iabf2hdb7ckmmzjin3d370k3s6xggj3n6yrxi53ar4")))

(define-public crate-verco-5.5.2 (c (n "verco") (v "5.5.2") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "0xjzkfdaq5b2zh2ibdfxn9bn0maarzfzq6g7hkgl8djybmlrj1ib")))

(define-public crate-verco-5.5.3 (c (n "verco") (v "5.5.3") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "1jcv2flp0z4igf4zxqm5ijjs3116z5gva6ybfjv8xd0gckgl9cfv")))

(define-public crate-verco-5.5.4 (c (n "verco") (v "5.5.4") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.1") (d #t) (k 0)))) (h "01nvyjjnk8mx54yliczd6ljg7znmrhfnz608vd5z3l5cnjyndny0")))

(define-public crate-verco-6.1.0 (c (n "verco") (v "6.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nff1dd29raffypdslfckbdkfqgafzy9vcbqv6bv3qzrgyxzn98d")))

(define-public crate-verco-6.1.1 (c (n "verco") (v "6.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04fzi4wjf0ak2an1gk6wq9h8cgvj718024wwcagbpi7nbbf6w9p1")))

(define-public crate-verco-6.1.2 (c (n "verco") (v "6.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ydrih6hl4mddzgfn1a1iypvki2a2ankcz3iza3vxc476ywnkphb")))

(define-public crate-verco-6.2.0 (c (n "verco") (v "6.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x1gnbvnc5gccl35wvaclws7dvpkwzikgzh2mac9hwdyk3r1d2zk")))

(define-public crate-verco-6.3.0 (c (n "verco") (v "6.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rchip6k7443vl897rxiln05fl0f9vln499pkr3qx1jpaf7xsfg5")))

(define-public crate-verco-6.3.1 (c (n "verco") (v "6.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12v9jixqh1559qzk91nw59mrzzmqxcywvjk9508lpq30rh46spgi")))

(define-public crate-verco-6.3.2 (c (n "verco") (v "6.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01gf935x0ki61z32kvp2brs3s1fl4nnpwbyqjag59pgn4vd7xx2g")))

(define-public crate-verco-6.3.3 (c (n "verco") (v "6.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kaqcdssz4xpjk5vf1nv7148bk8sj14kwbn55yvqkcfychxjdinn")))

(define-public crate-verco-6.4.0 (c (n "verco") (v "6.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d9gf02f0ckdgkkw6b5g7k4vf3a20padm3x8g0b9hxxzqnz0fg08")))

(define-public crate-verco-6.5.0 (c (n "verco") (v "6.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jgpaf2b4rddqgyrd9g92hiqg0vgaqkscw7qfz5y5n1rhn81ds0w")))

(define-public crate-verco-6.5.4 (c (n "verco") (v "6.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1szdq60ss8nk6ikqzm9gy9h74cld2k09lkdk7r6730c7kl0py3d8")))

(define-public crate-verco-6.5.5 (c (n "verco") (v "6.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mfpcl66q1shqa6jgf5cjqdiddy00wn7wrpsmpc9gnndlq776div")))

(define-public crate-verco-6.7.0 (c (n "verco") (v "6.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winuser" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0anzg4qcjdfdvvy3v29cs9lsy773mr672vlixx3hbysrczs2bzyw")))

