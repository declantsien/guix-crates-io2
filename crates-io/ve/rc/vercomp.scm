(define-module (crates-io ve rc vercomp) #:use-module (crates-io))

(define-public crate-vercomp-0.2.0 (c (n "vercomp") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gdv7wyxrdflaqprd5v6wn3vgy35bm00a2h33hfy53ry4kpkxh7l")))

(define-public crate-vercomp-0.3.0 (c (n "vercomp") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16z4n2v4033fka5sl20c0662r11f6va9pzvq72xy6n7msa7zwqp2")))

(define-public crate-vercomp-0.3.1 (c (n "vercomp") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a64l3nw1llc38fd5v3bl51qrkysagqxby2222i523wfhnnrxhqq")))

(define-public crate-vercomp-0.4.0 (c (n "vercomp") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yhmdpdjdm17n5k1g9prr5cm8asf823qzha0r1kpsyplg7c0g98w")))

(define-public crate-vercomp-0.5.0 (c (n "vercomp") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rqa4xjh9k6mxyjyy82db9s6h8rn1bv4hkhjqmdn8qdc6mm15pxc")))

