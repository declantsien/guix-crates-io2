(define-module (crates-io ve -o ve-orn-bindings) #:use-module (crates-io))

(define-public crate-ve-orn-bindings-0.0.1 (c (n "ve-orn-bindings") (v "0.0.1") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gi8fl2b0vd7r44ichf6z5wnl2cri60z6yk4xxn03ac69253gvxc")))

(define-public crate-ve-orn-bindings-0.0.2 (c (n "ve-orn-bindings") (v "0.0.2") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0692vjvwqrvw1fxbdjgixx8pszfvbsw0zyj0l7z56zgm9kpyjb4w")))

