(define-module (crates-io ve ri verify-same-kmer-content) #:use-module (crates-io))

(define-public crate-verify-same-kmer-content-1.0.0 (c (n "verify-same-kmer-content") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1ycnx03n4234xj0nh27m0h67258134agsg69qarqkhr22k4ji5is") (y #t) (r "1.63.0")))

(define-public crate-verify-same-kmer-content-1.0.1 (c (n "verify-same-kmer-content") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "05ib2f60ccpwj0bmjpr8rahk2yzsbsr0wm53sj9qxaiijf6vfddz") (y #t) (r "1.63.0")))

(define-public crate-verify-same-kmer-content-1.0.2 (c (n "verify-same-kmer-content") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1f8bgwimbqc0i1afrxxl0hw3mb2bsqd8g0hxnqai88620harp963") (r "1.63.0")))

(define-public crate-verify-same-kmer-content-1.1.0 (c (n "verify-same-kmer-content") (v "1.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "021h0bjjhrvbxbm4mliyzvmfy17mbbfqqzj7pmr4r40h81whh7h4") (r "1.63.0")))

(define-public crate-verify-same-kmer-content-1.1.1 (c (n "verify-same-kmer-content") (v "1.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0z0jqa9vymrxc5p4djpsimsv9cqbl0mdcv4d6nv0p3yw005pb0rv") (r "1.64.0")))

(define-public crate-verify-same-kmer-content-1.1.2 (c (n "verify-same-kmer-content") (v "1.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1nvxp95xaxvzbyan8dr1b0ghmix4rfdys32jfv5mxzilnwxq6x1w") (r "1.64.0")))

(define-public crate-verify-same-kmer-content-1.2.0 (c (n "verify-same-kmer-content") (v "1.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0fj0dkh7gazq1ss3xs03gr15fqqdk003975gbm07pmsciv8hwjw5") (r "1.64.0")))

(define-public crate-verify-same-kmer-content-1.2.1 (c (n "verify-same-kmer-content") (v "1.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "1x3l6hfj9vcvm7r8lshqrlf2xr8mjirhdxpwj80lffq2y0iqqpiz") (r "1.70.0")))

(define-public crate-verify-same-kmer-content-1.3.0 (c (n "verify-same-kmer-content") (v "1.3.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "0hk99jg7rphi3v05fjxhdvfbs9ysmh8jw3xj655n1c73mk13ilm2") (r "1.70.0")))

(define-public crate-verify-same-kmer-content-1.3.1 (c (n "verify-same-kmer-content") (v "1.3.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "08h4b69nwqkx26fvcxns90y84jbn5vzskx704cclvp5w2xasgsqq") (r "1.70.0")))

