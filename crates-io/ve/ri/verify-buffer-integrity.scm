(define-module (crates-io ve ri verify-buffer-integrity) #:use-module (crates-io))

(define-public crate-verify-buffer-integrity-0.1.0 (c (n "verify-buffer-integrity") (v "0.1.0") (d (list (d (n "bs58") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "solana-client") (r "^1.18.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.9") (d #t) (k 0)))) (h "01sz2ibh6dpd0kdnq3jrspm6254xl0p6fs02sj2pyhy5fd9fvyz8")))

(define-public crate-verify-buffer-integrity-0.2.0 (c (n "verify-buffer-integrity") (v "0.2.0") (d (list (d (n "bs58") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "solana-client") (r "^1.18.9") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.9") (d #t) (k 0)))) (h "1390j9aa4ln77xklmnvn6186qpfiykvf5ljzwqcqm9x71ld6c0sq")))

