(define-module (crates-io ve ri verify_macro) #:use-module (crates-io))

(define-public crate-verify_macro-0.1.0 (c (n "verify_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "17cq41k2m6bm109iwv5ckmg691jr84l5vczxz98x7l888qph41hi")))

(define-public crate-verify_macro-0.1.1 (c (n "verify_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0qkqqxrc7ni6sq2lnpwchi0kpgbxscl7j1qbif1hscdqrkv2wmsm")))

(define-public crate-verify_macro-0.2.0 (c (n "verify_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0ralls8cmc613nlx9pd4pyfi4pdhpb6rb7js8hx5s81sd82101r8")))

(define-public crate-verify_macro-0.2.1 (c (n "verify_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "02z7fqwr7lbsdajbx0idv9ll3drln7w1677xp3g6ggvv8fhfa65n")))

(define-public crate-verify_macro-0.2.2 (c (n "verify_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1qpa96ydcvzxj2h2wqm7z47q4cpxj03gcbykhdbnz71pigib4vma")))

(define-public crate-verify_macro-0.2.3 (c (n "verify_macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("derive" "proc-macro" "clone-impls" "default" "printing" "visit" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1a1qbz8r8pf59zawxmq7x2n5qcv7vcc4p5dfcnw42wd8730ahgya")))

