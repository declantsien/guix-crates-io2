(define-module (crates-io ve ri verilization-compiler-cli) #:use-module (crates-io))

(define-public crate-verilization-compiler-cli-0.1.0 (c (n "verilization-compiler-cli") (v "0.1.0") (d (list (d (n "verilization-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "verilization-compiler-cli-core") (r "^0.1.0") (d #t) (k 0)) (d (n "verilization-lang-java") (r "^0.1.0") (d #t) (k 0)) (d (n "verilization-lang-scala") (r "^0.1.0") (d #t) (k 0)) (d (n "verilization-lang-typescript") (r "^0.1.0") (d #t) (k 0)))) (h "0v3cad7y9dmkcm5hpiac0g9k8rgqnksagvxpmgwr89zxjxy6jcnz")))

