(define-module (crates-io ve ri verilization-lang-typescript) #:use-module (crates-io))

(define-public crate-verilization-lang-typescript-0.1.0 (c (n "verilization-lang-typescript") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)) (d (n "verilization-compiler") (r "^0.1.0") (d #t) (k 0)))) (h "0p8ahg2pr45p5n2930pxmgjpghxrxlmxkx90pwlnhnw5xq2dpyc5")))

