(define-module (crates-io ve ri verifyimg) #:use-module (crates-io))

(define-public crate-verifyimg-0.1.0 (c (n "verifyimg") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "gif") (r "^0.10.3") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)))) (h "0nzcbinblqjy1bbab08adavjh5dn7phsqvanw9mnf3pbq7p0z0f9")))

(define-public crate-verifyimg-0.1.1 (c (n "verifyimg") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)))) (h "00xgn1xrklbb3nznhglx6f93lnza6wfmzd597q0zld50gfzk5hyq")))

