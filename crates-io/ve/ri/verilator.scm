(define-module (crates-io ve ri verilator) #:use-module (crates-io))

(define-public crate-verilator-0.1.0 (c (n "verilator") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "0ba75ymdzxxsnv2n8n71220f0qjywh7shbk4ajdcslpym7ppqsin") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.1 (c (n "verilator") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "1nasrpck809r5w8zhv5vx49qa2g2n69n28bypjclbdifdnvi9fxz") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.2 (c (n "verilator") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "0qw1y1kd50cw6alc3sql0arkv9xw18k45g3jvsvfgsrqm23g62v3") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.3 (c (n "verilator") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "1yy4l22zbp9mb6lyva80zz0igl2rqkcqbq86q3x7w4rzvkwyqcfj") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.4 (c (n "verilator") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "0a8lj8bvl69lg41dccp9fn4am5bd4hszggypiwljrqvk8541s9s3") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.5 (c (n "verilator") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "1xyxr3n8955q0gm15x02mmsqqq2vq0m1s4qc4ns0zmyzws5bfv5z") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

(define-public crate-verilator-0.1.6 (c (n "verilator") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full" "visit"))) (o #t) (d #t) (k 0)))) (h "0l0wrxl9chnfpw2pdd23qrkpx4f9hfi31nvwpnhnr5jsj7z6pr0k") (f (quote (("module" "fnv" "syn") ("gen" "cc") ("default" "gen"))))))

