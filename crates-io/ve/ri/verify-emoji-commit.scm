(define-module (crates-io ve ri verify-emoji-commit) #:use-module (crates-io))

(define-public crate-verify-emoji-commit-0.1.0 (c (n "verify-emoji-commit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "^0.1.1") (d #t) (k 0)))) (h "1w2niw26xk2wr1gdz5a0n8nydas2g3py4zw2n0l1an8131w8ylhb")))

(define-public crate-verify-emoji-commit-0.1.1 (c (n "verify-emoji-commit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "^0.1.1") (d #t) (k 0)))) (h "0jwzx4prvc1jarjl96ih4h28liagxg4g9cz1ssl0n9nf7g7jbn2j")))

(define-public crate-verify-emoji-commit-0.2.0 (c (n "verify-emoji-commit") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "^0.1.1") (d #t) (k 0)))) (h "1milxpznk96f33jpb2b38yf43irk8jbzjjpn79158qlvfbqq33x0")))

