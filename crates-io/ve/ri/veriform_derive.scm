(define-module (crates-io ve ri veriform_derive) #:use-module (crates-io))

(define-public crate-veriform_derive-0.1.0 (c (n "veriform_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "07jyf6d4x30mnrxxa16jq8ci201klimbjfk67ym655wxmvxisrv8")))

(define-public crate-veriform_derive-0.2.0 (c (n "veriform_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1d4rw2gjg90g2s0j6nlvk6mfnxazhw0963bz14sqmm414iwbskmd")))

