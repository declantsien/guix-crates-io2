(define-module (crates-io ve ri verilogae_util) #:use-module (crates-io))

(define-public crate-verilogae_util-0.7.0-rc1 (c (n "verilogae_util") (v "0.7.0-rc1") (d (list (d (n "numpy") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "0hfx163mdc8s3vms9bgqp7g16rps1vwq4d1ngrl9lwnfz0h89vkm")))

(define-public crate-verilogae_util-0.7.0 (c (n "verilogae_util") (v "0.7.0") (d (list (d (n "numpy") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "0cc4qxzp43g35bf89f8ibyfblmn7r8biz0zdrixyif75s3bh0va6")))

(define-public crate-verilogae_util-0.7.1 (c (n "verilogae_util") (v "0.7.1") (d (list (d (n "numpy") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "19kvn4ljiah4hzvwl9yw0glzb2gswpq6dba097rz3kgl52b8z8ms")))

(define-public crate-verilogae_util-0.7.2 (c (n "verilogae_util") (v "0.7.2") (d (list (d (n "numpy") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "1l3s7fxvl2rzc74wxkxkhbhf3m8k6khxmhwdv3psk0sgxvy8nwd3")))

(define-public crate-verilogae_util-0.7.3 (c (n "verilogae_util") (v "0.7.3") (d (list (d (n "numpy") (r "^0.13") (d #t) (k 0)) (d (n "pyo3") (r "^0.13") (d #t) (k 0)))) (h "06iljxb5jv6rw2j7kvsfrwrb1rg7njglx9ybs2qf3y18aiv69l5x")))

(define-public crate-verilogae_util-0.8.0 (c (n "verilogae_util") (v "0.8.0") (d (list (d (n "numpy") (r "^0.13") (d #t) (k 0)) (d (n "pyo3") (r "^0.13") (d #t) (k 0)))) (h "1mqj4ghl9hl1nih0p3crw0hw32qs1bz552gdg0dl8fqgz9lvm1mx")))

