(define-module (crates-io ve ri verilog) #:use-module (crates-io))

(define-public crate-verilog-0.0.1 (c (n "verilog") (v "0.0.1") (d (list (d (n "lalrpop-util") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "0h5y3a20h7qlw9q44zms1rn5jz22wkgm99w85flsljz4h4wjlnd9")))

