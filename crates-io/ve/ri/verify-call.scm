(define-module (crates-io ve ri verify-call) #:use-module (crates-io))

(define-public crate-verify-call-0.1.0 (c (n "verify-call") (v "0.1.0") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)))) (h "1s82zrpc6gnawwlp3h2y4wq3xjhh3rqdp7wrrvxa1p389kcdv9w1") (r "1.56.1")))

(define-public crate-verify-call-0.1.1 (c (n "verify-call") (v "0.1.1") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)))) (h "19d42byhl7j76lm3ny6029aa1bzjfihj8hxhk9q1936ml8aj3z96") (r "1.56.1")))

(define-public crate-verify-call-0.1.2 (c (n "verify-call") (v "0.1.2") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)))) (h "0fy7fzjz31c87j7kna2k5dhg1n9zhqbmy52z4w327fwj996mgs63") (r "1.56.1")))

(define-public crate-verify-call-0.2.0 (c (n "verify-call") (v "0.2.0") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)))) (h "00c539y6g1l8mcls0m9b1qq7lry6q2377rv51jl0c3acpi042ghm") (r "1.56.1")))

(define-public crate-verify-call-0.3.0 (c (n "verify-call") (v "0.3.0") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)))) (h "13ag2hkijc9fb57c9w5pzs9hwd8prpnqjhlipx8nas6bm25mfj4q") (r "1.56.1")))

