(define-module (crates-io ve ri verify-keplr-sign) #:use-module (crates-io))

(define-public crate-verify-keplr-sign-0.1.0 (c (n "verify-keplr-sign") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "ecdsa") (r "^0.14.8") (f (quote ("der" "arithmetic" "digest"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "k256") (r "^0.11.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tendermint") (r "^0.28.0") (f (quote ("secp256k1"))) (d #t) (k 0)))) (h "17mb1sq8fnv0nbkhx330vlz8bjmnhbkphwbb42w8yfhild71d0rj")))

