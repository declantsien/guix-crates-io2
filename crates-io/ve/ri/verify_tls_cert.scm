(define-module (crates-io ve ri verify_tls_cert) #:use-module (crates-io))

(define-public crate-verify_tls_cert-0.1.0 (c (n "verify_tls_cert") (v "0.1.0") (d (list (d (n "ring") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.4") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "untrusted") (r "^0.3.1") (d #t) (k 0)) (d (n "untrusted") (r "^0.3.1") (d #t) (k 1)) (d (n "webpki") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.3") (d #t) (k 1)))) (h "0p2wi3g70r61za2gyyr2ss2w2ypkspgjslj6nrkv0dwhrrc3qvn0") (y #t)))

