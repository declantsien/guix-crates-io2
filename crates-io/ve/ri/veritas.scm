(define-module (crates-io ve ri veritas) #:use-module (crates-io))

(define-public crate-veritas-0.0.0 (c (n "veritas") (v "0.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)) (d (n "format") (r "^0.2.3") (d #t) (k 0)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "spectrum") (r "^0.0.0") (d #t) (k 0)))) (h "1kgiq81ah9rgim8qp0x1d1ijqi5ym9rgnvb07j4bj7fl1qrs57yx")))

