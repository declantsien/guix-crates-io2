(define-module (crates-io ve ri verilization-compiler) #:use-module (crates-io))

(define-public crate-verilization-compiler-0.1.0 (c (n "verilization-compiler") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)))) (h "14gxmcbffdfpi8il69kk39dppksd1ibalw6rbzwd4m0ppwcjs8ai")))

