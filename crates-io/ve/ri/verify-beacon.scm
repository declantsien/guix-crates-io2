(define-module (crates-io ve ri verify-beacon) #:use-module (crates-io))

(define-public crate-verify-beacon-0.1.2 (c (n "verify-beacon") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (f (quote ("compress"))) (d #t) (k 0)))) (h "02z34a3jq23z7iddygvyj6ni5gzv504jyjbqsjpj7glh7aikdbkb")))

