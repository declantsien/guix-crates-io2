(define-module (crates-io ve ri verified) #:use-module (crates-io))

(define-public crate-verified-0.1.0 (c (n "verified") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "verify_macro") (r "^0.1.0") (d #t) (k 0)))) (h "16xam518pvrsdwxwxmkcplmlhya8z3pjncy30axqhjb28mbjvarm")))

(define-public crate-verified-0.1.1 (c (n "verified") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "verify_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0hyfrsywjr8lh59c234rxl083sjzqifsspqfgvqbzq0s11yq1f6m") (y #t)))

(define-public crate-verified-0.1.2 (c (n "verified") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "verify_macro") (r "^0.1.1") (d #t) (k 0)))) (h "17ibdjpfiyl3iy917fs98krssbrxn6yamfl5l57s1w135y9p0s7v")))

(define-public crate-verified-0.2.0 (c (n "verified") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "verify_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1mchp80hmyy45n862ivf735ba54i7dr1jv31zajh06dkc8w1nxbi")))

(define-public crate-verified-0.2.1 (c (n "verified") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)) (d (n "verify_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0jldzxark555r0bm42jr48kn4rlgg7cqps56z8r929z00bk2mzb3")))

(define-public crate-verified-0.2.2 (c (n "verified") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)) (d (n "verify_macro") (r "^0.2.2") (d #t) (k 0)))) (h "1a9jl0gvshdjw3jw7h7qk3ap5vn8aqsqcq2hq892n7b94lhqkjzf")))

(define-public crate-verified-0.2.3 (c (n "verified") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)) (d (n "verify_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0knqbm5hh6nkpjyck0i40nv3bvic6ynrqpm2c0m8r39jws5vxb8d")))

