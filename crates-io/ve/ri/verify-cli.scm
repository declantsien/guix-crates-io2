(define-module (crates-io ve ri verify-cli) #:use-module (crates-io))

(define-public crate-verify-cli-0.1.0 (c (n "verify-cli") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0mrh4mvsyaw7iz3hrjjlc7d19s2kddqr3qbdxk3jk3nnxvvwsbdy")))

(define-public crate-verify-cli-0.1.1 (c (n "verify-cli") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "19ikf4hhdfb0cqidfbdxz7kvr6mnv6ir04lmiig1nhv8dd302wim")))

(define-public crate-verify-cli-0.2.0 (c (n "verify-cli") (v "0.2.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0pb6i7cw1233nz2z2fdf5k7iis8r5hm3m5k6z7ym6qz4wnn0w756")))

