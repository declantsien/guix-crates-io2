(define-module (crates-io ve ri verilated-module) #:use-module (crates-io))

(define-public crate-verilated-module-0.1.0 (c (n "verilated-module") (v "0.1.0") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3ah2dm0i26w32b0r1mhazsrd728xwz68xlqkjsaksis2jfvbg4")))

(define-public crate-verilated-module-0.1.1 (c (n "verilated-module") (v "0.1.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "19j1hx493ym8ab5w6vphrxxrybcbgsf44415sjgj3vvrf6qdg92a")))

