(define-module (crates-io ve ri verify-macros) #:use-module (crates-io))

(define-public crate-verify-macros-0.1.0 (c (n "verify-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0smfjx3a3lpwwlmygazgqaw5ry8jam38821lj9q6g79jb10c6yy2")))

