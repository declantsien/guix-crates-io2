(define-module (crates-io ve ri verilog-filelist-parser) #:use-module (crates-io))

(define-public crate-verilog-filelist-parser-0.1.0 (c (n "verilog-filelist-parser") (v "0.1.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1na8brpn7w4fdflg604njfv498iazpjx4nhlzw74g5r1jj7jnna1")))

(define-public crate-verilog-filelist-parser-0.1.1 (c (n "verilog-filelist-parser") (v "0.1.1") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0a0icjb5viyn87pv482wx3ycg4r9a372xymskivg0hkp501dpzg8")))

(define-public crate-verilog-filelist-parser-0.1.2 (c (n "verilog-filelist-parser") (v "0.1.2") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1zkiz15z46ahcs5vx3dma4l30337d55mchz620j6h0jk3i8pf58m")))

