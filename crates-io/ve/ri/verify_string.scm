(define-module (crates-io ve ri verify_string) #:use-module (crates-io))

(define-public crate-verify_string-0.1.0 (c (n "verify_string") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0wgc9brwf4hrcdwqw6vg1gsgd6am32skv602r5qnlapg0gjccda3")))

(define-public crate-verify_string-0.1.1 (c (n "verify_string") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1z8px5i5pvs7wb709fy6sa1qc98s3dw13fllbnbj5p3iffjs0csy")))

(define-public crate-verify_string-0.1.2 (c (n "verify_string") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "05s645si8arhmxswsw8fz8kxd3hk4rs0sz2wrr59v4h7n4gqlh2h")))

(define-public crate-verify_string-0.1.3 (c (n "verify_string") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0bk2prr89dx2w73ya4i20w7fma44bwg1k64d54rvgz6bqn75pzli")))

