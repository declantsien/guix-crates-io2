(define-module (crates-io ve ri verilot) #:use-module (crates-io))

(define-public crate-verilot-0.1.0 (c (n "verilot") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0plrym5bvshdwc19f6yh0wlcw0kk8i038i55qjc7kyrw1l5r1kvx")))

