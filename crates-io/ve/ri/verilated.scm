(define-module (crates-io ve ri verilated) #:use-module (crates-io))

(define-public crate-verilated-0.1.0 (c (n "verilated") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "verilator") (r "^0.1") (k 1)))) (h "1n5d141hmcflrwknglbnkprv5ifdp5240jilq70yrqscr036rilx")))

(define-public crate-verilated-0.1.1 (c (n "verilated") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "verilator") (r "^0.1") (k 1)))) (h "1kaba6lng02rd3zda3my4ly3dd69si6bijjb5richpa0fm3bkimr")))

(define-public crate-verilated-0.1.2 (c (n "verilated") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "verilator") (r "^0.1") (k 1)))) (h "0bgbh4ilra0jhki129q0plgryrxys1wkad0xvhz9j3cfp7a6vz3m")))

