(define-module (crates-io ve ri verity) #:use-module (crates-io))

(define-public crate-verity-0.0.0 (c (n "verity") (v "0.0.0") (h "0aa81yhpr581wwdsczlqa3n00waxz6n2blhdyhglkpga72p6b1n6")))

(define-public crate-verity-0.0.2 (c (n "verity") (v "0.0.2") (h "1lxwgm20sx0whngkkkvqj6k1b2qx6n6ifnzib95f2zwvh0lggg69")))

