(define-module (crates-io ve ri verilog-arc) #:use-module (crates-io))

(define-public crate-verilog-arc-0.4.0 (c (n "verilog-arc") (v "0.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "open-vaf") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)))) (h "0hghzwhaq4iszzd8x41h5m8lz9cdp5pa67avj97s0zdhljrl2qjf") (f (quote (("graph_debug" "open-vaf/graph_debug"))))))

(define-public crate-verilog-arc-0.4.1 (c (n "verilog-arc") (v "0.4.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "open-vaf") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)))) (h "17cxi9gxs80k11qab8cpgzzwwv0p0yhdh2khhsx07rw0rz0wx526") (f (quote (("graph_debug" "open-vaf/graph_debug"))))))

(define-public crate-verilog-arc-0.4.2 (c (n "verilog-arc") (v "0.4.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "open-vaf") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "09zw22xj5knlf6ww138hfd4bjqachqvbjx3z1hqk87hhswhnwikg") (f (quote (("graph_debug" "open-vaf/graph_debug"))))))

