(define-module (crates-io ve xr vexriscv) #:use-module (crates-io))

(define-public crate-vexriscv-0.0.1 (c (n "vexriscv") (v "0.0.1") (d (list (d (n "bare-metal") (r ">= 0.2.0, < 0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1xyg30wbq1l1xp26lfw7xp710x2yq0ym8bjwdwaxy6ghkjgg9ld2") (f (quote (("inline-asm"))))))

(define-public crate-vexriscv-0.0.2 (c (n "vexriscv") (v "0.0.2") (d (list (d (n "bare-metal") (r ">= 0.2.0, < 0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "0l8aiwfjmh3ciwxj7rdmggmg6a4xa9khfnlanfnrq1609sia9k58") (f (quote (("inline-asm"))))))

(define-public crate-vexriscv-0.0.3 (c (n "vexriscv") (v "0.0.3") (h "01ndfjg1wwfc3ksgh9b32bnz15inglc2qszaf9icfs04ilwkvb3f") (f (quote (("inline-asm"))))))

