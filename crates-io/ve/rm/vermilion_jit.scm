(define-module (crates-io ve rm vermilion_jit) #:use-module (crates-io))

(define-public crate-vermilion_jit-0.1.0 (c (n "vermilion_jit") (v "0.1.0") (d (list (d (n "vermilion-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "vermilion-object") (r "^0.1.0") (d #t) (k 0)) (d (n "vermilion-vm") (r "^0.1.0") (d #t) (k 0)))) (h "1mfzbvdmz0907s8xw9q0rwp57d0bjyg1gb47qsxrh6273y6fplzr")))

