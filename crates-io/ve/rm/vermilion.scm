(define-module (crates-io ve rm vermilion) #:use-module (crates-io))

(define-public crate-vermilion-0.0.0 (c (n "vermilion") (v "0.0.0") (h "0fvfymi5jxyqyig351vwy8fd0wvdnagqwpk94djnwwyykxar4h5r")))

(define-public crate-vermilion-0.0.0-unreleased (c (n "vermilion") (v "0.0.0-unreleased") (h "0z6zbqh1sn59fwmqda6kgygh26jwqipjpw8c7nr80crsp8y6ybwp")))

