(define-module (crates-io ve xi vexide-startup) #:use-module (crates-io))

(define-public crate-vexide-startup-0.1.0 (c (n "vexide-startup") (v "0.1.0") (d (list (d (n "vex-sdk") (r "^0.12.2") (d #t) (k 0)) (d (n "vexide-async") (r "^0.1.0") (d #t) (k 0)) (d (n "vexide-core") (r "^0.1.0") (d #t) (k 0)) (d (n "vexide-devices") (r "^0.1.0") (d #t) (k 0)))) (h "1hpidcwv70vkjv7jsvwd9qcb4qcnwgcl6a8c76vn6k4w0m1kbxbi") (f (quote (("no-banner") ("default"))))))

(define-public crate-vexide-startup-0.1.1 (c (n "vexide-startup") (v "0.1.1") (d (list (d (n "vex-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "vexide-async") (r "^0.1.1") (d #t) (k 0)) (d (n "vexide-core") (r "^0.2.0") (d #t) (k 0)) (d (n "vexide-devices") (r "^0.2.0") (d #t) (k 0)))) (h "0000hsj47aldj244b0xs86wvzx98i3x6nigr8d1s3x25jrdj7g7z") (f (quote (("no-banner") ("default"))))))

(define-public crate-vexide-startup-0.1.2 (c (n "vexide-startup") (v "0.1.2") (d (list (d (n "vex-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "vexide-async") (r "^0.1.1") (d #t) (k 0)) (d (n "vexide-core") (r "^0.2.0") (d #t) (k 0)) (d (n "vexide-devices") (r "^0.2.0") (d #t) (k 0)))) (h "12ga1snsj872jdqdwym6h6qr63n5iif7g3j7fzadys96i8dkni70") (f (quote (("no-banner") ("default"))))))

