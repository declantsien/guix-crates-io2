(define-module (crates-io ve xi vexillum) #:use-module (crates-io))

(define-public crate-vexillum-0.1.0 (c (n "vexillum") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18mvfmbhga43x6fg3zp0aw36ybx4chwx74j385n0cgrhgp4gng54")))

(define-public crate-vexillum-0.2.0 (c (n "vexillum") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qd6g86gkbrlk3vqydxg57ls43ly04hfsmjc2388pjvgrhvas6s2")))

(define-public crate-vexillum-0.3.0 (c (n "vexillum") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lami28v8np732jzh00i5j8kimp1bzgp5dylv177rakadv6awlyh")))

(define-public crate-vexillum-0.3.1 (c (n "vexillum") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ggrclxv8rfspdlcnc6pmcflmjnhzm2m6mkpc70scfbgh5l5yc4d")))

