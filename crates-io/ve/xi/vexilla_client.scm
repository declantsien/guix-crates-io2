(define-module (crates-io ve xi vexilla_client) #:use-module (crates-io))

(define-public crate-vexilla_client-0.0.1 (c (n "vexilla_client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0s451mrp8rk8354n389rrw9cimjaiv912d5jrb2aj8qr72k76929")))

(define-public crate-vexilla_client-1.0.0 (c (n "vexilla_client") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "053b15zdn09z2d4kp33syv6k7mm6d8ja0r8z58p8bfj00bw3grc7")))

