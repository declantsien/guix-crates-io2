(define-module (crates-io ve xi vexide-panic) #:use-module (crates-io))

(define-public crate-vexide-panic-0.1.0 (c (n "vexide-panic") (v "0.1.0") (d (list (d (n "vex-sdk") (r "^0.12.2") (d #t) (k 0)) (d (n "vexide-core") (r "^0.1.0") (d #t) (k 0)) (d (n "vexide-devices") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0xwdbclcn8i158ysck3w6y8mqj0s73w4n2miasarwnf48wdpb5w8") (f (quote (("default" "display_panics")))) (s 2) (e (quote (("display_panics" "dep:vexide-devices"))))))

(define-public crate-vexide-panic-0.1.1 (c (n "vexide-panic") (v "0.1.1") (d (list (d (n "vex-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "vexide-core") (r "^0.2.0") (d #t) (k 0)) (d (n "vexide-devices") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1wmshhp0cs6n5vha3xxf3ysxa7c41w44imdxf1hidy5gzm4v3967") (f (quote (("default" "display_panics")))) (s 2) (e (quote (("display_panics" "dep:vexide-devices"))))))

