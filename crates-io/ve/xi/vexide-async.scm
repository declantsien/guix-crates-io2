(define-module (crates-io ve xi vexide-async) #:use-module (crates-io))

(define-public crate-vexide-async-0.1.0 (c (n "vexide-async") (v "0.1.0") (d (list (d (n "async-task") (r "^4.5.0") (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-bool"))) (d #t) (k 0)) (d (n "vex-sdk") (r "^0.12.2") (d #t) (k 0)) (d (n "vexide-core") (r "^0.1.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.1") (d #t) (k 0)))) (h "0ldzwr2xa8xs81h7w0ylpx508fcvbkb65k6y2rfvb2bc51cvks5d")))

(define-public crate-vexide-async-0.1.1 (c (n "vexide-async") (v "0.1.1") (d (list (d (n "async-task") (r "^4.5.0") (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-bool"))) (d #t) (k 0)) (d (n "vex-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "vexide-core") (r "^0.2.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.1") (d #t) (k 0)))) (h "0z0iz262a5pqf4qgkyiwrfdv8xkkz0fwm11ysb2xasw3hgzsi9r9")))

