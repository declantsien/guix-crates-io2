(define-module (crates-io ve xi vexide-simulator-protocol) #:use-module (crates-io))

(define-public crate-vexide-simulator-protocol-0.1.0 (c (n "vexide-simulator-protocol") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w4861ppimv9mhmm80vc23fy82maci0xlw9qzwwl96x3g5smibiv")))

