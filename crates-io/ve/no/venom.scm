(define-module (crates-io ve no venom) #:use-module (crates-io))

(define-public crate-venom-0.0.1 (c (n "venom") (v "0.0.1") (h "1cscyz41v1gxd3slh8nc08ixs0p3sw5h284m1qjrnnm8vj08xyh6")))

(define-public crate-venom-0.0.2 (c (n "venom") (v "0.0.2") (h "1a9c0zj6a75xq14yddaklnx4jfyxafp0c8yqp4pj84w1653ypx6q")))

