(define-module (crates-io ve di vedirect) #:use-module (crates-io))

(define-public crate-vedirect-0.1.0 (c (n "vedirect") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i44zg9b6r50rd65ng55h6i9126972xzqvhdz6js7jnfqmsy7yfp")))

(define-public crate-vedirect-0.2.0 (c (n "vedirect") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serialport") (r "^4.1") (k 2)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m0bxxwjzqdc1nj91sjy2bmwm1yzibqa63cxmqwg5al44xvlg08s")))

