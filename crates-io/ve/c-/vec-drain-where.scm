(define-module (crates-io ve c- vec-drain-where) #:use-module (crates-io))

(define-public crate-vec-drain-where-1.0.0 (c (n "vec-drain-where") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1aswzbkkrbgcj4h881zn928fnqbky9z0gdjl6bk5ybsyhvhl1q13")))

(define-public crate-vec-drain-where-1.0.1 (c (n "vec-drain-where") (v "1.0.1") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "10prdijdbss8kky3a8ars9gn6yhj8qv27cf000fqqm5b1mdl47m3")))

