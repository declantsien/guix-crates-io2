(define-module (crates-io ve c- vec-with-positions) #:use-module (crates-io))

(define-public crate-vec-with-positions-0.1.1 (c (n "vec-with-positions") (v "0.1.1") (h "0zpxqb9inqp99hbx752qs754xqpqn1rprvradhn1d0bq2s2awldr")))

(define-public crate-vec-with-positions-0.1.2 (c (n "vec-with-positions") (v "0.1.2") (h "1igwimamigrjlg9fa4sx9skh8dsniaapbzdkywk5jc4dz3h8hks1")))

(define-public crate-vec-with-positions-0.2.0 (c (n "vec-with-positions") (v "0.2.0") (h "0ja3hd8g4ncjhdrld2iz03fxw9g71xi5pqjj3rsj9p5sjbfs5w5m")))

(define-public crate-vec-with-positions-0.2.1 (c (n "vec-with-positions") (v "0.2.1") (h "1aidnm1bpbvaqcpv6qwq3qlrknynqqhjjhby3198ycj6xdr3rqar")))

(define-public crate-vec-with-positions-0.3.0 (c (n "vec-with-positions") (v "0.3.0") (h "0wiq94f31gnazcmmsax1lhpr3q9lsh7dhl468his0w547s6iaqvg")))

(define-public crate-vec-with-positions-0.3.1 (c (n "vec-with-positions") (v "0.3.1") (h "0qpmf5jx8l64x95agbphggrcgwkwmprw51zj56hawcv4jbhw0irg")))

(define-public crate-vec-with-positions-0.4.0 (c (n "vec-with-positions") (v "0.4.0") (h "06zmibrvy1s402i4c0j4plscnm5x6c8a6qzcpc6nlyd71wdd7szx")))

(define-public crate-vec-with-positions-0.4.1 (c (n "vec-with-positions") (v "0.4.1") (h "0da9ih26374ba3dass736aqrrdgk2zndj9fz48bf6mi3w0nfkl3x")))

(define-public crate-vec-with-positions-0.4.2-alpha.0 (c (n "vec-with-positions") (v "0.4.2-alpha.0") (h "1kz7mvqiifdym5wxyk7cqjhrdz8hn8c7gz1s5bwy4gf8sldzmk72")))

(define-public crate-vec-with-positions-0.4.2-alpha.1 (c (n "vec-with-positions") (v "0.4.2-alpha.1") (h "1fri21s846dh1mxd1p32qa3dxznpk51bg68c9pbxlkx15hzplzvm")))

(define-public crate-vec-with-positions-1.0.0-alpha.0 (c (n "vec-with-positions") (v "1.0.0-alpha.0") (h "1jhk0fa13kwn4blj1qlrf11a7058h7lz953xv28x33bvyjipk0kk")))

(define-public crate-vec-with-positions-1.0.0-alpha.1 (c (n "vec-with-positions") (v "1.0.0-alpha.1") (h "07684iw70jhdnhf5qhmvwqf0xzy60yh65n33m1wbiapxw113q5mb")))

(define-public crate-vec-with-positions-1.0.0-alpha.2 (c (n "vec-with-positions") (v "1.0.0-alpha.2") (h "0wc1cz15l0csw2z7z93xhb8wnj91bmzn93dq6raqwy0dpw46n8si")))

(define-public crate-vec-with-positions-1.0.0-alpha.3 (c (n "vec-with-positions") (v "1.0.0-alpha.3") (h "02lqdmr5fidfd8mb4zf1jizpv6kh53rmp1xiaxsgxkkbpwv6sig6")))

(define-public crate-vec-with-positions-1.0.0-alpha.4 (c (n "vec-with-positions") (v "1.0.0-alpha.4") (h "027nnnr4w4pf8m6ynczyjcx8pcv3vrkfxcckj2d6yv1i07axy90f")))

(define-public crate-vec-with-positions-1.0.0-alpha.5 (c (n "vec-with-positions") (v "1.0.0-alpha.5") (h "0c463agqpzxmaxqilchbci942m5qg94q7nmg2d6wny3y3ry21l8n")))

(define-public crate-vec-with-positions-1.0.0-alpha.6 (c (n "vec-with-positions") (v "1.0.0-alpha.6") (h "0a2qm7l0gjdmc23m4z6fkkwiviji6fy9iw45ikilfb7bl99r79bc")))

(define-public crate-vec-with-positions-1.0.0-alpha.7 (c (n "vec-with-positions") (v "1.0.0-alpha.7") (h "13m70hdkhl5d4cr4pl158jm92sawlb8nrbmz649qxzysdckvmfja")))

(define-public crate-vec-with-positions-1.0.0-alpha.8 (c (n "vec-with-positions") (v "1.0.0-alpha.8") (h "1kx4v3d6lc2j8ill9kwfmsbm1yw7927q5s94r09dy26qrpvjzxsx")))

(define-public crate-vec-with-positions-1.0.0-alpha.9 (c (n "vec-with-positions") (v "1.0.0-alpha.9") (h "145043dd3j7nnddivs2r6qxfip9l6cxpnw56vxrgbhrmjd01kqgx")))

(define-public crate-vec-with-positions-2.0.0-alpha.0 (c (n "vec-with-positions") (v "2.0.0-alpha.0") (h "1p1xbnv43v1ir17q3h0zasp07s8q6qs6pl1i95mjg6l5z2yrg6k1")))

(define-public crate-vec-with-positions-2.0.0-alpha.1 (c (n "vec-with-positions") (v "2.0.0-alpha.1") (h "0p9klmjfjlbsdglm3m2jjpnl0d2246lby26lmha6qrwwdxzyj797")))

(define-public crate-vec-with-positions-3.0.0-alpha.0 (c (n "vec-with-positions") (v "3.0.0-alpha.0") (h "0zc7imfk3n4k8dazxzd35phwmzkfpay9kgy5ic58nmmz4xwa0vrk")))

(define-public crate-vec-with-positions-3.0.0-alpha.1 (c (n "vec-with-positions") (v "3.0.0-alpha.1") (h "1pgnjc8v1xhbm196jg7wlcpbnwc3yq2m8kjpmjqg37pd3i87sy9i")))

(define-public crate-vec-with-positions-3.0.0-alpha.2 (c (n "vec-with-positions") (v "3.0.0-alpha.2") (h "1rbn7n6y859yql6jqsg760a0mn46qfb8s9x6z4qmg4dv8lccgnwh")))

(define-public crate-vec-with-positions-3.0.0-alpha.3 (c (n "vec-with-positions") (v "3.0.0-alpha.3") (h "09x597m23l8dyanr91zkmcf4cirqwi5bhgl1ym0a4zys5sadc9n2")))

(define-public crate-vec-with-positions-3.0.0-alpha.4 (c (n "vec-with-positions") (v "3.0.0-alpha.4") (h "18gi43r42lnx07cd1fisbqfdir50csz4x9xrwr523l4v9zkjiw6d")))

(define-public crate-vec-with-positions-3.0.0-alpha.5 (c (n "vec-with-positions") (v "3.0.0-alpha.5") (h "1lqwr4gp2p6n9cqrwlak4hgra6nsidzy441q83yc34h5hy54r9ba")))

(define-public crate-vec-with-positions-3.0.0-alpha.7 (c (n "vec-with-positions") (v "3.0.0-alpha.7") (h "0mc2yif1ryy32jvlrqy6izcc0cx4qbz43g9z1zpspynbsj9mzl55")))

(define-public crate-vec-with-positions-3.0.0-alpha.8 (c (n "vec-with-positions") (v "3.0.0-alpha.8") (h "051r3wh8pxfr6n92x798mfnrnwv3i9asm7rjl5vhp6ia25spgim7")))

(define-public crate-vec-with-positions-3.0.0-alpha.9 (c (n "vec-with-positions") (v "3.0.0-alpha.9") (h "139bik99cf7pn1dahafv0yp95vyyi9iky0irvgj7z699as53sf8w")))

(define-public crate-vec-with-positions-3.0.0-alpha.10 (c (n "vec-with-positions") (v "3.0.0-alpha.10") (h "1kvg5da3dpa169np7kxlkwpak16c6acvj4jhclm8g1j2f0hclm66")))

(define-public crate-vec-with-positions-3.0.0-alpha.11 (c (n "vec-with-positions") (v "3.0.0-alpha.11") (h "0ffqbcin9vxpcpr63387ja6z1b9gbspvdvdny36ajsahacrhxfq3")))

(define-public crate-vec-with-positions-3.0.0-alpha.12 (c (n "vec-with-positions") (v "3.0.0-alpha.12") (h "0jdbbp470krc1v22rwasw33hijy039qsh9g114gzzwvkwyrxi203")))

(define-public crate-vec-with-positions-3.0.0-alpha.13 (c (n "vec-with-positions") (v "3.0.0-alpha.13") (h "17fb2cm1s81lmgmvxp4n6933bhgp8s2kpf3g95ps56xf38nb3nv8")))

(define-public crate-vec-with-positions-3.0.0-alpha.14 (c (n "vec-with-positions") (v "3.0.0-alpha.14") (h "1827cnnjhs03cnbbb1gv13m6cwvppg1kd99d5bgscwxrrha7n9sg")))

(define-public crate-vec-with-positions-3.0.0-alpha.15 (c (n "vec-with-positions") (v "3.0.0-alpha.15") (h "070b6byjydlcfqfchfb0kq033yxb2xf89czmyybfsq9nd0cn7cv2")))

(define-public crate-vec-with-positions-3.0.0-alpha.16 (c (n "vec-with-positions") (v "3.0.0-alpha.16") (h "0rys6jlxz8r7k3dw7l93ld7wql4bd08mdvcirif85mq946gbylnn")))

(define-public crate-vec-with-positions-3.0.0-alpha.17 (c (n "vec-with-positions") (v "3.0.0-alpha.17") (h "12q7kmyjpm4c175bxa47n3l2bspxqkr6c57fj8m916zxcgqxx156")))

(define-public crate-vec-with-positions-3.0.0-alpha.18 (c (n "vec-with-positions") (v "3.0.0-alpha.18") (h "15k77grs471wa8ichmr3mb4j7vy83530hjsq5aqhrcbxzdszqjqw")))

(define-public crate-vec-with-positions-3.0.0-alpha.19 (c (n "vec-with-positions") (v "3.0.0-alpha.19") (h "0491505j9w6nry7541ygsv3dl2dlvpi9ii39f38hpc7c275w2pck")))

(define-public crate-vec-with-positions-3.0.0-alpha.20 (c (n "vec-with-positions") (v "3.0.0-alpha.20") (h "12af9fs1wv2zig7nc6vc0xypras3xz38ircikri84h1xlf277kkd")))

(define-public crate-vec-with-positions-3.0.0-alpha.21 (c (n "vec-with-positions") (v "3.0.0-alpha.21") (h "0f2yf4xcaj91iildx0vl7rmq1ydrhccb4fr67s96kbw8zhj3f3v3")))

(define-public crate-vec-with-positions-3.0.0-alpha.23 (c (n "vec-with-positions") (v "3.0.0-alpha.23") (h "1ym2npq270krpp3snlkizash1ps24p8ygis8nnzgyj0f2ykv7jyv")))

(define-public crate-vec-with-positions-3.0.0-alpha.24 (c (n "vec-with-positions") (v "3.0.0-alpha.24") (h "1riibhsfcr3h5wnbm1wamfzcvh6n259a2in5i95dyd8cd25h9dim")))

(define-public crate-vec-with-positions-3.0.0-alpha.25 (c (n "vec-with-positions") (v "3.0.0-alpha.25") (h "1ibzr4nllpqblpk5k2nj7aaj6dn6q2ax6fpbih1qpm43nd5pbxvf")))

(define-public crate-vec-with-positions-3.0.0-alpha.26 (c (n "vec-with-positions") (v "3.0.0-alpha.26") (h "05ns1vv0v46hlq9kxkl0si9294vjxncjxhf6i8njmi91cik9x0zn")))

(define-public crate-vec-with-positions-3.0.0-alpha.27 (c (n "vec-with-positions") (v "3.0.0-alpha.27") (h "1in859j87xmnn6w3irisk9jvcddkl7hkd6abkdyz1flxw6vfhplq")))

(define-public crate-vec-with-positions-3.0.0-alpha.28 (c (n "vec-with-positions") (v "3.0.0-alpha.28") (h "0bkp90r0ia5m0pnyvixgxy285lvm4riacv3q3py17czby91ffff0")))

(define-public crate-vec-with-positions-3.0.0-alpha.29 (c (n "vec-with-positions") (v "3.0.0-alpha.29") (h "0zpahxfxlp2si8xvcp861c81yhdgp0936h744jdxyjw03cpin17g")))

(define-public crate-vec-with-positions-3.0.0-alpha.30 (c (n "vec-with-positions") (v "3.0.0-alpha.30") (h "12fh9zxymwqcf932whklzxig6k20l3zq2m05kb7s54wir5z5q48b")))

