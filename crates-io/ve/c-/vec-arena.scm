(define-module (crates-io ve c- vec-arena) #:use-module (crates-io))

(define-public crate-vec-arena-0.1.0 (c (n "vec-arena") (v "0.1.0") (h "0j6rglqzis6gyvah44wg0k1hrf2hn185kwbmvw113bd74wcqx65f")))

(define-public crate-vec-arena-0.1.1 (c (n "vec-arena") (v "0.1.1") (h "1s1ihh6qcbhp755kv463cnlshclmlc02fshbmmckqbx1yp00wrg0")))

(define-public crate-vec-arena-0.2.0 (c (n "vec-arena") (v "0.2.0") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0xmjrmay2i6mrslpbi8rk19919r53ssb8h9j9ny2p894kr5vk46d")))

(define-public crate-vec-arena-0.3.0 (c (n "vec-arena") (v "0.3.0") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1k4pivqpsa1g1y5x58i8sf2r6zz7m0lh6rnfasv21c89qvdr073j")))

(define-public crate-vec-arena-0.4.0 (c (n "vec-arena") (v "0.4.0") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0x3fwagxzgcxz6bibix638crzfprprnq65kamlqy1ybm5c4gf2a6")))

(define-public crate-vec-arena-0.4.1 (c (n "vec-arena") (v "0.4.1") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0ralzp5ics9dm8nvg1dsywgqxxxvdbb32h47dvws1jlz5g76wi07")))

(define-public crate-vec-arena-0.4.2 (c (n "vec-arena") (v "0.4.2") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0pxa1sb3wdg1f80nakjfz828rfrwzricq1m5vk38l68hkq8sfink")))

(define-public crate-vec-arena-0.5.0 (c (n "vec-arena") (v "0.5.0") (h "06drimq1dfalhdypj6v34v615w7g62mkvc3cc7s4743wym5vbpqp")))

(define-public crate-vec-arena-0.5.1 (c (n "vec-arena") (v "0.5.1") (h "1a3rdfh8pan90bxrhdwkygd108kn4dipvq3wsw6q1liwb3xagks7")))

(define-public crate-vec-arena-0.5.2 (c (n "vec-arena") (v "0.5.2") (h "0qdmvqxc0jyvjjdcax7khjinn4ic2chrn6pfb46pc283d5l85ccc")))

(define-public crate-vec-arena-1.0.0 (c (n "vec-arena") (v "1.0.0") (h "07866gmvn4cf2656bjf75nrmbnw4cj0cyqkv2wlmavzw5ndipz7a")))

(define-public crate-vec-arena-1.1.0 (c (n "vec-arena") (v "1.1.0") (h "1wfpz0d7pyyg0rblrbkg851p26f22f772gxcwfap1c4lnmjzdcil")))

(define-public crate-vec-arena-1.2.0 (c (n "vec-arena") (v "1.2.0") (h "0i9ihp4ykcykwa2fxqylli7na5b6l8ni346v3ldjvnrchxb3rqns")))

