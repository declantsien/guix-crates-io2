(define-module (crates-io ve c- vec-utilities) #:use-module (crates-io))

(define-public crate-vec-utilities-0.0.1 (c (n "vec-utilities") (v "0.0.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)))) (h "0lz1yzqgv5piwap4q32kcw2wmkwzq79q6ykywlphn54vpnzdh9r6")))

(define-public crate-vec-utilities-0.0.2 (c (n "vec-utilities") (v "0.0.2") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1ilgxhnrs0wkbkhgsigiq1i14g9cizc79zvydp2wnbxqpk9qir4a")))

