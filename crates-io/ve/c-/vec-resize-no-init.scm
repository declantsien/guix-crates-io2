(define-module (crates-io ve c- vec-resize-no-init) #:use-module (crates-io))

(define-public crate-vec-resize-no-init-0.1.0 (c (n "vec-resize-no-init") (v "0.1.0") (h "1wvq6pm7pbhhx70fkxfn2faq81kwv2afrvvp0dcvb3fs7lbb9vxa") (y #t)))

(define-public crate-vec-resize-no-init-0.1.1 (c (n "vec-resize-no-init") (v "0.1.1") (h "1q5gj0afbjchs4729r0bbim9rp87xxjwgs456dm1c3r9d4zrw9z2") (y #t)))

(define-public crate-vec-resize-no-init-0.1.2 (c (n "vec-resize-no-init") (v "0.1.2") (h "0f55zghp1hh310b4ynlbbjk075jngb7ib4hvbfhf0vmys0nl16q4") (y #t)))

