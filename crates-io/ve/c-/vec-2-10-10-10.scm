(define-module (crates-io ve c- vec-2-10-10-10) #:use-module (crates-io))

(define-public crate-vec-2-10-10-10-0.1.0 (c (n "vec-2-10-10-10") (v "0.1.0") (h "1k9bg3x20g6xkwr8wx4r2h5kss7awp6f4pz9h6a7nssk9yan6hfh")))

(define-public crate-vec-2-10-10-10-0.1.1 (c (n "vec-2-10-10-10") (v "0.1.1") (h "0kg7m2v7zwbb16l1agb3ncc4f3hjxfcschc14ssk2566ixbqmgcb")))

(define-public crate-vec-2-10-10-10-0.1.2 (c (n "vec-2-10-10-10") (v "0.1.2") (h "1giq864hlnvjcj1xzb5nwmjsjafiszplj2i4bsg15glpgzcc9wsf")))

