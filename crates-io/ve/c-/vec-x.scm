(define-module (crates-io ve c- vec-x) #:use-module (crates-io))

(define-public crate-vec-x-0.1.0 (c (n "vec-x") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "089damrwvy8zry44hy0v2c6vlxi8d157dnh49aa5n6cvfv49rnm0")))

(define-public crate-vec-x-0.1.1 (c (n "vec-x") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "0lc0c6fl8418nxqs59x4fq5ccbzvxifwz3p4l93rxjg7zsna3y6a")))

(define-public crate-vec-x-0.2.0 (c (n "vec-x") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "0kq1ys2bx2w6ia6d44p6i2hrk2mwjxzv4aaizz579vwqf7a5p4da")))

(define-public crate-vec-x-0.3.0 (c (n "vec-x") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1mbxc0hs74a8s1spr76wv4k5lwjbjlbfkc8xglhbmfbfyfhdg0bg")))

(define-public crate-vec-x-0.4.0 (c (n "vec-x") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1ivz3dcqy6ci2z9kcf3qjcyvjy5xs6m8lp4waz1jl6s11fbn8cb8")))

(define-public crate-vec-x-0.5.0 (c (n "vec-x") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1m38094vgl6g0cg271k7b7n8yy13whvknbf1kidmxlrzkapvpxq7")))

(define-public crate-vec-x-0.6.0 (c (n "vec-x") (v "0.6.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1jk8hx6g1sn6i2a0z57im7ka8c6wfvwaj8grlcpc77jfira2as1d")))

(define-public crate-vec-x-0.7.0 (c (n "vec-x") (v "0.7.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "0jqv5qa8zy4gj3wqhpl96bc3sf14m4c8jd8mvlhxl1iw3kwqnffy")))

