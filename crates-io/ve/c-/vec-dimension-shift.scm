(define-module (crates-io ve c- vec-dimension-shift) #:use-module (crates-io))

(define-public crate-vec-dimension-shift-1.0.0 (c (n "vec-dimension-shift") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1rjjkhhbzhswfxvrlcvpzwykmqh4drj3qk9l1lmsbxavw8v676jh") (f (quote (("default" "d2" "d3" "d4") ("d9") ("d8") ("d7") ("d6") ("d5") ("d4") ("d3") ("d2") ("d16") ("d15") ("d14") ("d13") ("d12") ("d11") ("d10"))))))

(define-public crate-vec-dimension-shift-1.0.1 (c (n "vec-dimension-shift") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1lb6ilfjkl78dzp1d6i16ms100r28d06s3nsaa1n0107mlwnnw62") (f (quote (("default" "d2" "d3" "d4") ("d9") ("d8") ("d7") ("d6") ("d5") ("d4") ("d3") ("d2") ("d16") ("d15") ("d14") ("d13") ("d12") ("d11") ("d10"))))))

