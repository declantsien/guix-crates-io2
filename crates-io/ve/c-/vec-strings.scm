(define-module (crates-io ve c- vec-strings) #:use-module (crates-io))

(define-public crate-vec-strings-0.1.0 (c (n "vec-strings") (v "0.1.0") (h "0zqqxdvd67zgdxk5869wj9r1bhrk4mnf4ahd8c07g691x2x3y5cw") (y #t)))

(define-public crate-vec-strings-0.2.0 (c (n "vec-strings") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0pifq2xhl1x83fm7xzxx3fi69m7cjzi2i2rddbry6ks8xvss3rvl") (y #t)))

(define-public crate-vec-strings-0.2.1 (c (n "vec-strings") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0dacbwvwlbydvr2rv4z47kjbpk1yjy608c9p3q1g7a5b54660lkm") (y #t)))

(define-public crate-vec-strings-0.2.2 (c (n "vec-strings") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "13hjar7f6p0cvfrlb4kfb545ksl2f433i28iwsv4vm9c0xhyx67s") (y #t)))

(define-public crate-vec-strings-0.3.0 (c (n "vec-strings") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "00ic4mzbn9s5m24aiqmxcb6b6z8x3swnbcyzinrd15swssz3jrr1") (y #t)))

(define-public crate-vec-strings-0.3.1 (c (n "vec-strings") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1nli121vqxvwby1i3w7bcgfn67nlydmv9z36v46wnhl0nfkv61wl") (y #t)))

(define-public crate-vec-strings-0.3.2 (c (n "vec-strings") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1jk068bhd3kxk7s1bdzimwgjkp1r2c52vjmwjh62s6i6dwi7ll02") (y #t)))

(define-public crate-vec-strings-0.3.3 (c (n "vec-strings") (v "0.3.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1h1a7a3a32cr4hfl1ab3z7kvd6mdg1hybs5vj7w51vrk9ls9p27i") (y #t)))

(define-public crate-vec-strings-0.4.0 (c (n "vec-strings") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1nr6sclsypzgrb7h3ajq60nsaf3w80s6mk9992n4mcjlkw02qscb") (y #t)))

(define-public crate-vec-strings-0.4.1 (c (n "vec-strings") (v "0.4.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1sr874ds3mdmq1yjmrcv1iyqn4h2cjr65ld5g985yyypsby5mh7d") (y #t)))

(define-public crate-vec-strings-0.4.2 (c (n "vec-strings") (v "0.4.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "068r98r261i1ghlxcw9q6xw32f24dpzk57aq5mas7fxj8ms1hm8r")))

(define-public crate-vec-strings-0.4.3 (c (n "vec-strings") (v "0.4.3") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "0ll49avda69fday1lzdi7979s4lbfalnab6r0bx9y2jdx650jal7")))

(define-public crate-vec-strings-0.4.4 (c (n "vec-strings") (v "0.4.4") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "02ppx3x7zivjx0l0j5j16yd0a9jdgnbwyk1cq68ssw627sgksn3x")))

(define-public crate-vec-strings-0.4.5 (c (n "vec-strings") (v "0.4.5") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "025g50wk3njzv133bcy1gh07pi3wk8rd8dssbgfyv8jvbsjhskvh")))

(define-public crate-vec-strings-0.4.6 (c (n "vec-strings") (v "0.4.6") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "1zn8bw0mc0lkjbv92pl7vxc79aw7abn6wc1r7lzx3nb82zraf1ip")))

(define-public crate-vec-strings-0.4.7 (c (n "vec-strings") (v "0.4.7") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "0z79x86am1p70khfax17an9s16n4vczwhcrpqbjr2k84z3d8lwlj")))

(define-public crate-vec-strings-0.4.8 (c (n "vec-strings") (v "0.4.8") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.4") (d #t) (k 0)))) (h "0nbnww23hi87dfn1bb0ii20cdgkhsdgx92i34aaj3vm7wa4r8l68")))

