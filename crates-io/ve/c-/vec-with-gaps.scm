(define-module (crates-io ve c- vec-with-gaps) #:use-module (crates-io))

(define-public crate-vec-with-gaps-0.1.0 (c (n "vec-with-gaps") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "11s8xphvz03q9kwflm6zy2h719s49kdfnvdiyy0mlvzrjiwyjh2m")))

(define-public crate-vec-with-gaps-0.1.1 (c (n "vec-with-gaps") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0gn9zk5bd21fhb170dh9r7fy5gl9xqwg15arrcka3dsp5hjbkbnh")))

(define-public crate-vec-with-gaps-0.2.0 (c (n "vec-with-gaps") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1ca2w3nwd2azajhlzflisi8105lcg9njdiawck5qi0cj9br774dc")))

(define-public crate-vec-with-gaps-0.3.0 (c (n "vec-with-gaps") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0nrzri1g042970fmf7f5nj3yncj7hkd0028cgmr60rjkr8cpfqfj")))

(define-public crate-vec-with-gaps-0.4.0 (c (n "vec-with-gaps") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1nnp80126yh6k719bbzkbpazsjwpxsz74bsjxbq1k8k6d8xb7rpv")))

(define-public crate-vec-with-gaps-0.5.0 (c (n "vec-with-gaps") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "13xnmwh06rgmh17lrpq86x2slr17w81jf6sl9291dav6whak5iyp")))

(define-public crate-vec-with-gaps-0.6.0 (c (n "vec-with-gaps") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1sgh92j89karppd2xvbiig5kiq2h55dqd0jah8xsbkq6c5iqs9x2")))

(define-public crate-vec-with-gaps-0.7.0 (c (n "vec-with-gaps") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1v70bv63bhxdvqvgwbx4lnpfszc6rg81gqnvn9sgqvz4dcrpf692")))

