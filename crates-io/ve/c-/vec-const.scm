(define-module (crates-io ve c- vec-const) #:use-module (crates-io))

(define-public crate-vec-const-1.0.0 (c (n "vec-const") (v "1.0.0") (h "0j4pyys5lmd4nib2mx0hh43q39gndzk85whsxb9innymis88c6fx") (y #t)))

(define-public crate-vec-const-1.0.1 (c (n "vec-const") (v "1.0.1") (h "109ajv5zj0kb4mn4xprxnjmvfd3zar5c4cwlqs9l1vy159nifar3") (y #t)))

(define-public crate-vec-const-1.1.0 (c (n "vec-const") (v "1.1.0") (h "1xc9sxj32fi7lnfrpfnvc7vbpivnkgy8qi2apn1pqxml13gj2q57") (y #t)))

(define-public crate-vec-const-1.1.1 (c (n "vec-const") (v "1.1.1") (h "14xp3smk8fiicna9wfcwbfiqqgiy3q83q5r13i7wqww5nidr32bw") (y #t)))

(define-public crate-vec-const-1.2.0 (c (n "vec-const") (v "1.2.0") (h "1rjkqdsbafw8b0s013yjir63wivbmhng4hbkhnfgrm5x4akfs3cd") (y #t)))

(define-public crate-vec-const-1.2.1 (c (n "vec-const") (v "1.2.1") (h "1xy5is803akj12qp0cx66jzslpfys898aiak3sjirls6fgrdv65n") (y #t)))

(define-public crate-vec-const-2.0.0 (c (n "vec-const") (v "2.0.0") (h "1bn8iv5jrr9ndcliv9zr501lqmmxyk2k2h3yfjf7hfjiwd7yz7a8") (y #t)))

(define-public crate-vec-const-2.0.1 (c (n "vec-const") (v "2.0.1") (h "06sds5bfyjhivri1x7kzwqkq4rpq4p9nwbwmc4hq3xxlk8cvg0cm") (y #t)))

(define-public crate-vec-const-3.0.0 (c (n "vec-const") (v "3.0.0") (h "08pnp42c85k2w6qxq63ha84zcxz5wq2vy4fr7j7g37jqhc0gaaiw") (y #t)))

(define-public crate-vec-const-3.0.1 (c (n "vec-const") (v "3.0.1") (h "1zq2mpcxckbkphmqgg132737ib9c3rismqbyr77h0i5gq168kvbs") (y #t)))

