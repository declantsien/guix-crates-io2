(define-module (crates-io ve c- vec-string) #:use-module (crates-io))

(define-public crate-vec-string-0.1.0 (c (n "vec-string") (v "0.1.0") (h "1x6l029cr6yfh7nmw3rj78v43w2lmdk3r1z2mgj8s8crrracf7xc")))

(define-public crate-vec-string-0.1.1 (c (n "vec-string") (v "0.1.1") (h "1spmn65hf8hi14735k0750srdxy9fc4p7dinzy0asx6i20cc2iaj")))

(define-public crate-vec-string-0.1.2 (c (n "vec-string") (v "0.1.2") (h "08kcjh4x2577n0yr9lmv26m8d8cf5ivmjvdga49j6sndppmf8c6c")))

(define-public crate-vec-string-0.2.0 (c (n "vec-string") (v "0.2.0") (h "1y7ypxb4a9jq1s6mq0blq5v9z2f76wwmi5d82avf25pq4xk28vyr")))

(define-public crate-vec-string-0.2.1 (c (n "vec-string") (v "0.2.1") (h "12i275va6yb4gp6zlbfq7ip62xam70dglmczzv6fnxcb2cn4i6zj")))

