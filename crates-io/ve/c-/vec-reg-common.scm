(define-module (crates-io ve c- vec-reg-common) #:use-module (crates-io))

(define-public crate-vec-reg-common-0.1.0 (c (n "vec-reg-common") (v "0.1.0") (h "0jdfvcv32x6ryi5xffglyxrqrmxa657lh906fxc4msqc3g3n7a0h")))

(define-public crate-vec-reg-common-0.2.0 (c (n "vec-reg-common") (v "0.2.0") (h "09ng7gmzx44mgsirn5xj6g7n2srxb6xrz0y75pafxcbv8gkkbyph")))

(define-public crate-vec-reg-common-0.3.0 (c (n "vec-reg-common") (v "0.3.0") (h "110wv2z77iswa9pmzxrlql9asism48ri9c9l8nm23f1mmwxiw8wy")))

(define-public crate-vec-reg-common-0.4.0 (c (n "vec-reg-common") (v "0.4.0") (h "02pqy6zsc4xaz8g0dmix3q61ih7f1q32acqm2ddkkii1c54gypn3")))

(define-public crate-vec-reg-common-0.5.0 (c (n "vec-reg-common") (v "0.5.0") (h "06kcvvc7740h6x20dzbw9cnk6qy01p7b9gq4s07fikigy1xlqnsm") (y #t)))

(define-public crate-vec-reg-common-0.5.1 (c (n "vec-reg-common") (v "0.5.1") (h "0ca8fyzyw6pdjw5f11w061sccvrm3kp3g5q2v49ypb3mi6ljkmg9")))

(define-public crate-vec-reg-common-0.6.0 (c (n "vec-reg-common") (v "0.6.0") (h "0mc17jd8v0iwd35hjj2rw1zv58rwmmjav1sz9d759hg3wpq2w67q")))

(define-public crate-vec-reg-common-0.7.0 (c (n "vec-reg-common") (v "0.7.0") (h "1xqj0cj5n51l7dyk7hngd1rdn50nrww4xahwsqkwqi602ha8qayp")))

(define-public crate-vec-reg-common-0.7.1 (c (n "vec-reg-common") (v "0.7.1") (d (list (d (n "tikv-jemallocator") (r "^0.5") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)))) (h "1qhxrgb1yr4bjf6xgkfp8y2hzhd0vr8jxkxrqfh5v8bznfds318h")))

