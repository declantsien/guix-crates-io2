(define-module (crates-io ve c- vec-option) #:use-module (crates-io))

(define-public crate-vec-option-0.1.0 (c (n "vec-option") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)))) (h "19kmj481xbcmig4dl8n9zcjsw7ihd1h8l8nq74df1cb8p9laz1mp") (f (quote (("nightly"))))))

(define-public crate-vec-option-0.2.0 (c (n "vec-option") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)))) (h "1kn6ki9phlvsn1vfiyfxvlhddsd7ckapmjilxa99ysw8zmgz9xry") (f (quote (("nightly"))))))

(define-public crate-vec-option-0.2.1 (c (n "vec-option") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)))) (h "0p33h32r2kk3zk3zdp6gliz78xakgypnn1fmjavqvqc16xzkfryd") (f (quote (("nightly"))))))

(define-public crate-vec-option-0.3.0 (c (n "vec-option") (v "0.3.0") (h "0klc6i5k5f79ac52scq1wppwhdnssqaxmxbb0nzjn02vzy425q8g") (f (quote (("nightly"))))))

