(define-module (crates-io ve c- vec-utils) #:use-module (crates-io))

(define-public crate-vec-utils-0.1.0 (c (n "vec-utils") (v "0.1.0") (h "058hlh030plhxjr5szqnr1v87a5fvzjlb5b423494n4ypwi76f5m")))

(define-public crate-vec-utils-0.1.1 (c (n "vec-utils") (v "0.1.1") (h "10k06ky376hxjvnwz9n3h8m4v1w5x1hs0pyjb9ay5wgdg1mrg233")))

(define-public crate-vec-utils-0.1.2 (c (n "vec-utils") (v "0.1.2") (h "1r8641qd4iyan8c908lrb9w446r5fkqc93pl77ag2zpbyx93p59a")))

(define-public crate-vec-utils-0.1.3 (c (n "vec-utils") (v "0.1.3") (h "09wvb9dl895ccs9j4blvsk1z7p4a9vk73gkiycq5nr5i6riv3w88")))

(define-public crate-vec-utils-0.1.4 (c (n "vec-utils") (v "0.1.4") (h "01wxg0w3ids22kdfq6g6g1fzispaphc99vss8bl5jcvlkdnrswnv")))

(define-public crate-vec-utils-0.2.0 (c (n "vec-utils") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0gxhq9ml6ij6fjj47x9p9b5l2kfzfa00qydb4yn2zb9wylmqp1mk")))

(define-public crate-vec-utils-0.3.0 (c (n "vec-utils") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0qb1v72jlnj1yv5ar3071ba0ymxxl7b30b3vxps6xhhnl159ib3d")))

