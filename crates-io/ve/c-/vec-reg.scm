(define-module (crates-io ve c- vec-reg) #:use-module (crates-io))

(define-public crate-vec-reg-0.1.0 (c (n "vec-reg") (v "0.1.0") (d (list (d (n "vec-reg-common") (r "^0.1.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0wsp487yvl70cqzcdfcv20dgbranmqfrdh5n2a5kp3vws8l8fnc2")))

(define-public crate-vec-reg-0.2.0 (c (n "vec-reg") (v "0.2.0") (d (list (d (n "vec-reg-common") (r "^0.2.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.2.0") (d #t) (k 0)))) (h "025p6jl4hjic60h7p0w1vl51crq98nmpy1hacc8ki9wqcnwhpnj2")))

(define-public crate-vec-reg-0.3.0 (c (n "vec-reg") (v "0.3.0") (d (list (d (n "vec-reg-common") (r "^0.3.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1m7hd39hbywb7rx844mzniswxnbajf858b13fk7c8p85y0x949kz")))

(define-public crate-vec-reg-0.4.0 (c (n "vec-reg") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0b2irj253v12n9bd9ci9513zn00q310ivmkx0flq3kczpjl9w6v8")))

(define-public crate-vec-reg-0.5.0 (c (n "vec-reg") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.5.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.5.0") (d #t) (k 0)))) (h "0xd9hdckbzb8blhgzam4cscc95qmslrx8w1hdpfal0hf58ri4rpj")))

(define-public crate-vec-reg-0.5.1 (c (n "vec-reg") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.5.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.5.0") (d #t) (k 0)))) (h "0740p0fiklhcwj11r9lnbmdgixp6hkxbrhi6m21b7kia9p7336ra")))

(define-public crate-vec-reg-0.6.0 (c (n "vec-reg") (v "0.6.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.6.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.6.0") (d #t) (k 0)))) (h "13c2anzfsz4qydhbm9w7f9z3xrf9ms3zgvrgcamy2c5ngpi02ylp")))

(define-public crate-vec-reg-0.7.0 (c (n "vec-reg") (v "0.7.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.7.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.7.0") (d #t) (k 0)))) (h "0hfn26935x0nmzm6f1dgn5fwf23awdk93876nacyw9dcyjf70hkm")))

(define-public crate-vec-reg-0.7.1 (c (n "vec-reg") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.7.0") (d #t) (k 0)) (d (n "vec-reg-macro") (r "^0.7.0") (d #t) (k 0)))) (h "0fz5rjdni6xr90cjs75kvjs0dzv0lwwp1pv85dvjmvq62dcha21x")))

