(define-module (crates-io ve c- vec-btree-map) #:use-module (crates-io))

(define-public crate-vec-btree-map-0.1.0 (c (n "vec-btree-map") (v "0.1.0") (h "1hcy9qf5dpgsx4pzpmlzb21ycck519a1d25ca09qcl5qfi4c6qv8")))

(define-public crate-vec-btree-map-0.2.0 (c (n "vec-btree-map") (v "0.2.0") (h "04w3jj85nr6774d9aj7ibiyp9k4nw3ml1nfy4qg2zwnmqj0l1b59")))

(define-public crate-vec-btree-map-0.3.0 (c (n "vec-btree-map") (v "0.3.0") (h "0q7667vhkmh15mwcy0y5z2mvhqhh6wn3dyw00a34nnf7ndzz9xkp")))

(define-public crate-vec-btree-map-0.4.0 (c (n "vec-btree-map") (v "0.4.0") (h "090sglmdahwfkw3ldc731a4njrwaxpizz5bj4a1ni8k3p25289j4")))

(define-public crate-vec-btree-map-0.5.0 (c (n "vec-btree-map") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0iy2azjjq22k1iyvvnz8kcydznnyav6k72sim1ngr4r2srl4flw0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-vec-btree-map-0.6.0 (c (n "vec-btree-map") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "07jqifql1m38qsvi7pnzpy0x9cihxz68nanw0ar6va5z8agvxpck") (s 2) (e (quote (("serde" "dep:serde"))))))

