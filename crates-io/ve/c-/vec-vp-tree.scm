(define-module (crates-io ve c- vec-vp-tree) #:use-module (crates-io))

(define-public crate-vec-vp-tree-0.1.0 (c (n "vec-vp-tree") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0wkzjr850wpkvkc5y16r2vs40fryh07f1jg9jngb37jbilxjasfj") (f (quote (("default" "strsim"))))))

(define-public crate-vec-vp-tree-0.1.1 (c (n "vec-vp-tree") (v "0.1.1") (d (list (d (n "order-stat") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (o #t) (d #t) (k 0)))) (h "132arh1fb2sr6p0dhjly45hh7pzfkkqrq3zw1cadlbp44wjv8dix") (f (quote (("default" "strsim"))))))

(define-public crate-vec-vp-tree-0.2.0-alpha.1 (c (n "vec-vp-tree") (v "0.2.0-alpha.1") (d (list (d (n "order-stat") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (o #t) (d #t) (k 0)))) (h "192m39yl2avc1xsjafsymi9y398v6nq8sxry4g4cq651blwzdwsn") (f (quote (("default" "strsim"))))))

