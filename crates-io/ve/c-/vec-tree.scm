(define-module (crates-io ve c- vec-tree) #:use-module (crates-io))

(define-public crate-vec-tree-0.1.0 (c (n "vec-tree") (v "0.1.0") (d (list (d (n "generational-arena") (r "^0.2.0") (d #t) (k 0)))) (h "0v9pqpk8wk6lg40f38v1x2d948krkyajjcjknnsbaz1im1y3y86v")))

(define-public crate-vec-tree-0.1.1 (c (n "vec-tree") (v "0.1.1") (d (list (d (n "generational-arena") (r "^0.2.0") (d #t) (k 0)))) (h "1c8vxaqkrvd8r37mh3zyvwanxp84c97v293a28izr7d6pdvz6qb8")))

(define-public crate-vec-tree-0.1.2 (c (n "vec-tree") (v "0.1.2") (d (list (d (n "generational-arena") (r "^0.2.0") (d #t) (k 0)))) (h "1rnmy62gqmlm71x3pii8l6cr3lpx86vb19n7f20kpb1wavw8ffmi")))

