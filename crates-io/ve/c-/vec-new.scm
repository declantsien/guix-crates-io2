(define-module (crates-io ve c- vec-new) #:use-module (crates-io))

(define-public crate-vec-new-0.0.1 (c (n "vec-new") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)))) (h "0mmcaqpq3wd99nxma003vc35w7zkd8vb2b97hk88j9crr99m12j6")))

(define-public crate-vec-new-0.0.2 (c (n "vec-new") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)))) (h "12pqsr4ydn0ic6jh34kyjhz5avyp6nk7s03xxgf4dkqmirzn2728")))

(define-public crate-vec-new-0.0.3 (c (n "vec-new") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)))) (h "1r07drdmn1xbpxq4dlax8vnmaj11773hby4mkkz2acn6aa5lxwqf")))

(define-public crate-vec-new-0.0.4 (c (n "vec-new") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)))) (h "18pzlpg5yhsks7kwxy7wpa72wvjrldmihzqfgm3m47x1rqc17fzy")))

