(define-module (crates-io ve c- vec-reg-macro) #:use-module (crates-io))

(define-public crate-vec-reg-macro-0.1.0 (c (n "vec-reg-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.1.0") (d #t) (k 0)))) (h "0ljnlczp9mb2k6ycxnvchx8irm2kz9b39m821mhqg5mcyap6jrgh")))

(define-public crate-vec-reg-macro-0.2.0 (c (n "vec-reg-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.2.0") (d #t) (k 0)))) (h "04b3w905y5vszishn8wq2vmkrqczqmjasczdcxv3r07pw0vc1w66")))

(define-public crate-vec-reg-macro-0.3.0 (c (n "vec-reg-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.3.0") (d #t) (k 0)))) (h "1dya1l76x2fimhcwhm16xr008ip0l5hbnqihim3yj145x37m53mi")))

(define-public crate-vec-reg-macro-0.4.0 (c (n "vec-reg-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.4.0") (d #t) (k 0)))) (h "0zqcawvg5v91bl9hk6vnlg9lwnnwjadxib5dr3gfvlzv8acyyks4")))

(define-public crate-vec-reg-macro-0.5.0 (c (n "vec-reg-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.5.0") (d #t) (k 0)))) (h "0k0jgicjzzfzv2602yfragrzv82n17jgigahr90h6mw4cwzq5z5i")))

(define-public crate-vec-reg-macro-0.5.1 (c (n "vec-reg-macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.5.0") (d #t) (k 0)))) (h "0iix5rwinvynvaf6dbk6q46s8dd4jgc1y7kwcivrfpgnh3h6lv9j")))

(define-public crate-vec-reg-macro-0.6.0 (c (n "vec-reg-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.6.0") (d #t) (k 0)))) (h "0bv61hs5rf7nqn3kg7cxmwp7928jylrd945lbgmfsy5apdi7s7w2")))

(define-public crate-vec-reg-macro-0.7.0 (c (n "vec-reg-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.7.0") (d #t) (k 0)))) (h "09k75461y91c9nfw93fraj339m8yivnzs3a9lr0hyr71fl8awnll")))

(define-public crate-vec-reg-macro-0.7.1 (c (n "vec-reg-macro") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "vec-reg-common") (r "^0.7.0") (d #t) (k 0)))) (h "0z3xdbsc5d6ppivla25f3xjnwq1rgkgl0b0gkalwm3xiwzvqcrhb")))

