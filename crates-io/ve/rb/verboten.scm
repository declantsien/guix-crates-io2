(define-module (crates-io ve rb verboten) #:use-module (crates-io))

(define-public crate-verboten-0.9.0 (c (n "verboten") (v "0.9.0") (d (list (d (n "eventlog") (r "^0.1") (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qargparser") (r "^0.5") (d #t) (k 0)) (d (n "registry") (r "^1") (d #t) (k 0)) (d (n "windows-service") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.8") (d #t) (k 0)))) (h "0ga1kqs6kfd6w7qk0gg0389r4sw3v0p9gf7jl2mgpvw7mxwnvj8b") (y #t)))

(define-public crate-verboten-0.9.1 (c (n "verboten") (v "0.9.1") (d (list (d (n "eventlog") (r "^0.1") (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qargparser") (r "^0.5") (d #t) (k 0)) (d (n "registry") (r "^1") (d #t) (k 0)) (d (n "windows-service") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.8") (d #t) (k 0)))) (h "1bjkw6mg17npyjfvhfpr3nnac10zp9l7jmdjqafxknd4g9kb4qar")))

(define-public crate-verboten-0.9.2 (c (n "verboten") (v "0.9.2") (d (list (d (n "eventlog") (r "^0.1") (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qargparser") (r "^0.5") (d #t) (k 0)) (d (n "registry") (r "^1") (d #t) (k 0)) (d (n "windows-service") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.8") (d #t) (k 0)))) (h "0z2r2vpi54814l0541i1yxqyhyml7dm6mjsikwcybnabxilij60a")))

