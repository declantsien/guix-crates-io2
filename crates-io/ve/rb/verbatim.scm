(define-module (crates-io ve rb verbatim) #:use-module (crates-io))

(define-public crate-verbatim-0.1.0 (c (n "verbatim") (v "0.1.0") (h "1bjr558i1ayskklkkhq7hjf9sdj1fcz1m8dg894ajvgpbz1qqhf6")))

(define-public crate-verbatim-0.1.1 (c (n "verbatim") (v "0.1.1") (h "1ilp583n9s3172aqnhdiyylaysy4z8p9n0alx4k14icv0xwhdbdv")))

