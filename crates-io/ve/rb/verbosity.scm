(define-module (crates-io ve rb verbosity) #:use-module (crates-io))

(define-public crate-verbosity-0.1.0 (c (n "verbosity") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0x5k440pl8l776i1w6d844zsy0l72878lpkiv1mn298bcynca4ic")))

