(define-module (crates-io ve rb verba) #:use-module (crates-io))

(define-public crate-verba-0.1.0 (c (n "verba") (v "0.1.0") (h "042kl2bn8cl7mfq7j0grnlkap0kv508485ijq0grwdqqzm5xzibn")))

(define-public crate-verba-0.2.0 (c (n "verba") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0bqyknv47k9xxr904a0h9w11w1g7jzsjwjar9b19wab9c3j763h9")))

(define-public crate-verba-0.3.0 (c (n "verba") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1188xzwxsc9x3x18c22vxhhrpginpbgp7ckrvgi9nmaqnjwfx3jg")))

(define-public crate-verba-0.4.0 (c (n "verba") (v "0.4.0") (d (list (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "02a6ajasmrjclry0g39zyssg243rr1ya0x5hwazx1jk7gnf8ljix")))

(define-public crate-verba-0.5.0 (c (n "verba") (v "0.5.0") (d (list (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0vfhaq11r52ipsklk9cq3jacmh40dcq4ra6q0wf3vxccb67f2jzk")))

(define-public crate-verba-0.5.1 (c (n "verba") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1mp7f6hi5sy8qh36gin9cag87gln3426apnnq9ansbldaiya1iqz")))

