(define-module (crates-io ve rb verbs-macros) #:use-module (crates-io))

(define-public crate-verbs-macros-0.2.3 (c (n "verbs-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wkr40hcg6x5mlf5mq5ciwdf4b68b0jd65zh1shcpwmbj6j3a1ml")))

(define-public crate-verbs-macros-0.3.0 (c (n "verbs-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j591r6g31g7vlbja972107r57mc6psy91l1jmy7jxjpx17crji8")))

(define-public crate-verbs-macros-0.3.1 (c (n "verbs-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kbfwfh4cky0f2yx6j0nxm7wqa60xqc0297mwy1hc8rxigk6s0wy")))

(define-public crate-verbs-macros-0.4.0 (c (n "verbs-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07vvwdcgafw5whgf75662jknqhcksbsq4a5j447jr8hczjk7lqli")))

(define-public crate-verbs-macros-0.4.1 (c (n "verbs-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sfik2b8qfq8arypgfp53pw8cl9rfwc2f4291lyqg1ng1mvzjfia")))

