(define-module (crates-io kg #{-i}# kg-io) #:use-module (crates-io))

(define-public crate-kg-io-0.1.0 (c (n "kg-io") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "kg-diag") (r "^0.1") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1maqvk72pl81g5ldyfq45qps0wgmb4dnsi6084difz0scggck28m")))

(define-public crate-kg-io-0.1.1 (c (n "kg-io") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "kg-diag") (r "^0.1.1") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0mxid8cxjfc2j9gp6875fvwph7rbi7kfzidsyspmhxxpr58y20sy")))

