(define-module (crates-io kg -d kg-diag) #:use-module (crates-io))

(define-public crate-kg-diag-0.1.0 (c (n "kg-diag") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1") (d #t) (k 0)))) (h "04k3y33hhdyfllrlqmnndwyf9nca4csryl054i01wfjy7a309w8j")))

(define-public crate-kg-diag-0.1.1 (c (n "kg-diag") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1v4xkiqgypvp0kx0pqc4ihk9fx55p954bcg35adg12za0iandyda")))

(define-public crate-kg-diag-0.2.0 (c (n "kg-diag") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "060kzza53w8a2bir5gi1znmlpadqd2qjqcrlxj7pk56v7p252wys")))

(define-public crate-kg-diag-0.2.1 (c (n "kg-diag") (v "0.2.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.33") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1ryvvlzcjiyxd0vymn81929kpppzv2iivra4v3yb1hgfnvg8js9m")))

(define-public crate-kg-diag-0.2.2 (c (n "kg-diag") (v "0.2.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.33") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "084ym393pqk2dh61z5vglp7d50mpk36r36kwx00c4sa3ckrgpxvq")))

(define-public crate-kg-diag-0.2.3 (c (n "kg-diag") (v "0.2.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.33") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1gf93xc29nwp5b6c6vpj5x3ckxna36yc9333j4f8fjrw339ka78k")))

(define-public crate-kg-diag-0.2.4 (c (n "kg-diag") (v "0.2.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.34") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0qfp45k8gjvygarwkjfc5pxckbgam8gb37hgkizzmzpz2z1q7214")))

(define-public crate-kg-diag-0.2.5 (c (n "kg-diag") (v "0.2.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.34") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "178547pwh4ncgnhffzdnnfmhfa1yk9b7rawrs6z94vpxqk65cbx7")))

(define-public crate-kg-diag-0.2.6 (c (n "kg-diag") (v "0.2.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.34") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "09fwvadlqmbkcgp08r3agg58i59kvcmqcf0mjx71na7fmscb66af")))

(define-public crate-kg-diag-0.2.7 (c (n "kg-diag") (v "0.2.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.34") (d #t) (k 0)) (d (n "kg-display") (r "^0.1.2") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "10945y6h0b4cwqhkbrnlhsgn749m7ajrgxaadn22kl4rsq71grgh")))

(define-public crate-kg-diag-0.3.0 (c (n "kg-diag") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.35") (d #t) (k 0)) (d (n "kg-display") (r "^0.1.2") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1gacgwvdc7kanrll7i51bp9s0vgkmcfbmddrxfqqkdb77fibzza0")))

(define-public crate-kg-diag-0.3.1 (c (n "kg-diag") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.35") (d #t) (k 0)) (d (n "kg-display") (r "^0.1.2") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1h1c27d7b7jdi0q0jr41jj8l3rmvq3n1v6r46xsjn4xsvgyxzdg7")))

(define-public crate-kg-diag-0.4.0 (c (n "kg-diag") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.37") (d #t) (k 0)) (d (n "kg-display") (r "^0.1.2") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "15cp1mbdgiqymv00awgvpkpja8sppr090c22plmx8mmij97p0gj3")))

