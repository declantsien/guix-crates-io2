(define-module (crates-io kg -d kg-display-derive) #:use-module (crates-io))

(define-public crate-kg-display-derive-0.1.0 (c (n "kg-display-derive") (v "0.1.0") (d (list (d (n "kg-display") (r "^0.1") (d #t) (k 0)) (d (n "kg-utils") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "083cc165zrql6bb5a9gw42xaimgcfbx82fyy6c1p84f17f3ncrxw")))

(define-public crate-kg-display-derive-0.1.1 (c (n "kg-display-derive") (v "0.1.1") (d (list (d (n "kg-display") (r "^0.1.2") (d #t) (k 0)) (d (n "kg-utils") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.11.0") (d #t) (k 0)))) (h "1gp9azlx55jh98lhlyc825126vs53bq9ldnwzjyln0wwyyvpbx84")))

