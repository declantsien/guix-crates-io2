(define-module (crates-io kg -d kg-display) #:use-module (crates-io))

(define-public crate-kg-display-0.1.0 (c (n "kg-display") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "1qz7sb918x8dvkrklw10hm7w9wk52bncnhkb7xp2bqbyvc4xr6rd")))

(define-public crate-kg-display-0.1.1 (c (n "kg-display") (v "0.1.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "18gdqdafb9nxvniy0dsw8wg3nl9aqri39dr6irs6iiz6xspdw5ks")))

(define-public crate-kg-display-0.1.2 (c (n "kg-display") (v "0.1.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1bphix7vvr5f6a6mbhywjpp1y50abp5y67miaiq5v3d7xcskari4")))

