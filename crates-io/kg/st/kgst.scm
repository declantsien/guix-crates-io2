(define-module (crates-io kg st kgst) #:use-module (crates-io))

(define-public crate-kgst-0.1.0 (c (n "kgst") (v "0.1.0") (d (list (d (n "bio") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1255qvf73ydwc0xpadd8yrc9y3962h0f1h6f9kwzmjyhb2h5ckgk")))

(define-public crate-kgst-0.1.1 (c (n "kgst") (v "0.1.1") (d (list (d (n "bio") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "105l91xmgwpw36848z1b85rnjjn2jgw68ybiksbdbslvp55mnzvy")))

