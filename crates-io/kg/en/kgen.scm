(define-module (crates-io kg en kgen) #:use-module (crates-io))

(define-public crate-kgen-0.1.0 (c (n "kgen") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j3nisv7gs0yyy20byc0rxbnab0129cq0adsjyxl3f5fhz2aj9cm") (y #t)))

(define-public crate-kgen-0.1.1 (c (n "kgen") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ac1qhh5mhbnw9mwmdf4rgxby273zgqzlp8rh3z2sk76qv7lk4x") (y #t)))

(define-public crate-kgen-0.3.0 (c (n "kgen") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "12ck6vj5ss7pakkq33vj6qxp3c7wicfwnir49n9cjgb025g352h9") (y #t)))

(define-public crate-kgen-0.4.0 (c (n "kgen") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0hrhgf9n6jxhc5m39r9f5f1688ayr68yyp2qwr84m089bqaarmiq") (y #t)))

(define-public crate-kgen-0.4.1 (c (n "kgen") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0r2vlf6wfcw2dp87qd8g3y5sq4hwpc9qx3nfiwygz8fkcjg17ygd") (y #t)))

(define-public crate-kgen-0.4.2 (c (n "kgen") (v "0.4.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1pwi73g36w4la3ggqg6asmajwv2b591haa9y31dybbd1ykigdf06")))

