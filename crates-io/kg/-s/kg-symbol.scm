(define-module (crates-io kg -s kg-symbol) #:use-module (crates-io))

(define-public crate-kg-symbol-0.1.0 (c (n "kg-symbol") (v "0.1.0") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qlirnmx6y1dpkl3fk50n8vb01nfvv2ysdw2ww8l8vkr5b3xvrnz") (y #t)))

(define-public crate-kg-symbol-0.1.1 (c (n "kg-symbol") (v "0.1.1") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vg5pksxbblgz814ilzqdyhk9br3mrpd6c1lvvbwrgwj9sglnly3") (y #t)))

(define-public crate-kg-symbol-0.1.2 (c (n "kg-symbol") (v "0.1.2") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jdrr7bk6p94xaq8zi653nh2l058hw68v37phbkbrhql9zf839kz") (y #t)))

(define-public crate-kg-symbol-0.1.3 (c (n "kg-symbol") (v "0.1.3") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "193i4f8pplivsmb6lzb30p9l7mfqjblnxrlwm7qdqsflcpphzgjy") (y #t)))

(define-public crate-kg-symbol-0.1.4 (c (n "kg-symbol") (v "0.1.4") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bdcxm0wm30giq0yrrh887i3g2zkka0jg5dnwc4wbf3q25vilcpz") (y #t)))

(define-public crate-kg-symbol-0.1.5 (c (n "kg-symbol") (v "0.1.5") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fib6j7izmdn82xy2vmapb8zflba2llaifbqfs8ygs9n4picprqc")))

(define-public crate-kg-symbol-0.1.6 (c (n "kg-symbol") (v "0.1.6") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kq38x2gk6pf88h9z9ajj9x9bndw96r8qbbi8783sf3rikf3sgly") (y #t)))

(define-public crate-kg-symbol-0.1.7 (c (n "kg-symbol") (v "0.1.7") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "189r0ja5qdfsybvywxgc1p8mp7gbfsw09cxxzjnlnag6f6a4xm6j")))

(define-public crate-kg-symbol-0.1.8 (c (n "kg-symbol") (v "0.1.8") (d (list (d (n "heapsize") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rpi97hh80hbrrr76hjihdfl9zi70c9y123blc4yl6gzq1cvrp6j")))

(define-public crate-kg-symbol-0.1.9 (c (n "kg-symbol") (v "0.1.9") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0c055vgbsjfgwabmkfghgbdy09jqzdlck5bbijc3njvnxwwr9hb9")))

(define-public crate-kg-symbol-0.1.10 (c (n "kg-symbol") (v "0.1.10") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0wrdnhkc3jnc0lyjdw8hnhgsk7hr1ijiafv2mivs486m6855gvgb")))

(define-public crate-kg-symbol-0.2.0 (c (n "kg-symbol") (v "0.2.0") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "1vq98ni32zyn3zny2p9plcs3chvzih16baynzal47vnmvng1f8zz")))

