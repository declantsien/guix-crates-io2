(define-module (crates-io kg -s kg-syntax) #:use-module (crates-io))

(define-public crate-kg-syntax-0.3.0 (c (n "kg-syntax") (v "0.3.0") (d (list (d (n "kg-diag") (r "^0.4.0") (d #t) (k 0)) (d (n "kg-diag-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "kg-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "kg-js") (r "^0.1.1") (d #t) (k 0)) (d (n "kg-tree") (r "^0.2.1") (d #t) (k 0)) (d (n "kg-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "14fkwhrg3nkpwvcxbdwdbk6dpvms703dj2wa2gh8zpfj201nlh6x")))

