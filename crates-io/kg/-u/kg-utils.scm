(define-module (crates-io kg -u kg-utils) #:use-module (crates-io))

(define-public crate-kg-utils-0.2.0 (c (n "kg-utils") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0apywdh4hs6hzhhwga7wh50qd37grnpda2q3b1zwdnm7cf84rks7")))

(define-public crate-kg-utils-0.2.1 (c (n "kg-utils") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yibkyk1zbfwqghkdm0ngsa9cnngywybsrhf1zr47jwhadcv8yk0")))

(define-public crate-kg-utils-0.2.2 (c (n "kg-utils") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b05d8lg36d5qvhndn020mrn2m5ah86r6d0wywrz3rvq751b2cp9")))

(define-public crate-kg-utils-0.2.3 (c (n "kg-utils") (v "0.2.3") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ya36lx5qshs7c5irjpbp23jrk2yzqmhk0a24icvn2xf6ngisd7r") (f (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.2.4 (c (n "kg-utils") (v "0.2.4") (d (list (d (n "heapsize") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (o #t) (d #t) (k 0)))) (h "08p5cbb4c5b00adk7hl83g8iar736l3rkf489pghc7gax9jjn8ya") (f (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.3.0 (c (n "kg-utils") (v "0.3.0") (d (list (d (n "heapsize") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (o #t) (d #t) (k 0)))) (h "1rzimdalw7dy969cmq9vdvglcdwc2c4w2019h8jm43q1dmjiq837") (f (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.3.1 (c (n "kg-utils") (v "0.3.1") (d (list (d (n "heapsize") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (o #t) (d #t) (k 0)))) (h "1426y45v42k0c7x5z168qwg4bian5v309b6irpyxpfqhgdivix7g") (f (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.4.0 (c (n "kg-utils") (v "0.4.0") (d (list (d (n "heapsize") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)))) (h "1rwlnsyqjn1xamvf47vb15zbcad7w76wyqwg7a52l6mk6in0ajfc") (f (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

