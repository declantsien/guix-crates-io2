(define-module (crates-io ly te lyte) #:use-module (crates-io))

(define-public crate-lyte-0.1.0 (c (n "lyte") (v "0.1.0") (d (list (d (n "insta") (r "^1.21") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1ph3x5ymwv5m3qglmnkz11fz4r4w9cxpplmndpmk3f3fvbwb1hmr")))

