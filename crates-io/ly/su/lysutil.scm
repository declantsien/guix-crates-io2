(define-module (crates-io ly su lysutil) #:use-module (crates-io))

(define-public crate-lysutil-0.0.1 (c (n "lysutil") (v "0.0.1") (d (list (d (n "string-builder") (r "^0.2") (d #t) (k 0)))) (h "1rz1nwq7wlbmycn53l4iy0fkf4bwy0rfz45371769266yi41lqj9")))

(define-public crate-lysutil-0.1.0 (c (n "lysutil") (v "0.1.0") (d (list (d (n "string-builder") (r "^0.2") (d #t) (k 0)))) (h "0yci154z1zkwa17gq73bfmxca1jbyjib5w5497729khw5ipgnkjk")))

(define-public crate-lysutil-0.1.1 (c (n "lysutil") (v "0.1.1") (d (list (d (n "string-builder") (r "^0.2") (d #t) (k 0)))) (h "1kxa83mg87spcrpy7mw9ldj4hf0gw844jxwlrbizcwldq642bx1r")))

