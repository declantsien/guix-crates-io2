(define-module (crates-io ly ri lyric_charter_bin) #:use-module (crates-io))

(define-public crate-lyric_charter_bin-0.1.0 (c (n "lyric_charter_bin") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "lyric_charter_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0rkf6q3yppp61x31x4dcd63x0injm9ixp9akgdvlnzf41d41k68z")))

