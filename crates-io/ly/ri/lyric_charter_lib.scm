(define-module (crates-io ly ri lyric_charter_lib) #:use-module (crates-io))

(define-public crate-lyric_charter_lib-0.1.0 (c (n "lyric_charter_lib") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0a79g36jnrb64xfcinz4xiyixrkqz2w3zcdc76jhwdj5fzda9dsx")))

