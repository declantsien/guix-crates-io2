(define-module (crates-io ly ri lyrica) #:use-module (crates-io))

(define-public crate-lyrica-0.1.0 (c (n "lyrica") (v "0.1.0") (d (list (d (n "midir") (r "^0.8.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zij5ric3vl7556zx77j6iiwbfnqzpjvxvga9pgl4ixwf477fs1j")))

(define-public crate-lyrica-0.1.1 (c (n "lyrica") (v "0.1.1") (d (list (d (n "midir") (r "^0.8.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1r5wdj8d7zxiw2dswa37fjbklqk92v2kfy3a84b8acxi8ycr4g02")))

(define-public crate-lyrica-0.2.0 (c (n "lyrica") (v "0.2.0") (d (list (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ignlhk3grnc033rfflqsf2yaaaxdrnr06n5dkxvgr9qhjqnyvcf")))

(define-public crate-lyrica-0.2.1 (c (n "lyrica") (v "0.2.1") (d (list (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1x4ylxmbsf0ghwg6cx8b6dc2csm3pkxjs75zwglr9d6nb2sxv5w8")))

