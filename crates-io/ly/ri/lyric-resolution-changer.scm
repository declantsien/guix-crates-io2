(define-module (crates-io ly ri lyric-resolution-changer) #:use-module (crates-io))

(define-public crate-lyric-resolution-changer-0.1.0 (c (n "lyric-resolution-changer") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1dbng0xs3kbkjvwm72l20ghdvsbpy0nybip7142x3fjbkr06g3c0")))

