(define-module (crates-io ly ri lyrics) #:use-module (crates-io))

(define-public crate-lyrics-0.1.0 (c (n "lyrics") (v "0.1.0") (d (list (d (n "bunt") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bfclb009j0ysrl97wf5ifvxhx986xh04v9wglwvs2dkak6p3qm2")))

(define-public crate-lyrics-0.2.0 (c (n "lyrics") (v "0.2.0") (d (list (d (n "bunt") (r "^0.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y4pcv41yrq2c715cn1zyb6zamwkm707sq2hy5hmjhix8r08g45a")))

