(define-module (crates-io ly ri lyricrustacean) #:use-module (crates-io))

(define-public crate-lyricrustacean-0.1.0 (c (n "lyricrustacean") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "16n58a5ic3s4mk5pfadvlla53g4wjlcjjfpncz5gms0j1njndqh4")))

