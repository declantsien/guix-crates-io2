(define-module (crates-io ly ri lyric_charter_web) #:use-module (crates-io))

(define-public crate-lyric_charter_web-0.1.0 (c (n "lyric_charter_web") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "lyric_charter_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Blob"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "0bpy1fbbkg4kslgg72diard2v70hpbqiyv3vfac0nplsw8zfk4wn")))

