(define-module (crates-io ly ri lyricer) #:use-module (crates-io))

(define-public crate-lyricer-1.0.0 (c (n "lyricer") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_error"))) (d #t) (k 0)) (d (n "mpris") (r "^1.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)))) (h "04pmi7pjif699s4sij90ynyqhgi50sk4nmx0xvix3cw0k939zvpd") (f (quote (("multiline-lrc") ("default" "multiline-lrc"))))))

(define-public crate-lyricer-1.0.1 (c (n "lyricer") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_error"))) (d #t) (k 0)) (d (n "mpris") (r "^1.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)))) (h "01635zinchw53b46n967vrcchj0y1j48b40ly95ansrw1b09x9wf") (f (quote (("multiline-lrc") ("default" "multiline-lrc"))))))

(define-public crate-lyricer-1.0.2 (c (n "lyricer") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_error"))) (d #t) (k 0)) (d (n "mpris") (r "^1.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)))) (h "08y31j6l4pfr0qmbwh2f9s3l8qkpw2cyilp9nqvn4xkdhlzizw47") (f (quote (("multiline-lrc") ("default" "multiline-lrc"))))))

(define-public crate-lyricer-1.0.3 (c (n "lyricer") (v "1.0.3") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_error"))) (d #t) (k 0)) (d (n "mpris") (r "^1.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)))) (h "0xsi0v6zvqnp3v5r8w2cc9f4py3lmgndx57d96rccf14c7brl5qj") (f (quote (("multiline-lrc") ("default" "multiline-lrc"))))))

