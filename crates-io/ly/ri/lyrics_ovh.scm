(define-module (crates-io ly ri lyrics_ovh) #:use-module (crates-io))

(define-public crate-lyrics_ovh-0.1.0 (c (n "lyrics_ovh") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0x8253lv6qrvf12n89913dlsy76r5sm6jvral3kvlbkffnvj3hxy")))

(define-public crate-lyrics_ovh-0.1.1 (c (n "lyrics_ovh") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0nfaik9pyxwk9fc1j9c81b5x538dhbl1na5bphck0ywnjyfhj1bk")))

