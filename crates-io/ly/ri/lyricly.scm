(define-module (crates-io ly ri lyricly) #:use-module (crates-io))

(define-public crate-lyricly-0.1.0 (c (n "lyricly") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "02k6xpmmfnq07s26pla2wb3rhjhrxh26x3znj7vj8cjk1manfqk2")))

(define-public crate-lyricly-0.2.0 (c (n "lyricly") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "0w9b1scg1hm6g6b7kzn2050dr395ljskk9qprbmpncyzhsqcz3yi")))

