(define-module (crates-io ly so lysogeny-broth) #:use-module (crates-io))

(define-public crate-lysogeny-broth-1.0.0 (c (n "lysogeny-broth") (v "1.0.0") (h "1f9wf83r60yb5w9h4yvib75a2gq88mfhqhlvc7cr8l1znvwsapki")))

(define-public crate-lysogeny-broth-1.1.1 (c (n "lysogeny-broth") (v "1.1.1") (h "0smjadjw5kmiayafa0rj3qmgryzmiy3mw764339cbk1346kbcb2x") (f (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1.1.2 (c (n "lysogeny-broth") (v "1.1.2") (h "09gvxspnqnwzn98zlmgj67p7f2yqacqsg504g2yvpz7zncckqdam") (f (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1.1.3 (c (n "lysogeny-broth") (v "1.1.3") (h "1anp92n80lxmzs4isj2g9qxwcy7wp3yzacdfbplpnv1v5vsca43q") (f (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1.1.4 (c (n "lysogeny-broth") (v "1.1.4") (h "0ncbncp08kd6xmqwdgc5179r8idhg8y595j3kr1ggmj0i16gsv7v") (f (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1.2.0 (c (n "lysogeny-broth") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1r6h91542qcf6m3931jk0bkrl3a6xfzpvsazy1pfm0n03bq9rzl1") (f (quote (("default" "dead-alive-only") ("dead-alive-u8-utils" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

