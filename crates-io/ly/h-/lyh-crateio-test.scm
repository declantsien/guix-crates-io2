(define-module (crates-io ly h- lyh-crateio-test) #:use-module (crates-io))

(define-public crate-lyh-crateio-test-0.1.0 (c (n "lyh-crateio-test") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1cjq3rhhayxr9wxpwvnvmw8xsndj3cwlph7ywsgl8a5fa02320vq")))

(define-public crate-lyh-crateio-test-0.1.1 (c (n "lyh-crateio-test") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0nrfj904hqz62bnbc3ijdncgimi68h4v9rhy20g8awpf3v1b0qq4")))

(define-public crate-lyh-crateio-test-0.1.2-pre.0 (c (n "lyh-crateio-test") (v "0.1.2-pre.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0aj5clsg335n6zhagqzk5kfqfd55v2jw57rd4y2z49m9xfv0g3mw")))

(define-public crate-lyh-crateio-test-0.1.2-pre.1 (c (n "lyh-crateio-test") (v "0.1.2-pre.1") (h "107cf1bjlb5sa0ai5zwwys4z4skdvsbgzr3kkxszgzx81d8rmyjk")))

(define-public crate-lyh-crateio-test-0.1.2 (c (n "lyh-crateio-test") (v "0.1.2") (h "0k35yaqw4ah99n6sgqdm0h5ghg1n59ylj13a3zp2d498gh158m6z")))

(define-public crate-lyh-crateio-test-0.1.3-pre.0 (c (n "lyh-crateio-test") (v "0.1.3-pre.0") (h "1dfl05bvvhyfn3r9ypdlqyzqwdsdilj2r4ypnfjijh46n60v7lzn")))

(define-public crate-lyh-crateio-test-0.1.3-pre.1 (c (n "lyh-crateio-test") (v "0.1.3-pre.1") (h "0ynxdjdrgw2gnicjninylyk8a6hrl96l23z7wy2y2gpbx1bz4qlw")))

(define-public crate-lyh-crateio-test-0.1.3 (c (n "lyh-crateio-test") (v "0.1.3") (h "184vr3s29mzpd0v03p5j10i0kavmib18l0lfzxi3cdnxd2dy0ijh")))

