(define-module (crates-io ly on lyon-weiler-atherton) #:use-module (crates-io))

(define-public crate-lyon-weiler-atherton-0.1.0 (c (n "lyon-weiler-atherton") (v "0.1.0") (d (list (d (n "lyon") (r "^0.17.5") (d #t) (k 0)) (d (n "lyon_algorithms") (r "^0.17.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0jpl5yqzf6d8nscnpm9n1nvm1mz0xqfdaxdyrjjrk3pvqpyk6ywr")))

