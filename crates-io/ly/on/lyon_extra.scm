(define-module (crates-io ly on lyon_extra) #:use-module (crates-io))

(define-public crate-lyon_extra-0.0.2 (c (n "lyon_extra") (v "0.0.2") (d (list (d (n "lyon_core") (r "^0.0.2") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "0vid8ry7clgjikwxsy0pc5wnxwpvqjlsk7i9lr0b3xh7w9js7kzf")))

(define-public crate-lyon_extra-0.0.3 (c (n "lyon_extra") (v "0.0.3") (d (list (d (n "lyon_core") (r "^0.0.3") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "02k2nfqa1frj52kghv0k2baqz0scy7rs70r40gqzx4azxqnk5w7q")))

(define-public crate-lyon_extra-0.1.0 (c (n "lyon_extra") (v "0.1.0") (d (list (d (n "lyon_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "015jhypc9ja206gp7g1xky584xfmyh2dh4n0l4fh56ffj9mi692v")))

(define-public crate-lyon_extra-0.1.1 (c (n "lyon_extra") (v "0.1.1") (d (list (d (n "lyon_core") (r "^0.1.1") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "0a682v1nxdmx2791pa6wrbf2q5qsvkq4ag241wfvb0h4swz0avr5")))

(define-public crate-lyon_extra-0.2.0 (c (n "lyon_extra") (v "0.2.0") (d (list (d (n "lyon_core") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.2.0") (d #t) (k 0)))) (h "1q1rqrvhn9rlsc6rrb1ish3w0llm3xqi83wzx6pb8i9pyl2446vz")))

(define-public crate-lyon_extra-0.2.1 (c (n "lyon_extra") (v "0.2.1") (d (list (d (n "lyon_core") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.2.0") (d #t) (k 0)))) (h "1whkwqfzkzz89jz3dva3d9lq11rhdx08ka18gdl286vc96c1aib4")))

(define-public crate-lyon_extra-0.3.0 (c (n "lyon_extra") (v "0.3.0") (d (list (d (n "lyon_core") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.0") (d #t) (k 0)))) (h "1k68cajpws0jkjfyaxhj7njxd6qfxb7ssi4xhpm002c402y9z10h")))

(define-public crate-lyon_extra-0.3.1 (c (n "lyon_extra") (v "0.3.1") (d (list (d (n "lyon_core") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_path") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.1") (d #t) (k 0)))) (h "03hmi5c8kkp73b9zqd98xylww76g80fdjrkln2klslzmvdrwf1zq")))

(define-public crate-lyon_extra-0.3.2 (c (n "lyon_extra") (v "0.3.2") (d (list (d (n "lyon_core") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_path") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.2") (d #t) (k 0)))) (h "1saivw548wz95v7lpx890g8c9hxwp3a1yq4zd9iqjl1c4jr967hf")))

(define-public crate-lyon_extra-0.4.0 (c (n "lyon_extra") (v "0.4.0") (d (list (d (n "lyon_core") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.4.0") (d #t) (k 0)))) (h "1ipgwwz7p27qpqvdzr5qrxgn6318zzfv5rgp7mdmjxi2zg992vj6")))

(define-public crate-lyon_extra-0.4.1 (c (n "lyon_extra") (v "0.4.1") (d (list (d (n "lyon_core") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_path") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.4.1") (d #t) (k 0)))) (h "0nvy6q299nx53vb0dkvdrjv1pky8zi7cdb5m34s5lb6bwp1cf3nv")))

(define-public crate-lyon_extra-0.5.0 (c (n "lyon_extra") (v "0.5.0") (d (list (d (n "lyon_core") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.5.0") (d #t) (k 0)))) (h "0r2lxqh2fr1swd51zb89j3jj7fr5h78cnbiw14gyn9i9srrlgkin")))

(define-public crate-lyon_extra-0.6.0 (c (n "lyon_extra") (v "0.6.0") (d (list (d (n "lyon_core") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.6.0") (d #t) (k 0)))) (h "0l69gyyb4g2rfm8qqrzqr4k59qqdx47f2h7ybdvr1gflblbv1f5s")))

(define-public crate-lyon_extra-0.7.0 (c (n "lyon_extra") (v "0.7.0") (d (list (d (n "lyon_core") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.7.0") (d #t) (k 0)))) (h "0v5qx8shfb6ydz9x65zyrgjc3xcrz45k542ng65vry7rr9yv21qs")))

(define-public crate-lyon_extra-0.8.0 (c (n "lyon_extra") (v "0.8.0") (d (list (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.0") (d #t) (k 0)))) (h "00kzi6w5p8k73r3sa3k1fc796n46mqg2g4r3c7936ixir3k7ynf5")))

(define-public crate-lyon_extra-0.8.1 (c (n "lyon_extra") (v "0.8.1") (d (list (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.0") (d #t) (k 0)))) (h "1hdc255b03qdsli5dp5bjn5ifdidyxarxbdc5q4szqs86nv5bpjd")))

(define-public crate-lyon_extra-0.8.5 (c (n "lyon_extra") (v "0.8.5") (d (list (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.8.0") (d #t) (k 0)))) (h "1fsj234pm6yih2sfkwibcflz2b5v581yfsrwxv75c2rlp4vlr7va")))

(define-public crate-lyon_extra-0.8.6 (c (n "lyon_extra") (v "0.8.6") (d (list (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path") (r "^0.8.6") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.6") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.8.6") (d #t) (k 0)))) (h "001lrlg9bkjxy5c2fmdfwkzq4zyckc5pwh7gav86s5g04305sbsm")))

(define-public crate-lyon_extra-0.9.0 (c (n "lyon_extra") (v "0.9.0") (d (list (d (n "lyon_path") (r "^0.9.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.9.0") (d #t) (k 0)))) (h "1hypyflwwpzch6rrlga5avr8p6wf713zkvga6pa3xc853kikc48q")))

(define-public crate-lyon_extra-0.10.0 (c (n "lyon_extra") (v "0.10.0") (d (list (d (n "lyon_path") (r "^0.10.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.10.0") (d #t) (k 0)))) (h "12vgk8v0p5za0kd151q35lxhgbg30yy5cf6ybaszp7zwfq52l5dl")))

(define-public crate-lyon_extra-0.11.0 (c (n "lyon_extra") (v "0.11.0") (d (list (d (n "lyon_path") (r "^0.11.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.11.0") (d #t) (k 0)))) (h "0cjsizs4207h120ah2s1h78zjc6zakaf006kg3ld3fg06gxcwrrw")))

(define-public crate-lyon_extra-0.13.0 (c (n "lyon_extra") (v "0.13.0") (d (list (d (n "lyon_path") (r "^0.13.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.13.0") (d #t) (k 0)))) (h "0arpwzk0gjkjwj9hlx3k2nydkxwrxr51zy3jx88kcl1d9vri97x0")))

(define-public crate-lyon_extra-0.14.0 (c (n "lyon_extra") (v "0.14.0") (d (list (d (n "lyon_path") (r "^0.14.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.14.0") (d #t) (k 0)))) (h "1q4clr5g146xk1rsrbgf95qzpp1v0vsyfdzcnij2nayrazyihi6m")))

(define-public crate-lyon_extra-0.15.0 (c (n "lyon_extra") (v "0.15.0") (d (list (d (n "lyon_path") (r "^0.15.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.15.0") (d #t) (k 0)))) (h "1lhb5zvfx3gpzs9q7zim3dvpcrdh51pf33zvpjrp0mgicm0iyvvg")))

(define-public crate-lyon_extra-0.16.0 (c (n "lyon_extra") (v "0.16.0") (d (list (d (n "lyon_path") (r "^0.16.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.16.0") (d #t) (k 0)))) (h "16bbrjhy7fjjyrcjjvmdpv0c1lfwghq0c4ywflp39hd7vns1xghp")))

(define-public crate-lyon_extra-0.17.0 (c (n "lyon_extra") (v "0.17.0") (d (list (d (n "lyon_path") (r "^0.17.0") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.17.0") (d #t) (k 0)))) (h "1s0gdyih5531iyqgrapnc5k13rwmf856p1jhpilz3x5ikzfmmlf1") (y #t)))

(define-public crate-lyon_extra-0.17.1 (c (n "lyon_extra") (v "0.17.1") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "lyon_svg") (r "^0.17.1") (d #t) (k 0)))) (h "10n8h48a363qa66byqfkg164542p97v5rrnb33gqhggah739nwm0")))

(define-public crate-lyon_extra-1.0.0 (c (n "lyon_extra") (v "1.0.0") (d (list (d (n "lyon_path") (r "^1.0.0") (d #t) (k 0)))) (h "1i5ghlbh48c9qv88n1xcw062qsi1fr6gb7cdq28752id8n53mil7") (f (quote (("serialization" "lyon_path/serialization"))))))

(define-public crate-lyon_extra-1.0.1 (c (n "lyon_extra") (v "1.0.1") (d (list (d (n "lyon_path") (r "^1.0.0") (d #t) (k 0)))) (h "0fwzr4q4c9hxvc72mwz8y22gm0amlx8xzmghq570k014izijmkmr") (f (quote (("serialization" "lyon_path/serialization"))))))

(define-public crate-lyon_extra-1.0.2 (c (n "lyon_extra") (v "1.0.2") (d (list (d (n "lyon_path") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "081vhfd1blfgk5gc201my5lnq6q8zgc8han7wqx5yzhywqy28jlc") (f (quote (("serialization" "lyon_path/serialization"))))))

