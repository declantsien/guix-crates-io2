(define-module (crates-io ly on lyon_tess2) #:use-module (crates-io))

(define-public crate-lyon_tess2-0.9.1 (c (n "lyon_tess2") (v "0.9.1") (d (list (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1mf1crf53w5rzsvcrh2lcg85rcj313y3l63yy7yb2p9p89azgqmd")))

(define-public crate-lyon_tess2-0.10.0 (c (n "lyon_tess2") (v "0.10.0") (d (list (d (n "lyon_tessellation") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1ifvlap2cj1gqj3vs6al139dwi593jrcrc5cdvihn4ykidp1jdhp") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.11.0 (c (n "lyon_tess2") (v "0.11.0") (d (list (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1mplcc64dpaakld8wfgq44a8id5x3l9fbjyz4x1hh8g7lj1v4y4a") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.13.0 (c (n "lyon_tess2") (v "0.13.0") (d (list (d (n "lyon_tessellation") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0h1np06mikhximangj5gjrd5ns64qngynhbv69khd7hchrvjxa54") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.13.1 (c (n "lyon_tess2") (v "0.13.1") (d (list (d (n "lyon_tessellation") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0vpy4nbisq2dzsij925anla6wdf77nb6jn2fxjnz411xr03vgpz4") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.14.0 (c (n "lyon_tess2") (v "0.14.0") (d (list (d (n "lyon_tessellation") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0rwb66hbf64d8fvyl02vlh1wsmv7gcqhp5wncg1c9aycpaqy1ny6") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.15.0 (c (n "lyon_tess2") (v "0.15.0") (d (list (d (n "lyon_tessellation") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1qnvnlhzv1pkclcn8imhvw6jfnvsbfqy8jrygbv9y067z14xcz45") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.16.0 (c (n "lyon_tess2") (v "0.16.0") (d (list (d (n "lyon_tessellation") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0wwn7slmq0jgrdm4wf1qgc82qpibyilg8qhnp2knkwknsrc1a7x0") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

(define-public crate-lyon_tess2-0.17.0 (c (n "lyon_tess2") (v "0.17.0") (d (list (d (n "lyon_tessellation") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "11fdyg87vk58afalfdrgf57a7hpny0r78h4p3hw01s2dcv4gxwy5") (f (quote (("serialization" "serde" "lyon_tessellation/serialization")))) (y #t)))

(define-public crate-lyon_tess2-0.17.1 (c (n "lyon_tess2") (v "0.17.1") (d (list (d (n "lyon_tessellation") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0m7j2qz2q9sgs367jxh1gq188k9h815pfkn1w9nwwcm8wn1iw8mz") (f (quote (("serialization" "serde" "lyon_tessellation/serialization"))))))

