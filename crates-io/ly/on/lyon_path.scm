(define-module (crates-io ly on lyon_path) #:use-module (crates-io))

(define-public crate-lyon_path-0.2.0 (c (n "lyon_path") (v "0.2.0") (d (list (d (n "lyon_bezier") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.2.0") (d #t) (k 0)))) (h "1yb8d941k57p2a4q6mv49iir6vzhal9q2ymzpb2p9bkvym1pk4gd")))

(define-public crate-lyon_path-0.3.0 (c (n "lyon_path") (v "0.3.0") (d (list (d (n "lyon_bezier") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.0") (d #t) (k 0)))) (h "1zvxxcyqx5aw0w7arrjpmsj025d717zn87aqbwjqgifd4azbhwlx")))

(define-public crate-lyon_path-0.3.1 (c (n "lyon_path") (v "0.3.1") (d (list (d (n "lyon_bezier") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.1") (d #t) (k 0)))) (h "1s9qi9j9hgxbsbmzhx0r96gsir3s8j70jw9d9ar6cllfsgg8c7mr")))

(define-public crate-lyon_path-0.3.2 (c (n "lyon_path") (v "0.3.2") (d (list (d (n "lyon_bezier") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.3.2") (d #t) (k 0)))) (h "1g43d1bqa2bq3jrszwmq1kx8zh2rd8wbpxcpvf8df81qng3qpfdn")))

(define-public crate-lyon_path-0.4.0 (c (n "lyon_path") (v "0.4.0") (d (list (d (n "lyon_bezier") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.4.0") (d #t) (k 0)))) (h "1h4hxxilnv93q3aygxckijyjc0ry2fzdlc5b6v2kzsqavjxagl0g")))

(define-public crate-lyon_path-0.4.1 (c (n "lyon_path") (v "0.4.1") (d (list (d (n "lyon_bezier") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_core") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.4.1") (d #t) (k 0)))) (h "1nag5w9fqn9cx78bsxsqrb8idjq8i24avhbzh1bg5izandxwpw43")))

(define-public crate-lyon_path-0.5.0 (c (n "lyon_path") (v "0.5.0") (d (list (d (n "lyon_bezier") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.5.0") (d #t) (k 0)))) (h "1qw5wapdb9m8ybmggmjyx94l0j60qyy51yrvjkpn0s950292pdq3")))

(define-public crate-lyon_path-0.6.0 (c (n "lyon_path") (v "0.6.0") (d (list (d (n "lyon_bezier") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.6.0") (d #t) (k 0)))) (h "12740lh1k9iyf5fpmc3w91mcy1hr0b95lqhx38p0gfwwv24swn92")))

(define-public crate-lyon_path-0.7.0 (c (n "lyon_path") (v "0.7.0") (d (list (d (n "lyon_bezier") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.7.0") (d #t) (k 0)))) (h "0lc5vj7pylhp8spiqxgz1vkaarl272130x28ch703kizz8y97arm")))

(define-public crate-lyon_path-0.8.0 (c (n "lyon_path") (v "0.8.0") (d (list (d (n "lyon_bezier") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.0") (d #t) (k 0)))) (h "11k96kdnzg6049cwgbfky7k6gbvr05hgdlya7zsb032xphqf24z6")))

(define-public crate-lyon_path-0.8.5 (c (n "lyon_path") (v "0.8.5") (d (list (d (n "lyon_bezier") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.5") (d #t) (k 0)))) (h "0q99bh7kkhzl70bglkk0nln52w3f2n095sawv0vkx85skb3iz3gn")))

(define-public crate-lyon_path-0.8.6 (c (n "lyon_path") (v "0.8.6") (d (list (d (n "lyon_bezier") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_path_builder") (r "^0.8.6") (d #t) (k 0)) (d (n "lyon_path_iterator") (r "^0.8.5") (d #t) (k 0)))) (h "0sj5s3z8hh3qnsrf0q0kvfjnpvrrd4a8wmm4p5lcax196c7r3nmn")))

(define-public crate-lyon_path-0.9.0 (c (n "lyon_path") (v "0.9.0") (d (list (d (n "lyon_geom") (r "^0.9.0") (d #t) (k 0)))) (h "179mvnjdb2939q14q30fxg8x6hmfz05xzaa61b0mxv50ndqan0mn")))

(define-public crate-lyon_path-0.10.0 (c (n "lyon_path") (v "0.10.0") (d (list (d (n "lyon_geom") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0pzbjfvisdqdvlfyqyama4h9kf96ac3lgcgr2m9dhny8n4l5x3xn") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.10.1 (c (n "lyon_path") (v "0.10.1") (d (list (d (n "lyon_geom") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "19adk1fypx9ydkkz0j3kkgypy42q86z57dyx6cc8z06jx2vx6bwk") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.11.0 (c (n "lyon_path") (v "0.11.0") (d (list (d (n "lyon_geom") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1w0q0dl50bxmvnzl398n243nj6z309rbkcp2jw0mkm4nay29phwq") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.12.0 (c (n "lyon_path") (v "0.12.0") (d (list (d (n "lyon_geom") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0hs8aja19v9ga2mb9sh1d9s6gc3v0gzgf0mnc0cs3k5p8q3qxp79") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.13.0 (c (n "lyon_path") (v "0.13.0") (d (list (d (n "lyon_geom") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1s87hlqid00n7pb841z6g2i7ippry7kkmkxrh4cjl4gh4rq5qhfq") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.13.1 (c (n "lyon_path") (v "0.13.1") (d (list (d (n "lyon_geom") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "05m7r618irapwssc71dz5j1rw6jf6wals4vhwi429hslz7s23njr") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.13.2 (c (n "lyon_path") (v "0.13.2") (d (list (d (n "lyon_geom") (r "^0.12.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0afg0hmzfjxpycr6zm9n7h8cgnqidpakx5890phqszigsd42ajdx") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.14.0 (c (n "lyon_path") (v "0.14.0") (d (list (d (n "lyon_geom") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qk8x46w0sf6j04l6gvhgn9kr4ymcqkmkh67w8wqahm54jn5gjqb") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.15.0 (c (n "lyon_path") (v "0.15.0") (d (list (d (n "lyon_geom") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0d5agfbv90m2vcvhjnmm11bravc9fs0ch204vm0s7ym1f039z205") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.15.1 (c (n "lyon_path") (v "0.15.1") (d (list (d (n "lyon_geom") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1dxz6lziw19v6qc9hgwrvz1vd3838n6bpl9bknnaa28i4d55hww3") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.15.2 (c (n "lyon_path") (v "0.15.2") (d (list (d (n "lyon_geom") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1n4dbc5a00szhb6p719d1944ccfj4047g1a7gcdhap4n2i2086n8") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.16.0 (c (n "lyon_path") (v "0.16.0") (d (list (d (n "lyon_geom") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0saga7h7vlv6h7b3f1nhki3zk26dzssgm113p27mhbf2gsmmm15z") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.16.2 (c (n "lyon_path") (v "0.16.2") (d (list (d (n "lyon_geom") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1jyq64w76c3wcy867k5axdy43w58ac14ly9q5pgfqxv6y7zp2g14") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.0 (c (n "lyon_path") (v "0.17.0") (d (list (d (n "lyon_geom") (r "^0.16.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ml35aa33y8b768kv84bpc9ndc7ycqlpyc3wwk4zm9gz0rfvria2") (f (quote (("serialization" "serde" "lyon_geom/serialization")))) (y #t)))

(define-public crate-lyon_path-0.17.1 (c (n "lyon_path") (v "0.17.1") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1has3ylgdrbmyrsb9vl1261yrzb25l9is124qydl6qxyjgmlpmi3") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.2 (c (n "lyon_path") (v "0.17.2") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1xbsbkynwmaf9jl7r0lycjpl01hfczsp9mxif121zgvwkwxzgf9f") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.3 (c (n "lyon_path") (v "0.17.3") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "18w0xqdf1mn6khwrjkqdbl8z0ww6hh2v8yqwfvdslafw46shzvkk") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.4 (c (n "lyon_path") (v "0.17.4") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0wig858p4b7zihdhqq9k6vwgfa0sf1y5aim1rv9kxc7002smd76i") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.5 (c (n "lyon_path") (v "0.17.5") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "08qvchy75dz0az7q5ac2vdjr0nnw489zb8fpqni21zgsjrj7cifn") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.6 (c (n "lyon_path") (v "0.17.6") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "016c3kcvnd31p7y0pxb6qvq1b5jsavgkrfsriz4mylflrhx47xwf") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-0.17.7 (c (n "lyon_path") (v "0.17.7") (d (list (d (n "lyon_geom") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1h7vbfaanf3x7xch6md4q4ja3xmvsi41n7d6ga40vjk7yzymj2jv") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-1.0.0 (c (n "lyon_path") (v "1.0.0") (d (list (d (n "lyon_geom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0i5cjl7p4fgxdjfsgxdjg73ir3g88pmfafqyziihxvby2ik0hhma") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-1.0.1 (c (n "lyon_path") (v "1.0.1") (d (list (d (n "lyon_geom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "02ggf82h49r7k122jrs9nkiz8lfs7zcn0qz9afd278za5c0q53c9") (f (quote (("serialization" "serde" "lyon_geom/serialization"))))))

(define-public crate-lyon_path-1.0.2 (c (n "lyon_path") (v "1.0.2") (d (list (d (n "lyon_geom") (r "^1.0.3") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "03bs1z9nil2rkq7rrr7frb98nyx5g6plq627nzl3zfr7986nqaib") (f (quote (("std" "lyon_geom/std" "num-traits/std") ("serialization" "serde" "lyon_geom/serialization") ("default" "std"))))))

(define-public crate-lyon_path-1.0.3 (c (n "lyon_path") (v "1.0.3") (d (list (d (n "lyon_geom") (r "^1.0.4") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "0q7nmlaawjxbka8b008s31k7ixy0fmdhpzcwc7j52mif0663ba3x") (f (quote (("std" "lyon_geom/std" "num-traits/std") ("serialization" "serde" "lyon_geom/serialization") ("default" "std"))))))

(define-public crate-lyon_path-1.0.4 (c (n "lyon_path") (v "1.0.4") (d (list (d (n "lyon_geom") (r "^1.0.4") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "0ibcgmy6bbxvll35hdvg2bbb1li938xvdrs4bipcgjvwp92pfl6a") (f (quote (("std" "lyon_geom/std" "num-traits/std") ("serialization" "serde" "lyon_geom/serialization") ("default" "std"))))))

(define-public crate-lyon_path-1.0.5 (c (n "lyon_path") (v "1.0.5") (d (list (d (n "lyon_geom") (r "^1.0.4") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "1ms00xq5inm4wqkznzjsfrgzpabal28sq65aqvb3i5m5qw3ac24w") (f (quote (("std" "lyon_geom/std" "num-traits/std") ("serialization" "serde" "lyon_geom/serialization") ("default" "std"))))))

