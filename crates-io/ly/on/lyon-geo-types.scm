(define-module (crates-io ly on lyon-geo-types) #:use-module (crates-io))

(define-public crate-lyon-geo-types-0.1.0 (c (n "lyon-geo-types") (v "0.1.0") (d (list (d (n "geo-clipper") (r "^0.7.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "lyon") (r "^0.17.5") (d #t) (k 2)) (d (n "lyon_path") (r "^0.17.4") (d #t) (k 0)))) (h "17nl46iq94h15psbyhf4hnwn5fcjn95fdaj2akalnry1936n2d4h")))

