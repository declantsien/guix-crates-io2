(define-module (crates-io ly on lyon_tesselation) #:use-module (crates-io))

(define-public crate-lyon_tesselation-0.0.2 (c (n "lyon_tesselation") (v "0.0.2") (d (list (d (n "lyon_core") (r "^0.0.2") (d #t) (k 0)) (d (n "lyon_extra") (r "^0.0.2") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "0fxsd5bkigshxs4zya6qlws7c2gvjlq45w936mwpnyhb7zxmy40j") (y #t)))

(define-public crate-lyon_tesselation-0.0.3 (c (n "lyon_tesselation") (v "0.0.3") (d (list (d (n "lyon_core") (r "^0.0.3") (d #t) (k 0)) (d (n "lyon_extra") (r "^0.0.3") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "0hwcklf61kmhjhrnqapv88ihqziq6p5cqy6p0wxnkr3nflyg6v4i") (y #t)))

(define-public crate-lyon_tesselation-0.1.0 (c (n "lyon_tesselation") (v "0.1.0") (d (list (d (n "lyon_core") (r "^0.1.0") (d #t) (k 0)) (d (n "lyon_extra") (r "^0.1.0") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "1m1y8cv0cs8wx2gkqz15mks17wvr36q5wl01g6xq2901s63wwiy7") (y #t)))

(define-public crate-lyon_tesselation-0.1.1 (c (n "lyon_tesselation") (v "0.1.1") (d (list (d (n "lyon_core") (r "^0.1.0") (d #t) (k 0)) (d (n "lyon_extra") (r "^0.1.0") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "0ffvrwwyq3ahdkrjxp1mjb94prb4vz9lzwwbk8zynkz38rh2k6q9") (y #t)))

