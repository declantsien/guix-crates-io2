(define-module (crates-io ly on lyon_core) #:use-module (crates-io))

(define-public crate-lyon_core-0.0.2 (c (n "lyon_core") (v "0.0.2") (d (list (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "1ln4w5684b6j20nzgry1y240ixvhns2naa4iwj025ay27y0jhcnd")))

(define-public crate-lyon_core-0.0.3 (c (n "lyon_core") (v "0.0.3") (d (list (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "vodk_math") (r "^0.0.3") (d #t) (k 0)))) (h "1an86igw389n2bfs3yr53x298wiby5yr7hhnw9lazwzfi1sbgaka")))

(define-public crate-lyon_core-0.1.0 (c (n "lyon_core") (v "0.1.0") (d (list (d (n "euclid") (r "^0.7.1") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "136d72iiikjyd6wnkdg32q8cggy3i23brv6b29kbiccvpa2s4xdw")))

(define-public crate-lyon_core-0.1.1 (c (n "lyon_core") (v "0.1.1") (d (list (d (n "euclid") (r "^0.7.1") (d #t) (k 0)) (d (n "sid") (r "^0.1.0") (d #t) (k 0)) (d (n "sid_vec") (r "^0.1.0") (d #t) (k 0)))) (h "1c9jzc55a20295iywh2hpyjx16ri3r563i8q1iz7jnjmk1qmhglg")))

(define-public crate-lyon_core-0.2.0 (c (n "lyon_core") (v "0.2.0") (d (list (d (n "euclid") (r "^0.8.1") (d #t) (k 0)))) (h "03w6pnk65c42nyk2ixm5abn3v1gla43z04n9sfil0kxddcc1krk3")))

(define-public crate-lyon_core-0.3.0 (c (n "lyon_core") (v "0.3.0") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "17a1awicxpk131kz3zjpxg868501mrhmjm1a7vgxxdbn5p5ylk4l")))

(define-public crate-lyon_core-0.3.1 (c (n "lyon_core") (v "0.3.1") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "1ja7x0vjsb9fhwinlla6bk76rnaallnvkfkf1bp7f74n716vh9hh")))

(define-public crate-lyon_core-0.3.2 (c (n "lyon_core") (v "0.3.2") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "1kjd5giprgj10rng0v08jqw3jr3k68hcmynqsgmdbn6lh6xylqn0")))

(define-public crate-lyon_core-0.4.0 (c (n "lyon_core") (v "0.4.0") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "0p3fblhvgj3h7lp7763qlj7nhxlvfvccsjmgl8k9vm6zncgfq3r2")))

(define-public crate-lyon_core-0.4.1 (c (n "lyon_core") (v "0.4.1") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "01n7ara1vdxzqs25g40pfgil2dlz5mi6hr57af6kdrg5pr2lrc7k")))

(define-public crate-lyon_core-0.5.0 (c (n "lyon_core") (v "0.5.0") (d (list (d (n "euclid") (r "^0.13") (d #t) (k 0)))) (h "04qx7ap4lcwdla5ipani9z6db0i1d9vr2dlwcqs8mh1wxjniz2z2")))

(define-public crate-lyon_core-0.6.0 (c (n "lyon_core") (v "0.6.0") (d (list (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "147ghh7nxh4b8f665fs440pb49syaw76ih2g6zxza2fggdf5w2g8")))

(define-public crate-lyon_core-0.7.0 (c (n "lyon_core") (v "0.7.0") (d (list (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "0xxcda76m06jqh10iwf5gywsw7kjbn5mml9h9hfx5626mnvhrfw7")))

(define-public crate-lyon_core-0.8.0 (c (n "lyon_core") (v "0.8.0") (d (list (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "1r79x7zay3z82jd0z2qwbbb8pa69ai00fzf1f3qh8aaqx6bz1vvg")))

