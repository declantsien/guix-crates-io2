(define-module (crates-io ly on lyon-usvg) #:use-module (crates-io))

(define-public crate-lyon-usvg-0.1.0 (c (n "lyon-usvg") (v "0.1.0") (d (list (d (n "lyon_path") (r "^0.17.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "usvg") (r "^0.15.0") (d #t) (k 0)))) (h "0qkwcl3r5qmxbjag6larzf74gh9m6ah9kgyq92mmx2jg827b3l66")))

(define-public crate-lyon-usvg-0.1.1 (c (n "lyon-usvg") (v "0.1.1") (d (list (d (n "lyon_path") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "usvg") (r "^0.16") (d #t) (k 0)))) (h "1aqlpp5lpp7136m4vma39i1rc9ig2xamd4q8zljh1i8jy5dsrivz")))

