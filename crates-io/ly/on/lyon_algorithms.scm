(define-module (crates-io ly on lyon_algorithms) #:use-module (crates-io))

(define-public crate-lyon_algorithms-0.11.0 (c (n "lyon_algorithms") (v "0.11.0") (d (list (d (n "lyon_path") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "08y7i8smj0hpyh848nr1grc87lb4g2pwf2x73qkvj74f1c307kah") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.11.1 (c (n "lyon_algorithms") (v "0.11.1") (d (list (d (n "lyon_path") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zqv0ia40xzydaba030chwx3psrpbdlxdmb20k4dircapapbx6db") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.11.2 (c (n "lyon_algorithms") (v "0.11.2") (d (list (d (n "lyon_path") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1gmbbb64pq6plbq8f946f20k3h5rv38jp69jsgcfg57spywygsxk") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.13.0 (c (n "lyon_algorithms") (v "0.13.0") (d (list (d (n "lyon_path") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.5.2") (d #t) (k 0)))) (h "1wa5755h7y2zpnjxnff30y24vh1da7psjm261rms5z7b78n11rz9") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.13.1 (c (n "lyon_algorithms") (v "0.13.1") (d (list (d (n "lyon_path") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.5.2") (d #t) (k 0)))) (h "01sxzqaanv3gx59x0ddwhliqw1xvqchrg5c5hdgx4mbli9lm4snv") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.13.2 (c (n "lyon_algorithms") (v "0.13.2") (d (list (d (n "lyon_path") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.5.2") (d #t) (k 0)))) (h "0vqzpxnkn9kk6ss59sxap7hy0i05h9p1cp0dp8p84jm6ll7bjd6r") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.13.4 (c (n "lyon_algorithms") (v "0.13.4") (d (list (d (n "lyon_path") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.5.2") (d #t) (k 0)))) (h "11q299zzpylq0h2xdw23c6zfj20wf72fibn74r0jsvcff0pz9kss") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.14.0 (c (n "lyon_algorithms") (v "0.14.0") (d (list (d (n "lyon_path") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.5.2") (d #t) (k 0)))) (h "16qqsxr9dp5sx3nd671zla78pxk7j91x4yhy12fa7zpv4ss1nhrl") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.15.0 (c (n "lyon_algorithms") (v "0.15.0") (d (list (d (n "lyon_path") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0yq2pp0axdx3b42f6f3d63s3bdds1fcmv9lqkx2mzrfvamh6xs6j") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.15.1 (c (n "lyon_algorithms") (v "0.15.1") (d (list (d (n "lyon_path") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "1cdbs7988pvar6xa35xdbl7hqw0x6sqljvzdxwkj8ij2ffa7jpgg") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.16.0 (c (n "lyon_algorithms") (v "0.16.0") (d (list (d (n "lyon_path") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "1a3jcshlqlmkqmfif13343rkij3bifgs2wdsnwsvzmz9llv3j0qi") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.0 (c (n "lyon_algorithms") (v "0.17.0") (d (list (d (n "lyon_path") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0flrkqwx11wxgxw1aqrf6ynb1caikvcrdgi793i18hnl2693jdhk") (f (quote (("serialization" "serde" "lyon_path/serialization")))) (y #t)))

(define-public crate-lyon_algorithms-0.17.1 (c (n "lyon_algorithms") (v "0.17.1") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "14yhrib97byrxvsn6h6qvxf4cwaf2v5lafahk0hvkl0a8qbp0l5c") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.2 (c (n "lyon_algorithms") (v "0.17.2") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "09xf28jgmk7is8n48jff8s6h086rddn1bln8ldi2gqymh2i39ypy") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.3 (c (n "lyon_algorithms") (v "0.17.3") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "09jmra2g6p4w9adcix94vmw03l40fvk0dhz1lvd1m33a6nc1s671") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.4 (c (n "lyon_algorithms") (v "0.17.4") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0cw95vpsm12zs3bvm835p5s51vialjgzq7x3h7byhjg6y22zlcb9") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.5 (c (n "lyon_algorithms") (v "0.17.5") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0q7r1gvhwmz9phivjlrhdvh6ijm83g8hjas0ksy2frh70x8vq7pa") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.6 (c (n "lyon_algorithms") (v "0.17.6") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0mrzj1npknd57307pw7vz47ahyz1cbb6i9vvhgj5awh7c8glhm9l") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-0.17.7 (c (n "lyon_algorithms") (v "0.17.7") (d (list (d (n "lyon_path") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sid") (r "^0.6") (d #t) (k 0)))) (h "0ji4i9698kdqb02rhm8dk5rsz2q6z1lh0p707m6xi80vahbgfdw0") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-1.0.0 (c (n "lyon_algorithms") (v "1.0.0") (d (list (d (n "lyon_path") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1sfcq9nrqa0sqc49lxxi8vhx15yspkzk865zrfcls459mlwz26z1") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-1.0.1 (c (n "lyon_algorithms") (v "1.0.1") (d (list (d (n "lyon_path") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0wq5ddgdahp0ywly6cjgc4qks3grrk3hqgfm0v1f6af7zgzc4lvd") (f (quote (("serialization" "serde" "lyon_path/serialization"))))))

(define-public crate-lyon_algorithms-1.0.2 (c (n "lyon_algorithms") (v "1.0.2") (d (list (d (n "lyon_path") (r "^1.0.2") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "0v2wmkrhvbisrp1sals4971p6zv41djwnhjxs9rrswhmbj2a3dzw") (f (quote (("std" "lyon_path/std" "num-traits/std") ("serialization" "serde" "lyon_path/serialization") ("default" "std"))))))

(define-public crate-lyon_algorithms-1.0.3 (c (n "lyon_algorithms") (v "1.0.3") (d (list (d (n "lyon_path") (r "^1.0.2") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "0vjdlksn6dyfwdxlwy9798xqw5zn7nv29a4kpf0hf9zhv2f39800") (f (quote (("std" "lyon_path/std" "num-traits/std") ("serialization" "serde" "lyon_path/serialization") ("default" "std"))))))

(define-public crate-lyon_algorithms-1.0.4 (c (n "lyon_algorithms") (v "1.0.4") (d (list (d (n "lyon_path") (r "^1.0.2") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "08y6njzmhlry2xc5gfshk8mbxngvxpavryr1m3jb6ma9k9gskg53") (f (quote (("std" "lyon_path/std" "num-traits/std") ("serialization" "serde" "lyon_path/serialization") ("default" "std"))))))

