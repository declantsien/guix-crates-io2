(define-module (crates-io ly on lyon_path_builder) #:use-module (crates-io))

(define-public crate-lyon_path_builder-0.2.0 (c (n "lyon_path_builder") (v "0.2.0") (d (list (d (n "lyon_bezier") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.2.0") (d #t) (k 0)))) (h "1h1b864ij6pr7w1i2g6gngv2wy1wdxnbznpmrhfnf8x3ssy27kdq")))

(define-public crate-lyon_path_builder-0.3.0 (c (n "lyon_path_builder") (v "0.3.0") (d (list (d (n "lyon_bezier") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.0") (d #t) (k 0)))) (h "1iymww2qhnz9hh83mihwrmkxqrz8bwkjb6mcmjpkayr1mal9dnf3")))

(define-public crate-lyon_path_builder-0.3.1 (c (n "lyon_path_builder") (v "0.3.1") (d (list (d (n "lyon_bezier") (r "^0.3.1") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.1") (d #t) (k 0)))) (h "1sg4c3ibxwnbcpisda5qyjx3cf1k6nii870kmrr2j66pqz1xlgvk")))

(define-public crate-lyon_path_builder-0.3.2 (c (n "lyon_path_builder") (v "0.3.2") (d (list (d (n "lyon_bezier") (r "^0.3.2") (d #t) (k 0)) (d (n "lyon_core") (r "^0.3.2") (d #t) (k 0)))) (h "0k93mwnpv06slr6x3xanadqzv9zvfrqgdn9bz8wm6hgkqs2k69ph")))

(define-public crate-lyon_path_builder-0.4.0 (c (n "lyon_path_builder") (v "0.4.0") (d (list (d (n "lyon_bezier") (r "^0.4.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.4.0") (d #t) (k 0)))) (h "0rsnj9lwfrk8minyaid2yfa161161cg1awilhsk1n2hbxc3gq69y")))

(define-public crate-lyon_path_builder-0.4.1 (c (n "lyon_path_builder") (v "0.4.1") (d (list (d (n "lyon_bezier") (r "^0.4.1") (d #t) (k 0)) (d (n "lyon_core") (r "^0.4.1") (d #t) (k 0)))) (h "0zhv8i79g360ypdky20p9i38jr0vigfwlgg518rv20r770pfhcpr")))

(define-public crate-lyon_path_builder-0.5.0 (c (n "lyon_path_builder") (v "0.5.0") (d (list (d (n "lyon_bezier") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.5.0") (d #t) (k 0)))) (h "01b1v3916csvlq5d3z181ra5d992fr02f9wkiiz2c4mj3i27s60q")))

(define-public crate-lyon_path_builder-0.6.0 (c (n "lyon_path_builder") (v "0.6.0") (d (list (d (n "lyon_bezier") (r "^0.6.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.6.0") (d #t) (k 0)))) (h "080j90lydpc0v69n9zcp93fmmzgppigmw217cfsz2m2fxpypkpsy")))

(define-public crate-lyon_path_builder-0.7.0 (c (n "lyon_path_builder") (v "0.7.0") (d (list (d (n "lyon_bezier") (r "^0.7.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.7.0") (d #t) (k 0)))) (h "0j8q7kk5kdh1wmr6as4f0dmxw8b0748q36jxgjxwmv1pany22p9k")))

(define-public crate-lyon_path_builder-0.8.0 (c (n "lyon_path_builder") (v "0.8.0") (d (list (d (n "lyon_bezier") (r "^0.8.0") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)))) (h "1nmg3rzrbqsymgzcg04zfarn8mgflwxswykjl0icrxss05aaf1hj")))

(define-public crate-lyon_path_builder-0.8.5 (c (n "lyon_path_builder") (v "0.8.5") (d (list (d (n "lyon_bezier") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)))) (h "1wqpfb45k555bify6glrkl2z95yaf0588l7nd50yr5j2qkfzmbnm")))

(define-public crate-lyon_path_builder-0.8.6 (c (n "lyon_path_builder") (v "0.8.6") (d (list (d (n "lyon_bezier") (r "^0.8.5") (d #t) (k 0)) (d (n "lyon_core") (r "^0.8.0") (d #t) (k 0)))) (h "16xwwsh2qmilip3r6hxwjf8mjglrs08ggmz1xl2v166b47w7xz6v")))

(define-public crate-lyon_path_builder-0.9.0 (c (n "lyon_path_builder") (v "0.9.0") (d (list (d (n "lyon_path") (r "^0.10") (d #t) (k 0)))) (h "0qcj4pl6zka8ji9b3nxxqc22dlmhxfkwri2y82sy28gzlplbvpzv")))

