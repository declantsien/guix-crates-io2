(define-module (crates-io ly on lyon_bezier) #:use-module (crates-io))

(define-public crate-lyon_bezier-0.2.0 (c (n "lyon_bezier") (v "0.2.0") (d (list (d (n "euclid") (r "^0.8.1") (d #t) (k 0)))) (h "02f1vc6gc80h1mg7zrz3k3i1d6lmb45jmq8qqhxy1jiw27d45v7a")))

(define-public crate-lyon_bezier-0.3.0 (c (n "lyon_bezier") (v "0.3.0") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "1hyhhk2hi2xjajfjv6j0m5c8m01f71sj7fc4rnrzyqcxa5y31pdx")))

(define-public crate-lyon_bezier-0.3.1 (c (n "lyon_bezier") (v "0.3.1") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "0vwkz0agryirv622xpdk4h8hj7q1n5j3d06c7lnam3w5cnqz1l9y")))

(define-public crate-lyon_bezier-0.3.2 (c (n "lyon_bezier") (v "0.3.2") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "1c01rqvrphfywryb80nf5r7bsyw40azwznsra41max2gc6768hhg")))

(define-public crate-lyon_bezier-0.4.0 (c (n "lyon_bezier") (v "0.4.0") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "0wp0inlrwm21njxmhw0wz5f5nqhxzv415cck2msc5pc3qqssz9ls")))

(define-public crate-lyon_bezier-0.4.1 (c (n "lyon_bezier") (v "0.4.1") (d (list (d (n "euclid") (r "^0.10.1") (d #t) (k 0)))) (h "02wkqmm1xg2dlpiby6clkxjfx245q2s66q6f784jhm0ir4h6bxg5")))

(define-public crate-lyon_bezier-0.5.0 (c (n "lyon_bezier") (v "0.5.0") (d (list (d (n "euclid") (r "^0.13") (d #t) (k 0)))) (h "0pc4qyjsqlrc1pxq4gcwmfhn4dbhnmrj0224jmpcrvmad3pmvwgb")))

(define-public crate-lyon_bezier-0.6.0 (c (n "lyon_bezier") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "0v3mlm8iw55d0zbiz2y3vj8n9b62aafahjmmcbjzw1sky70vdfhq")))

(define-public crate-lyon_bezier-0.6.1 (c (n "lyon_bezier") (v "0.6.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "025d3vygk1ba8x273iz5dl2gzjvyi2n7a207mm8ckvn7nngkrc7i")))

(define-public crate-lyon_bezier-0.7.0 (c (n "lyon_bezier") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "0zgqwxmla9k9lfchgsciiy9lbwrzd7cjabq7xsif1rd8ssj2kp0m")))

(define-public crate-lyon_bezier-0.7.1 (c (n "lyon_bezier") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "0gcs34fsx1xsyljwlb058rrm2c8nhxn0zyipl3g661px1i3d2zcv")))

(define-public crate-lyon_bezier-0.8.0 (c (n "lyon_bezier") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "11z4fx007gv7jkrxp1w5nmm0cmfwd9b76rpw4sa3myx5q4mg3f7v")))

(define-public crate-lyon_bezier-0.8.4 (c (n "lyon_bezier") (v "0.8.4") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "15bb43wbdmww26sl5jq5zgwydmsgrxa2br1ymks6bn7fj4qb4kab")))

(define-public crate-lyon_bezier-0.8.5 (c (n "lyon_bezier") (v "0.8.5") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)))) (h "1jyh1nzs74qhqy8wnrl0az0dzmchmi16ij65bgzadbv0g2cngxwp")))

(define-public crate-lyon_bezier-0.9.0 (c (n "lyon_bezier") (v "0.9.0") (d (list (d (n "lyon_geom") (r "^0.10.1") (d #t) (k 0)))) (h "1wbr53jp8dfs6c3r86z2nbf1ff24myi0rk9980b08b804xsh3c19")))

