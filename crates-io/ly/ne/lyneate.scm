(define-module (crates-io ly ne lyneate) #:use-module (crates-io))

(define-public crate-lyneate-0.1.0 (c (n "lyneate") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "10m4sxzyjcnmak9bn7vnm7cyflyc9zznr4a0yv46q98v3f597pf5")))

(define-public crate-lyneate-0.1.1 (c (n "lyneate") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "06irqva12fwp03nl5spgix6sssq3b92i8wpi27jrbp9nf3fd8jg5")))

(define-public crate-lyneate-0.2.0 (c (n "lyneate") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0wdbxqd9qidlbls1qxpq30vs5wcra6lxyla04y49jsmk5ir0f08r")))

(define-public crate-lyneate-0.2.1 (c (n "lyneate") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1qx5bw98jyl3711dhamb6m347n2ynzr62sjwdxwm44m7gqsg14yv")))

