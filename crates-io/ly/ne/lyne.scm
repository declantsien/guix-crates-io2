(define-module (crates-io ly ne lyne) #:use-module (crates-io))

(define-public crate-lyne-0.0.1-alpha (c (n "lyne") (v "0.0.1-alpha") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)))) (h "14z1s6rdvfsa213i9rym0wcay1vqkmlfqyqx7n167r7n11lkls03")))

(define-public crate-lyne-0.0.1-alpha.1 (c (n "lyne") (v "0.0.1-alpha.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)))) (h "1rnfn09a9mwgbwv7ar9n07k5w0mm911a3wwqx7xymis5r4qjb9rp")))

(define-public crate-lyne-0.0.1-alpha.2 (c (n "lyne") (v "0.0.1-alpha.2") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)))) (h "0yq6wl717fmyvg3k75ls5m2d88l86cf4zv8wrjqm2zl57i6f8n8w")))

(define-public crate-lyne-0.0.1-alpha.3 (c (n "lyne") (v "0.0.1-alpha.3") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)))) (h "13rs3j846f5bh01p08byd7xjr6dl8va3qfjqk3vfdhvyqpf49x5h")))

