(define-module (crates-io ly co lycopersicum) #:use-module (crates-io))

(define-public crate-lycopersicum-0.1.0 (c (n "lycopersicum") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "184vzf6p94wzz2sxf3pvi19rrwhn2biav81zgc6lx45xb9y8h9p9")))

(define-public crate-lycopersicum-0.2.0 (c (n "lycopersicum") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "0mvia9cj4djsc94zvcfgcanxgh558hsjinw1jv39jl5022m73rvg")))

(define-public crate-lycopersicum-0.3.0 (c (n "lycopersicum") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "0cykgwzcdizpah1qnq2gz31dcsgizwsv327qvbmm5fzrr3iii033")))

(define-public crate-lycopersicum-0.3.1 (c (n "lycopersicum") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "0isn29w966hq3m2f7xv3s3xlk5qkqzi2yc0i6dn8pqwp53xri5lk")))

(define-public crate-lycopersicum-0.3.2 (c (n "lycopersicum") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "16mg28124gh4rxffnqfs0301qlgnqzszh7s2m0ibzk98pzaql6mq")))

(define-public crate-lycopersicum-0.4.0 (c (n "lycopersicum") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "030fj830xvz03pqm72dqcyk8wr4rr759mhn0i18w029v951kfn4z")))

