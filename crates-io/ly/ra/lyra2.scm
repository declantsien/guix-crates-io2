(define-module (crates-io ly ra lyra2) #:use-module (crates-io))

(define-public crate-lyra2-0.1.0 (c (n "lyra2") (v "0.1.0") (d (list (d (n "blake-hash") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "groestl") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.0") (d #t) (k 0)))) (h "15l70smrv9kbbi6jbm28vfwrqdgyvalkccmylsy8j48xxhixws63")))

(define-public crate-lyra2-0.2.0 (c (n "lyra2") (v "0.2.0") (d (list (d (n "blake-hash") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "groestl") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.0") (d #t) (k 0)))) (h "0jmvn4n8sqiahrn3wzzhn9a9bl0js2fhbnjcigz5rm7c6ifrh8b8")))

(define-public crate-lyra2-0.2.1 (c (n "lyra2") (v "0.2.1") (d (list (d (n "blake-hash") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "groestl") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.0") (d #t) (k 0)))) (h "120wrrbypw94zf79s1wrda4bf9ccass9qd92jrdcwmin73f564vr")))

(define-public crate-lyra2-0.2.2 (c (n "lyra2") (v "0.2.2") (d (list (d (n "blake-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "groestl") (r "^0.9.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.0") (d #t) (k 0)))) (h "00sa3bn3y45zdlfr6k7wxx17mihiwsfd79r4nwpwpnxhz7yrzf98")))

(define-public crate-lyra2-0.2.4 (c (n "lyra2") (v "0.2.4") (d (list (d (n "blake-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "groestl") (r "^0.9.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.1") (d #t) (k 0)))) (h "00ir9zidwc9zx938sswmi1n46vm8jkp6zpvwja39q4h8syh1aqsd")))

(define-public crate-lyra2-0.2.5 (c (n "lyra2") (v "0.2.5") (d (list (d (n "blake-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "groestl") (r "^0.9.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.1") (d #t) (k 0)))) (h "0nqbr6lp6mi837khqls50z6y4mn3cxxl5kab1vkyhmnryr530gmx")))

(define-public crate-lyra2-0.2.6 (c (n "lyra2") (v "0.2.6") (d (list (d (n "blake-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "groestl") (r "^0.9.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "skein-hash") (r "^0.3.1") (d #t) (k 0)))) (h "1asvdwn7j63frgfakgk9ny1dmi3vhzbp7frbg1xjs1lpdj63lg7p")))

