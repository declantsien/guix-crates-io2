(define-module (crates-io nl pc nlpcloud) #:use-module (crates-io))

(define-public crate-nlpcloud-0.0.1 (c (n "nlpcloud") (v "0.0.1") (h "047pwcglj55m3icjx8f9x4w8f73fv2p3cyxnwwrv48r5myy0ppcd")))

(define-public crate-nlpcloud-0.0.2 (c (n "nlpcloud") (v "0.0.2") (h "0c22gn2l8bxj9yh4khy46jscgfdjdgwv349s3kn53a8j4a0km0d0")))

(define-public crate-nlpcloud-0.0.3 (c (n "nlpcloud") (v "0.0.3") (h "0aph2b35x0b31bnk0swdv36ivlfmdhblac5ix1sppfcp87scb8x9")))

