(define-module (crates-io nl p- nlp-annotations) #:use-module (crates-io))

(define-public crate-nlp-annotations-0.1.0 (c (n "nlp-annotations") (v "0.1.0") (h "04xi58b8jh8jq0x9z8b1nwv0rbbz0kvn6j2zlhvgs1qsr326h4d2") (y #t)))

(define-public crate-nlp-annotations-0.1.1 (c (n "nlp-annotations") (v "0.1.1") (h "1yzpw794jcw7ddc3v82i3y08g106yn9gv7v2z2a24vgg6pps3awx") (y #t)))

(define-public crate-nlp-annotations-0.1.2 (c (n "nlp-annotations") (v "0.1.2") (h "1n3h3h0vgki2nrb2yklgklmdikgscx6gjr5fblg78nnf6w53m1f1") (y #t)))

