(define-module (crates-io nl p- nlp-io) #:use-module (crates-io))

(define-public crate-nlp-io-0.1.1 (c (n "nlp-io") (v "0.1.1") (d (list (d (n "nlp-annotations") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "0dfphdy9rfnj429p9d7bi6vwgvdvd8k16dmdd7p3iqya3xx267p2") (y #t)))

(define-public crate-nlp-io-0.1.2 (c (n "nlp-io") (v "0.1.2") (d (list (d (n "nlp-annotations") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "0gqbny0m7qlylxd5gi0bq2v6sg1lb15vjqgdqhg1c8plvr10drgl") (y #t)))

