(define-module (crates-io nl sd nlsd) #:use-module (crates-io))

(define-public crate-nlsd-0.1.0 (c (n "nlsd") (v "0.1.0") (d (list (d (n "nl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gzpz4p2ds0idn78w5pbflipl1lgigsmjpabcydlga15bjqrx196") (f (quote (("default"))))))

(define-public crate-nlsd-0.1.1 (c (n "nlsd") (v "0.1.1") (d (list (d (n "nl-parser") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1q49v486b2lkrf43nnxrqf0bydf2a4kl1zky5cxccmala1wyx7az") (f (quote (("std" "nl-parser/std" "serde/std" "num-traits/std") ("default" "std"))))))

(define-public crate-nlsd-0.1.2 (c (n "nlsd") (v "0.1.2") (d (list (d (n "nl-parser") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1338g7qk4pi6cb1jp1az8xhqikpl198y9l1cvg53lvb4cxhxw8g8") (f (quote (("std" "nl-parser/std" "serde/std" "num-traits/std") ("default" "std"))))))

(define-public crate-nlsd-0.1.3 (c (n "nlsd") (v "0.1.3") (d (list (d (n "nl-parser") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1h8lzia118vin5jhbd2nzqp9j6qlcf7vshcrn3rdxwl50cbj1x1w") (f (quote (("std" "nl-parser/std" "serde/std" "num-traits/std") ("default" "std"))))))

