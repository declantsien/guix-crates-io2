(define-module (crates-io nl oq nloq) #:use-module (crates-io))

(define-public crate-nloq-0.1.0 (c (n "nloq") (v "0.1.0") (d (list (d (n "nl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "object-query") (r "^0.1.0") (d #t) (k 0)))) (h "1k90ijkfclnb9ywfr082wghd4mw9czkv1j3vkv0rgk7ac6nyk0q1") (f (quote (("default"))))))

(define-public crate-nloq-0.1.1 (c (n "nloq") (v "0.1.1") (d (list (d (n "nl-parser") (r "^0.1.1") (k 0)) (d (n "object-query") (r "^0.1.3") (k 0)))) (h "14v1zg72y7nlsrvbb3y7s5zyr22ip5dv4yrxlpbkcp33sj1mcfkz") (f (quote (("std" "nl-parser/std" "object-query/std") ("default" "std"))))))

