(define-module (crates-io nl zs nlzss11) #:use-module (crates-io))

(define-public crate-nlzss11-1.0.0 (c (n "nlzss11") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "15w6fbafj8qjkzwp9pfxjpr8gy2bvmpdphkinhjr53hh424j8x7q")))

(define-public crate-nlzss11-1.0.1 (c (n "nlzss11") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nlzss11-zlib") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0kqz4y39lmk14vca58b4773jbm713jjal2r89zs76z9sf5p42frr") (f (quote (("zlib" "nlzss11-zlib"))))))

