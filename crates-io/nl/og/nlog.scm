(define-module (crates-io nl og nlog) #:use-module (crates-io))

(define-public crate-nlog-0.1.0 (c (n "nlog") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0xwdw4higfpmi56h434gp69ddkhxcdzxl9jnsbfnpc52x2aa2xd2")))

(define-public crate-nlog-0.1.1 (c (n "nlog") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0ljfxdcs3r61ihskiv73rrqn4n1v58317ls950rv1zz18jicr7my")))

(define-public crate-nlog-0.2.1 (c (n "nlog") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "winuser"))) (d #t) (k 0)))) (h "0w7k750lwch5d0xlfdbqnnpjq8adx9i8yd9qhrhg1n6hpfasj98f")))

(define-public crate-nlog-0.2.2 (c (n "nlog") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "winuser"))) (d #t) (k 0)))) (h "01mnqxia04xlq5rlnblbrz7fdwl85k8jn6141286qw53j1f5yw5x")))

