(define-module (crates-io nl #{80}# nl80211-ng) #:use-module (crates-io))

(define-public crate-nl80211-ng-0.1.0 (c (n "nl80211-ng") (v "0.1.0") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11ffp4wr5pnx2lcbavbwhwan8myg1pwyljx77yg48km6dzskxy7i")))

(define-public crate-nl80211-ng-0.1.1 (c (n "nl80211-ng") (v "0.1.1") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cgl0106c4fkvsq8zsyvznhm6p4zgj0knvkdqx6cg45057c5gbrw")))

(define-public crate-nl80211-ng-0.1.2 (c (n "nl80211-ng") (v "0.1.2") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pgimicgckksyymwak9ri2xygbrq7qzx4j1g1j36pm2n8w5yj4qh")))

(define-public crate-nl80211-ng-0.1.3 (c (n "nl80211-ng") (v "0.1.3") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nifd4ysij99dxng6vms84hd8g50wcymfzrprb5vknll7bk7d5vm")))

(define-public crate-nl80211-ng-0.1.4 (c (n "nl80211-ng") (v "0.1.4") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06b5dz5rzdf2nny9s8f7n179bkcmgydgfwygm40gnh92mpszd7rh")))

(define-public crate-nl80211-ng-0.1.5 (c (n "nl80211-ng") (v "0.1.5") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h9ii3f6yhky3ixln4k5kqbjr37366ca7ff6jijr6hnygg4i10dh")))

(define-public crate-nl80211-ng-0.1.6 (c (n "nl80211-ng") (v "0.1.6") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ygyyji0dnypg2fvzip63dwh9bh3vqfwnlxzpr8pr208fagdh7mx")))

(define-public crate-nl80211-ng-0.1.7 (c (n "nl80211-ng") (v "0.1.7") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n0ihj8a3iviq7nwlh8w3gglcp21xv3s4qxn7q6pppn74avarmih")))

(define-public crate-nl80211-ng-0.2.0 (c (n "nl80211-ng") (v "0.2.0") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rw06fwn6521fhzg3xvhc6mr89ad9brmm0kl7ghhld598pmjfz02")))

(define-public crate-nl80211-ng-0.2.1 (c (n "nl80211-ng") (v "0.2.1") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15p3s4q4d5swb94fkpdcpvk6v93c8z2sijmfil4hhv6c6fbf9968")))

(define-public crate-nl80211-ng-0.2.2 (c (n "nl80211-ng") (v "0.2.2") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jyhnykz7x2limslhypzhfkjzsfbb5183l8mjapaybsqxyy93qc6")))

(define-public crate-nl80211-ng-0.2.3 (c (n "nl80211-ng") (v "0.2.3") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ajwby29glv94prb68ww844jn9mx37ffb72zfj68xm69iljc8zva")))

(define-public crate-nl80211-ng-0.2.4 (c (n "nl80211-ng") (v "0.2.4") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c63yg92yyr93azxf87hx5ajxsdsmnc8mdprzc0y3b4zg4nszgjb")))

(define-public crate-nl80211-ng-0.2.5 (c (n "nl80211-ng") (v "0.2.5") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pjlk1q31cvcanjb7y2vifn3xs8hgnhxr6w304w9n6gg63hzn58h")))

(define-public crate-nl80211-ng-0.2.6 (c (n "nl80211-ng") (v "0.2.6") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yj32s6k4yj7hvzckwwkapqr548h62f24km2dml4naps87wik4wi")))

(define-public crate-nl80211-ng-0.2.7 (c (n "nl80211-ng") (v "0.2.7") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y3kglpk6n19qjiwzg04xpy0vi4cdin1iyk1wsk02l13siqgwp3c")))

(define-public crate-nl80211-ng-0.2.8 (c (n "nl80211-ng") (v "0.2.8") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vlhx57p1zadngs9vmm47zyyfpcy46ww0166clfb64rb60jamzqp")))

(define-public crate-nl80211-ng-0.2.9 (c (n "nl80211-ng") (v "0.2.9") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yjwpxa6x0pwd1vkb0s74pvzbaqrbglgsxqrd8wc4jmvj18ch9sk")))

(define-public crate-nl80211-ng-0.2.10 (c (n "nl80211-ng") (v "0.2.10") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nwdvmscapvxxfanlcvza5yvw88y0g4bvhbxw4crrj00y7ygpsw7")))

(define-public crate-nl80211-ng-0.3.0 (c (n "nl80211-ng") (v "0.3.0") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18cyb1wkj3ipnm2k8gf98hs9jjppp8q594cl2imggxp7csbacdfr")))

(define-public crate-nl80211-ng-0.3.1 (c (n "nl80211-ng") (v "0.3.1") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rgbmafki6xylpb0kmsvz8kgh77lf2f5pwk767gd68g5iyvibl6b")))

(define-public crate-nl80211-ng-0.4.0 (c (n "nl80211-ng") (v "0.4.0") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02q3is01frm2hg04771an2gicgvya4gzzg0lkdla8c84nnqa86zd")))

(define-public crate-nl80211-ng-0.4.1 (c (n "nl80211-ng") (v "0.4.1") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07dxd7nb9cad37sr99d18r34kzx7cffchj6ps1rjvlhhpjs24q46")))

(define-public crate-nl80211-ng-0.5.0 (c (n "nl80211-ng") (v "0.5.0") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nnwi0my6jvcia9jbhvszl77wmrnvz8v96jyxvb0w9jrjk0g11v3")))

(define-public crate-nl80211-ng-0.5.1 (c (n "nl80211-ng") (v "0.5.1") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1i60f30vkkid21wi15l4y6z528wa7m7fg5pa82jf5k2vpibjjmsz")))

(define-public crate-nl80211-ng-0.5.2 (c (n "nl80211-ng") (v "0.5.2") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0i8n7fraz4d77pr715xv36k72ii9af75jadqapl78rvji83jijy6")))

(define-public crate-nl80211-ng-0.5.3 (c (n "nl80211-ng") (v "0.5.3") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ilq0aachq0yzw3r42bj342vv86hyf2snxyxxnb5lmlj070n4inh")))

(define-public crate-nl80211-ng-0.5.5 (c (n "nl80211-ng") (v "0.5.5") (d (list (d (n "neli") (r "^0.6.4") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h6scf549j3d4qmwiw90j7157frcqgggizh3hcw23kl5xvl330bi")))

