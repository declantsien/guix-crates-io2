(define-module (crates-io nl #{80}# nl80211-buildtools) #:use-module (crates-io))

(define-public crate-nl80211-buildtools-0.1.0 (c (n "nl80211-buildtools") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02ikb67bqzbj0ijzcxcdqw8wakgz7yk43mb1wr6fq1b1zy6383xs")))

