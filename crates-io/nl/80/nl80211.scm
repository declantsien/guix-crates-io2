(define-module (crates-io nl #{80}# nl80211) #:use-module (crates-io))

(define-public crate-nl80211-0.0.1 (c (n "nl80211") (v "0.0.1") (d (list (d (n "buffering") (r "^0.3.2") (f (quote ("copy"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "neli") (r "^0.4.3-r1") (d #t) (k 0)))) (h "1x01lj20ys3qp8a3c5m1h46702mfl9vf307zk3nx1sq03ydsnqmd")))

(define-public crate-nl80211-0.0.2 (c (n "nl80211") (v "0.0.2") (d (list (d (n "buffering") (r "^0.3.2") (f (quote ("copy"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "neli") (r "^0.4.3-r1") (d #t) (k 0)))) (h "0jgx12qy0a004sc4qpr3ahgn9gma3rln9gsxiq4cdw6dd8h4dmx0")))

