(define-module (crates-io nl #{80}# nl80211-rs) #:use-module (crates-io))

(define-public crate-nl80211-rs-0.1.0 (c (n "nl80211-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "netlink-rust") (r "^0.1") (d #t) (k 0)) (d (n "nl80211-buildtools") (r "^0.1") (d #t) (k 1)))) (h "1g99s952w5ncf1mnn05aniqjgh3ml7k6xl9f5k2mii2q5sajvg7z")))

