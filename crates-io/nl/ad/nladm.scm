(define-module (crates-io nl ad nladm) #:use-module (crates-io))

(define-public crate-nladm-0.1.0 (c (n "nladm") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nanoleaf") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05hx6fp8723qhm5aw37hcgk6smlwlvw8j4992fm61c8092gvvm5z")))

(define-public crate-nladm-0.1.1 (c (n "nladm") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nanoleaf") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05k4qpbilkxqgirhgz81vjramgw55yd26cvqrzh4mzca9z387njv")))

