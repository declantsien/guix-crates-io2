(define-module (crates-io nl sn nlsn-delaunay) #:use-module (crates-io))

(define-public crate-nlsn-delaunay-0.1.0 (c (n "nlsn-delaunay") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0gbxvs5j4gg7cjknqph535k9ckznjcfqhh18q8md7cal2halqxq0")))

(define-public crate-nlsn-delaunay-0.1.1 (c (n "nlsn-delaunay") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "082ddw8hw6f61vn16rs1p15g5xkk1b9lr5wz2nnfa09yr2lrb1qy")))

