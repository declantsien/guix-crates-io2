(define-module (crates-io nl -d nl-dump) #:use-module (crates-io))

(define-public crate-nl-dump-0.1.2 (c (n "nl-dump") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "pcap") (r "^0.5.0") (d #t) (k 0)))) (h "13nf38nlk12byz05n8i5skqbnffwyr1spi5kna3a70whvwsmk2lq")))

(define-public crate-nl-dump-0.1.3 (c (n "nl-dump") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "pcap") (r "^0.5.0") (d #t) (k 0)) (d (n "rust-enum-derive") (r "^0.4.0") (d #t) (k 1)))) (h "1qg30bfj8grs5acfp723ssgdasr186gcqlz9a522qcgnl272q41s")))

