(define-module (crates-io nl po nlpo3-cli) #:use-module (crates-io))

(define-public crate-nlpo3-cli-0.0.1 (c (n "nlpo3-cli") (v "0.0.1") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "nlpo3") (r "^1.1.2") (d #t) (k 0)))) (h "18154zv0y84lalryzjsg32r7lllrjpmjdwp6bzys3s6nb0cx0x7l")))

(define-public crate-nlpo3-cli-0.1.0 (c (n "nlpo3-cli") (v "0.1.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "nlpo3") (r "^1.2.0") (d #t) (k 0)))) (h "1fi2q0rk4c4m5ijzi3gjbssja6imm5draklbzbhcg9rfy7dnz479")))

(define-public crate-nlpo3-cli-0.2.0 (c (n "nlpo3-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "nlpo3") (r "^1.2.0") (d #t) (k 0)))) (h "14jxm2qmpjlmbdy67dagbym57fbi7c4jb0y38m8syd06bc8av2w3")))

