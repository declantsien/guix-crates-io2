(define-module (crates-io nl s_ nls_term_grid) #:use-module (crates-io))

(define-public crate-nls_term_grid-0.1.0 (c (n "nls_term_grid") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "01j58blafz3fkqfm6p37j31zyxh99swvrib7iriik2sqv1d7y91b") (r "1.70.0")))

(define-public crate-nls_term_grid-0.1.1 (c (n "nls_term_grid") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1djvjkk7azz72drja3xnpv3331pxqy47792zr0kvql3kskjw4vxw") (r "1.70.0")))

(define-public crate-nls_term_grid-0.2.0 (c (n "nls_term_grid") (v "0.2.0") (d (list (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0zprwan97h2xk9wafhylxm8vl1p4lj1bk23a1mhs2sbcri0wja7v") (r "1.70.0")))

(define-public crate-nls_term_grid-0.3.0 (c (n "nls_term_grid") (v "0.3.0") (d (list (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "0hsn1nb9hg4js4jwfllajrp416nhn0w7yx2nbgi2z7dp1azi282k") (r "1.70.0")))

