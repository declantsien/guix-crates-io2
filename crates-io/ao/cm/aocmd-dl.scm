(define-module (crates-io ao cm aocmd-dl) #:use-module (crates-io))

(define-public crate-aocmd-dl-0.1.0 (c (n "aocmd-dl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "html2md") (r "^0.2.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "029wjx63ix2ajsjhhzhzb7jlfvckbfa042vr9yvw8ysrcb0kzc2w")))

