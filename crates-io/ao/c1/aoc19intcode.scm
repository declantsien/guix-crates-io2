(define-module (crates-io ao c1 aoc19intcode) #:use-module (crates-io))

(define-public crate-aoc19intcode-0.1.0 (c (n "aoc19intcode") (v "0.1.0") (h "1ij9lgczkpk490c00np9p2r3w3bffxhihnd3ddnp213sq52slskx")))

(define-public crate-aoc19intcode-0.1.1 (c (n "aoc19intcode") (v "0.1.1") (h "14ma0xmasi7s2cm13g7wp2nzpb22bdvj0yqxc6654v83w8xrsp2y")))

(define-public crate-aoc19intcode-0.1.2 (c (n "aoc19intcode") (v "0.1.2") (h "13p3z25rbvcgwzcwbsri4n2bxi36gr2vf2w1q4wd6wkjvbgvmq9s")))

(define-public crate-aoc19intcode-0.2.0 (c (n "aoc19intcode") (v "0.2.0") (h "1nwkl4wk8pyj8plq6z0cwyrdxwxfzbqgnlmz93ryda2mhdgl103b")))

