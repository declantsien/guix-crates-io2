(define-module (crates-io ao tt aott) #:use-module (crates-io))

(define-public crate-aott-0.1.0 (c (n "aott") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)))) (h "1l0ggbq9yxh5rniaf8957v7g64i4wic6acpg88a3palginx458a2") (f (quote (("std") ("default" "std" "builtin-text") ("builtin-text") ("builtin-bytes"))))))

(define-public crate-aott-0.2.0 (c (n "aott") (v "0.2.0") (d (list (d (n "aott_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)) (d (n "zstd-safe") (r "^6.0.6") (f (quote ("std"))) (d #t) (k 2)))) (h "0w2n7g9kwl6vjlfgbqpg0kc6qxqp122fwg4smnqrzgcxw14kjkw9") (f (quote (("std") ("error-recovery") ("default" "std" "builtin-text" "error-recovery") ("builtin-bytes")))) (s 2) (e (quote (("builtin-text" "dep:unicode-ident"))))))

