(define-module (crates-io ao tt aott_derive) #:use-module (crates-io))

(define-public crate-aott_derive-0.1.0 (c (n "aott_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzbrr7afkb3300a9xrw86cryii3gxib6ri77kfi60j539qk1mbl")))

(define-public crate-aott_derive-0.2.0 (c (n "aott_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yh1lhbpv3jk8g20yx8nxcxxsiqicffa7m9hb84h6c1spwdmx6n0")))

