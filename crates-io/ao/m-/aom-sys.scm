(define-module (crates-io ao m- aom-sys) #:use-module (crates-io))

(define-public crate-aom-sys-0.1.0 (c (n "aom-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.2") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0iwkyxls401fh54wvg9y7h259gpzicwd68i1g66mrv6vlx13ik01") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.1.1 (c (n "aom-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.48.0") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1sac364mpv1f8m625xx31s45lk07nbxihi5vg5hfdimlcpan150y") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.1.2 (c (n "aom-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0c9axb4nyqf902bjmbw2pbz3hiiz75prih682rfhzxzj481gh4lx") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.1.3 (c (n "aom-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0ix3djcf84kk53h6fac73n7jc614745n7kbmikxwi3s73b6vzgsr") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.1.4 (c (n "aom-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1bqcpkycv1d67r6jcl9npfbw6rkl829rdq9w6vlpb0rjqxp0xzsn") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.2.0 (c (n "aom-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "07bjz4bx0i17r1dgq6jiqmy485n841b7ghkkpkbg3llnazik6dpy") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.2.1 (c (n "aom-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "03a0xhaafjn0hlpcf9ba73hv557m0jqnmj9wl57wzrcnka96zvgj") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.2.2 (c (n "aom-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "1d4cqb6wijb164qfzxqc1g5v19a3cmy0dqx8lqmxza2fy37ji42b") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.3.0 (c (n "aom-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "system-deps") (r "^3.1") (d #t) (k 1)))) (h "0dhikfl7l5nacspajbllbhhysad3vl845cpfplqgm5mf67nmx9w8") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.3.1 (c (n "aom-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "0mlc3lixcv2kq64l51wr5f9fqzg1issks7z7k0wvb8z9m9rkp7v5") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.3.2 (c (n "aom-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "0swg90iwypakh7vq77zwh34238c1r7vd5smj0vza7dv7xa22wh0g") (f (quote (("build_sources"))))))

(define-public crate-aom-sys-0.3.3 (c (n "aom-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "0bc1dzl3c95s44q7c1i0vnj7fhiqf44in8w22nw5vmp1vgbpadk2") (f (quote (("build_sources"))))))

