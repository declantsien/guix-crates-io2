(define-module (crates-io ao cr aocrun) #:use-module (crates-io))

(define-public crate-aocrun-0.1.0 (c (n "aocrun") (v "0.1.0") (d (list (d (n "aocdata") (r "^0.1.0") (d #t) (k 0)) (d (n "aocsol") (r "^0.1.1") (f (quote ("solvers"))) (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0cy0s33a64fkvzrnvw9hk7np8bw9q9v62z9f1741hdkcqx6v524k")))

