(define-module (crates-io ao _r ao_rs) #:use-module (crates-io))

(define-public crate-ao_rs-0.1.0 (c (n "ao_rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bdxy0rzq38ysna2dzfvqm6xq2rjqf40r2idbsn09n1y6g2xadhp")))

(define-public crate-ao_rs-0.1.2 (c (n "ao_rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mnqv05rfckzakzvkjmhv1s0mxg834k2m1vbyh2rsbhy1d51naai")))

(define-public crate-ao_rs-0.1.3 (c (n "ao_rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ibv7hsv3i4gxw8qdf8apgvlspfng35wyljz1pjsa3s037qhajxl")))

(define-public crate-ao_rs-0.1.4 (c (n "ao_rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09z0zkk045mgykrqx3xmlkacx2zy3lp7wx99na2zq52193w4zlc5")))

(define-public crate-ao_rs-0.1.5 (c (n "ao_rs") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03rki4wmrzki32l10i9hl8c816zgamsvkr1b8mgmf45w0fh8z1p6")))

