(define-module (crates-io ao pt aopt) #:use-module (crates-io))

(define-public crate-aopt-0.3.1 (c (n "aopt") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1qw3qvv74k1nrbbsjh6b7gjxjy350sdi58w9q3629479vcv6xd57") (y #t)))

(define-public crate-aopt-0.3.3 (c (n "aopt") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0yssdmb1hm3mj267p64kzmf5xzsx4z4vmxjyzcc5j8z3dwq028z3") (y #t)))

(define-public crate-aopt-0.3.4 (c (n "aopt") (v "0.3.4") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1zsi2iryx9vvpbyp7qn5g2mn1irbm0x2fkzp45nx27i1f9kcjcjm") (y #t)))

(define-public crate-aopt-0.3.5 (c (n "aopt") (v "0.3.5") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0l0kbi7hk4c9zjh1ahgxq8jm5dxpm4grd9z4bbafryb8fsfmkq2r") (y #t)))

(define-public crate-aopt-0.3.6 (c (n "aopt") (v "0.3.6") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0i3d1hfcrbkjivmfzqhw07c2vjck7z4j1sx2hs9mq9l7bk76qhy9") (y #t)))

(define-public crate-aopt-0.4.0 (c (n "aopt") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "06w8kliwbnpdc0n7846hijzpamsppjcmsgfirrlhda5wc6zf6gwd") (y #t)))

(define-public crate-aopt-0.4.1 (c (n "aopt") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "07ba4krviacx5p5wsf9j965hnm06rhijdyb9qjl11vmkbwlzczds") (y #t)))

(define-public crate-aopt-0.4.2 (c (n "aopt") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1qld2q810pwribiqj050ks99aicaq582v6x64fjwhqhbb1cmsfwm") (y #t)))

(define-public crate-aopt-0.4.3 (c (n "aopt") (v "0.4.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "082jwk9w2m65dnkdcl088hrph97jr9znb9hjr9r26h1yc6l3y9lq") (y #t)))

(define-public crate-aopt-0.4.4 (c (n "aopt") (v "0.4.4") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1wc2pg59yzss26wvqrb9wf7xcqa6q2wj2znjssrrq6cl3n5bmxyj") (y #t)))

(define-public crate-aopt-0.5.2 (c (n "aopt") (v "0.5.2") (d (list (d (n "aopt-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "15v49npybvfvan9ylnsnr78s84sagfziqnj7vry0k8v6b4xpv5k4") (y #t)))

(define-public crate-aopt-0.5.3 (c (n "aopt") (v "0.5.3") (d (list (d (n "aopt-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1g8qbjizhfl2i95j8mf4wiq7rjpivnkz11syd47lv3nm53l1nkh8") (y #t)))

(define-public crate-aopt-0.5.4 (c (n "aopt") (v "0.5.4") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "02w9zb7ksqxa4013pzb1ggpd1n97f0gyz2zfrkidywkc0p8valy9") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.5.5 (c (n "aopt") (v "0.5.5") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1jx8bgyx8yf8n3lifnc7yk77768a7ch4cbh74fkjyryrjzrggvwx") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.5.6 (c (n "aopt") (v "0.5.6") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "01cfb75vz4a8wncbd7mi4kq6m6yhpzcs4vizirgds0g4n8d1qxi9") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.5.7 (c (n "aopt") (v "0.5.7") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1xg667djsb4i34ak92z8samvsiap7yr6436v9hbzqcbzxl3ldlib") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.0 (c (n "aopt") (v "0.6.0") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0bgaai8v7m3k1dabsqsbrcchq3l5lvf64z00nsxw7kk37ax9qchg") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.1 (c (n "aopt") (v "0.6.1") (d (list (d (n "aopt-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "07is1q435691p8gz556syw7b2j99lfx9kp86z2a89w1ph84kqwdh") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.2 (c (n "aopt") (v "0.6.2") (d (list (d (n "aopt-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0lacmk4ply7vvplmvjij2rf5fgk1iryfdxs7gflis6gccb4rbcmm") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.5 (c (n "aopt") (v "0.6.5") (d (list (d (n "aopt-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "073pivia7d44p09f73h3s5gx4brddh6g7g0f96l8blwkpay8srv4") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.6 (c (n "aopt") (v "0.6.6") (d (list (d (n "aopt-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "04w7llp92jwv25kzy7vyhv91wii18i8amwp4jbwqkrw0wi60rhhm") (f (quote (("sync")))) (y #t)))

(define-public crate-aopt-0.6.7 (c (n "aopt") (v "0.6.7") (d (list (d (n "aopt-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1gli2wpbppkqbci5aa0db8dxgjspdnja2w7x5mvn000q52lyifsb") (f (quote (("sync"))))))

(define-public crate-aopt-0.7.0 (c (n "aopt") (v "0.7.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "02s3b28gygkha6j6b1wxmflrwqs666jgjxqffcbvlh4v7kff6vwl") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.7.1 (c (n "aopt") (v "0.7.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "080z6q2xxfsvdk9fsll9nq1w1d4lhm9dyip70p9l72xa2ydwk02d") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.7.3 (c (n "aopt") (v "0.7.3") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "11cjawy9692xbh9jaid2253d37vn37qs0b3n2pkr0q7kxnzb6b5r") (f (quote (("utf8") ("sync"))))))

(define-public crate-aopt-0.8.0 (c (n "aopt") (v "0.8.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "17rm6xcn3500nm7fj1xlibziqpscnr8jh659bqrpbbxkikz35drs") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.8.1 (c (n "aopt") (v "0.8.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ij8y6gsl5kc9alf93z5j77fv1nb5xmmbajkisyd6zpfxn7vls97") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.8.2 (c (n "aopt") (v "0.8.2") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1a9p5sipwyz64r1gl00714qj0a1c947mz39gihkbxlhsqkyvlxdh") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.8.3 (c (n "aopt") (v "0.8.3") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0ridj38m8f155v207g4zvhv613jyq1likw18dl74rb13cw5csyj2") (f (quote (("utf8") ("sync")))) (y #t)))

(define-public crate-aopt-0.8.4 (c (n "aopt") (v "0.8.4") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1sv6bi223f66bibnvzqi2z7gxkmzw2klarar1r1d5l2bpkri2idv") (f (quote (("utf8") ("sync"))))))

(define-public crate-aopt-0.9.0 (c (n "aopt") (v "0.9.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "043yb0rijppczrn8gci5svrkr25lyf2p8las5i2xz6yav5b3v6z0") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing") ("default" "utf8")))) (y #t)))

(define-public crate-aopt-0.9.1 (c (n "aopt") (v "0.9.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1n6wsx2fq826qnss4rsfgac27mydnaci01wr3bvbqz4wpd1m13ka") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing") ("default" "utf8")))) (y #t)))

(define-public crate-aopt-0.9.11 (c (n "aopt") (v "0.9.11") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wma2dwvasfnrmxq72vz7sn229yj8jljhw6vjxhwwvpf42qcd98k") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.9.20 (c (n "aopt") (v "0.9.20") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1y17k0d5salvxf7bkxq5hqn0yhs7q7m38c8x824sr2cz9pj9m12h") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.9.21 (c (n "aopt") (v "0.9.21") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jfwjmfw9gkldbhgsjh7sr8kv8b7sql4sqgchvdw1x1l32mhlb49") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.9.22 (c (n "aopt") (v "0.9.22") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "18pilys1lxqz9nhk2mid1gyzbndhjxr61mqarwdqk11mshwmsmzl") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.9.23 (c (n "aopt") (v "0.9.23") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0aa6mliq80669ycq08s3c9jsbndb9cz95d8g6vgz67yazvh238fh") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing"))))))

(define-public crate-aopt-0.10.0 (c (n "aopt") (v "0.10.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cote") (r "^0.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06p0wzy2p4zc8hf1x2zvm4qggkiw1zhcs9pd148063alzaxrwhhn") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.10.1 (c (n "aopt") (v "0.10.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "cote") (r "^0.2.0") (d #t) (k 2)) (d (n "neure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "112iawmffr106mgj9l67rka2sx6b8hl6m86gcw3zg8bsg0piaj60") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.10.2 (c (n "aopt") (v "0.10.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "cote") (r "^0.2.0") (d #t) (k 2)) (d (n "neure") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "115ym5pl7z4wkk003zg73sci9v2128742aqbpqp3fdxcxb5p0l1p") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.10.3 (c (n "aopt") (v "0.10.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "cote") (r "^0.2") (d #t) (k 2)) (d (n "neure") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1d3745n3bf7sl8mrl57bz7yc9ivavk2fa36ysv2lsay706jl2z7x") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.10.4 (c (n "aopt") (v "0.10.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "cote") (r "^0.2") (d #t) (k 2)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0caiz4yh8plsfa44a26g5jrlhgdzg3w62wf336rdzzj945icm33h") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing"))))))

(define-public crate-aopt-0.11.0 (c (n "aopt") (v "0.11.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "150vkw5mvnx42rak1ngz6gp3iqkk5nz6c0h4aa90lrswa975lhdy") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.11.1 (c (n "aopt") (v "0.11.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15sq8fd172law2l2k87nn9ljg0vblbxvccq3lwp39vl1cjvvxh1l") (f (quote (("utf8") ("sync") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.11.3 (c (n "aopt") (v "0.11.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qiar9czxi0x98pn2mqv59aicqwcfmyhwh48bj7sq850m6kcfack") (f (quote (("utf8") ("sync") ("shell") ("serde" "serde/derive") ("log" "tracing")))) (y #t)))

(define-public crate-aopt-0.11.5 (c (n "aopt") (v "0.11.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "11w9ych4kaai91jw62jd0g947hmmqwga14k4l7ksichivi8z0wnx") (f (quote (("utf8") ("sync") ("shell") ("serde" "serde/derive") ("log" "tracing"))))))

(define-public crate-aopt-0.12.0 (c (n "aopt") (v "0.12.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13446c8azm3x6iifw1hd31masnahwd1034ydrj2rcv7fvpk3m882") (f (quote (("sync") ("shell") ("serde" "serde/derive") ("log" "tracing") ("default"))))))

(define-public crate-aopt-0.12.1 (c (n "aopt") (v "0.12.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0r77rvp2ny8z0mm6yysw99d1056jxas0rywfdi5rbknsn9b2rw5q") (f (quote (("sync") ("shell") ("serde" "serde/derive") ("log" "tracing") ("default"))))))

(define-public crate-aopt-0.12.2 (c (n "aopt") (v "0.12.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "neure") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "16r0iimr821dxxb72b14rqkhsvi6c2089bs8wd42n6v8rjrpswz6") (f (quote (("sync") ("shell") ("serde" "serde/derive") ("log" "tracing") ("default"))))))

