(define-module (crates-io ao pt aopt-macro) #:use-module (crates-io))

(define-public crate-aopt-macro-0.0.2 (c (n "aopt-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qc74mavzasnasikzkg13isa8d1cjb2ck86ay0zxsj45j8p3yad4") (y #t)))

(define-public crate-aopt-macro-0.0.3 (c (n "aopt-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0b2c4r36hl88jb4ig5s0i5wynn06xvxk5g4dgbbadp93sxcgq3n1") (y #t)))

(define-public crate-aopt-macro-0.1.0 (c (n "aopt-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qd24dmfnzd4srlnndmngmqljzs62lv7x6d7wrjzrxcdjs8dcwi6") (y #t)))

(define-public crate-aopt-macro-0.1.2 (c (n "aopt-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xz1c5ldqcq3zlllkqlvwkw30dwr2jvphszkxcp3rc4cqm8fbaii") (y #t)))

