(define-module (crates-io ao pt aopt-help) #:use-module (crates-io))

(define-public crate-aopt-help-0.2.0 (c (n "aopt-help") (v "0.2.0") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0qilgp6ghfgqawfn8j54418qkja3s7mdj1cll9xfprrjlpg8fxdm")))

(define-public crate-aopt-help-0.2.1 (c (n "aopt-help") (v "0.2.1") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "17gg0xldi8hda4w7iibk57lflgl9sm6mw2csg13q2gp42z2c51jf")))

(define-public crate-aopt-help-0.2.2 (c (n "aopt-help") (v "0.2.2") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0qpsf41fl0hn4yizdgf7dnc86q714srm86cy9l8a1dzsjjw7522s") (y #t)))

(define-public crate-aopt-help-0.2.3 (c (n "aopt-help") (v "0.2.3") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1w8gis7zk09y63wvviac7h9ian1i8wifryws2pxrkniknrihz45z")))

(define-public crate-aopt-help-0.2.4 (c (n "aopt-help") (v "0.2.4") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1dksh1wawww9mk4f25736kkllracf2jaizhb3k12g5j462fsm3wi")))

(define-public crate-aopt-help-0.2.5 (c (n "aopt-help") (v "0.2.5") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "0szqr9l3wmk0gpgwsj14dcnj97r9yq9fzznmn2dxq2d023x4rins")))

(define-public crate-aopt-help-0.2.6 (c (n "aopt-help") (v "0.2.6") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1gflsy2f1gafv6wcck4m2l7p0qrn0ymn2x22fv43iwp3zc87ivxl")))

(define-public crate-aopt-help-0.2.8 (c (n "aopt-help") (v "0.2.8") (d (list (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ustr") (r "^0.8.1") (d #t) (k 0)))) (h "1zhxnbiinns2fgl60xx46x2xygxvkddc14sk556mvj4bqb9mvkd2")))

(define-public crate-aopt-help-0.3.0 (c (n "aopt-help") (v "0.3.0") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wn8zkkl9i673v24rmi5ycwfzw9m2818wyfwv45fbvdjqzkcn8vy")))

(define-public crate-aopt-help-0.3.1 (c (n "aopt-help") (v "0.3.1") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1b9swf1w1gj20wql0f4nx783fjpfbdizwdl2shlb1pnnvzgf74ai")))

(define-public crate-aopt-help-0.3.2 (c (n "aopt-help") (v "0.3.2") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fifb8qaj1fzflfhx1c8kpkh79yhrp60fkbdwg66aj75j99p937h")))

(define-public crate-aopt-help-0.3.3 (c (n "aopt-help") (v "0.3.3") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qxydck8d8gzxv44js50pgi7wxc16zxpmk7f4ywp2f42aprmzx8d")))

(define-public crate-aopt-help-0.3.4 (c (n "aopt-help") (v "0.3.4") (d (list (d (n "textwrap") (r "^0.16") (f (quote ("unicode-width" "smawk"))) (k 0)))) (h "1adhbzkqk1z3452giid98ax30w9gyfs9wpgc674mmakm9mh2mrhy")))

