(define-module (crates-io ao mm aommap) #:use-module (crates-io))

(define-public crate-aommap-0.0.0 (c (n "aommap") (v "0.0.0") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0crkyq34jcnqxpf1bpyg4hg4q4qd1cxd4574h1xdfrnmr2n2zm35") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.0 (c (n "aommap") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "166yhbml8s69szhyb6s2clw6fjari6dn1h5q3zslx4yzm2pq3zfz") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.1 (c (n "aommap") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1vy5s9msr1cg6r6j9zvqcv4xdhrq4wml9f2094gi1xw6d2p2z71l") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.2 (c (n "aommap") (v "0.1.2") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "14cswrwxpi8cln0gviv5vr08nw7kh2jk46waqwcdgkz6j85cn308") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.3 (c (n "aommap") (v "0.1.3") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "197yp8h3iqa2xl2c1crxx8kh4sd13lyz94jagjhiz80mk6j2v87b") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.4 (c (n "aommap") (v "0.1.4") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0i223whrdr0fv8jpniadx2xvabzgd9wwg7fcbz7l3c8ypgzshx02") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.5 (c (n "aommap") (v "0.1.5") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0xah95aqwcdpr966v8xc8a1a3n2791flimn3xvivn902d7h5nsa3") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1.6 (c (n "aommap") (v "0.1.6") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0p8m0wn5qm4w3vvgzc1vswr4f2dn106hlgmvymcm9vx96bm074wn") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.2.0 (c (n "aommap") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "09kjam6nvizwk5fpyms1v24c3wnwcllcy17d5v4qli64508vcmvg") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.2.1 (c (n "aommap") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "052idwz1i4ppzd47jg03pc54wxvzmb36s87imwpm7h4qk0dxmyfy") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.3.0 (c (n "aommap") (v "0.3.0") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0p2irrf07vh52h0rbyvyz7f8i5jq8z7dvqck8l1546njqkzn4nic") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.3.1 (c (n "aommap") (v "0.3.1") (d (list (d (n "bytes") (r "^1.4") (k 0)) (d (n "memmapix") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0qy1zx8d6jkmg2ykhcrq48wj1rqg73ga0lbh838wc0cpf51mn0f1") (f (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

