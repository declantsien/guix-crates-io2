(define-module (crates-io ao c_ aoc_utils_by_nifalu) #:use-module (crates-io))

(define-public crate-aoc_utils_by_nifalu-0.1.0 (c (n "aoc_utils_by_nifalu") (v "0.1.0") (h "1japw0yvzmnyscc05qvfwabjg1zmngbp5gqnmwzw4icw47hihbbf")))

(define-public crate-aoc_utils_by_nifalu-0.1.1 (c (n "aoc_utils_by_nifalu") (v "0.1.1") (h "0m64r86qrlxsdhnm890krfd1yk878m7b6cf6hs3rc1f9cwy37d3a")))

(define-public crate-aoc_utils_by_nifalu-0.1.2 (c (n "aoc_utils_by_nifalu") (v "0.1.2") (h "0sbsnafzff2yvg0jvl672nr792iy03aaygp0xi1ymvwm86c8qmzr")))

(define-public crate-aoc_utils_by_nifalu-0.1.3 (c (n "aoc_utils_by_nifalu") (v "0.1.3") (h "1zxjwj7vj8ms554xcajxh3p0xfzqn6c0sgvvslay0886hhyai7vf")))

(define-public crate-aoc_utils_by_nifalu-0.1.4 (c (n "aoc_utils_by_nifalu") (v "0.1.4") (h "0rz4xrw70pn7mwc69vpq2i8qiy1b2yxjkiqbva3i2zzvky197wyx")))

(define-public crate-aoc_utils_by_nifalu-0.1.5 (c (n "aoc_utils_by_nifalu") (v "0.1.5") (h "0l7qp79nsa7x0sq9fc45cdcwc3phqc5n0lg6lna2aczfg5p1z63x")))

(define-public crate-aoc_utils_by_nifalu-0.1.6 (c (n "aoc_utils_by_nifalu") (v "0.1.6") (h "0x5jslv133ynyf4jwmfis9l5rhn6638nlhsr8vkl9x7mvanwx64c")))

(define-public crate-aoc_utils_by_nifalu-0.1.7 (c (n "aoc_utils_by_nifalu") (v "0.1.7") (h "1bar6izy5c4ywyg78c02fqh30q9f7ynrfi5maw3mgspb1n3xj5ik")))

