(define-module (crates-io ao c_ aoc_driver) #:use-module (crates-io))

(define-public crate-aoc_driver-0.1.0 (c (n "aoc_driver") (v "0.1.0") (d (list (d (n "aoc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1dmax9v2hmgcpqzkykr7vs4w3wjfkfcl4yla27bav9gxrfbx6vmc")))

(define-public crate-aoc_driver-0.1.1 (c (n "aoc_driver") (v "0.1.1") (d (list (d (n "aoc_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0bf1s5gm5x58blb6rqnrsnbq5wr7fb6b4szfdpy1r1b1wfkcghxk")))

(define-public crate-aoc_driver-0.1.2 (c (n "aoc_driver") (v "0.1.2") (d (list (d (n "aoc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0611fiydrr8alnwsd4gzxlw46ya3rq92jlpscam4am3pgq4a4iph")))

(define-public crate-aoc_driver-0.1.3 (c (n "aoc_driver") (v "0.1.3") (d (list (d (n "aoc_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0r0h5nn8f5wb6g1h8rmb7k098kw2nlnqi2md12x01f9vqjvwn4bh")))

(define-public crate-aoc_driver-0.1.4 (c (n "aoc_driver") (v "0.1.4") (d (list (d (n "aoc_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0ag7lf4sfk0i6x15mjyl7phc9m1il9l7j61j2ydn8w51ih4gvqsd")))

(define-public crate-aoc_driver-0.1.5 (c (n "aoc_driver") (v "0.1.5") (d (list (d (n "aoc_macros") (r "^0.1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1l9n9kfwx1sbg23g8plmq273s394ivdxbprj1b9chinxrn9h4m2w")))

(define-public crate-aoc_driver-0.1.6 (c (n "aoc_driver") (v "0.1.6") (d (list (d (n "aoc_macros") (r "^0.1.6") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "13n6xk5m6i5jyg5f26rwy61rjpmi7kx89jdr3ibhbjafmidb1k01")))

(define-public crate-aoc_driver-0.1.7 (c (n "aoc_driver") (v "0.1.7") (d (list (d (n "aoc_macros") (r "^0.1.7") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0fxsqdrigglj2pcb8dlay1vrcy2ldp4qmzqvipr9j94jqd00dha9")))

(define-public crate-aoc_driver-0.1.8 (c (n "aoc_driver") (v "0.1.8") (d (list (d (n "aoc_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1xiz51bas0s8n4a54498rbi31ka20v0przfmkbv1anrhvxayn2gq")))

(define-public crate-aoc_driver-0.1.9 (c (n "aoc_driver") (v "0.1.9") (d (list (d (n "aoc_macros") (r "^0.1.9") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0pdsy99hp9sckbqc18psfgh4bp019pqqb16bkb2zz9hgzm6sa6f9")))

(define-public crate-aoc_driver-0.2.0 (c (n "aoc_driver") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0hc0jb19icic6nn7wxj5n4nlvkxa9sjdkxwxaky2sdq9z33d3fbx")))

(define-public crate-aoc_driver-0.2.1 (c (n "aoc_driver") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "06w2ypwg8d60gr6kgbhyib02jmpg1g1n8g4r0d582mzjvm3fghkr")))

(define-public crate-aoc_driver-0.2.2 (c (n "aoc_driver") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "04l8slawcl5c1s99jyplp76l4h1gm87617r5sykmvi8r9jlzpnn9")))

(define-public crate-aoc_driver-0.2.3 (c (n "aoc_driver") (v "0.2.3") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1qxzyqjk6q07fyf3vmpsiw0ppb64bkd43ji00nq7pms4b2gvx6i8")))

(define-public crate-aoc_driver-0.2.4 (c (n "aoc_driver") (v "0.2.4") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0ln4bbh36nlqyvbc71x6237xysjv7a3gn7xhp195mwpc1pqjfk6i")))

(define-public crate-aoc_driver-0.2.5 (c (n "aoc_driver") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "109mzhm5231d148nb2nwk8avjgc7bqhndjvhz1fkbhfg4q5xcabi") (f (quote (("local_cache" "serde" "serde_json" "chrono")))) (y #t)))

(define-public crate-aoc_driver-0.3.0 (c (n "aoc_driver") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "09dajrkl0gs0i72vj6znp3vm8cmk7x3l9cp7vay2ikr2xahm9al5") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3.1 (c (n "aoc_driver") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0apys58hbmsjis5zzd2x1ph4l8z27b1ya223k9ccpfxzjlxcaphd") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache")))) (y #t)))

(define-public crate-aoc_driver-0.3.2 (c (n "aoc_driver") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1d75jsg0yinqf6pjfd3ri2fm1n6izmh54gm12fjbqkh4rs4glnls") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3.3 (c (n "aoc_driver") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0kz09f9jscx2h3lzjcpirgsrhfmk1ml581dvnbbz8dzbv85wy129") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3.4 (c (n "aoc_driver") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1gh89vnmahyxzi6hpflmg6dsxcgcrqqwvkdism1v5j21p1wqnmhc") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3.5 (c (n "aoc_driver") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "16fd08mhpxb0s6lh9fhh7pzk4ag9qdpc0lc3w2hs30mrsv4gd8vf") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3.6 (c (n "aoc_driver") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "std" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "168hpk3sz8lcns730zlgwxmcs529iha960lpvq8d7fymkppmp8bh") (f (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

