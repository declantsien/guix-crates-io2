(define-module (crates-io ao c_ aoc_macros) #:use-module (crates-io))

(define-public crate-aoc_macros-0.1.0 (c (n "aoc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0kk6vxlp0n3w244najx1xkw8qbhamjnbzgag0yg98l6c24zx1ryr")))

(define-public crate-aoc_macros-0.1.1 (c (n "aoc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0j492z6z1mix89z0wchmynkalkc7dc7p92s7pm7x30bcg9z7zkh1")))

(define-public crate-aoc_macros-0.1.2 (c (n "aoc_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11v5wicxx2c4yz9q73wk8d52z7v2wisvw57gmsgx14kv8lpfqf8a")))

(define-public crate-aoc_macros-0.1.3 (c (n "aoc_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00a7xqmr5gkq7v21sn7p06839p75l0q2qqbmmnmnrvjp0dljdvpm")))

(define-public crate-aoc_macros-0.1.4 (c (n "aoc_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zjilsxsx2i8zjlrcgya4w8hkmn9zbwrkwr3h7ypibh4w68wzzi5")))

(define-public crate-aoc_macros-0.1.5 (c (n "aoc_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cqxbyll02l1dzqsfv5ix6411d46dc9kkryf9b1c3z0b14g809il")))

(define-public crate-aoc_macros-0.1.6 (c (n "aoc_macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fcqmqlyxpldiij47iybbkp9nw42ki0ydg9fgmiw1ksdqlvz5rr2")))

(define-public crate-aoc_macros-0.1.7 (c (n "aoc_macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05jg2k9yip0ks2r948idaggh0hmnc9brj51p9727g0ra9b6y7jgi")))

(define-public crate-aoc_macros-0.1.8 (c (n "aoc_macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dv570idmrj8sjsybh2g2c7a2va1p0xkxf42x86g5j7213zc3h9n")))

(define-public crate-aoc_macros-0.1.9 (c (n "aoc_macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hp6m7s1j0j6mxnrf1kn04bqx7j2r809gwpv296gvmpl71jmcqww")))

