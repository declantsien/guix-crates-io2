(define-module (crates-io ao c_ aoc_input) #:use-module (crates-io))

(define-public crate-aoc_input-0.1.0 (c (n "aoc_input") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0c63r2k57sivbica2yf627rbp23l98wl241zas8gf3gldpa2gjvb") (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2.0 (c (n "aoc_input") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0qxi9bnsvjc7kpabq6m6l4azqhcwxlwm6w2dxywpd5n3hyjb7hdz") (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2.1 (c (n "aoc_input") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)))) (h "044fsn636wachcg31dy0bj7v3raqcnpzvrq3cz6ijmb4hd6ddmp1") (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2.2 (c (n "aoc_input") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04i7dya466h9v8cc8mnddgdaz4sjxz7d9apd2j619sf40cn9y4c2") (s 2) (e (quote (("cli" "dep:clap"))))))

