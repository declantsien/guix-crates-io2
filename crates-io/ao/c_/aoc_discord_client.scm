(define-module (crates-io ao c_ aoc_discord_client) #:use-module (crates-io))

(define-public crate-aoc_discord_client-0.1.0 (c (n "aoc_discord_client") (v "0.1.0") (d (list (d (n "aoc_data") (r "^0.1.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9") (f (quote ("client" "gateway" "model" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "04i1kwgzvrs49mcnbap83sdwsnyv3n8kg9sbcrkj7lz01sglsbnr") (y #t)))

(define-public crate-aoc_discord_client-0.1.1 (c (n "aoc_discord_client") (v "0.1.1") (d (list (d (n "aoc_data") (r "^0.1.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9") (f (quote ("client" "gateway" "model" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "1lkhxf2a8yh79mq6lrlwd61xsp4kgfkbl0q32zg3rcivf0469lxy") (y #t)))

