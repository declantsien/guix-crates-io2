(define-module (crates-io ao c_ aoc_2018_state_machine) #:use-module (crates-io))

(define-public crate-aoc_2018_state_machine-0.1.0 (c (n "aoc_2018_state_machine") (v "0.1.0") (h "1h26zb2s0n8f5nwxkmicdacb3wr8xalk65j0r7632fsjnrz41gxw")))

(define-public crate-aoc_2018_state_machine-0.1.1 (c (n "aoc_2018_state_machine") (v "0.1.1") (h "0ivagkjr9jy1c48vv88wznf2xwmgs88gvlwpi8naml87lnlxh26a")))

