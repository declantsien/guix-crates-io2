(define-module (crates-io ao c_ aoc_codegen) #:use-module (crates-io))

(define-public crate-aoc_codegen-0.1.0 (c (n "aoc_codegen") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8hx2xvhid85v0f2kh1r00qnrc7kbrw3a8jrwhksa7k6f2sbyy1")))

(define-public crate-aoc_codegen-0.2.0 (c (n "aoc_codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "1qyfc4s9j4bp8y1r7bkk04fipzjhx8pp6m2hgrccq51dv5l3rzcz")))

(define-public crate-aoc_codegen-0.6.1 (c (n "aoc_codegen") (v "0.6.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "19x9m2vhyhkhngj1xv10f206p92zj28jjdiid7js5sj5vx44mx0c")))

