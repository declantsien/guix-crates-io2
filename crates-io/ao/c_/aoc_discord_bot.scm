(define-module (crates-io ao c_ aoc_discord_bot) #:use-module (crates-io))

(define-public crate-aoc_discord_bot-0.1.0 (c (n "aoc_discord_bot") (v "0.1.0") (d (list (d (n "aoc_data") (r "^0.1.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9") (f (quote ("client" "gateway" "model" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "19b2imf4mh880ndpxmlqfspi0fv3h9ijbyxmh1wgjcsmrcfclkqh")))

