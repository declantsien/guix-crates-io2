(define-module (crates-io ao c_ aoc_data) #:use-module (crates-io))

(define-public crate-aoc_data-0.1.0 (c (n "aoc_data") (v "0.1.0") (d (list (d (n "derive_more") (r ">=0.99") (d #t) (k 0)) (d (n "prettytable-rs") (r ">=0.8") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r ">=1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1") (d #t) (k 0)) (d (n "serenity") (r ">=0.9") (f (quote ("client" "gateway" "model" "rustls_backend"))) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)))) (h "1kd5xs1ccl4d5xj61czkrr9lsdbig5xqk9lhr8f47l8r41h1mf1i")))

