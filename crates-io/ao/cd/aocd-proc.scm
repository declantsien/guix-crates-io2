(define-module (crates-io ao cd aocd-proc) #:use-module (crates-io))

(define-public crate-aocd-proc-0.2.0 (c (n "aocd-proc") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "1flcaa9j70vbi7irzfkv4gq1hssym3v8vc79sgvlkjzam4qi2c6d")))

(define-public crate-aocd-proc-0.2.1 (c (n "aocd-proc") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "1zpxqg0w3ldk0dyx3m274w7483861cpl85rdbdvazj7f0lcd03jn")))

(define-public crate-aocd-proc-0.3.0 (c (n "aocd-proc") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "1z1kbpbid1im7qgqlff0hhf0kcmcgihq7am2d04abn5i0lank4m2")))

(define-public crate-aocd-proc-0.3.1 (c (n "aocd-proc") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "1gi8hhn6wvgfab4mr2552cx8g8ax0kdkdyi3zlp5bqwy5gjjhqw5")))

(define-public crate-aocd-proc-0.4.0 (c (n "aocd-proc") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1qj3dnf51s2cndsszx5rv1v8w1xvyl3fp7vv60gkq8v767h727i3")))

