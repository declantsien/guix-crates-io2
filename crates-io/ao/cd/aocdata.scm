(define-module (crates-io ao cd aocdata) #:use-module (crates-io))

(define-public crate-aocdata-0.1.0 (c (n "aocdata") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "sea-orm") (r "^0.11.3") (f (quote ("sqlx-postgres" "runtime-tokio-rustls" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0fcqp4bkgb5hfj4wfaj15p4s220jw1wiw3d7pzcf97rdf9zv3jzn")))

