(define-module (crates-io ao sp aosp-missing-blobs) #:use-module (crates-io))

(define-public crate-aosp-missing-blobs-0.4.0 (c (n "aosp-missing-blobs") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "1zv8sdbknlpkzgdn97a00xz3ap1y727c4mslaw8w8rfjwx98h4mh")))

(define-public crate-aosp-missing-blobs-0.5.0 (c (n "aosp-missing-blobs") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "1hxky0j3kjzcw7h98ink7bn4qhhm8mrnyhha2fajmwnck5fpqjfw")))

