(define-module (crates-io ao s- aos-specs) #:use-module (crates-io))

(define-public crate-aos-specs-0.1.0 (c (n "aos-specs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "04pa696h3wjyy6hf6rzyh06yy820vd8hkirsv3a57jhl1gdxam6g") (y #t)))

(define-public crate-aos-specs-0.1.1 (c (n "aos-specs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1fhqpz6qjv1k50mmsm5vlgskcy6hn5fgldx4vr4r6vb8g2xdfgz3") (y #t)))

