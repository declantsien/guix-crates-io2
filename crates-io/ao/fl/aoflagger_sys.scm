(define-module (crates-io ao fl aoflagger_sys) #:use-module (crates-io))

(define-public crate-aoflagger_sys-0.1.0 (c (n "aoflagger_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0d5kj3r5ifbkal63dy6azkgs8mpwm1szlm6y93gwifxy99wn5wk1")))

(define-public crate-aoflagger_sys-0.1.1 (c (n "aoflagger_sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.54") (d #t) (k 1)))) (h "1z5axgq7w4iyfkx6ymmccd7pg89zv06r2i0gkkxvl4rx5z4d077q") (l "aoflagger")))

