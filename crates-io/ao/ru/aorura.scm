(define-module (crates-io ao ru aorura) #:use-module (crates-io))

(define-public crate-aorura-0.1.0 (c (n "aorura") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0bgqcljrm2v8dxhbnbg8cddwrv2rscjbriqz4ibfhajb49y4knbz")))

