(define-module (crates-io ao bs aobscan-cli) #:use-module (crates-io))

(define-public crate-aobscan-cli-1.0.1 (c (n "aobscan-cli") (v "1.0.1") (d (list (d (n "aobscan") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1slfxh8lal21ni9v6w0xmbjndgn22bajms82rwn45nph1bw8p8ir")))

