(define-module (crates-io ao bs aobscan) #:use-module (crates-io))

(define-public crate-aobscan-0.1.0 (c (n "aobscan") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09a8xfm780nh6b00v1n3vszrkm4ig38ki2frahccgg2mb8mdgld3") (y #t)))

(define-public crate-aobscan-0.1.1 (c (n "aobscan") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02nci9vhk4sndlih6mslz7nd7q105mhm30y0ag18kgnmdmar32sv")))

(define-public crate-aobscan-0.1.2 (c (n "aobscan") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "16hllm16i5r03mcxl2z56hpwazn9qcvchz04y6ssjjwnas6hsks1")))

(define-public crate-aobscan-0.1.3 (c (n "aobscan") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0i9zbqw2za9hhkvhsqfrf8vf8zdmzg08wa7v2ni40mqchql9762g") (y #t)))

(define-public crate-aobscan-0.1.4 (c (n "aobscan") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0r5pcmc5r9h8vr40ln2barx1092dw3zg1hk0m3hz9m6si7k23abk")))

(define-public crate-aobscan-0.1.5 (c (n "aobscan") (v "0.1.5") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dlcw260y6xdsmd9g0i7yq77bpm4i6nispxa6jxgdw4a9gvy8cfg")))

(define-public crate-aobscan-0.1.6 (c (n "aobscan") (v "0.1.6") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0adkvjhv39r7rwirf2xny7byzdsi4fp7cimh3whyg968hwyyvpd8")))

(define-public crate-aobscan-0.1.7 (c (n "aobscan") (v "0.1.7") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "object") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1q6jyd8qxz30bsz7wcgmap8bhfqk7smdzsmagpv803z8dy8c9j8v")))

(define-public crate-aobscan-0.2.0 (c (n "aobscan") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "object") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06wnv7flin1w2qdcnjh4n9ccj0xcjqxw3n44ai9id37w3slj5km3")))

(define-public crate-aobscan-0.3.0 (c (n "aobscan") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.14") (d #t) (k 0)) (d (n "object") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0wnbaqlf3i3glsxz2yiyl05bbm773c6a3qljvbh589nx3c6j9s8d") (f (quote (("default" "object-scan")))) (s 2) (e (quote (("object-scan" "dep:object"))))))

