(define-module (crates-io ao cl aoclib) #:use-module (crates-io))

(define-public crate-aoclib-0.1.0 (c (n "aoclib") (v "0.1.0") (h "16b5yh3n52jbn592fk89w2yibxrw49zvsg4dpasc6hh7qcsxkhpj")))

(define-public crate-aoclib-0.2.0 (c (n "aoclib") (v "0.2.0") (h "0razm9falidgspwwxc25fpaaa3k42crcbmfiisfrmjqy8g2s6qa9")))

(define-public crate-aoclib-0.2.1 (c (n "aoclib") (v "0.2.1") (h "19vicrjwm2ja035kf7yf1kx806lmffqs54vbkx1gd51rilh431ly")))

