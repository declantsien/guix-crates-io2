(define-module (crates-io ao cl aocli) #:use-module (crates-io))

(define-public crate-aocli-0.0.1 (c (n "aocli") (v "0.0.1") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "1blsb042id4r64c31d0vjiw9ivbxims61v2792603i3i93bbji3b") (y #t)))

(define-public crate-aocli-0.0.2 (c (n "aocli") (v "0.0.2") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "0a20hncqxia9395pfrldlchg9dby4ci1g3kaq0jwiamd12x2ahha")))

(define-public crate-aocli-0.0.3 (c (n "aocli") (v "0.0.3") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "0f7nysnnz1llsl31anzcsyxyyz63j043dpa86b1x12za4rhqrrkd")))

(define-public crate-aocli-0.0.4 (c (n "aocli") (v "0.0.4") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "09ml7084052mn852cgd4ayhfjlcq862rmpi1js5qfw3piwxp3cj3")))

(define-public crate-aocli-0.0.5 (c (n "aocli") (v "0.0.5") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "05ghmrf2n362ycz092cb1mfy51dhqq99k9lqfd2kc57r5bpvzrpd")))

(define-public crate-aocli-0.0.6 (c (n "aocli") (v "0.0.6") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "1x698dss7mlh04ihv53l28fjb3dfvgpdml1gsq5arypcry427fpa")))

(define-public crate-aocli-0.0.7 (c (n "aocli") (v "0.0.7") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "1xapf9w18x190lpfn9i6x86ldhsalfxl3cp8095d613h9arfnm1x")))

(define-public crate-aocli-0.0.8 (c (n "aocli") (v "0.0.8") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "13xw62v1z30jwkcadjkmrypqb5a9j7mzqwa6qh0zgsi646a69c9f")))

(define-public crate-aocli-0.0.9 (c (n "aocli") (v "0.0.9") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "01f6b6isirk0if1v01rc3blwasqga4h1y2bz00fxgar9pzia81wm")))

