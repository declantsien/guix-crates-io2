(define-module (crates-io ao c2 aoc2023) #:use-module (crates-io))

(define-public crate-aoc2023-0.1.0 (c (n "aoc2023") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0hbb11fnmxi9wqz3l3gn3rhig9plgrgxx396s65wjjx9la8k3yb7")))

