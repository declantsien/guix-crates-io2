(define-module (crates-io ao c2 aoc23_parser) #:use-module (crates-io))

(define-public crate-aoc23_parser-0.1.0 (c (n "aoc23_parser") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)))) (h "152pkfb5zgxn6piz9w84lr3hqv8dn4s16y0335pxrgmzqan09ayh")))

(define-public crate-aoc23_parser-0.1.1 (c (n "aoc23_parser") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0w4998wbqkwrmwqvchbcvbhdbfgwm0p19aqiq909mp0172hmw5bw")))

