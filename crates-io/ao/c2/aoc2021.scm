(define-module (crates-io ao c2 aoc2021) #:use-module (crates-io))

(define-public crate-aoc2021-0.1.1 (c (n "aoc2021") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a88sx5a5qbg03bzks6pqcw12ad0v8cwig2g9clbwgiszysjnw9g")))

(define-public crate-aoc2021-0.1.2 (c (n "aoc2021") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wzzfwk655y1nv45if6z5690jcg7d8z1nzzfkbdc7n8gljsjyv6q")))

(define-public crate-aoc2021-0.2.0 (c (n "aoc2021") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1i9q221yzyzr16y8yrr88lzh9lq13vcd0ay2y9wq765604cdz78s")))

(define-public crate-aoc2021-0.3.0 (c (n "aoc2021") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dcxnq5jgjf6rsmjpkffbfgpdr95nh2bc607y1wl1ha498nr4735")))

