(define-module (crates-io ao nt aont) #:use-module (crates-io))

(define-public crate-aont-0.1.0 (c (n "aont") (v "0.1.0") (h "1rpxb83lqrbh956plyi09cr65ndh0f1vlx76pkl415aya0fz2gdd")))

(define-public crate-aont-0.1.1 (c (n "aont") (v "0.1.1") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)))) (h "1x4f6wyvz1zwq0pkkh75cr8cbwhnryw53ikzwpy8ng2xw9wgf9bw")))

