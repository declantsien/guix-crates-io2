(define-module (crates-io ao sa aosa) #:use-module (crates-io))

(define-public crate-aosa-0.1.0 (c (n "aosa") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c7iqb8h4pxl2bhg87l3p30bbily1p0yg2v35kca46s812imxb2q")))

(define-public crate-aosa-0.1.1 (c (n "aosa") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f93c6xsjlbbi8cbrqivljvfn0ng9c0gmp56jkghszswg8dj3r0y")))

