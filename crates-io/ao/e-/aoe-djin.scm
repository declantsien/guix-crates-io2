(define-module (crates-io ao e- aoe-djin) #:use-module (crates-io))

(define-public crate-aoe-djin-0.1.0 (c (n "aoe-djin") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "03gxkjs7n932w51l95yvmyd19krlvm8qpj6ysqk6cavn0j3llxyh")))

(define-public crate-aoe-djin-0.2.0 (c (n "aoe-djin") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1kip4g6lhlq6mhgp6dm13sx2lnh2ywjyabzb0m2viazhzz2jnvn6")))

(define-public crate-aoe-djin-0.2.1 (c (n "aoe-djin") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0gna5flkq5pj5sc3v5b0cw663q2ysb49nkpgmpd6fv5dqh559adz")))

(define-public crate-aoe-djin-0.3.0 (c (n "aoe-djin") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1mrh1javw88r906n2hggyr6gxnyjr1c7c68ipcnngid770j7zqac")))

(define-public crate-aoe-djin-0.3.1 (c (n "aoe-djin") (v "0.3.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0k31ishsrv647j7yybdpddxjlhm11id525i4zqjl5xschcwa5y0l")))

(define-public crate-aoe-djin-0.3.2 (c (n "aoe-djin") (v "0.3.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "djin-protocol") (r "^3") (d #t) (k 0)) (d (n "djin-protocol-derive") (r "^3") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1ml16vf2mi58180mx1m50hrswpj123ik4mg336c0gpp2gjr69x2h")))

