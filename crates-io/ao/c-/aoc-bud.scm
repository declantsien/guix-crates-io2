(define-module (crates-io ao c- aoc-bud) #:use-module (crates-io))

(define-public crate-aoc-bud-0.0.1 (c (n "aoc-bud") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04lm98dp6q8wn8riis5j2jz99w1px6jmb4zj95xjacg633b2aq0d")))

(define-public crate-aoc-bud-0.0.2 (c (n "aoc-bud") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "08navzgsrrg99jqvkf6a1q7bhgv82vaklix99njfawmjx9x1n1jm")))

(define-public crate-aoc-bud-0.0.3 (c (n "aoc-bud") (v "0.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "06b4v7khr0yg1n38z5dnhl09bwj91km1jmyw3zbgxngpvf6nsnrs") (s 2) (e (quote (("time" "dep:time"))))))

(define-public crate-aoc-bud-0.0.4 (c (n "aoc-bud") (v "0.0.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "115q0s4hx0an8jhx9l14xmmk39h87vhw7z52fwi848nm2a7kknmk") (s 2) (e (quote (("time" "dep:time"))))))

