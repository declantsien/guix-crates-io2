(define-module (crates-io ao c- aoc-runner-web) #:use-module (crates-io))

(define-public crate-aoc-runner-web-0.1.0 (c (n "aoc-runner-web") (v "0.1.0") (d (list (d (n "aoc-runner-web-derive") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0ksnp385rf6wk2fman59sc4n44rs72nys50vsp444cinc3gsffaj")))

