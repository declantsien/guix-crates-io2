(define-module (crates-io ao c- aoc-framework-rs) #:use-module (crates-io))

(define-public crate-aoc-framework-rs-0.1.0 (c (n "aoc-framework-rs") (v "0.1.0") (d (list (d (n "aoc-framework-core") (r "^0.1.0") (d #t) (k 0)) (d (n "aoc-framework-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "aoc-framework-parsers") (r "^0.1.0") (d #t) (k 0)) (d (n "aoc-framework-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1fgc42z7wibb8jqr52a8qb4k9yqdsmkyy3lw8qbydahp039v6cb4")))

