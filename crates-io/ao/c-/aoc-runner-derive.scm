(define-module (crates-io ao c- aoc-runner-derive) #:use-module (crates-io))

(define-public crate-aoc-runner-derive-0.1.0 (c (n "aoc-runner-derive") (v "0.1.0") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ziy0sry7nq9nx98lfympsfi3vzhcka3kyp718spphy7iphnkf8c") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.1 (c (n "aoc-runner-derive") (v "0.1.1") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v7n1ijqpkm3lmygys0pdzm9phvl8dwkf1qwdz1svhmh35992acg") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.2 (c (n "aoc-runner-derive") (v "0.1.2") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12znll03bl1q5v6k69hm9jsh9jvqlbwb7jkbqj8wp0xg6bxlj7rm") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.3 (c (n "aoc-runner-derive") (v "0.1.3") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bcwa5f3k3lv52yk0innnvyy940m1dwqa3hnh7gird17prirgn23") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.4 (c (n "aoc-runner-derive") (v "0.1.4") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c5k7j8hnhn6hl6vjr870msnlv3wi8g3aggl60kk9dy9r1hcvaha") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.5 (c (n "aoc-runner-derive") (v "0.1.5") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hjzav0cq0nvpdaigs999hw1mn6l5550ih1zpjqf1lw392kd4jsg") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.1.6 (c (n "aoc-runner-derive") (v "0.1.6") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "133q83dv8i9qil21mglf8d9qfklaaigys72k9p1mhmz8i71vcnf9") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.2.0-alpha1 (c (n "aoc-runner-derive") (v "0.2.0-alpha1") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jk2yvmwd08ilqgb3mxvhim209y5v9p2amcgfr2hq5zkn3b752p4") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.2.0 (c (n "aoc-runner-derive") (v "0.2.0") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06cyxck28lmh13d1vdrbf9np7xz9y2vy53yzxigxv7hkxr2a3vki") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.2.1 (c (n "aoc-runner-derive") (v "0.2.1") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n0qn0psin3jd97vm1mf9hc47gcg201rblhw6daj6ainbjnzs8xv") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.2.2 (c (n "aoc-runner-derive") (v "0.2.2") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lngs6rrnvj2vrqm1zvs7pmvaccax5mf9ab3kybsm1yxwkgh4s39") (f (quote (("default" "syn/full"))))))

(define-public crate-aoc-runner-derive-0.3.0 (c (n "aoc-runner-derive") (v "0.3.0") (d (list (d (n "aoc-runner-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k39028bizf3ih4x00mvjys4s14p2xg37cc1s92ydznkd51992xs") (f (quote (("default" "syn/full"))))))

