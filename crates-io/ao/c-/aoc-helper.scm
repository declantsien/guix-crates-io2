(define-module (crates-io ao c- aoc-helper) #:use-module (crates-io))

(define-public crate-aoc-helper-0.1.0 (c (n "aoc-helper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)))) (h "00w9qcrfgdb2p0341jixb8w03pa088kswcb3a5scwvy8m5k9hkym")))

(define-public crate-aoc-helper-0.1.1 (c (n "aoc-helper") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "0d3hf5inmvh8pkzkzg1gwyzvqq3427ij5qk1505h23cizhnzd5qx")))

(define-public crate-aoc-helper-0.1.2 (c (n "aoc-helper") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "0rdyxyvmlf59fddqa2vq8q4xdsgmfp6d0034fv0fhavv6zaf4a6f")))

(define-public crate-aoc-helper-0.1.3 (c (n "aoc-helper") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "04361glfv5ljx14b17qfrph4l9f9c5ja5v7mpw2y9n36qk4ncjli")))

(define-public crate-aoc-helper-0.2.0 (c (n "aoc-helper") (v "0.2.0") (d (list (d (n "time") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "0a43z101ldzz5dxz6knqk5sf3fl9qrlafbnwxap6h1sfvjdx81a2") (f (quote (("config-file" "toml"))))))

(define-public crate-aoc-helper-0.2.1 (c (n "aoc-helper") (v "0.2.1") (d (list (d (n "colored") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "1cs5hv9kyg9h4mh4visyffgb9fbzriwakx8cpf4gxi34h5p26b16") (f (quote (("default" "colored-output") ("config-file" "toml") ("colored-output" "colored"))))))

