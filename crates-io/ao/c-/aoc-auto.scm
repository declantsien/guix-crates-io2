(define-module (crates-io ao c- aoc-auto) #:use-module (crates-io))

(define-public crate-aoc-auto-0.1.0 (c (n "aoc-auto") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.2.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0rg9qfa42bkk28kagpnbhi3grg4mich7ik4gf5agmbyb097af2vm")))

