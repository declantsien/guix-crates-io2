(define-module (crates-io ao c- aoc-main) #:use-module (crates-io))

(define-public crate-aoc-main-0.1.0 (c (n "aoc-main") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.16.0") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 0)))) (h "0kqqyhzr76wb25b71cmm6v0bachg8ggclikga08as2rafjgsy31q")))

(define-public crate-aoc-main-0.2.0 (c (n "aoc-main") (v "0.2.0") (d (list (d (n "attohttpc") (r "^0.16.0") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)))) (h "15ijsrzjdb1a066w5x4cwlz43xvwbv02pacjm55nfjarbq7ywcss")))

(define-public crate-aoc-main-0.3.0 (c (n "aoc-main") (v "0.3.0") (d (list (d (n "attohttpc") (r "^0.16.0") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)))) (h "17c08i5yndlzdq7m7r3id0zdka69c2s75k96vif0jrhjzjmkh9wx") (f (quote (("bench" "criterion"))))))

(define-public crate-aoc-main-0.4.0 (c (n "aoc-main") (v "0.4.0") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^3.0.0-rc.3") (f (quote ("std"))) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)))) (h "1rx9xg0hl8x1d4ix7qgydj70n5sm5vdnch9d25ac5dpw2lq3bqpn") (f (quote (("bench" "criterion"))))))

(define-public crate-aoc-main-0.4.1 (c (n "aoc-main") (v "0.4.1") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)))) (h "1qxkwsn48rgrc92jl30c182ql4nzhhrdijxwnslibyl970z0imb2") (f (quote (("bench" "criterion"))))))

(define-public crate-aoc-main-0.5.0 (c (n "aoc-main") (v "0.5.0") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("tls"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)))) (h "0iv52nsgxph1pva6qw93qr4pbl96as87n57n7gzl9h1xhmp83f98") (f (quote (("bench" "criterion"))))))

