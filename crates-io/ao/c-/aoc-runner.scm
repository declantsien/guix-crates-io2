(define-module (crates-io ao c- aoc-runner) #:use-module (crates-io))

(define-public crate-aoc-runner-0.1.0 (c (n "aoc-runner") (v "0.1.0") (h "1b8g4r2h2c7qgmjkqbr1dr1wzj942hzj59p1p637xghsxri2jn7r")))

(define-public crate-aoc-runner-0.2.0-alpha1 (c (n "aoc-runner") (v "0.2.0-alpha1") (h "1ivfkv397k8vgx9y5x4k0xsiyg1hrs8yil8c1qgv1rycy0l5z74l")))

(define-public crate-aoc-runner-0.2.0 (c (n "aoc-runner") (v "0.2.0") (h "1wzy22c4p632hm9l1145j8chsd7bs651c4wraxrsywcn5h83rqy2")))

(define-public crate-aoc-runner-0.2.1 (c (n "aoc-runner") (v "0.2.1") (h "0ajk87vmdp4l4d3344kpf01lnf024bpm31nbi91hj49srw1v2qaa")))

(define-public crate-aoc-runner-0.2.2 (c (n "aoc-runner") (v "0.2.2") (h "0c6d76k30xhbpdbiy4hdxkzal7314pfrbmy1qbhnka13ffbjfjp3")))

(define-public crate-aoc-runner-0.3.0 (c (n "aoc-runner") (v "0.3.0") (h "1jyzp7bbjr791dbj6kyfj6nih4afl06r5s8qx6isa1nj98hgj7nj")))

