(define-module (crates-io ao c- aoc-framework-macros) #:use-module (crates-io))

(define-public crate-aoc-framework-macros-0.1.0 (c (n "aoc-framework-macros") (v "0.1.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cb25ypmgs7xv2x7wvvfanmz7yvl1nydj7xwhacayd488l6yqsl3")))

