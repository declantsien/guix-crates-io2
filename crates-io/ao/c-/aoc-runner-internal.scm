(define-module (crates-io ao c- aoc-runner-internal) #:use-module (crates-io))

(define-public crate-aoc-runner-internal-0.1.0 (c (n "aoc-runner-internal") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "10lrgh1r9jkcay8n0xxbh3l9sx1a0gmr9kxa1bn4b6k6yfkhnjr7")))

