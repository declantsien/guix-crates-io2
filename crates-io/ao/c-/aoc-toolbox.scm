(define-module (crates-io ao c- aoc-toolbox) #:use-module (crates-io))

(define-public crate-aoc-toolbox-0.1.0 (c (n "aoc-toolbox") (v "0.1.0") (h "14ni1vfclyddhj49mdcmb2lih9i2hqax6pzwdyvkrxck6rmi3aqk")))

(define-public crate-aoc-toolbox-0.2.0 (c (n "aoc-toolbox") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1p982cysc1gf29bpy3af8b1ip2iyz9jrxgd9szzzrph9kfpgapjj")))

(define-public crate-aoc-toolbox-0.3.0 (c (n "aoc-toolbox") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1rivbzkl7kivxvcpc6y94mv8a85b8rigcqjszwmqsxyppv0sjyhi")))

(define-public crate-aoc-toolbox-0.4.0 (c (n "aoc-toolbox") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aoc-toolbox-derive") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "1z5qk4478ggmjjsblj3b7mpkbcxz6xqrvjw2qnxp66iq5d2vz86w")))

(define-public crate-aoc-toolbox-0.4.1 (c (n "aoc-toolbox") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aoc-toolbox-derive") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "115kl3qgaxhppyxxndymynm6g5ih8dmx1ij259sc19v8qc1zjdar")))

