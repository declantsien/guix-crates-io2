(define-module (crates-io ao c- aoc-toolbox-derive) #:use-module (crates-io))

(define-public crate-aoc-toolbox-derive-0.1.0 (c (n "aoc-toolbox-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aq41kff0bxr1c31wp2gj3fkxis2969c2314zb8cqgmhfva6v5hf")))

(define-public crate-aoc-toolbox-derive-0.2.0 (c (n "aoc-toolbox-derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jkdrzk982a4l8466j6pk39mv0xy4z4gjp3h27wv950723jz8gsn")))

(define-public crate-aoc-toolbox-derive-0.3.0 (c (n "aoc-toolbox-derive") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rd5gpfpli60lrii11nzbhz9xqfnm1fhl8fxxdz2rbms4zni8fcr")))

(define-public crate-aoc-toolbox-derive-0.4.0 (c (n "aoc-toolbox-derive") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1blidwiw889zrak1d0srwsp1hjsczpbwzkdiy3frsiy6xsjr8ilp")))

(define-public crate-aoc-toolbox-derive-0.4.1 (c (n "aoc-toolbox-derive") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "032bzwqy6ikiv329xwq79wrk7h4pqdan0g85sbp12wq2p8ird3js")))

