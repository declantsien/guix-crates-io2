(define-module (crates-io ao c- aoc-helpers) #:use-module (crates-io))

(define-public crate-aoc-helpers-0.1.0 (c (n "aoc-helpers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "178c9lh7sg2jnf57rga184q9n1krl1chm2m0d2pxz982dqnx5h75")))

(define-public crate-aoc-helpers-0.1.1 (c (n "aoc-helpers") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1m90kpzx66knqj8s11s66l1m3c5zndvkkvqr7jb9k2g3bkf39xrm")))

(define-public crate-aoc-helpers-0.1.2 (c (n "aoc-helpers") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "04k9y7xhjcq010v4pi358lbyjsf2hl52pdwvbjywxzx8pbjw28z1")))

(define-public crate-aoc-helpers-0.2.0 (c (n "aoc-helpers") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0q4avnmdahcmya7kga5z4s3ycgyz9blghwd3025i8jjj0kx0mvxz")))

