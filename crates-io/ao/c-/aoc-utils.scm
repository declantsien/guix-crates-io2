(define-module (crates-io ao c- aoc-utils) #:use-module (crates-io))

(define-public crate-aoc-utils-0.1.0 (c (n "aoc-utils") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0fxzrq7iq9s1jlgybx427kmkxxwr1gzadk7m23wrspxkxg8nryyj")))

(define-public crate-aoc-utils-0.1.1 (c (n "aoc-utils") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "157hiq0w0kdmgrrkfdzmfk5qv1vq66ysianqq6a8nw6rldc0laza")))

(define-public crate-aoc-utils-0.2.0 (c (n "aoc-utils") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1ajgv3cf4l22az7kz9i5w2ap2hfszf68pq4gamyzr6qfg7bv5r62")))

(define-public crate-aoc-utils-0.2.1 (c (n "aoc-utils") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0f32g7lhysxqrf8xsmbb7prkqnklwl7pqwjica1z3c8ph7gjrvhy")))

(define-public crate-aoc-utils-0.3.0 (c (n "aoc-utils") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0h5z0s0fkmy2dmh4xzmmy3pmqyjx007pc94hm9lb618ir2rdq8sg")))

(define-public crate-aoc-utils-0.4.0 (c (n "aoc-utils") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "elapsed") (r "^0.1") (d #t) (k 0)))) (h "1ily5cll40rrdibhly141q401wzlqsiv536xf546f12l82li7s8b")))

(define-public crate-aoc-utils-0.4.1 (c (n "aoc-utils") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "elapsed") (r "^0.1") (d #t) (k 0)))) (h "0894k4lnxgxlih1v6srsssnqpfbjha4sy89sd2z2b07dkw6jn2d5")))

(define-public crate-aoc-utils-0.4.2 (c (n "aoc-utils") (v "0.4.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("std"))) (k 0)) (d (n "elapsed") (r "^0.1") (d #t) (k 0)))) (h "1d4jiay92kcc2f0siywmbjw8g4q513r06bvv5rn5gn3bymsglbsf")))

(define-public crate-aoc-utils-0.4.3 (c (n "aoc-utils") (v "0.4.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "help" "usage" "error-context"))) (k 0)) (d (n "elapsed") (r "^0.1") (d #t) (k 0)))) (h "1x3if3hl737syizbim6rxxn4ky60j0gn0v5drw36qrs2fdnv9hnh")))

(define-public crate-aoc-utils-0.5.0 (c (n "aoc-utils") (v "0.5.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("std" "help" "usage" "error-context"))) (k 0)))) (h "163r878v7ks008bk344r9pdh25kxszi63wmrj7rnqhj739abkhl2")))

