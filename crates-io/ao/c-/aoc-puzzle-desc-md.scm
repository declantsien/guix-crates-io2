(define-module (crates-io ao c- aoc-puzzle-desc-md) #:use-module (crates-io))

(define-public crate-aoc-puzzle-desc-md-0.0.0 (c (n "aoc-puzzle-desc-md") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0qrmsrsxh4i3q54s5bwgia41cq77l5a24cks6zyn8cz1p0mlrn1k")))

(define-public crate-aoc-puzzle-desc-md-0.7.0 (c (n "aoc-puzzle-desc-md") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "075z6kg28chnahib7hiychi0x3gwaf89g7gq0xqarblss5g2zd3r")))

(define-public crate-aoc-puzzle-desc-md-0.8.0 (c (n "aoc-puzzle-desc-md") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1hba1nhcqnc6hg11i1n8lgacyafwndmfxbzw6s72yc18mc5bvzq6")))

(define-public crate-aoc-puzzle-desc-md-0.9.0 (c (n "aoc-puzzle-desc-md") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0i546ggng04glg9j1d90wlsx1pziml42wx3crlr5vxfpnisw4m8m")))

(define-public crate-aoc-puzzle-desc-md-0.10.0 (c (n "aoc-puzzle-desc-md") (v "0.10.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0397ad6vq4qvnfdckiibhdf0gyghyi4mr2l6j9gvs283hk08j2j1")))

