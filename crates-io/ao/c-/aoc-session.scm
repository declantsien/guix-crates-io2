(define-module (crates-io ao c- aoc-session) #:use-module (crates-io))

(define-public crate-aoc-session-0.1.0 (c (n "aoc-session") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rookie") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h916v7wy2di93p3jhsac1bd1y56bb2lmcqzzylikmjpnmlxk05f")))

(define-public crate-aoc-session-0.1.1 (c (n "aoc-session") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rookie") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19h647bc24sl8kkpabap1nsyci4sdzh57pqmwm5gzffzy5famnbg")))

(define-public crate-aoc-session-0.2.0 (c (n "aoc-session") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rookie") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1829p18ffgyy43n7v9x59705nvqdnbbwsjyn8z84hqjnfmxs8fgd")))

(define-public crate-aoc-session-0.2.1 (c (n "aoc-session") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rookie") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09cgnl46aay4i5scm48g3kbhc3gqlqqsmyza70zdgsrvi9k9h0k5")))

