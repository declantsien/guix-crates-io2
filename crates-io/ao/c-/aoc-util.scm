(define-module (crates-io ao c- aoc-util) #:use-module (crates-io))

(define-public crate-aoc-util-0.1.0 (c (n "aoc-util") (v "0.1.0") (h "1hmdsx5d5ga16ch604fisw59bdavv3zkvhbp57z0z1yr1nizf2b0")))

(define-public crate-aoc-util-0.1.1 (c (n "aoc-util") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ibzzbvqdxhn7ls5x6vzs1n4wpkj089vrbdfky0p3bzswayxq19c")))

(define-public crate-aoc-util-0.1.2 (c (n "aoc-util") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14mzbv6zggfyip56l6d0aw8nwcjqxynfpqh2li9r7rhhk7lhvb43")))

