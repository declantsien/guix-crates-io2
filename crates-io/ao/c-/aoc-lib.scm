(define-module (crates-io ao c- aoc-lib) #:use-module (crates-io))

(define-public crate-aoc-lib-0.1.0 (c (n "aoc-lib") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1n0nxk4wv65qhrgclzya5ryfxmqlb26nz60zr6vpax3778wxpjqx")))

(define-public crate-aoc-lib-0.3.0 (c (n "aoc-lib") (v "0.3.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "02pabpcp9sm5139k1bcmv45pp0izlf5z8ymac69927s8l0lpc5hz")))

(define-public crate-aoc-lib-0.4.0 (c (n "aoc-lib") (v "0.4.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "19smkv6qv2vcajhkw0df93p2sqcv9baxw6x0zkh65kdj33mhv92i")))

(define-public crate-aoc-lib-0.4.1 (c (n "aoc-lib") (v "0.4.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "17bh7czvp3phmb9kmm6lg7rarkla21lcdng4qkcj6ksd6z1vci7p")))

(define-public crate-aoc-lib-0.4.2 (c (n "aoc-lib") (v "0.4.2") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0l5phl8w061nsz8np78qsn2z8gl08x3h8r3w2nqnkr4rmr9lbxl2")))

(define-public crate-aoc-lib-0.4.3 (c (n "aoc-lib") (v "0.4.3") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1jl9s9zdyrq9519n6xqiw7q6f649l7mv1yg530z11i3iwy391d6i")))

(define-public crate-aoc-lib-1.0.0 (c (n "aoc-lib") (v "1.0.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scanf") (r "^1.2.1") (d #t) (k 0)))) (h "0kmmjp6mg9cbindq8zq6zhj6p3735sdi2180468xkgvsn5frbnpb")))

(define-public crate-aoc-lib-1.0.1 (c (n "aoc-lib") (v "1.0.1") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scanf") (r "^1.2.1") (d #t) (k 0)))) (h "0r2dczhmaq96dy8x9vw2x4b0bs7v2d49s96sz6h99pa3nkv76dgq")))

(define-public crate-aoc-lib-1.0.2 (c (n "aoc-lib") (v "1.0.2") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scanf") (r "^1.2.1") (d #t) (k 0)))) (h "05wh3w5nbijsddclcxjlan1gqxj8xlklg49k71mx94wf2g5i9ay3")))

(define-public crate-aoc-lib-1.0.3 (c (n "aoc-lib") (v "1.0.3") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scanf") (r "^1.2.1") (d #t) (k 0)))) (h "0qhna2g7xl3gy3iga3lfgbkk2wa7d8g07vg0rpdk7ql9zrfbl1fy")))

