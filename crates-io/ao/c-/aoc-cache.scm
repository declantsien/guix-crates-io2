(define-module (crates-io ao c- aoc-cache) #:use-module (crates-io))

(define-public crate-aoc-cache-0.1.0 (c (n "aoc-cache") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1dswfywsxdabsx9zs9p35f6016qb1ysj95hm45f14rk1dk2c4yds") (y #t)))

(define-public crate-aoc-cache-0.1.1 (c (n "aoc-cache") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "16660936g6yxg07q2jhbw8ibmk27vvk5dkkf5vvvbrkvv3zzaxs5") (y #t)))

(define-public crate-aoc-cache-0.1.2 (c (n "aoc-cache") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "13xs1knak8yqaigbc3dkgyjzcfhdf0nr76bnzi9kp6qxzfy4r0dj") (y #t)))

(define-public crate-aoc-cache-0.1.3 (c (n "aoc-cache") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1dvl1b39wyzy1r2skh1q04jvl2ay48anibw18nqqi20xzgh1q49l")))

(define-public crate-aoc-cache-0.1.4 (c (n "aoc-cache") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1nj5cdad1lf82lhrgvqxf5wdcr7ykyb501jrmsp7hw14k4bg34wh")))

(define-public crate-aoc-cache-0.2.0 (c (n "aoc-cache") (v "0.2.0") (d (list (d (n "cookie") (r "^0.16.1") (d #t) (k 0)) (d (n "cookie_store") (r "^0.16.0") (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1k862sd3yaf47krkl9g5jpq6gxhs11rlmj14y8bdw8r6kw2csmgn")))

(define-public crate-aoc-cache-0.2.1 (c (n "aoc-cache") (v "0.2.1") (d (list (d (n "cookie") (r "^0.16.1") (d #t) (k 0)) (d (n "cookie_store") (r "^0.16.0") (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0vbs5gc4ch6r4mfnhk5hlh7q4jg0dn45rrv354gdvn5r1x7db8gl")))

(define-public crate-aoc-cache-0.2.2 (c (n "aoc-cache") (v "0.2.2") (d (list (d (n "cookie") (r "^0.18.0") (d #t) (k 0)) (d (n "cookie_store") (r "^0.20.0") (d #t) (k 0)) (d (n "scratch") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0vid36g5vya9ccs308ja0hmk6vzpw7a5p47vqasi1vrqbvfp5r28")))

