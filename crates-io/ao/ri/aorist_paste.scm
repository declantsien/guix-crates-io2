(define-module (crates-io ao ri aorist_paste) #:use-module (crates-io))

(define-public crate-aorist_paste-0.0.1 (c (n "aorist_paste") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d97i53r54dgdfwvpz9pnc5w6dxnkivcqqry84cxlw2r197nlik5")))

