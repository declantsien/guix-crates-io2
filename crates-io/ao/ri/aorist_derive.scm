(define-module (crates-io ao ri aorist_derive) #:use-module (crates-io))

(define-public crate-aorist_derive-0.0.1 (c (n "aorist_derive") (v "0.0.1") (d (list (d (n "aorist_util") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "sqlparser") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "0pprhg9myjymkvgm7qapdrnnbmsa5a2r29nypqvb7ks5qgdbpkxl")))

