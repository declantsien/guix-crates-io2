(define-module (crates-io ao ri aorist_extendr-api) #:use-module (crates-io))

(define-public crate-aorist_extendr-api-0.0.1 (c (n "aorist_extendr-api") (v "0.0.1") (d (list (d (n "aorist_extendr-engine") (r "^0.0.1") (d #t) (k 0)) (d (n "aorist_extendr-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libR-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)))) (h "1k627j0jcl4kr5hnwik3avvwg4s3bfzivx4a6qvdkgiwyl6p7df3") (f (quote (("tests-minimal" "libR-sys/use-bindgen") ("tests-all" "ndarray" "libR-sys/use-bindgen") ("default"))))))

