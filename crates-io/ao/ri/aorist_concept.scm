(define-module (crates-io ao ri aorist_concept) #:use-module (crates-io))

(define-public crate-aorist_concept-0.0.1 (c (n "aorist_concept") (v "0.0.1") (d (list (d (n "aorist_util") (r "^0.0.1") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.25") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.52") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1wz1j402vkn09m6js11z3h8czv82fk7fw5kpiyzf21xpz973a1ji") (f (quote (("trace_macros") ("rustfmt") ("log_syntax"))))))

