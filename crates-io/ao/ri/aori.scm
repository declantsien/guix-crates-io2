(define-module (crates-io ao ri aori) #:use-module (crates-io))

(define-public crate-aori-0.1.0 (c (n "aori") (v "0.1.0") (d (list (d (n "aori_requests") (r "^0.1.0") (d #t) (k 0)) (d (n "aori_types") (r "^0.1.0") (d #t) (k 0)))) (h "0cbbzfibv2wi9hz4ads67937rsdcqcrihkkix1b4h0rb76z0jq7n")))

(define-public crate-aori-0.1.2 (c (n "aori") (v "0.1.2") (d (list (d (n "aori_requests") (r "^0.1.2") (d #t) (k 0)) (d (n "aori_types") (r "^0.1.2") (d #t) (k 0)))) (h "064lsjjvmca7drzqzx9b2b012166bx7q7aca75z7yj2v73xsz664")))

(define-public crate-aori-0.1.3 (c (n "aori") (v "0.1.3") (d (list (d (n "aori_requests") (r "^0.1.3") (d #t) (k 0)) (d (n "aori_types") (r "^0.1.3") (d #t) (k 0)))) (h "01nz40raygy2fdp2l4frjirbjgz03hl4939fvh4bimafpr45743i")))

(define-public crate-aori-0.1.4 (c (n "aori") (v "0.1.4") (d (list (d (n "aori_requests") (r "^0.1.4") (d #t) (k 0)) (d (n "aori_types") (r "^0.1.4") (d #t) (k 0)))) (h "1bbs72f2gass0in6a99q08w46hhvzy8dl3f8h8i93rnbkaya8cd8")))

(define-public crate-aori-0.1.5 (c (n "aori") (v "0.1.5") (d (list (d (n "aori_requests") (r "^0.1.5") (d #t) (k 0)) (d (n "aori_types") (r "^0.1.5") (d #t) (k 0)))) (h "19q9dk8md8mh9wpxw60z47pd22ihqgh9r4q725vcdw6s077y5yal")))

