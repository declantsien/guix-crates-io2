(define-module (crates-io ao ri aorist_extendr-engine) #:use-module (crates-io))

(define-public crate-aorist_extendr-engine-0.0.1 (c (n "aorist_extendr-engine") (v "0.0.1") (d (list (d (n "libR-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1ar7c8zxaawrd0z2596hh52589d95advwi143m087d2sgw4hjj9a") (f (quote (("tests-all" "libR-sys/use-bindgen") ("default"))))))

