(define-module (crates-io ao ri aorist_extendr-macros) #:use-module (crates-io))

(define-public crate-aorist_extendr-macros-0.0.1 (c (n "aorist_extendr-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ysndislip2yss6ycxddbfq0ilnp926x0nh646km390qwdvav209")))

