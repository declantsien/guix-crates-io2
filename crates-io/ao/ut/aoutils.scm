(define-module (crates-io ao ut aoutils) #:use-module (crates-io))

(define-public crate-aoutils-0.1.0 (c (n "aoutils") (v "0.1.0") (h "1r0dpm7rwwdqfryk9zd9bcqjv673sc71im0y36adsz1xvq7kjcxn")))

(define-public crate-aoutils-0.1.1 (c (n "aoutils") (v "0.1.1") (h "04q0ryb2r64ravh1iny2g88h8afqycczgkmj38nla5d48gdv17ab")))

