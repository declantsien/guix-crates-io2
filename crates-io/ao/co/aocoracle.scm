(define-module (crates-io ao co aocoracle) #:use-module (crates-io))

(define-public crate-aocoracle-0.1.0 (c (n "aocoracle") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1iw3rbnv0x91s8n9d35wljpnmg4fh8jf1vp4073jr2x8lrdzngdb")))

(define-public crate-aocoracle-0.1.1 (c (n "aocoracle") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0n3isjx1nmps1p5bkakrasviivbx2m49is4s37yn5rcvc41x6mvm")))

(define-public crate-aocoracle-0.1.2 (c (n "aocoracle") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1cqjgg7228rb849mfb3bynw0wyzpfw2im1bkk7m2hxf9zivh70lp")))

