(define-module (crates-io ao ve aovec) #:use-module (crates-io))

(define-public crate-aovec-1.0.0 (c (n "aovec") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.3") (d #t) (k 0)))) (h "1rf16hzcwv60wyhks8940naiq2rq8wcpzyvckwh88gcln42p98aw") (y #t)))

(define-public crate-aovec-1.0.1 (c (n "aovec") (v "1.0.1") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.3") (d #t) (k 0)))) (h "1nfsi8h3nkxnys9bzj5a3xsh70nisydwahp7l1d5vp030qwqcqrd") (y #t)))

(define-public crate-aovec-1.0.2 (c (n "aovec") (v "1.0.2") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.3") (d #t) (k 0)))) (h "1yghwrkg5mbfbag0i6xv0yn4shxb8fhi66ir3nc36kmgc7dm61cc")))

(define-public crate-aovec-1.1.0 (c (n "aovec") (v "1.1.0") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.3") (d #t) (k 0)))) (h "1n8wg6mjbq1r2wskc17y5r8kfajdimbs93zxrfdjvm1vyy6xxkz6")))

