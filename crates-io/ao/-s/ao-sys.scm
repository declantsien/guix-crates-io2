(define-module (crates-io ao -s ao-sys) #:use-module (crates-io))

(define-public crate-ao-sys-0.0.1 (c (n "ao-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yz749dh7wyss8ji271a2wlf5nj28ibs6qizydnixxm514lb4ihl")))

(define-public crate-ao-sys-0.0.2 (c (n "ao-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jcg1rks92aa0skfd9b4hbhmjsknccpb6f2b9l8zvamzjgznqn9l")))

(define-public crate-ao-sys-0.1.0 (c (n "ao-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "089d6fljy9fvxs0qg4z3aqyhjp3jkgc433fmi64ckasd928xsh64")))

(define-public crate-ao-sys-0.1.1 (c (n "ao-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j73ni30cliidmq69k4vi1hsjhk5rkb9wf7qm62zw2saflpak4aq")))

(define-public crate-ao-sys-0.1.2 (c (n "ao-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10gvxr2lyff757vijhr4kyxw2q4yfpd1w2mk2ghi0sp2drcwbw8f")))

