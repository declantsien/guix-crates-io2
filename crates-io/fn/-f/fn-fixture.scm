(define-module (crates-io fn -f fn-fixture) #:use-module (crates-io))

(define-public crate-fn-fixture-1.0.0 (c (n "fn-fixture") (v "1.0.0") (d (list (d (n "fn-fixture-lib") (r "=1.0.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 2)))) (h "1cj10qvcknl41c5vnw7h7nbchprjal6v0457rq2bnjqy70zrb5yh")))

(define-public crate-fn-fixture-1.0.1 (c (n "fn-fixture") (v "1.0.1") (d (list (d (n "fn-fixture-lib") (r "=1.0.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 2)))) (h "0js3pjpdr5zqsb2j9vi5jqw72mnkpdc7pajfzyk9j14r80jqs0r3")))

(define-public crate-fn-fixture-1.0.2 (c (n "fn-fixture") (v "1.0.2") (d (list (d (n "fn-fixture-lib") (r "=1.0.2") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 2)))) (h "0f3x99vhm7lrx88d3rpdfzrj4lmmvi5bsqzcg04lm2c0zrs3rjvr")))

