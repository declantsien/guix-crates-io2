(define-module (crates-io fn -f fn-fixture-lib) #:use-module (crates-io))

(define-public crate-fn-fixture-lib-1.0.0 (c (n "fn-fixture-lib") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8s4mdba2idgxapjxkv6r4y7sjh25b40891qgyjxi3pj352a4fl")))

(define-public crate-fn-fixture-lib-1.0.1 (c (n "fn-fixture-lib") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c8lnqw200bg6z76y7fqj4kisrhwiwsr13rg1a0m0f365gdhh5a9")))

(define-public crate-fn-fixture-lib-1.0.2 (c (n "fn-fixture-lib") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1v6yq772ac2wkizg7dfvhzchjn4vlqwdbbbsxy2sy0db29l2fcd7")))

