(define-module (crates-io fn v_ fnv_rs) #:use-module (crates-io))

(define-public crate-fnv_rs-0.3.0 (c (n "fnv_rs") (v "0.3.0") (d (list (d (n "crypto-bigint") (r "^0.4.9") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "15h7diavv6bmhh9qvnnn5srz5jyiwhpllg51sqf8az0iy144x8z6") (f (quote (("std") ("default" "std")))) (y #t) (r "1.61")))

(define-public crate-fnv_rs-0.4.0 (c (n "fnv_rs") (v "0.4.0") (d (list (d (n "crypto-bigint") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1b3kl50bdkmzq414nz81sk57l9g87f9m30kmvgbqw7imphjcpkyq") (s 2) (e (quote (("bigint" "dep:crypto-bigint")))) (r "1.68")))

(define-public crate-fnv_rs-0.4.1 (c (n "fnv_rs") (v "0.4.1") (d (list (d (n "crypto-bigint") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0471w6nah2l00ibc10n68hls6xb8kzc9wgjqhspsprsq51g57rzc") (s 2) (e (quote (("bigint" "dep:crypto-bigint")))) (r "1.68")))

(define-public crate-fnv_rs-0.4.2 (c (n "fnv_rs") (v "0.4.2") (d (list (d (n "crypto-bigint") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1zgffxwj4x9fp8kfwm1n4fxczb4bdg9jpw2a22y16qlppyg57rfn") (s 2) (e (quote (("bigint" "dep:crypto-bigint")))) (r "1.68")))

(define-public crate-fnv_rs-0.4.3 (c (n "fnv_rs") (v "0.4.3") (d (list (d (n "crypto-bigint") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0aq9x6khsqarp0zjwf8fgrhi6f9z6yzhlm9f2rx12zw8sl5a5l9p") (s 2) (e (quote (("bigint" "dep:crypto-bigint")))) (r "1.68")))

