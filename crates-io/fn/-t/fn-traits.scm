(define-module (crates-io fn -t fn-traits) #:use-module (crates-io))

(define-public crate-fn-traits-0.1.0 (c (n "fn-traits") (v "0.1.0") (h "0xs8iyq2wfmi6h22650w1h3jlf6i1cacck6x9d0bb4w00w7iqbna")))

(define-public crate-fn-traits-0.1.1 (c (n "fn-traits") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0zikqmfjfqly1y62pwk5hhhw8vnkx0wbbn9qqx81jjcxljp5wrnk")))

(define-public crate-fn-traits-0.1.2 (c (n "fn-traits") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0312r1yykcx8lmp0g2siqrb8a195v5zm9asi3y3ri3fg31izy3nd")))

