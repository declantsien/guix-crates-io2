(define-module (crates-io fn st fnstack-cf-macro) #:use-module (crates-io))

(define-public crate-fnstack-cf-macro-0.1.0 (c (n "fnstack-cf-macro") (v "0.1.0") (h "1cww5n18hz21pyyg2gx59p04hgnlm2aaarl72dbwdclnrnxhqprr") (y #t)))

(define-public crate-fnstack-cf-macro-0.2.0 (c (n "fnstack-cf-macro") (v "0.2.0") (h "04mfm1kd65x6zq5jw8gpfm2lsfpvi1a6vp8l8kxj5czwh280c4ic") (y #t)))

(define-public crate-fnstack-cf-macro-0.2.1 (c (n "fnstack-cf-macro") (v "0.2.1") (h "1ln2msl8ia9s4r9dhc4xp3w66a0ldn444vbidwz9f7lgwgg1gi28") (f (quote (("axum")))) (y #t)))

(define-public crate-fnstack-cf-macro-0.2.2 (c (n "fnstack-cf-macro") (v "0.2.2") (h "0r0y56z6svb2yzdqvb820qvh5n5rz4xf1hx5bi8mjnrc0wn05xxx") (f (quote (("cloudflare") ("axum")))) (y #t)))

