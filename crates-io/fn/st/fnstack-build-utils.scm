(define-module (crates-io fn st fnstack-build-utils) #:use-module (crates-io))

(define-public crate-fnstack-build-utils-0.1.0 (c (n "fnstack-build-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9bz8qm38xrzkaslpcrxlpbk2s59wlg6bz54blxh0wa82rsx5in") (y #t)))

