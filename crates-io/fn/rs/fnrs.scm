(define-module (crates-io fn rs fnrs) #:use-module (crates-io))

(define-public crate-fnrs-0.1.0 (c (n "fnrs") (v "0.1.0") (h "00pnmv67khg033q8phhdph6rak0cah0sxflkfz33crf7hh0h9jsy") (y #t)))

(define-public crate-fnrs-0.1.1 (c (n "fnrs") (v "0.1.1") (h "1cp03k7fqzh93alnnlc3v6vzd2pwc1qb7d71w0lk956a0k4y8p7g") (y #t)))

(define-public crate-fnrs-0.1.2 (c (n "fnrs") (v "0.1.2") (h "1dk38vb23li9lsmg7f5729hlgj1xm3r3qwp1zd1g8y7icgji0hvf") (y #t)))

(define-public crate-fnrs-0.1.3 (c (n "fnrs") (v "0.1.3") (h "1y358in47v15csqpqnnvadqlki6f58mp4aaxzg9529cl21bqdrij") (y #t)))

(define-public crate-fnrs-0.1.4 (c (n "fnrs") (v "0.1.4") (h "14z5zqqqqrs254l3h2m40drbimvbjwf9kfc7qymm3vlgy7pb0paz") (y #t)))

(define-public crate-fnrs-0.1.5 (c (n "fnrs") (v "0.1.5") (h "0almpknq283f0800zj3kka90cr49vmcmn6vaa30wbj8il7zh9wm6") (y #t)))

(define-public crate-fnrs-0.1.6 (c (n "fnrs") (v "0.1.6") (h "06k2n673w7s60hdy43ildanvcmw69j6mlr5sypl4g7rqkzzvgmq9") (y #t)))

(define-public crate-fnrs-0.1.7 (c (n "fnrs") (v "0.1.7") (h "1nsq6dd54xc3w3pwg3jyi3va3sb68f3k23a0cba0in2hh98d1ckr")))

(define-public crate-fnrs-0.1.8 (c (n "fnrs") (v "0.1.8") (h "1cvhjx1qw5wc9c6v3kkz2fkpar1rw0ashq00gyjbr8gy92mrbgkp")))

