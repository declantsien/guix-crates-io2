(define-module (crates-io fn sq fnsql) #:use-module (crates-io))

(define-public crate-fnsql-0.1.0 (c (n "fnsql") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ph4xqia0pbvfc0v0acz0zzwh69j3xlrbwwbkqsaq2hxwsasfmlc")))

(define-public crate-fnsql-0.1.1 (c (n "fnsql") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07d2ypcs01948sjv931jqpmklrrfhd0s4y80gkcdfafnyxnkmvi3")))

(define-public crate-fnsql-0.1.2 (c (n "fnsql") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g56d28c2xj1nandivazs255xc0b7pybf9fk83kyl1rrxy5k68r5")))

(define-public crate-fnsql-0.1.3 (c (n "fnsql") (v "0.1.3") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mizbddhzllwzj0w56k8w0gcmd0avdgfjbjc33qg8qd8chxpbqlj")))

(define-public crate-fnsql-0.1.4 (c (n "fnsql") (v "0.1.4") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09nd0q88r6di4sixvkg3ahakhbisg2w497qj4sfizmcb3pfb9yib")))

(define-public crate-fnsql-0.2.0 (c (n "fnsql") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x9backjpygb80c88nlqqg0p8jfb1781nvc0pccvqsbgr9775afr")))

(define-public crate-fnsql-0.2.1 (c (n "fnsql") (v "0.2.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vs4nmakhpwqb4mfm8kgsm54z47rvn4h73sska1wfv0pipa4ppw1")))

(define-public crate-fnsql-0.2.2 (c (n "fnsql") (v "0.2.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qym9nvav2a0f2l6w3wwh3bin10d614h6hwn5cw8l7m7wsa798mc")))

(define-public crate-fnsql-0.2.3 (c (n "fnsql") (v "0.2.3") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xy2n9hx5r5nx8f9dchw0727askcdw6zk7ahd9rh88388sry4b9y")))

(define-public crate-fnsql-0.2.4 (c (n "fnsql") (v "0.2.4") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1571qhllwwj4r167dyw32p886vlc70gaq7lan62bzshn3n6gbzrc")))

(define-public crate-fnsql-0.2.5 (c (n "fnsql") (v "0.2.5") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r3rnmdabfk85207nrjbgkb3bpys85k3nljrqbrs5sjzp80ailvx")))

(define-public crate-fnsql-0.2.6 (c (n "fnsql") (v "0.2.6") (d (list (d (n "fnsql-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1p8y158brgy1r1qbcxy4aqxn07q8wszr0kls4wy037mj8qiy43nw") (f (quote (("with-rusqlite" "fnsql-macro/with-postgres") ("with-postgres" "fnsql-macro/with-rusqlite" "postgres" "tempdir") ("prepare-cache" "fnsql-macro/prepare-cache") ("default") ("all" "with-rusqlite" "with-postgres" "prepare-cache"))))))

(define-public crate-fnsql-0.2.7 (c (n "fnsql") (v "0.2.7") (d (list (d (n "fnsql-macro") (r "^0.2.7") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1b342hdz9ziz81mnad8h2cl17k56mm1w1n9nvpg2fbn9xfhc1aj0") (f (quote (("with-rusqlite" "fnsql-macro/with-postgres") ("with-postgres" "fnsql-macro/with-rusqlite" "postgres" "tempdir") ("prepare-cache" "fnsql-macro/prepare-cache") ("default") ("all" "with-rusqlite" "with-postgres" "prepare-cache"))))))

