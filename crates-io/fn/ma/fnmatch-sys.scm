(define-module (crates-io fn ma fnmatch-sys) #:use-module (crates-io))

(define-public crate-fnmatch-sys-1.0.0 (c (n "fnmatch-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1cs5dwcl05yxrrzyca2vjaicxsvfgwsiw6q1sb601vvldx9fhs96") (l "fnmatch")))

