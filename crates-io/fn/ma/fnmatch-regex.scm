(define-module (crates-io fn ma fnmatch-regex) #:use-module (crates-io))

(define-public crate-fnmatch-regex-0.1.0 (c (n "fnmatch-regex") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06b1a81gd3axqkhqj3gmrzniw3k6nx40q36dw5xdc5qdnn9mrlx4")))

(define-public crate-fnmatch-regex-0.2.0 (c (n "fnmatch-regex") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)))) (h "11kiqq52gqz5pp47d2bp3gp3r09x8qg23mw6w9750ilgapcx8djx")))

