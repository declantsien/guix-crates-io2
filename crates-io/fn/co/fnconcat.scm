(define-module (crates-io fn co fnconcat) #:use-module (crates-io))

(define-public crate-fnconcat-0.1.0 (c (n "fnconcat") (v "0.1.0") (h "05h95p25zbpydk426zk6w9j96ppxbafna89qx35bpp2y9bdrvgv3")))

(define-public crate-fnconcat-0.2.0 (c (n "fnconcat") (v "0.2.0") (h "0jx4i7jqhspwljyf8idi3ghny94z4g8a2x6qyjddncdfp7ypvgy5")))

(define-public crate-fnconcat-0.2.1 (c (n "fnconcat") (v "0.2.1") (h "1q6s3rxdwbvyrixiwgn1cmssbarr7i7gf95ffsifjxp4yjam4qwp")))

(define-public crate-fnconcat-0.2.2 (c (n "fnconcat") (v "0.2.2") (h "1qnmgsp4pl1n25f5asdid9cz8a5550cs7706q9vfsyml2crjvbpz")))

(define-public crate-fnconcat-0.2.3 (c (n "fnconcat") (v "0.2.3") (h "0h9bjkfyzb12z0bklln80hxbihm7pn4zpp15n9i0bqi8nch978jd")))

