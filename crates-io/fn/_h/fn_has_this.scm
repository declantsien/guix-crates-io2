(define-module (crates-io fn _h fn_has_this) #:use-module (crates-io))

(define-public crate-fn_has_this-0.1.0 (c (n "fn_has_this") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn_squash") (r "^0") (d #t) (k 0)))) (h "1l89vdgk5yl36f026q92m6w529sg31pjwdbb31w5ysyj1nv81cnv") (y #t)))

(define-public crate-fn_has_this-0.1.1 (c (n "fn_has_this") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn_squash") (r "^0") (d #t) (k 0)))) (h "1advip3hxfkmn3cwrbqkr3knv4jya59jl0cjf2b7cdi1cy0974zb")))

