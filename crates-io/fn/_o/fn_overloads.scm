(define-module (crates-io fn _o fn_overloads) #:use-module (crates-io))

(define-public crate-fn_overloads-0.1.0 (c (n "fn_overloads") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0dsb526iilikjac3m18nzccmfmwj75fsbgnljxwpn68ay4anvdf1") (f (quote (("std") ("impl_futures") ("default" "std"))))))

(define-public crate-fn_overloads-0.2.0 (c (n "fn_overloads") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1dxcnd32kq3pvay1jynqk8xv3gzw3q6j3ibg5j8xrz8fb9kj2ksk") (f (quote (("std") ("impl_futures") ("default" "std"))))))

