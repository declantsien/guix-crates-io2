(define-module (crates-io fn _o fn_once) #:use-module (crates-io))

(define-public crate-fn_once-0.1.0 (c (n "fn_once") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rr4vmkz1kgm9armxlr5g43b98q9vq9xrr9bjliydi1ph74jiykm")))

(define-public crate-fn_once-0.2.0 (c (n "fn_once") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14q92wlx0vglwbphlrs6davmydjm6w3fy2121k5bry4vphchwhx3")))

(define-public crate-fn_once-0.3.0 (c (n "fn_once") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ygi65sklvq7bf0d71bdymwk5qdffdsqy3mphcdlsfix5qwx5g1n")))

