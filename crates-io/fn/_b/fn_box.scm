(define-module (crates-io fn _b fn_box) #:use-module (crates-io))

(define-public crate-fn_box-1.0.0 (c (n "fn_box") (v "1.0.0") (h "1zrgr9raq829rkx2cqj2m2dc4impm4qp2sv8f3sid7kqdy1q71qy") (y #t)))

(define-public crate-fn_box-1.0.1 (c (n "fn_box") (v "1.0.1") (h "1m2rry2qkm5nsbl8m9y6wmyywrxgkjfcds8x9s35k7idr5yjlxx9") (y #t)))

(define-public crate-fn_box-1.0.2 (c (n "fn_box") (v "1.0.2") (h "1cs7cskk94bmg9wr4vjy2w7vjps7p3cpxj3vgarif37slw1b4zh3") (y #t)))

(define-public crate-fn_box-1.0.4 (c (n "fn_box") (v "1.0.4") (h "0fx35da1mjgahynfb0x1r993cflgwk1whxwml98xkjk8c1iwxi1c") (y #t)))

(define-public crate-fn_box-1.0.5 (c (n "fn_box") (v "1.0.5") (h "1181y77cchmsj5va7bfprh5xmph7i9d9zrznbbxs9cmdm78r7yvh") (y #t)))

