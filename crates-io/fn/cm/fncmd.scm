(define-module (crates-io fn cm fncmd) #:use-module (crates-io))

(define-public crate-fncmd-1.0.0 (c (n "fncmd") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "fncmd-impl") (r "^1.0.0") (d #t) (k 0)))) (h "1v4qam1xmiaf4xfng56j048mdzx0zs5c7vk4wavvzb2xdqg7b8fv")))

(define-public crate-fncmd-1.0.1 (c (n "fncmd") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "fncmd-impl") (r "^1.0.1") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "1ym7xlfk6v0kg32q1ra5n6fvgr55h73bqv84r4qv9qw4zx4c2fgi")))

(define-public crate-fncmd-1.0.2 (c (n "fncmd") (v "1.0.2") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "fncmd-impl") (r "^1.0.2") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "0h51fpzyi4r4zm2pgavgpaaglsckcpd2nbx5d63z275x6rm5cc3z")))

(define-public crate-fncmd-1.1.0 (c (n "fncmd") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.1.0") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "1x2bwa51wkmzld63rrbdlsy2wps27m0157m359a76cld2klg3n6s")))

(define-public crate-fncmd-1.1.1 (c (n "fncmd") (v "1.1.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.1.1") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "0lm44nbjv1ginl37p4ffx5sg0w5nrzp275ss1pd5abr072dcd5jd")))

(define-public crate-fncmd-1.2.0 (c (n "fncmd") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.0") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "1mdkif74nvzlaskpfb8dlng9712gsd944aafn2x8kb5miqxc8v93")))

(define-public crate-fncmd-1.2.1 (c (n "fncmd") (v "1.2.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.1") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "1v1hdab6i7q68wav7mr7my4b8kby0ad5dnib9wgki8p9b7l80b0m")))

(define-public crate-fncmd-1.2.2 (c (n "fncmd") (v "1.2.2") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.2") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)))) (h "1is8ldr52q438a1l60mr70kpa0px5ks42s6klijk3hz28hszhwh9")))

(define-public crate-fncmd-1.2.3 (c (n "fncmd") (v "1.2.3") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.3") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1kj4k1wn9ckgqdgm75ha2xf89x5afqqhhfs7b7qy5pl5biyybam0")))

(define-public crate-fncmd-1.2.4 (c (n "fncmd") (v "1.2.4") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.4") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0jpkigznjd6wapygrgavl211nafr18pjhkza7927l06mxc2marak")))

(define-public crate-fncmd-1.2.5 (c (n "fncmd") (v "1.2.5") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.2.5") (d #t) (k 0)) (d (n "git_hooks") (r "^0.1.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "126fvd99215plbjdzs6ywd9xs6hh8cmlk8ll3cm9kn9lj698r8sj")))

(define-public crate-fncmd-1.3.0 (c (n "fncmd") (v "1.3.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "08x65160528jhrqn2zszjjijsjbghng8aj61jgxssysymdbgsip7")))

(define-public crate-fncmd-1.3.1 (c (n "fncmd") (v "1.3.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=1.3.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "04lrln9w4bg2c1hjfxw1w49l0n7437wd1kwk5g9x7aqdajax200c")))

(define-public crate-fncmd-2.0.0 (c (n "fncmd") (v "2.0.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0mjba18l4j03l49njay8bx3p757x4z2z1igsm6gi3fgr3n3xa08s")))

(define-public crate-fncmd-2.0.1 (c (n "fncmd") (v "2.0.1") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=2.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.16") (f (quote ("examples"))) (d #t) (k 2)))) (h "04b6kns4qfmwa0cvbcy9pb6r77w533vki0ysyycwr6mv6n49ng6a")))

(define-public crate-fncmd-2.1.0 (c (n "fncmd") (v "2.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncmd-impl") (r "=2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.16") (f (quote ("examples"))) (d #t) (k 2)))) (h "08s85x2whc5bwsk6cck6d96jzyydiqbgwap28vfrvaanzy24mvip")))

