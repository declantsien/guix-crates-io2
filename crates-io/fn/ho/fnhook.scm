(define-module (crates-io fn ho fnhook) #:use-module (crates-io))

(define-public crate-fnhook-0.1.0 (c (n "fnhook") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "dynasm") (r "^2.0.0") (d #t) (k 0)) (d (n "dynasmrt") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "libproc") (r "^0.14.2") (d #t) (k 0)) (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "mach_object") (r "^0.1.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "system_error") (r "^0.1.1") (d #t) (k 0)))) (h "07q4njxxv672mz2l3sgnw03927vz838xas7qsj0pij5c646pmfyj")))

