(define-module (crates-io fn ic fnichol-cime) #:use-module (crates-io))

(define-public crate-fnichol-cime-0.1.1-dev (c (n "fnichol-cime") (v "0.1.1-dev") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "111v4525820cx2q3dhd0w03r1py0yc26jgrjxgqrg1rw3vbycphq") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.2.0 (c (n "fnichol-cime") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0qbsfcjpyvk4kqvf5lbg63dcdkp4dlccj2x0mk2b6vp55qjcys1z") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.4.0 (c (n "fnichol-cime") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1a7avn42zzpbbdmak5cisv98bf6sx6c2y2p479ybnhpp29qzf651") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.5.0 (c (n "fnichol-cime") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "02c2r2anm71wb02i38fnaxv41bda1nyj98wh3ycna0lb7kxlgwxn") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.6.0 (c (n "fnichol-cime") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0dx813z7h7ziiadz7jnfxq8lv2wspkxr5sl8ky34xm1kx5qvf8zr") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.7.0 (c (n "fnichol-cime") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0q6am1ywyfp7rnsw9jsppxm9ncd1xa894h3rn7qfm5xy4nny6dcf") (f (quote (("default" "application") ("application" "clap"))))))

