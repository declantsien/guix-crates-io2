(define-module (crates-io fn dg fndg) #:use-module (crates-io))

(define-public crate-fndg-0.1.0 (c (n "fndg") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)))) (h "1qadsikp98jmnxnwnbnjj5k3ps5ibgmvmrc4lvpaxrin2jm6f9nz")))

(define-public crate-fndg-0.2.0 (c (n "fndg") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bracket-noise") (r "^0.8.2") (d #t) (k 0)) (d (n "hex2d") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0f9j7g928vakl0a3nwq72il94f2im11080mpnr91w93wwfsmgnrs")))

(define-public crate-fndg-0.3.0 (c (n "fndg") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bracket-noise") (r "^0.8.2") (d #t) (k 0)) (d (n "hex2d") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0brjci45na0y647pjk5yk2h4syhk09sy3zsji05zbm9dw3pq4xsr")))

(define-public crate-fndg-0.4.0 (c (n "fndg") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bracket-noise") (r "^0.8.2") (d #t) (k 0)) (d (n "hex2d") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "03rvzibfbyz1zdglzk0hnhyzzzgdikbfdw6lk352c293hjs7d6y3")))

(define-public crate-fndg-0.4.1 (c (n "fndg") (v "0.4.1") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bracket-noise") (r "^0.8.2") (d #t) (k 0)) (d (n "hex2d") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0qrj1dgl0m09gh71n495dkcnx9a5yx7r72sq25b9qfbbwwwn8jy3")))

