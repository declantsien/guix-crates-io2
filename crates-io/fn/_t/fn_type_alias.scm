(define-module (crates-io fn _t fn_type_alias) #:use-module (crates-io))

(define-public crate-fn_type_alias-0.1.0 (c (n "fn_type_alias") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07vcrh975yvh5aqf3z3i77pvawb41k8ypglzjn4iyzr105zq17l0")))

