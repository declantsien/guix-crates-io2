(define-module (crates-io fn _m fn_meta) #:use-module (crates-io))

(define-public crate-fn_meta-0.1.0 (c (n "fn_meta") (v "0.1.0") (h "1wz68mvhs166ackg5abdailqp2hvmkq8af3w3988r7hwya99006y")))

(define-public crate-fn_meta-0.2.0 (c (n "fn_meta") (v "0.2.0") (h "18mh8ly6fly5y9h28kddyc9v5s4sbw1ynjvfbfdcsxsl8n7y8vp7")))

(define-public crate-fn_meta-0.3.0 (c (n "fn_meta") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "0k9cz8k51bl82hh42910pl760bl3dkw58fhwq2xq70gqmcxyp906") (f (quote (("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.4.0 (c (n "fn_meta") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "0miikjfp82is82xqwi8va23mycpvqz118ksrchrk4v9im6990iqp") (f (quote (("high_arg_count") ("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.4.1 (c (n "fn_meta") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "0ji392jbhcc9p42bk0kaf4pypwgpl2d4jkgvg3k997y5yvgmjfp1") (f (quote (("high_arg_count") ("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.5.0 (c (n "fn_meta") (v "0.5.0") (d (list (d (n "smallvec") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1m3r27xrijvihhmcz7jlsbv0g2w1dsp4jc2i6kwhvjxbqp5nmvsg") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.6.0 (c (n "fn_meta") (v "0.6.0") (d (list (d (n "smallvec") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0vbci77x7nplrfkqaqpcgcvrj7bar8k8my73pap92g3zgnx66bml") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7.0 (c (n "fn_meta") (v "0.7.0") (d (list (d (n "smallvec") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "18my0yk9y80a38f0xh5rfxf242pq2h5s5s56rlk759d06lshl7qq") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7.1 (c (n "fn_meta") (v "0.7.1") (d (list (d (n "smallvec") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "00rdzl3jjvrifzvg798v3nslsbkhxrpl8x5ibyx5ap2xgm16l57x") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7.2 (c (n "fn_meta") (v "0.7.2") (d (list (d (n "smallvec") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "0kvwd3a73488h224j895fdwr2xq0kmk7zfc1fw09bxmkb1gwvryp") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7.3 (c (n "fn_meta") (v "0.7.3") (d (list (d (n "smallvec") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "0zqq454x1xvxr8nyrdff2zni1y9wdc9vdfpyxcn308v28qf1bs15") (f (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

