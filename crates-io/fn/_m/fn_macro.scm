(define-module (crates-io fn _m fn_macro) #:use-module (crates-io))

(define-public crate-fn_macro-0.1.0 (c (n "fn_macro") (v "0.1.0") (h "175dwg0q64pcsk5m6wcw0451ps5mhp0vbcdyrb6rpfw30dydrvk9")))

(define-public crate-fn_macro-0.1.1 (c (n "fn_macro") (v "0.1.1") (h "0lmm2vgb0p4cw9k55y3kcqv5blrz24dg9m2ihq336l1f7cqy31gz")))

(define-public crate-fn_macro-0.1.2 (c (n "fn_macro") (v "0.1.2") (h "0ahbh100m2jr7vygyfs6ba7rp0fmpbfp1g0daalgcp38yl4cmfif")))

