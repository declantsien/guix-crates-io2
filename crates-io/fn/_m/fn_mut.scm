(define-module (crates-io fn _m fn_mut) #:use-module (crates-io))

(define-public crate-fn_mut-0.1.0 (c (n "fn_mut") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0l1bx7v0rrd044syamp9gjkprrkbjpfh7ls6ha15m5ghs2x4hbxf")))

