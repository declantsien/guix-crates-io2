(define-module (crates-io fn lo fnlog) #:use-module (crates-io))

(define-public crate-fnlog-0.1.0 (c (n "fnlog") (v "0.1.0") (d (list (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 2)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "stdext") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "1h28b8809ip9pfdf1x8a0b95jk785arhpplil3cr8kx07k63bqqs")))

(define-public crate-fnlog-0.1.1 (c (n "fnlog") (v "0.1.1") (d (list (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 2)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "stdext") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "1985n9dxajq1yjg65zlmfxh70rf529pyl4mm0xjx9hrhjwzhhgkf")))

(define-public crate-fnlog-0.1.2 (c (n "fnlog") (v "0.1.2") (d (list (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 2)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "stdext") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "08dnp2bj2hxlawhpvmv1dy2c2917rvpiwb175pg864m9h8m9zpx3")))

