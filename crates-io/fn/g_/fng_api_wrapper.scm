(define-module (crates-io fn g_ fng_api_wrapper) #:use-module (crates-io))

(define-public crate-fng_api_wrapper-1.0.0 (c (n "fng_api_wrapper") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "16d6dhh7k2h2x8s7giaivz725s859q42zq6rdyj7j5imh54j4f2v")))

(define-public crate-fng_api_wrapper-1.0.1 (c (n "fng_api_wrapper") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.23") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0yazwbnadrn1h7g3d0l9m4snmpjnqdilg2cng3nginasxbb83m3j")))

