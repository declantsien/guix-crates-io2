(define-module (crates-io fn -m fn-map) #:use-module (crates-io))

(define-public crate-fn-map-0.1.0 (c (n "fn-map") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "0irqb7g1gqzxfja8n88lf85lkc00q652bgvc87j9vg0h7m731mbf")))

