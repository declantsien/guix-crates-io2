(define-module (crates-io fn -m fn-memo) #:use-module (crates-io))

(define-public crate-fn-memo-0.1.0 (c (n "fn-memo") (v "0.1.0") (d (list (d (n "recur-fn") (r "^0.1.0") (d #t) (k 0)))) (h "1h703m8hic7zh1sz77489l21ix4r9p79p2vsix186b3pdl9dv7x9")))

(define-public crate-fn-memo-0.2.0 (c (n "fn-memo") (v "0.2.0") (d (list (d (n "chashmap") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "recur-fn") (r "^0.2.2") (d #t) (k 0)))) (h "1s5493mq4phvkj2fsj1nn903ggcjkd75w6f5y8r01fqzf19k2k35") (f (quote (("sync" "once_cell" "chashmap") ("default" "sync"))))))

(define-public crate-fn-memo-1.0.0 (c (n "fn-memo") (v "1.0.0") (d (list (d (n "chashmap") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "recur-fn") (r "^1.1.0") (d #t) (k 0)))) (h "1wfd93gilbpm54l8xslqp2l7d51f94n8n7nfhhp278yv7x5jw21h") (f (quote (("sync" "once_cell" "chashmap") ("default" "sync")))) (y #t)))

(define-public crate-fn-memo-1.1.0 (c (n "fn-memo") (v "1.1.0") (d (list (d (n "chashmap") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "recur-fn") (r "^1.2.0") (d #t) (k 0)))) (h "01bf9wp54sg4g7b47zp63madlgkb642za9cwbk351gz75zc3ajm3") (f (quote (("sync" "once_cell") ("default" "concurrent_hash_map") ("concurrent_hash_map" "chashmap" "sync"))))))

(define-public crate-fn-memo-1.1.1 (c (n "fn-memo") (v "1.1.1") (d (list (d (n "chashmap") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "recur-fn") (r "^1.2.0") (d #t) (k 0)))) (h "0wn4i63ggika71ralyhqzs3njdhccriwhk7a2n3d9vyyh86h7jaz") (f (quote (("sync" "once_cell") ("default" "concurrent_hash_map") ("concurrent_hash_map" "chashmap" "sync"))))))

(define-public crate-fn-memo-1.2.0 (c (n "fn-memo") (v "1.2.0") (d (list (d (n "chashmap") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "recur-fn") (r "^2.1") (d #t) (k 0)))) (h "1vfnr9xb4774z6gg3qbkblh6c0768bqb1nk6yc5h6k50mxc9cvah") (f (quote (("sync" "once_cell") ("default" "concurrent_hash_map") ("concurrent_hash_map" "chashmap" "sync"))))))

