(define-module (crates-io fn -s fn-sdk-macros) #:use-module (crates-io))

(define-public crate-fn-sdk-macros-0.0.0 (c (n "fn-sdk-macros") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02j6gzqwx7qm9ng562vpsp76bv01jvfq4mdkmm7mayyhmqnkbjsb")))

