(define-module (crates-io fn -s fn-store) #:use-module (crates-io))

(define-public crate-fn-store-0.1.0 (c (n "fn-store") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "19554vp86l1am1mq5bkqlfkwnzqa8fsbvm1fzagb6gszxk76cwg6")))

(define-public crate-fn-store-0.1.1 (c (n "fn-store") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "0vrklk6slb5dp4vwqldr8fi08i546d1fqk69z6yr5vwww82xwsyy")))

(define-public crate-fn-store-0.1.2 (c (n "fn-store") (v "0.1.2") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "1z4x2vj0vd0mnnvinvv8p5j3as0bv0vqmw28jmigspf2pqi1kwbh") (y #t)))

(define-public crate-fn-store-0.1.3 (c (n "fn-store") (v "0.1.3") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "08p4gnjvl6mlx1w7byzs3rzbi0rrfk0xlhvv5w94wwr33a52i3w1") (y #t)))

(define-public crate-fn-store-0.1.4 (c (n "fn-store") (v "0.1.4") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "0a52a748fqfxcvazca8f1yw57vscfazbgxcl2995a37ia06rwsyk")))

(define-public crate-fn-store-0.1.5 (c (n "fn-store") (v "0.1.5") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "1wa28zakyjpg6i50shkp5sa31qgnmzxw16va0yz4hna5fp3b6973")))

(define-public crate-fn-store-0.2.0 (c (n "fn-store") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "12bqx6h4ll2wlsk7zhk5l43h8i6h7d3n284gckgfzlrhppr2v0yr")))

(define-public crate-fn-store-0.3.0 (c (n "fn-store") (v "0.3.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "008wfdpgafgvblwhdih1g322ahyb0c36sjyjbbqv1iq5y3r92jn8")))

(define-public crate-fn-store-0.3.1 (c (n "fn-store") (v "0.3.1") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "0qdm2d49cw1lypby280pczpjay8dwi0m2l90pa0brw3sczwzzqw9")))

(define-public crate-fn-store-1.0.0 (c (n "fn-store") (v "1.0.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "062c5qwawmjkrmhflyjdk66m30kla94m3qik2py29v0p1p2wflsx")))

(define-public crate-fn-store-1.1.0 (c (n "fn-store") (v "1.1.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "1b5v22izx4ipb1gj4xq0s8j0hby5mrsbs7chla9bpgafi59p8n30")))

(define-public crate-fn-store-1.2.0 (c (n "fn-store") (v "1.2.0") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "0h16kamyfsd6h311qzg838nyc189986c4m1czbic3c8fsks3qq45") (y #t)))

(define-public crate-fn-store-1.2.1 (c (n "fn-store") (v "1.2.1") (d (list (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "type-key") (r "^1") (d #t) (k 0)))) (h "1mhc9pw1qwl0ippymlan3azyav1qjy2w4rdlg5707wkmhbbkby1k")))

