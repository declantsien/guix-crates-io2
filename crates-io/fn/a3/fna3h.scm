(define-module (crates-io fn a3 fna3h) #:use-module (crates-io))

(define-public crate-fna3h-0.1.0 (c (n "fna3h") (v "0.1.0") (d (list (d (n "fna3d") (r "^0.1.3") (d #t) (k 0)))) (h "0wpkl5f5sa7xp8kim71f73112rw7rnzlqngjhvmq2bgkamyb59in")))

(define-public crate-fna3h-0.1.1 (c (n "fna3h") (v "0.1.1") (d (list (d (n "fna3d") (r "^0.1.4") (d #t) (k 0)))) (h "0cj598j0gcpx29pxqll7zd2zck82v94g5as0krbd1qx9qlg9lsqg")))

(define-public crate-fna3h-0.1.2 (c (n "fna3h") (v "0.1.2") (d (list (d (n "fna3d") (r "^0.1.5") (d #t) (k 0)))) (h "0cb8hdmml5bwgxgbhar1vqjp720bngib40fclqqazvxssg58qsrw")))

