(define-module (crates-io fn a3 fna3d-sys) #:use-module (crates-io))

(define-public crate-fna3d-sys-0.1.0 (c (n "fna3d-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1dbk57vksi7l07fljggi3mpj6lf1k28hfisvav7jppri7jjn3kir") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.1 (c (n "fna3d-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0rps8vmspmavcwzs60rynvy5xi0gq987kjcl38brz10nrp4d7hfg") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.2 (c (n "fna3d-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vkw70rkk39h0104g3i0hxasmarvq7f2m6cj9sg18nqasyjkb6az") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.3 (c (n "fna3d-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1hkwfqzfq2gj25qm0rp0ymjgdhxmfyj5jlz1kcwi4nl4krwyks5a") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.4 (c (n "fna3d-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1pzhknglnnkpw18mk1w23n6pi0ybxa0c6yjy2g9s4k2qhh4f0x5i") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.5 (c (n "fna3d-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1f0lphb6fkcfnima4h41y16iqavlfjkygh41b2zpk6gx6z8mqzjk") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.6 (c (n "fna3d-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wkz1scrp32lgbhqqjmnwh0zl0y6d7nqzjvzaiz28b5gv1gyhfg2") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.7 (c (n "fna3d-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17s02m6jjrz669fqr6pbmlyv7dhqwc64md4yqrbdw30afl1n2z8k") (l "fna3d")))

(define-public crate-fna3d-sys-0.1.8 (c (n "fna3d-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vk5l6ckccgl878b8f6wmfrf50idkvx8mx8hslc80abv6yimdifm") (l "fna3d")))

