(define-module (crates-io fn ew fnew) #:use-module (crates-io))

(define-public crate-fnew-1.0.0 (c (n "fnew") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1a1k7a8bpnirv34qqf7fsslj5r43y0fs9dj51nj4gsn1qkc66mim")))

(define-public crate-fnew-1.0.1 (c (n "fnew") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0v4clk94jkhpd880xbdjaqwdjfq3j8ra5kgjpqli8nvnr3c0zpww")))

