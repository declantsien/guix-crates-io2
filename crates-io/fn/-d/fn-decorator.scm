(define-module (crates-io fn -d fn-decorator) #:use-module (crates-io))

(define-public crate-fn-decorator-1.0.0 (c (n "fn-decorator") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "0jsp1jqggay072k8wfg2dqvd79x7807wc3c1g5vfb2cl8q8w4677")))

(define-public crate-fn-decorator-1.0.1 (c (n "fn-decorator") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "0qqvay3326141lhhf9s19ld5m3w44260vqkwy395yjxzwil80ryk")))

(define-public crate-fn-decorator-1.1.0 (c (n "fn-decorator") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "0qdhw2xx1v344k4hyw2xyjp5d68vz9zjyc3j0xzfbh6xpfq5dzmc")))

(define-public crate-fn-decorator-1.1.1 (c (n "fn-decorator") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "1a6kspn9lm2c3r4g9pr2lbh0pznlkp2wqkz3bnscmpwyykgkzlvx")))

(define-public crate-fn-decorator-1.1.2 (c (n "fn-decorator") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "08ilkxjbdw2zsqbhrm5xckxzh6ps8slkgg7laxj697c1qz6yy8ck")))

(define-public crate-fn-decorator-1.2.0 (c (n "fn-decorator") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "1h2rrjzbaf6xnbb5h672iahm8j1ah0gz2ahkh85nf4l03095z16b")))

(define-public crate-fn-decorator-1.2.1 (c (n "fn-decorator") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "09nsy748r5xhkmffwzqzlh49033wln9i5s5dj0i6z2ycjylb28z0")))

(define-public crate-fn-decorator-1.3.0 (c (n "fn-decorator") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "1m4bmdqfij369jdaix83x00hrli0a0k5kh9v6x0z8sva4ihcyvdc")))

(define-public crate-fn-decorator-1.3.1 (c (n "fn-decorator") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "1pqpwk1lddnf88k5yrn7kp6wsylhr5fbihig4dmlsj0rqjcf9dmr")))

(define-public crate-fn-decorator-1.3.2 (c (n "fn-decorator") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "12vpyhpwa8bysyaz81nkg10layd3k4n3gcyhhrbvwq3ykw5v2kiw")))

