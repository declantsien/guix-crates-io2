(define-module (crates-io fn -c fn-cache) #:use-module (crates-io))

(define-public crate-fn-cache-0.1.0 (c (n "fn-cache") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "1xj0a9vqir5p6mj2dccmwyxmgi50gqcyac2094q3yj05xvwvv67x")))

(define-public crate-fn-cache-0.1.1 (c (n "fn-cache") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "00sxygx3gxmzhzamz91dpv3rad1mmgsykw36jkmwpc2yl58p2n51")))

(define-public crate-fn-cache-0.2.0 (c (n "fn-cache") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "0jshj3pkgpy6lnjcqr9jx97zksx4jc2p048734kni2zi3ysqf83y")))

(define-public crate-fn-cache-0.3.0 (c (n "fn-cache") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "0wjpwbas3qmni9q9sjs1yf9mnxy7i1m5xwz4529kcv1kw9qmvbv3")))

(define-public crate-fn-cache-0.4.0 (c (n "fn-cache") (v "0.4.0") (d (list (d (n "hashers") (r "^1.0.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "0nh4kgx6m4wn4q3g18c4m2qk25cf4cndk59k0yalrlsa51kvvasx")))

(define-public crate-fn-cache-0.4.1 (c (n "fn-cache") (v "0.4.1") (d (list (d (n "hashers") (r "^1.0.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "1n5rfid7djw521lz36sysxx97hzv821vs0g6zqjpqskv2khiq77c")))

(define-public crate-fn-cache-0.5.0 (c (n "fn-cache") (v "0.5.0") (d (list (d (n "hashers") (r "^1.0.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)))) (h "0ii0fbw6h9pzl6wkvcmdf6lzm6brlka2vxvqh6b48rzg4msz92hm")))

(define-public crate-fn-cache-1.0.0 (c (n "fn-cache") (v "1.0.0") (d (list (d (n "hashers") (r "^1.0.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)))) (h "1y0gv6k8w1c8sglyrkb0bavmmlcffljdjflir20w2rha9k19dwqf")))

(define-public crate-fn-cache-1.1.0 (c (n "fn-cache") (v "1.1.0") (d (list (d (n "hashers") (r "^1.0.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)))) (h "14qcdy9ay07s2jm158xvw7l80kblxnlhzmafxyx3z4sp20g2a9lh")))

(define-public crate-fn-cache-1.1.1 (c (n "fn-cache") (v "1.1.1") (d (list (d (n "hashers") (r "^1.0.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)))) (h "13ifky861wxdzl7l20jmq8rjbr58gdhnxmrv8z9xzwk1y506cbbr")))

