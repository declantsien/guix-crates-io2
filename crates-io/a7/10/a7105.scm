(define-module (crates-io a7 #{10}# a7105) #:use-module (crates-io))

(define-public crate-a7105-0.1.0 (c (n "a7105") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "maybe-async") (r "^0.2") (d #t) (k 0)))) (h "090ckqnmfb58xc4k4iqj3zmz92mz90wzcspd4g5mphjgnylyymx6") (f (quote (("default" "async") ("blocking" "embedded-hal" "maybe-async/is_sync") ("async" "embedded-hal-async"))))))

