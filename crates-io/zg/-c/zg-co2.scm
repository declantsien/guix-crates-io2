(define-module (crates-io zg -c zg-co2) #:use-module (crates-io))

(define-public crate-zg-co2-0.1.0-alpha.1 (c (n "zg-co2") (v "0.1.0-alpha.1") (d (list (d (n "assert_float_eq") (r "^1.1") (k 2)))) (h "1f3jn8iv13m22an5gxhylfihwbyql0m3wspmmwinmc8xdnk3s4h2") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-zg-co2-0.1.0-alpha.2 (c (n "zg-co2") (v "0.1.0-alpha.2") (d (list (d (n "assert_float_eq") (r "^1.1") (k 2)))) (h "0p2z9zwj1fb5gghs6px0wqwn0jdz5v29hndbk2d1pinn0x40nlwk") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1.0.0-rc.1 (c (n "zg-co2") (v "1.0.0-rc.1") (d (list (d (n "assert_float_eq") (r "^1.1") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "117wmh7qr1sgnbm4ncscv9dmwxmkqsjlm9s709w4xrs5s49d9j2k") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1.0.0-rc.2 (c (n "zg-co2") (v "1.0.0-rc.2") (d (list (d (n "assert_float_eq") (r "^1.1") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "19y30fdyblksaas0nmgwj1mbj20qnn2kfhwl3bczrl1viy2f4k5h") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1.0.0 (c (n "zg-co2") (v "1.0.0") (d (list (d (n "assert_float_eq") (r "^1.1") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0f2yxn3idixrb2jqy00njxgw2py7jz3a15ssjzr7bavfa1a3zffg") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2.0.0 (c (n "zg-co2") (v "2.0.0") (h "1wvqkmhw50k5mn8zbahqlp2r24b9m8mhq0n9n0qhj7ymhx5wis4b") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2.0.1 (c (n "zg-co2") (v "2.0.1") (h "1wl0bm50irq0yhkzaps2qm6mj50k2bmrfkzbzfqn2j5gwcpmvvmj") (f (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2.1.0 (c (n "zg-co2") (v "2.1.0") (h "0anczj35zg2h8fsf8kjw772k33yabayphgbjv80fmh4ykm8dzafk") (f (quote (("std") ("default" "std"))))))

