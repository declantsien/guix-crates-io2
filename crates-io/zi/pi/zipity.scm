(define-module (crates-io zi pi zipity) #:use-module (crates-io))

(define-public crate-zipity-0.0.1 (c (n "zipity") (v "0.0.1") (h "1acwm15cpa7hh5qdl3ifvnqln2mm5n49r37gs5zxgc4zl57d3h20") (y #t)))

(define-public crate-zipity-0.0.1-alpha.1 (c (n "zipity") (v "0.0.1-alpha.1") (h "15hb38ycyhsbfh8j3i2ghb09aq0yjwv35cxn7h5lxw0r8a1xrd6y") (y #t)))

(define-public crate-zipity-0.0.1-alpha.2 (c (n "zipity") (v "0.0.1-alpha.2") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (d #t) (k 0)) (d (n "actix-web-httpauth") (r "^0.8.0") (d #t) (k 0)) (d (n "askama") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "sitemap") (r "^0.4.1") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1wyrsbmx7xlc5a61r7nb9985vnavj3vssdg5id7m46xgyz5333rl")))

