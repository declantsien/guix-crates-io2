(define-module (crates-io zi pi zipimgzip) #:use-module (crates-io))

(define-public crate-zipimgzip-0.1.0 (c (n "zipimgzip") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0rhf6dc09xzs2m11jr53599lav6lfgzbdayvzaf0s6armvjlliva")))

(define-public crate-zipimgzip-0.1.1 (c (n "zipimgzip") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0a558yakx11md7vacxd6sksb66kpljkqkpsxys9622nfh5m1sj8r")))

(define-public crate-zipimgzip-0.1.2 (c (n "zipimgzip") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1g12rb9khnma3bk2vziy05v8vcp31vj3s9mnv3qcwnkff781f082")))

(define-public crate-zipimgzip-0.2.0 (c (n "zipimgzip") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0hgaccp0wkzpnx3n6k8y17ccqbagvfwsrklmdv72kbj509gsppcv")))

(define-public crate-zipimgzip-0.2.1 (c (n "zipimgzip") (v "0.2.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "07xgz9hfxbgf3ypq5p6wmkx4pah40pwzks0smiy0dxwsxk7iargd")))

(define-public crate-zipimgzip-0.2.2 (c (n "zipimgzip") (v "0.2.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1si00zjj8m799cfaapqaabb9l931ng8zl33k6xkz33sgrv6l8lbc")))

(define-public crate-zipimgzip-0.3.0 (c (n "zipimgzip") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1qzipsxxlp7py2lmsi1a4s035daalcd8azzazgrzj7wpxklqgz8q")))

(define-public crate-zipimgzip-0.3.1 (c (n "zipimgzip") (v "0.3.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0zl57al6fmfmdkpn5gy49zmlyq7syapmdwc3f9d0ylkqnxg7asax")))

