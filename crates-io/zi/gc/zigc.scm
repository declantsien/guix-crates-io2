(define-module (crates-io zi gc zigc) #:use-module (crates-io))

(define-public crate-zigc-0.0.0-alpha0.1 (c (n "zigc") (v "0.0.0-alpha0.1") (d (list (d (n "osstrtools") (r "^0.2.2") (d #t) (k 0)))) (h "020pvjy02pjlwf19jiq8ndw4r8kqcmlbbn4mx7imjh2xqb1msypi")))

(define-public crate-zigc-0.0.0-alpha0.2 (c (n "zigc") (v "0.0.0-alpha0.2") (d (list (d (n "osstrtools") (r "^0.2.2") (d #t) (k 0)))) (h "0r5yrsp9d61xl940nvfwr80d8nyrvxn61nl9cyf2cl85q5983800")))

(define-public crate-zigc-0.0.1 (c (n "zigc") (v "0.0.1") (d (list (d (n "osstrtools") (r "^0.2.2") (d #t) (k 0)))) (h "0i0vx5hp80pcy2v7riphv20hbszvwx7vy5k81d8kr0ck0alcqpx1")))

(define-public crate-zigc-0.0.2 (c (n "zigc") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "osstrtools") (r "^0.2.2") (d #t) (k 0)))) (h "1c57d5sqqld85dk8ydhhc7hgaiqdqn0k4znd13z48n8ii81jkmki") (f (quote (("log" "chrono"))))))

(define-public crate-zigc-0.0.3 (c (n "zigc") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "osstrtools") (r "^0.2.2") (d #t) (k 0)))) (h "0gkhjy5mi2qdk0chbsw2p4hvp5a8vvxva9m2af9vvnpykkqfpmfr") (f (quote (("log" "chrono"))))))

