(define-module (crates-io zi sv zisvalidator_derive) #:use-module (crates-io))

(define-public crate-zisvalidator_derive-0.1.0 (c (n "zisvalidator_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "0c656jcirqc5an355a3v18wsr8dvqq9pmm09g8r9byg7h6fr6cgg")))

(define-public crate-zisvalidator_derive-0.1.2 (c (n "zisvalidator_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "0qqb427vpyqghac5dl9b0m68ayrxyka1mcgs5rhrr4nx25g9l4p7")))

(define-public crate-zisvalidator_derive-0.1.3 (c (n "zisvalidator_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "0s75xspzz3z1nf54f8ysh4fqws382bpgd2rlghx9gkvhhcqgszcq")))

(define-public crate-zisvalidator_derive-0.1.4 (c (n "zisvalidator_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "1nn3smmmr7ybmsd2ipm8k566yqr3bki50i78adac6ln4sb1zpbl4")))

(define-public crate-zisvalidator_derive-0.1.5 (c (n "zisvalidator_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "1dskpg39djv9v1qgnkdlwpb4k9nfjp2cm314yf2mnffvl2b2bdz4")))

(define-public crate-zisvalidator_derive-0.1.6 (c (n "zisvalidator_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "16jnzh7fcl702xdrbpch7121wb1kshiy4w99vjrjwgqyzl9asa5p")))

