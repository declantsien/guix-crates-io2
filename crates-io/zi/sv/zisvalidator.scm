(define-module (crates-io zi sv zisvalidator) #:use-module (crates-io))

(define-public crate-zisvalidator-0.1.0 (c (n "zisvalidator") (v "0.1.0") (h "040n0y5g6m06114ga1xs1hxszyrfs44lnq5749cdfbccccs43b8z")))

(define-public crate-zisvalidator-0.1.1 (c (n "zisvalidator") (v "0.1.1") (d (list (d (n "zisvalidator_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1s4y4bxrb0dil3bdc8mag1s803w404qg42ibn5qw04jfj282zkhr") (f (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1.2 (c (n "zisvalidator") (v "0.1.2") (d (list (d (n "zisvalidator_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "109a90xf67vfjd2np38p2k0gyildrmqv999xr96632yrsbf3lj77") (f (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1.3 (c (n "zisvalidator") (v "0.1.3") (d (list (d (n "zisvalidator_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0cs5bvkb1vb8bhscy7l4k0gwg7km2nwiya1fnyyndmbhi4bfsb28") (f (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1.4 (c (n "zisvalidator") (v "0.1.4") (d (list (d (n "zisvalidator_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0jfx43k72r16bn4ijhh694njdq1hlfbzm7mwnz9rrs61binsrzf0") (f (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1.5 (c (n "zisvalidator") (v "0.1.5") (d (list (d (n "zisvalidator_derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1r2k0pxpvscnrqs6kqay086wjds3q6zq139ckiqqv5d6q5margn7") (f (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1.6 (c (n "zisvalidator") (v "0.1.6") (d (list (d (n "zisvalidator_derive") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "12dwb7jjglpzh0zzdz20xy0xzjb1n81qxqhikl9cxnhppl95s0ik") (f (quote (("derive" "zisvalidator_derive"))))))

