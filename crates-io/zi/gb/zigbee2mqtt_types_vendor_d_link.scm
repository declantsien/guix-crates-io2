(define-module (crates-io zi gb zigbee2mqtt_types_vendor_d_link) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_d_link-0.1.0 (c (n "zigbee2mqtt_types_vendor_d_link") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "074fkhin8c8immrpra6yikrzqw1wi9f6rwbsv8kbhhi4k4926zb6") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

