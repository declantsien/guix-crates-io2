(define-module (crates-io zi gb zigbee2mqtt_types_vendor_stelpro) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_stelpro-0.0.1 (c (n "zigbee2mqtt_types_vendor_stelpro") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a2r2rz7rvn579ax8x42a1zk37vfss1c3wnxmgn1crl27jdxl7by") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_stelpro-0.2.0 (c (n "zigbee2mqtt_types_vendor_stelpro") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0yi0cj796rfk8aqifdh8fkhmym7dwz1kf037zxw98vp137gn36ph") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

