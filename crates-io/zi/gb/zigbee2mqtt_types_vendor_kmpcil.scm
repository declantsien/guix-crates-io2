(define-module (crates-io zi gb zigbee2mqtt_types_vendor_kmpcil) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_kmpcil-0.0.1 (c (n "zigbee2mqtt_types_vendor_kmpcil") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ylhaajgww67623sbs7qblxdibwjlqnix1y387zivhwv5r0r5385") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_kmpcil-0.1.0 (c (n "zigbee2mqtt_types_vendor_kmpcil") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0phnzyizjwj749zq7pxr9ks77ck4442p4azz3iksxccsdwxjmbjr") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

