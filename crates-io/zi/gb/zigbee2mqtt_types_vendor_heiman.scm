(define-module (crates-io zi gb zigbee2mqtt_types_vendor_heiman) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_heiman-0.0.1 (c (n "zigbee2mqtt_types_vendor_heiman") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18hcxxljpzb7fj93k30i2zm3321zcijdfc6xis525vwclbf6xram") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_heiman-0.2.0 (c (n "zigbee2mqtt_types_vendor_heiman") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0rg4xgjc0i61a15svan7c4rinnrdg0icrln8z6px6q1p1b1sddpl") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

