(define-module (crates-io zi gb zigbee2mqtt_types_vendor_niko) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_niko-0.0.1 (c (n "zigbee2mqtt_types_vendor_niko") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09aff3s7xyj5wxlvx4zs9xn65ax6y7cpl5jgh2p09bnrpds9w75k") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_niko-0.2.0 (c (n "zigbee2mqtt_types_vendor_niko") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0h8d2w9d1860gngbbz3sqffd8zdr6l629l6nynyf6ir5zsl11ajz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

