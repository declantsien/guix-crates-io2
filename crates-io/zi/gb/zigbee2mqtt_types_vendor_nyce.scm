(define-module (crates-io zi gb zigbee2mqtt_types_vendor_nyce) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_nyce-0.0.1 (c (n "zigbee2mqtt_types_vendor_nyce") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03zii7d0dpm1vzdbacyxzm45l409gpgfsyf4iq3657xdz8n8y5q8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_nyce-0.1.0 (c (n "zigbee2mqtt_types_vendor_nyce") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0iav3swjkk0ajprfc7vyjd7d8lafr7v5m66qbxg32im8bcfsqs68") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

