(define-module (crates-io zi gb zigbee2mqtt_types_vendor_giderwel) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_giderwel-0.0.1 (c (n "zigbee2mqtt_types_vendor_giderwel") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02alazx5cd3i2x6iqn9jdhnzc4kmjkqxzl5a5bdmqlggj29r86iw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_giderwel-0.2.0 (c (n "zigbee2mqtt_types_vendor_giderwel") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0nbrv4bnrb6gmag8wzbwd98675364j3571xzg63rvj3qryzmpjh3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

