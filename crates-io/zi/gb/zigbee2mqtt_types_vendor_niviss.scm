(define-module (crates-io zi gb zigbee2mqtt_types_vendor_niviss) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_niviss-0.0.1 (c (n "zigbee2mqtt_types_vendor_niviss") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "152cwkd8ahxc3i4svk8f48kdky84k7pnravfx4pbkpzfxn034vrk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_niviss-0.2.0 (c (n "zigbee2mqtt_types_vendor_niviss") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1sxjpj71cdhwak00l3idbdjwspn07xgmn0z9bcp45x6g05qx9629") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

