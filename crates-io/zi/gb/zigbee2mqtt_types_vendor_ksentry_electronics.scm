(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ksentry_electronics) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ksentry_electronics-0.0.1 (c (n "zigbee2mqtt_types_vendor_ksentry_electronics") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aww20dahnrp14lgzxvcgwm902ha1vvbkn3vl33mklnp0b3mwf5z") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ksentry_electronics-0.2.0 (c (n "zigbee2mqtt_types_vendor_ksentry_electronics") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0fcyrdz6qq68p75ahzrnq61ihh3znxsaz72qdv0phydwh34sr4w2") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

