(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lellki) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lellki-0.0.1 (c (n "zigbee2mqtt_types_vendor_lellki") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r7r4dnnvp5v1r1pky5fjkzgg59xxa1hd4bd11x8in8if1rmqq5g") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lellki-0.1.0 (c (n "zigbee2mqtt_types_vendor_lellki") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1l9r3r6jg5dm0jamaarwbmcpsnsx0daxg84lcapssmidk87zc9ym") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

