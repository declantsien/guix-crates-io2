(define-module (crates-io zi gb zigbee2mqtt_types_vendor_kami) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_kami-0.0.1 (c (n "zigbee2mqtt_types_vendor_kami") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "044p2ijpsdj3bc0kxs7chwpzxp0q2pfyss15a1dz2l7bz6sc9197") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_kami-0.2.0 (c (n "zigbee2mqtt_types_vendor_kami") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "11hig5yqdrn2r4hhlw7g4pjb9pjkkyn83fjn32idd6vkqkr9k7cq") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

