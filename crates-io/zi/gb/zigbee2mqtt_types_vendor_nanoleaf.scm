(define-module (crates-io zi gb zigbee2mqtt_types_vendor_nanoleaf) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_nanoleaf-0.0.1 (c (n "zigbee2mqtt_types_vendor_nanoleaf") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "159h5iz9crygps2wqkvrcsclhf8y6c2cqmpphxaib7njgm482x6k") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_nanoleaf-0.2.0 (c (n "zigbee2mqtt_types_vendor_nanoleaf") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "19s9m1y3ncnwmyf6qdj07jvcwwr2yqviil550xkl7mni1kfwvfhx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

