(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hommyn) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hommyn-0.0.1 (c (n "zigbee2mqtt_types_vendor_hommyn") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p4zyzvb3am08s298d1xdl390pjc4c6nhwmz9q3ivv5xfqf2i3ys") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hommyn-0.1.0 (c (n "zigbee2mqtt_types_vendor_hommyn") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1rzpd0f6r61j53nc5hysqh528x9q2imn1rkn1x23zs8awwsjy8dv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

