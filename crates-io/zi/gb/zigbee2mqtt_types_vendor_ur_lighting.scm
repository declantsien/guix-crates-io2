(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ur_lighting) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ur_lighting-0.0.1 (c (n "zigbee2mqtt_types_vendor_ur_lighting") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fkygimq4ksf61nb9zxdws0wwn5sppqvf6vg2bi1fzxd468xb4as") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

