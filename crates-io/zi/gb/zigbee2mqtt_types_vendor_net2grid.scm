(define-module (crates-io zi gb zigbee2mqtt_types_vendor_net2grid) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_net2grid-0.0.1 (c (n "zigbee2mqtt_types_vendor_net2grid") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11xakw4vmzb4x8za47my611pigm8x4sig3gfpiy6hck2jnnps6lz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_net2grid-0.1.0 (c (n "zigbee2mqtt_types_vendor_net2grid") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "04n5vif0rabjfddvdlbvl1f408zvikmb1ldynqypvpd44vflg6xv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

