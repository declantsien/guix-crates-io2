(define-module (crates-io zi gb zigbee2mqtt_types_vendor_rademacher) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_rademacher-0.0.1 (c (n "zigbee2mqtt_types_vendor_rademacher") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v3ipj01vm5na4w2c30pani0fhvmpy6vx1i9mw4lmmscq6aiaaah") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_rademacher-0.2.0 (c (n "zigbee2mqtt_types_vendor_rademacher") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "01h58xvlsvrxglshkfbwxb6l60lfjp3yxhd5bg05m8ny1jkf0ndb") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

