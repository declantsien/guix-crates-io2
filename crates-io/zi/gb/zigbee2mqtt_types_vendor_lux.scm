(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lux) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lux-0.0.1 (c (n "zigbee2mqtt_types_vendor_lux") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k39p2dmi7hf32mwamp276cipchiakwpnb8znn1hpsfck1gchwvj") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lux-0.1.0 (c (n "zigbee2mqtt_types_vendor_lux") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1p5h87218myks88yq6nqj7l03xg4700y4x0kb1l30k4h4rm62vm0") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

