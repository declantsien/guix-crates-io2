(define-module (crates-io zi gb zigbee2mqtt_types_vendor_smart_home_pty) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_smart_home_pty-0.0.1 (c (n "zigbee2mqtt_types_vendor_smart_home_pty") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1549pqcz7bczr7cgzg6458zxanwpk9afcjsi0l6fqhrdydkasr1m") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_smart_home_pty-0.2.0 (c (n "zigbee2mqtt_types_vendor_smart_home_pty") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "177nxhvlk7cz7gxq6h2b1y14611sbf4gbcvpc3fmgvymznc5fa0s") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

