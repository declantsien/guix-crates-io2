(define-module (crates-io zi gb zigbee2mqtt_types_vendor_wally) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_wally-0.0.1 (c (n "zigbee2mqtt_types_vendor_wally") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lv7cc2x3s403hm33f4mqqrq5rwk9nsijja7l0jinjfvdrwk07r0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_wally-0.1.0 (c (n "zigbee2mqtt_types_vendor_wally") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0dxh82mygx2bspqlr3w2jspj1fi7ppcx388aznszc6hyq9magzhf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

