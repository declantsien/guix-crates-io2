(define-module (crates-io zi gb zigbee2mqtt_types_vendor_m_elec) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_m_elec-0.0.1 (c (n "zigbee2mqtt_types_vendor_m_elec") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cvcqbs90qh96rfc5wp0fm2vb07wdm4aaaf559448v5v9smavnza") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_m_elec-0.2.0 (c (n "zigbee2mqtt_types_vendor_m_elec") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0iwningaygac1gvhcai2vvcs1g3rggykzl6czc0yms741m65l4ab") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

