(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hzc) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hzc-0.1.0 (c (n "zigbee2mqtt_types_vendor_hzc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1n5bsc0w1psspplw4i2g3y20lbxp40f1f9c7wlq0rz0z2sf65q1a") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

