(define-module (crates-io zi gb zigbee2mqtt_types_vendor_anchor) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_anchor-0.0.1 (c (n "zigbee2mqtt_types_vendor_anchor") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1arwiscf3842rv2r8ca428vfh5d68k30k3wkn6lxn3zds6y10bj2") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_anchor-0.2.0 (c (n "zigbee2mqtt_types_vendor_anchor") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1m81mlp0jdzav2714cwqn52mah03bwvg5r9jg38k51k5h0ki15al") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

