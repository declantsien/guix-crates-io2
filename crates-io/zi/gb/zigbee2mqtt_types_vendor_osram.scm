(define-module (crates-io zi gb zigbee2mqtt_types_vendor_osram) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_osram-0.0.1 (c (n "zigbee2mqtt_types_vendor_osram") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qs3qcycrdql34h5k5pqk5ydyd09qbvqz566ydihs6qa9add2adv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_osram-0.2.0 (c (n "zigbee2mqtt_types_vendor_osram") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1hkqf2i361dkw3rdibh0vb799jahqgnpxg4bvv26x1m5gdfj44dd") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

