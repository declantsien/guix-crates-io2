(define-module (crates-io zi gb zigbee2mqtt_types_vendor_sunricher) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_sunricher-0.0.1 (c (n "zigbee2mqtt_types_vendor_sunricher") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "121gy0mk84qf661w3ip19nfdfgz7r0c7hx5w5nrvny7na4cq4wzv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_sunricher-0.2.0 (c (n "zigbee2mqtt_types_vendor_sunricher") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0x6xhn1p84ivhj82grc72i5fy1vxmczk2ql1g5d97q88c7xrrhdv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

