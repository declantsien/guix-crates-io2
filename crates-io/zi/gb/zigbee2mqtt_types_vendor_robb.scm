(define-module (crates-io zi gb zigbee2mqtt_types_vendor_robb) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_robb-0.0.1 (c (n "zigbee2mqtt_types_vendor_robb") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d3bw4hg05qiqmqvl4cvbnmikm35a813xsf7cfsn8xa1h9j1j1r9") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_robb-0.2.0 (c (n "zigbee2mqtt_types_vendor_robb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0pijcfx6yxxqrm3faqcp2bal1xalsgy9p7fn88135mnjhrp42p19") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

