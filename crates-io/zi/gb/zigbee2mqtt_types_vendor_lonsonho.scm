(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lonsonho) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lonsonho-0.0.1 (c (n "zigbee2mqtt_types_vendor_lonsonho") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z4xrgqmy1b6bxz51mw3v1yzsb6a50j17jf2g6c68mha897hxqq3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lonsonho-0.2.0 (c (n "zigbee2mqtt_types_vendor_lonsonho") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1nl997nfpjx6h5pcad9z0skqzl85gp3n0d33f49qziz9njp9m4xf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

