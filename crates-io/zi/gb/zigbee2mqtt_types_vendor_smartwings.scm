(define-module (crates-io zi gb zigbee2mqtt_types_vendor_smartwings) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_smartwings-0.0.1 (c (n "zigbee2mqtt_types_vendor_smartwings") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01jcv5sd7hcdnifpfxblbl15r663gi52487vx40ilw6s696wqki3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_smartwings-0.1.0 (c (n "zigbee2mqtt_types_vendor_smartwings") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "016v1yl39kq04kix2drir0zzbfv6yn57camy8sgvw67ggqwg66j3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

