(define-module (crates-io zi gb zigbee2mqtt_types_vendor_nue___3a) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_nue___3a-0.0.1 (c (n "zigbee2mqtt_types_vendor_nue___3a") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d6vzbivipl933ffxyi5dr8w0rjrckx3fkr5f3ns85jg4xjhkk0s") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_nue___3a-0.2.0 (c (n "zigbee2mqtt_types_vendor_nue___3a") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kcgxb5x7mnfia7b47xi2x8b1gygnly3b8haj29yrdyc0gz190wh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

