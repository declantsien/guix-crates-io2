(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lubeez) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lubeez-0.0.1 (c (n "zigbee2mqtt_types_vendor_lubeez") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mw2fwldmhxqmpyx095g91kz1j8kwvzr3scxkyb0dy4lslbk7dal") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lubeez-0.2.0 (c (n "zigbee2mqtt_types_vendor_lubeez") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1hhfcph8x102z2js8bbbi1al955974gr5ra0iycv1nmnf46gs1gx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

