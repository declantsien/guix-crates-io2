(define-module (crates-io zi gb zigbee2mqtt_types_vendor_shinasystem) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_shinasystem-0.0.1 (c (n "zigbee2mqtt_types_vendor_shinasystem") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wfa2ifkax0nms6fhymaplpjl8xf90wmjb2kri9bnxg4as8883nw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_shinasystem-0.2.0 (c (n "zigbee2mqtt_types_vendor_shinasystem") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "11c8r3qqmq2yqc7y9nmb31wdjqr20xjnswk2cf38vzm65fcayg7f") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

