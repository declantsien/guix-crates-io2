(define-module (crates-io zi gb zigbee2mqtt_types_vendor_gidealed) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_gidealed-0.0.1 (c (n "zigbee2mqtt_types_vendor_gidealed") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04yrij4nl6wiyf3pax6p46qj5584cr55fskpcah6bh5h3xkh6qif") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_gidealed-0.2.0 (c (n "zigbee2mqtt_types_vendor_gidealed") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1p6f2rvn5wrmqi2jsad3sh83qp5k8mjnfhz8fm8am2ql3zx5pzhs") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

