(define-module (crates-io zi gb zigbee2mqtt_types_vendor_micro_matic_norge_as) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_micro_matic_norge_as-0.0.1 (c (n "zigbee2mqtt_types_vendor_micro_matic_norge_as") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvbnp9c42lazhr7al2977z6rwj33ja63n4kx6kgn0i1dj41lccb") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_micro_matic_norge_as-0.1.0 (c (n "zigbee2mqtt_types_vendor_micro_matic_norge_as") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1g79617cdi8xipva85s5nmzkpb5z4pj7j2vlkpn6r0pvp6iqybgh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

