(define-module (crates-io zi gb zigbee2mqtt_types_vendor_shenzhen_homa) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_shenzhen_homa-0.0.1 (c (n "zigbee2mqtt_types_vendor_shenzhen_homa") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iinwrvf81y0xljflymv1dcfbzw0ygkl3m72hdkyfk20n89rn2a0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_shenzhen_homa-0.2.0 (c (n "zigbee2mqtt_types_vendor_shenzhen_homa") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "110lag8rs08pfn1bw28yrcqzgbaafd3xpkksyp9b23b9v324xyqr") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

