(define-module (crates-io zi gb zigbee2mqtt_types_vendor_moes) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_moes-0.0.1 (c (n "zigbee2mqtt_types_vendor_moes") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wyb2igjzfzbpd7y259d967wzy02mc9wnjq8lgcvv8dw0b2br4qj") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_moes-0.1.0 (c (n "zigbee2mqtt_types_vendor_moes") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0lnsyxm3x1iky9k5kb97gwzq0yd7ynhm3x1qp0hbxgx5kmc9lazm") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

