(define-module (crates-io zi gb zigbee2mqtt_types_vendor_netvox) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_netvox-0.0.1 (c (n "zigbee2mqtt_types_vendor_netvox") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aq0lhrxahin7grpwj00312f3q40f2xlh7vir2mxfqcp9w7sfpxw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_netvox-0.1.1 (c (n "zigbee2mqtt_types_vendor_netvox") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0az5n9blhd9hgfwvbalkidpx65r3abgaliyvs3nz598zfw5kq1s2") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

