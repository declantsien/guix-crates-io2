(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ilightsin) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ilightsin-0.0.1 (c (n "zigbee2mqtt_types_vendor_ilightsin") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rgwvc29gf8zlm5vr1bhizqwnq2hpvj9y9lp2nn4qv68cckifs66") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ilightsin-0.2.0 (c (n "zigbee2mqtt_types_vendor_ilightsin") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "14z9azjvql76n0lxrzcyrwvzk53gsg018s95b3yadxsnva1fjsi0") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

