(define-module (crates-io zi gb zigbee2mqtt-types-generator) #:use-module (crates-io))

(define-public crate-zigbee2mqtt-types-generator-0.2.2 (c (n "zigbee2mqtt-types-generator") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1pzlxgk4j10p39yhypl8svsgscwl5rh8sn67pjckgggmn7x5aqls") (y #t)))

