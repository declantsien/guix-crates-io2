(define-module (crates-io zi gb zigbee2mqtt_types_vendor_slv) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_slv-0.0.1 (c (n "zigbee2mqtt_types_vendor_slv") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wlwjliv7vxfdq0qy6jv10bny1yn0abmhr656p5dahrpxsfygwiw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_slv-0.2.0 (c (n "zigbee2mqtt_types_vendor_slv") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1j5q8bi7gqqmchaqziim059swv66xah29ivgw469kwlk3hr7w9y7") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

