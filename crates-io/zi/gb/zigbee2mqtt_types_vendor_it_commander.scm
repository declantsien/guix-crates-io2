(define-module (crates-io zi gb zigbee2mqtt_types_vendor_it_commander) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_it_commander-0.0.1 (c (n "zigbee2mqtt_types_vendor_it_commander") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jlm7a99izqvivl5bsbl61i4wi2sv4d904wb2p8x7fp7d3m6zb68") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_it_commander-0.1.0 (c (n "zigbee2mqtt_types_vendor_it_commander") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1hm3dz1m8da2gk8fdh87b4hjmfc16xm4dby64x0qxprfp11b43yb") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

