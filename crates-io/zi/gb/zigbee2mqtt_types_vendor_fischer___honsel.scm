(define-module (crates-io zi gb zigbee2mqtt_types_vendor_fischer___honsel) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_fischer___honsel-0.0.1 (c (n "zigbee2mqtt_types_vendor_fischer___honsel") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l0h2mb2yrfsajxgbz47zrdighpx5vnii0h6f2qjpbghyjzn43gp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_fischer___honsel-0.2.0 (c (n "zigbee2mqtt_types_vendor_fischer___honsel") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1c0dfbhafyi8msh5hjvcv7bwpdlqrs1shwk3s3rcg128blvfcap1") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

