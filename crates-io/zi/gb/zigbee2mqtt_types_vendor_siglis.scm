(define-module (crates-io zi gb zigbee2mqtt_types_vendor_siglis) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_siglis-0.0.1 (c (n "zigbee2mqtt_types_vendor_siglis") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02lj0bgqcmhnk5pjk3iwhln33j2bcyazr9ivdhfjspf63zhkrah4") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_siglis-0.1.0 (c (n "zigbee2mqtt_types_vendor_siglis") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "09k0wgid9vh1ly51yra95lban7zf5f6a9jpfkxqh4075ddcfn4w6") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

