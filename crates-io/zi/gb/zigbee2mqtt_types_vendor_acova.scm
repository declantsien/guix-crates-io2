(define-module (crates-io zi gb zigbee2mqtt_types_vendor_acova) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_acova-0.0.1 (c (n "zigbee2mqtt_types_vendor_acova") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09zzm88cbdkc33yfacrl9dkk6c5sr5g6iqdw9paa5i3maj1c2fsw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_acova-0.2.0 (c (n "zigbee2mqtt_types_vendor_acova") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0zx0v38ngfpxgab0gjc2d0nszcg3bixqgbpkl9xj93gdn3adva9a") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

