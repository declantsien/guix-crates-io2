(define-module (crates-io zi gb zigbee2mqtt_types_vendor_solaredge) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_solaredge-0.1.0 (c (n "zigbee2mqtt_types_vendor_solaredge") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1g2xr8gs68ykk9q52fhximz1fkqz7rym8ch1kbbr7xdxbavnk2ri") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

