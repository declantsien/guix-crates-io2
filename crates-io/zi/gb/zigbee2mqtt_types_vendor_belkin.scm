(define-module (crates-io zi gb zigbee2mqtt_types_vendor_belkin) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_belkin-0.0.1 (c (n "zigbee2mqtt_types_vendor_belkin") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ing144r6anwy2g6y66sp7silf4pvwzf5s4chiq3r2kkmla4f18g") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_belkin-0.2.0 (c (n "zigbee2mqtt_types_vendor_belkin") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1h7h3xrnpkc83sjqvsldnwmjqmswlz4p8yv68wvh8q9s63my0kp8") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

