(define-module (crates-io zi gb zigbee2mqtt_types_vendor_schneider_electric) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_schneider_electric-0.0.1 (c (n "zigbee2mqtt_types_vendor_schneider_electric") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ymbrsacca480y316cplpj0sjm3y36np8wp3d0sbjcs3nyxpz8j9") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_schneider_electric-0.2.0 (c (n "zigbee2mqtt_types_vendor_schneider_electric") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gsr03la5v6vkiv6pmrqrgpi0ag86vaxwg3b65hxwjxds1x91nkv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

