(define-module (crates-io zi gb zigbee2mqtt_types_vendor_cleode) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_cleode-0.0.1 (c (n "zigbee2mqtt_types_vendor_cleode") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05dj8fbnf60a8h7xdhfvm5256rifrr1znxs5g1n7iw6p358xyymr") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_cleode-0.1.0 (c (n "zigbee2mqtt_types_vendor_cleode") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "186gkzv21jl5d2cckp3dm9a4pdavg64vx5xf4jzar8zib8l5p4y3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

