(define-module (crates-io zi gb zigbee2mqtt_types_vendor_leedarson) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_leedarson-0.0.1 (c (n "zigbee2mqtt_types_vendor_leedarson") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1si6xw9376k428zcl47dinlc0y5ndl31sfz4lq8rc6mbg17n01p3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_leedarson-0.2.0 (c (n "zigbee2mqtt_types_vendor_leedarson") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gh5r8k49cbbhwls005jy0ay14ns8slfphyghc72n8jiis192jsr") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

