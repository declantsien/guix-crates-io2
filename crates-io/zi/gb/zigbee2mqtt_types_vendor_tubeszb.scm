(define-module (crates-io zi gb zigbee2mqtt_types_vendor_tubeszb) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_tubeszb-0.0.1 (c (n "zigbee2mqtt_types_vendor_tubeszb") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zkjqb18i1grk2f8g7ff56aph31dqqldl461d415vix1a6bfqfzz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_tubeszb-0.1.0 (c (n "zigbee2mqtt_types_vendor_tubeszb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0k2r49jbn3yzqi9rgn6ikcc4jca4b4bl0mx5y6v5z2qhnwgmvgwz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

