(define-module (crates-io zi gb zigbee2mqtt_types_vendor_blitzwolf) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_blitzwolf-0.0.1 (c (n "zigbee2mqtt_types_vendor_blitzwolf") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1haz0xz6lbznkzn8sy5ybwnjx3w496x7rlw5hmy08946ln7gb4r9") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_blitzwolf-0.1.0 (c (n "zigbee2mqtt_types_vendor_blitzwolf") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1aqasjxi9mb59iiyzj8fp947jjspl542a4g83jmd121y5vs42068") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

