(define-module (crates-io zi gb zigbee2mqtt_types_vendor_openlumi) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_openlumi-0.0.1 (c (n "zigbee2mqtt_types_vendor_openlumi") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cgfhld6b7wswyvdfb5yrriknbs80absaakz509239753hzyzf76") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_openlumi-0.1.0 (c (n "zigbee2mqtt_types_vendor_openlumi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1x7xcqwzvp8x98plw62mhascx1yba8v2500jscpi305bwjs30l1v") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

