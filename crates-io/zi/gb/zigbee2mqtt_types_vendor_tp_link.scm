(define-module (crates-io zi gb zigbee2mqtt_types_vendor_tp_link) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_tp_link-0.0.1 (c (n "zigbee2mqtt_types_vendor_tp_link") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zyivw25ccma1101bb7wn3cspfzcpphsyis1yk5k66nmspdzldqp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_tp_link-0.1.0 (c (n "zigbee2mqtt_types_vendor_tp_link") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16jqkq6ljxi1acymzncwr79p4n79s37dvs15r2pfaqilmzaizmc1") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

