(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lidl) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lidl-0.0.1 (c (n "zigbee2mqtt_types_vendor_lidl") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1glfff4plplrv9jd4gvlm33nmanqpxcr9fr4aqrxhd0bxhwxnffp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lidl-0.1.0 (c (n "zigbee2mqtt_types_vendor_lidl") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "08nfksqx329y729gy01mjr35fwr4a8j45pmpvy8k5sg9pgyplz8h") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

