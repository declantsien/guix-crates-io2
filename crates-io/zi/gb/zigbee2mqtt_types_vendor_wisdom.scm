(define-module (crates-io zi gb zigbee2mqtt_types_vendor_wisdom) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_wisdom-0.1.0 (c (n "zigbee2mqtt_types_vendor_wisdom") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1n1q8y03lfr73kia618b8cv33v4qnpkd1g7vffafhcj996n48q3q") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

