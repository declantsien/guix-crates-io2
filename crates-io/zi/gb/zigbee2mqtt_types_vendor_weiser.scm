(define-module (crates-io zi gb zigbee2mqtt_types_vendor_weiser) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_weiser-0.0.1 (c (n "zigbee2mqtt_types_vendor_weiser") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hw4yh7zxn8wa9bvrmq6lydq5bpb9a7i8d6sbr9l640sw6j93kjh") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_weiser-0.1.0 (c (n "zigbee2mqtt_types_vendor_weiser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1s6q88nf3hczjvdwhfmp4dpnlgm3ww27v9qd11d2irhyq35mkjhq") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

