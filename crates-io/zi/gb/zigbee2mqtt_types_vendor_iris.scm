(define-module (crates-io zi gb zigbee2mqtt_types_vendor_iris) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_iris-0.0.1 (c (n "zigbee2mqtt_types_vendor_iris") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y5mlcgh9g4n6dmqzl37k4lkj4r48yvqk8w3h93r0dj87ql2b7a2") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_iris-0.2.0 (c (n "zigbee2mqtt_types_vendor_iris") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "108na0jym7r8p2y8j9ypp0n479l8hr5dnbys3cdac944iza1y1gk") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

