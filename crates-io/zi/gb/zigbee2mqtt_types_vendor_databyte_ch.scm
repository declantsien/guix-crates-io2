(define-module (crates-io zi gb zigbee2mqtt_types_vendor_databyte_ch) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_databyte_ch-0.0.1 (c (n "zigbee2mqtt_types_vendor_databyte_ch") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i00bpp1d0dv9fyy3fv9jy9gi075frgjc5ganrr59jag2j3qkkwa") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_databyte_ch-0.2.0 (c (n "zigbee2mqtt_types_vendor_databyte_ch") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "14h2ryrqnky3hpi5kywbqk86gjkcdfg2l7x1m3qjbd4b2xr9x4vz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

