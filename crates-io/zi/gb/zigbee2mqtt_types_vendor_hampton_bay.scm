(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hampton_bay) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hampton_bay-0.0.1 (c (n "zigbee2mqtt_types_vendor_hampton_bay") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kv1v9v3w3gsbri7y261k19ib2dixhy4ma4ml0xgb8rngckb7crp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hampton_bay-0.2.0 (c (n "zigbee2mqtt_types_vendor_hampton_bay") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0n1paix76a77kfq5sf6zrzcbh8wnmqigdyy9sg0qybmb7i3s9kra") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

