(define-module (crates-io zi gb zigbee2mqtt_types_vendor_commercial_electric) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_commercial_electric-0.0.1 (c (n "zigbee2mqtt_types_vendor_commercial_electric") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nsdvsn9a4vk7r2k7pc7ydd6bk5vvfycxgyzsjczyl7wlw4y9gf5") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_commercial_electric-0.2.0 (c (n "zigbee2mqtt_types_vendor_commercial_electric") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gcf4x4qqygj41kbdnz8j3qqhjp9gmh9zpb7irvks6aifc08lp2q") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

