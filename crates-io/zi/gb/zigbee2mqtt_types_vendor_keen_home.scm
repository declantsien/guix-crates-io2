(define-module (crates-io zi gb zigbee2mqtt_types_vendor_keen_home) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_keen_home-0.0.1 (c (n "zigbee2mqtt_types_vendor_keen_home") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0606xpi6x4pid55b64dr02g087lnv8qn4478bsf3m3kzmyzlhmz2") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_keen_home-0.1.0 (c (n "zigbee2mqtt_types_vendor_keen_home") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "09mhli56fcajwzwxw471vd997jm6w7zm0kxsib5iksgkj77p3amd") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

