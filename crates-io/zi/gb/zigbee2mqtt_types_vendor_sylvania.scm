(define-module (crates-io zi gb zigbee2mqtt_types_vendor_sylvania) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_sylvania-0.0.1 (c (n "zigbee2mqtt_types_vendor_sylvania") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j8ga98wbcxwb7y1mp0mn9m06zw95m84dlzci1nn3wpg4iyazbia") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_sylvania-0.2.0 (c (n "zigbee2mqtt_types_vendor_sylvania") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1d0mwcbr368ac23rc5kjih1hflyqz5dkqjg8gaggx1f6v3l8afsh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

