(define-module (crates-io zi gb zigbee2mqtt_types_vendor_eaton_halo_led) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_eaton_halo_led-0.0.1 (c (n "zigbee2mqtt_types_vendor_eaton_halo_led") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sdbazfl82san4daxvibrfwlmj3nfavq5vdb6nxjj5s8fzgr7k46") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_eaton_halo_led-0.2.0 (c (n "zigbee2mqtt_types_vendor_eaton_halo_led") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1s9x3axizls7g10dfz13rghg6zfiv7rncnfgvnla0p6688ga66wk") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

