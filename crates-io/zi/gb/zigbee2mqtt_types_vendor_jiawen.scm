(define-module (crates-io zi gb zigbee2mqtt_types_vendor_jiawen) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_jiawen-0.0.1 (c (n "zigbee2mqtt_types_vendor_jiawen") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pr6dvshiv38jdwj736w8a74v14r57qhh0s9yv8406jcczgmjzha") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_jiawen-0.2.0 (c (n "zigbee2mqtt_types_vendor_jiawen") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16zbg40hxgr5wpzsjwa8192srl0vzk42jpflnyl68vlwq8xqxwzn") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

