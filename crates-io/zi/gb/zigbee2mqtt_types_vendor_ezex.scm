(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ezex) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ezex-0.0.1 (c (n "zigbee2mqtt_types_vendor_ezex") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kfb46x1lj7nmikpcdsg234l46v2cbd1s23zhhfdvbc9d3yv4b5i") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ezex-0.1.0 (c (n "zigbee2mqtt_types_vendor_ezex") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "10ydskjnxkxpf34q7w0gprhwa9p7bd1gbj71ag38a44s2nbvz447") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

