(define-module (crates-io zi gb zigbee2mqtt_types_vendor_paul_neuhaus) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_paul_neuhaus-0.0.1 (c (n "zigbee2mqtt_types_vendor_paul_neuhaus") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kfqf695z7dxki03wjn6cc93v156ifv9569cgiw2d62937b3bchv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_paul_neuhaus-0.2.0 (c (n "zigbee2mqtt_types_vendor_paul_neuhaus") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1r8zraizlfnyw9bcg8ryfxmxizdvflvn83clw0dvxs4hsisgjk94") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

