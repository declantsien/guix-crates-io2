(define-module (crates-io zi gb zigbee2mqtt_types_vendor_develco) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_develco-0.0.1 (c (n "zigbee2mqtt_types_vendor_develco") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0836643vm70gijyr42hdjncb4kissf4l838p65d8lx416hamihba") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_develco-0.2.0 (c (n "zigbee2mqtt_types_vendor_develco") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kxj8d9wlq8nf6p1rm2i0iqfg66rqfsig2fxj18bdjqa1v7q5m5z") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

