(define-module (crates-io zi gb zigbee2mqtt_types_vendor_adeo) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_adeo-0.0.1 (c (n "zigbee2mqtt_types_vendor_adeo") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b786b54vkmrpn2i7sh0zr7hgjd6c5xb78khb9fsfyg60g7c0k1f") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_adeo-0.2.0 (c (n "zigbee2mqtt_types_vendor_adeo") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "00agnj55p7myv99556cxdhm6chslj8ccyabfa9s62bq4jm0kbxgb") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

