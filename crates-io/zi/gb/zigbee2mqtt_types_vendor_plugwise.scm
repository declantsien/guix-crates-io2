(define-module (crates-io zi gb zigbee2mqtt_types_vendor_plugwise) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_plugwise-0.0.1 (c (n "zigbee2mqtt_types_vendor_plugwise") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bfg5n5cmn9hs8cdnp453xyrwpni7xma2f8pk9h8acf9a2lr9h8x") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_plugwise-0.2.0 (c (n "zigbee2mqtt_types_vendor_plugwise") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0q1nx2fsj87j5v4hdpvdrbgcqhc1x9yx0sa9d612ypsivr9p40r4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

