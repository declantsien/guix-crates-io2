(define-module (crates-io zi gb zigbee2mqtt_types_vendor_robb_smart) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_robb_smart-0.0.1 (c (n "zigbee2mqtt_types_vendor_robb_smart") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r1v39wik85y4d8g1djyvahhykd5469xp7ffn773b317nysv8xdz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_robb_smart-0.1.0 (c (n "zigbee2mqtt_types_vendor_robb_smart") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1carcplhzzhlbln737di02m19nrz0h49jb9a82jdr6v49dh0g5ij") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

