(define-module (crates-io zi gb zigbee2mqtt_types_vendor_smabit__bitron_video_) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_smabit__bitron_video_-0.0.1 (c (n "zigbee2mqtt_types_vendor_smabit__bitron_video_") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00dx2ybbrf3a0pfb117gdv9fjxcy8l2sqkqgq2ld7dgdaha2f7g8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_smabit__bitron_video_-0.2.0 (c (n "zigbee2mqtt_types_vendor_smabit__bitron_video_") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1cr31nq908af6nb8p5bhlbjkj7vf5nbrk9r74qmsrp258dx3hidd") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

