(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ilux) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ilux-0.0.1 (c (n "zigbee2mqtt_types_vendor_ilux") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wjs3cs18pd1vrcksv54lksj1qa33n9kgjkj6rr1fpw81plj3prf") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ilux-0.2.0 (c (n "zigbee2mqtt_types_vendor_ilux") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0ady8mgbh3zarj6yw0lnysi7fw1s6gm1rxn5c1c8w89lk7cbhyvi") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

