(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ledvance) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ledvance-0.0.1 (c (n "zigbee2mqtt_types_vendor_ledvance") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w8yj7pgwdb3mwwy4rnrwymljyqrrcfdjz18r2is5zwgyg83dcd3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ledvance-0.2.0 (c (n "zigbee2mqtt_types_vendor_ledvance") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "19g5hvhj3cylyvakqqschvygg4v5j9rx7a6dzn2z05ikd3gkm7j3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

