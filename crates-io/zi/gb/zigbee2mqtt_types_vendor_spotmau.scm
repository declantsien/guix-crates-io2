(define-module (crates-io zi gb zigbee2mqtt_types_vendor_spotmau) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_spotmau-0.0.1 (c (n "zigbee2mqtt_types_vendor_spotmau") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12jv97mxf3nh4v0p3mvd1grh2zfjd2csypf6a6ka6f0jw2k5sasz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_spotmau-0.2.0 (c (n "zigbee2mqtt_types_vendor_spotmau") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1m6c6n8sfhcfhlhzwf6pas0ansyr0y621j7nqlga6685n033w395") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

