(define-module (crates-io zi gb zigbee2mqtt_types_vendor_jumitech_aps) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_jumitech_aps-0.0.1 (c (n "zigbee2mqtt_types_vendor_jumitech_aps") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dpl5k1d76rq53pjzwjq280mbpgkjax1rxig7riai2mblch7bzav") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_jumitech_aps-0.2.0 (c (n "zigbee2mqtt_types_vendor_jumitech_aps") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1k5w33866d42plcpsyfy86rrpl7cirryxpp53wvjxq885gbfh6l4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

