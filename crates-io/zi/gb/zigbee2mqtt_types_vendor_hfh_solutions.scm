(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hfh_solutions) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hfh_solutions-0.0.1 (c (n "zigbee2mqtt_types_vendor_hfh_solutions") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18km2czvzkjc0gar2cmccmyxgalszqcp2n4ypghdplm1z3h4g3j8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hfh_solutions-0.2.0 (c (n "zigbee2mqtt_types_vendor_hfh_solutions") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0xn1wj5wdc41vi0zzxnzrwcc8bvgl2h2zf5y9hinycpmq7n991dc") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

