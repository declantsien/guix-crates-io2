(define-module (crates-io zi gb zigbee2mqtt_types_vendor_zipato) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_zipato-0.0.1 (c (n "zigbee2mqtt_types_vendor_zipato") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08bmf5snkrgmvpnb076r4hnd06vxj1rnjprdrqbr7dx65iwpvi24") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_zipato-0.2.0 (c (n "zigbee2mqtt_types_vendor_zipato") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kbqxxgzbxiwjfiaz206hbl5hy50c424zx3l4njqkkfg423vb4s3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

