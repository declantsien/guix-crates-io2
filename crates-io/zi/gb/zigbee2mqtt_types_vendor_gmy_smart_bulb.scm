(define-module (crates-io zi gb zigbee2mqtt_types_vendor_gmy_smart_bulb) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_gmy_smart_bulb-0.0.1 (c (n "zigbee2mqtt_types_vendor_gmy_smart_bulb") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0as7xqhlsh9zvvisqzwgl1119ql4kz91pyrh4k4gyg7jxz0i4i9r") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_gmy_smart_bulb-0.2.0 (c (n "zigbee2mqtt_types_vendor_gmy_smart_bulb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "00hp1r7mrwvcmv7z7ps6yvqs6dra2ivwmy72f36grv84r7s5zv7v") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

