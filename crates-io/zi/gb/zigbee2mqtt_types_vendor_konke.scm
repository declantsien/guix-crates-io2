(define-module (crates-io zi gb zigbee2mqtt_types_vendor_konke) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_konke-0.0.1 (c (n "zigbee2mqtt_types_vendor_konke") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hzgihidc64cdwbik259s6bhj23750h5j662cmfsdjyqrj3nh1mi") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_konke-0.2.0 (c (n "zigbee2mqtt_types_vendor_konke") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "04qnfk3s8bya4g69xhhhh75apcq2ai257wr2vvjpnsmq2jivxsia") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

