(define-module (crates-io zi gb zigbee2mqtt_types_vendor_klikaanklikuit) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_klikaanklikuit-0.0.1 (c (n "zigbee2mqtt_types_vendor_klikaanklikuit") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03zcfcnan0w15rsd6pjs4r6g8ayzsllp9gdadq1q2lpz9pam6bzw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_klikaanklikuit-0.2.0 (c (n "zigbee2mqtt_types_vendor_klikaanklikuit") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1vwhr65fj4lxcsd1fnr84bxrvj1q84d9xjdy40vqkzccgypqg397") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

