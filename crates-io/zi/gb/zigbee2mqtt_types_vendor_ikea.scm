(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ikea) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ikea-0.0.1 (c (n "zigbee2mqtt_types_vendor_ikea") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "136mvx7qcvs40wjbxibg4h35lw4hdgn6rs2yijzivvbl1vgdwm1i") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ikea-0.2.0 (c (n "zigbee2mqtt_types_vendor_ikea") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0ll0xfjhbi8k55zl3hqb3zlv4hfdnxkri9qywh57ljbrpv58hyml") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

