(define-module (crates-io zi gb zigbee2mqtt_types_vendor_linkind) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_linkind-0.0.1 (c (n "zigbee2mqtt_types_vendor_linkind") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a9975ibrl6zfqqj6b7gpg0br7g6i19r9q14pvfprbv5m4g2bf33") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_linkind-0.2.0 (c (n "zigbee2mqtt_types_vendor_linkind") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0xv48jcrnw3jhwslfy09ms9xqhpwh237pjjg6p1z71vlp61c2hnh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

