(define-module (crates-io zi gb zigbee2mqtt_types_vendor_led_trading) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_led_trading-0.0.1 (c (n "zigbee2mqtt_types_vendor_led_trading") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f651d6q48xgwbng9cf7hjy0k7jm18bwj76vsmypb59cw52mqma7") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_led_trading-0.2.0 (c (n "zigbee2mqtt_types_vendor_led_trading") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16d63agl1k084d34vcp8p81qv6m2ad2xdfn12is28h42w3g79wha") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

