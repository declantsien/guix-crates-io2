(define-module (crates-io zi gb zigbee2mqtt_types_vendor_easyaccess) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_easyaccess-0.0.1 (c (n "zigbee2mqtt_types_vendor_easyaccess") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0945iaid16z7zy03dz83d0gdd0rdfpimhfm9x5x9jyqhj5j6ap0s") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_easyaccess-0.1.0 (c (n "zigbee2mqtt_types_vendor_easyaccess") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0sdfp856qz114nj7jwrghbh1a0zals4phm4bjahpl8rp78h98rsd") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

