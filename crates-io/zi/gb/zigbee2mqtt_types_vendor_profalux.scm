(define-module (crates-io zi gb zigbee2mqtt_types_vendor_profalux) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_profalux-0.0.1 (c (n "zigbee2mqtt_types_vendor_profalux") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p3pvv99a03qvdgh8n95hmahq237z5kyrb60z8gipljjllhhv4xc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_profalux-0.1.1 (c (n "zigbee2mqtt_types_vendor_profalux") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "19x6rx76hfkjcq9bm8l2530hx38qx5pf60z521kidkkskhrc22ka") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

