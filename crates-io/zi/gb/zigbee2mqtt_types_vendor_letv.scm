(define-module (crates-io zi gb zigbee2mqtt_types_vendor_letv) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_letv-0.0.1 (c (n "zigbee2mqtt_types_vendor_letv") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h1rdzlhi4i5mvzhbxm3chy0lsqaq8hgag8x318h3kq8ip0mkzwf") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_letv-0.1.0 (c (n "zigbee2mqtt_types_vendor_letv") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1d7wvdmzvzw0r76nx6vbdzrzjq8c2w9ksyn6yfz380s39hf4nv9m") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

