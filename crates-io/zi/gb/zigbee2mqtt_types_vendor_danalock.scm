(define-module (crates-io zi gb zigbee2mqtt_types_vendor_danalock) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_danalock-0.0.1 (c (n "zigbee2mqtt_types_vendor_danalock") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i0s5rh6clrhgz0kb02j8jc8skwlvlxqx68am0cn5hjxk1f85bsg") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_danalock-0.1.0 (c (n "zigbee2mqtt_types_vendor_danalock") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1ncmqiv5id6kqi5nk8mvp1mm55w1zw8jglr6jgp8qwrga8xc0cgg") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

