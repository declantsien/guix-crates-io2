(define-module (crates-io zi gb zigbee2mqtt_types_vendor_technicolor) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_technicolor-0.0.1 (c (n "zigbee2mqtt_types_vendor_technicolor") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13zzirk5z21365kl82969lxpiv2njqqqwrgzl1n88y8n72i5dqqf") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_technicolor-0.1.0 (c (n "zigbee2mqtt_types_vendor_technicolor") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1l8b2kc5wi72b6rhsa1nc6nrk7yp97whkh8xndknzgz8habvvfdf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

