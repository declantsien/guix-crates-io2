(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lightsolutions) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lightsolutions-0.0.1 (c (n "zigbee2mqtt_types_vendor_lightsolutions") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02zg744s2pca6cbgmixg0mq5fv0yx8mgin1iyvvyvilsap2xpy6w") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lightsolutions-0.2.0 (c (n "zigbee2mqtt_types_vendor_lightsolutions") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1xv0qnxpcv27h327fpb5wpqjfn32r6q2lv5vaa6gnkhv7drknn4p") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

