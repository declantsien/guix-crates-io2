(define-module (crates-io zi gb zigbee2mqtt_types_vendor_jasco) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_jasco-0.0.1 (c (n "zigbee2mqtt_types_vendor_jasco") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q1588zi9vdfaxym92z79a7v3lsd021xg2792irzlirjyc79n9kv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_jasco-0.1.0 (c (n "zigbee2mqtt_types_vendor_jasco") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0wgzgxl4na60j4a95zsl4phki2bwick33sj060hc310n34d8m5w5") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

