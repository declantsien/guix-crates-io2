(define-module (crates-io zi gb zigbee2mqtt_types_vendor_sengled) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_sengled-0.0.1 (c (n "zigbee2mqtt_types_vendor_sengled") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01cbdhshsw3rvya3mhgrkjndscmjfiyi4vjq6df0j058kxnj2smq") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_sengled-0.2.0 (c (n "zigbee2mqtt_types_vendor_sengled") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1d5ysl8ai0qq24mby9ymyw4cj5qj8rzzfdc43xh75v4gc2z74ld4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

