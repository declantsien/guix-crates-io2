(define-module (crates-io zi gb zigbee2mqtt_types_vendor_prolight) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_prolight-0.0.1 (c (n "zigbee2mqtt_types_vendor_prolight") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1axmgsq5npip40vplwb0ladz04c44n99bcgzkq5abnvy6ggzzqlk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_prolight-0.2.0 (c (n "zigbee2mqtt_types_vendor_prolight") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0k808vbil997xv1s56ind9hmn57z4c16h147p6k4dmrm6dmr2f91") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

