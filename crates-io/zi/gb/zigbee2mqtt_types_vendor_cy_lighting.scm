(define-module (crates-io zi gb zigbee2mqtt_types_vendor_cy_lighting) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_cy_lighting-0.0.1 (c (n "zigbee2mqtt_types_vendor_cy_lighting") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p48cfv4mjiaa1ldgyc7g53kszp1r08wrhs098hi1wbd9ql15780") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_cy_lighting-0.2.0 (c (n "zigbee2mqtt_types_vendor_cy_lighting") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0a058gbvw9h74wj27144nq5z65kgqrqqavr7nb6f61x924n18n71") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

