(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hilux) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hilux-0.1.0 (c (n "zigbee2mqtt_types_vendor_hilux") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0bfr4gx6kq0rzmqmsspz9qkjz2agdh9w3gwyppxjybdi1saai0mx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

