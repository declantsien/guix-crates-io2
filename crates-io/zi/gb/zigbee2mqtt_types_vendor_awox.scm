(define-module (crates-io zi gb zigbee2mqtt_types_vendor_awox) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_awox-0.1.0 (c (n "zigbee2mqtt_types_vendor_awox") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1vx24vky47dkarmgzqfwmvr683yx5v3qlcqjjhpilb5q300b3jyc") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

