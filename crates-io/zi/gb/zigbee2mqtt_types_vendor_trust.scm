(define-module (crates-io zi gb zigbee2mqtt_types_vendor_trust) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_trust-0.0.1 (c (n "zigbee2mqtt_types_vendor_trust") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hbvprsx5n350rcwaxd2vdilw2p4jv1fs1grfpbfwp23vl5nk6ks") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_trust-0.2.0 (c (n "zigbee2mqtt_types_vendor_trust") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0f22pj333sva5mc72l951dryyv2lz5v4sg82dsw7sfd7dfb4r8dy") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

