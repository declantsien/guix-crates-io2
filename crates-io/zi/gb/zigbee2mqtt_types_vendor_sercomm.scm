(define-module (crates-io zi gb zigbee2mqtt_types_vendor_sercomm) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_sercomm-0.0.1 (c (n "zigbee2mqtt_types_vendor_sercomm") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wjikfpjlv5kd7cjds3nmsi4k7sa5na19k9s2c8gzq1q67ghn3l9") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_sercomm-0.1.0 (c (n "zigbee2mqtt_types_vendor_sercomm") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "118yjvk14llgmhnajas7739dm6qffmxglblbnryzhcfqs46j5nhk") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

