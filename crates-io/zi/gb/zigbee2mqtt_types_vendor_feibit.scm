(define-module (crates-io zi gb zigbee2mqtt_types_vendor_feibit) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_feibit-0.0.1 (c (n "zigbee2mqtt_types_vendor_feibit") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "112qs2a2f2i1abrgm2x8bmrnhs4zb1g3ip2fv3zzd8pq5nzrvcbk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_feibit-0.2.0 (c (n "zigbee2mqtt_types_vendor_feibit") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "13gbz9jb7hh3rl9k4r2amb6f5mr7g9sx7mnsqg5nif3q0zja93f5") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

