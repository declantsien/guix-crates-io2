(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lanesto) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lanesto-0.0.1 (c (n "zigbee2mqtt_types_vendor_lanesto") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kb3rrs70ar77m2y901l99syak1kax5wvrjzdyyx2imk9zhg5cgl") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lanesto-0.2.0 (c (n "zigbee2mqtt_types_vendor_lanesto") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1bm1cn2cqm5malk8v99q4si65ql9rmj3qvsnc1n9aswsm43w0w6r") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

