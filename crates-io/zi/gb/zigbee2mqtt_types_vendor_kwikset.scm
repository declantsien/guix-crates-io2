(define-module (crates-io zi gb zigbee2mqtt_types_vendor_kwikset) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_kwikset-0.0.1 (c (n "zigbee2mqtt_types_vendor_kwikset") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mdzi3arghglm4gl9mnvkbx9h2wr7q7gj0bf92572zqi26dx0mci") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_kwikset-0.1.0 (c (n "zigbee2mqtt_types_vendor_kwikset") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0d4pq5hs52ifjzbhsczzjisgcacblpvmv0znydanlcbg9v332wc3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

