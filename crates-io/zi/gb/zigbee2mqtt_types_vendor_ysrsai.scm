(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ysrsai) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ysrsai-0.0.1 (c (n "zigbee2mqtt_types_vendor_ysrsai") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a30dw9mzg539fqzd7kzbd1snv8j11qlaf78wph6wf1grrp2r71m") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ysrsai-0.2.0 (c (n "zigbee2mqtt_types_vendor_ysrsai") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0xkrslv8djkp6nvmajc9b3952riac4dyz56lgwpp89276h246j2r") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

