(define-module (crates-io zi gb zigbee2mqtt_types_vendor_neo) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_neo-0.0.1 (c (n "zigbee2mqtt_types_vendor_neo") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k4jkf01y0wjsxqxyplpl74pfjsan6cjbrcfp2n3pw93fdnzmivc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_neo-0.1.0 (c (n "zigbee2mqtt_types_vendor_neo") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1qi2sb0p0ynqcqn41mabm2ndixaw6wfk4vlygmvd8mk2xl0pgqki") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

