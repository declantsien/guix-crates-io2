(define-module (crates-io zi gb zigbee2mqtt_types_vendor_evology) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_evology-0.0.1 (c (n "zigbee2mqtt_types_vendor_evology") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dsn7569m14phf273d1f4kzaj1rgk0f30m7waj8d4nacrjn1g48v") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_evology-0.1.0 (c (n "zigbee2mqtt_types_vendor_evology") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0c37wirqr28bahdp9m0ydkq09r2x5jzrf0qabrqq5l8nrxzgdka6") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

