(define-module (crates-io zi gb zigbee2mqtt_types_vendor_weten) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_weten-0.0.1 (c (n "zigbee2mqtt_types_vendor_weten") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sqcv1d2xcc1hrd9v6lwh3r2j9bgn3np61i7kk5cwvqws22nsz65") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_weten-0.2.0 (c (n "zigbee2mqtt_types_vendor_weten") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1vq2qnr6zcgamg06nlqdir5f68pjdv6pz5x4g36z89m2fv1c7c3m") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

