(define-module (crates-io zi gb zigbee2mqtt_types_vendor_busch_jaeger) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_busch_jaeger-0.0.1 (c (n "zigbee2mqtt_types_vendor_busch_jaeger") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1443f6skjwvnjvr65df3j6300pd1d0c5rvw0n723dzjh7g2qs964") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_busch_jaeger-0.2.0 (c (n "zigbee2mqtt_types_vendor_busch_jaeger") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "041q4avx2f4ma2p7zmga62swgaszbhmsfw9kh7baq69956bgi8fl") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

