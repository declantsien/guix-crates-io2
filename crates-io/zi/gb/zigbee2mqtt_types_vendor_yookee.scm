(define-module (crates-io zi gb zigbee2mqtt_types_vendor_yookee) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_yookee-0.0.1 (c (n "zigbee2mqtt_types_vendor_yookee") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xddjzwjk57mhwr9bdk3cs71d09xqks0g03fq7zpl417rny6plf7") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_yookee-0.1.0 (c (n "zigbee2mqtt_types_vendor_yookee") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0hi9dql5cxy17y0jv5ggb7x2zg6nqqv7pdw1ghnzvz2kif41kpv5") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

