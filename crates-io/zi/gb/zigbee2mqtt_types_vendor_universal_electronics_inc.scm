(define-module (crates-io zi gb zigbee2mqtt_types_vendor_universal_electronics_inc) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_universal_electronics_inc-0.0.1 (c (n "zigbee2mqtt_types_vendor_universal_electronics_inc") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sb9g5n2k77cfwbmpkzdqs64p47xv7cjchadx8yrq70s2q4n8wyr") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_universal_electronics_inc-0.1.1 (c (n "zigbee2mqtt_types_vendor_universal_electronics_inc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1jjfcgmli09iw6a2lxjnqimvkh0k7gs23axf8fb8n407gh2rcjyx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

