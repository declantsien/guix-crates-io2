(define-module (crates-io zi gb zigbee2mqtt_types_vendor_j_xuan) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_j_xuan-0.0.1 (c (n "zigbee2mqtt_types_vendor_j_xuan") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qfpbwnnr0dm9pwbdg3gdaimcbdggnjwxrss083v0a73fkmlgqqk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_j_xuan-0.1.0 (c (n "zigbee2mqtt_types_vendor_j_xuan") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1ph1iws89146rj353pgvdgmsi535y9mkvpz6f3mf0jla0r392xa2") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

