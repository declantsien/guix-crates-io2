(define-module (crates-io zi gb zigbee2mqtt_types_vendor_schlage) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_schlage-0.0.1 (c (n "zigbee2mqtt_types_vendor_schlage") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aai4vhq82vvaakjsp3swlzblpj41465hvhpl11rjabcr1jyk0w5") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_schlage-0.1.0 (c (n "zigbee2mqtt_types_vendor_schlage") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "14cdxkhd52qpc2dy9cyyxy3dbf4460vsi72lx8cqj894g65yrqjs") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

