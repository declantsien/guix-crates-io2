(define-module (crates-io zi gb zigbee2mqtt_types_vendor_livingwise) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_livingwise-0.0.1 (c (n "zigbee2mqtt_types_vendor_livingwise") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h3fvjw186650fl9a42lqa9yvvn7yvaba4mk1kf7kf3vdmrbdl7w") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_livingwise-0.2.0 (c (n "zigbee2mqtt_types_vendor_livingwise") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gvwjd4wg6rxk9jpa67lz8q0gmybs96xigv2jzpf2h43ymga4dkf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

