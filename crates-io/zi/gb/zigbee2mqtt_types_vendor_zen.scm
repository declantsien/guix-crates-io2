(define-module (crates-io zi gb zigbee2mqtt_types_vendor_zen) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_zen-0.0.1 (c (n "zigbee2mqtt_types_vendor_zen") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bcniv3rsxlrjyk9nff5nfksmncsvzjp2pd7wfj3a8g1wyi782vf") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_zen-0.1.0 (c (n "zigbee2mqtt_types_vendor_zen") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0jgz527wkdx8axbd7yd2qyyxwv2v2ixygz3k2h1jgwybs8mdy381") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

