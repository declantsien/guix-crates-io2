(define-module (crates-io zi gb zigbee2mqtt_types_vendor_cleverio) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_cleverio-0.1.0 (c (n "zigbee2mqtt_types_vendor_cleverio") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0wp81dcc7gajjf4n2v2v23s9gz065krx0l62rwmh82sm560lic9f") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

