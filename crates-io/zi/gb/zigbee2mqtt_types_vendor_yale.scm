(define-module (crates-io zi gb zigbee2mqtt_types_vendor_yale) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_yale-0.0.1 (c (n "zigbee2mqtt_types_vendor_yale") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d1jm005im2759792jrwnbpx9cxhsiw12qdpz0cmfmc8xsc69c9y") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_yale-0.2.0 (c (n "zigbee2mqtt_types_vendor_yale") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "19q2xnzskgg7qj6ih9lfbs0adg3kb8v31z4n8a7d7dsbbzw1x2a8") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

