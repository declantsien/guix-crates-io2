(define-module (crates-io zi gb zigbee2mqtt_types_vendor_dnake) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_dnake-0.0.1 (c (n "zigbee2mqtt_types_vendor_dnake") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x1rhryd3s3dbp6lxcr6227hhxc3l7fq4diq95gsrdr19h28xz0p") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_dnake-0.2.0 (c (n "zigbee2mqtt_types_vendor_dnake") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1jli1j7g4alw8x34cradq8gi871l5grb40rip5hmbngcl745id7p") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

