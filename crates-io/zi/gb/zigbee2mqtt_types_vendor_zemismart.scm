(define-module (crates-io zi gb zigbee2mqtt_types_vendor_zemismart) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_zemismart-0.0.1 (c (n "zigbee2mqtt_types_vendor_zemismart") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bljcp58kay2jilks7mg821cviwd4g4vfwjvdlry4vz1wpa7nyfi") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_zemismart-0.2.0 (c (n "zigbee2mqtt_types_vendor_zemismart") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0cff0jmyp0x451390i58gx7p79m2lfxd9a6rc3w2r2lb287hazdv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

