(define-module (crates-io zi gb zigbee2mqtt_types_vendor_oujiabao) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_oujiabao-0.0.1 (c (n "zigbee2mqtt_types_vendor_oujiabao") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y0kqbjl0i7z24y2gkmigrvx6zp1wviifpq43r33as3p85gj2sqm") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_oujiabao-0.1.0 (c (n "zigbee2mqtt_types_vendor_oujiabao") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0vxhxizplbic1mk5mcwyixl283xrnlpi233nnypw3haqf1qyvsqj") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

