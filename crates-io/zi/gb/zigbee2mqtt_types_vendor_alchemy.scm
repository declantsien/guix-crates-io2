(define-module (crates-io zi gb zigbee2mqtt_types_vendor_alchemy) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_alchemy-0.0.1 (c (n "zigbee2mqtt_types_vendor_alchemy") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ywmxw2s2fimgf6pyy2i0m86j64j5hii53w46h2g3jsj2dys7m7q") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_alchemy-0.2.0 (c (n "zigbee2mqtt_types_vendor_alchemy") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1pkvq80b14hkwx6k0w5pllw80cpm4km9vlx3skg85czw092gn3lx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

