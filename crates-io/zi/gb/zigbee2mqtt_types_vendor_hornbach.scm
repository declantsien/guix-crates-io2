(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hornbach) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hornbach-0.0.1 (c (n "zigbee2mqtt_types_vendor_hornbach") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ik2bcm85xm6as1qb7ha3q64xnbb4a3il0pkll40aj2njpy51j6p") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hornbach-0.2.0 (c (n "zigbee2mqtt_types_vendor_hornbach") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0qj6653x8qsd8zysasr0ix7db3vmaqzglin1v46z8qvpzgg8r64r") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

