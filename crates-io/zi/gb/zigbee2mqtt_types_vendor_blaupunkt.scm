(define-module (crates-io zi gb zigbee2mqtt_types_vendor_blaupunkt) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_blaupunkt-0.0.1 (c (n "zigbee2mqtt_types_vendor_blaupunkt") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cs3lsk15ls4pndw3y8cvmcbhzpzdrqmww63apbwigczcbjwbawq") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_blaupunkt-0.1.0 (c (n "zigbee2mqtt_types_vendor_blaupunkt") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1vrhcfzdzc7ckc3rnfkj5lizrdw9a6d3lh1a3nf2z4sm3pawnwga") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

