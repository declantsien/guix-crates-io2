(define-module (crates-io zi gb zigbee2mqtt_types_vendor_iluminize) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_iluminize-0.0.1 (c (n "zigbee2mqtt_types_vendor_iluminize") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1afxyairqsih6l48fk36yris6bl40g7zndbkp46dxvbh17rkv77n") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_iluminize-0.2.0 (c (n "zigbee2mqtt_types_vendor_iluminize") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1r1n13471kng1z97lqi1gz7kbbslnryshx6c114j4w6xlqpb9ql1") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

