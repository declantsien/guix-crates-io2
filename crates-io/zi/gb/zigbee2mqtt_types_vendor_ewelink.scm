(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ewelink) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ewelink-0.0.1 (c (n "zigbee2mqtt_types_vendor_ewelink") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vafrfvply7glndxzjvv7hd2l1kw6kfpvwbpxypxpjdhbr901x81") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ewelink-0.2.0 (c (n "zigbee2mqtt_types_vendor_ewelink") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1577pyicz7sb7f4nzr5n5cq6j2z0s75a7vlzpa930s8ds7pi7gm7") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

