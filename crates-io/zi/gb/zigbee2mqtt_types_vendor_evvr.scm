(define-module (crates-io zi gb zigbee2mqtt_types_vendor_evvr) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_evvr-0.0.1 (c (n "zigbee2mqtt_types_vendor_evvr") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "043l7mr69njg1q8qzwj8am5i1kdjiws4wc9azdagxisccnmgfz8f") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_evvr-0.2.0 (c (n "zigbee2mqtt_types_vendor_evvr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0k5w8wrnmvm7r5bh463f4nhf8sgfzs6swvac3lq6dk9262npiwv8") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

