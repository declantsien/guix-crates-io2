(define-module (crates-io zi gb zigbee2mqtt_types_vendor_brimate) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_brimate-0.0.1 (c (n "zigbee2mqtt_types_vendor_brimate") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1srz30ydad0zfn3hg7w7bzdhv98hsd85lpz4349ni4nagrs36lrq") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_brimate-0.1.0 (c (n "zigbee2mqtt_types_vendor_brimate") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0557f124k9968p8ylw8ai3qgxxbzmdyfm0va641895zgmanj4qg5") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

