(define-module (crates-io zi gb zigbee2mqtt_types_vendor_eurotronic) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_eurotronic-0.0.1 (c (n "zigbee2mqtt_types_vendor_eurotronic") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hzq67886i98gymn6nlp9cv92l4cfrwnvbla8q649wdgvasrfv0y") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_eurotronic-0.1.0 (c (n "zigbee2mqtt_types_vendor_eurotronic") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1yyj7af6icwmymjzp0vphsknjf5p3lc4whkz4b8mmpfwy8aynmpz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

