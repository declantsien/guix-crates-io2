(define-module (crates-io zi gb zigbee2mqtt_types_vendor_icasa) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_icasa-0.0.1 (c (n "zigbee2mqtt_types_vendor_icasa") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v1cd97gkab89z8cy1svayilp659i4m3rfmiq8wskn2199m90h0n") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_icasa-0.2.0 (c (n "zigbee2mqtt_types_vendor_icasa") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "147clpv76x0svwriclzc24g6l7m1am4cmzmaxgc5y1a5qwv0c4pl") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

