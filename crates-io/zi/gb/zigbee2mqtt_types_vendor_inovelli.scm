(define-module (crates-io zi gb zigbee2mqtt_types_vendor_inovelli) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_inovelli-0.0.1 (c (n "zigbee2mqtt_types_vendor_inovelli") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04i275vyh4lyi99scnxsaddl95dihmzgzkbwj8m40kdpwf3813m0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_inovelli-0.2.0 (c (n "zigbee2mqtt_types_vendor_inovelli") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0hf03968ij795ywb7i1prp8g93lwrx8li18z125zpynljyr3p830") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

