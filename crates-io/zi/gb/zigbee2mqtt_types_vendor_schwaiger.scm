(define-module (crates-io zi gb zigbee2mqtt_types_vendor_schwaiger) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_schwaiger-0.0.1 (c (n "zigbee2mqtt_types_vendor_schwaiger") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hg1c2ardxmkpbp6vkvrf91q2im612hv59rgl1b8z830x04m7vxv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_schwaiger-0.2.0 (c (n "zigbee2mqtt_types_vendor_schwaiger") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0g5ci80r7ibkw40d1ww302nd5mp11xjw8smhrycg1rycjy83kz98") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

