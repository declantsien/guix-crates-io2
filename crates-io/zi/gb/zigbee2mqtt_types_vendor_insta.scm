(define-module (crates-io zi gb zigbee2mqtt_types_vendor_insta) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_insta-0.0.1 (c (n "zigbee2mqtt_types_vendor_insta") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0641vmni355x9qb56shj4zcbpxys3akzl992ras7dxbxaz6lnpbx") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_insta-0.1.0 (c (n "zigbee2mqtt_types_vendor_insta") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1jb5bwxb0k3vgbx9q45z6v5yjxh9zx7vlcr86p2s3vim4s7a65xg") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

