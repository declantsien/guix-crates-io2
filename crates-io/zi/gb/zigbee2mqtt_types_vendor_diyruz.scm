(define-module (crates-io zi gb zigbee2mqtt_types_vendor_diyruz) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_diyruz-0.0.1 (c (n "zigbee2mqtt_types_vendor_diyruz") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p7333kh0xga1189fvagyh387gpx0bp0gqqr1qb8f19dhcrq4fr6") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_diyruz-0.2.0 (c (n "zigbee2mqtt_types_vendor_diyruz") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1s838pc240bw0kv5pcly1a8qwsm3kai2k7nivff7cns6zcpap0dg") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

