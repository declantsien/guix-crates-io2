(define-module (crates-io zi gb zigbee2mqtt_types_vendor_tlw_global) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_tlw_global-0.1.0 (c (n "zigbee2mqtt_types_vendor_tlw_global") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1nkln34kwgmgxidkm9g2b5y8h4cycch2fn7ywl9vd748cjkxxmv3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

