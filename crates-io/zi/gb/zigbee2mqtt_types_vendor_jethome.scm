(define-module (crates-io zi gb zigbee2mqtt_types_vendor_jethome) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_jethome-0.0.1 (c (n "zigbee2mqtt_types_vendor_jethome") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11l8x4h3lsids92hdx67ybyrdhkxy8sgxcdv6b8nhsfgzw10flg1") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_jethome-0.1.0 (c (n "zigbee2mqtt_types_vendor_jethome") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1w01k35yc10crlisn68dc9ykmvqmph5vz1nxx4xzd1q3j5dd327y") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

