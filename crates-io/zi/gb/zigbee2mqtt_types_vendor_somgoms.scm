(define-module (crates-io zi gb zigbee2mqtt_types_vendor_somgoms) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_somgoms-0.0.1 (c (n "zigbee2mqtt_types_vendor_somgoms") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gbvyig3bxalkjmapjpqf2j1valxjk99fi6zzyjy2kmgqrbqvi8k") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_somgoms-0.1.0 (c (n "zigbee2mqtt_types_vendor_somgoms") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1sddp9v5lkfqywg4wyqlzsa9s6xvf3rd2b3z2r0s0xkdrmgdkx5a") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

