(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lixee) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lixee-0.0.1 (c (n "zigbee2mqtt_types_vendor_lixee") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f8zv4i3sjw83xxfjzlbrjbcllznp8z09bclhjsbvr7g9bp4b059") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lixee-0.1.1 (c (n "zigbee2mqtt_types_vendor_lixee") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0i49ld74q1p4rg32xjwzklasfkx9s836jr84d9cq6d42ncmy4qkz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

