(define-module (crates-io zi gb zigbee2mqtt_types_vendor_gewiss) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_gewiss-0.0.1 (c (n "zigbee2mqtt_types_vendor_gewiss") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k9fjy1fqjakbx5grl2sa507s7s3dbmdczsnwcjxrhwn3iw1890g") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_gewiss-0.2.0 (c (n "zigbee2mqtt_types_vendor_gewiss") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1lghhqsl2wx9l6mn7p2w00n7s81fgyn0yjrcknpcjxmwgqh7c1ld") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

