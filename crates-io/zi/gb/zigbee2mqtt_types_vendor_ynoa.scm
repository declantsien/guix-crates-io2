(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ynoa) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ynoa-0.0.1 (c (n "zigbee2mqtt_types_vendor_ynoa") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07qz1nr0v9b8kx8nvpyxxpjkm7lqw4bmpsap6bp5qdf90hpiii2c") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ynoa-0.2.0 (c (n "zigbee2mqtt_types_vendor_ynoa") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1icjb9sa41x0ybga957yc0jhjp4wm9pk1lap779pjkb21m314afw") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

