(define-module (crates-io zi gb zigbee2mqtt_types_vendor_owon) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_owon-0.0.1 (c (n "zigbee2mqtt_types_vendor_owon") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a2zs0ay23lfaqd9m6c7xfxj2hg9f6bf050s8wl663ms2rab6d0c") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_owon-0.2.0 (c (n "zigbee2mqtt_types_vendor_owon") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "18ds4z416wmjwv6fhwh4270h23k07l1y0d4547hcfapxl891x6jx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

