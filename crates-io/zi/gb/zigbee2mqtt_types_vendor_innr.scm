(define-module (crates-io zi gb zigbee2mqtt_types_vendor_innr) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_innr-0.0.1 (c (n "zigbee2mqtt_types_vendor_innr") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wk3db9c72951hfiglmkzxmm0zw7cw3kf5zvqyp7rbc07n2jbfwz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_innr-0.2.0 (c (n "zigbee2mqtt_types_vendor_innr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0dza6mf8vqnk8diy4aw6ds8430cnz5994dks3cyf4jsy59waq1fx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

