(define-module (crates-io zi gb zigbee2mqtt_types_vendor_etop) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_etop-0.0.1 (c (n "zigbee2mqtt_types_vendor_etop") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15gql8sv6d62c51j9hd4c3dfdyqrrc3g2y6s67j9qgbyk8s5hpw3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_etop-0.1.0 (c (n "zigbee2mqtt_types_vendor_etop") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1if3v4amnvn1h0pqqlkciz22817bm390hl3azqm4i4k6pgzwsc3v") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

