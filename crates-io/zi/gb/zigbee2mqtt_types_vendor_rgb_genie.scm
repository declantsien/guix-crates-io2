(define-module (crates-io zi gb zigbee2mqtt_types_vendor_rgb_genie) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_rgb_genie-0.0.1 (c (n "zigbee2mqtt_types_vendor_rgb_genie") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fcm65bc1ga4d2qk8fb4c725pyblzd3d0qqnkmiari2a0aq237h3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_rgb_genie-0.2.0 (c (n "zigbee2mqtt_types_vendor_rgb_genie") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0mk3c8arasv5f2plfrqsjysk0m4zhnk245wk42djyxr1dr144cih") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

