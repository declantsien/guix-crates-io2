(define-module (crates-io zi gb zigbee2mqtt_types_vendor_bosch) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_bosch-0.0.1 (c (n "zigbee2mqtt_types_vendor_bosch") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jjvy1hn9sls006r0l58hb3vnl6dzj3hr5srrfrygib8fdz8hxz5") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_bosch-0.1.1 (c (n "zigbee2mqtt_types_vendor_bosch") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1z7ls13bp9rmb7rd29xkwipa3q64fp4gqw2xgnzy07lchsc4rchp") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

