(define-module (crates-io zi gb zigbee2mqtt_types_vendor_enbrighten) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_enbrighten-0.0.1 (c (n "zigbee2mqtt_types_vendor_enbrighten") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lgmw0b0lxs0a70ki31552g5qyf74k33mf86wiqfks6ibjzbdz0q") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_enbrighten-0.2.0 (c (n "zigbee2mqtt_types_vendor_enbrighten") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0yg0ajmnvlvmax7q3hxrwcii10ar275y8x0whhslldi9zrjyn5nc") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

