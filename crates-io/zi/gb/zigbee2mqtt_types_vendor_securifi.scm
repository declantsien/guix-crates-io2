(define-module (crates-io zi gb zigbee2mqtt_types_vendor_securifi) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_securifi-0.0.1 (c (n "zigbee2mqtt_types_vendor_securifi") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i3kdxljyzh4bkr09qyan0f5gha9ynhykkr5q4g007z4pn3ciyiv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_securifi-0.1.0 (c (n "zigbee2mqtt_types_vendor_securifi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "09kcsk58fgvaqf1d5n8408cwi06mmfryaw06vj8grafxgk4d291p") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

