(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ge) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ge-0.0.1 (c (n "zigbee2mqtt_types_vendor_ge") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01l5rk6mvrr5yzq6yw181yl69kdrir94989c7xd7lq459n389bj8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ge-0.2.0 (c (n "zigbee2mqtt_types_vendor_ge") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "06g3al2jphpwj2lz6qpsf4v45wq83vy5c7qndkdk91jwydgwxkn1") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

