(define-module (crates-io zi gb zigbee2mqtt_types_vendor_saswell) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_saswell-0.0.1 (c (n "zigbee2mqtt_types_vendor_saswell") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0247mra9x5qwr3wjah2iwq0amnklfk7zkfpl6gmq6wq1mq80hrhz") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_saswell-0.1.0 (c (n "zigbee2mqtt_types_vendor_saswell") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1ccyjbb370aq3c9n26jdj5wghb9l7l43vflj9ss0mgfrxwm3l89j") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

