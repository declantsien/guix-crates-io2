(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ajax_online) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ajax_online-0.0.1 (c (n "zigbee2mqtt_types_vendor_ajax_online") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v00fk93amfslb3xjlfmyf4yknmhk9zkpng3kp9ypyn45cyim1ay") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ajax_online-0.2.0 (c (n "zigbee2mqtt_types_vendor_ajax_online") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0pbpp07n6zbqr72jmqppj9c8zr8i4dl4psnwmkjf4h9nalmgk439") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

