(define-module (crates-io zi gb zigbee2mqtt_types_vendor_paulmann) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_paulmann-0.0.1 (c (n "zigbee2mqtt_types_vendor_paulmann") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11x76d0q671qkbcrpb7y4751b1a250vgpgsj7bjj50yl07g1kslp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_paulmann-0.2.0 (c (n "zigbee2mqtt_types_vendor_paulmann") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "03cxsgikkrhj2ri8vznr6pxn7fsisvijj6rvg799p0772fgh9sj2") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

