(define-module (crates-io zi gb zigbee2mqtt_types_vendor_acuity_brands_lighting__abl_) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_acuity_brands_lighting__abl_-0.0.1 (c (n "zigbee2mqtt_types_vendor_acuity_brands_lighting__abl_") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rq0bpw44yhmhfwwc8n2vv5h16kxqkrjn89inbipkaknz1dg20vw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_acuity_brands_lighting__abl_-0.2.0 (c (n "zigbee2mqtt_types_vendor_acuity_brands_lighting__abl_") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1hm6n877k6k2a890jqd38c58m5s7g9pc2xfnyiccz3mia2b73hr9") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

