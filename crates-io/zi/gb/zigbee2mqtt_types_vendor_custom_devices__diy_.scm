(define-module (crates-io zi gb zigbee2mqtt_types_vendor_custom_devices__diy_) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_custom_devices__diy_-0.0.1 (c (n "zigbee2mqtt_types_vendor_custom_devices__diy_") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0paicw5bdbfs70dwdm0wv3fxr7x6q2gg7mhamm24mjbk6av94c5n") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_custom_devices__diy_-0.2.0 (c (n "zigbee2mqtt_types_vendor_custom_devices__diy_") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "15q57qi6z0ns4dmzr4vyg4zxcib0vvzpa8p73v3mpvn5dga408y0") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

