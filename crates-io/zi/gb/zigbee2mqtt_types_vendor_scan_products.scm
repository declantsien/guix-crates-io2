(define-module (crates-io zi gb zigbee2mqtt_types_vendor_scan_products) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_scan_products-0.0.1 (c (n "zigbee2mqtt_types_vendor_scan_products") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fgymxr4xmwhfwq8fxyljsj9q2gkw7vw0s7r31zc6pdw5wv33v7f") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_scan_products-0.2.0 (c (n "zigbee2mqtt_types_vendor_scan_products") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0k3yny1dbx5fslfcachbgsq9952wdvvmc8kbm2jd1pwyxmgj2szn") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

