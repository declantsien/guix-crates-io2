(define-module (crates-io zi gb zigbee2mqtt_types_vendor_airam) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_airam-0.0.1 (c (n "zigbee2mqtt_types_vendor_airam") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0faz65i2bjx297ac1acps7m7ri0xjlvzkbxb1glvp97pck8cw4jk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_airam-0.2.0 (c (n "zigbee2mqtt_types_vendor_airam") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1r17r797smd20ml0qy5skca2xjx2dzvmkgfmxa5lyd6id5jxkv7d") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

