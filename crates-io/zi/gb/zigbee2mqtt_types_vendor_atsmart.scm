(define-module (crates-io zi gb zigbee2mqtt_types_vendor_atsmart) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_atsmart-0.0.1 (c (n "zigbee2mqtt_types_vendor_atsmart") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0013yf0x61glshiykqvczx2r0skk53zyzdsvkzjz6r2bg68va1b0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_atsmart-0.1.0 (c (n "zigbee2mqtt_types_vendor_atsmart") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0jchbncndrszsp6d54qq3bkzdd7rq9pzyhafzb3jx8gahwc51ip3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

