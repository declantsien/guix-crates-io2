(define-module (crates-io zi gb zigbee2mqtt_types_vendor_bticino) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_bticino-0.0.1 (c (n "zigbee2mqtt_types_vendor_bticino") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1si4hffxpczcm7mw5k0swf54mf8z5fvfx6q8cnk4xvna7f1j8ha8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_bticino-0.2.0 (c (n "zigbee2mqtt_types_vendor_bticino") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0cc2ac97cphrydsh7g4ya0vhz09rpxplmc937ghz4s1m9wk752l9") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

