(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lifecontrol) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lifecontrol-0.0.1 (c (n "zigbee2mqtt_types_vendor_lifecontrol") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "107l3y0f9zkksn26iyk04slgpvcraqgagvnzcahdq8kz7jvz9cs5") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lifecontrol-0.2.0 (c (n "zigbee2mqtt_types_vendor_lifecontrol") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0xrqa12v4i0kv308890y4bdkl27warswavjc47l4bd2c0vc9jp8g") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

