(define-module (crates-io zi gb zigbee2mqtt_types_vendor_istar) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_istar-0.0.1 (c (n "zigbee2mqtt_types_vendor_istar") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gm9r9ix6qqz0hvqp0aa9q829j4pbyfdjfyf3vy4h0zp415bdqfc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_istar-0.2.0 (c (n "zigbee2mqtt_types_vendor_istar") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "03s2sn7z3vi52a860ahls0k62mc5limmqiynvv5zfj7ivl0jrngf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

