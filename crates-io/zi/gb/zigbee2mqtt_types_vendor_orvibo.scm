(define-module (crates-io zi gb zigbee2mqtt_types_vendor_orvibo) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_orvibo-0.0.1 (c (n "zigbee2mqtt_types_vendor_orvibo") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y9pnnjjds2ajbphm9vympk98l8r6w7shimvdyjy0bjdzg35l5b3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_orvibo-0.2.0 (c (n "zigbee2mqtt_types_vendor_orvibo") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "060qa1izmfldr15v4c9aiddkd6f2667ng0sggaabvmbr914j671w") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

