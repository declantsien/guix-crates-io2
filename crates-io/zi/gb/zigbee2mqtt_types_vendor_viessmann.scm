(define-module (crates-io zi gb zigbee2mqtt_types_vendor_viessmann) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_viessmann-0.0.1 (c (n "zigbee2mqtt_types_vendor_viessmann") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m5nsrbsg3pqfzv1rb9z4475hh60d85xzn2ql2ha0j4xfdn0frbq") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_viessmann-0.2.0 (c (n "zigbee2mqtt_types_vendor_viessmann") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kd9gm9jcigd5il2000fdrlbpmzm92pc8zd662z4w02rgqhf5r57") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

