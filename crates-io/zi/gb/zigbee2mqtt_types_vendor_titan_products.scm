(define-module (crates-io zi gb zigbee2mqtt_types_vendor_titan_products) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_titan_products-0.0.1 (c (n "zigbee2mqtt_types_vendor_titan_products") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10v608l5xhf6gx9q5dp67byh1p1a5sfz34l40q6faq8x53ldkxkc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_titan_products-0.1.0 (c (n "zigbee2mqtt_types_vendor_titan_products") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0w1zzblr9c404c2vcrjmpf5c271f8zhwp1i3pgc1r7jppr0gykqi") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

