(define-module (crates-io zi gb zigbee2mqtt_types_vendor_plaid_systems) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_plaid_systems-0.0.1 (c (n "zigbee2mqtt_types_vendor_plaid_systems") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01405r6h3sv6x4rnk665wvpy72wb4y1rycp85glikzcq4pwaka0c") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_plaid_systems-0.1.0 (c (n "zigbee2mqtt_types_vendor_plaid_systems") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16ajbxjc9bcmiwwpglxvc52i6xd511wwdws53qgxss5s872b754n") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

