(define-module (crates-io zi gb zigbee2mqtt_types_vendor_swann) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_swann-0.0.1 (c (n "zigbee2mqtt_types_vendor_swann") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ascmcj7sm1mw3nw1gmkpqr73mi6qadw5b3b3l1rqn4cnlc0yqi7") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_swann-0.1.0 (c (n "zigbee2mqtt_types_vendor_swann") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "03lnm02yshq05lm0nqq0v17lxfm2fnqd43rn4dm4q9vz9kadi0gx") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

