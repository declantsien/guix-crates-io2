(define-module (crates-io zi gb zigbee2mqtt_types_vendor_current_products_corp) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_current_products_corp-0.0.1 (c (n "zigbee2mqtt_types_vendor_current_products_corp") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j2n6j9vf7lay8z5x3az2a6vw2zfzjbqv0zpfbrirhhlq3x5j279") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_current_products_corp-0.1.0 (c (n "zigbee2mqtt_types_vendor_current_products_corp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16g7n3ph53267rss0hlblf8cikj52xni3v0sj5f8wyhwzgz1gbml") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

