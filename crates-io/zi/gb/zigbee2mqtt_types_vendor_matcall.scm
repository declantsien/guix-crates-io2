(define-module (crates-io zi gb zigbee2mqtt_types_vendor_matcall) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_matcall-0.0.1 (c (n "zigbee2mqtt_types_vendor_matcall") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "156n4ihcnbn2javpak2dnf3dxcl1a0xvici6npi27j83jx2b2d8i") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_matcall-0.2.0 (c (n "zigbee2mqtt_types_vendor_matcall") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1gfhfqqh47gp0h2f4lhm03i49l2f1ysr7wfc8cgvx6masfwq6dvl") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

