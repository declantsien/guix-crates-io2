(define-module (crates-io zi gb zigbee2mqtt_types_vendor_axis) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_axis-0.0.1 (c (n "zigbee2mqtt_types_vendor_axis") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cm0lfxqcp1fm340vs33sdw0kzn2shy96hvwn2gk4k1xj937l1xk") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_axis-0.1.0 (c (n "zigbee2mqtt_types_vendor_axis") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "03rr3fpkqz6jccsiiryhnqhb96jg175nr5aqacn33inl3anfvl49") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

