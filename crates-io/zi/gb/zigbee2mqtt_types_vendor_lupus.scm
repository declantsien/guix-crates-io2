(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lupus) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lupus-0.0.1 (c (n "zigbee2mqtt_types_vendor_lupus") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rs6bs3gx07fzw714pg6w4ii5nariw451xlwjk8ag9nnyf2bswhd") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lupus-0.2.0 (c (n "zigbee2mqtt_types_vendor_lupus") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0am5xz0xg6qn42pdry8843lcbn8ss16hak4df93q6qy8nfkqw19g") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

