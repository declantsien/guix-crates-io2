(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lg_electronics) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lg_electronics-0.0.1 (c (n "zigbee2mqtt_types_vendor_lg_electronics") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05g68dsdbxn6684279xq982kfdspfm5mbf7vwfxpx4j3fl6j917k") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lg_electronics-0.2.0 (c (n "zigbee2mqtt_types_vendor_lg_electronics") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "19lh5wasr82psgd2flbsvhnbplfvk8b36rdy1c3g4h7k2vvggs8h") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

