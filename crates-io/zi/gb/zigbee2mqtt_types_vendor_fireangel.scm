(define-module (crates-io zi gb zigbee2mqtt_types_vendor_fireangel) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_fireangel-0.0.1 (c (n "zigbee2mqtt_types_vendor_fireangel") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p338jfn8g4mc8fhhz0bd1q7ykh9d97z1wz4a6fwswpy3l656y18") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_fireangel-0.1.0 (c (n "zigbee2mqtt_types_vendor_fireangel") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gqazjw7dnp0p4w1mpzjhl9cf0ggdbdv698n6fd4smhxqnqzfppv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

