(define-module (crates-io zi gb zigbee2mqtt_types_vendor_philips) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_philips-0.0.1 (c (n "zigbee2mqtt_types_vendor_philips") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ijy1cgad6bwsab8gm5y702949wga0f8wb5awglxx8zls0p2hchn") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_philips-0.2.0 (c (n "zigbee2mqtt_types_vendor_philips") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0c8a0sjdm4pcshv8fcxzj1816mh065h3vccgilpjvpnfmw87bp0j") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

