(define-module (crates-io zi gb zigbee2mqtt_types_vendor_namron) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_namron-0.0.1 (c (n "zigbee2mqtt_types_vendor_namron") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a5ka4wb41rlxipq53y35486628d4csyz8py81xzzfk38mavndrc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_namron-0.2.0 (c (n "zigbee2mqtt_types_vendor_namron") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0xclv0ghn4ajn9nqd99ivrqylqvj9ffb1ldq9nw8xjlci6az3cyl") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

