(define-module (crates-io zi gb zigbee2mqtt_types_vendor_adurosmart) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_adurosmart-0.0.1 (c (n "zigbee2mqtt_types_vendor_adurosmart") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zxhj0avnhxj04gczkm4m7ycnbz4g1g6qmi4b77inp67w91v77ai") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_adurosmart-0.2.0 (c (n "zigbee2mqtt_types_vendor_adurosmart") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "00pbbqji34pdh2sm54rmkwljhy6pimh89gpqdig5irq65bm5q3sg") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

