(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hzc_electric) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hzc_electric-0.0.1 (c (n "zigbee2mqtt_types_vendor_hzc_electric") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n1xvpwh77afd21cav8rjyxcqkwsg35dmz6ccvbpy80jagjv363w") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hzc_electric-0.1.0 (c (n "zigbee2mqtt_types_vendor_hzc_electric") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0z8ss2sj22fcqhdgw9i7kqb340l9jvl9y2ih0rggyh4x85mglzg4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

