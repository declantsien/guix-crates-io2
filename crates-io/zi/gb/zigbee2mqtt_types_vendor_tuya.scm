(define-module (crates-io zi gb zigbee2mqtt_types_vendor_tuya) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_tuya-0.0.1 (c (n "zigbee2mqtt_types_vendor_tuya") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mcr4chl5ha4q56libhl0fcsv9xy9hpm0wsy7hzp2ymrqj2vb8bm") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_tuya-0.2.0 (c (n "zigbee2mqtt_types_vendor_tuya") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1acs8l5k9n8y2mpnjz6gv6vjy1zxkgl04p6adz1l6rfjixiahjz0") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

