(define-module (crates-io zi gb zigbee2mqtt_types_base_types) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_base_types-0.1.0 (c (n "zigbee2mqtt_types_base_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i11kpxnn61v13n49pcrgwjd6nvb52k7z6v8sxk9i0zcvzcynnm7") (f (quote (("debug") ("clone"))))))

