(define-module (crates-io zi gb zigbee2mqtt_types_vendor_envilar) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_envilar-0.0.1 (c (n "zigbee2mqtt_types_vendor_envilar") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ijjcfbpd9qgg506mzawdsqpv9rl65pkxk84vcdxkyxs3117xbfi") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_envilar-0.2.0 (c (n "zigbee2mqtt_types_vendor_envilar") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0awwl8057yc4412wrh8divl92557aq5rlycp3hqbdjyavrn6ll5p") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

