(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ninja_blocks) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ninja_blocks-0.0.1 (c (n "zigbee2mqtt_types_vendor_ninja_blocks") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0adf98d4263f5mxg265pfwiiv3vl1cddhjhwmg1d4pwv4clvmpfv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ninja_blocks-0.1.0 (c (n "zigbee2mqtt_types_vendor_ninja_blocks") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0qjfbdb5q9c1hnxvhl45c3xxknd1q10x243649kad3y1bxgn3b97") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

