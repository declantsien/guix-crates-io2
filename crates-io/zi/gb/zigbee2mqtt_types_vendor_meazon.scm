(define-module (crates-io zi gb zigbee2mqtt_types_vendor_meazon) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_meazon-0.0.1 (c (n "zigbee2mqtt_types_vendor_meazon") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01zh1ydf8qvk4mngf9z62mdacr6xcflgvcywwgik9vk9l3wc07ya") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_meazon-0.1.0 (c (n "zigbee2mqtt_types_vendor_meazon") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0bqjdpx1kffrfkhrr0w5c6c9hm6rrzl549ci0zl205jmrxmcqfqq") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

