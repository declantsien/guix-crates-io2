(define-module (crates-io zi gb zigbee2mqtt_types_vendor_smartenit) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_smartenit-0.0.1 (c (n "zigbee2mqtt_types_vendor_smartenit") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p8rv1v3yysk53111r7dgph1bb3wahycjj05i0kxdxf1gi9vwq5a") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_smartenit-0.1.0 (c (n "zigbee2mqtt_types_vendor_smartenit") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0r25k29w7rrzw8pkq7m45j6167zd86f6v3mf1vv2inywyss17iyd") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

