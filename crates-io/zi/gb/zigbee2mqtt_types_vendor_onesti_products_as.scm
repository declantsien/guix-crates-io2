(define-module (crates-io zi gb zigbee2mqtt_types_vendor_onesti_products_as) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_onesti_products_as-0.0.1 (c (n "zigbee2mqtt_types_vendor_onesti_products_as") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cgim9620p1gpw3f4adbd9wa19kax5f3498pb6d6sjmpx7h5lvz1") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_onesti_products_as-0.1.0 (c (n "zigbee2mqtt_types_vendor_onesti_products_as") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "09qyl10xkmmsb6pcgzxc4va66f674fsjc8z04y222zs1wwzn4ilb") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

