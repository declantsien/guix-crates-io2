(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ihorn) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ihorn-0.0.1 (c (n "zigbee2mqtt_types_vendor_ihorn") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06i4z66s356sd0im5ljy7djmk01a09vll84xb23nfdh1615vny80") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ihorn-0.1.0 (c (n "zigbee2mqtt_types_vendor_ihorn") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1g4bfgqwz33gd361z1y5dakvh4arr4ib5hgwd5rlk66rqgh3v2cf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

