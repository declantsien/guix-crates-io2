(define-module (crates-io zi gb zigbee2mqtt_types_vendor_eglo) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_eglo-0.0.1 (c (n "zigbee2mqtt_types_vendor_eglo") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bkcgiygh29c7vn3vwx7zdcd5b5mw8zv8n48jj9pk7mx07rwvvga") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_eglo-0.2.0 (c (n "zigbee2mqtt_types_vendor_eglo") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0pd4pz8nfpm4d33lbk7ainvmbklkcygrynkh023w73vfwhjbh5a6") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

