(define-module (crates-io zi gb zigbee2mqtt_types_vendor_nordtronic) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_nordtronic-0.0.1 (c (n "zigbee2mqtt_types_vendor_nordtronic") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nkqfjlcwxwzsy0bspaz4z2vmpx3cf2mg53n64wjkjilw316hs8w") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_nordtronic-0.2.0 (c (n "zigbee2mqtt_types_vendor_nordtronic") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kdx3jl9azbadlry1f67d3cj9a33q0p7f6aswkf3iy7b0ba0a2kq") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

