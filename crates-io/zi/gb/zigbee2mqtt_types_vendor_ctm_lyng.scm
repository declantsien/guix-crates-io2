(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ctm_lyng) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ctm_lyng-0.0.1 (c (n "zigbee2mqtt_types_vendor_ctm_lyng") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s9r2gvyisx74facal3kfx854bs7wqp4401s3w54ghbzkps6g2sf") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ctm_lyng-0.1.0 (c (n "zigbee2mqtt_types_vendor_ctm_lyng") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0wcaj7la1cks625x7s6qbrfp06b29wbwfirvmzq1dikwc0imp4lz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

