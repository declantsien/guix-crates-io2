(define-module (crates-io zi gb zigbee2mqtt_types_vendor_ecolink) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_ecolink-0.0.1 (c (n "zigbee2mqtt_types_vendor_ecolink") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dnxl0qg586qnkbzfvzb2w3w4da0g9f5vhzv0ian722jq9n6prh3") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_ecolink-0.1.0 (c (n "zigbee2mqtt_types_vendor_ecolink") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "01x4mzi4gyp8sgd3lvx8yl7cxy62yf50vgrbc3ma22s5arxrbjl3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

