(define-module (crates-io zi gb zigbee2mqtt_types_vendor_vbled) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_vbled-0.0.1 (c (n "zigbee2mqtt_types_vendor_vbled") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aadqhxln42lsylmz3l3jzm93fwfpyqb1k0q4rj6ki69z79f3a1h") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_vbled-0.2.0 (c (n "zigbee2mqtt_types_vendor_vbled") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0zs3wq921idqss76v2pj0plrj6sy3v9kqvw3ga87icxp64js1167") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

