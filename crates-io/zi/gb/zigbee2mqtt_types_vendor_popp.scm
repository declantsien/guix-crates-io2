(define-module (crates-io zi gb zigbee2mqtt_types_vendor_popp) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_popp-0.0.1 (c (n "zigbee2mqtt_types_vendor_popp") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x17vz6fsfkqqndsj065zbypza7547d88m5jcpih5mzzkr656vcp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_popp-0.2.0 (c (n "zigbee2mqtt_types_vendor_popp") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1gxvsfmn71cylcaqvpci7i30li4z8dnfq4r51bsch4y5gclf87vw") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

