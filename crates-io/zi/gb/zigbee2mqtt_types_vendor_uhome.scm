(define-module (crates-io zi gb zigbee2mqtt_types_vendor_uhome) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_uhome-0.1.0 (c (n "zigbee2mqtt_types_vendor_uhome") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0bjl546kjas7ah4a26wribh2k1af2gaasdsnl16y5l2bhzfsaibm") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

