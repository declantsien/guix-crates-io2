(define-module (crates-io zi gb zigbee2mqtt_types_vendor_gs) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_gs-0.0.1 (c (n "zigbee2mqtt_types_vendor_gs") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rmh78vybbakjafs4pp378aabznv77w1s02y4b696xr6kpzmpycg") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_gs-0.2.0 (c (n "zigbee2mqtt_types_vendor_gs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "09s4h58kzvzsfdbg9xh7fnbkx6llaklmzxyh5220y5ijavrcmv3q") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

