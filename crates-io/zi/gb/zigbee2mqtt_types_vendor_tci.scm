(define-module (crates-io zi gb zigbee2mqtt_types_vendor_tci) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_tci-0.0.1 (c (n "zigbee2mqtt_types_vendor_tci") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07yyvlqb69v2p5cmhy02a8z2nqv5yvhzvn3p663vwvks6v4jb5qg") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_tci-0.2.0 (c (n "zigbee2mqtt_types_vendor_tci") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1i05awd5ivhw73b8sidrcgjz2irljg3wbdr3x26z025076xb2w88") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

