(define-module (crates-io zi gb zigbee2mqtt_types_vendor_the_light_group) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_the_light_group-0.0.1 (c (n "zigbee2mqtt_types_vendor_the_light_group") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09cxjc1pgwqlb4ifq66kyirwjb3dfamhcr26lwzfbjbhy8aj1i4i") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_the_light_group-0.2.0 (c (n "zigbee2mqtt_types_vendor_the_light_group") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "00kzrhnfckz20jpx1rpsb6hv08srrcg20mv81vnkaz883qyac0nk") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

