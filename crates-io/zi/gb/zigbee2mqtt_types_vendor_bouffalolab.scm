(define-module (crates-io zi gb zigbee2mqtt_types_vendor_bouffalolab) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_bouffalolab-0.0.1 (c (n "zigbee2mqtt_types_vendor_bouffalolab") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0clci6ji07mspiahlkhxw1n0dfaxzkiri7xzqm5c14g4ja4sj937") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_bouffalolab-0.2.0 (c (n "zigbee2mqtt_types_vendor_bouffalolab") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "07py1az8vfaz3gkd15fakqnpfy9798yaqif94hc3wl751f8i4a2g") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

