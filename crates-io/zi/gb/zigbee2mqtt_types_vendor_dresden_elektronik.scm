(define-module (crates-io zi gb zigbee2mqtt_types_vendor_dresden_elektronik) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_dresden_elektronik-0.0.1 (c (n "zigbee2mqtt_types_vendor_dresden_elektronik") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v73jkfm0yp8izld1jl9fl4n171llf2zq7p1iynmcw8xz9jmxwvd") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_dresden_elektronik-0.2.0 (c (n "zigbee2mqtt_types_vendor_dresden_elektronik") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1h25zirqkpfnx9vakcjwpnc46jma52xs9wy5vi3rj2z595a1f14x") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

