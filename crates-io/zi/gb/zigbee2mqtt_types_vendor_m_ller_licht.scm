(define-module (crates-io zi gb zigbee2mqtt_types_vendor_m_ller_licht) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_m_ller_licht-0.0.1 (c (n "zigbee2mqtt_types_vendor_m_ller_licht") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10dfrj7dxswsw8j1z63krfyqjb04rwvnwryg6lbgkd4jx45biiir") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_m_ller_licht-0.2.0 (c (n "zigbee2mqtt_types_vendor_m_ller_licht") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0w8gnphhp2fq69pghvnp1xxk3r8gvpsj3rlnf1g4z1225bcqs5qv") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

