(define-module (crates-io zi gb zigbee2mqtt_types_vendor_edp) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_edp-0.0.1 (c (n "zigbee2mqtt_types_vendor_edp") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10r2jrkfbblgnsspsd3li2f7gby9h9qqd6c40d1y6jjvagqjmng4") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_edp-0.2.0 (c (n "zigbee2mqtt_types_vendor_edp") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "15a47bssls1ph8dygnjhk5qpj1aqj4q9iha6nsyclxhp54s4y987") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

