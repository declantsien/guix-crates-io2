(define-module (crates-io zi gb zigbee2mqtt_types_vendor_hej) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_hej-0.0.1 (c (n "zigbee2mqtt_types_vendor_hej") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05qsmhi4v4d6s8dpnkdd4zybk2l75ak3n8k4j4dckbg7bhkm0rwh") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_hej-0.2.0 (c (n "zigbee2mqtt_types_vendor_hej") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1iv92vspy6mw8b0kxgr3486d3fhrn3kdpf211pf9x241ljd6ca9x") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

