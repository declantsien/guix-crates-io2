(define-module (crates-io zi gb zigbee2mqtt_types_vendor_halemeier) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_halemeier-0.0.1 (c (n "zigbee2mqtt_types_vendor_halemeier") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wbbk1lddihxa8sz581pzladg0kknpirmry17ds4bcnayiansxdc") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_halemeier-0.2.0 (c (n "zigbee2mqtt_types_vendor_halemeier") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0100mv823vyl37mh0vymn106fmyxck613jyvra9r99m1af3qjybh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

