(define-module (crates-io zi gb zigbee2mqtt_types_vendor_letsleds) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_letsleds-0.0.1 (c (n "zigbee2mqtt_types_vendor_letsleds") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wiwj94nzdi31yhdwrf8ccjybgdgb1waq9yf5idi96kgclajjriv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_letsleds-0.2.0 (c (n "zigbee2mqtt_types_vendor_letsleds") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0lqhvpkxwsx2gkaliv3ppa2g5pncyaz8649nzcf400mg046ysjs4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

