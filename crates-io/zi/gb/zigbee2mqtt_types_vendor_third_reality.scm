(define-module (crates-io zi gb zigbee2mqtt_types_vendor_third_reality) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_third_reality-0.0.1 (c (n "zigbee2mqtt_types_vendor_third_reality") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15bxjahas2wmlnpq8302aixmc537nbl108b85dg0raibnl6wbxvv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_third_reality-0.2.0 (c (n "zigbee2mqtt_types_vendor_third_reality") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "03kiwr6bw38v91by7fv2pd9k82m3mkp87hjya9x5axz4s7br9l9f") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

