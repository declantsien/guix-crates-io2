(define-module (crates-io zi gb zigbee2mqtt_types_vendor_cree) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_cree-0.0.1 (c (n "zigbee2mqtt_types_vendor_cree") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m4c2v24ip1c4kqs66kjqw4qjknjpvsjj6ps1p37wiap7h15hqzw") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_cree-0.2.0 (c (n "zigbee2mqtt_types_vendor_cree") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gs707gdcfrx4481ig2x7985v6fhx1czcpjwix6sdh7aivglpw51") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

