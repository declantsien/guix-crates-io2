(define-module (crates-io zi gb zigbee2mqtt_types_vendor_candeo) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_candeo-0.0.1 (c (n "zigbee2mqtt_types_vendor_candeo") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x0mqi8ldi5600zavgfya4srpngp2kpz7njwzkq9nvh6qrr3pih0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_candeo-0.2.0 (c (n "zigbee2mqtt_types_vendor_candeo") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0idz76pv1nxm5il2wbczh1q4hss47q52qi757v4vpv007rcf4zv4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

