(define-module (crates-io zi gb zigbee2mqtt_types_vendor_nodon) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_nodon-0.0.1 (c (n "zigbee2mqtt_types_vendor_nodon") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fgxpvlmyzqs4l6g9lmcp0fzy83n31i8jjzckwfb5n4m6j4nzhg8") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_nodon-0.2.0 (c (n "zigbee2mqtt_types_vendor_nodon") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "17n5xgyzk6kkxq5pbyxx2gm8qylhw6r04pj5vlxiy9ky424vb4wz") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

