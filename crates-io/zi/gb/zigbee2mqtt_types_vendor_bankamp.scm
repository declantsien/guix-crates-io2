(define-module (crates-io zi gb zigbee2mqtt_types_vendor_bankamp) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_bankamp-0.1.0 (c (n "zigbee2mqtt_types_vendor_bankamp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0bd119dx5vhfk7i06kfcsq1q4zsvcvjy08irv8mq4yr0v93f6j9x") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

