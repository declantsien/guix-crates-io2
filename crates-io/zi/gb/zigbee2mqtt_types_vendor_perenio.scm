(define-module (crates-io zi gb zigbee2mqtt_types_vendor_perenio) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_perenio-0.0.1 (c (n "zigbee2mqtt_types_vendor_perenio") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xzmgiwlb5hs5vmn7h6f3s3g15y31lyz5acrjslhhj2vnrxn1r4f") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_perenio-0.1.0 (c (n "zigbee2mqtt_types_vendor_perenio") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1b7mcrx6kf3s9s677a70nphkvrfvfzl0dnf91c4rp6r55g5q80ln") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

