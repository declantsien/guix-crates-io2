(define-module (crates-io zi gb zigbee2mqtt_types_vendor_villeroy___boch) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_villeroy___boch-0.0.1 (c (n "zigbee2mqtt_types_vendor_villeroy___boch") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d3wkydxcp00awqvn6klp9j20l862nc163xa59jw0a9i76w925gd") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_villeroy___boch-0.2.0 (c (n "zigbee2mqtt_types_vendor_villeroy___boch") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "07ydp0bbad1032khwgka81a619c5x3y79459qbwkkhyp1swmj8hf") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

