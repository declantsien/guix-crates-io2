(define-module (crates-io zi gb zigbee2mqtt_types_vendor_evn) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_evn-0.0.1 (c (n "zigbee2mqtt_types_vendor_evn") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p1xdglf637vfl03gz1rc3xmyka54wc5hnp3qqimljfprcygdj7i") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_evn-0.2.0 (c (n "zigbee2mqtt_types_vendor_evn") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0yasy0q1aiz1c9zaad407mcyp5syj8bq0zssbbdwpdp3smcplsj7") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

