(define-module (crates-io zi gb zigbee2mqtt_types_vendor_leviton) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_leviton-0.0.1 (c (n "zigbee2mqtt_types_vendor_leviton") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yyx6gzcyj9z0mp0fmdhkmi6i6shpif6hn4pianygd4yshsmphv0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_leviton-0.2.0 (c (n "zigbee2mqtt_types_vendor_leviton") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "05a31fwzfiw0x1xq0wsp532hq8i1vnadrs4xxpw2xi6ib3ysvn9p") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

