(define-module (crates-io zi gb zigbee2mqtt_types_vendor_dawon_dns) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_dawon_dns-0.0.1 (c (n "zigbee2mqtt_types_vendor_dawon_dns") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cil9q983bmraxrg04y8g0w66vlzn8zdzvffbwy4jd1imcznx5vy") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_dawon_dns-0.1.0 (c (n "zigbee2mqtt_types_vendor_dawon_dns") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0wdjbb8cdq2jp6dhspjbw41wh1z461gilnk8p96zdd3srdjzz3ar") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

