(define-module (crates-io zi gb zigbee2mqtt_types_vendor_salus_controls) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_salus_controls-0.0.1 (c (n "zigbee2mqtt_types_vendor_salus_controls") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r2syvdk626qmwmfbj771z5yyd5kngwh5al0644cjf5hr0qaycs2") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_salus_controls-0.2.0 (c (n "zigbee2mqtt_types_vendor_salus_controls") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0fdvjddarir5sbqfx8vi4ak2rvs5hch516srzlp6jis4mzb2w6yk") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

