(define-module (crates-io zi gb zigbee2mqtt_types_vendor_danfoss) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_danfoss-0.0.1 (c (n "zigbee2mqtt_types_vendor_danfoss") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y2df5qmk8a8d64jkzpw2dkp0kk44x5salhwc6na3wcsz02rm437") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_danfoss-0.2.0 (c (n "zigbee2mqtt_types_vendor_danfoss") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0x29z8y2mijh317bkvvszkvi8nwdp1gn2iryqhfrcal7nd6rmb71") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

