(define-module (crates-io zi gb zigbee2mqtt_types_vendor_datek) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_datek-0.0.1 (c (n "zigbee2mqtt_types_vendor_datek") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gjgqpfb7qa6ixb1kdvyvkp54bjyshfq9marb95mijfvxvlbbz94") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_datek-0.2.0 (c (n "zigbee2mqtt_types_vendor_datek") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "1447vkx9j1aznxwvviivmm8wnkw4rikj12ld05yw6g1mmj8z51z3") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

