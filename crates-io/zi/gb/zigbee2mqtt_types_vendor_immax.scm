(define-module (crates-io zi gb zigbee2mqtt_types_vendor_immax) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_immax-0.0.1 (c (n "zigbee2mqtt_types_vendor_immax") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zpzwlafp92p2rzj19wd2vnww1fb8djyi7wb02a6a3l84lh138il") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_immax-0.2.0 (c (n "zigbee2mqtt_types_vendor_immax") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "07r06qbxf9n9kr3ffcg81g9yx1bmjnzk2yxyx7h5aqk1nz4smz7b") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

