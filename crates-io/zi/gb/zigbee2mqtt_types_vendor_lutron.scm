(define-module (crates-io zi gb zigbee2mqtt_types_vendor_lutron) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_lutron-0.0.1 (c (n "zigbee2mqtt_types_vendor_lutron") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iiig803ffvvpq0p05y6als08ghsq17zwsa5d4c3vl8pkk77qjlp") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_lutron-0.2.0 (c (n "zigbee2mqtt_types_vendor_lutron") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0zhy0cvz2435s018vbpg39wydksz0q7xgnhrwdqd8vkhbdz6gyry") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

