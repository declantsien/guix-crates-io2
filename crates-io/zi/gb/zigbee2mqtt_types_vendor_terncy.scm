(define-module (crates-io zi gb zigbee2mqtt_types_vendor_terncy) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_terncy-0.0.1 (c (n "zigbee2mqtt_types_vendor_terncy") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yhygkj5w1326d226snh6sxyc9k0wa846zvkv53dpc7r3brywniv") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_terncy-0.2.0 (c (n "zigbee2mqtt_types_vendor_terncy") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0bnpdcsqx0faw6zpkm2dllkg3dsdd5ili3id82j0s3acl7pllsym") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

