(define-module (crates-io zi gb zigbee2mqtt_types_vendor_gledopto) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_gledopto-0.0.1 (c (n "zigbee2mqtt_types_vendor_gledopto") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08fgxxm0x5a0l40vgam5rqrjgjwxirj27gqzjad8v5lnlamnxci0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_gledopto-0.2.0 (c (n "zigbee2mqtt_types_vendor_gledopto") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0c638ham8lnrs1p8pm8lv8ks5vb2jf0qx6ig5gihp9x7y5knghjh") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

