(define-module (crates-io zi gb zigbee2mqtt_types_vendor_vimar) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_vimar-0.0.1 (c (n "zigbee2mqtt_types_vendor_vimar") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rb7115a4y88fc7az289l5ccwns382drhd33ls2cadp16yppj89f") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_vimar-0.2.0 (c (n "zigbee2mqtt_types_vendor_vimar") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0sf6kpdx252hr1svkisq6iy1fm9iq9nd36zq6skn3qbxapir30g4") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

