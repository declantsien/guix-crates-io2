(define-module (crates-io zi gb zigbee2mqtt_types_vendor_atlantic_group) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_atlantic_group-0.0.1 (c (n "zigbee2mqtt_types_vendor_atlantic_group") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "095n6968b1wf97g594xb685njrqxiqi0jwkam9585zfjl2sgsl8x") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_atlantic_group-0.2.0 (c (n "zigbee2mqtt_types_vendor_atlantic_group") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "073ynfw9g7qf283xy9ii9x0m20k9cn77h1klphmyadgcib123fr7") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

