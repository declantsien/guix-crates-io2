(define-module (crates-io zi gb zigbee2mqtt_types_vendor_aurora) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_aurora-0.0.1 (c (n "zigbee2mqtt_types_vendor_aurora") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sixi7qg7fk5bmwf43krdjzxq9ss6f26ilqb6p53a9m4q580rjb0") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_aurora-0.2.0 (c (n "zigbee2mqtt_types_vendor_aurora") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "0gdlyixzyvrjhdy1l0ar2x8xfm4ylhc72qw0qnb04kjakmnhy08w") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

