(define-module (crates-io zi gb zigbee2mqtt_types_vendor_visonic) #:use-module (crates-io))

(define-public crate-zigbee2mqtt_types_vendor_visonic-0.0.1 (c (n "zigbee2mqtt_types_vendor_visonic") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h9phlvx9hph4vpjbfcyq9m50gzky6xhqj9kg7y8lc3ni1y5rb3w") (f (quote (("last_seen_iso_8601") ("last_seen_epoch") ("elapsed") ("debug") ("clone"))))))

(define-public crate-zigbee2mqtt_types_vendor_visonic-0.2.0 (c (n "zigbee2mqtt_types_vendor_visonic") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zigbee2mqtt_types_base_types") (r "^0.1.0") (d #t) (k 0)))) (h "16l0cdjhw2c55i834pn3myqfwqsbskhdbfxbwcjhaijn76kaxas5") (f (quote (("debug" "zigbee2mqtt_types_base_types/debug") ("clone" "zigbee2mqtt_types_base_types/clone"))))))

