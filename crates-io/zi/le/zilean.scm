(define-module (crates-io zi le zilean) #:use-module (crates-io))

(define-public crate-zilean-0.1.0 (c (n "zilean") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "1xqfl46zb1kc4iinp4y044xxvdb644lqviijcnjkkg05irs4bjlx")))

