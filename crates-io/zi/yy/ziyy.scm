(define-module (crates-io zi yy ziyy) #:use-module (crates-io))

(define-public crate-ziyy-0.1.0 (c (n "ziyy") (v "0.1.0") (h "0pprr8n0vlczl17q8c8zf7rf12f1yz3glhmqlnvwzwh1vdqqycr1")))

(define-public crate-ziyy-0.1.1 (c (n "ziyy") (v "0.1.1") (h "153d0q1mq34n3mhai42h1asfcz7mw88rhmjhg2wklr2nan8s2rc6")))

