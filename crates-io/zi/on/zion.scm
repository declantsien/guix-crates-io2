(define-module (crates-io zi on zion) #:use-module (crates-io))

(define-public crate-zion-0.1.0 (c (n "zion") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1v8k04khsr2qchnfyivr4xhi2a0gppr2vpjwfmmw6n65rngcy8qx")))

(define-public crate-zion-0.1.1 (c (n "zion") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0a8m3m23w0y7lgw0gg3hkq37gg14xg1nmwv3zvhq5vvzjg1kbl66")))

