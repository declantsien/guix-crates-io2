(define-module (crates-io zi o- zio-sendfile) #:use-module (crates-io))

(define-public crate-zio-sendfile-0.1.0 (c (n "zio-sendfile") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)))) (h "13jznn6lw8abk1li3bajp0mjm0jr3dnz7d0aapq1nbwl6qfr9mcy")))

(define-public crate-zio-sendfile-0.1.1 (c (n "zio-sendfile") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)))) (h "1cdsmdyd8f92hxj0iwgrbn1r3176g0zvmzsmf63yajish7kyc6a2")))

(define-public crate-zio-sendfile-0.2.0 (c (n "zio-sendfile") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)))) (h "093anzyvw7vpgm5wrb9banwyj0arb0xlywnckasa91dshfqj15xi")))

