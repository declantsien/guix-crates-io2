(define-module (crates-io zi pw zipwith) #:use-module (crates-io))

(define-public crate-zipWith-0.1.0 (c (n "zipWith") (v "0.1.0") (h "00wsmyhx77051y3fgnbwwq6v597my4d5vxlrf6wgibdyiysddjzb")))

(define-public crate-zipWith-0.2.0 (c (n "zipWith") (v "0.2.0") (h "01jnz0abz6d8yf80nwyr4mfawydph7as9ls97cnzacvd0d4f4b7v")))

