(define-module (crates-io zi pw zipwhip-rs) #:use-module (crates-io))

(define-public crate-zipwhip-rs-0.1.0 (c (n "zipwhip-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jy6rkqz1n0g5y7b7d7nbs9br6i3dyxla7zxg4ngv3d4ylsk78yp")))

(define-public crate-zipwhip-rs-0.2.0 (c (n "zipwhip-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqsy2fghs2kaflh4kww9063i63976lxlxipq3xnkb5f6n62lpw7")))

(define-public crate-zipwhip-rs-0.2.1 (c (n "zipwhip-rs") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1llnghfgfgnfs9g6cwqnxir69v3a3b0g1pjizyjh63gwbnjscgn2")))

(define-public crate-zipwhip-rs-0.3.0 (c (n "zipwhip-rs") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1316avw9g2hrxbb76d192g3jj83b64pzjjihkbhi2348z9gh80zb")))

(define-public crate-zipwhip-rs-0.4.0 (c (n "zipwhip-rs") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0psijcg46prq3lx750f7m8xgbvp94ih0wsq8cgh90kf9y9zky8q5")))

(define-public crate-zipwhip-rs-0.4.1 (c (n "zipwhip-rs") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0iciksn8xp2bli10jb49gdgjwjfhzfhaqsj6dvc3f9k4p9m9vpzc")))

(define-public crate-zipwhip-rs-0.5.0 (c (n "zipwhip-rs") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0sqn2yb9lsffqq8xriaqy7llj6m3kvlr3b7bv3x7j9ghgfyifk7m")))

(define-public crate-zipwhip-rs-0.5.1 (c (n "zipwhip-rs") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1k5wn8ayrhwqx3vbxpq0z24cq3mv4bdbbgjq228cg901pznnwsw2")))

(define-public crate-zipwhip-rs-0.5.2 (c (n "zipwhip-rs") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "164cdc350iip6m8yc6bmsgx2p0zrcvyy9pgfrg6k7grhgfmqs253")))

(define-public crate-zipwhip-rs-0.5.3 (c (n "zipwhip-rs") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1l7dkl7hx9y0yh17d66ylg06cq0ndqmaf8dm9mkj9qg3ml2ykhva")))

(define-public crate-zipwhip-rs-0.5.4 (c (n "zipwhip-rs") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "07q0xyn8wxm6sfdrdarmqv6r4jikjn3inqdqg8qz28fpbj812gri")))

(define-public crate-zipwhip-rs-0.6.0 (c (n "zipwhip-rs") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1p8jc6iahlx0qjbb0jbqlxb0pwp08c7r24mysqp7va69fr8ka3dc")))

