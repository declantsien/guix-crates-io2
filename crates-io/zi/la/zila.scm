(define-module (crates-io zi la zila) #:use-module (crates-io))

(define-public crate-zila-0.1.2 (c (n "zila") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "1qsxrx15rcj08wrqqz8hjqm7pzfmbvssmh70yb83r683rix9v9mi") (f (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1.3 (c (n "zila") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0sv2ljc9882z96n9idzrjr7c9vfknvv6yah96ifrhgjpjhj62rvw") (f (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1.4 (c (n "zila") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0pkbhz3mm9igpq399hwgiq5nh5kvyv1c39jblagw5981g8k4wa6m") (f (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1.5 (c (n "zila") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "1iyadgrpqxy42v53va95svzxv9wid0kw1fdmdkg44ll28rf4n0n7") (f (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1.6 (c (n "zila") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "03jzm25c47plk4dfn398l0q1nd6qpg5jbyk3hylcr156d4mpy3gd") (f (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1.7 (c (n "zila") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0lx06daqdif25xkvprfd1hdjwiwksav7x1n64ldfi6glh8xdqzjy") (f (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1.8 (c (n "zila") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0bgkhqq40l3k9fxgnk1kxsfrmv0rg6ny2izhjqgl348lvkc3cji0") (f (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("full" "day" "hour" "minute" "second" "timeout" "interval") ("default") ("day"))))))

