(define-module (crates-io zi pt ziptree) #:use-module (crates-io))

(define-public crate-ziptree-0.1.0 (c (n "ziptree") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1nyvfbg7c4r4bhh25xkdq9i1igs9zfyakn4d4bdjjqmpdd1wxns4")))

(define-public crate-ziptree-0.1.1 (c (n "ziptree") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1yvynmmpbnd9q8j1rh2kyghgqvdcqxrj1y3rq4cbnf1039j24q4m")))

