(define-module (crates-io zi ts zits) #:use-module (crates-io))

(define-public crate-zits-1.0.0 (c (n "zits") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "16vdz56zq8pfz625kk5qij4yfqc0xb5nmp5vh4z0ghawwqbyjgpb") (r "1.65.0")))

(define-public crate-zits-1.0.1 (c (n "zits") (v "1.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0fk7q4mx7k031bpi487jvj2qhdsycpg8vxkfs35260gf26jkzyb1") (r "1.65.0")))

(define-public crate-zits-1.0.2 (c (n "zits") (v "1.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qw4m69bcy0ldl5kpgdgiz87wblarcd6f9c5mj85pwmf2psvlkab") (r "1.65.0")))

(define-public crate-zits-1.1.0 (c (n "zits") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1pq6xxzqlfaiicxwzz7lj7rfdqp1ag6khzih6whp409g30c7nqbg") (r "1.65.0")))

(define-public crate-zits-1.2.0 (c (n "zits") (v "1.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1dadsjg3karfnd21nn1acipcpkf9dbrk6djzcd8bgingxf4mlj2w") (r "1.65.0")))

(define-public crate-zits-1.2.1 (c (n "zits") (v "1.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "09q2438g6ak3kvvnjl8qhwqwzpksbnjfws15pzzn9dj0fkdqq770") (r "1.65.0")))

(define-public crate-zits-1.3.0 (c (n "zits") (v "1.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0r8akj7fkbmfnrmlzvhbdad93yxbc051f05xbm9gyf5fyrlvi555") (r "1.65.0")))

(define-public crate-zits-1.4.0 (c (n "zits") (v "1.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1rrrgi4a7zknffhmmfadplbzqbbf9djcwilj1py6ih05biqshi5p") (r "1.65.0")))

(define-public crate-zits-1.6.0 (c (n "zits") (v "1.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jz10ygpf85hhdrq4zwxyqdrcz4dhcpknw0agd3gd8vdpqwx9zdv") (r "1.65.0")))

(define-public crate-zits-1.6.1 (c (n "zits") (v "1.6.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0aq4s943zl5gpabpxbbdnafz7k8gb8b9nligl8hhrrxv30bp62lf") (r "1.65.0")))

(define-public crate-zits-1.6.2 (c (n "zits") (v "1.6.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1f2fs2f5ga93rcw68vjgzqmvvzj7rg139fn5ns5s27nry7059xkx") (r "1.65.0")))

(define-public crate-zits-1.7.0 (c (n "zits") (v "1.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "142q3s3s85cwhv0b9nvvhrmw8ynsiyzi08y9nynd7r7z855xxhjh") (r "1.65.0")))

(define-public crate-zits-1.8.0 (c (n "zits") (v "1.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0mg4pgakqlrrjimmz3c4255ad5kbr1h4gvj5962y1why821nxs74") (r "1.65.0")))

(define-public crate-zits-1.9.0 (c (n "zits") (v "1.9.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0p3bbbfsq0hragxdxiascrq7l6prhfdzzaaaslwhcsg9vv02mp22") (r "1.65.0")))

(define-public crate-zits-1.10.0 (c (n "zits") (v "1.10.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xf9fw9ijnmn4mmn5n08ndmc1fgzsbg33lnrjzbrnq0367b12qiz") (r "1.65.0")))

(define-public crate-zits-1.11.0 (c (n "zits") (v "1.11.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1fc2px7qy0hibxzg6ayf4pbaxf84yl25j4j6vbd1kbfg9jijm2r5") (r "1.65.0")))

(define-public crate-zits-1.12.0 (c (n "zits") (v "1.12.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0mich4h0a87ny558i8vvc4l6ibajmy04ay4d5yw8flk92ajmhjvv") (r "1.65.0")))

(define-public crate-zits-1.13.0 (c (n "zits") (v "1.13.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0i5991r0y17dkxnajp0zbhfihjrynv1505cwicjlgg622f5dy0wm") (r "1.65.0")))

(define-public crate-zits-1.14.0 (c (n "zits") (v "1.14.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1fcsgdkiw1ki5sdk0bk97vbinhkjxrf78z56z5zp1zljynw5scvy") (r "1.65.0")))

