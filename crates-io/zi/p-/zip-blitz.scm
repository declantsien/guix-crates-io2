(define-module (crates-io zi p- zip-blitz) #:use-module (crates-io))

(define-public crate-zip-blitz-0.1.0 (c (n "zip-blitz") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "0.6.*") (d #t) (k 0)))) (h "0lpbib6dvi5nl69halrhslfy0pqri041slph91rz5smmyyj55jwz")))

(define-public crate-zip-blitz-0.2.0 (c (n "zip-blitz") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "0.6.*") (d #t) (k 0)))) (h "0w2izzhqqpillr40ka6nwmxmp61arsmynx9ag38r7cwhx2ldp49b")))

(define-public crate-zip-blitz-0.2.1 (c (n "zip-blitz") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "0.6.*") (d #t) (k 0)))) (h "0rnwdgzqfb7n9k2l1p2q4xc5p5azbg6v6jb5ckz3a7dyczffi71q")))

