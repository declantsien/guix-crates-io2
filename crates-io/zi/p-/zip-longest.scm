(define-module (crates-io zi p- zip-longest) #:use-module (crates-io))

(define-public crate-zip-longest-0.0.1 (c (n "zip-longest") (v "0.0.1") (h "1rg4z0s657wjs5vqhchl4dajbn82f0qf1zvw2gyp09gw750ka9f3")))

(define-public crate-zip-longest-0.1.0 (c (n "zip-longest") (v "0.1.0") (h "15rfw8qm5iqj6chbsjaajdlyi6nb93vh6gcc3592s2vnvhwga11v")))

(define-public crate-zip-longest-0.1.1 (c (n "zip-longest") (v "0.1.1") (h "1n9warzdr4p3rybz5yd2aqfc5aya1zfrld86asjqf283lfgx4c3i")))

(define-public crate-zip-longest-0.1.2 (c (n "zip-longest") (v "0.1.2") (h "01d0nj0qcmc6lj213p39a78f04nsspg67y5ddfb8qxkf39wz73wy")))

(define-public crate-zip-longest-0.1.3 (c (n "zip-longest") (v "0.1.3") (h "0chxkl9jk6w8cg553fkg5ps7mh0whf5aicb7x1ravnwlknvcjz2k")))

(define-public crate-zip-longest-0.1.4 (c (n "zip-longest") (v "0.1.4") (h "1zp0jypvhab1w9q417a37w3x95qi1z4zyj3nhd259qgs5p4dq23r")))

(define-public crate-zip-longest-0.1.5 (c (n "zip-longest") (v "0.1.5") (h "0by1npx8wy413gbgksxc9nmw71y5wk6zpba7ldlll6n732caln9l")))

(define-public crate-zip-longest-0.1.6 (c (n "zip-longest") (v "0.1.6") (h "1kr6sl8j66hb5yvngs9g13pi7nbr6an1zzbnaxzwapgv4k520g8i")))

(define-public crate-zip-longest-0.1.7 (c (n "zip-longest") (v "0.1.7") (h "02pvap0l7d09xmbm0zx1czrpnaqjkd17w0amq01i2mkcyq4l44va")))

