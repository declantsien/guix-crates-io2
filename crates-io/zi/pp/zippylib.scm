(define-module (crates-io zi pp zippylib) #:use-module (crates-io))

(define-public crate-zippylib-0.1.0 (c (n "zippylib") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.4.4") (f (quote ("static"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (f (quote ("static"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("flate2" "deflate-zlib"))) (d #t) (k 0)))) (h "1zlc75gvhr0yjs7v3w9s0svc31qlx7xbh19jhdfk9ckq0szigb9r")))

