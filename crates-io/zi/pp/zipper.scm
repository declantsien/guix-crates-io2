(define-module (crates-io zi pp zipper) #:use-module (crates-io))

(define-public crate-zipper-0.0.1 (c (n "zipper") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "060bm0rsq6y7n8252b0hsv14yaiszca5v6czdil0644pa7s4k4cw")))

(define-public crate-zipper-0.0.2 (c (n "zipper") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1rlxr3wpczkapspslnnx1zzpn33qnjlyw1sa56ljvw31j1g3kh1i")))

