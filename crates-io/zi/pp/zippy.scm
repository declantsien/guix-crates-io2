(define-module (crates-io zi pp zippy) #:use-module (crates-io))

(define-public crate-zippy-0.1.3 (c (n "zippy") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ioe") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "zip") (r "^0.4") (d #t) (k 0)))) (h "0k58laxlzd9bf4fmshn1hb0pn75216y31zx03bzc18ylg0vhmkaw")))

(define-public crate-zippy-0.2.0 (c (n "zippy") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ioe") (r "^0.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0wcv2gzgpv5l8xb2il5i87lhcvfz1k6snfm0793vyry1yhv2xx2d")))

(define-public crate-zippy-0.2.1 (c (n "zippy") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ioe") (r "^0.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "16z1pi3ly3rh7a7vx9m0d2bpzjgkvx5la4k6ygg0dmls96kc7alc")))

