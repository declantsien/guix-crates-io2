(define-module (crates-io zi pp zipp) #:use-module (crates-io))

(define-public crate-zipp-0.1.0 (c (n "zipp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "zipp-sys") (r "^0.1") (d #t) (k 0)))) (h "1m1hzxkkp8kf89f863w7d4n1fhsbkxnif46kpn986np55i5y7d3s")))

(define-public crate-zipp-0.1.1 (c (n "zipp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "zipp-sys") (r "^0.1") (d #t) (k 0)))) (h "1qnfah8c9scyny8h3akkk6cfghwijy1ig558x9pc1wlll9clsysd")))

(define-public crate-zipp-0.1.2 (c (n "zipp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "zipp-sys") (r "^0.1") (d #t) (k 0)))) (h "0wqx7q480h02dgvg1mpj163zhmagr4gk4pzb0paslkdnax535fg6")))

(define-public crate-zipp-0.1.3 (c (n "zipp") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "zipp-sys") (r "^0.1") (d #t) (k 0)))) (h "1brx5j0sr8qfw0ix2makwbs3i1k1g4qfm18zl71dykyzlgx5n5w8")))

