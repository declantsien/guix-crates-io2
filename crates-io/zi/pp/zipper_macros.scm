(define-module (crates-io zi pp zipper_macros) #:use-module (crates-io))

(define-public crate-zipper_macros-1.0.0 (c (n "zipper_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0z2b3krrmwky4pinvka1l4pqm6q6hkvrm0pb0ai17x8bjgvwcb1c")))

