(define-module (crates-io zi gg ziggy-honggfuzz) #:use-module (crates-io))

(define-public crate-ziggy-honggfuzz-0.5.55 (c (n "ziggy-honggfuzz") (v "0.5.55") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1q023gx88cf6sysarf6rj3s18ja8fs1q7zai7pvq92wja0n7023d") (f (quote (("default" "arbitrary"))))))

