(define-module (crates-io zi p3 zip32) #:use-module (crates-io))

(define-public crate-zip32-0.0.0 (c (n "zip32") (v "0.0.0") (h "0qsh971096r91kga2wdn07w965zpm77y9rqn1z4bg4q1da25lssi")))

(define-public crate-zip32-0.1.0 (c (n "zip32") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "blake2b_simd") (r "^1") (d #t) (k 0)) (d (n "memuse") (r "^0.2.1") (d #t) (k 0)) (d (n "subtle") (r "^2.2.3") (d #t) (k 0)))) (h "08kbbs454cb1algyjpfap2jv9r129sc45r8p6rzhpdfzwhxsc96p") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-zip32-0.1.1 (c (n "zip32") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "blake2b_simd") (r "^1") (d #t) (k 0)) (d (n "memuse") (r "^0.2.1") (d #t) (k 0)) (d (n "subtle") (r "^2.2.3") (d #t) (k 0)))) (h "17p1kphdkyic5rd1wkipwdyr2718gffyrzjd0qkpqh69x6pd09j2") (f (quote (("std") ("default" "std")))) (r "1.60")))

