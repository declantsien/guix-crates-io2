(define-module (crates-io zi p2 zip2arx) #:use-module (crates-io))

(define-public crate-zip2arx-0.2.0 (c (n "zip2arx") (v "0.2.0") (d (list (d (n "arx") (r "^0.2.0") (f (quote ("cmd_utils"))) (d #t) (k 0) (p "libarx")) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "jbk") (r "^0.2.0") (d #t) (k 0) (p "jubako")) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1hizcc1xhikjry56r2v6x4zwq5wgvrvnv81f7njpciivp62jijlj")))

(define-public crate-zip2arx-0.2.1 (c (n "zip2arx") (v "0.2.1") (d (list (d (n "arx") (r "^0.2.1") (f (quote ("cmd_utils" "zstd"))) (d #t) (k 0) (p "libarx")) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "human-panic") (r "^1.2.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "jbk") (r "^0.2.1") (d #t) (k 0) (p "jubako")) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1vnvh18rchyg6p6fqnq2yykkhpb75ywcdpwpbyw53p5w2c21704h")))

