(define-module (crates-io zi nf zinfo) #:use-module (crates-io))

(define-public crate-zinfo-0.1.0 (c (n "zinfo") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "1bi8r04833bdl9bfbgdz04ab30b328l73ph6lj431r29j06rniiw")))

(define-public crate-zinfo-0.1.1 (c (n "zinfo") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "1qmhhx0ygffhzc01y3ai1lbfh06j9jdrdjlx86zvgyx9qdbhd8ws")))

(define-public crate-zinfo-0.1.2 (c (n "zinfo") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "1c1bmjf79ifr9300ajjmj5mi0dxg2xfdv8d5xyi6x48wffbj20jm")))

(define-public crate-zinfo-0.1.3 (c (n "zinfo") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "0sjz4hn8wmchqrhn2apkiqflh2s1rl5w06nf7f158m5yb6cbl429")))

(define-public crate-zinfo-0.1.4 (c (n "zinfo") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "17cmyl8flj078l52dr47lvild874dv00sfmbyc2nzzi49b2rhkik")))

(define-public crate-zinfo-0.1.5 (c (n "zinfo") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "1jkcvik6cxlxdvzc6xv8qi310whq3yqrgajz8dbxjgr2h2ah72dz")))

(define-public crate-zinfo-0.2.0 (c (n "zinfo") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "09f403dj0f0gzskdzy095a91vszbrhg8awp2dpp44gvv65g3psj8")))

(define-public crate-zinfo-0.2.1 (c (n "zinfo") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "02dsx6j1wjrfx1s88nqpn3yygmgkm7409qkjp6vwm19lchqnfagf")))

(define-public crate-zinfo-0.2.2 (c (n "zinfo") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "1vzm7ccl85bs6l0mgpdq9dlhrb42cpiq8qyzwg2f24nggk2msqrw")))

(define-public crate-zinfo-0.2.3 (c (n "zinfo") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "0xbgpk4xpqawlac4lz5gi3ai3vsr51ir7bfq4g2qgnr66r2mdfkw")))

(define-public crate-zinfo-0.2.4 (c (n "zinfo") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)))) (h "10cgn7vgsl04yr3xdq2ji88kr5gv251zshxfzkfxgcvcg87lzxlj")))

(define-public crate-zinfo-0.2.5 (c (n "zinfo") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "0p20chjs10p081nk6lycj3xamihqkqararjr983csjfnx7dr3zmy")))

(define-public crate-zinfo-0.3.0 (c (n "zinfo") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_SystemInformation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1adncznmkj6sfrpvax99z21vynw8ixvq4m0cbhy6n2r8l6hg6kih")))

(define-public crate-zinfo-0.3.1 (c (n "zinfo") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1q3385psaashdcvxp66phx3jsikfi8q8q2cv5bvvv0pcll2k85s9")))

