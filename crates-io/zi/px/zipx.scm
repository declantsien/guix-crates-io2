(define-module (crates-io zi px zipx) #:use-module (crates-io))

(define-public crate-zipx-0.1.1 (c (n "zipx") (v "0.1.1") (h "00d2ghfnm0h91w09np0pvil6i5ak7lapiyzd5z2m86nvj25c6hdw") (y #t)))

(define-public crate-zipx-0.1.2 (c (n "zipx") (v "0.1.2") (h "0n5ff9akyjh6h8zm2sbj4gm2axri1c4sw1s63yk9dvwgcqzhj6f7") (y #t)))

