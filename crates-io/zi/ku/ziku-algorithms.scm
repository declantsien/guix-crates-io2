(define-module (crates-io zi ku ziku-algorithms) #:use-module (crates-io))

(define-public crate-ziku-algorithms-0.1.0 (c (n "ziku-algorithms") (v "0.1.0") (d (list (d (n "crdts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pds") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0jljzkjrp029kixm474qz2w905rqp3py3kg9gqijxr2simlijhpn") (f (quote (("default" "crdts")))) (y #t) (s 2) (e (quote (("pds" "dep:pds") ("crdts" "dep:crdts"))))))

(define-public crate-ziku-algorithms-0.1.1 (c (n "ziku-algorithms") (v "0.1.1") (d (list (d (n "ziku-crdts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ziku-pds") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1asv4azyx59i7m0wwsblj3xzxnr7jx5j7cis1ah23hjkw431vgc1") (f (quote (("default" "ziku-crdts" "ziku-pds")))) (s 2) (e (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

(define-public crate-ziku-algorithms-0.1.2 (c (n "ziku-algorithms") (v "0.1.2") (d (list (d (n "ziku-crdts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ziku-pds") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0rg13r43qnjyfb46lfln8w6dxahsxmzjfdlyvhpl751lvszl2wfh") (f (quote (("default" "ziku-crdts" "ziku-pds")))) (s 2) (e (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

(define-public crate-ziku-algorithms-0.1.3 (c (n "ziku-algorithms") (v "0.1.3") (d (list (d (n "ziku-crdts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ziku-pds") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11i14f5lvs7bs36pkg8cd0j7mhnjc23sp0q8xi8y17a952safra3") (f (quote (("default" "ziku-crdts" "ziku-pds")))) (s 2) (e (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

