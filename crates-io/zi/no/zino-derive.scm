(define-module (crates-io zi no zino-derive) #:use-module (crates-io))

(define-public crate-zino-derive-0.1.1 (c (n "zino-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino") (r "^0.1.1") (d #t) (k 0)))) (h "1ycycf208kraaq1h9aar6v3rccipzvbgrd0mc7g6fb4gnsfq81fz")))

(define-public crate-zino-derive-0.1.2 (c (n "zino-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino") (r "^0.1.2") (d #t) (k 0)))) (h "0qhd3j77m4dgnr5p0ax7sl1z9n2qjl1m2y65qxmg5sw49q4iwr8m")))

(define-public crate-zino-derive-0.2.0 (c (n "zino-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.2.0") (d #t) (k 0)))) (h "119ycjpcrv1c3i8lrm8fsywgvbrkq06sq1yxysxcv9j5rhc4mfri")))

(define-public crate-zino-derive-0.2.1 (c (n "zino-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.2.1") (d #t) (k 0)))) (h "02ma3373m3v6yk6pdrsxlgq4jlr7qrpxhmc5kxxlnbs1vgv2m87f") (r "1.68")))

(define-public crate-zino-derive-0.2.2 (c (n "zino-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.2.2") (d #t) (k 0)))) (h "0s33q92shkgzh7xac13qccb716mdk40qplgfidxh9x3aws18skas") (r "1.68")))

(define-public crate-zino-derive-0.2.3 (c (n "zino-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.2.3") (d #t) (k 0)))) (h "03yn3xl9yp6g6f7h20ladh5rfb6bnkak477f0d0bmp54jbb0kajb") (r "1.68")))

(define-public crate-zino-derive-0.3.0 (c (n "zino-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.3.0") (d #t) (k 0)))) (h "141x7gjdapw8ivibrqz1m4m56xw8hd9bj6m6gs4f5yra7x7p77vv") (r "1.68")))

(define-public crate-zino-derive-0.3.1 (c (n "zino-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.3.1") (d #t) (k 0)))) (h "16n8xlw29lzrfx75a2lb5pg1kcm8xbpka6qyqazhig57v7yzjcz0") (r "1.68")))

(define-public crate-zino-derive-0.3.2 (c (n "zino-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.4.0") (d #t) (k 0)))) (h "1i68i15d1ifymsymzmyvc45854j36d9mcgijp4yc9r2h6br9fkn4") (r "1.68")))

(define-public crate-zino-derive-0.3.3 (c (n "zino-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.4.1") (d #t) (k 0)))) (h "0500zi2qvqkjiddr7zqfn7mwx7m759jsh1kvilixlppswicsvzzx") (r "1.68")))

(define-public crate-zino-derive-0.3.4 (c (n "zino-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.4.2") (d #t) (k 0)))) (h "1z7jsqidpj1g08h21k5h19dibxlnakv2nf9qn7ffrgh57415dg12") (r "1.68")))

(define-public crate-zino-derive-0.3.5 (c (n "zino-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.4.3") (d #t) (k 0)))) (h "0q9f9w8n9d510v5jzjwgs5gzs1qkljz0dsj8x5pim487szsriwq0") (r "1.68")))

(define-public crate-zino-derive-0.3.6 (c (n "zino-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.4.4") (d #t) (k 0)))) (h "067w1dlfbc799bc7bifwirs7qsrp6l72l9sdrvkvp6zpxzh6r35z") (r "1.68")))

(define-public crate-zino-derive-0.3.7 (c (n "zino-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.5.0") (d #t) (k 0)))) (h "0hvrcq40kg2fyp2hmhfh53ymfyl203z7hsd9ifnkiml24b3qjnwa") (r "1.68")))

(define-public crate-zino-derive-0.3.8 (c (n "zino-derive") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.5.1") (d #t) (k 0)))) (h "1xa3b3qyv87fr1ag5w0hzlg0hqkx9w2bq35w6i31d3mchj7c9qsx") (r "1.68")))

(define-public crate-zino-derive-0.3.9 (c (n "zino-derive") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.5.2") (d #t) (k 0)))) (h "0gz6k4chd4yi5y1rdkrx79fvvcafbwb9vb49w8kny6m9jhzq7ma5") (r "1.68")))

(define-public crate-zino-derive-0.3.10 (c (n "zino-derive") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.5.3") (d #t) (k 0)))) (h "001n73kh0360zghvbjqs5mb0mir098rx1p1rkshmw0ai025i9lp7") (r "1.68")))

(define-public crate-zino-derive-0.4.0 (c (n "zino-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zino-core") (r "^0.6.0") (d #t) (k 0)))) (h "1hm6frq9c4s7cb31k7xazfsmda05j0gy504p2gz4zw56aj3mlf9n") (r "1.68")))

(define-public crate-zino-derive-0.4.1 (c (n "zino-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "zino-core") (r "^0.6.1") (d #t) (k 0)))) (h "146vj9l6gzk0w9rnapk7lmxfms7x3xq7bwwr7dq5x2nchk3y33fy") (r "1.68")))

(define-public crate-zino-derive-0.4.2 (c (n "zino-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "zino-core") (r "^0.6.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1265qp0i40rxclk34qv7mlanbh2c6wlkz3kmqi6m5am4731qgx7q") (r "1.68")))

(define-public crate-zino-derive-0.4.3 (c (n "zino-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "zino-core") (r "^0.6.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "1mayma0znx0viwh9ydi81i4h4i6h624lhwki969d4ansjqglck7y") (r "1.68")))

(define-public crate-zino-derive-0.4.4 (c (n "zino-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "zino-core") (r "^0.7.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "1wlxr8grxyj7bmp25ngcl6q366c4in3ciqbsghcp2i2k3vxmrmq4") (r "1.68")))

(define-public crate-zino-derive-0.4.5 (c (n "zino-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.44") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "zino-core") (r "^0.7.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "0fz2s9hrrf6l2xbjsfh92syphic6787yk412rdjilsjrnr7x9qik") (r "1.70")))

(define-public crate-zino-derive-0.5.0 (c (n "zino-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)) (d (n "zino-core") (r "^0.7.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1sjcr764h0403c93q44084sa7vvc38hb2r8j6m73vywifvidmyfd") (r "1.70")))

(define-public crate-zino-derive-0.5.1 (c (n "zino-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)) (d (n "zino-core") (r "^0.7.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "060a4r87jyli8x8kvb746ps7p7y13i5jc15z9vfiv679kp5jzkip") (r "1.70")))

(define-public crate-zino-derive-0.5.2 (c (n "zino-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)) (d (n "zino-core") (r "^0.8.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "1ri5swd2zfd3kll9akk074srf435z32x1j9ly49m7z9643is68xq") (r "1.70")))

(define-public crate-zino-derive-0.5.3 (c (n "zino-derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "zino-core") (r "^0.8.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1pgbz7rn13014ajjk5n9n3d8cylwyn5m3ngc010039dkcmxqi1x5") (r "1.70")))

(define-public crate-zino-derive-0.5.4 (c (n "zino-derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "zino-core") (r "^0.8.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "0qxj8rx90zmficzd8lgrz3zzn7kc17axabwpy6v25akcjm6yfsm8") (r "1.70")))

(define-public crate-zino-derive-0.5.5 (c (n "zino-derive") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "13i6wl09c0i54wjplzjqgggw9bakfh1rsc5znw1zd6bb3b3yyy91") (r "1.70")))

(define-public crate-zino-derive-0.5.6 (c (n "zino-derive") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "0424bjjzxmhh4g2l1cj0zvgnix4asc154i1wlirf2sg5452i6pcp") (r "1.70")))

(define-public crate-zino-derive-0.6.0 (c (n "zino-derive") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "10v8rkis6395rbl6y3n48iq5x8p7mcamvda7m1f52w15xd1z89h7") (r "1.70")))

(define-public crate-zino-derive-0.6.1 (c (n "zino-derive") (v "0.6.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0pp9k8zbb9iyjzb6cbaypz220fglshwp81ddjmfcmp6bnl30ia40") (r "1.70")))

(define-public crate-zino-derive-0.6.2 (c (n "zino-derive") (v "0.6.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "1699j4fbwbfq4viqkjwbc9931qwz9vfl76212h23rflp87jl2qvv") (r "1.70")))

(define-public crate-zino-derive-0.6.3 (c (n "zino-derive") (v "0.6.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.9.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "1x0lslg6b3iqdwdm4dsfnz3c7w6r5sc0zxqamqsijrqks8lml50k") (r "1.70")))

(define-public crate-zino-derive-0.7.0 (c (n "zino-derive") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0asalmk39p2w9dys5689ab5niyv1flv7af3xbdh5fdliwsqv7yc2") (r "1.70")))

(define-public crate-zino-derive-0.7.1 (c (n "zino-derive") (v "0.7.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "08akhy660ifyznsyb165d59lcd2x9kvkr764y7nvcc90x9qc4bdd") (r "1.70")))

(define-public crate-zino-derive-0.7.2 (c (n "zino-derive") (v "0.7.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1qxjs3nhwnqwrw60dk3c3zr4qhbz3rsfc723zxyawyq1a050k644") (r "1.70")))

(define-public crate-zino-derive-0.7.3 (c (n "zino-derive") (v "0.7.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "0fm8ac3czvsq7jrnp3vrk1x1mc6cq0qg0ldsxw171dxlijycamwp") (r "1.70")))

(define-public crate-zino-derive-0.7.4 (c (n "zino-derive") (v "0.7.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "08cyk0fyxr2qqq61sqixwan7rhm8bvrsyy9vvrrz74l956vz8pdy") (r "1.70")))

(define-public crate-zino-derive-0.7.5 (c (n "zino-derive") (v "0.7.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "0skpa77i85975acp92nm2c67i37isc5wllc5wipmq0h49130vqhz") (r "1.70")))

(define-public crate-zino-derive-0.7.6 (c (n "zino-derive") (v "0.7.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "0wcdxqb2y524lrjp97har2nlrm4abdq7xiv4y7i8022nvz2xp722") (r "1.72")))

(define-public crate-zino-derive-0.7.7 (c (n "zino-derive") (v "0.7.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)) (d (n "zino-core") (r "^0.10.6") (f (quote ("orm"))) (d #t) (k 0)))) (h "0cnk1hv5k13ig5lwdlyj4mb9x1p4zv4fkcj2bhnz9bclgjjmyy04") (r "1.72")))

(define-public crate-zino-derive-0.8.0 (c (n "zino-derive") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "1nc90pahxi4bb4hm9yajxkjq8c19p1rxbaxpy8f6x8k5cmix3bfg") (r "1.72")))

(define-public crate-zino-derive-0.8.1 (c (n "zino-derive") (v "0.8.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1wggvqz34k72mpclwa91r3xr7li687ha72cjg68gyf58x2m756z5") (r "1.72")))

(define-public crate-zino-derive-0.8.2 (c (n "zino-derive") (v "0.8.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "0z75i3ryd9vjycximla3b6kmn5lxr412phnr0wk748bllshr62ag") (r "1.72")))

(define-public crate-zino-derive-0.8.3 (c (n "zino-derive") (v "0.8.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "05qzxdmbhiccmd7s8aqivkbwmf6wds3b7bhmf81x0bqsl6r5rv63") (r "1.72")))

(define-public crate-zino-derive-0.8.4 (c (n "zino-derive") (v "0.8.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "1yc2nr9g186ixr5d43d2nsygkari3nzz5rim47xnhpwjxpf1303y") (r "1.72")))

(define-public crate-zino-derive-0.8.5 (c (n "zino-derive") (v "0.8.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "zino-core") (r "^0.11.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "1yds4r4ymb9rzi4rk9a6n0ir6yj9fak9rs8hz5p85vqaxxmbdv9b") (r "1.72")))

(define-public crate-zino-derive-0.9.0 (c (n "zino-derive") (v "0.9.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)) (d (n "zino-core") (r "^0.12.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0lxhc9j1f6c00cg6bc5caavn4n0bci3vr9djwnfhnsm256s4jzbd") (r "1.72")))

(define-public crate-zino-derive-0.9.1 (c (n "zino-derive") (v "0.9.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "zino-core") (r "^0.12.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "04vx0n82iy3kqxjc0g2mfpxj34bynnlah8d9cck10rrplgfnc0xa") (r "1.72")))

(define-public crate-zino-derive-0.9.2 (c (n "zino-derive") (v "0.9.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "zino-core") (r "^0.12.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "10lpmq5sc6fm17grcl23l6apy7banfhl604mnnccajmv4kkm5zb2") (r "1.72")))

(define-public crate-zino-derive-0.9.3 (c (n "zino-derive") (v "0.9.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "zino-core") (r "^0.12.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0cg3k54vwk1ch9bpr9rf925jjifjxxihbazvq2mklgg5irim2zky") (r "1.72")))

(define-public crate-zino-derive-0.9.4 (c (n "zino-derive") (v "0.9.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)) (d (n "zino-core") (r "^0.12.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "0li7xhzrn66g2cf5xa6ii618ffkqyziif1c0g84hy3cq8hk0j4hm") (r "1.72")))

(define-public crate-zino-derive-0.10.0 (c (n "zino-derive") (v "0.10.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)) (d (n "zino-core") (r "^0.13.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "1l7a9dp6hp9yh2f39rg9ysglhgy6fyh774a15y6sjh1mpjm0yfyk") (r "1.72")))

(define-public crate-zino-derive-0.10.1 (c (n "zino-derive") (v "0.10.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "zino-core") (r "^0.13.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "15qmzzakpj5hqjkapmk00cf0yra32753hd5ywrza1j1hddcf5zs4") (r "1.72")))

(define-public crate-zino-derive-0.10.2 (c (n "zino-derive") (v "0.10.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "zino-core") (r "^0.13.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1c0mhhzdg9gjmvj1k046rmidzqvdc94sgyk2pvn8c1kq5bkbrgf5") (r "1.72")))

(define-public crate-zino-derive-0.10.3 (c (n "zino-derive") (v "0.10.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "zino-core") (r "^0.13.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "1bkq6al18m0qw0z5vmm5jnswrzlv1bdz2dx1f39ca7fyfymybrx4") (r "1.72")))

(define-public crate-zino-derive-0.11.0 (c (n "zino-derive") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0686jarx8bm84gwayydv413l1gaijy4ipvpiva2srgrpz267zpyk") (r "1.72")))

(define-public crate-zino-derive-0.11.1 (c (n "zino-derive") (v "0.11.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1wjb57mf4wkr8yqgqp3sk3j7ymwbcq3dnzdghdaqf083wl4yc8lf") (r "1.72")))

(define-public crate-zino-derive-0.11.2 (c (n "zino-derive") (v "0.11.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "02arz0ry8dhvwp4rh7shbzmm5sj5szgdarjzqlx96lr1pjrw3xm0") (r "1.72")))

(define-public crate-zino-derive-0.11.3 (c (n "zino-derive") (v "0.11.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "1b1zj3f4y39kh7imc5n6nkjy2wh9987xahczf8g6148jzgj7378j") (r "1.73")))

(define-public crate-zino-derive-0.11.4 (c (n "zino-derive") (v "0.11.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "1yx73mcdzdn48gcqxb61m625vk5mjkhknb8z15rdzja6vjkybkmz") (r "1.73")))

(define-public crate-zino-derive-0.11.5 (c (n "zino-derive") (v "0.11.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.14.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "0lx61pxf0lg2c6yi8p0gc4c1m7l304xd3gis3as0nl0xskzy71cx") (r "1.73")))

(define-public crate-zino-derive-0.12.0 (c (n "zino-derive") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "08syz8iwqwj8c8v8gxdf1mn2bhrcxwcbx94fvs5w4x5zmpbs0ik7") (r "1.73")))

(define-public crate-zino-derive-0.12.1 (c (n "zino-derive") (v "0.12.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1mgdvjhfmd5c6a97wfpxs823c02nir0h861vnjmw8931akm582nd") (r "1.73")))

(define-public crate-zino-derive-0.12.2 (c (n "zino-derive") (v "0.12.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "0q9ylyinm48df0k7hb7y0g5akzdwkrm2ncaiv4sksqkmrzzm4n4x") (r "1.73")))

(define-public crate-zino-derive-0.12.3 (c (n "zino-derive") (v "0.12.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "180bkch2yf3j76caqndgsaqrpadxb185jq5lwzxrpxfdh90w9xhm") (r "1.73")))

(define-public crate-zino-derive-0.12.4 (c (n "zino-derive") (v "0.12.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "0spi8h35w64isa2v0af0p0r15d8vc6fbdg6xrmdbkjmqw9qj7wiq") (r "1.73")))

(define-public crate-zino-derive-0.13.0 (c (n "zino-derive") (v "0.13.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0qxcy5pgd6lql36xj7znviz9jyyh0w2d3hbhsvk0npqnj8n0qsli") (r "1.73")))

(define-public crate-zino-derive-0.13.1 (c (n "zino-derive") (v "0.13.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "0vqbsc0z61idzzwj210c5ksckvs7by026r7lxq0j9skpqydiz622") (r "1.73")))

(define-public crate-zino-derive-0.13.2 (c (n "zino-derive") (v "0.13.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "0nys79vb6xxvfzd0rnazxrw3a7rn14642bv85knpwx2amm1hp7yh") (r "1.73")))

(define-public crate-zino-derive-0.13.3 (c (n "zino-derive") (v "0.13.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "1jii035zp8sxbwn8j0vciqrcdbzm920dm1qpgg5ja18m12rnv0bn") (r "1.73")))

(define-public crate-zino-derive-0.13.4 (c (n "zino-derive") (v "0.13.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "16vvnigsxf2ckxmcbb73173szd05sw0bsqwa2jqksq401l28j61f") (r "1.73")))

(define-public crate-zino-derive-0.13.5 (c (n "zino-derive") (v "0.13.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "0zmg4grx24r0r5c4yija6k07pah2nrm6bg58y3wi2qqvviiwlhbg") (r "1.73")))

(define-public crate-zino-derive-0.14.0 (c (n "zino-derive") (v "0.14.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "08zwf4xd956wni41ycm187ryk4jsr7ss2lw8s02jj0l79w6nkglh") (r "1.73")))

(define-public crate-zino-derive-0.14.1 (c (n "zino-derive") (v "0.14.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1arbmys51whdvd6n3kfqy09nnpk2hqn34wvxdcmkjlsskmxky489") (r "1.73")))

(define-public crate-zino-derive-0.14.2 (c (n "zino-derive") (v "0.14.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "02cvas47fi2v7z3rjndava3clcnzm78agnk3w0720q45fs23lg3s") (r "1.73")))

(define-public crate-zino-derive-0.14.3 (c (n "zino-derive") (v "0.14.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0c0hxyxhy3amvi0768yj5c3hfjc0gy5l03rd9s0zmhmm57y93ijm") (r "1.73")))

(define-public crate-zino-derive-0.14.4 (c (n "zino-derive") (v "0.14.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "0j05n64zcjpkj92afxxs0w5wpbw3rfs6fnia54vzqklkqxfd6w15") (r "1.73")))

(define-public crate-zino-derive-0.15.0 (c (n "zino-derive") (v "0.15.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "19hpsdm2mzhzdrpsv52skadmrxgf5ljm16gaqgmx6j7z8fh9g0g0") (r "1.75")))

(define-public crate-zino-derive-0.15.1 (c (n "zino-derive") (v "0.15.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "0l4fn0mh7aljaky4jmg75m5x4074walr8f6zhpk2d043p553n07p") (r "1.75")))

(define-public crate-zino-derive-0.15.2 (c (n "zino-derive") (v "0.15.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "025f413acczdlzvjjf1361p7zrgv6grpbr8k6x6g7il7qja4iv32") (r "1.75")))

(define-public crate-zino-derive-0.15.3 (c (n "zino-derive") (v "0.15.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0y5z3zr1kizwxaz0d07yqqhyl90jqdaqxcc832v9wgrynykmvd31") (r "1.75")))

(define-public crate-zino-derive-0.15.4 (c (n "zino-derive") (v "0.15.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "1qgifpmr69lrb825k08p8z6jd0zxzkpxxsrdlkfhlxijvznjc3iw") (r "1.75")))

(define-public crate-zino-derive-0.16.0 (c (n "zino-derive") (v "0.16.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "04rrh8gdh3dj2v0cxbp8i024z4irjskacx8qcvy9vkmx74m4r2bc") (r "1.75")))

(define-public crate-zino-derive-0.16.1 (c (n "zino-derive") (v "0.16.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1fq00zpgd152rk680sv8i3c27xrlk7342njznmnkpqbvcjfjg5sk") (r "1.75")))

(define-public crate-zino-derive-0.16.2 (c (n "zino-derive") (v "0.16.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1srd3kr6zjc1m4n80wwmanbnnpxfgklhsvjq3nd9gvbhnhz2nmim") (r "1.75")))

(define-public crate-zino-derive-0.16.3 (c (n "zino-derive") (v "0.16.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0ds57sqx9b6za1pq42agpns05y1v2ldc5l7lhnxs6chkz3fjfrfg") (r "1.75")))

(define-public crate-zino-derive-0.16.4 (c (n "zino-derive") (v "0.16.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0lgss8kc22zgwfq01p0jfn60gi3hm8cpiyw957lwxbw07gzgax2k") (r "1.75")))

(define-public crate-zino-derive-0.16.5 (c (n "zino-derive") (v "0.16.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "14ghm1d67rs7mkp3x0b7wjcg3gb98b6n7nmmsz4lcx2wkqk3l1mf") (r "1.75")))

(define-public crate-zino-derive-0.16.6 (c (n "zino-derive") (v "0.16.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.6") (f (quote ("orm"))) (d #t) (k 0)))) (h "0iz5db3s70da61jja95hc9zr18zpy2aflqx8w289jggk8bgll3q7") (r "1.75")))

(define-public crate-zino-derive-0.17.0 (c (n "zino-derive") (v "0.17.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "zino-core") (r "^0.20.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0vds8kblrfbyiphghkl9kjq3x9ciw106mnksm86ljzsm7lpspcxx") (r "1.75")))

(define-public crate-zino-derive-0.17.1 (c (n "zino-derive") (v "0.17.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "zino-core") (r "^0.20.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1p2l1xpqxkkhbdbyfxynhyjy5kp3s0i3jpfg2brx32kiz6si8qfk") (r "1.75")))

(define-public crate-zino-derive-0.17.2 (c (n "zino-derive") (v "0.17.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "zino-core") (r "^0.20.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1yp4zqy0y7l3lfhng3lf9j0vabf73xqc225l5z5glvqf9wk1bb1b") (r "1.75")))

(define-public crate-zino-derive-0.17.3 (c (n "zino-derive") (v "0.17.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "zino-core") (r "^0.20.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "0wblv47k6ydfb7zx3iny9qw36rni6aicrig6qmdbiwyc1jf5490y") (r "1.75")))

(define-public crate-zino-derive-0.18.0 (c (n "zino-derive") (v "0.18.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "141dlqkrhml9i537blmh5kqjsqqg0hcjkc5pc44flvhrkb36xspj") (r "1.75")))

(define-public crate-zino-derive-0.18.1 (c (n "zino-derive") (v "0.18.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "0kbwpd4xld26rhfc4pf96gpxyaqbb9xxsy6jcjvip5kgsk0q8lx9") (r "1.75")))

(define-public crate-zino-derive-0.18.2 (c (n "zino-derive") (v "0.18.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1p053kbl1cvbg6pmms0qvxd6w0vjgywbbjynmcpgvk63a996xb8f") (r "1.75")))

(define-public crate-zino-derive-0.18.3 (c (n "zino-derive") (v "0.18.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "18i3jjsajym9gxqiddpfsg3r9bz9qyg8j6i9pd2yj3d5g639ifs5") (r "1.75")))

(define-public crate-zino-derive-0.19.0 (c (n "zino-derive") (v "0.19.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "0rl27minzwcq17xh6q6q04v6gfmwnidjiw38lpqjcsw5d8qhny48") (r "1.75")))

(define-public crate-zino-derive-0.19.1 (c (n "zino-derive") (v "0.19.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "1xlqrypnmc4fsmmb8hcd5pz458rgjwbyh2x2h51vjg5zrfmz4ckp") (r "1.75")))

(define-public crate-zino-derive-0.19.2 (c (n "zino-derive") (v "0.19.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "03j0577pm2g3xhzd925qkxvrwrnzynmazs65095ngkwlbcfigib7") (r "1.75")))

(define-public crate-zino-derive-0.19.3 (c (n "zino-derive") (v "0.19.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.3") (f (quote ("orm"))) (d #t) (k 0)))) (h "1airr7k3b27wb164397w5v8c77qcfwalrwacyj725ibrwvnpb4xp") (r "1.75")))

(define-public crate-zino-derive-0.19.4 (c (n "zino-derive") (v "0.19.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.4") (f (quote ("orm"))) (d #t) (k 0)))) (h "0y1065g2808vhv9flxsv18lvx4ml2bgl4gxz6jm4cw74hdbll2cq") (r "1.75")))

(define-public crate-zino-derive-0.19.5 (c (n "zino-derive") (v "0.19.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.5") (f (quote ("orm"))) (d #t) (k 0)))) (h "0qqa6i72fh00cx55q5gi8p3walpwadichm3r44xa2n95bfk1w2qn") (r "1.75")))

(define-public crate-zino-derive-0.20.0 (c (n "zino-derive") (v "0.20.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)) (d (n "zino-core") (r "^0.23.0") (f (quote ("orm"))) (d #t) (k 0)))) (h "1qgbn28llnadsqiqb618ffh2wl9qhr1k95mcwr6a722dglbh1lml") (r "1.75")))

(define-public crate-zino-derive-0.20.1 (c (n "zino-derive") (v "0.20.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)) (d (n "zino-core") (r "^0.23.1") (f (quote ("orm"))) (d #t) (k 0)))) (h "16wi7sm9a9wpvpfl3cbqw5q886snxdhagg3k0ir2cspmk2gwi413") (r "1.75")))

(define-public crate-zino-derive-0.20.2 (c (n "zino-derive") (v "0.20.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "zino-core") (r "^0.23.2") (f (quote ("orm"))) (d #t) (k 0)))) (h "1qzi36m5bxgr94g3ivjzzsjxq0a2ga55bhby55nr8diz7d1d24lr") (r "1.75")))

