(define-module (crates-io zi no zino-cli) #:use-module (crates-io))

(define-public crate-zino-cli-0.0.1 (c (n "zino-cli") (v "0.0.1") (h "19dwww7s8glq74vrjskxgb7hlf0krafxh5q6y6975naclj16iixf")))

(define-public crate-zino-cli-0.0.2 (c (n "zino-cli") (v "0.0.2") (h "1085f3vhmpkkagrqp3q64kxlrgnpl0ph5gxcfiacdqsf6isyl77j")))

(define-public crate-zino-cli-0.1.0 (c (n "zino-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.0") (d #t) (k 0)))) (h "1m537mlxw9lhmxj7jaqqsg0fwfqnhvhai8zyrmx8aiaqipfdkgq1") (r "1.73")))

(define-public crate-zino-cli-0.1.1 (c (n "zino-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.1") (d #t) (k 0)))) (h "1p261r5d4ns57d148x662spn0y8dy9cpq4fmacwm8vmh50xa6cwi") (r "1.73")))

(define-public crate-zino-cli-0.1.2 (c (n "zino-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.15.3") (d #t) (k 0)))) (h "08hk88n5291l8x3nwvisv3walms1dvl5w59kqwcac0hbbbca0ap8") (r "1.73")))

(define-public crate-zino-cli-0.1.3 (c (n "zino-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.16.0") (d #t) (k 0)))) (h "1z1fa6nca72f342gdmwhfm8c92hk5ndxh62s6zv583bblpazgpjp") (r "1.73")))

(define-public crate-zino-cli-0.1.4 (c (n "zino-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.17.0") (d #t) (k 0)))) (h "0phpwpxi4h3rry4accas3slbif4nlkpl03w94zc2bb0y6jz47mmh") (r "1.73")))

(define-public crate-zino-cli-0.2.0 (c (n "zino-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.0") (d #t) (k 0)))) (h "1i6z0z40sxh5p1p85m2c7szzy5fzs6j0h4xkklkhjrc0d03lnm73") (r "1.75")))

(define-public crate-zino-cli-0.2.1 (c (n "zino-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.18.1") (d #t) (k 0)))) (h "1mc2zr1xckrh3d5xfnply2s12chywi6qa0lni7k6bri2ich07pdp") (r "1.75")))

(define-public crate-zino-cli-0.2.2 (c (n "zino-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "zino-core") (r "^0.19.0") (d #t) (k 0)))) (h "1nv60msssz9wjpsbh6m9zskmsmr067rc8hax12s1873zygikgp50") (r "1.75")))

(define-public crate-zino-cli-0.2.3 (c (n "zino-cli") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.0") (d #t) (k 0)))) (h "0mj9xj7dc1mlnf8py9720jr92dqjxskjhpz2w6s6w84akgwx0b0p") (r "1.75")))

(define-public crate-zino-cli-0.2.4 (c (n "zino-cli") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zino-core") (r "^0.21.3") (d #t) (k 0)))) (h "0ld7znnk3pcqkyk8kazv10ik869k8ra5way0vacpx1isad2hvr5z") (r "1.75")))

(define-public crate-zino-cli-0.3.0 (c (n "zino-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zino-core") (r "^0.22.0") (d #t) (k 0)))) (h "1j8mpmlh0aa0f8l66yhxswcs2qm6iaqk91gzkm78avrkczssp7q8") (r "1.75")))

(define-public crate-zino-cli-0.3.1 (c (n "zino-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zino-core") (r "^0.23.0") (d #t) (k 0)))) (h "19b8b152cj6w5qvbcc2zj7kc2n4829c0jdj5g8gxz59d0rap8n3x") (r "1.75")))

