(define-module (crates-io zi pc zipcodes) #:use-module (crates-io))

(define-public crate-zipcodes-0.1.0 (c (n "zipcodes") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1drq3h3k0rc52zb1bi32bx0gdg81mbl7792fdlgymkjcgazbqf1b")))

(define-public crate-zipcodes-0.2.0 (c (n "zipcodes") (v "0.2.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1gfjwkbwhc58vs4cnifga27p3mqn055r0rch04vrdrznbpn0ibrr")))

(define-public crate-zipcodes-0.3.0 (c (n "zipcodes") (v "0.3.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1zz1h7dd5z5vsv5hjsy1ksclqc7zixmrfpvw825r68hiqjv7rdhf")))

(define-public crate-zipcodes-0.3.1 (c (n "zipcodes") (v "0.3.1") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1d9xzyhydxz4g9hzmk1fcysd1fhvkr48zg91cmwyrc2gyq4mzkdj")))

(define-public crate-zipcodes-0.3.2 (c (n "zipcodes") (v "0.3.2") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1qkb9j61xai0wzysdivp243g6wlnim02qfrrrw95akakr3z7rsps")))

(define-public crate-zipcodes-0.3.3 (c (n "zipcodes") (v "0.3.3") (d (list (d (n "bzip2") (r "~0.4.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1bbdfjhlgvvc54a0m0pgm82zi523wwnik537xkhqldbm80iy3fy1")))

(define-public crate-zipcodes-0.3.4 (c (n "zipcodes") (v "0.3.4") (d (list (d (n "bzip2") (r "~0.4.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0cp81j0jx9g8kfjbm3jq1gnx6whr1jkq6g8482p1vjhwf4v4hdiz")))

