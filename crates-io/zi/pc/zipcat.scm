(define-module (crates-io zi pc zipcat) #:use-module (crates-io))

(define-public crate-zipcat-0.1.0 (c (n "zipcat") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "12a3fmqp739kjpg2y78vgf5rhbak9mg2lz679yhfb43lmh5dri2b")))

