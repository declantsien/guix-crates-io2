(define-module (crates-io zi pk zipkin) #:use-module (crates-io))

(define-public crate-zipkin-0.1.0 (c (n "zipkin") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0xjx2vzar2z94nnj2a8iydbg0jh5i0xmbq3awn8qscj198fxgqv5")))

(define-public crate-zipkin-0.1.1 (c (n "zipkin") (v "0.1.1") (d (list (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "1gp01ygk9b4gg58pk6hyrx5yb33dyk537c4j7fd30knsfdj9afn9")))

(define-public crate-zipkin-0.1.2 (c (n "zipkin") (v "0.1.2") (d (list (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "13rfbgwv2y4kd1fyxp39f9794hv6zhfnnw5yqjprkzdggwg95w6a")))

(define-public crate-zipkin-0.1.3 (c (n "zipkin") (v "0.1.3") (d (list (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "1g0jz78w77iymz6356jnji2dla918vbndygcpj4drh31bzsaq132")))

(define-public crate-zipkin-0.1.4 (c (n "zipkin") (v "0.1.4") (d (list (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "15vnm1zmk5bvf3q6sm7xrc8njv0s1xg1s1w00pc02p0divxkbad5")))

(define-public crate-zipkin-0.1.5 (c (n "zipkin") (v "0.1.5") (d (list (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "13v1fcrs0q3wq7974y7a9i3a0za1v6xxhypviyzy29j85dz8pzi7")))

(define-public crate-zipkin-0.1.6 (c (n "zipkin") (v "0.1.6") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "data-encoding") (r "= 2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0m7bw98yqc4phxac07ksnrb35gdplgv90rjnprnw0rysrf6wcvk2")))

(define-public crate-zipkin-0.1.7 (c (n "zipkin") (v "0.1.7") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0bsz4lw0jic0zzas655mm86v0c56pvvlj2r664niwl6d591hn5pd")))

(define-public crate-zipkin-0.1.8 (c (n "zipkin") (v "0.1.8") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0m6500649h0szffajqj5339dvlgvmwpn74y884smyd252lgidw49")))

(define-public crate-zipkin-0.1.9 (c (n "zipkin") (v "0.1.9") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0ryq59fkhpk6faclkcnlscn8hv5w98lv365mnv4v2418c6rapwsb")))

(define-public crate-zipkin-0.2.0 (c (n "zipkin") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "01jsd0j6kwcq299wa257pqxfbdz6bm0hn1aifapm99ywqc7p062s")))

(define-public crate-zipkin-0.2.1 (c (n "zipkin") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0g980vznb9dybyls4vk53kjwnp10wy70s3vgk0jv2gckkjalpgak")))

(define-public crate-zipkin-0.2.2 (c (n "zipkin") (v "0.2.2") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "0hbsgij3z3chk2nsx0dzap80zfqk361dh2bjfa523fsk9j079fxd")))

(define-public crate-zipkin-0.3.0 (c (n "zipkin") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)))) (h "181i4k9m6jhl50z1lqlqabajplzgyvvlqz7jxabfy0qyziaqznlh")))

(define-public crate-zipkin-0.3.1 (c (n "zipkin") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "03y3ix0d747pdj55vwqrcck3fgk239p5h12rxabjbyvh8z43l905") (f (quote (("serde" "zipkin-types/serde"))))))

(define-public crate-zipkin-0.3.2 (c (n "zipkin") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "1g5118n1d2r19si4j7vy3hkkla2jagn0cnfmx4m166inagmmbqw3") (f (quote (("serde" "zipkin-types/serde"))))))

(define-public crate-zipkin-0.3.3 (c (n "zipkin") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "05vvq6dq62ayvqjm23z817kidavfl7hik189485i67clpqgnya00") (f (quote (("serde" "zipkin-types/serde")))) (y #t)))

(define-public crate-zipkin-0.3.4 (c (n "zipkin") (v "0.3.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "17kisismbm1212z8ys15rbxhn9adpqi90gxbqd3vznl88iy1hxqs") (f (quote (("serde" "zipkin-types/serde"))))))

(define-public crate-zipkin-0.3.5 (c (n "zipkin") (v "0.3.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thread-local-object") (r "^0.1") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "0jaa7ng7lm11rvv8asafqj7m04xql17c1d7skvvnpm9m8iisff4g") (f (quote (("serde" "zipkin-types/serde"))))))

(define-public crate-zipkin-0.4.0 (c (n "zipkin") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lazycell") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "0gi3npx446slzb777kbwnw3rdpi7hfby9h4drc2d8lmbvf6rnkw1") (f (quote (("serde" "zipkin-types/serde"))))))

(define-public crate-zipkin-0.4.1 (c (n "zipkin") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lazycell") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "zipkin-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "1bmc9r8lgyg8192ypwmnzjqs5a1d4j4ivi8m9s1zayzvvki1jixn") (f (quote (("serde" "zipkin-types/serde") ("macros" "zipkin-macros"))))))

(define-public crate-zipkin-0.4.2 (c (n "zipkin") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lazycell") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "zipkin-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "zipkin-types") (r "^0.1.0") (d #t) (k 0)))) (h "0ikfryfgvakb8hramrfazvhm64z17b6aq0s8ri1gm66y42ipligh") (f (quote (("serde" "zipkin-types/serde") ("macros" "zipkin-macros"))))))

