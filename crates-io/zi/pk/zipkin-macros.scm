(define-module (crates-io zi pk zipkin-macros) #:use-module (crates-io))

(define-public crate-zipkin-macros-0.1.0 (c (n "zipkin-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1frfabqavn3y9b5fszr3gyx9frxlarchxn1hq4mvs9szpd3r7sf5")))

(define-public crate-zipkin-macros-0.1.1 (c (n "zipkin-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zvh9gyqbsri45h6lnxz0qbivmghn6pxmw54xyp5bzqn19l7avq3")))

