(define-module (crates-io zi pk zipkin-types) #:use-module (crates-io))

(define-public crate-zipkin-types-0.1.0 (c (n "zipkin-types") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12flgcyxf2c0chfmcr8wfddgixiwkw3cvn2zjanxr4kran4n3sns")))

