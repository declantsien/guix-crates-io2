(define-module (crates-io zi pu ziputil) #:use-module (crates-io))

(define-public crate-ziputil-0.2.0 (c (n "ziputil") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "0rd9g6dn3k54fpsi7mlb5dzjlhqkxv0zrzk170v9i1fm00f463jw")))

(define-public crate-ziputil-0.3.0 (c (n "ziputil") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "1m4dhn1l5ayc6rb4xzqgs1rvwjrlqwqrf6c0d2gxpdh5jm3ibsbr")))

