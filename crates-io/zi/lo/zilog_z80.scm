(define-module (crates-io zi lo zilog_z80) #:use-module (crates-io))

(define-public crate-zilog_z80-0.5.0 (c (n "zilog_z80") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0cyqxm1jm199g9ksa4fzpg6ffhlzhracs5siqgws9aivl6xyhvqg")))

(define-public crate-zilog_z80-0.6.0 (c (n "zilog_z80") (v "0.6.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1dnbs9mbjrv3p2vbh8g01b54pd4asqgxcms6haayk3janij8k8f6")))

(define-public crate-zilog_z80-0.7.0 (c (n "zilog_z80") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0qj5j38ns67cxmhpq180vhrn93qkvllh8jachh2wa1fblqj37iy7")))

(define-public crate-zilog_z80-0.8.0 (c (n "zilog_z80") (v "0.8.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "19kwsrf7nzggrln7qq8pakwk7bq76mrzrdi7lplbvyp6p5bkfz7h")))

(define-public crate-zilog_z80-0.9.0 (c (n "zilog_z80") (v "0.9.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0ai3y2984czrhmjynwhkx8v5mil9jjv3xyv401s2ydiz5r7bqdrw")))

(define-public crate-zilog_z80-0.10.0 (c (n "zilog_z80") (v "0.10.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0ivvbqaaks8jfig9f8nyslrjlq70r45vi32kiwfxkj85ryv3rwj4")))

(define-public crate-zilog_z80-0.11.0 (c (n "zilog_z80") (v "0.11.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)))) (h "1b7mfknx3apbklp2aak05v4psczdswcd28yzqjzrayam82lad3gn")))

(define-public crate-zilog_z80-0.13.0 (c (n "zilog_z80") (v "0.13.0") (h "1ib9n8k167n6a9ll1fsn7050hgzjqjkf3lrcz2dlw8nv93kd0n4g")))

(define-public crate-zilog_z80-0.14.0 (c (n "zilog_z80") (v "0.14.0") (h "16bf2l5rgnjpvx32wyg4rlknd7wv1d8bqmpsc99w3vld5znpf9y2")))

(define-public crate-zilog_z80-0.15.0 (c (n "zilog_z80") (v "0.15.0") (h "0kxji9vywmmwn09vifblpxfgwyc0hbal9j1a7867ln1q5fxw4bnc")))

(define-public crate-zilog_z80-0.16.0 (c (n "zilog_z80") (v "0.16.0") (h "0c5svnmbpsj12wl0qd2lscywfilnfp171hg9a4spbn54mk7mcm6l")))

