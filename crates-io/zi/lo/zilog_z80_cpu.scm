(define-module (crates-io zi lo zilog_z80_cpu) #:use-module (crates-io))

(define-public crate-zilog_z80_cpu-0.1.0 (c (n "zilog_z80_cpu") (v "0.1.0") (h "1j9wq312wvafxxxdyjq9261f1vsmnvxvw40n6xxl0q9cygqsmvcb") (y #t)))

(define-public crate-zilog_z80_cpu-0.1.1 (c (n "zilog_z80_cpu") (v "0.1.1") (h "1h030m4csk99mdk1m22p5grg6pcxack369qg0ahzm00xs2cqnwfy")))

(define-public crate-zilog_z80_cpu-0.1.2 (c (n "zilog_z80_cpu") (v "0.1.2") (h "0flmgrp4rwp38rsbkkvsqzhsyr0nr3nv5f1lisx7n1ww90ppgwyr")))

