(define-module (crates-io zi nk zink) #:use-module (crates-io))

(define-public crate-zink-0.0.1 (c (n "zink") (v "0.0.1") (h "06ivcpvgzsd1bbjrz5dzfaqx1176q8lx0wpcfn8yz2kalz1vf11r")))

(define-public crate-zink-0.0.2 (c (n "zink") (v "0.0.2") (h "1i31mwk8g7jfqzm4dz38zw5x1mv1s89q8jdr16lby0yv3y1cikfp")))

(define-public crate-zink-0.0.3 (c (n "zink") (v "0.0.3") (h "1fv6nq9jcr9qbczyms296m0qy4dwv7j9rpiphya0vk7lfhs5pp6w")))

(define-public crate-zink-0.1.0 (c (n "zink") (v "0.1.0") (h "1kbwnnj6gjabjwnqkzgck37f54lxnk29yam7lg50wiy3ywv17yq9")))

(define-public crate-zink-0.1.1 (c (n "zink") (v "0.1.1") (h "0zh83zadnjzny6k88rapjrkg0gdzzflygsq4rz8zyi36p4pjbjvl")))

(define-public crate-zink-0.1.2 (c (n "zink") (v "0.1.2") (h "1qjibb7njdkh0immi49pzbl7vhdsrn1jbylhdbvxzwh0la025czj")))

(define-public crate-zink-0.1.3 (c (n "zink") (v "0.1.3") (h "1dqn20dnn1bfwxkl9wg3rpp92gdxxlbm1800xrsx8n9pmalwpsml")))

(define-public crate-zink-0.1.4 (c (n "zink") (v "0.1.4") (d (list (d (n "zink-codegen") (r "=0.1.4") (d #t) (k 0)))) (h "01g4153mg40wwcq5vrjzy8wlzy8dlgdv31vqshz4plqqkbfayldm")))

(define-public crate-zink-0.1.5-pre.1 (c (n "zink") (v "0.1.5-pre.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "filetests") (r "=0.1.5-pre.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2) (p "zinkc-filetests")) (d (n "paste") (r "^1.0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "=0.1.5-pre.1") (d #t) (k 0)) (d (n "zint") (r "=0.1.5-pre.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "08xm229lyl09x8zgcs1mkisq8rzvzgdpy3lzhyl6npyw458xvqdh")))

(define-public crate-zink-0.1.5-pre.2 (c (n "zink") (v "0.1.5-pre.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "paste") (r "^1.0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "=0.1.5-pre.2") (d #t) (k 0)) (d (n "zinkc-filetests") (r "=0.1.5-pre.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zint") (r "=0.1.5-pre.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "1l8s3dp1lqz0llz3bf6rc0p17yw9aad9h3wy60aycvv2cxvd5fpy")))

(define-public crate-zink-0.1.5 (c (n "zink") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "paste") (r "^1.0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "=0.1.5") (d #t) (k 0)) (d (n "zinkc-filetests") (r "=0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zint") (r "=0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0ng4jh7l0mhkfil85aixskjn3q543iqqj7aar548cpfcqpyj2ji9")))

(define-public crate-zink-0.1.6 (c (n "zink") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "paste") (r "^1.0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "=0.1.6") (d #t) (k 0)) (d (n "zinkc-filetests") (r "=0.1.6") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zint") (r "=0.1.6") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "12jlznzg9q7n8jrs5cl90wc234r539ikbkxfbgk06iijlvk6c874")))

(define-public crate-zink-0.1.7 (c (n "zink") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "paste") (r "^1.0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "=0.1.7") (d #t) (k 0)) (d (n "zinkc-filetests") (r "=0.1.7") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zint") (r "=0.1.7") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "05mz6qngjvsiz02a7wkvnmnfbn0ah97qlf9ilhps14za7vi2hmf7")))

(define-public crate-zink-0.1.8 (c (n "zink") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "filetests") (r "^0.1.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2) (p "zinkc-filetests")) (d (n "paste") (r "^1.0.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "^0.1.8") (d #t) (k 0)) (d (n "zint") (r "^0.1.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "137s3pwhd6hj6pwa0ifdayd9zid3dp9nd4ldi7riwfri90y2fj2n")))

(define-public crate-zink-0.1.9 (c (n "zink") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "filetests") (r "^0.1.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2) (p "zinkc-filetests")) (d (n "paste") (r "^1.0.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "^0.1.9") (d #t) (k 0)) (d (n "zint") (r "^0.1.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "1dcnqh1ggamxf9lz7agbly0lq0y8h4xlzdbdkqhfxg0xkfx0jwq2")))

(define-public crate-zink-0.1.10 (c (n "zink") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "filetests") (r "^0.1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2) (p "zinkc-filetests")) (d (n "paste") (r "^1.0.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "zink-codegen") (r "^0.1.10") (d #t) (k 0)) (d (n "zint") (r "^0.1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0lr68nhsqk1rvqq7p26hvxlrl453dcynkgxdkpavd164rz7klann")))

