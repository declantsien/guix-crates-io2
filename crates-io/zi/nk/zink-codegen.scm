(define-module (crates-io zi nk zink-codegen) #:use-module (crates-io))

(define-public crate-zink-codegen-0.0.0 (c (n "zink-codegen") (v "0.0.0") (h "186gxczhdrj2aa9glkfwsvrjbxvfvfyb2b2a3qimpn8jh909vi7h")))

(define-public crate-zink-codegen-0.1.4 (c (n "zink-codegen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "03yl21sarfmjm1yh05xsqf7qqw163jsbml8rmcffszn8bxxcpwds")))

(define-public crate-zink-codegen-0.1.5-pre.1 (c (n "zink-codegen") (v "0.1.5-pre.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "=0.1.5-pre.1") (d #t) (k 0)))) (h "02iwklwrxfjq6r5k1zfl4c1qca4ir0bi6qxrhjbwsdn9nkghbflk")))

(define-public crate-zink-codegen-0.1.5-pre.2 (c (n "zink-codegen") (v "0.1.5-pre.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "=0.1.5-pre.2") (d #t) (k 0)))) (h "1j6p9a9lqvyifdggymgvf8mm23612xpp2vc0n56df7p15wzq175b")))

(define-public crate-zink-codegen-0.1.5 (c (n "zink-codegen") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "=0.1.5") (d #t) (k 0)))) (h "1c9l3la9zk2a4a75r9w9mpcn3w8z8r65n2gsn59isab2b0a12zjg")))

(define-public crate-zink-codegen-0.1.6 (c (n "zink-codegen") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "=0.1.6") (d #t) (k 0)))) (h "0hnpzmqf7vwrw2mcdpvl79scmj5ljyilrwz1zvys0n014sfqjwcn")))

(define-public crate-zink-codegen-0.1.7 (c (n "zink-codegen") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "=0.1.7") (d #t) (k 0)))) (h "0n29z2jcin7y5n9pqww0bd3c98flz0036ci56qmm3qb5q6yhykfj")))

(define-public crate-zink-codegen-0.1.8 (c (n "zink-codegen") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "^0.1.8") (d #t) (k 0)))) (h "1hd5jwm5nq8blgj25rg8ahcaci6gw1bg9grszl7yqrhyjr5gb3wf")))

(define-public crate-zink-codegen-0.1.9 (c (n "zink-codegen") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "^0.1.9") (d #t) (k 0)))) (h "0ldyfpc5zycbrr7pvq7552w3rrrps8hlsrhhbl7bbpf26286drp6")))

(define-public crate-zink-codegen-0.1.10 (c (n "zink-codegen") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "zabi") (r "^0.1.10") (f (quote ("hex" "syn"))) (d #t) (k 0)))) (h "10f1f5lb4dfifdqm80s79f32vh3q23p360rxzxgaz4p1i7v6nxnc")))

