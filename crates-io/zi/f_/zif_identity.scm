(define-module (crates-io zi f_ zif_identity) #:use-module (crates-io))

(define-public crate-zif_identity-0.0.1 (c (n "zif_identity") (v "0.0.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)) (d (n "untrusted") (r "^0.5.0") (d #t) (k 0)))) (h "0509kh0i7zp4cx9fxs33i5d3znd7iw0yiyah1kp7qfw6lh3dnmdr")))

