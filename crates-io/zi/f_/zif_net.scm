(define-module (crates-io zi f_ zif_net) #:use-module (crates-io))

(define-public crate-zif_net-0.0.1 (c (n "zif_net") (v "0.0.1") (h "0z7qwqi2sq61m06z2xsjdz5b0yzmrmz4f1svzr4gjnx19acjw6kx")))

(define-public crate-zif_net-0.1.0 (c (n "zif_net") (v "0.1.0") (d (list (d (n "tiny_http") (r "^0.5") (d #t) (k 0)) (d (n "zif_identity") (r "^0.0.1") (d #t) (k 0)))) (h "0wcwsd9sc71ddrhq6zvl1fbcpjaqbm721rkg2bzb8lnzwb14w468")))

