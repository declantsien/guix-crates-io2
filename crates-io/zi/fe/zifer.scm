(define-module (crates-io zi fe zifer) #:use-module (crates-io))

(define-public crate-zifer-1.0.0 (c (n "zifer") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "09hb8x9pnl1vz6hha65vfnaba20hi8zyk4n19l33gcp6afr8d9kk")))

(define-public crate-zifer-1.0.1 (c (n "zifer") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0wiyr23h71f03iapgzlv228wfcigda7l98ik80p2knllqjjvvk40")))

(define-public crate-zifer-1.0.2 (c (n "zifer") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0nq9gznrvhk6vzr9mqnvilzkvigpkn84rrjwnfnngw8x16rn2zf2")))

(define-public crate-zifer-1.0.3 (c (n "zifer") (v "1.0.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1saj0v6ynpzgaxm3vzaf0d06h1b1z6l2q0zqiykbk19hxflxiabk")))

(define-public crate-zifer-1.0.4 (c (n "zifer") (v "1.0.4") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "00p1545va6pd4f87h9fr7csy7kvjglnxf41va97nrija53a5x2pk")))

(define-public crate-zifer-1.0.5 (c (n "zifer") (v "1.0.5") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0b60njipa2hp2bh1pglfzyihgnp66q1azms8k3vcslksc4r5w0wj")))

