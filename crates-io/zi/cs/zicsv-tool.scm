(define-module (crates-io zi cs zicsv-tool) #:use-module (crates-io))

(define-public crate-zicsv-tool-0.1.0 (c (n "zicsv-tool") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "ipnet") (r "^1.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "structopt-derive") (r "^0.2") (k 0)) (d (n "trust-dns-proto") (r "^0.3") (k 0)) (d (n "trust-dns-resolver") (r "^0.8") (k 0)) (d (n "url") (r "^1.7") (k 0)) (d (n "zicsv") (r "^0.1") (f (quote ("serialization"))) (d #t) (k 0)))) (h "1kjp4akc9zwbzflq6b4ggbas1kgvihswzcxadxzsyk4b1n4v6qhy")))

