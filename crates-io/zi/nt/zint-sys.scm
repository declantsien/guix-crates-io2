(define-module (crates-io zi nt zint-sys) #:use-module (crates-io))

(define-public crate-zint-sys-0.1.0 (c (n "zint-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "blake3") (r "^1.4.1") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 2)))) (h "1kvlpplz2mifiiiq3dq1h6chchal3572449gvgzr11chfkx2iy8d") (l "zint")))

(define-public crate-zint-sys-0.1.1 (c (n "zint-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "blake3") (r "^1.4.1") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 2)))) (h "1z1mlh25p7bpfn7parhx6qr6ilnrb383bwhhl8wqvc6766hp5q5w") (l "zint")))

