(define-module (crates-io zi p_ zip_long) #:use-module (crates-io))

(define-public crate-zip_long-0.1.0 (c (n "zip_long") (v "0.1.0") (h "0njz2xgrh54kw6nl0l8qp3sjlaqv5qf8g19fr39iybwa616z46i1")))

(define-public crate-zip_long-0.1.1 (c (n "zip_long") (v "0.1.1") (h "174b2gf8pv3s0hipb2xw666pr99vy6df1qi7pficac21lw0489yw")))

