(define-module (crates-io zi p_ zip_deployer) #:use-module (crates-io))

(define-public crate-zip_deployer-1.0.0 (c (n "zip_deployer") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.3") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.8.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.19") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "1ga6kzrj8rbplwvxjzd7zalmcrvagjkgyyhfjm5gk5w7snx2m0mf")))

