(define-module (crates-io zi p_ zip_codes_plus) #:use-module (crates-io))

(define-public crate-zip_codes_plus-0.1.0 (c (n "zip_codes_plus") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 1)) (d (n "titlecase") (r "^1.1.0") (d #t) (k 1)))) (h "15lcv2rq6dm5713yc21s86mbrna107cbyr4zw7wkdff5n940lkqg")))

(define-public crate-zip_codes_plus-0.2.0 (c (n "zip_codes_plus") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 1)) (d (n "titlecase") (r "^1.1.0") (d #t) (k 1)))) (h "0vrf1w2hmxks0mhx6pd82s0zbbgcvncii6ilpjgdsgl5m0b9bwdy")))

