(define-module (crates-io zi p_ zip_eq) #:use-module (crates-io))

(define-public crate-zip_eq-0.1.0 (c (n "zip_eq") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lipsum") (r "^0.8.0") (d #t) (k 2)))) (h "1bkh99c5wr21pkxa3fpvp49fx3by1mbsdbdrnz7j5yhwmrshmk6l")))

