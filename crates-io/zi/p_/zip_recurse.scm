(define-module (crates-io zi p_ zip_recurse) #:use-module (crates-io))

(define-public crate-zip_recurse-1.0.0 (c (n "zip_recurse") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate-zlib"))) (k 0)))) (h "1msy55n5wzih5zs775k1hjh8mkmy4cw616q7wzs3jsm2ljdidy08")))

(define-public crate-zip_recurse-1.0.1 (c (n "zip_recurse") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate-zlib"))) (k 0)))) (h "0dnn14g2c81ykskbnv9yvay41mmd6gbg53j9rs7860iflh369cgv")))

