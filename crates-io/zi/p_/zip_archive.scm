(define-module (crates-io zi p_ zip_archive) #:use-module (crates-io))

(define-public crate-zip_archive-0.1.1 (c (n "zip_archive") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "image_compressor") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "0shyhh4dfa6f6q69qqgis83ifbi7dqh4bvjpys6p3hpj4s9fm9f1")))

(define-public crate-zip_archive-1.0.0 (c (n "zip_archive") (v "1.0.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "1px9k4c63gs8nc0sv5g43ir1d2lq3r78y9ivk0kd7apm20j2i884")))

(define-public crate-zip_archive-1.2.0 (c (n "zip_archive") (v "1.2.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "function_name") (r "^0.2.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "14p21kn9b7v9kbmc7s1bsrdsv2y76md33am9pbs7i53m912d5amh")))

(define-public crate-zip_archive-1.2.1 (c (n "zip_archive") (v "1.2.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "function_name") (r "^0.2.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "17yyb19xrlngv1689z0iarh6kqnzgr731jxvs8sp0rlssi4yxlrc")))

(define-public crate-zip_archive-1.2.2 (c (n "zip_archive") (v "1.2.2") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "function_name") (r "^0.2.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "14pa7jwnc1gwg24mrq7dwxnnkm6dv9qdrm9gpjh1yw27q8aplxll")))

