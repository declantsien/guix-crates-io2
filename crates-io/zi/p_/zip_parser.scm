(define-module (crates-io zi p_ zip_parser) #:use-module (crates-io))

(define-public crate-zip_parser-0.1.0 (c (n "zip_parser") (v "0.1.0") (h "0dp24j209nqbzr3879xbimwplp9hw3mx4p7pc3xrcs06kgigw6x6") (f (quote (("std") ("default"))))))

(define-public crate-zip_parser-0.1.1 (c (n "zip_parser") (v "0.1.1") (h "1ri22macpnsfq7yw3d43pwzrdrlgzy2grwskfcwi9i53p0ds6fjs") (f (quote (("std"))))))

(define-public crate-zip_parser-0.2.0 (c (n "zip_parser") (v "0.2.0") (h "1f7ydr83rl5kbh99gik54f7pg5vkzb7b08i3n6yhsls0vxdrgkk9") (f (quote (("std"))))))

(define-public crate-zip_parser-0.2.1 (c (n "zip_parser") (v "0.2.1") (h "177yrs04g6axqqmvgbxn89ajm7ak25csj89jy00qqgbysrq2pr07") (f (quote (("std"))))))

(define-public crate-zip_parser-0.3.0 (c (n "zip_parser") (v "0.3.0") (h "1n0zvr4xm8sbmyzxdpffcr1x5hr4fw2gzrhbnz9lszhrnb379nkv") (f (quote (("std"))))))

(define-public crate-zip_parser-0.4.0 (c (n "zip_parser") (v "0.4.0") (h "0yrg00radz3hal6v96663yrccqvy1abh43gdvb85bisx8bkby79q") (f (quote (("std"))))))

(define-public crate-zip_parser-0.4.1 (c (n "zip_parser") (v "0.4.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "0z6flb5bkp1kys9y19p07j15sx8an5kzp7mlxl1z0hdx2n6mw9x7") (f (quote (("std"))))))

(define-public crate-zip_parser-0.4.2 (c (n "zip_parser") (v "0.4.2") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "03y5hc5pl0w8b4ihs52q2y1gz35dl08gjzzpxp1mky5lsyniwal9") (f (quote (("std"))))))

(define-public crate-zip_parser-0.4.3 (c (n "zip_parser") (v "0.4.3") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "1n9rp89yjmyh967si90gm118bhhvhh0jb60dbgm8ymlhmbaml9gk") (f (quote (("std"))))))

