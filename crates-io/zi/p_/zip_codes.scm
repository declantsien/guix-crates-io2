(define-module (crates-io zi p_ zip_codes) #:use-module (crates-io))

(define-public crate-zip_codes-0.0.1 (c (n "zip_codes") (v "0.0.1") (d (list (d (n "csv") (r "*") (d #t) (k 1)) (d (n "phf") (r "*") (d #t) (k 0)) (d (n "phf_codegen") (r "*") (d #t) (k 1)) (d (n "rustc-serialize") (r "*") (d #t) (k 1)))) (h "1hkrj56clmyz6fbr8ysdg5nly94x9pgg0wx5p0dcmj7khi9flxld")))

(define-public crate-zip_codes-0.1.0 (c (n "zip_codes") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 1)))) (h "0i2jk8fkfwjzf28rhaic21ymy02gzg8jy2y2jldh4b76msx0lkk4")))

