(define-module (crates-io zi p_ zip_structs) #:use-module (crates-io))

(define-public crate-zip_structs-0.1.0 (c (n "zip_structs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "vfs") (r "^0.5.0") (d #t) (k 2)))) (h "0f16f96zcafki42zkd5nv77qwsl41l6gh7jaaxs3qsqqpka4wkfd")))

(define-public crate-zip_structs-0.1.1 (c (n "zip_structs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "vfs") (r "^0.5.0") (d #t) (k 2)))) (h "0akjkwsm6kap8mljl61qnfa9jqb5hqxx7djnfklpai43ywhg8w7i")))

(define-public crate-zip_structs-0.2.0 (c (n "zip_structs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "vfs") (r "^0.5.1") (d #t) (k 2)))) (h "1jmr83wfz3m9m48da3bix4ar8cqs2s17mbph9mnmy87zg18ja6m0")))

(define-public crate-zip_structs-0.2.1 (c (n "zip_structs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "vfs") (r "^0.5.2") (d #t) (k 2)))) (h "1r8ffl8xvaz07pil56kljq4a90vw7fbj8vd31y1452gyzxmlm0nf")))

