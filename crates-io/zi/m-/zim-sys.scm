(define-module (crates-io zi m- zim-sys) #:use-module (crates-io))

(define-public crate-zim-sys-0.1.0 (c (n "zim-sys") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(target_family = \"unix\")") (k 1)))) (h "0s29smk6kh0rj7gadlnpkph3qdrjy2x0nxrkjx294kpraa7wgn7a") (l "zim")))

