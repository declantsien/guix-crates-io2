(define-module (crates-io zi m- zim-rs) #:use-module (crates-io))

(define-public crate-zim-rs-0.1.0 (c (n "zim-rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "zim-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1iaf0mb7swdgjchh518bpnc5fiyjkg1wmcv6rgl0xk09bps7mrl2")))

(define-public crate-zim-rs-0.1.1 (c (n "zim-rs") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "zim-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0h8kcsq2didhjk8lhq36fg39bdgjb7qx8bqmcqn2ihnrpsrnfay5")))

