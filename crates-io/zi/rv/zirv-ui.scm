(define-module (crates-io zi rv zirv-ui) #:use-module (crates-io))

(define-public crate-zirv-ui-0.1.0 (c (n "zirv-ui") (v "0.1.0") (d (list (d (n "gloo") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset" "wasm-bindgen"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)))) (h "12gb76wic23ppz3sciq0701zj4sgh322ppjxjv8qp3c5a1m1hzn5") (f (quote (("toast") ("full" "toast" "flex") ("flex"))))))

(define-public crate-zirv-ui-0.1.1 (c (n "zirv-ui") (v "0.1.1") (d (list (d (n "gloo") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset" "wasm-bindgen"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)))) (h "0j6h5q4q2a3l5pfs6vpcnn1anwfjf3mf6ag081ikk76hjhbh53wk") (f (quote (("toast") ("full" "toast" "flex") ("flex"))))))

(define-public crate-zirv-ui-0.2.0 (c (n "zirv-ui") (v "0.2.0") (d (list (d (n "gloo") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset" "wasm-bindgen"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)))) (h "126l9nmbicc9wzrc501b3n9l9rn1myz8s0gv2jad02gpj041337k") (f (quote (("toast") ("theme-toggle") ("image") ("full" "toast" "flex" "image" "theme-toggle") ("flex"))))))

(define-public crate-zirv-ui-0.2.1 (c (n "zirv-ui") (v "0.2.1") (d (list (d (n "gloo") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset" "wasm-bindgen"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (k 0)))) (h "1byqxjm7v0z54ia0sl6q9c9w7qsdk43767w3qbaj00qfghbc05yh") (f (quote (("toast") ("theme-toggle") ("image") ("full" "toast" "flex" "image" "theme-toggle") ("flex"))))))

