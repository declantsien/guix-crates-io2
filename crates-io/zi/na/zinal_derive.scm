(define-module (crates-io zi na zinal_derive) #:use-module (crates-io))

(define-public crate-zinal_derive-0.1.0 (c (n "zinal_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0sbcp7zn1bpxvrlajcc1b84kzmhgc0l00yqkq77n568yzga1l5ih") (f (quote (("axum"))))))

(define-public crate-zinal_derive-0.2.1 (c (n "zinal_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1alakl4fbi36zq4hi50c1q94i55jwbz5x24kzir6wz2yh5h3ixlb") (f (quote (("axum"))))))

