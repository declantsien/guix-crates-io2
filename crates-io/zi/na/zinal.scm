(define-module (crates-io zi na zinal) #:use-module (crates-io))

(define-public crate-zinal-0.1.0 (c (n "zinal") (v "0.1.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "zinal_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0wzasfi9jz7aalpb5w26y8yy1r3rfig40n9f5vasy5n2pgmazdan") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:zinal_derive") ("axum" "zinal_derive?/axum"))))))

(define-public crate-zinal-0.2.0 (c (n "zinal") (v "0.2.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "zinal_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "00d19pcs88vlj3sbpr8288mfvaaz6a4mawnjyd5is3q09g700n8n") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:zinal_derive") ("axum" "zinal_derive?/axum"))))))

(define-public crate-zinal-0.2.1 (c (n "zinal") (v "0.2.1") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "zinal_derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1wpi8kmxwsi1hdbfqd2nm7sppikpflhhyd30ijmxndwzjr9h5iii") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:zinal_derive") ("axum" "zinal_derive?/axum"))))))

