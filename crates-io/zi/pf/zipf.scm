(define-module (crates-io zi pf zipf) #:use-module (crates-io))

(define-public crate-zipf-0.1.0 (c (n "zipf") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bgx0lnamsn7mcrklmqcn60ql2mv0symid2zfn7lci7m09phjlx4")))

(define-public crate-zipf-0.1.1 (c (n "zipf") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1wg91d4qpb70zpyxrhv7a0vq41ac7cxa4npsmdylygp7hidh65h4")))

(define-public crate-zipf-0.2.0 (c (n "zipf") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "04jbx36vdvy8a7nwxpwk94s75lzjnaaw2vh9hw9jk1dm4dh93dm4")))

(define-public crate-zipf-1.0.0 (c (n "zipf") (v "1.0.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "1axsz2a00856f0l64ha7ac7n3x7z3wcl4k201s2h3vz2nq1lw9ma")))

(define-public crate-zipf-1.1.0 (c (n "zipf") (v "1.1.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "149z97diyw079dw6di953psf3whiiabh13js14b9bp2x2pszifxx")))

(define-public crate-zipf-2.0.0 (c (n "zipf") (v "2.0.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "0bxi8wj3wh2y51kypvw3mdsn1knm9b0nxrznigai5l7kfj5hr4sr")))

(define-public crate-zipf-3.0.0 (c (n "zipf") (v "3.0.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "1h6wj0karfays8bq23nwa4hr6s25mhb9bplf0c5q6r2mxa13r0gs")))

(define-public crate-zipf-3.1.0 (c (n "zipf") (v "3.1.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "08j1p57kybnml96dq7nkj76934cazmhxw826mp1c628nx3pifx72") (y #t)))

(define-public crate-zipf-4.0.0 (c (n "zipf") (v "4.0.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "0k0fabs83373s1k8v6gbwrfsh454np4vhxdfl0jpg0ddmll8nda4")))

(define-public crate-zipf-4.0.1 (c (n "zipf") (v "4.0.1") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "0gh5cl2vdn2wfnr5akv6yg5b55q9y3vs29dxnhxmgsz202rm9mm9")))

(define-public crate-zipf-4.1.1 (c (n "zipf") (v "4.1.1") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "1vb42dpjqch2v0lqnnrzxadlw33kk39vslqr08a51n9kwq2cikdw") (y #t)))

(define-public crate-zipf-5.0.0 (c (n "zipf") (v "5.0.0") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "1pv4lkalgayv2bi6gff952ihg1kclkxh5aqqfvrw0x0hfz8nig1a")))

(define-public crate-zipf-5.0.1 (c (n "zipf") (v "5.0.1") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "07x6a7072xvqxsl5yvam7hqhrz2j1w7fwhiqv7ps1pmyhwnpfmr0")))

(define-public crate-zipf-6.0.0 (c (n "zipf") (v "6.0.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "randomkit") (r "^0.1") (d #t) (k 2)))) (h "127lqpxx2hcywpgsp1326k8pg3p19kbadmrlb3192h0kls54h3pz")))

(define-public crate-zipf-6.0.1 (c (n "zipf") (v "6.0.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0j381nhsv2nlacn5r8lr9z0a8kl826rr2spib0n30z33mv3jqzvh")))

(define-public crate-zipf-6.1.0 (c (n "zipf") (v "6.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "12dmk8qsjqg7hd5fcsv12fxkvycj8gjkndpq6v967zsgg9kbh4ly")))

(define-public crate-zipf-7.0.0 (c (n "zipf") (v "7.0.0") (d (list (d (n "rand") (r "^0.8.0") (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0plr7vph8j9z13z7476df1d0krvrp77v9qdpnnpdzlmml6kqhml3")))

(define-public crate-zipf-7.0.1 (c (n "zipf") (v "7.0.1") (d (list (d (n "rand") (r "^0.8.0") (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1pcl3wpnil86dgn6dd4ghnfbjrprlrdzl58x03g3mk6q1vd523ir")))

