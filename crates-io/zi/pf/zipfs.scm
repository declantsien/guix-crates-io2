(define-module (crates-io zi pf zipfs) #:use-module (crates-io))

(define-public crate-zipfs-0.0.0 (c (n "zipfs") (v "0.0.0") (h "15g021f43j5fxpknzzn1069jsw4xyg66ncaq75zmvvyn461ga3yv")))

(define-public crate-zipfs-0.0.1 (c (n "zipfs") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_zip") (r "^0.0.11") (f (quote ("chrono" "deflate" "zstd"))) (d #t) (k 0)) (d (n "lunchbox") (r "^0.1") (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "fs"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1s4av1r7gjdh2w48q77ylnjfix09r9frgjmzq984gsxl1sm79pd4")))

(define-public crate-zipfs-0.0.2 (c (n "zipfs") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_zip") (r "^0.0.1") (f (quote ("chrono" "deflate" "zstd"))) (d #t) (k 0) (p "async_zip2")) (d (n "lunchbox") (r "^0.1") (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "fs"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("compat"))) (d #t) (k 0)))) (h "1rxlpmiz552hn3al4mahsynhm71372a95s21iyqs5c0jvjg8pad5")))

