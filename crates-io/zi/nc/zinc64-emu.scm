(define-module (crates-io zi nc zinc64-emu) #:use-module (crates-io))

(define-public crate-zinc64-emu-0.8.0 (c (n "zinc64-emu") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (k 0)) (d (n "resid-rs") (r "^1.0") (d #t) (k 0)) (d (n "zinc64-core") (r "^0.8") (d #t) (k 0)))) (h "0a5492lyp03lfa1k74k1a0y5gqlanqnq5zv7pq1qd17bppscw9qf") (f (quote (("std") ("default" "std"))))))

