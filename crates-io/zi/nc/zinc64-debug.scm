(define-module (crates-io zi nc zinc64-debug) #:use-module (crates-io))

(define-public crate-zinc64-debug-0.6.0 (c (n "zinc64-debug") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "zinc64") (r "^0.6.0") (d #t) (k 0)))) (h "14pwn82igpa7hicmm3svjd1h0rhzp2yb9vbik8vdp92wiwqjpqzr")))

(define-public crate-zinc64-debug-0.8.0 (c (n "zinc64-debug") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "zinc64-emu") (r "^0.8") (d #t) (k 0)))) (h "0ffs3qc906a7p3ba2y1vcgx06s4h7dd5c782xyaq2ds4azxjjsz7")))

