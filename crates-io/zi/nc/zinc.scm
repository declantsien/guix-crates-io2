(define-module (crates-io zi nc zinc) #:use-module (crates-io))

(define-public crate-zinc-0.0.1 (c (n "zinc") (v "0.0.1") (h "0nvdkxm34p4z23iqnmblf3mzr9mrwcywdsh72ydsca50xs9afdfq")))

(define-public crate-zinc-0.0.2 (c (n "zinc") (v "0.0.2") (h "0804j1y277qky8s0r9v7jk45j7xrwz286xbm1a0n3w2gqsg9zsdx")))

