(define-module (crates-io zi nc zinc64-loader) #:use-module (crates-io))

(define-public crate-zinc64-loader-0.6.0 (c (n "zinc64-loader") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "zinc64") (r "^0.6.0") (d #t) (k 0)))) (h "06lxvsdzgk4bgs0fx843sal42abykac3bpbpdh9b2rpgyqczdrzk")))

(define-public crate-zinc64-loader-0.8.0 (c (n "zinc64-loader") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "log") (r "^0.3") (k 0)) (d (n "zinc64-emu") (r "^0.8") (d #t) (k 0)))) (h "0i5f2l8ydphx33xcvl5mkd69604k2pis05zxcmp9n77q88kwzryz") (f (quote (("std") ("default" "std"))))))

