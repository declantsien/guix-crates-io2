(define-module (crates-io zi nc zinc64-sdl) #:use-module (crates-io))

(define-public crate-zinc64-sdl-0.6.0 (c (n "zinc64-sdl") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "zinc64") (r "^0.6.0") (d #t) (k 0)) (d (n "zinc64-debug") (r "^0.6.0") (d #t) (k 0)) (d (n "zinc64-loader") (r "^0.6.0") (d #t) (k 0)))) (h "173lcd3ni9lj2ik2vcf0jghqlryq3k3x4qwy6nj3j6s1gwwsb9r4")))

