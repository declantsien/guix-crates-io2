(define-module (crates-io wx f- wxf-converter) #:use-module (crates-io))

(define-public crate-wxf-converter-0.3.0 (c (n "wxf-converter") (v "0.3.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.2.2") (f (quote ("json" "pickle" "yaml"))) (k 0)))) (h "0w2qycbbhfzwg1747qyq4anf8qzrxrpy3fvfs10l1d2pwjly8gr6")))

(define-public crate-wxf-converter-0.3.1 (c (n "wxf-converter") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (f (quote ("json" "pickle" "yaml" "toml"))) (k 0)))) (h "1szw6d046ld6j6xn6f905i1zdpv74xaiv393skhlcfryrg9jdfzk")))

(define-public crate-wxf-converter-0.3.2 (c (n "wxf-converter") (v "0.3.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (f (quote ("json" "pickle" "yaml" "toml"))) (k 0)))) (h "0qbphj75ysr42y87x8xc2w0wl4h5nrh3x4lhjr4yfp5j6ajy1b60")))

