(define-module (crates-io wx ru wxrust-config) #:use-module (crates-io))

(define-public crate-wxrust-config-0.0.1-alpha (c (n "wxrust-config") (v "0.0.1-alpha") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "19qdrnjhxkcrz8kv2wz2mdppcq7qhihysbn8q1rn57mkwy7snc37") (f (quote (("vendored"))))))

(define-public crate-wxrust-config-0.0.1-alpha2 (c (n "wxrust-config") (v "0.0.1-alpha2") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1lmxdn5pnzigmaz67hwgvqay3a0wb2xnsvxdbzwj9fydn6i034gw") (f (quote (("vendored"))))))

