(define-module (crates-io wx ru wxrust-base) #:use-module (crates-io))

(define-public crate-wxrust-base-0.0.1-alpha (c (n "wxrust-base") (v "0.0.1-alpha") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "wxrust-config") (r "^0.0.1-alpha2") (d #t) (k 1)))) (h "1akza35j3cra5i1zgnih5yhzhzm6c5v8ncjdldav8mqdafs13m77") (f (quote (("vendored" "wxrust-config/vendored"))))))

