(define-module (crates-io wx -r wx-rs) #:use-module (crates-io))

(define-public crate-wx-rs-0.1.0 (c (n "wx-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0xsi979r8ljw38n9ljxlw0rakgxji5yqx6by5kqw9gcvhba9rk23")))

(define-public crate-wx-rs-0.1.1 (c (n "wx-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "18r2jjyrxfj2mymdk08cng61iylz875qi50q1c27w6pjsz0r0d8p")))

(define-public crate-wx-rs-0.1.2 (c (n "wx-rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0x300mfrvmkpw5rxyp5r7618vxl1zlidv2ivmcsc6pki3dhhx0dn") (f (quote (("stub_lib"))))))

(define-public crate-wx-rs-0.2.0 (c (n "wx-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0cwyv4s86nqa8ljkkf8sxjibj68d2gx47vwl0xjbczcrlbdj01mp") (f (quote (("stub_lib"))))))

