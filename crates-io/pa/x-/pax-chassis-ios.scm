(define-module (crates-io pa x- pax-chassis-ios) #:use-module (crates-io))

(define-public crate-pax-chassis-ios-0.10.2 (c (n "pax-chassis-ios") (v "0.10.2") (d (list (d (n "pax-chassis-common") (r "^0.10.2") (d #t) (k 0)))) (h "1lvyk5gjflh2dj0nyipgw0qga90iw7mw4k01yy3y341fiiq1b0ki")))

(define-public crate-pax-chassis-ios-0.10.3 (c (n "pax-chassis-ios") (v "0.10.3") (d (list (d (n "pax-chassis-common") (r "^0.10.3") (d #t) (k 0)))) (h "1z332fyfyn41zkbpvcx0i4m65f55zl5ry1i16ascbav3kll811w8")))

(define-public crate-pax-chassis-ios-0.10.4 (c (n "pax-chassis-ios") (v "0.10.4") (d (list (d (n "pax-chassis-common") (r "^0.10.4") (d #t) (k 0)))) (h "1bhrf62d3s78pgmnwbz5qgz24nb7l5a2gi5n8h2v5q04nj0a63b9")))

(define-public crate-pax-chassis-ios-0.10.5 (c (n "pax-chassis-ios") (v "0.10.5") (d (list (d (n "pax-chassis-common") (r "^0.10.5") (d #t) (k 0)))) (h "0xcqbd9830iqsnx12m7qq6m4n69mddj4j5h635px124zkybawfjz")))

(define-public crate-pax-chassis-ios-0.10.6 (c (n "pax-chassis-ios") (v "0.10.6") (d (list (d (n "pax-chassis-common") (r "^0.10.6") (d #t) (k 0)))) (h "1nqlmbw1f4fa71nnnysjnzynfl43b7a4gwlvfxf99ds01w570c3y")))

(define-public crate-pax-chassis-ios-0.10.7 (c (n "pax-chassis-ios") (v "0.10.7") (d (list (d (n "pax-chassis-common") (r "^0.10.7") (d #t) (k 0)))) (h "1sm8sr6w4r26s050r0lwf5cfyj9x7m2gy6y8za4jz9hzw0wpxyvf")))

(define-public crate-pax-chassis-ios-0.10.9 (c (n "pax-chassis-ios") (v "0.10.9") (d (list (d (n "pax-chassis-common") (r "^0.10.9") (d #t) (k 0)))) (h "011abj07cwmla4scsb6qfx7lrw1sy1jnzsas0skp6ja3p9jx0ya4")))

(define-public crate-pax-chassis-ios-0.10.10 (c (n "pax-chassis-ios") (v "0.10.10") (d (list (d (n "pax-chassis-common") (r "^0.10.10") (d #t) (k 0)))) (h "063gh5xriq6j02rchlrlszhslc1bmw43dq7ph3lv0z4nfmigrf0r")))

(define-public crate-pax-chassis-ios-0.11.1 (c (n "pax-chassis-ios") (v "0.11.1") (d (list (d (n "pax-chassis-common") (r "^0.11.1") (d #t) (k 0)))) (h "0fs3c0c4qzfnmx1b1ir8w35sx43sc1c3m0gc8vf1wmkkgml3j5km")))

(define-public crate-pax-chassis-ios-0.11.3 (c (n "pax-chassis-ios") (v "0.11.3") (d (list (d (n "pax-chassis-common") (r "^0.11.3") (d #t) (k 0)))) (h "1vglz891aprs592l444bgd6nppdjs06b7g4vzmlcv86n35qflvjw")))

(define-public crate-pax-chassis-ios-0.11.5 (c (n "pax-chassis-ios") (v "0.11.5") (d (list (d (n "pax-chassis-common") (r "^0.11.5") (d #t) (k 0)))) (h "03dvk5xy8l5fpgbg5rv0qyxv0bp3l0ha8gm4qdpmpllf70ilwznc")))

(define-public crate-pax-chassis-ios-0.12.0 (c (n "pax-chassis-ios") (v "0.12.0") (d (list (d (n "pax-chassis-common") (r "^0.12.0") (d #t) (k 0)))) (h "198wdyi4rr5682hp8n0r62yq96sdimn3s196mpjdq7mrzsgf7nsl")))

(define-public crate-pax-chassis-ios-0.12.1 (c (n "pax-chassis-ios") (v "0.12.1") (d (list (d (n "pax-chassis-common") (r "^0.12.1") (d #t) (k 0)))) (h "1vr73gcg3517gwipbiyhgv8qf2452dwxnanm6vvhwzb4q717fm25")))

(define-public crate-pax-chassis-ios-0.12.2 (c (n "pax-chassis-ios") (v "0.12.2") (d (list (d (n "pax-chassis-common") (r "^0.12.2") (d #t) (k 0)))) (h "1mrv30iab960imjv53rls2znzxfwcbs9027jzwlmz4qgs2nx2x0m")))

(define-public crate-pax-chassis-ios-0.12.3 (c (n "pax-chassis-ios") (v "0.12.3") (d (list (d (n "pax-chassis-common") (r "^0.12.3") (d #t) (k 0)))) (h "04gqis20952ms4fw7mvz4kds8c87xdgspmxdn2i20dgmvmy4ch5l")))

(define-public crate-pax-chassis-ios-0.12.6 (c (n "pax-chassis-ios") (v "0.12.6") (d (list (d (n "pax-chassis-common") (r "^0.12.6") (d #t) (k 0)))) (h "09kxkkghj8b5dlz8y1ab22vd6pfa7sj9qhx26afsxd4j1h1al2af")))

(define-public crate-pax-chassis-ios-0.12.7 (c (n "pax-chassis-ios") (v "0.12.7") (d (list (d (n "pax-chassis-common") (r "^0.12.7") (d #t) (k 0)))) (h "03f73i4781lz4p93sdcmgi8gy0bcvi9xi6yaxbwhhljzin2j0f98")))

(define-public crate-pax-chassis-ios-0.12.8 (c (n "pax-chassis-ios") (v "0.12.8") (d (list (d (n "pax-chassis-common") (r "^0.12.8") (d #t) (k 0)))) (h "03ywkrb7sxjwxap2g2x35kwj762fr6ycw6ww6isn79r49gwsykq6")))

(define-public crate-pax-chassis-ios-0.13.1 (c (n "pax-chassis-ios") (v "0.13.1") (d (list (d (n "pax-chassis-common") (r "^0.13.1") (d #t) (k 0)))) (h "0s12ajbdi7m5l68kqy57csivi2rzi31p98rxlm85v536vazqw47r")))

(define-public crate-pax-chassis-ios-0.13.2 (c (n "pax-chassis-ios") (v "0.13.2") (d (list (d (n "pax-chassis-common") (r "^0.13.2") (d #t) (k 0)))) (h "0vjwk3zrrww46i4dpd3yq7dmcw3wmbd5ffkqdbq000flcadb0xhh")))

(define-public crate-pax-chassis-ios-0.13.6 (c (n "pax-chassis-ios") (v "0.13.6") (d (list (d (n "pax-chassis-common") (r "^0.13.6") (d #t) (k 0)))) (h "1bbdigi8dl4xcdl8lk5mk0n1ai0lrw1vdz6rlc63r7685rsmxyn6")))

(define-public crate-pax-chassis-ios-0.13.7 (c (n "pax-chassis-ios") (v "0.13.7") (d (list (d (n "pax-chassis-common") (r "^0.13.7") (d #t) (k 0)))) (h "1hyl3irk33s6jbx63j3jwaj7qywwv7yrb6nvmr3rz1pjg6209jfl")))

(define-public crate-pax-chassis-ios-0.13.8 (c (n "pax-chassis-ios") (v "0.13.8") (d (list (d (n "pax-chassis-common") (r "^0.13.8") (d #t) (k 0)))) (h "0iplmkwm9874z397p028xxzjf0zxh8n6y8sr8gw0igqs92jq1323")))

(define-public crate-pax-chassis-ios-0.13.10 (c (n "pax-chassis-ios") (v "0.13.10") (d (list (d (n "pax-chassis-common") (r "^0.13.10") (d #t) (k 0)))) (h "0kvhhc9cjik6gr71spm1vp0x2xzilmgypj038cj8zqksz0107bzc")))

(define-public crate-pax-chassis-ios-0.13.11 (c (n "pax-chassis-ios") (v "0.13.11") (d (list (d (n "pax-chassis-common") (r "^0.13.11") (d #t) (k 0)))) (h "08zdi14j29wvw0z1vikqih5njqinn160cbya9azzfi51l9hxzf9m")))

(define-public crate-pax-chassis-ios-0.13.12 (c (n "pax-chassis-ios") (v "0.13.12") (d (list (d (n "pax-chassis-common") (r "^0.13.12") (d #t) (k 0)))) (h "0r72g24016ri9jzylpw2vgxky8cka9r7hidvs2sn4wsr1r2pssda")))

(define-public crate-pax-chassis-ios-0.13.16 (c (n "pax-chassis-ios") (v "0.13.16") (d (list (d (n "pax-chassis-common") (r "^0.13.16") (d #t) (k 0)))) (h "1ipc84k3c5j9qvqm7vh38fcg93ngn2iim5xl86j2r0v53pmzirnv")))

(define-public crate-pax-chassis-ios-0.13.17 (c (n "pax-chassis-ios") (v "0.13.17") (d (list (d (n "pax-chassis-common") (r "^0.13.17") (d #t) (k 0)))) (h "1a3h1zjxvfx0hr5kg0ipaibd5a667r15mmmj1v6h760ikj8b72bx")))

(define-public crate-pax-chassis-ios-0.13.18 (c (n "pax-chassis-ios") (v "0.13.18") (d (list (d (n "pax-chassis-common") (r "^0.13.18") (d #t) (k 0)))) (h "0p4fk16whcz71p65f5xj69n7kwz5jf9b9w2r57a0sq0944pga79g")))

(define-public crate-pax-chassis-ios-0.13.19 (c (n "pax-chassis-ios") (v "0.13.19") (d (list (d (n "pax-chassis-common") (r "^0.13.19") (d #t) (k 0)))) (h "0ixi0bc8jbk63zgjivd7widw0lfija24k3n6brjwr24yaib24rn7")))

(define-public crate-pax-chassis-ios-0.13.21 (c (n "pax-chassis-ios") (v "0.13.21") (d (list (d (n "pax-chassis-common") (r "^0.13.21") (d #t) (k 0)))) (h "1asc2h2hxf3z5nd4z85xnhs3gc7gzmc77nsvhyzv7pj5p35nhagh")))

(define-public crate-pax-chassis-ios-0.13.22 (c (n "pax-chassis-ios") (v "0.13.22") (d (list (d (n "pax-chassis-common") (r "^0.13.22") (d #t) (k 0)))) (h "1mppnbmj6wpic1lzqzgg2v9zz6mkbg25l4adk62dqxyj5cx8pwx7")))

(define-public crate-pax-chassis-ios-0.14.1 (c (n "pax-chassis-ios") (v "0.14.1") (d (list (d (n "pax-chassis-common") (r "^0.14.1") (d #t) (k 0)))) (h "01qy7rk3pg7x33cwqhh9cby1y63zgrszc0v5wjxdpaalqpqv6qnf")))

(define-public crate-pax-chassis-ios-0.14.2 (c (n "pax-chassis-ios") (v "0.14.2") (d (list (d (n "pax-chassis-common") (r "^0.14.2") (d #t) (k 0)))) (h "13dbidh0wfb07lb784acxzxw46p3603y5qp1ry1lnnxg2baq3k7v")))

(define-public crate-pax-chassis-ios-0.14.3 (c (n "pax-chassis-ios") (v "0.14.3") (d (list (d (n "pax-chassis-common") (r "^0.14.3") (d #t) (k 0)))) (h "0a3lp0bqziq5mgsnylzqqpnlf582cplm15fa71fca5zgsdkm5k9f")))

(define-public crate-pax-chassis-ios-0.14.4 (c (n "pax-chassis-ios") (v "0.14.4") (d (list (d (n "pax-chassis-common") (r "^0.14.4") (d #t) (k 0)))) (h "1h446rp5sip68bian9br2pk622jnz6yl1hhpd8yg50kf82747q2z")))

(define-public crate-pax-chassis-ios-0.14.5 (c (n "pax-chassis-ios") (v "0.14.5") (d (list (d (n "pax-chassis-common") (r "^0.14.5") (d #t) (k 0)))) (h "1i29ds01s20v4zin4razgrpyv2hv1w6ypb992h3xs6jksa3qlsf7")))

(define-public crate-pax-chassis-ios-0.14.6 (c (n "pax-chassis-ios") (v "0.14.6") (d (list (d (n "pax-chassis-common") (r "^0.14.6") (d #t) (k 0)))) (h "1wngbxri6ykyy9nisvdi70v7wac0fm9acyl2ww8gcmwz4f3ynczm")))

(define-public crate-pax-chassis-ios-0.14.8 (c (n "pax-chassis-ios") (v "0.14.8") (d (list (d (n "pax-chassis-common") (r "^0.14.8") (d #t) (k 0)))) (h "1znaswa6jjbazs26x0r5d5wmsrvgcrnxrni7fw2nadiyjsrhajgh")))

(define-public crate-pax-chassis-ios-0.14.9 (c (n "pax-chassis-ios") (v "0.14.9") (d (list (d (n "pax-chassis-common") (r "^0.14.9") (d #t) (k 0)))) (h "02cmvlipxnjyc44hsifwf4dicqx3n14k4g0pahm0ap87ij72xbfv")))

(define-public crate-pax-chassis-ios-0.15.1 (c (n "pax-chassis-ios") (v "0.15.1") (d (list (d (n "pax-chassis-common") (r "^0.15.1") (d #t) (k 0)))) (h "1n9j6aai0r071hvk59s1adrvcnqzkmyq7bj213dzmjmvq2l95ni3")))

(define-public crate-pax-chassis-ios-0.15.2 (c (n "pax-chassis-ios") (v "0.15.2") (d (list (d (n "pax-chassis-common") (r "^0.15.2") (d #t) (k 0)))) (h "1mj51xvx77q17ww8hjl19mayyn9fb5ydc67fs1spyymjf2lscn3a")))

