(define-module (crates-io pa x- pax-properties-coproduct) #:use-module (crates-io))

(define-public crate-pax-properties-coproduct-0.0.1 (c (n "pax-properties-coproduct") (v "0.0.1") (d (list (d (n "pax-runtime-api") (r "^0.0.1") (d #t) (k 0)))) (h "1xa0yy4jikcrfa6xf143byryk2plm0ry310xl4xhbkkn1s0icbfw")))

(define-public crate-pax-properties-coproduct-0.5.8 (c (n "pax-properties-coproduct") (v "0.5.8") (d (list (d (n "pax-runtime-api") (r "^0.5.8") (d #t) (k 0)))) (h "0ljv1i4ms26zdnl52af7nrzlyb8jvj3g4paxhq3629wk7zr13sbl")))

(define-public crate-pax-properties-coproduct-0.5.9 (c (n "pax-properties-coproduct") (v "0.5.9") (d (list (d (n "pax-runtime-api") (r "^0.5.9") (d #t) (k 0)))) (h "0c7r5n9cpxi79520vn8shval1325yl29nff5i32b2ggw07kb6448")))

(define-public crate-pax-properties-coproduct-0.6.0 (c (n "pax-properties-coproduct") (v "0.6.0") (d (list (d (n "pax-runtime-api") (r "^0.6.0") (d #t) (k 0)))) (h "0dr2s99ahrk82jxdyc8x4fvwi9pdbw9jb097s0m5cb28jd0ajq21")))

(define-public crate-pax-properties-coproduct-0.6.1-dev (c (n "pax-properties-coproduct") (v "0.6.1-dev") (d (list (d (n "pax-runtime-api") (r "^0.6.1-dev") (d #t) (k 0)))) (h "0ij61gsqp4x9jkxyk63qnr8262ffxczg9i6s0jw3i0sfisalssl1")))

(define-public crate-pax-properties-coproduct-0.6.2 (c (n "pax-properties-coproduct") (v "0.6.2") (d (list (d (n "pax-runtime-api") (r "^0.6.2") (d #t) (k 0)))) (h "0mh44kjqf8pcvrxr6ksd9h705rqzis65xcmypkq9qa4y4k0rvwr6")))

(define-public crate-pax-properties-coproduct-0.6.3 (c (n "pax-properties-coproduct") (v "0.6.3") (d (list (d (n "pax-runtime-api") (r "^0.6.3") (d #t) (k 0)))) (h "0rs5wx1fmqsjf7zn122fg6vrg1hsiaab218akj21jk3qj5sd7mjn")))

(define-public crate-pax-properties-coproduct-0.6.4 (c (n "pax-properties-coproduct") (v "0.6.4") (d (list (d (n "pax-runtime-api") (r "^0.6.4") (d #t) (k 0)))) (h "0zslvkss5xxdh1i4wbvr8myggy4n7caq1cmmdkj1s4v1l348vdvf")))

(define-public crate-pax-properties-coproduct-0.6.5 (c (n "pax-properties-coproduct") (v "0.6.5") (d (list (d (n "pax-runtime-api") (r "^0.6.5") (d #t) (k 0)))) (h "0rm4nj4fcrqv6ngrxjq2b8sqkzkg42bi3536h8axvdq6xs8q2zvc")))

(define-public crate-pax-properties-coproduct-0.6.6 (c (n "pax-properties-coproduct") (v "0.6.6") (d (list (d (n "pax-runtime-api") (r "^0.6.6") (d #t) (k 0)))) (h "09rwrs143mb601yv2l7gvqd2w0gk8zh0s2rvwyvp1ldqn5vrq8zm")))

(define-public crate-pax-properties-coproduct-0.6.7 (c (n "pax-properties-coproduct") (v "0.6.7") (d (list (d (n "pax-runtime-api") (r "^0.6.7") (d #t) (k 0)))) (h "1dk6hdlc49wg9rcnxwy97fk9aajkl1xgm3y4hjj45cqramiaw104")))

(define-public crate-pax-properties-coproduct-0.6.8 (c (n "pax-properties-coproduct") (v "0.6.8") (d (list (d (n "pax-runtime-api") (r "^0.6.8") (d #t) (k 0)))) (h "0zb38az2lnh3ch6clx61dki2b5xxx7zhl0ask4rfbagc1a8ciy45")))

(define-public crate-pax-properties-coproduct-0.6.9 (c (n "pax-properties-coproduct") (v "0.6.9") (d (list (d (n "pax-runtime-api") (r "^0.6.9") (d #t) (k 0)))) (h "1dhfv7kayv0hgksa1q83abgwrsd657b8vlgbbn1b6q466ng13rkp")))

(define-public crate-pax-properties-coproduct-0.6.10 (c (n "pax-properties-coproduct") (v "0.6.10") (d (list (d (n "pax-runtime-api") (r "^0.6.10") (d #t) (k 0)))) (h "11d5d88ynv3ip52ncgbajh1dk05237wy4k01hxb84zd8l5fai3mx")))

(define-public crate-pax-properties-coproduct-0.6.11 (c (n "pax-properties-coproduct") (v "0.6.11") (d (list (d (n "pax-runtime-api") (r "^0.6.11") (d #t) (k 0)))) (h "1vs6yy7wrhvcb84s3y8sh0lwd1rzp8dnxbadkbcc60gjhx8x7s1s")))

(define-public crate-pax-properties-coproduct-0.6.12 (c (n "pax-properties-coproduct") (v "0.6.12") (d (list (d (n "pax-runtime-api") (r "^0.6.12") (d #t) (k 0)))) (h "0wq2avbbxwackxba25la5cwyjzwg6zpsqbiizgpylpn315g4ab2g")))

(define-public crate-pax-properties-coproduct-0.6.13 (c (n "pax-properties-coproduct") (v "0.6.13") (d (list (d (n "pax-runtime-api") (r "^0.6.13") (d #t) (k 0)))) (h "14cqxzsbr89fgshrdwrs57xrhv5hx8y6kgy2j3jrnyp07sgib6s3")))

(define-public crate-pax-properties-coproduct-0.6.16 (c (n "pax-properties-coproduct") (v "0.6.16") (d (list (d (n "pax-runtime-api") (r "^0.6.16") (d #t) (k 0)))) (h "0dxsg0ddgg9kzfkkd6dg5bqc973ar5xhadxp9j05yzb7bhq2zq6f")))

(define-public crate-pax-properties-coproduct-0.6.17 (c (n "pax-properties-coproduct") (v "0.6.17") (d (list (d (n "pax-runtime-api") (r "^0.6.17") (d #t) (k 0)))) (h "1ygza9w052pznar7xhl07bfr18d0d0lf0zgz0nhzg0s97nm1amv9")))

(define-public crate-pax-properties-coproduct-0.7.0 (c (n "pax-properties-coproduct") (v "0.7.0") (d (list (d (n "pax-runtime-api") (r "^0.7.0") (d #t) (k 0)))) (h "1y35dhiz268gny26cvlw3zxcs3bmlpbns3nljgah7fg5z00w0qjn")))

(define-public crate-pax-properties-coproduct-0.7.2 (c (n "pax-properties-coproduct") (v "0.7.2") (d (list (d (n "pax-runtime-api") (r "^0.7.2") (d #t) (k 0)))) (h "18199rrg936603qslkhh3kp9hama6w2lzhm98xwm8ksn7bjv53fl")))

(define-public crate-pax-properties-coproduct-0.7.3 (c (n "pax-properties-coproduct") (v "0.7.3") (d (list (d (n "pax-runtime-api") (r "^0.7.3") (d #t) (k 0)))) (h "0p3j3rhxg3hpsyrlq2w27qqasjgh069i179ywfpqa8y5kqqzf650")))

(define-public crate-pax-properties-coproduct-0.7.4 (c (n "pax-properties-coproduct") (v "0.7.4") (d (list (d (n "pax-runtime-api") (r "^0.7.4") (d #t) (k 0)))) (h "072jyvh64r6pzb0xyy0gily2bgikjigqcl994igyhcifwk21541v")))

(define-public crate-pax-properties-coproduct-0.8.0 (c (n "pax-properties-coproduct") (v "0.8.0") (d (list (d (n "pax-runtime-api") (r "^0.8.0") (d #t) (k 0)))) (h "0szzwyvb71xmg96sq3ffgw13igs9gzl2zslvic4g3pd969ph2k8z")))

(define-public crate-pax-properties-coproduct-0.8.5 (c (n "pax-properties-coproduct") (v "0.8.5") (d (list (d (n "pax-runtime-api") (r "^0.8.5") (d #t) (k 0)))) (h "1hfra96wk9n5lg9vqikhf23n22j8s19l078x84xyqpnck2ack4dn")))

(define-public crate-pax-properties-coproduct-0.8.6 (c (n "pax-properties-coproduct") (v "0.8.6") (d (list (d (n "pax-runtime-api") (r "^0.8.6") (d #t) (k 0)))) (h "15plpgy434yxb5r025an7k6mmbfkag3irkz4x1xs0rh5nzl0wrch")))

(define-public crate-pax-properties-coproduct-0.9.0 (c (n "pax-properties-coproduct") (v "0.9.0") (d (list (d (n "pax-runtime-api") (r "^0.9.0") (d #t) (k 0)))) (h "0w849r6s3zh6lw5nkgiq96z5w6mc70ixpcv49xxabsqg4wg6l8ry")))

(define-public crate-pax-properties-coproduct-0.9.1 (c (n "pax-properties-coproduct") (v "0.9.1") (d (list (d (n "pax-runtime-api") (r "^0.9.1") (d #t) (k 0)))) (h "1pmqi7f9wjz131q556307v4i55nlir2b8fmzhy8snpqmjn2q088j")))

(define-public crate-pax-properties-coproduct-0.9.2 (c (n "pax-properties-coproduct") (v "0.9.2") (d (list (d (n "pax-runtime-api") (r "^0.9.2") (d #t) (k 0)))) (h "1m593ks8986v6cpqnqdmz7pwmmf233yng7y2sacc4zfklkjqx1yc")))

(define-public crate-pax-properties-coproduct-0.9.3 (c (n "pax-properties-coproduct") (v "0.9.3") (d (list (d (n "pax-runtime-api") (r "^0.9.3") (d #t) (k 0)))) (h "1v3x8z9avbjnv098c3ixlx7k8wlkpk3clcm87qxbjya2kcvhcwmf")))

(define-public crate-pax-properties-coproduct-0.9.4 (c (n "pax-properties-coproduct") (v "0.9.4") (d (list (d (n "pax-runtime-api") (r "^0.9.4") (d #t) (k 0)))) (h "0kagn0nd04glybgj2rk1dylmwcjn9kgxqqfla6y4z7z7089yizn3")))

(define-public crate-pax-properties-coproduct-0.9.5 (c (n "pax-properties-coproduct") (v "0.9.5") (d (list (d (n "pax-runtime-api") (r "^0.9.5") (d #t) (k 0)))) (h "0gil10q9aash31wbpj8wbbnfl8lprqlqrij1d7dg2hlr71qlc1ai")))

(define-public crate-pax-properties-coproduct-0.9.6 (c (n "pax-properties-coproduct") (v "0.9.6") (d (list (d (n "pax-runtime-api") (r "^0.9.6") (d #t) (k 0)))) (h "062n8psa4x0bi6zs03ax9di8di3kq9bd77ciscf5xcw0gnwd65q8")))

(define-public crate-pax-properties-coproduct-0.9.7 (c (n "pax-properties-coproduct") (v "0.9.7") (d (list (d (n "pax-runtime-api") (r "^0.9.7") (d #t) (k 0)))) (h "0zv0g2gws32r9pvxcgd5f3836qf4hh4g4hxpmyw3l2aygkahaksl")))

(define-public crate-pax-properties-coproduct-0.9.8 (c (n "pax-properties-coproduct") (v "0.9.8") (d (list (d (n "pax-runtime-api") (r "^0.9.8") (d #t) (k 0)))) (h "0abx73szds6xh84hdgs72kwbivs9m9v8nrb79qsc5fs8a95kmy7s")))

(define-public crate-pax-properties-coproduct-0.9.9 (c (n "pax-properties-coproduct") (v "0.9.9") (d (list (d (n "pax-runtime-api") (r "^0.9.9") (d #t) (k 0)))) (h "1lp02x47k51q5pql2h3y462hj0yaz92gja6kjd5k7ab6nj31k250")))

(define-public crate-pax-properties-coproduct-0.10.0 (c (n "pax-properties-coproduct") (v "0.10.0") (d (list (d (n "pax-runtime-api") (r "^0.10.0") (d #t) (k 0)))) (h "0zsx5l106chd675vc3jv1xcpsnbd6bwcq18b2qqh4fcdr14809cy")))

(define-public crate-pax-properties-coproduct-0.10.1 (c (n "pax-properties-coproduct") (v "0.10.1") (d (list (d (n "pax-runtime-api") (r "^0.10.1") (d #t) (k 0)))) (h "11dmqw90bc1lynfv0ln17dm9wmi6n88q457mdfglpk5gwn29niml")))

(define-public crate-pax-properties-coproduct-0.10.2 (c (n "pax-properties-coproduct") (v "0.10.2") (d (list (d (n "pax-runtime-api") (r "^0.10.2") (d #t) (k 0)))) (h "1xwip6h3v2cfhyf0acp0h5x6lgfa2hgx0lhhajm1yc7kxarmcih2")))

(define-public crate-pax-properties-coproduct-0.10.3 (c (n "pax-properties-coproduct") (v "0.10.3") (d (list (d (n "pax-runtime-api") (r "^0.10.3") (d #t) (k 0)))) (h "0abffgm8f1m8v077v665dvpj6fwh8gks73jfchsvpg2jwa92iahl")))

(define-public crate-pax-properties-coproduct-0.10.4 (c (n "pax-properties-coproduct") (v "0.10.4") (d (list (d (n "pax-runtime-api") (r "^0.10.4") (d #t) (k 0)))) (h "093c0z51pbhn2375bqij8ym5ppi4qnjdg4lmz53397my6wjzkpdw")))

(define-public crate-pax-properties-coproduct-0.9.10 (c (n "pax-properties-coproduct") (v "0.9.10") (d (list (d (n "pax-runtime-api") (r "^0.9.10") (d #t) (k 0)))) (h "0rlkaa51avvwf34mv4fsgpb8b8z787a5yrl75b7zy8p8xj210yhf") (y #t)))

(define-public crate-pax-properties-coproduct-0.9.11 (c (n "pax-properties-coproduct") (v "0.9.11") (d (list (d (n "pax-runtime-api") (r "^0.9.11") (d #t) (k 0)))) (h "0xd39d4k1032rr0bdmqr1pjafr8mybid3y1k02s94nwlaqw61kjk") (y #t)))

(define-public crate-pax-properties-coproduct-0.10.5 (c (n "pax-properties-coproduct") (v "0.10.5") (d (list (d (n "pax-runtime-api") (r "^0.10.5") (d #t) (k 0)))) (h "1dl5hnvag20lcw5qpapq3yg0ch8sjqns900y5w5phgsl95d2nhic")))

(define-public crate-pax-properties-coproduct-0.10.6 (c (n "pax-properties-coproduct") (v "0.10.6") (d (list (d (n "pax-runtime-api") (r "^0.10.6") (d #t) (k 0)))) (h "0dnavnk29r58abahvng9952w4h7kk70b7kh9prf7ccbyvkf5cyhs")))

(define-public crate-pax-properties-coproduct-0.10.7 (c (n "pax-properties-coproduct") (v "0.10.7") (d (list (d (n "pax-runtime-api") (r "^0.10.7") (d #t) (k 0)))) (h "1b2hkvs9lzm8bg47rqz7jip4bbv8gd7mnxb7ay0bs4dyl2745mz5")))

(define-public crate-pax-properties-coproduct-0.10.8 (c (n "pax-properties-coproduct") (v "0.10.8") (d (list (d (n "pax-runtime-api") (r "^0.10.8") (d #t) (k 0)))) (h "026nyasb0a0w1070j891qnpf4c7zymfsg8l0qlg0yslsdkpf97r4")))

(define-public crate-pax-properties-coproduct-0.10.9 (c (n "pax-properties-coproduct") (v "0.10.9") (d (list (d (n "pax-runtime-api") (r "^0.10.9") (d #t) (k 0)))) (h "1parg4d3kqpsy35pvzar680rpdw4dsx9j8w7r13zrk09spg7silx")))

(define-public crate-pax-properties-coproduct-0.10.10 (c (n "pax-properties-coproduct") (v "0.10.10") (d (list (d (n "pax-runtime-api") (r "^0.10.10") (d #t) (k 0)))) (h "1cmgyydzfzh1gcpwl58f18kdhqvl5gr3y8s0gnvxyhz6vrwr1y92")))

