(define-module (crates-io pa x- pax-pixels) #:use-module (crates-io))

(define-public crate-pax-pixels-0.10.7 (c (n "pax-pixels") (v "0.10.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "lyon") (r "^1.0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "12n0spdgpbxfqhh0sv8rvgh7ihz00fwpsy4wgg4q7plz5p9b8wdi")))

