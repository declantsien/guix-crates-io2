(define-module (crates-io pa x- pax-lang) #:use-module (crates-io))

(define-public crate-pax-lang-0.0.0 (c (n "pax-lang") (v "0.0.0") (h "1cmgxqxj40zc7q793wzkkspilnpjawrk60gna3gwpz0jcmm6fbxx")))

(define-public crate-pax-lang-0.0.1 (c (n "pax-lang") (v "0.0.1") (d (list (d (n "pax-compiler") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "pax-message") (r "^0.0.1") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.0.1") (f (quote ("parser"))) (d #t) (k 0)))) (h "0bd1a3grbfmrkrrl894zbp5603bld1qmdf9lkrmdnbzmcjmi132p") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.5.8 (c (n "pax-lang") (v "0.5.8") (d (list (d (n "pax-compiler") (r "^0.5.8") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.5.8") (d #t) (k 0)) (d (n "pax-message") (r "^0.5.8") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.5.8") (d #t) (k 0)))) (h "09914c6964chh67mbsbld0ngy4pahn1jv5qx33fih9xxjdd60630") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.5.9 (c (n "pax-lang") (v "0.5.9") (d (list (d (n "pax-compiler") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.5.9") (d #t) (k 0)) (d (n "pax-message") (r "^0.5.9") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.5.9") (d #t) (k 0)))) (h "05qg9pziqpqmkklnh05k5qdadf7s4337iys1dn0l3fhxw04mr84n") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.0 (c (n "pax-lang") (v "0.6.0") (d (list (d (n "pax-compiler") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.0") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.0") (d #t) (k 0)))) (h "0hc18b0qxiv398ldfmi3c7nhnyaqvid6rl95d0bsirc3psp6kdgv") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.1-dev (c (n "pax-lang") (v "0.6.1-dev") (d (list (d (n "pax-compiler") (r "^0.6.1-dev") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.1-dev") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.1-dev") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.1-dev") (d #t) (k 0)))) (h "1hncbyc0qs5g20cvw7xv9g1qfdg80chgw9aj8g86645qar4vdc2s") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.2 (c (n "pax-lang") (v "0.6.2") (d (list (d (n "pax-compiler") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.2") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.2") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.2") (d #t) (k 0)))) (h "1cd47bihxnx8h7gqz6sm8sdsbx2yin074hjvnjkdbzqk9dhcjica") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.3 (c (n "pax-lang") (v "0.6.3") (d (list (d (n "pax-compiler") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.3") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.3") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.3") (d #t) (k 0)))) (h "1f9i6vpq6xl8nlq0vvawm6al9hzpml2gms3lhp30f1ws6cyl41g6") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.4 (c (n "pax-lang") (v "0.6.4") (d (list (d (n "pax-compiler") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.4") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.4") (d #t) (k 0)))) (h "09yp3q6bbi0m5ig03fa1m90xjjb0690wbc791d4krg9cbh6vsi63") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.5 (c (n "pax-lang") (v "0.6.5") (d (list (d (n "pax-compiler") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.5") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.5") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.5") (d #t) (k 0)))) (h "145nqvb836a6blc9mjhqyqrfcmif0zipfwizsmqikdfbbpvyswpx") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.6 (c (n "pax-lang") (v "0.6.6") (d (list (d (n "pax-compiler") (r "^0.6.6") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.6") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.6") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.6") (d #t) (k 0)))) (h "13szwhd5r9zyi4bmiasz1jsvspxk8wvhxh5b6b9c0lmpy4kfh736") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.7 (c (n "pax-lang") (v "0.6.7") (d (list (d (n "pax-compiler") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.7") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.7") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.7") (d #t) (k 0)))) (h "15ai497ywaa85yp80vnr3ay7i3h6bg4p0bwrjjybgdrr2pyx2wvh") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.8 (c (n "pax-lang") (v "0.6.8") (d (list (d (n "pax-compiler") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.8") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.8") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.8") (d #t) (k 0)))) (h "1bql4hq6w4yyrc1y90jcsw82kd50sbrgk43kjv71klj7wg2cw7yv") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.9 (c (n "pax-lang") (v "0.6.9") (d (list (d (n "pax-compiler") (r "^0.6.9") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.9") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.9") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.9") (d #t) (k 0)))) (h "00px90j9l4c1hg54hjqbppc94gif2bzgckylnqyfkdqghisl9k5s") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.10 (c (n "pax-lang") (v "0.6.10") (d (list (d (n "pax-compiler") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.10") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.10") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.10") (d #t) (k 0)))) (h "0frgkr2y8f5zrarm34v5brixcmspy2d99alnxrg2m01rm3xdq0pq") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.11 (c (n "pax-lang") (v "0.6.11") (d (list (d (n "pax-compiler") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.11") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.11") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.11") (d #t) (k 0)))) (h "08knw7fgr3gq8d3gzi6myvsdxyhmcfd1r2fqpb3wmzd65x8limd1") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.12 (c (n "pax-lang") (v "0.6.12") (d (list (d (n "pax-compiler") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.12") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.12") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.12") (d #t) (k 0)))) (h "16clsf1z5rf0hw3sg2k81nnkm8hplfx65slhhwjkp4n6y4jyfimy") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.13 (c (n "pax-lang") (v "0.6.13") (d (list (d (n "pax-compiler") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.13") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.13") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.13") (d #t) (k 0)))) (h "0cwqly9rxnf8vz8hmz5k3vmdp7qlmsb61fdk1wdh5c9ha4yh06sk") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.14 (c (n "pax-lang") (v "0.6.14") (d (list (d (n "pax-compiler") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.14") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.14") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.14") (d #t) (k 0)))) (h "0rp624cz1ljfdyvh9gbf6fj578nyzb9agw3csckf1p2pi2nqiv4z") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.16 (c (n "pax-lang") (v "0.6.16") (d (list (d (n "pax-compiler") (r "^0.6.16") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.16") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.16") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.16") (d #t) (k 0)))) (h "0xaaqwhmck1zpwikdr882m92c1bha455687z32s4c56yd977vqs2") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.6.17 (c (n "pax-lang") (v "0.6.17") (d (list (d (n "pax-compiler") (r "^0.6.17") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.6.17") (d #t) (k 0)) (d (n "pax-message") (r "^0.6.17") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.6.17") (d #t) (k 0)))) (h "0fxpsjimnbpwscxms3bb2zvnr63hspblkxvmbsny263i2h3c4vbw") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.7.0 (c (n "pax-lang") (v "0.7.0") (d (list (d (n "pax-compiler") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "pax-message") (r "^0.7.0") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.7.0") (d #t) (k 0)))) (h "070wrim7dsqck5222qyp83cw82qi4hspja174d8l31whap46hdrf") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.7.2 (c (n "pax-lang") (v "0.7.2") (d (list (d (n "pax-compiler") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.7.2") (d #t) (k 0)) (d (n "pax-message") (r "^0.7.2") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.7.2") (d #t) (k 0)))) (h "1cdr0bg4plvamvmrwc59abc5ydikkagwyajkh8w82b6ap359ysql") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.7.3 (c (n "pax-lang") (v "0.7.3") (d (list (d (n "pax-compiler") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.7.3") (d #t) (k 0)) (d (n "pax-message") (r "^0.7.3") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.7.3") (d #t) (k 0)))) (h "100qxkm61xnq0zbi4q0pp03hbbnwp2rck6yaywmkrkh7z1xkgkkj") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.7.4 (c (n "pax-lang") (v "0.7.4") (d (list (d (n "pax-compiler") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.7.4") (d #t) (k 0)) (d (n "pax-message") (r "^0.7.4") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.7.4") (d #t) (k 0)))) (h "0lxgxavd24g9fna2sia45hzavfbcprlls9l10hvyljw555w74ivv") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.8.0 (c (n "pax-lang") (v "0.8.0") (d (list (d (n "pax-compiler") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "pax-message") (r "^0.8.0") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.8.0") (d #t) (k 0)))) (h "1gyfsblf88c9j6a2pagaxzfdwd8wlyknhx983k91n4k89kvmn0zc") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.8.5 (c (n "pax-lang") (v "0.8.5") (d (list (d (n "pax-compiler") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.8.5") (d #t) (k 0)) (d (n "pax-message") (r "^0.8.5") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.8.5") (d #t) (k 0)))) (h "0zka0fad595cvzdzvbqhpm632sblij2wvmq2xywgh9r0rq1xhjwb") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.8.6 (c (n "pax-lang") (v "0.8.6") (d (list (d (n "pax-compiler") (r "^0.8.6") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.8.6") (d #t) (k 0)) (d (n "pax-message") (r "^0.8.6") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.8.6") (d #t) (k 0)))) (h "04dyck873d512m8w3kh8kwpd6p4zz8iylzdih1ygmlz36gpr5c6g") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.0 (c (n "pax-lang") (v "0.9.0") (d (list (d (n "pax-compiler") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.0") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.0") (d #t) (k 0)))) (h "1bl2rnbw86w813ai1axiy26nssf62s76kld0qwin7hk4j2wsacv7") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.1 (c (n "pax-lang") (v "0.9.1") (d (list (d (n "pax-compiler") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.1") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.1") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.1") (d #t) (k 0)))) (h "1hf2lvc0zhql164jkax86byig0gh5b1vx0j96zxk69bnshia618d") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.2 (c (n "pax-lang") (v "0.9.2") (d (list (d (n "pax-compiler") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.2") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.2") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.2") (d #t) (k 0)))) (h "1y0fmp69bvbrvy06x6s3wl2ns3ijhik91flxb8sbb8cgxkhr0ndp") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.3 (c (n "pax-lang") (v "0.9.3") (d (list (d (n "pax-compiler") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.3") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.3") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.3") (d #t) (k 0)))) (h "0szg2bg728hypyf6h5qvf945wfvj3w9ysszj8834pf5dvghm2py0") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.4 (c (n "pax-lang") (v "0.9.4") (d (list (d (n "pax-compiler") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.4") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.4") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.4") (d #t) (k 0)))) (h "0h4n8irg40p6rs96840zxrcj9yw6x78nbl3fh5mn8x01qgrrmg2y") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.5 (c (n "pax-lang") (v "0.9.5") (d (list (d (n "pax-compiler") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.5") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.5") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.5") (d #t) (k 0)))) (h "0qfwlf9qcdbbmy5mpask7zw3kp51874hy64ixhxaia37w9c4lc71") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.6 (c (n "pax-lang") (v "0.9.6") (d (list (d (n "pax-compiler") (r "^0.9.6") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.6") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.6") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.6") (d #t) (k 0)))) (h "0xmi1w4kckwhszhmmb68fhjhm9p5i8dc7y8k8289w94ypzlff968") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.7 (c (n "pax-lang") (v "0.9.7") (d (list (d (n "pax-compiler") (r "^0.9.7") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.7") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.7") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.7") (d #t) (k 0)))) (h "02ni23x0ns0h3kyvlpq9hqjx7404rfr7m0nrch1z94iipapnfngv") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.8 (c (n "pax-lang") (v "0.9.8") (d (list (d (n "pax-compiler") (r "^0.9.8") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.8") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.8") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.8") (d #t) (k 0)))) (h "029lf223wr67990729s0cg8iq3awkzydnj0v71bcvqxif318aqab") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.9 (c (n "pax-lang") (v "0.9.9") (d (list (d (n "pax-compiler") (r "^0.9.9") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.9") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.9") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.9") (d #t) (k 0)))) (h "1dkrxy6y7d55dh1cjdsxz3riq5haw9x4fadrnfjy8cdr0pid6di8") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.0 (c (n "pax-lang") (v "0.10.0") (d (list (d (n "pax-compiler") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.0") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.0") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.0") (d #t) (k 0)))) (h "1j62nyz9s5nmiz7mhqv1clmvcvqciqqh42y7ih1yaw059mdvhq16") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.1 (c (n "pax-lang") (v "0.10.1") (d (list (d (n "pax-compiler") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.1") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.1") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.1") (d #t) (k 0)))) (h "0jj624v137hh4mkgdy4wb3zi8siq3qyrvs9n09a99r6p4mxqnvwn") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.2 (c (n "pax-lang") (v "0.10.2") (d (list (d (n "pax-compiler") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.2") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.2") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.2") (d #t) (k 0)))) (h "0mw0gal7gav0xcdwb8gq1cpz2lmkjygz4ifnwxb0j3zic7g7cdyh") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.3 (c (n "pax-lang") (v "0.10.3") (d (list (d (n "pax-compiler") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.3") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.3") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.3") (d #t) (k 0)))) (h "0980xvxwsfzh3dqx631vh89vqnpkbx08bsjr3ccvx5bhwdzx0800") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.4 (c (n "pax-lang") (v "0.10.4") (d (list (d (n "pax-compiler") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.4") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.4") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.4") (d #t) (k 0)))) (h "012hrbipvxgyi24r6vv861z95lppn8kslpiyg37mlfjf3qvrkg4v") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.10 (c (n "pax-lang") (v "0.9.10") (d (list (d (n "pax-compiler") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.10") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.10") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.10") (d #t) (k 0)))) (h "1nsqqbxxbn28l9rjnihrn1xvdzaxkk8zwd5ix5pmnq091jhx951z") (y #t) (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.9.11 (c (n "pax-lang") (v "0.9.11") (d (list (d (n "pax-compiler") (r "^0.9.11") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.9.11") (d #t) (k 0)) (d (n "pax-message") (r "^0.9.11") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.9.11") (d #t) (k 0)))) (h "1xl3c1yxrgyqdanpfm9cbdkdh4mqa7zfznysnzp03xfbwxmqc5b1") (y #t) (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.5 (c (n "pax-lang") (v "0.10.5") (d (list (d (n "pax-compiler") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.5") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.5") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.5") (d #t) (k 0)))) (h "1yw6kzkr51lr7avmjh30g435sah1xsk2dkd99x57h408p1z7qsii") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.6 (c (n "pax-lang") (v "0.10.6") (d (list (d (n "pax-compiler") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.6") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.6") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.6") (d #t) (k 0)))) (h "1887yzrlvrjlvvl8vxvgcn7w6qs350r1zbx75fyclfgmf3jcglfb") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.7 (c (n "pax-lang") (v "0.10.7") (d (list (d (n "pax-compiler") (r "^0.10.7") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.7") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.7") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.7") (d #t) (k 0)))) (h "103bnhx94n785yhwbqmyc0pz374x7q0cxf8y8qv0nblwg8yx7kpk") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.8 (c (n "pax-lang") (v "0.10.8") (d (list (d (n "pax-compiler") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.8") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.8") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.8") (d #t) (k 0)))) (h "07ihrqa0b79g8i36vp0hhixmk12pqgdyv91fsidwbkvybmhzwhr6") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.9 (c (n "pax-lang") (v "0.10.9") (d (list (d (n "pax-compiler") (r "^0.10.9") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.9") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.9") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.9") (d #t) (k 0)))) (h "1npzqhyg1a3b69gq7mjdi4458npbyy0dn19laf9bnq1qslnd1v25") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.10.10 (c (n "pax-lang") (v "0.10.10") (d (list (d (n "pax-compiler") (r "^0.10.10") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.10.10") (d #t) (k 0)) (d (n "pax-message") (r "^0.10.10") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.10.10") (d #t) (k 0)))) (h "06lg0izrg1432i4phhab3z9057ybsvwf9lw36ffg7509d0xfsf34") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.11.1 (c (n "pax-lang") (v "0.11.1") (d (list (d (n "pax-compiler") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.11.1") (d #t) (k 0)) (d (n "pax-message") (r "^0.11.1") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.11.1") (d #t) (k 0)))) (h "09j2b0ggwyg5ir89ld2hwkfyr9h69gnvjvxw7kgg18pxxhjd2vp4") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.11.3 (c (n "pax-lang") (v "0.11.3") (d (list (d (n "pax-compiler") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.11.3") (d #t) (k 0)) (d (n "pax-message") (r "^0.11.3") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.11.3") (d #t) (k 0)))) (h "0yr5zxpf0k96i0xyh9m1icdvyf1hhh5j50q5kzmxq0ln5j5lkqyp") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.11.5 (c (n "pax-lang") (v "0.11.5") (d (list (d (n "pax-compiler") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "pax-macro") (r "^0.11.5") (d #t) (k 0)) (d (n "pax-message") (r "^0.11.5") (d #t) (k 0)) (d (n "pax-runtime-api") (r "^0.11.5") (d #t) (k 0)))) (h "0y0igvjmaca3rg3rh8capcmjv4p3rnbw0592b2w1whsvwx8d407w") (s 2) (e (quote (("parser" "dep:pax-compiler"))))))

(define-public crate-pax-lang-0.13.21 (c (n "pax-lang") (v "0.13.21") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "090qf1djaabc82bzzjlxs2vjr0gr7kpgvxpxq83i9ql2kwz79gxs")))

(define-public crate-pax-lang-0.13.22 (c (n "pax-lang") (v "0.13.22") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1z553qmdnimp0pm0ybp8z3ah7qrv1lbrp9j7bp3zxcmvqpjafl5b")))

(define-public crate-pax-lang-0.14.4 (c (n "pax-lang") (v "0.14.4") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "0p41gwybjkfg9jz4gvqz7scnmqbgcfy5jdpy1qdgc8fbbxr5n483")))

(define-public crate-pax-lang-0.14.5 (c (n "pax-lang") (v "0.14.5") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "1rj5lwgmrhw8za3fp7iwzzy4bkcxxj1wi7yx8rd084whbklakqr8")))

(define-public crate-pax-lang-0.14.6 (c (n "pax-lang") (v "0.14.6") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "1vd2hi33k24d1b2hi7pfdgnafsm76b6ip5zmnypjzgygzywshw38")))

(define-public crate-pax-lang-0.14.7 (c (n "pax-lang") (v "0.14.7") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "1krkp1if0xb1grnnnd43n9lnqhcjgbhyn1blmgmnvhjdgdmlw59b")))

(define-public crate-pax-lang-0.14.8 (c (n "pax-lang") (v "0.14.8") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "0nwn0wwhfxlizislii8xn85s1vq1fwsmx2gyj3xp0xp8jxbayg3q")))

(define-public crate-pax-lang-0.14.9 (c (n "pax-lang") (v "0.14.9") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "0cn4pq1mzza41n12y17x5f7psgf42xzd3b29v2vyl7lnrrpwyggw")))

(define-public crate-pax-lang-0.15.1 (c (n "pax-lang") (v "0.15.1") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "0c2gqic1m8235jrjycw3zydmryw2844sb64nyssjhyyfxvkdsbkn")))

(define-public crate-pax-lang-0.15.2 (c (n "pax-lang") (v "0.15.2") (d (list (d (n "pest") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (f (quote ("std"))) (d #t) (k 0)))) (h "0r2mfxvyhydfil31rfpbw0zldnm897fmnhk8lfav5qjrs2sdwpvb")))

