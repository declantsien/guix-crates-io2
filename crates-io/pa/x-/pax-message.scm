(define-module (crates-io pa x- pax-message) #:use-module (crates-io))

(define-public crate-pax-message-0.0.1 (c (n "pax-message") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "081x25c6fd5g96sdfqpsxam1884hj2mq6kc7sv662b2banzrcslx")))

(define-public crate-pax-message-0.5.8 (c (n "pax-message") (v "0.5.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1622s2r2gdf0igzlq2dsj1rvv6jmbwf416k20byfpgkd52r95qqr")))

(define-public crate-pax-message-0.5.9 (c (n "pax-message") (v "0.5.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1rh8l4jq1szmjzhvabi4pwnyb2x1gyh6z6zmp241dchxi0gw17ds")))

(define-public crate-pax-message-0.6.0 (c (n "pax-message") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0hyla7cq3s6kjbxs5gqs0hkyb14drpf8lyr68gwxxwp4hhwvlh2n")))

(define-public crate-pax-message-0.6.1-dev (c (n "pax-message") (v "0.6.1-dev") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "152gkc6lc0fdcql33a7lqyglkqfnmj208qnknxyrqknn8wnjxqkz")))

(define-public crate-pax-message-0.6.2 (c (n "pax-message") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1ah2f66niaif4mjjlix0xfb2wi6x078m2ad0aaqrg74m9r1bf7h4")))

(define-public crate-pax-message-0.6.3 (c (n "pax-message") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0c62rf7gm8pgph5srp7iwr87kwqfn3s07s0wy3fzbrwjja3w2qxc")))

(define-public crate-pax-message-0.6.4 (c (n "pax-message") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1a8mrbxh2g196884qp19haxjqj0n6mxd4d0vm2glkmxadhk6i88x")))

(define-public crate-pax-message-0.6.5 (c (n "pax-message") (v "0.6.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "00hywqwsf12wrn8iandr3r1wz0vs5hayp3s4msyvvlxx7bn8m39l")))

(define-public crate-pax-message-0.6.6 (c (n "pax-message") (v "0.6.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "05yy1f71kj98r80nx985m9ccn0hd52wxclsfrl7fv1gwryhbf521")))

(define-public crate-pax-message-0.6.7 (c (n "pax-message") (v "0.6.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0m4vj9y6350mh48pgjfj89khxbf91f58gm1ar7d5kvmf1lm8adzj")))

(define-public crate-pax-message-0.6.8 (c (n "pax-message") (v "0.6.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "02rnvwnwxqlk6lnsdkyh2y3hwyhvl6lxs9hfm31c2wpwzb6k48qp")))

(define-public crate-pax-message-0.6.9 (c (n "pax-message") (v "0.6.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "027jdyljl026qfwgiz1mkjbrsha239x2j1q24m16lwrsm5f14hw0")))

(define-public crate-pax-message-0.6.10 (c (n "pax-message") (v "0.6.10") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "14iihan5fac2gh0lgqgikrpl19r9pfcz1p9ydv18b6gafr3i8c0w")))

(define-public crate-pax-message-0.6.11 (c (n "pax-message") (v "0.6.11") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "060rnxxszlk72jiz8g5qils8y9hv1ycv0qlxprr2r76ja22m5shx")))

(define-public crate-pax-message-0.6.12 (c (n "pax-message") (v "0.6.12") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1yz4vr5ykl79jm4a57z6wf0syhyc8i64pcbrqlzj0yrbakvh6ww4")))

(define-public crate-pax-message-0.6.13 (c (n "pax-message") (v "0.6.13") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "112cni3dlbdpj3h2gxzkcmj5vdhx0hvw5j25w6wwwrkm9gvqwimj")))

(define-public crate-pax-message-0.6.16 (c (n "pax-message") (v "0.6.16") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "15zagdkd1yk6kmq28m2g4ygxqrz7zp4x1bab4y39zcq538c2299i")))

(define-public crate-pax-message-0.6.17 (c (n "pax-message") (v "0.6.17") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1xhccf8mxbwjxcwx8wyg1vnpnp4lmihll78x5j7b87xkppy42car")))

(define-public crate-pax-message-0.7.0 (c (n "pax-message") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1d9kwvzxskx9gzbx5s5mlgldl5mwzjmh3ydjy7qx37mry5ch6nzm")))

(define-public crate-pax-message-0.7.2 (c (n "pax-message") (v "0.7.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "19yfypdy4aj3dv2ki3jr1kvw58qgwla173fwnddgricqhnm62z42")))

(define-public crate-pax-message-0.7.3 (c (n "pax-message") (v "0.7.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1yk12rii22xq25ia02nfdq9dvlmpdijh86v9rbksnpllwypv6hyx")))

(define-public crate-pax-message-0.7.4 (c (n "pax-message") (v "0.7.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1sdd85p59qnrvglmzrjw890qm9icqzrq0hm67m79s3kcfcza04ym")))

(define-public crate-pax-message-0.8.0 (c (n "pax-message") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1q12yc3fbqzhva32f17lij5ycvl920q4nqdf8sxlx8kf5haz6i1b")))

(define-public crate-pax-message-0.8.5 (c (n "pax-message") (v "0.8.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "174gx938l5iaa0gzd6gj7l3xrnccfz2fr47pda0d7mzxvzqkxym8")))

(define-public crate-pax-message-0.8.6 (c (n "pax-message") (v "0.8.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0a0ywj2xfp47ljj7bdq8iiy726fdpl9frh7m5b4w9qwv25i904p7")))

(define-public crate-pax-message-0.9.0 (c (n "pax-message") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17mdpb9j1wmglkb2rdbhf1lagvr8qwnxl65l81szjsk9qssw1lwz")))

(define-public crate-pax-message-0.9.1 (c (n "pax-message") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1lp2fc4d32rjb6albajqc00xcsc29kaw9xd0072dzz5jgq9xlkgd")))

(define-public crate-pax-message-0.9.2 (c (n "pax-message") (v "0.9.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0iyl74si7zihixbg9p2n6gbv909b6jywd5gfjafa5h7142r485fx")))

(define-public crate-pax-message-0.9.3 (c (n "pax-message") (v "0.9.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0vcaqk9kvshafdyck8fycwl4jj0ciddq22n6m8n9vhk1349nq4zd")))

(define-public crate-pax-message-0.9.4 (c (n "pax-message") (v "0.9.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1bcyyk5kd07wmjp1dfbbxwi4aijvmncqvsfxj4zbdwbda1ylgnd1")))

(define-public crate-pax-message-0.9.5 (c (n "pax-message") (v "0.9.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0r868h3agjpd92lfrfgmzz78wg5ng0dpvn0jc7lqbsw095hba5gj")))

(define-public crate-pax-message-0.9.6 (c (n "pax-message") (v "0.9.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0mvnpfh5fh4i8b3rkkqq97wjmlkr5i4fw6rfwcbclc2gck4dpgji")))

(define-public crate-pax-message-0.9.7 (c (n "pax-message") (v "0.9.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0jrdja2imsrnzyp1fmlaapxf6x3zw4gr2z19k13hjw4a40x9832l")))

(define-public crate-pax-message-0.9.8 (c (n "pax-message") (v "0.9.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1lfpian49a208kqkywq8r43562mm3j9iks7ky81lzw754g23zhvm")))

(define-public crate-pax-message-0.9.9 (c (n "pax-message") (v "0.9.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0angiscapn1z19mckjx8brn9fp5s51hm45qkx6hpcjlg1qsqlvx7")))

(define-public crate-pax-message-0.10.0 (c (n "pax-message") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1l8brl0dnh05mxg4gcxj702yiw9qzdhinisc5cs29k5xlsl9ny7z")))

(define-public crate-pax-message-0.10.1 (c (n "pax-message") (v "0.10.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0yrfx548ldqnqgj8yjhkngmkp3v6fgs5nbk8filvqg6br092n5ci")))

(define-public crate-pax-message-0.10.2 (c (n "pax-message") (v "0.10.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "05s3qz6n3mfrf72w18xpkjp9y6gr86kwq08wbqvkqrvsyw6byv5i")))

(define-public crate-pax-message-0.10.3 (c (n "pax-message") (v "0.10.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "15ngz9g2gdiw0nym563bc0yprf19456qvqdvlx9fnij3fapyi692")))

(define-public crate-pax-message-0.10.4 (c (n "pax-message") (v "0.10.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1lwjandkdbhypz9yqglj5b5sna9wm6zcrxrvrqgbmwlp8cszc90k")))

(define-public crate-pax-message-0.9.10 (c (n "pax-message") (v "0.9.10") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17i5c8irai84hjgbqhz79lml2ibk1ci0w0g08g3sqvklhvrfwyj1") (y #t)))

(define-public crate-pax-message-0.9.11 (c (n "pax-message") (v "0.9.11") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1b71zx2mzy478g3lqdfli8jip4ah8lnwm7f3h8whh6zjl7lz8va7") (y #t)))

(define-public crate-pax-message-0.10.5 (c (n "pax-message") (v "0.10.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1vdqhygw9igdms6aq1hjx6rl4098psksn28q2gwz10gm0z1yl0wr")))

(define-public crate-pax-message-0.10.6 (c (n "pax-message") (v "0.10.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0c9gzd8pbn7s7w9z781827rmij4q2dx1g14l8yh920gqimy41n4f")))

(define-public crate-pax-message-0.10.7 (c (n "pax-message") (v "0.10.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0z5ggg71p4im092ghvanp15bd2n9962rfkqnwzxck3rqdxm4lr3q")))

(define-public crate-pax-message-0.10.8 (c (n "pax-message") (v "0.10.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0s49gs0fpdk2jzl4n4hvz3m8hax95br7jccbwwd9gmdf86m9kjvr")))

(define-public crate-pax-message-0.10.9 (c (n "pax-message") (v "0.10.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1m352whzilwr2c5sg8y0zkswmb4pyxs6gfzn5wdxhdxgzz4xrnyc")))

(define-public crate-pax-message-0.10.10 (c (n "pax-message") (v "0.10.10") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "114fv1bccfx0111gal0wm9b9c7232b311xdq8z3acarvs3hgaf92")))

(define-public crate-pax-message-0.11.0 (c (n "pax-message") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "19a9zprwv1j3rq82icn4i13x80v1ab6vrhndybr2ix1z3s57c405")))

(define-public crate-pax-message-0.11.1 (c (n "pax-message") (v "0.11.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "10x0af75j97pwn0jk0s9wbw5bmfi5rxb9dy04d7r29w36cq4niyr")))

(define-public crate-pax-message-0.11.2 (c (n "pax-message") (v "0.11.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0r9qszy679iwjidq9zab4smqy1fxcf9fg6ln2wv96w1jb8nc9xj1")))

(define-public crate-pax-message-0.11.3 (c (n "pax-message") (v "0.11.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0bqnrraw6b4r854vkjg8lbf0i1671a0wbh1j07wl1ncafa357a1d")))

(define-public crate-pax-message-0.11.4 (c (n "pax-message") (v "0.11.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1h8c7npajxgmj3cfi5b6ix97fcqvxyq768padgx3ndm65y43v7c0")))

(define-public crate-pax-message-0.11.5 (c (n "pax-message") (v "0.11.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "03g79dxvch5m6ykzh65f5aqrrfib2xhc7nq6d171sdw1r0igpm7x")))

(define-public crate-pax-message-0.12.0 (c (n "pax-message") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "02g63bdgj2s9lcm1zrpasqbq0v06hdfs2a7x0arl8gcq0mz0d3zy")))

(define-public crate-pax-message-0.12.1 (c (n "pax-message") (v "0.12.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0nris1sncdn70ry5q7ca1pb1lp1vkhivp1rdy7yww5p8ljr8kzp7")))

(define-public crate-pax-message-0.12.2 (c (n "pax-message") (v "0.12.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0j3xl3a4pnvfrwnzd7z9i03cs5s3j3w41hk6b38yfc9iagf091cj")))

(define-public crate-pax-message-0.12.3 (c (n "pax-message") (v "0.12.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1rjcz57lzz7wwnmhyl9mx8a3b4s1xf77a7m41bl88kaxnhjycfbx")))

(define-public crate-pax-message-0.12.4 (c (n "pax-message") (v "0.12.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0l38jm1mw3fnbkadszpvd1iiw4f7agv5avm5mfw5p8l00bpg272v")))

(define-public crate-pax-message-0.12.5 (c (n "pax-message") (v "0.12.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "133wa2aq961sld97kzxn279gw8lwywfklhpwvq73gpb89hq15jh6")))

(define-public crate-pax-message-0.12.6 (c (n "pax-message") (v "0.12.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17px2h2zm8p0jn90raihz63a4jfhqfcila35s4pn1vb5sabwpvqj")))

(define-public crate-pax-message-0.12.7 (c (n "pax-message") (v "0.12.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "065xk6jjfmzfpfbw7fzvdaw5sh6sdd0vkz8f7yla46xfr8d85x5g")))

(define-public crate-pax-message-0.12.8 (c (n "pax-message") (v "0.12.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1lbm2rpqpwblrwmjqc4b9vwy8fj17k73x4al4m306zbxdyy9zg86")))

(define-public crate-pax-message-0.12.9 (c (n "pax-message") (v "0.12.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0gkszlszggpyqmrdf52f9yn6cqw1xkc4kwgzs8adjf95q5gdqy96")))

(define-public crate-pax-message-0.13.0 (c (n "pax-message") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1nxbsa0i16irc3s0ymfh660pqpx9gz8v5v216hfqw41wv7ms862h")))

(define-public crate-pax-message-0.13.1 (c (n "pax-message") (v "0.13.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0ws3450rbv6jfqpcl24g95wc15qgzjfcx544zhms02f2qgxcwl2k")))

(define-public crate-pax-message-0.13.2 (c (n "pax-message") (v "0.13.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0nmbxad1kzl5bjgz22chz1v9qcphb4cb9jnlppz4rnipjq1fgzyq")))

(define-public crate-pax-message-0.13.3 (c (n "pax-message") (v "0.13.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0l07xbfzvz7vgnbx15jfrbbiih922n311n72kbfs8pa7dsykgy5c")))

(define-public crate-pax-message-0.13.4 (c (n "pax-message") (v "0.13.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1f5igimv4cqriv7fd7yxwrwdyr51kyns2nbj6awv4db44p22if47")))

(define-public crate-pax-message-0.13.5 (c (n "pax-message") (v "0.13.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1rc4871a21fzng6xzn24aj8bqbr8s029ca3q1rxd3zj9wzkfy5wc")))

(define-public crate-pax-message-0.13.6 (c (n "pax-message") (v "0.13.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0mci3a6n2bil4c3bni8i2p8sxzksssacfdfvpxvhh52d2ikv5ram")))

(define-public crate-pax-message-0.13.7 (c (n "pax-message") (v "0.13.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1z9r471sirh4nk4ipi4r72awgkg729vnqgg1bqhv248xlm9f04vz")))

(define-public crate-pax-message-0.13.8 (c (n "pax-message") (v "0.13.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1b3crw6lnln4zl9rv3rd3k2pan6h8fda9m6791f1m32q7s6v7gsh")))

(define-public crate-pax-message-0.13.9 (c (n "pax-message") (v "0.13.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "11j0sxanfx0w5pwa2pd1i7kkj2q6d3y2c3bsm3rza4fa0jjjpcwn")))

(define-public crate-pax-message-0.13.10 (c (n "pax-message") (v "0.13.10") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "10mrqsmg5jqdm8bfrd33dhv3l6jsljnf9i4hb5vy1lqdm9a8ls4z")))

(define-public crate-pax-message-0.13.11 (c (n "pax-message") (v "0.13.11") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0bc5g84vprx7kfwcyyhamdk6kd2qwvrhmr51pvbvk5mymw3412rl")))

(define-public crate-pax-message-0.13.12 (c (n "pax-message") (v "0.13.12") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0dcd87nr1sdilvi8jd3nn4whbkv16gn97w6lm79sdx2j4pxgn2cg")))

(define-public crate-pax-message-0.13.15 (c (n "pax-message") (v "0.13.15") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1hc2sl22rbx9b8ws17ma9s03m6bwl8ll5jzl8060cl5n2zramyi1")))

(define-public crate-pax-message-0.13.16 (c (n "pax-message") (v "0.13.16") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1lsaq6ifwhx3sbqdyzl2ypmvfrqhvrla2b8qy5x7g2k2lwhg2im5")))

(define-public crate-pax-message-0.13.17 (c (n "pax-message") (v "0.13.17") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "04shhgn98cjdlngl772ljmazi0311vwzcasvcv9pvmfkc7hyq0fa")))

(define-public crate-pax-message-0.13.18 (c (n "pax-message") (v "0.13.18") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0j6afff7qyvghy9lp8az915xh4fy7dq5h364fwi22anmvr4iq9px")))

(define-public crate-pax-message-0.13.19 (c (n "pax-message") (v "0.13.19") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "15zgnx18xbnx1xchc2qv9ki39262vbrb2l9r89ka4w9j2p72xq7y")))

(define-public crate-pax-message-0.13.20 (c (n "pax-message") (v "0.13.20") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "00rs6h51p5kaiqaj9wrf1vkdgn2v98734i68zldxmwvzpsk465jm")))

(define-public crate-pax-message-0.13.21 (c (n "pax-message") (v "0.13.21") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "163zyyzz7yr1nnk6svnyxk4xr46vlvwyrk50drwhc5vppnhaqdjj")))

(define-public crate-pax-message-0.13.22 (c (n "pax-message") (v "0.13.22") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "03mwyj8q41nf34k7gsw9vkpiwbqv80bzdlmwc4nagpj22ljwv54q")))

(define-public crate-pax-message-0.14.0 (c (n "pax-message") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "16ag2agarxi28mag4j5wwvi60n9ah6ca0d2b1z1c8ld862x6v8yl")))

(define-public crate-pax-message-0.14.1 (c (n "pax-message") (v "0.14.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1mkacngr0rp9v97jrbqa263ivhxhgzh046x8wa1ngb810f3ck81n")))

(define-public crate-pax-message-0.14.2 (c (n "pax-message") (v "0.14.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0sxmkwb6i8wk75ma2ca8hqragrhpx6vxyz3sc0idm03vqvqrrl0p")))

(define-public crate-pax-message-0.14.3 (c (n "pax-message") (v "0.14.3") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0vi37big19xhnl7ahhpm3lj6yick238jhxrgq6mx5r3jy0qjdvcf")))

(define-public crate-pax-message-0.14.4 (c (n "pax-message") (v "0.14.4") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17qwzssmi9w3bwkw1wv9ylkb0hy8bfjz5wb711h6sklc02b9jviv")))

(define-public crate-pax-message-0.14.5 (c (n "pax-message") (v "0.14.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1ch7xl0vyx78kwb5qhsa1kgp11qnivggic6c9as1dfmb63z1n9dy")))

(define-public crate-pax-message-0.14.6 (c (n "pax-message") (v "0.14.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "139bhl8z0mjs7h34w57g30pj543j280zwk2fyxbdqz1p79d0ynqm")))

(define-public crate-pax-message-0.14.7 (c (n "pax-message") (v "0.14.7") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17l5c70jdiil6l8vqsq4a50hnq93n2diiy075maghnqy73k6gksl")))

(define-public crate-pax-message-0.14.8 (c (n "pax-message") (v "0.14.8") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0sjhypjb80nppaa11yz75m592d9mvg329x466wxz0p6dg48y7096")))

(define-public crate-pax-message-0.14.9 (c (n "pax-message") (v "0.14.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "01wnby5qvwmc963y5gh36gm7f39jnrrj26jn702nbkpinx1ignqx")))

(define-public crate-pax-message-0.15.0 (c (n "pax-message") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0b3gw0bpwpvz1h3ww8zk6gf3jd9wkx8z8w6nqggjfm3i3m6c3xqh")))

(define-public crate-pax-message-0.15.1 (c (n "pax-message") (v "0.15.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "04919xjw63fz9iz8yyxvsyfx79xydd6w8p6d9hyhi991mf1600a4")))

(define-public crate-pax-message-0.15.2 (c (n "pax-message") (v "0.15.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1xmy0fkzxgrmgk4an4fy3zb93n947bf7aizqilv291xd9xvfl6bc")))

