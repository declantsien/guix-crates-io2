(define-module (crates-io pa ms pamsm) #:use-module (crates-io))

(define-public crate-pamsm-0.1.0 (c (n "pamsm") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0iczf9fmy8zqqgmmpncm1wh803srw4mvrhw7hi2ydsgl6gxnfvf1")))

(define-public crate-pamsm-0.2.0 (c (n "pamsm") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "03687yi5mamsl8p3vwnlka9pb6cfqfdk410cv8ahl83ligb27nan")))

(define-public crate-pamsm-0.3.1-alpha.0 (c (n "pamsm") (v "0.3.1-alpha.0") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "03fwa2722bzp7ap21z0y4kcwax466h5nacdjpinahv7ar7caffbh") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.3.2 (c (n "pamsm") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "0r48d23jacq01bk0bjda1cik4n30f2xyazv0rzqzq1gwngal9z84") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.3.3 (c (n "pamsm") (v "0.3.3") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "01z3cqilm22npirgpnfk6dx6ncfjfqvygsf9fq33bjlmpq2fjxq4") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.3.4 (c (n "pamsm") (v "0.3.4") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "00msj83idp6cxl3l0xpyz5csfq22cic0n91a2m3v3wj72hca6hqn") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.4.0 (c (n "pamsm") (v "0.4.0") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "1p5a5nqhz2h0pam5hq7lv9h16irlwavc4lv9fi5nc1pyjxn8c1rm") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.4.1 (c (n "pamsm") (v "0.4.1") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "1pa3kvhqdhg918fnkis0ka6vq1sbpy532crjb3dp8p07pqpfv01m") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.4.2 (c (n "pamsm") (v "0.4.2") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "0sga9brxszyf52hg2bwil0japard7aqisiw23x5rfjhwxpnam853") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.4.3 (c (n "pamsm") (v "0.4.3") (d (list (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "16xc0kjpqdqcvzha16hrm5l8jqz7zxsl0nhgg8cjp7ih3374gr5l") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.5.0 (c (n "pamsm") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "09302mc1mgjdgqrcz3ddssyv672mx4j1278q2b7krjbmcipyn8l9") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.5.1 (c (n "pamsm") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "07pilh1qw2d6w3cm1sivf0pyl6vw2j5gwrl4f1fgxvhccdv0q74a") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.5.2 (c (n "pamsm") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "0imdraklnw3avm8xxcp63jr3bwgxdjdcsbiky4lp1bynhbzf5mv2") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.5.3 (c (n "pamsm") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "1vwrllyhmkqhq82dscdhffkjcgaq092jqi3chkf55fffmqnsjd98") (f (quote (("libpam"))))))

(define-public crate-pamsm-0.5.4 (c (n "pamsm") (v "0.5.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "02207ssjrjknv69agvwcyv7xpgy871p1ndhgjsn46rlpj159z84y") (f (quote (("libpam"))))))

