(define-module (crates-io pa vo pavo-traits) #:use-module (crates-io))

(define-public crate-pavo-traits-0.1.0 (c (n "pavo-traits") (v "0.1.0") (h "0kdgqhnqcpa7cpwsn1a2x8rp0ag0hckwgyida0frxi24xjx6zkk4")))

(define-public crate-pavo-traits-0.1.1 (c (n "pavo-traits") (v "0.1.1") (h "1vjvp3xrdlg47kjvxgfkpkm8lq5bn9ibinqgg1yhphki1y4id6r8")))

(define-public crate-pavo-traits-0.1.2 (c (n "pavo-traits") (v "0.1.2") (h "0y44mvnwyz9pf845lkx150bawcffvwvz5frgm943804a4fssfvyv")))

(define-public crate-pavo-traits-0.1.3 (c (n "pavo-traits") (v "0.1.3") (h "1v13x6m605ry1cypg0kf5a9b9kx1dc65a5c2cnr599l97rcphvx7")))

(define-public crate-pavo-traits-0.1.4 (c (n "pavo-traits") (v "0.1.4") (h "0112mnw79fq4jvbvy2cd786vy78ymcw8dyclrhsasl0ax6asib23")))

(define-public crate-pavo-traits-0.2.0 (c (n "pavo-traits") (v "0.2.0") (h "0nspwd4gamq4mmxdq5182ca803ja7dn4jqjvlbp2xqd8cmdnkwg0")))

(define-public crate-pavo-traits-0.3.0 (c (n "pavo-traits") (v "0.3.0") (h "18inc29pzhrc7g4a6afa6ann6f35vw6im6akifnaakxlwxjz9q9f")))

