(define-module (crates-io pa vo pavo-common) #:use-module (crates-io))

(define-public crate-pavo-common-0.1.0 (c (n "pavo-common") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "1q42323xa4717qi737xbfj4archf5fsjj91pwqr8kcbxphnfdf75")))

(define-public crate-pavo-common-0.1.1 (c (n "pavo-common") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "041b9p761agrhazlknbnmpvz5pjj75hw6la1kvkna1dkl763cnks")))

