(define-module (crates-io pa nf panfix) #:use-module (crates-io))

(define-public crate-panfix-0.4.0 (c (n "panfix") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gi0n3lzj8cl2zgs5w2ysa9db9r5j71abn9wcz1q51c2l7vns01v") (f (quote (("debug_mode"))))))

