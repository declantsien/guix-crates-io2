(define-module (crates-io pa rv parvati_derive) #:use-module (crates-io))

(define-public crate-parvati_derive-1.0.0 (c (n "parvati_derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "parvati") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vd1qfak52a7zs7jiiag1mwv66v0ja82s5h7937in95ix148z4i5")))

(define-public crate-parvati_derive-1.0.1 (c (n "parvati_derive") (v "1.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "parvati") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gp1hb95kpfxcsm4np0n1v5p23mh5y8d82baszcq7x0icqmvpc4i")))

(define-public crate-parvati_derive-1.0.2 (c (n "parvati_derive") (v "1.0.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "parvati") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ym60nshb7fbyj7784dvp05hzlnw4iis8salpy1p4k1gmq1dd711")))

