(define-module (crates-io pa c1 pac194x) #:use-module (crates-io))

(define-public crate-pac194x-0.1.0 (c (n "pac194x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "register_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1q842k240mj8ps8gjm9r98khgg5h2yg5n3r5j4a39byhgy9wpnjs")))

(define-public crate-pac194x-0.1.1 (c (n "pac194x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "register_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1gh97anbaa3ps4isc46xby5h2ikqm88pxqdvfd5fhg5fkhsdg1iz")))

(define-public crate-pac194x-0.1.3 (c (n "pac194x") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "register_derive") (r "^0.1.0") (d #t) (k 0)))) (h "04063szwjkmjl010jwaz8lwash48b855wkfrymmz1pr96pwzjs44")))

