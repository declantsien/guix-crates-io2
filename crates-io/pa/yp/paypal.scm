(define-module (crates-io pa yp paypal) #:use-module (crates-io))

(define-public crate-paypal-0.0.0 (c (n "paypal") (v "0.0.0") (h "1xad6k0s6b5n8is4mg5bw8qz00b3x07y9w3vwrrmf094ic370rsd")))

(define-public crate-paypal-0.1.0 (c (n "paypal") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "06g12lcadvlgx9y7iz6zw7fj44w9jwgw2hnvfqv9b0dnk45w7s1z")))

(define-public crate-paypal-0.2.0 (c (n "paypal") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1s61aqr3s7x4i6hd1s069xbasxq91v2hq4m6sy0dxdzrdv1s1xb5") (f (quote (("test-mode") ("default"))))))

