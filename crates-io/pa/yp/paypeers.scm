(define-module (crates-io pa yp paypeers) #:use-module (crates-io))

(define-public crate-payPeers-0.1.0 (c (n "payPeers") (v "0.1.0") (h "1ryrr87k7gwas1rdq0qj2f8fjza16027w7f5f86pahdjxnpjcwif")))

(define-public crate-payPeers-0.1.1 (c (n "payPeers") (v "0.1.1") (h "0gn52mljd2l3ma3qnj0ph8czd69zlzh8a84hvyjdjvjqbb2gl2ll")))

