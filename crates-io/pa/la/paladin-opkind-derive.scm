(define-module (crates-io pa la paladin-opkind-derive) #:use-module (crates-io))

(define-public crate-paladin-opkind-derive-0.1.0 (c (n "paladin-opkind-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0nri4kavckw6fs05ajmdda81kfa9c3x98lgmk6g864v6vhbmknsj")))

(define-public crate-paladin-opkind-derive-0.1.1 (c (n "paladin-opkind-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1xcpybbs2l5x5g0n1ji09kbyw54bj1w2r0az3kb1g4xivdc5pqn5")))

(define-public crate-paladin-opkind-derive-0.2.0 (c (n "paladin-opkind-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "11mp1jqq45cs493qwlrfksvgxb69yhvc30y9gxs4yx65rl9ahbb9")))

(define-public crate-paladin-opkind-derive-0.3.0 (c (n "paladin-opkind-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0svswwcx3f31wywc0lxbi9r7yx8a2w5shi54pzi9sqbc6fyqmrha")))

(define-public crate-paladin-opkind-derive-0.3.1 (c (n "paladin-opkind-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0apfyzqk3y2xkdx808ncb6sxjba1b9m0s46dn7n6misqmvri70xl")))

(define-public crate-paladin-opkind-derive-0.4.0 (c (n "paladin-opkind-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0v8mr7xqvr46r1p2928q7cd9rrf8pcgv5xdlmryf900mz791wh2i")))

(define-public crate-paladin-opkind-derive-0.4.1 (c (n "paladin-opkind-derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1jawbv26zfjhbbf3k4wbfxzpsy74g4p4wsg8psdfj33w1fqxq9dg")))

