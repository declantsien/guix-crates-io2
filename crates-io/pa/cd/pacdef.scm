(define-module (crates-io pa cd pacdef) #:use-module (crates-io))

(define-public crate-pacdef-1.0.0-beta7 (c (n "pacdef") (v "1.0.0-beta7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "^1.0.0-beta7") (d #t) (k 0)))) (h "1xwzsvib9v7ll4cwqqjjw1bglankfrgq78ba5hbnq2bd0kh53xhj") (y #t)))

(define-public crate-pacdef-1.0.0-beta9 (c (n "pacdef") (v "1.0.0-beta9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "^1.0.0-beta9") (d #t) (k 0)))) (h "15vbmjcfgmdbf6kgh4mgffla7svmz2svi6q3kpva1qxpsfwh67si") (y #t)))

(define-public crate-pacdef-1.0.0-beta.11 (c (n "pacdef") (v "1.0.0-beta.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.11") (d #t) (k 0)))) (h "022v4clax5g25in4fnss0a33xhbw1iw76q9az2y8ayqma6l7l2z3")))

(define-public crate-pacdef-1.0.0-beta.12 (c (n "pacdef") (v "1.0.0-beta.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.12") (d #t) (k 0)))) (h "1xpcksfvnnkk1wsycvlghhbm38jsgnx9n2ri525qwlzj7pzn833r")))

(define-public crate-pacdef-1.0.0-beta.13 (c (n "pacdef") (v "1.0.0-beta.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.13") (d #t) (k 0)))) (h "11dq74czbrf51nf8hrs9bqqnw093kc2lim3hkhp83gq5p412yd02")))

(define-public crate-pacdef-1.0.0-beta.14 (c (n "pacdef") (v "1.0.0-beta.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.14") (d #t) (k 0)))) (h "110q6xwaw8qrwapm73lb7nb0bqdiz0v8grj4b700rqj1q7s6ilj0")))

(define-public crate-pacdef-1.0.0-beta.15 (c (n "pacdef") (v "1.0.0-beta.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.15") (d #t) (k 0)))) (h "0a7j30xsymvn1bkxg6i9x5f6b5x6z1cf78vy6hzs6wm8k0b1491v")))

(define-public crate-pacdef-1.0.0-beta.16 (c (n "pacdef") (v "1.0.0-beta.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.16") (d #t) (k 0)))) (h "1fw59lpp53j48n8zlzdpqi0ldaq6jishj3czh9arg8i080yfi6g3") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch"))))))

(define-public crate-pacdef-1.0.0-beta.18 (c (n "pacdef") (v "1.0.0-beta.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0-beta.18") (d #t) (k 0)))) (h "1w2ipw9khqlihjzhqdrqdj53gsywfk0jnbadd0icbqalicfrkg65") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.0.0 (c (n "pacdef") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.0") (d #t) (k 0)))) (h "0bi7qbqfn48gfzbxh3h09z8hpcrda37my10vgrghwhxdxk5nzglc") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.0.1 (c (n "pacdef") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.0.1") (d #t) (k 0)))) (h "0bm9jsix4j52pl90jdfrw0a97a5zqhxx0hvhbb0cq61hyc92wly8") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.1.0 (c (n "pacdef") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.1.0") (d #t) (k 0)))) (h "13av1ga1i0vrl8jiys50pb25dwl8wg19hxjhm2qzf8bj9grvg2lq") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.1.1-beta.1 (c (n "pacdef") (v "1.1.1-beta.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.1.1-beta.1") (d #t) (k 0)))) (h "0rv9ablzbci7c3j6i3hy27jvy2184wm0fq9p6hly3fkiswf6bdv8") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.2.0 (c (n "pacdef") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.2.0") (d #t) (k 0)))) (h "1zvg2gkrwcmrzq1zlnwm8zy3dh5qix416w0gab5y2vgcww53g7is") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.2.1 (c (n "pacdef") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.2.1") (d #t) (k 0)))) (h "1a9vplvz1x5jb77iv7bfp5iyl1lf4888rmaa8gpa6rv9qcmdv7xl") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.2.2 (c (n "pacdef") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.2.2") (d #t) (k 0)))) (h "0z412fz6r6mqbaf37npzyczca21pbj9rzfgys7qfvx0pyr9x3p3n") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.3.0 (c (n "pacdef") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.3.0") (d #t) (k 0)))) (h "187964vvjc9jb6f5xjkqyzy35y88xd7nsfi5i5a16c4d8s5wwfh8") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.65")))

(define-public crate-pacdef-1.3.1 (c (n "pacdef") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.3.1") (d #t) (k 0)))) (h "0csmhgvmbx3p982jiqzrdbwlsh8r4ybxgbsvpl1p74pbgfa0rzdr") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.70")))

(define-public crate-pacdef-1.3.2 (c (n "pacdef") (v "1.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.3.2") (d #t) (k 0)))) (h "1sf5i6xs39kp296grimcsya4fcc9cgzisgkck2shy32pzd2ps2c3") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.70")))

(define-public crate-pacdef-1.4.0 (c (n "pacdef") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.4.0") (d #t) (k 0)))) (h "1x6d2c129sbp62mzyjp5cpp2frf28yya2jpvhhavzj17r12mxar2") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.70")))

(define-public crate-pacdef-1.4.1 (c (n "pacdef") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.4.1") (d #t) (k 0)))) (h "11xcb47xi4ppk7ndljcj17rzkjngr8ma3g4hykjlbrdja20wwk61") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.70")))

(define-public crate-pacdef-1.4.2 (c (n "pacdef") (v "1.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.4.2") (d #t) (k 0)))) (h "1x882dccy0zlnh47p3zfs8v8yqq95yk0zmsdv0blswi7kaj84ba0") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.70")))

(define-public crate-pacdef-1.5.0 (c (n "pacdef") (v "1.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.5.0") (d #t) (k 0)))) (h "169bdics3q45am93qlqfx3l4dp7by12q6ryhfga03h3qaxqcwllh") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.74")))

(define-public crate-pacdef-1.6.0 (c (n "pacdef") (v "1.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pacdef_core") (r "=1.6.0") (d #t) (k 0)))) (h "1swk03rwggasgs5q9s6lhrgb4xjhmh6yf4nvwm472x5jn4wxf6wi") (f (quote (("default") ("debian" "pacdef_core/debian") ("arch" "pacdef_core/arch")))) (r "1.74")))

