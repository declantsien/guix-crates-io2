(define-module (crates-io pa cd pacdef_macros) #:use-module (crates-io))

(define-public crate-pacdef_macros-0.1.1 (c (n "pacdef_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13pn7rjlfx1a01ia3py18lf18m4h95843vjv1kfi3l4wwf38607p")))

(define-public crate-pacdef_macros-1.0.0 (c (n "pacdef_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xr8j2a3jbhv2281wsgi482jxbf3bhppfx0gs5krfy6v9ji333yv")))

(define-public crate-pacdef_macros-1.0.1 (c (n "pacdef_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18jz9i2723s4myd9j3gm5qjm5xyinz5pxh24lgk50nm1ac4blmka")))

