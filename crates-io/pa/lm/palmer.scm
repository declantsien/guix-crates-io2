(define-module (crates-io pa lm palmer) #:use-module (crates-io))

(define-public crate-palmer-0.1.0 (c (n "palmer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.45") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0x97j7sgfdqcqwq0ry4j7ngabkx4l3m3qy8dqgy4mamgf4x72lpx")))

(define-public crate-palmer-0.1.1 (c (n "palmer") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.45") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "15l3252hq6w3hyg17ziwh53cvpw94lkr57wxsdmzdmg39h1a3m4b")))

