(define-module (crates-io pa lm palmdb) #:use-module (crates-io))

(define-public crate-palmdb-0.1.0 (c (n "palmdb") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.148") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "nom-test-helpers") (r "^3.0.0") (f (quote ("verbose-errors"))) (d #t) (k 2)))) (h "1lvbrbbwr3z3d1zgg11h4fkiij2wi5i0n9h262gg4m21q7zwb28k") (f (quote (("dev" "clippy") ("default"))))))

