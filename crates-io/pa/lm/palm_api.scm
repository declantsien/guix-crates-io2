(define-module (crates-io pa lm palm_api) #:use-module (crates-io))

(define-public crate-palm_api-0.1.0 (c (n "palm_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0anib5zsrxvmmdcqsg79zjd949s53v3ak15d8mnly2npdm1snvm5")))

(define-public crate-palm_api-0.1.1 (c (n "palm_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "16r7bw6jjzyqdph7ny7jxvnlsaf019p7gnd27r3kh96lrmbrlp6q")))

(define-public crate-palm_api-0.2.0 (c (n "palm_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0nlrkl253si90vcl3k05bcmgxh2fwnfadizkif3vk2nkycn420kj") (y #t)))

(define-public crate-palm_api-0.2.1 (c (n "palm_api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1pmvz6nmc2d51adz8hg79n9j6lillsszkzk2jqwn2laqnxb2b7i0")))

