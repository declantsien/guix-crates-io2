(define-module (crates-io pa lm palm2-sdk) #:use-module (crates-io))

(define-public crate-palm2-sdk-0.1.0 (c (n "palm2-sdk") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "11cjqhk8s446mygnxwd2y5y06yw6jmxyp84wx369admngqvzgv22")))

