(define-module (crates-io pa ku paku) #:use-module (crates-io))

(define-public crate-paku-0.0.1 (c (n "paku") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1zdljcc2nd3hrsp303j9dfq9jxkpk17c8aaf8gm66gf392sj6510")))

(define-public crate-paku-0.0.2 (c (n "paku") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "031vllhbmgmi339mn80rxbh9ljj5xj96j80wfs11gq6fd6zq54j6")))

