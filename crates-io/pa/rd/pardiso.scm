(define-module (crates-io pa rd pardiso) #:use-module (crates-io))

(define-public crate-pardiso-0.1.0 (c (n "pardiso") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 2)) (d (n "ndarray-linalg") (r "^0.12") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "pardiso-src") (r "^0.1") (d #t) (k 2)) (d (n "pardiso-sys") (r "^0.2") (d #t) (k 0)))) (h "0y9dxcxc9ncsv0s9kw9myv9n8xqby8skyzjyk6bvk9rfbqp9q2gc")))

