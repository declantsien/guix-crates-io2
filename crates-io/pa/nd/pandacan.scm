(define-module (crates-io pa nd pandacan) #:use-module (crates-io))

(define-public crate-pandacan-0.1.0 (c (n "pandacan") (v "0.1.0") (d (list (d (n "binascii") (r "^0.1.4") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 2)))) (h "0djaldnjknbc35zqpzkmbssizpyw82azh3ndr4vizmd495k7d58s")))

(define-public crate-pandacan-0.1.1 (c (n "pandacan") (v "0.1.1") (d (list (d (n "binascii") (r "^0.1.4") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 2)))) (h "0wjgv8p0hyhc4k8vw60wq6d6i888dbp8bh13izyf8knb786bd6br")))

