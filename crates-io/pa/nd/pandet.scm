(define-module (crates-io pa nd pandet) #:use-module (crates-io))

(define-public crate-pandet-0.1.0 (c (n "pandet") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "039zxsiy1fk86l158h2ff5gdcgm4h85gaagrmgdaykxsxdyi2110") (y #t)))

(define-public crate-pandet-0.1.1 (c (n "pandet") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1lc52gv7l6b6ssxfrs065xrhispbrl9ssqvnn2v233r0i6l7z1nz") (y #t)))

(define-public crate-pandet-0.1.2 (c (n "pandet") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "05vs0rfw08cxna1zb51x86zzl93i1dislcmrbb87km2pchdf2xhy") (y #t)))

(define-public crate-pandet-0.2.0 (c (n "pandet") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0faysmrcdy1rlxj18ikbgk854052cyl5yjhsbiq2bn6wyglghp5h") (y #t)))

(define-public crate-pandet-0.2.1 (c (n "pandet") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0rbqqrfpk7s6qrd5p9rbkfc80skgpmivsh4vxa3g56sdpnvjb4ma") (y #t)))

(define-public crate-pandet-0.2.2 (c (n "pandet") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vrg076cslds270y4jsd3kh1266q4sr07n0ss6m3v6fd6933zm9s")))

(define-public crate-pandet-0.3.0 (c (n "pandet") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1pi4x3pxbdb288dzq50xqh43lp5pqfsajwsqg388m0jsfdasdpmw")))

(define-public crate-pandet-0.4.0 (c (n "pandet") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0f3wa7ls09n7nd2l44y4dljzc3dasla159zxxnnlmvkmj4qyg6wp")))

