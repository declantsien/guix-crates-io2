(define-module (crates-io pa nd pandora) #:use-module (crates-io))

(define-public crate-pandora-0.1.0 (c (n "pandora") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.14") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.2") (d #t) (k 0)))) (h "0kjy5621n6vs8dny3jslnyyxwkxif8a4ihh26d4ivi9xnc9ybs91")))

(define-public crate-pandora-0.1.1 (c (n "pandora") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.14") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.2") (d #t) (k 0)))) (h "1idg8fx9hxqhcx6qb4d2h5x3g2rjbyv5nwgnk0dj0h03kqw88qcq")))

