(define-module (crates-io pa nd panduck-html) #:use-module (crates-io))

(define-public crate-panduck-html-0.1.0 (c (n "panduck-html") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "katex") (r "^0.4.2") (f (quote ("duktape"))) (o #t) (k 0)) (d (n "notedown_ast") (r "^0.13.9") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1.2") (f (quote ("markdown"))) (d #t) (k 2)) (d (n "pretty") (r "^0.11.1") (d #t) (k 0)) (d (n "syntect") (r "^4.6.0") (o #t) (d #t) (k 0)))) (h "1val6d7d3lwnq1rzsp9daprdixja2kc93nnzzg8fk2n3m60w509b") (f (quote (("default" "katex" "syntect"))))))

(define-public crate-panduck-html-0.1.1 (c (n "panduck-html") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "katex") (r "^0.4.2") (f (quote ("duktape"))) (o #t) (k 0)) (d (n "notedown_ast") (r "^0.14.0") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1") (f (quote ("markdown"))) (d #t) (k 2)) (d (n "pretty") (r "^0.11.2") (d #t) (k 0)) (d (n "syntect") (r "^4.6.0") (o #t) (d #t) (k 0)))) (h "1wz2z9f1pi25z463msdmiyq8b81xc73jxd3ik4vxd4whg7l2y91y") (f (quote (("default" "katex" "syntect"))))))

