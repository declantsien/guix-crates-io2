(define-module (crates-io pa nd pandoc_ast) #:use-module (crates-io))

(define-public crate-pandoc_ast-0.1.0 (c (n "pandoc_ast") (v "0.1.0") (d (list (d (n "serde") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.5") (d #t) (k 0)) (d (n "serde_macros") (r "^0.5") (d #t) (k 0)))) (h "168dx84v8gk0px8q1vm2fvfpi34j7bpan7i5p65gaavgc2513g7p")))

(define-public crate-pandoc_ast-0.2.0 (c (n "pandoc_ast") (v "0.2.0") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 0)))) (h "0c16wam25fp0zxwriqnl2lsp6d6v7hwcaxqx53sylw77izfkdgwz")))

(define-public crate-pandoc_ast-0.2.1 (c (n "pandoc_ast") (v "0.2.1") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "0qk800n8466yk65r37bs36dy78w8gzypvnwad6prrcr1yn27fphn")))

(define-public crate-pandoc_ast-0.2.2 (c (n "pandoc_ast") (v "0.2.2") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "19wfz6ikiidrgbalj0mwzpfxffhr1yb8yx1v9v7ga9rhxqkyybga")))

(define-public crate-pandoc_ast-0.3.2 (c (n "pandoc_ast") (v "0.3.2") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "1nrhrxgpkda9mhgq7agg4chc1w20455fjxjzg7zm8gyxfmf5d9vi")))

(define-public crate-pandoc_ast-0.3.3 (c (n "pandoc_ast") (v "0.3.3") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "0546bb5990vqn8wvj0p6xnzmjmshscxzswc0qls5y3332qs7vqkv")))

(define-public crate-pandoc_ast-0.3.4 (c (n "pandoc_ast") (v "0.3.4") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "1ml7kgph1iw79n3wnq1i988vzcngy1x4pha7zmm4mzkzl167dm5j")))

(define-public crate-pandoc_ast-0.4.0 (c (n "pandoc_ast") (v "0.4.0") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "0dwh7ah30gq3mb45qdpckkllaxj33qbykp6d1f1nrkvw93xb2xym")))

(define-public crate-pandoc_ast-0.5.0 (c (n "pandoc_ast") (v "0.5.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0qi1b4n929y4shvr8kamj633j5a4mrzqb7jbkrxdmcwjm4p7xhc1")))

(define-public crate-pandoc_ast-0.5.1 (c (n "pandoc_ast") (v "0.5.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1d8vxrv6qxjh07r8yg7j4f8gnjxiy2i37d06c8a3vbqw082wy0qq")))

(define-public crate-pandoc_ast-0.5.2 (c (n "pandoc_ast") (v "0.5.2") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1icvqqx1r6mjphcxhb3v8i7g4i3yp1zxsz7hh0gj9d92w55q71nb")))

(define-public crate-pandoc_ast-0.5.3 (c (n "pandoc_ast") (v "0.5.3") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0sq50pr9v16c0m0kzzl8i8isqdhp7696zi52q32dg0s2s3lh9zva")))

(define-public crate-pandoc_ast-0.6.0 (c (n "pandoc_ast") (v "0.6.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1w17p596r9hsjx14cazp6b31wbm1ryhrniz89kiw19l8kk5zjjmz")))

(define-public crate-pandoc_ast-0.6.1 (c (n "pandoc_ast") (v "0.6.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1hxryiwlrrq8ll7br677vzndhgwbpx56svxgzskrs0p3hzfxc49c")))

(define-public crate-pandoc_ast-0.7.0 (c (n "pandoc_ast") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "0bazr8hrzazszybvcx9h4zzcpbn7rj0zyb5f0w4xqxlq77kxz7pr")))

(define-public crate-pandoc_ast-0.7.1 (c (n "pandoc_ast") (v "0.7.1") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "15fyrf48raa1ggrqgnka1xskh2zcgm3jdj0m47g8jk2v0a96x6jb")))

(define-public crate-pandoc_ast-0.7.2 (c (n "pandoc_ast") (v "0.7.2") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "15jn9ipcip6r11kllka8fnn2h6617hycvx67cbq4qn3n4mlzdliz")))

(define-public crate-pandoc_ast-0.7.3 (c (n "pandoc_ast") (v "0.7.3") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "1rx98clzwm7v42b3k7v3l3as095rsahxvr5c8cmfnkzrg2dhv5kv")))

(define-public crate-pandoc_ast-0.8.0 (c (n "pandoc_ast") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "01mxycbm7ym2iij5qyiz9vbni6xwni3nipj7z3i7njmkclyfch59")))

(define-public crate-pandoc_ast-0.8.2 (c (n "pandoc_ast") (v "0.8.2") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "0vjkwnh2ga0rq4j3g45kzb6mimwqfzqfvspkh0j2g3z679snxxmk")))

(define-public crate-pandoc_ast-0.8.4 (c (n "pandoc_ast") (v "0.8.4") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "0x0sp8xk606gp85f6p0l31dyyj9b0amw1mcwqfmhimcxkkgd72bv")))

(define-public crate-pandoc_ast-0.8.5 (c (n "pandoc_ast") (v "0.8.5") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "1jcmlp2zv3fj30nzwfwjljkppnhih20scgf4z789cmklnjmkkxjn")))

(define-public crate-pandoc_ast-0.8.6 (c (n "pandoc_ast") (v "0.8.6") (d (list (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "1klla7iimk379b9lqg80ck5snc1smzprgdmqniyaqb7vz697mzz1")))

