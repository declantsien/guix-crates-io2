(define-module (crates-io pa nd pandas-cli) #:use-module (crates-io))

(define-public crate-pandas-cli-0.1.0 (c (n "pandas-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)))) (h "0k2p22kj4n59nxdrawm8wpidqdb963yyin7cvs1181b8f9qsmj13")))

(define-public crate-pandas-cli-0.1.1 (c (n "pandas-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)))) (h "1k2rxinj07hx5nfwh2806i5jjq7d6d9a1dvpbhrf76hr5wyr9h6i")))

(define-public crate-pandas-cli-1.0.0 (c (n "pandas-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)))) (h "0pa4yz0aa34zxkzcglfsvbn0v0s1f5rpcfbazyqvw7i4c85kachh")))

