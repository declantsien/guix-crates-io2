(define-module (crates-io pa nd panduck-sycamore) #:use-module (crates-io))

(define-public crate-panduck-sycamore-0.6.3 (c (n "panduck-sycamore") (v "0.6.3") (d (list (d (n "notedown_ast") (r "^0.13.3") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1.1") (d #t) (k 2)) (d (n "sycamore") (r "^0.6.3") (f (quote ("ssr"))) (d #t) (k 0)))) (h "0k3bckwwxchyhry5lv6rhkcp3fy991md1px7crl83v1cy8gbb10s")))

(define-public crate-panduck-sycamore-0.7.0 (c (n "panduck-sycamore") (v "0.7.0") (d (list (d (n "notedown_ast") (r "^0.13.9") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1.2") (d #t) (k 2)) (d (n "panduck-html") (r "^0.1.0") (k 0)) (d (n "sycamore") (r "^0.7.0") (f (quote ("ssr"))) (d #t) (k 0)))) (h "01cxgix2n89y45ylc8n92yrp16fhf4y249idpiw8kkbyqfag72xn") (f (quote (("local" "panduck-html/katex" "panduck-html/syntect") ("default" "local"))))))

(define-public crate-panduck-sycamore-0.7.1 (c (n "panduck-sycamore") (v "0.7.1") (d (list (d (n "notedown_ast") (r "^0.14.0") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1") (f (quote ("markdown"))) (d #t) (k 2)) (d (n "panduck-html") (r "^0.1") (k 0)) (d (n "sycamore") (r "^0.7.1") (f (quote ("ssr"))) (d #t) (k 0)))) (h "06czc914am9sgnxfgm3568f5b22gsklin8pr3vm3zf7shrfbxdbm") (f (quote (("local" "panduck-html/katex" "panduck-html/syntect") ("default" "local"))))))

