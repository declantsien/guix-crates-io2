(define-module (crates-io pa nd panda-re-sys) #:use-module (crates-io))

(define-public crate-panda-re-sys-0.1.0 (c (n "panda-re-sys") (v "0.1.0") (h "0h8jzdh6a3wimyym9bb0jhdi80h2a424pd99ghnl6sljy628235y") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips") ("libpanda") ("i386") ("arm"))))))

(define-public crate-panda-re-sys-0.2.0 (c (n "panda-re-sys") (v "0.2.0") (h "0jn9yzbya869zn9r4iiwbz84ilg9c3bqifs7adf8a27c0rlqn0qh") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips") ("libpanda") ("i386") ("arm"))))))

(define-public crate-panda-re-sys-0.3.0 (c (n "panda-re-sys") (v "0.3.0") (h "086hy9n8pjyf2rvas83iw90awn052glk3nmydwhqc19a174z6dvs") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

(define-public crate-panda-re-sys-0.4.0 (c (n "panda-re-sys") (v "0.4.0") (h "16ljavxidx19943zmxwi29nxs57508xx4pv2jr0shci9b1lijwn0") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

(define-public crate-panda-re-sys-0.5.0 (c (n "panda-re-sys") (v "0.5.0") (h "009fkylhrjmxch6j9hld8fv760x43vg299qnr0kz0g2dd2my77wv") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips64") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

(define-public crate-panda-re-sys-0.6.0 (c (n "panda-re-sys") (v "0.6.0") (h "0176lnjkiq98qsxrwkrl7b70p7jmvikdayb2f20fqyywgfahxnx4") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips64") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

(define-public crate-panda-re-sys-0.7.0 (c (n "panda-re-sys") (v "0.7.0") (h "10q7cvdrza2sbzd6h5yhdq6m99snfkcvcy2xlnhg8nrivh0f03p0") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips64") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

(define-public crate-panda-re-sys-0.7.1 (c (n "panda-re-sys") (v "0.7.1") (h "1xw3zykpsidgi6mx0dm7m3k6nv83d0f58isg7p3mi7rq0p6v21f1") (f (quote (("x86_64") ("ppc") ("mipsel") ("mips64") ("mips") ("libpanda") ("i386") ("arm") ("aarch64"))))))

