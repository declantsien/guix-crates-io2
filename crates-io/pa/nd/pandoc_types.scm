(define-module (crates-io pa nd pandoc_types) #:use-module (crates-io))

(define-public crate-pandoc_types-0.1.0 (c (n "pandoc_types") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)))) (h "0yd8gfm5ml8haz47gfk8bhmazrcqyr9wq2qbfhicp3rlprgnlxyv")))

(define-public crate-pandoc_types-0.2.0 (c (n "pandoc_types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vbprcqc79b05z1q4sanpryrsrr0y9gl2ish20a2ak6q1r7rr1i9")))

(define-public crate-pandoc_types-0.3.2 (c (n "pandoc_types") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dr70f5654lqbiww856minzm7c4s177lv27acjgxr7rk41y9zpj9")))

(define-public crate-pandoc_types-0.3.3 (c (n "pandoc_types") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bwcv0b9y9hv4p8cya9qfqq219lx683hwqyjzhlcmg469phdx383")))

(define-public crate-pandoc_types-0.4.0 (c (n "pandoc_types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ypbpby3q2wfm8q6wp8rq0jybbxjl1q3sdq017nppqishxyvp634")))

(define-public crate-pandoc_types-0.5.0 (c (n "pandoc_types") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0f7b90a49gawqq51n57564yyb5gvdf4haxz7l3jgmi8xr3rpq9y3")))

(define-public crate-pandoc_types-0.6.0 (c (n "pandoc_types") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d0l441p8i05qjha4ds9gi74vg240czx7ws7k7nhszxdp0j2y5qn")))

