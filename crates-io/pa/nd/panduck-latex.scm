(define-module (crates-io pa nd panduck-latex) #:use-module (crates-io))

(define-public crate-panduck-latex-0.1.0 (c (n "panduck-latex") (v "0.1.0") (d (list (d (n "notedown-error") (r "^1.1.2") (d #t) (k 0)) (d (n "notedown-rt") (r "^0.3.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.16.1") (d #t) (k 0)) (d (n "panduck-core") (r "^0.1") (f (quote ("markdown"))) (d #t) (k 2)) (d (n "panduck-pp") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "159mpyqznmvbx39ij33x8x9dhxcb3v6whzrysv24fqavpxxa9325")))

