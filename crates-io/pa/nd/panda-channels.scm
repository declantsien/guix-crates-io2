(define-module (crates-io pa nd panda-channels) #:use-module (crates-io))

(define-public crate-panda-channels-0.1.0 (c (n "panda-channels") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.19") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1pr1fv72hfjfvxdqv8nca5nkyi3kxfr5i62pw0xfk0q9m6pmvwf3") (f (quote (("c-bindings" "cbindgen"))))))

(define-public crate-panda-channels-0.2.0 (c (n "panda-channels") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.19") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0h6rwwb0id3lzi1dp7kafwfl4ndx5ffvx4gbilm06ln8pkc0fjx9") (f (quote (("c-bindings" "cbindgen"))))))

(define-public crate-panda-channels-0.3.0 (c (n "panda-channels") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.19") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "171fwp0c7w17x2vkwl8cb84z9sj8rgzjfq2wr600sf4p0djig7aj") (f (quote (("c-bindings" "cbindgen"))))))

(define-public crate-panda-channels-0.4.0 (c (n "panda-channels") (v "0.4.0") (d (list (d (n "cbindgen") (r "^0.19") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1dygxv7pf6swy8sv238vj48nk2zb7ighmwl673g2bpr94dg3ys7y") (f (quote (("c-bindings" "cbindgen"))))))

