(define-module (crates-io pa nd pandora-api-derive) #:use-module (crates-io))

(define-public crate-pandora-api-derive-0.1.0 (c (n "pandora-api-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v0z46bld5liflw4bx9macmfb9hdxj6a7miw8f99l49g42a3ndif")))

(define-public crate-pandora-api-derive-0.1.2 (c (n "pandora-api-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "152z7ph4z724gw86dsb7p4ym8jpklnfl5b8yzir63y3k2qxicrgr")))

(define-public crate-pandora-api-derive-0.1.3 (c (n "pandora-api-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0d2np9244hxfd23c4jk0aj4abm8cibwqrq3xjcdx61vqcizs0mka")))

