(define-module (crates-io pa nd pandas_rs) #:use-module (crates-io))

(define-public crate-pandas_rs-0.1.0 (c (n "pandas_rs") (v "0.1.0") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0ljnp1jz4mwylma3p4b4j9dyg02338jq57nmigx7q5idyi4z1vlw")))

(define-public crate-pandas_rs-0.1.1 (c (n "pandas_rs") (v "0.1.1") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1dk95wgys231cdj1il3sxy03lcb9h7anpxl0zd10fwsch2ycgr1w")))

(define-public crate-pandas_rs-0.1.2 (c (n "pandas_rs") (v "0.1.2") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1qhkbg76i4yrr4p2hd9xviwj8baky1wf02p09k8zw10124dywgin")))

(define-public crate-pandas_rs-0.1.3 (c (n "pandas_rs") (v "0.1.3") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1n3clp25lpw3s8rv05l9g4xrawlqqs5s4f3r4p5jq53pm8d5fxzg")))

(define-public crate-pandas_rs-0.1.4 (c (n "pandas_rs") (v "0.1.4") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0pwbkr5525aiznw2g01z0fdj7apjrn4ilk8c246h7js72l5wkwq5")))

(define-public crate-pandas_rs-0.1.5 (c (n "pandas_rs") (v "0.1.5") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "08rn7zw21s2q28hv8wbh8n94rdb1g9l213m77mn5q31gnmz0c313")))

(define-public crate-pandas_rs-0.1.6 (c (n "pandas_rs") (v "0.1.6") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0kw2xbhr38v6j2nqfyacykkq1d39g5b4xjjhayxb77fyfdhhn0dh")))

(define-public crate-pandas_rs-0.1.7 (c (n "pandas_rs") (v "0.1.7") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "02922wc8wbmnbq4k51i3ifyb5055x5qsjhb2dqs264crj573zr2g")))

(define-public crate-pandas_rs-0.1.8 (c (n "pandas_rs") (v "0.1.8") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "004kr2skg43msaflidrlw4jg3xr09rm17ii4di4c265kbxn05yq5")))

(define-public crate-pandas_rs-0.1.9 (c (n "pandas_rs") (v "0.1.9") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0gwkafly060m8grb3g3w3dm8g78xj0ml2rnvic93lrfrka7hjq5z")))

(define-public crate-pandas_rs-0.1.10 (c (n "pandas_rs") (v "0.1.10") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1yajyxp268b8qhg21zbnvd9w81669ki6kpgqkk8zr0lkvfxd64n5")))

(define-public crate-pandas_rs-0.1.11 (c (n "pandas_rs") (v "0.1.11") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0byj3zyx59k3v878nnpaxw1w2blfza997c97i3gw8hjmw213smry")))

(define-public crate-pandas_rs-0.1.12 (c (n "pandas_rs") (v "0.1.12") (d (list (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1977iwan4wh139pfb76ijc3z93a63nsvbczjqlrfz1vkihk9z9ni")))

(define-public crate-pandas_rs-0.1.13 (c (n "pandas_rs") (v "0.1.13") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0ic79a9a523ib37a30l7py79r8mkbrgmdg1z6ha2gknjl6z8vg9i")))

(define-public crate-pandas_rs-0.1.14 (c (n "pandas_rs") (v "0.1.14") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1m3f984p1ajdp62c4h9rg4x0x1d7xi6hr40fjfqh36hnbkvby7vb")))

(define-public crate-pandas_rs-0.1.15 (c (n "pandas_rs") (v "0.1.15") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "14vxgszrq03bz4s1kr3c0kni5qh958pxshhhirnclkva0gr03b64")))

(define-public crate-pandas_rs-0.1.16 (c (n "pandas_rs") (v "0.1.16") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "0r1vsqlis9p94a4qbfcajxa432ywpvwgj3aqrjqfw9dqapvvkqzy")))

(define-public crate-pandas_rs-0.1.17 (c (n "pandas_rs") (v "0.1.17") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1d9g1ip92vyahkilpi13jrak0n6jhzsclqgwvqmj9n8nc0wiqi0n")))

(define-public crate-pandas_rs-0.1.18 (c (n "pandas_rs") (v "0.1.18") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1jcw00y0xk3bf4m8s2avjdvp5r5q6gi65l7isl8qghcmnwcvyg2n")))

