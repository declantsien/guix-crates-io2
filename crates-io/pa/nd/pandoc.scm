(define-module (crates-io pa nd pandoc) #:use-module (crates-io))

(define-public crate-pandoc-0.1.0 (c (n "pandoc") (v "0.1.0") (d (list (d (n "itertools") (r "*") (d #t) (k 0)))) (h "0hwssbq17b3d4zinqz2d75vj9rkmimsjd5022pg1ikffnshmwqgr")))

(define-public crate-pandoc-0.2.0 (c (n "pandoc") (v "0.2.0") (d (list (d (n "itertools") (r "^0.3") (d #t) (k 0)))) (h "0hzzrb20prjh2a2r754jvaxk4i12flsyivp8khf1b52296pa6saj")))

(define-public crate-pandoc-0.3.0 (c (n "pandoc") (v "0.3.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "0i5q13q96fyn8r0a5h402gwmbysg6rbyhyld99f9qr1mn87i2vnv")))

(define-public crate-pandoc-0.4.0 (c (n "pandoc") (v "0.4.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1xbgrrwpyfa14dklx93m2nyrvh5w3rabp05x0b9bsl6259brvwg4")))

(define-public crate-pandoc-0.4.1 (c (n "pandoc") (v "0.4.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "054xdh2dq3rj0wrym6saxahj47fwy7lb999iah01zjwbd9paw7sj")))

(define-public crate-pandoc-0.4.2 (c (n "pandoc") (v "0.4.2") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1pm2krxrjzgj3wi1mzkzg42px1glnfd69jwyl10js26i505wjqmb")))

(define-public crate-pandoc-0.4.3 (c (n "pandoc") (v "0.4.3") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "0rnxzyjixw2cjv2wj2y4bk97b52rf86dwqc3w918g6c5lw5jkpjp")))

(define-public crate-pandoc-0.5.0 (c (n "pandoc") (v "0.5.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1ajxzybn9mxg6iqgxjlkz9wrw3ms57fwan71mllwl1cw86g6bn6i")))

(define-public crate-pandoc-0.5.1 (c (n "pandoc") (v "0.5.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "088m8rlyss4y30jfcs5v10iwg1c1v2cm2bjr2f9pwyg4j8b4hqxy")))

(define-public crate-pandoc-0.5.2 (c (n "pandoc") (v "0.5.2") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1fvl3xx5k2fhcz79iy7iq84idv2sqb05i8jpk39pnz4acl03amby")))

(define-public crate-pandoc-0.5.3 (c (n "pandoc") (v "0.5.3") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "0b2fmpll2g7l1hcczkigrrlxgv01ww3z4qp7shf1smfc1rlp04s2") (y #t)))

(define-public crate-pandoc-0.6.0 (c (n "pandoc") (v "0.6.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "1kdp3vcbq4gisfbg6hclakx6pkiikmzpscxypr8lljfsvf9mnszp")))

(define-public crate-pandoc-0.6.1 (c (n "pandoc") (v "0.6.1") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)))) (h "0mbii02x78zj0bn81f08gm83h5j764ykw7cywchcgxi12wcp9j8y")))

(define-public crate-pandoc-0.6.2 (c (n "pandoc") (v "0.6.2") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)))) (h "08rvamn3p5fabdih9xs15f0p6basnjdnh9lcyp5a7p9rnx8gyfaa")))

(define-public crate-pandoc-0.7.0 (c (n "pandoc") (v "0.7.0") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)))) (h "0x5kns2sykny98sax1ladkqyc2bcxan314l8sj30kzgp9vj7lq3r")))

(define-public crate-pandoc-0.8.0 (c (n "pandoc") (v "0.8.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "05a7kzb7wid0rlihdvk71kx3h94x3q3wyma9h3z18y621pyz7gkp")))

(define-public crate-pandoc-0.8.1 (c (n "pandoc") (v "0.8.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "14ix37s0mngjws6mlsfzxgic0n0alh411gcaxcahldn1fak9n4xg")))

(define-public crate-pandoc-0.8.2 (c (n "pandoc") (v "0.8.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1wmqsnzvww8zy2744qpxk4i0y8i24kk5xpcj6s4liy3jv7yjs3y6")))

(define-public crate-pandoc-0.8.3 (c (n "pandoc") (v "0.8.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0k65c99c42ha9hkv32wbqhj7sbkz0564xykj774gmknrdr9q8spn")))

(define-public crate-pandoc-0.8.4 (c (n "pandoc") (v "0.8.4") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0nh6yjiwikm6v5fpifbk811ngkdf4jcjqc1q82xcshmp5r20w1l4")))

(define-public crate-pandoc-0.8.5 (c (n "pandoc") (v "0.8.5") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0fddva2xkz3z0pqbpy0y2mdqgsvw7q4x6g8bkv242lm2f7ppc8f0")))

(define-public crate-pandoc-0.8.6 (c (n "pandoc") (v "0.8.6") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1sysnq31lsr9qs4ixijly7444pljcj4x24nlabl65vc1vnmv2nlc")))

(define-public crate-pandoc-0.8.7 (c (n "pandoc") (v "0.8.7") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1wpkdnajiidcazdf3qppixcl1da0nwh1sl3hkcsrnryqjy331fay")))

(define-public crate-pandoc-0.8.8 (c (n "pandoc") (v "0.8.8") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1fqddilkvnnpsrg196bgj01xwlp53ggskcs8xk2jbpp8gmdpib0f")))

(define-public crate-pandoc-0.8.9 (c (n "pandoc") (v "0.8.9") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "16lqz8api2w7hznipywbgm4xq6bws3jg2xi37s02gzmxnhl5fkhd")))

(define-public crate-pandoc-0.8.10 (c (n "pandoc") (v "0.8.10") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0808vxyqp6jlvwmncjacyk1n9agyflv6l1r9as9dg7zd4yfldf1f")))

(define-public crate-pandoc-0.8.11 (c (n "pandoc") (v "0.8.11") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)))) (h "0p56085drzwb143ximsvr5bni7k0cl9ld7dz7lh92hkslz8m6ga6")))

