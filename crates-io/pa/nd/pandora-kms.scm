(define-module (crates-io pa nd pandora-kms) #:use-module (crates-io))

(define-public crate-pandora-kms-0.1.2-alpha.1 (c (n "pandora-kms") (v "0.1.2-alpha.1") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1ym9gdjvim0r52bbx74px07dfvcym197ri5idqh37icpv1pk84aj")))

(define-public crate-pandora-kms-0.2.0 (c (n "pandora-kms") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "05xc4lf6by6n385hjxhfvcvpv011gai1dwzgi442w4qd5vd6c0c3")))

