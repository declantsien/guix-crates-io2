(define-module (crates-io pa nd pandora-rs2) #:use-module (crates-io))

(define-public crate-pandora-rs2-0.1.0 (c (n "pandora-rs2") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "18aqkzfz47qab3ywy5l3s3z63nyhlljc1gbq879nj83jdk614nwl")))

(define-public crate-pandora-rs2-0.1.1 (c (n "pandora-rs2") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0cvw544svy60dq88pka9vza9077bvyj4x7i99qq40afi2pdh3jkz")))

(define-public crate-pandora-rs2-0.1.2 (c (n "pandora-rs2") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "132lkxwpjsmgc2ypn4nsr3pfigmm4ysb2k1yg2nqn4qnwyss24ij")))

(define-public crate-pandora-rs2-0.1.3 (c (n "pandora-rs2") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "18fk1vlbxn3wq22y8hq23f3cldf88pd2xmxghnzwn2mc602xf5d8")))

(define-public crate-pandora-rs2-0.1.4 (c (n "pandora-rs2") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "12qmf993y5pw8hiw37diqa9zqc9glwwk8zihxjb8h05k9g6sq1zr")))

(define-public crate-pandora-rs2-0.1.5 (c (n "pandora-rs2") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1zcrwhlzrf7ivliliqbqr7z0k7dc3djgp0b4kqf009f0l19jjyfl")))

(define-public crate-pandora-rs2-0.1.6 (c (n "pandora-rs2") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0mxq46w86kn4pxacccv6npv9mi6iq28qp648hbcarbhv2vjgm40h")))

(define-public crate-pandora-rs2-0.1.7 (c (n "pandora-rs2") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0i0x1cfhcdqff9wpg463wyjm0g5vbz8wgslcigkwyzl2m4mym4j8")))

