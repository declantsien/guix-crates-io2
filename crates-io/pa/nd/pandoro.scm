(define-module (crates-io pa nd pandoro) #:use-module (crates-io))

(define-public crate-pandoro-0.1.0 (c (n "pandoro") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "jshell") (r "^0.1.1") (d #t) (k 0)) (d (n "jtar") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1favy99gprwqm3s70i89qx1a2g3fzm6m356vq2mfhgqv140x8hkb")))

