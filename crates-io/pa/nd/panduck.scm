(define-module (crates-io pa nd panduck) #:use-module (crates-io))

(define-public crate-panduck-0.2.0 (c (n "panduck") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.4") (d #t) (k 0)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g510sldp1cl30jb8q9f5yd47pqcjsrr22cywq63cvk1hfxbzsfy") (f (quote (("default"))))))

(define-public crate-panduck-0.2.1 (c (n "panduck") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.4") (d #t) (k 0)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.7.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lqzbqqf5m6mqiq7q028yizv9b38gpa7xq7c3y7d92bbnnmxqg7n") (f (quote (("default"))))))

(define-public crate-panduck-0.2.2 (c (n "panduck") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.4") (d #t) (k 0)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.7.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cq2dgswx5j4bm3ig6wszpgypdqjax5q65rwpgnh7lqrmh6myzcr") (f (quote (("default"))))))

(define-public crate-panduck-0.3.1 (c (n "panduck") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.4") (d #t) (k 0)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "notedown_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "rtf-grimoire") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mwpjphqmc2hr8d2xjzlxlxhcisqas36h92zjl5z0v5rmzjgmznv") (f (quote (("default"))))))

(define-public crate-panduck-0.4.0 (c (n "panduck") (v "0.4.0") (d (list (d (n "panduck-core") (r "^0.1") (d #t) (k 0)))) (h "0fw8fy7bclpsk3z5dc9d9axvxxb2yxb7v3shmfqixbjiw5m9yqsy") (f (quote (("default"))))))

