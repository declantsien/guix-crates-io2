(define-module (crates-io pa nd panduck-ebook) #:use-module (crates-io))

(define-public crate-panduck-ebook-0.1.0 (c (n "panduck-ebook") (v "0.1.0") (d (list (d (n "arc-rs") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)))) (h "0r1h8ha6i4qackca1knnpqwm6jw2qanzgcdff24ylimbghk200nh")))

