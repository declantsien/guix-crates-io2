(define-module (crates-io pa nd panduck-pp) #:use-module (crates-io))

(define-public crate-panduck-pp-0.1.0 (c (n "panduck-pp") (v "0.1.0") (d (list (d (n "pretty") (r "^0.11.2") (d #t) (k 0)))) (h "08mf4kg70f9pn5a87g8zm3pn50dcb3ilc8ifv7fiznhvl6q5f386") (f (quote (("default"))))))

(define-public crate-panduck-pp-0.1.1 (c (n "panduck-pp") (v "0.1.1") (d (list (d (n "pretty") (r "^0.11.2") (d #t) (k 0)))) (h "1pkj0ivl91n8gnzaagbl233jgcdkppm7pdd4nj8rsrgfryllxmn7") (f (quote (("default"))))))

(define-public crate-panduck-pp-0.2.0 (c (n "panduck-pp") (v "0.2.0") (d (list (d (n "pretty") (r "^0.11.2") (d #t) (k 0)))) (h "0zcrbbac5nwrnd659gbnyn4k3wwwg4mmshdx0df8w52jy3sf8092") (f (quote (("default"))))))

