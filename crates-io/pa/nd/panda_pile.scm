(define-module (crates-io pa nd panda_pile) #:use-module (crates-io))

(define-public crate-panda_pile-0.1.0 (c (n "panda_pile") (v "0.1.0") (d (list (d (n "slice_n") (r "^0.0.2") (d #t) (k 0)))) (h "0pn5q262dn6357jad3h6bwfrrhyvyhm8rdba0kms6485qhq6vvzb")))

(define-public crate-panda_pile-0.2.0 (c (n "panda_pile") (v "0.2.0") (d (list (d (n "slice_n") (r "^0.0.2") (d #t) (k 0)))) (h "1y9f8k4yxl5a9l0fwpdlvwk68qhhvax4b1h059f570mqwj79icq2")))

