(define-module (crates-io pa rl parley) #:use-module (crates-io))

(define-public crate-parley-0.1.0 (c (n "parley") (v "0.1.0") (d (list (d (n "fontique") (r "^0.1.0") (k 0)) (d (n "peniko") (r "^0.1.0") (k 0)) (d (n "skrifa") (r "^0.19.1") (k 0)) (d (n "swash") (r "^0.1.15") (d #t) (k 0)))) (h "1hvrjcbl22rpzap2ix0ma0kk5p8a153msjaznwp2gn8bkg4w21dy") (f (quote (("system" "std" "fontique/system") ("std" "fontique/std" "skrifa/std" "peniko/std") ("libm" "fontique/libm" "skrifa/libm" "peniko/libm") ("default" "system")))) (r "1.70")))

