(define-module (crates-io pa ro parol-macros) #:use-module (crates-io))

(define-public crate-parol-macros-0.1.0 (c (n "parol-macros") (v "0.1.0") (h "0nyfrlb2rw65hc9mjivpamy0zw5brf1r7j8yymi8l6px1rmacl70")))

(define-public crate-parol-macros-0.2.0 (c (n "parol-macros") (v "0.2.0") (h "0a3b8avicix1yl1xf3whkd9acyigxq3kh3jqxn2ryn4r38q4dmmx")))

