(define-module (crates-io pa ro paro-rs) #:use-module (crates-io))

(define-public crate-paro-rs-0.0.1 (c (n "paro-rs") (v "0.0.1") (h "0ana7a4v9ivd0wh58cfp3pw3382k13bfb16wr7wwsihdlxmh0129")))

(define-public crate-paro-rs-0.0.2 (c (n "paro-rs") (v "0.0.2") (h "0wijafjaz1xb91v7zqrzm9albsa6gjnlxmqakpx5zmc0bf649jfq")))

(define-public crate-paro-rs-0.0.3 (c (n "paro-rs") (v "0.0.3") (d (list (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0dgmr7q6dddd34rj8k6p6xcgzgmsyc56sdr6f8xmjc303l0gxdkb")))

(define-public crate-paro-rs-0.0.4 (c (n "paro-rs") (v "0.0.4") (d (list (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1jvc31bmvbz8m9plgbs7rqv3c3miys0yn2igkqxg6pfpbwc628qv")))

(define-public crate-paro-rs-0.0.5 (c (n "paro-rs") (v "0.0.5") (d (list (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1869scfwcxhc5qlqr5hq6nmz8jdq2qd22xzz0ai7ziwa2zrcrsl0")))

(define-public crate-paro-rs-0.0.6 (c (n "paro-rs") (v "0.0.6") (d (list (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1m9rn27kprrzdkl2ah5y8c4pa77k0af41gbdvb3b00p8z4h9sfzv")))

(define-public crate-paro-rs-0.0.7 (c (n "paro-rs") (v "0.0.7") (d (list (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1s565lmz0mv3lcj1frc0z7c5js7rna66xz8cb69p4kxq30mg0f5l")))

