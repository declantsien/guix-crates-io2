(define-module (crates-io pa at paat-core) #:use-module (crates-io))

(define-public crate-paat-core-0.1.0 (c (n "paat-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (k 0)))) (h "0n0ckgza3wvprx8pbr8x6kf5lyd4nqsd4bq3z9cyhkcbs1gp9qzp")))

