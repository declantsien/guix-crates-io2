(define-module (crates-io pa at paat-cli) #:use-module (crates-io))

(define-public crate-paat-cli-0.1.0 (c (n "paat-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paat-core") (r "^0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (f (quote ("wav"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "signal" "rt" "macros" "rt-multi-thread"))) (k 0)))) (h "12ldblfkn64y60pxrsg484wijp1hl9ka6bgblrxjvwywjy4fa4m6")))

