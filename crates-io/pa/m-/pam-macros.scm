(define-module (crates-io pa m- pam-macros) #:use-module (crates-io))

(define-public crate-pam-macros-0.0.1 (c (n "pam-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sik1pdk0hcd55cjvkq85s0bgha8gzx8askqfl2m68acbs251zpp")))

(define-public crate-pam-macros-0.0.2 (c (n "pam-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12d2rqlbv622zi112nbg7px6cy3hdd17bh72pbm0nyxkwyyzl3gn")))

(define-public crate-pam-macros-0.0.3 (c (n "pam-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04dqghkih4cvrs3adnyi2myjrq14kdiicjd1a576sg6zjydknky9")))

