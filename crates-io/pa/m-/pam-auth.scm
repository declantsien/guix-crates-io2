(define-module (crates-io pa m- pam-auth) #:use-module (crates-io))

(define-public crate-pam-auth-0.0.2-pre (c (n "pam-auth") (v "0.0.2-pre") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "14851z3hsx5b9dim4fwk9s84jcbf6i5j9gklfzdr1pimvb0isss9")))

(define-public crate-pam-auth-0.0.3 (c (n "pam-auth") (v "0.0.3") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "0vbc336fnbd6bv0h07069ypsllmrp5naw2wqvm2zdxs3dfjvazl0")))

(define-public crate-pam-auth-0.0.4-pre1 (c (n "pam-auth") (v "0.0.4-pre1") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "1hc5cmwwq2s3d7lkgvh5h7fnasfdirnd7p4rfq1axnxn1rz06s2f")))

(define-public crate-pam-auth-0.0.4 (c (n "pam-auth") (v "0.0.4") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "0agxcgg8bpbdag7wc7d5qzzdqn8b1k3c4d5xahq1jlnjz6qmnys0")))

(define-public crate-pam-auth-0.0.5-pre (c (n "pam-auth") (v "0.0.5-pre") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "0il2j8pxgpsfsfa83fxvpxvq6fgbiyjka6p3xr0306cj39jvdxyw")))

(define-public crate-pam-auth-0.0.5 (c (n "pam-auth") (v "0.0.5") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "13xvkwi890rijlr12csm9nhixf17llkrnirr1bbpvvqq8z8asz07")))

(define-public crate-pam-auth-0.1.0 (c (n "pam-auth") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)))) (h "1f03k6ra17jcy30k3mlmm6v5nrs48kzgmmcd18z3qpb8pn5kk358")))

(define-public crate-pam-auth-0.2.0 (c (n "pam-auth") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pam-sys") (r "*") (d #t) (k 0)) (d (n "users") (r "*") (d #t) (k 0)))) (h "1hffsc0kqwyg3f7zs7c8k4991yl61pprg5plh5wdc0rjm0b2m6hd")))

(define-public crate-pam-auth-0.2.1 (c (n "pam-auth") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "pam-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "users") (r "^0.4.2") (d #t) (k 0)))) (h "081d423j6gk45khqxa0h844skc0nipmq66shqyl9bfqcbl7gfmg6")))

(define-public crate-pam-auth-0.2.2 (c (n "pam-auth") (v "0.2.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "pam-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "users") (r "^0.4.2") (d #t) (k 0)))) (h "11fkbn46n74v7s4np286pxgbiw1h914mn5x7d885zcnvm4ww01wb")))

(define-public crate-pam-auth-0.3.0 (c (n "pam-auth") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "pam-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "users") (r "^0.4.4") (d #t) (k 0)))) (h "16d1p3j1ylj1v7ypgbgk3kjxvz37k5rnj6dli6034si7h3jmk011")))

(define-public crate-pam-auth-0.3.1 (c (n "pam-auth") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "pam-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "users") (r "^0.4.4") (d #t) (k 0)))) (h "12b6cl4w5zzx3bgf0z8v1d4jb884qi4w5v92im3lcwihric1hb5n")))

(define-public crate-pam-auth-0.4.0 (c (n "pam-auth") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "pam-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "users") (r "^0.5.1") (d #t) (k 0)))) (h "02hi47g271lnl15nd6knxi448q6iafyzphal981mc6nrgjf26028")))

(define-public crate-pam-auth-0.4.1 (c (n "pam-auth") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pam-sys") (r "^0.4.3") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "16w6q9dxjs4qcf8xy6ykql4iq012rwsa681vi5k14vrsv741sbvq")))

(define-public crate-pam-auth-0.5.0 (c (n "pam-auth") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "0awi360ykd9sjrs5fwh9fgr8qrxcn3imj8c2fvdg7bm6ap68bxpl")))

(define-public crate-pam-auth-0.5.1 (c (n "pam-auth") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "19yg7skdfm6rp55k50n5clrkgmjym1n2xb9f63zjwvx1ami280wy")))

(define-public crate-pam-auth-0.5.2 (c (n "pam-auth") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "013250cr1c3gf9mc9h598zrpr1q3r75xag4fk9y37cmyvj8fxpic")))

(define-public crate-pam-auth-0.5.3 (c (n "pam-auth") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.4") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "17dffi8dhdb79zv2vah79rx6fclbb8b0m7c5xhvpznbzyx2qhpqd")))

(define-public crate-pam-auth-0.5.4 (c (n "pam-auth") (v "0.5.4") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "users") (r "^0.5.3") (d #t) (k 0)))) (h "1qw4w8dqkm2fl5rr4sd850sk8m8ppzqdiiay4dd96xvaynbb2skq")))

(define-public crate-pam-auth-0.5.5 (c (n "pam-auth") (v "0.5.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5.6") (d #t) (k 0)) (d (n "users") (r "^0.5.3") (d #t) (k 0)))) (h "1ahpaaxm51p7fvmxi6qi0pgf931b9ray0lzinw3v9fka2x8d5wxh")))

(define-public crate-pam-auth-0.6.0 (c (n "pam-auth") (v "0.6.0") (h "12ymgvbc2rd4729csab1dpiznc5b8m49nk6bdkfwg61q3ym8wzv2")))

