(define-module (crates-io pa m- pam-bindings) #:use-module (crates-io))

(define-public crate-pam-bindings-0.1.0 (c (n "pam-bindings") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "1rdjcwdq45l0vbj2xmz1rx75ybkawbzcygbwspm3lw667xsr6jkv")))

(define-public crate-pam-bindings-0.1.1 (c (n "pam-bindings") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "1jk0fva8vp1x56m6q6zm2jzsamrrs7z1c46x7nfapdmc4blkghwm")))

