(define-module (crates-io pa m- pam-sys) #:use-module (crates-io))

(define-public crate-pam-sys-0.0.1-pre (c (n "pam-sys") (v "0.0.1-pre") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c31v7z0rb3knp5s6xzvjaqc3jzxqary7gz09b5d6hw8im91lgw9")))

(define-public crate-pam-sys-0.0.1 (c (n "pam-sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0py6czgx9vgzpgg118zs6zjvncg4b5aggwjx6ji7ypgm2qkqcc8w")))

(define-public crate-pam-sys-0.0.2-pre (c (n "pam-sys") (v "0.0.2-pre") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1l6dp79lclpcbypsqvxs0kwk1hiiciqzxm8hk8vlxm63ni9ww3c9")))

(define-public crate-pam-sys-0.0.2-pre2 (c (n "pam-sys") (v "0.0.2-pre2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15xzlwmg36z1v1ba4q1hdarvaqjj6sijz268s6wjvsyc5qvzq430")))

(define-public crate-pam-sys-0.0.2-pre3 (c (n "pam-sys") (v "0.0.2-pre3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gd8w6kjnn1rngnz5y3yi352xwfbar5cb9j9i4162frk5ifbx69d")))

(define-public crate-pam-sys-0.0.3 (c (n "pam-sys") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "10m4xfz3imfy0wc2kzhsibx77nxyqgkki67v298awhz9nbp2rvfv")))

(define-public crate-pam-sys-0.0.4 (c (n "pam-sys") (v "0.0.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1lgl7pv5kn9ksgb7xbfmq0ar74zbrl3a50a0l59xpqqf9z3wh65f")))

(define-public crate-pam-sys-0.1.0 (c (n "pam-sys") (v "0.1.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0xz5fddjm07h72nj35xlm1zd8gap9yph557sjc83jqh183fcph9v")))

(define-public crate-pam-sys-0.1.1 (c (n "pam-sys") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1kp0wc0jmg6lq9zsazk0pi4smm7x63x402h3iqg3w3n8xwsl6469")))

(define-public crate-pam-sys-0.1.2 (c (n "pam-sys") (v "0.1.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "09m3zpp4q3lf4g2y4mbb0hk013w3phay6dnrjkdb6a6z25cg04qq")))

(define-public crate-pam-sys-0.1.3 (c (n "pam-sys") (v "0.1.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "18d3xynjj88hcbs4y0piw0dhivg40n6fl5alkgkmvcridi19ilyl")))

(define-public crate-pam-sys-0.1.4 (c (n "pam-sys") (v "0.1.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0xs06irhix387zsc36qzhh8vjr08j86qygbyadjy3sah9hwp5s87")))

(define-public crate-pam-sys-0.2.0 (c (n "pam-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1zslfwm80ip97f90m4p24pzrn4rvvdk46x7ksd6naas8qfr65adh")))

(define-public crate-pam-sys-0.2.1 (c (n "pam-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1lgglnp6nzil9jgy2sa44khr8r1yaawmnp4hz4ng21y5bglh5d0m")))

(define-public crate-pam-sys-0.3.0 (c (n "pam-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1g4dddsyhhmkl8mjd3i1j0l8l65dv7ncmw3cfsyrnnibalsz722n")))

(define-public crate-pam-sys-0.4.0 (c (n "pam-sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0kk1szzygdi1ah2hgsjdwis24afvja24zk5axlkkc7x17kddy8wx")))

(define-public crate-pam-sys-0.4.1 (c (n "pam-sys") (v "0.4.1") (d (list (d (n "gcc") (r "^0.3.8") (d #t) (k 1)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1h6g8jwy9xpmp541jsd521rzvk0ci7jrfhnagrxvmc1221lai9pz")))

(define-public crate-pam-sys-0.4.3 (c (n "pam-sys") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1vimgi75npvq1082k6qjfhc9jz2p49jh6s3cy9vdf0wr0hsc3mrk")))

(define-public crate-pam-sys-0.5.0 (c (n "pam-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "08lksk0y054faib2yvcmfzsl05ih6xb7wg3ccxlkj6zw86rza74s") (y #t)))

(define-public crate-pam-sys-0.5.1 (c (n "pam-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1vizj7nwx3f2hvzc7x3akzy31ry5x5ivyjmpk90ny47rfylfmm3g") (y #t)))

(define-public crate-pam-sys-0.5.2 (c (n "pam-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "15zxl4a1iqbsydchxvzia9mrv68sz2v6bh8v5f5051i7ggb032qr") (y #t)))

(define-public crate-pam-sys-0.5.3 (c (n "pam-sys") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0mh12fi365klq42aldban0f7rsqrnl4dxvbnrkca46ma8r62rm8p")))

(define-public crate-pam-sys-0.5.4 (c (n "pam-sys") (v "0.5.4") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)))) (h "0fababg6vjhwjhsk6i9rzd6lj0fc9k8jqs21g28dhj2yb82rgrnc")))

(define-public crate-pam-sys-0.5.5 (c (n "pam-sys") (v "0.5.5") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "19j8v1x2bx9s0n0m4lckdn5qzby5argzq52v65c672s36h90lwd7") (l "pam")))

(define-public crate-pam-sys-0.5.6 (c (n "pam-sys") (v "0.5.6") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0d14501d5vybjnzxfjf96321xa5wa36x1xvf02h02zq938qmhj6d") (l "pam")))

(define-public crate-pam-sys-1.0.0-alpha1 (c (n "pam-sys") (v "1.0.0-alpha1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1f5vjbgv45i6hlcbaca6a4189mf6fmpgdszwv6a66a6239aq80p4") (l "pam")))

(define-public crate-pam-sys-1.0.0-alpha2 (c (n "pam-sys") (v "1.0.0-alpha2") (d (list (d (n "bindgen") (r "= 0.53.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "01d0qqihcsycs72zc31vyja561pwpyf58q1dqbw45ldlqfhs27ic") (l "pam")))

(define-public crate-pam-sys-1.0.0-alpha3 (c (n "pam-sys") (v "1.0.0-alpha3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "02fdsw7c6jh0yzhjm3vk8lylrqimvzw2m2hvn5myndscyy6f1mc4") (l "pam")))

(define-public crate-pam-sys-1.0.0-alpha4 (c (n "pam-sys") (v "1.0.0-alpha4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0sf98s00bc9nzn4vcv6fmqm3xjjrqafzsy8h12qnnslghm1gv7ay") (l "pam")))

(define-public crate-pam-sys-1.0.0-alpha5 (c (n "pam-sys") (v "1.0.0-alpha5") (d (list (d (n "bindgen") (r "^0.69") (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0ywrksnl0cwx42qllplwq3s1xwx5lsv1q6f5rnxc0liykdr8956f") (l "pam")))

