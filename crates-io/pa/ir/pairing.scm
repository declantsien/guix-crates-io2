(define-module (crates-io pa ir pairing) #:use-module (crates-io))

(define-public crate-pairing-0.9.0 (c (n "pairing") (v "0.9.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dlaa2vz4wxzb1a3ij0igli3vrmmrcajzzhrrx0gyccnn4gqjw4z")))

(define-public crate-pairing-0.10.0 (c (n "pairing") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.144") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0w4j4zjjrgaig0q3kdz7w3m11ahiqr3ycjcnm731n1ha4m0ak0rg") (f (quote (("unstable-wnaf") ("unstable-features" "unstable-wnaf"))))))

(define-public crate-pairing-0.10.1 (c (n "pairing") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.145") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ic9wx8lsnsp02dqds5bcyf6hldwzp4kcvv04kpac6zv333wyhk2") (f (quote (("unstable-wnaf") ("unstable-features" "unstable-wnaf"))))))

(define-public crate-pairing-0.10.2 (c (n "pairing") (v "0.10.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.148") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ifsxbslh4lwh61v67i2gblrmkpixsxk1h24cfyx9xl4974a1vsg") (f (quote (("unstable-wnaf") ("unstable-features" "unstable-wnaf"))))))

(define-public crate-pairing-0.11.0 (c (n "pairing") (v "0.11.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.151") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1iwnyfinha4hnn63vpb424jishg7qc39m0z43p3fxdxmv90211ka") (f (quote (("unstable-wnaf") ("unstable-features" "unstable-wnaf") ("u128-support") ("default" "u128-support"))))))

(define-public crate-pairing-0.12.0 (c (n "pairing") (v "0.12.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0np7krl0g5m27iph6b1vn9r5f1v47ffqf3a6czm5h336a2av0ha5") (f (quote (("unstable-features") ("u128-support") ("default"))))))

(define-public crate-pairing-0.13.0 (c (n "pairing") (v "0.13.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0w4dp0jrgqb4q7xv2bsxsnnfs9y03jgsm5mym7z6a4l7s49wrkn6") (f (quote (("unstable-features") ("u128-support") ("default"))))))

(define-public crate-pairing-0.13.1 (c (n "pairing") (v "0.13.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.174") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0xvd8az0vk4pgfwql1728ny4gqidvrgl4d4pnx84cp7rkb41a6wz") (f (quote (("unstable-features") ("u128-support") ("default"))))))

(define-public crate-pairing-0.13.2 (c (n "pairing") (v "0.13.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.174") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1h4m2kyd98aqdnrbmwkwdw1lqx6xhzip9jxzpap6zn4p2zpx9fk5") (f (quote (("unstable-features" "expose-arith") ("u128-support") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.14.0 (c (n "pairing") (v "0.14.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.186") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "16jcsz8f4hxlvgmznhm7rl7q5mslj1acy4im3sqds10ps8f1dk8i") (f (quote (("unstable-features" "expose-arith") ("u128-support") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.14.1 (c (n "pairing") (v "0.14.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.190") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1ymrpc7byl172b442xzjp0x3nsnmzbx5p6mmjvk7s9bq7901mwh6") (f (quote (("unstable-features" "expose-arith") ("u128-support") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.14.2 (c (n "pairing") (v "0.14.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.200") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0mqddp9lk0wsqmhbn89mimp5ll92mkc9imyk4ajdbijic89j3nnf") (f (quote (("unstable-features" "expose-arith") ("u128-support") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.15.0 (c (n "pairing") (v "0.15.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "group") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0nflhffhyvwzdw26yw2iaxk0f6h6xxczkfn7l3am4sdk350vdiqf") (f (quote (("unstable-features" "expose-arith") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.15.1 (c (n "pairing") (v "0.15.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "group") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1r3i6a7xgphvkjg1sw7wwhl4fjpww9zs578hsnf1ja4s8ws0bi4l") (f (quote (("unstable-features" "expose-arith") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.16.0 (c (n "pairing") (v "0.16.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "group") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1mjyzwvhg3z0gvgz7pi94z7k5lagng11s0v5rn12cw8a47m0sadq") (f (quote (("unstable-features" "expose-arith") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.17.0 (c (n "pairing") (v "0.17.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ff") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "group") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "subtle") (r "^2.2.1") (d #t) (k 0)))) (h "14av6fq9g9zpb9ixv4zmx3k9pzw4pk811v8ca7pha4dz471i6vwb") (f (quote (("unstable-features" "expose-arith") ("expose-arith") ("default"))))))

(define-public crate-pairing-0.18.0 (c (n "pairing") (v "0.18.0") (d (list (d (n "ff") (r "^0.8") (k 0)) (d (n "group") (r "^0.8") (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1zgpvdlim9z9cgmlx2sr2q8q5g354k4dw81c8mr642lyxvdjqw2g")))

(define-public crate-pairing-0.19.0 (c (n "pairing") (v "0.19.0") (d (list (d (n "ff") (r "^0.9") (k 0)) (d (n "group") (r "^0.9") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1k3764z5cfxrfcxviiw36rgi90vypfm1pfix6lcg0qq3y7mrks4v")))

(define-public crate-pairing-0.20.0 (c (n "pairing") (v "0.20.0") (d (list (d (n "group") (r "^0.10") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1wxpdnf1zg04j2i1ndygnlaa2zmwvg4q00zyjs0nx5n9cf9d1sbx")))

(define-public crate-pairing-0.21.0 (c (n "pairing") (v "0.21.0") (d (list (d (n "group") (r "^0.11") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0hhbai23lsfbd3bpfxrps6z4ba40r6qxlb28v7bns05397iibr7j")))

(define-public crate-pairing-0.22.0 (c (n "pairing") (v "0.22.0") (d (list (d (n "group") (r "^0.12") (k 0)))) (h "0fvbifnnxss25514yal3bq9zaa93j4mgplcwdws32axsppc90m8k")))

(define-public crate-pairing-0.23.0 (c (n "pairing") (v "0.23.0") (d (list (d (n "group") (r "^0.13") (k 0)))) (h "0vwr2c500jkw81ga8as4bx93ciyllspnr15n9gpl3kvkbric9zl1")))

