(define-module (crates-io pa ir pair_stack) #:use-module (crates-io))

(define-public crate-pair_stack-0.1.0 (c (n "pair_stack") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "postgres") (r "*") (d #t) (k 0)) (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "r2d2_postgres") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "staticfile") (r "*") (d #t) (k 0)))) (h "0yk2rgiiwj4xzsy294zk0c53ymkcj5xsn26dn5j5va3vq25bwa2f")))

