(define-module (crates-io pa ir pairlock) #:use-module (crates-io))

(define-public crate-pairlock-0.1.0 (c (n "pairlock") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 2)))) (h "1xa2yhgq96jvbhj74bgv3j6s9nchbjwair57l5ymawx4zx1lgk0x")))

