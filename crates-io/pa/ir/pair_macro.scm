(define-module (crates-io pa ir pair_macro) #:use-module (crates-io))

(define-public crate-pair_macro-0.1.0 (c (n "pair_macro") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (d #t) (k 0)))) (h "0p186mm7cnjl83a9fw6g4m31d3lw55scs719pa78csxnacj4ryhy") (f (quote (("default"))))))

(define-public crate-pair_macro-0.1.1 (c (n "pair_macro") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (d #t) (k 0)))) (h "0cwi1id594642k1n9b5lacy0k96z64f97a3fi3dmjikky8qsn1wh") (f (quote (("default"))))))

(define-public crate-pair_macro-0.1.2 (c (n "pair_macro") (v "0.1.2") (d (list (d (n "num-traits") (r ">=0.2.0, <0.3.0") (f (quote ("libm"))) (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1ak531dhw9ann8yvfai1aa33xy5my15fw1mmfnhs2i3nprww6dr1") (f (quote (("default"))))))

(define-public crate-pair_macro-0.1.3 (c (n "pair_macro") (v "0.1.3") (d (list (d (n "num-traits") (r ">=0.2.0, <0.3.0") (f (quote ("libm"))) (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0hpfhzf5rbc7la88xayfn0r400sya9sgmcjijhyk5la2dxardb7d") (f (quote (("default"))))))

(define-public crate-pair_macro-0.1.4 (c (n "pair_macro") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g67mkn2xjy40ly4z7afbsckz4fa8kxg2h2xkadc49rrn187l9c6") (f (quote (("default"))))))

