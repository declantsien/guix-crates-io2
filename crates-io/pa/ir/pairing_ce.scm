(define-module (crates-io pa ir pairing_ce) #:use-module (crates-io))

(define-public crate-pairing_ce-0.17.0 (c (n "pairing_ce") (v "0.17.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_ce") (r "^0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "03zl8g1g8b53qkzjb6g7rzjjv27mnwlkxmji6i109qwpizxjdrnc") (f (quote (("unstable-features" "expose-arith") ("expose-arith") ("default"))))))

(define-public crate-pairing_ce-0.18.0 (c (n "pairing_ce") (v "0.18.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0dlvgk7wnjn3bqqhnl1jm913q5p542izdpbdrc8n20p2f32sjxgh") (f (quote (("expose-arith") ("default"))))))

(define-public crate-pairing_ce-0.19.0 (c (n "pairing_ce") (v "0.19.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.8") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "18y9x01rrf7nz17vv77l64zii26b1naa77ivgnv6g341ihig8zjx") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.20.0 (c (n "pairing_ce") (v "0.20.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0f891dj5pllizdsqhaw1cjfrngjgrfvwap71x2jxk284gym26yvc") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.21.0 (c (n "pairing_ce") (v "0.21.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "0.10.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0zb54ccv96a49jjkrkf6fsynky6snndbqrs4vm1msnr4h2ys6p7m") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.21.1 (c (n "pairing_ce") (v "0.21.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "0.10.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1pnz3kq080rzlxxmaspp970n3cld7nlcwzwng830m7njn5jhvh5z") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.22.0 (c (n "pairing_ce") (v "0.22.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "0.11.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1k5x9kidil32m63rr539z5cpxjgcl4k57kpkzvv7qsi73cbnay1k") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.23.0 (c (n "pairing_ce") (v "0.23.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "0.11.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1vs7wyb4g5ddgpr16583pn5fml0fr8yjxy04yvvv097h3wg43gi2") (f (quote (("default"))))))

(define-public crate-pairing_ce-0.24.0 (c (n "pairing_ce") (v "0.24.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "ff") (r "0.12.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0qwpg8azdv8zvpwhf1d2n0dwd08x6y1yd6kfcxw30lgrmxm97zjn") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.24.1 (c (n "pairing_ce") (v "0.24.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "ff") (r "0.12.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0my74nxwvac2wmlhcscvfnanmak2f0qvgcdi0ag72ajv1nybjk5v") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.24.2 (c (n "pairing_ce") (v "0.24.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.12.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0n8p7xz8brr2y8dm782zk7bwks4a2jhbw56qf60akajycwakax8y") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.25.0 (c (n "pairing_ce") (v "0.25.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.12.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0y9xm54m0kgf9avcsz0kbcdnb4k23gbh5gws3hvf81aahk0jay9q") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.25.1 (c (n "pairing_ce") (v "0.25.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.12.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1xcd0ddvl5rqvlrh7pzn5dx97kvvhaw3sw6y06gnryhiz2s74ggh") (f (quote (("default") ("asm" "ff/asm_derive")))) (y #t)))

(define-public crate-pairing_ce-0.26.0 (c (n "pairing_ce") (v "0.26.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.13.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0ds6vl5wlk7lmg3gpnhdbsxzb5hhvhjbh2m80xwqw8137kb6nij0") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.0 (c (n "pairing_ce") (v "0.28.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vkdcjgx5jf55i8y8fa2n04vqaf52c0zs2sfgb4w0dz4ry6gw4p0") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.1 (c (n "pairing_ce") (v "0.28.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wj148gf7d36jac9dqv8k1a07hhyh7lxyc9h3zgkamx474602a7q") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.2 (c (n "pairing_ce") (v "0.28.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wf360afgr33pxz5h3j5yqz4gg6k3przf3wkiqml6yks9ba7kxhc") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.3 (c (n "pairing_ce") (v "0.28.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aa5d7rfm0c894w27rhq1wrvbnmmyapgl8cccis72sgs3h6mxpis") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.4 (c (n "pairing_ce") (v "0.28.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05gr4s6k3w1k6q38cnqkghzkp6acx9mar3lwa9dgq83qkrnqwny2") (f (quote (("default") ("asm" "ff/asm_derive"))))))

(define-public crate-pairing_ce-0.28.5 (c (n "pairing_ce") (v "0.28.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ff") (r "0.14.*") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15y5mpx2jqwkz40qda4nzcivyl1hy043arcfj4jx0q4n4lhpn06v") (f (quote (("default") ("asm" "ff/asm_derive"))))))

