(define-module (crates-io pa ir pairing-heap) #:use-module (crates-io))

(define-public crate-pairing-heap-0.1.0 (c (n "pairing-heap") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1kh7flap93z2il4l5r9rjvi5q4qrc5xmqjy4rn7d28hhvbzxpf44") (f (quote (("binary_heap_extras"))))))

