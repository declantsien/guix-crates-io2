(define-module (crates-io pa tt pattern_code) #:use-module (crates-io))

(define-public crate-pattern_code-0.1.0 (c (n "pattern_code") (v "0.1.0") (h "17ivw74c8imgf62rpbizy8akcsphkkdmlcclk39jq43prag966fh")))

(define-public crate-pattern_code-0.1.1 (c (n "pattern_code") (v "0.1.1") (h "0g6x27h83q5fpj8lhwx7a6f243qlwm91wwyzxs51f6b74yxsa35w")))

