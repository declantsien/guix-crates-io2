(define-module (crates-io pa tt pattern-matching) #:use-module (crates-io))

(define-public crate-pattern-matching-0.1.0 (c (n "pattern-matching") (v "0.1.0") (h "1llm2avjg0116san8v7v34jgdjafm4kqfygwin8940ly9k13kr2b")))

(define-public crate-pattern-matching-0.1.1 (c (n "pattern-matching") (v "0.1.1") (h "1y0ybgkw5ximlj0lz2qyy1799fknyxcl034247w2cbs64x2bildm")))

