(define-module (crates-io pa tt patternfly-dioxus) #:use-module (crates-io))

(define-public crate-patternfly-dioxus-0.1.0 (c (n "patternfly-dioxus") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xahjaw2z2yhdqqk0ib0wq4s6vxs9w0np6jkjl00v9d4dpycf3ck")))

(define-public crate-patternfly-dioxus-0.1.1 (c (n "patternfly-dioxus") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05ajki4a674zis3bp2g2zlg193lvn3p4l08b73aidj04n64d4zw8")))

(define-public crate-patternfly-dioxus-0.2.0 (c (n "patternfly-dioxus") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bp5rlgx98k2v7z6gpd9xmm88gr08mss1zxh5hr5dli0i33w10bp")))

(define-public crate-patternfly-dioxus-0.2.1 (c (n "patternfly-dioxus") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1362vb78k8nq1vcrzychvibp187mrzl3vhv4pvrqdsh896095ia0")))

(define-public crate-patternfly-dioxus-0.2.2 (c (n "patternfly-dioxus") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11lvig5w269jyi8lplq4z6vifvdns5n9sggrc0h7im0rag3l7i3s")))

(define-public crate-patternfly-dioxus-0.2.3 (c (n "patternfly-dioxus") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("web" "fermi" "ssr"))) (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m16l92241a4vxdwwyvd15j9c0pzambql71ivjfz9nif3wsbmxal")))

(define-public crate-patternfly-dioxus-0.3.0 (c (n "patternfly-dioxus") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-ssr") (r "^0.3.0") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.3.0") (d #t) (k 0)) (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14d88biab2bnr40wcjvddiw883z33k90l8am8wb9nsbw01gmgzmr")))

