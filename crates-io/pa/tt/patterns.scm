(define-module (crates-io pa tt patterns) #:use-module (crates-io))

(define-public crate-patterns-0.1.0 (c (n "patterns") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1j2id4ry1k6blxybhkaf0csd83hkk78yll4la9jq3gbp8rv19l6d")))

(define-public crate-patterns-0.1.1 (c (n "patterns") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0qkqhckzfk6ai4h8rngn9pdz0jpl69qk79ia3w8qzd39svnz10al")))

(define-public crate-patterns-0.1.2 (c (n "patterns") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "14i6a0rvqb1afbvkwkf10wjga26wnfi9jx63q8ab4fxn69pz59n5") (f (quote (("64bytes"))))))

(define-public crate-patterns-0.1.3 (c (n "patterns") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1y5h44xgaf72zlbdv8gqhsy3b6clizfwfdamsbvs7b497pz4l15g")))

(define-public crate-patterns-0.1.4 (c (n "patterns") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1frcq0w59d69yzribsnzqkx0hhc275fiaa22y77rnhxs0czznfwn")))

(define-public crate-patterns-0.2.0 (c (n "patterns") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1m5cjhrw94nmiq24j9lzbrlkj3nahd4m300laj827av9qd4z0qgi")))

(define-public crate-patterns-0.2.1 (c (n "patterns") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "04nxwv6ahrw3ya72zmj3mbic48vy9iafsxygy1jbln8466yfnc8y")))

(define-public crate-patterns-0.2.2 (c (n "patterns") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0z2p70zw23c2m2pxlqr6y5qmzfj4ddj7vm7j3ix5lzlpjaq9pdrf")))

(define-public crate-patterns-0.2.3 (c (n "patterns") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "084xjdagk20xihgsrczdd618nnyf5n6ix5s7cxrf9d9i5fkvza14") (y #t)))

(define-public crate-patterns-0.2.4 (c (n "patterns") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0k1q3c8zzgxjyk3p841mqnd1g6gk16w5b3vd5bjaf50mj0whb11m") (y #t)))

(define-public crate-patterns-0.2.5 (c (n "patterns") (v "0.2.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1nc9ricrbnbcvmvg5ld8h2r7jigm372akn1f406yfw4s24cd1hf6")))

