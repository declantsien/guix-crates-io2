(define-module (crates-io pa tt patternscanner) #:use-module (crates-io))

(define-public crate-patternscanner-0.0.0 (c (n "patternscanner") (v "0.0.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1p1rd381bimyyr53i2l7nxlizpcd2x7fra8ddkdj7609la4vf920")))

(define-public crate-patternscanner-0.1.0 (c (n "patternscanner") (v "0.1.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0cykwdwsy5yk7hhwc7x3b74k29r82z60qhacqf4s1rx5hhr6hybb")))

(define-public crate-patternscanner-0.2.0 (c (n "patternscanner") (v "0.2.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jnaxkm3cyrx2fl40zbnl3457yfap2h6yh3mrmy951hkpkf366pq")))

(define-public crate-patternscanner-0.3.0 (c (n "patternscanner") (v "0.3.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jxvrayydf9vr70zy54an5zhph7cglh3kj5aqasab1r1kqc4c262")))

(define-public crate-patternscanner-0.3.1 (c (n "patternscanner") (v "0.3.1") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zasgsfi2dljnxsva1cgdp0bzgccb5y7lzqbv5vr6qz5hhipqynj")))

(define-public crate-patternscanner-0.4.0 (c (n "patternscanner") (v "0.4.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "185m0rkyzcz8c91hzgaqrk894195xjv3q6kih0zrw5rb7w7d3asn")))

(define-public crate-patternscanner-0.5.0 (c (n "patternscanner") (v "0.5.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cq0lmnsj29wcw59gr5nnmp0dvmzah3j583dzsmmkmzbhl5047i3")))

