(define-module (crates-io pa tt patty) #:use-module (crates-io))

(define-public crate-patty-0.1.0 (c (n "patty") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_UI" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0yd7viczzdivrp5bq24i5pfibprbrssm8fgspykx1cf95r9y0jq6")))

(define-public crate-patty-0.1.1 (c (n "patty") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_UI" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1wi8sl4iwdg41lkk4sk6v62qv3pzvq0n7vdp0r745383l5mahkh6")))

