(define-module (crates-io pa tt pattern-compiler) #:use-module (crates-io))

(define-public crate-pattern-compiler-0.1.0 (c (n "pattern-compiler") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0rrqwr8qfwqvn3qv2c6g6ri9cpwr6n82iggzpfnl5aighf9sgzxc")))

