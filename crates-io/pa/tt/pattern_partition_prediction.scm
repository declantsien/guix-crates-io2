(define-module (crates-io pa tt pattern_partition_prediction) #:use-module (crates-io))

(define-public crate-pattern_partition_prediction-0.1.0 (c (n "pattern_partition_prediction") (v "0.1.0") (d (list (d (n "tabfile") (r "^0.2.1") (d #t) (k 0)))) (h "0ydr9j8pci16fjcchqv7gh9byqga708nsmzsxw2h5i17b4s6yk6j")))

(define-public crate-pattern_partition_prediction-0.1.1 (c (n "pattern_partition_prediction") (v "0.1.1") (d (list (d (n "tabfile") (r "^0.2.1") (d #t) (k 0)))) (h "1wxvvdy7mv4gpys6y5nn218w6p4w6dn2vc0qfqdfyyafy7ldc7jg")))

(define-public crate-pattern_partition_prediction-0.1.3 (c (n "pattern_partition_prediction") (v "0.1.3") (d (list (d (n "tabfile") (r "^0.2.1") (d #t) (k 0)))) (h "095c0l602682bgfr8pbs5fq43g1vx7jbpdcaasq85awzjc5rfann")))

(define-public crate-pattern_partition_prediction-0.1.4 (c (n "pattern_partition_prediction") (v "0.1.4") (d (list (d (n "tabfile") (r "^0.2.1") (d #t) (k 0)))) (h "0lhk727nwlbq9nld9f79pi6ajmkn9d0hhixka47pan1bzf165dhl")))

