(define-module (crates-io pa tt patternscan) #:use-module (crates-io))

(define-public crate-patternscan-0.1.0 (c (n "patternscan") (v "0.1.0") (h "0fr4pdjxi6vbs4i6jjp2f6jsvsyi5fwa24j2rpfxhhxj81ipikfd") (y #t)))

(define-public crate-patternscan-0.1.1 (c (n "patternscan") (v "0.1.1") (h "1f60j93j2pln4vrsld00zjcm4i9q7wh1x8fddarad3sjbia8yala")))

(define-public crate-patternscan-1.1.1 (c (n "patternscan") (v "1.1.1") (h "069q1md1kyz5fh9g1na8pr6q1i77q8a03d0507dx6m2wl8694qm2")))

(define-public crate-patternscan-1.2.0 (c (n "patternscan") (v "1.2.0") (h "1pmvsskfys9a2sdylv0cm70k2xh70mmhnzn57rs7ygbwmsaaryfb")))

