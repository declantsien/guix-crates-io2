(define-module (crates-io pa pe paper-mc) #:use-module (crates-io))

(define-public crate-paper-mc-0.1.0 (c (n "paper-mc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ys3ifpbfn8vb3bfb70r0wcy1zgycwf3hz1jns3s1a19p13gvmhr")))

