(define-module (crates-io pa pe paper_auto_update) #:use-module (crates-io))

(define-public crate-paper_auto_update-0.1.0 (c (n "paper_auto_update") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yq25zw8rfn50j66jk8qwqmc1bgs3z9mnbrzszj6dy1npdg40wv5") (y #t)))

