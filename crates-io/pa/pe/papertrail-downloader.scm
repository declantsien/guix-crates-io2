(define-module (crates-io pa pe papertrail-downloader) #:use-module (crates-io))

(define-public crate-papertrail-downloader-0.1.0 (c (n "papertrail-downloader") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("stream"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "fs"))) (d #t) (k 0)))) (h "03z83j654iah2y4nk43j8ya5dc4n8qk72r4wybs3njcnpp02pnj6")))

(define-public crate-papertrail-downloader-0.2.0 (c (n "papertrail-downloader") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "1fjy7yp0063fflzcmmmvfg4m74jacg794j5c39031l10l64bz6vv")))

