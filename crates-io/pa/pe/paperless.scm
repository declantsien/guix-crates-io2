(define-module (crates-io pa pe paperless) #:use-module (crates-io))

(define-public crate-paperless-0.1.0 (c (n "paperless") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex_color") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kck8ylbhw0jxf2l9fr74vlayh96d338mzssdqn8pnww08m57djx")))

