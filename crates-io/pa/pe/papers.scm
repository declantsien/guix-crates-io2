(define-module (crates-io pa pe papers) #:use-module (crates-io))

(define-public crate-papers-0.1.0 (c (n "papers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "config") (r "^0.9.2") (d #t) (k 0)) (d (n "crossref") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.7") (d #t) (k 0)) (d (n "multimap") (r "^0.4.0") (d #t) (k 0)) (d (n "orcid") (r "^0.1") (d #t) (k 0)) (d (n "pubmed") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "semanticscholar") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "wikibase") (r "^0.3") (d #t) (k 0)))) (h "0as0a7vfi292mcdd04v364pax5j33kn9h84ksj3011q05zl62ssz")))

