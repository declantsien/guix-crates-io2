(define-module (crates-io pa pe paper-latest) #:use-module (crates-io))

(define-public crate-paper-latest-0.1.0 (c (n "paper-latest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "attohttpc") (r "^0.16.1") (f (quote ("json" "tls"))) (k 0)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1m70jdnyhxd0if6gkva4pkih7zr1mn6jrcfb9r4s29wvha541hn7")))

(define-public crate-paper-latest-0.1.1 (c (n "paper-latest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "attohttpc") (r "^0.16.1") (f (quote ("json" "tls"))) (k 0)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "02yrx9z5c3xa7vbhnm0yy3sbr83iqgc8d67rb9xyls0ml9ljbs82")))

