(define-module (crates-io pa pe papergrid) #:use-module (crates-io))

(define-public crate-papergrid-0.1.0 (c (n "papergrid") (v "0.1.0") (h "00dnqc52dw429h1mzdggsc69q954k2bin3wqii4avwp39ngjmd83")))

(define-public crate-papergrid-0.1.1 (c (n "papergrid") (v "0.1.1") (d (list (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "0h7gfliy7rgziml4qc2yj4pir2jf8vlrqx7y323aylxxb5xyggp0")))

(define-public crate-papergrid-0.1.2 (c (n "papergrid") (v "0.1.2") (d (list (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "11l9c2aib3hk2nki0vln4dnyl5rhhlvm3q3gwwvpql6nwf2i34fz")))

(define-public crate-papergrid-0.1.3 (c (n "papergrid") (v "0.1.3") (d (list (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "00hb1g1n99xlk2kdz9yskl0ikj1faxbnrvc063lfj1qqc66nyhj9")))

(define-public crate-papergrid-0.1.7 (c (n "papergrid") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "console") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "0frkzj6b0rcs3bhq5xlg1zps2rm4f0an6nj7xjja43m7w4hjjsr5") (f (quote (("color" "console"))))))

(define-public crate-papergrid-0.1.8 (c (n "papergrid") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "1ry94wj95mxqmk2k5zrjhf91nsxyq0da1jzgfmx4b1hjgzvl86z2") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.9 (c (n "papergrid") (v "0.1.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "1m6wz5aazxnbysrq0p3ymx3aqwzi5bm5chrc9q84069lwdn7kgjm") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.10 (c (n "papergrid") (v "0.1.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "1xa0qpnaia5qigqwpvwysiq3sg30dcaq60fq0iijmjbrsl0ma4hx") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.11 (c (n "papergrid") (v "0.1.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "12ph68qqb69g3x0n7y73f06hc32swyyfczc9vvlvn9z1j8xqwy3l") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.13 (c (n "papergrid") (v "0.1.13") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.4") (d #t) (k 0)))) (h "1bi171l6qglbjbaamxvxfzp7adfkln3cgn7c10k0kljgbfdcd9zq") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.14 (c (n "papergrid") (v "0.1.14") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0wp3gd5fblzskbwkmqj3gr2pmns378nkppldpfsgr8ibvdvpdn8w") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.15 (c (n "papergrid") (v "0.1.15") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1483i8jcp4f81x5243alplrvyk5617yg02x4qz8zivhr133i3gbk") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.16 (c (n "papergrid") (v "0.1.16") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1ayr9k1g83sxnz4rhhx2vfl8gd0zcl7isrcczmxlqwwyzaxldjkq") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.17 (c (n "papergrid") (v "0.1.17") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "16vnhylcizzsg1lsh79km9cyql18hjlr1l7k9m2g3id67s5s13lb") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.18 (c (n "papergrid") (v "0.1.18") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1xawa14b1009fg2smjr98slxd183swxbpl1w2zy6bpp99naifiig") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.19 (c (n "papergrid") (v "0.1.19") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "16q6l9c0hzs2pmrif3g9kkn04jxhcqkarw9ihjgdc237mpxn3fwq") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.20 (c (n "papergrid") (v "0.1.20") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "02790h9i803x44m5z6zqkks4jxfsyaxdnkw9njd9r389fixwxpnh") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.21 (c (n "papergrid") (v "0.1.21") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1bvbdlskqwc3jmq7cah5ycxpy91x3pgwfrm7rkj0gmxvaihsnyhi") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.22 (c (n "papergrid") (v "0.1.22") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "13g1inn6ic12ymj0yfqb80a7clbjfjjxwdz5w7psqqyp5dbir386") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.1.23 (c (n "papergrid") (v "0.1.23") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1yyx5s3219bw69pzvwm5xg90h7lr996abvrz3z9akj37zhrkmsbg") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.2.0 (c (n "papergrid") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0crk48y1xgmdgzdnpvhi7pqn4qs45rkbakjak0f3fx58gc0kvzys") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.2.1 (c (n "papergrid") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "06dq4wnh2np86az8m49wni5br3l7gzk8k652z3j5dfvalfg5narc") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.3.0 (c (n "papergrid") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "12hvnq3vjf5zlkrxvc34004xx06f4y6jbn4ippvmiv62w889sw33") (f (quote (("color" "strip-ansi-escapes"))))))

(define-public crate-papergrid-0.4.0 (c (n "papergrid") (v "0.4.0") (d (list (d (n "ansi-str") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0ycyirwyljddxm0fi6mjvxflnjp36xhds1mxx0wymxgpmi2692v0") (f (quote (("color" "strip-ansi-escapes" "ansi-str"))))))

(define-public crate-papergrid-0.5.0 (c (n "papergrid") (v "0.5.0") (d (list (d (n "ansi-str") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1bqpx8ldi872ndxnyjs5v5rzbgl893ldacav46krdq22csphx64s") (f (quote (("color" "strip-ansi-escapes" "ansi-str"))))))

(define-public crate-papergrid-0.5.1 (c (n "papergrid") (v "0.5.1") (d (list (d (n "ansi-str") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "18hqlnb268lxprwj97wflnzcysbl9l6z6jqj39d4kbrp58gzfg25") (f (quote (("color" "strip-ansi-escapes" "ansi-str"))))))

(define-public crate-papergrid-0.6.0 (c (n "papergrid") (v "0.6.0") (d (list (d (n "ansi-str") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1644h6kvfil9bdr3130lb0w8ld579iqg8ib90lqnxaymh4jfv6xf") (f (quote (("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.6.1 (c (n "papergrid") (v "0.6.1") (d (list (d (n "ansi-str") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "19ziahlha2lgyyldc456dkgw9rbrmj2w9jjgm10g0zn803nhmavd") (f (quote (("color" "ansi-str" "ansitok")))) (y #t)))

(define-public crate-papergrid-0.7.0 (c (n "papergrid") (v "0.7.0") (d (list (d (n "ansi-str") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1a67c7vxfkb2ckg7jvf7rdvpgh8q6pxr2d07dli0hlqijhghx3sx") (f (quote (("color" "ansi-str" "ansitok")))) (y #t)))

(define-public crate-papergrid-0.7.1 (c (n "papergrid") (v "0.7.1") (d (list (d (n "ansi-str") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "00abpvzlfan17glph0ldczaq3pvyqli0ydhhzcww63pim5mbn9hm") (f (quote (("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.8.0 (c (n "papergrid") (v "0.8.0") (d (list (d (n "ansi-str") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "03p1n1g7l6vl5smh9rvcb1aapmk68jc4qqda4gd38f4ww66pz7qm") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.8.1 (c (n "papergrid") (v "0.8.1") (d (list (d (n "ansi-str") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1lamcs1z4hi1w6hq9klh4agkp8s1ln5k3alj9v4flpk8r603m5q1") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.8.2 (c (n "papergrid") (v "0.8.2") (d (list (d (n "ansi-str") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "04wvvvclkawzd90sclqyg5pcdrq9crrv6wq8csvajhknxlv9hp5i") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.9.0 (c (n "papergrid") (v "0.9.0") (d (list (d (n "ansi-str") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1cavmfw6smh3kmf9avndzyaxhy7asafcd3ypga455p0xql1ygpqz") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.9.1 (c (n "papergrid") (v "0.9.1") (d (list (d (n "ansi-str") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "141jx0n6iknn94x9hvfkcwmcfc3r8kk8y33rk11nx4lq4nr92y5f") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.10.0 (c (n "papergrid") (v "0.10.0") (d (list (d (n "ansi-str") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1j6hhk8lgzz53rzrlpxqrsq9gqi7cis445l7m7wn5nxny8avxk52") (f (quote (("std") ("default" "std") ("color" "ansi-str" "ansitok"))))))

(define-public crate-papergrid-0.11.0 (c (n "papergrid") (v "0.11.0") (d (list (d (n "ansi-str") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ansitok") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1yzppnq3v1ivwqbrp4f7b13ijxirfykb64072vwngxsf083krm4s") (f (quote (("std") ("default" "std") ("ansi" "ansi-str" "ansitok"))))))

