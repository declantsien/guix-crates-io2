(define-module (crates-io pa pe paperdoll-tar) #:use-module (crates-io))

(define-public crate-paperdoll-tar-0.0.1-alpha.0 (c (n "paperdoll-tar") (v "0.0.1-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "18a4j4ng7mdy39zy1v43594bglv5wn8kpff5kyckv4hlcklqisbi")))

(define-public crate-paperdoll-tar-0.0.1-alpha.1 (c (n "paperdoll-tar") (v "0.0.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "02c2brcbsk8kchawnpdrxjgwpp72qsvkpb79j0ggbmaaxwhi7b18")))

(define-public crate-paperdoll-tar-0.0.1-alpha.2 (c (n "paperdoll-tar") (v "0.0.1-alpha.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "17rjfa6y2yb3nszyj4mllqbrsi4rag4hsnjvh5gas8x0530pnavx")))

(define-public crate-paperdoll-tar-0.0.1-alpha.3 (c (n "paperdoll-tar") (v "0.0.1-alpha.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1qgyhnblffbn20shslkhiwgb9l7ldk8aw7vc56vz0ma7k7lp6wlw")))

(define-public crate-paperdoll-tar-0.0.1-alpha.4 (c (n "paperdoll-tar") (v "0.0.1-alpha.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1yk4fwnzzvq21wlr8dpj6q5vhznkwdnq01d7qxv3y1rdzsj4hicz")))

(define-public crate-paperdoll-tar-0.0.1-alpha.5 (c (n "paperdoll-tar") (v "0.0.1-alpha.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1rw415l3iwz9yv4lq23dz3simvz4mpbvmdzgkadqrjnlglk20rs4")))

(define-public crate-paperdoll-tar-0.0.1-alpha.6 (c (n "paperdoll-tar") (v "0.0.1-alpha.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "03qlmnvsyxznq2z726i9j4was0im0vlan5lvxp1jnqfbym3gsph2")))

(define-public crate-paperdoll-tar-0.0.1-alpha.7 (c (n "paperdoll-tar") (v "0.0.1-alpha.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0kmrvzkp4f6wab3z2mxpz7inyiyqlqssimxi49jdcy6ywnhf5s86")))

(define-public crate-paperdoll-tar-0.1.0 (c (n "paperdoll-tar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1jqqc0qwndf1q9gg6ldvpmics6xsbdk3gnw1x8s2qavd8d313m97")))

(define-public crate-paperdoll-tar-0.1.1 (c (n "paperdoll-tar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "paperdoll") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1g8wsf7zjcf9ql8lv4ipm0gw012gd0nhzjl7kg3avsyqb30gp12v")))

