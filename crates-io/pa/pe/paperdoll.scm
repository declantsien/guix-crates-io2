(define-module (crates-io pa pe paperdoll) #:use-module (crates-io))

(define-public crate-paperdoll-0.0.1-alpha.0 (c (n "paperdoll") (v "0.0.1-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vdzkp2357ynjp1l7jaiax9dyyqkwgyqga9k5nn6107psaghybd3")))

(define-public crate-paperdoll-0.0.1-alpha.1 (c (n "paperdoll") (v "0.0.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "171crdfl8agazgpiwc09s9cb3rrvzbnm8q5h51mrd1yip38n8rhq")))

(define-public crate-paperdoll-0.0.1-alpha.2 (c (n "paperdoll") (v "0.0.1-alpha.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fb1as4b9f5k6sqdcv6qa7fgxba5kk77k6lys5d7rafb0yg4963")))

(define-public crate-paperdoll-0.0.1-alpha.3 (c (n "paperdoll") (v "0.0.1-alpha.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kd1byj90hxj98mn38bwamn41jps064l9grzg9s17q9p7092asa6")))

(define-public crate-paperdoll-0.0.1-alpha.4 (c (n "paperdoll") (v "0.0.1-alpha.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q7p4ra1bspf9my9jq6asrhfksnz768yq8ka5dzi5g4rbg3j03a0")))

(define-public crate-paperdoll-0.0.1-alpha.5 (c (n "paperdoll") (v "0.0.1-alpha.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xrz31n5w2bmwm9f4pqxscyjfack4sq3ab7d6sy48wjjg5mh3xaq")))

(define-public crate-paperdoll-0.1.0 (c (n "paperdoll") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0izidwsf41235lqs1k7d9zsq87kiwdb14ahqj2rc70nmmlbfai7j")))

(define-public crate-paperdoll-0.1.1 (c (n "paperdoll") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ywj4zrx3287crbp41q0iygwkixrdrcv1l5a0ac6hqrqvj1047b")))

