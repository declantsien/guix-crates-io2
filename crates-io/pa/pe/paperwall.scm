(define-module (crates-io pa pe paperwall) #:use-module (crates-io))

(define-public crate-paperwall-0.1.0 (c (n "paperwall") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "terminal-log-symbols") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wallpaper") (r "^3") (f (quote ("from_url"))) (d #t) (k 0)))) (h "0jvpxcr08dx05pylf88ck64pdppzx9d4gzcvic4rnqw3nszkqry9")))

