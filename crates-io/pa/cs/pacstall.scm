(define-module (crates-io pa cs pacstall) #:use-module (crates-io))

(define-public crate-pacstall-0.1.0 (c (n "pacstall") (v "0.1.0") (h "1b3v07ha7bk1zm096zyxyxmy5qhlkci1bym8al8a1b9kgnp10l68")))

(define-public crate-pacstall-0.1.1 (c (n "pacstall") (v "0.1.1") (h "1rkmbqrlnryw7c22zdqgssrjv5igxpkm8k7w4g1qrf5sx0s2779b")))

