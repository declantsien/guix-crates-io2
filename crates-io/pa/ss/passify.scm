(define-module (crates-io pa ss passify) #:use-module (crates-io))

(define-public crate-passify-1.0.0 (c (n "passify") (v "1.0.0") (d (list (d (n "arboard") (r "^3.2") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0apr31gfy8yfdycsnlg5ksx85p21hsq5icnf83cimqa3gzb865w3")))

