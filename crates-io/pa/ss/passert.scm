(define-module (crates-io pa ss passert) #:use-module (crates-io))

(define-public crate-passert-0.1.0 (c (n "passert") (v "0.1.0") (h "1bgkbrvma5hrlkhbx43hccz1sdgi3hdzb9wzyjvn69nv1vx7lmdg")))

(define-public crate-passert-0.2.0 (c (n "passert") (v "0.2.0") (h "1a679f81mn0swa9rr2xr5bnck6i7gx3x0bv0mskhrmwx850xyry8")))

