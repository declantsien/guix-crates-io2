(define-module (crates-io pa ss passg-lib) #:use-module (crates-io))

(define-public crate-passg-lib-0.1.0 (c (n "passg-lib") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0bszj1dnj782w1hg7m6g9a1ml660b414fzjc6m5cr88j7ckp7qla")))

