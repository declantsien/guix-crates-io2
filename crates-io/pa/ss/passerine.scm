(define-module (crates-io pa ss passerine) #:use-module (crates-io))

(define-public crate-passerine-0.6.0 (c (n "passerine") (v "0.6.0") (h "0g0j3zh4fhayhdwm16idsmfcfcnd31k05p6s1m2a889apmshywy8")))

(define-public crate-passerine-0.6.1 (c (n "passerine") (v "0.6.1") (h "15pi4s1zrjfygwh9k9cngys5sjdxpjrc6j810qwsf5wqq7mrsp0i")))

(define-public crate-passerine-0.7.0 (c (n "passerine") (v "0.7.0") (h "0rgf4zfsra8fxa4z8j10bpzl9i9swsz2drl0vwhp6a3k7k9992yv")))

(define-public crate-passerine-0.7.1 (c (n "passerine") (v "0.7.1") (h "16hnklkzsrlbgqn96nm6lvj210skgq9zjj23caajbd4gqfzb7lbd")))

(define-public crate-passerine-0.7.2 (c (n "passerine") (v "0.7.2") (h "15pfkgbm0rr35ydkd4gkcbiynlj94czdljmz9xpsqfmb8i6wapc8")))

(define-public crate-passerine-0.8.0 (c (n "passerine") (v "0.8.0") (h "0qc9884ldmdlh5nzn9vpdhd95ah6k23n71w8i1f4236r7akjmc93")))

(define-public crate-passerine-0.8.1 (c (n "passerine") (v "0.8.1") (h "02xwinghvbqapb462mi8n9xbalm160h7kfqrca1zh0d371798b0p")))

(define-public crate-passerine-0.9.0 (c (n "passerine") (v "0.9.0") (h "08zn0w6ds1pmaz6szz0y9javly8a4pj0qzrh5j5djyqc8hk78wmg")))

(define-public crate-passerine-0.9.1 (c (n "passerine") (v "0.9.1") (h "1nam590hj3i75b8bblr4mx8xwnaga8c64dz675zfa8p2df0i33gk")))

(define-public crate-passerine-0.9.2 (c (n "passerine") (v "0.9.2") (h "0j5q6x58iqmbwmg70l3x9jll2wax7zpfy8kk6cfbm7n6q6c0p53q")))

(define-public crate-passerine-0.9.3 (c (n "passerine") (v "0.9.3") (h "0i1b4byrvhgkqplach796dpbflgz0qpakvkmrl1rq7nava4m00v9")))

