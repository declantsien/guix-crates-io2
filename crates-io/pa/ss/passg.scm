(define-module (crates-io pa ss passg) #:use-module (crates-io))

(define-public crate-passg-0.1.0 (c (n "passg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0p3pxpghl3pviflaz1ahn5ckg2anv18lb51xl27x6h0ikvzah0mw")))

(define-public crate-passg-0.2.0 (c (n "passg") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "1k1n6zmc9gymjmsd0ssqq4yl847sfvdriyzjwpmk8shw6h74zk05")))

