(define-module (crates-io pa ss passwd-rs) #:use-module (crates-io))

(define-public crate-passwd-rs-0.1.0 (c (n "passwd-rs") (v "0.1.0") (h "107mxq09ifkn2h9xlzz3ylgpd3im3w95lsmfpl4k9zc6ahg217w9")))

(define-public crate-passwd-rs-0.2.0 (c (n "passwd-rs") (v "0.2.0") (h "0730bblhn5003ig7rajhwil7lcj524k23nbf7pgj7wvlni5sy9qs")))

