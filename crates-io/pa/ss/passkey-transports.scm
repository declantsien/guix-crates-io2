(define-module (crates-io pa ss passkey-transports) #:use-module (crates-io))

(define-public crate-passkey-transports-0.0.1 (c (n "passkey-transports") (v "0.0.1") (h "0vmi2y829ygj5kpx2skvy2726lwzfmchq783ha3p76ianlwqv51c")))

(define-public crate-passkey-transports-0.1.0 (c (n "passkey-transports") (v "0.1.0") (h "1fygvvh31vwqmpm5lr0ar7cl54lq10jkl5841ycpfdf7ml7i214f")))

