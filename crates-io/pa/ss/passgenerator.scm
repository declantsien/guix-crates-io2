(define-module (crates-io pa ss passgenerator) #:use-module (crates-io))

(define-public crate-passgenerator-0.1.0 (c (n "passgenerator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0m8vv4q2pwxdj5gks97zif4q9jnkwgnkrazf92ia33lbq9phdgiv")))

(define-public crate-passgenerator-0.1.1 (c (n "passgenerator") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nglvhid6q516rimdc45pb6jwgymbiz4234c42j31bdk10qpaa6f")))

