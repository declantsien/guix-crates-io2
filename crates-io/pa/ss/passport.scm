(define-module (crates-io pa ss passport) #:use-module (crates-io))

(define-public crate-passport-0.1.0 (c (n "passport") (v "0.1.0") (h "1cnax8hf2d1aicrs8ink7wbr4xqsy495a3s4qm8p9dq0crd8zinl") (y #t)))

(define-public crate-passport-0.3.0 (c (n "passport") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0dc46w34lpqc23amw680k4zxf2q62jpdmvbzng5gncp9b4xvqn4z")))

(define-public crate-passport-0.3.1 (c (n "passport") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1ls15k5dr3fm4wjdcfkywfiapv0ynzl5xrr4ms3qmgm91bb4db6n")))

(define-public crate-passport-0.3.2 (c (n "passport") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0sdpkfx52r6y4xxib3j3lgdrygqyvc2llkvikxn6p7zg1ri318nh")))

(define-public crate-passport-0.3.3 (c (n "passport") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1ylaj3d5dddnkwkybm973fx67p0hs4n4wvfn2kd7z3ia3k9flbda")))

(define-public crate-passport-0.3.4 (c (n "passport") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0jalixxygq40qpgnmd4qcs6v16dwnrpy2j1v8hpkawvsm2ig82g8")))

(define-public crate-passport-0.4.0 (c (n "passport") (v "0.4.0") (d (list (d (n "jsonwebtoken") (r "^8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1qdqgwb8339i23lib23aqmxmrjn5wlx1z053wh1bvbzz5jgpjm8p")))

