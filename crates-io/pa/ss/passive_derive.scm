(define-module (crates-io pa ss passive_derive) #:use-module (crates-io))

(define-public crate-passive_derive-0.1.0 (c (n "passive_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "1czkgmanvh06g83l5fixhj20fhc9dqayyfxn0x1q0iqzm6jwcfdd") (y #t)))

(define-public crate-passive_derive-0.1.1 (c (n "passive_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "1k5rdizasz8r3fjb51icyi8s0ycvf1cqwyska4h3ap1b8276m3si")))

