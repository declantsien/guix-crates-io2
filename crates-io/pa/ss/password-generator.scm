(define-module (crates-io pa ss password-generator) #:use-module (crates-io))

(define-public crate-password-generator-0.1.0 (c (n "password-generator") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "passwords") (r "^3.1.9") (d #t) (k 0)))) (h "1ny25pb61rszgl3jkkzbvdddk0gwk8qsmcgyj0hjimv8qwvp529z")))

