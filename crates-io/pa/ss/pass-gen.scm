(define-module (crates-io pa ss pass-gen) #:use-module (crates-io))

(define-public crate-pass-gen-0.1.0 (c (n "pass-gen") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00sk3z8h3kwh2qa8cs51x07170y40j93mnx5wgkrcw0xrplm1h43")))

(define-public crate-pass-gen-1.0.0 (c (n "pass-gen") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1q8j673mxdqmgccan0wk5whkw0lff0qpgs4aiscqw3vvpsb9vf0w")))

(define-public crate-pass-gen-1.0.1 (c (n "pass-gen") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vyqf5lw6viak96726z824iv8vx540iav3vvsbppwwkf12hwc55x")))

(define-public crate-pass-gen-1.0.2 (c (n "pass-gen") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xad4zsrwizmx37ys2hiv13sc1i26q6hn4m3y427gn1mafys2k5d")))

(define-public crate-pass-gen-1.0.3 (c (n "pass-gen") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "035zgfifg2l6hfd4qfz8l9pg6yiyzj3nbhdhchbqxrvf76sparx4")))

(define-public crate-pass-gen-1.0.4 (c (n "pass-gen") (v "1.0.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1j9dmca9cpfx3a8ixqm71lxj0b5pa648446asx3bp216b9n0hkc3")))

