(define-module (crates-io pa ss passdata) #:use-module (crates-io))

(define-public crate-passdata-0.0.1 (c (n "passdata") (v "0.0.1") (d (list (d (n "either") (r "^1.9.0") (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (k 0)))) (h "13jr63h977rsk8d30bjw210sf5wfbk628b1chgk08sy45q4hnvli") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-passdata-0.0.2 (c (n "passdata") (v "0.0.2") (d (list (d (n "either") (r "^1.9.0") (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (k 0)))) (h "0l4rzax09sp0bpdx87p6db2ljwjn5cl7nmm052xwlhb91hhkk507") (f (quote (("std") ("default" "std") ("alloc"))))))

