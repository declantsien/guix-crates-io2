(define-module (crates-io pa ss password-as-service) #:use-module (crates-io))

(define-public crate-password-as-service-0.1.1 (c (n "password-as-service") (v "0.1.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1sypqqvv2m7pxzcd0k16wklw6ay77gxdx7vgby0ba1g5090b793w")))

(define-public crate-password-as-service-0.1.2 (c (n "password-as-service") (v "0.1.2") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0ma8d5cvsim45nvs3h1p3dabgl3v1a4ia99y1wxjkc3fap096fkr")))

(define-public crate-password-as-service-0.1.3 (c (n "password-as-service") (v "0.1.3") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0lwj267k3rq5safqqws21vsgdd175z3k1p3ip8db8hpq6fzbs520")))

(define-public crate-password-as-service-0.1.4 (c (n "password-as-service") (v "0.1.4") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "17gmqnf4zbw1rmmw30if22jcpzin46d00fkk6c7g78in546vyspk")))

