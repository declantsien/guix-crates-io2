(define-module (crates-io pa ss passgenr) #:use-module (crates-io))

(define-public crate-passgenr-0.1.0 (c (n "passgenr") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0sz6mnp352xhdbd0ahl3h4zrxp9wahrgsfi9d8lv8gaf7s3pinxq")))

(define-public crate-passgenr-0.2.0 (c (n "passgenr") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1zypjls2laliykik7xh9fq6vx37r05a8v8b62ryky99a3w6sy1dj")))

