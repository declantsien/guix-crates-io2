(define-module (crates-io pa ss passive) #:use-module (crates-io))

(define-public crate-passive-0.1.0 (c (n "passive") (v "0.1.0") (h "0mjyf2v9b8j91fb646ayd3dc2aw5dj5x0f67lr7bbxflwnqwmkbm") (y #t)))

(define-public crate-passive-0.1.1 (c (n "passive") (v "0.1.1") (d (list (d (n "derive") (r "^0.1") (o #t) (d #t) (k 0) (p "passive_derive")))) (h "0qrm50pdk2iaa7x1mh2p2y24fh63p5a8vcc00kh20gj7lyyd1rs0") (y #t)))

(define-public crate-passive-0.1.2 (c (n "passive") (v "0.1.2") (d (list (d (n "derive") (r "^0.1.1") (o #t) (d #t) (k 0) (p "passive_derive")))) (h "1rngp0lwyj42zqz08b315cwxhsa5rclpz363kw8bphp05nj018bm")))

(define-public crate-passive-0.1.3 (c (n "passive") (v "0.1.3") (d (list (d (n "derive") (r "^0.1.1") (o #t) (d #t) (k 0) (p "passive_derive")))) (h "06h300526rs6bnck6n3909zpk77i2lnyr60a4nfl4whhadi88qwv")))

(define-public crate-passive-0.1.4 (c (n "passive") (v "0.1.4") (d (list (d (n "derive") (r "^0.1.1") (o #t) (d #t) (k 0) (p "passive_derive")))) (h "1qn1wr46hgv0ixx5hgkm8a58wk68sj109nap7kdrmmfzamvnqk9a")))

