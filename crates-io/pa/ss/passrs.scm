(define-module (crates-io pa ss passrs) #:use-module (crates-io))

(define-public crate-passrs-0.5.3 (c (n "passrs") (v "0.5.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.8") (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "passwords") (r "^3.1") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)))) (h "1bqfa16bnz042cvcx38lrp722zbhyibx2msynclmq7qypah410ww")))

(define-public crate-passrs-0.5.4 (c (n "passrs") (v "0.5.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.8") (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "passwords") (r "^3.1") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)))) (h "1dmilqr3jp2j0whwkjjazkn0nci19dpqf3y4cc67fwvx5cl93pm9")))

