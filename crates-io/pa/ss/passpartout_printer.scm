(define-module (crates-io pa ss passpartout_printer) #:use-module (crates-io))

(define-public crate-passpartout_printer-1.0.0 (c (n "passpartout_printer") (v "1.0.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "device_query") (r "^0.1.0") (d #t) (k 0)) (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r30xhawch92g4b8r5mdkkdhqsygcbb1bk6fbzk75mi4ayy2002l")))

(define-public crate-passpartout_printer-1.1.0 (c (n "passpartout_printer") (v "1.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "device_query") (r "^1.1") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02pagb9mbsldbv2a88ns1l99fzqs37cpxz6zpmy9a5qg7spxq14g") (y #t)))

(define-public crate-passpartout_printer-1.1.1 (c (n "passpartout_printer") (v "1.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "device_query") (r "^1.1") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bdav160sqhdd4apdhl8xjpsvhbgz7wqpx1gcghlba2z6nkan4m3")))

