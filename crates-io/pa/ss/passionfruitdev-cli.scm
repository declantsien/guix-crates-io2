(define-module (crates-io pa ss passionfruitdev-cli) #:use-module (crates-io))

(define-public crate-passionfruitdev-cli-0.1.0 (c (n "passionfruitdev-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ci4khp6d03da0vhpfvsvm6h4alhcf88aj0gvd7n865j9h2jqyjw")))

(define-public crate-passionfruitdev-cli-0.1.1-2023.11.3.0.54.44+71d402eadf8c66a163f9b9f0b093aac4ff1af1f0 (c (n "passionfruitdev-cli") (v "0.1.1-2023.11.3.0.54.44+71d402eadf8c66a163f9b9f0b093aac4ff1af1f0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "071wgc973519xygfy0s7xdf2c2ys76zr9x73rac4wszr67rinj9y")))

(define-public crate-passionfruitdev-cli-0.1.1-2023.11.3.1.27.14+cfc4596f7957bbbef2675df0cc658ff9506d2fd8 (c (n "passionfruitdev-cli") (v "0.1.1-2023.11.3.1.27.14+cfc4596f7957bbbef2675df0cc658ff9506d2fd8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "128ndkpqnyzqjn1fk1air4265sm4d7xfrbxlk0xdvl3xiir17c9b")))

(define-public crate-passionfruitdev-cli-0.0.1-test (c (n "passionfruitdev-cli") (v "0.0.1-test") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "1zzb6vx28jhpw4wiij29rpwwaq0xr5f6d7p1ga4zighy97w26g3k") (y #t)))

(define-public crate-passionfruitdev-cli-0.0.1-test-nightly.20231103201430+e0ba55439a7d1713c650180413389a989ed54cc5 (c (n "passionfruitdev-cli") (v "0.0.1-test-nightly.20231103201430+e0ba55439a7d1713c650180413389a989ed54cc5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "15yva4mhw25a49a28kn7j4v4f2hfprci9cirx2hwb5dk3mbnqlrs")))

(define-public crate-passionfruitdev-cli-0.1.2-nightly.20231103201542+4bb37299548d0d4350953b0a9f2e0db25f138ffc (c (n "passionfruitdev-cli") (v "0.1.2-nightly.20231103201542+4bb37299548d0d4350953b0a9f2e0db25f138ffc") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "11s6ar0i6vv8vr1wimyc1l10kmf197wjvw0hcpdhnw8mn3w32rwx")))

(define-public crate-passionfruitdev-cli-0.1.2-official.20231103202139+913de377d09a26f31c1802bbc8608a7634bd5a67 (c (n "passionfruitdev-cli") (v "0.1.2-official.20231103202139+913de377d09a26f31c1802bbc8608a7634bd5a67") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "0cbiydafsqi3f14lpdpshbcsf600iwllj7nyjkvvc0x6q3hr94l2")))

(define-public crate-passionfruitdev-cli-0.1.2 (c (n "passionfruitdev-cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.7") (d #t) (k 0)))) (h "1s48wygair4j29533040z50ci4s5n90wiq8787icjnsffsrlp3k3")))

