(define-module (crates-io pa ss passcod-networkmanager) #:use-module (crates-io))

(define-public crate-passcod-networkmanager-0.5.0 (c (n "passcod-networkmanager") (v "0.5.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0yvcsgih42k89sca00vnsajvnx5c49axgqrssxmqswa0x9gng1y8")))

(define-public crate-passcod-networkmanager-0.5.1 (c (n "passcod-networkmanager") (v "0.5.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0kw1vyzh0qx52irl330kzad7zfziirv0jzkb5zrm8rfis5synnwa")))

(define-public crate-passcod-networkmanager-0.5.2 (c (n "passcod-networkmanager") (v "0.5.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mwbzn2li2jl5iwzka4q1b2m79h4i8pn5y0mm8k2p5ldnvw6l2ga")))

(define-public crate-passcod-networkmanager-0.5.3 (c (n "passcod-networkmanager") (v "0.5.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1159h9d0rxzi8kdxjaxkqrqcp7j15n9hhrbhazrw267rsr2a1mkw")))

(define-public crate-passcod-networkmanager-0.6.0 (c (n "passcod-networkmanager") (v "0.6.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0grm9pdmfd1wik24xcnp4xf82iw427p0apgp33v6vjm7905zrgag")))

(define-public crate-passcod-networkmanager-0.6.1 (c (n "passcod-networkmanager") (v "0.6.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1i0mbkhp8q5lq1yccxkj6gah8y0wxx2czn3prqkp4lsaiqxhq6i3")))

(define-public crate-passcod-networkmanager-0.7.0-pre.1 (c (n "passcod-networkmanager") (v "0.7.0-pre.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "0khi0z51bhxxf0b25p7djxxmq2cf2ikg2pk6d65di9x69pwv3nbb") (f (quote (("raw") ("default"))))))

