(define-module (crates-io pa ss passt) #:use-module (crates-io))

(define-public crate-passt-0.1.0 (c (n "passt") (v "0.1.0") (h "1cdhm0y4aqg5m53hi2xiv0rw5y1x8gs4cysq1wwyxbl6l4c6irk4")))

(define-public crate-passt-0.2.0 (c (n "passt") (v "0.2.0") (h "0la8jxlpzz7kxhp17zs8b9c0fcdsxc536mfn93zq463ppxd85axb")))

(define-public crate-passt-0.3.0 (c (n "passt") (v "0.3.0") (h "095bbc8l2jc4myi6jz8b1ilz428xx5rv5j03bl4shfbzx5f2l90k")))

