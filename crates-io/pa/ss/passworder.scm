(define-module (crates-io pa ss passworder) #:use-module (crates-io))

(define-public crate-passworder-0.1.0 (c (n "passworder") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14v8j6dsw9iv9dfb97kydy6qp32g29bf09h1amiqvlxsgxn6kkkj") (y #t)))

(define-public crate-passworder-0.1.1 (c (n "passworder") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0i3xf6mipz68plam0xlhzz6a7p9cx8rkfdjraglyd2pa99w2ma3i") (y #t)))

(define-public crate-passworder-0.1.2 (c (n "passworder") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02zk3vr1rhm84hhi8d375l0qv8w00ny4vb0x1a8hg8lq8bkyk94r") (y #t)))

(define-public crate-passworder-0.1.3 (c (n "passworder") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09ph04r3v9ixv89ih152fxlajnl1vblag8sawfdsq3jnsidbayma")))

