(define-module (crates-io pa ss password-gen) #:use-module (crates-io))

(define-public crate-password-gen-0.1.0 (c (n "password-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kxkynzyipz7hdy6xlbrczzxribsj8538vj18dhi2vyzibfr8qpa")))

(define-public crate-password-gen-0.1.1 (c (n "password-gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06xrrn2b4fd0wrh61mc2cb7p8yjgr54f6mwngjd8mp7sb9r572rr")))

(define-public crate-password-gen-0.1.2 (c (n "password-gen") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f5j58s1rm4q4ida1kzbgbkgx0wnqvvw5kv87jf4mynq1kjmbr9r")))

(define-public crate-password-gen-0.1.3 (c (n "password-gen") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bvw6v4ka0q57mclic098m05z79kizpzj7549v61bsd26sdg2vgn")))

(define-public crate-password-gen-1.0.0 (c (n "password-gen") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0l8fhbh4y04x7abpds3sym04qkcdjs69xzrqa4mk8phs63x2441q") (f (quote (("cereal" "serde/derive"))))))

(define-public crate-password-gen-2.0.0 (c (n "password-gen") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0xdgq7r4fmrhi4xmwmna37nl9d52r7vvqkq4k3ffdp46bv82y5hg") (f (quote (("cereal" "serde/derive"))))))

(define-public crate-password-gen-2.0.1 (c (n "password-gen") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0dzlh4g8jj87sbyr2k5a77ph7y0bc1kmv1rh9rx5g3shn4z34xw4") (f (quote (("cereal" "serde/derive"))))))

(define-public crate-password-gen-2.0.2 (c (n "password-gen") (v "2.0.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "07jfs97jqr7nmkpdr1axqqx4idi2rcbqdmfily7s62j4jd6f6395") (f (quote (("cereal" "serde/derive"))))))

(define-public crate-password-gen-2.1.0 (c (n "password-gen") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0cvsbv78c56qgv3ikpkn3svcz2ykm38zq13l5m882xbawjqd33bj") (f (quote (("cereal" "serde/derive"))))))

