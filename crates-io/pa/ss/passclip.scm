(define-module (crates-io pa ss passclip) #:use-module (crates-io))

(define-public crate-passclip-0.1.0 (c (n "passclip") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "13dm5qkqqs27agmx3f171hlbj0n0824nnhjadwvjdkqya2a1af1y")))

(define-public crate-passclip-0.1.1 (c (n "passclip") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0y20r3bxfpq10d3m51paxbxxw8ixhmklq2na5ix4br44m2kifnf4")))

(define-public crate-passclip-0.1.2 (c (n "passclip") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "14b5dkmvv9dvk3jjrk5dq3hbn4yr2lfjy5had1ghzw95hn2qr6mk")))

(define-public crate-passclip-0.1.3 (c (n "passclip") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1qi2075vbmn02z456vrlilrdghblh7gafnrqlzcipwx46vq9c33r")))

(define-public crate-passclip-0.1.4 (c (n "passclip") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1ks3in3i8l1ms8061bsyg4m433sy340jaksvv91lqml66pxp8938")))

