(define-module (crates-io pa ss passivized_vault_client_versions) #:use-module (crates-io))

(define-public crate-passivized_vault_client_versions-0.0.8-alpha (c (n "passivized_vault_client_versions") (v "0.0.8-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2") (d #t) (k 2)))) (h "1jspabfm7bn0lr6s0y36n9rigr9hrb3gjh53zgxia4axd16bkw8k")))

(define-public crate-passivized_vault_client_versions-0.0.8 (c (n "passivized_vault_client_versions") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2") (d #t) (k 2)))) (h "18z8nm8ryihaddzjbbh1dn1bfi2fj7lb5jic0rf5ag6d4gvn0128")))

(define-public crate-passivized_vault_client_versions-0.0.9 (c (n "passivized_vault_client_versions") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "0ra6fyd38lqfpisn1j2zhyjdm4v3iwj54gb9g4q5ggp9h426hfwc")))

(define-public crate-passivized_vault_client_versions-0.0.10 (c (n "passivized_vault_client_versions") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.1") (d #t) (k 2)))) (h "1nq6qqwy7gcpc8i6sis9zckc1mih5703c3pggpi72r8njsd5sm7w")))

