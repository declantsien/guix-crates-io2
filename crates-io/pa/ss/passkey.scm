(define-module (crates-io pa ss passkey) #:use-module (crates-io))

(define-public crate-passkey-0.0.1 (c (n "passkey") (v "0.0.1") (h "0zla8irjvysca466rrj0l1vi36nwnd3jq1ba2b8421ypkc5nlarj")))

(define-public crate-passkey-0.1.0 (c (n "passkey") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "coset") (r "^0.3") (d #t) (k 2)) (d (n "passkey-authenticator") (r "^0.1.0") (d #t) (k 0)) (d (n "passkey-client") (r "^0.1.0") (d #t) (k 0)) (d (n "passkey-transports") (r "^0.1.0") (d #t) (k 0)) (d (n "passkey-types") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 2)))) (h "11qbb4iar8426aid822sk9rhi7fqph0ns55qkfi4nwrxg517lihm")))

(define-public crate-passkey-0.2.0 (c (n "passkey") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "coset") (r "^0.3") (d #t) (k 2)) (d (n "passkey-authenticator") (r "^0.2") (d #t) (k 0)) (d (n "passkey-client") (r "^0.2") (d #t) (k 0)) (d (n "passkey-transports") (r "^0.1") (d #t) (k 0)) (d (n "passkey-types") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 2)))) (h "0g0yvicvprks4k7a9v89jyxh7j4vyzaib7rw6vmd9h547a95r7iy")))

