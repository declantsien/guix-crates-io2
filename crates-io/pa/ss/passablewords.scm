(define-module (crates-io pa ss passablewords) #:use-module (crates-io))

(define-public crate-passablewords-0.1.0 (c (n "passablewords") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.6") (d #t) (k 0)) (d (n "zxcvbn") (r "^0.4.3") (d #t) (k 0)))) (h "1lppwq1q59w3wbwj0qcix3wfp84vl86xl75788h1zkqyn1ww96k3")))

(define-public crate-passablewords-0.1.1 (c (n "passablewords") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.6") (d #t) (k 0)) (d (n "zxcvbn") (r "^0.4.3") (d #t) (k 0)))) (h "02q78zbj3fmx0ilfvxmqf8b4yy4y2298r7vpb0hh8mwdxajb3593")))

(define-public crate-passablewords-1.0.0 (c (n "passablewords") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^0.6") (d #t) (k 0)))) (h "1wxq0fcjw1y9bpmmp509mdsnfkzs8s4qxn9q2k56h45769516n9m")))

(define-public crate-passablewords-1.0.1 (c (n "passablewords") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "zxcvbn") (r "^0.6") (d #t) (k 0)))) (h "1ivfr9fx9a47pdl9dzxkpzz015waz19g2wzxngijls17s59s256n")))

