(define-module (crates-io pa ss passport-strategies) #:use-module (crates-io))

(define-public crate-passport-strategies-0.1.0 (c (n "passport-strategies") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "18axvbp966rnhf8vr149l076ij56yplsshq7hh5wr01lvc1crbk1")))

(define-public crate-passport-strategies-0.1.2 (c (n "passport-strategies") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "0rj2lqdajbbr5cvaa9c2p42sajwfmshq14q18za1c3g26pdgkb18")))

(define-public crate-passport-strategies-0.1.4 (c (n "passport-strategies") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "1k2nlln6xf9m8sbcq842s09fh55nlig344v2v74nwg3vy7yrv4ik")))

(define-public crate-passport-strategies-0.1.5 (c (n "passport-strategies") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "160pp9zig12nk3fs0vqdd1k0hr1njamxy6s02cdsyx2paxcyddz4")))

