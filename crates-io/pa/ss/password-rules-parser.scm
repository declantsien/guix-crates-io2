(define-module (crates-io pa ss password-rules-parser) #:use-module (crates-io))

(define-public crate-password-rules-parser-1.0.0 (c (n "password-rules-parser") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "pretty-lint") (r "^0.1") (d #t) (k 0)))) (h "1kpy45w409wjkcmxfsb41z03scfxixnbza4qy7nd2gqrhqsps3nq")))

(define-public crate-password-rules-parser-1.0.1 (c (n "password-rules-parser") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty-lint") (r "^0.1") (d #t) (k 0)))) (h "1jr1iki8n3y9jcrasgxz2j1rywrnirjhmihi395k88b3nnb57ibc")))

(define-public crate-password-rules-parser-1.0.2 (c (n "password-rules-parser") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.8") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty-lint") (r "^0.1") (d #t) (k 0)))) (h "1z1yy82h74iw5inj6sk32rkzbqrr7qi0lnf37mwmps72lwkndgdc")))

(define-public crate-password-rules-parser-1.0.3 (c (n "password-rules-parser") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.8") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty-lint") (r "^0.1") (d #t) (k 0)))) (h "04ld5xyj7204v07gw8iy3mxg9q2z62gk2ygbnz40yn26jg6whlja")))

