(define-module (crates-io pa ss passwd_gen) #:use-module (crates-io))

(define-public crate-passwd_gen-0.1.0 (c (n "passwd_gen") (v "0.1.0") (d (list (d (n "arboard") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fvfi22y4jbq70nfifd00hd1qi40ckc015wm4sba3j1yaq51xhjs")))

(define-public crate-passwd_gen-0.1.1 (c (n "passwd_gen") (v "0.1.1") (d (list (d (n "arboard") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1slc2gd3l0mx8n0iy7g0zgyr5k6ypw955jq7gbdmmrv0s9wfgp00")))

