(define-module (crates-io pa ss passterm) #:use-module (crates-io))

(define-public crate-passterm-1.0.0 (c (n "passterm") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "001pa8iw4mvmdbyw9i44sjdslrxgbrl1fnhlypy61j08xhahz47s")))

(define-public crate-passterm-1.0.1 (c (n "passterm") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y21qdkx30g6r97a6krj2gd2lrib2li1mfnx6vkgjscn12v2m1wx")))

(define-public crate-passterm-1.0.2 (c (n "passterm") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dgyk8wdbrywda690k8nd98kwclhnm5vl8n17c79yxvsd4rs3qcz")))

(define-public crate-passterm-1.0.3 (c (n "passterm") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xi3rabdnm87g5mf727nfmpi2grgh618v3mzs780d8bkxy2ykbl4")))

(define-public crate-passterm-1.1.0 (c (n "passterm") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pyy4n3i2pd0xhs3xvdd8i11b7yllgb1cgx8ixgbdsr1f2hacmch")))

(define-public crate-passterm-1.1.1 (c (n "passterm") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.30.0") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nnrka1lixpsb86cvmy18hm7k93c4zcr4vn45kr228zr5yp1x1ws")))

(define-public crate-passterm-1.1.2 (c (n "passterm") (v "1.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xbasw9l42i6r0zy3471r4sic2bhmaazpyiiwh2bi2idsr2ybm9w")))

(define-public crate-passterm-1.1.3 (c (n "passterm") (v "1.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.32") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kglck53wxyrvc17w71zkdg86m16xvgc9c8b8c9vvwhp2inzzzy3")))

(define-public crate-passterm-1.1.4 (c (n "passterm") (v "1.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.37") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11gr1rih76qb7xnar5zsmirjciqqrwrwabk7mlajn62nwndiyvj4")))

(define-public crate-passterm-1.1.5 (c (n "passterm") (v "1.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.37") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mr4iazwsyfpmm7phi1n7kdfcnjq0pisq0w0p4ak4kxwgp3njv87")))

(define-public crate-passterm-1.1.6 (c (n "passterm") (v "1.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.37") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bv0a8g5d1k82vzlf9a74hsh9ndymcd9qa6wsq3h1pphj5fvsx1w")))

(define-public crate-passterm-1.1.7 (c (n "passterm") (v "1.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.44") (f (quote ("Win32_System_Console" "Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ijcq1v06hdj9xgq4irhlws7j4v18s0kd521z22i7l9xpg4qvqdx")))

(define-public crate-passterm-2.0.0 (c (n "passterm") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_Storage_FileSystem" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "zeroize") (r "^1.6") (o #t) (d #t) (k 0)))) (h "05rsnxhq1li8v8lwlyp5khp39lvgjj8lcwqd04wwp19bhpgjj30h") (f (quote (("secure_zero" "zeroize") ("default"))))))

(define-public crate-passterm-2.0.1 (c (n "passterm") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_Storage_FileSystem" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "zeroize") (r "^1.6") (o #t) (d #t) (k 0)))) (h "18jzvqwpg64l7fqvvc8fd8qh8gismlh4hgnpvq0n98xs8l2548b4") (f (quote (("secure_zero" "zeroize") ("default"))))))

(define-public crate-passterm-2.0.2 (c (n "passterm") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_Storage_FileSystem" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0s30scf4rcw0yfnwwgflry1h278p5rxpy2xvvzmlxwlb5nqmj5bx") (f (quote (("secure_zero" "zeroize") ("default"))))))

(define-public crate-passterm-2.0.3 (c (n "passterm") (v "2.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "169brlsb50m3nl9bv9qqssp2hpyxh2q2hpya60icacna3fcfi9zf") (f (quote (("secure_zero" "zeroize") ("default"))))))

