(define-module (crates-io pa ss pass_tool) #:use-module (crates-io))

(define-public crate-pass_tool-0.1.0 (c (n "pass_tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1bzzjxgbbxf4qn9fiacb0qhffzm2pn042949drp5al4ack3mc6s7")))

(define-public crate-pass_tool-0.2.0 (c (n "pass_tool") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1pq5jjsi31p1jykqmsg11zck6b8cbv2j6fc8bwlhw1lvaf0xn22f")))

(define-public crate-pass_tool-0.3.0 (c (n "pass_tool") (v "0.3.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0izppjcpxip4fmnkxc7iavwzhxkknwzv7xbzywvnd3v1pr5fbqjf")))

(define-public crate-pass_tool-0.3.1 (c (n "pass_tool") (v "0.3.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0i4k1a8pdd89z2s1xi7z3rwnlc7a6npan50iy5ndwkzw4hykp4kx")))

(define-public crate-pass_tool-0.3.4 (c (n "pass_tool") (v "0.3.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1fr4qfany78849dif88bfdvqgh49109fvrhf5w900vp30cyjjfcv")))

(define-public crate-pass_tool-0.3.6 (c (n "pass_tool") (v "0.3.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("user"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1bif3n72g4kx48yshy2j9c4dyb41gfadbc0a432kay4i5mh0gdg5")))

