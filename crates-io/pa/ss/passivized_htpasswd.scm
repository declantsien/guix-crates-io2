(define-module (crates-io pa ss passivized_htpasswd) #:use-module (crates-io))

(define-public crate-passivized_htpasswd-0.0.1 (c (n "passivized_htpasswd") (v "0.0.1") (h "1fgkyg5mcimx19r2zmjj8n7qmybf8i92yl99wivrwh87r2dhnjwh") (r "1.64")))

(define-public crate-passivized_htpasswd-0.0.2 (c (n "passivized_htpasswd") (v "0.0.2") (d (list (d (n "bcrypt") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03px91c1b35vy9f9ngcnsdp5ni0vq8miv8dwd4q6lqzcg77myd8z") (r "1.60")))

(define-public crate-passivized_htpasswd-0.0.3 (c (n "passivized_htpasswd") (v "0.0.3") (d (list (d (n "bcrypt") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "pwhash") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jl6czy45bwf0wnglqk3avigcgg4r0bwvqqgb814gawha0b15wwb") (r "1.60")))

(define-public crate-passivized_htpasswd-0.0.5 (c (n "passivized_htpasswd") (v "0.0.5") (d (list (d (n "bcrypt") (r "^0.15") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "pwhash") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "~3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s36637hzpc5bh0nhml7b0cjqzl8by5aqd2hlyg1kf9qdhi1kpdd") (r "1.68")))

(define-public crate-passivized_htpasswd-0.0.6 (c (n "passivized_htpasswd") (v "0.0.6") (d (list (d (n "bcrypt") (r "^0.15") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "pwhash") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mdbzjhl5q4z9ly08gp92835qn75yqvdlkryjn70hv7aha2r10px") (r "1.68")))

