(define-module (crates-io pa ss passkit) #:use-module (crates-io))

(define-public crate-passkit-0.0.0 (c (n "passkit") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0nzj0qx5c5mjp5pgc6w8drzrka9awvlsdkvmddjnh2dy6plzdhf8")))

(define-public crate-passkit-0.0.1 (c (n "passkit") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0mvav4m6lvifw55c77mhhlrpaj7pciy6n9gmsrlmlpwx2jnm9xki")))

(define-public crate-passkit-0.0.2 (c (n "passkit") (v "0.0.2") (d (list (d (n "openssl") (r "^0.10.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "08aj3hw9hwxbjmfrw481825k5ra81pxq6ym4fy7b4dc266daxrd4")))

(define-public crate-passkit-0.0.3 (c (n "passkit") (v "0.0.3") (d (list (d (n "openssl") (r "^0.10.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "06i9zw3py31dbaqbasvgji7zjbfd8r0a680c58gy47i39k6aqdzm")))

(define-public crate-passkit-0.0.4 (c (n "passkit") (v "0.0.4") (d (list (d (n "openssl") (r "^0.10.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "162yfq322yzrq00zcykd60cnxc2mm3h9ivwymvwy1jcd2f3jhlx7")))

(define-public crate-passkit-0.0.6 (c (n "passkit") (v "0.0.6") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "keychain-services") (r "^0.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "zip") (r "^0.5.0") (d #t) (k 0)))) (h "0n53lf04bhmwqw0mbr5crq3n8d252a4zjw750wyw0hy26l74n7fh")))

