(define-module (crates-io pa ss pass-fu) #:use-module (crates-io))

(define-public crate-pass-fu-0.1.0 (c (n "pass-fu") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k2bhdy2lps2mb8fjpby96jab6ck7j97iwasfnbc4ws68n2ibgvw") (f (quote (("default"))))))

(define-public crate-pass-fu-1.0.0 (c (n "pass-fu") (v "1.0.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "totp-rs") (r "^3") (f (quote ("otpauth"))) (k 0)))) (h "0r77azm30z58bnh2711j6ayc78g1sh4bb1rg735ipx20gygqqi2j") (f (quote (("default"))))))

(define-public crate-pass-fu-1.0.1 (c (n "pass-fu") (v "1.0.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "totp-rs") (r "^3") (f (quote ("otpauth"))) (k 0)))) (h "120nyn8i0vgx8rwnh2c08lj649i1ppfxc7a4ds2i96cq4yx5djc1") (f (quote (("default"))))))

(define-public crate-pass-fu-1.0.2 (c (n "pass-fu") (v "1.0.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "totp-rs") (r "^3") (f (quote ("otpauth"))) (k 0)))) (h "18q82i143aly3r4xfa6x5gay93k1vi7vfa355i17dsn8rbh9rcc0") (f (quote (("default"))))))

(define-public crate-pass-fu-1.0.3 (c (n "pass-fu") (v "1.0.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "totp-rs") (r "^4") (f (quote ("otpauth"))) (k 0)))) (h "0fd6v1cwg1aw77lsb68hm99b08dhwxgw0698d2jmbm6gma3h1zfr") (f (quote (("default"))))))

