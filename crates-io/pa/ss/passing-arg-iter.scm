(define-module (crates-io pa ss passing-arg-iter) #:use-module (crates-io))

(define-public crate-passing-arg-iter-0.1.0 (c (n "passing-arg-iter") (v "0.1.0") (h "1gi93j00l7pvvkjz7wbcrndq55q2v1ynbll2a7749zyqk0108p64") (y #t)))

(define-public crate-passing-arg-iter-0.1.1 (c (n "passing-arg-iter") (v "0.1.1") (h "1kpvpqjnqnn1szrz9pz1az95c7b1rcn1la9i7svpd67pfwk39vjd") (y #t)))

(define-public crate-passing-arg-iter-0.1.2 (c (n "passing-arg-iter") (v "0.1.2") (h "1ahfr6xdi5ll5fbm6jcq2az9xp40yav1r4k7bfvz63xlsyy2h0j1") (y #t)))

