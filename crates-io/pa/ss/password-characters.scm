(define-module (crates-io pa ss password-characters) #:use-module (crates-io))

(define-public crate-password-characters-1.0.0 (c (n "password-characters") (v "1.0.0") (h "0gq7m68k0mrd4da808cw30ppxvkhdxncpnzn3pvm22ybpkfvpdql")))

(define-public crate-password-characters-1.0.1 (c (n "password-characters") (v "1.0.1") (h "18gdcx1qmk3lb24r50k8s14qnavr1q5j6qzig1ksyxaf5xdgx57s")))

