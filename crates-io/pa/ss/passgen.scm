(define-module (crates-io pa ss passgen) #:use-module (crates-io))

(define-public crate-passgen-0.1.0 (c (n "passgen") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1izaf0krx2plawfxmdfqk93lbrssr2mv0g2dvzx36s8xhkb9001y")))

(define-public crate-passgen-0.2.1 (c (n "passgen") (v "0.2.1") (d (list (d (n "clap") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1x5p8xcagm22msmnxn4whnllk8akjwyp0drv1bj9nvwy0ldisrcl")))

(define-public crate-passgen-0.3.0 (c (n "passgen") (v "0.3.0") (d (list (d (n "clap") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "170p441arssdmk9m8pk7d6fkcssfc5lijcr838fvd2pzc7m51vw1")))

(define-public crate-passgen-0.3.1 (c (n "passgen") (v "0.3.1") (d (list (d (n "clap") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1d7957vg4jdvj501v3k49mwcm31k6r56cdx3xqqirmqqqb6flv1c")))

(define-public crate-passgen-0.3.2 (c (n "passgen") (v "0.3.2") (d (list (d (n "clap") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dj6zdx4sh0p5xbinc9zcy8hqzxrn5c3xjrqspnkyip9gxakymrw")))

