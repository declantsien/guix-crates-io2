(define-module (crates-io pa ss passphrase) #:use-module (crates-io))

(define-public crate-passphrase-1.0.0 (c (n "passphrase") (v "1.0.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "01nhchscqfcp5hld8s5fs32ii136ldy29jh05wdnap2r0axss15b") (y #t)))

(define-public crate-passphrase-1.0.1 (c (n "passphrase") (v "1.0.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "1i0azmljx67j8pid50rlh2jbmcnfqi3k555phxf7c1096g1ggywr") (y #t)))

(define-public crate-passphrase-1.0.2 (c (n "passphrase") (v "1.0.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^1.0.1") (d #t) (k 0)))) (h "1c94zz04zcdg6k54mxrjbnmcgg2bwr3k4l1xacc0ysjv9mk9p245")))

