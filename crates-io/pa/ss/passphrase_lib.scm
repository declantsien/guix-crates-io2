(define-module (crates-io pa ss passphrase_lib) #:use-module (crates-io))

(define-public crate-passphrase_lib-1.0.2 (c (n "passphrase_lib") (v "1.0.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ir8jr19vj8k4hdv9jnb1c56iwxmkc8yjr4xk0y71jc76zy56cmf")))

(define-public crate-passphrase_lib-1.0.3 (c (n "passphrase_lib") (v "1.0.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1qphgm3a86gw3csnri0mpdjjs54zclr35q9hhfvyx8njilk2mg7w")))

(define-public crate-passphrase_lib-1.0.4 (c (n "passphrase_lib") (v "1.0.4") (d (list (d (n "nanorand") (r "^0.5.0") (f (quote ("chacha"))) (d #t) (k 0)))) (h "1cqk54lygbcvcmqsr9f6w8fcqykf694yk8ylnpwjnhm8s3ipgm8z")))

