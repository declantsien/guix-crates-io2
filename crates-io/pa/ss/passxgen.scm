(define-module (crates-io pa ss passxgen) #:use-module (crates-io))

(define-public crate-passxgen-0.3.0 (c (n "passxgen") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "01j3zx45kaycpl3468510nc0r1cz175sw6aywxb0ff07rr8h7i3a")))

