(define-module (crates-io pa ss password-encryptor) #:use-module (crates-io))

(define-public crate-password-encryptor-0.0.1 (c (n "password-encryptor") (v "0.0.1") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1m443ksb1a94xjsj7zkbfdal5ypz634za54rz1w369h7fvd94j4i")))

(define-public crate-password-encryptor-0.1.0 (c (n "password-encryptor") (v "0.1.0") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1l138sjj66gs5fs2xcgh9vlg3srcl4bkgspqyg6g1x2a73glk91r")))

(define-public crate-password-encryptor-0.1.1 (c (n "password-encryptor") (v "0.1.1") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1m8pmragzhb1ikh7k8akyvfsjl61my1xsng31rzjpq7d2i97sv84")))

(define-public crate-password-encryptor-0.1.2 (c (n "password-encryptor") (v "0.1.2") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "16a9jnyqxf9x0l77dcsg27x8fzdhhsgn7s4mgsqcqnnr4188ym76")))

(define-public crate-password-encryptor-0.1.3 (c (n "password-encryptor") (v "0.1.3") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1wi57h88ranvdd88q7yx0fg32ca2kq7kxv2kx4qk6cqc50bnb5n8")))

(define-public crate-password-encryptor-0.1.4 (c (n "password-encryptor") (v "0.1.4") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "18q58zvg1s1wzb8va04fafmda0hvrj7fz2hjq2cxii7ygpv5hsyy")))

(define-public crate-password-encryptor-1.0.0 (c (n "password-encryptor") (v "1.0.0") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0nz8zsa3zf5wq5si60cz8fgfxfkyb4s5cwf77xs73i740pc4hadd")))

(define-public crate-password-encryptor-1.1.0 (c (n "password-encryptor") (v "1.1.0") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "01nh0mzv9kmjzx07m4v13mimfh57c088phv0c36z004kcg18w9wl")))

(define-public crate-password-encryptor-1.1.1 (c (n "password-encryptor") (v "1.1.1") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0dmn9zll9v4vr4qxgj9q4cb71gyl1c275fhd2s3rzs56lz5f4za9")))

(define-public crate-password-encryptor-1.1.2 (c (n "password-encryptor") (v "1.1.2") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "18mxqyn2sa80c4zfak538jvkkjyc2qlrw6zslyghlfsw48n3bchn")))

(define-public crate-password-encryptor-2.0.0 (c (n "password-encryptor") (v "2.0.0") (d (list (d (n "base64-url") (r "^2.0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0yxih1chfcz90ym5igp5v0cwdlbxyjw7038zxw01i6csm06f3j16")))

