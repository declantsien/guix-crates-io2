(define-module (crates-io pa ss passage-id) #:use-module (crates-io))

(define-public crate-passage-id-0.1.0 (c (n "passage-id") (v "0.1.0") (d (list (d (n "jsonwebkey") (r "^0.3.5") (f (quote ("jwt-convert"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w7l95xaxsqs3jql4jrvjc2dv3h78yq3c5frhbir6s4738im6dzi")))

(define-public crate-passage-id-0.1.1 (c (n "passage-id") (v "0.1.1") (d (list (d (n "jsonwebkey") (r "^0.3.5") (f (quote ("jwt-convert"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ch4yrj8dpbn846dacwyr55r2giqvp47z4zcnp7vyjkrdrmv3qcn")))

(define-public crate-passage-id-0.1.2 (c (n "passage-id") (v "0.1.2") (d (list (d (n "jsonwebkey") (r "^0.3.5") (f (quote ("jwt-convert"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01cx62m86rpygkmdc4iy43pfs724swx6dzfmpgfcgn5cx37n4ik2")))

(define-public crate-passage-id-0.1.3 (c (n "passage-id") (v "0.1.3") (d (list (d (n "jsonwebkey") (r "^0.3.5") (f (quote ("jwt-convert"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f6rg63ivn0rxbfmj8q40g3v1crw98q1wgd123v1ny09r7bmybyr")))

(define-public crate-passage-id-0.2.0 (c (n "passage-id") (v "0.2.0") (d (list (d (n "jsonwebkey") (r "^0.3.5") (f (quote ("jwt-convert"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p0xydsk9mcgw7r9jnkf77fmpfdxxjycxgkvgkcsk40j6zl8j3xg")))

