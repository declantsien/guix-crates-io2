(define-module (crates-io pa ss password-worker) #:use-module (crates-io))

(define-public crate-password-worker-0.4.0 (c (n "password-worker") (v "0.4.0") (d (list (d (n "bcrypt") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rust-argon2") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "13xbxmraikfwfjy4gx0wc7zv7m28bdml6f4j86fa1dxlnfz7v54k") (f (quote (("default" "bcrypt"))))))

