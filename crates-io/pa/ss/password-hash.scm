(define-module (crates-io pa ss password-hash) #:use-module (crates-io))

(define-public crate-password-hash-0.0.0 (c (n "password-hash") (v "0.0.0") (h "0b287pqfg5sdxyycwi1hvcrigx5yjp3hhqz77z57y3b38qi888li") (y #t)))

(define-public crate-password-hash-0.1.0 (c (n "password-hash") (v "0.1.0") (d (list (d (n "base64ct") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "1r4vhkal1amyv1wc18wpq9771bw0lf6c4fps09dnqniw96d56ddd") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (y #t)))

(define-public crate-password-hash-0.1.1 (c (n "password-hash") (v "0.1.1") (d (list (d (n "base64ct") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "01j1x80f7113sa0r15i6r7ff2livcn5vm266hqc480qq9zhlj6kj") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (y #t)))

(define-public crate-password-hash-0.1.2 (c (n "password-hash") (v "0.1.2") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "0h1dhipnwqwpimk4nab3dzy52k1rv5dyabhr5aak3081dkmgmn45") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.1.3 (c (n "password-hash") (v "0.1.3") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "1f1ilgjf7chxp49457ggfk27sh56cgrkqxji6ld8f73p9f8my4jh") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.1.4 (c (n "password-hash") (v "0.1.4") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "1ymh3np2bamjy8rszimksxcp264dclil4620bxm8rff9pyj6m62l") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.2.0 (c (n "password-hash") (v "0.2.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)))) (h "1nhldm2g7iqr6dklkh2p6d4g59fyk2da43fyrhsiqdac4rsahqbm") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.2.1 (c (n "password-hash") (v "0.2.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "10i2jp5d3cng482baqrfwz01i0p981ianjmqffpc3lh5qblx99f1") (f (quote (("std" "alloc" "base64ct/std") ("default" "rand_core") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.2.2 (c (n "password-hash") (v "0.2.2") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "11fp0mfjq87rqi3adnx8q7yv4z4ncq7iq7prq0zak9gvikxjsj7x") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.2.3 (c (n "password-hash") (v "0.2.3") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r ">=2, <2.5") (k 0)))) (h "1rr4kd52ld978a2xhcvlc54p1d92yhxl9kvbajba7ia6rs5b5q3p") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.3.0-pre.1 (c (n "password-hash") (v "0.3.0-pre.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r ">=2, <2.5") (k 0)))) (h "1r2sgdhycsg1z66azkvjrc922wq5gn818ahyp4ldg6d5z89x89sg") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.3.0 (c (n "password-hash") (v "0.3.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r ">=2, <2.5") (k 0)))) (h "14aq3jzn7iyq3h8wlbmn3zmsxqgszdcd6qc3vvfkyimwz672dmvs") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (y #t)))

(define-public crate-password-hash-0.3.1 (c (n "password-hash") (v "0.3.1") (d (list (d (n "base64ct") (r ">=1, <1.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r ">=2, <2.5") (k 0)))) (h "0abijrf961dkqnsrbwl5yvqmhr4gjibrkl74dbsfn739jqi3y42i") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (y #t)))

(define-public crate-password-hash-0.3.2 (c (n "password-hash") (v "0.3.2") (d (list (d (n "base64ct") (r ">=1, <1.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r ">=2, <2.5") (k 0)))) (h "1n7ig9j5x2q0fk12nny40faggrs0ra5bbxp6gz5yghfwlqw1ay8x") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc"))))))

(define-public crate-password-hash-0.4.0 (c (n "password-hash") (v "0.4.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0sbpn2dr2rkgib0ldjircdcxfxzbzd0fy34wvn1vprnn7ifgs9ma") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.57")))

(define-public crate-password-hash-0.4.1 (c (n "password-hash") (v "0.4.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "1552dd98v6yd4l5myz4g1r2hzln8dfng22638590dc4gpi5fjag0") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.57")))

(define-public crate-password-hash-0.4.2 (c (n "password-hash") (v "0.4.2") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "003p2hssyrcaxyq9fs8x2wx5di8ny9byaakskrf352pfm963fxkn") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.57")))

(define-public crate-password-hash-0.5.0-pre.0 (c (n "password-hash") (v "0.5.0-pre.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "023rwdvwbxwsyx4xwfzafx6px8zfnkgjsjld58naz1pqvdr7z7ad") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.57")))

(define-public crate-password-hash-0.5.0-pre.1 (c (n "password-hash") (v "0.5.0-pre.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "17psx1qhg5641s5ll16vq7mh5qfmhpj0sicnd4m1p45hm5hh3ci1") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.60")))

(define-public crate-password-hash-0.5.0-rc.0 (c (n "password-hash") (v "0.5.0-rc.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0838vcwmpla6zg591sjjbwawkc26f96v52n2w7pp5kz8vk6cn4xg") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("getrandom" "rand_core/getrandom") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.60")))

(define-public crate-password-hash-0.5.0 (c (n "password-hash") (v "0.5.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0ri1mim11zk0a9s40zdi288dfqvmdiryc7lw8vl46b59ifa08vrl") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("getrandom" "rand_core/getrandom") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.60")))

(define-public crate-password-hash-0.6.0-pre.0 (c (n "password-hash") (v "0.6.0-pre.0") (d (list (d (n "base64ct") (r "^1.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "1m57adkj26n7wn473927sz2srmfn5i33r64j5gnr7flkj9fcha8p") (f (quote (("std" "alloc" "base64ct/std" "rand_core/std") ("getrandom" "rand_core/getrandom") ("default" "rand_core") ("alloc" "base64ct/alloc")))) (r "1.60")))

