(define-module (crates-io pa ci pacifist-chess-simulation) #:use-module (crates-io))

(define-public crate-pacifist-chess-simulation-0.1.0 (c (n "pacifist-chess-simulation") (v "0.1.0") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)) (d (n "chess-turn-engine") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "09yajib88yjhnpgdn0704pqnr18rhr4d99c7p20yf42r0is5k72d")))

(define-public crate-pacifist-chess-simulation-0.1.1 (c (n "pacifist-chess-simulation") (v "0.1.1") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)) (d (n "chess-turn-engine") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0prv2ngvlhcxm9w3ji3zi6hyjyk0099q9p6c6nh63l0nhap29mfz")))

(define-public crate-pacifist-chess-simulation-0.1.2 (c (n "pacifist-chess-simulation") (v "0.1.2") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)) (d (n "chess-turn-engine") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "11hxf30gpjmdkjwwlrhwys1kzw20cgjw0qzwnsqxxnlh9lw3kv02")))

