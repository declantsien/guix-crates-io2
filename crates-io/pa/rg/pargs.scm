(define-module (crates-io pa rg pargs) #:use-module (crates-io))

(define-public crate-pargs-0.1.0 (c (n "pargs") (v "0.1.0") (h "15sdzpvgibcc2jzzbpdssxwfbpmn6ighslgfmkjgv7m06m1zq2zs")))

(define-public crate-pargs-0.1.1 (c (n "pargs") (v "0.1.1") (h "10749l51wyl3cvc1ia94clb0lqbai1isd27nwlqxsws737j9kwys")))

(define-public crate-pargs-0.1.2 (c (n "pargs") (v "0.1.2") (h "1d02b1mirf0bvikfydwcskc66jc12sz2k8jg555ysaizw7fsr07h")))

(define-public crate-pargs-0.1.3 (c (n "pargs") (v "0.1.3") (h "11s7dd08ksmw9qmdr8m7gcrqziq01c1pl5y2x3s1vqxcf1275670")))

(define-public crate-pargs-0.1.4 (c (n "pargs") (v "0.1.4") (h "0805cm6yx07ykdk54wy65wmggkxyl4sy2zc81pj4y337bs4i7xls")))

(define-public crate-pargs-0.2.0 (c (n "pargs") (v "0.2.0") (h "1h0qwlip2vpai2m69090751fkrh7v37ri5mhrv382r7b6294z76q")))

(define-public crate-pargs-0.2.1 (c (n "pargs") (v "0.2.1") (h "1qq90vrsv5iyxvbnaydhcj22cmnlhx8k4z4gkk7mmscca7yj345b")))

