(define-module (crates-io pa rg pargraph) #:use-module (crates-io))

(define-public crate-pargraph-0.1.0 (c (n "pargraph") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0brm562w51i7gpvjb69n1lifs04xv1i0511y502yy161zr720hv3")))

(define-public crate-pargraph-0.1.1 (c (n "pargraph") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1lzpqcf51y36z4jl3bx2lln77sbjgis554wa0279prmm5z620p24")))

(define-public crate-pargraph-0.1.2 (c (n "pargraph") (v "0.1.2") (d (list (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (d #t) (k 0)))) (h "0s7gd28r9vacnyq6wg5a26w1vgji0v84yhvwsq78y1hm7ypyxm2a")))

(define-public crate-pargraph-0.1.3 (c (n "pargraph") (v "0.1.3") (d (list (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (d #t) (k 0)))) (h "0dq79lpzs4xlsqvhbxdsq1wnx57iqhlk5hwlrskxh6jyifhlk2gc")))

(define-public crate-pargraph-0.1.4 (c (n "pargraph") (v "0.1.4") (d (list (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (d #t) (k 0)))) (h "1b84yydiiksd44x90hpqwlzflhhkxji5drw702qbyrjqjmac51jv")))

