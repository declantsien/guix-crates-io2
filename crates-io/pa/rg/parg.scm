(define-module (crates-io pa rg parg) #:use-module (crates-io))

(define-public crate-parg-0.1.0 (c (n "parg") (v "0.1.0") (h "0fadijlbfyri9jsw1h8ni420vhbc1d7dkrncf562ar4sxq2qhdqx")))

(define-public crate-parg-0.2.0 (c (n "parg") (v "0.2.0") (h "0x1dfc8xi9dhkp5ij9sxsq52fxqn6n4gfbbc28bysnsacm8xlfdd")))

