(define-module (crates-io pa th pathsub) #:use-module (crates-io))

(define-public crate-pathsub-0.1.0 (c (n "pathsub") (v "0.1.0") (h "0rvhkb77f54zc3xa793qb14lq36ki08c4l211cpm7lbx6dx2pvly") (y #t)))

(define-public crate-pathsub-0.1.1 (c (n "pathsub") (v "0.1.1") (h "0w590309xh8dag2gz4h7ky4yzd5s2j3c84h485j35r6b7c9kipfs")))

