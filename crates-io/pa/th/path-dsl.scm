(define-module (crates-io pa th path-dsl) #:use-module (crates-io))

(define-public crate-path-dsl-0.1.0 (c (n "path-dsl") (v "0.1.0") (h "0ilsxmwf3d9w3pgy0ppi48jkljk4720zmfmdy2kchh06mn6gbf3y")))

(define-public crate-path-dsl-0.1.1 (c (n "path-dsl") (v "0.1.1") (h "0i027a4nwjkxhyhv6ixrplmp72mrls0srr8x7svlsk66vmgcckiq")))

(define-public crate-path-dsl-0.2.0 (c (n "path-dsl") (v "0.2.0") (h "1lhbd7x9s5blrxha5clkdmfk7xh50mj2wk4frwc9a7vffxkppy6z")))

(define-public crate-path-dsl-0.3.0 (c (n "path-dsl") (v "0.3.0") (h "1jd3rnr7jph922cyx7gfzil6s5p8zayx032sv0m5cgzivds98bdr")))

(define-public crate-path-dsl-0.4.0 (c (n "path-dsl") (v "0.4.0") (h "1hmfi1xrw6yvbnqc51r8qkzk0aa3ysb6d9kfv39ifnwxdblbk8bk")))

(define-public crate-path-dsl-0.5.0 (c (n "path-dsl") (v "0.5.0") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "050zb2hh50q5cz5yg7pbxmwidwhcwfz15lv49b5r7j6dwadw6dqb")))

(define-public crate-path-dsl-0.5.1 (c (n "path-dsl") (v "0.5.1") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "1p6f9ap6wkychcl0j8gi34cck0yzgc5zbw21k54l6q749mxbdnwn")))

(define-public crate-path-dsl-0.5.2 (c (n "path-dsl") (v "0.5.2") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "08f4zghcxqbymq2in4w36bw585l4sikd141xqkflqqqk5hrhimrx")))

(define-public crate-path-dsl-0.5.3 (c (n "path-dsl") (v "0.5.3") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "0d1kp0xvkdwmnv3jvwa96l3nvrm576s0y29ym6ilik1jhph029ci")))

(define-public crate-path-dsl-0.5.4 (c (n "path-dsl") (v "0.5.4") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "16zbmgdvzf9n23hn93h2j8gr0s309ddcf0bpxrf4glixirqzyn71")))

(define-public crate-path-dsl-0.6.0 (c (n "path-dsl") (v "0.6.0") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "0qpyv3xxdd6sszb3lqkb5a53c9krgh5d0n690l4hzkk8mjxcvvwf")))

(define-public crate-path-dsl-0.6.1 (c (n "path-dsl") (v "0.6.1") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)))) (h "0x0cpsgg45a1mj07k8hjc84nydmhq6bjalqmshcjwjxxs9bds69x")))

