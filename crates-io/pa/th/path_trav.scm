(define-module (crates-io pa th path_trav) #:use-module (crates-io))

(define-public crate-path_trav-1.0.0 (c (n "path_trav") (v "1.0.0") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0g43vypf0riphz3mg3f84zv5h561d89wdb5xkgy133c9khkx7f3q") (y #t)))

(define-public crate-path_trav-1.0.1 (c (n "path_trav") (v "1.0.1") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "18sfrdhgwq5knw0isb0yminmifggln4k6ldq8zyhgi9izqkpminj") (y #t)))

(define-public crate-path_trav-2.0.0 (c (n "path_trav") (v "2.0.0") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0p8m0zqav7zhp086scyxwj5zwzh1nk7da7iz1nq4i415lasrgl0k")))

