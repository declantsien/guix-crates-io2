(define-module (crates-io pa th pathtrie) #:use-module (crates-io))

(define-public crate-pathtrie-0.1.0 (c (n "pathtrie") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indenter") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (f (quote ("fmt"))) (d #t) (k 2)))) (h "10hdc3a24qm6ngvfnb9k9c26rymmw59xpnd0fzy0jir45zjbkass")))

(define-public crate-pathtrie-0.1.1 (c (n "pathtrie") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indenter") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (f (quote ("fmt"))) (d #t) (k 2)))) (h "13rd30l7v45h0hb7lfpnfw01ika53dx3r4qbfb9sihx2wm71sg1z")))

