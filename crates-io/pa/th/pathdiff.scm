(define-module (crates-io pa th pathdiff) #:use-module (crates-io))

(define-public crate-pathdiff-0.1.0 (c (n "pathdiff") (v "0.1.0") (h "0cfg3isnx6mf3wbi7rsg4nmvywby40sbcs589n20fgi09l4p1gx3")))

(define-public crate-pathdiff-0.2.0 (c (n "pathdiff") (v "0.2.0") (h "0d2aqgrqhdn5kxlnd5dxv7d6pgsgf92r6r9gqm6bdh0mvsrk0xl7")))

(define-public crate-pathdiff-0.2.1 (c (n "pathdiff") (v "0.2.1") (d (list (d (n "camino") (r "^1.0.5") (o #t) (d #t) (k 0)))) (h "1pa4dcmb7lwir4himg1mnl97a05b2z0svczg62l8940pbim12dc8")))

