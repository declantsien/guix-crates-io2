(define-module (crates-io pa th path_to_unicode_filename) #:use-module (crates-io))

(define-public crate-path_to_unicode_filename-0.1.0 (c (n "path_to_unicode_filename") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ucd") (r "^0.1.1") (d #t) (k 2)))) (h "04867lxx9bd0m29gczqsxckmqv209rgrix090f7cxfxf97b0l26l")))

(define-public crate-path_to_unicode_filename-0.1.1 (c (n "path_to_unicode_filename") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ucd") (r "^0.1.1") (d #t) (k 2)))) (h "0b1bgrc2x3blbh2vyd8gpy4bq1i2qxc3iaxmk3hwscg37sz6j90i")))

