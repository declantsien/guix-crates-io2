(define-module (crates-io pa th path-slash) #:use-module (crates-io))

(define-public crate-path-slash-0.0.1 (c (n "path-slash") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0mp3n7by41f2vdvplqrkf4n2z00k4sxkxbbkpfjn1ja45x9viggn")))

(define-public crate-path-slash-0.1.0 (c (n "path-slash") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "1c89szszv25yb89xkp33fwrkj74b54br7vzzwrx06nkrxj1d3x8y")))

(define-public crate-path-slash-0.1.1 (c (n "path-slash") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0bvfzywc2nh6kdwh3nanyd6dibbhmzhppb2f3xa7aqhkv7s8m1d0")))

(define-public crate-path-slash-0.1.2 (c (n "path-slash") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0pf8x6axrlky4sm0297w623f2argsj753yrmf519g8b321n5dyfx")))

(define-public crate-path-slash-0.1.3 (c (n "path-slash") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0s2cy9hrxriizb7m9c680ngd7ianxyx98qnv0fcrga6b2xd72rgz")))

(define-public crate-path-slash-0.1.4 (c (n "path-slash") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "06dnnmd3fvmr9ngwgj0xrfj9s8h09m9dgf3zlqsbalzk9wybpb1w")))

(define-public crate-path-slash-0.1.5 (c (n "path-slash") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "15mni0f28mblwd97192c4zyyi6054yljmiqrdb6bx97ga69hk2j9")))

(define-public crate-path-slash-0.2.0 (c (n "path-slash") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0a6fg01a4yngj8gf85gd3yyzanvgigvjclkk50li500q7jx18h65")))

(define-public crate-path-slash-0.2.1 (c (n "path-slash") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0hjgljv4vy97qqw9gxnwzqhhpysjss2yhdphfccy3c388afhk48y") (r "1.38")))

