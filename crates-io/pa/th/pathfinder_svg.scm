(define-module (crates-io pa th pathfinder_svg) #:use-module (crates-io))

(define-public crate-pathfinder_svg-0.5.0 (c (n "pathfinder_svg") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "pathfinder_color") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_content") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_renderer") (r "^0.5") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (d #t) (k 0)))) (h "0vm5qm2nymsvshfaklcbx12z3h8vk9pb4zh8qimibknz665wdynb")))

