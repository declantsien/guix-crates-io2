(define-module (crates-io pa th path-finding-lib) #:use-module (crates-io))

(define-public crate-path-finding-lib-0.1.0 (c (n "path-finding-lib") (v "0.1.0") (h "0paqlxhhs3bwxv8kq723bgxfwaiznw78h46n29xssr15d3imwfgw")))

(define-public crate-path-finding-lib-0.1.1 (c (n "path-finding-lib") (v "0.1.1") (h "1bhb65l7kjrsk6nd61h02n57bvdg44vfr241nfvzk952z8irpb2x")))

(define-public crate-path-finding-lib-0.1.2 (c (n "path-finding-lib") (v "0.1.2") (h "12idil5c6lf6r7ablgc50i0pz3l09cx4hzlnw84cj60q8xi07lly")))

(define-public crate-path-finding-lib-0.1.3 (c (n "path-finding-lib") (v "0.1.3") (h "05gdj22gybysacp9sx1av5590bm4h5zqir0lp4zfw07nznzpsx2w")))

(define-public crate-path-finding-lib-0.1.4 (c (n "path-finding-lib") (v "0.1.4") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "00b9jn155pwwv19525a8w6ambygl1iqmbjdy3a3jmmrlkmwphhwv")))

(define-public crate-path-finding-lib-0.1.6 (c (n "path-finding-lib") (v "0.1.6") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "1a1i41sd247dai6l43694aa3m3y5x6n6vi0n4jya9l0mqc0b2vw2")))

(define-public crate-path-finding-lib-0.2.0 (c (n "path-finding-lib") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.0") (d #t) (k 0)))) (h "08zdxyrmq8jn8kg34qrzj73j2prp99phlidg1xhfkgjy5wfqfrm5")))

(define-public crate-path-finding-lib-0.2.1 (c (n "path-finding-lib") (v "0.2.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.0") (d #t) (k 0)))) (h "1jqb2ivaf8pafxf4p9snn2k3f1hq8aicb7k1mnaijrb3z5x16xkj")))

(define-public crate-path-finding-lib-0.3.1 (c (n "path-finding-lib") (v "0.3.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.0") (d #t) (k 0)))) (h "1ygvirp8phw4f57n240ziwvicxd85kd7m3k9xi1kkznwgf0h3azf")))

