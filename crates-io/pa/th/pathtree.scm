(define-module (crates-io pa th pathtree) #:use-module (crates-io))

(define-public crate-pathtree-0.0.0 (c (n "pathtree") (v "0.0.0") (h "0mhajv36yp50djpawrjxfnjk7xqbzrig3mkxm1xcpbp5navd10dr")))

(define-public crate-pathtree-0.1.0 (c (n "pathtree") (v "0.1.0") (h "1gwz2bffcq6z0l69q56aag61hshfx5d6z77wwn45z6s63ah0ayxc")))

