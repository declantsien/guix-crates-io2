(define-module (crates-io pa th path-here) #:use-module (crates-io))

(define-public crate-path-here-0.1.0 (c (n "path-here") (v "0.1.0") (d (list (d (n "find-target") (r "^0.1.1") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "1byb4gmfrkm04cmb4p2b9ijpb4qvscjwlya8lk8hd6xqwinmc8cm") (f (quote (("default"))))))

