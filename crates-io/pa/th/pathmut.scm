(define-module (crates-io pa th pathmut) #:use-module (crates-io))

(define-public crate-pathmut-0.1.0 (c (n "pathmut") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "color"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "1yimfhw4r61j5kdrisxbr2wx6hb6d6bkf6d06hip4njfx1g7qack")))

(define-public crate-pathmut-0.2.0 (c (n "pathmut") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "color"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "169gvcdcmyd4g68617ccrfzbxlmwnzmjcvfap90pprng5lqmqdff")))

(define-public crate-pathmut-0.3.0 (c (n "pathmut") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "color"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "0k3lmn31lmq9js0y3p00c3xq6xjc5vxnigbasqgg8aa9awgaaiqd")))

