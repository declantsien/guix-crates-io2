(define-module (crates-io pa th path2regex) #:use-module (crates-io))

(define-public crate-path2regex-0.0.0-alpha (c (n "path2regex") (v "0.0.0-alpha") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1j257db55cihym2qpkrzxac9sgwdv51pch2mn1xihh7mbwp9ssz0") (y #t) (r "1.60")))

(define-public crate-path2regex-0.0.1 (c (n "path2regex") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 2)))) (h "0d9wn6znmx2wcmq32zsr4b5ylrhc7mhg9px87ia8hnq9h16vqy1m") (f (quote (("default" "compile" "match")))) (s 2) (e (quote (("match" "dep:serde_json") ("compile" "dep:serde_json")))) (r "1.60")))

(define-public crate-path2regex-0.0.2 (c (n "path2regex") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 2)))) (h "15c3i1pqy0rivpvb3fbj0lnvrlwls851zklqf9fp5yki8x9z18ha") (f (quote (("default" "compile" "match")))) (s 2) (e (quote (("match" "dep:serde_json") ("compile" "dep:serde_json")))) (r "1.60")))

(define-public crate-path2regex-0.0.3 (c (n "path2regex") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 2)))) (h "0qpw27pw4ks4rxrvf2f68bir503nn9xs157aiy4r9m3q5c7vjiyb") (f (quote (("default" "compile" "match")))) (s 2) (e (quote (("match" "dep:serde_json") ("compile" "dep:serde_json")))) (r "1.60")))

(define-public crate-path2regex-0.0.4 (c (n "path2regex") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 2)))) (h "1ssp86lx0n3jvhm3vzrydws79n2jxb8h1ba6vdhvpw927lxjaq83") (f (quote (("default" "compile" "match")))) (s 2) (e (quote (("match" "dep:serde_json") ("compile" "dep:serde_json")))) (r "1.60")))

