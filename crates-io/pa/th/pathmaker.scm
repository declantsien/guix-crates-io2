(define-module (crates-io pa th pathmaker) #:use-module (crates-io))

(define-public crate-pathmaker-0.1.0 (c (n "pathmaker") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0z4h6ri20k43szfnhc7f2nw2y2blgfbwigfvbzfcy89aq82j04xn") (f (quote (("with-hyper" "hyper" "futures") ("default" "with-hyper"))))))

(define-public crate-pathmaker-0.2.0 (c (n "pathmaker") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0n65z4r14gixs5vn7dvll95k71gqsx23xw3hsk3n0mvif13nnzv8") (f (quote (("with-hyper" "hyper" "futures") ("default" "with-hyper"))))))

