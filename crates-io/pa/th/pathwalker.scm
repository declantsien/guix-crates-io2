(define-module (crates-io pa th pathwalker) #:use-module (crates-io))

(define-public crate-pathwalker-0.1.0 (c (n "pathwalker") (v "0.1.0") (h "0g35a7zfp5yy6hz7cls0c3kgikxv02zzri9jf4y0w9sn70di49f2")))

(define-public crate-pathwalker-0.1.1 (c (n "pathwalker") (v "0.1.1") (h "0p6dmf8rld69vrfccqk2qzfbk0762gjbdj0v5dhncg5051alpcb7")))

(define-public crate-pathwalker-0.1.2 (c (n "pathwalker") (v "0.1.2") (h "1yy0a2di62kyyqmn150nyi1md495an9s0p0nycg7zcbij459i2v5")))

(define-public crate-pathwalker-0.2.0 (c (n "pathwalker") (v "0.2.0") (d (list (d (n "pathfilter") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1f61a8biyywdqldr4m7xl94qr5j327z31cnihkfnd4nz0gfj7qfk")))

(define-public crate-pathwalker-0.2.1 (c (n "pathwalker") (v "0.2.1") (d (list (d (n "pathfilter") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1cxsf9ffhwqg0n0ldd7a4h406ajhbik4cp5k72v43mslhqjlsf9r")))

(define-public crate-pathwalker-0.2.2 (c (n "pathwalker") (v "0.2.2") (d (list (d (n "pathfilter") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0kg1kx0j2lh6wxqg1qwxr9g3837cc34jgbbxv9fq9dkc9ryrda5q")))

(define-public crate-pathwalker-0.2.3 (c (n "pathwalker") (v "0.2.3") (d (list (d (n "pathfilter") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "14czz9yq9nlv8a2asvyhi8w7bj60z00hy94v2y2zmdpbx24pzsw0")))

(define-public crate-pathwalker-0.2.4 (c (n "pathwalker") (v "0.2.4") (d (list (d (n "pathfilter") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "09ly4msw2v3fpjmbb9vs4fa8f8mm1jlpdpivzvcky6h16i4wh3wm")))

(define-public crate-pathwalker-0.3.0 (c (n "pathwalker") (v "0.3.0") (d (list (d (n "direntryfilter") (r "^0.2.0") (d #t) (k 0)) (d (n "moar_options") (r "^0.1.1") (d #t) (k 0)) (d (n "pathfilter") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "1984h860v1dn4p85j2ccv5sg1hniz6ssn4ljr5znm16bisjqdmjm")))

(define-public crate-pathwalker-0.4.0 (c (n "pathwalker") (v "0.4.0") (d (list (d (n "direntryfilter") (r "^0.4.1") (d #t) (k 0)) (d (n "moar_options") (r "^0.1.1") (d #t) (k 0)) (d (n "pathfilter") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "19kfk9xl55vgcjf8w58xm1ybmsrzqmy0w15v65m1q3dssx35vmk2")))

