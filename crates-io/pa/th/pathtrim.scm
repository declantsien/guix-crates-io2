(define-module (crates-io pa th pathtrim) #:use-module (crates-io))

(define-public crate-pathtrim-1.0.0 (c (n "pathtrim") (v "1.0.0") (h "0da73zxvf48znxjmsy9bby1i9vvj6w1wzfn5425hm2868paz9ndr")))

(define-public crate-pathtrim-1.0.1 (c (n "pathtrim") (v "1.0.1") (h "1071l7xrk8y1wzwni16qa1ihs0kznb486s9rxzffkx1zb8sdr6ck")))

(define-public crate-pathtrim-1.0.2 (c (n "pathtrim") (v "1.0.2") (h "0vyhq49qjyajwg7hb2qhqr2sgvr3i5fg7k7ia497cg18jqqy6a8i")))

(define-public crate-pathtrim-1.0.3 (c (n "pathtrim") (v "1.0.3") (h "1pqgzwglpcfkdkym59dkp6cvs73dwx5jd3p35ahipcfb3f7l3v7h")))

(define-public crate-pathtrim-1.0.4 (c (n "pathtrim") (v "1.0.4") (h "0d31kj1i009761ihiscz4q9bmm92yib348zcwjxzjxlgvrzdg6nn")))

(define-public crate-pathtrim-1.0.5 (c (n "pathtrim") (v "1.0.5") (h "04izx4xypcbzlx4qbf67i5r2hqpn9xfhwdpy9jykbqal55f5fqla")))

(define-public crate-pathtrim-1.0.6 (c (n "pathtrim") (v "1.0.6") (h "1ygp10q1nqbx1vbp5zasix3b6bkh5qrkvq620fawz5zydqrqy01b")))

(define-public crate-pathtrim-2.0.0 (c (n "pathtrim") (v "2.0.0") (h "0a2mk0pzv4a8rpcnlzp9ign3mgjxq1gv8whs5545cvrjapkclrrd")))

