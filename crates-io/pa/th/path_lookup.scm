(define-module (crates-io pa th path_lookup) #:use-module (crates-io))

(define-public crate-path_lookup-0.1.0 (c (n "path_lookup") (v "0.1.0") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "0c6s0dma7365pbn6bl65jlpa1vxvbrhism3nhxihhpx5yg5f9jsz")))

(define-public crate-path_lookup-0.1.1 (c (n "path_lookup") (v "0.1.1") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "0n4fmh1nxsasnwwdmawvd2sf083jrqvhjpycjz3s34id4ywy5gm2")))

(define-public crate-path_lookup-0.1.2 (c (n "path_lookup") (v "0.1.2") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "0c9visc38hjdkq4zl0i4ww1r5bi64vk84x56ra5acbiqqwwafngr")))

(define-public crate-path_lookup-0.1.3 (c (n "path_lookup") (v "0.1.3") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "17gpfjhkjsknwb7ymsrj4ram7c05828qjzk1xvdh2slw23hdrll2")))

(define-public crate-path_lookup-0.1.4 (c (n "path_lookup") (v "0.1.4") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "16gcvvj9vcxz6l0v01ssg602pq0pkcrz4if7hkn9vvhlflldqfc2")))

(define-public crate-path_lookup-0.1.5 (c (n "path_lookup") (v "0.1.5") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "1agzwg0yqafbqabwzrpqawh7in7wqxxzp6i9jdcnw9rfim2ylndp")))

