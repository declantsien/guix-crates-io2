(define-module (crates-io pa th pathfilter) #:use-module (crates-io))

(define-public crate-pathfilter-0.1.0 (c (n "pathfilter") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0r51pjr6114j5zing053si3hsj2p66wjcqk610sq852swmy2dj8l")))

(define-public crate-pathfilter-0.2.0 (c (n "pathfilter") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0jb01h5vfh2033z6qbnh8nrmp8g8hrqq1p6wrynw98hc1ljw57bb")))

(define-public crate-pathfilter-0.3.0 (c (n "pathfilter") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0z1djj0icz9w5aazrw5x4yq48bg0xv76n2g78qpaq7wvjjylbrsr")))

(define-public crate-pathfilter-0.3.1 (c (n "pathfilter") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0h3zmyzvxc3wfzxfxvs9ryljl841cskyydj75qfkhknrdxn7rhl3")))

(define-public crate-pathfilter-0.4.1 (c (n "pathfilter") (v "0.4.1") (d (list (d (n "regex") (r "^1.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1vfi8qy6ryjg6cj05iqlbkw1a59xmdrc2xjxqdiznqacd4h318s0") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_regex") ("regex" "dep:regex"))))))

