(define-module (crates-io pa th path-cleaner) #:use-module (crates-io))

(define-public crate-path-cleaner-0.1.0 (c (n "path-cleaner") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "expand_str") (r "^0.1.1") (d #t) (k 0)) (d (n "promptuity") (r "^0.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (k 0)))) (h "0jp45ggsdjyl1rf09w55g2nsc993020wlmszcv90hgk8rgwnyzfr")))

