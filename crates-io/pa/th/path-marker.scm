(define-module (crates-io pa th path-marker) #:use-module (crates-io))

(define-public crate-path-marker-0.1.0 (c (n "path-marker") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bzig8xf71gqadhf8f64pr9d0rfgml4nj72dn724jfncd87pi20s")))

(define-public crate-path-marker-0.1.1 (c (n "path-marker") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c5qf4n6dgr1gr7zpjlfyy938a6nmjzfl86kww8fqm3j0w4mgssg")))

(define-public crate-path-marker-0.2.0 (c (n "path-marker") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "13xnf7krwj8rax81rxm9s8f008qhaqpbz8rxgdflwk9b6af12m6s")))

(define-public crate-path-marker-0.2.1 (c (n "path-marker") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1j3bl4ildq3f7s5yg6sqhj75zs190jyaj1cyxll93mr8jys8jarg")))

(define-public crate-path-marker-0.2.2 (c (n "path-marker") (v "0.2.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1synb0a04lih8164wzalvjiy0j48sgi7v0d8ywx45nvwnbgs2jj7")))

(define-public crate-path-marker-0.2.3 (c (n "path-marker") (v "0.2.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0qk7a8c4p9y658qqgnf40jzd3fbnb1rvk70pxpkrwsdfqir0282f")))

(define-public crate-path-marker-0.2.4 (c (n "path-marker") (v "0.2.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1nvhzl23055ffh2cddavrghhcpb3ywc01ygbylngqxq212minj2k")))

