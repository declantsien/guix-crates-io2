(define-module (crates-io pa th pathutil) #:use-module (crates-io))

(define-public crate-pathutil-0.1.0 (c (n "pathutil") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)))) (h "16azzs01zwlr584c8ag8z07yin12r5hwnnbrjw1nf7r31ikfgirv") (y #t)))

(define-public crate-pathutil-0.1.1 (c (n "pathutil") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0f6ql8vwz3zz661k4lvq0nlnlk97w7zdif08wr51hjw9ah487j44") (y #t)))

(define-public crate-pathutil-0.1.2 (c (n "pathutil") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "06mfxl7wah8pniqf5v491yrqiayibbj8y4815wmrqvvrmijwc4bh") (y #t)))

(define-public crate-pathutil-0.1.3 (c (n "pathutil") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "13zyf6jsp973jcgkdvw13157nldpl5baxcayp6gv2x84si4w7s8d") (y #t)))

(define-public crate-pathutil-0.1.4 (c (n "pathutil") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "14pxpwyz2r18ywlx50rr4gf6cxgz0ws5nlyr3lnl8srgzinb57bn") (y #t)))

(define-public crate-pathutil-0.1.5 (c (n "pathutil") (v "0.1.5") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "error-annotation") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0rinas6xbxw0ckp1g098vd3rbm9n4bg6a61gc4q50x70fz3m2sdp") (y #t)))

