(define-module (crates-io pa th path-tools) #:use-module (crates-io))

(define-public crate-path-tools-0.0.0 (c (n "path-tools") (v "0.0.0") (h "1k2z2ji6q2428chgii40y48p3bkhqmas8pb86ri2i11bkj8dfvfr")))

(define-public crate-path-tools-0.1.0 (c (n "path-tools") (v "0.1.0") (h "0z37qdkzl006ynx7hx5a7r64f9zzfyxhl7v99c9bl93lmckjvg0w")))

