(define-module (crates-io pa th path-dedot) #:use-module (crates-io))

(define-public crate-path-dedot-1.0.0 (c (n "path-dedot") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0876j38gdn5g4kjfw4sld50ypw66a69xrkvlss1hm4lbw2q6am1l")))

(define-public crate-path-dedot-1.0.1 (c (n "path-dedot") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "10xk4r0j813hvp5xxahhfv9yivqf6c8haj1x6njv37xslarisjfq")))

(define-public crate-path-dedot-1.0.2 (c (n "path-dedot") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1r3qdcyg9gyqamxgj91w0wc8c25wx8nnm58mn075nd66vy1g40gp")))

(define-public crate-path-dedot-1.0.3 (c (n "path-dedot") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1nq4g3pgrj1qqmwrb4wsgrd6qqi7hgpnn3s09mfmgg5ci6i16xs7")))

(define-public crate-path-dedot-1.0.4 (c (n "path-dedot") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "02zqlngf3f6fghxwzs0kdjn57yggcb17grs1z70ladw718cgxym6")))

(define-public crate-path-dedot-1.0.5 (c (n "path-dedot") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "14i7qhli54dkjgxbbygkcg2028vscwgp8aj3k87zi9a3zbrh3xhp")))

(define-public crate-path-dedot-1.0.6 (c (n "path-dedot") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "18hj7p4csr6asm88aqsv4ljkx3n8ck36s4v24vlz9qh0pii11917")))

(define-public crate-path-dedot-1.1.0 (c (n "path-dedot") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0nskzskwp3jcsan4f5jq715lfd2hhlg79mvdxff8p42yzi4aa2mc")))

(define-public crate-path-dedot-1.1.1 (c (n "path-dedot") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "19sfrmfgy0l142dyklhij4aq9hv8zrgmzdlldsqqvn4nbz1q9vip")))

(define-public crate-path-dedot-1.1.2 (c (n "path-dedot") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1nwh46yin79k1k1w80ymgc77v67hilfivjf5fjj6j7jbqf1g8flh")))

(define-public crate-path-dedot-1.1.3 (c (n "path-dedot") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0lzkgdqlpxhv2m7xamamis2nnfr8d6pf13kp966490r7k9rxjam8")))

(define-public crate-path-dedot-1.1.4 (c (n "path-dedot") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1miph1dml3779024khzmpaifxgphikmqfrc8rmrhd3k1gzxlxbp3")))

(define-public crate-path-dedot-1.1.5 (c (n "path-dedot") (v "1.1.5") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1vz07w4wp425hslpnx8k50dgj48vfwl9wgbp0fxnz2iac0nq8vy2")))

(define-public crate-path-dedot-1.1.6 (c (n "path-dedot") (v "1.1.6") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0iqbk0nxl5qqwimrg81zzcr553fc4c1whzrkrvssrm8zkhpqav3x")))

(define-public crate-path-dedot-1.1.7 (c (n "path-dedot") (v "1.1.7") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1kx67ch60vana4m99yv7sszihz2jbnn4xc9gj72ipaalzha1rwv1")))

(define-public crate-path-dedot-1.1.8 (c (n "path-dedot") (v "1.1.8") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1p790kjyi3imzlvzf96faagyi2nx98g3dz3xzniqbpzn460aqmmg")))

(define-public crate-path-dedot-1.1.9 (c (n "path-dedot") (v "1.1.9") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "127jhs2630wpr5rn978dcf17hb9vvdhgjxw51q71rxsb4vj6kysv")))

(define-public crate-path-dedot-1.1.10 (c (n "path-dedot") (v "1.1.10") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0spfpsmdqr328k21pqyvipagbz5y2g66fr88p2bbc07fpq9mcr3m")))

(define-public crate-path-dedot-1.1.11 (c (n "path-dedot") (v "1.1.11") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0qbz67lbiq1k2ymb8pvzazs8ll3v2z4f1ypg7vlz6i69wa8jskq1")))

(define-public crate-path-dedot-1.1.12 (c (n "path-dedot") (v "1.1.12") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1b888ngw9amyvd68wla9w2ai8gv9kxlnwas5i8m3wwgp78c5nb6g")))

(define-public crate-path-dedot-1.1.13 (c (n "path-dedot") (v "1.1.13") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1y1hg1ajlg0kk8943i3hviwzrs136y1v0h9sq7ms8315934apqwy")))

(define-public crate-path-dedot-1.2.0 (c (n "path-dedot") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1a2kyzx2fs8iczh0wr22d9xpf4d92iah4gkn1lm7991g6xlmzasz") (y #t)))

(define-public crate-path-dedot-1.2.1 (c (n "path-dedot") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1xqxp2m0kzaixl0400qcfbc9bpfnfii35q2s0drl3rcm289lp93d") (y #t)))

(define-public crate-path-dedot-1.2.2 (c (n "path-dedot") (v "1.2.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0kzj0z2sfqyyb17q7rv161ayn2knmlqc44cwkmrq94sj7dm2zwsc") (y #t)))

(define-public crate-path-dedot-1.2.3 (c (n "path-dedot") (v "1.2.3") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1njr1291xzkq7hvlh27imhhhks3y35q96hy65vp3sjl94w1adr23") (y #t)))

(define-public crate-path-dedot-1.2.4 (c (n "path-dedot") (v "1.2.4") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "16q3ldg2hy1sqmz8dfgar2dwwhyxg6hy821zpg87fgxhxnqqmia5")))

(define-public crate-path-dedot-2.0.0 (c (n "path-dedot") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1ixjrrm8wvw5wkjw39h4gj45nqba7fld8wv4q54gm7fdxl6w64h2") (f (quote (("unsafe_cache") ("lazy_static_cache") ("default"))))))

(define-public crate-path-dedot-2.0.1 (c (n "path-dedot") (v "2.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "16dhl9fv0vycbacrg7b86v53q4q08j2i4jx97h257zis7ip8rrfv") (f (quote (("unsafe_cache") ("lazy_static_cache") ("default"))))))

(define-public crate-path-dedot-2.0.2 (c (n "path-dedot") (v "2.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0p7q3nwrm2c9acylz28glz4xhcyfjd4nkirvcxxsy4wjcjddrjxi") (f (quote (("unsafe_cache") ("lazy_static_cache") ("default"))))))

(define-public crate-path-dedot-2.0.3 (c (n "path-dedot") (v "2.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1gblisfbhgg289zm45ysy2s61zr76n371r9p6z4ml24pcq3k84ax") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-2.0.4 (c (n "path-dedot") (v "2.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "18nx27va3xxg4q72y0070aqi37l0q7333jf2xsn8p5sb9gbcgls7") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-2.0.5 (c (n "path-dedot") (v "2.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0n2aidqw6hwrvxghgi8zmmmpiahagaknx6wgphbf5l1s589xkwy0") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-3.0.0 (c (n "path-dedot") (v "3.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1hsqlzcz72fq9h57bihc7w30cw9y188vgq0zwmr7y4922nycvp36") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-3.0.1 (c (n "path-dedot") (v "3.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07hpyk0nhyqjmvl0lhbwi7r4c3cmfxf0mw8057hjcssxdsjyzcbh") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-3.0.2 (c (n "path-dedot") (v "3.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0h1a03nr4y1mwnh8lnli9v06d9zdgswck6pvcaciix99h716wbkj") (f (quote (("unsafe_cache") ("lazy_static_cache"))))))

(define-public crate-path-dedot-3.0.3 (c (n "path-dedot") (v "3.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "11ysbffgb0w77q6d256nrdyv49dy5nlzdnvhd8br3z6s6bwf680z") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.4 (c (n "path-dedot") (v "3.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1c0644gl4bdkgcnlmxnp4v8cq1b5yq66yq420jqn0p5721rlk0b5") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.5 (c (n "path-dedot") (v "3.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0m71kf9gv31x37d462vyi0lqfrvrmm0qnyq60f52h2jhvi8r35mi") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.6 (c (n "path-dedot") (v "3.0.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "17zcvxqfc7c8m43qfddh24zji8a6ky21zajlsd3r9qy743bmssx4") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.7 (c (n "path-dedot") (v "3.0.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0q74a8jn732wjk2knnydlccjrs6blc46rj77kwl2b76fbyc6x335") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.8 (c (n "path-dedot") (v "3.0.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1d9g6z89km4pg4gaak1r73yj5x9i1dls17lb3vwfwqjf0fa4nyf7") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static")))) (y #t)))

(define-public crate-path-dedot-3.0.9 (c (n "path-dedot") (v "3.0.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1n1pyjhnggpapvnk4cvh6jigfl6ra1z00y3fw9g45kramch563qd") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static")))) (y #t)))

(define-public crate-path-dedot-3.0.10 (c (n "path-dedot") (v "3.0.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0xsfmkb89wx9dfrqsjgvwvig75kvsh1b820f3gkzzyqmpwpz6vrs") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.11 (c (n "path-dedot") (v "3.0.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "17bsf62vxlid21k46a68vs52llxfky26dljn1allpsa6z8hbrsar") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.12 (c (n "path-dedot") (v "3.0.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "09kb4v8avsghiahr8s3ifnvg5nb1w7hqbnv8rxsvsslyni3wl50s") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.13 (c (n "path-dedot") (v "3.0.13") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1jff98xfq8p5zmkhnx2mygsgkbjab5qki7c6by74bx1myb1ani03") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.14 (c (n "path-dedot") (v "3.0.14") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "105gs4dpx2v53x2z8vca02q3qffs5nbhfavygx7m5s3bdyap5yjb") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.15 (c (n "path-dedot") (v "3.0.15") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "12j8bq65i35v7khj54v41yl6bpg36yxrdxl1rx39p6124h8z84b3") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.16 (c (n "path-dedot") (v "3.0.16") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0a0bd9hjyqgka8i53jrph3h2c8frdf1vjfv3skisb18n6fiy49pk") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.17 (c (n "path-dedot") (v "3.0.17") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0nxvvpwa3sj6ldv2y0q12761v2z597wd3w7b6j577cvj2clxa4fn") (f (quote (("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.0.18 (c (n "path-dedot") (v "3.0.18") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "00a9gqzh5a5g9zhc1fjzsb4p6z0npl99p0kj5fvn85jmjh6m90cs") (f (quote (("use_unix_paths_on_wasm") ("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static"))))))

(define-public crate-path-dedot-3.1.0 (c (n "path-dedot") (v "3.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0pm1sm2cgq3936sbgckkwlicnfqviiiwagparmrrkjvs6f3f8mcx") (f (quote (("use_unix_paths_on_wasm") ("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static")))) (r "1.56")))

(define-public crate-path-dedot-3.1.1 (c (n "path-dedot") (v "3.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "15wkx8q3vra34fslzlg1lkq7liyxwqrpbxiz44a28wa7w3bhmfh7") (f (quote (("use_unix_paths_on_wasm") ("unsafe_cache") ("once_cell_cache") ("lazy_static_cache" "lazy_static")))) (r "1.60")))

