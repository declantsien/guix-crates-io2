(define-module (crates-io pa th pathfinder_geometry) #:use-module (crates-io))

(define-public crate-pathfinder_geometry-0.3.0 (c (n "pathfinder_geometry") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_warn"))) (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.3") (d #t) (k 0)))) (h "0gvswhiwfcyf578lxh4xap663fxms38p3z48fwh04d2s9lgq99aq")))

(define-public crate-pathfinder_geometry-0.4.0 (c (n "pathfinder_geometry") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_warn"))) (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.4") (d #t) (k 0)))) (h "15kzvd3avq1zygw0pilwcj2jrbkzx813ww56kzs6dh074f5p02y1")))

(define-public crate-pathfinder_geometry-0.5.0 (c (n "pathfinder_geometry") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)))) (h "138dqfiajbs1bm7702k9wcpy0v857z0lxr5q1cz0kbrk1j3i47s5")))

(define-public crate-pathfinder_geometry-0.5.1 (c (n "pathfinder_geometry") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)))) (h "1lssir0s1cmrpzzrk49jm31nkssh2j715gryww6700x79rxpwyqb")))

