(define-module (crates-io pa th pathtracer) #:use-module (crates-io))

(define-public crate-pathtracer-0.6.5 (c (n "pathtracer") (v "0.6.5") (d (list (d (n "gif") (r "^0.10.2") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "18vbkz6x0rgqiah0p0jsqbyqp160gidj7rf7vvnsbc58kqy4la19")))

