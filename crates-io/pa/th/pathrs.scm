(define-module (crates-io pa th pathrs) #:use-module (crates-io))

(define-public crate-pathrs-0.0.0 (c (n "pathrs") (v "0.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "073nz1ca4yj9ggxgkyf6i078g6jx4rdnjdjdpgd9jwa6gkpn6mhz") (y #t)))

(define-public crate-pathrs-0.0.1 (c (n "pathrs") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "0y16vn247as0lwhypdk0yl1i2p9xjshwb2slhn5bkr4w2gvg9a07")))

(define-public crate-pathrs-0.0.2 (c (n "pathrs") (v "0.0.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "11jsxxgc4dsmlyzixzhyb3zx7qdmsc9fv23pl0jdvi46lqrdbxr1")))

