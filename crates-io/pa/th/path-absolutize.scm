(define-module (crates-io pa th path-absolutize) #:use-module (crates-io))

(define-public crate-path-absolutize-1.0.0 (c (n "path-absolutize") (v "1.0.0") (d (list (d (n "path-dedot") (r "^1.0.4") (d #t) (k 0)))) (h "1pjvf514xj1di9a0qdq9pbqhj5x0iq4kiy85lavzi49lv1vhpp3l")))

(define-public crate-path-absolutize-1.0.1 (c (n "path-absolutize") (v "1.0.1") (d (list (d (n "path-dedot") (r "^1.0.5") (d #t) (k 0)))) (h "10ackcxpnddjnq3q8h1mqi1g4ri2r0k4dm5ya83bgxaxk2h3hbvb")))

(define-public crate-path-absolutize-1.0.2 (c (n "path-absolutize") (v "1.0.2") (d (list (d (n "path-dedot") (r "^1.0.5") (d #t) (k 0)))) (h "0nlqv9vxcnvdb9zxbarvs4s6p0d8z9llwdcr7c1xpycyh1qrhc9s")))

(define-public crate-path-absolutize-1.1.0 (c (n "path-absolutize") (v "1.1.0") (d (list (d (n "path-dedot") (r "^1.1.5") (d #t) (k 0)))) (h "072yq0mhy70y7fsnbr9jpxd9pa044y73yhhi8il9pwqzijs7zh03")))

(define-public crate-path-absolutize-1.1.1 (c (n "path-absolutize") (v "1.1.1") (d (list (d (n "path-dedot") (r "^1.1.6") (d #t) (k 0)))) (h "1iyfka1d0a2q1d7f6awi3kzcc9d8hspyqimz687vji80fd30blkb")))

(define-public crate-path-absolutize-1.1.2 (c (n "path-absolutize") (v "1.1.2") (d (list (d (n "path-dedot") (r "^1.1.6") (d #t) (k 0)))) (h "0gk7j0x4cw1pa03ib9yvnlcwiv68vs4zhlwnxn65ir00h348br49")))

(define-public crate-path-absolutize-1.1.3 (c (n "path-absolutize") (v "1.1.3") (d (list (d (n "path-dedot") (r "^1.1.6") (d #t) (k 0)))) (h "0hka5y5ink585j667c9qs965iicd046vya03680bmj8xrx9w71ky")))

(define-public crate-path-absolutize-1.1.4 (c (n "path-absolutize") (v "1.1.4") (d (list (d (n "path-dedot") (r "^1.1.10") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "17mnmsm5n8npfvjyklwmaqjbkna7an2zrksyxzayjk9r273bbxbq")))

(define-public crate-path-absolutize-1.1.5 (c (n "path-absolutize") (v "1.1.5") (d (list (d (n "path-dedot") (r "^1.1.11") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "0n0miczagr4qh4imxq4lndvq7p4vjwan9grnnlh3bsak25gxqfgs")))

(define-public crate-path-absolutize-1.1.6 (c (n "path-absolutize") (v "1.1.6") (d (list (d (n "path-dedot") (r "^1.1.11") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "0vn1px9rn5ids93fmyv7pmxj364l1iayli4sch0a4ckvzymk4zks")))

(define-public crate-path-absolutize-1.1.7 (c (n "path-absolutize") (v "1.1.7") (d (list (d (n "path-dedot") (r "^1.1.11") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "0zw706x5c11zizsd9vdmrqam776jvaqyk5825f9yr49pgck1cp96")))

(define-public crate-path-absolutize-1.2.0 (c (n "path-absolutize") (v "1.2.0") (d (list (d (n "path-dedot") (r "^1.2.2") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "1p58ycnj7pgwhzbgdm7fs7jvwbiwhq8irldnsk2dak2vwaddfnzp") (y #t)))

(define-public crate-path-absolutize-1.2.1 (c (n "path-absolutize") (v "1.2.1") (d (list (d (n "path-dedot") (r "^1.2.4") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.1") (d #t) (k 0)))) (h "142225b05ljy8567kn0vri944bghsb786xj58538nxcsgn1drvnf")))

(define-public crate-path-absolutize-2.0.0 (c (n "path-absolutize") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^2") (o #t) (d #t) (k 0)) (d (n "path-dedot-lazy-static-cache") (r "^2") (f (quote ("lazy_static_cache"))) (o #t) (k 0) (p "path-dedot")) (d (n "path-dedot-unsafe-cache") (r "^2") (f (quote ("unsafe_cache"))) (o #t) (k 0) (p "path-dedot")) (d (n "slash-formatter") (r "^2.2.6") (d #t) (k 0)))) (h "0bfpzddzlsk5gg436g8inycivnpp4hlhp52i40jz67vzjm35q42z") (f (quote (("unsafe_cache" "path-dedot-unsafe-cache") ("lazy_static_cache" "path-dedot-lazy-static-cache") ("default" "path-dedot"))))))

(define-public crate-path-absolutize-2.0.1 (c (n "path-absolutize") (v "2.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^2") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0iagkvwl1bb2s251pyl7dz0z48dgzkz18i0rg9ana2a8bgjk8xw1") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-2.0.2 (c (n "path-absolutize") (v "2.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^2") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.6") (d #t) (t "cfg(windows)") (k 2)))) (h "11idyz7ds8s36bwkfiyjqh7655rv9s1v7pdl3yghfmid2n8cydg2") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.0 (c (n "path-absolutize") (v "3.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3") (d #t) (k 0)) (d (n "slash-formatter") (r "^2.2.6") (d #t) (t "cfg(windows)") (k 2)))) (h "0m7x9y3arybp43lsqf1lzi0yq3fb7r1znhpkcs4i95zzj3m1l5gi") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.1 (c (n "path-absolutize") (v "3.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1yr2lzqw1w2hcjgavp41mjhzs24b6g5nfaz7wgq1mh0xl6qkzvcf") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.2 (c (n "path-absolutize") (v "3.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "0755k3iz8xd758m6gd40cc7s3x5pw00jzl9xvb7nkdp81ydzwfq8") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.3 (c (n "path-absolutize") (v "3.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "09cacyfg5j8s9qq77351vky4c4x1kjv9jvxnhvmp0rxl4mq7zcp5") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.4 (c (n "path-absolutize") (v "3.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.4") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "198d9bj3qrsnl3kdsjlgxc3x259bx73zj750hcrzx2qgp8ch111k") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.5 (c (n "path-absolutize") (v "3.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.4") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1warl0mn62lsi211dfl51kik3d01l06f39bd9ysad3ywig2jy74s") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.6 (c (n "path-absolutize") (v "3.0.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.4") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "16n57apk1qmhm1fjnih6aqr53yhmmf73kr26vf2fvvzslnmb4sks") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.7 (c (n "path-absolutize") (v "3.0.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.4") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1s9y0xmsmhv43kxrnij0kgp3vgd9znkpnx450fnzjqnpamd4bgx7") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache")))) (y #t)))

(define-public crate-path-absolutize-3.0.8 (c (n "path-absolutize") (v "3.0.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.10") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1cdnpnhzpnj8s60yc1cz98xffciv4ylk3gw5qjhw2i0zxjkm256h") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.9 (c (n "path-absolutize") (v "3.0.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.10") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "00hwxgf5d6x5g7mwjly4y0vh3m6i7wa0cl40w1ccri13dfdcql8g") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.10 (c (n "path-absolutize") (v "3.0.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.12") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1ff9f4820jm3qhd6ir5aybbmfjhyhzhsmqvfzvza02b018pymzph") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.11 (c (n "path-absolutize") (v "3.0.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.12") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1x9v9dsmr1q3y7aiwsdh6w9dkwij1mcvm09ikr9l4yrsga52k25j") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.12 (c (n "path-absolutize") (v "3.0.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.12") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "0baxbh6nh1gaqgnpdahlf1p6sl8hkd2n2iawa4ivbsn4q7bpjaha") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.13 (c (n "path-absolutize") (v "3.0.13") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.12") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "04qzdj2f1ambr01bpzl84f05k0ak15630f642h7n8dlppm04ppnk") (f (quote (("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.0.14 (c (n "path-absolutize") (v "3.0.14") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.0.18") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "0licsnmy1khaxa6213fvaa3q1dvx6aiwdhqq1kcjawvgn69lj78g") (f (quote (("use_unix_paths_on_wasm" "path-dedot/use_unix_paths_on_wasm") ("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache"))))))

(define-public crate-path-absolutize-3.1.0 (c (n "path-absolutize") (v "3.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.1.0") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1pm0sx3d48z1b77fzidlw1xfy029m2q48kxl6wdlw89sqsakbss3") (f (quote (("use_unix_paths_on_wasm" "path-dedot/use_unix_paths_on_wasm") ("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache")))) (r "1.56")))

(define-public crate-path-absolutize-3.1.1 (c (n "path-absolutize") (v "3.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "path-dedot") (r "^3.1.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (t "cfg(windows)") (k 2)))) (h "1xc36c5lz187wy452qph3lrr41x8ffgxk1clj2s9b8czwwgkibz4") (f (quote (("use_unix_paths_on_wasm" "path-dedot/use_unix_paths_on_wasm") ("unsafe_cache" "path-dedot/unsafe_cache") ("once_cell_cache" "path-dedot/once_cell_cache") ("lazy_static_cache" "path-dedot/lazy_static_cache")))) (r "1.60")))

