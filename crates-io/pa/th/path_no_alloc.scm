(define-module (crates-io pa th path_no_alloc) #:use-module (crates-io))

(define-public crate-path_no_alloc-0.1.0 (c (n "path_no_alloc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1mcxiqimcw8vj5wfza0bq7xds80az2kg17q9lcc4nxnd2gpj06z1")))

(define-public crate-path_no_alloc-0.1.1-beta (c (n "path_no_alloc") (v "0.1.1-beta") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1y9gr8s30k0w9c6i0r0qi8p2x9a898bl9pf09n1d45g209xsm6gm")))

(define-public crate-path_no_alloc-0.1.1 (c (n "path_no_alloc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1kkyligcnz46gk9fypcmzgxmbary9bc44cq4qmw314fpkxsgv4z4")))

(define-public crate-path_no_alloc-0.1.2 (c (n "path_no_alloc") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0i83v6z5ip0shczq3v0nlxiglsx8h0c90mh7ayfihy4ww6wrdmxz")))

