(define-module (crates-io pa th pathfinder_color) #:use-module (crates-io))

(define-public crate-pathfinder_color-0.1.0 (c (n "pathfinder_color") (v "0.1.0") (d (list (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)))) (h "0m1kp5b92n9q9z3sgw53lcl22862yiwb5rk7c3qxama97a3k1qr0")))

(define-public crate-pathfinder_color-0.5.0 (c (n "pathfinder_color") (v "0.5.0") (d (list (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)))) (h "0qhcgy69y8qshfl09jliw1qnnai6z5nyak9p3dgf6nfmfz9c1gb9")))

