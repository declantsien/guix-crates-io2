(define-module (crates-io pa th pathetic) #:use-module (crates-io))

(define-public crate-pathetic-0.1.0 (c (n "pathetic") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1dys4bzcq4chifyhirvsphvnixashkb8zz4w6mncmcaahlivph0q")))

(define-public crate-pathetic-0.2.0 (c (n "pathetic") (v "0.2.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1p361hiblram524fpac8yw8gfvnfzlnnwmkp1sifabd31bmnrss5")))

(define-public crate-pathetic-0.2.1 (c (n "pathetic") (v "0.2.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "02vdwjmv63gjhpz141sk8d5lngh1rnyyr0fwgjz92b9nmd7kf96x")))

(define-public crate-pathetic-0.3.0 (c (n "pathetic") (v "0.3.0") (d (list (d (n "actix-web") (r "^2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "19c13hdc73g31ypxa3nnwz322rxj1g1gypcvw1bi5nqwsqpng77d")))

