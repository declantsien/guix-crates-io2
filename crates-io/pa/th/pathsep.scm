(define-module (crates-io pa th pathsep) #:use-module (crates-io))

(define-public crate-pathsep-0.1.0 (c (n "pathsep") (v "0.1.0") (h "0p6a015xmkyqqi6by0arsn237i8ysx59ryljmq1sr3hd2lb0rbip")))

(define-public crate-pathsep-0.1.1 (c (n "pathsep") (v "0.1.1") (h "16alazm41dlbfylvy5aiahhj16k6dkwsws9fj9sys9mk1hvkq8ps")))

