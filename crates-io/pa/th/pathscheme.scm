(define-module (crates-io pa th pathscheme) #:use-module (crates-io))

(define-public crate-pathscheme-0.1.0 (c (n "pathscheme") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m7gmrkzr5lxsh5ldvl2nyknbqiiamh02h6d899m3lb01gk51kyg")))

(define-public crate-pathscheme-0.2.0 (c (n "pathscheme") (v "0.2.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14zb4fqys784bm6dw25f69liffhzkf21vpljh146pzmrgzd29ihz")))

(define-public crate-pathscheme-0.2.1 (c (n "pathscheme") (v "0.2.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0syrrgdawj537ix1gx11j4ll4sxwyklbd0q63qhhn3b2vpk1jdv4")))

