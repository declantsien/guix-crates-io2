(define-module (crates-io pa th pathfinder_text) #:use-module (crates-io))

(define-public crate-pathfinder_text-0.5.0 (c (n "pathfinder_text") (v "0.5.0") (d (list (d (n "font-kit") (r "^0.6") (d #t) (k 0)) (d (n "pathfinder_content") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_renderer") (r "^0.5") (d #t) (k 0)) (d (n "skribo") (r "^0.1") (d #t) (k 0)))) (h "1ldq60pmbpw987gqq6vyz5qfnmgxcxgl1zv5qmbdqy2h486dzxjb")))

