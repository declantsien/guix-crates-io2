(define-module (crates-io pa th path-url-rewrite) #:use-module (crates-io))

(define-public crate-path-url-rewrite-0.0.1 (c (n "path-url-rewrite") (v "0.0.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "^0.76") (f (quote ("ecma_plugin_transform"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1wnkx460ni1s6gsimd6555qd3d1nlrqdf93r720yrlldgpwp91xm")))

