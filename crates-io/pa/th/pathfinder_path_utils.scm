(define-module (crates-io pa th pathfinder_path_utils) #:use-module (crates-io))

(define-public crate-pathfinder_path_utils-0.2.0 (c (n "pathfinder_path_utils") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "lyon_path") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1asgfzqwa46hhpnhc0vwr8cdb40cg18hjwxkahxyh5dh6i12dj7x")))

