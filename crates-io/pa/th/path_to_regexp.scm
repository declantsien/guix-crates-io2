(define-module (crates-io pa th path_to_regexp) #:use-module (crates-io))

(define-public crate-path_to_regexp-0.1.0 (c (n "path_to_regexp") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)))) (h "1psynfjpjj8x754r2p1lbjsbxmb9l7s8iknysz9058hcxq0cps8w")))

(define-public crate-path_to_regexp-0.1.1 (c (n "path_to_regexp") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "125gwjikrmkl5y4kv8512sg1s65yqsiv2wz2aw7ynj1c0n0gspxb")))

(define-public crate-path_to_regexp-0.1.2 (c (n "path_to_regexp") (v "0.1.2") (d (list (d (n "fancy-regex") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0fr25dlk2g5fzdksrvxbpy6h1g3japz8ilz054w54d877jy3xg27")))

