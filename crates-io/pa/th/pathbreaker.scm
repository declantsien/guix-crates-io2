(define-module (crates-io pa th pathbreaker) #:use-module (crates-io))

(define-public crate-pathbreaker-0.1.0 (c (n "pathbreaker") (v "0.1.0") (d (list (d (n "kurbo") (r "^0.6") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (d #t) (k 0)))) (h "1v5zjjka261bg3zw4y24cww1fw7827s1yxny8ycv2fyccp7p5j84")))

