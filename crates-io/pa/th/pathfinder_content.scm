(define-module (crates-io pa th pathfinder_content) #:use-module (crates-io))

(define-public crate-pathfinder_content-0.5.0 (c (n "pathfinder_content") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathfinder_color") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "0sfa4n4cszl10hqhzkf9flrs8pk253wprnk7jiqq59vwl37y0jb4") (f (quote (("pf-image" "image") ("default" "pf-image"))))))

