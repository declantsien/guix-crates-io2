(define-module (crates-io pa th path-value) #:use-module (crates-io))

(define-public crate-path-value-0.1.0 (c (n "path-value") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "100amsqx51l93wwmpaj485wimh13qq9h8f214lyn3jzlpimlxdf7") (y #t)))

(define-public crate-path-value-0.1.1 (c (n "path-value") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12673ack5sdznbrf6asp6k776v2smaj7bg2rinnzp2vcgbjvgpd2") (y #t)))

(define-public crate-path-value-0.1.2 (c (n "path-value") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16j9r47sijddfhwkd7243q5qw2g0skis1hl8clrd5mxbjz0pwv8h") (y #t)))

(define-public crate-path-value-0.1.3 (c (n "path-value") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rq2hjq9jzwhy21k41qpahby7crdrm1khysaj0xv89zg66wdk9m4") (y #t)))

(define-public crate-path-value-0.1.4 (c (n "path-value") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0kq3dbczqamqh2w0wsq2pqs1bj96gx8mxmwpr7201x40z7xfzd4q")))

(define-public crate-path-value-0.1.5 (c (n "path-value") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1im2zcdplplbp37nfzf0sphlyp1yhdshx4y13z2vys257py0l924")))

