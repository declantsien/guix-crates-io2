(define-module (crates-io pa th path-calculate) #:use-module (crates-io))

(define-public crate-path-calculate-0.1.0 (c (n "path-calculate") (v "0.1.0") (d (list (d (n "path-absolutize") (r "^3.0.6") (d #t) (k 0)))) (h "194gm5nm99wn5rxw916nqjmdhkp33f8adn1l5mb8a3061gl6hw0d") (y #t)))

(define-public crate-path-calculate-0.1.1 (c (n "path-calculate") (v "0.1.1") (d (list (d (n "path-absolutize") (r "^3.0.6") (d #t) (k 0)))) (h "080nmxydqyd8f23285cgycp23dim377rbhwp4cfki1aybkb0yhyv") (y #t)))

(define-public crate-path-calculate-0.1.2 (c (n "path-calculate") (v "0.1.2") (d (list (d (n "path-absolutize") (r "^3.0.6") (d #t) (k 0)))) (h "1zi5ls6liv4761613xayl74shbiygp8zzz6va6s0nj72xsc09i7x")))

(define-public crate-path-calculate-0.1.3 (c (n "path-calculate") (v "0.1.3") (d (list (d (n "path-absolutize") (r "^3.0.6") (d #t) (k 0)))) (h "19rd52j8giqvispf169vaymdf9dkh26vk2ni246llyl6758hb426")))

