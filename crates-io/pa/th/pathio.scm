(define-module (crates-io pa th pathio) #:use-module (crates-io))

(define-public crate-pathio-0.0.1 (c (n "pathio") (v "0.0.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0fya0l5cj74pmrxjmw6qrgfg34fzwjd2nwyicbz3iqnm2y3nijly")))

(define-public crate-pathio-0.0.2 (c (n "pathio") (v "0.0.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1p7ll1jbi989jax0ldbja2b3p0frxrqll2vz9kbxcp7y0l10a4s5")))

(define-public crate-pathio-0.0.3 (c (n "pathio") (v "0.0.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0kvyki9pqca749rsyc8mf2rsv6cc5vzwidp6hqmb3bphnlmxgzqq")))

(define-public crate-pathio-0.0.4 (c (n "pathio") (v "0.0.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1qf9hvx16zvhqrzbj6wyh5br7n11ygy2bicjbgkdzn9g17sd4fm1")))

(define-public crate-pathio-0.0.5 (c (n "pathio") (v "0.0.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0ks03bmd31h3qxxpwhzi2kj9xgh50vpyzk0pkm4wav37v7da2n06")))

(define-public crate-pathio-0.1.0 (c (n "pathio") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "13lbs8ais0balchrkk2f57bl9lzyz0gxz7ldr7lnfpijqsn4lr5s")))

(define-public crate-pathio-0.1.1 (c (n "pathio") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (o #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)))) (h "1r82v50mja7nhk1fq6dq96pr51d7gnifnpcnysfmlqpiqsyk80vv") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.1.2 (c (n "pathio") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (o #t) (k 0)) (d (n "bevy_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)))) (h "0557c6cfgy97qip2jgqwp9b17646xhkqq79y0mcbm1bl0salx118") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.1.3 (c (n "pathio") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0r84g1la64x0zvhbd499yz32193qzf7ahzaissf2whpxx646qpik") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.1.4 (c (n "pathio") (v "0.1.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1sshlvdn6vw91chcycv09nacf32crcdysjc7q5zaz2vkl5sgm3a6") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.1.5 (c (n "pathio") (v "0.1.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "12sippqj2r54iyqvs747fd86a5sqprmsfydy7c98yppwpgajf8qn") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.1.6 (c (n "pathio") (v "0.1.6") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0q3g4rxm3lysc6v3vxwpm8bqq0vq9nqik1g6lbhbdr507hymrq5k") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.2.0 (c (n "pathio") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1ayzlpskf3layd3mc4917wwflp5gs21q5bj5hvkfzj9kp0z7hcxj") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.2.1 (c (n "pathio") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1ygd19f64lw8w3gc53cjrbwsrkdd3cfd86jd987zkrmqfl690c7m") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.2.2 (c (n "pathio") (v "0.2.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "13sksmi1h3wf83m4zc9qx02ndmqzjcsichw34lbd77alwmh68wlz") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

(define-public crate-pathio-0.2.3 (c (n "pathio") (v "0.2.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1i55g65f3z1rxaq4gcwx0m6xy50105l2ncqxbiilsadcl6ch3v6d") (s 2) (e (quote (("serde" "dep:serde" "ahash/serde") ("bevy" "dep:bevy"))))))

