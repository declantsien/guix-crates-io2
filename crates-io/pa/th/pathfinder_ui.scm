(define-module (crates-io pa th pathfinder_ui) #:use-module (crates-io))

(define-public crate-pathfinder_ui-0.5.0 (c (n "pathfinder_ui") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pathfinder_color") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_gpu") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_resources") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12gf0yxwgvz1ddig70c1py98vlc9lwn1f906qxz1crrjfh0x4hd0")))

