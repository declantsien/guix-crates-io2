(define-module (crates-io pa th path-router) #:use-module (crates-io))

(define-public crate-path-router-0.1.0 (c (n "path-router") (v "0.1.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1jv27rfa5r2x05vpbmfgib07k8c1pm0m5ggj17pip9qnlxjh1s73") (y #t)))

(define-public crate-path-router-0.1.1 (c (n "path-router") (v "0.1.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "12w19niqnnrzcwisck2cwj9n267mixglszcnhy4c354fqr2r3hnf")))

(define-public crate-path-router-0.2.0 (c (n "path-router") (v "0.2.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "18l03h7jfvsr1cpx7xv4mcmvf2x01q1g19zrmdcb8mvxkcpx7bif")))

(define-public crate-path-router-0.2.1 (c (n "path-router") (v "0.2.1") (h "0xhgx7vbh40zs91xwxkbnd755170kgihi6p27hjjrj8c6x7m70sg")))

