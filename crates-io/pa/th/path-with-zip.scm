(define-module (crates-io pa th path-with-zip) #:use-module (crates-io))

(define-public crate-path-with-zip-0.0.2 (c (n "path-with-zip") (v "0.0.2") (d (list (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "075qiy3rr2svz5bsvdz81g6v78fpicdh5fii9ciq4m67bi0jpl28")))

(define-public crate-path-with-zip-0.1.0 (c (n "path-with-zip") (v "0.1.0") (d (list (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "09w8kdf1ygj77yqvlwlqvkw7wljnfzm0dki68i6p2dg01dymifc0")))

