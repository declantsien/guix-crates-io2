(define-module (crates-io pa th pathext) #:use-module (crates-io))

(define-public crate-pathext-0.1.0 (c (n "pathext") (v "0.1.0") (h "119nsqjcgjf6iyr94lqpzw80afnlaa4rrbpnqllw4nawdcxlsgnc") (y #t)))

(define-public crate-pathext-0.1.1 (c (n "pathext") (v "0.1.1") (h "0yzividczj0l48r3zdivcb82v3qb06vmpd9xgm89x4npjj467m02") (y #t)))

(define-public crate-pathext-0.1.2 (c (n "pathext") (v "0.1.2") (h "04haqqmc2fagch34clban9drvz5v99hnq08fgvrhmhb2nllaz6ql")))

(define-public crate-pathext-0.1.3 (c (n "pathext") (v "0.1.3") (h "0014dhnzp67imrcxhdgpmlcb1926658dbbivgx7v5q6plrmxhmzb")))

(define-public crate-pathext-0.1.4 (c (n "pathext") (v "0.1.4") (h "1b6wxqdja863n2c1li81yry7mc2lf0i5l3sw5r8kimnphrd0rq9a")))

(define-public crate-pathext-0.1.5 (c (n "pathext") (v "0.1.5") (h "1752w972qpa8r3n0b7gg4f6w97wgiwbpc2slnm7qsq2v4wfsgy0l")))

(define-public crate-pathext-0.1.6 (c (n "pathext") (v "0.1.6") (h "1qjadkirb0b434ljxfinn32d8fx2yiaqngyddz76iijlvy46rfmn")))

(define-public crate-pathext-0.2.0 (c (n "pathext") (v "0.2.0") (h "1cs0akx8ynj1v7llb21mv273cm8gdhrpy52jdmzy99f7w84hdkrd")))

(define-public crate-pathext-0.2.1 (c (n "pathext") (v "0.2.1") (h "0h5vfhqy77m32jk53jnnwg7icak20izmdzqz3lmx9zgl52p4s75f")))

