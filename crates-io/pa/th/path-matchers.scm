(define-module (crates-io pa th path-matchers) #:use-module (crates-io))

(define-public crate-path-matchers-1.0.0 (c (n "path-matchers") (v "1.0.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1niy71jsqfx9214k5jpr6xxw02wjq0shcxza2qs0216hds7haa7j") (f (quote (("default" "glob"))))))

(define-public crate-path-matchers-1.0.1 (c (n "path-matchers") (v "1.0.1") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0q02d70ik4lfigdmi79lrs6xbf9f8k37kipfb5fhr8wfpr5z5swm") (f (quote (("default" "glob"))))))

(define-public crate-path-matchers-1.0.2 (c (n "path-matchers") (v "1.0.2") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0avv7zcg7jakz765kg0zbva7nsv8mgcjj0jz78cyqybnlir9pk9n") (f (quote (("default" "glob"))))))

