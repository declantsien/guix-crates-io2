(define-module (crates-io pa th pathfinder) #:use-module (crates-io))

(define-public crate-pathfinder-0.2.1 (c (n "pathfinder") (v "0.2.1") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "013cb6brz40nzbgyp56w4xrmzb4fwxa0dmvv47aniczgg9l300cf")))

(define-public crate-pathfinder-0.2.2 (c (n "pathfinder") (v "0.2.2") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0632cgli580m4gnr9blawszq3f55xr9xk5l0plq5g8lfk2shm9np")))

(define-public crate-pathfinder-0.3.0 (c (n "pathfinder") (v "0.3.0") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0j12l6534w33xzpc9izdg65mfpg4czsnhaj1s5xd72y8s1inwcx1")))

(define-public crate-pathfinder-0.3.1 (c (n "pathfinder") (v "0.3.1") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0f5h504ffbsj9nzkwxw4gqqkr89nfmrak2k6dbf366qn7crn991b")))

(define-public crate-pathfinder-0.3.2 (c (n "pathfinder") (v "0.3.2") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mqyh9a864a3vdw25p0cnz5pb89rv3ydd69qvikw1mwhlnpm31n5")))

(define-public crate-pathfinder-0.3.3 (c (n "pathfinder") (v "0.3.3") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0rq6q4v02l9d3jmvqmg4ar6fkbz9ffainlash61k82malfsqxrnk")))

(define-public crate-pathfinder-0.3.5 (c (n "pathfinder") (v "0.3.5") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "07zfm5mcgwycdiyw5pcf6dxnh388qxrcpab2b1dmfm7g338vg420")))

(define-public crate-pathfinder-0.3.6 (c (n "pathfinder") (v "0.3.6") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1aa0dgfwix2mm7d8afbqq6anqnis8s3pfmsp4209xy7ppd08917z")))

(define-public crate-pathfinder-0.3.7 (c (n "pathfinder") (v "0.3.7") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0k49n2gm0y0grjsp6ay7v64ap1lb95a6axgaslv37s2hxash71cv")))

(define-public crate-pathfinder-0.3.8 (c (n "pathfinder") (v "0.3.8") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1z12hjxlzpj09wdlmyb7a464d33qwz24x4w3bmyjgk7zglnlwcjl")))

(define-public crate-pathfinder-0.3.9 (c (n "pathfinder") (v "0.3.9") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gjhvnn1xplny0zi91zcnpn19knp2dpwx3q4b90aykbvw61in73j")))

(define-public crate-pathfinder-0.4.0 (c (n "pathfinder") (v "0.4.0") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01xrj6pc2lgq4a3785jhcxf96m5sg8w52gy16fxd2b0hkhvnx0dq")))

(define-public crate-pathfinder-0.4.1 (c (n "pathfinder") (v "0.4.1") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1im4g207hyhwrk4bxmdj7g6ydnf3knv5c02di9aks6bws2n5l1av")))

(define-public crate-pathfinder-0.4.2 (c (n "pathfinder") (v "0.4.2") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1s9nkk2mfm5aqjb0xrbvzjkh7y67h7n7y9d7cn3xklk215wxdbn4")))

(define-public crate-pathfinder-0.4.3 (c (n "pathfinder") (v "0.4.3") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1naa7ksc5vf9c7ssgkvwa0mcpjmcm88qpxfg3s1ah2flmjh4b97i")))

(define-public crate-pathfinder-0.5.0 (c (n "pathfinder") (v "0.5.0") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0yq3bsar8dlpn6c2ydwiqcdwcmhhk7j5x7x6j3s5ca0224xrkcr3")))

(define-public crate-pathfinder-0.5.1 (c (n "pathfinder") (v "0.5.1") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mnb4vcxr7j3lppwd3s6xx693vwvc5pjp5syr69dwgwzqkav7qj4")))

(define-public crate-pathfinder-0.5.2 (c (n "pathfinder") (v "0.5.2") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1im8p6z3xq8fb834ppb02xb7vp4ifnp6mgpvypm4jb4s8vf8vyb7")))

(define-public crate-pathfinder-0.5.3 (c (n "pathfinder") (v "0.5.3") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "194mh15z1na1xagind3dah20gmqwd4jv40c8cvasax5j631g2a32")))

(define-public crate-pathfinder-0.5.4 (c (n "pathfinder") (v "0.5.4") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1n6ccf7fn9xcpzbna443v8jd5308k3hfwx49zan5b1kz4ynla0m5")))

(define-public crate-pathfinder-0.5.5 (c (n "pathfinder") (v "0.5.5") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "078l3qp7k8sw08snx4ij4klvfh1am2hp8wqgj3gvy6q25mgi12di")))

(define-public crate-pathfinder-0.5.6 (c (n "pathfinder") (v "0.5.6") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "12zaqrwpsfanbkbi7krrgf3ynx16c7959xlslfakqyn7hmppifah")))

(define-public crate-pathfinder-0.5.7 (c (n "pathfinder") (v "0.5.7") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1hci2i9rqf6sqsy3svmpxcwxrv33j2hkvhif6rczhn2klh0j52hh")))

(define-public crate-pathfinder-0.5.8 (c (n "pathfinder") (v "0.5.8") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0c19ving9inyg8isv3ky0rl1sp95vmifwi1mdqbsdabf9rw418ab")))

(define-public crate-pathfinder-0.5.9 (c (n "pathfinder") (v "0.5.9") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "091ylsisqbgmjbb922lfhkswgmw1qpi3mm8snz5k93ry3k4r6hvx")))

(define-public crate-pathfinder-0.5.10 (c (n "pathfinder") (v "0.5.10") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0vi1hh0h752p9vilcbkf68d2hwczvczzkr0xndp5468iwwlc00aq")))

(define-public crate-pathfinder-0.6.0 (c (n "pathfinder") (v "0.6.0") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0f9j0cakvvdj5jr1vrrdpy220rgrgy6yg7avlnghwi5yg5vs1l7k")))

(define-public crate-pathfinder-0.6.1 (c (n "pathfinder") (v "0.6.1") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0jx5b1vwbfwd3cm9cr9j39950vb3n2dn4ysbbqc7ffkmi74d0pdv")))

(define-public crate-pathfinder-0.6.2 (c (n "pathfinder") (v "0.6.2") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "02z4bqzrrv0afjlj0ghnlnd29bsw1wn58hbmjv4l942kfgk36xiz")))

(define-public crate-pathfinder-0.6.3 (c (n "pathfinder") (v "0.6.3") (d (list (d (n "gif") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "132nkgsjy4dgq9f56v1j4y4i6zd7qda6lbzl4abl85vwpkd7vhzb")))

(define-public crate-pathfinder-0.6.4 (c (n "pathfinder") (v "0.6.4") (d (list (d (n "gif") (r "^0.10.2") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0bl90aqq00rm964vvk5fm2ha5p3i20w0wybfyj96509yaknys18z")))

(define-public crate-pathfinder-0.6.5 (c (n "pathfinder") (v "0.6.5") (d (list (d (n "gif") (r "^0.10.2") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pythagoras") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "17drx7yzps4w9bsj6x2sq94lbiz9n1rw9p3hygc3k1zpxidn0mrf")))

