(define-module (crates-io pa th path_ratchet) #:use-module (crates-io))

(define-public crate-path_ratchet-0.1.0 (c (n "path_ratchet") (v "0.1.0") (h "0dxakxflb9m0019f32n8vrdcqnmcjnxvybcc28n3k21z07kpgq53")))

(define-public crate-path_ratchet-0.2.0 (c (n "path_ratchet") (v "0.2.0") (h "0xzp6qjfrk6r4gjb8a5mlxckyg0slj3gpak3f3hgr6jswc38fq46")))

(define-public crate-path_ratchet-0.3.0 (c (n "path_ratchet") (v "0.3.0") (h "0gwqwy1qpawlawzqz2sq44wg2kigvl0gwispdxhlv18an3x49pz5")))

