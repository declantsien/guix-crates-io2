(define-module (crates-io pa th path) #:use-module (crates-io))

(define-public crate-path-0.1.0 (c (n "path") (v "0.1.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "1dz6ki8hpdhvrgj2s584mzxaqyv9m14dxk0d22jav7ycncw0f4yd")))

(define-public crate-path-0.2.0 (c (n "path") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "11x6vrwmwqgdvk092dfdv3ds51wyi17kklvwivwgyl842q9xhax1")))

(define-public crate-path-0.3.0 (c (n "path") (v "0.3.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "0ij344wf1qknizx75rrb14bf8ygwnglmaymkxmgp2za2aiwnff3w")))

(define-public crate-path-0.4.0 (c (n "path") (v "0.4.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "0yy5nr0yfxn1sci9nyk75l7nh0gnzgcjxgp17j2gxz362gcazy11")))

(define-public crate-path-0.5.0 (c (n "path") (v "0.5.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "1yxcbrcnx31z5517bmylkr54l784z841aihkwqwwgkjyszwdliz5")))

(define-public crate-path-0.6.0 (c (n "path") (v "0.6.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "1kicpqbi9pzkdq3jcgsrn3pqh60s4srscs3jkmlxff7w255pqx6c")))

(define-public crate-path-0.7.0 (c (n "path") (v "0.7.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "0wzjvhpvn8wdlgbw5xr8ij996i5dsi91frrx3ak6d69kf3kn7pby")))

(define-public crate-path-0.8.0 (c (n "path") (v "0.8.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "08sla8al865md0hl3rji64zx7kvbbisnygqvlll8cry1hp6iba4v")))

(define-public crate-path-0.8.1 (c (n "path") (v "0.8.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "06fnnirzy6nb9cd08jnhqjkbhns46yi9yg5bjh500bbxl72wxyxm")))

(define-public crate-path-0.8.2 (c (n "path") (v "0.8.2") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "0a4xxf8nxp74d0yq3q5lngcb7p1zdcqvy2mjdfbcdwlw8i55f5sr")))

