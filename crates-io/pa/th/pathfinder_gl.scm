(define-module (crates-io pa th pathfinder_gl) #:use-module (crates-io))

(define-public crate-pathfinder_gl-0.5.0 (c (n "pathfinder_gl") (v "0.5.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "half") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_gpu") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_resources") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_simd") (r "^0.5") (d #t) (k 0)))) (h "11760x5xzgmcjimqnqj4awjm9f9x96iiy8qzy7sbysm8ahfhydnv")))

