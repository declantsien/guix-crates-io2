(define-module (crates-io pa th pathfinder_canvas) #:use-module (crates-io))

(define-public crate-pathfinder_canvas-0.5.0 (c (n "pathfinder_canvas") (v "0.5.0") (d (list (d (n "font-kit") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "pathfinder_color") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_content") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_renderer") (r "^0.5") (d #t) (k 0)) (d (n "pathfinder_text") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "skribo") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jgi9x5fr4gcqcg6g00pqm5l8f687i2h85h25xv10rvn8bmasrm5") (f (quote (("pf-text" "pathfinder_text" "skribo" "font-kit"))))))

