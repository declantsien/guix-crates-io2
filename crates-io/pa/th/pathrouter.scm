(define-module (crates-io pa th pathrouter) #:use-module (crates-io))

(define-public crate-pathrouter-0.1.0-beta.1 (c (n "pathrouter") (v "0.1.0-beta.1") (h "195s1l2babp5gnpymjjlsmvm2ibnvwcpk0j1ji61jxqzq3rk4fwx")))

(define-public crate-pathrouter-0.1.0 (c (n "pathrouter") (v "0.1.0") (h "0yrydsnxms43n79dqibk6cj9bpv7v2nay5rd1ag9sc5m275dbi4h")))

(define-public crate-pathrouter-0.2.0-beta.1 (c (n "pathrouter") (v "0.2.0-beta.1") (h "1y2srl98c0gvq1a22n7a429sinhl3mfnc357g6cngi5069za5v4s")))

(define-public crate-pathrouter-0.2.0 (c (n "pathrouter") (v "0.2.0") (h "1jb31q684wci2qdzfq26x6wzdxmglga9zrxrigrx6bghrcvxp1pj")))

