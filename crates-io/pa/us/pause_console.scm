(define-module (crates-io pa us pause_console) #:use-module (crates-io))

(define-public crate-pause_console-0.1.0 (c (n "pause_console") (v "0.1.0") (h "1r8bsr5dvwgjqcd17baqy5gjgg61778wsvz1fy6zcqp3l7bhpfki")))

(define-public crate-pause_console-0.1.1 (c (n "pause_console") (v "0.1.1") (h "1q6ljq016jgqpfy2vgryq28g9hkm7n57jyq7f006ycf7f5176ds0")))

(define-public crate-pause_console-0.1.2 (c (n "pause_console") (v "0.1.2") (h "104m9wpwshvzs13s4gcd2sa45n47k8z39357j8k5wd6idd0a6zpq")))

(define-public crate-pause_console-0.1.3 (c (n "pause_console") (v "0.1.3") (h "1wqpn8vvdp79kpmwaklqs0xq0glf67h6m65kva6wn9m21y11ma8j")))

(define-public crate-pause_console-0.2.0 (c (n "pause_console") (v "0.2.0") (h "0rfrpbpk49h3wvyvkncd1qhjx9slalr1hp32yd6mr0wnnvdk7ad9")))

