(define-module (crates-io pa us pausable_clock) #:use-module (crates-io))

(define-public crate-pausable_clock-0.1.0 (c (n "pausable_clock") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0j5zplili7dda1bdzkx6xr9gn812c76qh7r7p44ngvlx7d3yl16q")))

(define-public crate-pausable_clock-0.2.0 (c (n "pausable_clock") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1a3wj2fp6b0n38c8gcz3bfbxlkwy5ar4csbv6kp6yq7h78f13baa")))

(define-public crate-pausable_clock-0.3.0 (c (n "pausable_clock") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1zjha8nypx950nl9m3h8fgr2c8r6ccbiqiqimyqvi0bd823gqn2y")))

(define-public crate-pausable_clock-0.3.1 (c (n "pausable_clock") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)))) (h "15nd0543is1v9bsqlmvnf4hamf0lmqrf2ni1hki6sza87nm9spsg")))

(define-public crate-pausable_clock-0.3.2 (c (n "pausable_clock") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)))) (h "12xarj7lv9zclrbnbnd7hif38rkagfd1ppvyacb24n79lvm2rry0")))

(define-public crate-pausable_clock-1.0.0 (c (n "pausable_clock") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)))) (h "162is1bwaqz7n3vab9ijr34xy87xdnrjaskh8xrd56hvlzfqxvv3")))

(define-public crate-pausable_clock-1.0.1 (c (n "pausable_clock") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)))) (h "0id71p05ld92gv6l0flaarx8dpkgk52b58bnk5s180yx9r60ggsk")))

