(define-module (crates-io pa us pausable_future) #:use-module (crates-io))

(define-public crate-pausable_future-0.1.0 (c (n "pausable_future") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "~0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "18drjzr4nj75gq8rgqiafr3rc4cgf84m8dsm5p4zgfvsdjhc0i99")))

