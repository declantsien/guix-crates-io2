(define-module (crates-io pa to patoz) #:use-module (crates-io))

(define-public crate-patoz-0.1.0 (c (n "patoz") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jigyy2r1c1mby186m4bdq4ms969350bkk7dijjzgw4c9irbdhbn")))

