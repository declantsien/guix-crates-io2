(define-module (crates-io pa m_ pam_groupmap) #:use-module (crates-io))

(define-public crate-pam_groupmap-0.1.0 (c (n "pam_groupmap") (v "0.1.0") (d (list (d (n "ldap3") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "1wfcnqdgl85i3h9x44bkj8d5kfx6fwh4cn8cs5dq4br4hk63v6pl")))

(define-public crate-pam_groupmap-0.1.1 (c (n "pam_groupmap") (v "0.1.1") (d (list (d (n "ldap3") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "15ifcv90ibirnriflq56awdz74khc1vq7qa33gf6hiss20hssa7w")))

(define-public crate-pam_groupmap-0.1.3 (c (n "pam_groupmap") (v "0.1.3") (d (list (d (n "ldap3") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0m51djl9fga9m2nm2nm643qirmadxdxl0pl3qanl4hshxz81jksa")))

