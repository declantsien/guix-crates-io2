(define-module (crates-io pa pm papm) #:use-module (crates-io))

(define-public crate-papm-0.1.0 (c (n "papm") (v "0.1.0") (h "1gicqcl1mjs3rgry3ikr5kb6bbcwv6bma6jcgx8clc82ibzkw6ch") (r "1.77.2")))

(define-public crate-papm-0.2.0 (c (n "papm") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "12fsp82xqw711s9lycqvdh0nlfwxxzz1rslmq37imcylmsqw7jjs") (r "1.77.2")))

(define-public crate-papm-0.3.1 (c (n "papm") (v "0.3.1") (d (list (d (n "argon2") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11mi75j64mmy5gwsxk19bzficcs4c217ck68j24g8rrbx65ys965") (r "1.77.2")))

(define-public crate-papm-0.3.2 (c (n "papm") (v "0.3.2") (d (list (d (n "argon2") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c20j07r9cir0zrsd6qy0qfdr3kspbkgqxlhhj2a20wk4albrjp2") (r "1.77.2")))

