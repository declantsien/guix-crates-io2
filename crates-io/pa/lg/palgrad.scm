(define-module (crates-io pa lg palgrad) #:use-module (crates-io))

(define-public crate-palgrad-0.0.1 (c (n "palgrad") (v "0.0.1") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg" "png"))) (k 0)) (d (n "palette") (r "^0.5") (f (quote ("std"))) (k 0)))) (h "0dmdpqk46z4imykmh6i14nvi2ar4bzy8j02xra2jxz6a60x7a6q6")))

