(define-module (crates-io pa ig paige) #:use-module (crates-io))

(define-public crate-paige-0.1.0 (c (n "paige") (v "0.1.0") (h "08j9g1s29biy5pr9chvp0rhyywj284rsfwla5z6lllgg15azgb07")))

(define-public crate-paige-0.1.1 (c (n "paige") (v "0.1.1") (h "0s2lyjg3qbivs3w2zw760kfn1l5w6nw8pxbksgv4yxkmb8fnq352")))

(define-public crate-paige-0.1.2 (c (n "paige") (v "0.1.2") (h "1qm50xb5ngybf7dm9i2rvcijx1128iryykwc7mr2xnb1wc4w39rr")))

(define-public crate-paige-1.0.0 (c (n "paige") (v "1.0.0") (h "1n506pwr6z32mmc7giwhhcl2kr4sl567ncqfqz2my56f2nnsfnc9")))

