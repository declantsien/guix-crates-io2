(define-module (crates-io pa rk parkour_derive) #:use-module (crates-io))

(define-public crate-parkour_derive-0.1.0 (c (n "parkour_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0fgiaja80rbcb4xabcjh73xyml7kw2w5liqrsn7dzjrllgcai775")))

