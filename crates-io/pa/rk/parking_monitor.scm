(define-module (crates-io pa rk parking_monitor) #:use-module (crates-io))

(define-public crate-parking_monitor-0.1.0 (c (n "parking_monitor") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "09z6qbz3bdmk83mdl6hbnahlanyw0vj81pbpp1pnzkc8fy4msq3b")))

(define-public crate-parking_monitor-0.1.1 (c (n "parking_monitor") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "01hkr85rw4lk9dmbvi6iir3gri8z6hyhprcyk206yp4vg7ba4vx4")))

