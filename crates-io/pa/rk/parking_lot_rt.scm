(define-module (crates-io pa rk parking_lot_rt) #:use-module (crates-io))

(define-public crate-parking_lot_rt-0.12.1 (c (n "parking_lot_rt") (v "0.12.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.6") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0l4svhrb27pz320q0hdv4z4z2gaprcir8gkgmjsz873mlzdx7yfp") (f (quote (("serde" "lock_api/serde") ("send_guard") ("owning_ref" "lock_api/owning_ref") ("nightly" "parking_lot_core/nightly" "lock_api/nightly") ("hardware-lock-elision") ("default") ("deadlock_detection" "parking_lot_core/deadlock_detection") ("arc_lock" "lock_api/arc_lock")))) (r "1.56")))

