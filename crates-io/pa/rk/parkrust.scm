(define-module (crates-io pa rk parkrust) #:use-module (crates-io))

(define-public crate-parkrust-0.1.0 (c (n "parkrust") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parkrust_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)))) (h "07a7kzwi4qiagcd2qfybqq3x4dj2hn7y87clqidlr401x5bayi69")))

