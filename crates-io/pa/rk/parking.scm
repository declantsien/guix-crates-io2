(define-module (crates-io pa rk parking) #:use-module (crates-io))

(define-public crate-parking-1.0.0 (c (n "parking") (v "1.0.0") (h "06yzd41vc0d8wklpp8a1n4ligra177898j3hmwxz3fas0yxk3ch1")))

(define-public crate-parking-1.0.1 (c (n "parking") (v "1.0.1") (h "1m1dkjcsv4w12vg2baiimjqf3bc8splv623mi9i1g5pq5lvaszws")))

(define-public crate-parking-1.0.2 (c (n "parking") (v "1.0.2") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "12l48cxnwxyi9acbvnkgxn7y5sj00gnydn7m08ylm3jgws7abjhv")))

(define-public crate-parking-1.0.3 (c (n "parking") (v "1.0.3") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "01hsbaq46lckw8fa8sm0nahbhwzgznar02zk88pdjqjaa31rn0n4")))

(define-public crate-parking-1.0.4 (c (n "parking") (v "1.0.4") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "0l73gcxyrglk5l013wmqxvjskfka3h9mylj0w8941f93dlyfxz0y")))

(define-public crate-parking-1.0.5 (c (n "parking") (v "1.0.5") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "01zrwwa4q57h6w03yqzcw116i6j3zgl8zqrgacr4l57q67dadm2h")))

(define-public crate-parking-1.0.6 (c (n "parking") (v "1.0.6") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "0z6q9rxm98vrp3fimw8b5syzwgf8l0pnn6y0cqm4lbblf7r01cvc")))

(define-public crate-parking-2.0.0 (c (n "parking") (v "2.0.0") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)))) (h "0wnxxnizfxlax3n709s5r83f4n8awy3m4a18q4fdk0z7z693hz22")))

(define-public crate-parking-2.1.0 (c (n "parking") (v "2.1.0") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(loom)") (k 0)))) (h "0kirm3yimp8rwxs1yh5c86ikmrkqjmj2j822nndysh2ahcn2bwhl") (r "1.39")))

(define-public crate-parking-2.1.1 (c (n "parking") (v "2.1.1") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (t "cfg(loom)") (k 0)))) (h "0rr0kddcjc7cir02bj2j8rhmm9wivmrkzr2j3hfrqd9r9i57fb75") (r "1.39")))

(define-public crate-parking-2.2.0 (c (n "parking") (v "2.2.0") (d (list (d (n "easy-parallel") (r "^3.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (t "cfg(loom)") (k 0)))) (h "1blwbkq6im1hfxp5wlbr475mw98rsyc0bbr2d5n16m38z253p0dv") (r "1.51")))

