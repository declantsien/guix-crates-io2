(define-module (crates-io pa rk parking_method) #:use-module (crates-io))

(define-public crate-parking_method-0.1.0 (c (n "parking_method") (v "0.1.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0fd4wrflijyvwsmqxzm4fys0swnsdqhifp3y7x7sfnlm84wjwndp")))

(define-public crate-parking_method-0.2.0 (c (n "parking_method") (v "0.2.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0kxyi5qirh811541s0rbfbhwfnqnb5rxwqa28jzy1f7jxb59gqcv")))

(define-public crate-parking_method-0.3.0 (c (n "parking_method") (v "0.3.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0d2a9mq5h8ij9d6l21qb1xhxpsxs0axxx4lycxrm047vrm24ns82")))

(define-public crate-parking_method-0.4.0 (c (n "parking_method") (v "0.4.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0xhj4kxjxmh42sffiyl4dpfxrjw3fxqpjrv7wr28s070lm7l29d0")))

