(define-module (crates-io pa pa papago) #:use-module (crates-io))

(define-public crate-papago-0.1.0 (c (n "papago") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.4") (d #t) (k 0)))) (h "0pfacwwba54bd0r25rlfs7d49j8gh9w4jry90aa8lghflzlbjjlf")))

(define-public crate-papago-0.1.1 (c (n "papago") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.4") (d #t) (k 0)))) (h "162y3gyivs5ccyppwn2w6zk2j90y0fiyvq0vxiispdrx9hh104k1")))

