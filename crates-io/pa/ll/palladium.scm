(define-module (crates-io pa ll palladium) #:use-module (crates-io))

(define-public crate-palladium-0.0.1 (c (n "palladium") (v "0.0.1") (h "16nbclhmzrqxv13iwsri3gsnvz3vwjdf6jdxgm631xqy9583kr6p")))

(define-public crate-palladium-0.0.2 (c (n "palladium") (v "0.0.2") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1kmb2agdm0l6lzhplg6w2ql9ns1ir9g9ls4p8drc8ka08b9prs8s")))

(define-public crate-palladium-0.0.25 (c (n "palladium") (v "0.0.25") (d (list (d (n "iridium") (r "^0.0.24") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0r6zdd37blcxi3arsammc7d2gfczymb3hx21769xssdvinj4p6jg")))

(define-public crate-palladium-0.0.26 (c (n "palladium") (v "0.0.26") (d (list (d (n "iridium") (r "^0.0.33") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "106bqghhg0i4j37grc7cxxxydphgfkjg8dmps3n1pm8m85zf1c2w")))

