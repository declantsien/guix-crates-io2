(define-module (crates-io pa ll pallet-evm-precompile-simple) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-simple-1.0.0 (c (n "pallet-evm-precompile-simple") (v "1.0.0") (d (list (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "ripemd160") (r "^0.9") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "08v2zz7byadjz26c5fspvlbp9ykbh932n9wx56dyrhvr9r87b7qj") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std" "ripemd160/std") ("default" "std"))))))

