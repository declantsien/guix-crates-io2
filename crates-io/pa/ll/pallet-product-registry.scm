(define-module (crates-io pa ll pallet-product-registry) #:use-module (crates-io))

(define-public crate-pallet-product-registry-2.0.0 (c (n "pallet-product-registry") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 2)) (d (n "sp-io") (r "^2.0.0") (k 2)) (d (n "sp-runtime") (r "^2.0.0") (k 2)) (d (n "timestamp") (r "^2.0.0") (k 0) (p "pallet-timestamp")))) (h "02lyad8xkgq9irfrdxxhgfxfm67lprxnlga6753kgb3qh7vqzs22") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "timestamp/std") ("default" "std"))))))

