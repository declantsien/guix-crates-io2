(define-module (crates-io pa ll pallas-codec) #:use-module (crates-io))

(define-public crate-pallas-codec-0.7.0-alpha.0 (c (n "pallas-codec") (v "0.7.0-alpha.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0va80b32c11avjryyv0xmg6ylv4bcnpsxi0i3rdxk0vy0b4va476")))

(define-public crate-pallas-codec-0.7.0-alpha.1 (c (n "pallas-codec") (v "0.7.0-alpha.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "07zmqv5xi0fi2chfy2v31rf85r1q3l58jmqs59f1dsqvg9m5g5zb")))

(define-public crate-pallas-codec-0.7.0 (c (n "pallas-codec") (v "0.7.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1gxp7njbcl6f8a57hy3lgs0z5h470v1b00rb7nd7w78sq5xyl16n")))

(define-public crate-pallas-codec-0.7.1 (c (n "pallas-codec") (v "0.7.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0k1rwl3i5z0a1xi1wi7v7hx864lr6x3xnpmlrlxjdqwgcpa1cy6f")))

(define-public crate-pallas-codec-0.8.0-alpha.0 (c (n "pallas-codec") (v "0.8.0-alpha.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1766530l2sc15yv5xffd1nykrdm2wfsaydssgslal9xplclwxanc")))

(define-public crate-pallas-codec-0.8.0-alpha.1 (c (n "pallas-codec") (v "0.8.0-alpha.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1rbpk9wy9hdi9z02h65i0qald4n3bdj23m1m2jnrn594fld4v0jh")))

(define-public crate-pallas-codec-0.8.0 (c (n "pallas-codec") (v "0.8.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0mwbgxpylkv8fxj2hvajsapi85aisgznslr3y4q1pw45n9rvswk6")))

(define-public crate-pallas-codec-0.9.0-alpha.0 (c (n "pallas-codec") (v "0.9.0-alpha.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1qx3n4a2igbkbdaimbl6f5i61hpvz024mw1rhr5r8ziy0dzifx6c")))

(define-public crate-pallas-codec-0.9.0-alpha.1 (c (n "pallas-codec") (v "0.9.0-alpha.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1s6ypy5k03cv9zj5rsshc738zqq3vz77cz68dqmnk1mjzab8srfs")))

(define-public crate-pallas-codec-0.9.0 (c (n "pallas-codec") (v "0.9.0") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "09vlavspszxf7bb409a88r0cs0q280wpbd7qfm0wc4mrdww4a249")))

(define-public crate-pallas-codec-0.9.1 (c (n "pallas-codec") (v "0.9.1") (d (list (d (n "minicbor") (r "^0.15") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1brv9yj63gxlmxn0p48m4ggnf8wjxg5h82yc77k41ibrd79p4sss")))

(define-public crate-pallas-codec-0.10.0 (c (n "pallas-codec") (v "0.10.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0q4n7b9lggnam6dc2kn956f1d3y4vw6ijvp4r90aliv8f9gfa0v5")))

(define-public crate-pallas-codec-0.11.0-alpha.0 (c (n "pallas-codec") (v "0.11.0-alpha.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1ygn6m03iv0gi2hwbylvjp2pxlryl8av1sjqw5d9s35bjgg86647")))

(define-public crate-pallas-codec-0.11.0-alpha.1 (c (n "pallas-codec") (v "0.11.0-alpha.1") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0jr77qd7wq9b8viyvf147jk6a83cb6ajsg2ff1zwcnnsh10m0rm9")))

(define-public crate-pallas-codec-0.11.0-alpha.2 (c (n "pallas-codec") (v "0.11.0-alpha.2") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1sb48szqcyynpz6qr07f37p21qzsn0dnhz22dpfnk9gp7zvcvdsy")))

(define-public crate-pallas-codec-0.11.0-beta.0 (c (n "pallas-codec") (v "0.11.0-beta.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "10cyswwjc1cg5p5d4mxd5rk05x9i9zsbhnsk6af587x88blrfn0s")))

(define-public crate-pallas-codec-0.11.0 (c (n "pallas-codec") (v "0.11.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1hqwyf17vmyh6xk3pxhzqsphm3py6jc6jylbpnr7zcsavh9ad28s")))

(define-public crate-pallas-codec-0.12.0-alpha.0 (c (n "pallas-codec") (v "0.12.0-alpha.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1cwf9cq5y0lkyqqkdwlx8aww4swb5q9gzkr93mwpqm38l9f42zg8")))

(define-public crate-pallas-codec-0.12.0 (c (n "pallas-codec") (v "0.12.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0pca77scdqasmkirr1r6k2qdr601fhvsdd1b7vja1ha1ffhhxkhd")))

(define-public crate-pallas-codec-0.13.0 (c (n "pallas-codec") (v "0.13.0") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0zfb4p68dsrbaipsjzwh902r3jy7c1r5qq8g9dmdwskbqdfmz6pq")))

(define-public crate-pallas-codec-0.13.1 (c (n "pallas-codec") (v "0.13.1") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "0pwbkjm6fvcx2p9zkiil1x8zxbj36axyxir5vb6386gzi79d1cl6")))

(define-public crate-pallas-codec-0.13.2 (c (n "pallas-codec") (v "0.13.2") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1kxlby33f4fip0ybdxxnxv619p85vni4wj6g0d2a4ci3kf5scn0p")))

(define-public crate-pallas-codec-0.14.0-alpha.0 (c (n "pallas-codec") (v "0.14.0-alpha.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fl8aggahqcrbv4xx4737si81b5g24jwbiqd6cs7b9ifz7ccmhi3")))

(define-public crate-pallas-codec-0.14.0-alpha.1 (c (n "pallas-codec") (v "0.14.0-alpha.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0777mndw404xacrji9yg8mfci264s7dgbcjbsxyabfdpq5cjkcn9")))

(define-public crate-pallas-codec-0.14.0-alpha.2 (c (n "pallas-codec") (v "0.14.0-alpha.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sjjxiv7inqm1x04ig086m983jx945j3ydr4wvr0xf00af4zii4a")))

(define-public crate-pallas-codec-0.14.0-alpha.3 (c (n "pallas-codec") (v "0.14.0-alpha.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "15xcisd2mdjawd1v23hnkdq4qiwawkz6qz9b7sgnm01vjnqy57bl")))

(define-public crate-pallas-codec-0.14.0-alpha.4 (c (n "pallas-codec") (v "0.14.0-alpha.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "019xw6yj4xvq4v4bk6v9i9nj3akf0gk9hz1vwmcfi15afakkbh35")))

(define-public crate-pallas-codec-0.14.0-alpha.5 (c (n "pallas-codec") (v "0.14.0-alpha.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b0pm97djpn09qimnqj9nld3pi9hx4cq8lp2pcpy4vhb5kpjjyk9")))

(define-public crate-pallas-codec-0.13.3 (c (n "pallas-codec") (v "0.13.3") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1rqgpihblmmgsrsvfxq4y779057wqk6kqrvrkrz10sbkm0x00lnr")))

(define-public crate-pallas-codec-0.14.0-alpha.6 (c (n "pallas-codec") (v "0.14.0-alpha.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xb039lz40byqmxwp8xj6wm61s45kqp48nclkla9023dbhjrn33h")))

(define-public crate-pallas-codec-0.14.0 (c (n "pallas-codec") (v "0.14.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ywf7ghfq3l9nr2jsshg671284psc09ylizplk3pdmsvishnmij")))

(define-public crate-pallas-codec-0.14.1 (c (n "pallas-codec") (v "0.14.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "18j4grpp4s5gwqln16jmlxs6w09lndik7fmjig4mpxijki0nzw5m") (y #t)))

(define-public crate-pallas-codec-0.15.0 (c (n "pallas-codec") (v "0.15.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l77p12p64l460brxp3zwnr9k6a409y82k5ypbakz2vblbbvqhn4")))

(define-public crate-pallas-codec-0.13.4 (c (n "pallas-codec") (v "0.13.4") (d (list (d (n "minicbor") (r "^0.17") (f (quote ("std" "half" "derive"))) (d #t) (k 0)))) (h "1qvd2y4lx3siz9r51zl52aqh91a70wqh0y0dc5a3ba8ladmqbl53")))

(define-public crate-pallas-codec-0.14.2 (c (n "pallas-codec") (v "0.14.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "173n1qk9qipxnygvyhjhmpw12rnljxjwgnzx2lvdfkzcw1jqb0k8")))

(define-public crate-pallas-codec-0.16.0 (c (n "pallas-codec") (v "0.16.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dy90jwsxhrplz3rn9din43q0gwhbcg5bgm5aix0732xh8v2xnrn")))

(define-public crate-pallas-codec-0.17.0 (c (n "pallas-codec") (v "0.17.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qqj1dkn14wv8riyl15ac4k8zphla3rhp3zr6ws0f33qi9pwy881")))

(define-public crate-pallas-codec-0.18.0 (c (n "pallas-codec") (v "0.18.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ys9lr1n897rvdci7icr4dr1a5wrqph407hjqzla1bx1jb8kr2l4")))

(define-public crate-pallas-codec-0.19.0-alpha.0 (c (n "pallas-codec") (v "0.19.0-alpha.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dmrpn51649jagfdigvw719imshdz462ik5zi9ddmsw07a9zaip7")))

(define-public crate-pallas-codec-0.18.1 (c (n "pallas-codec") (v "0.18.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wyfkzad88kkgbg57brp8ciliils8jcjbvjm17sajnx19zpc353i")))

(define-public crate-pallas-codec-0.19.0-alpha.1 (c (n "pallas-codec") (v "0.19.0-alpha.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nbkvw7j68hcv64knb2xmjqv2qzqhl59amw2isbv86gha39kqjp7")))

(define-public crate-pallas-codec-0.19.0-alpha.2 (c (n "pallas-codec") (v "0.19.0-alpha.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dyrdv9fh4s8h48q2whwacykcn6iis09bxvmfpzvicb2nddg67z6")))

(define-public crate-pallas-codec-0.18.2 (c (n "pallas-codec") (v "0.18.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g0hys5mb743kqd6x16cyjxhjksmybab3j3qdm9679j2bp806vlb")))

(define-public crate-pallas-codec-0.19.0 (c (n "pallas-codec") (v "0.19.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1avimkn46gy57lrpkzhsvapkhamqr8grbi5lz2lp5g1s1abgi1rw")))

(define-public crate-pallas-codec-0.19.1 (c (n "pallas-codec") (v "0.19.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yn2s21pf2sxksbdifk5fmk83zb45jd0q8k2yh52f51s7nxf9w4s")))

(define-public crate-pallas-codec-0.20.0 (c (n "pallas-codec") (v "0.20.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1bni8y1dmz58qca2kj1v3n8s2d544505rlx8h9989a2rim7cqhsl")))

(define-public crate-pallas-codec-0.21.0 (c (n "pallas-codec") (v "0.21.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "10hnr7wpjjz5z2nak6wlxk99fkdxvbl8yb9f0f0a6hmrnsqbzwda")))

(define-public crate-pallas-codec-0.22.0 (c (n "pallas-codec") (v "0.22.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0j5jhw1jgzq2kynk4ss2nkrwzcm7ydvzv36npx36n387b2jx2c25") (f (quote (("default"))))))

(define-public crate-pallas-codec-0.23.0 (c (n "pallas-codec") (v "0.23.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0z9x7sczmxfhw40yj17hbkg152h336dwj9kwmdmrl3q02wgx9y5r") (f (quote (("default"))))))

(define-public crate-pallas-codec-0.24.0 (c (n "pallas-codec") (v "0.24.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0ymdrj87zs7fm1zgxr1vzs3cldvvidiq6gj2375rbgqjz6g6bxpj") (f (quote (("default"))))))

(define-public crate-pallas-codec-0.25.0 (c (n "pallas-codec") (v "0.25.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "18a6qgqasxvj93461ff1jyrnz94xlnp1mjpgdmc3qnv83jkh2wrm") (f (quote (("default"))))))

(define-public crate-pallas-codec-0.26.0 (c (n "pallas-codec") (v "0.26.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "minicbor") (r "^0.20") (f (quote ("std" "half" "derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1qs0f6n2jxr9xwdis2x45fym9kjb2yn237p0dazxmhn1qfry1wxg") (f (quote (("default"))))))

