(define-module (crates-io pa ll palletizer-tools) #:use-module (crates-io))

(define-public crate-palletizer-tools-0.1.0 (c (n "palletizer-tools") (v "0.1.0") (d (list (d (n "palletizer") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1jsjlcy344zfhr7c2k0qyaz4n6qsd2dl57jrfwmndbfs7nzc8wkh") (y #t)))

(define-public crate-palletizer-tools-0.1.1 (c (n "palletizer-tools") (v "0.1.1") (d (list (d (n "palletizer") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0l5ica0z0n7bb6l4vhr1if04n8q7fsy1sbfb84g7g3i6bxryskp1") (y #t)))

(define-public crate-palletizer-tools-0.1.2 (c (n "palletizer-tools") (v "0.1.2") (d (list (d (n "palletizer") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "145r7805hhbz0w7rmr5viyl8lp1c5q235xrpskpfic551pvq0vl3") (y #t)))

(define-public crate-palletizer-tools-0.1.3 (c (n "palletizer-tools") (v "0.1.3") (d (list (d (n "palletizer") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0xsv218y5yviiyi1lzrvasl5qn0vp4qqy2vvfykw23dh5ikmpbxm")))

(define-public crate-palletizer-tools-0.2.0 (c (n "palletizer-tools") (v "0.2.0") (d (list (d (n "palletizer") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1g594g5pp30chqh2b2db3dplnd6h3x2bqj5wdjn0xaz0apwkdn0a")))

(define-public crate-palletizer-tools-0.2.1 (c (n "palletizer-tools") (v "0.2.1") (d (list (d (n "palletizer") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "06dqxlr2shr1i2bncz0k3gm2i6hrd19dfqn326y0gcjr4qqnc445")))

(define-public crate-palletizer-tools-0.2.2 (c (n "palletizer-tools") (v "0.2.2") (d (list (d (n "palletizer") (r "^0.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "01rs6kll8pqc7rhrabvxkkbipk75m1sy7d5bb62zvafljqwnhsxj")))

(define-public crate-palletizer-tools-0.2.3 (c (n "palletizer-tools") (v "0.2.3") (d (list (d (n "palletizer") (r "^0.2.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1akvbzhqsl7pplnks84lg1fxz3kszr8qlbacw4hb9crbwacjdpl5") (y #t)))

(define-public crate-palletizer-tools-0.2.4 (c (n "palletizer-tools") (v "0.2.4") (d (list (d (n "palletizer") (r "^0.2.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "027igfdqn4km2xw4420vfnw1xayx9xr3x6s5yz6pxdrpss94cglp")))

