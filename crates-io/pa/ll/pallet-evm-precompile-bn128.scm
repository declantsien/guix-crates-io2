(define-module (crates-io pa ll pallet-evm-precompile-bn128) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-bn128-1.0.0 (c (n "pallet-evm-precompile-bn128") (v "1.0.0") (d (list (d (n "bn") (r "^0.5") (k 0) (p "substrate-bn")) (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "1v5phnypzygznbagl3b676mk1k37ci19m7mp0n0qcrf8k6x0i0rc") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std") ("default" "std"))))))

