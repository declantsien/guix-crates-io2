(define-module (crates-io pa ll pallet-delegated-staking) #:use-module (crates-io))

(define-public crate-pallet-delegated-staking-0.0.0 (c (n "pallet-delegated-staking") (v "0.0.0") (h "1hf509nma1fnxflg2ln353g961vd13d6d3jn2j4yddwi124kj0n6")))

(define-public crate-pallet-delegated-staking-1.0.0 (c (n "pallet-delegated-staking") (v "1.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^34.0.0") (k 0)) (d (n "frame-system") (r "^34.0.0") (k 0)) (d (n "scale-info") (r "^2.10.0") (f (quote ("derive"))) (k 0)) (d (n "sp-runtime") (r "^37.0.0") (k 0)) (d (n "sp-staking") (r "^32.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "1nck8xgwrcw7l3x5vy0bjmpzks8pmq3chjgnpjxrpbgwxq2z1yx5") (f (quote (("try-runtime" "frame-support/try-runtime" "frame-system/try-runtime" "sp-runtime/try-runtime") ("std" "codec/std" "frame-support/std" "frame-system/std" "scale-info/std" "sp-runtime/std" "sp-staking/std" "sp-std/std") ("runtime-benchmarks" "frame-support/runtime-benchmarks" "frame-system/runtime-benchmarks" "sp-runtime/runtime-benchmarks" "sp-staking/runtime-benchmarks") ("default" "std"))))))

