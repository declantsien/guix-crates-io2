(define-module (crates-io pa ll pallet-mmr-primitives) #:use-module (crates-io))

(define-public crate-pallet-mmr-primitives-3.0.0 (c (n "pallet-mmr-primitives") (v "3.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^3.0.0") (k 0)) (d (n "frame-system") (r "^3.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sp-api") (r "^3.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-runtime") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "1dyfpwpnhck6vpibi81s9x3bnpphc85q1s5l026lh64gqjk00ss6") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "serde" "sp-api/std" "sp-core/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

