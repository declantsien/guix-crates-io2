(define-module (crates-io pa ll pallet-evm-precompile-ed25519) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-ed25519-1.0.0 (c (n "pallet-evm-precompile-ed25519") (v "1.0.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0") (f (quote ("alloc" "u64_backend"))) (k 0)) (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "0dd46l2psmhsz6dd0nbp4c73a0bgdz2ps1klkiri6dlp6wyrdfln") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std" "ed25519-dalek/std") ("default" "std"))))))

