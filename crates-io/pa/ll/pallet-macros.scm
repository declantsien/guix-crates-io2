(define-module (crates-io pa ll pallet-macros) #:use-module (crates-io))

(define-public crate-pallet-macros-0.3.0 (c (n "pallet-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wwap6v7ily7cgwln00y3wnsvywp8p8l3hjpiq514va5i5afs7dr")))

(define-public crate-pallet-macros-0.3.2 (c (n "pallet-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d2nxyhv6qjwwqf0jv4qnky8wj2jj43ww3z1ar2640yc90gczaxw")))

(define-public crate-pallet-macros-0.4.0 (c (n "pallet-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01wvcg613j0dq6hxfbg8iw79030fp0hvvsy3msxhy8ww0w8hj2g7")))

