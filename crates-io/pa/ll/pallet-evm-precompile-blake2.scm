(define-module (crates-io pa ll pallet-evm-precompile-blake2) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-blake2-1.0.0 (c (n "pallet-evm-precompile-blake2") (v "1.0.0") (d (list (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "069lnkxd0g29p377sa3xrp96589nsiyplg904llhymw5pqfld7k6") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std") ("default" "std"))))))

