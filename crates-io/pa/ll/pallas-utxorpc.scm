(define-module (crates-io pa ll pallas-utxorpc) #:use-module (crates-io))

(define-public crate-pallas-utxorpc-0.19.0-alpha.1 (c (n "pallas-utxorpc") (v "0.19.0-alpha.1") (d (list (d (n "pallas-codec") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0v0dnzai8a10n14h993cc0n1vl497v1h9zy4wi20iyshzhp65f4j")))

(define-public crate-pallas-utxorpc-0.19.0-alpha.2 (c (n "pallas-utxorpc") (v "0.19.0-alpha.2") (d (list (d (n "pallas-codec") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "^0.19.0-alpha.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0jqfr43hkhpf5453f7r0wzix34vcp5v2h3dqw2ng7vlrwbh3fyh0")))

(define-public crate-pallas-utxorpc-0.19.0 (c (n "pallas-utxorpc") (v "0.19.0") (d (list (d (n "pallas-codec") (r "=0.19.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.19.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.19.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "088vg755gbfrhjbvkc9w933gslndycj6j39fgx7awm2acz2n8zmn")))

(define-public crate-pallas-utxorpc-0.19.1 (c (n "pallas-utxorpc") (v "0.19.1") (d (list (d (n "pallas-codec") (r "=0.19.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.19.1") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.19.1") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "11w24kqpkg69j0s8y3d1dcyna8lssrhcvl9dx9ijkw1lj66kmhgm")))

(define-public crate-pallas-utxorpc-0.20.0 (c (n "pallas-utxorpc") (v "0.20.0") (d (list (d (n "pallas-codec") (r "=0.20.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.20.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.20.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0cn07khl8cm134r5z9yr0ffi35ajqpknp8pqr1f2pga5rlp226q6")))

(define-public crate-pallas-utxorpc-0.21.0 (c (n "pallas-utxorpc") (v "0.21.0") (d (list (d (n "pallas-codec") (r "=0.21.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.21.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.21.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0rqrnp9gmiawzlf99ryxdscww6gd554fxqwib9w45hb2458zm1f5")))

(define-public crate-pallas-utxorpc-0.22.0 (c (n "pallas-utxorpc") (v "0.22.0") (d (list (d (n "pallas-codec") (r "=0.22.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.22.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.22.0") (d #t) (k 0)) (d (n "utxorpc") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0y171lc698lyydi1kjl1f1mm475nflp3f6lsx2a9in73lnnzhiwm")))

(define-public crate-pallas-utxorpc-0.23.0 (c (n "pallas-utxorpc") (v "0.23.0") (d (list (d (n "pallas-codec") (r "=0.23.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.23.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.23.0") (d #t) (k 0)) (d (n "utxorpc-spec") (r "^0.3.0") (d #t) (k 0)))) (h "1qm00rv6iribiy7zqkgkqhhlzrv678j2q8g57w0nl3d8hn1f933c")))

(define-public crate-pallas-utxorpc-0.24.0 (c (n "pallas-utxorpc") (v "0.24.0") (d (list (d (n "pallas-codec") (r "=0.24.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.24.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.24.0") (d #t) (k 0)) (d (n "utxorpc-spec") (r "^0.3.0") (d #t) (k 0)))) (h "04p9hgzjgpc6nf9dczmbxhq7rndbmzn6gsrwzbpa9wjwvbqyxcqv")))

(define-public crate-pallas-utxorpc-0.25.0 (c (n "pallas-utxorpc") (v "0.25.0") (d (list (d (n "pallas-codec") (r "=0.25.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.25.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.25.0") (d #t) (k 0)) (d (n "utxorpc-spec") (r "^0.4.4") (d #t) (k 0)))) (h "17pvip6bvba5lxq3v34gav45q9kmrm03k6baplwy7aca0ppb0yag")))

(define-public crate-pallas-utxorpc-0.26.0 (c (n "pallas-utxorpc") (v "0.26.0") (d (list (d (n "pallas-codec") (r "=0.26.0") (d #t) (k 0)) (d (n "pallas-crypto") (r "=0.26.0") (d #t) (k 0)) (d (n "pallas-primitives") (r "=0.26.0") (d #t) (k 0)) (d (n "pallas-traverse") (r "=0.26.0") (d #t) (k 0)) (d (n "utxorpc-spec") (r "^0.5.0") (d #t) (k 0)))) (h "1a05bq3df99q4j6b8raz4aa2marsg71cy08a9qlwpm5ydgx4pk5b")))

