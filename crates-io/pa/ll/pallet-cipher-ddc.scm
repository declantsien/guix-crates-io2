(define-module (crates-io pa ll pallet-cipher-ddc) #:use-module (crates-io))

(define-public crate-pallet-cipher-ddc-3.0.0 (c (n "pallet-cipher-ddc") (v "3.0.0") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (d #t) (k 2)) (d (n "sp-io") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "0vb13swnih1i8x6ma5xj040258q04nvqkp853p908aikg4pisfzx") (f (quote (("std" "codec/std" "sp-io/std" "sp-std/std" "sp-runtime/std" "frame-support/std" "frame-system/std") ("default" "std")))) (y #t)))

