(define-module (crates-io pa ll pallet-staking-reward-fn) #:use-module (crates-io))

(define-public crate-pallet-staking-reward-fn-0.0.0 (c (n "pallet-staking-reward-fn") (v "0.0.0") (h "1n6fvj9kpad3y79k77dx8i3gblzfrpin9jpj676d2a2liv2gy5j6")))

(define-public crate-pallet-staking-reward-fn-4.0.0 (c (n "pallet-staking-reward-fn") (v "4.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^7.0.0") (k 0)))) (h "1ng92kxfdq099yjih79mcgipirh62hgwlh59s07nn574068k35cl") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-5.0.0 (c (n "pallet-staking-reward-fn") (v "5.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^8.0.0") (k 0)))) (h "0m2n1wc7r2jpqcc1q4x6y6769zlr791q8z8z8vba987wlwfhlwz7") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-6.0.0 (c (n "pallet-staking-reward-fn") (v "6.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^9.0.0") (k 0)))) (h "1p954w87rhmwjvxy9vwc81ww0yv637xfrgvhp321y51pzj1kjm51") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-7.0.0 (c (n "pallet-staking-reward-fn") (v "7.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^10.0.0") (k 0)))) (h "169y4wbmrvsvxxcx3wxzh82kc7inxf2hhiss141hszrvqpvf33dm") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-8.0.0 (c (n "pallet-staking-reward-fn") (v "8.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^11.0.0") (k 0)))) (h "11z36z28pxkjbnvl52h8qn16sw6h1d71hsl5xjvfhk0g172smnmb") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-9.0.0 (c (n "pallet-staking-reward-fn") (v "9.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^12.0.0") (k 0)))) (h "0j6pa2xn28av60vpj4h4b8pq18fqwpg9n19w2rzcffmqh97gs4dj") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-10.0.0 (c (n "pallet-staking-reward-fn") (v "10.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^13.0.0") (k 0)))) (h "1j5h6yl0n3przb72scvfqsz46qv7zf9xpj8z9b6j4qmsqqg1czyq") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-11.0.0 (c (n "pallet-staking-reward-fn") (v "11.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^14.0.0") (k 0)))) (h "1w8k1i4i2cmglxaz7x3313axrzw7ighg8l2dpxzclb8pnvlh7y6h") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.0.0 (c (n "pallet-staking-reward-fn") (v "12.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^15.0.0") (k 0)))) (h "0i4zmsmnib4pj10ksb86gmz2ivw5yd6y9nc643x9f31y6ywgm507") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev1 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev1") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^16.1.0-dev1") (k 0)))) (h "1la2d6f0v1pxfrlz67ixp0wgsy06a5gg5vwqlac2p3vb67qcad39") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev.2 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev.2") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^16.1.0-dev.2") (k 0)))) (h "0iqps2c28mkiya1dsywd5bbybbc16nlx1llqahpb5891pn1jj61q") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev.3 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev.3") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^16.1.0-dev.3") (k 0)))) (h "0knkxjbly3ybsf8pxs6ca6ga94zqgfa41l3lzaajiqg7hm04p0z9") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev.4 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev.4") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^16.1.0-dev.4") (k 0)))) (h "093a6l9ss4z72avfc2760w6gq6qlg9gflkz708y0myqmcn8j3f2a") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev.5 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev.5") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "=16.1.0-dev.5") (k 0)))) (h "1pjly70s53fi689bf1x4a32pkam5jdn7knsq3zsva27gjbilslzm") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-12.1.0-dev.6 (c (n "pallet-staking-reward-fn") (v "12.1.0-dev.6") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "=16.1.0-dev.6") (k 0)))) (h "06g5xakigv3mp4mi726fh03vpkymwjbzss9imawf3l4wzggwxxz7") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-13.0.0 (c (n "pallet-staking-reward-fn") (v "13.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^17.0.0") (k 0)))) (h "0v0xjmpk2ji0zmlv3rhx1wi69rghwlrmw2r4w9vd6za7q718pgik") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-14.0.0 (c (n "pallet-staking-reward-fn") (v "14.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^18.0.0") (k 0)))) (h "19kdqyp40vkzk0rf9wdyk9m61sfypzsf8h4d12lgjkr6yj48km77") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-15.0.0-dev.1 (c (n "pallet-staking-reward-fn") (v "15.0.0-dev.1") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "=19.0.0-dev.1") (k 0)))) (h "1ikd2rbhbz9a9d6c1aqydkfva4gmdjnhn1m6b9hi099aagz1c2iz") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-15.0.0 (c (n "pallet-staking-reward-fn") (v "15.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^19.0.0") (k 0)))) (h "1lgqb0m8xvzz803jrr10xqd4n59cjbawgla8whwmyav0zffyz052") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-16.0.0 (c (n "pallet-staking-reward-fn") (v "16.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^20.0.0") (k 0)))) (h "0fq23c7viy03mwaqbw97kcavh0jbsvgc3v6z1gvvbb9vclrgidj5") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-17.0.0 (c (n "pallet-staking-reward-fn") (v "17.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^21.0.0") (k 0)))) (h "0rbblxm04vjrdmdzd4dfrw1z3ypqpf9f1j4gmn65yyw40ks5yv02") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-18.0.0 (c (n "pallet-staking-reward-fn") (v "18.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^22.0.0") (k 0)))) (h "1ymya0w5pfllvjf53za3j2n4wzivakcpwmihyw84b9q64gav1xf7") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-19.0.0 (c (n "pallet-staking-reward-fn") (v "19.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^23.0.0") (k 0)))) (h "0pk0kibv28w9nda9cjc7gz52djb4cna356azz4ric447vbj3ccz2") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-20.0.0 (c (n "pallet-staking-reward-fn") (v "20.0.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-arithmetic") (r "^24.0.0") (k 0)))) (h "179ih2lf7x6xdhghkj7xdzj3xlj4c9mhmw2izdajy1ddigh4apah") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-21.0.0 (c (n "pallet-staking-reward-fn") (v "21.0.0") (d (list (d (n "log") (r "^0.4.20") (k 0)) (d (n "sp-arithmetic") (r "^25.0.0") (k 0)))) (h "065flqssjz9qd0z42n8m1ni5sk1wccsasrpivj7bch0h913iqd0y") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

(define-public crate-pallet-staking-reward-fn-22.0.0 (c (n "pallet-staking-reward-fn") (v "22.0.0") (d (list (d (n "log") (r "^0.4.21") (k 0)) (d (n "sp-arithmetic") (r "^26.0.0") (k 0)))) (h "1g3fzg7lgdcdqy5qyza471af7zsnx60rch0j1gdlnkf8mjz7x2lq") (f (quote (("std" "log/std" "sp-arithmetic/std") ("default" "std"))))))

