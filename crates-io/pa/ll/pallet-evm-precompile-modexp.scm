(define-module (crates-io pa ll pallet-evm-precompile-modexp) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-modexp-1.0.0 (c (n "pallet-evm-precompile-modexp") (v "1.0.0") (d (list (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "13wqqkn710lyngdwgaq9fckzfnj8qjs5xsr689xlpxqmq555znsn") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std" "num/std") ("default" "std"))))))

