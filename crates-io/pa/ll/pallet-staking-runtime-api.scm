(define-module (crates-io pa ll pallet-staking-runtime-api) #:use-module (crates-io))

(define-public crate-pallet-staking-runtime-api-0.0.0 (c (n "pallet-staking-runtime-api") (v "0.0.0") (h "1622pnbpb2gzxh9r619fg0y8bjbzx5qk43b1498241cgkhm0m4i9")))

(define-public crate-pallet-staking-runtime-api-4.0.0 (c (n "pallet-staking-runtime-api") (v "4.0.0") (d (list (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^16.0.0") (k 0)))) (h "06wcii8yykp7f8v7by1daz1rw2k7nync0q57l94nc1rdf00x1rfm") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-5.0.0 (c (n "pallet-staking-runtime-api") (v "5.0.0") (d (list (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^17.0.0") (k 0)))) (h "1qw5dy2j6h9wrzgk11a08sq4li7bn8dmlszv1b1d44aq6pqmdid2") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-6.0.0 (c (n "pallet-staking-runtime-api") (v "6.0.0") (d (list (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^18.0.0") (k 0)))) (h "1n862m37n2cp9q92v1w76visbxk8hngn38ldn33zlpm7qxnv60wj") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-7.0.0 (c (n "pallet-staking-runtime-api") (v "7.0.0") (d (list (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.0.0") (k 0)))) (h "0v9jwgvdrn6zb4dksm1pfq5y3j6sbyziw4z50cb26c2b2gm47c9p") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-7.1.0-dev.2 (c (n "pallet-staking-runtime-api") (v "7.1.0-dev.2") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.1.0-dev.2") (k 0)))) (h "1663fydc59gmml6kaf5iq7wbv9yblvws9nkxkprgcb95gj4v1x49") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-7.1.0-dev.3 (c (n "pallet-staking-runtime-api") (v "7.1.0-dev.3") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.1.0-dev.3") (k 0)))) (h "1vkw8vswv71djcdd7mhm0kszadw8an8dmhp4ab9bs2ia4jxdmr2m") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-7.1.0-dev.5 (c (n "pallet-staking-runtime-api") (v "7.1.0-dev.5") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=19.1.0-dev.5") (k 0)))) (h "1731ayv2gjp7psnykrrqgk11alp0wmmfbnzmgdrd8gr3vyfpi2p1") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-7.1.0-dev.6 (c (n "pallet-staking-runtime-api") (v "7.1.0-dev.6") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=19.1.0-dev.6") (k 0)))) (h "0vk88i655kr56mj89xb3licg888a68b13vwnplry5w3nm6z9jch5") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-8.0.0 (c (n "pallet-staking-runtime-api") (v "8.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^20.0.0") (k 0)))) (h "1xiffkz826ixcxl1326l2n023wzlqj2zsz6jlw8c5b3ikbhd3vb6") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-9.0.0 (c (n "pallet-staking-runtime-api") (v "9.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^21.0.0") (k 0)))) (h "19n8pgd4xf7mz3x0834q7iddnvl2734a0wnnmkg8iipk6j090fqa") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-10.0.0-dev.1 (c (n "pallet-staking-runtime-api") (v "10.0.0-dev.1") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=22.0.0-dev.1") (k 0)))) (h "068h9xi95czzb45f8lrvj0qggv2x7m4pdsvm9hrlgwfrwngh4akk") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-10.0.0 (c (n "pallet-staking-runtime-api") (v "10.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^22.0.0") (k 0)))) (h "198kvp6l67cm2mng16466zjy34gicjz3zmyjgcjmnwn58bmlvdky") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-11.0.0 (c (n "pallet-staking-runtime-api") (v "11.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^23.0.0") (k 0)))) (h "0j272xn7k91j0xvvq131y4syix0aihffjhvvwh2vb92dmlj0sg3p") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-12.0.0 (c (n "pallet-staking-runtime-api") (v "12.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^24.0.0") (k 0)) (d (n "sp-staking") (r "^24.0.0") (k 0)))) (h "1r7kbdw8bzalm7pikywpij6lqhxmh353x9v69af3fi6z5fc8c2z3") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-13.0.0 (c (n "pallet-staking-runtime-api") (v "13.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^25.0.0") (k 0)) (d (n "sp-staking") (r "^25.0.0") (k 0)))) (h "09c7yx6br6lila0kr76hflv91fiaz4rhzy532r84bhymrvzrvqac") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-14.0.0 (c (n "pallet-staking-runtime-api") (v "14.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^26.0.0") (k 0)) (d (n "sp-staking") (r "^26.0.0") (k 0)))) (h "0zpycz7kz4b71yii7f8qkrccdncqdqdipsm1n6f57jzcfavmcwg2") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-15.0.0 (c (n "pallet-staking-runtime-api") (v "15.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^27.0.0") (k 0)) (d (n "sp-staking") (r "^27.0.0") (k 0)))) (h "19f1540zalxsb5krwn6f7jm04qvp86r7z59ygirblv6b21d7nz93") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-16.0.0 (c (n "pallet-staking-runtime-api") (v "16.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^28.0.0") (k 0)) (d (n "sp-staking") (r "^28.0.0") (k 0)))) (h "1vscckpiwx8xj31215gf99a5awy4bb8ywkqakbn70kackhgsywr7") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-17.0.0 (c (n "pallet-staking-runtime-api") (v "17.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^29.0.0") (k 0)) (d (n "sp-staking") (r "^29.0.0") (k 0)))) (h "0m5b1j31r2jn4j8jgqi360sc7rwmi0592lb2swppwvpd234vp65k") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-18.0.0 (c (n "pallet-staking-runtime-api") (v "18.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^30.0.0") (k 0)) (d (n "sp-staking") (r "^30.0.0") (k 0)))) (h "0y2h286cv0f4743gmpd6y4lcibxiwbidf71hfzhp0bdl6nr951sb") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-19.0.0 (c (n "pallet-staking-runtime-api") (v "19.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^31.0.0") (k 0)) (d (n "sp-staking") (r "^31.0.0") (k 0)))) (h "0rzfglyk11pmfjq1886fn5zyyr40sd2p7yr9x2gv6n42vdsancf4") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-15.0.1 (c (n "pallet-staking-runtime-api") (v "15.0.1") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^27.0.0") (k 0)) (d (n "sp-staking") (r "^27.0.0") (k 0)))) (h "1iqvv0fkqyxmvzb5njawj2vdmw0yzrx10w7i3sv31nq3062p6z74") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

(define-public crate-pallet-staking-runtime-api-20.0.0 (c (n "pallet-staking-runtime-api") (v "20.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^32.0.0") (k 0)) (d (n "sp-staking") (r "^32.0.0") (k 0)))) (h "1f23i748bxb7dqszw5vw798lndzjcdlmifynv0cxjgz8jvqb49nw") (f (quote (("std" "codec/std" "sp-api/std" "sp-staking/std") ("default" "std"))))))

