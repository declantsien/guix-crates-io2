(define-module (crates-io pa ll pallas-machines) #:use-module (crates-io))

(define-public crate-pallas-machines-0.1.0 (c (n "pallas-machines") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.11.4") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.1.1") (d #t) (k 0)))) (h "1ch7ph62gq95qby2yfwyn3967xvjnlrn32yg7l6gngxwbkmnlz90")))

(define-public crate-pallas-machines-0.3.0 (c (n "pallas-machines") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.11.4") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.3.0") (d #t) (k 0)))) (h "0x9jf1g80h5jsazy8imr00zv51md9rmzzk5p4cdnrhfzal4283b8")))

(define-public crate-pallas-machines-0.3.1 (c (n "pallas-machines") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.11.4") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.3.0") (d #t) (k 0)))) (h "1nys2463a6wzpj5s76zidrb9llxsmqjhnba5h3ac0p184acv0nmq")))

(define-public crate-pallas-machines-0.3.3 (c (n "pallas-machines") (v "0.3.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.12") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.3.0") (d #t) (k 0)))) (h "19kqpgblp1py45w0gglffk7c3w9gz6nmzr31r0db4cg0ckm4fdbc")))

(define-public crate-pallas-machines-0.3.4 (c (n "pallas-machines") (v "0.3.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.12") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.3.0") (d #t) (k 0)))) (h "1j2yvrjymifw7j9gdsl7xs15rkhq64kh9y0ah0jjbf1dyhkz02ka")))

(define-public crate-pallas-machines-0.3.5 (c (n "pallas-machines") (v "0.3.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.12") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.3.0") (d #t) (k 0)))) (h "1j5vik3kmkv7z00yf0hl8yqazp0svws4gp05fl0gihmgj9vswdaz")))

(define-public crate-pallas-machines-0.4.0 (c (n "pallas-machines") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("half" "std"))) (d #t) (k 0)) (d (n "pallas-multiplexer") (r "^0.4.0") (d #t) (k 0)))) (h "11z883x7sq4z35m67510h11w5i4dfxw7qd0aq0kkl1mcvqx73a27")))

