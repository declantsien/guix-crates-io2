(define-module (crates-io pa ll pallete) #:use-module (crates-io))

(define-public crate-pallete-0.1.6 (c (n "pallete") (v "0.1.6") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1q7si6wgv34sm9nsr124z7jh8zbs80n5g79vn16g67vfn0ri5qvn")))

(define-public crate-pallete-1.0.0 (c (n "pallete") (v "1.0.0") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1wv2andazpq4zq6rsp29lfgsj84ljirzrp50drkm9cyyv2p86bi5")))

(define-public crate-pallete-1.1.0 (c (n "pallete") (v "1.1.0") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0jakbb13bvmv96dx2c7r84dzwp2pys8gb8bi8vpixg4ycqhhddaw")))

