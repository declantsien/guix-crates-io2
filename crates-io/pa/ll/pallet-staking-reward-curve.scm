(define-module (crates-io pa ll pallet-staking-reward-curve) #:use-module (crates-io))

(define-public crate-pallet-staking-reward-curve-2.0.0-alpha.3 (c (n "pallet-staking-reward-curve") (v "2.0.0-alpha.3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0bkmh8d7g4zini8s1drwqqg391b579pcbzxw6zxcn304byag3vy1")))

(define-public crate-pallet-staking-reward-curve-2.0.0-alpha.5 (c (n "pallet-staking-reward-curve") (v "2.0.0-alpha.5") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1hvjyidfazl8dj4dq8rp52h7zds2h6i768hl68ld8zqk61shfpq7")))

(define-public crate-pallet-staking-reward-curve-2.0.0-alpha.6 (c (n "pallet-staking-reward-curve") (v "2.0.0-alpha.6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xnkfq5j6394a5krr0x524nqvhw52xhrkd42yggg9hz09sknwj2d")))

(define-public crate-pallet-staking-reward-curve-2.0.0-alpha.7 (c (n "pallet-staking-reward-curve") (v "2.0.0-alpha.7") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1j316dv1fi7i3595xmwhbv949hmw2x74ypxnd0bx4xvs42l1v1gq")))

(define-public crate-pallet-staking-reward-curve-2.0.0-alpha.8 (c (n "pallet-staking-reward-curve") (v "2.0.0-alpha.8") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0b2np4pmcv56yb874pdrb7cgpvndm1w799xa88v9x5yd75vgc6za")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc1 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1whba9bandgf179vf0gywminvbqj7g5higp7qcm0wad8vqqiaxln")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc2 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0r2g77z4f0zapcfpp0jv689qg4mrnc5nxrssfnzfzdylqmy1ijkv")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc3 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "079bv780bgfmsbvxy0qv9nbbqlc3j20hb3s7q368ifjryyp2a6qf")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc4 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc4") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1aiskbd2m5dsyc9yxk3y7f1a8zr53ypzrkkc9v18zsy843a6cqwg")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc5 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc5") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0wdvcz4bg18rjpsfgbn3i8f7qgxmimrfywy9wlirjkj59yd0irwg")))

(define-public crate-pallet-staking-reward-curve-2.0.0-rc6 (c (n "pallet-staking-reward-curve") (v "2.0.0-rc6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0fa03b67cjw2jajv8j9n96ircr5z5nfaylqfkq1w4b3mdik7bv0a")))

(define-public crate-pallet-staking-reward-curve-2.0.0 (c (n "pallet-staking-reward-curve") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1z2kvkrmmpky76p919bbb858pznn5jrpwgbdh118py3aqcd75bhy")))

(define-public crate-pallet-staking-reward-curve-2.0.1 (c (n "pallet-staking-reward-curve") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xdaak9lzb5ipaj6hs6w6q7nqp68g26a2k7mrnlcxnq6fgfg8ma1")))

(define-public crate-pallet-staking-reward-curve-3.0.0 (c (n "pallet-staking-reward-curve") (v "3.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1459pp3pwyb7gv6khvyzcpy7aqsyrl4vy3kbg3y7rklp4ng6vcbw")))

(define-public crate-pallet-staking-reward-curve-4.0.0 (c (n "pallet-staking-reward-curve") (v "4.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0chpzgw3wx913p058vbk3xyvgs1mhc82hrzmjg5l3l6sjvfr9gmb")))

(define-public crate-pallet-staking-reward-curve-5.0.0 (c (n "pallet-staking-reward-curve") (v "5.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0hxibk812ylsfzffb2xhafdsq9y6jrgq9iz7cw2z0nymj04b5c87")))

(define-public crate-pallet-staking-reward-curve-6.0.0 (c (n "pallet-staking-reward-curve") (v "6.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "18h5wwyh1x72m63ab2vgxk638hf8l457nyivdkk7rbbqwcrx54zx")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev1 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0a6mgkdr5wiizxlq48rvmhzhhc7wjp0mmzslxy32qrvzqzf3mfwz")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev.2 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev.2") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0nv8vys0c1j1dzcf2j8b3vhhdka9ma6yw41zzxs337mfl1q7ysww")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev.3 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev.3") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "19s0lbchkf3q4rwdcx2ky75i400s9wsw7kg4xgam1grfics02xvb")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev.4 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev.4") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ch3griq52197b6rk86a0kiaagkhximy1m5l5k7fyf3lql51x9bk")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev.5 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev.5") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0f38j22xzcs3gf3392aybvay91vj885c573zyph03k3a5dpifacb")))

(define-public crate-pallet-staking-reward-curve-6.1.0-dev.6 (c (n "pallet-staking-reward-curve") (v "6.1.0-dev.6") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "01jrijv7vvn5mvgwssxc5r1y2fqxa76yr8wfv2x43vfcn134ndsi")))

(define-public crate-pallet-staking-reward-curve-7.0.0 (c (n "pallet-staking-reward-curve") (v "7.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11jijf1m1aswh21yinzgv1wi3pzsx5a6knznk8inidk7xwshbqfa")))

(define-public crate-pallet-staking-reward-curve-8.0.0 (c (n "pallet-staking-reward-curve") (v "8.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1hshmc980af5yj5kqhxqvrjbc31ck8nwkm717lf0lhy36n2l7cbm")))

(define-public crate-pallet-staking-reward-curve-9.0.0-dev.1 (c (n "pallet-staking-reward-curve") (v "9.0.0-dev.1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0zlz9767zlarbg5wfj8g8dibwp32ghiikhvijn4skcj1n9g0k9c0")))

(define-public crate-pallet-staking-reward-curve-9.0.0 (c (n "pallet-staking-reward-curve") (v "9.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1z431vma185q50733d59zv6ykp9rfxi0mnnvf29w3pmnxlhap6cx")))

(define-public crate-pallet-staking-reward-curve-10.0.0 (c (n "pallet-staking-reward-curve") (v "10.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1mw40vna3s2mz5bmcf4gzb8sjh9f8qgn455pn70il01xkzi7i26z")))

(define-public crate-pallet-staking-reward-curve-11.0.0 (c (n "pallet-staking-reward-curve") (v "11.0.0") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1567njbbvq3g64iwrhw26wgbj5lccjmhgzpm7b4d49rl8955mjpg")))

