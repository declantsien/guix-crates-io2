(define-module (crates-io pa ll pallet-plonk) #:use-module (crates-io))

(define-public crate-pallet-plonk-0.0.1 (c (n "pallet-plonk") (v "0.0.1") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-benchmarking") (r "^3.1.0") (o #t) (k 0)) (d (n "frame-support") (r "^3.0.0") (k 0)) (d (n "frame-system") (r "^3.0.0") (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 2)) (d (n "sp-core") (r "^3.0.0") (k 2)) (d (n "sp-io") (r "^3.0.0") (k 2)) (d (n "sp-runtime") (r "^3.0.0") (k 2)))) (h "12gd5aq2wkqdi6sd6say38nmk29mabj11hl9q1m8xa8907ibvq34") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "frame-benchmarking/std") ("runtime-benchmarks" "frame-benchmarking") ("default" "std"))))))

