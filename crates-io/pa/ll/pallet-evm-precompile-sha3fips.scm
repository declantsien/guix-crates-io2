(define-module (crates-io pa ll pallet-evm-precompile-sha3fips) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-sha3fips-1.0.0 (c (n "pallet-evm-precompile-sha3fips") (v "1.0.0") (d (list (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("fips202"))) (d #t) (k 0)))) (h "0hc7vj1r1qnmlyigdkv0hkr0b3qsq3x4mg8mylh7ii75xx93rwi4") (f (quote (("std" "sp-core/std" "sp-io/std" "fp-evm/std" "evm/std") ("default" "std"))))))

