(define-module (crates-io pa ll pallet-evm-precompile-dispatch) #:use-module (crates-io))

(define-public crate-pallet-evm-precompile-dispatch-1.0.0 (c (n "pallet-evm-precompile-dispatch") (v "1.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "frame-support") (r "^3.0.0") (k 0)) (d (n "pallet-evm") (r "^3.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-io") (r "^3.0.0") (k 0)))) (h "0pwfjv0nlfpqkr89mwzjcn4jglm19yvs7c1s2d6wz4v812lw7rqr") (f (quote (("std" "sp-core/std" "sp-io/std" "frame-support/std" "pallet-evm/std" "fp-evm/std" "evm/std" "codec/std") ("default" "std"))))))

