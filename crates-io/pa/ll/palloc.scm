(define-module (crates-io pa ll palloc) #:use-module (crates-io))

(define-public crate-palloc-0.1.0 (c (n "palloc") (v "0.1.0") (d (list (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "0y3s40624pnw62i3ix460223hf4xjlwg6ib8finkb9hmp79r4ryi") (f (quote (("default" "spin" "allocator_api") ("allocator_api"))))))

(define-public crate-palloc-0.1.1 (c (n "palloc") (v "0.1.1") (d (list (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "08zka4j0ynyi2cm57820g19mg8c1alin3r6yillddaw6hfxxrlwl") (f (quote (("default" "spin" "allocator_api") ("allocator_api"))))))

(define-public crate-palloc-0.1.2 (c (n "palloc") (v "0.1.2") (d (list (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "119sxdx57anqixlmqdnmvnqw8sys6q7x9j2c0bkk6qzp3bfjs86n") (f (quote (("default" "spin" "allocator_api") ("allocator_api"))))))

(define-public crate-palloc-0.1.3 (c (n "palloc") (v "0.1.3") (d (list (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "0ncvxl3zshmcdd9g82kbabl77b4pjci0xgmbardwnr68afkxan6j") (f (quote (("default" "spin" "allocator_api") ("allocator_api"))))))

