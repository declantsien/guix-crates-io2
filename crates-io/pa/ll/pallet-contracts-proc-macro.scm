(define-module (crates-io pa ll pallet-contracts-proc-macro) #:use-module (crates-io))

(define-public crate-pallet-contracts-proc-macro-3.0.0 (c (n "pallet-contracts-proc-macro") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0l7x4rv6yw726snyvihxb4lbfgn93gxwsxvc59spfgzfmqbn014h") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-4.0.0 (c (n "pallet-contracts-proc-macro") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "11dq88nbph46k72jxaya1aqy1y4vc0ds4wz3jwq8zh9gs9q3pmvm") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-5.0.0 (c (n "pallet-contracts-proc-macro") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzlaran177axchya3sv27rf0j1kq9rcvxa89r1yfvb78r3bxxmy") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-6.0.0 (c (n "pallet-contracts-proc-macro") (v "6.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1icps6bx28nvq5kma0aqsh9d6mlm8wwgacjgcr5gcq925zijzck1") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-7.0.0 (c (n "pallet-contracts-proc-macro") (v "7.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1fq1b3ddsrr95qamp3qnq6nlf5ap7yjk72xrkpj0yj1c4wangk26") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-8.0.0 (c (n "pallet-contracts-proc-macro") (v "8.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0xa88nfxwy5nq1ddy74zpw2gxswq641zvkndv2pqgvkp8nr7ldv6") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-9.0.0 (c (n "pallet-contracts-proc-macro") (v "9.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "040d0wn0zwh33ims9xzhafib21ly7k2c33pl1c1cjxc2jwiqg9gy") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-10.0.0 (c (n "pallet-contracts-proc-macro") (v "10.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1svixvz60qy5qzkc01nas09nx81hym5fdlaa5ygk0q5rahmbwpma") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-11.0.0 (c (n "pallet-contracts-proc-macro") (v "11.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0akby05jfjxw55br3z81cyiglxa7liv11zfyh4198rwrq8vr7cq1") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.0.0 (c (n "pallet-contracts-proc-macro") (v "12.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1df1rlq9s1kjv46n9922q6n511yla5gavkmqras88skq87svdr4x") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev1 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0s44yvvmqigz4wckgsfrv13h7pbpimqq4mbji33dnm8szlm9g9mc") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev.2 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0i5w5y7wkd60gn6wxlcaamigrf1v75byfdd4lm2r7ddq4qmb2dhr") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev.3 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "11ki5kqkj7fq3ck3cfxb23nll6mjjxcb3i41h4n6r2kzblpqf09c") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev.4 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "18n2q9p93cgcngl2k3wcywlqj7h5m0mpyj3bl4bx7jyf4zlwc5m8") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev.5 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "03qim2hzsrj0kws8m5faj7sjxdb5adsqg0lw3bfjri5pybwa4zj9") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-12.1.0-dev.6 (c (n "pallet-contracts-proc-macro") (v "12.1.0-dev.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0c66bqn8vwx8vnnn04zakjx7y54l689qjnz6babdvjl0ks1512wm") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-13.0.0 (c (n "pallet-contracts-proc-macro") (v "13.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "07n6phazbhs8lzh7fn4z7w6b22y4276d932rfw5d32dzqibpi09p") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-14.0.0 (c (n "pallet-contracts-proc-macro") (v "14.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1xpv6ivpblxdy9ckgfrp1ssl2qgqcrvhzxj2l8ygnl6lw9myphzs") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-15.0.0-dev.1 (c (n "pallet-contracts-proc-macro") (v "15.0.0-dev.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "061a3lad16hii4dyi1c5lvwcsf9fmq48430mi5ln7w7zc635k6ha") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-15.0.0 (c (n "pallet-contracts-proc-macro") (v "15.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0rhg7r57v819vavni51cbc0xxklpcyfl1gwh56nw4k6q7qcqw7ph") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-16.0.0 (c (n "pallet-contracts-proc-macro") (v "16.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1hcdhx6wymxi6rbjrllynqc8zhwfwbccgks5dl1ya028ykxmqyvj") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-17.0.0 (c (n "pallet-contracts-proc-macro") (v "17.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "064979x6472qdjwlidppppi6rlhgqkzc6adxl59gsip0ns09zib4") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-18.0.0 (c (n "pallet-contracts-proc-macro") (v "18.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "01yha79ggvhjyv1kh3av2b5fd1hh6mrnypryqnlmc2lz590wapz6") (f (quote (("full"))))))

(define-public crate-pallet-contracts-proc-macro-19.0.0 (c (n "pallet-contracts-proc-macro") (v "19.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "02g5pi62y1d352fha314p6z6k7lw9mwblin5fk5hqnmm46yccqri")))

(define-public crate-pallet-contracts-proc-macro-20.0.0 (c (n "pallet-contracts-proc-macro") (v "20.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "15mb4656vx3cci4ngmmpx4jsygfn8w0pbawfpqf7xllf3004yv64")))

(define-public crate-pallet-contracts-proc-macro-21.0.0 (c (n "pallet-contracts-proc-macro") (v "21.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "1364q8yz80z4m8cica4ggs5pnimr7xlcgb8ma37n92f50kcv236y")))

