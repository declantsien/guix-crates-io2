(define-module (crates-io pa sh pash) #:use-module (crates-io))

(define-public crate-pash-0.1.0 (c (n "pash") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0kns1844m1bvp8prbidmrhcqzcrkqv04hi3rqja5n12yxrjz343k")))

(define-public crate-pash-0.1.1 (c (n "pash") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "12c9lih9q799pkpnj4pkx4ivl9yicqarhkz1q6drdb2ia08apswk")))

(define-public crate-pash-0.2.0 (c (n "pash") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "144911lw3wz0dsl70dg90sdvpcqzsi0kvn81kfxrb6a0lbfcndzk")))

(define-public crate-pash-0.3.0 (c (n "pash") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1skgilir50hhl9rlflwsi9nlb0kaf4iybj8y942vdy3y6nwf80rd")))

