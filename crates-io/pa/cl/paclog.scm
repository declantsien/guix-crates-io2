(define-module (crates-io pa cl paclog) #:use-module (crates-io))

(define-public crate-paclog-0.1.0 (c (n "paclog") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zen-colour") (r "^1.0.1") (d #t) (k 0)))) (h "1hsyzcmla6gk70hldy3rda8r1hrxvm974ffswl2kqvxbkbnm6a2b")))

(define-public crate-paclog-1.0.0 (c (n "paclog") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.0.1") (d #t) (k 0)))) (h "0wfhhxn2gvas2hczxd7s1j8hpika307q3w9953p4an5zp48walwh")))

(define-public crate-paclog-1.1.0 (c (n "paclog") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.1.1") (d #t) (k 0)))) (h "0n4x5fwqwqsym2yi0yrsqv3gqc4iwkrmj2wgpdlvnnxggyjlk5x9")))

(define-public crate-paclog-2.0.0 (c (n "paclog") (v "2.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.1.1") (d #t) (k 0)))) (h "10k3n36wd51f42sklq9hdz63rvkbsn2d6ch6c5ha67vvx53kyg7p")))

(define-public crate-paclog-3.0.0 (c (n "paclog") (v "3.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.1.1") (d #t) (k 0)))) (h "03h0lgwy15h7anrfkl4y2y7jc3k6l0m410mgsq05njh22si49lqb")))

(define-public crate-paclog-3.1.0 (c (n "paclog") (v "3.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.1.1") (d #t) (k 0)))) (h "09ivx26hdpdb7arq8n45mfvg7izi29gvcv5hp24a09yqknv9nb61")))

(define-public crate-paclog-3.2.0 (c (n "paclog") (v "3.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)) (d (n "zen-colour") (r "^1.1.1") (d #t) (k 0)))) (h "0vaaxd5087wvdlp6gnphhgwy29x67h82gpy5wfivqlzp9i42zxha")))

