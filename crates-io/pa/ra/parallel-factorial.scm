(define-module (crates-io pa ra parallel-factorial) #:use-module (crates-io))

(define-public crate-parallel-factorial-0.1.0 (c (n "parallel-factorial") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "00ypmrg2vbqs8km2j637xgwwy2z725i01v3mvfy4kd3rxk5p3yg9")))

(define-public crate-parallel-factorial-0.2.0 (c (n "parallel-factorial") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ibig") (r "^0.3.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0kkzhy3b8pxv1hwb2m7ilyplrjn41i9qaa2cf4fqcvb69p8saz6k")))

(define-public crate-parallel-factorial-0.2.1 (c (n "parallel-factorial") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ibig") (r "^0.3.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0glq8dnjyj2ql5mxjdw8xhrqlmpwc8jawsf0fyj6py5zcs3yg79y")))

