(define-module (crates-io pa ra paradox) #:use-module (crates-io))

(define-public crate-paradox-0.0.0 (c (n "paradox") (v "0.0.0") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.15") (d #t) (k 0)))) (h "16bqb13npphk24bq7b3xvq0j4i0n9xb84pxsp0hwa2lz4maxypmm")))

(define-public crate-paradox-0.1.0 (c (n "paradox") (v "0.1.0") (d (list (d (n "assembly") (r "^0.3") (d #t) (k 0)))) (h "02p07a5w9snsy6vlzwcnc9kxbgv0kbq7ifq8iyb815n20wq16llv")))

