(define-module (crates-io pa ra paramdex-rs) #:use-module (crates-io))

(define-public crate-paramdex-rs-0.1.0 (c (n "paramdex-rs") (v "0.1.0") (d (list (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "09lks2jn2jnxn39ik578gf6bczy06gbklkpamkpvy3wbb1967mxl")))

(define-public crate-paramdex-rs-0.1.0+build.2 (c (n "paramdex-rs") (v "0.1.0+build.2") (d (list (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0mx9c4mxkqpxfdkk212an8hwwp1ggr99yx1w689hl45yphxb0hig")))

