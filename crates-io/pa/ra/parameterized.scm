(define-module (crates-io pa ra parameterized) #:use-module (crates-io))

(define-public crate-parameterized-0.1.0 (c (n "parameterized") (v "0.1.0") (d (list (d (n "parameterized-macro") (r "^0.1") (d #t) (k 0)))) (h "0bv8f66r6lcg2iwrkqja1gyh4vghk9qk5xxgbkxdjxp27jyinljf")))

(define-public crate-parameterized-0.1.1 (c (n "parameterized") (v "0.1.1") (d (list (d (n "parameterized-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1wvmvw1jcl1qg4akdllj7x7h8j5c7cf73wyjdbn9hml5prklj3y9")))

(define-public crate-parameterized-0.2.0 (c (n "parameterized") (v "0.2.0") (d (list (d (n "parameterized-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1zwrxdkvhy70l2grkc94cxnf9gffbm76nmw0hv3709pa417zcwab")))

(define-public crate-parameterized-0.3.0 (c (n "parameterized") (v "0.3.0") (d (list (d (n "parameterized-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1xpj04kf99nchchc08flli52lz7xnxdpi280krdyfl1gx45ncf77")))

(define-public crate-parameterized-0.3.1 (c (n "parameterized") (v "0.3.1") (d (list (d (n "parameterized-macro") (r "=0.3.1") (d #t) (k 0)))) (h "19crksznaiscmym6vzb9hkb8gg7dm5xghar4lj074l5xwygrh5m0")))

(define-public crate-parameterized-1.0.0 (c (n "parameterized") (v "1.0.0") (d (list (d (n "parameterized-macro") (r "^1") (d #t) (k 0)))) (h "071qk22880v1mym10xfa8mv33hzxy33x54xi6n46sbc8zqbksm1k") (f (quote (("square-brackets-old-error-message" "parameterized-macro/square-brackets-old-error-message"))))))

(define-public crate-parameterized-1.0.1 (c (n "parameterized") (v "1.0.1") (d (list (d (n "parameterized-macro") (r "^1") (d #t) (k 0)))) (h "06xikpnkr8zai5iv5d4sl5fb1gk2kh4rvbxdrcyvna8qrrxcirds") (f (quote (("square-brackets-old-error-message" "parameterized-macro/square-brackets-old-error-message"))))))

(define-public crate-parameterized-1.1.0 (c (n "parameterized") (v "1.1.0") (d (list (d (n "parameterized-macro") (r "^1") (d #t) (k 0)))) (h "081x6kjm6ap0bj5v860lmswkc4ky2nqp1cq3ps8s4flnkr4yacrh") (f (quote (("square-brackets-old-error-message" "parameterized-macro/square-brackets-old-error-message"))))))

(define-public crate-parameterized-2.0.0 (c (n "parameterized") (v "2.0.0") (d (list (d (n "parameterized-macro") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kmxw0kxa341n7y25mf9frc453w2bwr4vlhv9wnmbnjdcybz8jqr") (f (quote (("square-brackets-old-error-message" "parameterized-macro/square-brackets-old-error-message")))) (r "1.63")))

