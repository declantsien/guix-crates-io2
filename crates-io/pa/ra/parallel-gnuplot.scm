(define-module (crates-io pa ra parallel-gnuplot) #:use-module (crates-io))

(define-public crate-parallel-gnuplot-0.1.2 (c (n "parallel-gnuplot") (v "0.1.2") (d (list (d (n "blockcounter") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0mmfixsfrc0djn9iy9knad9v3h82k4kcfhg5b64j9k24pq7nmfp9")))

(define-public crate-parallel-gnuplot-0.1.3 (c (n "parallel-gnuplot") (v "0.1.3") (d (list (d (n "blockcounter") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1hk1r99w353m9rajpw2y58rdgapb0d351ym7xk2yragyyiq9h5pp")))

(define-public crate-parallel-gnuplot-0.1.4 (c (n "parallel-gnuplot") (v "0.1.4") (d (list (d (n "blockcounter") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "02j1243fs4jk4d1c0wm080s9v8bvx00qfr404cd116yci34m2mi2")))

(define-public crate-parallel-gnuplot-0.1.5 (c (n "parallel-gnuplot") (v "0.1.5") (d (list (d (n "blockcounter") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 1)))) (h "1nl0fzv5vhgfmx2wxaj01wwp0hcnaq7hnllph4iyrla03s4qyrhg")))

(define-public crate-parallel-gnuplot-0.1.6 (c (n "parallel-gnuplot") (v "0.1.6") (d (list (d (n "blockcounter") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 1)))) (h "12f65l7z48psmqkd0mrj4h8pwmjhrsx3vvligiqhdzl5zspf4wvw")))

(define-public crate-parallel-gnuplot-0.1.7 (c (n "parallel-gnuplot") (v "0.1.7") (d (list (d (n "blockcounter") (r "^0.2.4") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 1)))) (h "16bgfk4nib0fbs3ba61c78a5hbdcjm33rbz1yijg8j5k7rkvi7sw")))

(define-public crate-parallel-gnuplot-0.2.0 (c (n "parallel-gnuplot") (v "0.2.0") (d (list (d (n "blockcounter") (r "^0.2.4") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0m3apc5qi0gym48bxr3iamvh8jm8kizyj5r5phg5qxc4hxajhdbh")))

(define-public crate-parallel-gnuplot-0.2.1 (c (n "parallel-gnuplot") (v "0.2.1") (d (list (d (n "blockcounter") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1dcyy5pl0hdknqw4pfs0yshawsx34vc9x2y0c7qxn83nb8gsfbz3")))

(define-public crate-parallel-gnuplot-0.2.2 (c (n "parallel-gnuplot") (v "0.2.2") (d (list (d (n "blockcounter") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0l2dnhvcdfpl40f9vqrvmv65dqc6689y1b2j5dl321vpw9m5i1yg")))

