(define-module (crates-io pa ra parallel-iterator) #:use-module (crates-io))

(define-public crate-parallel-iterator-0.1.0 (c (n "parallel-iterator") (v "0.1.0") (d (list (d (n "chan") (r "^0.1.21") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)))) (h "0wdn3mzlkh09zhcqhp8imq75xym7xx0f3dpyhmwrazspb72y0yyw")))

(define-public crate-parallel-iterator-0.1.1 (c (n "parallel-iterator") (v "0.1.1") (d (list (d (n "chan") (r "^0.1.21") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)))) (h "0zx10fimyy55rcgd360zhjllz2flsniiil8m141307rdjhsv64hk")))

(define-public crate-parallel-iterator-0.1.2 (c (n "parallel-iterator") (v "0.1.2") (d (list (d (n "chan") (r "^0.1.23") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "1m0lbiq8l0h9919fq4ryn3yibdd31w14ycqiv1zj3w760x14d900")))

(define-public crate-parallel-iterator-0.1.3 (c (n "parallel-iterator") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.3.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "0knz3gdy42c85gsr93qplqq0irwhg4wr61bab8rqdw0f9rggk98p")))

(define-public crate-parallel-iterator-0.1.4 (c (n "parallel-iterator") (v "0.1.4") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "15rsqwri73165fw083j48nzwidbrngg2z1xv81j6x9v96n5whgbx")))

(define-public crate-parallel-iterator-0.1.5 (c (n "parallel-iterator") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)))) (h "1xmqk5ll5qh0fydxk4n22qx3k3w6drfgq5vn2ks7z8glwgky5h0k")))

(define-public crate-parallel-iterator-0.1.6 (c (n "parallel-iterator") (v "0.1.6") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0idf4v5l5h5i2m63x422spm4jl3q2xhbq5z2706i2i7drgacl0ln")))

