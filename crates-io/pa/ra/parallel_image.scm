(define-module (crates-io pa ra parallel_image) #:use-module (crates-io))

(define-public crate-parallel_image-0.1.0 (c (n "parallel_image") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "0k6ll2ypjbp1zpin27jah4xpr8zm5vyf6rqblicwawq88rrx8hkz") (y #t)))

(define-public crate-parallel_image-0.2.0 (c (n "parallel_image") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ravif") (r "^0.11") (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "0v8wgxsjmb89rxj5gb8wsnsz7qfiq4ij4ymlid8yd63c22177lg6") (f (quote (("asm" "ravif/asm")))) (y #t)))

(define-public crate-parallel_image-0.3.0 (c (n "parallel_image") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ravif") (r "^0.11") (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "10mhjkw0rigifas765qr1kz2k4dynjvb4pnbkgaxvncq0g2k8m3i") (f (quote (("asm" "ravif/asm")))) (y #t)))

(define-public crate-parallel_image-0.4.0 (c (n "parallel_image") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libwebp2") (r "^0.1") (f (quote ("download"))) (o #t) (d #t) (k 0)) (d (n "ravif") (r "^0.11") (o #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)))) (h "01ij0nfw5wc590ix071qh1nzir9frgn920xwam5x94xm621nzdkp") (y #t) (s 2) (e (quote (("webp" "dep:libwebp2" "dep:anyhow") ("avif" "dep:ravif" "dep:rgb" "dep:anyhow") ("asm" "ravif?/asm"))))))

(define-public crate-parallel_image-0.5.0 (c (n "parallel_image") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "webp_encoder") (r "^0.6") (d #t) (k 0)))) (h "0c9fr3cp55xwkl1fwwzw27hwj59zpzrw92sngvr96b7h5d82sn2x")))

