(define-module (crates-io pa ra parasail-rs) #:use-module (crates-io))

(define-public crate-parasail-rs-0.1.0 (c (n "parasail-rs") (v "0.1.0") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1f7xdkm33vpqpibvsfgqx8zzkrnvyqvgd5l9p3f4py3lfkzmcx1h")))

(define-public crate-parasail-rs-0.2.0 (c (n "parasail-rs") (v "0.2.0") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0cgcg7xsq3mjzk9lxgsm7qv6nzh8brj5dwr2rmr31i8zl2cjqvs5")))

(define-public crate-parasail-rs-0.2.1 (c (n "parasail-rs") (v "0.2.1") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "102288yqb15rx05m064rjbkfg2mbid7p4z24kwrw02xd6ihwvcqs")))

(define-public crate-parasail-rs-0.2.2 (c (n "parasail-rs") (v "0.2.2") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0sa1350dcbackk52iip0q9d88dya2n3ic9z1xdkf95x1mp7hvaag")))

(define-public crate-parasail-rs-0.2.3 (c (n "parasail-rs") (v "0.2.3") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "179whfdfagxd9r33qm87m4pd62wgip4amrd7ccqlh4xwiyplx2wn")))

(define-public crate-parasail-rs-0.3.0 (c (n "parasail-rs") (v "0.3.0") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1jia1qx0hz40zvhyb9jjlmhif1hqafxcyrwhb8ka4c7qd9lkr43i")))

(define-public crate-parasail-rs-0.4.0 (c (n "parasail-rs") (v "0.4.0") (d (list (d (n "libparasail-sys") (r "^0.1.2") (d #t) (k 0)))) (h "14qyafmblydh0rfqpnqx3cyqf8gb01qzpjshw7b80srfhzp9kqzk")))

(define-public crate-parasail-rs-0.5.0 (c (n "parasail-rs") (v "0.5.0") (d (list (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1vvmcgzza3bpb2hkmnwx1grdrv27b07bzih8kh0bppky0jc36wn8")))

(define-public crate-parasail-rs-0.5.1 (c (n "parasail-rs") (v "0.5.1") (d (list (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0wrlkzyn9p9c6hcmnsak2f3dlbg4dvjrkqw809ajjx4s0r8c6l5z")))

(define-public crate-parasail-rs-0.6.0 (c (n "parasail-rs") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0fjhpk2k8xajfrh5qv9fhgn4nx6kfgl12crlwjxdq892gspi1ybg")))

(define-public crate-parasail-rs-0.7.0 (c (n "parasail-rs") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "187y43ss8g4960i82vbn0j26j9b5a1swrp4pxmdi6xq6cd202g31")))

(define-public crate-parasail-rs-0.7.1 (c (n "parasail-rs") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0imxmj0hrg7xmq8nwb8vq59zd0w2lcvd9rpj84bvckx0yz69wgb5")))

(define-public crate-parasail-rs-0.7.2 (c (n "parasail-rs") (v "0.7.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libparasail-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0vjsqbkha9p3azdkxiq2w7zabj65a28f0b0gazhdavs0ssh3sv9i")))

(define-public crate-parasail-rs-0.7.3 (c (n "parasail-rs") (v "0.7.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libparasail-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0vj30g8137kd8hp10p2jjxw29b885chxzb84gmr8ricmnixq8k4b")))

