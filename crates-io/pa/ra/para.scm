(define-module (crates-io pa ra para) #:use-module (crates-io))

(define-public crate-para-0.1.0 (c (n "para") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "1dzzs52x7d5543jmrq4j1kk6idm36v0w2dslm45aafa3yqi6m1rf")))

(define-public crate-para-0.1.1 (c (n "para") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0n2xcx7jxgnl5hlyc2s1mdd7ggzgqvn935rycdfbvnhdvcjxb6i9")))

(define-public crate-para-0.1.2 (c (n "para") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0iga18ldzwwpv7dim5vvdlqyi8wszddgi60hwkp68j2n046yn4fk")))

(define-public crate-para-0.2.0 (c (n "para") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "1jqqx6myzdb3wx1cfa0rfyqgksac7nlmdpljfz18ysyz9h2mq06k")))

