(define-module (crates-io pa ra parameter) #:use-module (crates-io))

(define-public crate-parameter-0.1.0 (c (n "parameter") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.3.0") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.3") (d #t) (k 0)))) (h "1pg24klr547kz60zw4jpfmfybmqz1qpxw5dy20snfkkq4bqfbz7f")))

(define-public crate-parameter-0.1.1 (c (n "parameter") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1.3.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.3") (d #t) (k 0)))) (h "0kr7ix4hhz174m6mnvw9v3znlxfskca6lqc5igp0ndpam3xc26ph")))

(define-public crate-parameter-0.1.2 (c (n "parameter") (v "0.1.2") (d (list (d (n "arc-swap") (r "^1.3.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.3") (d #t) (k 0)))) (h "0hcvkixbpq69vmvncdxqfmkbp47ywjgfkkyzhjh1v4zvpz45y9v8")))

