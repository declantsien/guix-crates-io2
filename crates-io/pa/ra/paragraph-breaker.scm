(define-module (crates-io pa ra paragraph-breaker) #:use-module (crates-io))

(define-public crate-paragraph-breaker-0.1.0 (c (n "paragraph-breaker") (v "0.1.0") (h "0sly6wmhhq8jzpgbycpgp8irih0484p32sv291zfqi4l19gxl2qi")))

(define-public crate-paragraph-breaker-0.2.0 (c (n "paragraph-breaker") (v "0.2.0") (h "1pchxdsxa8si4b44mf0r9xdshibx879sbj67z2hq2gzfgxdc0l4n")))

(define-public crate-paragraph-breaker-0.2.1 (c (n "paragraph-breaker") (v "0.2.1") (h "1mlrnfyczpmffmh6678adnvy5f6bsbbvm84r90hd3piq6jqj2jvc")))

(define-public crate-paragraph-breaker-0.3.0 (c (n "paragraph-breaker") (v "0.3.0") (h "1hh951xp7znxax3s1v5i7yyflrv60jdiw4ds7jjvrb8hzrhbnfbb")))

(define-public crate-paragraph-breaker-0.3.1 (c (n "paragraph-breaker") (v "0.3.1") (h "1s0rnjk2hwpki0zfayadb80z8yw6yfs1s1adnzm0g1gwkhdgjy1x")))

(define-public crate-paragraph-breaker-0.3.2 (c (n "paragraph-breaker") (v "0.3.2") (h "14fvrj1anbpapj8zy0r4cq2idhzcpcrl296ygz7z8rxkpdj3kam1")))

(define-public crate-paragraph-breaker-0.3.3 (c (n "paragraph-breaker") (v "0.3.3") (h "0s6fnrcmznbkrncnrylr0ysbx6hm4hvr38dwjvirwn74gwlyp1hk")))

(define-public crate-paragraph-breaker-0.3.4 (c (n "paragraph-breaker") (v "0.3.4") (h "1b8pfm290kxl7zcqwakm3yf8q7nsgxv7qcb0jc3x4375vilgfyy3")))

(define-public crate-paragraph-breaker-0.3.5 (c (n "paragraph-breaker") (v "0.3.5") (h "1l487cw1jv9cpd94qdv8yrks6q4r7cs03javsrgx46dinsv86kzs")))

(define-public crate-paragraph-breaker-0.3.6 (c (n "paragraph-breaker") (v "0.3.6") (h "0jaz2z01i78c8104qwx62qyk0qk9c1d1f50mc2grfz9rg365j6ys")))

(define-public crate-paragraph-breaker-0.3.7 (c (n "paragraph-breaker") (v "0.3.7") (h "19xggsjaaissn5bgrx147ajkd7mkrf5h4v59sxxjqpvdpl819khr")))

(define-public crate-paragraph-breaker-0.3.8 (c (n "paragraph-breaker") (v "0.3.8") (h "0hrpljl3bxri2k32ss4d3rkzxkiscywwgyf00xaad3j52bz160qw")))

(define-public crate-paragraph-breaker-0.4.0 (c (n "paragraph-breaker") (v "0.4.0") (h "12wpdkcgpaczwsylad5mi9q0fzgyck4mar8vb7730j8ig8kiq30i")))

(define-public crate-paragraph-breaker-0.4.1 (c (n "paragraph-breaker") (v "0.4.1") (h "01whgg1iicx33b16xnf0jz651igwsn36xbz2fk99nqnfixiz5i17")))

(define-public crate-paragraph-breaker-0.4.2 (c (n "paragraph-breaker") (v "0.4.2") (h "1p394hsi3flr74pvablpbaz1r0asj6z53vip5kprl3h2y7asxpdv")))

(define-public crate-paragraph-breaker-0.4.3 (c (n "paragraph-breaker") (v "0.4.3") (h "1zxdq7cbbsipkfb809a9g0j6ncb0cd4mk2pmjc0ws2n66f9g40by")))

(define-public crate-paragraph-breaker-0.4.4 (c (n "paragraph-breaker") (v "0.4.4") (h "1lvkzm6jhpqfnsxryh73ka7rq65yfyrkpy83x49f4x6rafy6acyz")))

