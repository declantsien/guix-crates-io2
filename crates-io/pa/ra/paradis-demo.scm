(define-module (crates-io pa ra paradis-demo) #:use-module (crates-io))

(define-public crate-paradis-demo-0.1.0 (c (n "paradis-demo") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "paradis-core") (r "^0.1.0") (d #t) (k 0)))) (h "18pmsy67gbzjfa08g6hwl7gcsv6ksqppir3pfahq9jpap950akzn")))

