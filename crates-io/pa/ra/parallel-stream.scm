(define-module (crates-io pa ra parallel-stream) #:use-module (crates-io))

(define-public crate-parallel-stream-0.0.0 (c (n "parallel-stream") (v "0.0.0") (h "1q2zzrim0hng6qa5vnb4a7qg8dl09qw6iqdgk3x5yvb486f4zrxz")))

(define-public crate-parallel-stream-1.0.0 (c (n "parallel-stream") (v "1.0.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "12haqki1xqgiz9q0qgig5hlk04nlap47wp0y1mazbdamp65b2i61")))

(define-public crate-parallel-stream-1.0.1 (c (n "parallel-stream") (v "1.0.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "19dfdbrv5p3mnjagi5bbwgwvybb32jrpfylagv9cq8sdm62686pn")))

(define-public crate-parallel-stream-2.0.0 (c (n "parallel-stream") (v "2.0.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "167qcv2rp9kifq4w4x9rfhjqrc92g2gh4ywr5vv04wcsp11cacr6")))

(define-public crate-parallel-stream-2.1.0 (c (n "parallel-stream") (v "2.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "1ix1c0zvv2x45cp99ybgk826yk5jg28v4nxzbf4d14m7cvzpaabi")))

(define-public crate-parallel-stream-2.1.1 (c (n "parallel-stream") (v "2.1.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "07qgk71f1yakidvv0hw736hil7b92bhg7rpin9nbxv28r8rkk43k")))

(define-public crate-parallel-stream-2.1.2 (c (n "parallel-stream") (v "2.1.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)))) (h "0j1hd64llawddmmqqb5fishsqx53523p8s2kiwsw8617m15sk62n")))

(define-public crate-parallel-stream-2.1.3 (c (n "parallel-stream") (v "2.1.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)))) (h "1671xnfn6jpz6nkmpv6nqajzczj3kvkaipa2djligfj19w4i651c")))

