(define-module (crates-io pa ra parallel-getter) #:use-module (crates-io))

(define-public crate-parallel-getter-0.1.0 (c (n "parallel-getter") (v "0.1.0") (d (list (d (n "numtoa") (r "^0.2.3") (d #t) (k 0)) (d (n "progress-streams") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)))) (h "0fvjnxsjpj7q659gdpvm0z8cxrql3h0mbag6r8jr44npslr7x3as") (y #t)))

(define-public crate-parallel-getter-0.2.0 (c (n "parallel-getter") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 0)) (d (n "progress-streams") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)))) (h "0dvk9y1bhdn9skhii88j2vm4ichnc3ss1fjlqhvd2y2inksmd4fy") (y #t)))

