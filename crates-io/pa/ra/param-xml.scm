(define-module (crates-io pa ra param-xml) #:use-module (crates-io))

(define-public crate-param-xml-1.0.0 (c (n "param-xml") (v "1.0.0") (d (list (d (n "prc-rs") (r "^1.2") (f (quote ("xml"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zissyz09yn0749nmr40p07qmia6aljr93mjm3f304a42k0hvm7m")))

(define-public crate-param-xml-1.1.0 (c (n "param-xml") (v "1.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prc-rs") (r "^1.4") (f (quote ("xml-feat"))) (d #t) (k 0)))) (h "0djd09kqvqz15gbpgkln738c9fbrg9hs3v94d9brda90414c5a0d")))

