(define-module (crates-io pa ra parametrics) #:use-module (crates-io))

(define-public crate-parametrics-0.1.0 (c (n "parametrics") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wxmg7cq3bp4zbw57xdcj2zbz5s6gczznkw1yfvgzzbwn9paxrd3")))

(define-public crate-parametrics-1.0.0 (c (n "parametrics") (v "1.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rpf3zfjky5nbczbgskcgn111zxh1xhy3png1x8hzdzxsfy35c2h")))

(define-public crate-parametrics-1.0.1 (c (n "parametrics") (v "1.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pp57mshnvm36a2swy9spvq4k8szrvyxkkyg5mw60f7ylbvn82li")))

(define-public crate-parametrics-1.0.2 (c (n "parametrics") (v "1.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bzpk5ir7mhb0w8l9rjr6dq7xppsxmg8k8pg1qx8lpi43wgq27lz")))

(define-public crate-parametrics-1.0.3 (c (n "parametrics") (v "1.0.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yfjsm2vbcrp1ghbaxrbyp89aipq3870c4p8hxxpcycscadzbwp8")))

(define-public crate-parametrics-1.0.4 (c (n "parametrics") (v "1.0.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gcyis52z8vp4c3cddg1h52qxmhfvyi0b2fk1nwd3lwp7rngipx4")))

(define-public crate-parametrics-1.1.0 (c (n "parametrics") (v "1.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xp6ka1agvdl1q15k9afwji6c5w0cx6gkm85vbaridwqxwczqfz2")))

(define-public crate-parametrics-1.2.0 (c (n "parametrics") (v "1.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rg396ldsppxsv2hjw6ns5ha0wjg4jiiw329lwjx0z0x6lgi3dmb")))

(define-public crate-parametrics-1.2.1 (c (n "parametrics") (v "1.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06gcbybbrya6dq03gz53qsqgjnb6cl9gay95adiga7jcj4zcqs6q")))

(define-public crate-parametrics-1.3.0 (c (n "parametrics") (v "1.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "095cmhhqw5k8h5i4bai00cnyrqcn52nfzn64jp8q3qpyl1zl62ah")))

(define-public crate-parametrics-1.4.0 (c (n "parametrics") (v "1.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gyypnfqfj65a0syzh25cn676w526rc0bwq1q9asag6lpnzlf7b6")))

(define-public crate-parametrics-1.5.0 (c (n "parametrics") (v "1.5.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "089f0vldraxf7nl7s6admnih7a875bddhqkn2z3kim01309vzpps")))

