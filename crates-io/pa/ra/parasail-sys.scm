(define-module (crates-io pa ra parasail-sys) #:use-module (crates-io))

(define-public crate-parasail-sys-0.1.0 (c (n "parasail-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05h1p967w1k39gayqs1bqy975f8q25ryxj16gf2l50ql3l3jlhh2")))

(define-public crate-parasail-sys-0.1.1 (c (n "parasail-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07inqka1xi02wy0hkl37lavy5pnpl2l6kgdpczg9l820w25rk599")))

(define-public crate-parasail-sys-0.2.0 (c (n "parasail-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08yr75sq9da1ga7m5ym3ma5zfww8zadin0ymffciskl2xcljl0d6")))

(define-public crate-parasail-sys-0.2.1 (c (n "parasail-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02x8krqp4z40iyx999day0yiy4shbpl493i9ln7lfra1vsq8k6a6")))

(define-public crate-parasail-sys-0.2.3 (c (n "parasail-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hvg9lf1lyxw2wwzd26i49mwwv1gkb51y06ybcwwbvdx8cnpc3mh")))

(define-public crate-parasail-sys-0.2.4 (c (n "parasail-sys") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01364n4dhzip0nmmv2zphmk8ryx3ynj3pwhwr6l3j5z5ckxf20dj")))

(define-public crate-parasail-sys-0.2.5 (c (n "parasail-sys") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "156fwfdbcfpzmx4k274jqdp8jzmllmdnhd1ibnh7kgd9cp7ni6ac")))

