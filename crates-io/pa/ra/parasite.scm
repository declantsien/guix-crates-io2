(define-module (crates-io pa ra parasite) #:use-module (crates-io))

(define-public crate-parasite-0.1.0 (c (n "parasite") (v "0.1.0") (h "10vp46ixhi62gdqkasx95i2bfq67fxzlrh763bhmz8cfz6nx14c2")))

(define-public crate-parasite-0.1.1 (c (n "parasite") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0zbpb2r44x2kinjjvp9w6kwkgzlxd73xmhxvj36knzr65r64q4i7") (f (quote (("default" "panic" "logs")))) (s 2) (e (quote (("panic" "dep:chrono" "dep:widestring") ("logs" "dep:simplelog"))))))

