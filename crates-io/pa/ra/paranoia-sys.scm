(define-module (crates-io pa ra paranoia-sys) #:use-module (crates-io))

(define-public crate-paranoia-sys-0.1.0 (c (n "paranoia-sys") (v "0.1.0") (h "1shfa342zgn66w3kqgzn6b34ccalh2yq6ywvq8i3xdf2499f5ldn")))

(define-public crate-paranoia-sys-0.1.1 (c (n "paranoia-sys") (v "0.1.1") (h "1z9vimpnh6vk4gr1mpcxpkdjyv4fgb0q14r72ffyx423484vmpyz")))

