(define-module (crates-io pa ra parapet) #:use-module (crates-io))

(define-public crate-parapet-0.1.0 (c (n "parapet") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.3.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1rg9jiyjs4mv7mmmkil03w73pjbsxfva6rf9qgdy07sw9x6lg9jq")))

(define-public crate-parapet-0.1.1 (c (n "parapet") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.11") (d #t) (k 0)) (d (n "dot") (r "^0.1") (d #t) (k 0)) (d (n "graphsearch") (r "^0.6") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "protocol") (r "^0.1.9") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "slab") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "06zpb31pki7wgwqxfvjl3vk6f39nbz1j4ipfmjj42fcyqxq1j3jj")))

