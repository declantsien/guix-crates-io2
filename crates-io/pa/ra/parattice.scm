(define-module (crates-io pa ra parattice) #:use-module (crates-io))

(define-public crate-parattice-0.2.1 (c (n "parattice") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1rq112algr65c0rfp8f7lx81p4aky5gisbly11ay7g6dp2a26air")))

(define-public crate-parattice-0.2.2 (c (n "parattice") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "12n5v9yqcwhl7b56nw7y99psrp3drxancjycc9nlwndf4i14ylaz")))

