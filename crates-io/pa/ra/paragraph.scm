(define-module (crates-io pa ra paragraph) #:use-module (crates-io))

(define-public crate-paragraph-0.0.1 (c (n "paragraph") (v "0.0.1") (h "1q5zil7nk05mmhccjwn4sbpzpbsbbx9l6fiizjj42rdhchqs1i27")))

(define-public crate-paragraph-0.0.2 (c (n "paragraph") (v "0.0.2") (h "0m1sd5nx7b3x64jw13n2an4ia62lr3xh0k8xgw0bz9qwdx2r0sls")))

