(define-module (crates-io pa ra paranoia) #:use-module (crates-io))

(define-public crate-paranoia-0.1.0 (c (n "paranoia") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.83") (d #t) (k 0)) (d (n "paranoia-caller") (r "^0.1.0") (d #t) (k 0)) (d (n "paranoia-sys") (r "^0.1.0") (d #t) (k 0)))) (h "07yzazs3x9vm7n3884fq11fz2x02d9gcnnjc5k4q58n5zcklyh3r")))

(define-public crate-paranoia-0.1.1 (c (n "paranoia") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.83") (d #t) (k 0)) (d (n "paranoia-caller") (r "^0.1.0") (d #t) (k 0)) (d (n "paranoia-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0npc2bdhyvjl9xdwk32kdxxy2a42dbx1gdg1b6g0lj45man76nnf")))

(define-public crate-paranoia-0.1.2 (c (n "paranoia") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.83") (d #t) (k 0)) (d (n "paranoia-caller") (r "^0.1.0") (d #t) (k 0)) (d (n "paranoia-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0dzvwzd6fgy6qsqj2343azc3zjkiaswbpnq8fskm8p8ciy9rayb3")))

(define-public crate-paranoia-0.1.3 (c (n "paranoia") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.83") (d #t) (k 0)) (d (n "paranoia-caller") (r "^0.1.0") (d #t) (k 0)) (d (n "paranoia-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0z6sdysn7srwbk1pqg0p800h6biglpa65zzij54jz0pbmxqpj4xi")))

(define-public crate-paranoia-0.1.4 (c (n "paranoia") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.83") (d #t) (k 0)) (d (n "paranoia-caller") (r "^0.1.0") (d #t) (k 0)) (d (n "paranoia-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0d1426x1z4dvzzwpndayz81hskpz6zb6yd89iqpw7q2bsdphwiks")))

