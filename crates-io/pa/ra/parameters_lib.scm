(define-module (crates-io pa ra parameters_lib) #:use-module (crates-io))

(define-public crate-parameters_lib-0.1.0-dev.1 (c (n "parameters_lib") (v "0.1.0-dev.1") (d (list (d (n "regex") (r "^1.5.0") (d #t) (k 0)))) (h "1discfhlskm0khwrw492pw38hdszdhfb9cmad607mzzpl5l41m6w")))

(define-public crate-parameters_lib-0.1.0-dev.2 (c (n "parameters_lib") (v "0.1.0-dev.2") (d (list (d (n "regex") (r "^1.5.0") (d #t) (k 0)))) (h "1pp5hgw86mndmxwbip6xfv36znqajily77cfrfqnkgfg827yz6sz")))

(define-public crate-parameters_lib-0.1.0 (c (n "parameters_lib") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.0") (d #t) (k 0)))) (h "01hqh51mhlrjw1zr1xhiqha3z6cmhv887kirb8vk3z6jyi0blwrw")))

(define-public crate-parameters_lib-0.2.2 (c (n "parameters_lib") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1hzj56y8402hxxj5y0k76nd0gd3gnyq26bfkqp14w42j7z5kpvdw")))

