(define-module (crates-io pa ra parasol-db) #:use-module (crates-io))

(define-public crate-parasol-db-0.1.0 (c (n "parasol-db") (v "0.1.0") (h "0azbah7wp4l8grcqjxpq206jz030f3hl9v8lmjnayc65a95kbf47")))

(define-public crate-parasol-db-0.1.1 (c (n "parasol-db") (v "0.1.1") (h "1hwyfnkg03akyn8zcc2grd4blpppv472j6wf6a0319d0kzw4w65q")))

(define-public crate-parasol-db-0.1.2 (c (n "parasol-db") (v "0.1.2") (h "0b7yw9nzh28njcxrk88wlvby088k25mya0xivixcsllad544q426")))

(define-public crate-parasol-db-0.1.3 (c (n "parasol-db") (v "0.1.3") (h "0bkparg21qhww4wagqrsh7mlm6z97gkh258ra22f5smjmy15d14b")))

(define-public crate-parasol-db-0.1.4 (c (n "parasol-db") (v "0.1.4") (h "0gn5np96npm7j5krxbccr0d6arhlx6wsfwln7fz35jwcz5m8cjrc")))

(define-public crate-parasol-db-0.1.5 (c (n "parasol-db") (v "0.1.5") (h "1fg997y1hlbg02nq3qxz13pbrc920sc1gz8gjxp4wa615ilq2zck")))

(define-public crate-parasol-db-0.1.6 (c (n "parasol-db") (v "0.1.6") (h "1mhw4nfj5r4gxq0p6bczgcqzy7p64d12qi36fjr1qyf8lw54j5dd")))

(define-public crate-parasol-db-0.1.7 (c (n "parasol-db") (v "0.1.7") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)))) (h "0afg43y7rg36djly84960mdr3yjvxaqxsgkhkxi9qm295amx6b1r")))

(define-public crate-parasol-db-0.1.8 (c (n "parasol-db") (v "0.1.8") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)))) (h "1h2l38blhhpzp7rsyx9sw2bpizy5krcvhk8ghlqq7a755y7yiyfq")))

(define-public crate-parasol-db-0.1.9 (c (n "parasol-db") (v "0.1.9") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)))) (h "1rqqx0d2j3f0gpsqv6rjisb4pa86xlswkhwlr4m6zknr134qqpp3")))

(define-public crate-parasol-db-0.1.10 (c (n "parasol-db") (v "0.1.10") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)))) (h "0s862dv1lvbsdbc639sfsqava9s0632wc6svb5bxymn3h2qnghl8")))

