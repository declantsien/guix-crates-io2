(define-module (crates-io pa ra param_builder) #:use-module (crates-io))

(define-public crate-param_builder-0.1.0 (c (n "param_builder") (v "0.1.0") (d (list (d (n "tokio-postgres") (r "^0.7.7") (d #t) (k 0)))) (h "1gkspvdw7pp2gmbca4kpbpxr8fky7sdfd0irfn1zri7xycr1m0w8")))

(define-public crate-param_builder-0.1.1 (c (n "param_builder") (v "0.1.1") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "1b0gxm4ix9ysmlik1z82dzq8wb0s4qghgw201364zxl7v40jw5jv")))

(define-public crate-param_builder-0.1.2 (c (n "param_builder") (v "0.1.2") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "0dbdzr86r7mgj7lar0ygw90grigzr62v7ydiqxmpgmwlgvh44w8w")))

(define-public crate-param_builder-0.1.3 (c (n "param_builder") (v "0.1.3") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "11s56facjh84s6png9888218r8212n4ksv1082qixqrfa4cb1z95")))

(define-public crate-param_builder-0.1.4 (c (n "param_builder") (v "0.1.4") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "0sm23y39zkhh1fcl06y1xdgj0mhb09wxw76vzksfx2wjjanwjqgi")))

(define-public crate-param_builder-0.1.5 (c (n "param_builder") (v "0.1.5") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "0fdq8x2lffjfpd3wmr5415kbj66lgamas110mfxz7ns7nc9kxspd")))

