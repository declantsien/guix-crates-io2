(define-module (crates-io pa ra parallel-sh) #:use-module (crates-io))

(define-public crate-parallel-sh-0.1.0 (c (n "parallel-sh") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "0lbl6i5m88cpxb092r5js2yvi48cdwn2dw7m3jk6iwjwp3nqgyhi")))

(define-public crate-parallel-sh-0.1.1 (c (n "parallel-sh") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "1bllnykp810fiwnzwm0msnrrplms1c0g7q105lg2nx6xwdmw5xyf")))

(define-public crate-parallel-sh-0.1.2 (c (n "parallel-sh") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "16xzmj8i419b1by66kh7xgysqzkcd9zdxnmz7707l9x4hrxjl8x7")))

(define-public crate-parallel-sh-0.1.3 (c (n "parallel-sh") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "1ghmayawlx99qchx1nykym7mblsv5432ax4kg8lnhnll3l3idhn2")))

(define-public crate-parallel-sh-0.1.4 (c (n "parallel-sh") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "0942iyzdpn95g7hbmqbv16dx76rdg4rwsjg5i6973mq3d203jq71")))

(define-public crate-parallel-sh-0.1.6 (c (n "parallel-sh") (v "0.1.6") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1p36fmb1fihnab60m37yj0b239gg26ing7wq7db8ld9374567hy5")))

(define-public crate-parallel-sh-0.1.7 (c (n "parallel-sh") (v "0.1.7") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0y7lnpys2arc54iz31kmvgranvw72q4ccm8z2z4m70lqhdvl68hw")))

(define-public crate-parallel-sh-0.1.8 (c (n "parallel-sh") (v "0.1.8") (d (list (d (n "clap") (r "^3") (f (quote ("std" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1x4pxjkkp137bimxgrfix24p6av24iig6jpc9wiy6qy1qxnqx2qc")))

(define-public crate-parallel-sh-0.1.9 (c (n "parallel-sh") (v "0.1.9") (d (list (d (n "clap") (r "^3") (f (quote ("std" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0mvv7y6v6a42xkb3y144wz15pvn0rl5v0hfxs082rjpvccjqlb1j")))

(define-public crate-parallel-sh-0.1.10 (c (n "parallel-sh") (v "0.1.10") (d (list (d (n "clap") (r "^3") (f (quote ("std" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "184b4bsghhf9fkgzjlmr2f8mz6bbmz8z2vckf9fybfakz0vs4sll")))

(define-public crate-parallel-sh-0.1.11 (c (n "parallel-sh") (v "0.1.11") (d (list (d (n "clap") (r "^4") (f (quote ("std" "help" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0yyqm1brz4fpfaprd0dyvca4fqbzjkgj4vgmzq4vaky9dxwv1s3h")))

(define-public crate-parallel-sh-0.1.12 (c (n "parallel-sh") (v "0.1.12") (d (list (d (n "clap") (r "^4") (f (quote ("std" "help" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1c5lfi8gqdygwfazywlkbjld8bc1rxf4hks8v2n93sjnjv2lwsh3")))

(define-public crate-parallel-sh-0.1.13 (c (n "parallel-sh") (v "0.1.13") (d (list (d (n "clap") (r "^4") (f (quote ("std" "help" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "130jkqhllv9ijihkwy48d0hnca0lcl162bfzkbn4gki17h1x6whp")))

(define-public crate-parallel-sh-0.1.14 (c (n "parallel-sh") (v "0.1.14") (d (list (d (n "clap") (r "^4") (f (quote ("std" "help" "deprecated"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1r24aixvqz1h5vc6i8fm83l09nkmfvfvx0cdbhdjwdxv65i8fzgs")))

