(define-module (crates-io pa ra parallel) #:use-module (crates-io))

(define-public crate-parallel-0.1.0 (c (n "parallel") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "127w5x6rv5vxkawj3f2vvzwcqaw9csww5cjq34x6yr9b4wr63byr") (y #t)))

(define-public crate-parallel-0.1.1 (c (n "parallel") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "1lxnwiira3kyl9lhd0sf752l70jq8dq0pyakfy3rm2qpszglkzn6") (y #t)))

(define-public crate-parallel-0.2.0 (c (n "parallel") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "0rk59qw7kspxlyg5ldw8k2jkfpw6mi5sqqb50s32gv8paq2v972f") (y #t)))

(define-public crate-parallel-0.2.1 (c (n "parallel") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "15k81w6bqkqv963611z4w07xpmrprssmqvsmg9v9ina11jzqnsza") (y #t)))

(define-public crate-parallel-0.2.2 (c (n "parallel") (v "0.2.2") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "1py18miwy1zrriga7bmrylpqaliwr0lcw5ijf1qpb02i0cwvp017") (y #t)))

(define-public crate-parallel-0.2.3 (c (n "parallel") (v "0.2.3") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "01ml0r519bykgdj35m5fmr9rmimhizrydf7c3xylly905b3a4flw") (y #t)))

(define-public crate-parallel-0.3.0 (c (n "parallel") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)))) (h "0s4rr0f9kiib2p44nabfy3b5jnq5zm7fph738irrypzw2vcxlhi5") (y #t)))

(define-public crate-parallel-0.4.1 (c (n "parallel") (v "0.4.1") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.0") (d #t) (k 0)))) (h "0941dmf2w8bmrp9z8pw4jsqg34zkk4a9w943hx0v5lcik5yzx6qk") (y #t)))

(define-public crate-parallel-0.4.2 (c (n "parallel") (v "0.4.2") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.3") (d #t) (k 0)))) (h "1i05zk2na1fjllchnix2v5msfccaxc6d9al27j0wndlfj7mk4rgx") (y #t)))

(define-public crate-parallel-0.5.0 (c (n "parallel") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.3") (d #t) (k 0)))) (h "05k8d9jsyrhxm5rrbv62pa2r0si3rl5xk1fshzffkiq5n2shxswy") (y #t)))

(define-public crate-parallel-0.6.0 (c (n "parallel") (v "0.6.0") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.3") (d #t) (k 0)))) (h "1ar1j0gyddvjj26ag32m25krh4xnbvgnwqyy0a91i3mav74ilrcx") (y #t)))

(define-public crate-parallel-0.6.1 (c (n "parallel") (v "0.6.1") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.3") (d #t) (k 0)))) (h "116g28p342km6r41nhnya2ddbyj1s5dx5glnqchxfl2swiwk58mf") (y #t)))

(define-public crate-parallel-0.6.2 (c (n "parallel") (v "0.6.2") (d (list (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1.3") (d #t) (k 0)))) (h "1p7iyjdycvyjxzgxlj1aifwj9s6ldz8nkpxfmlr9l4l8fhahmvi1") (y #t)))

(define-public crate-parallel-0.6.3 (c (n "parallel") (v "0.6.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1") (d #t) (k 0)))) (h "01camd12by3lrw6zynfg2kqrjwvmxavh5p75biyadc444wa4p36v") (y #t)))

(define-public crate-parallel-0.6.4 (c (n "parallel") (v "0.6.4") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1") (d #t) (k 0)))) (h "0xaczd6n4pg1xf84miw4vxbqfk89c637pwpmc6sqs75x3awfh5n6") (y #t)))

(define-public crate-parallel-0.6.5 (c (n "parallel") (v "0.6.5") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "permutate") (r "^0.1") (d #t) (k 0)))) (h "1r3fvr915mxczhbbin7ina3nipr0zjswqn8g0gi8rsg583q24f38") (y #t)))

(define-public crate-parallel-0.7.0 (c (n "parallel") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)))) (h "1v6mfjc162n90z47xpx3f5by8v921mqh7fws5fgn31biy9vjlawh") (y #t)))

(define-public crate-parallel-0.8.0 (c (n "parallel") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)))) (h "064dpiiyjajd55d83bazxz9s5wcmfp3cxia5lcf1xkfpy3cgd74z") (y #t)))

(define-public crate-parallel-0.9.0 (c (n "parallel") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1h09jdjhj5lngrmzxyc65fyaf8fpvbfyqmp6mdam5gd6rggj8awn") (y #t)))

(define-public crate-parallel-0.10.0 (c (n "parallel") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "148mgl3wh5bni860jim512fvaq3s86x8cyvjf71xrgy3554cphyc") (y #t)))

(define-public crate-parallel-0.10.1 (c (n "parallel") (v "0.10.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "17wkd0wsinn6v5bp2xfxs1b967c66dai41xqks3gdz213hq6ig74") (y #t)))

(define-public crate-parallel-0.10.2 (c (n "parallel") (v "0.10.2") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "184fh5gxm869q0s9l508q6g8a70nkqlw26kcdgjd9bmiq18i9fbn") (y #t)))

(define-public crate-parallel-0.10.3 (c (n "parallel") (v "0.10.3") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1bz0p84vjpfbnkga2kg679sgk9lqfn0fccbkqdlb39pfp8qgbzdr") (y #t)))

(define-public crate-parallel-0.10.4 (c (n "parallel") (v "0.10.4") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1anp244c00plf3vz2952pnswf98ridz3bhar7vrqnxavrsw2sh8q") (y #t)))

(define-public crate-parallel-0.10.5 (c (n "parallel") (v "0.10.5") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.0.5") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1n0kn8axdmsinqvgn0rzh86rl27kq778vlrvffcrij4sa9ji2g5f") (y #t)))

(define-public crate-parallel-0.10.6 (c (n "parallel") (v "0.10.6") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.0.5") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1kwh9zrgnifm0g48rrhzyam31bdzlp08lfyb8ikf7wxkfj4slmi2") (y #t)))

(define-public crate-parallel-0.10.7 (c (n "parallel") (v "0.10.7") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.0.5") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1hqx7skbssnkf1y3px96i8kgznmcas74illmdhjz2m06z3vg9sjc") (y #t)))

(define-public crate-parallel-0.11.0 (c (n "parallel") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.0.6") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1azsmp6zq12j0545gzajrp7kh8i995m93di85f7ayfnq356vw7wy") (y #t)))

(define-public crate-parallel-0.11.1 (c (n "parallel") (v "0.11.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "numtoa") (r "^0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "030rj989slhw22ikdbjw1ypx2gnqldy4qw25sbik04ffw0hkxx28") (y #t)))

(define-public crate-parallel-0.11.2 (c (n "parallel") (v "0.11.2") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5") (d #t) (k 0)) (d (n "numtoa") (r "^0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "0y8n0dm5bgdsvnbsl688ydymsn19q3bqzz8x6jgw0nb0kw4lpxl6") (y #t)))

(define-public crate-parallel-0.11.3 (c (n "parallel") (v "0.11.3") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "itoa") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5") (d #t) (k 0)) (d (n "numtoa") (r "^0.0") (d #t) (k 0)) (d (n "permutate") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "sys-info") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (d #t) (k 0)))) (h "1sc9zpcmfqs0im96pvbbxx3p5y637fgppdm0sqlmpr3zrb74xkxz") (y #t)))

