(define-module (crates-io pa ra parametrizer) #:use-module (crates-io))

(define-public crate-parametrizer-0.0.0 (c (n "parametrizer") (v "0.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "00nwg9gpc6ycvj26mxhbidn8hbdi51ppsc15izzm4gzw63m2q7kr")))

(define-public crate-parametrizer-0.0.1 (c (n "parametrizer") (v "0.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0wxm3imn02s4bbhrcihm630jrrdjzbzz9pqd6vl53ryx29lrwjbs")))

(define-public crate-parametrizer-1.0.0 (c (n "parametrizer") (v "1.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0hjzkd1ckyxw4rnyklr95waig3ym47z3vrgn0q5zpbk7hb98mjfr")))

(define-public crate-parametrizer-1.0.1 (c (n "parametrizer") (v "1.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1z8idgng705kfwgjb0ilm4dh4xbvvm4rm7dfbiyf8xqr5w2xrrxj")))

(define-public crate-parametrizer-1.1.0 (c (n "parametrizer") (v "1.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0fqpbcc0b372q901n89zwcy723q3cd3klll94qlzp3y01g56rnqd")))

(define-public crate-parametrizer-1.1.1 (c (n "parametrizer") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19zav3mgpsd61l1gbczwbvhx4h4b2bgpwjp314zk6ql579s5cch9")))

(define-public crate-parametrizer-1.1.2 (c (n "parametrizer") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0abd7j6j97klqgbn2gmpd5fpdzbwi5nd2gmprgm8jzy36lys1lvs")))

(define-public crate-parametrizer-1.2.0 (c (n "parametrizer") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07jwfx78m680z8z3a558y6y2g0i6zvpms2mmazkq8rh7nhdcvqy9")))

