(define-module (crates-io pa ra paradis-core) #:use-module (crates-io))

(define-public crate-paradis-core-0.0.1 (c (n "paradis-core") (v "0.0.1") (h "14ngw9pq4mfy2vnz8rdpbhsl6n2da7n7bdqbv47kpzyzwxw0np7k")))

(define-public crate-paradis-core-0.0.3 (c (n "paradis-core") (v "0.0.3") (h "0x7dq8qd4ldg8h34cn8vz3xqbd2g766yd1km5imvsbp7crj4lhzi")))

(define-public crate-paradis-core-0.1.0 (c (n "paradis-core") (v "0.1.0") (h "1yfkxalvgh4swyadhrr3fl3223161mshmp383jgdjbb3wh560nvi")))

