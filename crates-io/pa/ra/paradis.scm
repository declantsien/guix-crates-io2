(define-module (crates-io pa ra paradis) #:use-module (crates-io))

(define-public crate-paradis-0.0.1 (c (n "paradis") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 2)) (d (n "paradis-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1wjyl8navkvv0jjhbdm40p2kpkb3mgnzbhr0k03pq0hpvq62mc7c") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-paradis-0.0.2 (c (n "paradis") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 2)) (d (n "paradis-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1zisc86gfv25bmyb4biz90d54m2vgik06f2fikhyzp4d4hhaiq0k") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-paradis-0.0.3 (c (n "paradis") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 2)) (d (n "paradis-core") (r "^0.0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1c409p6cv4j3972c6bwz2afxi4ahb302v3gkk46vnr11zdz5n6k8") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-paradis-0.1.0 (c (n "paradis") (v "0.1.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 2)) (d (n "paradis-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "14ypwghkv6lb4z0jyza6nx1am5q3hng606b2h0wx8h3m44qa1cga") (s 2) (e (quote (("rayon" "dep:rayon"))))))

