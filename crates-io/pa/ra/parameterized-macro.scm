(define-module (crates-io pa ra parameterized-macro) #:use-module (crates-io))

(define-public crate-parameterized-macro-0.1.0 (c (n "parameterized-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0br3k2ivjnk94rvpjr1a5lrz3jwa8c4znch6bjq7bq96fvrldq5n")))

(define-public crate-parameterized-macro-0.1.1 (c (n "parameterized-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0nyign5n4gcbjpbfi0s8j6vm18am489wz6xwr58hi11m41xdy0ca") (f (quote (("enable_test_with_wildcard"))))))

(define-public crate-parameterized-macro-0.2.0 (c (n "parameterized-macro") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dazsnkkcp3cargv8dxbhvlf6bs553lanqvbxq77r1p1inpnb052")))

(define-public crate-parameterized-macro-0.3.0 (c (n "parameterized-macro") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0shvgflfdh0dmca1kn423c5vg7bxl3bhphl6jc81jfpck2wsbl57")))

(define-public crate-parameterized-macro-0.3.1 (c (n "parameterized-macro") (v "0.3.1") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1f5csj9f37khz50v92xj5glhlhs6il1i7xp03kz4kc5lpand2dac")))

(define-public crate-parameterized-macro-1.0.0 (c (n "parameterized-macro") (v "1.0.0") (d (list (d (n "indexmap") (r "=1.6.2") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1slwmpqvzx9vq9ywk4mjsc2s0hm65731n72kpim4a1qdnl3j8y94") (f (quote (("square-brackets-old-error-message"))))))

(define-public crate-parameterized-macro-1.0.1 (c (n "parameterized-macro") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.6.2") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "055cgxnq23k9mm4y2bg0fx7j1ajygd0aijsjsgrdmycvvc56xvqx") (f (quote (("square-brackets-old-error-message"))))))

(define-public crate-parameterized-macro-1.1.0 (c (n "parameterized-macro") (v "1.1.0") (d (list (d (n "indexmap") (r "^1.6.2") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15b3awhs9x5z0r35w58bbanpxp1w2rcr8kg77670mdmpsnvb7zw9") (f (quote (("square-brackets-old-error-message"))))))

(define-public crate-parameterized-macro-2.0.0 (c (n "parameterized-macro") (v "2.0.0") (d (list (d (n "indexmap") (r "^1.6.2") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1b3pjwm482248w1v9di6ak7pvsww4cgdzjgv9xy32ymbxnjvqx0k") (f (quote (("square-brackets-old-error-message"))))))

