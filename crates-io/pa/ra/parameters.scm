(define-module (crates-io pa ra parameters) #:use-module (crates-io))

(define-public crate-parameters-0.0.0 (c (n "parameters") (v "0.0.0") (h "0k347arirlcz4safjws1i491815kswzhf32z8vrnzg1ih4nafing")))

(define-public crate-parameters-0.1.0 (c (n "parameters") (v "0.1.0") (d (list (d (n "parameters_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0mly7qrf58z7zqkna1yw0qns0c82fr0haxnq8na9icsd1qvz1laa")))

(define-public crate-parameters-0.2.4 (c (n "parameters") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "parameters_lib") (r "^0.2") (d #t) (k 0)))) (h "0i9frwhxi74a087zpz4483w64w0cn24xivrzvpmcbs3l6wnpks2k")))

(define-public crate-parameters-0.2.7 (c (n "parameters") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "parameters_lib") (r "^0.2") (d #t) (k 0)))) (h "11bdnwjanjmfmxyqpljw3w017dz5v0qjcimivnaqiyfxzxdxgz0y")))

