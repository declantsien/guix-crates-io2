(define-module (crates-io pa ra parachute) #:use-module (crates-io))

(define-public crate-parachute-0.1.0 (c (n "parachute") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "159yzwls49s83lgcvbrck0li39b29sbcd979v8ry61nnz38350j7")))

(define-public crate-parachute-0.1.1 (c (n "parachute") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0m0lds94bq1jqfxqkcmggk14vw3nmvmy65qzwldpnay7sbzcnx0m")))

(define-public crate-parachute-0.1.2 (c (n "parachute") (v "0.1.2") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1l6hc36vizwxvzvkrzc8jalwpqz0gabpx8p6lcrkqpbzgw8fyx4a")))

(define-public crate-parachute-0.1.3 (c (n "parachute") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1xlpqzbpwi9czcyq3ylydrdlkrrxld1kj650dmy81sjm0jpwk1p6")))

(define-public crate-parachute-0.1.4 (c (n "parachute") (v "0.1.4") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0sjga4qa9kw80pzkmzd3zwc85k034433v5frvbw5gji235h93nf2")))

(define-public crate-parachute-0.1.5 (c (n "parachute") (v "0.1.5") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1wb04lz1sqy8hbh16i1yv4j3sya6k43yql06khv1zbp26l1q86i4")))

(define-public crate-parachute-0.1.6 (c (n "parachute") (v "0.1.6") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "01s7zrj8mlkdiry9g19wb4qxqv86sfi3lzh4xfdfga7mcfc822h0")))

(define-public crate-parachute-0.1.7 (c (n "parachute") (v "0.1.7") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1j7dzrm9k7bm4krg5f6q92q0xav3czg4m53ar0qh67vdj5lcghsi")))

(define-public crate-parachute-0.1.8 (c (n "parachute") (v "0.1.8") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "15r7kqjjxick5acl5isfjwkwji3qh5rrcckdw1ajfwhfl2immyqv")))

