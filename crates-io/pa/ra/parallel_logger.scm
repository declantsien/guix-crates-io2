(define-module (crates-io pa ra parallel_logger) #:use-module (crates-io))

(define-public crate-parallel_logger-0.2.0 (c (n "parallel_logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "1skw5ai6lv2g5di8y1jzg4fr0npjkh8l2hggahccrv1jz5yq7fcy")))

(define-public crate-parallel_logger-0.3.0 (c (n "parallel_logger") (v "0.3.0") (d (list (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serializable_log_record") (r "^0.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "0y8yz1ad5fwcdqgr2i2irjgsq6z9v2krlawc7hbhgixnz97ian0m")))

(define-public crate-parallel_logger-0.3.1 (c (n "parallel_logger") (v "0.3.1") (d (list (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serializable_log_record") (r "^0.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "0725dylnn5zwzcw2b9b7h186m76kk8lf1ccf1n180rrva1vi5jf1")))

(define-public crate-parallel_logger-0.4.0 (c (n "parallel_logger") (v "0.4.0") (d (list (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serializable_log_record") (r "^0.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "0vhs3x22f9gl7i8frwabcm13pp3wgmpan5k77wff6d3zzx3lf8ll")))

