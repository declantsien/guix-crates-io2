(define-module (crates-io pa ra paragraphs) #:use-module (crates-io))

(define-public crate-paragraphs-0.0.1 (c (n "paragraphs") (v "0.0.1") (h "1bzm5k59my39ij5c2sz25hfan5ianglq2sgb3vvw23hrvp6vss53")))

(define-public crate-paragraphs-0.1.0 (c (n "paragraphs") (v "0.1.0") (h "07n6davl4l81yh78sqibcld1q47vxc7svhp5ba3dn86qy88h7pjb")))

(define-public crate-paragraphs-0.1.1 (c (n "paragraphs") (v "0.1.1") (h "1qm70bxzgpc0fj8wp1sfjv0f07zqqra9ylyl2yw5bdm7hd4xy02d")))

(define-public crate-paragraphs-0.2.0 (c (n "paragraphs") (v "0.2.0") (h "1w6c71fzapdzf7302ld6dc6sv0qsy61b4dyaf5wc0pnn04h7c0wc")))

(define-public crate-paragraphs-0.2.1 (c (n "paragraphs") (v "0.2.1") (h "0zcg7z7gp2c1w3farwf06mxngkdl0x476h1y1dvyg16f9v51k90r")))

(define-public crate-paragraphs-0.2.2 (c (n "paragraphs") (v "0.2.2") (h "0w2gjcbsyjgl0a9bc37gnnrg5y004n2cvsda9a3vfdzhz0nqksx7")))

(define-public crate-paragraphs-0.3.0 (c (n "paragraphs") (v "0.3.0") (h "0bn0f00kvaar45gam8an9dqw610zp978q95lmiiya8l6smf9w4d4")))

(define-public crate-paragraphs-0.3.1 (c (n "paragraphs") (v "0.3.1") (h "02sh6j8abvqkmm49ycpsn9c5i5mcb4c7iyqxanrwq63i89nhdc6s")))

