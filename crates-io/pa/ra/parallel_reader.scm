(define-module (crates-io pa ra parallel_reader) #:use-module (crates-io))

(define-public crate-parallel_reader-0.1.0 (c (n "parallel_reader") (v "0.1.0") (d (list (d (n "ring") (r "^0.16.16") (d #t) (k 2)))) (h "1n6s0nabwjxjp409sffwjd8s30mgnypkyjy7am0hz2pwmc47amhx")))

(define-public crate-parallel_reader-0.1.1 (c (n "parallel_reader") (v "0.1.1") (d (list (d (n "ring") (r "^0.16.16") (d #t) (k 2)))) (h "0rbb6xs7376vi5zi6s98aqr9d4ac860d0jqnym3r3330ip7ml0ya")))

(define-public crate-parallel_reader-0.1.2 (c (n "parallel_reader") (v "0.1.2") (d (list (d (n "ring") (r "^0.16.16") (d #t) (k 2)))) (h "123rz4vg7i66sxwd4172h5hcz4n47arsdygxmbs8ninfxb0w2wfw")))

