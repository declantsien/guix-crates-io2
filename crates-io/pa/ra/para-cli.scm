(define-module (crates-io pa ra para-cli) #:use-module (crates-io))

(define-public crate-para-cli-0.1.0 (c (n "para-cli") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1bbzkvshdc0q7md3w6wiikj5cv4f2bq6sw3sc199zbkicrhywjbg")))

