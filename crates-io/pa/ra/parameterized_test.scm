(define-module (crates-io pa ra parameterized_test) #:use-module (crates-io))

(define-public crate-parameterized_test-0.1.0 (c (n "parameterized_test") (v "0.1.0") (h "1psi8y2ymwgnpdi9iqm67cgjahlvap540xsxz492dqzfnci57z7i")))

(define-public crate-parameterized_test-0.2.0 (c (n "parameterized_test") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0j42xahypxx578kn6mjbfsdjvz21i73ra46gyidr2bvffxn36r4y") (f (quote (("propagation" "anyhow") ("default" "propagation")))) (y #t)))

(define-public crate-parameterized_test-0.2.1 (c (n "parameterized_test") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0grkjvj5c5ab3k1fzv5mnmbpx4hhqh7dbvvamlf9vqr70pk591bi") (f (quote (("propagation" "anyhow") ("default" "propagation"))))))

