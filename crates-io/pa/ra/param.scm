(define-module (crates-io pa ra param) #:use-module (crates-io))

(define-public crate-param-0.1.0 (c (n "param") (v "0.1.0") (h "0fa0lrir959rbxcyx4bqdqx0v5ki9pjndxlr6d7wghh7cimg9z6x")))

(define-public crate-param-0.1.1 (c (n "param") (v "0.1.1") (h "1lxwngd7vbp8f5pn4j6vqswdd7ymismq08vrkq0rqrlij5f53qbn")))

(define-public crate-param-0.1.2 (c (n "param") (v "0.1.2") (h "0iw2cx9x92hz0k9fm0isypar1p3k64qbfh009gjnam6p7zqlm8id")))

