(define-module (crates-io pa ra paranoid-hash) #:use-module (crates-io))

(define-public crate-paranoid-hash-0.1.0 (c (n "paranoid-hash") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0hsml1700k11zg47w8rjvxixqqiwkdrjckirvzxxn3mj6hjnf2pj")))

(define-public crate-paranoid-hash-0.2.0 (c (n "paranoid-hash") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1mp6v4cjmfjivjiw5jgp81rvdc7myw5dxic1y90gg9cq8y2g2331")))

(define-public crate-paranoid-hash-0.3.0 (c (n "paranoid-hash") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "02l6mx0k3iy803j2larqbvwkw7zynz9h5jvprmks9qg7gzc2s519")))

(define-public crate-paranoid-hash-0.4.0 (c (n "paranoid-hash") (v "0.4.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "11w0w67b1in2ickn6h503vaxxczfjl2fz6vq32qib0s7wfx57yrd")))

(define-public crate-paranoid-hash-0.5.0 (c (n "paranoid-hash") (v "0.5.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0c1sfv049ihxz3pdxdryvqawfc3dy508rip5db9hgyak9f1amcw1")))

