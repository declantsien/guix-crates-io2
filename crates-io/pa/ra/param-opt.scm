(define-module (crates-io pa ra param-opt) #:use-module (crates-io))

(define-public crate-param-opt-0.1.0 (c (n "param-opt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1arza55fmb4ihahdcy32racgvh6wwl4b0857ji9y4r95s7klbyfw")))

(define-public crate-param-opt-0.2.0 (c (n "param-opt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1ays6d2di7bg84qyd2qr7gnsjds7hqdlrigg8m677z952r1w3jqj")))

