(define-module (crates-io pa lk palkia_macros) #:use-module (crates-io))

(define-public crate-palkia_macros-0.1.0 (c (n "palkia_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0px3j9rdaq9g7jwa0y39dfyw1kl5n2hsp0vx2281gvynfypgcbwf")))

(define-public crate-palkia_macros-0.13.0 (c (n "palkia_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0vi3v1cq1wyz34qsgfsw6298vnw3srl18ahg2zjx2lpcpn7f2brc")))

(define-public crate-palkia_macros-0.13.1 (c (n "palkia_macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0bcxpjh4k0336grlgycpjghmb87i6x5znfvxkag6m575wf8dcnpp")))

