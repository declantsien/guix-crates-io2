(define-module (crates-io pa ck packapp) #:use-module (crates-io))

(define-public crate-packapp-0.0.0 (c (n "packapp") (v "0.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0vjn46q3nrgwmhj6dix7cwlypd79kz3ccsagmjrjarf5s6q8sczx")))

(define-public crate-packapp-0.0.1 (c (n "packapp") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "16yrwb19fq7d6jfjl514x4v8dj3wlwv2mpq3vaphn4am4gsd9mqg")))

(define-public crate-packapp-0.0.2 (c (n "packapp") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plist") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "05cqbkwq0pjh3ihp37blajgxb3v3vcilc6893h4yf5x8ia51nkbw")))

(define-public crate-packapp-0.0.3 (c (n "packapp") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plist") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0v5y1rc9ii7knjz15qagpazvc1s31728vnggp3i261903d0hbw02")))

(define-public crate-packapp-0.0.4 (c (n "packapp") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plist") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0l65g0c8xvqcs2z8la4bvj20xm57n2sm1jqz4qrwg9c865d121d0")))

(define-public crate-packapp-0.0.5 (c (n "packapp") (v "0.0.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plist") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0r9filqyb2fvg4zj6b6f9cjbm9d0mq0jn899shbfzx08m2925avs")))

(define-public crate-packapp-0.1.0 (c (n "packapp") (v "0.1.0") (d (list (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "plist") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "08ix3s60ci4qadpdgscwh23cgmsysr374hpsdvniq9g71zq6wrkz")))

(define-public crate-packapp-0.1.1 (c (n "packapp") (v "0.1.1") (d (list (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "plist") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09frmgl976x64s6nrqy49jab11mn8p09432s9ndb81svc6999jd3")))

(define-public crate-packapp-0.1.2 (c (n "packapp") (v "0.1.2") (d (list (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "plist") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1amc3822b9xja5mc2cdryb4z3b7nlrik2i302irimcl65lmyj9hh")))

(define-public crate-packapp-0.1.3 (c (n "packapp") (v "0.1.3") (d (list (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "fs_extra") (r "~1.1") (d #t) (k 0)) (d (n "plist") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04i5gcgg0g2vnkz931nhs4zj5k50slszgmmpx0z0m6626w61g9gx")))

(define-public crate-packapp-0.2.0 (c (n "packapp") (v "0.2.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xczmxr8w2yg9d1qly234wxbg8d592ksa55i1kq5743m7dc2an7g")))

(define-public crate-packapp-0.3.0 (c (n "packapp") (v "0.3.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "12q6mk4564as41lfdgwdpz4szrj44w0yg7dwr23izrgf7bskrmvj")))

(define-public crate-packapp-0.3.1 (c (n "packapp") (v "0.3.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "069m5hyqnz8jmvcq51qbv0rnl189c3sn4w1rs2h42p5hk8csya2h")))

(define-public crate-packapp-0.4.0 (c (n "packapp") (v "0.4.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p9pk31kj3ryk8g039k6zl4rz60zc0fp6gqshdsbsc2d54k99xjw")))

