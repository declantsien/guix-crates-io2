(define-module (crates-io pa ck pack-it) #:use-module (crates-io))

(define-public crate-pack-it-0.1.0 (c (n "pack-it") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.9") (f (quote ("io_parquet" "io_parquet_compression"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qk5iq24hf4i465k0ah5m4nb1qwd119msa8m9k93dd3ybmm2l1g6")))

(define-public crate-pack-it-0.1.1 (c (n "pack-it") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.9") (f (quote ("io_parquet" "io_parquet_compression"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gxi1iv9w60fbgsswsivckfs70chkpdg0jcrls01sfcpffz2idqh")))

(define-public crate-pack-it-0.2.0 (c (n "pack-it") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.10") (f (quote ("io_parquet" "io_parquet_compression"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00mzi06h0dpfr0cz6k3f2p65abg0zfb2gw9x17sx7nvqiafrdbbr")))

(define-public crate-pack-it-0.2.1 (c (n "pack-it") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.12") (f (quote ("io_parquet" "io_parquet_compression"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pgcdslgmzvdhw1acb2sifla3gsxk5k2f7b165q2rbcxgv4dvrgb")))

(define-public crate-pack-it-0.2.2 (c (n "pack-it") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.16") (f (quote ("io_parquet" "io_parquet_compression"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0d8z2kfsc829rfnah095aknjvq2fryjzfpifsnlwxvmh5y1vs0kb")))

