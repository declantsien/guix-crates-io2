(define-module (crates-io pa ck packeteer) #:use-module (crates-io))

(define-public crate-packeteer-0.1.0 (c (n "packeteer") (v "0.1.0") (h "092j5fdy6bc5xd490md75j2zi8rmk2yzbmnxi53kwkfripgqif1b") (f (quote (("http1"))))))

(define-public crate-packeteer-0.1.1 (c (n "packeteer") (v "0.1.1") (h "1biy2ag1c8695fyfpyfvrmgb8b97bddhhrff95zflj99nbvq9ykr") (f (quote (("http1"))))))

(define-public crate-packeteer-0.1.2 (c (n "packeteer") (v "0.1.2") (h "17mv091dsb8vx24vy5h9zfhcx3yzxdg5jrdi74sjm49mh7glc77z") (f (quote (("http1"))))))

(define-public crate-packeteer-0.1.3 (c (n "packeteer") (v "0.1.3") (h "1iv3ppv2n47r0c3a8kmffbv9w9zx3z911zc13xgdhb2h5cc879mv") (f (quote (("http1"))))))

(define-public crate-packeteer-0.1.4 (c (n "packeteer") (v "0.1.4") (h "0995a6fhk6gr5rvh376ppzdfm46qzz7l6469zha6zgij6l94rjk0") (f (quote (("http1"))))))

(define-public crate-packeteer-0.2.0 (c (n "packeteer") (v "0.2.0") (h "0h186cy9yrzc2345bid0kj0z6sq0i3l9cp368xqdqd0y1jzl1p1l") (f (quote (("http1") ("gemini"))))))

(define-public crate-packeteer-0.2.1 (c (n "packeteer") (v "0.2.1") (h "0d52jznx6dc791drankjzfqvywhkr7s22i70v4vlklw5rxgbh001") (f (quote (("http1") ("gemini"))))))

(define-public crate-packeteer-0.2.2 (c (n "packeteer") (v "0.2.2") (h "0r4agqqp5fm9smwjz9a2f58n92czwbrldxx9gc4b162kdhwwkpvj") (f (quote (("http1") ("gemini"))))))

(define-public crate-packeteer-0.3.0 (c (n "packeteer") (v "0.3.0") (h "1jfhix7pdk6km6p1y2y3p0jbw101xmnlcicair7mgaw8zrpdfdgw") (f (quote (("http1") ("gemini") ("ftp"))))))

(define-public crate-packeteer-0.3.1 (c (n "packeteer") (v "0.3.1") (h "0sywyqdsqabwvyiz2hsx5nl7kznmrl83bnwcvammpaiqjn0xyx1s") (f (quote (("http1") ("gemini") ("ftp")))) (y #t)))

(define-public crate-packeteer-0.3.2 (c (n "packeteer") (v "0.3.2") (h "0585iqszi1hy6j2jyhkw700h4r0ndyqca7f9d61k0kbn0qzvc6ky") (f (quote (("http1") ("gemini") ("ftp"))))))

(define-public crate-packeteer-0.4.0 (c (n "packeteer") (v "0.4.0") (h "19a4gwda5nnhh2lq03drl1h5g4r9ikzxix67a20cls3adfyvr6wv") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

(define-public crate-packeteer-0.4.1 (c (n "packeteer") (v "0.4.1") (h "00nbb29l2a3262kw88lm6ysqbiqygnbh88rj7pbd0z2fwdjkv0mz") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

(define-public crate-packeteer-0.4.2 (c (n "packeteer") (v "0.4.2") (h "0wm0x62hi8szqak8nch1wr7kdfz56bqgdf33fpbbwr7kbyc4pfh3") (f (quote (("http1") ("gemini") ("ftp") ("dns")))) (y #t)))

(define-public crate-packeteer-0.4.3 (c (n "packeteer") (v "0.4.3") (h "1nj47f1vazld3vab0pajxms685hgp70plzn6k522jgag782vl8qs") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

(define-public crate-packeteer-0.5.0 (c (n "packeteer") (v "0.5.0") (h "1s1h1k9pjzgwmbdl3r6gr7iyccpmz4ik9wrqrh9hr2739v4p4ck9") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

(define-public crate-packeteer-0.5.1 (c (n "packeteer") (v "0.5.1") (h "05f81sdg04ahcxa3qxd0ynab4j1qqanb2n4n0w6hysaw771w4bsq") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

(define-public crate-packeteer-0.5.2 (c (n "packeteer") (v "0.5.2") (h "0ixnpi21w1zvzxlpyzvf2jxyqkcpn3cln2kfilg56pb3m5dx69al") (f (quote (("http1") ("gemini") ("ftp") ("dns"))))))

