(define-module (crates-io pa ck packed_str) #:use-module (crates-io))

(define-public crate-packed_str-0.2.0 (c (n "packed_str") (v "0.2.0") (h "0f7mdik2h61dgz04zd0gvvv8qw3vdbqa09x9xprijrjz0bja7i7r") (r "1.56")))

(define-public crate-packed_str-0.2.1 (c (n "packed_str") (v "0.2.1") (h "09bfiim23d493x7vagdjaaxa1i4im13hqn85qraqv2g44349as7j") (r "1.56")))

