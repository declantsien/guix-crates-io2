(define-module (crates-io pa ck package-lock-json-parser) #:use-module (crates-io))

(define-public crate-package-lock-json-parser-0.1.0 (c (n "package-lock-json-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01blsc44j1lkvprfqlzxrbp4p75ic9v9f37kgs58szh4cl88vp73")))

(define-public crate-package-lock-json-parser-0.1.1 (c (n "package-lock-json-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "15dq06g1d177l48j58db79z6i9ay46w3qzmbc18fn47ahh7918qh")))

(define-public crate-package-lock-json-parser-0.2.0 (c (n "package-lock-json-parser") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1vqkrb7xm2hmwbmywq9hqsl8s9akp8qdk3jalvkznpv6c1i0w79y")))

(define-public crate-package-lock-json-parser-0.3.0 (c (n "package-lock-json-parser") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "096q5w1ignispz50grzqk820np5sbrx0p0a4sd561h4f7jk5v874")))

(define-public crate-package-lock-json-parser-0.4.0 (c (n "package-lock-json-parser") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lw7yigfmwm61937pwbgfk03nqnqh9xzr2hbjwrwgyb7997x61w7")))

