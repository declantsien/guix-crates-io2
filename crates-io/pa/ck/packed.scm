(define-module (crates-io pa ck packed) #:use-module (crates-io))

(define-public crate-packed-0.2.0 (c (n "packed") (v "0.2.0") (h "08z3fln0qrbr3ligyhi5c6pyjaxcr2sjmpld9d55b4925xvnkgqk") (f (quote (("unstable"))))))

(define-public crate-packed-0.3.0 (c (n "packed") (v "0.3.0") (h "0nl041pszbvr50ajqxzk7p3pj14vb0vs6hxmsif3yflkmi7xa676") (f (quote (("unstable"))))))

(define-public crate-packed-0.4.0 (c (n "packed") (v "0.4.0") (h "13sh7xprwaq27k08pmp0gkpqysk7dlfasb7jnp4s6jzjd0wf92mk") (f (quote (("oibit"))))))

(define-public crate-packed-0.4.1 (c (n "packed") (v "0.4.1") (h "02jk05sbxk44qdkxwhnj8yqscjcp4y653py7w7yks31m5hgrzffz") (f (quote (("oibit"))))))

(define-public crate-packed-0.4.2 (c (n "packed") (v "0.4.2") (h "0d2k56ldnhiw4sd67hrm8svgxcjhw1l5nr6qmxiykghhmy0q28a1") (f (quote (("oibit"))))))

