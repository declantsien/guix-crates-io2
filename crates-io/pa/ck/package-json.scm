(define-module (crates-io pa ck package-json) #:use-module (crates-io))

(define-public crate-package-json-0.1.0 (c (n "package-json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "03pxz1vmi5124f5w50zhvci66im2k0wsblf8r5bblhq18m6bxa6h") (f (quote (("default"))))))

(define-public crate-package-json-0.2.0 (c (n "package-json") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0x383qwwhivc8kniy7kkdncy8gkxpmrdll53pbng89l78a5lia7b") (f (quote (("default"))))))

(define-public crate-package-json-0.3.0 (c (n "package-json") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "114r1hm7ja49q4kxb3jaxfrw94sjr7ssyy2gcwppvzxwdrkyjqzy") (f (quote (("default"))))))

(define-public crate-package-json-0.4.0 (c (n "package-json") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "08h9cmi2dqyh3cblafznylc1vjg9nl62hqbnqaabgbz6jnj6qd1h") (f (quote (("default"))))))

