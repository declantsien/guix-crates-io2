(define-module (crates-io pa ck pack_2048) #:use-module (crates-io))

(define-public crate-pack_2048-0.4.3 (c (n "pack_2048") (v "0.4.3") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1d1i784pvw2s6zq2flb6k4wd3fbxjdv6dw65159j2867m1l47kvr")))

(define-public crate-pack_2048-0.4.4 (c (n "pack_2048") (v "0.4.4") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0wbaix713q9f3mvmkqvl4bx342q4kssvcixk1jdxxr257xjgj21v")))

