(define-module (crates-io pa ck packattack) #:use-module (crates-io))

(define-public crate-packattack-0.1.0 (c (n "packattack") (v "0.1.0") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "bitreader_async") (r "^0.2.0") (d #t) (k 0)))) (h "1wpjhh0ny7aqyq63arxmb5y3nmks1l0lxghj7xcky8vqnhqlv4ka")))

