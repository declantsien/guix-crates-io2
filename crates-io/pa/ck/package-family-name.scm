(define-module (crates-io pa ck package-family-name) #:use-module (crates-io))

(define-public crate-package-family-name-1.0.0 (c (n "package-family-name") (v "1.0.0") (d (list (d (n "sha2") (r "^0.10") (k 0)))) (h "112myywplm3npddy2mjfx8slz9balxcxbvpvbyhq8r9pl4zr5ipj")))

(define-public crate-package-family-name-1.1.0 (c (n "package-family-name") (v "1.1.0") (d (list (d (n "fast32") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0s35spn6dc42sxhh82zag37maqhbgjsp42w1279lnnabwd70bcri")))

