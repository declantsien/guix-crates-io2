(define-module (crates-io pa ck packet_snooper) #:use-module (crates-io))

(define-public crate-packet_snooper-1.0.1 (c (n "packet_snooper") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.21") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1cbl6nrs160241jjdirjqqjlsq1k14ixh67gnak3fn7916gl91sj")))

(define-public crate-packet_snooper-1.0.2 (c (n "packet_snooper") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.21") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1adfh3hpl8prnzgsz705c3jbi760mpzg39ih6z0bj0199jm1kriq")))

