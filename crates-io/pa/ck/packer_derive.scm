(define-module (crates-io pa ck packer_derive) #:use-module (crates-io))

(define-public crate-packer_derive-0.2.0 (c (n "packer_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1q7vqxfb8dz72fnix0pg5c8fwsgyb542n4525r1z0bykbq7y0fix")))

(define-public crate-packer_derive-0.3.0 (c (n "packer_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1m68y68kic7bgx8rfcrjrh5lxk16pl3gvcxnj2lm88rbmxvq39c6")))

(define-public crate-packer_derive-0.4.0 (c (n "packer_derive") (v "0.4.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "01d44c989w43dds5b833d3r5jzjs5zskn05gsy7cz161w1shchdk") (f (quote (("ignore" "glob") ("default" "ignore"))))))

(define-public crate-packer_derive-0.5.0 (c (n "packer_derive") (v "0.5.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1zs0f4v41vjwld797cy36iqf4y5c85cadlf5a5p12cangyxpyk8c") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

(define-public crate-packer_derive-0.5.1 (c (n "packer_derive") (v "0.5.1") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "063qgh9fl7fjxijhkf29hy8wgkazlwz15mxqhqgccvc3maj11vyk") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

(define-public crate-packer_derive-0.5.2 (c (n "packer_derive") (v "0.5.2") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "035x3bwnifq5mrxjh3kbx7q4nbzimjjz2pgh3f6a3inrwlsy39ny") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

(define-public crate-packer_derive-0.5.3 (c (n "packer_derive") (v "0.5.3") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0fpbsmbq7vzqlbx17xjbd6gic35a0xcxcxqh7wlqm00kvidpqs6b") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

(define-public crate-packer_derive-0.5.5 (c (n "packer_derive") (v "0.5.5") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "19bamirb978w3bpppq8flzknfmrabbrj6nx5hchby56drrv3815q") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

(define-public crate-packer_derive-0.5.6 (c (n "packer_derive") (v "0.5.6") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0xrh4xlpyq3c1cdlclxkc8az74n6sy85a1qdd9mhwsxklkzrcc8x") (f (quote (("ignore" "glob") ("default" "ignore") ("always_pack"))))))

