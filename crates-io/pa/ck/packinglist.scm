(define-module (crates-io pa ck packinglist) #:use-module (crates-io))

(define-public crate-packinglist-0.1.0 (c (n "packinglist") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1g5v2sfg5bd152nx4w3gii6fbahxpspzmh9xfhbvh7yb682y5i8f")))

(define-public crate-packinglist-0.2.0 (c (n "packinglist") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lp4rfnc48qrknkbcqb9w5yr2g2f7n5lwyfnwlhj5vwb0symmksw") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde-std" "dep:serde" "serde/default") ("serde-nostd" "dep:serde"))))))

(define-public crate-packinglist-0.3.0 (c (n "packinglist") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w1c2s9nij3b29f4ahi1n1nrp54xljwwadmmb20ynrfdbkjmj05n") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde-std" "dep:serde" "serde/default") ("serde-nostd" "dep:serde"))))))

