(define-module (crates-io pa ck packed_bools) #:use-module (crates-io))

(define-public crate-packed_bools-0.1.0 (c (n "packed_bools") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gvq3jfm47z6v6vkg1dfgzmf7gl9nsib3agz8czyab36yxxnqmbr")))

