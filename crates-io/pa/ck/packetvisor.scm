(define-module (crates-io pa ck packetvisor) #:use-module (crates-io))

(define-public crate-packetvisor-1.0.0 (c (n "packetvisor") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "clap") (r "^4.3.8") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)))) (h "13n3hqw4vzw8j3szxsgvk9l9mn4v1ljf0gxf51wi84is55g0jpyv")))

(define-public crate-packetvisor-1.0.1 (c (n "packetvisor") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "clap") (r "^4.3.8") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)))) (h "1l8xbspndnv0n9vxls6mskijq0h980pb6vf38fjjasnz64ab9x3z")))

