(define-module (crates-io pa ck packing) #:use-module (crates-io))

(define-public crate-packing-0.1.0 (c (n "packing") (v "0.1.0") (d (list (d (n "packing_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1fbwbiih5m2y00hkb8zkzd98afiwhvlik4fx7k697k4zsmf6zijl")))

(define-public crate-packing-0.2.0 (c (n "packing") (v "0.2.0") (d (list (d (n "packing_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1kdy7wqrj049azzn2h0g56d0sg6cvi4s317643703a212q8g6i22")))

