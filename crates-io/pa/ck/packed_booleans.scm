(define-module (crates-io pa ck packed_booleans) #:use-module (crates-io))

(define-public crate-packed_booleans-0.1.0 (c (n "packed_booleans") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "1hwiryhhi4lbblcxyrgmhly4f4ssd6yxrrsx5108359mm57mj4vx") (y #t)))

(define-public crate-packed_booleans-0.1.1 (c (n "packed_booleans") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "15jklfhm5zm22mnn8ia6f8rx1gpmivpsacr881azzjx230lnw449") (y #t)))

(define-public crate-packed_booleans-0.1.2 (c (n "packed_booleans") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "0s5w744zvqv5hsrsn9fzzy5h67whx02y7scp7ci49ak3xjpfvlrb") (y #t)))

(define-public crate-packed_booleans-0.1.3 (c (n "packed_booleans") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "0zm21lsbw1y1x30fvhhza6l0y6w1w2v7yfwy6rhiwz7g340fjksh") (y #t)))

(define-public crate-packed_booleans-0.2.0 (c (n "packed_booleans") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "1wva1g4w765f5kkybb4l596bqbfiz8glnyzd93ybk6rp4p6hq159")))

(define-public crate-packed_booleans-0.2.1 (c (n "packed_booleans") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "07zsi1bri7y378hz6qzwf0h5li5jmb9igi6gsqqd8sf5pccfnkzs")))

(define-public crate-packed_booleans-0.3.0 (c (n "packed_booleans") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)))) (h "1kb5wsmb8wvpbchshkf76hp0188667wrb7sq0xk80554gz613ncw")))

