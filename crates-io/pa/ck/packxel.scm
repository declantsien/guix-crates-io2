(define-module (crates-io pa ck packxel) #:use-module (crates-io))

(define-public crate-packxel-0.0.1 (c (n "packxel") (v "0.0.1") (d (list (d (n "packxel-utils") (r "^0.1.0") (d #t) (k 0)))) (h "08gcyl33gh4janvbgygk921x438wz3gm4vjbqz0z067lgc2fy8sl")))

(define-public crate-packxel-0.0.2 (c (n "packxel") (v "0.0.2") (d (list (d (n "packxel-utils") (r "^0.1.1") (d #t) (k 0)))) (h "16i5z42zihlpmihyng4f4544dbym612k72f7qznfgh1r8cr79ibc")))

(define-public crate-packxel-0.1.0 (c (n "packxel") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "packxel-utils") (r "^0.2.0-beta") (f (quote ("full"))) (d #t) (k 0)))) (h "0xzgv2n18p1vyx11q2c8504jhjw5a3r9jsrls48cz4bhz70vafj2")))

