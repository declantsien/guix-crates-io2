(define-module (crates-io pa ck packed-uints) #:use-module (crates-io))

(define-public crate-packed-uints-0.1.0 (c (n "packed-uints") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "1zpli3p446dzc2cz0wlv9ynk51hk63kwi7xvzwl1xdy0f7hy35cn")))

(define-public crate-packed-uints-0.1.1 (c (n "packed-uints") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "0cpp6pvsx6w3q16vz3rivh3yvnz1rgdyiard4wpjpil8m72ywssr")))

(define-public crate-packed-uints-0.1.2 (c (n "packed-uints") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "0acl716zi3dxm05f6w9bsrl4jrg7wwbkx76j39ykwqqvggm3i2hb")))

(define-public crate-packed-uints-0.1.3 (c (n "packed-uints") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "0rh58l9i0rrjv2x2lasfv74sapw1lfa9g8sijj2y38yxqagf47gb")))

(define-public crate-packed-uints-0.1.4 (c (n "packed-uints") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "182ff9g82xhsc94am8cy84jpjqbf25l8m8pgmsb6l3x7685ywcq4")))

(define-public crate-packed-uints-0.1.5 (c (n "packed-uints") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "0aqacpb77qcn5k1q03sb64g6k473srcs0f91ffy2xbsbkwj9fvym")))

(define-public crate-packed-uints-0.1.6 (c (n "packed-uints") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "0m4p303gkw457gfi9acanxwa3ax64qskz4mblkn8vpv079a5854p")))

(define-public crate-packed-uints-0.1.7 (c (n "packed-uints") (v "0.1.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unthbuf") (r "^1.0") (d #t) (k 2)))) (h "04qwv9szf149vapkyx9c3ir0r7zj4lpbmzx1dc0j3illw4s72zpv")))

