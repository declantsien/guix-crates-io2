(define-module (crates-io pa ck packall) #:use-module (crates-io))

(define-public crate-packall-0.1.0 (c (n "packall") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1kykn725a2fbpivzniqj50wg0ir7cg502q9fljb9s21500p22wm2")))

