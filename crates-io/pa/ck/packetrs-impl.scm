(define-module (crates-io pa ck packetrs-impl) #:use-module (crates-io))

(define-public crate-packetrs-impl-0.1.0 (c (n "packetrs-impl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bit-cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x1qvykmjvv5z7hil6cmrdkbnf1n1h2ddvy4rmcycwpmiz52wpci")))

(define-public crate-packetrs-impl-0.2.0 (c (n "packetrs-impl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bit-cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03jf1615xi28lvy55s3qyc8zpgc775hki2c9c7i4cdr4a1qfnyy4")))

(define-public crate-packetrs-impl-0.3.0 (c (n "packetrs-impl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bit-cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hnzin13yg33764h1q4m19mls6p9lz5w043nky7j2f0xwfp5a0yl")))

(define-public crate-packetrs-impl-0.4.0 (c (n "packetrs-impl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bit-cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n0rbkb007l2yvr1nwxma0l5gsk4bvfd1brmqy9ggy3lgcijjlyn")))

(define-public crate-packetrs-impl-0.5.0 (c (n "packetrs-impl") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bit-cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11xdv13pj7fc8zhxxvkli1mv4va2ffi2divk6h3d7nlv5vk0fhc3")))

