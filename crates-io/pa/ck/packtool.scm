(define-module (crates-io pa ck packtool) #:use-module (crates-io))

(define-public crate-packtool-0.1.0 (c (n "packtool") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "packtool-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "18pkr26v7svv29cvkm9iv909ks9gcbn5b76zaf0kycxfxwf1fbhs")))

(define-public crate-packtool-0.2.0 (c (n "packtool") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "packtool-macro") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0lv3j7gi5fnpbl5xbmassnb29yg14pq4lrs49h9rvdiwzj4imx6g")))

(define-public crate-packtool-0.3.0 (c (n "packtool") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "packtool-macro") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0cmmv6gfzn712b3f3imvhxsil6i2f9lwqa8hpigm02spfn59w3cy")))

