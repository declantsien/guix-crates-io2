(define-module (crates-io pa ck packs-proc) #:use-module (crates-io))

(define-public crate-packs-proc-0.1.0 (c (n "packs-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (d #t) (k 0)))) (h "0i3l22149j2qmm2li0rwpbr1w6aymcvc36cm6l5brsry4b1f9fs6")))

(define-public crate-packs-proc-0.2.0 (c (n "packs-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (d #t) (k 0)))) (h "12payin8sjcijri8yj5fhh3nwz71bkyby36232yjdk65s5rh910g")))

