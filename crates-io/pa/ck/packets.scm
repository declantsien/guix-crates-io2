(define-module (crates-io pa ck packets) #:use-module (crates-io))

(define-public crate-packets-0.1.0 (c (n "packets") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "bincode") (r ">=1.3.1, <2.0.0") (d #t) (k 0)) (d (n "byteorder") (r ">=1.3.4, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r ">=0.3.16, <0.4.0") (d #t) (k 0)))) (h "1g5rrys2hvrhbil4nwrsjdc67f27d633gnhn0n1a0lkpzq3dik1n")))

