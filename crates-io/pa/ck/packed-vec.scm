(define-module (crates-io pa ck packed-vec) #:use-module (crates-io))

(define-public crate-packed-vec-0.0.1-alpha.1 (c (n "packed-vec") (v "0.0.1-alpha.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0n6wjp50w160gq94iasj80lh91lrwna33hzwc4g4xk1h5xjhlc6x")))

(define-public crate-packed-vec-0.0.1-alpha.2 (c (n "packed-vec") (v "0.0.1-alpha.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1m419fbc09h7mp5k4wjp9nlg9vxhbqlgcdrnfsshd8hj8mfnnr9p")))

(define-public crate-packed-vec-0.0.1-alpha.3 (c (n "packed-vec") (v "0.0.1-alpha.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0nr8h0g50l35k8l1hqsfv84sczg1sdf3lg7hhhl0i5zidfzfcd0b")))

(define-public crate-packed-vec-0.0.1-alpha.4 (c (n "packed-vec") (v "0.0.1-alpha.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "140vhr9lqmrvh1k12mvwgp7dlymj5bh4qjmnsx32gnvglbw6bm3h")))

(define-public crate-packed-vec-0.0.1-alpha.5 (c (n "packed-vec") (v "0.0.1-alpha.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0chi7a5l3vayydqrgfygng44pyq2ckb1akapqh3lbnykdix5q8rz")))

(define-public crate-packed-vec-0.0.1-alpha.6 (c (n "packed-vec") (v "0.0.1-alpha.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hapfcs8na1yn7zp0qcvxfiv1zfp20kxd0nmghps2ni6q8jik8my")))

(define-public crate-packed-vec-0.0.1-alpha.7 (c (n "packed-vec") (v "0.0.1-alpha.7") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xyy80a1pgr1ld4mjcidnp6cbqvd0j7yzqvg82mlx0grbyz3icmn") (y #t)))

(define-public crate-packed-vec-0.0.1-alpha.8 (c (n "packed-vec") (v "0.0.1-alpha.8") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "00jkxnggpb9pbk0zgjg0rkdgs42qhmryrs4pfjbbiv9xxk994cfx")))

(define-public crate-packed-vec-0.0.1-alpha.9 (c (n "packed-vec") (v "0.0.1-alpha.9") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1l44qfb1bvhx4lp8dkwprbk2ckmvr7qzsy1zl2v328k24n9vkpd9")))

(define-public crate-packed-vec-0.0.1-alpha.10 (c (n "packed-vec") (v "0.0.1-alpha.10") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ky557bfd6v92a20q4784mkad46fc2xx6zjxpwpbjzxlsl41384s")))

(define-public crate-packed-vec-0.0.1-alpha.11 (c (n "packed-vec") (v "0.0.1-alpha.11") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "prefix-varint") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0wsgdi6j6db5sg7mb7064hw19z56dlds48238dfdz0afc9gppz3j")))

(define-public crate-packed-vec-0.0.1-alpha.12 (c (n "packed-vec") (v "0.0.1-alpha.12") (d (list (d (n "prefix-varint") (r "^0.0.1-alpha.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lywiqi5y5j5ysq23rsdkf1ym838wrpvdi48gc1qz168sqn7kpk6")))

