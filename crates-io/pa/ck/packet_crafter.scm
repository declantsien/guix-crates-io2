(define-module (crates-io pa ck packet_crafter) #:use-module (crates-io))

(define-public crate-packet_crafter-0.1.0 (c (n "packet_crafter") (v "0.1.0") (d (list (d (n "add_getters_setters") (r "^1.1") (d #t) (k 0)))) (h "188z086csd7cr2p6cyv27q77f1s8wn39rw0ck66kl702mpsmsg9l")))

(define-public crate-packet_crafter-0.1.1 (c (n "packet_crafter") (v "0.1.1") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "0p5qqlyj0xxlh8zmcs2b7dgx2l7gcgh1szs9n39dnm86b7skzd87")))

(define-public crate-packet_crafter-0.1.2 (c (n "packet_crafter") (v "0.1.2") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "1m34sbnz8dy1pimzk16mbl4dpsh9s341w7iavrwzhqwnlsf82ap9")))

(define-public crate-packet_crafter-0.1.3 (c (n "packet_crafter") (v "0.1.3") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "0bnc8lbn15ifd89z64byx6hafxm57n0dc32vchyc3lp5zj350dx8")))

(define-public crate-packet_crafter-0.1.4 (c (n "packet_crafter") (v "0.1.4") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "0jzax0m1zpqgni0iknxzyaajn5zm4i6wjcvrdl7f2f9lhx3z1jkz")))

(define-public crate-packet_crafter-0.1.5 (c (n "packet_crafter") (v "0.1.5") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "12wrv8lim42l1sp2d0qnkkx2xg1ll8vgmi4xcw04n25k7rxn45fy")))

(define-public crate-packet_crafter-0.1.6 (c (n "packet_crafter") (v "0.1.6") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "1cn82ikbr0rvi9nagx77al424ads892hjz8aik9wg0big2if11a2")))

(define-public crate-packet_crafter-0.1.7 (c (n "packet_crafter") (v "0.1.7") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "06k1ggc45wg082cy5in8gg0mybgbqlv23ry9jkh4qw5lk8pgh9zn")))

(define-public crate-packet_crafter-0.2.0 (c (n "packet_crafter") (v "0.2.0") (d (list (d (n "add_getters_setters") (r "~1") (d #t) (k 0)))) (h "13wm3975v5s306m0bzrrxavv54mijdzcrgy5wrsj3hdw9ayn1bkj")))

