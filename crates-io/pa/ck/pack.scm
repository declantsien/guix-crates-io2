(define-module (crates-io pa ck pack) #:use-module (crates-io))

(define-public crate-pack-0.1.0 (c (n "pack") (v "0.1.0") (h "0aji7lb8kk6cw91m48gqp9lmcn053xwqh1yz3kpnh8d1j2n7rf7j")))

(define-public crate-pack-0.2.0 (c (n "pack") (v "0.2.0") (h "14ssl5d5mwhv2rky6z2b3k8bmzcmagscfc0snvyyp0jcagdqc4vk")))

(define-public crate-pack-0.3.0 (c (n "pack") (v "0.3.0") (h "0db5ql97zr2gn5jbb0nlflvf8hg1si27yadccphpgi561y2h91zw")))

(define-public crate-pack-0.4.0 (c (n "pack") (v "0.4.0") (h "1s60jb4j1idr50sgxf28j257icmab4bh9w054ps6jir9lgfri9hm")))

