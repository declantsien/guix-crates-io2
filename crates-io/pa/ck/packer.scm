(define-module (crates-io pa ck packer) #:use-module (crates-io))

(define-public crate-packer-0.1.0 (c (n "packer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1xn15wqdgbyrra53aszc096srvvhkz8ix8dy9qmcx4mwfjrd6qxl")))

(define-public crate-packer-0.2.0 (c (n "packer") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "packer_derive") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "1frrzhkp9kv9ivw2whvymfmazppl98qv2j3kdjddr3f5hmfsw2c1")))

(define-public crate-packer-0.3.0 (c (n "packer") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "packer_derive") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0yg1yvwdirlh0fh7sz33ww3kxmnf0h1ilma6r44saym9b4665q87")))

(define-public crate-packer-0.3.1 (c (n "packer") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "packer_derive") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0qasvkv1m310w450h3f78z4nrcbfmxfsn83hd1ihh32cg8rig9ax")))

(define-public crate-packer-0.4.0 (c (n "packer") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "packer_derive") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0dlr5w75w7n53s9gs2l41qgk8j7ahycawnq2xf3hq25rnpkajbxd") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore"))))))

(define-public crate-packer-0.4.1 (c (n "packer") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "packer_derive") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0lljb2d6cc9yps3cn8vddhbvcawm1vp4bwpv225aaphd35jzyb5h") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore"))))))

(define-public crate-packer-0.5.0 (c (n "packer") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "1vrh57dxxdzwz4i1nixanzq3i2mnwhpp61s562mmzmrk6nc3fpbg") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

(define-public crate-packer-0.5.1 (c (n "packer") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "06906amr8krv0n679lxb2vf4ca2ir3my81hv2v6i9zrdlqn5999w") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

(define-public crate-packer-0.5.2 (c (n "packer") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "1xmswagrcz79mvq52cxjjax2wh14z32dryb7x6mqk68mfjiz5l9h") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

(define-public crate-packer-0.5.3 (c (n "packer") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "1adrvz1bav3nc45vshgyahhrrr0waycyf5sj8yhi21gldsr18s2l") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

(define-public crate-packer-0.5.5 (c (n "packer") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5.5") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 2)))) (h "01qxmq2hrr7hpalv0lx066lrbs70scj5v28msf931d0lywmmv36c") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

(define-public crate-packer-0.5.7 (c (n "packer") (v "0.5.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "packer_derive") (r "^0.5.6") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 2)))) (h "05l43s5m28jfv62wlxbbiihssjqhlav6gvrz0s7ra77a1zbbmp49") (f (quote (("ignore" "packer_derive/ignore") ("default" "ignore") ("always_pack" "packer_derive/always_pack"))))))

