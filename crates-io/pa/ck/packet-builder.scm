(define-module (crates-io pa ck packet-builder) #:use-module (crates-io))

(define-public crate-packet-builder-0.1.0 (c (n "packet-builder") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.22.0") (d #t) (k 0)))) (h "02g53wb27cdxgj8qc0x4fr99dkz3pagpgzb9vgqjlaadgfz5jy3i")))

(define-public crate-packet-builder-0.1.1 (c (n "packet-builder") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.22.0") (d #t) (k 0)))) (h "1r0198nmbmsmyzgg3blwii4gl0i8dckla1fhdnj4vymsmlhp25vq")))

(define-public crate-packet-builder-0.2.0 (c (n "packet-builder") (v "0.2.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.22.0") (d #t) (k 0)))) (h "08c8x14qxgg6i8kggsxddxca3wkl0xm9xcvy2y6r9mq2smrcdnis")))

(define-public crate-packet-builder-0.3.0 (c (n "packet-builder") (v "0.3.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.22.0") (d #t) (k 0)))) (h "02x26n6hzzzb84wj37rxhcsl3n2gv9ssvzdzjvwl2mbj17bi6jvr")))

(define-public crate-packet-builder-0.4.0 (c (n "packet-builder") (v "0.4.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.23.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.23.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.23.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.23.0") (d #t) (k 0)))) (h "1a2sx0aza2ciwfbcylhjgs4hmnzs7396bl82h2qkjpkgjwpw9963")))

(define-public crate-packet-builder-0.5.0 (c (n "packet-builder") (v "0.5.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.26.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.26.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.26.0") (d #t) (k 0)))) (h "1mkqsl3gs8242pvfz7i51wly0wfasdskfnrc6vyiqgvb6pc217rn")))

(define-public crate-packet-builder-0.6.0 (c (n "packet-builder") (v "0.6.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.30.0") (d #t) (k 0)))) (h "0kls87nzfdh11mg6cy3pl9a163hvaydy88xkjjc916hnj14q5nl4")))

(define-public crate-packet-builder-0.7.0 (c (n "packet-builder") (v "0.7.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.19.0") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)))) (h "146d60zvhfzrgiry0v3bkix8wdi4w4k81jv32z7r3l6r0yjwa8hb")))

