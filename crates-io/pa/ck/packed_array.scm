(define-module (crates-io pa ck packed_array) #:use-module (crates-io))

(define-public crate-packed_array-0.1.0 (c (n "packed_array") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1hjql0rkw8k7xvshvjg4sm9h3r9nf2px3j8sfmw8p7nhi54qzm61") (y #t)))

(define-public crate-packed_array-0.1.1 (c (n "packed_array") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1r80cc4pyqb28p08zrysni364bdc1sakg67pwy76spm979ln8150") (y #t)))

(define-public crate-packed_array-0.1.2 (c (n "packed_array") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1qsq00lcy1zlnk6rk51zs095ivf2liyk2ys1ck99znpy4x35djrc")))

(define-public crate-packed_array-0.2.0 (c (n "packed_array") (v "0.2.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "071kv7ns7nh308px2jv4szkblzb5g46381ri7ma6k7yzcnkx8q1m")))

