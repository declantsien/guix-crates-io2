(define-module (crates-io pa ck package-key) #:use-module (crates-io))

(define-public crate-package-key-0.0.0 (c (n "package-key") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.155") (d #t) (k 0)))) (h "0iqn73y4f653lw3qw6qjf3lma5a3xhcdxi8dy2h347hzl6cxf543") (f (quote (("default"))))))

(define-public crate-package-key-0.1.0 (c (n "package-key") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.155") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "0ycvpzprni0rn03nrnndcgx3b6n1vg4qsagsidixb5h2d64j3jf8") (f (quote (("default" "serde"))))))

(define-public crate-package-key-0.1.1 (c (n "package-key") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.155") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "0qx37fwvnsvmfzma71n852rk5r6f37nh8spx1yg2xzv00siar2n2") (f (quote (("default" "serde"))))))

(define-public crate-package-key-0.1.2 (c (n "package-key") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.155") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1vc63syjk1a5mx2xipxcx9xw6ifzmpadq8w9wb2mrykmyj2zbnd7") (f (quote (("default" "serde"))))))

(define-public crate-package-key-0.2.0 (c (n "package-key") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.155") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "011al54q2x4gbpn3w0pbxin1msck76dgry52h5zmz6y6sbscsdah") (f (quote (("default" "serde"))))))

