(define-module (crates-io pa ck packing_codegen) #:use-module (crates-io))

(define-public crate-packing_codegen-0.1.0 (c (n "packing_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vsl6lbdjp62n6bcvcc3qbzip90fb40lmq263nzq3mhw70jym860") (f (quote (("diagnostic-notes"))))))

