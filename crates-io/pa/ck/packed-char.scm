(define-module (crates-io pa ck packed-char) #:use-module (crates-io))

(define-public crate-packed-char-0.1.0 (c (n "packed-char") (v "0.1.0") (h "1xcr6y1jcrq67m7irxg6y1rn3ja29cpcqgp3zlxjl3nny392f86c")))

(define-public crate-packed-char-0.1.1 (c (n "packed-char") (v "0.1.1") (h "17vbh454srwb8s0i6kg6k8gy3f52k8l7cij5a7w11hq6k3hck3fq")))

