(define-module (crates-io pa ck pack1) #:use-module (crates-io))

(define-public crate-pack1-0.1.0 (c (n "pack1") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b5z1rafr2c69qlbz4qlfja1v43bxb0slxvx4z3gp79zdcrghzks")))

(define-public crate-pack1-0.1.1 (c (n "pack1") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vr8cvlk4zk844sigcsl3g3fdwj4maqh7rr5l95b1yxxr0ysys2m")))

(define-public crate-pack1-1.0.0 (c (n "pack1") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mkjbcy7f8l3790064rbm5qvgjh6q2hwm80r2n1jrp1qssdwvryn")))

