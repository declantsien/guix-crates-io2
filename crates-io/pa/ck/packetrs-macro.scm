(define-module (crates-io pa ck packetrs-macro) #:use-module (crates-io))

(define-public crate-packetrs-macro-0.2.0 (c (n "packetrs-macro") (v "0.2.0") (d (list (d (n "packetrs-impl") (r "=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1svlb0svwp3bjrlcicca32n0sj12p684iyrlizy4g8b6kgfq9jjj")))

(define-public crate-packetrs-macro-0.3.0 (c (n "packetrs-macro") (v "0.3.0") (d (list (d (n "packetrs-impl") (r "=0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11rh3m8a1jc61w7z11yf36fs5zkl6ap8620m54644gnvr41ay512")))

(define-public crate-packetrs-macro-0.4.0 (c (n "packetrs-macro") (v "0.4.0") (d (list (d (n "packetrs-impl") (r "=0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jxgg8sr1wjpaidkxh2wxmkjy29lzqgivambipjlfn8870hhzpx2")))

(define-public crate-packetrs-macro-0.5.0 (c (n "packetrs-macro") (v "0.5.0") (d (list (d (n "packetrs-impl") (r "=0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gmcl73522cv416ppb1qncpfc5whfxc9wfpvmimyia16hwqp5lhn")))

