(define-module (crates-io pa ck packedtime-rs) #:use-module (crates-io))

(define-public crate-packedtime-rs-0.1.0 (c (n "packedtime-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0qwg7qrnriqpcbdq7lfpwi3k5v1r3p998jwnnic8im0cfq0wwkr8")))

(define-public crate-packedtime-rs-0.1.1 (c (n "packedtime-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "131c2fv45dkb3l6f83kr7jq0mzqvlxqli9k36y8n957f5jxdl3yv") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.0 (c (n "packedtime-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "0j012lklb3ax4092l2nq00m5n13vj3m9qjq5c5zgs16msq9240r4") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.1 (c (n "packedtime-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "1ykrj6xnzc395rgq97z67ljdmkxjf6wmm0hbmiwlkx6dh46alcfg") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.2 (c (n "packedtime-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "1qwwqi6f1hqdxqkpziz9fb5x9y3ildfpyba9aq9ls3ndaylann70") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.3 (c (n "packedtime-rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "0wvcfjqpa2l68l1q0dvi2hvqzhcl79cn8hr0scppk1kjj38ba8rg") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.4 (c (n "packedtime-rs") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing"))) (d #t) (k 2)))) (h "0fqk42q9rgbksfg2jp0pwhqpz844w4jiwv767ybvw4d874n9c047") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.5 (c (n "packedtime-rs") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing" "formatting"))) (d #t) (k 2)))) (h "07dhykh64rvw0lr679x6d2jbgz12xww3azx1k8aq5x27nml0hxqk") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.2.6 (c (n "packedtime-rs") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing" "formatting"))) (d #t) (k 2)))) (h "1p6f0sc03d4rgm6iq4vq8wy2ck86igbmzi3fmcl98b8a6wbc6wwc") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.3.0 (c (n "packedtime-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing" "formatting"))) (d #t) (k 2)))) (h "01b29i761yz6kr1iixvhha0hl7qwyr8llbpfvpr2w73nhdcmn53d") (f (quote (("expensive_tests") ("default"))))))

(define-public crate-packedtime-rs-0.3.1 (c (n "packedtime-rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("parsing" "formatting"))) (d #t) (k 2)))) (h "10vr6lf9q5msl3bxc6v10an738v6jhgjfpm62i1lh3l7ycdwilwy") (f (quote (("expensive_tests") ("default"))))))

