(define-module (crates-io pa ck pack-cli) #:use-module (crates-io))

(define-public crate-pack-cli-0.1.0 (c (n "pack-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)))) (h "07v7xpby79wr13dy1l8jgz3f2iazhr3rfp4vzy330zhh18kki0y4") (y #t)))

