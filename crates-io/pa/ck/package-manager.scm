(define-module (crates-io pa ck package-manager) #:use-module (crates-io))

(define-public crate-package-manager-0.1.0 (c (n "package-manager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0aw7zm77w9bkdfrdq599r09bb0b2m5mxybbjlh9pxnfbrk1vyaln")))

