(define-module (crates-io pa ck packet) #:use-module (crates-io))

(define-public crate-packet-0.1.0 (c (n "packet") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0hd9x4sqhrc1l8vhv8m3vkqzicdjk35f1g6i6lxlh8nndcjs5ms7")))

(define-public crate-packet-0.1.1 (c (n "packet") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "09hk00dr4yi9r96vp43p63ghx4gmjqc5akf5i9ilhqgzaaymkwi2")))

(define-public crate-packet-0.1.2 (c (n "packet") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "hwaddr") (r "^0.1") (d #t) (k 0)))) (h "1xmlf81iykf2qbihb7bl39z3vd1ivz2hzs2clrdn42kzgj2p97qn")))

(define-public crate-packet-0.1.3 (c (n "packet") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hwaddr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17iyk7fm9js0l4jwh4f1b6l2ghzp0g33jnmxy2sgsi73w263hv8z")))

(define-public crate-packet-0.1.4 (c (n "packet") (v "0.1.4") (d (list (d (n "bitflags") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "byteorder") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "hwaddr") (r ">=0.1.0, <0.2.0") (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0i39j7iy19x3wsb7jw2xgcz6i03cv1mgdv2ai644zv8r0snwfdn1")))

