(define-module (crates-io pa ck packed-encoder) #:use-module (crates-io))

(define-public crate-packed-encoder-0.1.0 (c (n "packed-encoder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1qnwhgk7fnwfxc00dkg8hzb9mskiwiy5lw9srhskga6pawbr4r1f")))

(define-public crate-packed-encoder-0.1.1 (c (n "packed-encoder") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1r4ydcffd5zl003zi91mpjns6d224ghj6a38lzgg96l0835xcngh")))

