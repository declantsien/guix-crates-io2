(define-module (crates-io pa ck packetdog) #:use-module (crates-io))

(define-public crate-PacketDog-0.1.0 (c (n "PacketDog") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pcap") (r "^0.7.0") (d #t) (k 0)) (d (n "pktparse") (r "^0.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hp0xzvgxlpqsfq92g4hg6vb7h8w6bhqd0mh6bsv50zr6ppdjaa4")))

