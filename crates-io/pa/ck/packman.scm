(define-module (crates-io pa ck packman) #:use-module (crates-io))

(define-public crate-packman-0.1.0 (c (n "packman") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1k803p2g8xq6vmmbgc86agc274h0939smn7p6i3m5q2rh3lvw9r8")))

(define-public crate-packman-0.1.1 (c (n "packman") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vj6lg7hn61hqvd70iv5z791sp7pccq22r0racimy972m1zr7xqx")))

(define-public crate-packman-0.1.2 (c (n "packman") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03gjc4ihzwzyijmfhky1097rlq78cq3vywpmzys1s8g246l693cr")))

