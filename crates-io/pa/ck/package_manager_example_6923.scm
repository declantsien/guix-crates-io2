(define-module (crates-io pa ck package_manager_example_6923) #:use-module (crates-io))

(define-public crate-package_manager_example_6923-0.1.0 (c (n "package_manager_example_6923") (v "0.1.0") (h "13ybndakv7kdwcwmcw3jgjap8aj27vi8a4av99liw2h8a08ypx4z") (y #t)))

(define-public crate-package_manager_example_6923-0.1.1 (c (n "package_manager_example_6923") (v "0.1.1") (h "1w6ssniyfa0xcb35pfmb69fngf3ipxl9994p2pr3n9vwpk87adjh") (y #t)))

(define-public crate-package_manager_example_6923-0.1.2 (c (n "package_manager_example_6923") (v "0.1.2") (h "1gn007qkvanyzfjby6bk8xrln7ix7yljyygm5v36713k4kxlza6l") (y #t)))

(define-public crate-package_manager_example_6923-0.1.3 (c (n "package_manager_example_6923") (v "0.1.3") (h "1pa7mzf4rs57lwkl1y0vw3bl1grw14f95mzpqzv29ad9y0nzjhg6")))

