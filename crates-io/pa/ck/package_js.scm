(define-module (crates-io pa ck package_js) #:use-module (crates-io))

(define-public crate-package_js-0.1.0 (c (n "package_js") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "013m5w3l7h8n2ri5rbpqfw01kysnbvgknf1cnghqz2fllfw1lx8s")))

(define-public crate-package_js-0.1.1 (c (n "package_js") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b8i1pm9pmis6jqgr5ccglssyi672kznnn0x3dz4fi4pq03czydw")))

(define-public crate-package_js-0.1.2 (c (n "package_js") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dbalg0pi43x5bnixbk0qsjl47b16pd32bcd07g3nfv37h1rs6kg")))

(define-public crate-package_js-0.1.3 (c (n "package_js") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v08pbl0zpi2qqzjk9d39fkj2c9rcgrk015v2z3sv8z64r3a50rh")))

(define-public crate-package_js-0.2.0 (c (n "package_js") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fd65argm9j1zd782h9gjl92k4g56mlhra7fjik0dxy2i45p4x7d")))

(define-public crate-package_js-0.3.0 (c (n "package_js") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cmmhgid3c8akrmgil483x8w01ikqdbbjhwas6a7sl4jb4m9hl11")))

