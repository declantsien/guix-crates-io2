(define-module (crates-io pa ck packr2) #:use-module (crates-io))

(define-public crate-packr2-0.1.0 (c (n "packr2") (v "0.1.0") (h "1fvhdcs6k65in10asfsdb6lxmxpbz7n7bmnlb7fxs1gkr7cmdff5")))

(define-public crate-packr2-0.2.0 (c (n "packr2") (v "0.2.0") (h "033lbcgfw570fhh2f8i8fbyn3qfdrfq41zdwv9ry7kr7i4f6031b")))

(define-public crate-packr2-0.3.0 (c (n "packr2") (v "0.3.0") (h "0nbss89wxcdc0k4zg0p50xlfd2pl5g6dsjghl2rj7rv58fgg60c6")))

(define-public crate-packr2-0.3.1 (c (n "packr2") (v "0.3.1") (h "15w3kwzjz5m5f68xb5qwi6nf128iq5vksl670yyp252hx8s2p8q0")))

(define-public crate-packr2-0.3.2 (c (n "packr2") (v "0.3.2") (h "0bywchjy96why9phkafbhbzj9fxaidr6d807nb3kcrfvy75ifdj8")))

(define-public crate-packr2-0.3.3 (c (n "packr2") (v "0.3.3") (h "04ildrflvm4m0d5p71ks60sbhq1pqd8p5l753bp1i97lza9ymmbn")))

