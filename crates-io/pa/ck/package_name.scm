(define-module (crates-io pa ck package_name) #:use-module (crates-io))

(define-public crate-package_name-0.1.0 (c (n "package_name") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i6rqfm3ljsg4h8wqdlr9fvyfvn1d1cy6f06n7sv9slka7clpgda")))

