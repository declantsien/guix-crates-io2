(define-module (crates-io pa ck package_own) #:use-module (crates-io))

(define-public crate-package_own-0.0.1 (c (n "package_own") (v "0.0.1") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0hx06226z6karzvs954zjnwz8wjqghz7mk6qc2d5y293x11h28pq")))

(define-public crate-package_own-0.0.2 (c (n "package_own") (v "0.0.2") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "110kwipx2wsmpiawcdf1zrn10j0cparrfvff3wncgzgyljgjnirz")))

(define-public crate-package_own-0.0.3 (c (n "package_own") (v "0.0.3") (h "1wbz053lm64s9ab2s7h6mwpn5mm58ng339lw60wy1yiscs6gw6iv")))

