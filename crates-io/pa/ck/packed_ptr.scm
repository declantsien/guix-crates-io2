(define-module (crates-io pa ck packed_ptr) #:use-module (crates-io))

(define-public crate-packed_ptr-0.1.0 (c (n "packed_ptr") (v "0.1.0") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "099l6l4n1qv6wnb9y700ra94ffh6kpsyhansmvw75bpvdm87masy") (f (quote (("default")))) (r "1.51.0")))

(define-public crate-packed_ptr-0.1.1 (c (n "packed_ptr") (v "0.1.1") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1587wdp0jnhg5v7h55wzlci413vgp884rm8c3f0h79a3h57hc0zs") (f (quote (("default")))) (r "1.51.0")))

(define-public crate-packed_ptr-0.1.2 (c (n "packed_ptr") (v "0.1.2") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1q767jzf76kz5hqyx12a5i8cxhjcz93hrpyndwcdi82gb67xi3ra") (f (quote (("default")))) (r "1.51.0")))

