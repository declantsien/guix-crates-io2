(define-module (crates-io pa ck packed_struct_codegen) #:use-module (crates-io))

(define-public crate-packed_struct_codegen-0.1.0 (c (n "packed_struct_codegen") (v "0.1.0") (d (list (d (n "packed_struct") (r "^0.1.0") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "02zdy1r3jqf71kpqzlscjqkaynajll382mzdqqwlbld26g01cyd8") (f (quote (("std") ("default" "std") ("core_collections"))))))

(define-public crate-packed_struct_codegen-0.2.0 (c (n "packed_struct_codegen") (v "0.2.0") (d (list (d (n "packed_struct") (r "^0.1.0") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1a3nzr30km48yfan66lhhfjs8ds74aqdcjh7jq1d80b9dfg87ml8") (f (quote (("std") ("default" "std") ("core_collections"))))))

(define-public crate-packed_struct_codegen-0.2.1 (c (n "packed_struct_codegen") (v "0.2.1") (d (list (d (n "packed_struct") (r "^0.2") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1rgsxqld6185rz7kwqjhdmi8qfh4dhxki2y0va20c9hyrh7cx2f9") (f (quote (("std" "packed_struct/std") ("default" "std") ("core_collections"))))))

(define-public crate-packed_struct_codegen-0.2.2 (c (n "packed_struct_codegen") (v "0.2.2") (d (list (d (n "packed_struct") (r "^0.2") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1w4q22x2jqgf4xkjsn5rsh793ycnr9xch6kbql79qz35ap9yl53h") (f (quote (("std" "packed_struct/std") ("default" "std") ("core_collections"))))))

(define-public crate-packed_struct_codegen-0.2.3 (c (n "packed_struct_codegen") (v "0.2.3") (d (list (d (n "packed_struct") (r "^0.2") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0qg2j52b484mw3q74lsw9qj3k3v7fpll5pryw4szmr1wjc3zsml6") (f (quote (("std" "packed_struct/std") ("default" "std") ("alloc" "packed_struct/alloc"))))))

(define-public crate-packed_struct_codegen-0.2.4 (c (n "packed_struct_codegen") (v "0.2.4") (d (list (d (n "packed_struct") (r "^0.2") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1a44qvmyj451yp8j8cyhdsb7d2d8b2mq34rss6v69ldp1vyv093s") (f (quote (("std" "packed_struct/std") ("default" "std") ("alloc" "packed_struct/alloc"))))))

(define-public crate-packed_struct_codegen-0.3.0 (c (n "packed_struct_codegen") (v "0.3.0") (d (list (d (n "packed_struct") (r "^0.3") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1z2p8g56z01gy0drvfcyxkm54ihkpdssmm4vi0l7nyz3xcaxlvwz") (f (quote (("std" "packed_struct/std") ("default" "std") ("alloc" "packed_struct/alloc"))))))

(define-public crate-packed_struct_codegen-0.3.1 (c (n "packed_struct_codegen") (v "0.3.1") (d (list (d (n "packed_struct") (r "^0.3") (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0g8z4j9zshp373hmzwsmw0nrmck6rvx4gpbzpqlrd5p7ysy3zsma") (f (quote (("std" "packed_struct/std") ("default" "std") ("alloc" "packed_struct/alloc"))))))

(define-public crate-packed_struct_codegen-0.4.0 (c (n "packed_struct_codegen") (v "0.4.0") (d (list (d (n "packed_struct") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1gavvb7mzyxxgnnci2hn9n5gs7dyvmpsi2p09qpjz4g9yj4rpd") (f (quote (("std" "packed_struct/std") ("default" "std") ("alloc" "packed_struct/alloc"))))))

(define-public crate-packed_struct_codegen-0.5.0 (c (n "packed_struct_codegen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d4dlv0ydpz3w6gk9fkzggwm0x5kgrrm9wmrdbn8mqsnil1p3bxa") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-packed_struct_codegen-0.6.0 (c (n "packed_struct_codegen") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bay9c14qdfanq7cc7zdaramgpzvp5gd2xhbfg5pch9scf82jp68") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-packed_struct_codegen-0.6.1 (c (n "packed_struct_codegen") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v39ini58y42xb1vdq73bjb48kd8hddryjq9hrgzyyrymh814wzq") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-packed_struct_codegen-0.10.0 (c (n "packed_struct_codegen") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g247v4f26c3wich0b4is0q33k526wb9a7j4rf6d9hbyhqmnkqsn") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-packed_struct_codegen-0.10.1 (c (n "packed_struct_codegen") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "164z2y7xp2wy41dawrp94g6slky46k0157m0d87kxmahzrnp1mlw") (f (quote (("std") ("default") ("alloc"))))))

