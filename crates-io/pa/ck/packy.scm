(define-module (crates-io pa ck packy) #:use-module (crates-io))

(define-public crate-packy-0.0.1 (c (n "packy") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1sxrbvyzyxkq3pvaf7489r6rr6q901ahvsmdvb9a04dyxarn3lwy")))

(define-public crate-packy-0.0.2 (c (n "packy") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "131dx3yq9n7p9cni7klhvny27417ky7j7piy2cldfx47z3sbbdkd")))

(define-public crate-packy-0.0.3 (c (n "packy") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)) (d (n "zstd") (r "^0.13") (d #t) (k 0)))) (h "1i0mrhmznj1dr9kkxazp8ragd24jjfw3ahpbdjvvni5s0j6d89sw")))

