(define-module (crates-io pa ck pack_it_up) #:use-module (crates-io))

(define-public crate-pack_it_up-0.1.0 (c (n "pack_it_up") (v "0.1.0") (h "0ri2wcwgfmd14kkx7wlzfd4wxl92yaby1p37bynlm69kmzdb2pq2")))

(define-public crate-pack_it_up-0.1.1 (c (n "pack_it_up") (v "0.1.1") (h "0j7sdcsa1spz6x99qnkzhlm1rwbqgslay5s9z6bmyapjpj16qfnd")))

(define-public crate-pack_it_up-1.0.0 (c (n "pack_it_up") (v "1.0.0") (h "1ds85b9gs2icl6smmp8d60whrj2w1hz1d21adp9j7gxfiyysqkm4")))

