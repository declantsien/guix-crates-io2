(define-module (crates-io pa ck packattack-derive) #:use-module (crates-io))

(define-public crate-packattack-derive-0.2.0 (c (n "packattack-derive") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sr95f7fp5xnpdarp0fgrwzfwf7ww6g45s1csfwp3fg7fs96kc3y")))

(define-public crate-packattack-derive-0.2.1 (c (n "packattack-derive") (v "0.2.1") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0npdvj3lsmbgw9ilppyrhs5ch1fnxc7f89wvsgzvza2g0w4k4gl2")))

(define-public crate-packattack-derive-0.3.0 (c (n "packattack-derive") (v "0.3.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zqn9889jh4vql9dqnmplkx1m9p4hjpcyirj1ljnv7dj8g5vn1ks")))

(define-public crate-packattack-derive-0.3.1 (c (n "packattack-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pi4gdbany18qwy75nlvk4171hndgba9gxkps8j4248387i71q0v")))

