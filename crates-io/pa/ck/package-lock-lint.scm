(define-module (crates-io pa ck package-lock-lint) #:use-module (crates-io))

(define-public crate-package-lock-lint-0.1.0 (c (n "package-lock-lint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "01ahxfm2vwkci48nbfmsaahci8rxqbh78drm0m8mmkhzzrw9mi1q")))

(define-public crate-package-lock-lint-0.1.1 (c (n "package-lock-lint") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "058srykj8srfkvf2r9j59f3k01fr6whi1k11cf4ibk5kqbr2p49l")))

(define-public crate-package-lock-lint-0.2.0 (c (n "package-lock-lint") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "05333yf68dqafqgrp3p007qw9b2smq27gz8i73glw6f0g4zqycsp")))

(define-public crate-package-lock-lint-0.2.1 (c (n "package-lock-lint") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "07ys69xybsg7ypwmsx706hry2als5y5majr9di6bf7wk7jpdzzc7")))

(define-public crate-package-lock-lint-0.2.2 (c (n "package-lock-lint") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ji3g6h8y41mccqc5hmzidaymcssxkcrpc3l9g56anns8gf68bww") (r "1.56")))

(define-public crate-package-lock-lint-0.2.3 (c (n "package-lock-lint") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0lq4bji2brp5zf3hqghvj6v5csyvsxfrfqmcxj0j7nihrq8761ax") (r "1.56")))

(define-public crate-package-lock-lint-0.2.4 (c (n "package-lock-lint") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18mc50kphfmjfij74ym1jv3k14sszc39f4wrplh9v0dh62y3f8vj") (r "1.56")))

(define-public crate-package-lock-lint-0.2.5 (c (n "package-lock-lint") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "12hzfq5d8sh519wnzyy46m9sz4sf0sh82zmfcwkhsi59ninhqz8f") (r "1.56")))

