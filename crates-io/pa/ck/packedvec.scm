(define-module (crates-io pa ck packedvec) #:use-module (crates-io))

(define-public crate-packedvec-1.0.0 (c (n "packedvec") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wicinvsrghw5rkwn1lx9jyk3whq14ncb1lcapj8k6ywki2i6dcz")))

(define-public crate-packedvec-1.1.0 (c (n "packedvec") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0da9d8jqvph39qf5b3kvgjq2l7cj32clwz24i4f42hszz90hyvmx")))

(define-public crate-packedvec-1.2.0 (c (n "packedvec") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0an9h8fzxl4080g1fiarzrny9njm9b3hzqjpfyhf2vxz2p1dag0d")))

(define-public crate-packedvec-1.2.1 (c (n "packedvec") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i9vxc94dsgqx783yaplb29zmml85hd19zcwfwgvkdkm2sg926vw")))

(define-public crate-packedvec-1.2.2 (c (n "packedvec") (v "1.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pawv02k8pr2rgka8gy1q5fj1v3wd1pd23cxgsyksi5s70jm7khz")))

(define-public crate-packedvec-1.2.3 (c (n "packedvec") (v "1.2.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zrbimnzjq7pbddjq9f8f8i130dhv919gh4vnhzjwgfk1b5wy3ik")))

(define-public crate-packedvec-1.2.4 (c (n "packedvec") (v "1.2.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i9a0vlzvql5ip62l7hs058fp08i8nd2iw26zfsa5r10xj8cdqxx")))

