(define-module (crates-io pa ck packtool-macro) #:use-module (crates-io))

(define-public crate-packtool-macro-0.1.0 (c (n "packtool-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1k9bchfv8n7f769bgpzdancpdnbd44y48jy49350201pifgwi842")))

(define-public crate-packtool-macro-0.2.0 (c (n "packtool-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0l572y6fraff3nxrqk4mlpmjr3gsfxwc37nyg6y4gsj57j43xd3r")))

(define-public crate-packtool-macro-0.3.0 (c (n "packtool-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "185abfbbdvni51garnh00vcw1i6kbfy954ghwb4vx161xnnwpc53")))

