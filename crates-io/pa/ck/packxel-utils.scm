(define-module (crates-io pa ck packxel-utils) #:use-module (crates-io))

(define-public crate-packxel-utils-0.1.0 (c (n "packxel-utils") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "19ly94h0v2hj1nd3xdppgcx2xayxwd0f0jlbr9g34c5aspfbvazp")))

(define-public crate-packxel-utils-0.1.1 (c (n "packxel-utils") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0mpr3yi03m620gws6j8kzabqgcb7q5srq46f20ddz95qv2g0kxgx")))

(define-public crate-packxel-utils-0.2.0-beta (c (n "packxel-utils") (v "0.2.0-beta") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0wkkqshxq7k9cbbk45ygmsljvhms6zwhm23f78lfcfka33614c81") (f (quote (("full" "logging") ("default")))) (s 2) (e (quote (("logging" "dep:log" "dep:log4rs"))))))

(define-public crate-packxel-utils-0.3.0 (c (n "packxel-utils") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0xrqqp0lmx2cj8fz9amx294wlk0zcxg0x0j8a1s0wdh4nr11af11") (f (quote (("full" "logging") ("default")))) (s 2) (e (quote (("logging" "dep:log" "dep:log4rs"))))))

(define-public crate-packxel-utils-0.3.1 (c (n "packxel-utils") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0c42vbg6j0s33pa1ly2xqb70yf5vz7yg7v2j7wqyd9fnb1109fch") (f (quote (("full" "logging") ("default")))) (s 2) (e (quote (("logging" "dep:log" "dep:log4rs"))))))

