(define-module (crates-io pa ck package_file_verify) #:use-module (crates-io))

(define-public crate-package_file_verify-0.1.0 (c (n "package_file_verify") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_info") (r "^3") (k 0)))) (h "04kimyzy4j96w2j4p2s3yhqh491nlxlb1p97bv1h33c3f6hwfjjj") (f (quote (("cli" "env_logger" "clap"))))))

(define-public crate-package_file_verify-0.1.1 (c (n "package_file_verify") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_info") (r "^3") (k 0)))) (h "1c3kh24q1s1p6c6g5wysk5mm4bg8l8x96395ixy6m2pf2bf9fhwz") (f (quote (("cli" "env_logger" "clap"))))))

