(define-module (crates-io pa ck packos) #:use-module (crates-io))

(define-public crate-packos-0.1.0 (c (n "packos") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "insta") (r "^0.13.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1lp63blf4lzfyj822hfaf8xck118766s56apq6hb7iks8m8kh6z4")))

