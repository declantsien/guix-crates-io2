(define-module (crates-io pa ck pack-release) #:use-module (crates-io))

(define-public crate-pack-release-0.16.0 (c (n "pack-release") (v "0.16.0") (d (list (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1qxcs4c0bwf8py7lp7h9h5vpcca2dnm65qa2d7vpsi128y1nwyfk")))

(define-public crate-pack-release-0.17.0 (c (n "pack-release") (v "0.17.0") (d (list (d (n "xshell") (r "^0.1.8") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0hh7g49gh8csk0xzhr8gvq5bvhmdf5vq62knck986y69zh9mp6gp")))

(define-public crate-pack-release-0.18.0 (c (n "pack-release") (v "0.18.0") (d (list (d (n "xshell") (r "^0.1.8") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0bds7yiacpblq936k0dfyh36d6ak98ir288x5qws6hp4dba6zmgn")))

(define-public crate-pack-release-0.19.0 (c (n "pack-release") (v "0.19.0") (d (list (d (n "xshell") (r "^0.1.13") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "138b1ym04jmwqxz7k3inwxcnvsxx64y0x2x9kginr0d4jrgspxlj")))

