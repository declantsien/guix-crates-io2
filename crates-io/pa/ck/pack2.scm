(define-module (crates-io pa ck pack2) #:use-module (crates-io))

(define-public crate-pack2-0.1.0 (c (n "pack2") (v "0.1.0") (d (list (d (n "etherparse") (r "^0.10") (d #t) (k 2)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (k 0)))) (h "1r122wac7fzr7zsfpwphd3y4bkldxp4bfqhhnjqijrn98y2ism11")))

(define-public crate-pack2-0.1.1 (c (n "pack2") (v "0.1.1") (d (list (d (n "etherparse") (r "^0.10") (d #t) (k 2)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (k 0)))) (h "1b7an8bm6vzrlp8c8shp2i3hfnbcc40z4n5ijavl5al1lvs118d8")))

(define-public crate-pack2-0.1.2 (c (n "pack2") (v "0.1.2") (d (list (d (n "etherparse") (r "^0.10") (d #t) (k 2)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (k 0)))) (h "0kyrbag05f3cmjc3bvnz31h9d7b0wy35h93ixbmqbcaxpfrc4fy2")))

(define-public crate-pack2-0.1.3 (c (n "pack2") (v "0.1.3") (d (list (d (n "etherparse") (r "^0.10") (d #t) (k 2)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO"))) (d #t) (k 0)))) (h "1cdpnq74z0qkslwzzlkqmmb659cpw72akvczni56m82s92mwmnld")))

