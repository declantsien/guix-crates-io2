(define-module (crates-io pa ck packs) #:use-module (crates-io))

(define-public crate-packs-0.1.0 (c (n "packs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "packs-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "19ma2774z1l0ynm396ddxvi1c3795ppp8bibm86374kfkdqjgzan") (f (quote (("std_structs" "derive") ("derive" "packs-proc") ("default" "std_structs"))))))

(define-public crate-packs-0.2.0 (c (n "packs") (v "0.2.0") (d (list (d (n "packs-proc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "packs-proc") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0cz3dyjyhaj7yhnygn7nskadpwwr1jm5c4vjhyjinab60qmmy3k9") (f (quote (("std_structs" "derive") ("derive" "packs-proc") ("default" "std_structs"))))))

