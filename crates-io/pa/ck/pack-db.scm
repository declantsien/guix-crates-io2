(define-module (crates-io pa ck pack-db) #:use-module (crates-io))

(define-public crate-pack-db-0.1.0 (c (n "pack-db") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "rmp") (r "^0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xpyf17sbzcw4kavi8wv677rdyg02naddkk13dsls6ar6z4r56l4")))

(define-public crate-pack-db-0.2.0 (c (n "pack-db") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "rmp") (r "^0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j9a23iqkxhg5rkjfy9cp95bn36x1h3ij1jvh34w1clm5cp6q0js")))

