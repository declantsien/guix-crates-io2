(define-module (crates-io pa ck packed_integer_array) #:use-module (crates-io))

(define-public crate-packed_integer_array-0.1.0 (c (n "packed_integer_array") (v "0.1.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "indexed_bitvec") (r "^2.3") (d #t) (k 0)) (d (n "indexed_bitvec_core") (r "^3.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hsis5prq964bcw4xj3ihydfid101cbkm74ncq6gb6gyaznqgzcy") (f (quote (("implement_heapsize" "heapsize" "indexed_bitvec/implement_heapsize" "indexed_bitvec_core/implement_heapsize") ("default"))))))

(define-public crate-packed_integer_array-0.2.0 (c (n "packed_integer_array") (v "0.2.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "indexed_bitvec") (r "^2") (d #t) (k 0)) (d (n "indexed_bitvec_core") (r "^3") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hal8kmjq56w30s6gikyabxzw71l947vs27z6q850smf5gah28z2") (f (quote (("implement_heapsize" "heapsize" "indexed_bitvec/implement_heapsize" "indexed_bitvec_core/implement_heapsize") ("default"))))))

