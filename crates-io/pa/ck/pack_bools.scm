(define-module (crates-io pa ck pack_bools) #:use-module (crates-io))

(define-public crate-pack_bools-0.1.0 (c (n "pack_bools") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "0lmg1djdpkk7xh09ijxsf8qqs64p85r8la01ckiw8iq93pxryxv4")))

(define-public crate-pack_bools-0.1.1 (c (n "pack_bools") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "1f4aagl19imiywffarcs4077nx90wilxb8x63kdpszsymf4f35by")))

(define-public crate-pack_bools-0.1.2 (c (n "pack_bools") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "1p8r2j7z12ljmlmkahljprvxc8yvvfw9rz6qgc6sbdzzrzfwgkac")))

