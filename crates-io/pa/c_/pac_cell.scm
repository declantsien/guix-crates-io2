(define-module (crates-io pa c_ pac_cell) #:use-module (crates-io))

(define-public crate-pac_cell-0.1.0 (c (n "pac_cell") (v "0.1.0") (h "032y1lkcabmp0srmcwf09v04bzpqgxq948zll5liafy3kgak3x71")))

(define-public crate-pac_cell-0.1.1 (c (n "pac_cell") (v "0.1.1") (h "1psb8wzcakmq62ya46igzd46k26a6hj1ph1r40n6cqig8w6divag")))

