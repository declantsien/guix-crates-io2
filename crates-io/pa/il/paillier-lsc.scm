(define-module (crates-io pa il paillier-lsc) #:use-module (crates-io))

(define-public crate-paillier-lsc-0.1.0 (c (n "paillier-lsc") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-lsc") (r "^0.1.0") (f (quote ("num-bigint"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "160w9kdh2y2c3v5smm6scqqrxiq9rwbi3gcfy2xbi9cknc1v3g8f") (y #t)))

(define-public crate-paillier-lsc-0.1.1 (c (n "paillier-lsc") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-lsc") (r "^0.1.0") (f (quote ("num-bigint"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ipys3acy6209jz97bgwibnwjl3zpiz816gm07c2gb8bqrsgnmn6") (y #t)))

(define-public crate-paillier-lsc-0.1.2 (c (n "paillier-lsc") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-lsc") (r "^0.1.2") (f (quote ("num-bigint"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wysk03wafdp35mm5mm7v19iy9cwav601g8b4p0qryhsfilzqhvi")))

(define-public crate-paillier-lsc-0.1.3 (c (n "paillier-lsc") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-lsc") (r "^0.1.2") (f (quote ("num-bigint"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00rszjzjsc0m5pvykk80386lpymamlahxh9hlazi7d6qz4bmag43")))

