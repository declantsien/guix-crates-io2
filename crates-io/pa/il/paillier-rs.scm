(define-module (crates-io pa il paillier-rs) #:use-module (crates-io))

(define-public crate-paillier-rs-0.1.0 (c (n "paillier-rs") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-primes") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "0h2g1qb9y4sdawd6v9wk7gfh4kjr522masqhijhc8mrbja50xl21")))

(define-public crate-paillier-rs-0.2.0 (c (n "paillier-rs") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-primes") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "1fmdrpi1gz0v1iirsapnvj6z87bbkkj624k2brydyd9qy3yzwqpv")))

(define-public crate-paillier-rs-0.2.1 (c (n "paillier-rs") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-primes") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "1qqvw3fip4q27xb578lgc84mdkhxb7j6pm04rrp9bwpds1g0k617")))

(define-public crate-paillier-rs-0.2.2 (c (n "paillier-rs") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-primes") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "0ai68a0kic5nfdw0l6xnf0sin9mlgmbqafn64f35cci8k65gbnv8")))

(define-public crate-paillier-rs-0.2.3 (c (n "paillier-rs") (v "0.2.3") (d (list (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-primes") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)))) (h "1ginh8aq1plj1lxs727cbpah7dqai3p9i0vph30mcw7dar1p1vvx")))

