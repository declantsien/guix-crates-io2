(define-module (crates-io pa yu payu-india) #:use-module (crates-io))

(define-public crate-payu-india-0.0.1 (c (n "payu-india") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.2.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1w26abd2g2a9cwzllaxdgrrgz9573bpls63dr9ynhcjprzfn4cs4")))

(define-public crate-payu-india-0.0.2 (c (n "payu-india") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.2.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04xzppa28igx062f7gyhyafvyj1pgk0zsy29f8m05fxa76xwnlqs")))

