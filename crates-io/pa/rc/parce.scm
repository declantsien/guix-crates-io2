(define-module (crates-io pa rc parce) #:use-module (crates-io))

(define-public crate-parce-0.0.0 (c (n "parce") (v "0.0.0") (d (list (d (n "parce_macros") (r "^0.0.0") (d #t) (k 0)))) (h "19wgbxdbxfkhsfk5b3d4k8gwjjbnhgzpsz8gl0rzj3vifizqvm9i")))

(define-public crate-parce-0.0.1 (c (n "parce") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parce_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0l0n50as5fq137ab5x1q7f0yiljbhcw7lx587nihdrli6v5k1h0w")))

