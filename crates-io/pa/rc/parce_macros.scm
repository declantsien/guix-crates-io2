(define-module (crates-io pa rc parce_macros) #:use-module (crates-io))

(define-public crate-parce_macros-0.0.0 (c (n "parce_macros") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "003fcgqwmabmm30p4w3gak8vlkdz81qjl0wxag26maxwrp6cyz9v")))

(define-public crate-parce_macros-0.0.1 (c (n "parce_macros") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "check_keyword") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0jkzqkknnbl3a2wi8xsm44w9pfn1vsf296s4vfqmkqzh912l1r67")))

