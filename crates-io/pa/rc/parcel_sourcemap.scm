(define-module (crates-io pa rc parcel_sourcemap) #:use-module (crates-io))

(define-public crate-parcel_sourcemap-2.0.0-rc.2 (c (n "parcel_sourcemap") (v "2.0.0-rc.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "0kknnd88vm5svyaqqsdz4595bkyhdgg2aaqgbb90s4578h75m2h7")))

(define-public crate-parcel_sourcemap-2.0.0-rc.3 (c (n "parcel_sourcemap") (v "2.0.0-rc.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1h28k41x052kq0a98v06d7q7hwpf87v4i7z187bw7scnbvncvf66")))

(define-public crate-parcel_sourcemap-2.0.0-rc.4 (c (n "parcel_sourcemap") (v "2.0.0-rc.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1b0fnwak9kibx8fqrl7gqf4r67k90y2hxih238hqy7kg2lxfy013")))

(define-public crate-parcel_sourcemap-2.0.0-rc.5 (c (n "parcel_sourcemap") (v "2.0.0-rc.5") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0592l0j6lp3v7m9avp48kxvljfw23by1rgk93kfhjrjy91bjhg52")))

(define-public crate-parcel_sourcemap-2.0.0-rc.6 (c (n "parcel_sourcemap") (v "2.0.0-rc.6") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "06vg5h76k10mim39lwpsj8g0hrm9mlgzgwpv70kp9fbqbgr8bb07")))

(define-public crate-parcel_sourcemap-2.0.0-rc.7 (c (n "parcel_sourcemap") (v "2.0.0-rc.7") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1dscp29lf9d9qb7zcaw1c1vz83agxvl6cp0g9ns64wmx64s5w09y")))

(define-public crate-parcel_sourcemap-2.0.0 (c (n "parcel_sourcemap") (v "2.0.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "napi") (r "^1.6.1") (d #t) (k 0)) (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "114kzl23pcfz084py17jwin4lg4yv31h5pc2lmii8r19jw3hh7p8")))

(define-public crate-parcel_sourcemap-2.0.2 (c (n "parcel_sourcemap") (v "2.0.2") (d (list (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "07p2dqp9gyvlsvp8arn2i1xwixaprg15nm48z54p9ghnym9dlwsy")))

(define-public crate-parcel_sourcemap-2.0.3 (c (n "parcel_sourcemap") (v "2.0.3") (d (list (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "07av4chdav8k63jwlnlxw41spgcs03sf4gz1kw222iv7vd5r9k9q")))

(define-public crate-parcel_sourcemap-2.0.4 (c (n "parcel_sourcemap") (v "2.0.4") (d (list (d (n "rkyv") (r "^0.7.38") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "1aw9rm500aiv89izjliy5r82dgg0yxbavc3dawjr380qba6rvj85")))

(define-public crate-parcel_sourcemap-2.0.5 (c (n "parcel_sourcemap") (v "2.0.5") (d (list (d (n "rkyv") (r "^0.7.38") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "0bcfj4b5jf1nixwsn8148s83xm20h5bcs2c92rdqyhkyqpv5qcdn")))

(define-public crate-parcel_sourcemap-2.1.0 (c (n "parcel_sourcemap") (v "2.1.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "data-url") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "05wrxml5invg3a521l0m804pbkc4bznkf6z8znanq6i46mndw5ik") (f (quote (("json" "serde" "serde_json" "base64" "data-url"))))))

(define-public crate-parcel_sourcemap-2.1.1 (c (n "parcel_sourcemap") (v "2.1.1") (d (list (d (n "base64-simd") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "data-url") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)))) (h "1fsvw1mlqc5x4psj90jxrdbivq8sqvxi5zz3q2vv4s4047bp8ns8") (f (quote (("json" "serde" "serde_json" "base64-simd" "data-url"))))))

