(define-module (crates-io pa rc parcel) #:use-module (crates-io))

(define-public crate-parcel-0.0.1 (c (n "parcel") (v "0.0.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "esparse") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "1n6rsj0dfwl5363ay7mz01issgq7qb2k6pr32fig2d6rdhny148w")))

(define-public crate-parcel-0.0.2 (c (n "parcel") (v "0.0.2") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "esparse") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "1hc3880w38qrf8bl11damxi21j1x0sg04hp4cw0xpl1b46bcd9gw")))

(define-public crate-parcel-0.1.1 (c (n "parcel") (v "0.1.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "esparse") (r "^0.0.3") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "156y1pmmmm3hy6yp1g7ysan7m3fgb0hfh5rqbmjj0g9hricbpbnx")))

(define-public crate-parcel-0.1.2 (c (n "parcel") (v "0.1.2") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "esparse") (r "^0.0.3") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "1w4ldmcbp21m53yjwzaa2ns7v5mr08b9gsr70adddwag9211gqd2")))

(define-public crate-parcel-1.0.0 (c (n "parcel") (v "1.0.0") (h "0ywykrqy9i4m2h721fpfh9nm8ycldj7aa69nnpikwjc2h2cfc4x0")))

