(define-module (crates-io pa rc parcos) #:use-module (crates-io))

(define-public crate-parcos-0.0.1 (c (n "parcos") (v "0.0.1") (h "06g19832y24v0x87506bh7yb5329c9vs12wv4vhkxjm5az4ssb4m")))

(define-public crate-parcos-0.0.2 (c (n "parcos") (v "0.0.2") (h "0y93mzx6mgna4mn9p0ngg3967209cbgibxqgcbcsxsqizrbg3nwx")))

(define-public crate-parcos-0.0.3 (c (n "parcos") (v "0.0.3") (h "0xk2z1iwkzdfglfa1l19rm69aib4mnvypcz18jgnfpk9cha57w5i")))

(define-public crate-parcos-0.0.4 (c (n "parcos") (v "0.0.4") (h "1jvrkzkijx2qfpav910hr2xqxnm0b43zp3128i7lp3cvrdmgqx3x")))

(define-public crate-parcos-0.0.5 (c (n "parcos") (v "0.0.5") (h "1bpbdzkg3njs17b5hhj3igj06mcq2f4sj8mgsz9y9x7lc9h2f2sn")))

