(define-module (crates-io pa rc parch-lib) #:use-module (crates-io))

(define-public crate-parch-lib-0.1.0 (c (n "parch-lib") (v "0.1.0") (d (list (d (n "aead") (r "^0.4.3") (f (quote ("std" "stream"))) (d #t) (k 0)) (d (n "aes-gcm-siv") (r "^0.10.3") (d #t) (k 0)) (d (n "brotli") (r "^3.3.2") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (f (quote ("reduced-round"))) (d #t) (k 0)) (d (n "crypto") (r "^0.3.0") (f (quote ("aead"))) (d #t) (k 0)) (d (n "deoxys") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "0zy6sgm8awr06625kmfk53ci9jr8f9dksnwnps779bmc0l8bd0dm") (y #t)))

