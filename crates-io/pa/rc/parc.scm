(define-module (crates-io pa rc parc) #:use-module (crates-io))

(define-public crate-parc-1.0.0 (c (n "parc") (v "1.0.0") (h "0hyhy3dw9hzknln53h0a90inw8zis80hrnnn88s6ll4292yf5nlw") (f (quote (("std") ("default" "std"))))))

(define-public crate-parc-1.0.1 (c (n "parc") (v "1.0.1") (h "0kqg845pbqpwlymhh4frxfxz5j0zfwmpsyi2biyqw16j28c3542m") (f (quote (("std") ("default" "std"))))))

