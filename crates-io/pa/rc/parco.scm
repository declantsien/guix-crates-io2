(define-module (crates-io pa rc parco) #:use-module (crates-io))

(define-public crate-parco-1.0.0 (c (n "parco") (v "1.0.0") (h "0yn1iadpz4mrxp9fqf5p2yhl5cw267075ia0xiqs6nbwcqm6mpbr")))

(define-public crate-parco-1.1.0 (c (n "parco") (v "1.1.0") (h "0dqgwjd8527yq7fhx4c388z43vsizh0bv69zzys0ry5l56rgr3m0")))

(define-public crate-parco-1.2.0 (c (n "parco") (v "1.2.0") (h "0fdvlnb7gya4086bccimqnbsj0rw6lrbyl1kjncr3cc5f51yzwn2")))

(define-public crate-parco-1.3.0 (c (n "parco") (v "1.3.0") (h "0qx0r24qn56342m11w5bz2wn8dwzx5ypcp143ppk6lis01kv0aca")))

(define-public crate-parco-1.4.0 (c (n "parco") (v "1.4.0") (h "1lakx14f5i5akz62b8jsfjpkcxzv2c8ylik5gny47rd2p88sl1x0")))

(define-public crate-parco-2.0.0 (c (n "parco") (v "2.0.0") (h "1hjn966n2n56g7y8yh7cvv47d39n3yl1ybzm3lj9xq4scjag7ilm")))

(define-public crate-parco-3.0.0 (c (n "parco") (v "3.0.0") (h "0fwciv5laa35mycphp6fdc8pn7hwyw1gg5pn4fy42040r4wxz2ig")))

(define-public crate-parco-3.0.1 (c (n "parco") (v "3.0.1") (h "1rrphw6c4bkxxqdsbnm97r8rc1i1f1ajqbx8x5lmpzk3xshdhrfj")))

(define-public crate-parco-4.0.0 (c (n "parco") (v "4.0.0") (h "1c62ywlyk6lxqsx15kdrk42vcss5hgis1vhpd5gb584aywnhxfkr")))

(define-public crate-parco-4.1.0 (c (n "parco") (v "4.1.0") (h "1brgh6065vbym94vr4bvn9is49gzli1nq5d2ziywy18a2ddxbk84")))

(define-public crate-parco-5.0.0 (c (n "parco") (v "5.0.0") (h "18w7v8h7j151jilhi08dd804dmcycxdarli944x9gsxkdp69mvyc")))

(define-public crate-parco-5.1.0 (c (n "parco") (v "5.1.0") (h "0z3w57pnsli2ax7c0i9hfph4w0yznbda6qw7jhw8rywklws27ky1")))

