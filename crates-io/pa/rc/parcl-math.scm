(define-module (crates-io pa rc parcl-math) #:use-module (crates-io))

(define-public crate-parcl-math-1.0.0 (c (n "parcl-math") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (d #t) (k 0)))) (h "11ap15w6cvifxj8vx09d64kky0pbyamff4bgdikrnfzhh0rci0g3") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

