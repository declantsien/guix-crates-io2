(define-module (crates-io pa gi paging) #:use-module (crates-io))

(define-public crate-paging-0.0.0 (c (n "paging") (v "0.0.0") (h "055j4a8yzqmcsbxmi6yq8vagmxi9dgs2wk4xwmkyamdz55msckjd")))

(define-public crate-paging-0.1.0 (c (n "paging") (v "0.1.0") (h "1fra3k12fv9qwj91663imnniv5hv1954pwysyyd53yrmay4816nz")))

