(define-module (crates-io pa gi pagino) #:use-module (crates-io))

(define-public crate-pagino-1.0.0 (c (n "pagino") (v "1.0.0") (h "076gl5iap5ljggcfg79wbl2fkfnwgshyyrzs9363z5ysjh99pyia")))

(define-public crate-pagino-1.0.1 (c (n "pagino") (v "1.0.1") (h "1njbbwbssbd43zlzxaaa6q6cymz3vn4xh468bxqf26wg244mmqhg")))

(define-public crate-pagino-1.0.2 (c (n "pagino") (v "1.0.2") (h "1vcidc4432pd7lc5llpl177i359sfpsgll4vsw49zr2wk8iwv2kb")))

(define-public crate-pagino-1.0.3 (c (n "pagino") (v "1.0.3") (h "0w8mf97s5vysygq4imrfqy38c792c5zd3nvxb2bnm84k09kc2bjn")))

(define-public crate-pagino-1.0.4 (c (n "pagino") (v "1.0.4") (h "1nx7synrwd3ysk6nfj1x3cxzxa65vw2i83g5678j4a130yhdlh66")))

(define-public crate-pagino-1.0.5 (c (n "pagino") (v "1.0.5") (h "1rvjddawqhc3zc5bbs7v9108lc8738wmv61dcn28vymqcv2linmd")))

