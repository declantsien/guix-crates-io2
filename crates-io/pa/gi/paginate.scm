(define-module (crates-io pa gi paginate) #:use-module (crates-io))

(define-public crate-paginate-1.0.0 (c (n "paginate") (v "1.0.0") (h "1w8ds7hrfylcdiy6fgag24vjanxhisi6hbgxbj1ln5ng91ckg6hp")))

(define-public crate-paginate-1.0.1 (c (n "paginate") (v "1.0.1") (h "1zfjfhxxyczkx68cdkq6ghqzhaf5b7a7bi3g344y5w08clywqab8")))

(define-public crate-paginate-1.1.0 (c (n "paginate") (v "1.1.0") (h "0sdxi7zpby3dy0wn7anj4cca57fk8278x8bkw53wkxcrrxcsfqar")))

(define-public crate-paginate-1.1.1 (c (n "paginate") (v "1.1.1") (h "0m8472gw87di91fyf3x3yp6ags3qigx905j4rkva1hm1jcz1aj3x")))

(define-public crate-paginate-1.1.2 (c (n "paginate") (v "1.1.2") (h "07h6vrcal3aj18ylm7ynhs6g8jj9w82cdzc6i1mqb0qhxd5qag8a")))

(define-public crate-paginate-1.1.3 (c (n "paginate") (v "1.1.3") (h "1402zlss3r9cykd6zzvsf8rfn17kq4wx8wa4w5xfir8r5k0fnvy5")))

(define-public crate-paginate-1.1.4 (c (n "paginate") (v "1.1.4") (h "0f794c32rab4x8m30cis5f9d7dkmsi3vzsrxj4yq1ahwlfwykp4w")))

(define-public crate-paginate-1.1.6 (c (n "paginate") (v "1.1.6") (h "07v941rih91c59wr30zbzinlswagrzx0adpgc9vfr4df5s9dwfdl")))

(define-public crate-paginate-1.1.7 (c (n "paginate") (v "1.1.7") (h "0sklfvvgkfx966lf2xvfva3crx940068l7qkzrqdli84x56x6pzi")))

(define-public crate-paginate-1.1.8 (c (n "paginate") (v "1.1.8") (h "15440k6y839ddqirhp3iy5nas02m3868s299aks2mlbdd05sy52b")))

(define-public crate-paginate-1.1.9 (c (n "paginate") (v "1.1.9") (h "065j4658kjda5xdivxcljxghb6r6f15wq5wpam33g86ah0389jbi")))

(define-public crate-paginate-1.1.10 (c (n "paginate") (v "1.1.10") (h "1k2873y62fdgphsky3b587d5qr20dpqfm8mwhm0kf94jizi559yn")))

(define-public crate-paginate-1.1.11 (c (n "paginate") (v "1.1.11") (h "1j348rk2i65g9fhqrsr0rl348jcqc7blwx2v8gjchvcxklhdphas")))

