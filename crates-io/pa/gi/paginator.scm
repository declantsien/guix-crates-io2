(define-module (crates-io pa gi paginator) #:use-module (crates-io))

(define-public crate-paginator-0.1.0 (c (n "paginator") (v "0.1.0") (h "1bif9lqqdnliqkbwyb4j5wxmrg5xc1aplpg4jpbsbv53k3g81r7c") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1.1 (c (n "paginator") (v "0.1.1") (h "1dvpcyi88p4z39pvjn4aw54xv44irq8bxch0n51kl62l2hw4k8hw") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1.2 (c (n "paginator") (v "0.1.2") (h "1wiapanndw203g7mlhk9k0r7ghwghnpi7d94mzqmdb5rv5k6sg3x") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1.3 (c (n "paginator") (v "0.1.3") (h "1x4dw4crjp234w5s5zzfz3v39g5gi218v7bwd1gh8giqc2cdh5xx") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1.4 (c (n "paginator") (v "0.1.4") (h "1728wld5x9bwygw8x905l0ikg1df76155akc2srxvnn5sxhip18h") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1.5 (c (n "paginator") (v "0.1.5") (h "1f0kjidwzhg4qiwc8d81cvqkymn0f10d5l0pczrj75q4s9lybrws") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.0 (c (n "paginator") (v "0.2.0") (h "19hmmfz054n8qjsfdb8pj5r46jmxzanafn5mykqq19maibp4mb32") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.1 (c (n "paginator") (v "0.2.1") (h "0fhvdz2y8kb8c0zsx8b7x0k0bn3flcas5bi94lpsk9hy96dcyv5w") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.2 (c (n "paginator") (v "0.2.2") (h "073h652gzdl6ya9craz3ldw15gfwzmy4d9db2gzv6vsd94mh1wcd") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.3 (c (n "paginator") (v "0.2.3") (h "16s6jhq8mzygp0qq6chn39jw9pm0niayqmdgzraqh10893d3d4z1") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.4 (c (n "paginator") (v "0.2.4") (h "1k45d4pcy19aidizsv3d0hvf1nhvalynlkdiidb64fssqfblqbr1") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.5 (c (n "paginator") (v "0.2.5") (h "0bf61zmnirsxwlgz57snk5dhpna3a4d5gw62wra4c8066q0r051x") (f (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2.6 (c (n "paginator") (v "0.2.6") (h "16s2in69ls6yk2jwcy33mbhfmh2im2z2dl0a5ywjb1i1fhhq3awc") (f (quote (("std") ("default" "std"))))))

