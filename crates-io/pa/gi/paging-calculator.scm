(define-module (crates-io pa gi paging-calculator) #:use-module (crates-io))

(define-public crate-paging-calculator-0.1.0 (c (n "paging-calculator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)))) (h "1z4qa3nzqc5ymg687l4yfq0kcw337wmip59xg41s52zh681kl9jz")))

(define-public crate-paging-calculator-0.1.1 (c (n "paging-calculator") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)))) (h "0p90psjhqfkmg8sfra3484crlpwmmnghhsyhidyh6vxv17ymrsxq")))

(define-public crate-paging-calculator-0.1.2 (c (n "paging-calculator") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)))) (h "1x23v9cyci9030lc1f3x1v8a2g9gjld59nv9i4pbifzgx75600vp")))

(define-public crate-paging-calculator-0.2.0 (c (n "paging-calculator") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)))) (h "0nxgssbjx48fp9dwir7sa0yfm5vhjhxl2g9hnknya8lsm2kg4smb")))

(define-public crate-paging-calculator-0.3.0 (c (n "paging-calculator") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)))) (h "0lacjs90yx2k269534ya6dklv8f0bszgvypd15lis56mydk06pxc")))

