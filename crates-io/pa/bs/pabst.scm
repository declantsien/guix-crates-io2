(define-module (crates-io pa bs pabst) #:use-module (crates-io))

(define-public crate-pabst-0.1.0 (c (n "pabst") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "las") (r "^0.3") (d #t) (k 0)) (d (n "rivlib") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sdc") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1ll5a011ililyvkw5hmcbfi7p1rm4hcq7ff1l2f79c9kqa819p4q") (f (quote (("rxp-source" "rivlib"))))))

