(define-module (crates-io pa wn pawn) #:use-module (crates-io))

(define-public crate-pawn-0.9.0 (c (n "pawn") (v "0.9.0") (h "19d5qdjwkd3hzq7p472di70ymbzaplxzxfazdk7c3r4bd22pbiga")))

(define-public crate-pawn-0.9.1 (c (n "pawn") (v "0.9.1") (h "03gal5w1aabvm3v4hhps9cfhkgvl2drja7z1n0k8iaq636jx78dx")))

