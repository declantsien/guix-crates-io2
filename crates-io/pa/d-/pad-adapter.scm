(define-module (crates-io pa d- pad-adapter) #:use-module (crates-io))

(define-public crate-pad-adapter-0.1.0 (c (n "pad-adapter") (v "0.1.0") (h "1b2kfdy652kl9x411hyxv92zngk7h66qpw2aya021r0jxk9h63xr")))

(define-public crate-pad-adapter-0.1.1 (c (n "pad-adapter") (v "0.1.1") (h "0qwbjfpmzrz6nkc577jky5qb980glchxz98h5azfh8b79gy0xn2n")))

