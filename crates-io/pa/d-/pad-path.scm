(define-module (crates-io pa d- pad-path) #:use-module (crates-io))

(define-public crate-pad-path-0.1.0 (c (n "pad-path") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0a4c1hvv7i0afrkmbxr4yab0rpqnj6nfi294h2v5fnr7vdaajrkd")))

(define-public crate-pad-path-0.1.1 (c (n "pad-path") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0xni1cxfzz232qb523fpfzal5d0arbz3d9pc1ng6aal3m8bp0hpv")))

(define-public crate-pad-path-0.1.2 (c (n "pad-path") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "18vhp248dpp4ryw64cfiyb0cigw1s2x8iihc374bwcryaa54l0c8")))

(define-public crate-pad-path-0.1.3 (c (n "pad-path") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "1nyzqc3h16h83s32ybb4d3ic5gcj2snhs1036w0mliin6bjg1vrn")))

(define-public crate-pad-path-0.1.4 (c (n "pad-path") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "19la8qzwlp53wmavq7gvl1207j3lnrjq29dkm04jm54ydskg7qrk")))

(define-public crate-pad-path-0.1.5 (c (n "pad-path") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "1h8bv081rigrl2i22l709d5h2b9pz8wgni9gmip1gkvxxwci7f2k")))

(define-public crate-pad-path-0.1.7 (c (n "pad-path") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "1a2lzaf9lr9aa63p83y71s34lizxhsa8zwqjf46rfg4nnhvmrbc4")))

(define-public crate-pad-path-0.1.8 (c (n "pad-path") (v "0.1.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "13p4snh2kh8d2cnc12x7hc977j2vmarxnqibg6j7lja3czilwk39")))

(define-public crate-pad-path-0.1.9 (c (n "pad-path") (v "0.1.9") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "05zyyly2gy4mzpgph564gjkjvw63hs2wkh74kz73dvppzpnvf3yq")))

(define-public crate-pad-path-0.1.10 (c (n "pad-path") (v "0.1.10") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "0cw92615k5c27qry1qy70djiays4kfzly7qkcgdxv62hn4xl52r0")))

(define-public crate-pad-path-0.2.0 (c (n "pad-path") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)))) (h "1856k3cxlpf6s4c81fzli16ai1hzb0qipg5811fypdpbizciv054")))

(define-public crate-pad-path-0.2.1 (c (n "pad-path") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)))) (h "0w664m5i557mii16sbzpmn7yl5nyfp4gydmczxl29dzi2mr04drm")))

(define-public crate-pad-path-0.2.2 (c (n "pad-path") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1mi1nklzp4f1r0hl4hld7p0cwmkshzy6mipxyzl2qxlgl5clkh9y")))

(define-public crate-pad-path-0.2.3 (c (n "pad-path") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0cpxfqq7qz5izaacqhi0kydih1103l3mjzn1939rdvfmngckgy7s")))

(define-public crate-pad-path-0.2.5 (c (n "pad-path") (v "0.2.5") (d (list (d (n "cargo-make") (r "^0.36.1") (d #t) (k 1)) (d (n "cargo-nextest") (r "^0.9.38") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)))) (h "1iakhq419i19bk661svqyxk05bh23716rvqaq44rbm18la95bbx6")))

(define-public crate-pad-path-0.2.6 (c (n "pad-path") (v "0.2.6") (d (list (d (n "cargo-make") (r "^0.36") (d #t) (k 1)) (d (n "cargo-nextest") (r "^0.9") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)))) (h "00d28r0sf54cxr1w85answfgy4sifv3wv18wa4nb242vsc2030q4")))

(define-public crate-pad-path-0.2.7 (c (n "pad-path") (v "0.2.7") (d (list (d (n "cargo-make") (r "^0.36") (d #t) (k 1)) (d (n "cargo-nextest") (r "^0.9") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2") (d #t) (k 0)))) (h "0l3y6i4gykwmhllgdbpns3p8l0z1nmn2hzfs77g2wjd0zymkiikl")))

