(define-module (crates-io pa gu pagurus_tui) #:use-module (crates-io))

(define-public crate-pagurus_tui-0.7.0 (c (n "pagurus_tui") (v "0.7.0") (d (list (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "00nn23m89kzd1x41wzamjvka5iij7gmn6vsr0sw6jbd41j5zqw84")))

(define-public crate-pagurus_tui-0.7.1 (c (n "pagurus_tui") (v "0.7.1") (d (list (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0l5zi11fk2jx232017m79ip340s8sgiqa18pfkyyhadfgw56kvrm")))

(define-public crate-pagurus_tui-0.7.2 (c (n "pagurus_tui") (v "0.7.2") (d (list (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "03m9f0qgp5f0kk3zrmngzw0zqhz4bvwg965ahk1bar8l1h9dmgyz")))

(define-public crate-pagurus_tui-0.7.3 (c (n "pagurus_tui") (v "0.7.3") (d (list (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "10dchlng6wls9ya5wkilkj3hji8g3skq9lzrlsbvs0x6jkqa5bmd")))

(define-public crate-pagurus_tui-0.7.4 (c (n "pagurus_tui") (v "0.7.4") (d (list (d (n "orfail") (r "^1") (d #t) (k 0)) (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0h4djz4yj0n1pi4pfsar1623qccnh3wck0yp3fjnv69sn8raiph9")))

(define-public crate-pagurus_tui-0.7.5 (c (n "pagurus_tui") (v "0.7.5") (d (list (d (n "libpulse-binding") (r "^2.28.1") (o #t) (d #t) (k 0)) (d (n "libpulse-simple-binding") (r "^2.28.1") (o #t) (d #t) (k 0)) (d (n "orfail") (r "^1") (d #t) (k 0)) (d (n "pagurus") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "12dar8lpm388pbrknhg61r3jzx5fjg76qspwcrd3p2zyapgfnsld") (f (quote (("video" "termion") ("default" "video" "audio") ("audio" "libpulse-binding" "libpulse-simple-binding"))))))

