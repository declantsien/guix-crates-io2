(define-module (crates-io pa gu pagurus_sdl_system) #:use-module (crates-io))

(define-public crate-pagurus_sdl_system-0.1.0 (c (n "pagurus_sdl_system") (v "0.1.0") (d (list (d (n "pagurus") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1s1pxd471g21r5ayrmg9g536wrbf2lyqirnnxz8fgxzdf0kdwnxf")))

(define-public crate-pagurus_sdl_system-0.2.0 (c (n "pagurus_sdl_system") (v "0.2.0") (d (list (d (n "pagurus") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "105k6gpfa1ysj8171sdwfb4q1mnpm6jnb2i263qbx62b2zyp1fn9")))

(define-public crate-pagurus_sdl_system-0.3.0 (c (n "pagurus_sdl_system") (v "0.3.0") (d (list (d (n "pagurus") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0da4pk3rzgq7rfxz3dqgphlakzhqyxhdwj7vmrhgm0z1vha9jvz1")))

(define-public crate-pagurus_sdl_system-0.4.0 (c (n "pagurus_sdl_system") (v "0.4.0") (d (list (d (n "pagurus") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1ysbk6kv0x869r7n7ig395vq17w37bjq81pcxclabbgl76iygxs0")))

(define-public crate-pagurus_sdl_system-0.5.0 (c (n "pagurus_sdl_system") (v "0.5.0") (d (list (d (n "pagurus") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0db8rm1nnnixywd1rczq6iqj91qx06w27kz8psm574zng8nzxvzd")))

(define-public crate-pagurus_sdl_system-0.6.0 (c (n "pagurus_sdl_system") (v "0.6.0") (d (list (d (n "pagurus") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "13pqb5bmk052shicf55yivrdlby8i3k7p2z1lbkw5f9x7v3qw643")))

