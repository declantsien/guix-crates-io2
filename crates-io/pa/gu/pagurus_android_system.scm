(define-module (crates-io pa gu pagurus_android_system) #:use-module (crates-io))

(define-public crate-pagurus_android_system-0.1.0 (c (n "pagurus_android_system") (v "0.1.0") (d (list (d (n "ndk") (r "^0.6") (f (quote ("aaudio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.6") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pagurus") (r "^0.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0x67wy5zdg7c1r9iqfrjnhjb0a29n1rd0x4sljx39155bzc1dphb")))

(define-public crate-pagurus_android_system-0.1.1 (c (n "pagurus_android_system") (v "0.1.1") (d (list (d (n "ndk") (r "^0.6") (f (quote ("aaudio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.6") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pagurus") (r "^0.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0hcy7ida49cpaf6kcvda2dgdq74a646gwglf42c136ysnp7gy6k4")))

(define-public crate-pagurus_android_system-0.2.0 (c (n "pagurus_android_system") (v "0.2.0") (d (list (d (n "ndk") (r "^0.6") (f (quote ("aaudio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.6") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pagurus") (r "^0.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "157lf7gazmlhaj105g88l9s30fln7vv1f8ihwmqvpzrjxm3zk8i3")))

(define-public crate-pagurus_android_system-0.3.0 (c (n "pagurus_android_system") (v "0.3.0") (d (list (d (n "ndk") (r "^0.6") (f (quote ("aaudio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.6") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pagurus") (r "^0.3") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "075ldwb99ajdgsqj358dl0bn4g4j3y4sqmggd87rpdfrn91zz1qb")))

(define-public crate-pagurus_android_system-0.4.0 (c (n "pagurus_android_system") (v "0.4.0") (d (list (d (n "ndk") (r "^0.7") (f (quote ("audio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.7") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4") (d #t) (k 0)) (d (n "pagurus") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0l37lip9b6kk81wam8plqhn3f0bjrbb27aw85b81wcxm05zmyksw")))

(define-public crate-pagurus_android_system-0.5.0 (c (n "pagurus_android_system") (v "0.5.0") (d (list (d (n "ndk") (r "^0.7") (f (quote ("audio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.7") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4") (d #t) (k 0)) (d (n "pagurus") (r "^0.5") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1l7v6ix0r8gvp29vy6ak8j2q5l72sxiplgy145yi3r9p6dvdnm08")))

(define-public crate-pagurus_android_system-0.6.0 (c (n "pagurus_android_system") (v "0.6.0") (d (list (d (n "ndk") (r "^0.7") (f (quote ("audio"))) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.7") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.4") (d #t) (k 0)) (d (n "pagurus") (r "^0.6") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0gsn596zwfj1h8x7xwhp0lwfl45ggrbbylfzchy3ps9dsar05xnc")))

