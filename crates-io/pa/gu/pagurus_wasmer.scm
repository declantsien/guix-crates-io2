(define-module (crates-io pa gu pagurus_wasmer) #:use-module (crates-io))

(define-public crate-pagurus_wasmer-0.1.0 (c (n "pagurus_wasmer") (v "0.1.0") (d (list (d (n "pagurus") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2") (d #t) (k 0)))) (h "0z0izpc6mfsxmvvifrdv02q28q69b74n4gi5nz9a2vbj9xbwqvr9")))

(define-public crate-pagurus_wasmer-0.2.0 (c (n "pagurus_wasmer") (v "0.2.0") (d (list (d (n "pagurus") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2") (d #t) (k 0)))) (h "12vxzkqalj0fh2xr6jcz0jxzbsxrllz0h57lys5p3gpfpk84i683")))

(define-public crate-pagurus_wasmer-0.3.0 (c (n "pagurus_wasmer") (v "0.3.0") (d (list (d (n "pagurus") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2") (d #t) (k 0)))) (h "1znhlhlqdqbvf0arxlj0xn70cxmg9di2sgy49q6hqnys7503zkfw")))

(define-public crate-pagurus_wasmer-0.4.0 (c (n "pagurus_wasmer") (v "0.4.0") (d (list (d (n "pagurus") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2") (d #t) (k 0)))) (h "0xadk5wr0lbw65ffhp0xzjvizc1cbkrygrqsx1vy4h310340jni9")))

(define-public crate-pagurus_wasmer-0.5.0 (c (n "pagurus_wasmer") (v "0.5.0") (d (list (d (n "pagurus") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2") (d #t) (k 0)))) (h "158zsl6l07bddj9r19igl20sibsyj1i1ljv3py834ryymvzzfchh")))

