(define-module (crates-io pa cm pacman) #:use-module (crates-io))

(define-public crate-pacman-0.1.0 (c (n "pacman") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)))) (h "0dsi3d6h5014ly5bfjvjz0qwva1da7qpislvkjizmn464fws2dzj")))

(define-public crate-pacman-0.1.1 (c (n "pacman") (v "0.1.1") (h "16wcc9v5mq750hh7j5z2vdzb4d9xln9qa60m9gfizbmqli0l70m5")))

(define-public crate-pacman-0.1.2 (c (n "pacman") (v "0.1.2") (h "0vd06wb69vz1b9hz6aqr5k05inz2hnx44kicvg9gk93ays6i8w7j")))

(define-public crate-pacman-0.1.3 (c (n "pacman") (v "0.1.3") (h "156vr6pwiw2nvi3bzzqj9m1j9d6dsv7b7w5x7ibhqhl11r1d1iim")))

