(define-module (crates-io pa cm pacmanager_wrapper) #:use-module (crates-io))

(define-public crate-pacmanager_wrapper-0.1.0 (c (n "pacmanager_wrapper") (v "0.1.0") (d (list (d (n "async-process") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dkzqvs0b0mcj8gz12iac7jhcikag14fws0xhshlfvjfwsq0l2j6")))

(define-public crate-pacmanager_wrapper-0.1.1 (c (n "pacmanager_wrapper") (v "0.1.1") (d (list (d (n "async-process") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1yl3fynagbqmwj48wvxvh3h40pllnlp35zf1nhrlv3vlj9r84sa0")))

(define-public crate-pacmanager_wrapper-0.1.2 (c (n "pacmanager_wrapper") (v "0.1.2") (d (list (d (n "async-process") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r0nq7hb896l21i64p2837ywlhx46gpcxggms4sqagl9s27mnymc")))

(define-public crate-pacmanager_wrapper-0.2.0 (c (n "pacmanager_wrapper") (v "0.2.0") (d (list (d (n "async-process") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0754j4c7wpx6ix2vavyb1pn9wrxl65gzq2n7gj83j6yakz09617g")))

