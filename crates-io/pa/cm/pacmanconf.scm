(define-module (crates-io pa cm pacmanconf) #:use-module (crates-io))

(define-public crate-pacmanconf-0.1.0 (c (n "pacmanconf") (v "0.1.0") (d (list (d (n "cini") (r "^0.1") (d #t) (k 0)))) (h "07lmw2q65xgvxjhkmg2i3ddxpx9qmxv31qwj95ja6gn2ll1ip24l")))

(define-public crate-pacmanconf-0.1.1 (c (n "pacmanconf") (v "0.1.1") (d (list (d (n "cini") (r "^0.1") (d #t) (k 0)))) (h "032p6060wvz4rcy3msl8py1jhfi7n9340q6lab486wqaa0q34zjd")))

(define-public crate-pacmanconf-0.1.2 (c (n "pacmanconf") (v "0.1.2") (d (list (d (n "cini") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0fp6laqla6gciwc8i60xhqzi3vy6bxn8v9gnbf5bwdglw16005ds")))

(define-public crate-pacmanconf-0.1.3 (c (n "pacmanconf") (v "0.1.3") (d (list (d (n "cini") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0bj69pyq8gnk1zyhx4ala3wsz1g7cc67fx45j7gsjrq8r76fp4mq")))

(define-public crate-pacmanconf-0.2.0 (c (n "pacmanconf") (v "0.2.0") (d (list (d (n "cini") (r "^0.1") (d #t) (k 0)))) (h "0m4yy8144nb5x4gplqrl2malhv863h9c8l9nvdv05i8bwx11qwxh")))

(define-public crate-pacmanconf-1.0.0 (c (n "pacmanconf") (v "1.0.0") (d (list (d (n "cini") (r "^0.1") (d #t) (k 0)))) (h "1x7yhjpn4ad7f7yzbrr8il40wldk0zw0wk4s8p9xzm9p89g8kfps")))

(define-public crate-pacmanconf-2.0.0 (c (n "pacmanconf") (v "2.0.0") (d (list (d (n "cini") (r "^1.0.0") (d #t) (k 0)))) (h "11b1xhzf5gsnjqss0zimcb0n92id69kpnjf8r1ynb2a5bpxgsr5s")))

(define-public crate-pacmanconf-2.1.0 (c (n "pacmanconf") (v "2.1.0") (d (list (d (n "cini") (r "^1.0.0") (d #t) (k 0)))) (h "1cdv01qx4g5jr733pb43qdyfm9s4mpgqkdxgxrbgqvvwjhgq3n9i")))

