(define-module (crates-io pa cm pacman-bintrans-monitor) #:use-module (crates-io))

(define-public crate-pacman-bintrans-monitor-0.1.0 (c (n "pacman-bintrans-monitor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "pacman-bintrans-common") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("process" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "03yvfx14bq5ndd9zlj0njh0rj2j3zcs3p11rb0klxbvivm1qsfq1")))

(define-public crate-pacman-bintrans-monitor-0.2.0 (c (n "pacman-bintrans-monitor") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "pacman-bintrans-common") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("process" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1hv5hla1hcr23zf7sp2gr8yfa5cz6xh4w4772iclllzqblr61x82")))

