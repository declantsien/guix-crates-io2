(define-module (crates-io pa cm pacmog) #:use-module (crates-io))

(define-public crate-pacmog-0.3.1 (c (n "pacmog") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cpal") (r "^0.14.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.20.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.2") (k 0)))) (h "02mhfhv704f9c1va96rgigf4ksb9qai5s55hy12v7wyn3z7vy9s6") (r "1.66")))

(define-public crate-pacmog-0.4.0 (c (n "pacmog") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "arbitrary-int") (r "^1.2.3") (k 0)) (d (n "cpal") (r "^0.15.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.22.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (k 0)))) (h "0zcni9m2dwc2wfcvcmbhs0fj6l4gvgb56ws8f9ivc9ggmqygvyan") (r "1.66")))

(define-public crate-pacmog-0.4.1 (c (n "pacmog") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "arbitrary-int") (r "^1.2.3") (k 0)) (d (n "cpal") (r "^0.15.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed") (r "^1.23.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (k 0)))) (h "0nigp043hav7i4x51hc05fh7pllw4fcby77yysnj3yj7r2fzn90z") (r "1.66")))

