(define-module (crates-io pa cm pacman-helper) #:use-module (crates-io))

(define-public crate-pacman-helper-0.1.2 (c (n "pacman-helper") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "182h3jglk0z6y6rdfrf308xmmf7ib29wq9pavrclmv440pyqlfjh") (r "1.61.0")))

