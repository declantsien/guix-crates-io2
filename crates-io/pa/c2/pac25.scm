(define-module (crates-io pa c2 pac25) #:use-module (crates-io))

(define-public crate-pac25-0.1.0 (c (n "pac25") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kx2fjw6x51wcyicnzr3i6093zaz8j1j6w0nmalyhrgb8acf7vj7") (f (quote (("rt" "cortex-m-rt/device") ("pac25140") ("default" "critical-section" "rt")))) (s 2) (e (quote (("atomics" "dep:portable-atomic")))) (r "1.65")))

(define-public crate-pac25-0.1.1 (c (n "pac25") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xsdzx7scfabdqqhcb9zv9c1saz9pgrgrnxq46g0rczw7pjszv9s") (f (quote (("rt" "cortex-m-rt/device") ("pac25140") ("default" "critical-section" "rt")))) (s 2) (e (quote (("atomics" "dep:portable-atomic")))) (r "1.65")))

