(define-module (crates-io pa c2 pac2) #:use-module (crates-io))

(define-public crate-pac2-0.1.0 (c (n "pac2") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.10.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.10.1") (d #t) (k 1)))) (h "12z1vpvppcq3zd7wlyx6wn6cfcxcdbvd1wrf2ij08mx81h1xl888")))

