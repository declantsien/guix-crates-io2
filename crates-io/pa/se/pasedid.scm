(define-module (crates-io pa se pasedid) #:use-module (crates-io))

(define-public crate-pasedid-0.2.0 (c (n "pasedid") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "concat-arrays") (r "^0.1.2") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)))) (h "0vcakqxjca2zkj9br51r1kp19fwmhvn3jdkz8yqhk8z74bhx9q6n")))

