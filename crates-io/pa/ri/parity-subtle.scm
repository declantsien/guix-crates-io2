(define-module (crates-io pa ri parity-subtle) #:use-module (crates-io))

(define-public crate-parity-subtle-0.1.0 (c (n "parity-subtle") (v "0.1.0") (d (list (d (n "parity-scale-codec") (r "^2.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "13v4rrqa40ibbinanxqv77zqm76vrhd9aanivhzixgxlh7l3hmq4") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128")))) (y #t)))

(define-public crate-parity-subtle-0.1.1 (c (n "parity-subtle") (v "0.1.1") (d (list (d (n "parity-scale-codec") (r "^2.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)))) (h "144p7z5ph7lmh27q4day4yw04wpgfccn8xfasavspid6x0a6p603") (f (quote (("nightly") ("i128") ("default" "i128")))) (y #t)))

