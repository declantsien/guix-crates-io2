(define-module (crates-io pa ri parity-daemonize) #:use-module (crates-io))

(define-public crate-parity-daemonize-0.1.0 (c (n "parity-daemonize") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1f47gyhqblwhdnvlcyra9jcbkcs8w43m9ys8xczr7bbp6mj7q9pb")))

(define-public crate-parity-daemonize-0.1.1 (c (n "parity-daemonize") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "14m29lw5icl08100vdhhk2hm3dnfa493d80fzq3x7pd3azlsmny6")))

(define-public crate-parity-daemonize-0.2.0 (c (n "parity-daemonize") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1yww84ivrs1235rzq2msf11zk866hgwgq175cgi5z1cck5i7ycwv")))

(define-public crate-parity-daemonize-0.2.1 (c (n "parity-daemonize") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "154smf3mc35138yczpvacqjrqhnyfq2ph8wq8yqxqcfhfq0aswi6") (y #t)))

(define-public crate-parity-daemonize-0.3.0 (c (n "parity-daemonize") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "0xyani3zkrhbarsfl66jricyrss44pllw2na7xqm5zwk4w5r3cb9")))

