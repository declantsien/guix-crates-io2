(define-module (crates-io pa ri parity-runtime) #:use-module (crates-io))

(define-public crate-parity-runtime-0.1.1 (c (n "parity-runtime") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "0fhs1nkx2w5hbqxmxqcprp65hdlrphrlv943m997k0k9jy78s3ki") (f (quote (("test-helpers"))))))

(define-public crate-parity-runtime-0.1.2 (c (n "parity-runtime") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat"))) (k 0)) (d (n "futures01") (r "^0.1") (d #t) (k 0) (p "futures")) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-compat") (r "^0.1") (d #t) (k 0)))) (h "0jzx0vgfrjdp310hq7nv53clzagfszgkwnkkiwj79hhzdp5i8qj8") (f (quote (("test-helpers"))))))

