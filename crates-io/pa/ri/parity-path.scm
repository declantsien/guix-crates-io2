(define-module (crates-io pa ri parity-path) #:use-module (crates-io))

(define-public crate-parity-path-0.1.1 (c (n "parity-path") (v "0.1.1") (h "1xy3z3jmix0f0kyrxrb85jy8cy1rn5xg6dgmvnd5v2fkk47m8qjr")))

(define-public crate-parity-path-0.1.2 (c (n "parity-path") (v "0.1.2") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "09v6nna560x6qfx7d7p89kcywhlgbs0ggv2w02vn2w2j4ampl0kv")))

(define-public crate-parity-path-0.1.3 (c (n "parity-path") (v "0.1.3") (d (list (d (n "home") (r "^0.5.1") (d #t) (k 0)))) (h "13c47mjxyy3kials40k23p26gv52fhpspb6hy4b7rrhg41x34spn")))

