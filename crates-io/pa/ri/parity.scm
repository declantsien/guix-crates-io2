(define-module (crates-io pa ri parity) #:use-module (crates-io))

(define-public crate-parity-0.0.0 (c (n "parity") (v "0.0.0") (h "0a5mcq3csl77lbmic4in8j7k29bx9xn8v13ff0ajypvjd38i5kqa") (y #t)))

(define-public crate-parity-0.1.0 (c (n "parity") (v "0.1.0") (h "0mqngbvbg7vzg786l4k4hvggl0dy59lb8f64k5r49h8is4dar3zc")))

