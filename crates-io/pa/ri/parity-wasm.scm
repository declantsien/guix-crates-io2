(define-module (crates-io pa ri parity-wasm) #:use-module (crates-io))

(define-public crate-parity-wasm-0.2.0 (c (n "parity-wasm") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "1427bpg0nrqqm56wm3ksv2wvgw73h2bm8yabjf1qphd2prkv58h6")))

(define-public crate-parity-wasm-0.3.0 (c (n "parity-wasm") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "12pbb851sg50pkk817pk4ymls9w4bmx65yvyvgbiy52rhmz4llzm")))

(define-public crate-parity-wasm-0.3.3 (c (n "parity-wasm") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "16wd24mcixnn2zs0g3lv9x5sll1n51csc9ydi42xw2w94sd48vq9")))

(define-public crate-parity-wasm-0.4.0 (c (n "parity-wasm") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "0alagb747cs6lv50847fxw1b5jlss82s9z5rhnkwpxs8fvx99fc3")))

(define-public crate-parity-wasm-0.5.0 (c (n "parity-wasm") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0f4v127w85n4yl6j4312jf8ly1cans238c0rlyhna9g1p71h7x6m")))

(define-public crate-parity-wasm-0.5.1 (c (n "parity-wasm") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0hsqj9dk9axg8i3yi4a3vfa4mwmrzlx7xnsd9f60xlz5j2qwxy6a")))

(define-public crate-parity-wasm-0.5.2 (c (n "parity-wasm") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0ciiq7cskh7mcn9ndf9gjw4szf1rw109bw78p87yvw66jcd73xhs")))

(define-public crate-parity-wasm-0.5.3 (c (n "parity-wasm") (v "0.5.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "17p7cvaxnzghf0756j61zb1far7jymjkfwz5z4hpy2alnha6f24g")))

(define-public crate-parity-wasm-0.5.4 (c (n "parity-wasm") (v "0.5.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1mpqzhq4fwnyxf8fcqf4l4fb9iz8r0l9n9mh93izmy297nqcp1rb")))

(define-public crate-parity-wasm-0.5.5 (c (n "parity-wasm") (v "0.5.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1igndrj10gj04k1c1ig3yw42dvd87g1bcbr7j1p6h39zi5bcczax")))

(define-public crate-parity-wasm-0.5.6 (c (n "parity-wasm") (v "0.5.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "040skp031p775qfsc4wphcs8i3bg7icc5wg0lh776dq5xkli7710")))

(define-public crate-parity-wasm-0.6.0 (c (n "parity-wasm") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "05zpkv9768sh3vyc6zp5yn8grliznv431dzqpdaimra4baqai2dv")))

(define-public crate-parity-wasm-0.7.0 (c (n "parity-wasm") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1wdvani161yjr3rvz88lqsv94nj9ljxk557c4j232x8vxrgnbqiw")))

(define-public crate-parity-wasm-0.8.0 (c (n "parity-wasm") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "09cy8p4bib3faazval9gs21s19b5cb3kdf987kv6mqvmnwyr48rb")))

(define-public crate-parity-wasm-0.8.1 (c (n "parity-wasm") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0np88nqjk0mjipj1bv0byqvjv43sks2iyddvnydm94z984h1mpf0")))

(define-public crate-parity-wasm-0.8.2 (c (n "parity-wasm") (v "0.8.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1ivjijp4qz9vnyhjch4m087kchn59pxms657i0kg6fs5lp784vp4")))

(define-public crate-parity-wasm-0.8.3 (c (n "parity-wasm") (v "0.8.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "08wzx8qy8nlwaw6wrbc2hxfq116225rk19pv1isgrha4akz78nbf")))

(define-public crate-parity-wasm-0.8.4 (c (n "parity-wasm") (v "0.8.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0i0ps457kl6paa8b0ii0140lvk9z7g6xndhpza5mgw2pgvlg4d1p")))

(define-public crate-parity-wasm-0.8.5 (c (n "parity-wasm") (v "0.8.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0xz9rplwcvlnqmz14r7q0skmqbky70wxncvnqgciv0br33r0ky8x")))

(define-public crate-parity-wasm-0.8.6 (c (n "parity-wasm") (v "0.8.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1vwbgd75jb5vmnnzf550y1vcm82j6rfn9rmhk3pxlqvl0iw24jfh")))

(define-public crate-parity-wasm-0.9.0 (c (n "parity-wasm") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "00835gvnlga419gb4h8zxzf7h1yy0678rxzdm51jkbv95iyra1vn")))

(define-public crate-parity-wasm-0.9.1 (c (n "parity-wasm") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "088hd5fx14paylq5vmqhjwck8w3i3n35ksyr48viy6iydz8ssrlc")))

(define-public crate-parity-wasm-0.10.0 (c (n "parity-wasm") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0293wxr41351igjj5f6hiy8r3gzpphf3rwzn7zhb2vyvwg0152kp")))

(define-public crate-parity-wasm-0.11.0 (c (n "parity-wasm") (v "0.11.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1m9zm85rkid3lng00f2j4lg14dsy19jxv2kwhi1hlik3ab5xw7bv")))

(define-public crate-parity-wasm-0.12.0 (c (n "parity-wasm") (v "0.12.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "12yp1nbg21yq0nkh91365hbgwqz6r7j9x9rz8ca9qw22y2lycbbs")))

(define-public crate-parity-wasm-0.12.1 (c (n "parity-wasm") (v "0.12.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1simbvk9x7g01w5qfg8sl2s2f6dw6ymxyrdp1az0xkd5in5lq42i")))

(define-public crate-parity-wasm-0.12.2 (c (n "parity-wasm") (v "0.12.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0zv9prlddsnm4asrlrs2zib4l51nw3kiicg5qwyvdyvszxcz7362")))

(define-public crate-parity-wasm-0.12.3 (c (n "parity-wasm") (v "0.12.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0lnxv99rkh23dy9r0v57ng8yj0w0jvpz4014gp9xkirlc6b1vr4v")))

(define-public crate-parity-wasm-0.13.0 (c (n "parity-wasm") (v "0.13.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0gin2qw0dlxxgd580k3y5ihxmva2azmvvv82zp5hb82z0wgzn2aq")))

(define-public crate-parity-wasm-0.14.0 (c (n "parity-wasm") (v "0.14.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0dimmfkh6mc3h4lznxbcw0m3cnvf5rvcf3plxgy9qyrv6yxv5gjg")))

(define-public crate-parity-wasm-0.14.1 (c (n "parity-wasm") (v "0.14.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0mm4lc7y32ygn7q64a7lv9fsxyhpnv21p1ii9l55zbm1xsvv1f34")))

(define-public crate-parity-wasm-0.14.2 (c (n "parity-wasm") (v "0.14.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "11c1x7mc4abwd4j6gdcv690hbm7kxw57bbvjnbw7rwp0i4bk078i")))

(define-public crate-parity-wasm-0.14.3 (c (n "parity-wasm") (v "0.14.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1fwj02gpr71zml476jb4j3ykslnk3qg0iz5h6ydgifql6r102v26")))

(define-public crate-parity-wasm-0.14.4 (c (n "parity-wasm") (v "0.14.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "04dyzwq3pih5pf81k22w9lll2srgzngsa33dny675kkbh8ay7s3q")))

(define-public crate-parity-wasm-0.14.5 (c (n "parity-wasm") (v "0.14.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1hsgigqqdzmf20nmhrgmiwl80kcgq56fmagwfa7bv5kx84c2wl6l")))

(define-public crate-parity-wasm-0.14.6 (c (n "parity-wasm") (v "0.14.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0jqzz81jpdr556dz2qzyi9m39gbar0g0ji3a0w2r322mw5sl0cp6")))

(define-public crate-parity-wasm-0.15.0 (c (n "parity-wasm") (v "0.15.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0pqp55nhqmcrwrjin12l5bvs4vm3mw86vz8zkl1cfci4dhwl0wsb")))

(define-public crate-parity-wasm-0.15.1 (c (n "parity-wasm") (v "0.15.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "06jqhzm29334yjmn1wz55i921z3yh48h1zgdnl1zkbbg5ly29xlm")))

(define-public crate-parity-wasm-0.15.2 (c (n "parity-wasm") (v "0.15.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1y8973snd89c73826ibi97wky3x688q7njlqlp3yyrbxs3w1hbp9")))

(define-public crate-parity-wasm-0.15.3 (c (n "parity-wasm") (v "0.15.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0zd83rlg039drs6nphh08flh6wnc36isqakr3bbvrkw8mn2a2cc4")))

(define-public crate-parity-wasm-0.15.4 (c (n "parity-wasm") (v "0.15.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1xqhnfljbkf2r2d235982g7s8h4z7j1sckkz62xw960raglh2n13")))

(define-public crate-parity-wasm-0.16.0 (c (n "parity-wasm") (v "0.16.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1i1pfabf4vrqy7cfhdfiq6rsp0jlb59djllamdywzhnv9vmycl8m")))

(define-public crate-parity-wasm-0.17.0 (c (n "parity-wasm") (v "0.17.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1p6sqsr5zi9jh02ws6qh7cdjj45vviwy12s01clhm28c39hv5wsp")))

(define-public crate-parity-wasm-0.18.0 (c (n "parity-wasm") (v "0.18.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0mksjkcljy82firan8h7j1d967sx2jmxq5xqk4vqgf0km7ykabph")))

(define-public crate-parity-wasm-0.19.0 (c (n "parity-wasm") (v "0.19.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0p5cvx7hxadfd6k6lyzak0k4mxzbqi5iylnlrix0drb8hwg42nn2")))

(define-public crate-parity-wasm-0.19.1 (c (n "parity-wasm") (v "0.19.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1a8mf7vbwvpay8acv20mzq6rgpqysya7w79wnz91zsnc195s9ns8")))

(define-public crate-parity-wasm-0.19.2 (c (n "parity-wasm") (v "0.19.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0426fcxhzy6nl27mm7sicmndd0lkdcabnrrha08xc4i4nhq6q9z9")))

(define-public crate-parity-wasm-0.19.3 (c (n "parity-wasm") (v "0.19.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1scza5h329l695g14jgq83kwynr8cngay6k0bqxn9mydsqf2fl1x")))

(define-public crate-parity-wasm-0.20.0 (c (n "parity-wasm") (v "0.20.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1cyw946a3dmvz0qdmpz00gmq980ljz8f65ckaq32c48y2sq6aq4h")))

(define-public crate-parity-wasm-0.21.0 (c (n "parity-wasm") (v "0.21.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0b123gc9qy4z8i9z8qqygz60w59dwdgqazkyp6jd9vr4mcdyi8ww")))

(define-public crate-parity-wasm-0.21.1 (c (n "parity-wasm") (v "0.21.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "0x2hxyg0rcjfwq20kij76hjg31lfhx7k3a1g4z3mnbzl4991d24j")))

(define-public crate-parity-wasm-0.22.0 (c (n "parity-wasm") (v "0.22.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "193cbqjwr60vp60ca8ws6wx6avpvw4skj5whkxawk43yfrkjngvs")))

(define-public crate-parity-wasm-0.23.0 (c (n "parity-wasm") (v "0.23.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1nfljzh2f7vj4qyq0p0vvp33f7myrl08v3dbysa6ddrn88flpfmi")))

(define-public crate-parity-wasm-0.24.0 (c (n "parity-wasm") (v "0.24.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0x7w2sykjs74zibdljwgqxr6gpsp3z2scamwf19jbsiq3ydfkdzf")))

(define-public crate-parity-wasm-0.24.1 (c (n "parity-wasm") (v "0.24.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ia855hq9zrmk7m01hc7bnmkkszw6vdmk1iizf30hgv6d49qa998")))

(define-public crate-parity-wasm-0.24.2 (c (n "parity-wasm") (v "0.24.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1gffd72pwmjgyvlh1p5lj396sy366in93c8kabb3z9z2irhkvwiw")))

(define-public crate-parity-wasm-0.25.0 (c (n "parity-wasm") (v "0.25.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1pigl8giwcmfxzh7s2jz011kpc07c845ki360pz0ryjjyn8kr9iv")))

(define-public crate-parity-wasm-0.26.0 (c (n "parity-wasm") (v "0.26.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1k67jl2az74n7d975zils09nw4xjnc8rz9fl1a1h3fq79p4xsqbg")))

(define-public crate-parity-wasm-0.26.1 (c (n "parity-wasm") (v "0.26.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0kkm9i6ia8glp0v3kh66mg4lfpfrbc8zrg6ciwzg735bd5hzgpqj")))

(define-public crate-parity-wasm-0.27.0 (c (n "parity-wasm") (v "0.27.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "10ji0g1cdmgzjjlxlq32z9nq5dcyp8g8ddfpqcfvbcpd7k1ddh0s")))

(define-public crate-parity-wasm-0.27.1 (c (n "parity-wasm") (v "0.27.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "07a163ypw0q51wq00gnhciwff5h9fib2nzjza8ix9jfw2bi1bxr9")))

(define-public crate-parity-wasm-0.27.2 (c (n "parity-wasm") (v "0.27.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1iv32i069f4sfipbwic8y80w4phq8yrgrdqnwy888kpdbyw22g4x")))

(define-public crate-parity-wasm-0.27.3 (c (n "parity-wasm") (v "0.27.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "02pzk7mnxdviny20qzrpd8pzhw33mkrizi3wxa6lq4amqx6a1ipj")))

(define-public crate-parity-wasm-0.27.4 (c (n "parity-wasm") (v "0.27.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "04nd30mvjxzgygrdyz31qf8nsif6wikph485rxdl8r9qq6pcx88v")))

(define-public crate-parity-wasm-0.27.5 (c (n "parity-wasm") (v "0.27.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1hz23pxly91izjb78r8389kz85bvq2clli66cjpsds3wyrqxffm9")))

(define-public crate-parity-wasm-0.27.6 (c (n "parity-wasm") (v "0.27.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1gbcxnffsc6kfpanshdcizdvqy9cji32r6a8kq85ncd0h0mc0kdx")))

(define-public crate-parity-wasm-0.28.0 (c (n "parity-wasm") (v "0.28.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0zimmpgvmxywpx89ij5zgas85swp9df6wscg9spkmnk4sxa79pvr")))

(define-public crate-parity-wasm-0.29.0 (c (n "parity-wasm") (v "0.29.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1582kankx8mz199hm610vj8qfh17kfrvyb37ky233ib8rw5q7pjg") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-wasm-0.30.0 (c (n "parity-wasm") (v "0.30.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1gkshp3n5nm17n6hqx0biznklrcjbwypgln4z80qmfqap1bkj221") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.31.0 (c (n "parity-wasm") (v "0.31.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0h4shkic5465v619w6wwfjclf28xi553scnyra7bgmabs6ci3jg1") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.31.1 (c (n "parity-wasm") (v "0.31.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1sjh0lslyfnik8n7dqqjdrmvh0jmh267ygw7fyj1lp14wsk6y0lg") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.31.2 (c (n "parity-wasm") (v "0.31.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "03al2g1znwx827bzb4vpii9mjnb2dbcpa8l617ln9lsa36px7597") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.31.3 (c (n "parity-wasm") (v "0.31.3") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1z3v16jgqs5f4gknqrbnz6lhiqd7llkscprgblww4c2236l7j4si") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.32.0 (c (n "parity-wasm") (v "0.32.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1b0j0l02fmshhg7rll5pl3nph56j3f32cbmwncwk6d2dgj0m5b8n") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.33.0 (c (n "parity-wasm") (v "0.33.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0b9lvczrlz0m44rqrn6ylcg0gl0c47nyxmnacdnwd8sp3jbcpxpf") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.34.0 (c (n "parity-wasm") (v "0.34.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "07hd7sdlz9iam1cdq2r0acdj8gmf0mfigih5hfnmlx1dsp50h375") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.0 (c (n "parity-wasm") (v "0.35.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0yhhc04cfxkv66zp99s074ly4k3zl8xrabkycv2nsgxx98pxij24") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.1 (c (n "parity-wasm") (v "0.35.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "17hwv1y33l61fly4r75s7scza1b5qb04wfdhfpc6hj9i9lpijzr1") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.2 (c (n "parity-wasm") (v "0.35.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "19k4yg4zn6cd2cgsgisx1wqqlj5zycg3ykpri3j81c21dwrjf3c1") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.3 (c (n "parity-wasm") (v "0.35.3") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0c3d14s73z76npbfilhkbj6mv6b65p63q58j8qp3sgci285wc0sk") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.4 (c (n "parity-wasm") (v "0.35.4") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1v09jc95gzfilij7grfdrncfchw9ppqc64np191wb45fb956azny") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.5 (c (n "parity-wasm") (v "0.35.5") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "18wvazca9arhs8zba3g5x4prjjgjqc7b0g11nhshg4hhwin7nw85") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.6 (c (n "parity-wasm") (v "0.35.6") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "012n3gmsk1nkxf1ldz7d3q6idzz8yzpaqa7zd64cxmghg4yynz38") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.35.7 (c (n "parity-wasm") (v "0.35.7") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "14sx2m1sj20n3m1fywa68apa7fwh5zs8sfkrs1n9nf819rn0f7iy") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-0.36.0 (c (n "parity-wasm") (v "0.36.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0mnl5xvy7y1y9vi6h96qjskpcbnhzs53n1m8ick6hb2m81k83rnp") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-wasm-0.37.0 (c (n "parity-wasm") (v "0.37.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0phwn57mp0kx1q91yd9m79h36mvh5hc4fbwd386g96whrdsvmbhl") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-wasm-0.38.0 (c (n "parity-wasm") (v "0.38.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0qpfwb9adyi6g98q1w0xiqdzkv4r1p7b2w19wd5cr57rlwifbmr0") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-wasm-0.39.0 (c (n "parity-wasm") (v "0.39.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ys7rl6byrrgzah700j3ib1m7mcjanzdp928w70asfxxqm82pkqp") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.40.0 (c (n "parity-wasm") (v "0.40.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0sp1nv5yjw2wpg2j7mjq3mhqqjqb85wmbxrgk27q2zvw7ddqmrky") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.40.1 (c (n "parity-wasm") (v "0.40.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1p84f0k36q05j18jy66n122lyali794cj78hbxgy9wj6si84plqd") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.40.2 (c (n "parity-wasm") (v "0.40.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0lgq2pi40gd1h042z47hqx39fw9hb2czxjrfblsfaswpa4sy7la9") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.40.3 (c (n "parity-wasm") (v "0.40.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "03qycy21avz4970zc7aj8rj5h4wvi4qsrc90a6hpws1a56mglf8y") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.41.0 (c (n "parity-wasm") (v "0.41.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0rfqgjyxrxrfjq5r5n81mdklahl8g4az6yhyyvw25nh0mj6qgz6x") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.42.0 (c (n "parity-wasm") (v "0.42.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "04kmh53hdvr6ggr1b646n3apizppw30xj6gf625q3cjflpjss8cr") (f (quote (("std") ("simd") ("sign_ext") ("multi_value") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.42.1 (c (n "parity-wasm") (v "0.42.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0r35za8qrbgfq44zh8nn9nv5mkkp1bykdmys7vvwajxr6vg9fxyi") (f (quote (("std") ("simd") ("sign_ext") ("multi_value") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.42.2 (c (n "parity-wasm") (v "0.42.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "14mfpzjby2in3i3mlzq2p12yzlgml20pvn166kwasajhcv116pmy") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-parity-wasm-0.45.0 (c (n "parity-wasm") (v "0.45.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "013k2l79sdisylkad175f23a5a8z7vvjmjsg4nqi9nn163zhmbg1") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

