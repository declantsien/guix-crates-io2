(define-module (crates-io pa ri paris) #:use-module (crates-io))

(define-public crate-paris-1.0.0 (c (n "paris") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "0kzyc6imk5rqcx9q135ryn25h1mx0skjrb1fjx4vn7wxp33wh4ix")))

(define-public crate-paris-1.0.1 (c (n "paris") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "18f0gb23w8rprljmjj5h8r8lhf08bwjx32lhv3il0mppk28146la")))

(define-public crate-paris-1.0.2 (c (n "paris") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "16x10xkh2ivkh3mihbjwrrdjfcxkd0lk4jmn5nd1xp94mcrwrspk")))

(define-public crate-paris-1.0.3 (c (n "paris") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "1mgs9m2lnx234p0y21ng6qm5lhrhcl7z3jz5irk6j43v15kxjn45")))

(define-public crate-paris-1.0.4 (c (n "paris") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "0vjxaw41l549m4f83994x9q6farky79bncr8r6dgg1sch8w6d7kz")))

(define-public crate-paris-1.1.0 (c (n "paris") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pjfpizbifbh90v0zzg2j8dwrvpqh7j5b78wnaa4apvxhlvkyq2q")))

(define-public crate-paris-1.2.0 (c (n "paris") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c4vrbjkil2hg6kfgal4kw015wmg27g8kmkxsc8hwma36h77wkl4")))

(define-public crate-paris-1.2.1 (c (n "paris") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09lw63a2kxmm4vmnivszb9n4lmiljsdybwp6gkd1ldxi4yhp7k9g")))

(define-public crate-paris-1.2.2 (c (n "paris") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sph1kcglmglq7hz19qcgfxi5zimm3mqqw1wsf8h4m7fwchhx710")))

(define-public crate-paris-1.2.3 (c (n "paris") (v "1.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1hhb3399vgi8jlkrdzyw07ndk3pbax60vrqlkd1kil589kg6zffj")))

(define-public crate-paris-1.3.0 (c (n "paris") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1r61x4hwqq5ys55nhy0z3iasiapgkss51xk9lvj0iv2lhycq3590") (f (quote (("timestamps" "chrono"))))))

(define-public crate-paris-1.3.1 (c (n "paris") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1syzm4grplj5bviw2whif2zqhfd7qgnh9l97fh1hp21q736kw3qa") (f (quote (("timestamps" "chrono"))))))

(define-public crate-paris-1.4.0 (c (n "paris") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "05qmap14ans78gv62aazb71jwr7qdh8dxmh5p6f5bzzhahlccq36") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.4.1 (c (n "paris") (v "1.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17236w3qnbsdf2criah1nsa7lwg77p7y7y8ziiagx6mfb8pwvg4p") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.4.2 (c (n "paris") (v "1.4.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1z88a7zsv70cav5yqq3bd9s1db6fz7ybgqy8f6vsviix4zp2kp9n") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.4.3 (c (n "paris") (v "1.4.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1hmr50sg53gbvxl4ffd6ryy9j7gil05128c1wf72kvkwzcg6dqbk") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.0 (c (n "paris") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "14v2x15xc7fb9jfyzrd8jhqh17wgpknakbh5gznkicga0c1waqxi") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.1 (c (n "paris") (v "1.5.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "197sk4b54iaycdshjbl1r2dnm9mf0s80m25nxx2hwzl0qigprcr4") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.2 (c (n "paris") (v "1.5.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yhcna9kncc1gqn3shwrfjb4gqfzav4pk5ff78968lz1595v3pma") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.3 (c (n "paris") (v "1.5.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00p459rrgr5jzxd57bi03gxx2fwbyc3zpx8p8vwakaz4m8rzmfg3") (f (quote (("timestamps" "chrono") ("no_logger") ("macros")))) (y #t)))

(define-public crate-paris-1.5.4 (c (n "paris") (v "1.5.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0154xgjhzk498p3irynjn8myjgx3r4k0w5l58w9b732asziiyaah") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.5 (c (n "paris") (v "1.5.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00m8gwmh7bpnaq089k17a7md1wy7di4f80yvdiv25jpi28sm401m") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.6 (c (n "paris") (v "1.5.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0r9wljdpsd05wsq20s1wgslp8adsq4dh5zm6mnqyjs3dr77y9x30") (f (quote (("timestamps" "chrono") ("no_logger") ("macros")))) (y #t)))

(define-public crate-paris-1.5.7 (c (n "paris") (v "1.5.7") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1z5rv044mfn78q25xzkc49b8vvm68m2p8isysinlpibh6s6n00s5") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.8 (c (n "paris") (v "1.5.8") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0rai37xp97pn8ahrnv6ikx7xrvbh1xmnvpfw9ymm4r1ma2y48nx2") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.9 (c (n "paris") (v "1.5.9") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0i8dy41v7malj279snap1z1awpyd7hfr1nc1s7kr2dvn8xhqb786") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.10 (c (n "paris") (v "1.5.10") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0vlnv4yb9farkdbnmmis378r04hq4qsq49bf00k19dvh3jhyflwd") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.11 (c (n "paris") (v "1.5.11") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1mb7vdxww8wfcsd19clcp33dsg0mml37ljvxs95vka5v12i1k7f6") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.12 (c (n "paris") (v "1.5.12") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ni48lgps44grrfj884jqd7znvdvfqz97ism8yppraq849d6jmzm") (f (quote (("timestamps" "chrono") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.13 (c (n "paris") (v "1.5.13") (h "0418ldrgfm2z43j6mwmw989mg9ly3gkbvgkjikrrzpbirlcj7brf") (f (quote (("timestamps") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.14 (c (n "paris") (v "1.5.14") (h "00fbi7aywy5gx72wcdbg2rv2zi8haycszj83553rm5c35blrabii") (f (quote (("timestamps") ("no_logger") ("macros"))))))

(define-public crate-paris-1.5.15 (c (n "paris") (v "1.5.15") (h "0bd6kb8kzbvy1mfk8jfpp0cjzi717q7hdjwjy98phg294cvspv4g") (f (quote (("timestamps") ("no_logger") ("macros"))))))

