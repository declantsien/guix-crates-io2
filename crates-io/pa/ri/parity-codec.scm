(define-module (crates-io pa ri parity-codec) #:use-module (crates-io))

(define-public crate-parity-codec-1.0.0 (c (n "parity-codec") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "03s6mvkz3jp187k6yw5piyddwnzg8lrbdg5nvxrbpkbspdmj2q4j") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-1.1.0 (c (n "parity-codec") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "1p7lpiykll3dfiamrcc4icy2jnz6l44v59a7h7byxjh4mfs90wln") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-1.1.1 (c (n "parity-codec") (v "1.1.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "17pbwzvrq9h8v2hnb625670ms0b6k1jrlafbg1qjz28rzbyfvr5y") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.0 (c (n "parity-codec") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "0b5xk61m60vvnf9kwz96ff48962xm0sbg2j9j2jr34a68aa1vns8") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.1 (c (n "parity-codec") (v "2.0.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "0b9s41jl65z6vkvznjdn6fl15044iv15hll1nknfs81p9hwy0irb") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.2 (c (n "parity-codec") (v "2.0.2") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "18g1p35ppcgwz2rgpqykssc60211gqjlprzhx5jfks0iz9fab4nz") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.3 (c (n "parity-codec") (v "2.0.3") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "1jwyynwhi3cvxin1rjmp37pr0syydb8v16xh2g7ammmpw9pb3i33") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.4 (c (n "parity-codec") (v "2.0.4") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "0p0grxhncsd2ixkajlcmd3rw9hjn6qnnjwy81ysn0jpj0razxp8k") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.0.5 (c (n "parity-codec") (v "2.0.5") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "1mg8qfnkpak983r0gq6bm6v222jmvb5cvp3b0lfgg9nzxakz69is") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-2.1.0 (c (n "parity-codec") (v "2.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pzw58q0adnlsld70jml2m54bpdanxi6k3p0jc1zli0j3b9wyswn") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.1.1 (c (n "parity-codec") (v "2.1.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vc0anw0v912rx9rka2cmd0w8f88ciky1h5c0pi9v9l9wxmkq9sd") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.1.2 (c (n "parity-codec") (v "2.1.2") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11js3yz8f012pqvf2n50ckncn7p0lqsgjjdr8iazs1xfb7az82f7") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.1.3 (c (n "parity-codec") (v "2.1.3") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qb4d24vgh6gjykwi4kk3s8miw1z144i6r54fpjf83f8b0qxvnwq") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.1.4 (c (n "parity-codec") (v "2.1.4") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s01pjn4qzkk0qcqzi6q42xsa1l7sxk7fb70g79ywdxj8dj8ai19") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.1.5 (c (n "parity-codec") (v "2.1.5") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0k6b3094b5dzw28jl8j2h0krv34a98mpwkz55jdwhchnbvm8k8yw") (f (quote (("std" "serde") ("default" "std"))))))

(define-public crate-parity-codec-2.2.0 (c (n "parity-codec") (v "2.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^2.2") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^2.2") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15s0sihp596avg8hrr5rk1x6hqihvv78wczknnxsd2p71wls3dp7") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-2.3.0 (c (n "parity-codec") (v "2.3.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^2.2") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^2.2") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1b5vpgg5pfx70davy6m1ah1zdci4ryhvi9qyhfld9r352cirz7ll") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std")))) (y #t)))

(define-public crate-parity-codec-3.0.0 (c (n "parity-codec") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.0") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.0") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1m5w0a5n9d9r3wihfjxld5pqb2f01xh8daivmk76swvv6629kxl8") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.1.0 (c (n "parity-codec") (v "3.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.1") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.1") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hamrz40s1yh1bnjw0ypgazwdjjbncxksvjd8pqjf7p365wd5937") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.2.0 (c (n "parity-codec") (v "3.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.1") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.1") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01r95cavkb3y6cnd6ikn1y97iq96c6n8rwjgjs1ysw9wcahw7j91") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.3.0 (c (n "parity-codec") (v "3.3.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.2") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.2") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04v7rhxsbwrz277b81wrjkimdjzavhl7namwm6sc6x9yjyib4cq3") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.4.0 (c (n "parity-codec") (v "3.4.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f0f00014x6dl0db9ir0xg2krr7animmi6a2a9zpp71vmz6q1p9f") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.5.0 (c (n "parity-codec") (v "3.5.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ljxwdqbb584y0y6zzhs7z85rq82zrjffvs82h12n7zrcig8ypw5") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std")))) (y #t)))

(define-public crate-parity-codec-3.5.1 (c (n "parity-codec") (v "3.5.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0dp3jx0dgimnpaaiwnwxy4ihy7nsjibg2yrjlx73ph3izc2krd6w") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-3.5.2 (c (n "parity-codec") (v "3.5.2") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1x6lmmlk10hy203drv1s60fxhbvm5z8pgkafgbbyv0790wxycba5") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast")))) (y #t)))

(define-public crate-parity-codec-4.0.0 (c (n "parity-codec") (v "4.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1yfg0dpby6l8ja3pbkdfm986n6a85zrwrpj711rblybapp1fvvjy") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

(define-public crate-parity-codec-4.1.0 (c (n "parity-codec") (v "4.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "049wv4nqlcwv4ylnfxkybgsv44nggcmx31mbiqag4wbdqafz9xw2") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

(define-public crate-parity-codec-3.5.3 (c (n "parity-codec") (v "3.5.3") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12scb3wl6g5nhh88n21pzzy6nrcgf6afsjr9dnmc6vnmlqgqz5rq") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std")))) (y #t)))

(define-public crate-parity-codec-3.5.4 (c (n "parity-codec") (v "3.5.4") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jgy1a5zcap6s9bn3sw1fh9w5b21jc7b7michpc45x8964lg379b") (f (quote (("std" "serde") ("full") ("derive" "parity-codec-derive") ("default" "std"))))))

(define-public crate-parity-codec-4.1.1 (c (n "parity-codec") (v "4.1.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0v4hq1wiw82kzppma3vlgbk530lki63r3h8iyabihhrvknrxw0kr") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

(define-public crate-parity-codec-4.1.2 (c (n "parity-codec") (v "4.1.2") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01716m6ybvamrxqvj85zdncd0baj1kkcqfnn4gahdvhjbid6hpi4") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

(define-public crate-parity-codec-4.1.3 (c (n "parity-codec") (v "4.1.3") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1b7jrz39q3cdwpm2amp8f758s5rn5didn8i9dbk0vygdariz0019") (f (quote (("std" "serde" "bitvec/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

(define-public crate-parity-codec-4.1.4 (c (n "parity-codec") (v "4.1.4") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "bitvec") (r "^0.11") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "byte-slice-cast") (r "^0.3.2") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (o #t) (k 0)) (d (n "parity-codec-derive") (r "^3.3") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0mdx99yigajp1vrryr68khg1vnn43phs9hckm9f28jffaj60w6kw") (f (quote (("std" "serde" "bitvec/std" "byte-slice-cast/std") ("full") ("derive" "parity-codec-derive") ("default" "std") ("bit-vec" "bitvec" "byte-slice-cast"))))))

