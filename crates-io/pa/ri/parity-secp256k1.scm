(define-module (crates-io pa ri parity-secp256k1) #:use-module (crates-io))

(define-public crate-parity-secp256k1-0.6.0 (c (n "parity-secp256k1") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 1)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "15aa5x164xkwrrhfiy27hbzi11g4ca3hjqjh5r5ff09v8d8wzc4i") (f (quote (("unstable") ("dev" "clippy") ("default")))) (y #t)))

(define-public crate-parity-secp256k1-0.7.0 (c (n "parity-secp256k1") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 1)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "1haz5q20iafjjgqrh5fihp2bp88qlaj4kfdfpn5pxqyazj14zjjg") (f (quote (("unstable") ("dev" "clippy") ("default")))) (y #t)))

