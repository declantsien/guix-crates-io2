(define-module (crates-io pa ri parity-map) #:use-module (crates-io))

(define-public crate-parity-map-0.1.0 (c (n "parity-map") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "02gc9xijlbpjddld054099sbzkd5via34qwk8dla9ja0kvjzcb3k")))

(define-public crate-parity-map-0.1.1 (c (n "parity-map") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0cq79zlqr6vj4s16h8dkzx679lw2hzm7ck2abzxmgqrmfx73bsl3")))

