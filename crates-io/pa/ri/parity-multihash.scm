(define-module (crates-io pa ri parity-multihash) #:use-module (crates-io))

(define-public crate-parity-multihash-0.1.0 (c (n "parity-multihash") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "0xjhv5fn2qck7wgcj36i4m66w7agbbf2989pwchpipnchw1ap3iy")))

(define-public crate-parity-multihash-0.1.1 (c (n "parity-multihash") (v "0.1.1") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "00dgl3mm9s6q0jpxpg3d9wgf4nxvyqv4kpfqgglajd5b0y7admh5")))

(define-public crate-parity-multihash-0.1.2 (c (n "parity-multihash") (v "0.1.2") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "19cgks78qkv8agjxkzxds4b3av8l5pn1z62qsr22rr851j53b0zb")))

(define-public crate-parity-multihash-0.1.3 (c (n "parity-multihash") (v "0.1.3") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "1qscxxa5590chl640mfmlvhfv9mbkf60zsw79ygdk3w44zf1ffnz")))

(define-public crate-parity-multihash-0.1.4 (c (n "parity-multihash") (v "0.1.4") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "1x19wn68vqk7ykw0a6p40ssxm93va2sjg8d5f75y80lyjczx03il") (y #t)))

(define-public crate-parity-multihash-0.2.0 (c (n "parity-multihash") (v "0.2.0") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "1p9c743gbawa8f31cakqq6i62h23nvpj7s3rcv753pbjb22ss367")))

(define-public crate-parity-multihash-0.2.1 (c (n "parity-multihash") (v "0.2.1") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.3") (d #t) (k 0)))) (h "14njcsb6y7yc5xab65afiyymp649v31viir95jpgbgsibsqdg93h")))

(define-public crate-parity-multihash-0.2.2 (c (n "parity-multihash") (v "0.2.2") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.3") (d #t) (k 0)))) (h "03c04p6829ln1gg65c9yl3hmfnjd6g9ln5bp3c3ca8d0sfxl47xi")))

(define-public crate-parity-multihash-0.2.3 (c (n "parity-multihash") (v "0.2.3") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.3") (d #t) (k 0)))) (h "0xgp8rp56arcf5mavj7xhj37378116r9ylpcgcv826rr0axd473s")))

