(define-module (crates-io pa ri parity-rocksdb-sys) #:use-module (crates-io))

(define-public crate-parity-rocksdb-sys-0.5.0-beta.0 (c (n "parity-rocksdb-sys") (v "0.5.0-beta.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1.0-beta") (d #t) (k 0)))) (h "1zvyvggn07vb711b8i3kcalvw08va8dnswccn50g2ns5v99ysax4") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.0 (c (n "parity-rocksdb-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "0kwljpl2iq26zf2vih9lk5pibqks27ibh764v7ypy0dv9w42i2gv") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.1 (c (n "parity-rocksdb-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "05i1bxjl9i5imfdl27nxf8yl4nfanwqhvxbafl8gbb2kl5cv7ych") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.2 (c (n "parity-rocksdb-sys") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "17z4il1pzd898khxqnrsxw4zq1xqjqclg7y1awcm8n97zd5pvq4s") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.3 (c (n "parity-rocksdb-sys") (v "0.5.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "0ldb7h97pqclyy9rjh2p67sh7m5di5llzrqvryk985024fjfsn8f") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.4 (c (n "parity-rocksdb-sys") (v "0.5.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "1dapciz4plllyjfnasd913xdn9av5wmfbhgypqapdnqm04500wn1") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.5 (c (n "parity-rocksdb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "0kqwk13i2p5lpajsbl6l0j2mq67f5mky5w30chpm4s67c8929fsv") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-parity-rocksdb-sys-0.5.6 (c (n "parity-rocksdb-sys") (v "0.5.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "0msz9nwhc5jrzx5rfjfkqibzpbxadzjjwdrq0r80hf1zqswfd0cm") (f (quote (("default")))) (l "rocksdb")))

