(define-module (crates-io pa ri parity-bytes) #:use-module (crates-io))

(define-public crate-parity-bytes-0.1.0 (c (n "parity-bytes") (v "0.1.0") (h "037x2zck2favb421jdfba3f6b4xwa4pv7zy69dg87ws1rys6hlgs")))

(define-public crate-parity-bytes-0.1.1 (c (n "parity-bytes") (v "0.1.1") (h "02p3sxml1djkilzb3nfjha15p99kcyh4jbg0g4jqqfrkqmv6s9qc") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-bytes-0.1.2 (c (n "parity-bytes") (v "0.1.2") (h "0rww406dih6yxr4iadz7l07sibklqywxz10gjzdqn4r04hx6xd8n") (f (quote (("std") ("default" "std"))))))

