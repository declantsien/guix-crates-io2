(define-module (crates-io pa ri parity-hash) #:use-module (crates-io))

(define-public crate-parity-hash-1.0.0 (c (n "parity-hash") (v "1.0.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1x9pclc76hgkrfvwp9in0z1g67cjlr7jbfvvbachyylfgg9x9yr9") (f (quote (("std" "bigint/std") ("default" "std"))))))

(define-public crate-parity-hash-1.0.1 (c (n "parity-hash") (v "1.0.1") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "15k764m3x5r913z2g52vpyyadccbrx7nddjakmfygs7v81gsmwvh") (f (quote (("std" "bigint/std") ("default" "std"))))))

(define-public crate-parity-hash-1.1.0 (c (n "parity-hash") (v "1.1.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "04xjsvx8mxiqr4m6pjai4fhp6aigjrs04lnpx5pmpf1l17043mzx") (f (quote (("std" "bigint/std") ("default" "std"))))))

(define-public crate-parity-hash-1.2.0 (c (n "parity-hash") (v "1.2.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "085hr19g3khm619la0qyrxf2zjzfp4bqgq72msvwdm8xfh8h02dd") (f (quote (("std" "bigint/std") ("serialize" "serde" "serde_derive") ("default" "std"))))))

(define-public crate-parity-hash-1.2.2 (c (n "parity-hash") (v "1.2.2") (d (list (d (n "rustc-hex") (r "^2.0") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "uint") (r "^0.3") (d #t) (k 0)))) (h "1a0hb3dm5lmcayvmpagn6mgalnxwwq243ia7rgw8ryjn0mb98n5z") (f (quote (("std" "uint/std") ("serialize" "serde" "serde_derive") ("default" "std"))))))

