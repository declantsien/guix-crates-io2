(define-module (crates-io pa ri parity-snappy-sys) #:use-module (crates-io))

(define-public crate-parity-snappy-sys-0.1.0-beta.0 (c (n "parity-snappy-sys") (v "0.1.0-beta.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0crviix2vbgjallskf8mf82mbh64z37839bmw11xsf52sf85q559") (f (quote (("default")))) (l "snappy")))

(define-public crate-parity-snappy-sys-0.1.0 (c (n "parity-snappy-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iaca67b5a3gb3rmxgyhvjjmx2s19ca12rby8riwc4g30xv5bdfw") (f (quote (("default")))) (l "snappy")))

(define-public crate-parity-snappy-sys-0.1.1 (c (n "parity-snappy-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08zl74vlj0aix79rk0ykym9ir3l8ymj1qzhdnyf2hy8cqjm6q262") (f (quote (("default")))) (l "snappy")))

(define-public crate-parity-snappy-sys-0.1.2 (c (n "parity-snappy-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06vgws1q7gcq05bn6wwaikxxyyd2wjc2k6fyr4h774p1wm8ksh8s") (f (quote (("default")))) (l "snappy")))

