(define-module (crates-io pa ri parity-rocksdb) #:use-module (crates-io))

(define-public crate-parity-rocksdb-0.5.0-beta.0 (c (n "parity-rocksdb") (v "0.5.0-beta.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-rocksdb-sys") (r "^0.5.0-beta") (d #t) (k 0)))) (h "1kib74i7x46y3p1yvs66j5iyzh70vmfmknz4fd2bd1k72fdi0pz5") (f (quote (("valgrind") ("default"))))))

(define-public crate-parity-rocksdb-0.5.0 (c (n "parity-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "02z0mdpnjc72bm6fsp7r2hp40akxrjd2rx8w0agwj3h0svbd4mfd") (f (quote (("valgrind") ("default"))))))

(define-public crate-parity-rocksdb-0.5.1 (c (n "parity-rocksdb") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-rocksdb-sys") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1a5m7s1jjkjq4pm7m30yhyc4yxphwcaqdx7k881bf90fckvcl5rd")))

