(define-module (crates-io pa ri parity-codec-derive) #:use-module (crates-io))

(define-public crate-parity-codec-derive-1.0.0 (c (n "parity-codec-derive") (v "1.0.0") (d (list (d (n "parity-codec") (r "~1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "09w8gdfayv1rb9mvxynsz7ar5ggr16k4g82cx8d2chn3hbbn9nhy") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.0 (c (n "parity-codec-derive") (v "2.0.0") (d (list (d (n "parity-codec") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1ibc9b5c6gpj54smv98458pc8svgviv43x9f0qpwxgl7p5mlr2yv") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.1 (c (n "parity-codec-derive") (v "2.0.1") (d (list (d (n "parity-codec") (r "^2.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "185vhpg6q307fpmqp4gdxacy9j49miaj8lybdzg80f0wyjajin4a") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.2 (c (n "parity-codec-derive") (v "2.0.2") (d (list (d (n "parity-codec") (r "^2.0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1fh3359jkif43r402qyvbpvz8njyf2f6api5sjdm1qpf1hr5ray1") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.3 (c (n "parity-codec-derive") (v "2.0.3") (d (list (d (n "parity-codec") (r "^2.0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0zfn14zbxavzcixz28c9wv6l6956nyld48wxxqyi74dsifbg4l5j") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.4 (c (n "parity-codec-derive") (v "2.0.4") (d (list (d (n "parity-codec") (r "~2.0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0ibr7vaalyvb3305x4bflvbfk11nj1390cwgx830qgfpipfpcfb9") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.0.5 (c (n "parity-codec-derive") (v "2.0.5") (d (list (d (n "parity-codec") (r "~2.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0snijnpgqf2rg1j406s28gjyyshz6wqrzr90600cwbnbz7x392cm") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.1.0 (c (n "parity-codec-derive") (v "2.1.0") (d (list (d (n "parity-codec") (r "~2.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "12f1v99ijk60klicrwnpyi6syd5p9k4yh9jvqw90pdlknhn2r97z") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.2.0 (c (n "parity-codec-derive") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "06dz7d41xm3sjj7yhx2y26ml3gpch9m67561qwwvkj1p1x9jyqz2") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-2.3.0 (c (n "parity-codec-derive") (v "2.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1pwcc86a0vgm6nn0sw07drdvxx1w0dlfc439vn2q1xyq663mfz23") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-parity-codec-derive-3.0.0 (c (n "parity-codec-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zydkb65clmh6rk18d39cmz7dp6kc4wmib3347694psr24ra72x5") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-3.1.0 (c (n "parity-codec-derive") (v "3.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1y9dq5xdbkivalh45dh1x256fazsd9bvc4dmsvq3h2wcnmk9ykl6") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-3.2.0 (c (n "parity-codec-derive") (v "3.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "19ss69cn6dcfmb95hwhwhzkr49hmwmj0wgpfriwa80jc3czyp45y") (f (quote (("std") ("default" "std"))))))

(define-public crate-parity-codec-derive-3.3.0 (c (n "parity-codec-derive") (v "3.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0v05wj777937fqxw8kr790g1qmw23x5kca795p5xv0ik73yqd900") (f (quote (("std") ("default" "std"))))))

