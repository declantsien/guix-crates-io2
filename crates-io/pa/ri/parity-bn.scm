(define-module (crates-io pa ri parity-bn) #:use-module (crates-io))

(define-public crate-parity-bn-0.4.4 (c (n "parity-bn") (v "0.4.4") (d (list (d (n "bincode") (r "^0.6") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "^1.0") (f (quote ("i128"))) (d #t) (k 0)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "10axm0vi05g13qn6k2x68pxwr7f169bxrnql5v9k81f9f8v8493f") (f (quote (("default" "rustc-serialize"))))))

