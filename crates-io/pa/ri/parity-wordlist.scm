(define-module (crates-io pa ri parity-wordlist) #:use-module (crates-io))

(define-public crate-parity-wordlist-1.0.0 (c (n "parity-wordlist") (v "1.0.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1lmxjx02ivp14lm2hhfbiwni7bizkp4l9xpw63pcp2lm3nqrlxq7")))

(define-public crate-parity-wordlist-1.0.1 (c (n "parity-wordlist") (v "1.0.1") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "139l6gpzv9fn4yvzi6zvfvajsc2grsys33gwy1zgzxslfxqjs52j")))

(define-public crate-parity-wordlist-1.1.0 (c (n "parity-wordlist") (v "1.1.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19mb7m0jlbl4l0jn9naf5jq336sknlya22jazkw8dl81n7x1nic1")))

(define-public crate-parity-wordlist-1.2.0 (c (n "parity-wordlist") (v "1.2.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14z6vhh7fh94a11c06qznka18gcmxv5ldd385wa5p13q8h9fq38x")))

(define-public crate-parity-wordlist-1.2.1 (c (n "parity-wordlist") (v "1.2.1") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0k2i4x9z9y7bs08ap9kbca1g9nl2xcnv89m52va4m3yrxcpi04yg")))

(define-public crate-parity-wordlist-1.3.0 (c (n "parity-wordlist") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0gkz8pbf4j0nv499qnil4vdxivnsnljjgpn1vby6z2mwsgq0hgap")))

(define-public crate-parity-wordlist-1.3.1 (c (n "parity-wordlist") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "182l3lb3mrbswsmhdwbzzfw1h7l89c9q3ai27yi80kqmdj4v2npl")))

