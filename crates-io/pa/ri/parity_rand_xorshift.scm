(define-module (crates-io pa ri parity_rand_xorshift) #:use-module (crates-io))

(define-public crate-parity_rand_xorshift-0.1.0 (c (n "parity_rand_xorshift") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "parity-scale-codec") (r "^2.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "03abwrj9jxnn3fs4lnf32cw6zy6ddcr100ch4cv5ppl0608r0p12") (f (quote (("serde1" "serde")))) (y #t)))

