(define-module (crates-io pa ri parity_clippy_utils) #:use-module (crates-io))

(define-public crate-parity_clippy_utils-0.1.73 (c (n "parity_clippy_utils") (v "0.1.73") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "0x7v5rh40m1z70cbdmab62q9253b121n4rv4p1h655w6q3drw6jb") (f (quote (("internal") ("deny-warnings"))))))

