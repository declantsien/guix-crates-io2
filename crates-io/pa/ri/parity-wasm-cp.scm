(define-module (crates-io pa ri parity-wasm-cp) #:use-module (crates-io))

(define-public crate-parity-wasm-cp-0.1.0 (c (n "parity-wasm-cp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0mf2wb84akiy05r7kv83fxpv8rclpnq2z5pmc3362plgig78aayl") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.1 (c (n "parity-wasm-cp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1f6ibj71rca9ky76zk458y8glyy5qk9vsgmc3w6mccjqn16g2n9n") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.2 (c (n "parity-wasm-cp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0sjznwqdbv484dips5cv3p3z8drb4al8hbc0wzjcx5mjwcb7dcng") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.3 (c (n "parity-wasm-cp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0b12zmpy4zz2s57v8aqs3ck7zyrcgsis9hpmnhvzl9zbszdng9kd") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.4 (c (n "parity-wasm-cp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "17rmq5230bjsli50kf28ys5h7wb95j0pd2sv5ih7fihnd50d377s") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.5 (c (n "parity-wasm-cp") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "03r0sc5jy9jgm3rn9b8wh03b9cprj3nnh4kvaf2vsc8zwqpg9763") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-parity-wasm-cp-0.1.6 (c (n "parity-wasm-cp") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1s7hazhg88xfr50kljyd5ngl70i5jcvjm68cjf34iljhyimm3yys") (f (quote (("std" "byteorder/std") ("default" "std"))))))

