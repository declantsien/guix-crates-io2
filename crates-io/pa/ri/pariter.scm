(define-module (crates-io pa ri pariter) #:use-module (crates-io))

(define-public crate-pariter-0.5.0 (c (n "pariter") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0jixig0v1xzcxng7yf8sqw9wampw5l6h35qyirn0x69kjqbsv2m6")))

(define-public crate-pariter-0.5.1 (c (n "pariter") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1ng34zrn8y0sy5lc1yarz51vd2q2z1020an9mk071wmmwywn4jij")))

