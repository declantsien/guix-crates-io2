(define-module (crates-io pa ri parity-util-mem-derive) #:use-module (crates-io))

(define-public crate-parity-util-mem-derive-0.1.0 (c (n "parity-util-mem-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1chhs58il2awl2blyvg7l39zlpjz5701j5j7474hg2i6dlnc6mzm")))

