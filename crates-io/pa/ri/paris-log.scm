(define-module (crates-io pa ri paris-log) #:use-module (crates-io))

(define-public crate-paris-log-1.0.0 (c (n "paris-log") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("no_logger"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "03fkf9cdg676jvfl7rd0xzqxicf27jk00m6p6r23yql1sis2fgil") (f (quote (("icons"))))))

(define-public crate-paris-log-1.0.1 (c (n "paris-log") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("no_logger"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "0dcwr9grljck6fnmhfkmc9520p5a99534dgkq2gdrdzv445dzx6r") (f (quote (("icons"))))))

(define-public crate-paris-log-1.0.2 (c (n "paris-log") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("no_logger"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "1syp82r34s9kc7m3qm5iy1lhamwzkmw34sxf5x39dvpy01nbwkbp") (f (quote (("icons"))))))

