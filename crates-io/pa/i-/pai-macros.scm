(define-module (crates-io pa i- pai-macros) #:use-module (crates-io))

(define-public crate-pai-macros-0.1.0 (c (n "pai-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "124kj42br9nyf65i760kh32avqdkfwzlmm24fvgi6sdpsk21m8bh")))

(define-public crate-pai-macros-0.1.1 (c (n "pai-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "19nbgf7rzfzqvjv1d3ys8nq763320fdnm0ihgj19ilp06a8by38v")))

(define-public crate-pai-macros-0.1.2 (c (n "pai-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "13s4f6vrs7klqm23gx139rnpj2iznrsdvylh6shhn2kgw1fid2ab")))

