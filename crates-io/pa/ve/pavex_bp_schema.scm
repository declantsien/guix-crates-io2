(define-module (crates-io pa ve pavex_bp_schema) #:use-module (crates-io))

(define-public crate-pavex_bp_schema-0.1.0 (c (n "pavex_bp_schema") (v "0.1.0") (h "0pk2x6bjppnm4gph3c14981wbyzqihbc22667rqza8lb7drzg398") (y #t)))

(define-public crate-pavex_bp_schema-0.1.9 (c (n "pavex_bp_schema") (v "0.1.9") (d (list (d (n "pavex_reflection") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04fy5zgfw148fm1x1i62gkxhd56zi1j2nxxas5slyz8i2yxjhzy4") (y #t)))

(define-public crate-pavex_bp_schema-0.1.10 (c (n "pavex_bp_schema") (v "0.1.10") (d (list (d (n "pavex_reflection") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h1hadxf54z3waf2jjbrndiqs6nl4z8cnnzpvhm57d688rfqf21b") (y #t)))

(define-public crate-pavex_bp_schema-0.1.11 (c (n "pavex_bp_schema") (v "0.1.11") (d (list (d (n "pavex_reflection") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ar6rbjby9zf040z3728svvgin02dgv0vnqinrrcpsjks9qa2j33")))

(define-public crate-pavex_bp_schema-0.1.12 (c (n "pavex_bp_schema") (v "0.1.12") (d (list (d (n "pavex_reflection") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lqjd2blqadb2l2qqhkcjclwy5xwi7sdikhp22qbj791rxd4zjia")))

(define-public crate-pavex_bp_schema-0.1.13 (c (n "pavex_bp_schema") (v "0.1.13") (d (list (d (n "pavex_reflection") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06sg15a8dxzwvn8a6lc8mf7j58fdf06b0cjpf1fy9r3x73z37s9s")))

(define-public crate-pavex_bp_schema-0.1.14 (c (n "pavex_bp_schema") (v "0.1.14") (d (list (d (n "pavex_reflection") (r "^0.1.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hjm5px76cl8b14v2cpg7q7yw2d1z59jq97vn7a02k5z1zhxjc6b")))

(define-public crate-pavex_bp_schema-0.1.15 (c (n "pavex_bp_schema") (v "0.1.15") (d (list (d (n "pavex_reflection") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gnvflrddzdafmkl594cr8bsmf01z4c1vnxzlfynix8p02z2y6z8")))

(define-public crate-pavex_bp_schema-0.1.16 (c (n "pavex_bp_schema") (v "0.1.16") (d (list (d (n "pavex_reflection") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r9y5fw0rfldwscw1gsh6x79n2miw7c1pnbv0w0247mhsyhp544n")))

(define-public crate-pavex_bp_schema-0.1.17 (c (n "pavex_bp_schema") (v "0.1.17") (d (list (d (n "pavex_reflection") (r "^0.1.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bcqycac7mlhy5536nbkafyrqdb1sv86fa7ij5zq3iwccvy41bw8")))

(define-public crate-pavex_bp_schema-0.1.18 (c (n "pavex_bp_schema") (v "0.1.18") (d (list (d (n "pavex_reflection") (r "^0.1.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00gjwswk5mx99kz77zwfii74jddgy2a352v48627fkx6wqi2y7df")))

(define-public crate-pavex_bp_schema-0.1.19 (c (n "pavex_bp_schema") (v "0.1.19") (d (list (d (n "pavex_reflection") (r "=0.1.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nvzf7vafhll0bawq1q9gil9vjdqnzs3m6dvrfbwhlwf212mqqmq")))

(define-public crate-pavex_bp_schema-0.1.20 (c (n "pavex_bp_schema") (v "0.1.20") (d (list (d (n "pavex_reflection") (r "=0.1.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "050xv6f8y5lbbyhanj8ks79105zpx28ij809w7ydjgqblrmpp5a2")))

(define-public crate-pavex_bp_schema-0.1.21 (c (n "pavex_bp_schema") (v "0.1.21") (d (list (d (n "pavex_reflection") (r "=0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ll75qkp8nqh75rblk1gl3hqr2mad90xd34kf7r9hhph1x2zy7sz")))

(define-public crate-pavex_bp_schema-0.1.22 (c (n "pavex_bp_schema") (v "0.1.22") (d (list (d (n "pavex_reflection") (r "=0.1.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "154cnhv2k9aphzmz9nqzvzz5liw4bnjgg7xdd83hdf3m9cgjhvz2")))

(define-public crate-pavex_bp_schema-0.1.23 (c (n "pavex_bp_schema") (v "0.1.23") (d (list (d (n "pavex_reflection") (r "=0.1.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "020vnrwg7nzbzmbr9pd43gz6dfvc23xa85hlfhcrg5s8lxgf0q99")))

(define-public crate-pavex_bp_schema-0.1.24 (c (n "pavex_bp_schema") (v "0.1.24") (d (list (d (n "pavex_reflection") (r "=0.1.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cvgi68asx13jwaxc5rla4aw4lkj3xs4zf6dr9yz9gaqy4xbf9jx")))

(define-public crate-pavex_bp_schema-0.1.25 (c (n "pavex_bp_schema") (v "0.1.25") (d (list (d (n "pavex_reflection") (r "=0.1.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y00grc8srsnm77zbiylgb2glfj4ldwn6p069xyp5fdknfqwpfqb")))

(define-public crate-pavex_bp_schema-0.1.26 (c (n "pavex_bp_schema") (v "0.1.26") (d (list (d (n "pavex_reflection") (r "=0.1.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03hl9q54vq6rs79yv9byls80hj1xqbnn4xzk9ia2q7223j9jcpf2")))

(define-public crate-pavex_bp_schema-0.1.27 (c (n "pavex_bp_schema") (v "0.1.27") (d (list (d (n "pavex_reflection") (r "=0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vd2w1k7aj008648z3v6bbx15wkqqdnfzb0bjymv9rlcrsbgdfnv")))

(define-public crate-pavex_bp_schema-0.1.28 (c (n "pavex_bp_schema") (v "0.1.28") (d (list (d (n "pavex_reflection") (r "=0.1.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivkwbbhbgbjxjgpwn8054y406icp3kdbr1g84gqxxfcsr1ryrrb")))

(define-public crate-pavex_bp_schema-0.1.29 (c (n "pavex_bp_schema") (v "0.1.29") (d (list (d (n "pavex_reflection") (r "=0.1.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cmy4piyi5niwdx73didwrqqins4fnqrrhvqhljdvl6pkvpblmbc")))

(define-public crate-pavex_bp_schema-0.1.30 (c (n "pavex_bp_schema") (v "0.1.30") (d (list (d (n "pavex_reflection") (r "=0.1.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "084qafgvr7fxz1vjixpvqks8dclb98vx66f7vjwrscz9pgpm8p4y")))

(define-public crate-pavex_bp_schema-0.1.31 (c (n "pavex_bp_schema") (v "0.1.31") (d (list (d (n "pavex_reflection") (r "=0.1.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "083aqdzggdb8z44ijl7ga8jyparlbjvqs8kah3zcwlqh3xk8imbb")))

(define-public crate-pavex_bp_schema-0.1.32 (c (n "pavex_bp_schema") (v "0.1.32") (d (list (d (n "pavex_reflection") (r "=0.1.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdyggmdahn5xpg0q2g0d7cxsnf1g4mav4jyx8qrwqffv99brg3k")))

(define-public crate-pavex_bp_schema-0.1.33 (c (n "pavex_bp_schema") (v "0.1.33") (d (list (d (n "pavex_reflection") (r "=0.1.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gainydnkr7m2ylia28vs9xyjh4d3la0qf2gq1lpfrhc3h4i7b3i")))

(define-public crate-pavex_bp_schema-0.1.34 (c (n "pavex_bp_schema") (v "0.1.34") (d (list (d (n "pavex_reflection") (r "=0.1.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s9d0bfl89hq5cp7zd0inyhmjic349ca867hxw7p6g69ldzfaw6d")))

(define-public crate-pavex_bp_schema-0.1.35 (c (n "pavex_bp_schema") (v "0.1.35") (d (list (d (n "pavex_reflection") (r "=0.1.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14hv1dzmhk3jwfc6hwd2z18wzp7mzr21gniw3s8nsppdn7vkjv7z")))

(define-public crate-pavex_bp_schema-0.1.36 (c (n "pavex_bp_schema") (v "0.1.36") (d (list (d (n "pavex_reflection") (r "=0.1.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xz41h93rg5v5j3nsa38dpwy67zmykv55ps1pw68gj352rh66lw4")))

(define-public crate-pavex_bp_schema-0.1.37 (c (n "pavex_bp_schema") (v "0.1.37") (d (list (d (n "pavex_reflection") (r "=0.1.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "04zl67ri2az6132khsi0vyc5smb14r9n4v6rb817vdgfgwxyfs00")))

(define-public crate-pavex_bp_schema-0.1.38 (c (n "pavex_bp_schema") (v "0.1.38") (d (list (d (n "pavex_reflection") (r "=0.1.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j34ry9szcj8y8jg20wqzan4k4n5zdn3h9429qiqhb92hgm5fb4z")))

