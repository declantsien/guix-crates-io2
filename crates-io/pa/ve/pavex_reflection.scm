(define-module (crates-io pa ve pavex_reflection) #:use-module (crates-io))

(define-public crate-pavex_reflection-0.1.0 (c (n "pavex_reflection") (v "0.1.0") (h "15zqqhcydy9cklgv8xf0z65a5w29qxn1hihg555111zmac3iila1") (y #t)))

(define-public crate-pavex_reflection-0.1.9 (c (n "pavex_reflection") (v "0.1.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sgw0lprycd737jysbm1dlnhq9mrqfz97h8pib9nh5gppm7bczyy") (y #t)))

(define-public crate-pavex_reflection-0.1.10 (c (n "pavex_reflection") (v "0.1.10") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kc4hnm5s93q0v5jf8k0vnbn8g1v4mxcr5jd2ajsmd15vpw1wm74") (y #t)))

(define-public crate-pavex_reflection-0.1.11 (c (n "pavex_reflection") (v "0.1.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mycdc6x3nlq8w6hb4qgx9vl6c8wifjvrvmx3zcmpym3h4d1snlc")))

(define-public crate-pavex_reflection-0.1.12 (c (n "pavex_reflection") (v "0.1.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nj4p7pcl2s0drbkcncaby5d2dnp0pgk0hrnw7mqdw79z66dydsj")))

(define-public crate-pavex_reflection-0.1.13 (c (n "pavex_reflection") (v "0.1.13") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y0w48i3fl1kmifi3znj6fbgdbmxgkdpraiz9pdy399mh2zsqb51")))

(define-public crate-pavex_reflection-0.1.14 (c (n "pavex_reflection") (v "0.1.14") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1734y2p337b6287bwbqw9hd52hmc8z20a97n5gfgav9zvw7x2glf")))

(define-public crate-pavex_reflection-0.1.15 (c (n "pavex_reflection") (v "0.1.15") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gs28x2db5m1rx6qyw48w8fw6jzap4cy0nf6p7ck1383rl9q00kh")))

(define-public crate-pavex_reflection-0.1.16 (c (n "pavex_reflection") (v "0.1.16") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ajdkjw03i0fin8p03kbjj5mw24rj32c9kqi345ijdgs921gmzm")))

(define-public crate-pavex_reflection-0.1.17 (c (n "pavex_reflection") (v "0.1.17") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lvz91ii40lnp7wxpklwz71zvfqzapd7l9q96m5y8rk10m71pgc2")))

(define-public crate-pavex_reflection-0.1.18 (c (n "pavex_reflection") (v "0.1.18") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0whdj4ylbdrwymp8qqig047nnzfn5hxwmk13h3s58wfl9mmh1gan")))

(define-public crate-pavex_reflection-0.1.19 (c (n "pavex_reflection") (v "0.1.19") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kw3rpr33zwgq939xmqihyr3vv0jygf7rngbj4cgi70dh1ab21za")))

(define-public crate-pavex_reflection-0.1.20 (c (n "pavex_reflection") (v "0.1.20") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00sp2g65hp355frffz6v7siyfvjcsapmcf2dyl9wsrd86n7ql9p6")))

(define-public crate-pavex_reflection-0.1.21 (c (n "pavex_reflection") (v "0.1.21") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k1b67fidl6wb1m2c6lmyw79z2zky78ipql1fp4ywdzvg51mxv8y")))

(define-public crate-pavex_reflection-0.1.22 (c (n "pavex_reflection") (v "0.1.22") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jwj6j07hykm9gw0ccs45kk8h22q06vblpd3m3541rjyrgzi6by1")))

(define-public crate-pavex_reflection-0.1.23 (c (n "pavex_reflection") (v "0.1.23") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h3f20rzkpq4h2y84qwzx0zaim04hl7hsvr4nyisbfipk63rh84z")))

(define-public crate-pavex_reflection-0.1.24 (c (n "pavex_reflection") (v "0.1.24") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "089dad7rbya1zcpnq0ngznpywla0x949qncwgrr56y6ar2d65yyg")))

(define-public crate-pavex_reflection-0.1.25 (c (n "pavex_reflection") (v "0.1.25") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v6ifdq9njfsgj5xwqrkdzs4cdsr4xcmyacbl2fsrik75b7hjhyi")))

(define-public crate-pavex_reflection-0.1.26 (c (n "pavex_reflection") (v "0.1.26") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gi7dqnf2w1jlq0c02kngfn0jrks98i86mlg4h78qyn18wyg4wjl")))

(define-public crate-pavex_reflection-0.1.27 (c (n "pavex_reflection") (v "0.1.27") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "111m1acbc0msbciv1mmw3j7q35vq6bc6xympvkg3jrjfx71r8apg")))

(define-public crate-pavex_reflection-0.1.28 (c (n "pavex_reflection") (v "0.1.28") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xh8kcv64s5n73znvcdyikg3v0dcj5l6ph2sbiky7idhzv9mv2xn")))

(define-public crate-pavex_reflection-0.1.29 (c (n "pavex_reflection") (v "0.1.29") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pkcj5cwa0ncn6f7za0r1dvx9csq9lxlzrcpirrsy1ckl7prl20c")))

(define-public crate-pavex_reflection-0.1.30 (c (n "pavex_reflection") (v "0.1.30") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fgsypir1lbgdnl312wsz05di6b3g7s4sggj4c2xlryyznzpskqs")))

(define-public crate-pavex_reflection-0.1.31 (c (n "pavex_reflection") (v "0.1.31") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06mvhvnlx6c4yg7bp91pql628c0d60z1dnflmzm9v3p94pawm25y")))

(define-public crate-pavex_reflection-0.1.32 (c (n "pavex_reflection") (v "0.1.32") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12qjidnhi16b4r2k4jav16sj2g9g4w3ci3nfgyvz95yjsa531sr3")))

(define-public crate-pavex_reflection-0.1.33 (c (n "pavex_reflection") (v "0.1.33") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00s0knw8s2jyljwysr2hf1vc48q55pn9ws4knywyrd8m626v6m07")))

(define-public crate-pavex_reflection-0.1.34 (c (n "pavex_reflection") (v "0.1.34") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bkx6vvdq5cna51gmx1zk3a0xhz555xkhvzzz6a5xnnxq4dyd69j")))

(define-public crate-pavex_reflection-0.1.35 (c (n "pavex_reflection") (v "0.1.35") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f29sjy8laiqx39ryhgbnr8cjsfm0z59w4ikwr6psspgvncc1591")))

(define-public crate-pavex_reflection-0.1.36 (c (n "pavex_reflection") (v "0.1.36") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdi59hhwaj706c0j5f4k9vzkk63rfc6pn13h3gjg0z3lr2fg9h8")))

(define-public crate-pavex_reflection-0.1.37 (c (n "pavex_reflection") (v "0.1.37") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kfj9hcx67n0sbxl5h6ik7nxsl8gyxdhmgrdc4qfxjr4panmy97p")))

(define-public crate-pavex_reflection-0.1.38 (c (n "pavex_reflection") (v "0.1.38") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ji5iqncn2y6x5sgdmrkrfsndk9l8k22lw7zqzxr9q49cw7sxxks")))

