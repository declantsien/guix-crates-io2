(define-module (crates-io pa ve pavexc_cli_client) #:use-module (crates-io))

(define-public crate-pavexc_cli_client-0.1.9 (c (n "pavexc_cli_client") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "06nb38b9zqq3nx31xcr4k18c1d5v9r1l36qkbn28qs48vmkk8dr3") (y #t)))

(define-public crate-pavexc_cli_client-0.1.10 (c (n "pavexc_cli_client") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0fll27ipnrhyj4kwwl9sb9h8v96ygvxz36gl4gi8ifzmlgl7lgwi") (y #t)))

(define-public crate-pavexc_cli_client-0.1.11 (c (n "pavexc_cli_client") (v "0.1.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1iarg78mx8ibafgky7iirgqnf56xj34z0dcdvj0k21zwjc3qby7h")))

(define-public crate-pavexc_cli_client-0.1.12 (c (n "pavexc_cli_client") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1dvdwx5i03xz3g1385akyncwn9ps431nw9hcaaf7scy1jp8802yd")))

(define-public crate-pavexc_cli_client-0.1.13 (c (n "pavexc_cli_client") (v "0.1.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1i52qacnsrzfz34j8ynjif9nga4yxr8ff9yphk0j7k4ss2qhvjmy")))

(define-public crate-pavexc_cli_client-0.1.14 (c (n "pavexc_cli_client") (v "0.1.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05rh3d2qgqlw2ng1nkrqaa5syfcpi4x2w7igz3q7bv9qjx2mv568")))

(define-public crate-pavexc_cli_client-0.1.15 (c (n "pavexc_cli_client") (v "0.1.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17rvcsccyi8rz2mhawn5mppq8mi57b6q3az6b9927lsvqpzx7bz5")))

(define-public crate-pavexc_cli_client-0.1.16 (c (n "pavexc_cli_client") (v "0.1.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1892qzwqj8h59ligr4xmv3wzsd4sv9y3xkjzydbygg3ji43gvciw")))

(define-public crate-pavexc_cli_client-0.1.17 (c (n "pavexc_cli_client") (v "0.1.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0glfxq1yx83l8nmqm4gjbn7mfd8ivmj5nv0y82x8r74x0a3j85al")))

(define-public crate-pavexc_cli_client-0.1.18 (c (n "pavexc_cli_client") (v "0.1.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0mn5hh65d76vwvgxsz7w6xvq9gpxd14jdb6vwhgmp4rnvm41yn5x")))

(define-public crate-pavexc_cli_client-0.1.19 (c (n "pavexc_cli_client") (v "0.1.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0cq2xddm0pfx0g0k9icywl9x94w3z82iam6yb73fy6w3m71xd11f")))

(define-public crate-pavexc_cli_client-0.1.20 (c (n "pavexc_cli_client") (v "0.1.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0l6pndn5njf7dpab76v4vb75q3fbqz3ywjsvv9kvw3ms2ryvra9f")))

(define-public crate-pavexc_cli_client-0.1.21 (c (n "pavexc_cli_client") (v "0.1.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0yqn64wccwsb7s91clmk1k4ffpcqb0prn1ybjynijb5j8vmjnj8z")))

(define-public crate-pavexc_cli_client-0.1.22 (c (n "pavexc_cli_client") (v "0.1.22") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1kzsrrvlmqb8kk0lkh8cr4z63yfz7m60jsqaqca8xffnk2kw4jhm")))

(define-public crate-pavexc_cli_client-0.1.23 (c (n "pavexc_cli_client") (v "0.1.23") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "09nnajcf735rvsrj6izmln5mb4721rslvmxacxqxkixkpmcqrk3m")))

(define-public crate-pavexc_cli_client-0.1.24 (c (n "pavexc_cli_client") (v "0.1.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0awa5nsyq2pxdyjpyr8ih6ggpnva504rnwg8hsyj95ffxyxsbffv")))

(define-public crate-pavexc_cli_client-0.1.25 (c (n "pavexc_cli_client") (v "0.1.25") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0g79qccy6h6avhpqc3s55vlavsqigimb2h4mr1jai9n2iknjs45r")))

(define-public crate-pavexc_cli_client-0.1.26 (c (n "pavexc_cli_client") (v "0.1.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0kr6rcnq3i1pv53kgg80fcaavkb1yx0ijzi6nmdk9hwzpsp7dszp")))

(define-public crate-pavexc_cli_client-0.1.27 (c (n "pavexc_cli_client") (v "0.1.27") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17fapmj6q4njxr4cdf3wqvqdfhxl8a4zlby9p3h4fl4hwxkna0gk")))

(define-public crate-pavexc_cli_client-0.1.28 (c (n "pavexc_cli_client") (v "0.1.28") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "09m6agd4vfcvm7x6vjzl7pjcabz3bnv3r5wdmy4v0hrvpkps92wr")))

(define-public crate-pavexc_cli_client-0.1.29 (c (n "pavexc_cli_client") (v "0.1.29") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0lbn442r55k86xq3yli1z9iwlwd8alcqjj2l0k2bw1dpibzwir5a")))

(define-public crate-pavexc_cli_client-0.1.30 (c (n "pavexc_cli_client") (v "0.1.30") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "11ggkmhnm2jlfpgiaksk6p4ladc5g42qfzkpykcjk5gsaj3356hf")))

(define-public crate-pavexc_cli_client-0.1.31 (c (n "pavexc_cli_client") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "00zl2f137ydyiw1da50aibdl0vj6y7i84n8zpl5lf9hq3yfzbd52")))

(define-public crate-pavexc_cli_client-0.1.32 (c (n "pavexc_cli_client") (v "0.1.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1hqcd19fmaxfwbn8jcjzbbc3952l6n0cxrcahl76wgi326zbbajh")))

(define-public crate-pavexc_cli_client-0.1.33 (c (n "pavexc_cli_client") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0f7h97w2scvbnw6hkf41vgks4nhg93aq99fhyq1r25dgwraj2h5r")))

(define-public crate-pavexc_cli_client-0.1.34 (c (n "pavexc_cli_client") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0az9id5p5k70nvhagccf05qw1v2r4b1z1hshx37fg5dfkvbz10w1")))

(define-public crate-pavexc_cli_client-0.1.35 (c (n "pavexc_cli_client") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0aap3s56ck3ff78k1mss0cgd4kaxs64cpfx3x51ldvan0f4zgnbf")))

(define-public crate-pavexc_cli_client-0.1.36 (c (n "pavexc_cli_client") (v "0.1.36") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.36") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "000l8nx1kiim06xfnsyk1lrk374xicjrci82d4lcbbxiv9n8g6ap")))

(define-public crate-pavexc_cli_client-0.1.37 (c (n "pavexc_cli_client") (v "0.1.37") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.37") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12s9qswjvg50hd25zvxq7i1jyhndj5h60rznlfb7rwbd9f9fljc1")))

(define-public crate-pavexc_cli_client-0.1.38 (c (n "pavexc_cli_client") (v "0.1.38") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1rx2fxwajdk38nhz03fzdl2b2ak3vqxmk1q4w32qjnyv2b82yvn7")))

