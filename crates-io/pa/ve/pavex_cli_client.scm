(define-module (crates-io pa ve pavex_cli_client) #:use-module (crates-io))

(define-public crate-pavex_cli_client-0.1.10 (c (n "pavex_cli_client") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1zs22pmzh69ym0r5g8ysmdqmnrrp3shcxj8qb6w6z9y82vy225yx") (y #t)))

(define-public crate-pavex_cli_client-0.1.11 (c (n "pavex_cli_client") (v "0.1.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mbdawjyk2jdmcskl2kwrq997qpqq23km75l2g6f9i5c31hzp5wp")))

(define-public crate-pavex_cli_client-0.1.12 (c (n "pavex_cli_client") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1h4wwhs86zfzszi7mp0p2znrf8ss9lj0h3jfpc1l4n8whibjni4h")))

(define-public crate-pavex_cli_client-0.1.13 (c (n "pavex_cli_client") (v "0.1.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1kcyix9nijyc9nws0wzyj2cnzq6yqv6vg0zjm8ndkg1idwqr9d4c")))

(define-public crate-pavex_cli_client-0.1.14 (c (n "pavex_cli_client") (v "0.1.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1yrwj86i24q7w5lvb291pn2z7mw3z4a8vhxwfgg84digxv084hs9")))

(define-public crate-pavex_cli_client-0.1.15 (c (n "pavex_cli_client") (v "0.1.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01nz9df1a3ya6cm3wc71xfnn4cq82jb39cwakg808yfl88i0wk29")))

(define-public crate-pavex_cli_client-0.1.16 (c (n "pavex_cli_client") (v "0.1.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ggc62dhh5waand860accqvad23h7hqvfx5qx52amqlykasmqj5y")))

(define-public crate-pavex_cli_client-0.1.17 (c (n "pavex_cli_client") (v "0.1.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05l4zji28s37hjrpc5hbmzd2698qvj1ipkj3q1z4p0v6jscf5fix")))

(define-public crate-pavex_cli_client-0.1.18 (c (n "pavex_cli_client") (v "0.1.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "188bhcxywkynl5s741hzpvvdx41xbwc096jy46a7hafxpwwfpm4z")))

(define-public crate-pavex_cli_client-0.1.19 (c (n "pavex_cli_client") (v "0.1.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1b1n4k2z5i4c4vjr5pfn4ma4nxj60mg0g5h0wjql7c1ml1g05gmk")))

(define-public crate-pavex_cli_client-0.1.20 (c (n "pavex_cli_client") (v "0.1.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0gn3z9w100zrvzmlg475g8lnz1yfjc595i6zc76f54b362jjbcwc")))

(define-public crate-pavex_cli_client-0.1.21 (c (n "pavex_cli_client") (v "0.1.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0gnw1vjd09azs1qrncf4967ipbq4q7cwwrbpag8mavivh2px3ny9")))

(define-public crate-pavex_cli_client-0.1.22 (c (n "pavex_cli_client") (v "0.1.22") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12mwpmhdzcs59wwmy3byf6dlmmdmwxnrchmv0pk224jylljw96qn")))

(define-public crate-pavex_cli_client-0.1.23 (c (n "pavex_cli_client") (v "0.1.23") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1k994sa46hdg2aagp5djfl24r3ym0w1rnwvagqbpgba0r4j7c9p8")))

(define-public crate-pavex_cli_client-0.1.24 (c (n "pavex_cli_client") (v "0.1.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0wkr0n2z1zfx9pbhy99wfqb6zk5xvlndchqlx8xvfdaa57qsq64p")))

(define-public crate-pavex_cli_client-0.1.25 (c (n "pavex_cli_client") (v "0.1.25") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01a5fg1a16vfnlppc2s8d8jffhvz7b10mag2slz3frpik69zj4ip")))

(define-public crate-pavex_cli_client-0.1.26 (c (n "pavex_cli_client") (v "0.1.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0z6gybhfvixr3q8slyh1ifjcp2amsssninz55319fyymbws4j6gn")))

(define-public crate-pavex_cli_client-0.1.27 (c (n "pavex_cli_client") (v "0.1.27") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0bw38vhldpzmmsi93wnhkw6w6nn21sd5ma3pjc4k30h75ghk8ygj")))

(define-public crate-pavex_cli_client-0.1.28 (c (n "pavex_cli_client") (v "0.1.28") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0kkjmni2vyqz3ncw5bdagz5rjqni64q1q459xji5aax7mzy2zq17")))

(define-public crate-pavex_cli_client-0.1.29 (c (n "pavex_cli_client") (v "0.1.29") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0d9y46ar420l5phyq8vxyms2hibh0gfsjjpjsilxsxrdfb3ncv5l")))

(define-public crate-pavex_cli_client-0.1.30 (c (n "pavex_cli_client") (v "0.1.30") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1rdrmwyldw59d8fcsszsq920qqmgq188hf7z4rc49qabxyqw0n38")))

(define-public crate-pavex_cli_client-0.1.31 (c (n "pavex_cli_client") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "07cnw6cmbgb62l99lsp9vv04pcsd4pagbf33g73chs5wigqb0iby")))

(define-public crate-pavex_cli_client-0.1.32 (c (n "pavex_cli_client") (v "0.1.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12664qr43prddzwdmnvrl3917lpyxcm1ilc4fmpqd223336r8c54")))

(define-public crate-pavex_cli_client-0.1.33 (c (n "pavex_cli_client") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "064q7xyzbc4vj2fdrdll2xrqf5q8yjsap7a42jpd9gkf55c8bxi7")))

(define-public crate-pavex_cli_client-0.1.34 (c (n "pavex_cli_client") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0pfap1cjams3p6a66bhf6qd912xb2m31i410wmk3qshsj84kbq0g")))

(define-public crate-pavex_cli_client-0.1.35 (c (n "pavex_cli_client") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pavex") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1zyi7hhssgwgvzw80lr4bym2bmszsjj5fld1wqyzh935iaxzpjmi")))

(define-public crate-pavex_cli_client-0.1.36 (c (n "pavex_cli_client") (v "0.1.36") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.36") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0vx9qigaw8iacalf8nmc90pprgrr0iax2ykfr3madc9cf4xjdizy")))

(define-public crate-pavex_cli_client-0.1.37 (c (n "pavex_cli_client") (v "0.1.37") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.37") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0lsyvqvrq8j76jm461xjw4y7zwybmpp4bhjnq8li7jd15403j71j")))

(define-public crate-pavex_cli_client-0.1.38 (c (n "pavex_cli_client") (v "0.1.38") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pavex") (r "^0.1.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "15kbw8iximkkrmkwn8y35vj66jh0i73m40f3n2s0l53agwdv7hik")))

