(define-module (crates-io pa ve pavex_tracing) #:use-module (crates-io))

(define-public crate-pavex_tracing-0.1.19 (c (n "pavex_tracing") (v "0.1.19") (d (list (d (n "pavex") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "19p7icm5gf6w11z5yyv2q3bjy7q7fdvqgyqwcjw00s1wy5bw2vgv")))

(define-public crate-pavex_tracing-0.1.20 (c (n "pavex_tracing") (v "0.1.20") (d (list (d (n "pavex") (r "^0.1.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "1d45pd1iqzjqfgvga1djbfw8ma7nqgd5ql83i84ijin6v0c4ljf6")))

(define-public crate-pavex_tracing-0.1.21 (c (n "pavex_tracing") (v "0.1.21") (d (list (d (n "pavex") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "07x3gr0pnjsw3qshm8zdv4qfj2566dll6jyl5r45qwa5g14s7xf6")))

(define-public crate-pavex_tracing-0.1.22 (c (n "pavex_tracing") (v "0.1.22") (d (list (d (n "pavex") (r "^0.1.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "01hbyiw8l5p4msnfn64bvixcha4pjf2nz4f70d17cb00idx2yzcw")))

(define-public crate-pavex_tracing-0.1.23 (c (n "pavex_tracing") (v "0.1.23") (d (list (d (n "pavex") (r "^0.1.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "07ybwl756i43sdl31nnsarb36hmv4nv10z2ahp3hvcn8w6fzyny6")))

(define-public crate-pavex_tracing-0.1.24 (c (n "pavex_tracing") (v "0.1.24") (d (list (d (n "pavex") (r "^0.1.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "1dicil2kk5l56sm7balf7f9vk84c9z3f832wf27qdq4dplma8wcw")))

(define-public crate-pavex_tracing-0.1.25 (c (n "pavex_tracing") (v "0.1.25") (d (list (d (n "pavex") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "1i101i217wvwcrbnjk1p3dxn7vjawqvxnkngyqw1qlfb5dhw27gz")))

(define-public crate-pavex_tracing-0.1.26 (c (n "pavex_tracing") (v "0.1.26") (d (list (d (n "pavex") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "00jmsdiyl9m1rjrjhpkhp0dl3s1wgp8ffj54z463rgzhwh1i3dnc")))

(define-public crate-pavex_tracing-0.1.27 (c (n "pavex_tracing") (v "0.1.27") (d (list (d (n "pavex") (r "^0.1.27") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0291708xnhpsvmxh1sqb99xw88d60za0lab7wfq8hc8dlwvy55ms")))

(define-public crate-pavex_tracing-0.1.28 (c (n "pavex_tracing") (v "0.1.28") (d (list (d (n "pavex") (r "^0.1.28") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0bja0hycgy38j3n2x1z2iya2qzrq6zygh3jrl51a6n7a4fd25xi0")))

(define-public crate-pavex_tracing-0.1.29 (c (n "pavex_tracing") (v "0.1.29") (d (list (d (n "pavex") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0ixim1v6r3pjyppfq6vjkq66l6v64bvhcb0rx3ib5rmwg9qz3y5d")))

(define-public crate-pavex_tracing-0.1.30 (c (n "pavex_tracing") (v "0.1.30") (d (list (d (n "pavex") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0s27f96dvq1fzay127rj7768swkbrmbwklv0hx99h23l6rdhzbrl")))

(define-public crate-pavex_tracing-0.1.31 (c (n "pavex_tracing") (v "0.1.31") (d (list (d (n "pavex") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "14x1xm9yapjb7y768gd1ylh01i39370w7b5fi5db6nc51g5ajbnh")))

(define-public crate-pavex_tracing-0.1.32 (c (n "pavex_tracing") (v "0.1.32") (d (list (d (n "pavex") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0drpk68fpxisqnjbik1ia14l9fi8q8s1y6hahx8hr4xv3qmqffaf")))

(define-public crate-pavex_tracing-0.1.33 (c (n "pavex_tracing") (v "0.1.33") (d (list (d (n "pavex") (r "^0.1.33") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0pzxd06i8nr1nivpfs3xf060ackcw0pv7wr94nxgdn3q05861rzn")))

(define-public crate-pavex_tracing-0.1.34 (c (n "pavex_tracing") (v "0.1.34") (d (list (d (n "pavex") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0algnscrsizh99vydyh776s2y0d300qxgzia08dnl4lywpi4icqr")))

(define-public crate-pavex_tracing-0.1.35 (c (n "pavex_tracing") (v "0.1.35") (d (list (d (n "pavex") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "15f1mv2ffqh42zgd021c0v812gz838cvqimwlblacymalaplq7lb")))

(define-public crate-pavex_tracing-0.1.36 (c (n "pavex_tracing") (v "0.1.36") (d (list (d (n "pavex") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "079i1d6as8q5lkyvb92wkjgpcf3d8517s1x0ak17nfdj2w87dxq1")))

(define-public crate-pavex_tracing-0.1.37 (c (n "pavex_tracing") (v "0.1.37") (d (list (d (n "pavex") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0nz3z2021v209lbmrjw1phzyrdhsciggpy6pccqagpkwk2cb8yjb")))

(define-public crate-pavex_tracing-0.1.38 (c (n "pavex_tracing") (v "0.1.38") (d (list (d (n "pavex") (r "^0.1.38") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0fr05r96abix952v7lvlfm8pgpb8wz22h68158ziggkhkj96rvbs")))

