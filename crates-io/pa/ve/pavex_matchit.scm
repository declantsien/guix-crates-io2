(define-module (crates-io pa ve pavex_matchit) #:use-module (crates-io))

(define-public crate-pavex_matchit-0.1.0 (c (n "pavex_matchit") (v "0.1.0") (h "0w6lwm5aab1f17aiz3wzy27an303gcrgvvd0y1ffnjxd1pq4ng6j") (y #t)))

(define-public crate-pavex_matchit-0.7.4 (c (n "pavex_matchit") (v "0.7.4") (d (list (d (n "actix-router") (r "^0.2.7") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "gonzales") (r "^0.0.3-beta") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "path-tree") (r "^0.2.2") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3.0") (d #t) (k 2)) (d (n "routefinder") (r "^0.5.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("make" "util"))) (d #t) (k 2)))) (h "1z2ilpp3vw0xqkxws6qwj03qb6kdc96nq536d62xh7rq0cxpbfbp") (f (quote (("default") ("__test_helpers"))))))

