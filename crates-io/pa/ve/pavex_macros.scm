(define-module (crates-io pa ve pavex_macros) #:use-module (crates-io))

(define-public crate-pavex_macros-0.1.9 (c (n "pavex_macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0l4kchmsai6fbd6brg55c95j0c9s2jd6g8wjivz4g6rjgm511c8a") (y #t)))

(define-public crate-pavex_macros-0.1.10 (c (n "pavex_macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bs4zy7cjd2949g63hjlgb54s003ilarf1jndg5msvpp5fj3088a") (y #t)))

(define-public crate-pavex_macros-0.1.11 (c (n "pavex_macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0x3q6wzbgpz0jnllhzzgahkjn2kdhn32323j4kc2wf3dmykijmc5")))

(define-public crate-pavex_macros-0.1.12 (c (n "pavex_macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1h1hswz5q1cqvsiywzd9pr90w6r0wvw715qksrgmnpgmyip6xgsx")))

(define-public crate-pavex_macros-0.1.13 (c (n "pavex_macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wx8m567qh36k375aiq67g840riqpiqx13cy7gmw6jrsg4k7d9x1")))

(define-public crate-pavex_macros-0.1.14 (c (n "pavex_macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15990k09aslc9nphmapb7xln3kwx17hbgizl44h4vq64w113fqlh")))

(define-public crate-pavex_macros-0.1.15 (c (n "pavex_macros") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1qbxfcld8zl9652y0r1wdl5x7qybacswrigccvpn8baincyn0knm")))

(define-public crate-pavex_macros-0.1.16 (c (n "pavex_macros") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0syp7k2yycg9b53fjvwb3wkn0kdlx3ijph4z1daqvkg7a8fdqwwm")))

(define-public crate-pavex_macros-0.1.17 (c (n "pavex_macros") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0i87j02pa06njizlcgppdgybv8j90cq9lysm0jyfiwqx52xvjlwi")))

(define-public crate-pavex_macros-0.1.18 (c (n "pavex_macros") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17jfvivs6axn1x9fhsiqw1f0x02ggarmza82ahkzxmg6qwd80px2")))

(define-public crate-pavex_macros-0.1.19 (c (n "pavex_macros") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d7w3378lqddcccfmc4ypclhqkkak6qqqr4imrzgjy6mma3dbpwc")))

(define-public crate-pavex_macros-0.1.20 (c (n "pavex_macros") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zsf1rpn3nb1n64ai8a979nnyk0mjl4fngpv0401gq8795xbwq1v")))

(define-public crate-pavex_macros-0.1.21 (c (n "pavex_macros") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11b3cb61dw26ps0xrd9n51fa6r2anmfx1nrhhzhgza6rzx65spln")))

(define-public crate-pavex_macros-0.1.22 (c (n "pavex_macros") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06zrpa5wv363hsgj5dmkh8yz6c43k8lsla0yf7i44skm3zc75a6h")))

(define-public crate-pavex_macros-0.1.23 (c (n "pavex_macros") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0l51mz3iy5yr5h5kxfs53b36vrmdlnzk6nxqjf21isb4b0mm4wmf")))

(define-public crate-pavex_macros-0.1.24 (c (n "pavex_macros") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "126ngx7lbkzg4kridnh2fs237nfl7a8hbpszb0lsmm2ash972z3i")))

(define-public crate-pavex_macros-0.1.25 (c (n "pavex_macros") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00vpl56i25y84ijx5a64hvrd9acca1k4s6ga5kh4cs5cxd47ccbr")))

(define-public crate-pavex_macros-0.1.26 (c (n "pavex_macros") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11rzh7y1l5awxw5dc4pyb5m30pwd9vq89r9wnp90fcjvqbcizqb2")))

(define-public crate-pavex_macros-0.1.27 (c (n "pavex_macros") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d7rwnynn93riw5i825sq8z5qp1k053f0gg9v7bsdv8ky98vs52m")))

(define-public crate-pavex_macros-0.1.28 (c (n "pavex_macros") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ajjmxdhcg5y9npn1az15h3gyfhjs0c798albg3c6wpapqp2wrx8")))

(define-public crate-pavex_macros-0.1.29 (c (n "pavex_macros") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13lcqfyhkh7br64hh8267lkff8jh4hq3479h1r4i9wx1chs51564")))

(define-public crate-pavex_macros-0.1.30 (c (n "pavex_macros") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1irsafnyhh9hh3rzgaagcnfy9qcff74276lqh2qcjp80sc8k4ffm")))

(define-public crate-pavex_macros-0.1.31 (c (n "pavex_macros") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hv6r7hdri0b48zs9rphnnsgb86h75k1jxpxhhf8plgvm5syfrdf")))

(define-public crate-pavex_macros-0.1.32 (c (n "pavex_macros") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rc7cc56h586lvrqz1wx83i3lf2zgp9klyw2mgaac6c4i7zbxill")))

(define-public crate-pavex_macros-0.1.33 (c (n "pavex_macros") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "118r50dhvyfzjhlszq8sdgwvgki6mp527s6xhd44mchlm6rmx42s")))

(define-public crate-pavex_macros-0.1.34 (c (n "pavex_macros") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14q7v34cm5y031zb499lrl874qah1yba0wxmsl08i6pigl7pzbh6")))

(define-public crate-pavex_macros-0.1.35 (c (n "pavex_macros") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03h1csfbxcb9nk1iqsnnq4l17y5naqz3zwvgfviil0gbmp1cjybr")))

(define-public crate-pavex_macros-0.1.36 (c (n "pavex_macros") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03g0wbkydbcx8x9qpha24dxgbh86n0cmqyjsdf2ljv4kxnlh5nhf")))

(define-public crate-pavex_macros-0.1.37 (c (n "pavex_macros") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1iff5wh7pf6n338xkgkbfqcl64bzzrhzfvf7q9m71j0gf535dh2b")))

(define-public crate-pavex_macros-0.1.38 (c (n "pavex_macros") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ihjaapmz1f7knf0xlfi4i134q6sfq02jr4fsg3v23bk0ifx15fi")))

