(define-module (crates-io pa sk pask) #:use-module (crates-io))

(define-public crate-pask-0.1.0 (c (n "pask") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1k6yq5dgpqiqrg9617xjj0sg3cmqmc338m2h6h2ly9l5gd8ndz2d")))

(define-public crate-pask-0.1.1 (c (n "pask") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1kayxlwnhnp3q1m3r5s7vnxvy61ml7fw50k1yzxb57kpmjgjb1zy")))

