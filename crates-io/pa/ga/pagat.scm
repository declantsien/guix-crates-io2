(define-module (crates-io pa ga pagat) #:use-module (crates-io))

(define-public crate-pagat-0.0.1 (c (n "pagat") (v "0.0.1") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0ffw6m8cqknck4y5firl38h2nfzaz3k4vxy52p9l0jpizs07yamx")))

(define-public crate-pagat-0.0.2 (c (n "pagat") (v "0.0.2") (d (list (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1pzih1qvd5q8rs1n8h236vql2in17w4xq52x7d9j56d7mcf20p35")))

