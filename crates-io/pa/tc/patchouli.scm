(define-module (crates-io pa tc patchouli) #:use-module (crates-io))

(define-public crate-patchouli-0.1.0 (c (n "patchouli") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1mr297f4db37p6c1dpy7glnyh1krl0ww9rwymbqqfzdmvwbf1v9n")))

