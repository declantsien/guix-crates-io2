(define-module (crates-io pa tc patchwork) #:use-module (crates-io))

(define-public crate-patchwork-0.1.0 (c (n "patchwork") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (f (quote ("mint"))) (d #t) (k 0)))) (h "1dx5gnciml3ncypigyjapgi780kb0f26p8w431xydg6hnfxc66cy")))

