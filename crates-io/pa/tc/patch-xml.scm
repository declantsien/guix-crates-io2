(define-module (crates-io pa tc patch-xml) #:use-module (crates-io))

(define-public crate-patch-xml-0.0.1 (c (n "patch-xml") (v "0.0.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "15cs225ndfb9q1f2r1xgnph99j82m9xfsxi48ip6n1sy4bcf1iq0")))

(define-public crate-patch-xml-0.0.2 (c (n "patch-xml") (v "0.0.2") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1i66gslliklhkkpibk1x576lza3v8lg5ypp0m387fmvvgpav878w")))

(define-public crate-patch-xml-0.0.3 (c (n "patch-xml") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "1m19xwy9vm9yvfr0cm41rg8nbjlnkk2ankgkzz8d0h2bk3n07m6h")))

