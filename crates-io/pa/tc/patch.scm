(define-module (crates-io pa tc patch) #:use-module (crates-io))

(define-public crate-patch-0.1.0 (c (n "patch") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1jbn7s1bk675kwmhqhx902crcznbcwq6ms7vl5h8a6mgkls6plid")))

(define-public crate-patch-0.2.0 (c (n "patch") (v "0.2.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "08ilfjr940z2dvpzb4kjmcrzi80gs7pzfi82afrlsxsjizfbmvm6")))

(define-public crate-patch-0.3.0 (c (n "patch") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1mcn12cd4rpcsiayjmlwy5nwd6w76qc597i01vpmwdhpkkx4k9a4")))

(define-public crate-patch-0.4.0 (c (n "patch") (v "0.4.0") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0mdqry8y65kag677ri72hq4vd75520l24j3qizp6mhncjkn1kpni")))

(define-public crate-patch-0.4.1 (c (n "patch") (v "0.4.1") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "18r0nki0lrr550z6rzrqwvb30l8vbz5w35yi45y90i12r7hns032")))

(define-public crate-patch-0.4.2 (c (n "patch") (v "0.4.2") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0ijqck4rr5sz4v46mnzb7n3h7qkygnqid0gdwyca4p33qwwl9lsq")))

(define-public crate-patch-0.5.0 (c (n "patch") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "nom_locate") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "08803a72k3pylajns53d6wpg20nrgfwfj8n7s2qw8sdb1xy7hgmg")))

(define-public crate-patch-0.6.0 (c (n "patch") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "18wz2495m31lvxqn718019psdxgds31j154qwjzgnwankmil60v6")))

(define-public crate-patch-0.7.0 (c (n "patch") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "0yjfs637c7pqifldpaddc8kxbjrldidikg155b7vs1cbvpf7zh0m")))

