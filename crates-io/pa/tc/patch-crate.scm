(define-module (crates-io pa tc patch-crate) #:use-module (crates-io))

(define-public crate-patch-crate-0.1.0 (c (n "patch-crate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "1xlypw6v2hy0riickb618wyqzqm6qw92p4av7bawsbvg5gwhq04w")))

(define-public crate-patch-crate-0.1.1 (c (n "patch-crate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0sxn5xib2v6v4rckx2j5xi6l6fs02p6ifz2zfypkhsxrgaa0d2a5")))

(define-public crate-patch-crate-0.1.2 (c (n "patch-crate") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "1zwb7r3pjf9457kwywxv5hqcr87zn4vbsaahaiyn1816w16dna5b")))

(define-public crate-patch-crate-0.1.3 (c (n "patch-crate") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "08k0sx53rxvk1939qrnbcidkgxvlr3bsy5y7dgw1jyxfqlswwbl5")))

(define-public crate-patch-crate-0.1.5 (c (n "patch-crate") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ih03bj1vdjnmy3rffh6b044a9j6c7dhj46vdy7dkz5a4ps47sj1")))

(define-public crate-patch-crate-0.1.6 (c (n "patch-crate") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nxg6iaxps2yx58rz9xmm171zfbvw3d54w8jazj3gz1msqn1z817")))

(define-public crate-patch-crate-0.1.7 (c (n "patch-crate") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0mvddwmn1a91gkgg180kn9bqrppss6hglk20pc15w7z2gm4sssr9")))

(define-public crate-patch-crate-0.1.8 (c (n "patch-crate") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "12wqw9szxvzi02w8kw8a8dqxalr2i56mr3sifilrw8vv5xz71hbb")))

(define-public crate-patch-crate-0.1.9 (c (n "patch-crate") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0cfv1psy9iqch3lf5cpf7xiwbw298mhvxq39z8vgxg97g80fal9b")))

