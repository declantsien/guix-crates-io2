(define-module (crates-io pa tc patch-rs) #:use-module (crates-io))

(define-public crate-patch-rs-0.1.0 (c (n "patch-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)))) (h "0h5iyca2kl48h3s4wd8jb4rh6jxv468adhacddfm6h4y6s0ylzjb")))

(define-public crate-patch-rs-0.2.0 (c (n "patch-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)))) (h "04nmyhccn1lzhp9a0lzn8sqak371mqpzfaaw8sy0l0zkb7x16qjg")))

(define-public crate-patch-rs-0.3.0 (c (n "patch-rs") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)))) (h "06jav8mishrvzzkdgzij3iis4hc2acbin1yk6r352n4rwa2prc9n")))

(define-public crate-patch-rs-0.4.0 (c (n "patch-rs") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0ypahjrayj70pfngz78f43rcv80vr1zna7nrp9rc507v5qbis6m0")))

(define-public crate-patch-rs-0.5.0 (c (n "patch-rs") (v "0.5.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0w9kcwh1dg6svgs4hmbr8h93c8q4wqr83m6yw4b7k905cmln0qhp")))

(define-public crate-patch-rs-0.5.1 (c (n "patch-rs") (v "0.5.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "189d9knfs70fpkm59mfifdgw81hzhnf95c2bgr4xflcqwckpsid5")))

(define-public crate-patch-rs-0.5.2 (c (n "patch-rs") (v "0.5.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1bygv757yc31qqv85kdfcv7x3i5xsvx0k07s5bhvjdbdq2b7ckzk")))

(define-public crate-patch-rs-0.5.3 (c (n "patch-rs") (v "0.5.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "12sb89wx2bp5z4zy4zhd08dzdimr7wmlzi8zzldj3vdcjj1sms0h")))

(define-public crate-patch-rs-0.5.4 (c (n "patch-rs") (v "0.5.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "15qs2fl1cmmx24lhhsh70q058cg4mganqgmnlkmkpy6nb53p7yb1")))

(define-public crate-patch-rs-0.5.5 (c (n "patch-rs") (v "0.5.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0b6y41i2bahlpz637ywpzalixdnfl7rzdxgya3gmmmpnjvfvgpn3")))

(define-public crate-patch-rs-0.6.0 (c (n "patch-rs") (v "0.6.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0kldihb83rnkdqww3x5zs23rpd8k51myf0lgxd6nmljfrkwj6zfb")))

(define-public crate-patch-rs-0.6.1 (c (n "patch-rs") (v "0.6.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0n4qaqhz8mnf8zcrpbhhk5czldwyjmxngjqhg8vcg5l7bzvsc885")))

(define-public crate-patch-rs-0.6.2 (c (n "patch-rs") (v "0.6.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1wfg5mj9kha2qdphq97kr0ljy7kc422cflqqa15ycdh188s85c5c")))

