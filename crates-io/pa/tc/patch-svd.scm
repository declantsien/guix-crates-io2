(define-module (crates-io pa tc patch-svd) #:use-module (crates-io))

(define-public crate-patch-svd-0.0.1 (c (n "patch-svd") (v "0.0.1") (d (list (d (n "itertools") (r "0.9.*") (d #t) (k 0)) (d (n "regex") (r "1.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde-xml-rs") (r "0.4.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_regex") (r "1.1.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)) (d (n "tinytemplate") (r "1.1.*") (d #t) (k 0)))) (h "0l7dhjf4sc7bdqxzcx8f57j7j0nijidyxp9qm5fyx0rbbm1rqib3")))

(define-public crate-patch-svd-0.0.2 (c (n "patch-svd") (v "0.0.2") (d (list (d (n "itertools") (r "0.9.*") (d #t) (k 0)) (d (n "regex") (r "1.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "0.4.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)))) (h "08k5g487yyxrq0m206b8kvzyy08z1msj8py7pajwwrw69xa7fi8h")))

(define-public crate-patch-svd-0.0.3 (c (n "patch-svd") (v "0.0.3") (d (list (d (n "itertools") (r "0.9.*") (d #t) (k 0)) (d (n "regex") (r "1.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "0.4.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)))) (h "0dl9i49cs13ikmg7bvapw03d5yglmj7awhk0s5bir187c66fqvls")))

(define-public crate-patch-svd-0.0.4 (c (n "patch-svd") (v "0.0.4") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "patch-xml") (r "^0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "07k69gmb0441ipg1ajpz1m5gqwa5cxgs0z8168gv1nb8n2nfd4m2")))

(define-public crate-patch-svd-0.1.0 (c (n "patch-svd") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "patch-xml") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "08acmmhkxq144vjkh16jv8pmf7xwmci1gswbibf1yn3yzazldpkm")))

