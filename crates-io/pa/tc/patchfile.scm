(define-module (crates-io pa tc patchfile) #:use-module (crates-io))

(define-public crate-patchfile-0.1.0 (c (n "patchfile") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)))) (h "01wg2s3xp3rxxjjpgc834gwq81nqql87avcfvria9ajwxw7y4vlx")))

(define-public crate-patchfile-0.2.0 (c (n "patchfile") (v "0.2.0") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "18mjqp6lh9dw0zchrh9h40iqmrzv13jxag5xdxbb27sym2ab28c0")))

(define-public crate-patchfile-0.2.1 (c (n "patchfile") (v "0.2.1") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "045hscf9s5zmfdrxda4g8whny0q1i8g60v2jqdmcikdah76xjjhy")))

(define-public crate-patchfile-0.2.2 (c (n "patchfile") (v "0.2.2") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "0sc2rk485g3k8xdr3sdn38dlm21icgmd39h4kb89z0lwjnm5npcl")))

