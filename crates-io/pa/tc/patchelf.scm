(define-module (crates-io pa tc patchelf) #:use-module (crates-io))

(define-public crate-patchelf-0.1.0 (c (n "patchelf") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zypmk8lsg5dk2i4c3d1zl2a5sw44pfd3qifar7gxa7g20s3jqgy") (y #t)))

(define-public crate-patchelf-0.2.0 (c (n "patchelf") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mxgb3pgkwiffk2yl5aqivy2cdd7jwpiyp45l21rz67259dsspqf") (y #t)))

(define-public crate-patchelf-0.2.1 (c (n "patchelf") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "090mm7jj25d84w26nx00wslqswff21cnfrpfns613jv1w85svnfs")))

