(define-module (crates-io pa tc patch-json) #:use-module (crates-io))

(define-public crate-patch-json-0.1.0 (c (n "patch-json") (v "0.1.0") (d (list (d (n "json-patch") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1lxrfwvr0jq4hypm7dqf9snvd1wwszvq3gmjqbg2x706s9q89j3n")))

(define-public crate-patch-json-0.1.1 (c (n "patch-json") (v "0.1.1") (d (list (d (n "json-patch") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0rnxaw7n4i7af4kg9059pgynnc7i3pgxfkng18swwwif076d9bgr") (r "1.59")))

