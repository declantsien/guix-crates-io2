(define-module (crates-io pa tc patchable-macros) #:use-module (crates-io))

(define-public crate-patchable-macros-0.1.0 (c (n "patchable-macros") (v "0.1.0") (d (list (d (n "patchable-core") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w7y4ngf8qy5q7ic7n1jlzdw4ilyd8ahsk7fpk9n2hx4sl7cg4wc")))

