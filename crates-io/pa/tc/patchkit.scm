(define-module (crates-io pa tc patchkit) #:use-module (crates-io))

(define-public crate-patchkit-0.1.0 (c (n "patchkit") (v "0.1.0") (h "1wgv9pia1y03bcclhqvzv8vb79n3f793z2adcrpc2v8bzb3vjr14")))

(define-public crate-patchkit-0.1.1 (c (n "patchkit") (v "0.1.1") (h "1x61brwhgmxdy9z6w7kiks9x7q0w3kyha7a67mh7xlw8nsj6q00g")))

(define-public crate-patchkit-0.1.2 (c (n "patchkit") (v "0.1.2") (h "05gw8sys5jkcz8i6waj52xzrf6vclnsx5bw6da6iszz5pwzsy2m3")))

