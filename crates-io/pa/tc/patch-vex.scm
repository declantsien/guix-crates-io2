(define-module (crates-io pa tc patch-vex) #:use-module (crates-io))

(define-public crate-patch-vex-0.3.0 (c (n "patch-vex") (v "0.3.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "116yayq1pr8wdynl8g36zcbcadpbkj2wnaiqpdlachjpr0zcarbr") (f (quote (("tasks-experimental"))))))

