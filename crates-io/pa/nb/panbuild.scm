(define-module (crates-io pa nb panbuild) #:use-module (crates-io))

(define-public crate-panbuild-0.0.7 (c (n "panbuild") (v "0.0.7") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1v286dzyw3djziqbk45g33rhawwl45a18yr145xff2fr3phx5h9j")))

(define-public crate-panbuild-0.0.8 (c (n "panbuild") (v "0.0.8") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1n9kf2ii1lqrwmjk27xpy8f026y2fw8l6qv89y9hy250xjgly3zr")))

(define-public crate-panbuild-0.0.10 (c (n "panbuild") (v "0.0.10") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0p387zqnl69z31bd3s3cylqar0nfm56mckk97n0n7zlcymd6hwgk")))

(define-public crate-panbuild-0.0.11 (c (n "panbuild") (v "0.0.11") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "18bvrpp96ynfa5a8b8x6sa63xczk718xpp0bj9j65g7g2hapkxq0")))

