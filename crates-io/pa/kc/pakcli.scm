(define-module (crates-io pa kc pakcli) #:use-module (crates-io))

(define-public crate-pakcli-0.1.0 (c (n "pakcli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "076wsgfwnpwr17aq8y52vcn995g4pb7pg7j5v39p07q3bgqrpw9s")))

(define-public crate-pakcli-0.2.1 (c (n "pakcli") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "0ipah16pakd8k5mz44j7i98krggz7nvhxz6vn37522pvzvsrhn94")))

(define-public crate-pakcli-1.0.0 (c (n "pakcli") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "0q3vcyymsbnh6vg6aps15cyxxxbcjfrybcpj5g447drhchs3zhwi")))

(define-public crate-pakcli-1.0.1 (c (n "pakcli") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "1d5vhz84jhw5mdm0azalp6hwh3dx93vrv09jmrhbng6qwrimy2q4")))

(define-public crate-pakcli-1.0.2 (c (n "pakcli") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "0fv8q8c0jgrrs25p731i4pnbidkcm70c20hqv95437abj3arn5ys")))

