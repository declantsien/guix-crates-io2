(define-module (crates-io pa ks paksir) #:use-module (crates-io))

(define-public crate-paksir-0.1.0 (c (n "paksir") (v "0.1.0") (h "0v2jg8q57y0a8liz8l6qbrzz84iwgx7ik5xlcq2mizgz8jsqcqf1")))

(define-public crate-paksir-0.1.1 (c (n "paksir") (v "0.1.1") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)))) (h "1wdyaw7agx9mwj08r0lvla505m8avrhy3a85zh0vcgjsgmzqhpbc")))

