(define-module (crates-io pa ho paho-mqtt-sys) #:use-module (crates-io))

(define-public crate-paho-mqtt-sys-0.2.0 (c (n "paho-mqtt-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.42") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1fs1xcs4ryz9w1h9hxigfvfczcmhfi6w72ma29pxzbbkmr25rvmf") (f (quote (("ssl") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.2.1 (c (n "paho-mqtt-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "17qydcvrr846c843iysgna1gwgarcdlw63jsbaazy0d9hg0mfnza") (f (quote (("ssl") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.2.2 (c (n "paho-mqtt-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0j65blkwxgk1yxv9y3fzi6dkf8qpzam1dfyzhh50i60w4ddn5qdh") (f (quote (("ssl") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen")))) (y #t)))

(define-public crate-paho-mqtt-sys-0.3.0 (c (n "paho-mqtt-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1pkqzcjfphic74h8l9jbws8fa3awxls4zyz7nq8ghgmgpjmq2vx2") (f (quote (("ssl") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.4.0 (c (n "paho-mqtt-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "13345gvr8hbyyxxrg6cw0qz1qbq1s3p3wj1xqpavn3mr4cdd03qb") (f (quote (("ssl") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.5.0 (c (n "paho-mqtt-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1r4cma4qazwdp8fj1x5hiz1y9jb6nr5bjqhjsl6cgrx7fxmarn8s") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.6.0 (c (n "paho-mqtt-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "=0.1.45") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "171krr8qbw8s921wmq9svhsfbww8mr6v2vl0gzgpvss09pgcbp1q") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.7.0 (c (n "paho-mqtt-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "=0.1.45") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1if0ass262d5kfdhl3q3nvxhcnbqgyns667315byskk44x7j9rhh") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.7.1 (c (n "paho-mqtt-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "=0.1.45") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "13basvk2nlm9kf734lrc5dnnc0gkfs1klvr6vclk4cvdw6d9hw7r") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.7.2 (c (n "paho-mqtt-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1j0ikygha38my884ss23gplq48rammdf439122a5n43hrlp2r8yr") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen"))))))

(define-public crate-paho-mqtt-sys-0.8.0 (c (n "paho-mqtt-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "03387palrf2rsjijfzrgijsbwbwf2rf0x0hnnyym79gd3l4nf9zr") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen")))) (r "1.63")))

(define-public crate-paho-mqtt-sys-0.8.1 (c (n "paho-mqtt-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1jab8q2kybq96xmnbn1svcyxjdzgl9sk2zac58d9abvibpkvb0hp") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen")))) (r "1.63.0")))

(define-public crate-paho-mqtt-sys-0.9.0 (c (n "paho-mqtt-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1x3bmd20k55bj76awryj2b3pvy4lbw7xgvh77k24xbs7v0cj8j2y") (f (quote (("vendored-ssl" "bundled" "ssl" "openssl-sys/vendored") ("ssl" "openssl-sys") ("default" "bundled" "ssl") ("bundled" "cmake") ("build_bindgen" "bindgen")))) (r "1.63.0")))

