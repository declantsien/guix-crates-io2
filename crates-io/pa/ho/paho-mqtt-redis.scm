(define-module (crates-io pa ho paho-mqtt-redis) #:use-module (crates-io))

(define-public crate-paho-mqtt-redis-0.2.1 (c (n "paho-mqtt-redis") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0xg0h4dpj6nz10nhhp6q2nxg99bgx4v0l7x00cy489dp5rq0axhk")))

(define-public crate-paho-mqtt-redis-0.2.2 (c (n "paho-mqtt-redis") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "02ga8cp74sx07g8x2k1bxv4hxn8b75mmc53iq1krmncji3xpvirx")))

(define-public crate-paho-mqtt-redis-0.3.0 (c (n "paho-mqtt-redis") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.19") (d #t) (k 0)))) (h "1lspziz9slsz8af1r4lizlkx9rrw3fmp196kczgy0zigib2z8xab")))

(define-public crate-paho-mqtt-redis-0.3.1 (c (n "paho-mqtt-redis") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)))) (h "15k60afvjq29dlsal5dl17f5f24pyq9w70yh4hs7rcigbd7j91in")))

(define-public crate-paho-mqtt-redis-0.3.2 (c (n "paho-mqtt-redis") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.12") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1x3g078p3dfq5ap2prwdknq35rf2859186ppynv56xf4k441d8zg")))

