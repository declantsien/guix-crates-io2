(define-module (crates-io pa ho paho_mqtt_c-sys) #:use-module (crates-io))

(define-public crate-paho_mqtt_c-sys-0.1.0 (c (n "paho_mqtt_c-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.34") (d #t) (k 1)))) (h "126ab9drkrrfb42g59zii6mlslnn8l99bdkziv8i7kxmyyx3r1nw")))

(define-public crate-paho_mqtt_c-sys-0.1.1 (c (n "paho_mqtt_c-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.34") (d #t) (k 1)))) (h "0mkjhrwhy0vyhs63cpa0r757swgnq0x36csib2jg6qhi3fvy496f")))

(define-public crate-paho_mqtt_c-sys-0.1.2 (c (n "paho_mqtt_c-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.34") (d #t) (k 1)))) (h "1v1rxzrgi29jb5yg6za61mcgmhf5248gclk28q2c992ixc4klg5n") (l "paho_mqtt_c_sys")))

