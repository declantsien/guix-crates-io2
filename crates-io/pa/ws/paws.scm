(define-module (crates-io pa ws paws) #:use-module (crates-io))

(define-public crate-paws-0.0.0 (c (n "paws") (v "0.0.0") (h "0x02jd7vnzah1zmw3nh2rv2yz19j4p0pmiw6dqi70ylgz7djiyfk")))

(define-public crate-paws-0.1.0 (c (n "paws") (v "0.1.0") (h "05aqkpnnchiy7fwkm6zkrsg7s2xcpspvy1f9b1c82hirnk91qwz6")))

(define-public crate-paws-0.2.0 (c (n "paws") (v "0.2.0") (h "0pxac196ah07n4qb5cljbhkg71z0fnpqrr1ysr2sphj9n9rabgr4")))

(define-public crate-paws-0.3.0 (c (n "paws") (v "0.3.0") (h "1jav8lfryc94jk3cdrm7l77lhsvzhxrx20b4h5hbny5942lna0av")))

(define-public crate-paws-0.3.1 (c (n "paws") (v "0.3.1") (h "0j0fhbnz1znmx9dmlh2zmzh50gpwb9v6fxvh8lccvv82mdnyjflm")))

(define-public crate-paws-0.3.2 (c (n "paws") (v "0.3.2") (h "049a20kilhrr22r37mm250wd7ph7y1sr41n35aj0xkh3ymmw1c3l")))

(define-public crate-paws-0.3.3 (c (n "paws") (v "0.3.3") (h "1qghh4njd5l9cs015v6l2gkl5hvrhvl57j66ryb58hfp2lpfij7h")))

