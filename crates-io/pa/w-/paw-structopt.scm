(define-module (crates-io pa w- paw-structopt) #:use-module (crates-io))

(define-public crate-paw-structopt-1.0.0 (c (n "paw-structopt") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1iwg83xqjpfgpy8wrq173cy7zgkyxfryd230sh34f5qsjdx7zap4")))

