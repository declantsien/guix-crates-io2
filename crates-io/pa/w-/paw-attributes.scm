(define-module (crates-io pa w- paw-attributes) #:use-module (crates-io))

(define-public crate-paw-attributes-0.0.0 (c (n "paw-attributes") (v "0.0.0") (h "1f9lz46a629q3hjsnkiynlqx5nv3zg9pxjwbhqhs0p3i4jv6rxfq")))

(define-public crate-paw-attributes-1.0.0 (c (n "paw-attributes") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sasafgblzl87629qlr1arwdfv6qpbqhvyf056snxf16lrlw34fp")))

(define-public crate-paw-attributes-1.0.2 (c (n "paw-attributes") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fda1v7y5pfmg8d2v7m0pyvif6c44qjz914jjn718pdyclrmhd8g")))

