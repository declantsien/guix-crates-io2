(define-module (crates-io pa -r pa-rs) #:use-module (crates-io))

(define-public crate-pa-rs-0.1.0 (c (n "pa-rs") (v "0.1.0") (h "16i8bbsp3vhb94f4p777cyh4c20rm26hwxaa63saipc21qmdfcz0")))

(define-public crate-pa-rs-0.1.1 (c (n "pa-rs") (v "0.1.1") (h "18ijvbi1qk0xhg39288bh5yjzyw88w9rk7y5m9s2gam2pnmirzil")))

(define-public crate-pa-rs-0.1.2 (c (n "pa-rs") (v "0.1.2") (h "11w6a9jmwgdks4fdwkha46fr24n8mbbgz7d599jjyp6zmzvfbfp9")))

(define-public crate-pa-rs-0.1.3 (c (n "pa-rs") (v "0.1.3") (h "1hppqm3n057xjq5jflk0szfxrv44vg0plmzwhqygagk1d4gcyyac")))

(define-public crate-pa-rs-0.1.4 (c (n "pa-rs") (v "0.1.4") (h "07657zjqq1d8f4lbc4qlx9jwgd2ijq75cwqgp5iggxvxg6b8fmpy")))

(define-public crate-pa-rs-0.1.5 (c (n "pa-rs") (v "0.1.5") (h "0jipghvzjk1ngy3ygv1x5xg1wq4620mhvlkknqjgskclym522jrb")))

