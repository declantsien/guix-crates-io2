(define-module (crates-io pa -r pa-rse) #:use-module (crates-io))

(define-public crate-Pa-rsE-1.0.2 (c (n "Pa-rsE") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive" "std"))) (k 0)) (d (n "colored") (r "^2.0.0") (k 0)) (d (n "dirs") (r "^4.0.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 0)))) (h "00y2cy1jm21gy0jywsrg30l63mwmiv8xpa9p5xbj0077s681jr2d")))

