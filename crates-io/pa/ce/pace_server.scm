(define-module (crates-io pa ce pace_server) #:use-module (crates-io))

(define-public crate-pace_server-0.1.0 (c (n "pace_server") (v "0.1.0") (h "0cd24yyymrd4cgxbb3qccr0rzf2zzzvnbwckhkq0vd50phygdn60") (r "1.74.1")))

(define-public crate-pace_server-0.1.1 (c (n "pace_server") (v "0.1.1") (h "12hyf7jj4afx9f2825znhb575nc836g1d17jh5zsw7himaxdrvdd") (r "1.74.1")))

(define-public crate-pace_server-0.1.2 (c (n "pace_server") (v "0.1.2") (h "1ay5bivnmzz5lg5n0zi63bpmsqjpwivj33y223hhsbl64v2dr3rw") (r "1.74.1")))

(define-public crate-pace_server-0.1.3 (c (n "pace_server") (v "0.1.3") (h "0k5dw405f304b1cpapsq4f2vfq0jln2rhd77m7d7s2njpnczf4l1") (r "1.74.1")))

