(define-module (crates-io pa ce pace_testing) #:use-module (crates-io))

(define-public crate-pace_testing-0.1.0 (c (n "pace_testing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pace_core") (r "^0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "154qcvy43756zagh4siwyw6iid0fxsy5qfhmb45nscmlnkkwrwzx")))

(define-public crate-pace_testing-0.1.1 (c (n "pace_testing") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pace_core") (r "^0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0c2y67rac2f68iwj2by9m606ykv5gizw5x778vrqhrd9906pq058")))

(define-public crate-pace_testing-0.1.2 (c (n "pace_testing") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pace_core") (r "^0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "15q6qvggcf4j7arjscsam24zx28bfklsbazmh7m8dazmjvlpn8jy")))

(define-public crate-pace_testing-0.1.3 (c (n "pace_testing") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pace_core") (r "^0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1ibn5n2vm23ilbq7yz79kvmglxg1g9sysk143m4473qv78g78qrj")))

(define-public crate-pace_testing-0.2.0 (c (n "pace_testing") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "pace_core") (r "^0") (d #t) (k 0)) (d (n "pace_time") (r "^0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 0)))) (h "0jl9abciqsn0kqgqlpcaqcd2whd4b96y591zrg0aw6pfirndq6mz")))

