(define-module (crates-io pa py papyrust) #:use-module (crates-io))

(define-public crate-papyrust-0.1.0 (c (n "papyrust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kcc1y6x1609a2nnnasmfr80ji3ay38b1b630ivc8wynf6fx168g")))

(define-public crate-papyrust-0.1.1 (c (n "papyrust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zjq2b3ljxnq5wl8jwmfl2c0f96l96a8qj4irjpvydjahcqbwypf")))

