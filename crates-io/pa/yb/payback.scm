(define-module (crates-io pa yb payback) #:use-module (crates-io))

(define-public crate-payback-0.4.2 (c (n "payback") (v "0.4.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.2.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)))) (h "08fnfgy6kdkkf1dxi5lskpf4gmfg2hm91qb66mkrlmhdnlsllcyb")))

