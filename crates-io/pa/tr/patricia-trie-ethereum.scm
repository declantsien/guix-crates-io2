(define-module (crates-io pa tr patricia-trie-ethereum) #:use-module (crates-io))

(define-public crate-patricia-trie-ethereum-0.1.0 (c (n "patricia-trie-ethereum") (v "0.1.0") (d (list (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hashdb") (r "^0.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-bytes") (r "^0.1") (d #t) (k 0)) (d (n "patricia-trie") (r "^0.2.1") (d #t) (k 0)) (d (n "rlp") (r "^0.2.4") (f (quote ("ethereum"))) (d #t) (k 0)))) (h "0lvy26xbj9l0b48jnxqbw9qj521bjqcbgj9w5m50va4a5z9mzp7j")))

