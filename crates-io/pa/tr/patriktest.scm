(define-module (crates-io pa tr patriktest) #:use-module (crates-io))

(define-public crate-patriktest-0.1.0 (c (n "patriktest") (v "0.1.0") (h "1138plwgs2bg5cbv1ps92g6jwb9m3pkqka3nlyxz73bgvw1cajq1")))

(define-public crate-patriktest-0.2.0 (c (n "patriktest") (v "0.2.0") (h "0cbfcn9cxqyqxlzgn0d0c395pc2grc41wjmhqk3pa1q1881dxl3n")))

(define-public crate-patriktest-0.3.0 (c (n "patriktest") (v "0.3.0") (h "1s7d0kpljcs7sz1zys0gxj6swvgqba6iqb33k1yn7h4dq7v1m8mv")))

(define-public crate-patriktest-0.3.0-fooo-2132.bar-test (c (n "patriktest") (v "0.3.0-fooo-2132.bar-test") (h "0f1gynm1ab7hix8y50r9srgq0q46mxqm3483600nmmn23hwl91ac")))

