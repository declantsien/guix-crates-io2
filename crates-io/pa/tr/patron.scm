(define-module (crates-io pa tr patron) #:use-module (crates-io))

(define-public crate-patron-0.1.0 (c (n "patron") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "135bsicza2k9ihf1vas29zgz9ix6z45zj8i60a7mwbna92m60rsa")))

(define-public crate-patron-0.2.0 (c (n "patron") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1viparkdphjb1dc01lnbxrlrsmlng2k33ll2v7a0k0vl5g12b98f")))

(define-public crate-patron-0.2.1 (c (n "patron") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0bjagz317ga41zd6w98law5iznwwslakcfprlv2p607xph5nbwxk")))

