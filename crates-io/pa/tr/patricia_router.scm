(define-module (crates-io pa tr patricia_router) #:use-module (crates-io))

(define-public crate-patricia_router-0.1.0 (c (n "patricia_router") (v "0.1.0") (h "18v3xgkl6zprg9lb6rh7hj20068382k7p0hn665wvd12g4lpgdi5")))

(define-public crate-patricia_router-0.1.1 (c (n "patricia_router") (v "0.1.1") (h "1g72r91l1p85rwq4sjckf2jlb38ii8v3nzanq6lmsi64lda38vzr")))

