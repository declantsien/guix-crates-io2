(define-module (crates-io pa rs parse_sbus) #:use-module (crates-io))

(define-public crate-parse_sbus-0.1.0 (c (n "parse_sbus") (v "0.1.0") (h "17vgq8xgslsp8wqy88i5cm47fvkqdrpkfd32hsv7vbw1s9gc6vd6")))

(define-public crate-parse_sbus-0.1.1 (c (n "parse_sbus") (v "0.1.1") (h "1x4hrm3cv1ril2vns5xsanc1si8jp978613282njq647ahwskzxx")))

