(define-module (crates-io pa rs parsec-kak) #:use-module (crates-io))

(define-public crate-parsec-kak-0.1.0 (c (n "parsec-kak") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)))) (h "0br1nbaird0nxyma8ymi0dsrfl60isswr1zkliijbx027zji69ky")))

(define-public crate-parsec-kak-0.1.1 (c (n "parsec-kak") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)))) (h "0gd7mxyzsn6gw0v8mw0v3vgj5ia6gp6pdahlrzs8mqc50h5qiylf")))

(define-public crate-parsec-kak-0.1.2 (c (n "parsec-kak") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)))) (h "0wyb1x457fhl9d1wxvwbcpi49i1q3g7m91apmi3jwa02av5rvpd9")))

(define-public crate-parsec-kak-0.1.3 (c (n "parsec-kak") (v "0.1.3") (d (list (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r ">=0.1.2") (d #t) (k 0)))) (h "1pn577qr4gdf50ka2ifjkdxh75j5d36jjhaq5s96khk8ri7bvsci")))

(define-public crate-parsec-kak-0.2.0 (c (n "parsec-kak") (v "0.2.0") (d (list (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r ">=0.2.0") (d #t) (k 0)))) (h "1d9mlqn7j97b0q9by6xp1pdz1zc20s4gqkbiz73jjja96p01k89a")))

