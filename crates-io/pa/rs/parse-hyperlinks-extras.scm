(define-module (crates-io pa rs parse-hyperlinks-extras) #:use-module (crates-io))

(define-public crate-parse-hyperlinks-extras-0.23.1 (c (n "parse-hyperlinks-extras") (v "0.23.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1pjsjfk8qi18r0ji1ksp6f54gzpz723qrisy2bhnbny4v504239g")))

(define-public crate-parse-hyperlinks-extras-0.23.2 (c (n "parse-hyperlinks-extras") (v "0.23.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0fcpcqw1axq4rf96k75gbmdjmiyabgml953i9xxmax7rk2lzs1bg")))

(define-public crate-parse-hyperlinks-extras-0.23.3 (c (n "parse-hyperlinks-extras") (v "0.23.3") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1f3r1ida5qkiqdbvp6y399v4mdzx8l5h0hvnc7wfniz4cw76x704")))

(define-public crate-parse-hyperlinks-extras-0.23.4 (c (n "parse-hyperlinks-extras") (v "0.23.4") (d (list (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "049rw420m9wkvgb4h49d8i9nzb3xxwvi3l8ww76adbwlwdpcc84n")))

(define-public crate-parse-hyperlinks-extras-0.24.0 (c (n "parse-hyperlinks-extras") (v "0.24.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.24.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1g5k30wzywrkggijb4djvjfwdapslh9cnp5dnb7j16la92yq3ygn") (y #t)))

(define-public crate-parse-hyperlinks-extras-0.24.1 (c (n "parse-hyperlinks-extras") (v "0.24.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.24.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0pjsm21afl7v7i9fjbl71z7qq5gl7bam96jpl9sns5rkhayrm913")))

(define-public crate-parse-hyperlinks-extras-0.24.2 (c (n "parse-hyperlinks-extras") (v "0.24.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.24.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "06mp7crsbd074394qi0i72xa8m93mz452nrdmrw3czd1vcymvlmm")))

(define-public crate-parse-hyperlinks-extras-0.25.0 (c (n "parse-hyperlinks-extras") (v "0.25.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.25.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0bxp1zbprbh4495ykgzh6hadxhfbwa4z7fd8h5pdlh4k678nrqjh")))

(define-public crate-parse-hyperlinks-extras-0.26.0 (c (n "parse-hyperlinks-extras") (v "0.26.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.26.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ymklyy1hk2cijb7d048riznzjn6wjfznsnfqrkrrx1a0si2xy1q")))

(define-public crate-parse-hyperlinks-extras-0.25.1 (c (n "parse-hyperlinks-extras") (v "0.25.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.25.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "01mx6qf1qhzjrzahxyr71rsx0hqn8sxap4himqfav9l0gh47lxav")))

(define-public crate-parse-hyperlinks-extras-0.26.1 (c (n "parse-hyperlinks-extras") (v "0.26.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.26.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0vm6bkxwcj77zcsqrknx3z8fvkn94br60ix1ynk5lsflbims3brg")))

(define-public crate-parse-hyperlinks-extras-0.26.2 (c (n "parse-hyperlinks-extras") (v "0.26.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.26.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0x1q21az6hb3braiirg5rrdamcfrc38hxlr5hp6kg1ldkd25m3ad")))

(define-public crate-parse-hyperlinks-extras-0.26.3 (c (n "parse-hyperlinks-extras") (v "0.26.3") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.26.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "14q9nc7rwa6rjk100g3wz4pn08rhqwnj5qa5czqmas207b2q5fvs")))

(define-public crate-parse-hyperlinks-extras-0.26.4 (c (n "parse-hyperlinks-extras") (v "0.26.4") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.26.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ybk43gwsj1caqvdws0qclavdjfw864lnfvfng84fs8wfqhsda2s")))

(define-public crate-parse-hyperlinks-extras-0.27.0 (c (n "parse-hyperlinks-extras") (v "0.27.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.27.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1ph4ikkchbmypngi5yf8cpbn8mdwrkv8388j8qmd6nqf7vbdw9y2")))

(define-public crate-parse-hyperlinks-extras-0.27.1 (c (n "parse-hyperlinks-extras") (v "0.27.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.27.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "068n9p566gwlba62chc5nscq68hiziklqvffbjrka0nmp6npgan6")))

(define-public crate-parse-hyperlinks-extras-0.27.2 (c (n "parse-hyperlinks-extras") (v "0.27.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.27.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "08ivs870457kqc7dhwqd4fwbqcbyw96pq94ws96a6y02v66rj6wy")))

