(define-module (crates-io pa rs parsimonious) #:use-module (crates-io))

(define-public crate-parsimonious-0.0.1 (c (n "parsimonious") (v "0.0.1") (h "1csazvi635i6ak71l47cv22gc9c97r9dmwxr37vhrp1ra0nn8l94")))

(define-public crate-parsimonious-0.0.2 (c (n "parsimonious") (v "0.0.2") (h "0yl15gyp72mhgnpbvq7pr3s0kxlm126l5mcilpdpcsn6kp2fij5l")))

(define-public crate-parsimonious-0.0.3 (c (n "parsimonious") (v "0.0.3") (h "00qslsdhla8fd9vfsrv01cn4dyrvnj2jas4ghi99l7saj92xsggz")))

(define-public crate-parsimonious-0.0.4 (c (n "parsimonious") (v "0.0.4") (h "0api03gg4jji11r226rv1zh3k862ddcl61mvvahy0kdwnpyxqv2s")))

(define-public crate-parsimonious-0.0.5 (c (n "parsimonious") (v "0.0.5") (h "04br19li5qlhjcggp1crjcqvw9pvg2clyigmmwn4jffylm2nvi2x")))

(define-public crate-parsimonious-0.0.6 (c (n "parsimonious") (v "0.0.6") (h "0p9yjb4bnbvrr22c7bcm20i9gf7swdr2b4hibv90qaj83dykg630")))

(define-public crate-parsimonious-0.0.7 (c (n "parsimonious") (v "0.0.7") (h "1jqvnm7aav9iva4d6zk9cd8zkm98wc3jln6lk5g884zicl49mgl5")))

(define-public crate-parsimonious-0.0.8 (c (n "parsimonious") (v "0.0.8") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1mxgnzwj7mlryk60r2nc159j2fdcmwvzyy0rxz2vsawy7zgkpnh1")))

