(define-module (crates-io pa rs parse_duration) #:use-module (crates-io))

(define-public crate-parse_duration-0.1.0 (c (n "parse_duration") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "02wjrf4k34k8da22zppfwrx58kfmghg3llhzi3jljxjphh6i7zxv")))

(define-public crate-parse_duration-1.0.0 (c (n "parse_duration") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "06k114gd0vppm0ya2qgsaw6cimkgvsbypk218jg5cypjw37g05qx")))

(define-public crate-parse_duration-1.0.1 (c (n "parse_duration") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1q79r26sr8vr879xhbfrry6ywwxmky2vh02bwj19jjb9xc5b1a1w")))

(define-public crate-parse_duration-1.0.2 (c (n "parse_duration") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06qcg4b1jclzad0vqwqmp02qjk1kwbsv4vlp6hm8ivd0mwc8ix6c")))

(define-public crate-parse_duration-1.0.3 (c (n "parse_duration") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m5292zzx71wjr50r7ianl4chh1r3iq4liyd3gca2h94x9nilcxh")))

(define-public crate-parse_duration-2.0.0 (c (n "parse_duration") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c37ii35sjzcrxdp99ycpil4id6p73bish1kqhj81bhgq8sickpr")))

(define-public crate-parse_duration-2.0.1 (c (n "parse_duration") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hvf96anhkj888k9v80l4s3j3k2i9pz94485lnihvclmsj8g4hc4")))

(define-public crate-parse_duration-2.1.0 (c (n "parse_duration") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bljgqz2c46vgppajlcb9361ys3jdz3pisjxqnx1smsgp8j873fw")))

(define-public crate-parse_duration-2.1.1 (c (n "parse_duration") (v "2.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pd97dmlv1i6pvr2byi65q1fzv667gvhnf3ld2lsawh17vlyadvh")))

