(define-module (crates-io pa rs parse-hosts) #:use-module (crates-io))

(define-public crate-parse-hosts-0.1.0 (c (n "parse-hosts") (v "0.1.0") (d (list (d (n "multistr") (r "^0.2") (d #t) (k 0)))) (h "1khvncswasslnwhvr5zrrklbj8as2c7sx9yda8w9lh6611mgmbfh")))

(define-public crate-parse-hosts-0.2.0 (c (n "parse-hosts") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "multistr") (r "^0.2") (d #t) (k 0)))) (h "1pf1rynajm2n5jwcxd0yxnly06dwwgbnkidivf3k41my4235bhk0")))

(define-public crate-parse-hosts-0.3.0 (c (n "parse-hosts") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "multistr") (r "^0.2") (d #t) (k 0)))) (h "01gps6ii1ipzikdxxhlyrhz6c1j5j751ygvmk86z2yglz2gf49kn")))

(define-public crate-parse-hosts-0.4.0 (c (n "parse-hosts") (v "0.4.0") (d (list (d (n "multistr") (r "^0.5") (d #t) (k 0)))) (h "19lmjnwkwj3p7l55yvv4vd6snrjs6a9rr48ald0dkracz2jg9sws")))

(define-public crate-parse-hosts-0.5.0 (c (n "parse-hosts") (v "0.5.0") (d (list (d (n "multistr") (r "^0.5.2") (d #t) (k 0)))) (h "0a5ww8hv0p3a6h8cq5fdq13mf0kkdbnhj5vgdvdr25i852w2v17k")))

