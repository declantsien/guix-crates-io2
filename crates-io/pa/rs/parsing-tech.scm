(define-module (crates-io pa rs parsing-tech) #:use-module (crates-io))

(define-public crate-parsing-tech-0.1.0 (c (n "parsing-tech") (v "0.1.0") (h "06j8rqpid1zgjkjy64g1h2qiqjjwli8xnq9qx7lhzhlir2g4rpcr") (y #t)))

(define-public crate-parsing-tech-0.1.1 (c (n "parsing-tech") (v "0.1.1") (d (list (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 1)))) (h "12wbb0zi7g2n44a24vfrh9m9v00fpn3g55iw3k4r8m1i2cpv5z7d") (y #t)))

(define-public crate-parsing-tech-0.1.2 (c (n "parsing-tech") (v "0.1.2") (d (list (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w00ax7fbcnv47xq04r2yykrf3i6fwm7f009xskf3ci3k9hk79y9") (y #t)))

