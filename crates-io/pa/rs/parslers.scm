(define-module (crates-io pa rs parslers) #:use-module (crates-io))

(define-public crate-parslers-0.1.0 (c (n "parslers") (v "0.1.0") (h "0pzmmznqmi86301wdpfw6a4ccbqfyhdsz90crh5mnx8s7f5c3bfx")))

(define-public crate-parslers-0.1.1 (c (n "parslers") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "parslers-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0f4wfal2bmd6xqx8bzp6yb4ic85r8w6lmvwpzj9w2xyqfyhqf5zg") (r "1.70.0")))

