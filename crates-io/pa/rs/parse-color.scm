(define-module (crates-io pa rs parse-color) #:use-module (crates-io))

(define-public crate-parse-color-0.1.0 (c (n "parse-color") (v "0.1.0") (h "1s72jcz5aiwnf62810rhghg29w2cmclrp80yr809z4dndaj4xrwy") (f (quote (("tailwind") ("default" "tailwind"))))))

(define-public crate-parse-color-0.1.1 (c (n "parse-color") (v "0.1.1") (h "12w96ryhqzh7anf2k94lq43qjsfy3153rwzlfr5nblymf558sl42") (f (quote (("tailwind") ("default" "tailwind"))))))

(define-public crate-parse-color-0.1.2 (c (n "parse-color") (v "0.1.2") (h "0l1qxq14a7nviiqckbpsl5i75jbbs18kdxyqmxggzcx2nx6chns0") (f (quote (("tailwind") ("default" "tailwind"))))))

