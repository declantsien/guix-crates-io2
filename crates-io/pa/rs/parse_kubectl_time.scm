(define-module (crates-io pa rs parse_kubectl_time) #:use-module (crates-io))

(define-public crate-parse_kubectl_time-0.1.0 (c (n "parse_kubectl_time") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "00fgv2i95glrrqkqj67vcqvh6psayja9sf1wg4gfx0papxkc18sb")))

(define-public crate-parse_kubectl_time-0.2.0 (c (n "parse_kubectl_time") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1mp78yfqw36yic0bd5lwqgi4grb1q2aqjrrlv4w0c48yv9j9my0y")))

(define-public crate-parse_kubectl_time-0.2.1 (c (n "parse_kubectl_time") (v "0.2.1") (h "0kpffc9w2nzklphfbmb94hygk59ii57j94lkwbdnw03fcwbci744")))

(define-public crate-parse_kubectl_time-0.2.2 (c (n "parse_kubectl_time") (v "0.2.2") (h "0vi5arq1d4b94k0fsaj2nbv9p5b5x63b5pkg1inv714qbzk5jyh9")))

