(define-module (crates-io pa rs parse_duration0) #:use-module (crates-io))

(define-public crate-parse_duration0-3.0.0 (c (n "parse_duration0") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.5.6") (f (quote ("std" "perf"))) (k 0)))) (h "10hq0195kdlkgiq677cq4xbbhw20300y49f0wx8hh17vz6jjj49l")))

