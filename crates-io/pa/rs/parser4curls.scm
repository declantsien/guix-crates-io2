(define-module (crates-io pa rs parser4curls) #:use-module (crates-io))

(define-public crate-parser4curls-0.1.0 (c (n "parser4curls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06zpyzj1bgp5z4j7jhiisw2kp650mjb4m8czvaiqqhyma3zv9la5")))

(define-public crate-parser4curls-0.1.1 (c (n "parser4curls") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1c1c3jyh1z5hjb6dn5b5r3byg832cvx3pqyqzd02rlm2x9054vbq")))

