(define-module (crates-io pa rs parsiphae) #:use-module (crates-io))

(define-public crate-parsiphae-0.3.0 (c (n "parsiphae") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1c668r8ss6kscq2hadhi6waljcbbsya1hdwscn2cf1snd0sk57vw")))

