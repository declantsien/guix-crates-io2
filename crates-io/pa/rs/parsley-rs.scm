(define-module (crates-io pa rs parsley-rs) #:use-module (crates-io))

(define-public crate-parsley-rs-0.0.1 (c (n "parsley-rs") (v "0.0.1") (h "15xa0xa9pxw1drc96kzzj08is2f8s6jzzzlh8w9fs4kvz5fa8q6b")))

(define-public crate-parsley-rs-0.0.2 (c (n "parsley-rs") (v "0.0.2") (h "1y8d03y5y8lja076qclnxw8dxvl3zlkf068ld4398na0c6c6cxwg")))

(define-public crate-parsley-rs-0.0.3 (c (n "parsley-rs") (v "0.0.3") (h "1qa6jqgj5yh64kik9fpj221kxhsdy26q1whiyip0cgpmzsijqnkc")))

(define-public crate-parsley-rs-0.0.4 (c (n "parsley-rs") (v "0.0.4") (h "14k3r4lp0frv37fvbija7nhczbk2995x8g57yf4v9d2r7cwqk2kk")))

(define-public crate-parsley-rs-0.0.5 (c (n "parsley-rs") (v "0.0.5") (h "0xvcdqdnfhf0qv26qj6s1rrsa1bvf0xrmyaqmjqdhg14l17qm9pi")))

(define-public crate-parsley-rs-0.0.6 (c (n "parsley-rs") (v "0.0.6") (h "0a7rcwpk416wk11xzxdq7fcikf9a921b9nca5xx263636h7q6dvf")))

(define-public crate-parsley-rs-0.0.7 (c (n "parsley-rs") (v "0.0.7") (h "1bnf40pnsfrly77k1yql1i3gcaw4bvgwyx5n41267zbqlvvcm30a")))

(define-public crate-parsley-rs-0.0.8 (c (n "parsley-rs") (v "0.0.8") (h "09kiqndcv6lqv03ymxi9zsfrnx9ckmvnyjlnkj4zc9kzf96jbfnp")))

(define-public crate-parsley-rs-0.0.9 (c (n "parsley-rs") (v "0.0.9") (h "1jw726g7bkrnqij9p7qyx1lrkfy6wkz8xij706brr22yjpyy4q08")))

(define-public crate-parsley-rs-0.0.10 (c (n "parsley-rs") (v "0.0.10") (h "1kbncxghya1anbzj1mhlhxv9fnks6wk4h98pqs741c1y4nq81n27")))

(define-public crate-parsley-rs-0.0.11 (c (n "parsley-rs") (v "0.0.11") (d (list (d (n "character-stream") (r "^0.2.0") (d #t) (k 0)))) (h "0rwsmvcpjv44irx98mripvgc53005fq4s7r8zmyqwwaxpfz66kxn")))

(define-public crate-parsley-rs-0.0.12 (c (n "parsley-rs") (v "0.0.12") (d (list (d (n "character-stream") (r "^0.3.0") (d #t) (k 0)))) (h "03jg3pg61p3wf92x0rbpc8hymn45jbh1nvsacrbw6g9iincj36wj")))

(define-public crate-parsley-rs-0.1.0 (c (n "parsley-rs") (v "0.1.0") (d (list (d (n "character-stream") (r "^0.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0krxd7ymvq778k0h5b398wsviglikw7incnhvgvd152b74v7w3rr")))

(define-public crate-parsley-rs-0.2.0 (c (n "parsley-rs") (v "0.2.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1vl0w22nxl3swpn66a2v8fmjcgvbphbn8qsnanv591dc3c0c6xv8")))

(define-public crate-parsley-rs-0.2.1 (c (n "parsley-rs") (v "0.2.1") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1w7na2p8gcj1dvxwarnyqkycc1f3wjbfglmqmf20ifia92nzc532")))

(define-public crate-parsley-rs-0.3.0 (c (n "parsley-rs") (v "0.3.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "16c2ylgjmijcr84cry7msm99wqhvxdbqcv7apdhs4fvy08rjdnzk")))

(define-public crate-parsley-rs-0.3.1 (c (n "parsley-rs") (v "0.3.1") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "14cjjwzqw4k71n214kh4djjammfqkq3ndq0lq99bwnzkmwid7ijw")))

(define-public crate-parsley-rs-0.4.0 (c (n "parsley-rs") (v "0.4.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "02kgcgjaxwr24rs6c8bc1s5pgd10vk0ipijna4nhzci96121kp5g")))

(define-public crate-parsley-rs-0.4.1 (c (n "parsley-rs") (v "0.4.1") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1gzs12j0gkpx7pr8y8fmyasf9wwcgh32z1r4sgnp2ww2l4vfkn73")))

(define-public crate-parsley-rs-0.5.0 (c (n "parsley-rs") (v "0.5.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0xp13h5j94mna3xrr5q7fk6aysjah7n8si5s9q076b9xi9s1wws8")))

(define-public crate-parsley-rs-0.6.0 (c (n "parsley-rs") (v "0.6.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0hyl9sqq0ha2vqx23cwq42i3f6jnbzcyrlbpz52s8x1j0kkpap9x")))

(define-public crate-parsley-rs-0.6.1 (c (n "parsley-rs") (v "0.6.1") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0h74djqy3dia2y3qy326b9cvvggxcab5j5klkzly8zzaywakd97g")))

(define-public crate-parsley-rs-0.7.0 (c (n "parsley-rs") (v "0.7.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0sxxvxxdkz6ihy3ky0qd3khjy351azpmhqg7dz8lnc4x4c4mgwgy")))

(define-public crate-parsley-rs-0.8.0 (c (n "parsley-rs") (v "0.8.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0kx7insknnyywjnlqfrli05bip59000bh5wmlighlg9306k1x09p")))

(define-public crate-parsley-rs-0.9.0 (c (n "parsley-rs") (v "0.9.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1ljg5kka04h63rsrzzag8qyv4rg5zs273mxpkq9cniip6akgchqs")))

(define-public crate-parsley-rs-0.10.0 (c (n "parsley-rs") (v "0.10.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1qs442vs8q32f1m7ncwj2hip95kcbhf384pz9w1pchyz44b6skl3")))

(define-public crate-parsley-rs-0.11.0 (c (n "parsley-rs") (v "0.11.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1sk9ixjwq5zi6psciickp3wj36b870z27lhcnpg7fm48piwggny2")))

(define-public crate-parsley-rs-0.12.0 (c (n "parsley-rs") (v "0.12.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0fpdyyghb6k12awwmy81a8n2y2j5i2j895zkl0xfx44js0fpbsmi")))

(define-public crate-parsley-rs-0.13.0 (c (n "parsley-rs") (v "0.13.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0ajx3rg78gysvwvfxnw4x6giq3104fnkgsyfxmpnr5gg81pldwim")))

(define-public crate-parsley-rs-0.14.0 (c (n "parsley-rs") (v "0.14.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1699z5ycnzcgrva0k5ra1h5i6f83gbcwz8xncgkn102rmz7spv1q")))

(define-public crate-parsley-rs-0.15.0 (c (n "parsley-rs") (v "0.15.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "1qyl1xa26g2dm62j5ijbzq3r1fch70dqqh473sbjlkc6gp4kli36")))

(define-public crate-parsley-rs-0.16.0 (c (n "parsley-rs") (v "0.16.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "00frqsczrbx6416r997awy898lh8cn8hmlhkfbmwkfjfqjr2pzw9")))

(define-public crate-parsley-rs-0.17.0 (c (n "parsley-rs") (v "0.17.0") (d (list (d (n "character-stream") (r "^0.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.2") (d #t) (k 0)))) (h "0pxrvk7smy0sz2iwqg3wwyjbxsxgc610m2rv2kpdr5gxbmyyrsah")))

(define-public crate-parsley-rs-0.18.1 (c (n "parsley-rs") (v "0.18.1") (d (list (d (n "character-stream") (r "^0.8") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "1v01kry5f0khakh5q3wdjps4pcnrpv7i5yrih7kg7rkk2lv024pz") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.19.0 (c (n "parsley-rs") (v "0.19.0") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "185164py6g34r8cfjsyqlanqxjzllpim0y9q4gfwxz4rp9g104ss") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.20.0 (c (n "parsley-rs") (v "0.20.0") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "0hnlyp57ckh248yv3ziswyyw5rjjxpjsvqhwz7axgfdp7sgqz4ii") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.20.1 (c (n "parsley-rs") (v "0.20.1") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "0v69zpmahckrpmv4pp507mqsl6b55ym4nvld43dfxd5haijwjiv8") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.20.2 (c (n "parsley-rs") (v "0.20.2") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "17b6n9bk15gicxwshpvvznslwgp07a3iqhmmylpvi3w77c909x6s") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.20.3 (c (n "parsley-rs") (v "0.20.3") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "11dv5md913gdi4wy609pfsyka9d8s8j1yarxp13qsyxk3wc55ra0") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

(define-public crate-parsley-rs-0.20.4 (c (n "parsley-rs") (v "0.20.4") (d (list (d (n "character-stream") (r "^0.9") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "parsley-rs-hack") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "0qs6mxbrmilb52487br39fxhw4cikb6xp101ns691nki4rlwzb2j") (f (quote (("net") ("default" "fs" "buffer" "net") ("buffer")))) (s 2) (e (quote (("fs" "dep:parsley-rs-hack") ("dns" "net" "dep:dns-lookup"))))))

