(define-module (crates-io pa rs parser) #:use-module (crates-io))

(define-public crate-parser-0.0.0 (c (n "parser") (v "0.0.0") (d (list (d (n "binding_powers") (r "^0.1.0") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "insta") (r "^1.7.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rowan") (r "^0.12.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1rgd94244jzz2ilfvn4h6jxyip8v695n4202mp16cgypjq9s7xyz") (y #t)))

