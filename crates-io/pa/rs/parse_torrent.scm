(define-module (crates-io pa rs parse_torrent) #:use-module (crates-io))

(define-public crate-parse_torrent-0.5.2 (c (n "parse_torrent") (v "0.5.2") (d (list (d (n "bencode") (r "^0.1.16") (d #t) (k 0)) (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8.22") (d #t) (k 0)) (d (n "serde_bencode") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.22") (d #t) (k 0)) (d (n "sha1") (r "^0.2.0") (d #t) (k 0)))) (h "0g6h39lqq32d1imblhzja7vx4wcqq2cxnkad3dhng2vhpi34jbbg")))

