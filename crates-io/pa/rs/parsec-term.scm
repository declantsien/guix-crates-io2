(define-module (crates-io pa rs parsec-term) #:use-module (crates-io))

(define-public crate-parsec-term-0.1.0 (c (n "parsec-term") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "02qvwsq85fqqf2r4zp2fdpnk9xpv7a6cc2snxjvqr40admqk8ng0")))

(define-public crate-parsec-term-0.1.1 (c (n "parsec-term") (v "0.1.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "13j8861zvdfqchdpmmy596q5wcf7x5904d3cdswqwh1022anilqk")))

(define-public crate-parsec-term-0.1.2 (c (n "parsec-term") (v "0.1.2") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0klqd220sn67kr5ynbm4qkxxmrsjbzvaazfn8a4c37ifya32kw5q")))

(define-public crate-parsec-term-0.1.4 (c (n "parsec-term") (v "0.1.4") (d (list (d (n "cassowary") (r ">=0.3.0") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r ">=1.10.0") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1.10") (d #t) (k 0)))) (h "0ikbacll86pb0f8jvqrhgss610y9fx7kvjbp1xjrirf6cvgczl3s")))

(define-public crate-parsec-term-0.2.0 (c (n "parsec-term") (v "0.2.0") (d (list (d (n "cassowary") (r ">=0.3.0") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "parsec-core") (r ">=0.2.0") (d #t) (k 0)) (d (n "smallvec") (r ">=1.10.0") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1.10") (d #t) (k 0)))) (h "0gch9fz58iyixw8nvnbs5qv99yqjx2sf660hvd4h4xanpd97y9bl")))

