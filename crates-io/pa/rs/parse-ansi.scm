(define-module (crates-io pa rs parse-ansi) #:use-module (crates-io))

(define-public crate-parse-ansi-0.1.0 (c (n "parse-ansi") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0fgqwjlkcc230p9z3znr0l0c7969v1mha2bvn8w9rb76gf6sv6bb")))

(define-public crate-parse-ansi-0.1.1 (c (n "parse-ansi") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0mq8pim309969w0k8nlvhmg5ilib7rp3ni03jd7z8ia7gbpf11lc")))

(define-public crate-parse-ansi-0.1.2 (c (n "parse-ansi") (v "0.1.2") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0ghk9wzlr2scl2lizh4l8m46zj0zfhn6mrcnn4hdkwns8wrhix2r")))

(define-public crate-parse-ansi-0.1.3 (c (n "parse-ansi") (v "0.1.3") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0xirrhsxk19aqgqqsa0fclj09lnlprjrqzlvazxgjwm5l4dxanp3")))

(define-public crate-parse-ansi-0.1.4 (c (n "parse-ansi") (v "0.1.4") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0j0s921zv4234kzx0bq9wzqgsna17hb8xk3q90r755914pldl4rn")))

(define-public crate-parse-ansi-0.1.5 (c (n "parse-ansi") (v "0.1.5") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1cv0r6fykni328m1sgbq2wjz1b1x0g94ll99r64k5kyj2yp1218y")))

(define-public crate-parse-ansi-0.1.6 (c (n "parse-ansi") (v "0.1.6") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0y7qvkjawfzwvmjb65xni3nch5rzb2vw0zdkdazi12jga96qpi2h")))

