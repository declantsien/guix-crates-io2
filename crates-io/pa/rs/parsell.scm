(define-module (crates-io pa rs parsell) #:use-module (crates-io))

(define-public crate-parsell-0.1.0 (c (n "parsell") (v "0.1.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "15jfn8rbvkhgkrghnai754jr6s431a914i9bm7c8chf8hxd99m2v")))

(define-public crate-parsell-0.1.1 (c (n "parsell") (v "0.1.1") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "138lkzvqsq2kbk0q0hwxicbrkn4mg9fhag14j97sh925rlrv107s")))

(define-public crate-parsell-0.2.0 (c (n "parsell") (v "0.2.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1n3jpsx4ciqshnaddl0v2ij60y5f32fz8vazlqnz89i67ishwrll")))

(define-public crate-parsell-0.2.1 (c (n "parsell") (v "0.2.1") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1s1miir68q5bq10qk0q7ysmyx7p1gv3r93iraa1s83fxsyl06r8g")))

(define-public crate-parsell-0.3.0 (c (n "parsell") (v "0.3.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0wwcs6cqv6zczrcrk7arxr2x86x2wq2xnyymw6b0fgvrk7l7i0wj")))

(define-public crate-parsell-0.3.1 (c (n "parsell") (v "0.3.1") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0q4v1inyjsz005nv23fvrq0d5ahscld7qg9z24cqmi45v07am1v0")))

(define-public crate-parsell-0.4.0 (c (n "parsell") (v "0.4.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0dkc9xi7fry4wfhbzswx9fila8rxl20vyslsr9hk90n3yxgsmzba")))

(define-public crate-parsell-0.4.1 (c (n "parsell") (v "0.4.1") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1aqg2s162yh4gvvh71p16kx4dyc5mhyancv53mcrwm7bgmnwm53f")))

(define-public crate-parsell-0.4.2 (c (n "parsell") (v "0.4.2") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "18kvw60lc8dayk5znai9v72cqby6dhdjfik2l9217p751ahc2wd1")))

(define-public crate-parsell-0.4.3 (c (n "parsell") (v "0.4.3") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "06xajdc2lhv40jxain0f37wyggfb8g0jyrs52bgskj6f7g36rk5a")))

(define-public crate-parsell-0.5.0 (c (n "parsell") (v "0.5.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "022qbj6m53mx27kfj9cizks528j61g9hlnrk5h8jdkpjps6lpsi2")))

(define-public crate-parsell-0.6.0 (c (n "parsell") (v "0.6.0") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1x9yjj3gk1f2mdzqi60akcb50qinibn3kk37ydz4zx0ly40d2jih")))

(define-public crate-parsell-0.6.1 (c (n "parsell") (v "0.6.1") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1i56i5f24lahngvfvigpm2h21090imgixivdhvvwzmi9cfb8dg22")))

(define-public crate-parsell-0.6.2 (c (n "parsell") (v "0.6.2") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "1knmh9gzxr55nk7bicjy5h7qs7dpqy9857b23r9549394rnav291")))

(define-public crate-parsell-0.6.3 (c (n "parsell") (v "0.6.3") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "01hg7snipk2098nw3mqyq64rqn76vgacrvsl8jb2md0hldql4r77")))

(define-public crate-parsell-0.6.4 (c (n "parsell") (v "0.6.4") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0ld7amsvw33hdx693425svg0x4j7bslf5raxlwk64g6sfsiz2k1r")))

(define-public crate-parsell-0.6.5 (c (n "parsell") (v "0.6.5") (d (list (d (n "skeptic") (r "^0.4.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.4.0") (d #t) (k 2)))) (h "0iadsnwk87d02yjmg6kx273hnkmp5bm78j70c1nbph54kkw0769w")))

