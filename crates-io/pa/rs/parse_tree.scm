(define-module (crates-io pa rs parse_tree) #:use-module (crates-io))

(define-public crate-parse_tree-0.0.1 (c (n "parse_tree") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1piic951hlmlimicl7rrg1nfwjrkz92fhmh7p7jha80acmrv2vmd")))

(define-public crate-parse_tree-0.0.2 (c (n "parse_tree") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0rk41ll8rn27wm107il1ag9hj8i6m35jsr8zsg9y0x174zipwldz")))

(define-public crate-parse_tree-0.0.3 (c (n "parse_tree") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "01j705gckir8zr65php776qqawi63sgskf3k0dkviic4grimwhva")))

(define-public crate-parse_tree-0.0.4 (c (n "parse_tree") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1pzyz8dkh8bjn3z76kin8i76bdc31zag2i4sla0cxy77kck8h94h")))

(define-public crate-parse_tree-0.0.5 (c (n "parse_tree") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0p162ry53fbpmj8y5v8q3s8sfn3fbpw7sw65h8nqq7ymgkv38617")))

(define-public crate-parse_tree-0.0.6 (c (n "parse_tree") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0x3d4j8cli52dnjgza2bjw0bhz7d6kk4ghzmychy6ci8v3plywg7")))

(define-public crate-parse_tree-0.0.7 (c (n "parse_tree") (v "0.0.7") (d (list (d (n "text_unit") (r "^0.1.0") (d #t) (k 0)))) (h "1p95ifz3vysgnfa37irr00nnybwr668bivjnc8axy67m3vs5fvf2")))

