(define-module (crates-io pa rs parsicle) #:use-module (crates-io))

(define-public crate-parsicle-0.1.0 (c (n "parsicle") (v "0.1.0") (d (list (d (n "article_scraper") (r "^2.1.0") (d #t) (k 0)) (d (n "comrak") (r "^0.23.0") (d #t) (k 0)) (d (n "html2md") (r "^0.2.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "rust-bert") (r "^0.22.0") (f (quote ("download-libtorch"))) (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "00wd54vc5zm7mjhl60h1kk01ml1mvaqyafizimb4w7ymniyq14n0")))

