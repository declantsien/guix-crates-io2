(define-module (crates-io pa rs parsable) #:use-module (crates-io))

(define-public crate-parsable-0.1.0 (c (n "parsable") (v "0.1.0") (d (list (d (n "parsable-macro") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wndh5r5l589z3q69sd14kp01f93mir66wcjf1wya2fy4sjisl13")))

(define-public crate-parsable-0.1.1 (c (n "parsable") (v "0.1.1") (d (list (d (n "parsable-macro") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pmax50fn1k6qrdmw90rh4d9qinnzvbdkx737kynyfp0klm9dhpc")))

(define-public crate-parsable-0.1.2 (c (n "parsable") (v "0.1.2") (d (list (d (n "parsable-macro") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04gg70qyrcap3ja51y13jxbbxhm5f2bkpcj088p87zf8k1giwggn")))

