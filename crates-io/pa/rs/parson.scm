(define-module (crates-io pa rs parson) #:use-module (crates-io))

(define-public crate-parson-1.0.0 (c (n "parson") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "11v46x10inpm4k621cfd16vp4kwbxh05iz2shwc7720w1bdgjl9m")))

(define-public crate-parson-1.0.1 (c (n "parson") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "0y8jdcqs5mwiw8fwlv95xkvax4hgwr3kmj3y6hqxj18ll0mss9n4")))

(define-public crate-parson-1.1.0 (c (n "parson") (v "1.1.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "11bz4ynp277wwnl8gy771604cfzrhsjmfdhq308pmip7822xmiq5")))

