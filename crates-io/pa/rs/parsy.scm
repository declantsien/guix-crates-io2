(define-module (crates-io pa rs parsy) #:use-module (crates-io))

(define-public crate-parsy-0.1.0 (c (n "parsy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nwgmwq27c1ma8pw9xa053n64902krqjjdm9xa9522710p0cq0lk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.0 (c (n "parsy") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07jhk7akcm7q34idyq9qkki7qnz68ll6b6llyb0k3hnaycbqyphc") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.1 (c (n "parsy") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03hfjx5vc8qnifwgfjj17gq1sxwjlr4n6vfm1shsgcz1v80p742w") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.2 (c (n "parsy") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15gl4dv58jwjs1xbzj0fipyyzv0qpha66mhgrds2000wja9ykx1f") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.3 (c (n "parsy") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dm6zmrn6rzg6x22z06m9l4wqs4k19i7l5ra7l37cnxp1n2ik5c0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.4 (c (n "parsy") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11v9k8wk3fkaldj32630nbbg5ahpiwgsdacj8dzjkxbhiccwsbyl") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.5 (c (n "parsy") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g1kabss4rf1ahpci1dmvlihjgn6cwni4dlvh4f0gckyz5wbfnki") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.6 (c (n "parsy") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vs48r73qhyqfbfiyd1cawmiaw2c31r88jgbmlcq15s4mx5csgs6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.7 (c (n "parsy") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08lkja6vlyb1w3cjq3sgqpqygqiwmy96d62cgf1mzpfwafgpd4ki") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.8 (c (n "parsy") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ri080a1kgvvrfnfvn6d1cqasbkz4nzyzwgip8yz447b0hz3hr1j") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.9 (c (n "parsy") (v "0.2.9") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a2d24vmfidan3pajiprmplgni65q01r3y4892ga794ngb343yhm") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.11 (c (n "parsy") (v "0.2.11") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17yc2w5qlmmmg6q40qn049fa43y8zifwq0lp5fqb7b47gy12l28s") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.12 (c (n "parsy") (v "0.2.12") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pzhy2ij8dd80s41nz8ch1cldgvw7j59gfq9fb07339saza3z3r6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.13 (c (n "parsy") (v "0.2.13") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1in5374ilqd7h86ks2lqip06cx7536zln4zk358kbqw7qcvgaqlv") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.14 (c (n "parsy") (v "0.2.14") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d166xwvr7lykvz2k7gdbc9b1n7k0rpsaal0hsvijvm2khsv79ld") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.15 (c (n "parsy") (v "0.2.15") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nvcb00p59jpdxpr1zx6vyiw9niy6gzsmybnwavsbfncgpv6haia") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.16 (c (n "parsy") (v "0.2.16") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c9smsfbwdc7z3207gmknfwaifi8qi4i56y8plfs92hb0jbw7fq6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.2.17 (c (n "parsy") (v "0.2.17") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i6i9hx8qdqaq71r8x76wcgl3fp22fy218x68b7xv28ir7mlc734") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.0 (c (n "parsy") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kczsw2gdx0na4h6nvddmv7clybza5pwffn5j9n1svbap9fl51s3") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.1 (c (n "parsy") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ska4mbb8ghqdgv837k43wh657jhd5wxpz114sd8r56qavwnqw39") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.2 (c (n "parsy") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j9jfx30200cimv7y40sfw1kshfi0k4idkx138dh12br23xggbgc") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.3 (c (n "parsy") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i6rqnzz0wlk8d1kya020kjqw16vcck3pgcgnvjizc18b8vj6cp9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.4 (c (n "parsy") (v "0.3.4") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xahn5gw45pnkald4j96ifddlx3afhvgbza6z48q0cid1iapybnc") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.5 (c (n "parsy") (v "0.3.5") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16lrgrl4vn5j9y01hhpq9asyv4avg3xraknd3ycqdy552kmzi2n9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.6 (c (n "parsy") (v "0.3.6") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p5phz7lhk2lm2kdg7vy3rcl9anw9yn9xvy6rdg1ihlg91qdiz9g") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.7 (c (n "parsy") (v "0.3.7") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15cj72hgs6bk9ifngqmc9d8766fnvxryqsvyg18zzzjg56jqadhn") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.8 (c (n "parsy") (v "0.3.8") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k4qvc5arsyw3zhyb12asjpynfx1sr3ivx28qzzicspr9vv4f2hf") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.9 (c (n "parsy") (v "0.3.9") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pjwj5d0cbm5hblrdb1hxzv7gfgzm26nvsmghsyj5q5h3q0gqcz8") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.10 (c (n "parsy") (v "0.3.10") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nkx8kvqplnzv1sb3mf82c9v2rlcwljmdyzr9j1v5gpkjkxkwwxx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.11 (c (n "parsy") (v "0.3.11") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dkwpqymb6slh8dpyp37hinb3pfwnfvmnnyz9yi56xa8km0pr0ns") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.12 (c (n "parsy") (v "0.3.12") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f899h9pqqr2flazabn9kfwvasl9zsb1191xc1bhnvwf1ab85lvq") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.13 (c (n "parsy") (v "0.3.13") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j8pkp7514hs3yaa0pzshjhh5hj7adbpnjrd94gb3s0k974zizwj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.14 (c (n "parsy") (v "0.3.14") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dx2iflggy67jx73xxdlxaji3cy560pfc4zdg5wpwx5n6m4fcp5f") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.15 (c (n "parsy") (v "0.3.15") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05209wvpp59kxplzxjpb0yw35r5bxir1vfn3cjzqdhr7i17m99ki") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.16 (c (n "parsy") (v "0.3.16") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d5lhcm9wv88azi2b3m9qds9wxnagly0khli654a80rd7vfg9xv2") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.17 (c (n "parsy") (v "0.3.17") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mp5q8lvgbkqpasr3jv86qrvff4lyi2yjqviav363slqgdshw3da") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.3.18 (c (n "parsy") (v "0.3.18") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hq5sgdj8vza4ykqply67ngb2wyzb162yqmh81cpp5nihb995yxb") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.0 (c (n "parsy") (v "0.4.0") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qg9bq1ysdkl03h0xxwy5lridp6j7j6hz3ws2rqhakzn6j2pxbwh") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.1 (c (n "parsy") (v "0.4.1") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sck7k95729yvp3zs17y15cwc9csg99c8s7q1w69q4mp9wf526kz") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.2 (c (n "parsy") (v "0.4.2") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03xcwmixwljrgzndi9cwi4j00cwf24a32p7gnvmrgnnczi0n2q0q") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.3 (c (n "parsy") (v "0.4.3") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "152f5sa6ymwlbjgnm1ha3ynbxp7672h4pxnzd7bk5yyds52pz2rg") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.4 (c (n "parsy") (v "0.4.4") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00r46fr0m72hb4qdahsvn7ibgnj8d88figpc2pfg66l8vywls7v6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.5 (c (n "parsy") (v "0.4.5") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hmv66l3c712s7ys04d4wmxxj58nn279k4fxb9y9r76dv72vqd91") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.6 (c (n "parsy") (v "0.4.6") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x5gvx92p4lrwi2bmpvd9pvx2qwn9rprwfpb0h73sy13abiisjp1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.4.7 (c (n "parsy") (v "0.4.7") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j0b2qb3c6szja0hzr5bg7kqw99zrx3ias37gv8gs6az12m1d4ii") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.0 (c (n "parsy") (v "0.5.0") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gqwl549bssayvafsqbgxnzs12a28inzy7p4bvkmq3zd2ry2zkp1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.1 (c (n "parsy") (v "0.5.1") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19fxzi1d9hjdc2k7r0h6zxmr5gcnxmxbilr6glxz4szngyxarr7l") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.2 (c (n "parsy") (v "0.5.2") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qr1amhxdj69s8pa9zs3p7frlm20gi79a4i3jlbv9qxrzrsib0li") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.3 (c (n "parsy") (v "0.5.3") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d9vbhc6sng21ry4m75m61cal0fa8anxbx74lvchplk2xdjy0add") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.4 (c (n "parsy") (v "0.5.4") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q904mpbsw0l13kx4kxpqs8dkfln8sg1jx2bn46p44rd342zgfw9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.5 (c (n "parsy") (v "0.5.5") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k4838szb1n37jmzpjxln98d6c0hrlbj9knxhx8arlxm8d63ifsq") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.5.6 (c (n "parsy") (v "0.5.6") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1y75bh81x4sq894r82yzg279qjpzw86y4azjfnc31807fkd5nng2") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.0 (c (n "parsy") (v "0.6.0") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "162lnghycsjbqb15m2sf4wq591ai4ya36dyyxhvcvqvnlwz7n2qk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.1 (c (n "parsy") (v "0.6.1") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "131dahi0mdb4a3jbq61pnj8z2h45qcvsbr5jzn8mi137wybz5i60") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.2 (c (n "parsy") (v "0.6.2") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qy8anq5fiyqr2c7vb16n40ymskabdqgknqdpkarq7pbx35n74pi") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.3 (c (n "parsy") (v "0.6.3") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1248119ha0jn5jisfcjizpzrpz0lf6kcf4c5qfg67m0ggk1j414i") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.4 (c (n "parsy") (v "0.6.4") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xmszlbxz5l7mc667sap4mzq6dankyanql17ql3sfgjy00v3vd6j") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.6.5 (c (n "parsy") (v "0.6.5") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b3nzw68096yp92s2767hxv49sisf4k00wb0yl1h85lbbhdpk6i3") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.7.0 (c (n "parsy") (v "0.7.0") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07n07203s9jg64h72abn4bikxxy8yi0kg4zjbbrphxr5b0krwm06") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-parsy-0.7.1 (c (n "parsy") (v "0.7.1") (d (list (d (n "perfect-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jm2r2w6h5945p795m2c5nin0ingj8xs4478xgba3bsaqpkp1aa7") (s 2) (e (quote (("serde" "dep:serde"))))))

