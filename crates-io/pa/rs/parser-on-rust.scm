(define-module (crates-io pa rs parser-on-rust) #:use-module (crates-io))

(define-public crate-parser-on-rust-0.1.0 (c (n "parser-on-rust") (v "0.1.0") (h "1xsm728hs6y2jwkp1w498kny1r19vwws7ywck403ggi6dfaw0ahz") (y #t)))

(define-public crate-parser-on-rust-0.1.1 (c (n "parser-on-rust") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "12zgnv94wqhz2h2a6yx8316bl65aqyywfnkgnwlvaa9w6aib569y") (y #t)))

(define-public crate-parser-on-rust-0.1.2 (c (n "parser-on-rust") (v "0.1.2") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0xn5hckfz59ywl7kprrnslg79n2zf40js0mxa1mdkw25763drfmx") (y #t)))

(define-public crate-parser-on-rust-0.1.3 (c (n "parser-on-rust") (v "0.1.3") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1n1mjixq4grny2qq6kp560da6m7j11j259q2iy4v9nxqpzavwlgd") (y #t)))

