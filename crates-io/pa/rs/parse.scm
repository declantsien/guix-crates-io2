(define-module (crates-io pa rs parse) #:use-module (crates-io))

(define-public crate-parse-0.1.0 (c (n "parse") (v "0.1.0") (h "1d23gns4kpghd1q10j893rbg61xrb2aagcrgz665rhx2gc5ngw61")))

(define-public crate-parse-0.1.1 (c (n "parse") (v "0.1.1") (h "0i0kqkhl92ln1qa3zqgfx1frfd2rrj94986918wwmwc3939g3q4f")))

(define-public crate-parse-0.1.2 (c (n "parse") (v "0.1.2") (h "0f8j2lm0di9pv3hf79cn9vj8d237ya01jwx53arx02jhkbdisda7")))

