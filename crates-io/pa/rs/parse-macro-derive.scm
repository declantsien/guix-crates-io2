(define-module (crates-io pa rs parse-macro-derive) #:use-module (crates-io))

(define-public crate-parse-macro-derive-0.1.0 (c (n "parse-macro-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "025g2dpaiad2f07dds6y72zn2ql16v9h3zsfw7fqr0dhr3a23hwm") (y #t)))

(define-public crate-parse-macro-derive-0.1.1 (c (n "parse-macro-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1j08b3w0xznnh75fikqwm8p25xj00hk3vjh928izz31v0fv1q214") (y #t)))

(define-public crate-parse-macro-derive-0.2.0 (c (n "parse-macro-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1djxzpxf1rvc4b76xp1c6cw1zwyg1y7n7d9wkrp48wvf55hkmm5p") (y #t)))

(define-public crate-parse-macro-derive-0.2.1 (c (n "parse-macro-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "05kgbjvprgxbmba55gs2y1x7yiypa8z4zm08xycg1698cvrcbjs5") (y #t)))

