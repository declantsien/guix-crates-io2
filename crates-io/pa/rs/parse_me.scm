(define-module (crates-io pa rs parse_me) #:use-module (crates-io))

(define-public crate-parse_me-0.4.0 (c (n "parse_me") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "19css2sxmsidmgr6igyihxfllx892675j88y2p6xr09l5fyyaqkm")))

(define-public crate-parse_me-0.4.1 (c (n "parse_me") (v "0.4.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1flqss2w5i1126bkqxsw6iklg8qyl7d10k2bpv4ywmhxs5pd219m")))

(define-public crate-parse_me-0.4.2 (c (n "parse_me") (v "0.4.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1xpq6drssh06ia0vckrlzyq2ihz8iw3x5qimrl287jjp6ki58z79")))

(define-public crate-parse_me-0.4.3 (c (n "parse_me") (v "0.4.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "09g35729n8lgix0ycbsvswbzzmim1mvvi1hplshyvgpx34vqb8j1")))

(define-public crate-parse_me-0.4.4 (c (n "parse_me") (v "0.4.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "04v61srdw1k0grvgdyqkvxyivwn9d6qlganwkx359jwjmziaq38g")))

(define-public crate-parse_me-0.4.5 (c (n "parse_me") (v "0.4.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1yndnw3lrv5h8wprlykjnnl0zskzlmscw702gys6ybfn76863q1z")))

(define-public crate-parse_me-0.4.6 (c (n "parse_me") (v "0.4.6") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "19lmxqbdnbdlhxxgsdyz4nsh6j7rpjbar4rvsphb9hf6d4cmm3l3")))

(define-public crate-parse_me-0.5.0 (c (n "parse_me") (v "0.5.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ls34ixqf30wnxzk7b4mrg4f2q4wjdqhc96yabfsq18ksn3kdx72")))

(define-public crate-parse_me-0.5.1 (c (n "parse_me") (v "0.5.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0g6d6n6mgviaw1wz6qc5bvyp0rdllsw5ficqvlj1jqb9kq4skgj1")))

(define-public crate-parse_me-0.5.2 (c (n "parse_me") (v "0.5.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1wclmq40v6b3lfwvk1lq04q59ypwvnrphmrgiy5ilpq3c4sjd297")))

(define-public crate-parse_me-0.5.3 (c (n "parse_me") (v "0.5.3") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0fj8rfg8f32znn3f02z77p7zapsjxrv22y5w44b4nkidzq8j7prf")))

(define-public crate-parse_me-0.6.0 (c (n "parse_me") (v "0.6.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0l2sr8lmn2lklcyn5ldn2jk2dypk4bci48i38ysfas9rkd6jpgbc")))

