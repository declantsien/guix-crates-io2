(define-module (crates-io pa rs parseal) #:use-module (crates-io))

(define-public crate-parseal-0.1.0 (c (n "parseal") (v "0.1.0") (d (list (d (n "parse-macro-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "01jcw7373acrsl36c299f95p33g7qf1rqvxf8jdw3qxzikbf8fp6")))

(define-public crate-parseal-0.1.1 (c (n "parseal") (v "0.1.1") (d (list (d (n "parse-macro-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1a3ccg8md509h8m6jmcc1xiafkv9pi6bl1xnfvl1i6d3yjs748la") (s 2) (e (quote (("derive" "dep:parse-macro-derive"))))))

(define-public crate-parseal-0.2.0 (c (n "parseal") (v "0.2.0") (d (list (d (n "parse-macro-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "168kirad6sk4m2pnl1f2jmgn1ykh6hfg47gm6l83gzrc838bam6i") (s 2) (e (quote (("derive" "dep:parse-macro-derive"))))))

(define-public crate-parseal-0.2.1 (c (n "parseal") (v "0.2.1") (d (list (d (n "parseal-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "03m418wjq36wgk04wf29vb0wp0cp141wgkrm65wx3mllbyallllv") (s 2) (e (quote (("derive" "dep:parseal-derive"))))))

(define-public crate-parseal-0.2.2 (c (n "parseal") (v "0.2.2") (d (list (d (n "parseal-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0jggngrjk4096rmpg7fg3mhnb8aaafvf5pwwynkiz0m7wxiccqgn") (s 2) (e (quote (("derive" "dep:parseal-derive"))))))

(define-public crate-parseal-0.2.3 (c (n "parseal") (v "0.2.3") (d (list (d (n "parseal-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0mw0a76rw2xx916w9ijjzypjan605rjymmwk04db4p83bz8dhyd6") (f (quote (("data-formats" "derive")))) (s 2) (e (quote (("derive" "dep:parseal-derive"))))))

(define-public crate-parseal-0.2.4 (c (n "parseal") (v "0.2.4") (d (list (d (n "parseal-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0c0jrwvfa6b9ibdh9xcbg48cn6lxzn58r1l60b7n6im3dk14nv7p") (f (quote (("language-formats" "derive") ("data-formats" "derive")))) (s 2) (e (quote (("derive" "dep:parseal-derive"))))))

