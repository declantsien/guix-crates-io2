(define-module (crates-io pa rs parse_proc_maps) #:use-module (crates-io))

(define-public crate-parse_proc_maps-0.1.0 (c (n "parse_proc_maps") (v "0.1.0") (h "1qbj583qg8m9vxh6dwsavaxfnx8fvzx6mwalmfvzwqs24q069l1c")))

(define-public crate-parse_proc_maps-0.1.1 (c (n "parse_proc_maps") (v "0.1.1") (h "0nrc3wdc018rwydxm32w29vcgs7a2ar0cfg2kikbgw6p5skvcklf")))

(define-public crate-parse_proc_maps-0.1.2 (c (n "parse_proc_maps") (v "0.1.2") (h "18fddxyrs1kzakahsfsvfs41h4pxzv5mmyl91azjc5zc7psrjgsr")))

(define-public crate-parse_proc_maps-0.1.3 (c (n "parse_proc_maps") (v "0.1.3") (h "0zfx38wd0by969l270x1r9abk9dmdgkgama9v1brk9hvps48pxif")))

(define-public crate-parse_proc_maps-0.1.4 (c (n "parse_proc_maps") (v "0.1.4") (h "088inljyy6g7y60fmra90m417py2rimahsh1gxlj0mjdcr2xhznq")))

(define-public crate-parse_proc_maps-0.1.8 (c (n "parse_proc_maps") (v "0.1.8") (h "18p0krf2arh9clih8w7lhdj8nnc60c0ibjj1f4xj2ra9gfyzz4f7")))

