(define-module (crates-io pa rs parsers) #:use-module (crates-io))

(define-public crate-parsers-0.3.1 (c (n "parsers") (v "0.3.1") (h "1z9k0x1hgvqma4parwid009n6m1qyq4rs50f33gppsgf9lxrgjz7") (y #t)))

(define-public crate-parsers-0.3.2 (c (n "parsers") (v "0.3.2") (h "17hwmcks9dwckzrpp65sksphd8v8s0lmjcr2prfiykwc51fxfix2") (y #t)))

(define-public crate-parsers-0.3.3 (c (n "parsers") (v "0.3.3") (h "19b4xsllgs6ajy54d4rlbsyi552pmwy1pknidjhjma1kb21c4zwk") (y #t)))

(define-public crate-parsers-0.3.4 (c (n "parsers") (v "0.3.4") (h "0g17hfcrv9y2p2hni38pbcdrflsvd369kd6cv0bg4s768v4fnjfs") (y #t)))

