(define-module (crates-io pa rs parst_derive) #:use-module (crates-io))

(define-public crate-parst_derive-0.1.0 (c (n "parst_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "0nxwz94sh7b84psn2x1qhbx70lic8mhyjvlbcxq1zwb3wp3bz1hm")))

(define-public crate-parst_derive-0.1.1 (c (n "parst_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "0559y27dj62fn3ss35571cchy08g9jkmdhvznq4lbggz62lyxlfx")))

(define-public crate-parst_derive-0.1.2 (c (n "parst_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "0y1xdln113bj72lyygbayv2bv19m4v5vzb9hvwbxg1w0x1cdf5yf")))

(define-public crate-parst_derive-0.1.3 (c (n "parst_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g4mb1wcyq5i5v76fjhz6pdvqpdrpjk67h1yqh0ry2xqzr8ar4cg")))

(define-public crate-parst_derive-0.1.6 (c (n "parst_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1casqds8h8fxn2dsyq4vgr0biy9c2i8q49gwri424xblzkqkvwqj")))

(define-public crate-parst_derive-0.1.7 (c (n "parst_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zx0b3xhkn3ln4v75w12318aq2sfgj9m13m9mwhlkam89jvnql7m")))

(define-public crate-parst_derive-0.1.8 (c (n "parst_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dbfrhy2vdfyzyybqlnjvjmlg8l9f0h5fgc0gpxrlb8aa56clqfk")))

(define-public crate-parst_derive-0.1.9 (c (n "parst_derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cv1ng19if9aysl5flyw9hlnmmhkzx5wd9q8d2w0fzqs9yr1jmzk")))

(define-public crate-parst_derive-0.1.10 (c (n "parst_derive") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13myn8lxz8jkwmlxiln0gsl2piz1vv5xrq68fxx35rf0brvqllvf")))

(define-public crate-parst_derive-0.1.11 (c (n "parst_derive") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11rilcmvn7vchps9x4sgwybdghzivv9lpiwq1hjpir7lcpkwmhlv")))

(define-public crate-parst_derive-0.1.12 (c (n "parst_derive") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pk9k9w3kyf2690jfy0ddbnzdsa69chwzhsss696n4nbqyqp5q3m")))

(define-public crate-parst_derive-0.1.13 (c (n "parst_derive") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ndk82157pnh0dxgbpqawggp3b0kc60ibiycscil61ba06k2hlgr")))

(define-public crate-parst_derive-0.1.14 (c (n "parst_derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "142qvjrd63azkkbr6lrj9k79j8czhap33vy08lx91cr0jdf78niw")))

(define-public crate-parst_derive-0.1.15 (c (n "parst_derive") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y9caa96frmzd6q2lg7cbi2y6lkrnqsc1ich15m3kzjy91ybzwwl")))

(define-public crate-parst_derive-0.1.16 (c (n "parst_derive") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12k3fs7pjpdalsnyr9pkspjcslznr1xy8xiqqhdmadm774k0skh2")))

(define-public crate-parst_derive-0.1.17 (c (n "parst_derive") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j07xaz486anf2zrrrlqbhki230qplp6qz53x7l7ygp4c4a5nps9")))

(define-public crate-parst_derive-0.2.0 (c (n "parst_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l8cb9k0rb72avrjifcd9aarkmh67zbz4l0c4r79qzhf7a0sn5h5")))

