(define-module (crates-io pa rs parser-c-macro) #:use-module (crates-io))

(define-public crate-parser-c-macro-0.1.0 (c (n "parser-c-macro") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (f (quote ("full"))) (d #t) (k 0)))) (h "025b6ayp0yihz1cci6916c5j4fgiwc6g6d3aihm1flp4zjcv61a3")))

(define-public crate-parser-c-macro-0.3.0 (c (n "parser-c-macro") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0agf0k4lz2fgd0kw7m2madnq05z5jfv34hsgd60q8ad07kixg4nf")))

