(define-module (crates-io pa rs parsercher) #:use-module (crates-io))

(define-public crate-parsercher-1.0.0 (c (n "parsercher") (v "1.0.0") (h "12bllvcic5b2fii057fyi3l9fz1dxf2vnfdld130x9cjlpix1a17")))

(define-public crate-parsercher-2.0.0 (c (n "parsercher") (v "2.0.0") (h "1cdb355b93cpks57dgcvnh3p7l76xmzn8ffnqag7vx8bs6a47l4x")))

(define-public crate-parsercher-2.1.0 (c (n "parsercher") (v "2.1.0") (h "049dwy0x58xysa6pbqprlqhp9z1yzzxn9ggpsyla4acx8dkc60vy")))

(define-public crate-parsercher-3.0.0 (c (n "parsercher") (v "3.0.0") (h "12z7xyw880iq5jgk5db0l9qcw9sq3s5qqqdzdrcpnb2wdvcrxiva")))

(define-public crate-parsercher-3.1.0 (c (n "parsercher") (v "3.1.0") (h "14vavkrnkrpdykqcz2jgg3acbdf9klshciwvdvzqn2mb3dh9kqz1")))

(define-public crate-parsercher-3.1.1 (c (n "parsercher") (v "3.1.1") (h "1rgxj88fsbzl7z2mhk046r5gpnccp0gfrll3pfpxvp304b3z5wg4")))

(define-public crate-parsercher-3.1.2 (c (n "parsercher") (v "3.1.2") (h "0wp40cak2p01529mdfwild7mklvc4gh4x3xv9n1b82dzdd1jqnrn")))

(define-public crate-parsercher-3.1.3 (c (n "parsercher") (v "3.1.3") (h "11svvawj2j8rxgs7aaqjmsjp9qa114gaphmm2l1ikagy0f37mphl")))

(define-public crate-parsercher-3.1.4 (c (n "parsercher") (v "3.1.4") (h "124svdpcl1wb8gsbi4l5zi70ggpxkrcs26nmdwkxhrmkbh1cbdw6")))

(define-public crate-parsercher-3.1.5 (c (n "parsercher") (v "3.1.5") (h "0a71p2xyyqln735hw054jbywvpklibivr9dv6gl3i5nsc2apmaz9")))

(define-public crate-parsercher-3.1.6 (c (n "parsercher") (v "3.1.6") (h "0j68y714xahipszyg4hm7pxr7l2y88xkswb4ss2a4wm8yan2prr0")))

