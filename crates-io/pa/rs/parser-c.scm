(define-module (crates-io pa rs parser-c) #:use-module (crates-io))

(define-public crate-parser-c-0.0.1 (c (n "parser-c") (v "0.0.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "0v76v3nsvilpbpcgx6bj1knpinbnjgxj1b9iwq86ki4q1x6hnk4s")))

(define-public crate-parser-c-0.2.0 (c (n "parser-c") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "parser-c-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0wpcqwyb4c7b2msdz07ziikilczkjvkyffjybfbjdknw2dva3f2i")))

(define-public crate-parser-c-0.2.1 (c (n "parser-c") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "parser-c-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1638bbxi81iyxpwnjjnqgpg690g20m4dp547l3xsivd64qywn605")))

(define-public crate-parser-c-0.3.0 (c (n "parser-c") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "either") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "parser-c-macro") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 2)))) (h "1xdpcb078n33l1914kdw7390bpyb2n6386jq9psslcdcrsf5fhi9")))

