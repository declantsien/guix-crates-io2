(define-module (crates-io pa rs parser-combinators) #:use-module (crates-io))

(define-public crate-parser-combinators-0.0.1 (c (n "parser-combinators") (v "0.0.1") (h "1f8vzslp6axkmpwgjgmz5362ash75ya85in5vp4lqwyhak7j5d4b")))

(define-public crate-parser-combinators-0.0.2 (c (n "parser-combinators") (v "0.0.2") (h "0z71wq7a9y5r6pn5b5ifyhny11hslyrmwv6xapnbyfpygbh5mky3")))

(define-public crate-parser-combinators-0.1.0 (c (n "parser-combinators") (v "0.1.0") (h "1mksrla9qcmf37r9imqml8vy9dwsdic96hrs4rx7p2sh38zv140j")))

(define-public crate-parser-combinators-0.2.0 (c (n "parser-combinators") (v "0.2.0") (h "1pcv8i5fnfrrwx3x4bjsqgkpjj1g09fcv6bigb7lkf8i4cc5121d")))

(define-public crate-parser-combinators-0.2.1 (c (n "parser-combinators") (v "0.2.1") (h "0bg3y1grvfbbmh5qxb4dx008qk7f32515cxb0g115v2ni45v7lll")))

(define-public crate-parser-combinators-0.2.2 (c (n "parser-combinators") (v "0.2.2") (h "1v8j9ixkiiqxanb6cjyzfz7i11zb1r534gwg85i4mva23f3dh0zg")))

(define-public crate-parser-combinators-0.2.3 (c (n "parser-combinators") (v "0.2.3") (h "1krdfhgzyamrdmfscm3q6lzyd3kb4fdip9n4f0j2mmyafrgifvzs")))

(define-public crate-parser-combinators-0.2.4 (c (n "parser-combinators") (v "0.2.4") (h "0s1zdd8pr11bvnrpa0bnr23rivhxx6jwz4070grfpvnffiwdh550")))

(define-public crate-parser-combinators-0.2.5 (c (n "parser-combinators") (v "0.2.5") (h "1r7gbd9j7137v3rml65m5klkf29jvbgpclaa7h186qfrjkn68c9k")))

(define-public crate-parser-combinators-0.2.6 (c (n "parser-combinators") (v "0.2.6") (h "00lzc487j8pysz6zi2xm9plhjybv6qbq29vnwalgzzcwz9bbbl47")))

(define-public crate-parser-combinators-0.3.0 (c (n "parser-combinators") (v "0.3.0") (h "0njy59snhzwyxlgxviibsl38bgp0ygmdv997n3w624ni8flmgx5x")))

(define-public crate-parser-combinators-0.3.1 (c (n "parser-combinators") (v "0.3.1") (h "03xbqiayr2bazwlbflnv48g2vmf9fx0q817741pih9jis1p4sqs1")))

(define-public crate-parser-combinators-0.3.2 (c (n "parser-combinators") (v "0.3.2") (h "1vsrfc83fh2b250hpwzb6sn0dxyvamrfhnwdkag91ifgsbp0zn9x")))

(define-public crate-parser-combinators-0.4.0 (c (n "parser-combinators") (v "0.4.0") (h "0mk3i4ynfr07bgdm1rba5banrvpsy18npxy6x36nsvf85gkvkkwr")))

(define-public crate-parser-combinators-0.5.0 (c (n "parser-combinators") (v "0.5.0") (h "1jx5rxnxb82aqgbwkj684kai9a77mpjkwpsj0c3557jphapg6nyd")))

(define-public crate-parser-combinators-0.5.1 (c (n "parser-combinators") (v "0.5.1") (h "04p9g1smajnarwm4svks1dsda7mfzgaj3sb2d7aq52gy93ic7ncp")))

(define-public crate-parser-combinators-0.6.0 (c (n "parser-combinators") (v "0.6.0") (h "1jvyh2z1gnjwhww4nv1qd4w0als7s2rinr0zhh8qrimawxd9ysji")))

(define-public crate-parser-combinators-0.7.0 (c (n "parser-combinators") (v "0.7.0") (h "18azffk9lrkyzapz6shv0g031i7p9m4191c7vnhpxkmfbi60zv7v")))

(define-public crate-parser-combinators-0.7.1 (c (n "parser-combinators") (v "0.7.1") (h "0q82ylhqk771z5zhj26sixin098gwgsc690lll9zh8hm4vn2kfvq")))

