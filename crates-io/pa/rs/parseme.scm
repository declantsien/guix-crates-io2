(define-module (crates-io pa rs parseme) #:use-module (crates-io))

(define-public crate-parseme-0.1.0-alpha (c (n "parseme") (v "0.1.0-alpha") (d (list (d (n "parseme-xid") (r "^0.1.0-alpha") (o #t) (d #t) (k 0)))) (h "1c3csp1wkpmfalwza67d4c2y452mppbaaz72lyf0ba6z7ckym55a") (f (quote (("xid" "parseme-xid") ("default" "xid"))))))

(define-public crate-parseme-0.2.0-alpha (c (n "parseme") (v "0.2.0-alpha") (d (list (d (n "parseme-xid") (r "^0.2.0-alpha") (o #t) (d #t) (k 0)))) (h "0lsbhv41y2xlsi40wxc9gp6dfbs7b913xrycwpmry94ahcdvgzbq") (f (quote (("xid" "parseme-xid") ("default" "alloc" "xid") ("alloc"))))))

