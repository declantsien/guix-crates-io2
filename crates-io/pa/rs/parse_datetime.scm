(define-module (crates-io pa rs parse_datetime) #:use-module (crates-io))

(define-public crate-parse_datetime-0.4.0 (c (n "parse_datetime") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1psphzy31ysa20s7l5khfvzjwx4iphhs71w60nc9lyknwznymk7y")))

(define-public crate-parse_datetime-0.5.0 (c (n "parse_datetime") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "18c6nl8c54ahra3gjlyknqwmxzmdb1inc7la0470hh9qn4jlxgrv")))

(define-public crate-parse_datetime-0.6.0 (c (n "parse_datetime") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1l4n9gsflf1fksfrvyhqyf8bdwxrk0s705l7x86g4jnxwds08wm8")))

