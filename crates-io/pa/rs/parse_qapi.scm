(define-module (crates-io pa rs parse_qapi) #:use-module (crates-io))

(define-public crate-parse_qapi-0.1.0 (c (n "parse_qapi") (v "0.1.0") (d (list (d (n "nom") (r "~1.2") (d #t) (k 0)))) (h "1ldf22zbgah9qg5n60v6xfhfzi2fcx3ynks5vxdicm7s67wgsbmd")))

(define-public crate-parse_qapi-0.1.1 (c (n "parse_qapi") (v "0.1.1") (d (list (d (n "json") (r "~0.8") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)))) (h "1ig191dbyrm3399g21rkcrcimjd21ll90r87adkid5fwsy027ic3")))

(define-public crate-parse_qapi-0.1.2 (c (n "parse_qapi") (v "0.1.2") (d (list (d (n "json") (r "~0.8") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)))) (h "0mr28jlbnpwndmwwrnll1ahdyjnq5d5sdfsl7c9rabmcvqfwvr3d")))

