(define-module (crates-io pa rs parse_url) #:use-module (crates-io))

(define-public crate-parse_url-0.1.0 (c (n "parse_url") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "01nvzrhynkljpzsbhay41yha7hmjrxg2ygxsfsjr5s630xbcmy0d")))

