(define-module (crates-io pa rs parslers-macro) #:use-module (crates-io))

(define-public crate-parslers-macro-0.1.0 (c (n "parslers-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 1)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 1)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits" "full"))) (d #t) (k 1)))) (h "0ivvfnkdbdsqd1rzhhx1xp6yzd7n4bylszisb8frjv6yn30j6ixj") (r "1.70.0")))

