(define-module (crates-io pa rs parsnip) #:use-module (crates-io))

(define-public crate-parsnip-0.1.0 (c (n "parsnip") (v "0.1.0") (h "1v89kzm5kx6smr51kpwjzcs92w13zh2674507lhvz9xx1vgs7ng4")))

(define-public crate-parsnip-0.1.1 (c (n "parsnip") (v "0.1.1") (h "0f618hjf4mwqmsi2h1cdvgp2y8z8k4x9gkbnjjdddv4k8d00yw1k")))

(define-public crate-parsnip-0.1.2 (c (n "parsnip") (v "0.1.2") (h "0cglk9p4vzpf1jgl9zm84zvi3bgm98gg2lxjyw9lzg2wmdiy6y69")))

(define-public crate-parsnip-0.1.3 (c (n "parsnip") (v "0.1.3") (h "04pzpfs7znlga08xb0gk6zbh536xgn9sqs5hdqbjainaym8mx5z4")))

(define-public crate-parsnip-0.2.0 (c (n "parsnip") (v "0.2.0") (h "01h77r4gqqxcq6q9ikay0m9dcfznsq6pr328gaqizfrgr11s2asn")))

(define-public crate-parsnip-0.2.1 (c (n "parsnip") (v "0.2.1") (h "0pinh4rgrmsl864n18ab9zr3i884fgms3m5wy0y9x22l2qyi9gik")))

(define-public crate-parsnip-0.2.2 (c (n "parsnip") (v "0.2.2") (h "0y1mn8vn1wqmbh6k757hgk0wml63n3pnidiv9758mkc75xf84kwy")))

(define-public crate-parsnip-0.3.0 (c (n "parsnip") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)))) (h "1rjr5v6dyg5lmkkzs0wsn0ipkqwdhwbg4gixn2hjzmh598xa8cbb")))

