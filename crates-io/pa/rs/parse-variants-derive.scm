(define-module (crates-io pa rs parse-variants-derive) #:use-module (crates-io))

(define-public crate-parse-variants-derive-0.1.0 (c (n "parse-variants-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1gaskg9ybv3qksi4jb6405ggigz15afsnv7lm92gjnbccs4n1skc")))

(define-public crate-parse-variants-derive-1.0.0 (c (n "parse-variants-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1xjgcfcypz2cxkqcb4kw8pjyndi0d4aaxir4vqfyyif68dn75pvh")))

