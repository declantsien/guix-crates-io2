(define-module (crates-io pa rs parse-generics-shim) #:use-module (crates-io))

(define-public crate-parse-generics-shim-0.1.0 (c (n "parse-generics-shim") (v "0.1.0") (d (list (d (n "parse-generics-poc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1rmj490gcphmx7v82j3xvdd3x93fvwnpk0h9ci11pp1wzbdgln4y") (f (quote (("use-parse-generics-poc" "parse-generics-poc"))))))

(define-public crate-parse-generics-shim-0.1.1 (c (n "parse-generics-shim") (v "0.1.1") (d (list (d (n "parse-generics-poc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 2)))) (h "0cgcg3zszmsa40xzdkkapf1rv0vdjjq415vfbibc3pw3awjg9bf4") (f (quote (("use-parse-generics-poc" "parse-generics-poc"))))))

