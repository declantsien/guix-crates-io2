(define-module (crates-io pa rs parsley-rs-hack) #:use-module (crates-io))

(define-public crate-parsley-rs-hack-0.1.0 (c (n "parsley-rs-hack") (v "0.1.0") (d (list (d (n "filepath") (r "^0.1.2") (d #t) (t "cfg(any(target_os = \"macos\", windows, target_os = \"linux\"))") (k 0)) (d (n "url") (r "^2.2.2") (d #t) (t "cfg(any(target_os = \"macos\", windows, target_os = \"linux\"))") (k 0)))) (h "1xi75fzb42xnrl1kkzbyxg6z01inpb4d9bda4frg8fpn6bkbzj52")))

