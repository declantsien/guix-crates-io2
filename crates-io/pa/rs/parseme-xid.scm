(define-module (crates-io pa rs parseme-xid) #:use-module (crates-io))

(define-public crate-parseme-xid-0.1.0-alpha (c (n "parseme-xid") (v "0.1.0-alpha") (d (list (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "037qhjwwxq3160xxgg2lmc2w5hv090igajrlk6mvbbzfbcd9mnzf")))

(define-public crate-parseme-xid-0.2.0-alpha (c (n "parseme-xid") (v "0.2.0-alpha") (d (list (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "0nynjz2qnc1amr5319lymx7cisliqzs40h7z2l3pqcgvgv4cky01")))

