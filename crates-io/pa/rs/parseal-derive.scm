(define-module (crates-io pa rs parseal-derive) #:use-module (crates-io))

(define-public crate-parseal-derive-0.2.0 (c (n "parseal-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1xnr4sd7ixr9dn8m342wz3kla2kx3mb6dgby9lnzi9am4y14pizb")))

(define-public crate-parseal-derive-0.2.1 (c (n "parseal-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "0blvjg28289rlhvx6l95p841ymzj2nsc659xkbvspcm6jsrnyyd0")))

(define-public crate-parseal-derive-0.2.2 (c (n "parseal-derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1s24pnzgdp3x97bcgacnrqgqkvalv9kp6pgxj7xza0ki7lbyjasa")))

(define-public crate-parseal-derive-0.2.3 (c (n "parseal-derive") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1mcl6yn70w8699jp1qsh76jsml7cafqswbpi7qbhv10di4lry78l")))

