(define-module (crates-io pa rs parser-pda) #:use-module (crates-io))

(define-public crate-parser-pda-0.1.1 (c (n "parser-pda") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0w40qlfxp2w8zagw2fr3drsbjkywgsl9kgrn9ynaf67gq2fn1ana") (y #t)))

(define-public crate-parser-pda-0.1.2 (c (n "parser-pda") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "08c5lchqq5cg01bbfsac0pgi4iklc6wv8jahzxxi4k77kifwklh3")))

(define-public crate-parser-pda-0.1.3 (c (n "parser-pda") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "17yidkdh0c06cd6j58yq8r5gh2mzndk4l473g7m3b69wfd8vdizc")))

(define-public crate-parser-pda-0.1.4 (c (n "parser-pda") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0gllziqdd2zq5vmqy0lyppx788p3blp9pgnci5ljdga1zp0jf83d")))

(define-public crate-parser-pda-0.1.5 (c (n "parser-pda") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1pgsdg602c74sj6gxx86dg4b95zwz8mlif88bv29pafidmqdnw0n")))

(define-public crate-parser-pda-0.1.6 (c (n "parser-pda") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0skik5yi0qmypa7zc3y34jlqgjkl42pdlvc4lwsvdh5adph9ki1q")))

