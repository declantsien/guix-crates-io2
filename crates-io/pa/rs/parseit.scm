(define-module (crates-io pa rs parseit) #:use-module (crates-io))

(define-public crate-parseit-0.1.0 (c (n "parseit") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "04znc0b7f61zj2llin780d3iprrvv09aag105i2jqkc735jk5cc8") (f (quote (("gzip" "flate2")))) (r "1.56.1")))

(define-public crate-parseit-0.1.1 (c (n "parseit") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0z59wq9j6ihra5fp779s940rg8hzq44dycxqby23zm2mw5w4mgpg") (f (quote (("gzip" "flate2")))) (r "1.56.1")))

(define-public crate-parseit-0.1.2 (c (n "parseit") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pr0zzy0d2bq3211ms39hafvyj89mkflhp6c5klhq5hknqs3x3vd") (f (quote (("gzip" "flate2")))) (r "1.63.0")))

