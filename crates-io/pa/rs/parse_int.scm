(define-module (crates-io pa rs parse_int) #:use-module (crates-io))

(define-public crate-parse_int-0.1.0 (c (n "parse_int") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1m5zrmr61pzmr6kf835fpcyd5qda2xn7092yy2cimsg73b6i2bf5")))

(define-public crate-parse_int-0.1.1 (c (n "parse_int") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "00r4qh6bvyrsc15pp9z8zcv9g6lyswrv5yzxwk79s72nakbxgcg7")))

(define-public crate-parse_int-0.2.0 (c (n "parse_int") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jyyzqfpi5lf0xskjmqm4vxzanbgfpx0kfxnv4ng0lp873d0ic5c")))

(define-public crate-parse_int-0.2.1 (c (n "parse_int") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "05wsl0plb2qp759jxqc0n056ifjvgk68qhpsanm96x61r03hmv39")))

(define-public crate-parse_int-0.3.0 (c (n "parse_int") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m0drnq4n9ddk6s5zk2gps0xfq5fff0lyg35ha06sxlwd0gbf7gq") (f (quote (("implicit-octal") ("default"))))))

(define-public crate-parse_int-0.4.0 (c (n "parse_int") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j6cs1ijnh4qbwwf7nz6w2xrays4i3x07lys1nqn62cgq754inw2") (f (quote (("implicit-octal") ("default"))))))

(define-public crate-parse_int-0.5.0 (c (n "parse_int") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "032zf8ygr9rg054x7rzjia188rm6vb2a724amlxhmykwaw06by8m") (f (quote (("implicit-octal") ("default"))))))

(define-public crate-parse_int-0.6.0 (c (n "parse_int") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0niwamixgrpzb1k5l035aa40450dnrx69rvvzyy0hb3aj5wmns9d") (f (quote (("implicit-octal") ("default"))))))

