(define-module (crates-io pa rs parse-hyperlinks) #:use-module (crates-io))

(define-public crate-parse-hyperlinks-0.8.0 (c (n "parse-hyperlinks") (v "0.8.0") (d (list (d (n "nom") (r ">=6.0.1, <7.0.0") (d #t) (k 0)))) (h "0byjhhyhbx5gvfdx3qhg5mzjgdlb696r2ya7zf2hlxpz7hjs7h16") (y #t)))

(define-public crate-parse-hyperlinks-0.8.1 (c (n "parse-hyperlinks") (v "0.8.1") (d (list (d (n "nom") (r ">=6.0.1, <7.0.0") (d #t) (k 0)))) (h "0cslix1s05ppcwc3jw20znaghskp2lh8sy6hyb66rfmasw33l3m7")))

(define-public crate-parse-hyperlinks-0.9.0 (c (n "parse-hyperlinks") (v "0.9.0") (d (list (d (n "nom") (r ">=6.0.1, <7.0.0") (d #t) (k 0)))) (h "0zbrqgp0w1z23dacc3q06jj9m2bz40754y28nmf83wn1xsklmqyf")))

(define-public crate-parse-hyperlinks-0.9.1 (c (n "parse-hyperlinks") (v "0.9.1") (d (list (d (n "nom") (r ">=6.0.1, <7.0.0") (d #t) (k 0)))) (h "1r6d47c47ryfi90pln6mrpanasad3w0554738q6pz9925acmg3wz")))

(define-public crate-parse-hyperlinks-0.9.2 (c (n "parse-hyperlinks") (v "0.9.2") (d (list (d (n "nom") (r ">=6.0.1, <7.0.0") (d #t) (k 0)))) (h "0gqjnyw8afr74zx04jwmfkbnz8pchgh0gkdqlj09rc403nspiqzj")))

(define-public crate-parse-hyperlinks-0.9.3 (c (n "parse-hyperlinks") (v "0.9.3") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "1bjhpxi7v6gv9va14z5dvzc8gi4kirjlpy9axp8yf3nwjmdk0sqq")))

(define-public crate-parse-hyperlinks-0.9.4 (c (n "parse-hyperlinks") (v "0.9.4") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "17aj3brybfvg49j6hkicmfvgpnqx4r777l1n11z6d1d55dlxrsfa")))

(define-public crate-parse-hyperlinks-0.9.5 (c (n "parse-hyperlinks") (v "0.9.5") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "0c8qwqffdfkdpgj719a5ykiz3jsz5gd4gkl8qy66lb43nspksmj5")))

(define-public crate-parse-hyperlinks-0.10.0 (c (n "parse-hyperlinks") (v "0.10.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "140wqlnvhyvm79qi1lbgaj0pz30j0xn9x9gv12cl0ga2ba1ib5k2")))

(define-public crate-parse-hyperlinks-0.11.0 (c (n "parse-hyperlinks") (v "0.11.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "0wfv6jjlfb7jasfxcbzgjljs79pq10iq1lyy1g2k354gww8pkxjv")))

(define-public crate-parse-hyperlinks-0.12.0 (c (n "parse-hyperlinks") (v "0.12.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0nrklx999jwahb7qk3ighl71kwk8dzs0c3b64g3d7z6gn4zbd6rq")))

(define-public crate-parse-hyperlinks-0.12.1 (c (n "parse-hyperlinks") (v "0.12.1") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1g63kndhnb1ddwjxvkqaj9hzz9cl5533mk77pzhv165nv2r8y5f9")))

(define-public crate-parse-hyperlinks-0.12.2 (c (n "parse-hyperlinks") (v "0.12.2") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1hl1nr7kq6hzyrj0wn48ad7rc6s73vsc57wmd010ygn6xw4akw95")))

(define-public crate-parse-hyperlinks-0.13.0 (c (n "parse-hyperlinks") (v "0.13.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0s9rk2ccss8nvx56780226nbbd1qqx841lbbhbv233ywcsijhvlm")))

(define-public crate-parse-hyperlinks-0.14.0 (c (n "parse-hyperlinks") (v "0.14.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0hnwd2pzllbq50gvi7jy62ak6ly9h4lqiyr2s78zasz1hzv5qdag")))

(define-public crate-parse-hyperlinks-0.15.0 (c (n "parse-hyperlinks") (v "0.15.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1nzc42yym4ksm9ad0civqqacyk841n842cnw2yasadag5l5zqdla")))

(define-public crate-parse-hyperlinks-0.16.0 (c (n "parse-hyperlinks") (v "0.16.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1x1m4hqqrxm1z258h3i76w2jvri9kf185i0iwbji0yg2w4d222hi")))

(define-public crate-parse-hyperlinks-0.17.0 (c (n "parse-hyperlinks") (v "0.17.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0cnl7hrpa6yxdljxrvv2q5qk66ycfvfs135zprhf2mrd5r57brzh")))

(define-public crate-parse-hyperlinks-0.17.1 (c (n "parse-hyperlinks") (v "0.17.1") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0jwm01jzq2d1b3c40njmyyrhff2ciamyyx01j0a31zjpyi13y20v")))

(define-public crate-parse-hyperlinks-0.17.2 (c (n "parse-hyperlinks") (v "0.17.2") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1vrzk4qjvq833n08klkyinqv13mpa9yf1w5hd4km5xx57by2j8rv")))

(define-public crate-parse-hyperlinks-0.17.3 (c (n "parse-hyperlinks") (v "0.17.3") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "081m9z38kr8j7kz2lvkjj0806ii5jfm7qgxg37k3bs1p2pjx9a6c")))

(define-public crate-parse-hyperlinks-0.17.4 (c (n "parse-hyperlinks") (v "0.17.4") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0jil6dbl02xyxx33axhyvv9giasw0i64ag5lv1viim9wb9gdf0a9")))

(define-public crate-parse-hyperlinks-0.17.5 (c (n "parse-hyperlinks") (v "0.17.5") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1hww430ygiiy2alm3py0yhkivrpw851rcwfcxachvw0afb16hv6l")))

(define-public crate-parse-hyperlinks-0.18.0 (c (n "parse-hyperlinks") (v "0.18.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0l7sqyhrh3xjvjjj2c7ld8rma14q5j5kvhkrsab4s6q0m64n19mh")))

(define-public crate-parse-hyperlinks-0.19.0 (c (n "parse-hyperlinks") (v "0.19.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "10v5gcs2gvjfxblqkr7payff4ip5dvrb7xd3g39xyjvpfvha9jd5") (y #t)))

(define-public crate-parse-hyperlinks-0.19.1 (c (n "parse-hyperlinks") (v "0.19.1") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0fp5a2x7xgy18psl0bsc3i8z59bs5kwia9jg6cyhs1lpbwrxlprc")))

(define-public crate-parse-hyperlinks-0.19.2 (c (n "parse-hyperlinks") (v "0.19.2") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0gz114zw6g9nnary352j1bik4pyap3457ymw13ci9mg4fy9hd1bp")))

(define-public crate-parse-hyperlinks-0.19.3 (c (n "parse-hyperlinks") (v "0.19.3") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0lqrfjr4sn8hf8j5jvlxbajsw15qzxqclqh2pfdwfrq2d3r3s7b4")))

(define-public crate-parse-hyperlinks-0.19.4 (c (n "parse-hyperlinks") (v "0.19.4") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0jh4qsp01fff5hv09k0cq7f5xxr032v2nk0hq9wyd0aniymcsmip")))

(define-public crate-parse-hyperlinks-0.19.5 (c (n "parse-hyperlinks") (v "0.19.5") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1rx8ym9ngf1i5px3mj5qp5kpfsrqc0ih48wxwck8fi3pjq07agcf")))

(define-public crate-parse-hyperlinks-0.19.6 (c (n "parse-hyperlinks") (v "0.19.6") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1zqh6fhalkracjpmhxz0wrd9b1kah3gwb8nzq5cd3fpx08w1bzcx")))

(define-public crate-parse-hyperlinks-0.20.0 (c (n "parse-hyperlinks") (v "0.20.0") (d (list (d (n "html-escape") (r "^0.2.7") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "04hqqbc2567324bv2accr3m75xnpqk8kz6vd8mhlmr7s59ifv7c5") (y #t)))

(define-public crate-parse-hyperlinks-0.21.0 (c (n "parse-hyperlinks") (v "0.21.0") (d (list (d (n "html-escape") (r "^0.2.7") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "12z0crjhad2jkbq0nhi39xhdy9m4gw1mk5ac9z2lclc3vn47bq22") (y #t)))

(define-public crate-parse-hyperlinks-0.22.0 (c (n "parse-hyperlinks") (v "0.22.0") (d (list (d (n "html-escape") (r "^0.2.7") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1jzfdm91i2klr0dvwqmc0kvm12w8b6jwxiavz11lwqvsq47sr4ix")))

(define-public crate-parse-hyperlinks-0.22.1 (c (n "parse-hyperlinks") (v "0.22.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0ianrmb7awzj6qja323y7zbiabf61i9bx5nqx3yps9xcjf3fcyjn")))

(define-public crate-parse-hyperlinks-0.23.0 (c (n "parse-hyperlinks") (v "0.23.0") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "00pvc9nkwymkayx10dgfhga9kdg8fzygsbxax55czwzjd8vv3bnb")))

(define-public crate-parse-hyperlinks-0.23.1 (c (n "parse-hyperlinks") (v "0.23.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1lp5p84rvvsgnfjd40l4yi5s3hzqi95rdm5v5d4vd802853y7cjn")))

(define-public crate-parse-hyperlinks-0.23.2 (c (n "parse-hyperlinks") (v "0.23.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0hbygzwqdjlnl5i3pkij3gczhfgghsd3wxawc5srhfafjaacdfld")))

(define-public crate-parse-hyperlinks-0.23.3 (c (n "parse-hyperlinks") (v "0.23.3") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "04nplp6fml4dkyiz5fw23lh6csp758ks347lsgwk3zkfaj58s8f1")))

(define-public crate-parse-hyperlinks-0.23.4 (c (n "parse-hyperlinks") (v "0.23.4") (d (list (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "13jblp4r58sshnrbq23av9hmc01km70j7y3wpv45rqss9myd7081")))

(define-public crate-parse-hyperlinks-0.24.0 (c (n "parse-hyperlinks") (v "0.24.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "127v4ac9jbx048cysxjgr0ihnn9a4bi19yc3gv5dh8x3547p3378") (y #t)))

(define-public crate-parse-hyperlinks-0.24.1 (c (n "parse-hyperlinks") (v "0.24.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "06gqsjgvvrid9sspr1dmyxll5f46n7bkd3apfb1g7g7dnb5sxjvj") (y #t)))

(define-public crate-parse-hyperlinks-0.24.2 (c (n "parse-hyperlinks") (v "0.24.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1g4rh5gbirgvcbchrzr0g34b4rivfh1fn5q7cda7xg8b8npskvy1") (y #t)))

(define-public crate-parse-hyperlinks-0.25.0 (c (n "parse-hyperlinks") (v "0.25.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0i81whvdlq5fwsalwbpa0alwgxjw7walhiinxvz40sq8pz37h5a3") (y #t)))

(define-public crate-parse-hyperlinks-0.26.0 (c (n "parse-hyperlinks") (v "0.26.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "09mdrk0mj0g398pjxy96084n0nd66jdjfqis88r0bbf8q24dk8c0") (y #t)))

(define-public crate-parse-hyperlinks-0.25.1 (c (n "parse-hyperlinks") (v "0.25.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1yyvj75fg9bc78g8axgq9f0909aj1q6sl1q730z1xymz7526cs33") (y #t)))

(define-public crate-parse-hyperlinks-0.26.1 (c (n "parse-hyperlinks") (v "0.26.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0k012sw9jwqavib9jklcmbv740znrnnfsna91xj9nkfplvq87dvq") (y #t)))

(define-public crate-parse-hyperlinks-0.26.2 (c (n "parse-hyperlinks") (v "0.26.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1qdf5mawi71mra3nqzrsms77pkhrc3kdw28jm9b1qg3xnb5srd09") (y #t)))

(define-public crate-parse-hyperlinks-0.26.3 (c (n "parse-hyperlinks") (v "0.26.3") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "13jx9fk6nbfvqp1nvss8dd0zvjizczqvhzsw2q3i4wa62mcdv2k3") (y #t)))

(define-public crate-parse-hyperlinks-0.26.4 (c (n "parse-hyperlinks") (v "0.26.4") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1bbyf2fw57bmi0ac6p1zzwzni42rq8414rg84rqg3d8f6z6h41hg") (y #t)))

(define-public crate-parse-hyperlinks-0.27.0 (c (n "parse-hyperlinks") (v "0.27.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0pc0h94kvn77flpzdnkk8is4w69ldhrjqmbmk7873l3a1iiry2vm")))

(define-public crate-parse-hyperlinks-0.27.1 (c (n "parse-hyperlinks") (v "0.27.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0rl8bibigl9zfld6fs98a3di8i9alvpx1bx5gx7qs8c21hznqhmw") (y #t)))

(define-public crate-parse-hyperlinks-0.27.2 (c (n "parse-hyperlinks") (v "0.27.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1rj0hrf5rva8wwddvlqzq68h9zwlsl8b6a0w1mi8jyncaw371b4j")))

