(define-module (crates-io pa rs parsr) #:use-module (crates-io))

(define-public crate-parsr-0.1.0 (c (n "parsr") (v "0.1.0") (h "0l3bb3j0vvdywhrih3b70ynnywv7v0ids9nm6vh240yc2xrlmaiy")))

(define-public crate-parsr-0.1.1 (c (n "parsr") (v "0.1.1") (h "142981izmlmgd9laawwka5m3p382rdmwrndpm2y5dcbdqha727hr")))

(define-public crate-parsr-0.1.2 (c (n "parsr") (v "0.1.2") (h "0jij2wrsxy785wgn2jwnqdyg0y3clmz74l6wkg59wzs5qjrx0agh")))

(define-public crate-parsr-0.1.3 (c (n "parsr") (v "0.1.3") (h "1l9k9y6qb7agji2a4gf77901gh1zjd2qr9293j7552fcsh6syl3r")))

(define-public crate-parsr-0.1.4 (c (n "parsr") (v "0.1.4") (h "1cbwjlqxh0klgf0z1x7zxfgxlbg55snagfcrwq2g92w9r4hqraln")))

(define-public crate-parsr-0.2.0 (c (n "parsr") (v "0.2.0") (h "0wszfvz17mdx9w9yxj139nmrsk9z583ri0p0psclgs82fsppai3i")))

(define-public crate-parsr-0.2.1 (c (n "parsr") (v "0.2.1") (h "07ddy9shwllyc8psh9wgisiwjhr4xp72ky0wi7blpmg4lywyri4w")))

(define-public crate-parsr-0.2.2 (c (n "parsr") (v "0.2.2") (h "1qzwy0q8z241jwcd1dpigr8510m4jq75f3wxyq49pqs2gsy3dfnp")))

(define-public crate-parsr-0.2.3 (c (n "parsr") (v "0.2.3") (h "1yc1xfxpmh9ipcpz6c2i20zrh8xpfnk7ig3n06ygpskwf8hdaayi")))

(define-public crate-parsr-0.2.4 (c (n "parsr") (v "0.2.4") (h "0ffyj8xw2czbygs6nc5jjzabnyrc2szcn6q20yhd7ywszgpz0xml")))

(define-public crate-parsr-0.2.5 (c (n "parsr") (v "0.2.5") (h "1h6g5x4vvrzyyzqmc0znlqrwn1lk28sgb8z2i6vrymkslv11w3ha")))

(define-public crate-parsr-0.2.6 (c (n "parsr") (v "0.2.6") (h "1z9n05zjpwaf43bja14qy8slry4pc8livfbbig2yx0ih0mrh3r5s")))

(define-public crate-parsr-0.3.0 (c (n "parsr") (v "0.3.0") (h "1y657whs4r7173c8b4mb80lns2km98sana0b52qidi1l3aq1i9vf")))

(define-public crate-parsr-0.3.1 (c (n "parsr") (v "0.3.1") (h "0cwb42qxs7jlipd91mc42rkymwi5sn6zcr9q6w4ya9lbggglv2k9")))

(define-public crate-parsr-0.4.0 (c (n "parsr") (v "0.4.0") (h "1347gqwfprh99m99fyyjavbsvwp2jyfw4m4wfjj6w0qajc3rjvzw")))

