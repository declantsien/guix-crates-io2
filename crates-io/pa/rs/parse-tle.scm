(define-module (crates-io pa rs parse-tle) #:use-module (crates-io))

(define-public crate-parse-tle-0.1.0 (c (n "parse-tle") (v "0.1.0") (h "0vmr613r0irljvpwbhrwr4796iysppbkrmkc1a0dzv28q5f5sh95")))

(define-public crate-parse-tle-0.1.1 (c (n "parse-tle") (v "0.1.1") (h "1l372ywx3p5iqzgkxvbr5999pxhlpnmlqm5hbisx1w6mqkfcik6y")))

(define-public crate-parse-tle-0.1.2 (c (n "parse-tle") (v "0.1.2") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)))) (h "0jyhz4s9xfb3hhb6jv3yji4j59ipa0s146zxz01izgy4hix0g93c")))

(define-public crate-parse-tle-0.1.3 (c (n "parse-tle") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zbpwnqilhv9iy9zby036kq95fx5247g6xsagg985f0v66fs0j3p")))

(define-public crate-parse-tle-0.1.4 (c (n "parse-tle") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y1bbmfzni0gfici2q9j9bn6m7rdvzyhkjlppwybcmjm4j5bw70b")))

