(define-module (crates-io pa rs parse-zoneinfo) #:use-module (crates-io))

(define-public crate-parse-zoneinfo-0.1.0 (c (n "parse-zoneinfo") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0gnganpymhnxhyj0p55dqqrzwxbwdk2hd3c48ga4xyg0rxwps7b7")))

(define-public crate-parse-zoneinfo-0.1.1 (c (n "parse-zoneinfo") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1skshsb54sv51vhlkgkdfqsdsa0j9xqzjrrllcssxbbdcniikvpl")))

(define-public crate-parse-zoneinfo-0.2.0 (c (n "parse-zoneinfo") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rbrnzfqx0kcaddnvznvr721pnk79cg5m7ch7267pmydrn63k6h8")))

(define-public crate-parse-zoneinfo-0.2.1 (c (n "parse-zoneinfo") (v "0.2.1") (d (list (d (n "regex") (r "^1.3.1") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "01zzysdmin7fw88vsnlwqic93wnch7zxq2nhly102h1v278fkv7y")))

(define-public crate-parse-zoneinfo-0.3.0 (c (n "parse-zoneinfo") (v "0.3.0") (d (list (d (n "regex") (r "^1.3.1") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "0h8g6jy4kckn2gk8sd5adaws180n1ip65xhzw5jxlq4w8ibg41f7")))

(define-public crate-parse-zoneinfo-0.3.1 (c (n "parse-zoneinfo") (v "0.3.1") (d (list (d (n "regex") (r "^1.3.1") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "093cs8slbd6kyfi6h12isz0mnaayf5ha8szri1xrbqj4inqhaahz") (r "1.56.0")))

