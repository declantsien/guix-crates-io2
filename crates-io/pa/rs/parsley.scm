(define-module (crates-io pa rs parsley) #:use-module (crates-io))

(define-public crate-parsley-0.1.0 (c (n "parsley") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1001m98s1hb90slj4l9ddx3jsy60g0dxy8v9da2hxkn8qbmdc1hq")))

(define-public crate-parsley-0.2.0 (c (n "parsley") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1g2ia1d0la2lqcw457qvgbdi6ippli1g1c8h348jhg22pqcj7sr8")))

(define-public crate-parsley-0.2.1 (c (n "parsley") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ypivw58yclp21lmvykydwlyfcq5p4291y2080858cckpkxgqas3")))

(define-public crate-parsley-0.2.2 (c (n "parsley") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1yjwkl1fix7zlqz6b5m1kgc7r1sn9s8x3pbvcdvhz9m0w2f2kc7z")))

(define-public crate-parsley-0.3.0 (c (n "parsley") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0crbvs99jfwqrcf4dd0zpzn55cnpbiial60sd1i6gj0xbmrpd867")))

(define-public crate-parsley-0.3.1 (c (n "parsley") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "08m832gsd4fj5fmjix3vmqi59dgy5hjkbbkq8y2cr94mj8i4bkh0")))

(define-public crate-parsley-0.4.0 (c (n "parsley") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0vmsqw99a9ndl6d9wpbpbqzisc9jhgk85iq3ql6c5965pjzbj0b2")))

(define-public crate-parsley-0.4.1 (c (n "parsley") (v "0.4.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0maq0zp45jwpcqhd497v68g9vnq84wxkb41r3k9l64800ajrbg95")))

(define-public crate-parsley-0.4.2 (c (n "parsley") (v "0.4.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1s65lw4d7dima505xkhfn5pidqwjlspnv51iigif2ydzz85ivkxa")))

(define-public crate-parsley-0.4.3 (c (n "parsley") (v "0.4.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1igjyjdqkf6p0lnp5rh4c855fw4yyp0jc6bb8jd7fmjkzbwlahin")))

(define-public crate-parsley-0.5.0 (c (n "parsley") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0f2nlj2jasjn7b2iz7aw8fvibmd7pxcspl1wn1b43z76x30jl0bn")))

(define-public crate-parsley-0.6.0 (c (n "parsley") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1vzp48986pmzv3a793c7qglzn1vs2ivgmlgk9j1dahm54w5k1xnm")))

(define-public crate-parsley-0.6.1 (c (n "parsley") (v "0.6.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0jqv8z04raadwqnp4i4ygg1l6xyx3jmwd350f49nqnbjg3mim58w")))

(define-public crate-parsley-0.6.2 (c (n "parsley") (v "0.6.2") (d (list (d (n "rustyline") (r "^3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "14zglx3cmmwmp6daqiqlg0v6aisbzff2ap8z3wa5wh3nwac3gr8g")))

(define-public crate-parsley-0.7.0 (c (n "parsley") (v "0.7.0") (d (list (d (n "rustyline") (r "^3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "149vv5126vzv4zzinf5x2wg64wcl8m1ais9yjbzckd7pn2fm84im")))

(define-public crate-parsley-0.8.0 (c (n "parsley") (v "0.8.0") (d (list (d (n "rustyline") (r "^3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "199jmas5z1a4x7hpv595x4fv2skiigi44ljlb2rxim0zqkfq8kl2")))

(define-public crate-parsley-0.8.1 (c (n "parsley") (v "0.8.1") (d (list (d (n "rustyline") (r "^3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1y6mnqyv009948sjkz5jzmf0nlsjky8ydmzpz4p7sli824n4rv5v")))

(define-public crate-parsley-0.9.0 (c (n "parsley") (v "0.9.0") (d (list (d (n "rustyline") (r "^3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "structopt") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1q2g7nm3axky31ql8qm1y2g0a1vfsksckjws4lpqhvqfsvfb1q8x")))

(define-public crate-parsley-0.10.0 (c (n "parsley") (v "0.10.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustyline") (r "^10.0.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0mwsyjpgyjyaa0577pf7qpjv43ixd6w03mwpnvmzwxrrxiqdyj5d")))

