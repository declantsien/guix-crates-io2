(define-module (crates-io pa rs parse-dat-url) #:use-module (crates-io))

(define-public crate-parse-dat-url-0.1.0 (c (n "parse-dat-url") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.99") (d #t) (k 2)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "00iyd7faj57ka9k5qxqp55c6nihymf4kwcp6s4qmd0xac0dgjn72") (f (quote (("default" "serde"))))))

