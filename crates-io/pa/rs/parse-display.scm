(define-module (crates-io pa rs parse-display) #:use-module (crates-io))

(define-public crate-parse-display-0.1.0 (c (n "parse-display") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wbadjiyhim3pk3kha1l62sypqwypqrdsxmadiijaqfz8srck695")))

(define-public crate-parse-display-0.1.1 (c (n "parse-display") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dnqfr39jqqrvwsrh24sym50l62mkcnkpz3z9wvvcmmhqqml52vi")))

(define-public crate-parse-display-0.1.2 (c (n "parse-display") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04400mms2zhrv747qcchfxilw47d2jhma7zrc3lpzbnb6mb751xx")))

(define-public crate-parse-display-0.2.0 (c (n "parse-display") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1nwi30hpln9c248hkghrpx84cy0k2838rw9jmppj3sqrb8xxhyrz") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.3.0 (c (n "parse-display") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1kzmrpfb30s7pz3gd9vqmpgvjkyzz8cvk71bp4pgv584bj0z28wc") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.3.1 (c (n "parse-display") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "052gz371gkm281cs1pz8i1mz7ix4sr8vdb9xv3qk3q54n4y72cfn") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.4.0 (c (n "parse-display") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "14ff0l5rchdzj9r2ix99j0fkjnzlqm6q78ygkzsz9nanwhr5a0qj") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.4.1 (c (n "parse-display") (v "0.4.1") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1ccpba91a4qbgb4j5lj66imlci17iqjabrwqjrr7rh267hmiawbj") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.0 (c (n "parse-display") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "1s9z2h0vn87kxpvfy2jg1dkly2viwp3ccmgl1sa8021y0km9hznc") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.1 (c (n "parse-display") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "0a53xf7sjnyf4fgwkz1rhplawf6k5yk4gf7a7bnk3f762hg025wg") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.2 (c (n "parse-display") (v "0.5.2") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "180lpn55ijs8x21cxlagn52spaaqpmb0kcbnrjc8mv2jjkyfmdnd") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.3 (c (n "parse-display") (v "0.5.3") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "0jiki631a00h7hzax6xnn2vfa9g81ayhngvc9vyxppk9lp1g92w9") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.4 (c (n "parse-display") (v "0.5.4") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "1s1pb7dxlh9c4bw3vsc3fhgan1swa11hga9x5k17w7fvw8s01ccb") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.5.5 (c (n "parse-display") (v "0.5.5") (d (list (d (n "once_cell") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "19zs24adgpgr352d6vv7h3rxcywnbkfb03msvvlv5gid4g392gl1") (f (quote (("std") ("default" "std" "regex" "once_cell"))))))

(define-public crate-parse-display-0.6.0 (c (n "parse-display") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0r3r6an8gnbjh0k1f93qbjbyzihx6rp6k6ml3ys2i1aaz57ay99v") (f (quote (("std" "regex" "once_cell") ("default" "std"))))))

(define-public crate-parse-display-0.7.0 (c (n "parse-display") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "13aqqaa3abf7pvci02024dwqk22f349a8nqg9jvkha11r3v34sxc") (f (quote (("std" "regex" "once_cell") ("default" "std"))))))

(define-public crate-parse-display-0.8.0 (c (n "parse-display") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0v68dgb9irgl1v0yfjfnwvmf5hs1257a2ff2lawnp2bj7l1wr5kg") (f (quote (("std" "regex" "once_cell") ("default" "std"))))))

(define-public crate-parse-display-0.8.1 (c (n "parse-display") (v "0.8.1") (d (list (d (n "once_cell") (r "^1.17.1") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (o #t) (d #t) (k 0)))) (h "1a33sdpiyl3a1aid5s5dvfvv9ibankzq2rhcg24km85aqb6wdjnx") (f (quote (("std" "regex" "once_cell") ("default" "std"))))))

(define-public crate-parse-display-0.8.2 (c (n "parse-display") (v "0.8.2") (d (list (d (n "once_cell") (r "^1.17.1") (o #t) (d #t) (k 0)) (d (n "parse-display-derive") (r "=0.8.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (o #t) (d #t) (k 0)))) (h "0p5w8jmb05lp3s9m5crwp1fkmgnc49xh5wlpzvdfhlrbf849sl66") (f (quote (("std" "regex" "once_cell") ("default" "std"))))))

(define-public crate-parse-display-0.9.0 (c (n "parse-display") (v "0.9.0") (d (list (d (n "parse-display-derive") (r "=0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1rfyaagvihz49npz04qbp1f8lcm86wp62ba6m2dvsizb6f9mzbq6") (f (quote (("std" "regex" "regex-syntax") ("docs") ("default" "std"))))))

