(define-module (crates-io pa rs parsec-core) #:use-module (crates-io))

(define-public crate-parsec-core-0.1.0 (c (n "parsec-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "06qc4a5q3nsqrqh6vhnas3jdlmry5xinwxskgabwzczn3fcqql9f")))

(define-public crate-parsec-core-0.1.1 (c (n "parsec-core") (v "0.1.1") (d (list (d (n "any-rope") (r "^1.0.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)))) (h "1hdv28327wghl4gdz2f3c60z7nhdvslk33dqfxra38wssn49670i")))

(define-public crate-parsec-core-0.1.2 (c (n "parsec-core") (v "0.1.2") (d (list (d (n "any-rope") (r "^1.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)))) (h "0w73z2i28azf2z6blp96di14n1nj5n5cljj91qrs8xvf6likrnws") (f (quote (("wacky-colors") ("disable-testing") ("deadlock-detection"))))))

(define-public crate-parsec-core-0.1.3 (c (n "parsec-core") (v "0.1.3") (d (list (d (n "any-rope") (r "^1.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)))) (h "1mxpnyp87gskp7wg3z0lrq0fpqbq76w8s0mvnspg3p9c6bkhlnlh") (f (quote (("wacky-colors") ("disable-testing") ("deadlock-detection"))))))

(define-public crate-parsec-core-0.1.4 (c (n "parsec-core") (v "0.1.4") (d (list (d (n "any-rope") (r ">=1.1.0") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "ropey") (r ">=1.6.0") (d #t) (k 0)))) (h "0fsp7kvpd85jswdh65arf54l5arnccrkys2fr90m5ypnj3vnr70i") (f (quote (("wacky-colors") ("disable-testing") ("deadlock-detection"))))))

(define-public crate-parsec-core-0.2.0 (c (n "parsec-core") (v "0.2.0") (d (list (d (n "any-rope") (r ">=1.2.2") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "ropey") (r ">=1.6.0") (d #t) (k 0)))) (h "0d6mqhmy86sbmc6vim4lag847075n1g2464xjvwhq5dxvkp8b9ry") (f (quote (("wacky-colors") ("disable-testing") ("default"))))))

