(define-module (crates-io pa rs parse_argument) #:use-module (crates-io))

(define-public crate-parse_argument-0.1.0 (c (n "parse_argument") (v "0.1.0") (h "1hrrzaqba8yjj4x2279a2hj91v9q8xlmmdxi8ji96x0r0m28npbm") (y #t)))

(define-public crate-parse_argument-0.1.1 (c (n "parse_argument") (v "0.1.1") (h "1grdbn9waxm0hy3xk24a10s5hzxpcgqmzvhk8y1q7p5m1az19n8g") (y #t)))

(define-public crate-parse_argument-0.1.2 (c (n "parse_argument") (v "0.1.2") (h "0nrp0g6wp1n6yrwifmlx6crqg146p3fk1fqv8gpsdv2fghqa7hl9") (y #t)))

(define-public crate-parse_argument-0.1.3 (c (n "parse_argument") (v "0.1.3") (h "09smhz31x5sr4ijisz58rva9sxf3pcr553yza28ccp9bcs7nsd9b") (y #t)))

(define-public crate-parse_argument-0.1.4 (c (n "parse_argument") (v "0.1.4") (h "0gy80f3dhzav2jl7ymdyz897gbfw8z36wx40341kjksifcid9n32")))

(define-public crate-parse_argument-0.1.5 (c (n "parse_argument") (v "0.1.5") (h "067manrlnkfwhp9c5asbslybhvf1m4m6kxafcrrr0q2xg66llp17")))

