(define-module (crates-io pa rs parsli) #:use-module (crates-io))

(define-public crate-parsli-0.0.1 (c (n "parsli") (v "0.0.1") (d (list (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0mx2p83kcfxi1kpg5yb7a5fplp76dplk1dwr1yngz5ygcfaa2yzq")))

(define-public crate-parsli-0.0.2 (c (n "parsli") (v "0.0.2") (d (list (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0ra2syn4lhmrqw6hps9xyw03l099cwxvkfndpjglpk6a8ink4j5n")))

(define-public crate-parsli-0.0.3 (c (n "parsli") (v "0.0.3") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "1vz8dccvs8wh635jk4dsdjbc1h9mpid70ps5l12wrrdxh2qcwf6n")))

(define-public crate-parsli-0.1.0 (c (n "parsli") (v "0.1.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1460dhf9q6li8n1lfb61f06n1wrgfb9xzy2r1grq9rwxznf9pfib")))

(define-public crate-parsli-0.1.1 (c (n "parsli") (v "0.1.1") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "06psdf6mix03df1zkr31dj35msf1v2h5rlpf0d1j2rl1yiwsq1cy")))

(define-public crate-parsli-0.1.2 (c (n "parsli") (v "0.1.2") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "116n6cbzxcqjw0yvr05xs99ingzp8qr1mzybczfyyc6021blxh6w")))

(define-public crate-parsli-0.1.3 (c (n "parsli") (v "0.1.3") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "19kg3ygl06z00p5bx0fva7zzg12ad948amamd9mgq1rwv31p3r6j")))

(define-public crate-parsli-0.1.4 (c (n "parsli") (v "0.1.4") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "indicatif") (r "=0.17.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1gzgp69biy65vd454545sbiz3axk9fy52c62lmf2cn59lxwb988p")))

