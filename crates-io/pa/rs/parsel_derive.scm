(define-module (crates-io pa rs parsel_derive) #:use-module (crates-io))

(define-public crate-parsel_derive-0.1.0 (c (n "parsel_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05ls0qh6x1gi4n5gj2dwz8lfpd0c9syi4cr7z9mqdxglz7py54gs")))

(define-public crate-parsel_derive-0.1.1 (c (n "parsel_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1z6yxcdkv26x5zlf77qzf6953kwj3cgb826pj5jqw9458rq20f1h")))

(define-public crate-parsel_derive-0.2.0 (c (n "parsel_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hjf4s56z8r9w13g0rsqgi1ji9j4p0nkmws5189snd28mf397jzl")))

(define-public crate-parsel_derive-0.3.0 (c (n "parsel_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qr81vwbf7blfzkiplpwnf8s70g8qlxdkz18ys0dpxifmdyqqjyp")))

(define-public crate-parsel_derive-0.3.1 (c (n "parsel_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xymk8a0cdmvbkaxp7zqdzcwlrz1znjwjwasjcddc1kgiymbglxp")))

(define-public crate-parsel_derive-0.4.0 (c (n "parsel_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03h4caz245w2839xn69l91hyls912q4ipx503a499bbjgvz5agvj")))

(define-public crate-parsel_derive-0.5.0 (c (n "parsel_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h321776615f8rkgk0pg74k529wi7g9v3z1yp2i3n4wripqh3s50")))

(define-public crate-parsel_derive-0.6.0 (c (n "parsel_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xwa62z0szks2yigp3mailsgj11sr3j4qf9m9a4xkim8s6zgsz9k")))

(define-public crate-parsel_derive-0.6.1 (c (n "parsel_derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04fcjs3ndgvq3cg5ry84gxn02pf75cfvp9fw3z8zj9pxk62l1gz9")))

(define-public crate-parsel_derive-0.6.2 (c (n "parsel_derive") (v "0.6.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "175sv664b3pd1qxgqkr0a2bv5m7xv5255byjlnb4pvb7rxj4pmfm")))

(define-public crate-parsel_derive-0.7.0 (c (n "parsel_derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f2x5afp2x5s4nf1r7611c15v2q9zd2mhnxcjhg82lzszgi35n9x")))

(define-public crate-parsel_derive-0.7.1 (c (n "parsel_derive") (v "0.7.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xnaaadlcjp8g2kamqfr7bmp1kmjlyzrj63r9nfkhvpbz15mpmap")))

(define-public crate-parsel_derive-0.8.0 (c (n "parsel_derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rfxl2ydbpx7slwik47br8drsx86y9p4h7w7yjzjb1gc00ymvcgd")))

(define-public crate-parsel_derive-0.8.1 (c (n "parsel_derive") (v "0.8.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10f2j1s6kgmw9an39rnn3kz79ppvv5i4cb53l5prg42m5czvw4ki")))

(define-public crate-parsel_derive-0.8.2 (c (n "parsel_derive") (v "0.8.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13m7pg31hqc2cqcb93dz6hhk83vjkbyzwfjhmphaz54bakzjx1if")))

(define-public crate-parsel_derive-0.8.3 (c (n "parsel_derive") (v "0.8.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04c7mvvf4mj3jl5nhwray15hr2rryxz16ixv2kz44qlqcmajz9qa")))

(define-public crate-parsel_derive-0.9.0 (c (n "parsel_derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18mkdbwi4s3dbbiywbv0frgl1cc1xc7l2z3nxr9bpnsmqkb1sdbw")))

(define-public crate-parsel_derive-0.9.1 (c (n "parsel_derive") (v "0.9.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08ls9alc1bxnw6vsyc2jla38b11d37ymg4qgpw00vc5gnxh5m6ss")))

(define-public crate-parsel_derive-0.9.2 (c (n "parsel_derive") (v "0.9.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "012259baqfxw4j4n0a4pq7vrx50cnyzawk8ykmjqgy85z3ih0zr8")))

(define-public crate-parsel_derive-0.9.3 (c (n "parsel_derive") (v "0.9.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.44") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qg2835sx6p28vkb403gvk1vihk6lx2nxh1nvchsmf7qcchzbmsv")))

(define-public crate-parsel_derive-0.9.4 (c (n "parsel_derive") (v "0.9.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dp256ww84andj6qmv1bdgjjvjdyj0cz2qy3lb705qq7nbx1dbm8")))

(define-public crate-parsel_derive-0.10.0 (c (n "parsel_derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p7d0q02hyfgyiznxz7ibkyyhcsqz3316xmcx9rw8x3aivl7jmy4")))

(define-public crate-parsel_derive-0.11.0 (c (n "parsel_derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p93h8yj97wrkl5p5n7xv3lpbvm09wn5k1jlz0rkaf6fyjchdklb")))

(define-public crate-parsel_derive-0.12.0 (c (n "parsel_derive") (v "0.12.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qnjxi0lgnjd8443kfk1iwyj5knbr2xwkzfjwzalcilpd6vw6437")))

(define-public crate-parsel_derive-0.13.0 (c (n "parsel_derive") (v "0.13.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xwm94ig4idasf9682nxpyprfp2n4y086rva49bsr1swbgcmzm0y")))

(define-public crate-parsel_derive-0.13.1 (c (n "parsel_derive") (v "0.13.1") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fmwmwmdf9wj3034dy41gxx4xnpvwni6hh6qnpxz3xi9jfn136al")))

(define-public crate-parsel_derive-0.14.0 (c (n "parsel_derive") (v "0.14.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m92a49m2kcpvd7zmmqn7p9sdmzi1q2wa9x9qxsgmlkafbk4pbp2")))

