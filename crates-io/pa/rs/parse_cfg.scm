(define-module (crates-io pa rs parse_cfg) #:use-module (crates-io))

(define-public crate-parse_cfg-0.1.0 (c (n "parse_cfg") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "03k849zqdbwm86hccnsm8pfp2g7x3dfy2vm4ax2mn9crmk0d3iq0") (y #t)))

(define-public crate-parse_cfg-1.0.0 (c (n "parse_cfg") (v "1.0.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0ajmgkh83bci6z1vc3d4nyswqzgvgbwhl10x5yngbq2mbzx8gjvp") (y #t)))

(define-public crate-parse_cfg-1.0.1 (c (n "parse_cfg") (v "1.0.1") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "00s5vgffdqgp18i0j1girlccv2yi9772xfp71jl702jjwv1l81a9") (y #t)))

(define-public crate-parse_cfg-2.0.0 (c (n "parse_cfg") (v "2.0.0") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "1iqc2c2xhzdqf39f5bwciv5y8sgfy57pkfr4d5ipdqh6bfqv87nj")))

(define-public crate-parse_cfg-3.0.0 (c (n "parse_cfg") (v "3.0.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1ibdm9j8h1cxn2d81jx4hry8fmmbxf940r7v386lzdivw9v2a4m8")))

(define-public crate-parse_cfg-3.1.0 (c (n "parse_cfg") (v "3.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "05kpiqxdjhrg4i9c5dpzb4d6vyhzrraf7cx7pvkbbnkyp1pdbw60")))

(define-public crate-parse_cfg-4.0.0 (c (n "parse_cfg") (v "4.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0l64gjn1y7c036hxipzwpi5f76byyw5zrj27mh1q8wxbn38gcabn")))

(define-public crate-parse_cfg-4.1.0 (c (n "parse_cfg") (v "4.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "17h436fxhih1x5a1mm7pwgfs9g35ayascwqlp3l511cbchv6607y")))

(define-public crate-parse_cfg-4.1.1 (c (n "parse_cfg") (v "4.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0jgalw2hwv3348q2i98qy6h3rngkhlp2b6kwir023ix26jj8fmwh")))

