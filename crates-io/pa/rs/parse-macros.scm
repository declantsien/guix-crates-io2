(define-module (crates-io pa rs parse-macros) #:use-module (crates-io))

(define-public crate-parse-macros-0.1.0 (c (n "parse-macros") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.4") (d #t) (k 2)) (d (n "parse-generics-poc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "parse-generics-shim") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)) (d (n "serde") (r "^0.6.11") (d #t) (k 2)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 2)))) (h "1swrhzjmvydaai2hfr254wc5hqp0f1mdk99mzm2wqyfmz1jjr3am") (f (quote (("use-parse-generics-poc" "parse-generics-poc" "parse-generics-shim/use-parse-generics-poc"))))))

