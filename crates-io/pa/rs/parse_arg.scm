(define-module (crates-io pa rs parse_arg) #:use-module (crates-io))

(define-public crate-parse_arg-0.1.0 (c (n "parse_arg") (v "0.1.0") (h "1xgxivyfgcvs3wmss3m7h14y2qxsa9j760aslick5avkar6j4zjm")))

(define-public crate-parse_arg-0.1.1 (c (n "parse_arg") (v "0.1.1") (h "0rniv51cc2v8gc6m6467960qrncc42iyimr2ijnfldnd4xvwp87y")))

(define-public crate-parse_arg-0.1.2 (c (n "parse_arg") (v "0.1.2") (h "101zhkbflz84fmh96hsbhpbs4xv1xl7nz9kxj8335qw1gyaz4n4j")))

(define-public crate-parse_arg-0.1.3 (c (n "parse_arg") (v "0.1.3") (h "1bi4ra8mcdn07z87r4fb4kx0rv7znc729qqx4r97lqrawmg9k9ha")))

(define-public crate-parse_arg-0.1.4 (c (n "parse_arg") (v "0.1.4") (h "0m9yrpy49fydqhc1hwx2kc9gmr19vq9ica9a28h0wdgdxk48q90l")))

