(define-module (crates-io pa rs parsenmap) #:use-module (crates-io))

(define-public crate-parsenmap-0.1.0 (c (n "parsenmap") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0vmxdyslr6q3dr7va8ckzraq12s28hwzjhphs839s29ikl9g0r5a")))

(define-public crate-parsenmap-0.1.1 (c (n "parsenmap") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0h8nxr29famf5qlqfcqlsklr7nly63vjydysb5mgxa6p0kis2xrp")))

