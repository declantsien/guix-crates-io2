(define-module (crates-io pa rs parse_link_header) #:use-module (crates-io))

(define-public crate-parse_link_header-0.1.0 (c (n "parse_link_header") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1w64vp6kf0xg5hqwdrifajrp8jw8qg4y7clbs9csayiqqwprpwc0")))

(define-public crate-parse_link_header-0.1.1 (c (n "parse_link_header") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0l09j8ncy05bvhy7l6fr41ns1bc6dmq6hvxlcayki44mgybf1dbv")))

(define-public crate-parse_link_header-0.2.0 (c (n "parse_link_header") (v "0.2.0") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18dk6hwwqhbv411qz3pkp8vskbh4cfg9n4zkp8vm4nxmq9xz62ab")))

(define-public crate-parse_link_header-0.2.1 (c (n "parse_link_header") (v "0.2.1") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "182q5w0zwx681632pz2znrwrnna92wda4a06cz12d9bxakwqhkwv")))

(define-public crate-parse_link_header-0.2.2 (c (n "parse_link_header") (v "0.2.2") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16jpz5sk2clnzbx6x63a7jfnh3mdmm898h5783775527pk66gskn")))

(define-public crate-parse_link_header-0.3.0 (c (n "parse_link_header") (v "0.3.0") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "0adin3v2cnl5bvsln7jy6j9zml918yphh59knn4sisjxg21x1f1q")))

(define-public crate-parse_link_header-0.3.1 (c (n "parse_link_header") (v "0.3.1") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1ipxi2rpp8ifrranl1mrd33iki1k5ck9my9iqyc0cfh54nbsmah0")))

(define-public crate-parse_link_header-0.3.2 (c (n "parse_link_header") (v "0.3.2") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1ycdnhnfg1vq6hs6k2k548rwscfw9w2sp1ckyi2lr66y06f8qwj0")))

(define-public crate-parse_link_header-0.3.3 (c (n "parse_link_header") (v "0.3.3") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1ppn1if9h4rh9gsrmcs9fij256q489mvra41yccs1wmvxffzx1rn")))

