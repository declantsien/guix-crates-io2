(define-module (crates-io pa rs parse_mediawiki_dump) #:use-module (crates-io))

(define-public crate-parse_mediawiki_dump-0.1.0 (c (n "parse_mediawiki_dump") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 2)) (d (n "quick-xml") (r "^0.12") (d #t) (k 0)))) (h "1rkprjhdp025zpjvm9jj2h8qnkrn8b961275db6qbp8pawa81i86")))

