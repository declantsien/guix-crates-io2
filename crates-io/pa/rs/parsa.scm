(define-module (crates-io pa rs parsa) #:use-module (crates-io))

(define-public crate-parsa-0.1.0 (c (n "parsa") (v "0.1.0") (d (list (d (n "nevermore") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "1rqk1lkbgyx4d33gj440wyv3jfiv33v755yrxpsk7lv7g30i6svx") (f (quote (("default" "builtins") ("builtins" "error-macro")))) (s 2) (e (quote (("error-macro" "dep:thiserror"))))))

(define-public crate-parsa-0.1.1 (c (n "parsa") (v "0.1.1") (d (list (d (n "nevermore") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "1qn7q0f3m54vl3452l98d4mw6m9kcpzvmsgfz8igxw20baxq0gjv") (f (quote (("default" "builtins") ("builtins" "error-macro")))) (s 2) (e (quote (("error-macro" "dep:thiserror"))))))

(define-public crate-parsa-0.1.2 (c (n "parsa") (v "0.1.2") (d (list (d (n "nevermore") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "1zh1wl7sa65pbpm28xrgppdfmbg2h2724czv1s9qkp3qbgwv6vy0") (f (quote (("default" "builtins") ("builtins" "error-macro")))) (s 2) (e (quote (("error-macro" "dep:thiserror"))))))

(define-public crate-parsa-0.2.0 (c (n "parsa") (v "0.2.0") (d (list (d (n "nevermore") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "0i1i098kvkvzf96h7kc86dzlb28zizq560phwlz2f3jrydcwlkrw") (f (quote (("default" "builtins") ("builtins" "error-macro" "num-traits")))) (s 2) (e (quote (("error-macro" "dep:thiserror"))))))

(define-public crate-parsa-0.3.0 (c (n "parsa") (v "0.3.0") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0jajdr3r452gm56w908bi4si5pwp46ag32b63japi5jxb6yycj9i") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

(define-public crate-parsa-0.4.0 (c (n "parsa") (v "0.4.0") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ix1zf38ybncscmdk9zqfi0c31pc72q79rlxck9sgjlba63m8c7z") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

(define-public crate-parsa-1.0.0 (c (n "parsa") (v "1.0.0") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0j77xc65w1cx5prjxpzf7csyx1rxgqz5agb4j09xn3acxyh89prx") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

(define-public crate-parsa-1.0.1 (c (n "parsa") (v "1.0.1") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0m5qh5vf49nnq9hfjvidpl7i8slzj7finagbdf5a4xq6qz78v5gj") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

(define-public crate-parsa-1.1.0 (c (n "parsa") (v "1.1.0") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1bzrv0wfaz040iwxlgdl1cjgvjkd35vbmrc78mdd62m5i2ip6c6m") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

(define-public crate-parsa-1.1.1 (c (n "parsa") (v "1.1.1") (d (list (d (n "nevermore") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "020zmvwqyb6mi9nldcf2ka4hqb3kc1qpndnaqcfbqcyr74id6i71") (f (quote (("default" "builtins" "nevermore") ("builtins" "num-traits"))))))

