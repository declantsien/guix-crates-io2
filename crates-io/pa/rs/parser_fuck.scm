(define-module (crates-io pa rs parser_fuck) #:use-module (crates-io))

(define-public crate-parser_fuck-0.0.0 (c (n "parser_fuck") (v "0.0.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1gf6lcfpgzmjqmhiz31axly7vy3qjjr1s2wyz6lqpvbyc9vc001m")))

(define-public crate-parser_fuck-0.1.0 (c (n "parser_fuck") (v "0.1.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0p85nni7aj8gpw60p2z7frhvw648sh9z6ipr9dlmq9f4w9gqj6is")))

(define-public crate-parser_fuck-0.2.0 (c (n "parser_fuck") (v "0.2.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "06qhskv4digx34vk8x1bzsp7lk65986bdzw07s1cwvz2wszk3crk")))

(define-public crate-parser_fuck-0.2.1 (c (n "parser_fuck") (v "0.2.1") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1rd82dyv9giw5s47r546il16jss5xzvs7akbz2l8z0q3apf82rnr")))

(define-public crate-parser_fuck-0.2.2 (c (n "parser_fuck") (v "0.2.2") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0klyqa2ycmsw6j555dyallw7q8alfm9ya56g8l4pkw9facbkp3l4")))

(define-public crate-parser_fuck-0.3.0 (c (n "parser_fuck") (v "0.3.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0zd93r7lz4m4hqib5ji59z87m6dhbqhzg575a22hmrx825n6yk6a")))

(define-public crate-parser_fuck-0.3.1 (c (n "parser_fuck") (v "0.3.1") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0mx56pr7m6m3y2gwlvpi5nz2fqx0hrh7pgzzhr5msfa1pnm6cs83")))

(define-public crate-parser_fuck-0.3.2 (c (n "parser_fuck") (v "0.3.2") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1q4y85ddpzc5bqr1p38cgvfxw436acyv23q53gw4jb6lpfwgv1b5")))

(define-public crate-parser_fuck-0.4.0 (c (n "parser_fuck") (v "0.4.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0z1l3p2bclfry1qj2jpsvf6fmynf64iiilaj9i455i2xksc0di0j")))

(define-public crate-parser_fuck-0.4.1 (c (n "parser_fuck") (v "0.4.1") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0lwsbdp5mfdrpacq9xk1h1vd6rxzrfp7z7rni8mrpw7mgsy4nhqr")))

(define-public crate-parser_fuck-0.5.0 (c (n "parser_fuck") (v "0.5.0") (d (list (d (n "batch_oper") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0y6y6kf4sxk2w2n2wvhpw76ld8whc9mzqrdc8vfbzsz9q9x26p9f")))

(define-public crate-parser_fuck-0.6.0 (c (n "parser_fuck") (v "0.6.0") (d (list (d (n "easybench") (r "^1") (d #t) (k 2)) (d (n "libsugar") (r "^2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^1.5") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "138asb4pfzq0v0ffdxpcpmh6zh3hd9r10wk661prg9fjf08xzn13")))

