(define-module (crates-io pa rs parse_api) #:use-module (crates-io))

(define-public crate-parse_api-0.1.0 (c (n "parse_api") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0f7f4lwmn4ncll0bf686f48768q61hsxyz73223pwgas6863m450")))

(define-public crate-parse_api-0.1.1 (c (n "parse_api") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "14966rm2ydqjrb2iiznbhiklfrbijc6c4pbdwjjl4d4ifa1b756a")))

