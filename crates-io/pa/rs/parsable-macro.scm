(define-module (crates-io pa rs parsable-macro) #:use-module (crates-io))

(define-public crate-parsable-macro-0.1.0 (c (n "parsable-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01y58739pw9f0sz612badfjhlnxcah3rdhrii96v8i64ra7cr2rw")))

(define-public crate-parsable-macro-0.1.1 (c (n "parsable-macro") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04xc7ip8lkv2xc2v696ng30gxz4nwl09h7gfykmrxda5jmzx5i83")))

