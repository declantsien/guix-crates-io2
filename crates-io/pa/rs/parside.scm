(define-module (crates-io pa rs parside) #:use-module (crates-io))

(define-public crate-parside-0.1.1 (c (n "parside") (v "0.1.1") (h "1m400gpla60yja2wgzb983x3ngm3riynvsv9rk4yf53pkqqh4sfy")))

(define-public crate-parside-0.1.3 (c (n "parside") (v "0.1.3") (d (list (d (n "cesride") (r "^0.1.3") (d #t) (k 0)))) (h "13a5jbx4qnjp1ykwbkgxw5g9ildaylcca6rkdxcxvfbk0nj0xdz7")))

(define-public crate-parside-0.2.0 (c (n "parside") (v "0.2.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "cesride") (r "^0.6.0") (d #t) (k 0)) (d (n "nom") (r "~7.1") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "~0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "07li3z9xy34bbbjm8r4m2npk7bx67rpc9xa9w2zm7cxg1f9x5hd6")))

