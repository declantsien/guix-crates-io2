(define-module (crates-io pa rs parser-test) #:use-module (crates-io))

(define-public crate-parser-test-0.1.0 (c (n "parser-test") (v "0.1.0") (h "1kwfpwwhnxbykabpcvc9hf0zsj5rmn7598v45z95ns5ilcjrxynk")))

(define-public crate-parser-test-0.2.0 (c (n "parser-test") (v "0.2.0") (h "1098wzfa25v9cdc3k4fmi4vlr4y4hgj12p3p0vk56lcs73mhvk7k")))

(define-public crate-parser-test-0.3.0 (c (n "parser-test") (v "0.3.0") (h "16pbgizpv0nsiylglkncq9w98fjarfdq3qw19rhqczqdg6xvix7n")))

(define-public crate-parser-test-0.3.1 (c (n "parser-test") (v "0.3.1") (h "16vmgnsnfvlgr6k272c1q98pbjzhar2kk6dz6myz09yff6nyf2hz")))

(define-public crate-parser-test-0.3.2 (c (n "parser-test") (v "0.3.2") (h "0hqni3cws6izcfpydwbnvp2p2rhvbv2d82qz67cxnp7j9mg763dq")))

(define-public crate-parser-test-0.3.3 (c (n "parser-test") (v "0.3.3") (h "19v788y2a9i77a3xzvj0lw12c6y5ja1nyh4i9s5sywqad6rizvav")))

