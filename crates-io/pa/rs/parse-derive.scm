(define-module (crates-io pa rs parse-derive) #:use-module (crates-io))

(define-public crate-parse-derive-0.1.0 (c (n "parse-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "09i57hfbncjb4q5nl84drr3h7p99m2hivblmg6i8a4mf59qa86wr") (y #t)))

