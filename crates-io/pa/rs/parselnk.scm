(define-module (crates-io pa rs parselnk) #:use-module (crates-io))

(define-public crate-parselnk-0.1.0 (c (n "parselnk") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0kqda7yh6zmbn9vplj371agylwi92md5bhb04hqq0zzlkr6j166h") (f (quote (("default" "chrono"))))))

(define-public crate-parselnk-0.1.1 (c (n "parselnk") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1hvdj044h5hhz1503q5idshzicryfks174vvj1wsnlzydrp63200") (f (quote (("default" "chrono"))))))

