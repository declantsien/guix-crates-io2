(define-module (crates-io pa rs parsing) #:use-module (crates-io))

(define-public crate-parsing-0.1.0 (c (n "parsing") (v "0.1.0") (d (list (d (n "grammar-macro") (r "^0.1") (d #t) (k 0)) (d (n "grammar-tech") (r "^0.1") (d #t) (k 0)))) (h "1nmqwsb9vdwcfyixcxj5v90xwfi7396b6bchdxg7vfykiya73ib1") (y #t)))

(define-public crate-parsing-0.1.1 (c (n "parsing") (v "0.1.1") (d (list (d (n "grammar-macro") (r "^0.1") (d #t) (k 0)) (d (n "grammar-tech") (r "^0.1") (d #t) (k 0)))) (h "0vz14zmrrz3m99vsyhwxhbabmylimi3assjg4w45m1njvqyv6kpm") (y #t)))

