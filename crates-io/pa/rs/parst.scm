(define-module (crates-io pa rs parst) #:use-module (crates-io))

(define-public crate-parst-0.1.0 (c (n "parst") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "parst_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gw0gc9bvhdy9nxcm3spp47aba25w3sb9fnrrwn5lv14l43lgc97")))

(define-public crate-parst-0.1.1 (c (n "parst") (v "0.1.1") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "parst_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kfz3d6caykwb4piqr2sjdgdiymqsygmil3xfjlnds1lbdlbwk10")))

(define-public crate-parst-0.1.2 (c (n "parst") (v "0.1.2") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "parst_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09zpw4096bdmssmivi35wnzh9hvfmpzbzv9l33b712gvfczi1q1j")))

(define-public crate-parst-0.1.3 (c (n "parst") (v "0.1.3") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "parst_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lfhy3hyhdc5b2q089a9ckfsx6mb3gfarm40qf301k7ghb1lrfbc") (f (quote (("derive" "parst_derive") ("default" "derive"))))))

(define-public crate-parst-0.1.4 (c (n "parst") (v "0.1.4") (d (list (d (n "parst_derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01b5bnyfsq1417jkwx2i82kh8h4xwjqbfbx3zab1qbaliz26nmp3") (f (quote (("derive" "parst_derive") ("default" "derive"))))))

(define-public crate-parst-0.1.5 (c (n "parst") (v "0.1.5") (d (list (d (n "parst_derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16bpc4m12f7z0fd47mmh24kjdzk89w971mx4lp4ywmrvr7b7k244") (f (quote (("derive" "parst_derive") ("default" "derive"))))))

(define-public crate-parst-0.1.6 (c (n "parst") (v "0.1.6") (d (list (d (n "parst_derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00ck2qy02n8jaz3grvcq6lfc1rhaln51d74nwlxl512prhqjl3hn") (f (quote (("extra") ("derive" "parst_derive") ("default" "derive" "extra"))))))

(define-public crate-parst-0.1.7 (c (n "parst") (v "0.1.7") (d (list (d (n "parst_derive") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09zh9hr9va6zbd3j520ys85bb86ns6c6xjwqs97pbc11zqpkan2h") (f (quote (("extra") ("derive" "parst_derive") ("default" "derive" "extra"))))))

(define-public crate-parst-0.1.8 (c (n "parst") (v "0.1.8") (d (list (d (n "parst_derive") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qxq7sca75clz5rqmvxcnclgx6zgih25b4liqp1gaq9ynqyjw02n") (f (quote (("extra") ("derive" "parst_derive") ("default" "derive" "extra"))))))

(define-public crate-parst-0.1.9 (c (n "parst") (v "0.1.9") (d (list (d (n "parst_derive") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "18bkkqdvwdjlyfk0nnzddl7swyh0g4m3f3pppb1lwmxk4pb95mbq") (f (quote (("extra") ("derive" "parst_derive") ("default" "derive" "extra"))))))

(define-public crate-parst-0.1.10 (c (n "parst") (v "0.1.10") (d (list (d (n "parst_derive") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nx3y3fkrj0641kzp0vf3x592x6a9pd9vd9792kcf5jbn0vfinng") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.11 (c (n "parst") (v "0.1.11") (d (list (d (n "parst_derive") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xsvb1ha8985cdvclh2gryc8c2zmpjy7dqgd5xhzzig2lil778kk") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.12 (c (n "parst") (v "0.1.12") (d (list (d (n "parst_derive") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1bsvg6zfv9175sz0f0sshqrk0w5j31fsrgp6p73hfbf3qmsmw622") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.13 (c (n "parst") (v "0.1.13") (d (list (d (n "parst_derive") (r "^0.1.13") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11rxh2hnx0nr7d3kjcfzj1wh4qqmpmnm75dk7nklwz3vx7k0gvag") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.14 (c (n "parst") (v "0.1.14") (d (list (d (n "parst_derive") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bxla7ga477p9lb2xdyysw6zxf798752828jpzciyhs9vsqy6v2m") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.15 (c (n "parst") (v "0.1.15") (d (list (d (n "parst_derive") (r "^0.1.15") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ldl2aa5gya09h24xfy6cq4p9n7pmwcn31mr35da0jmf980a7h9m") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.16 (c (n "parst") (v "0.1.16") (d (list (d (n "parst_derive") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0y99kxp0rvgnwvp6sqj82ms0lvd6ci0vc10rfq7dffwg3qmyp6fs") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.1.17 (c (n "parst") (v "0.1.17") (d (list (d (n "parst_derive") (r "^0.1.17") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0v3v0glxahac8k208j9a7w32mz9m4vq2a4algf2fw9sbagjd99zq") (f (quote (("extra") ("endian") ("derive" "parst_derive") ("default" "derive" "endian" "extra"))))))

(define-public crate-parst-0.2.0 (c (n "parst") (v "0.2.0") (d (list (d (n "parst_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "18rvmwrzp7nzns3aay7nzkaywigls76q8q1gw1x2mrxnd15rk6sy") (f (quote (("endian") ("derive" "parst_derive") ("default" "derive" "endian"))))))

