(define-module (crates-io pa rs parself) #:use-module (crates-io))

(define-public crate-parself-0.1.0 (c (n "parself") (v "0.1.0") (h "0d01drljxm3fw4ch3wggsp35hg2bdqxizny4m9r6gvbjs4nnj03r") (y #t)))

(define-public crate-parself-0.1.1 (c (n "parself") (v "0.1.1") (h "1v90pg3ickv98ybij7wzm1a4g9gh2svf0sn9cvjkgs6w3380dwb2")))

