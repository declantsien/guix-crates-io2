(define-module (crates-io pa rs parse-fmt-str) #:use-module (crates-io))

(define-public crate-parse-fmt-str-0.1.0 (c (n "parse-fmt-str") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "09lkg9qfqnr26aiy22g63p9wn4yszx74nd2wcsdx4847aj71di0w")))

(define-public crate-parse-fmt-str-1.0.0 (c (n "parse-fmt-str") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "1rkms6vja41i6rqc39q55wfp8p5cbljpciclzqqz1ckkfiixrqmj")))

(define-public crate-parse-fmt-str-2.0.0 (c (n "parse-fmt-str") (v "2.0.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "1k34dimp1nfgk4np66jgnbw6jshqivvmjib8g432m1f29fk1x0bh")))

