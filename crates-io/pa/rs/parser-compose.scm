(define-module (crates-io pa rs parser-compose) #:use-module (crates-io))

(define-public crate-parser-compose-0.1.0 (c (n "parser-compose") (v "0.1.0") (h "1f638k53lxf5475i02bvi3iv3ppcz7y71abis90nc43snyl0r17c")))

(define-public crate-parser-compose-0.2.0 (c (n "parser-compose") (v "0.2.0") (h "0gy7w6fpk0zydyyhcn8lglirx6m04sga30svzq9886gar0vsgf01")))

(define-public crate-parser-compose-0.3.0 (c (n "parser-compose") (v "0.3.0") (h "11wbfh2kkq5varmdm8cq1li08jgbjrsvni193y7529yxpdd7a9v8")))

(define-public crate-parser-compose-0.4.0 (c (n "parser-compose") (v "0.4.0") (h "1vgny24bxqm01ilsgw1y86hcq6c88zhbqhnzaw4h54d74l6gdrqj")))

(define-public crate-parser-compose-0.5.0 (c (n "parser-compose") (v "0.5.0") (h "050pm3rifhi0swgw7azdq757rchhxfhm125hpxpjbg4dnmg2whrj")))

(define-public crate-parser-compose-0.6.0 (c (n "parser-compose") (v "0.6.0") (h "0phv0lm9s50rzisc1bgpmjvn33sw9ljwyi5b0icxsk680q4vbmjj")))

(define-public crate-parser-compose-0.7.0 (c (n "parser-compose") (v "0.7.0") (h "0lp4hvgv5xj5h60b7z7whxympq518jgsv2jlhrb4x77qgh8va91g")))

(define-public crate-parser-compose-0.8.0 (c (n "parser-compose") (v "0.8.0") (h "11dzhqsmn79lv691nqjabcl0fwj3nkjlzvc8g4vcfd2dhs31lh1y")))

(define-public crate-parser-compose-0.9.0 (c (n "parser-compose") (v "0.9.0") (h "0kn4pm54i2jgdyvcm4xyrifybnag4bxl0i4b56d8rmyhcm41bq5g")))

(define-public crate-parser-compose-0.10.0 (c (n "parser-compose") (v "0.10.0") (h "1pm4gab01l1vmg9r8zc8qrs48lsbp46109jpgq9i1a9ifi8ph2gq")))

(define-public crate-parser-compose-0.11.0 (c (n "parser-compose") (v "0.11.0") (h "02wishp635j95vxdfq84926db1sq2n2wjydfhci49g2az22hrn6q")))

(define-public crate-parser-compose-0.12.0 (c (n "parser-compose") (v "0.12.0") (h "0k14kwj6ip7g88fjk6jypw97yblsc92dja6j7q95mmrj4njw95ha")))

(define-public crate-parser-compose-0.13.0 (c (n "parser-compose") (v "0.13.0") (h "0xw5vnzc69h06kdpdfn71y2kr2d4dbln7f1gn1c7b4axx1mjyyr1")))

(define-public crate-parser-compose-0.14.0 (c (n "parser-compose") (v "0.14.0") (h "12rml09znghfpl6l5xqplpxf3g3798m4n4khaml9l8w3p5fd8gc5")))

(define-public crate-parser-compose-0.15.0 (c (n "parser-compose") (v "0.15.0") (h "02mlkdmlly9mj592jgpklgvl9kkh9rxpws7alqnpq64b029c3a2z")))

(define-public crate-parser-compose-0.16.0 (c (n "parser-compose") (v "0.16.0") (h "07d3dvmq43kfwm8irym4sssqdisxxwc833rdjncwswwd9xhnvr7c")))

(define-public crate-parser-compose-0.17.0 (c (n "parser-compose") (v "0.17.0") (h "133m8q7qgm668zl2bly5s16iyrs5j6nyf9dcr9md5igv3dgd67wd")))

(define-public crate-parser-compose-0.18.0 (c (n "parser-compose") (v "0.18.0") (h "0awiap4v53wypayilynibn4ql8hhgnfjjp24xfd0nf4vd2iakbya")))

(define-public crate-parser-compose-0.19.0 (c (n "parser-compose") (v "0.19.0") (h "08v1wlcnz84jsp8ac4hi9637jibanq0pvi2mn60fyfq8k3gr7j69")))

