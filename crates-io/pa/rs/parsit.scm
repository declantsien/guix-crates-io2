(define-module (crates-io pa rs parsit) #:use-module (crates-io))

(define-public crate-parsit-0.1.0 (c (n "parsit") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0ryvc6ajp3xdmsdjkd3vqhzns9kwpf85b14zdpx0kkc2cxrzzlg8")))

(define-public crate-parsit-0.1.1 (c (n "parsit") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "1gb7az7iiggrn538a8x2q5hs9ygijxs9m3qyh9mfmqyjfn8nx88w")))

(define-public crate-parsit-0.1.2 (c (n "parsit") (v "0.1.2") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "05mhgk6cwj59ja619hwjsn87a9banknks4fkg2nmfwhmza0nh6zz")))

(define-public crate-parsit-0.1.3 (c (n "parsit") (v "0.1.3") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0acfsrsrqlk1f9jqxid767pxn8ggvyxsc8z8bhjh6nd2743rhrg7")))

(define-public crate-parsit-0.1.4 (c (n "parsit") (v "0.1.4") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0g6ddijs5rpmx14ibw1pa2yapanvz03bycw63l9v65mgxkr2b66r")))

(define-public crate-parsit-0.1.5 (c (n "parsit") (v "0.1.5") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0ymw9n28li3fnrbf60v9hzj4sn2v9ybizznc6ad1fa3rchya4f3k")))

(define-public crate-parsit-0.1.6 (c (n "parsit") (v "0.1.6") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "13hwlll819kxhkfz1pxfg0ybf3qdaqy59rqhcf46viwx7lismmck")))

(define-public crate-parsit-0.1.7 (c (n "parsit") (v "0.1.7") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "08bmxxj75f01djyx04wqa388h1lskhmama0k23824f0j00ilaid3")))

(define-public crate-parsit-0.1.8 (c (n "parsit") (v "0.1.8") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0wivawkckx1n4h4dfh2dvv6d4qnyxgzjcy0mmlswmv4fisa0wi55")))

(define-public crate-parsit-0.1.9 (c (n "parsit") (v "0.1.9") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "090arjgz5zh1axflg11f1x4nb6wk47f43pi2masnrg85s2py5wvk")))

(define-public crate-parsit-0.1.10 (c (n "parsit") (v "0.1.10") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "1gpmjp694xldncs13zqfdrr7ixizx8k4v9gdp7yjnwfh9qlcn9i2")))

(define-public crate-parsit-0.1.11 (c (n "parsit") (v "0.1.11") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "02p26ycdrn4i3i6vg2g9ragyqknp44jrlys6nm542bkhlpfkxjvq")))

(define-public crate-parsit-0.1.12 (c (n "parsit") (v "0.1.12") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)))) (h "0lwh9dpzyianmqjhipiwamxafcq37fksva597656fy6njny23hn7")))

(define-public crate-parsit-0.1.13 (c (n "parsit") (v "0.1.13") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "logos-derive") (r "^0.13.0") (d #t) (k 0)))) (h "1h32jycrd0ihbrgy0wh11xb9r1cj0rvjdsxlnc6f63zv6dj23p46")))

(define-public crate-parsit-0.1.14 (c (n "parsit") (v "0.1.14") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "logos-derive") (r "^0.13.0") (d #t) (k 0)))) (h "1gjixfa33fgm06xls9qzq23gr6aqzx2akxx4klpnqa2w8d1yjw6p")))

(define-public crate-parsit-0.1.15 (c (n "parsit") (v "0.1.15") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "logos-derive") (r "^0.13.0") (d #t) (k 0)))) (h "0m81sph2yy4hzigb9fjkrvsv99nrv9c559wi9gk870c29a7jfpwy")))

(define-public crate-parsit-0.2.0 (c (n "parsit") (v "0.2.0") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "logos-derive") (r "^0.13.0") (d #t) (k 0)))) (h "0p02igmw0z8fgkq213vqb49vfcli1glid1yign0yka7p0bxk32am")))

