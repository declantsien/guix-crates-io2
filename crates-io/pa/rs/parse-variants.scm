(define-module (crates-io pa rs parse-variants) #:use-module (crates-io))

(define-public crate-parse-variants-0.1.0 (c (n "parse-variants") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "parse-variants-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0k1k7gqqvayvvb2rynwx7xxvv6dk5v31aglwg7nvanypfqnvkwgs")))

(define-public crate-parse-variants-0.1.1 (c (n "parse-variants") (v "0.1.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "parse-variants-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "08841siwk1qvw4ifch640nr13i6fhqrz92nxdmpghfa6hpgay3gv")))

(define-public crate-parse-variants-1.0.0 (c (n "parse-variants") (v "1.0.0") (d (list (d (n "parse-variants-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1jh38mnr92g0qklc3fz39s9nmjc4nv554bsqi7yj5k7v9bbrfh8f")))

(define-public crate-parse-variants-1.0.1 (c (n "parse-variants") (v "1.0.1") (d (list (d (n "parse-variants-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1qybi30ph1awjjnvg0icv4l9swqjl0vrqag4q1gf3aj60q8liw40")))

