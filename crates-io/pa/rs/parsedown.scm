(define-module (crates-io pa rs parsedown) #:use-module (crates-io))

(define-public crate-parsedown-0.1.1 (c (n "parsedown") (v "0.1.1") (h "0xkk9l2qjl92n9xrgx8rndcyy6jc4ssy3n8642vkj7q7kbcq2ma9")))

(define-public crate-parsedown-0.1.2 (c (n "parsedown") (v "0.1.2") (h "1aj85kl2balnh7m8w3xxwym5mahmlwdanbklmgc4gcaqa5gjl4rs")))

