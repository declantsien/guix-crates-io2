(define-module (crates-io pa rs parser-haskell) #:use-module (crates-io))

(define-public crate-parser-haskell-0.1.0 (c (n "parser-haskell") (v "0.1.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.12.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.5") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0n5vkxdbq3wwpjwbfnkr580x7dqqgyab9bkw8fvq04mvlf746l6p")))

(define-public crate-parser-haskell-0.2.0 (c (n "parser-haskell") (v "0.2.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "errln") (r "^0.1.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.12.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.5") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1fqz5v9py7x80kw9cgyq5jhs454icd8sw3f2ni9yjy309z79syb4")))

