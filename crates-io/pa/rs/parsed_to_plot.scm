(define-module (crates-io pa rs parsed_to_plot) #:use-module (crates-io))

(define-public crate-parsed_to_plot-0.1.0 (c (n "parsed_to_plot") (v "0.1.0") (d (list (d (n "id_tree") (r "^1.8.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1fmvhim3rirpnfsd0np47v99p615i52dnb9m3rq7v3fmhbfyjylg")))

(define-public crate-parsed_to_plot-0.1.1 (c (n "parsed_to_plot") (v "0.1.1") (d (list (d (n "id_tree") (r "^1.8.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1460hagvb4l7pbc7z4hgk9lizm4w8a7zf7ayriprmxp090jaq74j")))

(define-public crate-parsed_to_plot-0.2.0 (c (n "parsed_to_plot") (v "0.2.0") (d (list (d (n "id_tree") (r "^1.8.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0myn4054c1h1lcllkvnc9d7rcf0m2i30nwlpa4m7xmnhpyaxsz4d")))

