(define-module (crates-io pa rs parsys) #:use-module (crates-io))

(define-public crate-parsys-0.1.0 (c (n "parsys") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "0bjbnfys44l5vmpkzyk7rklnfvy9fw3q1kwl955fpqmr03kdkyq7")))

(define-public crate-parsys-0.2.0 (c (n "parsys") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "02f7gggzccdqfhx0k4v3jn2i5d0w92wqn7sq8qkhjv5c5jp5xll2")))

