(define-module (crates-io pa rs parser-derive) #:use-module (crates-io))

(define-public crate-parser-derive-0.1.0 (c (n "parser-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1k6g7prdcg0bn1j288yrmfflfmajr646aypgnnvgmavrq6d6c5wb")))

