(define-module (crates-io pa rs parse_interval) #:use-module (crates-io))

(define-public crate-parse_interval-0.1.0 (c (n "parse_interval") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (f (quote ("std" "perf" "unicode-case"))) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1zl6nspsr2628n4vg0hip3kfvbvm53kh3bccdcc8x1kzd5mas391")))

(define-public crate-parse_interval-0.2.0 (c (n "parse_interval") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (f (quote ("std" "perf" "unicode-case"))) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0avl3krmsp1dahq2iw811sqjzq519rlb2gnbviz8vry2i32yiay9")))

