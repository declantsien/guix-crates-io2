(define-module (crates-io pa rs parsing-macro) #:use-module (crates-io))

(define-public crate-parsing-macro-0.1.0 (c (n "parsing-macro") (v "0.1.0") (d (list (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 1)) (d (n "parsing-tech") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ar7q9v8wn7nzlxwm9bg6ic6vrb1aix39wfr0jjwlkcvwz0kwqys") (y #t)))

