(define-module (crates-io pa rs parstream) #:use-module (crates-io))

(define-public crate-parstream-0.1.0 (c (n "parstream") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)))) (h "1wdbslvb6fz9swn35bbdbiknbrc582wykk3v45hwdd2f4gnlqfsf")))

(define-public crate-parstream-0.2.0 (c (n "parstream") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)))) (h "0abv1zxwsfl0gfcfl4ln6hprqi69c8cp3pc7gc1zx3nq30fh8yfz")))

(define-public crate-parstream-0.2.1 (c (n "parstream") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)))) (h "0zzmzc6y25jq8ynwy8lnisq1xs80h2kwc5khsihc06pz58zi1aj4")))

