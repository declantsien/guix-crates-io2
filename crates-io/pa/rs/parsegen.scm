(define-module (crates-io pa rs parsegen) #:use-module (crates-io))

(define-public crate-parsegen-0.1.0 (c (n "parsegen") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "lexgen") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1629lr30zv9qyl8fnx33axvi120bsqa3fd0m3m7m4g5zz99rk9wp")))

(define-public crate-parsegen-0.1.1 (c (n "parsegen") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "lexgen") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "08rgia92fxvp7aljy7hjf43103z5df4kfamazaaagrzdhqx68z0i")))

