(define-module (crates-io pa rs parse_list) #:use-module (crates-io))

(define-public crate-parse_list-1.0.0 (c (n "parse_list") (v "1.0.0") (d (list (d (n "big_s") (r "^1.0.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1888yim6j5zcdrsqzqh95iqx1hnlx95r8zn33wbf50zb54djvznh")))

(define-public crate-parse_list-2.0.0 (c (n "parse_list") (v "2.0.0") (d (list (d (n "big_s") (r "^1.0.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0ahjdq67w2sqk1kqdjy2z82a51xagk9h03r8kf1dcfghq3w8m7c8")))

