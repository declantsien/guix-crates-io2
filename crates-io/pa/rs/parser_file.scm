(define-module (crates-io pa rs parser_file) #:use-module (crates-io))

(define-public crate-parser_file-0.1.0 (c (n "parser_file") (v "0.1.0") (h "0grr3s56skh2ivda2zwpdgnlpqmaza4a481xd3bw5p1wj9ffbzzc")))

(define-public crate-parser_file-0.2.0 (c (n "parser_file") (v "0.2.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1q53hd2vi2ad2gh027imn7l8q7x2zxgdx8fr0yd7kbylnhyg0992")))

(define-public crate-parser_file-0.2.1 (c (n "parser_file") (v "0.2.1") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1wvv4fdppj13xxgi4m70jwy29sm24w4r0r50imcjr6ka7d814ipg")))

