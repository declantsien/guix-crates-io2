(define-module (crates-io pa rs parse_mediawiki_dump_reboot) #:use-module (crates-io))

(define-public crate-parse_mediawiki_dump_reboot-1.0.0 (c (n "parse_mediawiki_dump_reboot") (v "1.0.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 2)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sdbvg54km1sh7raw61rywk10r7sdibmdr88hbndg4nqs6wrwz4s")))

(define-public crate-parse_mediawiki_dump_reboot-1.0.1 (c (n "parse_mediawiki_dump_reboot") (v "1.0.1") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 2)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hgkzwlvnmdqbc50c34rl07nag0i96yhndq99xq1mvyz0z7qzd86")))

