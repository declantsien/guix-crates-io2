(define-module (crates-io pa rs parse-up) #:use-module (crates-io))

(define-public crate-parse-up-0.1.0 (c (n "parse-up") (v "0.1.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0n60d935vbffkq70fv35lxf4cmn2dpxq231kh4s8mhrspfgmi5yq")))

