(define-module (crates-io pa rs parse_wiki_text) #:use-module (crates-io))

(define-public crate-parse_wiki_text-0.1.0 (c (n "parse_wiki_text") (v "0.1.0") (h "17hcqk2dqzp2cy4fql9r2ibwj4k1ygf5cx2xhkc7p7acxl0xjgqm")))

(define-public crate-parse_wiki_text-0.1.1 (c (n "parse_wiki_text") (v "0.1.1") (h "1zd8m6pp73pc46iifll4wr00hmwr4araa7g7xqcj2h4mzmn121zx")))

(define-public crate-parse_wiki_text-0.1.2 (c (n "parse_wiki_text") (v "0.1.2") (h "0awg05hjqy8swrhik89qp7qn2yn75icrjm93q6s8zibwq416gk9p")))

(define-public crate-parse_wiki_text-0.1.3 (c (n "parse_wiki_text") (v "0.1.3") (h "1znb1qa4bjz0wrg6xn8nckbw4i6v0f94568llwwzvwqbxfdm01xj")))

(define-public crate-parse_wiki_text-0.1.4 (c (n "parse_wiki_text") (v "0.1.4") (h "1wb858w5fmg44zq3wvg1qsxa9y3mkklfajpx2ddx38bjsyd4s9ma")))

(define-public crate-parse_wiki_text-0.1.5 (c (n "parse_wiki_text") (v "0.1.5") (h "1pbif5qb8q1vlk5nnb09a3b552f2skcjyin9ffyh5ylfqm2s4nfx")))

