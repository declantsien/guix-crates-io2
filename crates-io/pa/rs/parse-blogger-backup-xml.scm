(define-module (crates-io pa rs parse-blogger-backup-xml) #:use-module (crates-io))

(define-public crate-parse-blogger-backup-xml-0.1.2 (c (n "parse-blogger-backup-xml") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "14ir9y135vq751pk5v9x3mf8ws3bl3p5ymgp5fl4brsdnw4y8kc3")))

(define-public crate-parse-blogger-backup-xml-0.1.3 (c (n "parse-blogger-backup-xml") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "070ymrfaa6pnb42pwdk28rzw7had3qb7gglcr1yq4mvrnqib395r")))

