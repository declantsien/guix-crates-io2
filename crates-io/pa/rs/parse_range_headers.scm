(define-module (crates-io pa rs parse_range_headers) #:use-module (crates-io))

(define-public crate-parse_range_headers-0.1.0 (c (n "parse_range_headers") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "0ynh8mhb3jay5ffqpvd3rgyhbbxhaaqzy7qc4ycvcj5jb2rr8x07") (f (quote (("with_error_cause")))) (y #t)))

