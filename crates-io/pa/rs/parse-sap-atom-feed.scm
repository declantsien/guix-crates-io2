(define-module (crates-io pa rs parse-sap-atom-feed) #:use-module (crates-io))

(define-public crate-parse-sap-atom-feed-0.1.0 (c (n "parse-sap-atom-feed") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "150lcj6y46iksnrcgrqf9aw3686bkyz74lxc7cwsl7hg2jmy1768")))

(define-public crate-parse-sap-atom-feed-0.1.1 (c (n "parse-sap-atom-feed") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k4v9lz1b2rip983gf3m4zrcm74snr98vdn1z9i93zpaivarc96c")))

(define-public crate-parse-sap-atom-feed-0.1.2 (c (n "parse-sap-atom-feed") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kqsfnjzzslxly27xhrjasmyvmqi1l11q9q743dwb8s9vilf35d8")))

(define-public crate-parse-sap-atom-feed-0.1.3 (c (n "parse-sap-atom-feed") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ipxs5r2gwhngbyqk8a34ck2889sn4mi9537xgs6n5ndlgm0y7a1")))

(define-public crate-parse-sap-atom-feed-0.1.4 (c (n "parse-sap-atom-feed") (v "0.1.4") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wb4bmjidscgwn6zk9zxk6f5nmk07k5amkjwy1riq5vbml7hb439")))

(define-public crate-parse-sap-atom-feed-0.1.5 (c (n "parse-sap-atom-feed") (v "0.1.5") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03cx0jcf7850wdxnfy156b007i6hj9g7caxjzvjjgfa77mlvklcj")))

(define-public crate-parse-sap-atom-feed-0.1.6 (c (n "parse-sap-atom-feed") (v "0.1.6") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pwq1acy8gj58zvwr48v4klkxlg8qfq2cs0h2zxd2cidk4qqy00g")))

(define-public crate-parse-sap-atom-feed-0.1.7 (c (n "parse-sap-atom-feed") (v "0.1.7") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x7fw1cxqnmjb85691smmkbn1sywkz2kbvg89arz9mchx0i1lzdx")))

(define-public crate-parse-sap-atom-feed-0.1.8 (c (n "parse-sap-atom-feed") (v "0.1.8") (d (list (d (n "quick-xml") (r "^0.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sk8jyiv073saqdjw8rgkd53bs5vc9w8cqnjjhiyw8na8j944hxf")))

(define-public crate-parse-sap-atom-feed-0.2.0 (c (n "parse-sap-atom-feed") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ascg3bfrrxip7k9xrhi82fz4xvbwzwc1ja2r0ayvlg2xs0haay8")))

(define-public crate-parse-sap-atom-feed-0.2.1 (c (n "parse-sap-atom-feed") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14x3v3g2i9krasy2m8l90ghmbn4bp2xs663mpqsfr1pzp5lxvq8x")))

(define-public crate-parse-sap-atom-feed-0.2.2 (c (n "parse-sap-atom-feed") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aqvhvfx55srqg8yv21zq5dfcjzpqd1vk15s66zc4zfxp2h30a6b")))

(define-public crate-parse-sap-atom-feed-0.2.3 (c (n "parse-sap-atom-feed") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n6kxr3yq8lasfn934608zc7708zyxyqwdgh5l98par5xi00w8rz")))

