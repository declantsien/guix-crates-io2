(define-module (crates-io pa rs parsed) #:use-module (crates-io))

(define-public crate-parsed-0.1.4 (c (n "parsed") (v "0.1.4") (h "0in7ay28wf0nrwd3yfvw0r2hkg3hhsgbmwqjzrlmwgj0qrdjgi6c")))

(define-public crate-parsed-0.1.5 (c (n "parsed") (v "0.1.5") (h "1qk6998a9l56cq09pjavy9k12j95m98m6nylm19j3ybrmy2wwj90")))

(define-public crate-parsed-0.1.6 (c (n "parsed") (v "0.1.6") (h "19nlgrjwfhjpvr94hnj0nvnaldgxgdpk9ad46b8lgnb7liifh872")))

(define-public crate-parsed-0.2.0 (c (n "parsed") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "06v6wf187yhmq9x5q0mframr5pf4khbghhm5zzrpqivwrd8hcw2c")))

(define-public crate-parsed-0.2.1 (c (n "parsed") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "1m25wgzz06dacy7qzq7lzl9sh3faymj9vvw3xq07laar9x819j63")))

(define-public crate-parsed-0.3.0 (c (n "parsed") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "00bkj717sn680943cfcl912694c70hkfi9gmsari0sxc3n6c7dh0") (f (quote (("http") ("default"))))))

