(define-module (crates-io pa ge pagerduty) #:use-module (crates-io))

(define-public crate-pagerduty-0.1.0 (c (n "pagerduty") (v "0.1.0") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "0ac0a2jb6aq50ij0m422l7y0mrl1ahkx2m6nc935mggdcdlvlf47") (f (quote (("live_tests"))))))

(define-public crate-pagerduty-0.1.1 (c (n "pagerduty") (v "0.1.1") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1i0fzd8llknvwll7vhbzhs49anq9hpfdp7bvcsss4iji2gflqzcx") (f (quote (("live_tests"))))))

(define-public crate-pagerduty-0.2.0 (c (n "pagerduty") (v "0.2.0") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1iksx3bb6rp2x8c0nfdmchjph5wr31lrv6h9gs2bh9xv59d9zrjf")))

