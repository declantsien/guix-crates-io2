(define-module (crates-io pa ge pages) #:use-module (crates-io))

(define-public crate-pages-0.1.0 (c (n "pages") (v "0.1.0") (h "1jk8x74l5gab2mkdm9gb94k5mw341314dmm1876zwana0qchjsmw")))

(define-public crate-pages-0.1.1 (c (n "pages") (v "0.1.1") (h "03yjazfpfr4clnvf624vh88ljhs17n1pnvdd9f7wfdjqwwcphasi")))

(define-public crate-pages-0.2.0 (c (n "pages") (v "0.2.0") (h "1snlj0w51gk8ll35abhrhyv9vcbz7337lkgkb3rqx2x26nb9r7ra")))

