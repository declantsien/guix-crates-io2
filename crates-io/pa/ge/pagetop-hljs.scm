(define-module (crates-io pa ge pagetop-hljs) #:use-module (crates-io))

(define-public crate-pagetop-hljs-0.0.1 (c (n "pagetop-hljs") (v "0.0.1") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1m7irznmxlkvjxkdfqbwhsiclyrn0fha8zbwabgfilgqkhjk2sv3")))

(define-public crate-pagetop-hljs-0.0.2 (c (n "pagetop-hljs") (v "0.0.2") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "17n88rzd01sfkszvqrx6bk1ifb5ci4znvng5qlzwp4c35mjsjkyj")))

(define-public crate-pagetop-hljs-0.0.3 (c (n "pagetop-hljs") (v "0.0.3") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0bjxc5cvcd66b5v56nps49ii6k1bjycgnk9m9ixa8chrxs7430yc")))

(define-public crate-pagetop-hljs-0.0.4 (c (n "pagetop-hljs") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0131mb8gvi9p5wh6za4y7rpqikyf8icssdi1fii1dj0vq1rg5ycp")))

(define-public crate-pagetop-hljs-0.0.6 (c (n "pagetop-hljs") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1fpg4p29vbsq2zc7xqpc8gcn6axvkwm0pk4ji0v73dvjh6xph2xl")))

(define-public crate-pagetop-hljs-0.0.7 (c (n "pagetop-hljs") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1l9rfy8m2404j2dxjk9wlan4mws7zx3dwbiag8dwrjvjpp2hgfj1")))

(define-public crate-pagetop-hljs-0.0.8 (c (n "pagetop-hljs") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1yzlj7xmqdg19jmas47as5fvrikxi1mk6ndhyfaj84ipnazw6zc8")))

(define-public crate-pagetop-hljs-0.0.9 (c (n "pagetop-hljs") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0asmqm2kbm7fm102ba2pn3hp75p99f8yyjqb5hqy665rsrbmkrxw")))

(define-public crate-pagetop-hljs-0.0.11 (c (n "pagetop-hljs") (v "0.0.11") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0rrzj0zhqwrwfcq3nhvwa3jbgcysa9ffkkg00avk0mssawb9r573")))

(define-public crate-pagetop-hljs-0.0.12 (c (n "pagetop-hljs") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "16c4psdfjbz1a97kw5i2z0w929gcil6drzlzw4w0dsn9fmj3la54")))

(define-public crate-pagetop-hljs-0.0.13 (c (n "pagetop-hljs") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0414lirqlalvpx9kp16q00f9kz874amhsr3mk5fqkwa63xqy8vi6")))

