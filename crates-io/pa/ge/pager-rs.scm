(define-module (crates-io pa ge pager-rs) #:use-module (crates-io))

(define-public crate-pager-rs-0.1.0 (c (n "pager-rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "08c52bhvwbkkj30hdd56f9y24rfrsczg4df1gl7mqk5p66vqhdvp")))

(define-public crate-pager-rs-0.2.0 (c (n "pager-rs") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "06v0wp7j3f5j3issxql3ij694gwhn52v3csgz20ldp5rlbf4w1qm")))

(define-public crate-pager-rs-0.2.1 (c (n "pager-rs") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0rg6zbf3pkv1d6zd63l60hh512zfz9zmzd3y4cwncx86igdc7mb1")))

(define-public crate-pager-rs-0.2.3 (c (n "pager-rs") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0iz477ncp582rdj5yg3k1w5fq5v7mdaryk6dc1hgvnqwzihgh5b4")))

(define-public crate-pager-rs-0.3.0 (c (n "pager-rs") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "04fsj2hh4yy07wyqlv8cy94q5djcpj6q3n006ighr5wr265j422k")))

