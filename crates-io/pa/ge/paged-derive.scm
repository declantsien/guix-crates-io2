(define-module (crates-io pa ge paged-derive) #:use-module (crates-io))

(define-public crate-paged-derive-0.1.0 (c (n "paged-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1q28ls3bjbcwfxk2idckvlr567hpp4p9npwc3w09bjhk17lb6la8")))

(define-public crate-paged-derive-0.1.1 (c (n "paged-derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "13kvg9ap1x1fl2pqsk1hws66bd1lpw09hn3bsas7ndknk38iajgj")))

