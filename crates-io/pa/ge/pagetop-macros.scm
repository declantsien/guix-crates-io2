(define-module (crates-io pa ge pagetop-macros) #:use-module (crates-io))

(define-public crate-pagetop-macros-0.0.1 (c (n "pagetop-macros") (v "0.0.1") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ixxv420wxi20gssq8v9f5kzmmssxbk65r9q3ac03f509rkmadri")))

(define-public crate-pagetop-macros-0.0.2 (c (n "pagetop-macros") (v "0.0.2") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnp5fdlij6k4qgm2ghljrnqjx1ha1ml61ckkm2fhqwq6sa2rc0v")))

(define-public crate-pagetop-macros-0.0.3 (c (n "pagetop-macros") (v "0.0.3") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd1zq6qkar3cm9qcpjhl2pwswsijyq8m547yhkfa32r9r2laf2s")))

(define-public crate-pagetop-macros-0.0.4 (c (n "pagetop-macros") (v "0.0.4") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03003h79dfps4qnl53m0f4jhkcq2by3d7sv3j7qkhingis7994rv")))

(define-public crate-pagetop-macros-0.0.5 (c (n "pagetop-macros") (v "0.0.5") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fc5s32b11xx4zxb47yv3hxy6vbgxggcs26w8xihl7qj5y8v3j6m")))

(define-public crate-pagetop-macros-0.0.6 (c (n "pagetop-macros") (v "0.0.6") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08v73jswxd7hb64f2s7a3pxsjppf1mjyxkxf0ipbnykbd0s5qvrx")))

(define-public crate-pagetop-macros-0.0.7 (c (n "pagetop-macros") (v "0.0.7") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qqvdkqmd662ifsvd51gwcjv8jczr5q2fsyq5yzwgdb7pvi591kh")))

(define-public crate-pagetop-macros-0.0.8 (c (n "pagetop-macros") (v "0.0.8") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14lyg3inbfparr61nvandlj1fk6z4983bws7lhxgivamqxkns9jd")))

(define-public crate-pagetop-macros-0.0.9 (c (n "pagetop-macros") (v "0.0.9") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k5jll9bxnykdv8v9v4kpix72cn1gk2zyb89d2i5vxvy4nhk7yn9")))

(define-public crate-pagetop-macros-0.0.10 (c (n "pagetop-macros") (v "0.0.10") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18g1p3q59na0ryv4l3xw2wbgm2wm6nykglrrh6nfjlcd717rli2x")))

(define-public crate-pagetop-macros-0.0.11 (c (n "pagetop-macros") (v "0.0.11") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03mhlawdl45f7l07ivcckcw7a9c29350b2vq56hfh9gfg0j14g5z")))

(define-public crate-pagetop-macros-0.0.12 (c (n "pagetop-macros") (v "0.0.12") (d (list (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gn7rpyracdw9s2flz81v2bz86a7lj0xwxjg5qa7xlly3f7l270v")))

