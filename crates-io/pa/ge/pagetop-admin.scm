(define-module (crates-io pa ge pagetop-admin) #:use-module (crates-io))

(define-public crate-pagetop-admin-0.0.1 (c (n "pagetop-admin") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0dqi0sqfrxgcfx6fsxccsc5m8p34kh5g1a8mskdybx4wb3zirggg")))

(define-public crate-pagetop-admin-0.0.2 (c (n "pagetop-admin") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0xkr7bfw697xq2byx623fdk681ni0kyf9ysdnym9956gpv88zhpq")))

(define-public crate-pagetop-admin-0.0.3 (c (n "pagetop-admin") (v "0.0.3") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "11sh40l1h7dgf36ni6rcal3xw5yc7xa2i8b680lwn6x859as7y7z")))

(define-public crate-pagetop-admin-0.0.4 (c (n "pagetop-admin") (v "0.0.4") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0hgd6a00mx6mw6l4kka3nm7kkysd4d5ndvw2d3cmq14wb6vip223")))

(define-public crate-pagetop-admin-0.0.5 (c (n "pagetop-admin") (v "0.0.5") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)))) (h "11927qgvj8vx3hbwrcmhrhzywyh15apbniqa2rps3xhkm217a5k3")))

(define-public crate-pagetop-admin-0.0.6 (c (n "pagetop-admin") (v "0.0.6") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)))) (h "13v8psb94r3244ly2bc8xwwhywkg904di5l0s4ddrm0z20ip2v44")))

(define-public crate-pagetop-admin-0.0.7 (c (n "pagetop-admin") (v "0.0.7") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "139kdb1q9sshqd5csmxmgsmf1ikb4p761x1yi8n0k6fk02nsk858")))

(define-public crate-pagetop-admin-0.0.8 (c (n "pagetop-admin") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "0gy26zl1mq109lcg6h37jsbwb5fimkzgnqwld284m2j7y8x69ch2")))

(define-public crate-pagetop-admin-0.0.9 (c (n "pagetop-admin") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "0zk7i6i8rv8dlpaq68wyjy5q82mi6fm7aa1aswxywzbzxarbsc3s")))

(define-public crate-pagetop-admin-0.0.10 (c (n "pagetop-admin") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "1j0jl6r49dk5xrsvmc926rhmlngfgd3ld6na41r2mbcnj8dankz1")))

(define-public crate-pagetop-admin-0.0.11 (c (n "pagetop-admin") (v "0.0.11") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "0qr1qq9s5p885dvg9940ijk1vad6bkjkq722y6v4k4sx3wb3h52k")))

(define-public crate-pagetop-admin-0.0.12 (c (n "pagetop-admin") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "0n6ym99fygb103hqgz5fgdx07ngg14pip41g92gd8p51yrvk5bl5")))

(define-public crate-pagetop-admin-0.0.13 (c (n "pagetop-admin") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-megamenu") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)))) (h "16l8ygjx932mm6jj9c6ly0r1k131mf1khgdx6krk8pxka6fjg996")))

(define-public crate-pagetop-admin-0.0.14 (c (n "pagetop-admin") (v "0.0.14") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "1b985nv27zv12wb03bc7iwn73p45rrfay92r41prbanjx61637yq")))

(define-public crate-pagetop-admin-0.0.15 (c (n "pagetop-admin") (v "0.0.15") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0rhixfijm1f5axykq71jbf2n4b2xd28cni2m7cm53ps1li3q7b3j")))

(define-public crate-pagetop-admin-0.0.16 (c (n "pagetop-admin") (v "0.0.16") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "19kmipsz8b4jvj4ard0xrfcr9jkfap1kfycq4wg50dmsxv4smwm9")))

(define-public crate-pagetop-admin-0.0.17 (c (n "pagetop-admin") (v "0.0.17") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0mrk1f7isbzrn6r49lbg3vx31993gb2q62x8xf8pkbj49ija41z2")))

(define-public crate-pagetop-admin-0.0.18 (c (n "pagetop-admin") (v "0.0.18") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0flxxi6a1gyfyr7pigi2h767vyh9j7vwplfx3b1s545pna1xiwmv")))

(define-public crate-pagetop-admin-0.0.19 (c (n "pagetop-admin") (v "0.0.19") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "08hj3rgn9ffijkmd7rzcbdvrppk8q74qv9bmmsg3ckxhxnf43kb2")))

(define-public crate-pagetop-admin-0.0.20 (c (n "pagetop-admin") (v "0.0.20") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "03bpwizawnk9mi0zbqymsfrwyrczr4az9wwzi222sns4gb2c9w3m")))

(define-public crate-pagetop-admin-0.0.21 (c (n "pagetop-admin") (v "0.0.21") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "1yy5883r32av1qzl6jazmr2c8hd9hbm6rfnkqilbdk4vxbymfmmk")))

