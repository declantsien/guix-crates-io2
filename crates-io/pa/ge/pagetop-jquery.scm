(define-module (crates-io pa ge pagetop-jquery) #:use-module (crates-io))

(define-public crate-pagetop-jquery-0.0.1 (c (n "pagetop-jquery") (v "0.0.1") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0ly87siifjzlk8454209jvqs5d50ahif8sgr0swxslllb7h6gd8g")))

(define-public crate-pagetop-jquery-0.0.2 (c (n "pagetop-jquery") (v "0.0.2") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0dsvx6nway4csdvbbldnvx2pz3s4iiaxvy9vv1gg0ab2cmk4if45")))

(define-public crate-pagetop-jquery-0.0.3 (c (n "pagetop-jquery") (v "0.0.3") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1yqjj4qd9zzngg1w59shnzffszgv1zbw7ps99nddf93faprpinvs")))

(define-public crate-pagetop-jquery-0.0.4 (c (n "pagetop-jquery") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "04myiw7vw3cdzvzafps5rm3qby6z441qr1rq4b4y2wh625hwnjr6")))

(define-public crate-pagetop-jquery-0.0.5 (c (n "pagetop-jquery") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1g51akb2r6957ik9axzgn7lc89yg008nqhg1m4yvkasj46y1s3jg")))

(define-public crate-pagetop-jquery-0.0.6 (c (n "pagetop-jquery") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "12nrzvpqhd0rbzsmwl4hcnfqsba1wacgb2gimn6f9rrm2nb9f92g")))

(define-public crate-pagetop-jquery-0.0.7 (c (n "pagetop-jquery") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "01rdhjna2gxkv6wvq4axavmwph1qmwn6l6h7czfcpc0wqz5b9h00")))

(define-public crate-pagetop-jquery-0.0.8 (c (n "pagetop-jquery") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1kwip2vajlnbmjp6d2gczisszmyyivh29ca9zhijd6gd2sdpbjzc")))

