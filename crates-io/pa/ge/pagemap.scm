(define-module (crates-io pa ge pagemap) #:use-module (crates-io))

(define-public crate-pagemap-0.1.0 (c (n "pagemap") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wgfq7qaz2j04dda8j9mkf906lcgs13pa3l098gan7466vbhpcdr")))

