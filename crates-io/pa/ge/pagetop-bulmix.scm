(define-module (crates-io pa ge pagetop-bulmix) #:use-module (crates-io))

(define-public crate-pagetop-bulmix-0.0.1 (c (n "pagetop-bulmix") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1sjf8dd3amzgsjxxga5d7pwgwi5s4qhw2khxadf4cy4v9h40kq96")))

(define-public crate-pagetop-bulmix-0.0.2 (c (n "pagetop-bulmix") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0c9yc5mh4msj5s6vzmswn1rv51431w0mnbw5wlr6vzx4caq9ligw")))

(define-public crate-pagetop-bulmix-0.0.3 (c (n "pagetop-bulmix") (v "0.0.3") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0cyv72rmasar6dpbn3aivw6rj74m197wfvpg6f7i1izq9sr1fsgn")))

(define-public crate-pagetop-bulmix-0.0.4 (c (n "pagetop-bulmix") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1izzkbrr9qsip5vigh4inrryvkvkf9xy3f597dva7b70rys2vaag")))

(define-public crate-pagetop-bulmix-0.0.5 (c (n "pagetop-bulmix") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1bjam723s99rgdym2jxxg0akhdx7in0gmjc3ckp5q1sr4zkrd3qa")))

(define-public crate-pagetop-bulmix-0.0.6 (c (n "pagetop-bulmix") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1nmkhq18bi0van841gljpiai4camy0jvq1rf8sg47qjx48a19b8k")))

(define-public crate-pagetop-bulmix-0.0.7 (c (n "pagetop-bulmix") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0vm6z1p7pqrryx4whjvgrnfsji8xna9vn2ds8j7zgq01cbcrd1nf")))

(define-public crate-pagetop-bulmix-0.0.8 (c (n "pagetop-bulmix") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0k57ag62w3s8ih9yz9xjpnri6ibrakci7br6qi9g4wy913fx7xxb")))

(define-public crate-pagetop-bulmix-0.0.9 (c (n "pagetop-bulmix") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0xkcx6hp045ipr0g8cir8gjcbn5xliisxzb444i1xkpcm0x6q0zd")))

(define-public crate-pagetop-bulmix-0.0.10 (c (n "pagetop-bulmix") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0f4acrcfx69zblvb1sc2192alkm6nr689hdd4rqnl04f8bls3603")))

(define-public crate-pagetop-bulmix-0.0.11 (c (n "pagetop-bulmix") (v "0.0.11") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "16qbk74fimrzk7an0fiylp354q6x8qbmq6hwmc10b5041l74z860")))

(define-public crate-pagetop-bulmix-0.0.12 (c (n "pagetop-bulmix") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "07xl9wyxyyzih8ng49135a5csr9qxv6znpvxc4asad81r9d93jy9")))

(define-public crate-pagetop-bulmix-0.0.13 (c (n "pagetop-bulmix") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0l746d53ailfgwy7wlxparrwxl2ss3qfwdmjnlc7wcs2h89i9ip7")))

(define-public crate-pagetop-bulmix-0.0.14 (c (n "pagetop-bulmix") (v "0.0.14") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1sy50wvxxfpawifcyms7piy9mwi9cwkjqfapsm1xdxx92jv7281f")))

(define-public crate-pagetop-bulmix-0.0.15 (c (n "pagetop-bulmix") (v "0.0.15") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "10p9n8cdsx3r008n4fqdk0wbsyhdf1lpld0vrfcbzv15cdfizl3x")))

(define-public crate-pagetop-bulmix-0.0.16 (c (n "pagetop-bulmix") (v "0.0.16") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0anfpanmpnnwq4hxpqyhca0x9gi5q0r7ym5gs3g2s719ixy4zq3x")))

(define-public crate-pagetop-bulmix-0.0.17 (c (n "pagetop-bulmix") (v "0.0.17") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "096mwwc2050sl8amw1z61ksrz1xwc3n9j1gxji2v9xmm4jhw5xhx")))

