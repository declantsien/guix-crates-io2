(define-module (crates-io pa ge pagebrowse) #:use-module (crates-io))

(define-public crate-pagebrowse-0.1.0 (c (n "pagebrowse") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "pagebrowse_manager") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16w3wxphy6kn8nnd1axfqlpfdwfdixwd0ks21d8v4yncz41pm0an")))

(define-public crate-pagebrowse-0.1.1 (c (n "pagebrowse") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "pagebrowse_types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12n4aak0ma3amn1bbh1hp8ysw6vzhh198c8qjqcl6a3a8bqj90gh")))

