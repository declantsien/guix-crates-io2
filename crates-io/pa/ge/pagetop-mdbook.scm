(define-module (crates-io pa ge pagetop-mdbook) #:use-module (crates-io))

(define-public crate-pagetop-mdbook-0.0.1 (c (n "pagetop-mdbook") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1ysp5h39r07k6yj7yas44dcmbg18b2jj4l4x46pl7zrr5rp8l249")))

(define-public crate-pagetop-mdbook-0.0.2 (c (n "pagetop-mdbook") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "10nkm338xf6zmsq7g8wzaavaba4fr6zsbdzmin8pzx26a5yk6wms")))

(define-public crate-pagetop-mdbook-0.0.3 (c (n "pagetop-mdbook") (v "0.0.3") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0c5wwc6x4fjsj70gb1qqgz2prb684y9a7b9pb8m104gyqw6xcgsc")))

(define-public crate-pagetop-mdbook-0.0.4 (c (n "pagetop-mdbook") (v "0.0.4") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "159qn9wxjaz6j31h21f3xqfp4fdywsj3a73sh94bk8lqghf5iyrk")))

(define-public crate-pagetop-mdbook-0.0.5 (c (n "pagetop-mdbook") (v "0.0.5") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0qpag9f8770h95lsm09xqq01si6ad1xlgi62fg5p1mm8b0iwq86m")))

(define-public crate-pagetop-mdbook-0.0.6 (c (n "pagetop-mdbook") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "149jwa5mvh0yjgfhkfqbisifmlzfwwi9hn7dbprgd7wjbwrpzfrp")))

(define-public crate-pagetop-mdbook-0.0.7 (c (n "pagetop-mdbook") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0r31k75k7gk9d2ly9w5x6lga80pakhb3a1iki4iwsi0palzy4y5n")))

(define-public crate-pagetop-mdbook-0.0.8 (c (n "pagetop-mdbook") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0m2hrnjsjqhh3f9skxnzx93gy4hb4vr9c6nq49fdv3ag6b69lfbs")))

(define-public crate-pagetop-mdbook-0.0.9 (c (n "pagetop-mdbook") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1qxl54pgm2j7qlq2kd0xhdn44g5my7hvf7sxiakgfrih944zlyr7")))

(define-public crate-pagetop-mdbook-0.0.10 (c (n "pagetop-mdbook") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1x36ib0cmzhs528kf9q6csinc9iszhgd1zchd24zax7j3hp2wb0y")))

(define-public crate-pagetop-mdbook-0.0.12 (c (n "pagetop-mdbook") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0vlgl3xnq61igbaxmprnpvqqhk0rwklrjd8zzpml0ddrg9a7q5i9")))

(define-public crate-pagetop-mdbook-0.0.13 (c (n "pagetop-mdbook") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1kdkr7rs84hcpax1d7zxddgsaim5kdvd5an9hz6f887c87y5ha04")))

(define-public crate-pagetop-mdbook-0.0.14 (c (n "pagetop-mdbook") (v "0.0.14") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "128wsxw3ldlv3v51fbz5d8mcl86lkzysawahamwgp1sq3w6aq6hh")))

(define-public crate-pagetop-mdbook-0.0.15 (c (n "pagetop-mdbook") (v "0.0.15") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "181z42x3zxqj79nk81aaj9pl8fk4lja3jbhwxigiqcir0shzanv5")))

(define-public crate-pagetop-mdbook-0.0.17 (c (n "pagetop-mdbook") (v "0.0.17") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0rl522nv95nsldl1znh5f28zx3dxzz1s4ybyhrjq3lk9lnmklyfh")))

(define-public crate-pagetop-mdbook-0.0.18 (c (n "pagetop-mdbook") (v "0.0.18") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "03x69nvhk5qvxmv8qppp93dx77372mzsap75d08cz2246ibcbrla")))

