(define-module (crates-io pa ge pagerank_rs) #:use-module (crates-io))

(define-public crate-pagerank_rs-0.1.0 (c (n "pagerank_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1g0jya31fib5yb207yawvmld1ls27qgl39d39nh0l58p9bmyi8d6")))

