(define-module (crates-io pa ge pagetop-megamenu) #:use-module (crates-io))

(define-public crate-pagetop-megamenu-0.0.1 (c (n "pagetop-megamenu") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "00hjd6i0y2jydnqzhaafbr23hy8j9xa4gjaban51sg4wnlv2gv6j")))

(define-public crate-pagetop-megamenu-0.0.2 (c (n "pagetop-megamenu") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1vpcb71ib8zaikhj1710p2410mkfq48av40wxxl7slj8ljkhccki")))

(define-public crate-pagetop-megamenu-0.0.3 (c (n "pagetop-megamenu") (v "0.0.3") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1p171bn6n3iqpbldgsn2hnfkj6j8nrv90da3gcs5qsx4drbdjh8x")))

(define-public crate-pagetop-megamenu-0.0.4 (c (n "pagetop-megamenu") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1cvw1kj0xklvrq7fv421wapd4cwa673j3z6khcmkprbvwln11zgz")))

(define-public crate-pagetop-megamenu-0.0.5 (c (n "pagetop-megamenu") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0q7svvd5vkslmp4h9mkvz55ar46pmgb16b5qdjkv49hg3ps0gzq4")))

(define-public crate-pagetop-megamenu-0.0.6 (c (n "pagetop-megamenu") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0zarj3pxjl90a800aqjlnm6dlhn4kfxjg83bw8r533bm93xkyj1l")))

(define-public crate-pagetop-megamenu-0.0.7 (c (n "pagetop-megamenu") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1hlxmpgjcy9d8n361q6j2vbbn886agz79gsscw4jqyrfmyyz0mi3")))

(define-public crate-pagetop-megamenu-0.0.8 (c (n "pagetop-megamenu") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0svp8pr4m9qhma9xgip2da0dxwkqmg6147fj3mplfwshdc1p1ppf")))

(define-public crate-pagetop-megamenu-0.0.9 (c (n "pagetop-megamenu") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1wg6663yzjh8qw2lmdib8yx1q2pv51yzi34sfgapihd0vd63a36f")))

(define-public crate-pagetop-megamenu-0.0.10 (c (n "pagetop-megamenu") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1bjxq98qxjvjw4fdxry8dw3fjg98wyzy0rwk16132bii0chxsz7v")))

