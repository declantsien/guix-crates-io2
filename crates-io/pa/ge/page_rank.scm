(define-module (crates-io pa ge page_rank) #:use-module (crates-io))

(define-public crate-page_rank-0.1.0 (c (n "page_rank") (v "0.1.0") (h "036ls63vvk9xy97qmbgvvg96klzz98nhfd2410q47ds8b6v02a06") (y #t)))

(define-public crate-page_rank-0.2.0 (c (n "page_rank") (v "0.2.0") (h "1pllds0d2nr82d8jgdn5m7cs4irj5sc0yxcnjlslck9ifz50bpdp")))

