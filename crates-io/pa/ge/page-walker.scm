(define-module (crates-io pa ge page-walker) #:use-module (crates-io))

(define-public crate-page-walker-0.1.0 (c (n "page-walker") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11ry7nv80qdvy62280l7skmqdpb09m2r1hycam1pa2gbs9r85frb")))

(define-public crate-page-walker-0.2.0 (c (n "page-walker") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1f8ylb86iics8lvbn1sdz74wgy01kjn67kma83mpy717yj6zazj3")))

(define-public crate-page-walker-0.2.1 (c (n "page-walker") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1fliys008988j8r634ypld9mk7z0jyrzjd2fispnjj1ijx4az91c")))

(define-public crate-page-walker-0.3.0 (c (n "page-walker") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "16g5g1kj8kk88h4f45h11dl3vzzpc00myy6dpc3vn34z0aqiybsc")))

