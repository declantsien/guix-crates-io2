(define-module (crates-io pa ge pager) #:use-module (crates-io))

(define-public crate-pager-0.1.0 (c (n "pager") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xn2n6g573kgvs5rax3ldbpiiqgvah8nka2abi8zl9sq5sbg30z4")))

(define-public crate-pager-0.1.1 (c (n "pager") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08lxg891gc57iixv7m5yikdvmmqm7a3j9rwnghn8lrzzgz32q361")))

(define-public crate-pager-0.2.0 (c (n "pager") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08z6njd7hdjjs2k9inkfb1vnp21n4wxyw4yvl9s0vfq46i5m2qm2")))

(define-public crate-pager-0.2.1 (c (n "pager") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "047asxwp896yg64s1y3r7ph2bqlkkgh1a3jm9gxblh8al0dnpnws")))

(define-public crate-pager-0.2.2 (c (n "pager") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "073jyw4jghszbsm4q3rh8wmq6m24asn2j0i4zrmh2x573sklksd3")))

(define-public crate-pager-0.9.0 (c (n "pager") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "013lpqhkf9gbl7dz042h8xn19lc7zpvi40mw6l4vhf1rnbmmcmf6")))

(define-public crate-pager-0.9.1 (c (n "pager") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lw47sw0m3ds3sypwjzajrbimwwyb617x5kd17pv3p0z6v50a8kx")))

(define-public crate-pager-0.9.2 (c (n "pager") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07s342g2mydmnvfz0qkvjg5il6kvwcgdvbsr54724f2b4fmp8j8p")))

(define-public crate-pager-0.9.3 (c (n "pager") (v "0.9.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09ldngr07djsqiin3krc5mpw6w3j77s28miadrdz7agv5x5ilffa")))

(define-public crate-pager-0.9.4 (c (n "pager") (v "0.9.4") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mpydhbqpndddxx7mg984h2jqrjinc8kv8s6imlx8cx4jzrfaw0p")))

(define-public crate-pager-0.10.0 (c (n "pager") (v "0.10.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c4crrvq7qrk1qiv0l9x4zplb87iplzhjk9x4mdpvznidifqgcld")))

(define-public crate-pager-0.11.0 (c (n "pager") (v "0.11.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0aibk3ijdl73yn1hqlmz1cghwsd6nk1v8qyylwfbhd0ygzq901d7")))

(define-public crate-pager-0.12.0 (c (n "pager") (v "0.12.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g7c9xgxc6vjpfhn2rv761k0j3pl7f38qln0kbsv31ygn2v7fdpc")))

(define-public crate-pager-0.12.1 (c (n "pager") (v "0.12.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1iv0hh41dwdsiidp4kzvzvpp3z81gx2mbbijhall9ghgv36dn002")))

(define-public crate-pager-0.13.0 (c (n "pager") (v "0.13.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1861m4sc4bxdgcsbhd8h63c9mg25p9aq7c4cyg7541r9sprdc5mp") (f (quote (("pedantic"))))))

(define-public crate-pager-0.14.0 (c (n "pager") (v "0.14.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xg1mla63myz4jzsvv6pbrdpsmsfv66gcknvlsrwn7y5nv46wphp") (f (quote (("pedantic"))))))

(define-public crate-pager-0.15.0 (c (n "pager") (v "0.15.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a35mg68s0p63ya2k5hsg620c4llkjw2fx1sfi0laz4pz8myv75n") (f (quote (("pedantic"))))))

(define-public crate-pager-0.16.0 (c (n "pager") (v "0.16.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s0r95q3jfbh2c3paab2bpl158lyaq35xnzy1x7mrdfhy26d1iq5") (f (quote (("pedantic"))))))

(define-public crate-pager-0.16.1 (c (n "pager") (v "0.16.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10188qgnsz988g30fvl4whkljh2zl4gpbp6kc48bpywpbhd23695") (f (quote (("pedantic"))))))

