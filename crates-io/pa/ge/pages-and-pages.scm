(define-module (crates-io pa ge pages-and-pages) #:use-module (crates-io))

(define-public crate-pages-and-pages-0.1.0 (c (n "pages-and-pages") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)))) (h "1m8pa7fqkjvyb64rhmkli5h0jwrz8wb85r6kk23v3c5b2zm1f3dx")))

(define-public crate-pages-and-pages-0.2.0 (c (n "pages-and-pages") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)))) (h "0w26px9r7d37yp7gl7rwm8rxgan2w7qmir6n3s4mxinxg5pjr5k1")))

(define-public crate-pages-and-pages-0.2.1 (c (n "pages-and-pages") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)))) (h "1ay705g1ziglhshpk6gy1rd27q3b0kpamzyxgz671ar0450y5vs0")))

(define-public crate-pages-and-pages-0.3.0 (c (n "pages-and-pages") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03a2pw6w1radwa9h2ndc54f8xd08h93k3c4qqxligpsbv5s826n7")))

(define-public crate-pages-and-pages-0.4.0 (c (n "pages-and-pages") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v1jvyjdm28a2k23nabm6nj1w2ccy1gkz4aa4qwasqa697y12mj4")))

(define-public crate-pages-and-pages-0.4.1 (c (n "pages-and-pages") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19jl5i58dayl5wycvn9yx2q00zyhjvfwvnhdl4nxib80p0qvymhh")))

