(define-module (crates-io pa ge pagetop-bootsier) #:use-module (crates-io))

(define-public crate-pagetop-bootsier-0.0.1 (c (n "pagetop-bootsier") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1v4xdls6fi7qb3v98ywa2627a8cx51ybgpp81nl9zvvcf7icyx39")))

(define-public crate-pagetop-bootsier-0.0.2 (c (n "pagetop-bootsier") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1cqg113hj6ckxcf55sqrnywxnrp0k4mf2gacnvix3rs6y0315isz")))

(define-public crate-pagetop-bootsier-0.0.3 (c (n "pagetop-bootsier") (v "0.0.3") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0ijy5lj27ijjkrzbbfc65wwb0n2w3jh5bz4jpjf1m50y1jvw0rw2")))

(define-public crate-pagetop-bootsier-0.0.4 (c (n "pagetop-bootsier") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "15jvfi7dihc01f1ndf6fxs0m9jv4npmjivy4whccdypj40dv7d6h")))

(define-public crate-pagetop-bootsier-0.0.5 (c (n "pagetop-bootsier") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1gx1hj9p5dv64c84vi3kp8i58s9dx3w8s2il0mixzdv8qm41z0ws")))

(define-public crate-pagetop-bootsier-0.0.6 (c (n "pagetop-bootsier") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "151z9rh60qm1cbc7p2ljs33bx0sfxff7fsvfm20k1d7hsdy5wvsy")))

(define-public crate-pagetop-bootsier-0.0.7 (c (n "pagetop-bootsier") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1xj1rxxwrbwgl83p5bvbhl68b6jvvc6rrjh5971scq81qrzdkk6z")))

(define-public crate-pagetop-bootsier-0.0.8 (c (n "pagetop-bootsier") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0hxw6170lpww7722vl4s84jh3k0b5nn1wwnbsadvh4jglg1kghhy")))

(define-public crate-pagetop-bootsier-0.0.9 (c (n "pagetop-bootsier") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0w0317l4ryh93kif8g6hvir3wyywhlyv2yarpmk86xjzrlq3f2f9")))

(define-public crate-pagetop-bootsier-0.0.10 (c (n "pagetop-bootsier") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-jquery") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "13bxx8jrb0f1m8k5f2c443bj9sws04yj0n1hxac2fgc0q81524h4")))

(define-public crate-pagetop-bootsier-0.0.11 (c (n "pagetop-bootsier") (v "0.0.11") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "03hhn264a9rp8mml412rkix23r0ffz3jr9cn5mkkb74zajxy3v34")))

(define-public crate-pagetop-bootsier-0.0.12 (c (n "pagetop-bootsier") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "157zy6ip3ygp3qwczic437f19jd6k9hxgfjqaqdlc1z69imldyy5")))

(define-public crate-pagetop-bootsier-0.0.13 (c (n "pagetop-bootsier") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "094iblmwwslnr8id7w03nbxc123043fk8d7708pnghskz8jm1k4c")))

(define-public crate-pagetop-bootsier-0.0.14 (c (n "pagetop-bootsier") (v "0.0.14") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "04x3symr6f7jif9mvr42g5p16m4d9jph1hk8h49c9xy3yz68fzhi")))

(define-public crate-pagetop-bootsier-0.0.15 (c (n "pagetop-bootsier") (v "0.0.15") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "13mdlf5r75kcvv5f4qjfsj3bapb0ma0faspndpcs6wl12l1ifh2b")))

(define-public crate-pagetop-bootsier-0.0.16 (c (n "pagetop-bootsier") (v "0.0.16") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "169dw64avnfy9ip8r8n5x97nin6gzcicz04zz063xsngbzhjr5fb")))

(define-public crate-pagetop-bootsier-0.0.17 (c (n "pagetop-bootsier") (v "0.0.17") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "18w0i4gzid537jkxip2qpz5lhzc1dymp8s8rm1q9924vyawbwfjf")))

(define-public crate-pagetop-bootsier-0.0.18 (c (n "pagetop-bootsier") (v "0.0.18") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0q8rkj9lnwwmix07hbvcrji5167flmnbmql0z3ilh5r8kd2qkqyj")))

