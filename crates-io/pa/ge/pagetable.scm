(define-module (crates-io pa ge pagetable) #:use-module (crates-io))

(define-public crate-pagetable-0.0.1 (c (n "pagetable") (v "0.0.1") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "model") (r "^0.0") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "15cvdrw0n2vczglbvpnm0ar63lsq91hrmhxgcnk8vahwrjcigw7m")))

(define-public crate-pagetable-0.0.2 (c (n "pagetable") (v "0.0.2") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "model") (r "^0.0") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0jld4z5p0qrasyr7i9s4087dj2vvph3nppnklry34a46j0hifwic")))

(define-public crate-pagetable-0.0.3 (c (n "pagetable") (v "0.0.3") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "model") (r "^0.0") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0pc3zijkvaldb9gg9cf4jd57srpid1pp0y9ay3wwlip9vfjvxx8a")))

(define-public crate-pagetable-0.1.0 (c (n "pagetable") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "model") (r "^0.0") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1vxckr6mf4332lbh1hwnm4hyp4njra9s9y0wgdk9vsf6lq9lpisa")))

(define-public crate-pagetable-0.1.1 (c (n "pagetable") (v "0.1.1") (d (list (d (n "model") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sled_sync") (r "^0.1") (d #t) (k 0)))) (h "0jrdy462hlpp1rfj49a1jac8p087mzpdwdmdsps5ah99aabfd5jf")))

(define-public crate-pagetable-0.1.3 (c (n "pagetable") (v "0.1.3") (d (list (d (n "model") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sled_sync") (r "^0.2") (d #t) (k 0)))) (h "03vzqr16wkbnfwc758nak8xflnqca57q02cqxhmhd279jblci1xg")))

(define-public crate-pagetable-0.1.4 (c (n "pagetable") (v "0.1.4") (d (list (d (n "model") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sled_sync") (r "^0.2.2") (d #t) (k 0)))) (h "0qg20hjjpxb2k9a8spbv200yizv03gpbj2wciz8jj4x271y57a96")))

(define-public crate-pagetable-0.1.5 (c (n "pagetable") (v "0.1.5") (d (list (d (n "model") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sled_sync") (r "^0.3") (d #t) (k 0)))) (h "1zcra8yklvgajibljrbsfsxxy9kwa0glkdnvvz2s62hj0s8sjcm1")))

(define-public crate-pagetable-0.2.0 (c (n "pagetable") (v "0.2.0") (h "1g53a287sz2hmbyfjlh1s1jmhqa2ia33hbxqbpg9rky4rrm30nq1")))

(define-public crate-pagetable-0.2.1 (c (n "pagetable") (v "0.2.1") (h "1w8d9h2db7ncj9prw7a70yrq39vvdw7icnxhbqjrm50vzkxjsqgl")))

(define-public crate-pagetable-0.2.2 (c (n "pagetable") (v "0.2.2") (h "1d7v66ixzaqa5gw6a89s8kl0s3zrjrrjjg68rla0hrsrdzpanp2w")))

(define-public crate-pagetable-0.2.3 (c (n "pagetable") (v "0.2.3") (h "12gdc76j1zh3jy94fy2wlsslx9q2xzcb66wixyckn8rsv9w3k640")))

(define-public crate-pagetable-0.2.4 (c (n "pagetable") (v "0.2.4") (h "065y7l8dh9wdnc1j8iwqfz7kpzwgsjgm9h7d76zr8mpqjl822c38")))

(define-public crate-pagetable-0.2.5 (c (n "pagetable") (v "0.2.5") (h "0nzp66lmjrchymd9jd2s2fkhbhjmbsv9x635avj4wlqd23wfsi2v")))

(define-public crate-pagetable-0.2.6 (c (n "pagetable") (v "0.2.6") (h "0k1xs3injs9x8gkvn96jrp2k3fld57h36izpf9bv8r1psghfwm6r")))

(define-public crate-pagetable-0.3.0 (c (n "pagetable") (v "0.3.0") (h "1y0gyfwmbbsbq5hx6qp6zfc2jwqgxay9fswz9w3ipc5ihr8bhg79")))

(define-public crate-pagetable-0.3.1 (c (n "pagetable") (v "0.3.1") (h "0vaanxskcc4f18rh7wg0z7yqa2pvd8g0ajg06sdkdjgh21yvybdx")))

(define-public crate-pagetable-0.4.0 (c (n "pagetable") (v "0.4.0") (h "1f09pavkd2hdkpw60y57afzva1qal6bqhn6qli0qla34p5kfxchf")))

(define-public crate-pagetable-0.4.1 (c (n "pagetable") (v "0.4.1") (h "1z3m03g9pbs53id9crcgpsgs7xji4ldfy9rc6l2jnp9jqri9fqb9")))

(define-public crate-pagetable-0.4.2 (c (n "pagetable") (v "0.4.2") (h "0l69z4ll2gf3rsn3pxi775vvlxajbf3hmxqz47110fg4dmzc3jvl")))

(define-public crate-pagetable-0.4.3 (c (n "pagetable") (v "0.4.3") (h "0mi0kwf4rwqd5n21pq1b6x4y5r8kqwj5lvyfkppzsy7kwbsbgbxl")))

(define-public crate-pagetable-0.4.4 (c (n "pagetable") (v "0.4.4") (h "0zxxiqjypqc1z27lh8il8w6k574blk47jjr5nkkrf6rq95bck9xq")))

(define-public crate-pagetable-0.4.5 (c (n "pagetable") (v "0.4.5") (h "1q7nfh6m19121djkr6ykrfp03ahk0c7dv9g72xf7zs0rasiid9cj")))

