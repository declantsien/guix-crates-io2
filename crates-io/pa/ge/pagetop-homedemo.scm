(define-module (crates-io pa ge pagetop-homedemo) #:use-module (crates-io))

(define-public crate-pagetop-homedemo-0.0.1 (c (n "pagetop-homedemo") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0929jzk996x82dlb1y7gpah3qfvbkh4ss7h5r1x2ig3j6a8p83g4")))

(define-public crate-pagetop-homedemo-0.0.2 (c (n "pagetop-homedemo") (v "0.0.2") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1c9cw92r7c6hb4pk8v042xv3mj9ssx3wnj4hy9n50csyw7r02wg9")))

(define-public crate-pagetop-homedemo-0.0.3 (c (n "pagetop-homedemo") (v "0.0.3") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "12a5g0nix8myhq4qp1rbvjck4i16fvq1x56hrzr4r8fsykbi95bm")))

(define-public crate-pagetop-homedemo-0.0.4 (c (n "pagetop-homedemo") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0ivm4ldyvb6h0c58ifnf5y1wv36jczmywl6dffi25jnyi0fzw5ih")))

(define-public crate-pagetop-homedemo-0.0.5 (c (n "pagetop-homedemo") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "12i5zgncr240pah70qcc4859f7zsrbr3b52icd944j284flq951b")))

(define-public crate-pagetop-homedemo-0.0.6 (c (n "pagetop-homedemo") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0lhxkyaa1f84kw0ksi28afrwyd7q2p9y87dyqr2262dwkq104h0k")))

(define-public crate-pagetop-homedemo-0.0.7 (c (n "pagetop-homedemo") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1ydamkh0al1f76izai8xhnsvzmgd3vn31imjwa39flz9n2j4bpjv")))

(define-public crate-pagetop-homedemo-0.0.8 (c (n "pagetop-homedemo") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0kx3pjwbq894ylm449hdwca9w1z70ygilf98g89c5cd821qbi0qz")))

(define-public crate-pagetop-homedemo-0.0.9 (c (n "pagetop-homedemo") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "pagetop-minimal") (r "^0.0") (d #t) (k 0)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "15npj8qnmpqnpg0jxmmjlgwq91sf45ql3vzqpskf7vacyl02arqx")))

(define-public crate-pagetop-homedemo-0.0.10 (c (n "pagetop-homedemo") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0b9fw6m4rp0jdqk373v9hydwpdswd6hk463kqm5imqr56mz0wl1s")))

(define-public crate-pagetop-homedemo-0.0.11 (c (n "pagetop-homedemo") (v "0.0.11") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0dik127n80872gv3f3hr7p65nf0r54b4z4i4836k56022bpjhqhj")))

(define-public crate-pagetop-homedemo-0.0.12 (c (n "pagetop-homedemo") (v "0.0.12") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1hglv2r07fzm37dps6qkc6nwxakn6maajca2klkjcgzvyzv6wmw8")))

(define-public crate-pagetop-homedemo-0.0.13 (c (n "pagetop-homedemo") (v "0.0.13") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "08m2lq8nhs5q628r7vbvlk05yq6pidm7zfd2v37pq36l4rxwsghj")))

