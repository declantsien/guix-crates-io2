(define-module (crates-io pa ge paged) #:use-module (crates-io))

(define-public crate-paged-0.1.0 (c (n "paged") (v "0.1.0") (d (list (d (n "educe") (r "^0.4.22") (d #t) (k 0)) (d (n "paged-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sharded-slab") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "008vyb0ajlq4lx2i3dfwn0q3yw45p9sdhfg1yg42cqkkvsfxkyrd") (f (quote (("derive" "paged-derive"))))))

(define-public crate-paged-0.1.1 (c (n "paged") (v "0.1.1") (d (list (d (n "educe") (r "^0.4.22") (d #t) (k 0)) (d (n "paged-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sharded-slab") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jqlll711kidj45k4zzvw29mz6m1ynk1j1w0vscnjr315628gk4g") (f (quote (("derive" "paged-derive"))))))

