(define-module (crates-io pa ge page-lock) #:use-module (crates-io))

(define-public crate-page-lock-0.1.0 (c (n "page-lock") (v "0.1.0") (h "0vwc3rglvfa9pkw9096w2p1dnr9r37l1i8dy50y6p3cam68l8wj3")))

(define-public crate-page-lock-1.0.0 (c (n "page-lock") (v "1.0.0") (h "15nqyadq5wsr9r5qxij0wdgdq3n39lnzpv0vvcg1rxyrm0r092nf")))

(define-public crate-page-lock-1.0.1 (c (n "page-lock") (v "1.0.1") (h "0vbnsignajqjq4bbm0mzqcza0smyrjvqnik8hj558y7xv9hlplkm")))

(define-public crate-page-lock-1.0.2 (c (n "page-lock") (v "1.0.2") (h "0mjmpxxvy5sd13gc1b3khh0fdx3y3kqxpwqs28jdva1c1xjg906n")))

(define-public crate-page-lock-1.0.3 (c (n "page-lock") (v "1.0.3") (h "01n7nkaq33srajz8rhbivy27y144ig11105fla1sy4i456xb0d5d")))

(define-public crate-page-lock-1.0.4 (c (n "page-lock") (v "1.0.4") (h "1cl8ljr0i0ycvy23mg8jrlkyy1z0na3rv7y4n182x271kwrriix8")))

(define-public crate-page-lock-3.0.0 (c (n "page-lock") (v "3.0.0") (d (list (d (n "c-map") (r "^0.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "19jrv6w12w93lqw89d5vdyk9j0q32bzgyqf7d5wjxw9siwah7cp3")))

(define-public crate-page-lock-3.0.1 (c (n "page-lock") (v "3.0.1") (d (list (d (n "c-map") (r "^0.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0fbwd70xxbw4pslvl5m59vaircnpffp5cpzc0mfw780f5vs0dn0f")))

(define-public crate-page-lock-3.1.0 (c (n "page-lock") (v "3.1.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "02ky2h56yvfwhgsxhdcw3l6402q1fj8b3fkcz2xqf6kgmq14h3r0")))

