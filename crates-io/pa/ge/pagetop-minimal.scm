(define-module (crates-io pa ge pagetop-minimal) #:use-module (crates-io))

(define-public crate-pagetop-minimal-0.0.1 (c (n "pagetop-minimal") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "0r2kc58ljmhd1zschw6zpw98hcgp6ln31czarrqzk0zdj63qz3k4")))

(define-public crate-pagetop-minimal-0.0.2 (c (n "pagetop-minimal") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)))) (h "1jf72gzill4f01w0y7mqy75nswxg1hywzxa6v9nj92vj04dicahn")))

(define-public crate-pagetop-minimal-0.0.3 (c (n "pagetop-minimal") (v "0.0.3") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1n3nn6pq7iqjcz3009w45n2cc0imxrhx6lq65b6j8fy3n0rc5dvb")))

(define-public crate-pagetop-minimal-0.0.4 (c (n "pagetop-minimal") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0r82ribfahjfhnkxg9ha731q9y5rksgwlrm3g59jp62y8lk5f89x")))

(define-public crate-pagetop-minimal-0.0.5 (c (n "pagetop-minimal") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1xg7jlcxbb71bp7c9dc5qdkhlnfj32ng77q4mxziqz08p3yyw1dl")))

(define-public crate-pagetop-minimal-0.0.6 (c (n "pagetop-minimal") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "17xwhma6jhngazqym8kbjkwz3g4dvhbfmzvjv3s464vwfd6hm573")))

(define-public crate-pagetop-minimal-0.0.7 (c (n "pagetop-minimal") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "19wd7pv4ql0jb0hx7c1hpdd5rypnrf9s6hv0ghw83d7qsks68qxa")))

(define-public crate-pagetop-minimal-0.0.8 (c (n "pagetop-minimal") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "075qv9xa8yxqqrfrz01lk8disqjizwxknp1yx38li04syikqrply")))

(define-public crate-pagetop-minimal-0.0.9 (c (n "pagetop-minimal") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0cj7ar1zd4ck56491fnxcvbljw3ndy4hdvk38hrspm7w0cga77gf")))

(define-public crate-pagetop-minimal-0.0.10 (c (n "pagetop-minimal") (v "0.0.10") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0bqifc204m07vi71mq3bz9faanzhh7axkwma2bcnhkgy58xijbm2")))

