(define-module (crates-io pa ge page-table) #:use-module (crates-io))

(define-public crate-page-table-0.0.1 (c (n "page-table") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1zy4mvxm96mgdr7j4x4qsmcmsg52bi68zsn70wnilj7arkbq5lk6")))

(define-public crate-page-table-0.0.2 (c (n "page-table") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0j2f2yfg76xjvk6w0q1yzhfhl7n83idykzddjci408fkmiygwxi0")))

(define-public crate-page-table-0.0.3 (c (n "page-table") (v "0.0.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0vcaw35ng7khq9jxznn93i64qhdv7p6knfmizr8gp0x76bqhrz7k")))

(define-public crate-page-table-0.0.4 (c (n "page-table") (v "0.0.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0rynyzp688vcb284prm91dl8kwzm4681pvlhcmacb51kd4jjwylh")))

(define-public crate-page-table-0.0.5 (c (n "page-table") (v "0.0.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0m1kbc423lfjrb09darci056q7ib2flfyfi2rgwlrdvcvyxr2abj")))

(define-public crate-page-table-0.0.6 (c (n "page-table") (v "0.0.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1h5jlamvy0hjsqk2laz379h2lj1c6i5ahcvfpc6n7yq2qkk998w2")))

