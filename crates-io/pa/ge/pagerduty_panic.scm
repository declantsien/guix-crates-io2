(define-module (crates-io pa ge pagerduty_panic) #:use-module (crates-io))

(define-public crate-pagerduty_panic-0.1.0 (c (n "pagerduty_panic") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "000sy5w8i8vqirv6x65sw1piys4rhb5433g4d815d4m99gmy025w")))

(define-public crate-pagerduty_panic-0.1.1 (c (n "pagerduty_panic") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "0yi88vd6k8c3mda2kw8zl8xvahamcq5n20y10d0qr84p15ndpbf0")))

