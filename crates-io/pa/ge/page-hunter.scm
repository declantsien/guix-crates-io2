(define-module (crates-io pa ge page-hunter) #:use-module (crates-io))

(define-public crate-page-hunter-0.1.0 (c (n "page-hunter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "1jm3j7hldm64b19sy34j7ia6s48ss6294ajd8flc30j04caxhpkj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-page-hunter-0.1.1 (c (n "page-hunter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "1isi8lrfk6r6rghzjmzvydy210pvkj2h5kbal9kkr9fgrsrd1jzz") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-page-hunter-0.1.2 (c (n "page-hunter") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.202") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "sqlx") (r "^0.7.4") (f (quote ("runtime-tokio" "uuid" "time" "chrono" "json" "postgres"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.8.0") (d #t) (k 2)))) (h "071m99zi3zk09wd3wayz710423p7kk99b027jk4warncvpabcl3l") (s 2) (e (quote (("serde" "dep:serde") ("pg-sqlx" "dep:sqlx"))))))

(define-public crate-page-hunter-0.1.3 (c (n "page-hunter") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.202") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "sqlx") (r "^0.7.4") (f (quote ("runtime-tokio" "uuid" "time" "chrono" "json" "postgres" "mysql"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.8.0") (d #t) (k 2)))) (h "165q7lyl5c23s8rzxsnk5wgncwa7wvj5rxkxmdz957nbkai0x0xi") (s 2) (e (quote (("serde" "dep:serde") ("pg-sqlx" "dep:sqlx") ("mysql-sqlx" "dep:sqlx"))))))

