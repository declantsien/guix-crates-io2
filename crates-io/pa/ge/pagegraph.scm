(define-module (crates-io pa ge pagegraph) #:use-module (crates-io))

(define-public crate-pagegraph-0.1.0 (c (n "pagegraph") (v "0.1.0") (d (list (d (n "adblock") (r "^0.2") (d #t) (k 0)) (d (n "addr") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0ln3aa8ylnsjr3nwigcjk615mdb5jjcwpslmvih506fmmcjm8by1") (f (quote (("default" "serde"))))))

(define-public crate-pagegraph-0.1.1 (c (n "pagegraph") (v "0.1.1") (d (list (d (n "adblock") (r "^0.2") (d #t) (k 0)) (d (n "addr") (r "^0.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "05wc2lkxjznldblqlcfflgfipbmwqw3gkcjj2qz76g90w07kh437") (f (quote (("default" "serde"))))))

(define-public crate-pagegraph-0.1.2 (c (n "pagegraph") (v "0.1.2") (d (list (d (n "adblock") (r "^0.7") (d #t) (k 0)) (d (n "addr") (r "^0.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0m8ncfgvddq10rx32jyg7sv50i34fck8w85z41l8w1p8vy9pf4vh") (f (quote (("default" "serde"))))))

(define-public crate-pagegraph-0.1.3 (c (n "pagegraph") (v "0.1.3") (d (list (d (n "adblock") (r "^0.7") (d #t) (k 0)) (d (n "addr") (r "^0.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1s57gmgfzgvd4nliknkqh7hpig8b2zwgdjlwsm626qpa00kz87rq") (f (quote (("default" "serde"))))))

