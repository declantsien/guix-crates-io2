(define-module (crates-io pa ge page-turner) #:use-module (crates-io))

(define-public crate-page-turner-0.6.0 (c (n "page-turner") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt"))) (d #t) (k 2)))) (h "1jy9m9l255v9fg0b2mwz6pvw9fm3xwmjr01gp6wz2fi98z4rjgj1") (y #t)))

(define-public crate-page-turner-0.7.0 (c (n "page-turner") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt"))) (d #t) (k 2)))) (h "1ns6pmnqmlfcdpg55gh9708l7j0sffqcyc6y5xsllr8fhyr75si7")))

(define-public crate-page-turner-0.8.0 (c (n "page-turner") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt"))) (d #t) (k 2)))) (h "1gkf5dx3pjmsi7rdbj228vq2ajdm7mjn0hmmc9yc9gsh4bd5nwpd") (y #t)))

(define-public crate-page-turner-0.8.1 (c (n "page-turner") (v "0.8.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt"))) (d #t) (k 2)))) (h "0cd71884wkqygxzp58y5h69vd3da1xh2zlqnjrw7q413aw86f4xq")))

(define-public crate-page-turner-0.8.2 (c (n "page-turner") (v "0.8.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt"))) (d #t) (k 2)))) (h "01qd4f034yrxidlq9fv66lf2dw7k899xxk6g5aphymbyv47n1qh3")))

(define-public crate-page-turner-1.0.0-rc (c (n "page-turner") (v "1.0.0-rc") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0i1p83lpwpgbkp3vfgs1w1ars0n6j3hjy29l85ap3ksligv8bfm6") (f (quote (("mutable" "local") ("mt") ("local") ("dynamic" "mt" "async-trait") ("default" "mt")))) (y #t) (r "1.75")))

(define-public crate-page-turner-1.0.0-rc.1 (c (n "page-turner") (v "1.0.0-rc.1") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1sz5i3hr7r90ybnjinvpc0fbr5iw2ad3kcis4jbwwamwjh86fcbp") (f (quote (("mutable" "local") ("mt") ("local") ("dynamic" "mt" "async-trait") ("default" "mt")))) (y #t) (r "1.75")))

(define-public crate-page-turner-1.0.0-rc.2 (c (n "page-turner") (v "1.0.0-rc.2") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r5z51933z26rxrdi02pvdkq891g3kppv6q7qh9si88f82faaa0v") (f (quote (("mutable" "local") ("mt") ("local") ("dynamic" "mt" "async-trait") ("default" "mt")))) (y #t) (r "1.75")))

(define-public crate-page-turner-1.0.0 (c (n "page-turner") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1s7xg18bn5xqhimzji6j1vn51x1653668bhwdjrn67imkdpbj21n") (f (quote (("mutable" "local") ("mt") ("local") ("dynamic" "mt" "async-trait") ("default" "mt")))) (r "1.75")))

