(define-module (crates-io pa ge pagefind_stem) #:use-module (crates-io))

(define-public crate-pagefind_stem-0.1.0 (c (n "pagefind_stem") (v "0.1.0") (h "1mwpr65awqaha41ybcxa4q1a9mlsnnrpygmg62d1chmmcrslj30k") (f (quote (("yiddish") ("turkish") ("tamil") ("swedish") ("spanish") ("serbian") ("russian") ("romanian") ("portuguese") ("porter") ("norwegian") ("nepali") ("lovins") ("lithuanian") ("kraaij_pohlmann") ("italian") ("irish") ("indonesian") ("hungarian") ("hindi") ("greek") ("german2") ("german") ("french") ("finnish") ("english") ("dutch") ("danish") ("catalan") ("basque") ("armenian") ("arabic"))))))

(define-public crate-pagefind_stem-0.2.0 (c (n "pagefind_stem") (v "0.2.0") (h "0rql77kn9dba89jd9acnz3rccifsrxgdi1a3wlrdsryq7ifwzfbh") (f (quote (("yiddish") ("turkish") ("tamil") ("swedish") ("spanish") ("serbian") ("russian") ("romanian") ("portuguese") ("porter") ("norwegian") ("nepali") ("lovins") ("lithuanian") ("kraaij_pohlmann") ("italian") ("irish") ("indonesian") ("hungarian") ("hindi") ("greek") ("german2") ("german") ("french") ("finnish") ("english") ("dutch") ("danish") ("catalan") ("basque") ("armenian") ("arabic"))))))

