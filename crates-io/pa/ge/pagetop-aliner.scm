(define-module (crates-io pa ge pagetop-aliner) #:use-module (crates-io))

(define-public crate-pagetop-aliner-0.0.1 (c (n "pagetop-aliner") (v "0.0.1") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1gkag9dhw1qcgfyblgq1s5qkvw99d4gpm4yd800m58ma63mnvf9w")))

(define-public crate-pagetop-aliner-0.0.2 (c (n "pagetop-aliner") (v "0.0.2") (d (list (d (n "maud") (r "^0.24.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "17pw5x6mviy2cvmmvprdyxxlknbda934l768ma06zb9kcydg7fby")))

(define-public crate-pagetop-aliner-0.0.3 (c (n "pagetop-aliner") (v "0.0.3") (d (list (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "00nk4qxcgprxrrzn4nxr140r1b9lvrrsqry1s3igbg78mkpv2bsw")))

(define-public crate-pagetop-aliner-0.0.4 (c (n "pagetop-aliner") (v "0.0.4") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0c6qlm7cq173860hjldb0v8mskgymaxwrdja9imxkslr47vxv7md")))

(define-public crate-pagetop-aliner-0.0.5 (c (n "pagetop-aliner") (v "0.0.5") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0kazs051i6gga74052pdblk8r9rssna7z7apcg2rmz7lm0vbg0zm")))

(define-public crate-pagetop-aliner-0.0.6 (c (n "pagetop-aliner") (v "0.0.6") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0bmb6rcpgqmkhkb3gncqggapq8fnbwjqjz3dfjpgsn3lcipwgqkr")))

(define-public crate-pagetop-aliner-0.0.7 (c (n "pagetop-aliner") (v "0.0.7") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0rkfmk9y587crzady7kbbd2y3fxgxlpnpb9b125bn6ah986ys3r5")))

(define-public crate-pagetop-aliner-0.0.8 (c (n "pagetop-aliner") (v "0.0.8") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "093f4krv68d9qc3fp4nqrqdizaz73l00z6v08g59n4bj950anw22")))

(define-public crate-pagetop-aliner-0.0.9 (c (n "pagetop-aliner") (v "0.0.9") (d (list (d (n "pagetop") (r "^0.0") (d #t) (k 0)) (d (n "pagetop-build") (r "^0.0") (d #t) (k 1)) (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0hi1ibpx655yd30w5nnfwrdncvm0g41maa76rgfk2hsw9dg86pa9")))

