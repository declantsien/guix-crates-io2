(define-module (crates-io pa ge pagetop-build) #:use-module (crates-io))

(define-public crate-pagetop-build-0.0.1 (c (n "pagetop-build") (v "0.0.1") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1z6yqv2vqvp8ahy1jckqdkgyqmx74752fgzhrsvb6grdqn0vxv8h")))

(define-public crate-pagetop-build-0.0.2 (c (n "pagetop-build") (v "0.0.2") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "054m8xfj629gm6ijzhj19fkr9a714fh9jr7x25sax6kl7fhyqf12")))

(define-public crate-pagetop-build-0.0.3 (c (n "pagetop-build") (v "0.0.3") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "08imbwqk5wlha2i21w86px7s3cf3ma3rkb7fj9541z17wx1jd6rf")))

(define-public crate-pagetop-build-0.0.4 (c (n "pagetop-build") (v "0.0.4") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1w23p7qjbbdr00k8qg05jx1fx59ialxqnmw94if7ssv10nv00z4p")))

(define-public crate-pagetop-build-0.0.5 (c (n "pagetop-build") (v "0.0.5") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "09lhb0vj5s5yppyj2iqsv8k7s2x1rszsm46l79glgha643f9351q")))

(define-public crate-pagetop-build-0.0.6 (c (n "pagetop-build") (v "0.0.6") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1haqmz1085ldkmry4x1mfg9snlddp473myv2ryazqlqrkjq3gg9i")))

(define-public crate-pagetop-build-0.0.7 (c (n "pagetop-build") (v "0.0.7") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "0pdzmi4yismrdrzqnfxrax9harp48qirkvpg6ypq4sjazh7fq58x")))

(define-public crate-pagetop-build-0.0.8 (c (n "pagetop-build") (v "0.0.8") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "1s4rx2g5v7dxlvn8p1s3s9l29nzjn7iwdm5wq7m8lcwhp3dj3xsd")))

(define-public crate-pagetop-build-0.0.9 (c (n "pagetop-build") (v "0.0.9") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "09q8hymhrb34pnazmvmlasjhnsbwhl8nfc437j5x2a77bkwnlyxc")))

(define-public crate-pagetop-build-0.0.10 (c (n "pagetop-build") (v "0.0.10") (d (list (d (n "static-files") (r "^0.2.3") (d #t) (k 0)))) (h "09sc3ynbxn7a23vmyc809vpx89xj6db2vh49857vc0bxlj6nk1bz")))

