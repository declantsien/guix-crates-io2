(define-module (crates-io pa ge page_size) #:use-module (crates-io))

(define-public crate-page_size-0.1.0 (c (n "page_size") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1sy7y7b1llih5y3r4blcx9mhj144qd9xnj82xn1h6rs36wnggjkw") (f (quote (("no-std" "spin"))))))

(define-public crate-page_size-0.1.1 (c (n "page_size") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1svid8xf384py9kblcpxlz547y9308vak0rh4wday31irwgz2s42") (f (quote (("no-std" "spin"))))))

(define-public crate-page_size-0.2.0 (c (n "page_size") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "162q86r25lf7winjp1p8j35afi14qaw8qigs8hlywpggnrvm40yj") (f (quote (("no-std" "spin"))))))

(define-public crate-page_size-0.2.1 (c (n "page_size") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "14942c7wipqvj4f6pfg2wjmjnrkxgzbbh86zp8fipaq3lxrj2wsg") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.3.0 (c (n "page_size") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0lxpi7p0saby50a0chr3sl95h17z3xs295ch0zx0mlsk077ah182") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.4.0 (c (n "page_size") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.31") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1wkb3nzjppvn68l1vlcnbqap11kkb1m5j0xbg11x77mpzql9spri") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.4.1 (c (n "page_size") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0hkjlvslam55kjq6lmdv2bpkjqigx65g7lj33ayhshij7n5zb7pq") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.4.2 (c (n "page_size") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kgdv7f626jy4i2pq8czp4ppady4g4kqfa5ik4dah7mzzd4fbggf") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.5.0 (c (n "page_size") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qdm2b6h63bwxy8s6jccd1v30f6dysbq9ylfs0cdikwhs75n6xhv") (f (quote (("no_std" "spin"))))))

(define-public crate-page_size-0.6.0 (c (n "page_size") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nj0rrwpvagagssljbm29ww1iyrrg15p1q4sk70r2cfi9qcv5m9h") (f (quote (("no_std" "spin"))))))

