(define-module (crates-io pa ge pagersduty) #:use-module (crates-io))

(define-public crate-pagersduty-0.1.0 (c (n "pagersduty") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)))) (h "1f5w8r4hq7xb7xc09vlrzj53xxl2cddjcm9yy7skvnb98f73sv2z")))

(define-public crate-pagersduty-0.2.0 (c (n "pagersduty") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zyrn81j7lc65rdxjsin8mmwj6qbzwwzc2v0fkfc0fi76470fdks")))

