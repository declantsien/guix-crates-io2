(define-module (crates-io pa tu patum) #:use-module (crates-io))

(define-public crate-patum-0.1.6 (c (n "patum") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1hxgsx2g6yjnhhvqs0igbrxh0r4j23l1rv9z2a62k2j40q8rh7aw")))

