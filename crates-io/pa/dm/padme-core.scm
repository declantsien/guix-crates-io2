(define-module (crates-io pa dm padme-core) #:use-module (crates-io))

(define-public crate-padme-core-0.0.1 (c (n "padme-core") (v "0.0.1") (d (list (d (n "enum_dispatch") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)))) (h "1nybfjh2lvlvfiywdn3m1frdbaq1l31b8s6z8s8imqhpy8qg6b0n")))

