(define-module (crates-io pa no panoptes) #:use-module (crates-io))

(define-public crate-panoptes-0.0.1-rc0 (c (n "panoptes") (v "0.0.1-rc0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "13mxamjg8z7fwqwizggnfw9dqv8pd629m83bpb7ygy9ci9acfv35")))

(define-public crate-panoptes-0.0.2 (c (n "panoptes") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1j0n99c35r15kwji3882ah937ywibdjqpm0njb6yma8q746q9vzx")))

