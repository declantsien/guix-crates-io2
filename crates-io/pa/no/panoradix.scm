(define-module (crates-io pa no panoradix) #:use-module (crates-io))

(define-public crate-panoradix-0.1.0 (c (n "panoradix") (v "0.1.0") (h "1ichwv90i6j9a0a0k934psmzhlwp1629i71labsy7w6f3m4zq8wi")))

(define-public crate-panoradix-0.2.0 (c (n "panoradix") (v "0.2.0") (h "0lal2xwkmknidsa3gfsqdfjxyykkpm1qpyzqw08kazw7fsg3xfp5")))

(define-public crate-panoradix-0.3.0 (c (n "panoradix") (v "0.3.0") (h "1r5qxv7yppchxyp060p6ayyyrmqk7pr09p65ww4hcxbbi7qhyl2w")))

(define-public crate-panoradix-0.3.1 (c (n "panoradix") (v "0.3.1") (h "16wxkamcys7snfs1b0j2w140q9dhxil8qwvgvw8nvc0jkfi4d66v")))

(define-public crate-panoradix-0.3.2 (c (n "panoradix") (v "0.3.2") (h "017r9hw7yjlllnrcv12fpa2ih0xj40x18h37aphpj7mza5rns1q2")))

(define-public crate-panoradix-0.3.3 (c (n "panoradix") (v "0.3.3") (h "1p0r61ciyis76rnna7i4hh4zkqcncs6zdbccg6xhyqxxdh5lgkfv")))

(define-public crate-panoradix-0.3.4 (c (n "panoradix") (v "0.3.4") (h "03446gy6hpw9f9p7h1s0kld049xxl4mqssh0mpnc1664s97lmq1g")))

(define-public crate-panoradix-0.3.5 (c (n "panoradix") (v "0.3.5") (h "0d3jd9z3kv1x7cv6g8pq1546hdk37cylnmdmnnjggb96vz61qwfw")))

(define-public crate-panoradix-0.4.0 (c (n "panoradix") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0x1wdpgnh2rgrkqjjqh259lmvx0jl4kr77wihbms1yw5x9k9w5wy") (f (quote (("default"))))))

(define-public crate-panoradix-0.5.0 (c (n "panoradix") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0iirh9dmnjvzsbs37kr97aa51dw2ks0z73ji74jmfc2n7c3k5l5n") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.0 (c (n "panoradix") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "07darr7zgmq4xfmmmcwzz4ai7xjmrbb3dmayk760b2z2lcda3bb0") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.1 (c (n "panoradix") (v "0.6.1") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0w6k007wypsbmsravk6b7h0ssjy8sipk35cz5487jczswxnwzxzj") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.2 (c (n "panoradix") (v "0.6.2") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "1xf6apj843gs7140xb98z501lyfxd2yx78z1jybks4fxqvjjvrz0") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.3 (c (n "panoradix") (v "0.6.3") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0yda4n5zqq365wnx8v90r7j8lr3lylazzbjpkdyya3hdjsjv6ws0") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.4 (c (n "panoradix") (v "0.6.4") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0gzvrp8z443zfkn9lf9jn9jax1zqpmlljw8r3m6iwbb6awcwapi4") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.5 (c (n "panoradix") (v "0.6.5") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0rp8vybh51lc67kmvs0ib86rhff0v3vkrwqvjfynfkayg3wfvcrp") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.6 (c (n "panoradix") (v "0.6.6") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "0cibyiqbkr7ih4nqaglghafbn286i0ym481hpc6h2w3wv0vygqpb") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.7 (c (n "panoradix") (v "0.6.7") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)))) (h "1vspavgw2c4swjbycd3xbxhi5qfmbyq8axpd2nb09g9rfpl2kw1z") (f (quote (("default"))))))

(define-public crate-panoradix-0.6.8 (c (n "panoradix") (v "0.6.8") (h "1w25rf1kvm6igqxcqaqhihz100pjqpdcwlazfsfjknixj4q8xndj") (f (quote (("default"))))))

