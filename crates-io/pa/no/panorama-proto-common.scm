(define-module (crates-io pa no panorama-proto-common) #:use-module (crates-io))

(define-public crate-panorama-proto-common-0.0.1 (c (n "panorama-proto-common") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "format-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)))) (h "1r25d84y8wkhfg0pkj94119xggdf8pfh4jc24ynk49d4ddyyjma0")))

