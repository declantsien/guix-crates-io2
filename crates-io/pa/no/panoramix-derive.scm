(define-module (crates-io pa no panoramix-derive) #:use-module (crates-io))

(define-public crate-panoramix-derive-0.0.1 (c (n "panoramix-derive") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)))) (h "03wnp7dhrv3fy4w9ka5m4np26i7xcjbqf9dy1b99iilkfdk95jkm")))

(define-public crate-panoramix-derive-0.0.2 (c (n "panoramix-derive") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "panoramix") (r "^0.0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)))) (h "03xh6nv08vp0swwz8qfdg53xndi6hs5206x3zq7l5x0bysb9n0ry")))

