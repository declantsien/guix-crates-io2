(define-module (crates-io pa ni panic-rtt-target) #:use-module (crates-io))

(define-public crate-panic-rtt-target-0.1.0 (c (n "panic-rtt-target") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.1.1") (d #t) (k 0)))) (h "1d6yr1qrqd3rrmjwh5vjaaciay6k9w6rynxminr66kc3imfyqgdb")))

(define-public crate-panic-rtt-target-0.1.1 (c (n "panic-rtt-target") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.1") (d #t) (k 0)))) (h "101i8hxbvimpnhjp5r48dmg966xb1f7l8c85rmysvpjyrzn4vkfg")))

(define-public crate-panic-rtt-target-0.1.2 (c (n "panic-rtt-target") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (d #t) (k 0)))) (h "1sfwavv406npgzl1c6mwhzm70dihq5bwcn7rj163wic1r1xvcshd")))

(define-public crate-panic-rtt-target-0.1.3 (c (n "panic-rtt-target") (v "0.1.3") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "rtt-target") (r "^0.5.0") (d #t) (k 0)))) (h "1znbn6r7lnl897hfm6cbv49hla5bqxwi5634hdg0v5nqkn01v3b0")))

