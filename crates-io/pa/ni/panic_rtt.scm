(define-module (crates-io pa ni panic_rtt) #:use-module (crates-io))

(define-public crate-panic_rtt-0.1.0 (c (n "panic_rtt") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "jlink_rtt") (r "~0.1") (d #t) (k 0)))) (h "0562jgqnq9chcxz95byj4sjxf0bzfpaf1yw08k63s7fisp2lc1h6")))

(define-public crate-panic_rtt-0.2.0 (c (n "panic_rtt") (v "0.2.0") (d (list (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "jlink_rtt") (r "~0.1") (d #t) (k 0)))) (h "1byzs5ng6j9spmvsfq5bs3qzs1kydmr412v2kc8l67j90m5fh5gz")))

(define-public crate-panic_rtt-0.3.0 (c (n "panic_rtt") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "jlink_rtt") (r "~0.2") (d #t) (k 0)))) (h "10pb66i2p1kdz302n4b71i41zyzj7yfn2a4fkwywk3shpaml7nzq")))

