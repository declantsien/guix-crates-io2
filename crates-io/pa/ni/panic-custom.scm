(define-module (crates-io pa ni panic-custom) #:use-module (crates-io))

(define-public crate-panic-custom-0.1.0 (c (n "panic-custom") (v "0.1.0") (d (list (d (n "panic-custom-proc-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "08mck6wz2ydq2vcyri1g83p0ssyp4arh4yvcgzlqy1b856kiwv8j") (f (quote (("proc_macros" "panic-custom-proc-macros") ("abort_on_release") ("abort_on_debug"))))))

(define-public crate-panic-custom-0.1.1 (c (n "panic-custom") (v "0.1.1") (d (list (d (n "panic-custom-proc-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "147dzk52gk1jzp017fyzxj8068yl2qkdd194j4ipapd5vzdv1vjl") (f (quote (("proc_macros" "panic-custom-proc-macros") ("abort_on_release") ("abort_on_debug"))))))

