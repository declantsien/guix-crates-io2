(define-module (crates-io pa ni panini_macros) #:use-module (crates-io))

(define-public crate-panini_macros-0.0.0 (c (n "panini_macros") (v "0.0.0") (d (list (d (n "aster") (r "^0.16") (d #t) (k 0)) (d (n "bit-matrix") (r "^0.2") (d #t) (k 0)) (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0") (d #t) (k 2)) (d (n "enum_stream_codegen") (r "^0.0") (d #t) (k 0)) (d (n "gearley") (r "^0.0") (d #t) (k 0)) (d (n "panini") (r "^0.0") (d #t) (k 0)) (d (n "panini_codegen") (r "^0.0") (d #t) (k 0)) (d (n "panini_macros_snapshot") (r "^0.0") (d #t) (k 0)) (d (n "quasi") (r "^0.10") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.10") (d #t) (k 0)))) (h "15l821bd18wj6rfgjhiw05f5gb09swcx0zb4mdw790nk0knnv2x8")))

