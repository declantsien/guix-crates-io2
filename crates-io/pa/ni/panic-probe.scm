(define-module (crates-io pa ni panic-probe) #:use-module (crates-io))

(define-public crate-panic-probe-0.0.0 (c (n "panic-probe") (v "0.0.0") (h "0zfilbb2dknhr3x7a3cy03bgy8p8w7wmr4al7anh2n2c47jkqpzd")))

(define-public crate-panic-probe-0.1.0 (c (n "panic-probe") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "defmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1w5mzv4nf9yr060xd7z8slvqlzqkp1057b1i5dblckzdczgnmq0d") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.2.0 (c (n "panic-probe") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1p60wq68am4pk0aqhjngsjdb5zyzk32hvcs0paribx2idcfwida5") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.2.1 (c (n "panic-probe") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1japj39ya7gf7q7mrizyi93pb9v51lpq0wyk1bbx0w2wylka43va") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3.0 (c (n "panic-probe") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0fngdpm4cdr07vp5dddmcv0s1cr051gmbsfawpw1ig92mh7g1c9s") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3.1 (c (n "panic-probe") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1fajcv95jjx2v0k8jzwyq3433q8wkjpr5ahf6k6n19zmbrjaavxa") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3.2 (c (n "panic-probe") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rtt-target") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1c159xprb4q7dmf804vc9lalykdmvmzd19vxr5ndc8qlblixjis0") (f (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

