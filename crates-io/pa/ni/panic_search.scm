(define-module (crates-io pa ni panic_search) #:use-module (crates-io))

(define-public crate-panic_search-0.1.0 (c (n "panic_search") (v "0.1.0") (d (list (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "18sm357mjs4q8fxz34zfp66xw62228rmp7gk697b9073vlmpkfgq")))

(define-public crate-panic_search-0.1.1 (c (n "panic_search") (v "0.1.1") (d (list (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "1ijii50g124a65gip1vj01pbp868yhwp6bsdmcrjpqxslwral78r")))

(define-public crate-panic_search-0.1.2 (c (n "panic_search") (v "0.1.2") (d (list (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "1b11fqpqnszcvga3m6qfibhiyryp7c2pjkgblh0lynh2an3rgwzb")))

(define-public crate-panic_search-0.1.3 (c (n "panic_search") (v "0.1.3") (d (list (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "08b9xc4jmf2icvip1b9dmgj0wavi85crjgq5268bqq2ylgpv088l")))

(define-public crate-panic_search-0.1.4 (c (n "panic_search") (v "0.1.4") (d (list (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "1ycf2c5agrab2k718cfibw95a3dsd4zhjna6721ykhb6mai4p3b9")))

