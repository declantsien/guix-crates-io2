(define-module (crates-io pa ni panim-loader) #:use-module (crates-io))

(define-public crate-panim-loader-0.1.0 (c (n "panim-loader") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1q531ra6nmw8xndl6r3xsanmr7syp8j9w1b530q8sm9gy0m1khds")))

(define-public crate-panim-loader-0.1.1 (c (n "panim-loader") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0r8fixkfx6rib8jw4grh0c55ygnvfv2na8lrab05qlhjc0x7i7v6")))

(define-public crate-panim-loader-0.2.0 (c (n "panim-loader") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0mx2kdba80ajfp92g6qbssizqng1vz7rs7fz4f1skn7xj252yz60")))

(define-public crate-panim-loader-0.2.1 (c (n "panim-loader") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0g6rf6rc2rs5ygl9lmsa8d3rlp4v257n7iij3n7fgsnx4g6i5fng")))

(define-public crate-panim-loader-0.2.2 (c (n "panim-loader") (v "0.2.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "06wj9q9ngy8bz3iyz656klimbkrxjyk9k9xvzwwzdbh65nbg3982")))

(define-public crate-panim-loader-0.2.3 (c (n "panim-loader") (v "0.2.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "04cbjbgxfxhw097bxw8bwh3q0dngdxdah0hv3njc3ddd552mvwjn")))

