(define-module (crates-io pa ni panic-context) #:use-module (crates-io))

(define-public crate-panic-context-0.1.0 (c (n "panic-context") (v "0.1.0") (d (list (d (n "gag") (r "^0.1.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0rqmhy61x1x6xbplw9nlbi5202jbx9sazmwqc8hl8yma6krnvl9c") (f (quote (("keep-debug-context"))))))

