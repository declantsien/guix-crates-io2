(define-module (crates-io pa ni panic-ramdump) #:use-module (crates-io))

(define-public crate-panic-ramdump-0.1.0 (c (n "panic-ramdump") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)))) (h "0klfj164i09d5nkzm7mijh6lp52z2jch1slg5sqdx1z97a65z7k9")))

(define-public crate-panic-ramdump-0.1.1 (c (n "panic-ramdump") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)))) (h "054cj4zbxy17xpcms2y8xb1fwiw8vmpwknrqkrcwpk3rnm7v2ygr")))

