(define-module (crates-io pa ni panic-handler) #:use-module (crates-io))

(define-public crate-panic-handler-0.0.0 (c (n "panic-handler") (v "0.0.0") (h "0nfq4fb4sarxr3n7153y2kp365v40zx8rzii40iakn3618wfxd1j") (y #t)))

(define-public crate-panic-handler-2.0.1 (c (n "panic-handler") (v "2.0.1") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)))) (h "15x1qwfhhm1y3bw8z8hislddh17374irwriwhbq5lqa3bvj26i1i")))

(define-public crate-panic-handler-2.0.2 (c (n "panic-handler") (v "2.0.2") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)))) (h "14grxda01bdl9wq9zkp0iv85x1rk073147mbhdfa5vq4r8x1pjm0")))

(define-public crate-panic-handler-2.1.2 (c (n "panic-handler") (v "2.1.2") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)))) (h "07qiazg6a0rbkcg0adwzrf56sazsy39b0qfa65gmzj8j87r231vc")))

