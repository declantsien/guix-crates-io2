(define-module (crates-io pa ni panic-serial) #:use-module (crates-io))

(define-public crate-panic-serial-0.1.0 (c (n "panic-serial") (v "0.1.0") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "083bdmn9qri0by6rfxl6zaw6x6ygjp1b6352b53a0928rj3x6qwi") (f (quote (("message") ("location") ("full" "location" "message"))))))

(define-public crate-panic-serial-0.1.1 (c (n "panic-serial") (v "0.1.1") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1rl4088zclyqb81mcdrzml7sfbqhbkpxdxysvinnrff1dagp0sqv") (f (quote (("message") ("location") ("full" "location" "message"))))))

(define-public crate-panic-serial-0.1.2 (c (n "panic-serial") (v "0.1.2") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1ndga3j7kp5nwfn2qfdrsdxbixwqc1ws5n6q5nimx0mhb06wdby3") (f (quote (("message") ("location") ("full" "location" "message"))))))

