(define-module (crates-io pa ni panic-persist) #:use-module (crates-io))

(define-public crate-panic-persist-0.2.0 (c (n "panic-persist") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)))) (h "0wqr44h02znh0qf1ryq4i5xwnd4h970cq2hra5jsnqv81zx9zijl") (f (quote (("utf8") ("default"))))))

(define-public crate-panic-persist-0.2.1 (c (n "panic-persist") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)))) (h "0hpfypb9bb84ih9iyljib3c7haikqissmnzm2q6fpqslb4m6jbnx") (f (quote (("utf8") ("default"))))))

(define-public crate-panic-persist-0.3.0 (c (n "panic-persist") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)))) (h "1mczyfpgirn0ih0i3jhwnfvl73ds01kxb7yzsk9c9lpci4k3ifrj") (f (quote (("utf8") ("min-panic") ("default") ("custom-panic-handler"))))))

