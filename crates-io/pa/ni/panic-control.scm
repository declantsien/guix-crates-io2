(define-module (crates-io pa ni panic-control) #:use-module (crates-io))

(define-public crate-panic-control-0.1.0 (c (n "panic-control") (v "0.1.0") (h "0z4b6sgxfp9i0y44dhchikrr2qyql0jjv33drnbj4bza7hdk6fqg")))

(define-public crate-panic-control-0.1.1 (c (n "panic-control") (v "0.1.1") (h "0hyflgwjdj1412hhqap0vsikmvxkgn6rykgrizykfsnh8bf344j0")))

(define-public crate-panic-control-0.1.2 (c (n "panic-control") (v "0.1.2") (h "1i1q8m4y08vgh6ym2z64g1r5zylz3s2155vzq15dr42qlmgvxnvf")))

(define-public crate-panic-control-0.1.3 (c (n "panic-control") (v "0.1.3") (h "0dxjizvsav2azln539mib2sfr66g9l552il2c193i1fi9zz0l8d3")))

(define-public crate-panic-control-0.1.4 (c (n "panic-control") (v "0.1.4") (h "1iyng766mbh611yf6jd902039dp3c3qknaq434369d23pjp7768m")))

