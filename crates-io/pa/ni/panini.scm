(define-module (crates-io pa ni panini) #:use-module (crates-io))

(define-public crate-panini-0.0.0 (c (n "panini") (v "0.0.0") (d (list (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "gearley") (r "^0.0") (d #t) (k 0)) (d (n "ref_slice") (r "^1.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.2") (d #t) (k 0)))) (h "0r61sylz44qi2pj6ss88qn3d1yqhlwwiqgx75cy15hbg4fnkaddx")))

