(define-module (crates-io pa ni panic-analyzer) #:use-module (crates-io))

(define-public crate-panic-analyzer-0.1.0 (c (n "panic-analyzer") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0x8y7f0xr2qhhnzkqczmv67hg0vp8p38k4wr3ssxb1ll6mvjp3zj")))

(define-public crate-panic-analyzer-0.1.1 (c (n "panic-analyzer") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "14lxawrk7r3n1z2p3qcr09kmbn2svlf2bgpg9pdi5s07yh953z9i")))

(define-public crate-panic-analyzer-0.1.2 (c (n "panic-analyzer") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1dv8sfcnaf92pmrj20a8xybsdlgn3rqxzmkvxzgqziwybrqmyp7p")))

(define-public crate-panic-analyzer-0.1.3 (c (n "panic-analyzer") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "05bl3h8f90c58j3a6sb77qbynzvhh6chcanh1176ircabrh0awpz")))

(define-public crate-panic-analyzer-0.1.4 (c (n "panic-analyzer") (v "0.1.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1z6586kdwbnvs42ac5ciqzph9a7w2y05kpaar1nd4a276dks8zy3")))

