(define-module (crates-io pa ni panic-itm) #:use-module (crates-io))

(define-public crate-panic-itm-0.1.0 (c (n "panic-itm") (v "0.1.0") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)))) (h "0zyy36zmrl2v4w0qmhsrymb4cbb31076abjkm97k0apgvnnkh42z")))

(define-public crate-panic-itm-0.1.1 (c (n "panic-itm") (v "0.1.1") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)))) (h "1nsxc4vcar8xrhqdqqsiafbs8951qir5rdy7wljvbalinl99fq0w")))

(define-public crate-panic-itm-0.2.0 (c (n "panic-itm") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)))) (h "025vh88nyq094kpilvgzwlv7aw4f81kdp3sa8g965a9bqs11nmvb")))

(define-public crate-panic-itm-0.3.0 (c (n "panic-itm") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)))) (h "0mkz7x6bx07bramzzhq7rc1dqjfwqwxv34l97m4n2bn50jvpjs41")))

(define-public crate-panic-itm-0.4.0 (c (n "panic-itm") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)))) (h "0rl6hfbpq581zk1mxgca45h20f6rnw28cn1l9i5xgfardy6mby6j")))

(define-public crate-panic-itm-0.4.1 (c (n "panic-itm") (v "0.4.1") (d (list (d (n "cortex-m") (r ">= 0.5.6, < 0.7") (d #t) (k 0)))) (h "1hvcsrknl94qmhw3a3jb2g72dls3n44k1apd85z211smm4bhv0wq")))

(define-public crate-panic-itm-0.4.2 (c (n "panic-itm") (v "0.4.2") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "0k0s76aicj6hzjkxrnc7xrgfz5371r3z5pbdgc46h4mks6bpsmrx")))

