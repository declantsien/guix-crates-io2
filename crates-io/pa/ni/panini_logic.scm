(define-module (crates-io pa ni panini_logic) #:use-module (crates-io))

(define-public crate-panini_logic-0.0.0 (c (n "panini_logic") (v "0.0.0") (d (list (d (n "bit-matrix") (r "^0.4") (d #t) (k 0)) (d (n "cfg") (r "^0.4") (d #t) (k 0)) (d (n "cfg-regex") (r "^0.1") (d #t) (k 0)) (d (n "enum_coder") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gearley") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)))) (h "0b1ripqfajzscdnniww0r5azpzpvb070qqa8ryvqh9571s92vlk1")))

