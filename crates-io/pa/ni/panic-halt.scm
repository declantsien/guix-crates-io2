(define-module (crates-io pa ni panic-halt) #:use-module (crates-io))

(define-public crate-panic-halt-0.1.0 (c (n "panic-halt") (v "0.1.0") (h "1p0p6nfi23pvbxfwd5r2jcnizv39y3fxmk32k72jg707x9c4hkfd")))

(define-public crate-panic-halt-0.1.1 (c (n "panic-halt") (v "0.1.1") (h "1asj7g021clsicdvc5yyg6lwirhjlba4kzd9ir8pckjq8a3rnhyp")))

(define-public crate-panic-halt-0.1.2 (c (n "panic-halt") (v "0.1.2") (h "1v8srsn6hykj37ha61j7ffr0lbgba5j2ds1yvcsfwmzdjgccfqkb")))

(define-public crate-panic-halt-0.1.3 (c (n "panic-halt") (v "0.1.3") (h "138khyhq59s98fb2kpv0is5c3ihs3ax7bbpzrcz9b2lmyg2im3g7")))

(define-public crate-panic-halt-0.2.0 (c (n "panic-halt") (v "0.2.0") (h "04nqaa97ph20ppyy58grwr23hrbw83pn0gf7apf73rdx1q7595ny")))

