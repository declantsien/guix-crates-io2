(define-module (crates-io pa ni panic-usb-boot) #:use-module (crates-io))

(define-public crate-panic-usb-boot-0.1.0 (c (n "panic-usb-boot") (v "0.1.0") (d (list (d (n "rp2040-hal") (r "^0.8.2") (d #t) (k 0)))) (h "1vkq1gysf18wl928m7nkyz61a2nc5lzgdlsmacx378qgb51hp34n") (f (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

(define-public crate-panic-usb-boot-0.2.0 (c (n "panic-usb-boot") (v "0.2.0") (d (list (d (n "rp2040-hal") (r "^0.9.0") (d #t) (k 0)))) (h "1vly39nvxghqs4nsc2qh3c2v66ffgzbnh72clxask5krnl33liiz") (f (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

(define-public crate-panic-usb-boot-0.3.0 (c (n "panic-usb-boot") (v "0.3.0") (h "0ni63kzavi7hwlfk0znwr7h6lgsj9ga191r9vcnfkjgjckcwxm2f") (f (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

