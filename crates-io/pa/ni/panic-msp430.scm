(define-module (crates-io pa ni panic-msp430) #:use-module (crates-io))

(define-public crate-panic-msp430-0.1.0 (c (n "panic-msp430") (v "0.1.0") (d (list (d (n "msp430") (r "^0.1.0") (d #t) (k 0)))) (h "0ww20vfp4asbm89b0y0wlyajflm9qsmp9w6nx4a7qrcldl1zbzbf")))

(define-public crate-panic-msp430-0.2.0 (c (n "panic-msp430") (v "0.2.0") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)))) (h "0klq06i8ar8imirgddrw5yqvph45vfvbgxh97igchmb9sdlmcg2d")))

(define-public crate-panic-msp430-0.3.0 (c (n "panic-msp430") (v "0.3.0") (d (list (d (n "msp430") (r "^0.3.0") (d #t) (k 0)))) (h "04piaan0vn6pgcdgdqp0hpznpgl8j1zii6hpdyxh8jwxx3n7i77r")))

(define-public crate-panic-msp430-0.4.0 (c (n "panic-msp430") (v "0.4.0") (d (list (d (n "msp430") (r "^0.4.0") (d #t) (k 0)))) (h "15rw7xcwknyg68lh7rirbh45h9i7a72glqgdbj5q8g2p9sm32kc4")))

