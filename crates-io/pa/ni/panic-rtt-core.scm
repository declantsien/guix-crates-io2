(define-module (crates-io pa ni panic-rtt-core) #:use-module (crates-io))

(define-public crate-panic-rtt-core-0.1.0 (c (n "panic-rtt-core") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "rtt-target") (r "^0.1.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0n6cyxwgjjpyv9ibryl1f0r2bnh7ywnx2hbzw2pi8b5gwrq5g60i")))

(define-public crate-panic-rtt-core-0.1.1 (c (n "panic-rtt-core") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "rtt-target") (r "^0.1.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "1wpaxq4v15bivz4shmx0sphgi3fdfjvdwyg3r8ihr9flfgf1sfhn")))

(define-public crate-panic-rtt-core-0.2.1 (c (n "panic-rtt-core") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.2") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "1jzsizbml7vqn9nwwzlsbqc1xsm86x1hk4ggmw5f8b5j7xwr9mf7")))

