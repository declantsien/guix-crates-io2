(define-module (crates-io pa ni panic-custom-proc-macros) #:use-module (crates-io))

(define-public crate-panic-custom-proc-macros-0.1.0 (c (n "panic-custom-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dsxs6c3linm3c8fg3y1vm9y9dqhcwgnwgqmsghqwa7jynh64nzw")))

