(define-module (crates-io pa ni panic-reset) #:use-module (crates-io))

(define-public crate-panic-reset-0.1.0 (c (n "panic-reset") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)))) (h "19d8lhkm630ay9il8c35narjqi8h8wl8gvk11p3wb1w4lcsqbvmc")))

(define-public crate-panic-reset-0.1.1 (c (n "panic-reset") (v "0.1.1") (d (list (d (n "cortex-m") (r ">0.6, <0.8") (d #t) (k 0)))) (h "1xd4lc1kkb83a2c1dl6bf4hffc36dd3l7akj8pcqsiqsbcmgzwbc")))

