(define-module (crates-io pa ni panic-message) #:use-module (crates-io))

(define-public crate-panic-message-0.2.0 (c (n "panic-message") (v "0.2.0") (h "097kkvlgdh7i8963ddq972xdaqpl4p9gls7sp73yflsrvq0cf7fn")))

(define-public crate-panic-message-0.3.0 (c (n "panic-message") (v "0.3.0") (h "0ba75hapwknxljlcw2719l9zk8111hk1d0ky64ybwk5xizym4kiq")))

