(define-module (crates-io pa ni panicui) #:use-module (crates-io))

(define-public crate-panicui-0.1.0 (c (n "panicui") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3.27") (d #t) (k 0)))) (h "0slw5rrdk26v7mkmv5wc8c5h7f54lk28c60pmlrr0zzxb7zikvfl") (f (quote (("shared" "fltk/fltk-shared") ("default" "bundled") ("bundled" "fltk/fltk-bundled"))))))

