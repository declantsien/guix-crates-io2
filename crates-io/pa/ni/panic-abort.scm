(define-module (crates-io pa ni panic-abort) #:use-module (crates-io))

(define-public crate-panic-abort-0.1.0 (c (n "panic-abort") (v "0.1.0") (h "1jlywsldk0h5n2dakjsqkq6sw5klfyykzfy8g99hff6rmznyca06")))

(define-public crate-panic-abort-0.1.1 (c (n "panic-abort") (v "0.1.1") (h "0birc161hd6nf66n1lbb3kdf206vrv46yr2aybcs49ql64q3qmbm")))

(define-public crate-panic-abort-0.2.0 (c (n "panic-abort") (v "0.2.0") (h "0gppi8v07s4dwgxiksfq1ypyaynn7x9miig7zza5cw7j4339divb")))

(define-public crate-panic-abort-0.3.0 (c (n "panic-abort") (v "0.3.0") (h "1i5x06imydb8r3l05f4qxwcp2fbn2v3zpyxwm6bpwv67x3l5pn65")))

(define-public crate-panic-abort-0.3.1 (c (n "panic-abort") (v "0.3.1") (h "03v3gvwcmnwma7m5xc8xi5nq61yjgy6qcsr5njlbc5zd25jsc51c")))

(define-public crate-panic-abort-0.3.2 (c (n "panic-abort") (v "0.3.2") (h "1vkqqwsgylnpgcmx4prxfnh6yv1mdcs44jmhh3r15i5vkd4yc82f")))

