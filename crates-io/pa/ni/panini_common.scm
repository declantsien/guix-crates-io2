(define-module (crates-io pa ni panini_common) #:use-module (crates-io))

(define-public crate-panini_common-0.0.0 (c (n "panini_common") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l2irvxc12jz1v4ilrvvq354saal55rqdpahdx1wc90bbjvxcmp5")))

