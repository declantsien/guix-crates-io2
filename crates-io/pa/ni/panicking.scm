(define-module (crates-io pa ni panicking) #:use-module (crates-io))

(define-public crate-panicking-0.0.1 (c (n "panicking") (v "0.0.1") (h "1dvpz44f3kzv29h3f4lxyk4z1b6nrhl4hj3kf51k2606rrkzilhy") (f (quote (("std") ("default" "std"))))))

(define-public crate-panicking-0.0.2 (c (n "panicking") (v "0.0.2") (h "0gsjg4a1kyk6lh1c4cpyq5vpg4rm8mr4q7mh3jdzdr8lzfvw6nqw") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-panicking-0.1.0 (c (n "panicking") (v "0.1.0") (h "1nkfrlcmsigqi9zv79i5vv7znxlvjfrmzdx8pxppicac6z31f3p2") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-panicking-0.1.1 (c (n "panicking") (v "0.1.1") (h "04ivn6sg3c0f63bs1nfcpl6pix9y2z083c9k6isbv4mx6m5i2j6l") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-panicking-0.1.2 (c (n "panicking") (v "0.1.2") (h "0i0kl14w1gmafih6rb8krllim2pxri9l8zz3sqd9sc1xvqzi6cgj") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-panicking-0.2.0 (c (n "panicking") (v "0.2.0") (h "1wvc7dcx819a8vyypn38p27fah5xmnmjylgz8kxjrvq6pxjxcjkr") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-panicking-0.3.0 (c (n "panicking") (v "0.3.0") (h "0iwfarkivp4b8vh3f4v3mj505jgiz631yngrl8bj7hynkdrqg8vl") (f (quote (("std") ("default" "std") ("abort")))) (y #t) (r "1.59")))

(define-public crate-panicking-0.3.1 (c (n "panicking") (v "0.3.1") (h "1zvyj4dnvkf7zmnrfw2gsr2qf2v33f1d4x6kj83mxhia1kp73r1z") (f (quote (("std") ("default" "std") ("abort")))) (r "1.59")))

(define-public crate-panicking-0.4.0 (c (n "panicking") (v "0.4.0") (h "1pl0njhi94xyqxghsqzsy90l0bqylx6ff5s6px1zkbb7sr65ksr1") (f (quote (("std") ("default" "std") ("abort")))) (r "1.59")))

