(define-module (crates-io pa ni panini_codegen) #:use-module (crates-io))

(define-public crate-panini_codegen-0.0.0 (c (n "panini_codegen") (v "0.0.0") (d (list (d (n "aster") (r "^0.16") (d #t) (k 0)) (d (n "bit-matrix") (r "^0.2") (d #t) (k 0)) (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "cfg-regex") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "gearley") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quasi") (r "^0.10") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.10") (d #t) (k 0)))) (h "137f4qshypzskj0gs61bwr7skahqhsj70gpy8m7gs5jqpifxbzhp")))

