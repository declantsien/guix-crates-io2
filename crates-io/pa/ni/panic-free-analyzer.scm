(define-module (crates-io pa ni panic-free-analyzer) #:use-module (crates-io))

(define-public crate-panic-free-analyzer-0.1.0 (c (n "panic-free-analyzer") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0p9qg806w45fgdhpvm8qk3gp20l29isd5lj8hhjxslp9pmvmv6wm") (y #t)))

