(define-module (crates-io pa ni panic-semihosting) #:use-module (crates-io))

(define-public crate-panic-semihosting-0.1.0 (c (n "panic-semihosting") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.2.0") (d #t) (k 0)))) (h "0zvjisxi9pw0mczryj8x7q4hxyaqiv373p8zagikysqd0vn30j11")))

(define-public crate-panic-semihosting-0.2.0 (c (n "panic-semihosting") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)))) (h "1vgbmv1f2fmwdx4clzzn7aqkbk3jbpkdjqjyydbxv0iwg8acrj20") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.3.0 (c (n "panic-semihosting") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)))) (h "1z0k1szp7wr0bivqs2rkjycwh0xmp8049as0ih7x9ygbkb6lvxjj") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.4.0 (c (n "panic-semihosting") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 0)))) (h "0ss6ja239582bghaswxmzrj9802jg69c4b97vrb21z82k7vkz1cm") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.5.0 (c (n "panic-semihosting") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 0)))) (h "043jxa6148sgs1v764j5rfqxqmlsn64rnddci1j266k2n56qa5qh") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.5.1 (c (n "panic-semihosting") (v "0.5.1") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 0)))) (h "12c6am63xscsaf5l9ljphv0ra761kzb9dkb6qzm9vxh0gcgdhix6") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5.2 (c (n "panic-semihosting") (v "0.5.2") (d (list (d (n "cortex-m") (r ">= 0.5.6, < 0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 0)))) (h "097qzq57dn61avabzdgp4kfkplfcih0qai7iig0hqprv3myb7kwp") (f (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5.3 (c (n "panic-semihosting") (v "0.5.3") (d (list (d (n "cortex-m") (r ">= 0.5.6, < 0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (d #t) (k 0)))) (h "0b34ia0pz16j7jnqgps5mililzr1mbs8cllg61mc2xi8hsn68f60") (f (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5.4 (c (n "panic-semihosting") (v "0.5.4") (d (list (d (n "cortex-m") (r ">=0.5.6, <0.8") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r ">=0.3, <0.5") (d #t) (k 0)))) (h "0n7yrnwfg0xjhakaa84p1yr16y00r0wcn68kvmhr3vnhc6vnxldf") (f (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5.5 (c (n "panic-semihosting") (v "0.5.5") (d (list (d (n "cortex-m") (r ">=0.5.6, <0.8") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r ">=0.3, <0.5") (d #t) (k 0)))) (h "0a1dyacabbghy229y0r92dqr6xmbgq8dvim098x2nfj2jx2d7hyy") (f (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit")))) (y #t)))

(define-public crate-panic-semihosting-0.5.6 (c (n "panic-semihosting") (v "0.5.6") (d (list (d (n "cortex-m") (r ">=0.5.6, <0.8") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r ">=0.3, <0.5") (d #t) (k 0)))) (h "1v0ydab8js85abppgq9aq5my6v01szs0lvk42hjx1pq1spnmvmf3") (f (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.6.0 (c (n "panic-semihosting") (v "0.6.0") (d (list (d (n "cortex-m") (r ">=0.5.6, <0.8") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r ">=0.5.0, <0.6") (d #t) (k 0)))) (h "1axisy9dgmivq52s953s0qya9vjfrq93a8khm1v3s1yr6c93x2pf") (f (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("exit")))) (r "1.59")))

