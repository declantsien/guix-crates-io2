(define-module (crates-io pa ni panic_unwind) #:use-module (crates-io))

(define-public crate-panic_unwind-0.0.0 (c (n "panic_unwind") (v "0.0.0") (d (list (d (n "sgx_libc") (r "= 1.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)) (d (n "sgx_unwind") (r "^0.1.0") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "00jd0wb3svagghrrwblqh3grlaijz7iqqmq61kf4z4lzr4bma8ji")))

