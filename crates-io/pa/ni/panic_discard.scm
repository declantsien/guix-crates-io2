(define-module (crates-io pa ni panic_discard) #:use-module (crates-io))

(define-public crate-panic_discard-0.1.0 (c (n "panic_discard") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0ir22rd58q18y77k2bykdybrk3mfgk9knligj1fb0g1dwa8vnnm7")))

