(define-module (crates-io pa ul pauli) #:use-module (crates-io))

(define-public crate-pauli-0.2.0 (c (n "pauli") (v "0.2.0") (d (list (d (n "sprs") (r "^0.9.3") (d #t) (k 0)))) (h "1yj2wnpabxr7azxgqcnjqv6hamwd4wy6nmyccpwvj87pwfnz98q1")))

(define-public crate-pauli-0.2.1 (c (n "pauli") (v "0.2.1") (d (list (d (n "sprs") (r "^0.9.3") (d #t) (k 0)))) (h "1cfr059fgqph891s9zk2wbrb2wh71qspqkxnx7hfm13v9kzryswz")))

(define-public crate-pauli-0.2.2 (c (n "pauli") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.9.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0d7gnn50f1abcj4cr3mm5d9gz5qfyjr4bqbbb0fkqxvfjg8gzwja")))

(define-public crate-pauli-0.3.0 (c (n "pauli") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sprs") (r "^0.9.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1d4197gwhbns1ki2pi3975qpnr87q3riy577cq6pvrfcq9saascc")))

