(define-module (crates-io pa ti patiencediff) #:use-module (crates-io))

(define-public crate-patiencediff-0.1.0 (c (n "patiencediff") (v "0.1.0") (h "00k9gg2wvwjxdwamnxnyj49z5p73m6czdkrfr7fw8wrsrld239p0")))

(define-public crate-patiencediff-0.1.1 (c (n "patiencediff") (v "0.1.1") (h "1zy7fxgw6rwsbkl3vmfwyf536gqbhj5j0y0ca0brzs4ail34a5ym")))

