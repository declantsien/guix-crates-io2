(define-module (crates-io pa ti patience-diff-rs) #:use-module (crates-io))

(define-public crate-patience-diff-rs-0.1.1 (c (n "patience-diff-rs") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1ivs57vyazwa0iwr8bhpkwx67vlqhachhkagmvma6jcs6dh3h2fj")))

(define-public crate-patience-diff-rs-0.1.2 (c (n "patience-diff-rs") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1pcf7y5cjb6hjs98ghvb6x6r0nq7bpi3q15a7d2r92kaikr2cwjy")))

(define-public crate-patience-diff-rs-0.1.3 (c (n "patience-diff-rs") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "0k16f99kh4v6hs7v6g35jqp2nnnrnhqjd9l8lzsi35g9dvkgmgh6")))

(define-public crate-patience-diff-rs-0.1.4 (c (n "patience-diff-rs") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1fx20csjx4l6b4v1c2grqnnkhpl30p37786j6k21djgrsxq2q823")))

