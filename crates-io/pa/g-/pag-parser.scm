(define-module (crates-io pa g- pag-parser) #:use-module (crates-io))

(define-public crate-pag-parser-0.1.0-alpha.1 (c (n "pag-parser") (v "0.1.0-alpha.1") (d (list (d (n "ariadne") (r "^0.2") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pag-lexer") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (f (quote ("std" "memchr"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1vi6g4dak2zydavmp79mfj02ay5dsga1imzw9h4lk7znagviswwn") (r "1.71.0")))

