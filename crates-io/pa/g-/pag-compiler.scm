(define-module (crates-io pa g- pag-compiler) #:use-module (crates-io))

(define-public crate-pag-compiler-0.1.0-alpha.1 (c (n "pag-compiler") (v "0.1.0-alpha.1") (d (list (d (n "pag-parser") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (f (quote ("verbatim"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04827c4kadrc6mh80n62yk00frkbk7qcfv395gggr3bwc257abbm") (r "1.71.0")))

