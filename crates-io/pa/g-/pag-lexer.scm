(define-module (crates-io pa g- pag-lexer) #:use-module (crates-io))

(define-public crate-pag-lexer-0.1.0-alpha.1 (c (n "pag-lexer") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0arc7ix3y3k62ipilgfsj501cqvnqaa0pkzpb6m73z114p2k2q38") (r "1.71.0")))

