(define-module (crates-io pa r_ par_io) #:use-module (crates-io))

(define-public crate-par_io-0.3.7 (c (n "par_io") (v "0.3.7") (h "0sdhj6gq20d7n7p2ccpa7719c3avypp8jzf16dsd6l0cr1f5h27p") (r "1.60")))

(define-public crate-par_io-0.3.8 (c (n "par_io") (v "0.3.8") (h "194ja1lac784qj9mf0xb4g6w8nlavx3sq44xwcmxwkz0rdyx9y44") (r "1.60")))

(define-public crate-par_io-0.4.2 (c (n "par_io") (v "0.4.2") (h "0qpd36pb5s1whrh672adcxh2wa5hm5mrfch2qldlbx70dwm0fp58") (r "1.60")))

