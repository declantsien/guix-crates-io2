(define-module (crates-io pa in painless_input) #:use-module (crates-io))

(define-public crate-painless_input-0.1.0 (c (n "painless_input") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1ifz1mybm5c07c95lp08s4psiypsgp5rn6qn54gk6ynzcsa3ri26") (y #t)))

(define-public crate-painless_input-0.1.1 (c (n "painless_input") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "19qa2jl728mwl3ryhr9bsxl6zd9634ajkscj4msxfxqnl08b4572") (y #t)))

(define-public crate-painless_input-0.1.2 (c (n "painless_input") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "04z848vl487dyjdhsp6b6s84lssr2rl95qfcv39c0zn48dzfmgrd") (y #t)))

(define-public crate-painless_input-0.1.3 (c (n "painless_input") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0339xffyx8nbrywsnad32p9i4mk94d7qzqvymr67ymycy80gcyg8") (y #t)))

(define-public crate-painless_input-0.1.4 (c (n "painless_input") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0bsx9w6h3ngl5px7arpjnszgxl9jg8k710hvjq60n7v91s57j2fh") (y #t)))

