(define-module (crates-io pa in paint) #:use-module (crates-io))

(define-public crate-paint-0.1.0 (c (n "paint") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.3") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "07bs5hsr93h7prl62yk5x4wwk5yczwpjwi9wfm90sgqaffmabxbd")))

