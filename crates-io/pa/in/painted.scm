(define-module (crates-io pa in painted) #:use-module (crates-io))

(define-public crate-painted-1.0.0 (c (n "painted") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0wr6zf3a2a76m23d32vxcndmx6y834h70vaffdqkilr86i2v386s") (f (quote (("no-color"))))))

