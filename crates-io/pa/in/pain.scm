(define-module (crates-io pa in pain) #:use-module (crates-io))

(define-public crate-pain-0.0.1 (c (n "pain") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0nxzsz3z5nih079xv01ragpl9ag2bkcy2vy3a731g8fid49kl1d7") (f (quote (("default")))) (r "1.66.1")))

