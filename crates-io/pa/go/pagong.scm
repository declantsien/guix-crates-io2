(define-module (crates-io pa go pagong) #:use-module (crates-io))

(define-public crate-pagong-0.1.0 (c (n "pagong") (v "0.1.0") (d (list (d (n "atom_syndication") (r "^0.9.0") (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.1") (d #t) (k 0)))) (h "1pm559nfkfy1v1psn7fgc0rkdh38majkwy9wnl1xjl6xyps1l2zz")))

(define-public crate-pagong-0.1.1 (c (n "pagong") (v "0.1.1") (d (list (d (n "atom_syndication") (r "^0.9.0") (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.1") (d #t) (k 0)))) (h "0ngy697w16dgxv530wac49b7lh2fbz1pvcwiy70jrkissdfxwipq")))

(define-public crate-pagong-0.2.0 (c (n "pagong") (v "0.2.0") (d (list (d (n "atom_syndication") (r "^0.10") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "hyperbuild") (r "^0.2.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "1p1s2m3w0xs6w11xshz9vpfim7w0s422hjzcr7zdiwfvs8yr6aw8")))

