(define-module (crates-io pa xo paxos) #:use-module (crates-io))

(define-public crate-paxos-0.0.1 (c (n "paxos") (v "0.0.1") (d (list (d (n "sled") (r "^0.15") (d #t) (k 0)))) (h "1qsz59lnahgn9dl5cx57vs6fdh3wir83x1259nqvrx5v84xibw2d")))

(define-public crate-paxos-0.0.2 (c (n "paxos") (v "0.0.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.15") (d #t) (k 0)))) (h "0ksfkn2acwb11xvaamzwak19lyyrgrz01xd6hvgxvkz8wvikjl4d")))

(define-public crate-paxos-0.0.5 (c (n "paxos") (v "0.0.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.111") (d #t) (k 0)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)))) (h "1dqvvfwf3aq5xn4h4lb0hw2446s0cnfzbszhm7qdgp3zgrpzw3cp")))

