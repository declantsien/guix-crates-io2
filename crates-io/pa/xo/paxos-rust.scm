(define-module (crates-io pa xo paxos-rust) #:use-module (crates-io))

(define-public crate-paxos-rust-0.1.0 (c (n "paxos-rust") (v "0.1.0") (h "0kgnb1a6lr84k0gzliaa9xlch0qq4rgrxw9wk6zl2w5psd4jnb41")))

(define-public crate-paxos-rust-0.1.1 (c (n "paxos-rust") (v "0.1.1") (h "0rwlsb2jwky93p1xgix20x41mnd0djvz9zpp80m6h8lsyjp19dkj")))

(define-public crate-paxos-rust-0.2.0 (c (n "paxos-rust") (v "0.2.0") (h "0rawfz4qmbsps9xhczs7sa32c90sv7q1r7b8bfx5g6g7dxnpgcx9")))

