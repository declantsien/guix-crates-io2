(define-module (crates-io pa u- pau-rust) #:use-module (crates-io))

(define-public crate-pau-rust-0.0.1 (c (n "pau-rust") (v "0.0.1") (h "14diwiil9x4xrkyh331vgm0db4w1a0c89m1ygc64bahxf4irar70")))

(define-public crate-pau-rust-0.0.2 (c (n "pau-rust") (v "0.0.2") (h "1mr7qz4792ac44xq3n7npjbg9gmm090677p6n7527krhbrjpmdi6")))

