(define-module (crates-io pa wd pawd) #:use-module (crates-io))

(define-public crate-pawd-0.1.0 (c (n "pawd") (v "0.1.0") (d (list (d (n "psutil") (r "^3.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)))) (h "1wpm8xz8sc5hmy4xcz45dxbrnvdwqmaq1482fqnqp725pgahgaka")))

(define-public crate-pawd-0.1.1 (c (n "pawd") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "psutil") (r "^3.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1a2pmqkw8frnfsxn02jc0qpb7knyg6xalgcnpga7a7nkx5pj9pgm")))

