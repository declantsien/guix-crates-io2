(define-module (crates-io pa dd paddleocr) #:use-module (crates-io))

(define-public crate-paddleocr-0.2.1 (c (n "paddleocr") (v "0.2.1") (h "1xblyfrh1r0zzxv8z0lp3crwddicq9sn44nzpib2a9j734pq4sv7")))

(define-public crate-paddleocr-0.2.2 (c (n "paddleocr") (v "0.2.2") (h "06brz2h409mn9kg8inlph4r2sbxynpl6njxz34b4vy9p461gxjdn")))

(define-public crate-paddleocr-0.2.3-alpha (c (n "paddleocr") (v "0.2.3-alpha") (h "1gkswmh2v8sfr9jg58gd2x596p46hn5kvbipdrfa7dv8fi57n86n") (y #t)))

(define-public crate-paddleocr-0.2.3 (c (n "paddleocr") (v "0.2.3") (h "07llm6rp26408ng78a2389qr3gjmcn24c5b1hij66d0fx2rz96v7")))

(define-public crate-paddleocr-0.3.0 (c (n "paddleocr") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10w6bhyg94jv722mdn6x9j7kqwdvzrq3yhfccm1i5ha0xsp7xsis") (s 2) (e (quote (("parse" "dep:serde" "dep:serde_json"))))))

(define-public crate-paddleocr-0.4.0 (c (n "paddleocr") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sa463vd9hcliwc2g99mzgpmgbf12jdyab5x7rjzzws7n2853snn") (s 2) (e (quote (("bytes" "dep:base64"))))))

(define-public crate-paddleocr-0.4.1 (c (n "paddleocr") (v "0.4.1") (d (list (d (n "base64") (r "^0.21.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wyf3xz31g0x5p6z9vrqbz6qszwbddms72cap9knrlpg55c82p8f") (s 2) (e (quote (("bytes" "dep:base64"))))))

