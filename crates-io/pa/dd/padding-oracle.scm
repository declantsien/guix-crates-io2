(define-module (crates-io pa dd padding-oracle) #:use-module (crates-io))

(define-public crate-padding-oracle-0.1.0 (c (n "padding-oracle") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 2)) (d (n "cbc") (r "^0.1.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0) (p "thiserror-core")))) (h "1878sfyi4vmbd1w5bz8hdswgfalc0d3qzabyrhbiz1ckd9l60dhb")))

(define-public crate-padding-oracle-0.1.1 (c (n "padding-oracle") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 2)) (d (n "cbc") (r "^0.1.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0) (p "thiserror-core")))) (h "0kwlx26zd9k6ly0vj1lm9w73lkk55x8csblzvpqapjrsfvf5d449") (f (quote (("std" "thiserror/std") ("default" "std"))))))

