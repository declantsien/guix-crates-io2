(define-module (crates-io pa dd paddle-sys) #:use-module (crates-io))

(define-public crate-paddle-sys-0.1.0 (c (n "paddle-sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.6.6") (d #t) (k 0)))) (h "1wca84qnxmiavmdwl5jdxs5msj22zcr6dz8gxn3n9kghria9b4db")))

(define-public crate-paddle-sys-0.1.1 (c (n "paddle-sys") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1ld8li2f1nb38awian8726yaxyvpvgrw0kmaf90abv7czg93xvn1")))

(define-public crate-paddle-sys-0.1.2 (c (n "paddle-sys") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "165mfgrmzqhzwgdv2hdnhrxfav43x1479py4cbgz8qnnh4bl65yh")))

