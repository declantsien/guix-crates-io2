(define-module (crates-io pa dd paddleocr_rs) #:use-module (crates-io))

(define-public crate-paddleocr_rs-0.1.0 (c (n "paddleocr_rs") (v "0.1.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ort") (r "^2.0.0-alpha.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1694c8rj3vxwm42jc5zdmk786ycfwbnhz3jpbsb6dgkvcipak5iq")))

