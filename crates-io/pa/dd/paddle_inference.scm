(define-module (crates-io pa dd paddle_inference) #:use-module (crates-io))

(define-public crate-paddle_inference-0.1.0 (c (n "paddle_inference") (v "0.1.0") (h "19ypbqywi35gcggkggsz3scgicgcwsdyk6kdc87f18p5g0fyipql") (y #t)))

(define-public crate-paddle_inference-0.2.0 (c (n "paddle_inference") (v "0.2.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0j58w5wx7xi2fn9h6pizip6rwapjzgzj7qjyhm2kb5cvq01a7lrx") (y #t)))

(define-public crate-paddle_inference-0.3.0 (c (n "paddle_inference") (v "0.3.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "14grwik0hb2c8mpvc41x18gq5ryl4rnvl2y728ss2mp8ly27fxm9")))

(define-public crate-paddle_inference-0.4.0 (c (n "paddle_inference") (v "0.4.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b1b1hmrvjgiaqhjhkx2s9bagbjkb5p9kwlvb3zvz2v9k548prql") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

