(define-module (crates-io pa dd padder) #:use-module (crates-io))

(define-public crate-padder-0.1.0 (c (n "padder") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "18ldlv09vlkr5hcj1nbjr23a3npkrvlpapvxwngq7cbypc9ai1fj")))

(define-public crate-padder-0.1.1 (c (n "padder") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1x9csm0nwvk2afvaqnhgzky7b4w49zprxgg8yv04vhyhgggq3a9z")))

(define-public crate-padder-1.0.0 (c (n "padder") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "17jcjgfaag16plcfwybhwczba8jssji3lyax5qmmd4gyfs9dn1v4")))

(define-public crate-padder-1.1.0 (c (n "padder") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0sms6jar46h8zj8ii6n4x7hk0fqiw0nc3wl5zz3zvcs59krfbhg2")))

(define-public crate-padder-1.2.0 (c (n "padder") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "187qvid9yykvxhh73c58ccgzxap3ns1nxr68v04lglcwkkwccr0h") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

