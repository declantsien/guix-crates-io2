(define-module (crates-io pa sc pascal_string) #:use-module (crates-io))

(define-public crate-pascal_string-0.0.0 (c (n "pascal_string") (v "0.0.0") (h "0y5y4zg1djdfmyy311ya5vl6rjw49h4yys9xiwn26imzir3rp68p")))

(define-public crate-pascal_string-0.1.0 (c (n "pascal_string") (v "0.1.0") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)))) (h "1yvhcf9s61nxf0hf8cr6rpjxjxrzvdn8wcq8cmsw1b54a37nrsvr")))

(define-public crate-pascal_string-0.2.0 (c (n "pascal_string") (v "0.2.0") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)))) (h "0wsn7335hb6n0kknscg0la9yhv1q2gm4wpm6jbfq5fy5ggxsd9wj")))

(define-public crate-pascal_string-0.2.1 (c (n "pascal_string") (v "0.2.1") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)))) (h "00rsv2cmd47sdd9apsidxsi8hkz1y2j80szphdl7d4vlxhj6vkm8")))

(define-public crate-pascal_string-0.3.0 (c (n "pascal_string") (v "0.3.0") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)))) (h "1xy6r54qy6imy4wqzxb44r4s1kdzggb0xxcikxpba8pfgfnh86ag")))

(define-public crate-pascal_string-0.4.0 (c (n "pascal_string") (v "0.4.0") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)))) (h "1541sjjhhw43gaq7rzdl1q8xsnx4cvxxajjqjwsw35qrfmi9fvla")))

