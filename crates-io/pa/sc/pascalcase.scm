(define-module (crates-io pa sc pascalcase) #:use-module (crates-io))

(define-public crate-PascalCase-0.1.0 (c (n "PascalCase") (v "0.1.0") (h "0mykhfzpzpsyqp2ax4zg0z9bwj8sc4mhs1h41075hp21yd1rhj4p")))

(define-public crate-PascalCase-2.0.0 (c (n "PascalCase") (v "2.0.0") (h "130ipbjw73sdf5k3hlpnqhdc8ygxy2nsk233nvxvbbi0yybp9jy4")))

(define-public crate-PascalCase-1.0.0 (c (n "PascalCase") (v "1.0.0") (h "0jffx0ggmbhzcy7n737rid344pj65np2l9w5xb6vcwz1ggvsqdk6")))

(define-public crate-PascalCase-0.3.0 (c (n "PascalCase") (v "0.3.0") (h "14n05ylqmcr9i7rkldffwv06v5xb58lhjbqjnvbcxajabawzr215")))

