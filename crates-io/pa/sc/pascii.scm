(define-module (crates-io pa sc pascii) #:use-module (crates-io))

(define-public crate-pascii-0.1.0 (c (n "pascii") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg"))) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "05vgg74cd10xjlc9zqvwrhv4n4d9pcrmziwj2gyb0m0acxh8x3jf")))

