(define-module (crates-io pa ls palserializer) #:use-module (crates-io))

(define-public crate-palserializer-0.2.0 (c (n "palserializer") (v "0.2.0") (h "0qfm3fv0l1yqa5bkggghk3d0f2yx8l7qpyd8x9jviixrlmq43c78")))

(define-public crate-palserializer-0.1.2 (c (n "palserializer") (v "0.1.2") (h "1rwpjbdvb8y540pcba2745rvhxhbsi16zkaharbl574yhng0xyqn")))

(define-public crate-palserializer-0.2.2 (c (n "palserializer") (v "0.2.2") (h "1qvb2kqa8gdlid424kf3gcyfbh044ln52vh359rbi2rapcamgyg4")))

(define-public crate-palserializer-0.2.3 (c (n "palserializer") (v "0.2.3") (h "0cz3x91hchlhakws1qxdg530qrrq8354hd36bqb2gqz68km13lqk")))

(define-public crate-palserializer-0.2.4 (c (n "palserializer") (v "0.2.4") (h "01vsinj48varzri8ifyg77d0rlra8p5dxlc8hahq4x5jq4g05mfp")))

(define-public crate-palserializer-0.2.5 (c (n "palserializer") (v "0.2.5") (h "18qnlb3hf3hw02z099a4lz3sbj57fpdh05zxczv956g3axng1kin")))

(define-public crate-palserializer-0.3.0 (c (n "palserializer") (v "0.3.0") (h "1qfcc3yzdgxnzr84y3cr8inhm304hcbd8xwibrapbj6a3sz0fqjx")))

