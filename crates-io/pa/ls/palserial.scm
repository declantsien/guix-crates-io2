(define-module (crates-io pa ls palserial) #:use-module (crates-io))

(define-public crate-PALSerial-0.1.0 (c (n "PALSerial") (v "0.1.0") (h "0g0ln4l0gbhygyilsimcmnxy0fbbbap07y7mlzagcp87n5s8x572") (y #t)))

(define-public crate-PALSerial-0.2.0 (c (n "PALSerial") (v "0.2.0") (h "0bmh4r1kky1czdvfv1rkwrclxirz1ycrcan7r259wdqzjhbx7hn9")))

