(define-module (crates-io pa nc pancakestack) #:use-module (crates-io))

(define-public crate-pancakestack-0.1.0 (c (n "pancakestack") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "014mxsxf18l77bspv9lxlazgalbhiv2ylwznc30scnfij0y38zhm")))

(define-public crate-pancakestack-0.1.1 (c (n "pancakestack") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0f549vyvzphqppg8fzrjlwjwf5yygcf9f6yagp2i1s6042nn8pnb")))

(define-public crate-pancakestack-0.1.2 (c (n "pancakestack") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1a8jmqj3knw34rpapa9i2pgjk1f6kc8nfg0155758siwcxrflssn")))

(define-public crate-pancakestack-0.1.3 (c (n "pancakestack") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1nc3khkn6xz1gc2cb12231h1fj6bsm2k8p1kwwd5hl420irvk9pj")))

(define-public crate-pancakestack-0.1.4 (c (n "pancakestack") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "10zcpkf3xzw7c6aq9zrw6bqylz8hwya12mkg257a1y8hcnfh1hss")))

(define-public crate-pancakestack-0.2.0 (c (n "pancakestack") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "01k8wmqhww8jwncyb5k4fry007dk155z1mxahn42q1acns6ddjxx")))

(define-public crate-pancakestack-0.3.0 (c (n "pancakestack") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1var08gq1z3v26qchcr66350mxa32l8x4kwgm14wzvmjpz1q3bm4")))

(define-public crate-pancakestack-0.3.1 (c (n "pancakestack") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0ja2wq7j3ww16kasa7gf0b97411i2zbpv8cdp3b3mpyiyzjy4wp5")))

(define-public crate-pancakestack-0.4.0 (c (n "pancakestack") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "1njbqmdr58ah5y6p4p6a4q7q293hqzpm909hpssibzkls1jds758")))

(define-public crate-pancakestack-0.4.1 (c (n "pancakestack") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "1bfkz7kf73qsp4b7fw0d6g4imj89rbj0phrvri5qkbn6l8vxvk0l")))

(define-public crate-pancakestack-0.5.0 (c (n "pancakestack") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy-regex") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0qifgh5fx29gmqw9rcb0p8hwm1x0bwgkga5gk6d2dib3fq0s7144")))

(define-public crate-pancakestack-0.6.0 (c (n "pancakestack") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1ck7vmhnri9g5vr7v9wdd90q7p3v9zrf6kd16iwbc5b7xfprcd0c")))

