(define-module (crates-io pa nc pancurses-result) #:use-module (crates-io))

(define-public crate-pancurses-result-0.1.0 (c (n "pancurses-result") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0y71viv85q091fy5001mkhmd7kppllrf76m917cvxp2350ss6flq") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide"))))))

(define-public crate-pancurses-result-0.2.0 (c (n "pancurses-result") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "18jkbd6w60hcl7lgrd213x2z3bkr4zp3v31ksv5x627fizqlak4g") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide") ("show_menu" "pancurses/show_menu") ("disable_resize" "pancurses/disable_resize"))))))

(define-public crate-pancurses-result-0.3.0 (c (n "pancurses-result") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1x7qhxvy6mlwlmhzdawpl3ra0hr444ny4m44kqgrwbk6666jpnqd") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide") ("show_menu" "pancurses/show_menu") ("disable_resize" "pancurses/disable_resize"))))))

(define-public crate-pancurses-result-0.4.0 (c (n "pancurses-result") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0n6h8qiciv6vk4cdqcyq8fajykszv3547gsr29zcbsvk6jz46jrl") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide") ("show_menu" "pancurses/show_menu") ("disable_resize" "pancurses/disable_resize"))))))

(define-public crate-pancurses-result-0.5.0 (c (n "pancurses-result") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1yxj15835dnj3wljkigyxn8aqkx33lvync0ar06c543781nvw7a9") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide") ("show_menu" "pancurses/show_menu") ("disable_resize" "pancurses/disable_resize"))))))

(define-public crate-pancurses-result-0.5.1 (c (n "pancurses-result") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1snxhc0kvl5cwqrvy9s2ljjrg1zdbpmq914xiggf7z3mzncf6dj5") (f (quote (("win32a" "pancurses/win32a") ("win32" "pancurses/win32") ("wide" "pancurses/wide") ("show_menu" "pancurses/show_menu") ("disable_resize" "pancurses/disable_resize"))))))

