(define-module (crates-io pa nc pancake-db-core) #:use-module (crates-io))

(define-public crate-pancake-db-core-0.0.0-alpha.0 (c (n "pancake-db-core") (v "0.0.0-alpha.0") (d (list (d (n "pancake-db-idl") (r "^0.0.0-alpha.0") (d #t) (k 0)) (d (n "protobuf") (r "^3.0.0-alpha.1") (d #t) (k 0)) (d (n "q_compress") (r "^0.3.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1bvbi88nkyrj0m31i223314cr0rqp2cg5mi6w57szp9q821fp19p")))

(define-public crate-pancake-db-core-0.0.0-alpha.1 (c (n "pancake-db-core") (v "0.0.0-alpha.1") (d (list (d (n "pancake-db-idl") (r "=0.0.0-alpha.1") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "q_compress") (r "^0.4.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "17c5k4g3sq2zf02hjwhbrh3rdi68aj59mnim8hc2c5fhjx30gvsi")))

(define-public crate-pancake-db-core-0.1.0 (c (n "pancake-db-core") (v "0.1.0") (d (list (d (n "pancake-db-idl") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "q_compress") (r "^0.4.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0zy13dwj0ag9zvngv23mrdlby4k8xj338yrsqfqrzk2kmdphxk4h")))

(define-public crate-pancake-db-core-0.1.1 (c (n "pancake-db-core") (v "0.1.1") (d (list (d (n "pancake-db-idl") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "q_compress") (r "^0.7.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0z8rmlfhxrpd5y56irah40sgc591mj1jw0bbzipwg2iqsacszf2j")))

(define-public crate-pancake-db-core-0.1.2 (c (n "pancake-db-core") (v "0.1.2") (d (list (d (n "pancake-db-idl") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "q_compress") (r "^0.9.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1d77lr6byfvws6kb1v1nqs5rycy3c0ciifg1w9g6kgjg5w4hjgln")))

(define-public crate-pancake-db-core-0.2.0 (c (n "pancake-db-core") (v "0.2.0") (d (list (d (n "pancake-db-idl") (r "^0.2.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "q_compress") (r "^0.9.1") (d #t) (k 0)) (d (n "zstd") (r "^0.10") (d #t) (k 0)))) (h "080mywbyx594ci8wwqvv55imgdnglgym1ida3nvc9mk96gqwj243")))

