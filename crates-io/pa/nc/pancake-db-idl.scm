(define-module (crates-io pa nc pancake-db-idl) #:use-module (crates-io))

(define-public crate-pancake-db-idl-0.0.0-alpha.0 (c (n "pancake-db-idl") (v "0.0.0-alpha.0") (d (list (d (n "protobuf") (r "^3.0.0-alpha.1") (d #t) (k 0)))) (h "0pgra38zcsqjlalnwf94cw3fjz3nvp7a9s1hc8gbrx4ji77cix21")))

(define-public crate-pancake-db-idl-0.0.0-alpha.1 (c (n "pancake-db-idl") (v "0.0.0-alpha.1") (d (list (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)))) (h "1x1sgqln0hvc4g7da8rmkmcr729hy483sa21zdr94jxbn38ly2iv")))

(define-public crate-pancake-db-idl-0.1.0 (c (n "pancake-db-idl") (v "0.1.0") (d (list (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)))) (h "118lmawk5y1fanz9ylssinvwyqgq1wrv6b0j2xr54zazzdkkp0ab")))

(define-public crate-pancake-db-idl-0.2.0 (c (n "pancake-db-idl") (v "0.2.0") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0mzff3b5m389z5sqq28nvb98s20lfdsszip3bdv86y2hax59kl2x") (f (quote (("service" "tonic"))))))

