(define-module (crates-io pa rt partial_config_derive) #:use-module (crates-io))

(define-public crate-partial_config_derive-0.1.0 (c (n "partial_config_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17zmfbci75qxfvdyjsv9x0dr303cb4ys9sn9mn70mh1sgkcrl9q9") (f (quote (("tracing") ("log"))))))

(define-public crate-partial_config_derive-0.2.0 (c (n "partial_config_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l37y7w78nndh7gbr01sm70692a1bccyscv1bd2jrg4aihqsihv7") (f (quote (("tracing") ("log"))))))

(define-public crate-partial_config_derive-0.2.1 (c (n "partial_config_derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0slqis3difaiv1mjr2yvqisb1r2v2scxv4vn3676z9d165cc398d") (f (quote (("tracing") ("serde") ("log"))))))

(define-public crate-partial_config_derive-0.3.0 (c (n "partial_config_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "037z02y7l06v9g6glasqwkd4mdw7z9mrkfhh1mn9r3jcb39xigwx") (f (quote (("tracing") ("serde") ("log"))))))

