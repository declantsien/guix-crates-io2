(define-module (crates-io pa rt partial_derive2) #:use-module (crates-io))

(define-public crate-partial_derive2-0.1.1 (c (n "partial_derive2") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1q97pd3hl9qvljs35baf5zqyp802lsb21dhcfpkswfqgkyb2dm5p")))

(define-public crate-partial_derive2-0.1.2 (c (n "partial_derive2") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05zbpn2gk4zyi204l14jg1k7q8c0y6kwim057prn9bfkkrybzykm")))

(define-public crate-partial_derive2-0.1.3 (c (n "partial_derive2") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vrqqg5z3kmkxw5bjqk3pb374qypcvsk7xh0xjs4c2rvpxiprhqr")))

(define-public crate-partial_derive2-0.1.4 (c (n "partial_derive2") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sc9ss898kb1pangskmjc40in67hrixw5bcrc86n7y32isyddqlf")))

(define-public crate-partial_derive2-0.1.7 (c (n "partial_derive2") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p6vmj2gh4nfkz9gciczrgvygsni1aqqjsmiyay04jc5991skham")))

(define-public crate-partial_derive2-0.1.9 (c (n "partial_derive2") (v "0.1.9") (d (list (d (n "partial_derive2_derive") (r "^0.1.9") (d #t) (k 0)))) (h "12wv1vi65g8xaqh3b25s86x8nz74943wa0n2k3fjsapypzzkzn4b")))

(define-public crate-partial_derive2-0.2.0 (c (n "partial_derive2") (v "0.2.0") (d (list (d (n "partial_derive2_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1pyqp0y240xj16n1x1f8q667r0xsxb5a19rk3lms60719kr18v7r")))

(define-public crate-partial_derive2-0.2.1 (c (n "partial_derive2") (v "0.2.1") (d (list (d (n "partial_derive2_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1wljz3a3kyvkw8c9namj84grlny0mn4c69k9pavm0zlc1a149b2f")))

(define-public crate-partial_derive2-0.2.2 (c (n "partial_derive2") (v "0.2.2") (d (list (d (n "partial_derive2_derive") (r "^0.2.2") (d #t) (k 0)))) (h "13a7iskdy05hlaczw2k04wk317lfs29jrk6bpc7s2pw7fsiazv1f")))

(define-public crate-partial_derive2-0.3.0 (c (n "partial_derive2") (v "0.3.0") (d (list (d (n "partial_derive2_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1j4rrz128s6z41pwggqycssidw4inqrl4x0fmra0l1rhj7k2bxk0")))

(define-public crate-partial_derive2-0.3.1 (c (n "partial_derive2") (v "0.3.1") (d (list (d (n "partial_derive2_derive") (r "^0.3.1") (d #t) (k 0)))) (h "1rk4gyjvcpnqkk6s0c8x29k83ly7w9ca75gyl6zq3q8w8m2kwqnx")))

(define-public crate-partial_derive2-0.3.2 (c (n "partial_derive2") (v "0.3.2") (d (list (d (n "partial_derive2_derive") (r "^0.3.2") (d #t) (k 0)))) (h "12xiss18i5fyiw3cl639lhpxvz8dik636shgh6m45iihbfhz92r5")))

(define-public crate-partial_derive2-0.4.0 (c (n "partial_derive2") (v "0.4.0") (d (list (d (n "partial_derive2_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1hpdavyy7vdknibprvqi7hfw2mc14fxv38nqm89wy2gy17kvgdii")))

(define-public crate-partial_derive2-0.4.1 (c (n "partial_derive2") (v "0.4.1") (d (list (d (n "partial_derive2_derive") (r "^0.4.1") (d #t) (k 0)))) (h "121mfg34cz62pdiirm287d55mmydsaynw9b8x9rnzm8lk3q8izap")))

(define-public crate-partial_derive2-0.4.2 (c (n "partial_derive2") (v "0.4.2") (d (list (d (n "partial_derive2_derive") (r "^0.4.2") (d #t) (k 0)))) (h "0wmrv808mnqry5fvvv3rp654jkj3n2cms6vgmsvhdhvbsxdr2ywa")))

