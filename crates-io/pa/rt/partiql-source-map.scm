(define-module (crates-io pa rt partiql-source-map) #:use-module (crates-io))

(define-public crate-partiql-source-map-0.1.0 (c (n "partiql-source-map") (v "0.1.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "~1.8.0") (d #t) (k 0)))) (h "0lysanzsnb0vrh2r3rn3mj2nvhbd5w1li6q0y5bcrlghcls5nrrn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.2.0 (c (n "partiql-source-map") (v "0.2.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "1ab9prr2qschhdhyh1c2f03plvbipg089v13i34fakdhxjd58z9k") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.3.0 (c (n "partiql-source-map") (v "0.3.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "1c84sp9p95ppsq19p18r5f6nc2ihid72dk2q5r8sbxyy4njqj803") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.4.0 (c (n "partiql-source-map") (v "0.4.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "1h31qbyw2isxb0jq1m6nz06m1h4q83wi09yq0jyabkwrlwdrgqlw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.4.1 (c (n "partiql-source-map") (v "0.4.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "0gh8baiyw0b3f0n7h52v9paad8jl3wxw19lw2lq95hc1hc9fpamn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.5.0 (c (n "partiql-source-map") (v "0.5.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "0dwmj8m2lyr9b1kllfx8gv6bwjbgbdb58499m24makrmm3b7hg4j") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.6.0 (c (n "partiql-source-map") (v "0.6.0") (d (list (d (n "partiql-ast") (r "0.6.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "1pf7lwxdywy2g5bn0pah7vdn0wv1gpiyqw4sma755yp8v3cc4i33") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.7.0 (c (n "partiql-source-map") (v "0.7.0") (d (list (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "0dsjpwli06yx8xqkk4sx0p6dqh7ph0zq13sn8jj6hj111nhpif88") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.7.1 (c (n "partiql-source-map") (v "0.7.1") (d (list (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "054clc54pzx65rl4qsbkyqb67n5md8ycb9zk5wpnsf3294i3fx7b") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-partiql-source-map-0.7.2 (c (n "partiql-source-map") (v "0.7.2") (d (list (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "037946770zr5gsfw1yjr25yv0fr4w82ci0wyhsf92d9a3fb4yn89") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "smallvec/serde"))))))

