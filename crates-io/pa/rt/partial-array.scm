(define-module (crates-io pa rt partial-array) #:use-module (crates-io))

(define-public crate-partial-array-0.1.0 (c (n "partial-array") (v "0.1.0") (h "126jbhrc6smm6y05xslwvc5sbj1nsh2wxdj43fv1n8g90wx1ckhy")))

(define-public crate-partial-array-0.1.1 (c (n "partial-array") (v "0.1.1") (h "1nhsvrq4gyfc8nlw7s8s0dgg9k3lmk8fkfwal1l4b2zaq6afkpga")))

(define-public crate-partial-array-0.1.2 (c (n "partial-array") (v "0.1.2") (h "1b8k2ynaa0mip6bjg1kicx4p8i2858ngmfpch4hrrmlj6szzipqc")))

(define-public crate-partial-array-0.1.3 (c (n "partial-array") (v "0.1.3") (h "04ay3fl4j2azvwkygkrsgbwwmrcjihxxc8a9dihfwcxmnlr9jnhq")))

