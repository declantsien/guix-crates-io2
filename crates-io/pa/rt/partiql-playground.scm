(define-module (crates-io pa rt partiql-playground) #:use-module (crates-io))

(define-public crate-partiql-playground-0.1.0 (c (n "partiql-playground") (v "0.1.0") (d (list (d (n "partiql-parser") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1zwjcdk4xhw4bfk4hcflglk28snhwvdai3w0vib28ili0x9r6f1f") (y #t)))

