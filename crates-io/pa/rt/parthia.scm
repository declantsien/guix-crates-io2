(define-module (crates-io pa rt parthia) #:use-module (crates-io))

(define-public crate-parthia-0.1.0 (c (n "parthia") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "13yrpwkz1w35s964mplq2zdxz03b4rjz8c5s2kj02kq6b0xasiww")))

(define-public crate-parthia-0.1.1 (c (n "parthia") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1sj9y8mbcdsm0qc81bk80fm91yqkz96y83vx1jgvlb47rcj99lf6")))

