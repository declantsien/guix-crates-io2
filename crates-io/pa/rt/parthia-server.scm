(define-module (crates-io pa rt parthia-server) #:use-module (crates-io))

(define-public crate-parthia-server-0.1.0 (c (n "parthia-server") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "parthia") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1rd1s9kg3f64gsrdr2ngxbm8n2d3igh3jyb75fvfwjfvccgiyyvh")))

