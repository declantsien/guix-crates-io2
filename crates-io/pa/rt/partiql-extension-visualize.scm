(define-module (crates-io pa rt partiql-extension-visualize) #:use-module (crates-io))

(define-public crate-partiql-extension-visualize-0.7.0 (c (n "partiql-extension-visualize") (v "0.7.0") (d (list (d (n "dot-writer") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "itertools") (r "0.10.*") (o #t) (d #t) (k 0)) (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "partiql-logical") (r "0.7.*") (d #t) (k 0)))) (h "0qv3nxa5q0rypx1vg8s5pp09l5bs8vqxcinc8cxfdmqamivhgapa") (f (quote (("default")))) (s 2) (e (quote (("visualize-dot" "dep:dot-writer" "dep:itertools"))))))

(define-public crate-partiql-extension-visualize-0.7.1 (c (n "partiql-extension-visualize") (v "0.7.1") (d (list (d (n "dot-writer") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "itertools") (r "0.10.*") (o #t) (d #t) (k 0)) (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "partiql-logical") (r "0.7.*") (d #t) (k 0)))) (h "1i15r86qxycnk9p1a2sngz0fa68yl95hn1s14kjrs7bx89lq09r0") (f (quote (("default")))) (s 2) (e (quote (("visualize-dot" "dep:dot-writer" "dep:itertools"))))))

(define-public crate-partiql-extension-visualize-0.7.2 (c (n "partiql-extension-visualize") (v "0.7.2") (d (list (d (n "dot-writer") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "itertools") (r "0.10.*") (o #t) (d #t) (k 0)) (d (n "partiql-ast") (r "0.7.*") (d #t) (k 0)) (d (n "partiql-logical") (r "0.7.*") (d #t) (k 0)))) (h "1xm9agpns8zrns96y41yqgk6gxh604kk2mf3vsg5svdn6a094kkn") (f (quote (("default")))) (s 2) (e (quote (("visualize-dot" "dep:dot-writer" "dep:itertools"))))))

