(define-module (crates-io pa rt partial_ref) #:use-module (crates-io))

(define-public crate-partial_ref-0.1.0 (c (n "partial_ref") (v "0.1.0") (d (list (d (n "partial_ref_derive") (r "= 0.1.0") (d #t) (k 0)))) (h "0a3xxaz2917cz8x9gi66vml9qf302k3bm0x6fji4yln85imy1ni3")))

(define-public crate-partial_ref-0.1.1 (c (n "partial_ref") (v "0.1.1") (d (list (d (n "partial_ref_derive") (r "= 0.1.1") (d #t) (k 0)))) (h "03m74fh21725pkdpm6sp3vvs37xmdrw0vp7mabkly2nkgmxfjszm")))

(define-public crate-partial_ref-0.1.2 (c (n "partial_ref") (v "0.1.2") (d (list (d (n "partial_ref_derive") (r "= 0.1.2") (d #t) (k 0)))) (h "1drm2l6ba1mhab9kyabq3l7hnhp8h1p66cfvkrk3cy9dfbcks3mn")))

(define-public crate-partial_ref-0.1.3 (c (n "partial_ref") (v "0.1.3") (d (list (d (n "partial_ref_derive") (r "= 0.1.3") (d #t) (k 0)))) (h "1aaxmv7dp939xfc51gp8w6380k8q6cxb6fmc8l046ib28lgbh0pm")))

(define-public crate-partial_ref-0.2.0 (c (n "partial_ref") (v "0.2.0") (d (list (d (n "partial_ref_derive") (r "= 0.2.0") (d #t) (k 0)))) (h "04r5c67mbdj6l3nlfas25sk1lqshq0bgkx24i3z7n7dr9m5w8v8m")))

(define-public crate-partial_ref-0.3.0 (c (n "partial_ref") (v "0.3.0") (d (list (d (n "partial_ref_derive") (r "= 0.3.0") (d #t) (k 0)))) (h "1jxhy3a20lfzfn0rxx61l5xbf2njjz0n4yq14jrn1f8d9sn6mpgn")))

(define-public crate-partial_ref-0.3.1 (c (n "partial_ref") (v "0.3.1") (d (list (d (n "partial_ref_derive") (r "= 0.3.1") (d #t) (k 0)))) (h "1g192blgq05h323a1w85al3qx36kbqxkf7426sfzbaq2kal5zf5j")))

(define-public crate-partial_ref-0.3.2 (c (n "partial_ref") (v "0.3.2") (d (list (d (n "partial_ref_derive") (r "=0.3.2") (d #t) (k 0)))) (h "1w8ikaxpvividb9m44xxzsqr1gdc4iqvc5yj5c536dxijj74jdqp")))

(define-public crate-partial_ref-0.3.3 (c (n "partial_ref") (v "0.3.3") (d (list (d (n "partial_ref_derive") (r "=0.3.3") (d #t) (k 0)))) (h "034i78nmzp2bdapvpz8fgh14932aj0s70l5s1kj5d5j7n74qnwhg")))

