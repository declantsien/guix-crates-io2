(define-module (crates-io pa rt particular_derive) #:use-module (crates-io))

(define-public crate-particular_derive-0.1.0 (c (n "particular_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19bwj3sn4rk5wy0as54cay06kzrx3ypq26za3wrlm8mncgp8xpcj")))

(define-public crate-particular_derive-0.1.1 (c (n "particular_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0735pkhhwd1ix06iqhywmxnlsaj2rslr3gxg8iaznq0vmxqplqkf")))

(define-public crate-particular_derive-0.2.0 (c (n "particular_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01jz7y12jg8iywhbwmp0f79pxyaivrnpqap0k3w85nfdk53lv8xh")))

(define-public crate-particular_derive-0.3.0 (c (n "particular_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j7xpvnccmsvani86jangiyhgy5qci6l02m4n0hmy0jskj3j6kr8")))

(define-public crate-particular_derive-0.4.0 (c (n "particular_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rjgjpfqdgazmihwqxrqr6g7hpimi9vrb274ps57adhyhdfvwx1z")))

(define-public crate-particular_derive-0.5.0 (c (n "particular_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ig13v2rh9qwwc64pikpfp1k88nv4nwd4686m1r03lkica97zhvs")))

(define-public crate-particular_derive-0.6.0 (c (n "particular_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0p835qqg8jf9p4kck2z5iwn905i5mn7n4f8karc0li98970h1nvm")))

(define-public crate-particular_derive-0.7.0 (c (n "particular_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "07sxvjq82z2byjbj90vmgi4v5qkwqvsnln37c1ga6w04ibh7ncnr")))

