(define-module (crates-io pa rt partial_ref_derive) #:use-module (crates-io))

(define-public crate-partial_ref_derive-0.1.0 (c (n "partial_ref_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "06mj9i6apml27wk1ksg37bx6810pjzf09p1yf2vc6j9hswc0vmj0")))

(define-public crate-partial_ref_derive-0.1.1 (c (n "partial_ref_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0zx72cani8kqmq2y3cxhf6l5hzw1v0n150nw7dygk5wcj5phrwc2")))

(define-public crate-partial_ref_derive-0.1.2 (c (n "partial_ref_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0kqm1sm2k18540s5946al3s2q7hp4swjvkdcvkbizj6305xyy8d4")))

(define-public crate-partial_ref_derive-0.1.3 (c (n "partial_ref_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "18x346q183klj5bhblzp5vikryv9wfg8hfgq22m145f63dzlysw7")))

(define-public crate-partial_ref_derive-0.2.0 (c (n "partial_ref_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "1sgbsk5zkns02sxlasx59jisxdsdfd7fg16b700lcqgdcadybhi3")))

(define-public crate-partial_ref_derive-0.3.0 (c (n "partial_ref_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "0gvr64s618dl6vk8bf8p21id9320axvp1jvi91ri3ry2395f734g")))

(define-public crate-partial_ref_derive-0.3.1 (c (n "partial_ref_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "16q7zyf1cs3iq23616d8nvsilyknzg8v53y9kqky8cyhhnvik4vm")))

(define-public crate-partial_ref_derive-0.3.2 (c (n "partial_ref_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0r6hsm4mqzcx896ldpccxcsqyklvq1y58xpxvjldx915swvpmv2r")))

(define-public crate-partial_ref_derive-0.3.3 (c (n "partial_ref_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "10bm0pxwjph40z9pqwdzkkkbyz9n1h6lx69f6jjvb65qnln1s3ih")))

