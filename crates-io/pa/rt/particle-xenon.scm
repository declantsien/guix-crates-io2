(define-module (crates-io pa rt particle-xenon) #:use-module (crates-io))

(define-public crate-particle-xenon-0.0.1 (c (n "particle-xenon") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.6.0") (f (quote ("inline-asm" "const-fn"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "~0.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "~0.1") (d #t) (k 2)) (d (n "nrf52840-hal") (r "^0.8.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)) (d (n "panic-semihosting") (r "~0.5") (d #t) (k 2)))) (h "19wr8ljbz41g86p7q6yag374q2d8fyjjyv1m5hxy0jd80xn4aajd") (f (quote (("rt" "nrf52840-hal/rt") ("default" "rt"))))))

