(define-module (crates-io pa rt partiql-rewriter) #:use-module (crates-io))

(define-public crate-partiql-rewriter-0.0.0 (c (n "partiql-rewriter") (v "0.0.0") (h "1cyl85s7zk1jffd30z1n7023w79yy6xkl5r82x1dlh2cpcpn6w7k")))

(define-public crate-partiql-rewriter-0.2.0 (c (n "partiql-rewriter") (v "0.2.0") (h "1nwavrsp8dhgyl1n9al3rvv5cwnl24ncb455rxbmsspr7m4k2s47")))

(define-public crate-partiql-rewriter-0.3.0 (c (n "partiql-rewriter") (v "0.3.0") (h "0qk3w1xc8ymq8irb4cw9lh0a7v0csw69i4r6g564sp2gxs6ryxbr")))

(define-public crate-partiql-rewriter-0.4.0 (c (n "partiql-rewriter") (v "0.4.0") (h "1m6wqc0shlrymcmvi6pw1zhxxgpzcqnkja9xslqn33mmg0nhmzzp")))

(define-public crate-partiql-rewriter-0.4.1 (c (n "partiql-rewriter") (v "0.4.1") (h "0acyv95fx4qn8ls3jwlycfw66i8snc45bchqgc811f6jprf7knha")))

(define-public crate-partiql-rewriter-0.5.0 (c (n "partiql-rewriter") (v "0.5.0") (h "18sv54qmh0p1zi63ghwvdnhslcsap051shwg5810qq5kg05467lq")))

(define-public crate-partiql-rewriter-0.6.0 (c (n "partiql-rewriter") (v "0.6.0") (h "07yn6jvdls5gdiang9q79wwafwy3r7s7bv42f4v53hvgjqn22dra")))

(define-public crate-partiql-rewriter-0.7.0 (c (n "partiql-rewriter") (v "0.7.0") (h "0bw3458k5wd3gdxbrdgilkh35cmkjw8396600g0hibxq0p3fn6jn")))

(define-public crate-partiql-rewriter-0.7.1 (c (n "partiql-rewriter") (v "0.7.1") (h "0bimjm65saqj49cb8s6j4d90a2zblygiy32kj51xvadbm8ny0rb4")))

(define-public crate-partiql-rewriter-0.7.2 (c (n "partiql-rewriter") (v "0.7.2") (h "0bgafcbg27a5cbjpw4sjibhbxk3cxkc94dgrvxzia2277ialjq9z")))

