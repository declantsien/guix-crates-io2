(define-module (crates-io pa rt partialdebug-derive) #:use-module (crates-io))

(define-public crate-partialdebug-derive-0.0.1 (c (n "partialdebug-derive") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05zy6rqc4bg89f2lpdn0im84agn2akz6gfv3r4h3ddbrzq7mzxb4")))

(define-public crate-partialdebug-derive-0.1.0 (c (n "partialdebug-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18h9vy34mdwnpnw00qaifg8i8bcb94972j1429yrx1bbnnqwpaya")))

(define-public crate-partialdebug-derive-0.1.1 (c (n "partialdebug-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x571ardq45qcb0947j1r1gf63b31g0baf77618lpni23kw8ih87")))

(define-public crate-partialdebug-derive-0.2.0 (c (n "partialdebug-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1i5md92cg2dwj3g5ncq3z48ar2zw8frf5h2al8pfgm38mc8r6w") (f (quote (("unstable"))))))

