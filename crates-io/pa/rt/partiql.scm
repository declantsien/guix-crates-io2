(define-module (crates-io pa rt partiql) #:use-module (crates-io))

(define-public crate-partiql-0.0.0 (c (n "partiql") (v "0.0.0") (h "0a9qhxl757c81xvayiax740xbk8jndb16dvxb1rwv00s5x1160x0")))

(define-public crate-partiql-0.1.0 (c (n "partiql") (v "0.1.0") (h "0hi1108mfgh470nzq51wykg80r4a3xksi4ahb2swgadphs818d47")))

(define-public crate-partiql-0.2.0 (c (n "partiql") (v "0.2.0") (h "0zwg27q5vx4kl9yhqr7bzfmvd7xvr1k15h5qils103vi3ywbdqxs")))

(define-public crate-partiql-0.3.0 (c (n "partiql") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0c2hy4fklxcw9ywvdhpg0ycir0l21acdc61khcvi0kyp15xizmyx")))

(define-public crate-partiql-0.4.0 (c (n "partiql") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1l3hzbjdnlxdkgajvwhz8xndzb606fg97a79jpfzvnylvysgfgp2")))

(define-public crate-partiql-0.4.1 (c (n "partiql") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "19liqraq3gjg7c52dzrikbga540b0f44z0g9ssh6bqg052919yhb")))

(define-public crate-partiql-0.5.0 (c (n "partiql") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0r2pqd752k0a4cknmc7zpmd8hfc7i2kqmp60c0zvz0aa364z8vwr")))

(define-public crate-partiql-0.6.0 (c (n "partiql") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zpwz313c7z7bq3yvx5slid4p1fx33py742ysvkmdi7dcakdkbz9")))

(define-public crate-partiql-0.7.0 (c (n "partiql") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1sxs3vfzl7hp8c21f3z4g1gqad26g8y6s63aia8s62rnqvd25gjg")))

(define-public crate-partiql-0.7.1 (c (n "partiql") (v "0.7.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0dlf98v2z2bh8xvar3isdfk12rlam2xd5a3inav0c6lnjj8f015g")))

(define-public crate-partiql-0.7.2 (c (n "partiql") (v "0.7.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1n6vrpdc9l7x6q0jsr18hh275b0a4xpzhlkpi7j7pvnrkskwvpjk")))

