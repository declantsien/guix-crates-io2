(define-module (crates-io pa rt partial-context-codegen) #:use-module (crates-io))

(define-public crate-partial-context-codegen-0.1.0 (c (n "partial-context-codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8kka36p5z5f77zn2wxqk6a47w8wb61y9qpjybwh2wn16yy6i1j") (r "1.75.0")))

