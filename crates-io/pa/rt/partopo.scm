(define-module (crates-io pa rt partopo) #:use-module (crates-io))

(define-public crate-partopo-0.1.0 (c (n "partopo") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "daggy") (r "^0.8") (f (quote ("stable_dag"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1jxicwcda1lc4n5kxz9wkf5f2vrxbiy3hxi27nxschl3vpvlwv67")))

