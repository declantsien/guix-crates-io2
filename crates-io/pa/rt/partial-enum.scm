(define-module (crates-io pa rt partial-enum) #:use-module (crates-io))

(define-public crate-partial-enum-0.0.1 (c (n "partial-enum") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1i6vinl8dfv9zg5wl1h9qvk3fzybifhk0jim14ls7mjbx0ahcj9x")))

(define-public crate-partial-enum-0.0.2 (c (n "partial-enum") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1qlww4asy4m2sglyqbkbvi9f4ygk8h6jm1glsnw0fyypgp08iain")))

(define-public crate-partial-enum-0.0.3 (c (n "partial-enum") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggbcscrlpgmkwxrsd2iprwdlv5argw3lbyv25x7yyhl2dw99h66")))

(define-public crate-partial-enum-0.0.4 (c (n "partial-enum") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1wangicwhi175f526c6xdl00jfn7sapk98kn5syar1whkqndhpcq") (f (quote (("never"))))))

