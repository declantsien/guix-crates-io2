(define-module (crates-io pa rt partial_derive) #:use-module (crates-io))

(define-public crate-partial_derive-0.1.0 (c (n "partial_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dn0nqdkq3i5ll7spzykx53y7g9dy60c8jkqsrdvd679m2jn31n5")))

