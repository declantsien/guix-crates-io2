(define-module (crates-io pa rt partition-point-veb-layout) #:use-module (crates-io))

(define-public crate-partition-point-veb-layout-0.1.0 (c (n "partition-point-veb-layout") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pcg_rand") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics"))) (d #t) (k 2)))) (h "1ckf2lf5qgkgclzfbijwgz786gmxwc281fsvcaaphx6r7vsbpfp9") (f (quote (("default" "rayon"))))))

(define-public crate-partition-point-veb-layout-0.1.1 (c (n "partition-point-veb-layout") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pcg_rand") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics"))) (d #t) (k 2)))) (h "07amvi0g2bk6a4sjql079p8c0gm6y4yh3api9hzmx58xn5k5a8w2") (f (quote (("default" "rayon")))) (r "1.60")))

