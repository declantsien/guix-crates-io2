(define-module (crates-io pa rt partial-borrow) #:use-module (crates-io))

(define-public crate-partial-borrow-0.0.1 (c (n "partial-borrow") (v "0.0.1") (d (list (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "partial-borrow-macros") (r "^0.0.1") (d #t) (k 0)))) (h "0r6hrapjy9ibq164bckbzlb4v2qn18ryxhznfb04rg0jw5ys0lh0")))

(define-public crate-partial-borrow-0.1.0 (c (n "partial-borrow") (v "0.1.0") (d (list (d (n "easy-ext") (r "^1") (d #t) (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "partial-borrow-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ly9fy0db8zkhxgrza5cid3q9d3c9n2w01rn1a2b7gnv3gay1r0r")))

(define-public crate-partial-borrow-1.0.0 (c (n "partial-borrow") (v "1.0.0") (d (list (d (n "easy-ext") (r "^1") (d #t) (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "partial-borrow-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "10c6aflln2y4zw0griw3qc16jh0qfj9a0ncswyplblnh91rlja4g")))

(define-public crate-partial-borrow-1.0.1 (c (n "partial-borrow") (v "1.0.1") (d (list (d (n "easy-ext") (r "^1") (d #t) (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "partial-borrow-macros") (r "^1.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "12mgk0p6lp3qylx50g556zx713xcgzwgmla68a1rm8k46w2b6ha8")))

