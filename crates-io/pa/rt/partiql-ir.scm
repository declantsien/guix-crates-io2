(define-module (crates-io pa rt partiql-ir) #:use-module (crates-io))

(define-public crate-partiql-ir-0.0.0 (c (n "partiql-ir") (v "0.0.0") (h "0mawvpl1hlah59v9piqgqxpa6gr8jqaggji01zkqgxd3msxvag5f")))

(define-public crate-partiql-ir-0.2.0 (c (n "partiql-ir") (v "0.2.0") (h "10iysnskk52pkbjz1xviahk42vl06b7pkj0pccaxmcdw8blg9ari")))

(define-public crate-partiql-ir-0.3.0 (c (n "partiql-ir") (v "0.3.0") (h "1yg9h1dwqzva3pwykdmx8sglnar6551d9byrby4wsy6mn821zq3f")))

(define-public crate-partiql-ir-0.4.0 (c (n "partiql-ir") (v "0.4.0") (h "16ygawsk7jnxj1hp19s3pylvnlmw3axs8n66ym08iv98x3b0h1f9")))

(define-public crate-partiql-ir-0.4.1 (c (n "partiql-ir") (v "0.4.1") (h "0r5dyj6411ayy4xk65dzx5zsbw9rgwhkxmywzg1s1swmnmbibc4x")))

(define-public crate-partiql-ir-0.5.0 (c (n "partiql-ir") (v "0.5.0") (h "1bgkgml37ay3p2bzd9rsflaz1igyf30dlmqj4b1vkd5sbwcpsvr9")))

(define-public crate-partiql-ir-0.6.0 (c (n "partiql-ir") (v "0.6.0") (h "1s2iz6pn6ngbd7ixp0s98c8kfs51kq31yvfwrfac8cjsj9nz8i48")))

(define-public crate-partiql-ir-0.7.0 (c (n "partiql-ir") (v "0.7.0") (h "0c32z11ih6rmwvzyslpdmqjdp4v42rypv5v8x420sy9d950nyzmj")))

(define-public crate-partiql-ir-0.7.1 (c (n "partiql-ir") (v "0.7.1") (h "1rb6sy6ih72wr2d90968kk8hl4265shnf8dncfdmxh1xqd1k1bzy")))

(define-public crate-partiql-ir-0.7.2 (c (n "partiql-ir") (v "0.7.2") (h "12yw5y8sfw379i6b69jfb2h3a5r1j76qjm52b9jr34h5v6lcyvy3")))

