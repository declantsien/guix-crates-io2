(define-module (crates-io pa rt partially) #:use-module (crates-io))

(define-public crate-partially-0.1.0 (c (n "partially") (v "0.1.0") (d (list (d (n "partially_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0wyx7wa60ggf2ip6jidi6qzw5x39vhcimd908ay9hxcd1mk2gpz1") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:partially_derive"))))))

(define-public crate-partially-0.1.1 (c (n "partially") (v "0.1.1") (d (list (d (n "partially_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0srmnbjz7sqldgzzklfmih2zqhgfnd58mnsx4nqw2gbkgiszqk9i") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:partially_derive"))))))

(define-public crate-partially-0.2.0 (c (n "partially") (v "0.2.0") (d (list (d (n "partially_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0djay62dw42cgdn08a5412kj22hcdd3z9f0wxlvaxgjdy6f6m568") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:partially_derive"))))))

(define-public crate-partially-0.2.1 (c (n "partially") (v "0.2.1") (d (list (d (n "partially_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1hg3i4irbywg9r838b3kcjay5cip3ra79hgm1gzk1q0cnmc26rlc") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:partially_derive"))))))

