(define-module (crates-io pa rt partiql-types) #:use-module (crates-io))

(define-public crate-partiql-types-0.0.0 (c (n "partiql-types") (v "0.0.0") (h "0ayljn1xxzskqdmn4rv9fhp5bvilvkj4rkb7f27xn5yxkjhh1yzl")))

(define-public crate-partiql-types-0.2.0 (c (n "partiql-types") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "2.*") (d #t) (k 0)))) (h "0fani5c9ar81q53madp7n2595h1shyxqjkhdhs2wm9icx7nnir8h")))

(define-public crate-partiql-types-0.3.0 (c (n "partiql-types") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "2.*") (d #t) (k 0)))) (h "1r5bhc461fzm5d64qr53xdg7djiwn81w71pmsy54ia34mag7jyry")))

(define-public crate-partiql-types-0.4.0 (c (n "partiql-types") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "10n4ahdv188zinqcbyc748c93bd2hvb6293nm494p67sxkvks88j")))

(define-public crate-partiql-types-0.4.1 (c (n "partiql-types") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "16nyqlb6hp1q1lnnrzi8mqawn0jh35yj2q5avp2jzrih4kafdzsb")))

(define-public crate-partiql-types-0.5.0 (c (n "partiql-types") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0p9ih7jc791hz4fysinipnaf2xkzbzqgkps2qxcciar334c40vh2")))

(define-public crate-partiql-types-0.6.0 (c (n "partiql-types") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "004cv4g4rzrjgqyj25xqa7psxfz7ry76fvrn4bdg14f75fcqxyn8")))

(define-public crate-partiql-types-0.7.0 (c (n "partiql-types") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0848drmp23m1l4hmqr3b68gk2cs1qv8091w08xd2hn3ad46s30xg")))

(define-public crate-partiql-types-0.7.1 (c (n "partiql-types") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0rq8xrm9whsf00168n2wyq799zhmkab6ikdzwkrwizfzrsbzckgb")))

(define-public crate-partiql-types-0.7.2 (c (n "partiql-types") (v "0.7.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "ordered-float") (r "3.*") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "19m9w07cnn6cqva8k8sp1fvzn9yjpn33jlb9z0b53n01j5f27q6d")))

