(define-module (crates-io pa rt partially_derive) #:use-module (crates-io))

(define-public crate-partially_derive-0.1.0 (c (n "partially_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0mr8m8y56dgm8ylwh3kxd10kb7zpcvndfmcd1a6j52vdsr2y1zvd")))

(define-public crate-partially_derive-0.1.1 (c (n "partially_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjnb7zgr8fy0brksvgw5451j5rrsvx9zrjsh7xr7dp7px4qz6z3")))

(define-public crate-partially_derive-0.2.0 (c (n "partially_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "00af680brw5sx1sn8ilv9faq1rjzdl4lp8as8qdmiv9iarjfyjax")))

(define-public crate-partially_derive-0.2.1 (c (n "partially_derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0f2bw7a7djmsjkgs9lxqs0aramkf1kxdnxcac4xbna3v1iwmjc75")))

