(define-module (crates-io pa rt partial_const) #:use-module (crates-io))

(define-public crate-partial_const-0.1.0 (c (n "partial_const") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "1p1wb4i42dzf3k1yyipy9idpp53y1mjn0wys5pkfma5qw8pg75ry") (f (quote (("usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("isize") ("incomplete") ("i8") ("i64") ("i32") ("i16") ("i128") ("default" "usize" "isize" "i8" "i16" "i32" "i64" "i128" "u8" "u16" "u32" "u64" "u128" "char" "bool") ("char") ("bool"))))))

(define-public crate-partial_const-0.2.0 (c (n "partial_const") (v "0.2.0") (d (list (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "1m807gwavahg0k7ppx0ff691c10vvp8ixqz1llx988974a9wwx5b") (f (quote (("usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("isize") ("incomplete") ("i8") ("i64") ("i32") ("i16") ("i128") ("default" "usize" "isize" "i8" "i16" "i32" "i64" "i128" "u8" "u16" "u32" "u64" "u128" "char" "bool") ("char") ("bool")))) (y #t)))

(define-public crate-partial_const-0.2.1 (c (n "partial_const") (v "0.2.1") (d (list (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "10k8hq383lkkgsf840353bam1v358d1pz82gpzjx7pn0ysimzsky") (f (quote (("usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("isize") ("incomplete") ("i8") ("i64") ("i32") ("i16") ("i128") ("default" "usize" "isize" "i8" "i16" "i32" "i64" "i128" "u8" "u16" "u32" "u64" "u128" "char" "bool") ("char") ("bool"))))))

