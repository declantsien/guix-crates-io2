(define-module (crates-io pa rt partial-default-derive) #:use-module (crates-io))

(define-public crate-partial-default-derive-0.1.0 (c (n "partial-default-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03g7mk1281i53h4v5s86l616vz9z23qpsjwf84nj1jqqg9yi4nbl")))

