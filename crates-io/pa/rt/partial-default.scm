(define-module (crates-io pa rt partial-default) #:use-module (crates-io))

(define-public crate-partial-default-0.1.0 (c (n "partial-default") (v "0.1.0") (d (list (d (n "partial-default-derive") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "0jy1jb86gi6x16xc3bgpdrv9hjd5m0lrj4idashb6vzv3z1c6k8j") (s 2) (e (quote (("derive" "dep:partial-default-derive"))))))

