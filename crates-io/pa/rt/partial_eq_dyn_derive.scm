(define-module (crates-io pa rt partial_eq_dyn_derive) #:use-module (crates-io))

(define-public crate-partial_eq_dyn_derive-0.1.0 (c (n "partial_eq_dyn_derive") (v "0.1.0") (d (list (d (n "partial_eq_dyn") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1qgyc61f9f66139b7ir4vi7zgigf763qikfi7f6ryir53h825jyg")))

