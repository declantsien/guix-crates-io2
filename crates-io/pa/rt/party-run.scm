(define-module (crates-io pa rt party-run) #:use-module (crates-io))

(define-public crate-party-run-0.1.0 (c (n "party-run") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1lizki5iiqwr242af6dz6g8a62vyklql8imj4v00zh0hgbf25fpw")))

