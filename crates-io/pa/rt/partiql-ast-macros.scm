(define-module (crates-io pa rt partiql-ast-macros) #:use-module (crates-io))

(define-public crate-partiql-ast-macros-0.2.0 (c (n "partiql-ast-macros") (v "0.2.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "0.14.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnfvrs6iq18cbmwqis4ffkbyiq7cr054hla90qp3isnspxwvnxj")))

(define-public crate-partiql-ast-macros-0.3.0 (c (n "partiql-ast-macros") (v "0.3.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "0.14.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "05w8rqa5q9m5yrblrjh4infyqhjz61bz4bj40bj5fmsbf74sdhrl")))

(define-public crate-partiql-ast-macros-0.4.0 (c (n "partiql-ast-macros") (v "0.4.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qm226qlbk2d19rbjjkrav0a0yjx92vw5si99ixjgyifgq5pqrvw")))

(define-public crate-partiql-ast-macros-0.4.1 (c (n "partiql-ast-macros") (v "0.4.1") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ppmmx4gclp0fb30lwvyy67ispap5ma6kryr3z40cv6w69dkgjmv")))

(define-public crate-partiql-ast-macros-0.5.0 (c (n "partiql-ast-macros") (v "0.5.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ya6z9akixkp15433bv5j6vwwn84qb4c95aw08r7gwy6b53n4j89")))

(define-public crate-partiql-ast-macros-0.6.0 (c (n "partiql-ast-macros") (v "0.6.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhcrfc6sgd7322vclwkp3n6k807fcf3gybw8r4m67s6h2va1xg3")))

(define-public crate-partiql-ast-macros-0.7.0 (c (n "partiql-ast-macros") (v "0.7.0") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rm0drlnfca06w932n6q5pn02vmdaaq0m4ifg6i1rz31xdg7h3hv")))

(define-public crate-partiql-ast-macros-0.7.1 (c (n "partiql-ast-macros") (v "0.7.1") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wnqw1n4hynrjx22idcpc1qdnsz6y64p81i826c0fb8z1wqncw6z")))

(define-public crate-partiql-ast-macros-0.7.2 (c (n "partiql-ast-macros") (v "0.7.2") (d (list (d (n "Inflector") (r "0.11.*") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1apyzwqyag1pips58pml9jjkwn612069b14biwi3wcpizwv8z9bm")))

