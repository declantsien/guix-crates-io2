(define-module (crates-io pa rt partial-context) #:use-module (crates-io))

(define-public crate-partial-context-0.1.0 (c (n "partial-context") (v "0.1.0") (d (list (d (n "partial-context-codegen") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jb09ks4y687vp6vy8skyzzdrcncl30sxixvd6lllhd8q46p673y") (f (quote (("derive" "partial-context-codegen") ("default" "derive")))) (r "1.75.0")))

