(define-module (crates-io pa rt partial_derive2_derive) #:use-module (crates-io))

(define-public crate-partial_derive2_derive-0.1.8 (c (n "partial_derive2_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1x5jfmaqjl10l549damrjjs08x3h34lb206n40r7hsvil919gdw6")))

(define-public crate-partial_derive2_derive-0.1.9 (c (n "partial_derive2_derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05s8qpviykcp8lh8inxki7bvv05wi9z1ys119l0pzqkzg5m4x2ya")))

(define-public crate-partial_derive2_derive-0.2.0 (c (n "partial_derive2_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02ihaql9p0ibffsw9i8jdcd8p7siwglc763dajsx3681czajl83s")))

(define-public crate-partial_derive2_derive-0.2.1 (c (n "partial_derive2_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1qf1vqmdhi5rvnwcbkpv8yji9fp2hm60v4fg19j589lpwj12ahdd")))

(define-public crate-partial_derive2_derive-0.2.2 (c (n "partial_derive2_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p1brs1y0gbxag58hmg7mlvg6syrw9fnrl42p9vn73fzr19k4m2s")))

(define-public crate-partial_derive2_derive-0.3.0 (c (n "partial_derive2_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mkgy5p3svj3nxajys7phxfy0f7j8ds0ffz2a3ilrpxlxsldyyr3")))

(define-public crate-partial_derive2_derive-0.3.1 (c (n "partial_derive2_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1anbiv8z65lsr1bk24lpvyl4sr12mddz94y3jcslcpnp0b6zwzhw")))

(define-public crate-partial_derive2_derive-0.3.2 (c (n "partial_derive2_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lcw3y4h47rl8qy9jpid0x3y2im2jclcnaf5c9ivbpscaw8797z4")))

(define-public crate-partial_derive2_derive-0.4.0 (c (n "partial_derive2_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1s6h5r342cvgh0kp30yswki1b2smd3lnknf44zbwx36dsfjd4qy7")))

(define-public crate-partial_derive2_derive-0.4.1 (c (n "partial_derive2_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06bnrxwvgzzppw0pj870h8rxh6qy6b9mzlz3adx6wkndw8vcpixx")))

(define-public crate-partial_derive2_derive-0.4.2 (c (n "partial_derive2_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ki9d99i16jwhyfppd0g5png5ma31fqi89cxk43v5g351sfc9anx")))

