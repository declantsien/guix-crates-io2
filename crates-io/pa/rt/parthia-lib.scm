(define-module (crates-io pa rt parthia-lib) #:use-module (crates-io))

(define-public crate-parthia-lib-0.1.0 (c (n "parthia-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0zxs15xhx458pqvxgdkz94pdydf5m7pdf7bldda8g6n9hmxpwa5h")))

