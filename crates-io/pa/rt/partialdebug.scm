(define-module (crates-io pa rt partialdebug) #:use-module (crates-io))

(define-public crate-partialdebug-0.0.1 (c (n "partialdebug") (v "0.0.1") (d (list (d (n "partialdebug-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1054yhzmzpa60qals4gggjkb704qfnfyqfrkjn73hmip9vzh06qw")))

(define-public crate-partialdebug-0.0.2 (c (n "partialdebug") (v "0.0.2") (d (list (d (n "partialdebug-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0cayb3w2qhsk259bwfqizd25w0mjrx73wxxcrj4bpc035k3iynxb")))

(define-public crate-partialdebug-0.1.0 (c (n "partialdebug") (v "0.1.0") (d (list (d (n "partialdebug-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1p0gaizvld50w5k4dfl7yzs5sm1r81b97ncggdjnvfilazq0dw9m")))

(define-public crate-partialdebug-0.1.1 (c (n "partialdebug") (v "0.1.1") (d (list (d (n "partialdebug-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0qlqh8b258gnavj2bnp9hrkxp75pff37k2k1gvklzyy9crchsjzh")))

(define-public crate-partialdebug-0.2.0 (c (n "partialdebug") (v "0.2.0") (d (list (d (n "partialdebug-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1s6i9n8qkpcd6v7pl6bw8wgdgn22nhvnfpz0q5jhbvhr1ma1z7jq") (f (quote (("unstable" "partialdebug-derive/unstable"))))))

