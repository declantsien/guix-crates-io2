(define-module (crates-io pa rt partial-application-rs) #:use-module (crates-io))

(define-public crate-partial-application-rs-0.1.0 (c (n "partial-application-rs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8smi7l74jb0vb2m3piy9m680h5f9spb815kpjikq3mgvrlivn0")))

