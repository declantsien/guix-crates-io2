(define-module (crates-io pa rt partial_function) #:use-module (crates-io))

(define-public crate-partial_function-0.1.0 (c (n "partial_function") (v "0.1.0") (h "1nf1xbdcj100x9hl8sk7k7ghp5zjinc9llqppvcw4rinl31dj5l8")))

(define-public crate-partial_function-0.1.1 (c (n "partial_function") (v "0.1.1") (h "1rvimp5yn0r1vddazv1kf8j14z0wckck3pkdq4hplpgh5igs5vpc")))

(define-public crate-partial_function-0.1.2 (c (n "partial_function") (v "0.1.2") (h "1vx676ghxvdm2c0sll5cmp4asz7i7v0c511mdxd6n192j1db8mdg")))

(define-public crate-partial_function-0.2.0 (c (n "partial_function") (v "0.2.0") (h "1slgar4lxbf1kdna55nmj7grjbdnl171ki3x2pg0vmbkxc9gyibq")))

(define-public crate-partial_function-0.3.0 (c (n "partial_function") (v "0.3.0") (h "12dfmi1zq3dj14i84bc8dwhv483wlvffmiiamnvzbxxy45wv96ja")))

(define-public crate-partial_function-0.4.0 (c (n "partial_function") (v "0.4.0") (h "14rcyldmha89mm52b08072ssxi0birz0qlkc9m96b7bf503ix7n3")))

(define-public crate-partial_function-0.5.0 (c (n "partial_function") (v "0.5.0") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)))) (h "0vdqkd9m1ip28svhlpnh0zcpvla4ibg8d7zjizkh8n2i8b8f8ah9")))

