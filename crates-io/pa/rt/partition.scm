(define-module (crates-io pa rt partition) #:use-module (crates-io))

(define-public crate-partition-0.1.0 (c (n "partition") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0p07zlbbn6p0h8pf9zgkmcqvfck4pipph8i25zq2hfmc5dak3zi5")))

(define-public crate-partition-0.1.1 (c (n "partition") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "125vhwbrl5nkkb3hfy178yjhgzzmnjhb3zf6pzw4hz4z4cbbd76g")))

(define-public crate-partition-0.1.2 (c (n "partition") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1fiapzg2mnafr3zpc4w67grljk3qk1n4gh67iqmz2p2qm8x86zwl")))

