(define-module (crates-io pa rt partition-identity) #:use-module (crates-io))

(define-public crate-partition-identity-0.1.0 (c (n "partition-identity") (v "0.1.0") (h "1mmq9bpk4527vixfkv3n5c4hm6606f7r77klmysvlii9b5qyg73w")))

(define-public crate-partition-identity-0.1.1 (c (n "partition-identity") (v "0.1.1") (h "0my4chj01di13hbhfpsl9n13pg4gxchv1xmmhhykxj8zydm5kk3x")))

(define-public crate-partition-identity-0.1.2 (c (n "partition-identity") (v "0.1.2") (h "1wh1cgxgrjpvzazzmia78klnxkva32jk2z29i2qli5bd0h49q3g2")))

(define-public crate-partition-identity-0.1.3 (c (n "partition-identity") (v "0.1.3") (h "06yplmzhirar2awfgmpmnw94nvba0jw4lxpdd4fvckv3n5m8hq58")))

(define-public crate-partition-identity-0.1.4 (c (n "partition-identity") (v "0.1.4") (h "10vk67fnr8krhig2phpprgfnb6cahzsgr7yqai2fjm21bnvjxk0l")))

(define-public crate-partition-identity-0.1.5 (c (n "partition-identity") (v "0.1.5") (h "0mvjw0grwmqdyr5ya8jwlw4gchm557c8454jvgbj1n0plnqd3k1c")))

(define-public crate-partition-identity-0.1.6 (c (n "partition-identity") (v "0.1.6") (h "1nmsf2dp1z714ykwdcym7fn1ky2ss1h8a2qqpsf4n3bvrqcksbmj")))

(define-public crate-partition-identity-0.1.7 (c (n "partition-identity") (v "0.1.7") (h "11pkqir9bpch2nz80nncvk590lmaf6phir6qfkh2m9vvpn90k633")))

(define-public crate-partition-identity-0.2.0 (c (n "partition-identity") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "02k3lm014dixdhhlg9dvccdz1c1j2m8znh80zmf4g5sz7y9y1gh8")))

(define-public crate-partition-identity-0.2.1 (c (n "partition-identity") (v "0.2.1") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "1bf4kwhrva84xg74qysflmzz2s1s4l95f9d1jpy7s1mw1n40dd94")))

(define-public crate-partition-identity-0.2.2 (c (n "partition-identity") (v "0.2.2") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "0rhkmirzlmm8nqi1vqlflqpx1kfa2sjrrl7vx00x8whjldbcbbkf")))

(define-public crate-partition-identity-0.2.3 (c (n "partition-identity") (v "0.2.3") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)))) (h "176kxq5rz67khxglb2n364qnzd18im10c5sxxv9kvmiimbl35xw5")))

(define-public crate-partition-identity-0.2.4 (c (n "partition-identity") (v "0.2.4") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0fqx3i7scgka53dbz74nrf08vsxpc5c4i428dylgc8bnw15mnggk")))

(define-public crate-partition-identity-0.2.5 (c (n "partition-identity") (v "0.2.5") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)))) (h "1108ha2nhb7pgkcldbnz498x69ir5rdimh5k1rmzlcyzccpdlgpw")))

(define-public crate-partition-identity-0.2.6 (c (n "partition-identity") (v "0.2.6") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)))) (h "01cgfbmf0rz7l6pb5zgd76srga9bl78701bf76zbmiwkf4k0jfdk")))

(define-public crate-partition-identity-0.2.7 (c (n "partition-identity") (v "0.2.7") (d (list (d (n "err-derive") (r "^0.2.3") (d #t) (k 0)))) (h "0xwbwv0gzk97gvwf8nv2s63rbgn85lilhccya0y09nr2fgnzjhv1")))

(define-public crate-partition-identity-0.2.8 (c (n "partition-identity") (v "0.2.8") (d (list (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)))) (h "1miy02p3ri6ipdd3awfp22w2jzggwblvdhbfkyl10p7c1sdbl4zc")))

(define-public crate-partition-identity-0.3.0 (c (n "partition-identity") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08cymccnyf2b0pwc7x4wj889k1knqmrb8500idsjslybpvwjbacz")))

