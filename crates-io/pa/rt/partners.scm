(define-module (crates-io pa rt partners) #:use-module (crates-io))

(define-public crate-partners-0.0.1 (c (n "partners") (v "0.0.1") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "07j2scx1zvhfzx5w6y8iapnpn7k85xsilyrrns9p8ax7rxy87sv5")))

(define-public crate-partners-0.1.0 (c (n "partners") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.74") (d #t) (k 0)))) (h "1jxjjjvqzd9imjy3pk2zvgxb7ii08qfzzsl3922qlxkg8wvv9yi0")))

(define-public crate-partners-0.2.0 (c (n "partners") (v "0.2.0") (d (list (d (n "clap") (r "^2.20.5") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "docopt") (r "^0.6.74") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "0brvpzdhjp3ip133sdj2a5cgd96pzhfaayaqg9473iw1q260f16k")))

(define-public crate-partners-0.2.1 (c (n "partners") (v "0.2.1") (d (list (d (n "clap") (r "^2.20.5") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "docopt") (r "^0.6.74") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "04ryipmvrqlq19980cv762wlvnrb2k3yx7y2y5c96lk88nfbliis")))

