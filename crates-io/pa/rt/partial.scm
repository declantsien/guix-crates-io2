(define-module (crates-io pa rt partial) #:use-module (crates-io))

(define-public crate-partial-0.1.0 (c (n "partial") (v "0.1.0") (h "166r99j5y3vq4ma77gpjj4q17738m4cqm69xhxfsh0vilxllc0nz")))

(define-public crate-partial-0.1.1 (c (n "partial") (v "0.1.1") (h "01vc3dv0b4z9argdw204hhppzh1lkyxs3awwqyivf3j3qmg9vdi7")))

(define-public crate-partial-0.1.2 (c (n "partial") (v "0.1.2") (h "0rav0hsay8jkwm7h77388kvn4hr5ivppqfd4yrcp9h7sn212dxhc")))

(define-public crate-partial-0.1.3 (c (n "partial") (v "0.1.3") (h "0z2nmwvbx3iwakp4j1b90yi7nn2rzyhpq1i95f1kab0v0alkr60i")))

(define-public crate-partial-0.2.0 (c (n "partial") (v "0.2.0") (h "0kp4nv2v85cyjcghv927sac9gshn8wqd7gdwynfmd4d7b05hfilk")))

(define-public crate-partial-0.2.1 (c (n "partial") (v "0.2.1") (h "182bk0fd31cd9vbjj2l23kp57gakjgfsmiv61fphs3sz71ckdaig")))

(define-public crate-partial-0.2.2 (c (n "partial") (v "0.2.2") (h "1ym12qjmfy1hb4qnqq91vbcbhi19y21psfkdcay467z8p1kgjnxi")))

(define-public crate-partial-0.2.3 (c (n "partial") (v "0.2.3") (h "1y81jvpcf0cr62psgh04x3s2cp5irqjli230fz09vk7xz65m4wpa")))

(define-public crate-partial-0.3.0 (c (n "partial") (v "0.3.0") (h "1khjsr9zll1v4gx9z6i3hy0r5yngcxagzx8016imdi5f24i2n6y2")))

(define-public crate-partial-0.4.0 (c (n "partial") (v "0.4.0") (h "09ccphygka11c1zzs7yh055d8vzdm4jw336as6q50vg1bj7xbiwz")))

