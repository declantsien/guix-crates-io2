(define-module (crates-io pa rt particle-argon) #:use-module (crates-io))

(define-public crate-particle-argon-0.1.0 (c (n "particle-argon") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "00rcpfpkaay68l9r2hrh5wzpr0d2q5k3n4l2m3lb3rvdjw5xvf14")))

