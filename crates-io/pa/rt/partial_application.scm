(define-module (crates-io pa rt partial_application) #:use-module (crates-io))

(define-public crate-partial_application-0.1.0 (c (n "partial_application") (v "0.1.0") (h "1dhysisffd3xpcz54k3hgh74h0mjy9pkp98bqcq9slq38hw2k04d")))

(define-public crate-partial_application-0.2.0 (c (n "partial_application") (v "0.2.0") (h "1f1i1lh4d17bsvdf31v1fs5dvbqlz8z944dam4m1r6wzynaaqvs3")))

(define-public crate-partial_application-0.2.1 (c (n "partial_application") (v "0.2.1") (h "109h2iwbx8rsagc0v5icip6lvrb30chj3dwf9ckj4bf3d1wmc9pr")))

