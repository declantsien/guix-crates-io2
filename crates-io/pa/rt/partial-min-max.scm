(define-module (crates-io pa rt partial-min-max) #:use-module (crates-io))

(define-public crate-partial-min-max-0.3.0 (c (n "partial-min-max") (v "0.3.0") (h "0avcqgsij1f1vkjzndswbfxzdzyxcgb6r7fqmvzcmv0f7rigpyww")))

(define-public crate-partial-min-max-0.4.0 (c (n "partial-min-max") (v "0.4.0") (h "133m16n5c4wi5w2025hr0l657v0jk8q43awx9z3bn2y6hb9ssj34")))

