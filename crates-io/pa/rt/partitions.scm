(define-module (crates-io pa rt partitions) #:use-module (crates-io))

(define-public crate-partitions-0.1.0 (c (n "partitions") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b8xhzpyy7k9mw906dipg5jq26fy19iyay3h7qnmhhkwda40fskd") (f (quote (("default" "rayon") ("compact"))))))

(define-public crate-partitions-0.2.0 (c (n "partitions") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10zd2y1nbkqndzjkwyfhjmyibby6cggdcs6mfwr9rq08vwvc1hr5") (f (quote (("default" "rayon") ("compact"))))))

(define-public crate-partitions-0.2.1 (c (n "partitions") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0k5950372kbv9gc0545hj910x59wlf2d1cwxrj5lcm8hzmhh5cl5") (f (quote (("default" "rayon") ("compact"))))))

(define-public crate-partitions-0.2.2 (c (n "partitions") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vba6kpdyakykn30v5v0sv03kvng9pn31pras7qnk3g4pisyrhmh") (f (quote (("default" "rayon") ("compact"))))))

(define-public crate-partitions-0.2.3 (c (n "partitions") (v "0.2.3") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "187x2wqwy6j2wwhfs9rm38167n2ds1r0ldwq30np1mjfi7h8yl9k") (f (quote (("default" "rayon") ("compact"))))))

(define-public crate-partitions-0.2.4 (c (n "partitions") (v "0.2.4") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n0h2m00ai0jj1j1893bc3sa73zjpbqrlr6cd6yjw3m6wmgp8jcj") (f (quote (("default" "rayon") ("compact"))))))

