(define-module (crates-io pa rz parze-declare) #:use-module (crates-io))

(define-public crate-parze-declare-0.1.0 (c (n "parze-declare") (v "0.1.0") (h "1iwy8a49hrxs24x7vlszw000mslw86x5y8iy5sy4z26zyfgj39ci")))

(define-public crate-parze-declare-0.2.0 (c (n "parze-declare") (v "0.2.0") (h "0v23ab2r63hmdjwdnianpkl5mws92yi0kgqi90z7di0z01hkm2gs")))

