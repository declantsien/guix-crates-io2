(define-module (crates-io pa rz parze) #:use-module (crates-io))

(define-public crate-parze-0.1.0 (c (n "parze") (v "0.1.0") (h "0d6mmzh2bdz6pww2r4wfgr5va5ma89avp2qcix42n71kaq2vjyj1")))

(define-public crate-parze-0.2.0 (c (n "parze") (v "0.2.0") (h "0pwj7dk2gr2gw5imks9lnlnz2zmyppbkaf9qva8qxy7gshxzg4vl")))

(define-public crate-parze-0.3.0 (c (n "parze") (v "0.3.0") (h "1w6z48l20whxdzqpx5wzvdlig0nqgrhm3hwb7as8f9kcgmkkz59j")))

(define-public crate-parze-0.4.0 (c (n "parze") (v "0.4.0") (h "0xxxcr56bjqb1zh25r8jbipi2zmw6dm9dncbzj163l5xxdbz6amm")))

(define-public crate-parze-0.4.1 (c (n "parze") (v "0.4.1") (h "1c7p2wf4wfkg25hjkrmwag06pymxzl942v39ivzk9s6ckzh3g2s5")))

(define-public crate-parze-0.4.2 (c (n "parze") (v "0.4.2") (h "0n4nrpq3jkvmfvx4dm2pcxgbblh9m6k73zixmwjl8cl9562g5vns")))

(define-public crate-parze-0.5.0 (c (n "parze") (v "0.5.0") (d (list (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "0y6x03vgs38lcrdc49y0afanlhdzyvvx9xhxlsdmniwkrrxfblsj")))

(define-public crate-parze-0.5.1 (c (n "parze") (v "0.5.1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "0hx8k2ilkzlmnknh91asdp3arljfaxbaaapnljd229xq3x5sf6zq")))

(define-public crate-parze-0.6.0 (c (n "parze") (v "0.6.0") (d (list (d (n "parze-declare") (r "^0.1") (d #t) (k 0)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "013lkb0brryclji6lm4fv9qzdqyqn6635k48ywhva4vlf3rs496c") (f (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7.0 (c (n "parze") (v "0.7.0") (d (list (d (n "parze-declare") (r "^0.2") (d #t) (k 0)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "1p7vxwdwdym7nddb952523dkf6dk2y1pywj8rw7n6d8ha62br115") (f (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7.1 (c (n "parze") (v "0.7.1") (d (list (d (n "parze-declare") (r "^0.2") (d #t) (k 0)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "13ncidw620mjqrxivjfszq3xvz9lwpmh2rfqlfq5rlv2797fznjn") (f (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7.2 (c (n "parze") (v "0.7.2") (d (list (d (n "parze-declare") (r "^0.2") (d #t) (k 0)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "1fm29wypplhpvhkdaa6hy5m8k9m5jj9hva9marjj9npp6ijgh4zs") (f (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7.3 (c (n "parze") (v "0.7.3") (d (list (d (n "parze-declare") (r "^0.2") (d #t) (k 0)) (d (n "pom") (r "^3.0") (d #t) (k 2)))) (h "0kch2d1rjwypzz06a6wflwq4hpw7w6jbblliwvmy36y02z70b2c2") (f (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

