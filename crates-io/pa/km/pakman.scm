(define-module (crates-io pa km pakman) #:use-module (crates-io))

(define-public crate-pakman-0.1.0 (c (n "pakman") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "09b45cjpdng4frk80p23r34gf7g4pya4wq4z74zfpy36343pyc2i")))

