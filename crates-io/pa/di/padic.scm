(define-module (crates-io pa di padic) #:use-module (crates-io))

(define-public crate-padic-0.1.0 (c (n "padic") (v "0.1.0") (h "1lmm5j421ww18cwk7zwz682ww0pw9cr8prwi7zi10d9x4jgv96pp")))

(define-public crate-padic-0.1.1 (c (n "padic") (v "0.1.1") (h "00cll76prsbakk6h36c11a65i3apy01ydi0v9dxq3lwdsvi3wzd7")))

(define-public crate-padic-0.1.2 (c (n "padic") (v "0.1.2") (h "1f2mvjjscm3fzl4vf0gh2v5jvi67ij0wq3kj3h5h00dv6a2c0n3w")))

(define-public crate-padic-0.1.3 (c (n "padic") (v "0.1.3") (h "1hgb3v7gi50bk3ghgchwn7zy1adqd1kiysfyj2bhmbf89amv962y")))

(define-public crate-padic-0.1.4 (c (n "padic") (v "0.1.4") (h "0xvq8h79b42i5rs0wsxf6pnjirlmrkwdch1h5z412scqygwdvlip")))

(define-public crate-padic-0.1.5 (c (n "padic") (v "0.1.5") (h "0a6k5w1whb9lj2kc4zf4jlpfvnyjd1f048kpdwhclcm44fj2jv1m")))

(define-public crate-padic-0.1.6 (c (n "padic") (v "0.1.6") (h "1q17rfjzsx68fbrvscqg4vm162ima3pq4zxrr5y1k7b2dqksbmwn")))

