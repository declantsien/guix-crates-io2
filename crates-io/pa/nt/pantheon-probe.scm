(define-module (crates-io pa nt pantheon-probe) #:use-module (crates-io))

(define-public crate-pantheon-probe-0.1.0 (c (n "pantheon-probe") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "061b512h3yjxvbr21hcc1157swrbj8srxnhyr3f4ncf0xvdgf2q5")))

(define-public crate-pantheon-probe-0.1.1 (c (n "pantheon-probe") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ry03lxp5w84g8xyq1a8wsiawpm55fjg2kgqf9kdypyy0zzkmyx9")))

(define-public crate-pantheon-probe-0.1.3 (c (n "pantheon-probe") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m26jx6s8h9wd7m4ygd8pwkar8r2m5i3dk4ar72pzybym265liq9")))

