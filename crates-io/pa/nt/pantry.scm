(define-module (crates-io pa nt pantry) #:use-module (crates-io))

(define-public crate-pantry-1.0.0 (c (n "pantry") (v "1.0.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0wbks1g1hfmazaka5ql9gdzadc692y35yhfbxd8h8z5mpmx3pyxh")))

(define-public crate-pantry-1.0.1 (c (n "pantry") (v "1.0.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "09qpjm0nmw798v0rkbdsva3s2gmqw86l4h20h328fvj5jkhnxviw")))

(define-public crate-pantry-1.0.2 (c (n "pantry") (v "1.0.2") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "180ynd9vgpq69wz81x7bpiz8c88hm2wwh2g82f471zrkz3d49bc1")))

