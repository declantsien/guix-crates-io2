(define-module (crates-io pa rr parrot_ar_drone) #:use-module (crates-io))

(define-public crate-parrot_ar_drone-0.1.0 (c (n "parrot_ar_drone") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0hwn5bfi13v7cz4i4pmsh2sjm88nn8al8247b46lji85mgjighaj")))

(define-public crate-parrot_ar_drone-0.1.1 (c (n "parrot_ar_drone") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0p6snzqgkvyl1gk8yf2g6yl3x5hcj2dkqkpp4qrj5jxlmy7lf1xy")))

(define-public crate-parrot_ar_drone-0.1.2 (c (n "parrot_ar_drone") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0p9na57bhfm19airvs7bqp7znijxxk5yyqmhcp6319sn7mf7rji8")))

(define-public crate-parrot_ar_drone-0.1.3 (c (n "parrot_ar_drone") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0jmyrpgllad6fpg5zsjv6qhvqk2b0r5cqv172h49hw5linfgw1dk")))

