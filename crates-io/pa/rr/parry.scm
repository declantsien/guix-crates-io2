(define-module (crates-io pa rr parry) #:use-module (crates-io))

(define-public crate-parry-0.1.0 (c (n "parry") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rayon") (r "^0.0.1") (d #t) (k 0)))) (h "15wy24vgwpsycy8cxakqkrspl7bg32lq118f62zm0nhxd7w6v5qx") (f (quote (("unstable") ("test-everything"))))))

