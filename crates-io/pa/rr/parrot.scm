(define-module (crates-io pa rr parrot) #:use-module (crates-io))

(define-public crate-parrot-0.1.0 (c (n "parrot") (v "0.1.0") (d (list (d (n "latin") (r "0.1.*") (d #t) (k 2)) (d (n "lux") (r "^0.1.1") (d #t) (k 2)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "sha1") (r "0.1.*") (d #t) (k 2)))) (h "151gm63yzkwi0pwgsq303lvwfncz64bafar8jc1mp8kc3rsclh7c")))

