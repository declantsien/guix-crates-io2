(define-module (crates-io pa rr parrot-snap) #:use-module (crates-io))

(define-public crate-parrot-snap-0.0.1 (c (n "parrot-snap") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "derive" "std"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "06hfs4xsz9g51pn1dj9gkn59l9mzqq5gr34sxz0mvz399gglj4hd")))

(define-public crate-parrot-snap-0.0.2 (c (n "parrot-snap") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "derive" "std"))) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1qcf69f82kwhivwf12j38v6awf8zhdx1xwcwfpksyy8xydc8dl7b")))

(define-public crate-parrot-snap-0.0.3 (c (n "parrot-snap") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "derive" "std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1iz0znhawvvy62ivayinqvk4qa9f1jiql6jkvisn60f96xjj3458")))

