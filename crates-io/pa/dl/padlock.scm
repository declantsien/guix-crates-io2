(define-module (crates-io pa dl padlock) #:use-module (crates-io))

(define-public crate-padlock-0.1.0-alpha (c (n "padlock") (v "0.1.0-alpha") (h "00pvhwdg82aw2n2ykbmyhbqkgid8sf8xqlrxi3qbsbwijfg67hig")))

(define-public crate-padlock-0.1.1-alpha (c (n "padlock") (v "0.1.1-alpha") (h "0gxfn295k3slvvc9qz28ismrnjy4dfmvf38v8714b1x5c2k70p99")))

(define-public crate-padlock-0.2.0 (c (n "padlock") (v "0.2.0") (h "0vwb9s9c629yj2crir4f5zf4ab2d0ngf8ymy1prxkb0xi8vnj1f1")))

