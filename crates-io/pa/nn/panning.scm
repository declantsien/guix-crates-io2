(define-module (crates-io pa nn panning) #:use-module (crates-io))

(define-public crate-panning-0.1.0 (c (n "panning") (v "0.1.0") (h "1hprapkfbxmfmq2qshc939g487zakqql6a7s08fv9hvjlij74mka")))

(define-public crate-panning-0.1.1 (c (n "panning") (v "0.1.1") (h "1q7iikw9y23axv79hjbhz44nn4mnpb540z38y8klcaihsbkynm1n")))

