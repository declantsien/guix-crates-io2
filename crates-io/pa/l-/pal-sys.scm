(define-module (crates-io pa l- pal-sys) #:use-module (crates-io))

(define-public crate-pal-sys-0.1.0 (c (n "pal-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "pkg-config") (r "0.3.*") (d #t) (k 1)))) (h "16cc3g2pzm98jvlr1m8da0dwxb3lcv5f8vzvpkskf59c4v8v4ay9") (f (quote (("static" "autotools")))) (l "pal")))

