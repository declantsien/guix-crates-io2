(define-module (crates-io pa re parenthesized_c) #:use-module (crates-io))

(define-public crate-parenthesized_c-0.1.0 (c (n "parenthesized_c") (v "0.1.0") (d (list (d (n "outer_attribute") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing" "full"))) (d #t) (k 2)))) (h "0ggc1frpsgd20f24ggqs3vb94xhgkjjv78gjzac045rbf6gjs0bd") (r "1.56.1")))

