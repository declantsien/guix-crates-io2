(define-module (crates-io pa re parentree) #:use-module (crates-io))

(define-public crate-parentree-0.0.1 (c (n "parentree") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "0smcf2bqfa5lpaixzxvqb7nhf2qj9dfbfjid4dmmg18n9cs0qr7f")))

