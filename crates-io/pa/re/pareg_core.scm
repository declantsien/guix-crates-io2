(define-module (crates-io pa re pareg_core) #:use-module (crates-io))

(define-public crate-pareg_core-0.1.0 (c (n "pareg_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "05gc3zf6gircd5ynic2g3b5pjqfym7vfqx9qb5fypqq4w9ps69vg")))

(define-public crate-pareg_core-0.1.1 (c (n "pareg_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1fmnp7d6d6c71san0x9kdayx2skmw1jkzrzzlkl99wawkadi93cf")))

(define-public crate-pareg_core-0.2.0 (c (n "pareg_core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1gnbqybwnc4krixak4fjx10mzm5csrprf9aq9i75vrb8kql9h8pv")))

(define-public crate-pareg_core-0.3.0 (c (n "pareg_core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "05q1x1nrda9cl5nq2gkyknfxbgs33cgpwnm7brkgn2lwb2qw3kn8")))

