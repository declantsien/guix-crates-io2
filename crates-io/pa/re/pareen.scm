(define-module (crates-io pa re pareen) #:use-module (crates-io))

(define-public crate-pareen-0.1.0 (c (n "pareen") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0y178jwhd0l0gslg7ihlidv5nyh8v5db2hskcqpr0xpmw30990l0")))

(define-public crate-pareen-0.1.1 (c (n "pareen") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nxkx062xxlly4dh71fxcd2fcvvsfpi5ys5y00hq8hj9zqf2lnnm")))

(define-public crate-pareen-0.1.2 (c (n "pareen") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14i6dqjj8xxpg4cbzwrwaiy2csyr1xwkswa0r1fgflns6jspyl0c")))

(define-public crate-pareen-0.1.3 (c (n "pareen") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yjqpaqvqgm8jmwlzzv7safmi8q867n4qj2khfnxsavxb77js0ig")))

(define-public crate-pareen-0.2.0 (c (n "pareen") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0imbr2x9cw4hnb83a14cjd6a46fdlnna5xramf1z7c9h79vkc4lk")))

(define-public crate-pareen-0.2.1 (c (n "pareen") (v "0.2.1") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "001a949280bkdi31zzi0x88wjypxia2yczqrnyzk623nxwd3dibw")))

(define-public crate-pareen-0.2.2 (c (n "pareen") (v "0.2.2") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0sqncln4nvdcmx34jsb0iszbbcq93yfnwxzydybhyjnzcny9lg1h")))

(define-public crate-pareen-0.2.3 (c (n "pareen") (v "0.2.3") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "035jhgdskwrgzy1y401bg8yrwr8kdljscv2xg62s4c88adkxh3a5")))

(define-public crate-pareen-0.2.4 (c (n "pareen") (v "0.2.4") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sn4h9m4yhmrlzvxsmzzxx82as8z2ck10sx3pxv3b96jv8xl877d") (y #t)))

(define-public crate-pareen-0.2.5 (c (n "pareen") (v "0.2.5") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ci4mmbsxy6zlrbn94bbs7b69smxy25y3w3adhbgjjyd1ihv14s7")))

(define-public crate-pareen-0.2.6 (c (n "pareen") (v "0.2.6") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j81v0x5h1r4pm0l46rw15fks64q8ln8p5b2ha3r8c523afh3h7f")))

(define-public crate-pareen-0.3.0 (c (n "pareen") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n95v0q30wzcy7ap9ijf7nzxgz0x2phlqg2j717vanwrgfkwmrmy")))

(define-public crate-pareen-0.3.1 (c (n "pareen") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jm9mj25ajrp4ay6kpjwmmd1gzi3d3m1slym72kn9yinsk39hwim")))

(define-public crate-pareen-0.3.2 (c (n "pareen") (v "0.3.2") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09firhvixgdk9yhh75wrfsg9hw71lrby3g1bwkrr86n3q68shakm")))

(define-public crate-pareen-0.3.3 (c (n "pareen") (v "0.3.3") (d (list (d (n "assert_approx_eq") (r "^1.0") (d #t) (k 2)) (d (n "easer") (r "^0.2") (o #t) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1p3833ymnb1r3x5fanz1d303w4k68w93q2dkpqd6hgpk1j121rxj") (f (quote (("std" "alloc" "num-traits/std") ("libm" "num-traits/libm") ("default" "std") ("alloc"))))))

