(define-module (crates-io pa re pared) #:use-module (crates-io))

(define-public crate-pared-0.1.0 (c (n "pared") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1kmn9ymajx63vhqg9807j87il2f6vkkli0vq1ais1kwymiaxgsvb") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pared-0.2.0 (c (n "pared") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0g1ha4c7nhyjg0bajcppiymn9p5v7fcg5ilq38a0301b1pss5ib6") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pared-0.2.1 (c (n "pared") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1y2n9m6fny08ii0mkzcgj65yrdd0587qkvc0f4azdpl6187r5cha") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-pared-0.2.2 (c (n "pared") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0g2pnibfxb2c3f6n1752bmnjkbnxf18bsa1bnvj5yh6b1pjf77fm") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-pared-0.2.3 (c (n "pared") (v "0.2.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1r31i4b279jfjap0hhd2fk79cy0bc8ykc8mhj5prpkrg2n12cyd7") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-pared-0.3.0 (c (n "pared") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0wy0x6n90jdrwsr06scsqy3v6b3rl2vqy5py82zvdpsng159z1mx") (f (quote (("std") ("default" "std")))) (r "1.56")))

