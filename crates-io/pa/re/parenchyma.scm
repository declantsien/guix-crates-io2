(define-module (crates-io pa re parenchyma) #:use-module (crates-io))

(define-public crate-parenchyma-0.0.2 (c (n "parenchyma") (v "0.0.2") (h "1fchdxxg4r583a8grpjqzl87v0a1rs1knzcqxi4nccx4m9vf2685") (f (quote (("unstable_alloc"))))))

(define-public crate-parenchyma-0.0.3 (c (n "parenchyma") (v "0.0.3") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "1rsp63b7g817w0zqf1zpb6n7i1kj2fjz6i864yv849pjx5zv727y")))

(define-public crate-parenchyma-0.0.3-1 (c (n "parenchyma") (v "0.0.3-1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "1mimak2lj66cv2ii5ch9h8c9nwr1vrl0nm0bj3xbvni2zp0vab1r")))

(define-public crate-parenchyma-0.0.32 (c (n "parenchyma") (v "0.0.32") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "1r8n2wxng3cpk51f07yf7sqv2k36nxywa5rc0h8kgwfa0xx6xaxp")))

(define-public crate-parenchyma-0.0.33 (c (n "parenchyma") (v "0.0.33") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "082p68bivx6w7mw0d0y9w331m466xm4lnyh50hy763k0bmzf3zbf")))

