(define-module (crates-io pa re pareg_proc) #:use-module (crates-io))

(define-public crate-pareg_proc-0.1.0 (c (n "pareg_proc") (v "0.1.0") (d (list (d (n "pareg_core") (r "^0.1.0") (d #t) (k 0)))) (h "13g3xdykxba2xqq686gj35y9rx202hcbb2jxc65lfpzqaslf5naj")))

(define-public crate-pareg_proc-0.1.1 (c (n "pareg_proc") (v "0.1.1") (d (list (d (n "pareg_core") (r "^0.1.1") (d #t) (k 0)))) (h "0ngrai2hzjhdmlhvsd7f6r31s5gp1wrwfvcxsvm0x4g2gv8slbb7")))

(define-public crate-pareg_proc-0.2.0 (c (n "pareg_proc") (v "0.2.0") (d (list (d (n "pareg_core") (r "^0.2.0") (d #t) (k 0)))) (h "06cd315hpwg5jj28ciiaxgkl7axrr5k9v9yk37l2iikgxysvjh65")))

(define-public crate-pareg_proc-0.3.0 (c (n "pareg_proc") (v "0.3.0") (d (list (d (n "pareg_core") (r "^0.3.0") (d #t) (k 0)))) (h "18l9jscwij7hmx4iyjhk2hfldblw1ycq36p93wx8c9k83gj9cx7d")))

