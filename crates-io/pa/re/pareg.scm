(define-module (crates-io pa re pareg) #:use-module (crates-io))

(define-public crate-pareg-0.1.0 (c (n "pareg") (v "0.1.0") (d (list (d (n "pareg_core") (r "^0.1.0") (d #t) (k 0)) (d (n "pareg_proc") (r "^0.1.0") (d #t) (k 0)))) (h "0whmvmgdicb3klasckkf68gphq258zj7nr0mfm4r5q51knpzvb60")))

(define-public crate-pareg-0.1.1 (c (n "pareg") (v "0.1.1") (d (list (d (n "pareg_core") (r "^0.1.1") (d #t) (k 0)) (d (n "pareg_proc") (r "^0.1.1") (d #t) (k 0)))) (h "017arrrhvdh0m44ciw35c9p6si1dac82651j2f8dshbngpra1zgq")))

(define-public crate-pareg-0.2.0 (c (n "pareg") (v "0.2.0") (d (list (d (n "pareg_core") (r "^0.2.0") (d #t) (k 0)) (d (n "pareg_proc") (r "^0.2.0") (d #t) (k 0)))) (h "0vbnir8ycndv5xb08xw0nadsz0yicfas9fi0v4f2h6nlzli00iqz")))

(define-public crate-pareg-0.3.0 (c (n "pareg") (v "0.3.0") (d (list (d (n "pareg_core") (r "^0.3.0") (d #t) (k 0)) (d (n "pareg_proc") (r "^0.3.0") (d #t) (k 0)))) (h "0l338mxlb7nvzbl6ivrfydv9y0vinrqw8ixzhxyny8v2xdr5k9fb")))

