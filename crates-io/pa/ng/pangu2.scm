(define-module (crates-io pa ng pangu2) #:use-module (crates-io))

(define-public crate-pangu2-0.1.0 (c (n "pangu2") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "191q1mvzrqlvwsfqgmil67q4l6w6frvw9b1wpk4kmr9fggax818c")))

(define-public crate-pangu2-0.1.1 (c (n "pangu2") (v "0.1.1") (d (list (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0d7qsq4imrp7wyzyjwyqh6xbp7ycs2c1krxfj0j7rd1kfgca4sz5")))

