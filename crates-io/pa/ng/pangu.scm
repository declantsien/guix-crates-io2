(define-module (crates-io pa ng pangu) #:use-module (crates-io))

(define-public crate-pangu-0.0.1 (c (n "pangu") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "18f52bccddz63hvxg5s9kl166l015bjq6qlay14hp4cpa535yc9c")))

(define-public crate-pangu-0.1.0 (c (n "pangu") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1gmg74cfx133yhlq5dh45z48z0m2d2w17n25kv2jq9v3nj23zd2h")))

(define-public crate-pangu-0.2.0 (c (n "pangu") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00abp8lhr1ipd6kpz7sd30mg51nsqcwxk7sslk3gmspphjf7gp6a")))

