(define-module (crates-io pa wa pawawwewism) #:use-module (crates-io))

(define-public crate-pawawwewism-0.1.0 (c (n "pawawwewism") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16sy0z1nv22bw6qvip3dba7isyqx3v6sqg05q2ca5s0yk031r1sz")))

(define-public crate-pawawwewism-0.1.1 (c (n "pawawwewism") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0l8ki9q3jn1v0zzcvrp54j5g563spm0vszfkdwxd19g25cp54khg")))

(define-public crate-pawawwewism-0.1.2 (c (n "pawawwewism") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07swvhvks1vk1vx2qg25mvc4i7dxv9qzx7nzialfv7a11cz6z5mc")))

(define-public crate-pawawwewism-0.1.3 (c (n "pawawwewism") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dsfr22vrznxq26yahbgx2pp8xgjd2nv0453a3ara5jv3a74ayp5")))

