(define-module (crates-io pa tp patp) #:use-module (crates-io))

(define-public crate-patp-0.1.0 (c (n "patp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xinpjd0vyqh6d2pxa10l84igd9l6w3bwz557axpsvxzbdgy3wlh") (y #t)))

(define-public crate-patp-0.1.1 (c (n "patp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nd7s21r166rkgsddyd4v7mrvcv89d71yknsi662ls975z5wjdd4")))

(define-public crate-patp-0.1.2 (c (n "patp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g3c7h6nvrllxx3azhp6g5w5226w6345mrhzplzks5p9bdbpjns6")))

