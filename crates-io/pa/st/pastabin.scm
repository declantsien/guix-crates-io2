(define-module (crates-io pa st pastabin) #:use-module (crates-io))

(define-public crate-pastabin-0.1.0 (c (n "pastabin") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "11vshq2xwrx728yz68dwsc9bzdhn9r6367wsvssr9lv2y5h8chm4")))

(define-public crate-pastabin-0.1.1 (c (n "pastabin") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "18byfq5jw8b79r7vd0riybgrwywq1c9frlys4s85nc8fags06npi")))

(define-public crate-pastabin-0.1.2 (c (n "pastabin") (v "0.1.2") (d (list (d (n "magic-crypt") (r "^3.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0am14vyrlmwkmlx17z3y8xi5wiws7ij85mdlpz0n0dc60kag8z3l")))

