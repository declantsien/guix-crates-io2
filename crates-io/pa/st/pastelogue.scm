(define-module (crates-io pa st pastelogue) #:use-module (crates-io))

(define-public crate-pastelogue-0.1.0 (c (n "pastelogue") (v "0.1.0") (d (list (d (n "kamadak-exif") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1xsf3g51am9zvsqgk5c9aq8kv1pzabqskv0zbaf1lckcav1glf7d")))

