(define-module (crates-io pa st pastel_colours) #:use-module (crates-io))

(define-public crate-pastel_colours-0.1.1 (c (n "pastel_colours") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "02cm584gmm4zgzxrh1gkws1304l7w676jigb3ka5ai9v58g5jh5p")))

(define-public crate-pastel_colours-0.1.0 (c (n "pastel_colours") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1g26268h2rg95smyny1z3mpzziq9hkp7cpy5cj17d7q520vw8ch1")))

(define-public crate-pastel_colours-0.1.2 (c (n "pastel_colours") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1f5k0dd47xsa559drm2zwf0x4g19zhbr5f81y4qwsci62fcdsz4s")))

(define-public crate-pastel_colours-0.1.3 (c (n "pastel_colours") (v "0.1.3") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0nfpf1pzk22g2r01g6gb38hj3qmr7xgd81jajlzf91k7a4cbfr1l")))

(define-public crate-pastel_colours-0.2.0 (c (n "pastel_colours") (v "0.2.0") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1gr99dkllcy4msq5b07lkpdll4q1fbsbbcx2172md60wazy9lqrr")))

