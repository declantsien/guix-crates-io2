(define-module (crates-io pa st paste) #:use-module (crates-io))

(define-public crate-paste-0.1.0 (c (n "paste") (v "0.1.0") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0v285fl6x5c3plx2chc1kcm63c9wrjzv352iawd1zk6iryiizbqq")))

(define-public crate-paste-0.1.1 (c (n "paste") (v "0.1.1") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "08lmx6ibcpfhsbf9w6wgkf7wb676kd58ma8fr12kh5hmw4nnrzw7")))

(define-public crate-paste-0.1.2 (c (n "paste") (v "0.1.2") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1b3w1dvv9kk43vgxslxxqbldvpkfg54qagc7lqf1swp2j5c2miai")))

(define-public crate-paste-0.1.3 (c (n "paste") (v "0.1.3") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "18q7fa8pfkpmgl4s8izm22rmc1jmybwdvfdybnyvxzq246w1lvr8")))

(define-public crate-paste-0.1.4 (c (n "paste") (v "0.1.4") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "17aga345115vfz224qnngcq7vm5n1v24qhbkjbpgp4jh4v8r40zm")))

(define-public crate-paste-0.1.5 (c (n "paste") (v "0.1.5") (d (list (d (n "paste-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ygs077hlq8qlx5y46sfgrmhlqqgkmvvhn4x3y10arawalf4ljhz")))

(define-public crate-paste-0.1.6 (c (n "paste") (v "0.1.6") (d (list (d (n "paste-impl") (r "= 0.1.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0jdaxr0hpkim6as7ywx7x3f479igxpczj85pfcg8z0kf3jg52fj2")))

(define-public crate-paste-0.1.7 (c (n "paste") (v "0.1.7") (d (list (d (n "paste-impl") (r "= 0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "0in0dqar8s16w6gbwyzwvckm80ala02pq87innx1w6yp73kszqb3")))

(define-public crate-paste-0.1.8 (c (n "paste") (v "0.1.8") (d (list (d (n "paste-impl") (r "= 0.1.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1j8i50j2y1811x4g0mazk4s7xkm007r6mhwh9hn5bnqxx3hw34l2")))

(define-public crate-paste-0.1.9 (c (n "paste") (v "0.1.9") (d (list (d (n "paste-impl") (r "= 0.1.9") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cir81r4xrrnkvzjnxbi6lcjrz15pagli1chsjxp0zw4ywdpjb89")))

(define-public crate-paste-0.1.10 (c (n "paste") (v "0.1.10") (d (list (d (n "paste-impl") (r "= 0.1.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yk4zbi7128dcrklsbwfa63d39x0dv8f7pdbrylvdlcj0s9v2kxb")))

(define-public crate-paste-0.1.11 (c (n "paste") (v "0.1.11") (d (list (d (n "paste-impl") (r "= 0.1.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ip15mfhg7ywl2z29pb1sr51y05m5cbdj950wdx4zwv39xs9gj53")))

(define-public crate-paste-0.1.12 (c (n "paste") (v "0.1.12") (d (list (d (n "paste-impl") (r "= 0.1.12") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xhlaasb4h2krhpv05fc5mfzac7ihj099c5rlp5fv4n6b0f9n8ha")))

(define-public crate-paste-0.1.13 (c (n "paste") (v "0.1.13") (d (list (d (n "paste-impl") (r "= 0.1.13") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "01isn6vbi53ai5f73w5s0m3gcbjjlwlx44bzf4ijliv1jghjg3v7")))

(define-public crate-paste-0.1.14 (c (n "paste") (v "0.1.14") (d (list (d (n "paste-impl") (r "= 0.1.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cgmc59hhgnrf1ryrq2q28vwn020h7cr1j6yj6psgy4h5gvyhc9l")))

(define-public crate-paste-0.1.15 (c (n "paste") (v "0.1.15") (d (list (d (n "paste-impl") (r "=0.1.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0srlbc516ayfzq27m97rqc4lsrsn95w8gy397f6w08blsgf82cfm")))

(define-public crate-paste-0.1.16 (c (n "paste") (v "0.1.16") (d (list (d (n "paste-impl") (r "=0.1.16") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1v1dvjxznn34p431cibxdba86g63kxxvywb3d7p3hp0yxcp4j26m")))

(define-public crate-paste-0.1.17 (c (n "paste") (v "0.1.17") (d (list (d (n "paste-impl") (r "=0.1.17") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1040isbbjn44ng8qa5zvkhzx2n6lasb5mv5z481vwqjk4kz66v02")))

(define-public crate-paste-0.1.18 (c (n "paste") (v "0.1.18") (d (list (d (n "paste-impl") (r "=0.1.18") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10587zrlmzhq66yhd0z36fzglf32m1nlhi9bxxm6dgl0gp3j1jj5")))

(define-public crate-paste-1.0.0 (c (n "paste") (v "1.0.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0waxph8j4w0458n8pa7n8wj9qnprfsb8pdy718cdj0fy8phwipgn")))

(define-public crate-paste-1.0.1 (c (n "paste") (v "1.0.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "131wax27p3yw3x4xhy4r5limfawj0wasd4z0pcyn96fgshkay805")))

(define-public crate-paste-1.0.2 (c (n "paste") (v "1.0.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15zdi22xw6wbwxpvp5z08bb6v8lnsmq0rdssvggjvl0f32if2yms")))

(define-public crate-paste-1.0.3 (c (n "paste") (v "1.0.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1adza85rax0l60d97x0ay7djyg8wy29dvz39ss7dakk6n21v0lbi")))

(define-public crate-paste-1.0.4 (c (n "paste") (v "1.0.4") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1hfikh0bds8hqn371l2wf039mp6b5wrmwrwg96jcs6lkjm6mrmn5")))

(define-public crate-paste-1.0.5 (c (n "paste") (v "1.0.5") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0n7y1pabn6vspdxgzx62rs9wdlbnay9r1g84j8jk2pn6s1x59gxc")))

(define-public crate-paste-1.0.6 (c (n "paste") (v "1.0.6") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1dcg6ll2in45066kvramw83cp1p0vcbafl6bjkrxfv8szrm14i07") (r "1.31")))

(define-public crate-paste-1.0.7 (c (n "paste") (v "1.0.7") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1z15h1rnq1wcacpcvgm77djl3413gs1nlhmn90qpcvjx2c2hwlhc") (r "1.31")))

(define-public crate-paste-1.0.8 (c (n "paste") (v "1.0.8") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "08jv4b10pjdzxqgcn2id9216m30yjlhnylvs50lkc13s5yry48wl") (r "1.31")))

(define-public crate-paste-1.0.9 (c (n "paste") (v "1.0.9") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q8q3ygjcdm90ai29yjywfkhsjnby3rfsyizyy1sq1dr3xajxpmi") (r "1.31")))

(define-public crate-paste-1.0.10 (c (n "paste") (v "1.0.10") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jsb961ka244jwqhiwh5gdsfhfm8am9scjwi3h2g3hk649s2q76g") (r "1.31")))

(define-public crate-paste-1.0.11 (c (n "paste") (v "1.0.11") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fpf7n6wvlf1pg5xplgaknaim5kgrbmigpcq005hf02d8b85n6nh") (r "1.31")))

(define-public crate-paste-1.0.12 (c (n "paste") (v "1.0.12") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ybxr9wjw3fi0ha008cqfx08vk1iakqq5pbl77i3zym8cm06qx4z") (r "1.31")))

(define-public crate-paste-1.0.13 (c (n "paste") (v "1.0.13") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0d9brkgzxq8izy3c6gqagi8lpjydi6a0ghmg3312549npsvpmcml") (r "1.31")))

(define-public crate-paste-1.0.14 (c (n "paste") (v "1.0.14") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0k7d54zz8zrz0623l3xhvws61z5q2wd3hkwim6gylk8212placfy") (r "1.31")))

(define-public crate-paste-1.0.15 (c (n "paste") (v "1.0.15") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "02pxffpdqkapy292harq6asfjvadgp1s005fip9ljfsn9fvxgh2p") (r "1.31")))

