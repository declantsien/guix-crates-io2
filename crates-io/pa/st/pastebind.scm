(define-module (crates-io pa st pastebind) #:use-module (crates-io))

(define-public crate-pastebind-0.1.0 (c (n "pastebind") (v "0.1.0") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongo_driver") (r "^0.12") (d #t) (k 0)) (d (n "pastebin") (r "^0.17") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1lwxj8w3viv65xaynfkpa1ykmwcaarqzb8bknxqqmr7497m2968p")))

(define-public crate-pastebind-0.1.1 (c (n "pastebind") (v "0.1.1") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongo_driver") (r "^0.12") (d #t) (k 0)) (d (n "pastebin") (r "^0.17") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0r1a30yfabgc40ydh3cfdsa1dajxcds8h8iaxfbixpp4r90ldlsg")))

