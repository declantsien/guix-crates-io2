(define-module (crates-io pa st pasture-derive) #:use-module (crates-io))

(define-public crate-pasture-derive-0.1.0 (c (n "pasture-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lhc9wbd14mkksqjm5aypjisbqgawzp5p8ihs2p8c7shgsaqnxzm")))

(define-public crate-pasture-derive-0.2.0 (c (n "pasture-derive") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jxf87qi7a8f738z18shs1185m8dmmyja9mchhnqja8kd0hgvf5g")))

(define-public crate-pasture-derive-0.4.0 (c (n "pasture-derive") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vkg0h4qyswsw87q1vn0kix5bi325kg0a1shwq1sxz0migjjhqn0")))

