(define-module (crates-io pa st pastemyst) #:use-module (crates-io))

(define-public crate-pastemyst-0.1.0 (c (n "pastemyst") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "026iq8561379gyaav2zgsrjcs4d8vzcab10lmx8s0b5s7ly3xnaf")))

(define-public crate-pastemyst-0.2.25 (c (n "pastemyst") (v "0.2.25") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1vj27jhr56gjxqnh3v5chf0lp1pcr74wlnayh5dxcs285ag7mxax")))

(define-public crate-pastemyst-0.7.75 (c (n "pastemyst") (v "0.7.75") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "172rq02gci54wm069ark5klhkawfb005r1c11xlz7ajxlsivl8db")))

(define-public crate-pastemyst-0.8.0 (c (n "pastemyst") (v "0.8.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "014hqk9rwnapjbbf5064ys033m4ikd15brhx99d7z4arg8b89q8g")))

(define-public crate-pastemyst-1.0.0 (c (n "pastemyst") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0prvk2k60ghrsr29lxx2w3x9niaxqv0p15b5p11ymqcw129jx2sh")))

