(define-module (crates-io pa st pasts) #:use-module (crates-io))

(define-public crate-pasts-0.0.1 (c (n "pasts") (v "0.0.1") (h "10qffa16vshyj1rzs1djicj5w85gjqqjzf9c7ddvj9wp693gzx6d") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.1.0 (c (n "pasts") (v "0.1.0") (h "04rzgpik5pigzqcwc30q398pib364cvpgvdcywg61y5hfn267nk1") (f (quote (("std") ("docs-rs") ("default" "std"))))))

(define-public crate-pasts-0.2.0 (c (n "pasts") (v "0.2.0") (h "1csvgy8qjcadrb5i6hgas5qqgrwx2965m5mbzalrcalcv8mm1mri") (f (quote (("std") ("docs-rs") ("default" "std"))))))

(define-public crate-pasts-0.3.0 (c (n "pasts") (v "0.3.0") (h "1l3rdr75li9n9gr63h0hd5ifw8b7lcvc66ar334a0nxdhrld9x8b") (f (quote (("std") ("docs-rs") ("default" "std"))))))

(define-public crate-pasts-0.4.0 (c (n "pasts") (v "0.4.0") (h "11rdczdhpazclhkbbjafv5nd9ybll9a110crhh67si0p5rdc6mz7") (f (quote (("std" "alloc") ("docs-rs") ("default" "std") ("alloc"))))))

(define-public crate-pasts-0.5.0 (c (n "pasts") (v "0.5.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "1zb4p5jbmb36rfmfgzm648ml5m1rn23qigbffqyi7pl93c27af0p") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.6.0 (c (n "pasts") (v "0.6.0") (d (list (d (n "async-std") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0sp1jvv50zhz0i7yfp2y4yrar6gp84634xig7p4ixcbv950n9zp5") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.7.0 (c (n "pasts") (v "0.7.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "045x8by8jv5iary3qfd27mypvc5f8y8ym58syr2yx9n2wq26rkfa") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.7.1 (c (n "pasts") (v "0.7.1") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "0h07x0lf9blsfjfnidfqfng49g7jfrxb4fix87jr4qswm67k96na") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.7.2 (c (n "pasts") (v "0.7.2") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "1awprwv6krjxmw7crp4y1ccca30fgbxk0vz84lv4i0y5sn6hdp00") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.7.3 (c (n "pasts") (v "0.7.3") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "0snj6cgvjvp9yva8phzqhkchzd6b8v5b639m096xs108jzph0hgg") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.7.4 (c (n "pasts") (v "0.7.4") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "091gaylwlax1d9hxw7wfg9hjvr0vxd3blb9gwskilcp4i3cin6v8") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.8.0 (c (n "pasts") (v "0.8.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "0jvzljwj0mpip793k8g78asr1n08mghg99xfm74adr921pq499lq") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.9.0 (c (n "pasts") (v "0.9.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "0msj6s1795aj26bgx8s1s7wzjpqhhsyvqsjv853s03r39fjai4xy") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.10.0 (c (n "pasts") (v "0.10.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)))) (h "1nbkgsvmlabj81rchl6niyssxs75b4501s5zc3l73i53f3w7gzl1") (f (quote (("std") ("default" "std"))))))

(define-public crate-pasts-0.11.0 (c (n "pasts") (v "0.11.0") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)))) (h "052dqi1lmvyvfyb23c0sncx1cbyy2x00ww9xylw1wvscfiik31pw") (f (quote (("web" "wasm-bindgen-futures") ("std") ("default" "std"))))))

(define-public crate-pasts-0.12.0 (c (n "pasts") (v "0.12.0") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)))) (h "16ny5rm0306by1b55waag1yimnn2ndjk8rd0aw8fc3wz4saawcza") (s 2) (e (quote (("web" "dep:wasm-bindgen-futures") ("no-std" "dep:pin-utils"))))))

(define-public crate-pasts-0.13.0 (c (n "pasts") (v "0.13.0") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yka598s239vypc8mly14y6zvswlidjxf6mfjvxv50pdlpk7147z") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

(define-public crate-pasts-0.13.1 (c (n "pasts") (v "0.13.1") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.2") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "whisk") (r "^0.10") (d #t) (k 2)))) (h "0pzhqg7f3g0ii7a638kzlr9r2fnq7mj7chqpqxrwl1iiqd7m1jk5") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

(define-public crate-pasts-0.14.0 (c (n "pasts") (v "0.14.0") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.2") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "whisk") (r "^0.10") (d #t) (k 2)))) (h "08q4glj5609byfixig41hlf3x0xiad8551jpb5g3vg1wwmc8h0v4") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

(define-public crate-pasts-0.14.1 (c (n "pasts") (v "0.14.1") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "whisk") (r "^0.10") (d #t) (k 2)))) (h "04g8r08vv1wy9r1l2w8rwxy496rfiv5kksji0gy1kaj0fhvhmmvn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

(define-public crate-pasts-0.14.2 (c (n "pasts") (v "0.14.2") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "whisk") (r "^0.11") (d #t) (k 2)))) (h "12pw1i5x5zx47bk1iq47sgyz93xah59j7600s3433sqs4srgqj2z") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

(define-public crate-pasts-0.14.3 (c (n "pasts") (v "0.14.3") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 2)) (d (n "async_main") (r "^0.4") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "whisk") (r "^0.11") (d #t) (k 2)))) (h "1klp3r9l29rb4qkgm4iancmljp676vvrmg6s8xx9gyvi70q3dkgg") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("web" "dep:wasm-bindgen-futures"))))))

