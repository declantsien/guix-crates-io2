(define-module (crates-io pa st paste_rs-cli) #:use-module (crates-io))

(define-public crate-paste_rs-cli-0.1.0 (c (n "paste_rs-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "006zqan4g2hhrbga4bgj9kha3iphf8hdga6idjx74001nljr4v62")))

(define-public crate-paste_rs-cli-0.2.0 (c (n "paste_rs-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1r1r8ymxzbbnl5yzz6w74mcy7j15isrwfhar0gr34n666m6q4z6f")))

(define-public crate-paste_rs-cli-0.2.1 (c (n "paste_rs-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1svj3fmbjnzd39lssb550pf5z21yvfy563dhd73f2i97rxw43z7l")))

(define-public crate-paste_rs-cli-0.3.0 (c (n "paste_rs-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste_rs_api") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1db2y90lhp39nyicinzzbl9m6qhmrskcx14fsl9vb3p7xdlnnd1s")))

(define-public crate-paste_rs-cli-0.3.2 (c (n "paste_rs-cli") (v "0.3.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste_rs_api") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1bf5q3hn1p4izmxc97159gzl847sd9hb17nqawk01zl4wm0zna5c")))

(define-public crate-paste_rs-cli-0.4.0 (c (n "paste_rs-cli") (v "0.4.0") (d (list (d (n "clap") (r "~3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste_rs_api") (r "~0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0nifcf6wljk32rmij64xdyn64arg7sgw5dx722m03jlw31cy6wv1")))

(define-public crate-paste_rs-cli-0.5.0 (c (n "paste_rs-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1ckg0a98dffz3abqcqayvyhhid3qvyvvn1bwwaxx0b0w616di6d4") (y #t)))

(define-public crate-paste_rs-cli-0.5.1 (c (n "paste_rs-cli") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1w5zq3bng3l2zybf8hk1n1slp70ngwsbl5nsw5yy6xyvriqfm2r2")))

(define-public crate-paste_rs-cli-0.6.0 (c (n "paste_rs-cli") (v "0.6.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1amhwxg00h3kw9sa1gcfc2n2g763cwx3w4v6a4gsrjygm291mqd5")))

(define-public crate-paste_rs-cli-0.7.0 (c (n "paste_rs-cli") (v "0.7.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1babn9nkxp6vzdq9ybn7p6kc0is3bkqdwqfvy3rdhkn5wfnf7vvs")))

(define-public crate-paste_rs-cli-0.7.1 (c (n "paste_rs-cli") (v "0.7.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1j560v61gjc8rj7m6bam7wd7lfff00vm0p1rvg5cpxvgzqlr74hj")))

