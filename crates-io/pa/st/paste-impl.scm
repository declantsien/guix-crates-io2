(define-module (crates-io pa st paste-impl) #:use-module (crates-io))

(define-public crate-paste-impl-0.1.0 (c (n "paste-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "06fgmgh9pmkmc99sxw8rdsbfnb9c73wwdsif6r1pd9bgx3pl1hcd")))

(define-public crate-paste-impl-0.1.1 (c (n "paste-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ab9yq10xcwyclwn072cdx1i1dqrg9sz07xbpll82zya1acjj6mk")))

(define-public crate-paste-impl-0.1.2 (c (n "paste-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "165wwcvd4gd0m6hbac9g51v9zd2sv9r8vjwmdbgb8y1vlj3ardxp")))

(define-public crate-paste-impl-0.1.3 (c (n "paste-impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0scjwphxdj76479hljsvc07268xgkfswbfkrz0cyqmn3np1xy1rc")))

(define-public crate-paste-impl-0.1.4 (c (n "paste-impl") (v "0.1.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0yj6v5j6if4461601r0jsn3dnfan6qz93bfwn8rqjpm5wcpm3kd3")))

(define-public crate-paste-impl-0.1.5 (c (n "paste-impl") (v "0.1.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1rkh8nixmb7r1y0mjnsz62p6r1bqah5ciri7bwhmgcmq4gk9drr6")))

(define-public crate-paste-impl-0.1.6 (c (n "paste-impl") (v "0.1.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ig6ahgh27kdz4midarryzx2y6xdi9za9a8vp11byqgg2blwj522")))

(define-public crate-paste-impl-0.1.7 (c (n "paste-impl") (v "0.1.7") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fwj11j5lhya5fjr4gfljxfm74ahlr09c8xbb8f22hzpyskw8kbd")))

(define-public crate-paste-impl-0.1.8 (c (n "paste-impl") (v "0.1.8") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0755b2x014dfvk57sj5rjwbkspx7hql07ba44m2yw1jzckr4772y")))

(define-public crate-paste-impl-0.1.9 (c (n "paste-impl") (v "0.1.9") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0aap6bw840j6q9r8z7kys75wzy6nggnbiaxvm5l6zk259kxj6v20")))

(define-public crate-paste-impl-0.1.10 (c (n "paste-impl") (v "0.1.10") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12jsm83dnsqnrcabfacnwcxh3h4kykl622vi7glv2wg527hqc956")))

(define-public crate-paste-impl-0.1.11 (c (n "paste-impl") (v "0.1.11") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12db6ir34wkh2v6mv3kdvaygg4p2h8ny88zwngbjsksrwf96zzb6")))

(define-public crate-paste-impl-0.1.12 (c (n "paste-impl") (v "0.1.12") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1356ijxcasp0x3062q06ynn468xxskjbb2vas7v7zrj7whwz42rf")))

(define-public crate-paste-impl-0.1.13 (c (n "paste-impl") (v "0.1.13") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c2ak58a4aw6hf5g5r40ixhmcamd9cxqf23vcxvy7n25i898k40l")))

(define-public crate-paste-impl-0.1.14 (c (n "paste-impl") (v "0.1.14") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01jnvr6y7nfz0yq07x93dk4pclwvzahgi2v0hj6lsa5sfb45zbr5")))

(define-public crate-paste-impl-0.1.15 (c (n "paste-impl") (v "0.1.15") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05wdlskzvh1847mwj9j43aca1iafj35yvldl28aafd60l47lkjh5")))

(define-public crate-paste-impl-0.1.16 (c (n "paste-impl") (v "0.1.16") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cidyy918bs1ainsrpqqsnj5hryvg6zb9yz5zhrv74iilsk2iww4")))

(define-public crate-paste-impl-0.1.17 (c (n "paste-impl") (v "0.1.17") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1c2ld6g7lminzlb1cz6krq21ns261wykranjvhvk48gcd2i834kv")))

(define-public crate-paste-impl-0.1.18 (c (n "paste-impl") (v "0.1.18") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1dlqzk05cx74522s4iyhyzzhszig4n401pp6r1qg6zmr02r7snnr")))

