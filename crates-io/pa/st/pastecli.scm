(define-module (crates-io pa st pastecli) #:use-module (crates-io))

(define-public crate-pastecli-0.1.0 (c (n "pastecli") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0nqdszf3crg6wfvfhrpp3715431l0n7f207rmpq6n3jwhdhfawhi")))

(define-public crate-pastecli-0.1.1 (c (n "pastecli") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "1s6f30rp8m2ra0vz76fb463ws0l1gsqg1q7alvmh37danydwmfdp")))

