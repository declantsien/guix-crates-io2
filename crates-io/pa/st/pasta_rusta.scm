(define-module (crates-io pa st pasta_rusta) #:use-module (crates-io))

(define-public crate-pasta_rusta-0.1.0 (c (n "pasta_rusta") (v "0.1.0") (h "1rynraw1m26bfihggk4hqb8r38ndpyrnb19pjzhj79i6k0fspw6h")))

(define-public crate-pasta_rusta-0.1.1 (c (n "pasta_rusta") (v "0.1.1") (h "11hj3f96p4q4ydniag4ks8bf4s6n0zyr32asr0jy5yy4fdg2cqjh")))

(define-public crate-pasta_rusta-0.1.2 (c (n "pasta_rusta") (v "0.1.2") (h "0nwv2rc35l9745ckk4icfh40qiq3b1g3jkhb8493krwm9rlf5qn8")))

(define-public crate-pasta_rusta-0.1.3 (c (n "pasta_rusta") (v "0.1.3") (h "14irwksi6japxiic1pzk5580isgdwqn469llvlhhkr4yhcc62kxw")))

(define-public crate-pasta_rusta-0.1.4 (c (n "pasta_rusta") (v "0.1.4") (h "10rw7c1ddf7y7yf9d12zgqsdlpzxf9m4ksxk6jrg1hmb6yzidzf1")))

(define-public crate-pasta_rusta-0.1.5-pre.1 (c (n "pasta_rusta") (v "0.1.5-pre.1") (h "1zfp2hcj55xv08zi4k5l5347071z89j5d6gyl9akb8m06hz80rjs")))

(define-public crate-pasta_rusta-0.1.5-hello (c (n "pasta_rusta") (v "0.1.5-hello") (h "1syxcfsfbnybqgdk34a3ypynicczk9vsldr91yv0a0p8ylrhqdnl")))

(define-public crate-pasta_rusta-0.1.6 (c (n "pasta_rusta") (v "0.1.6") (h "1pzqbpmmlw1fcj983bqzgrw2s40jd4la0472b22lqdsin2x0jsxa") (y #t)))

(define-public crate-pasta_rusta-0.1.6-release (c (n "pasta_rusta") (v "0.1.6-release") (h "0y6n2k2kxrhc6dwnswmh2af4pngbivcgagg94i3v7b4gvjqpwrhz")))

(define-public crate-pasta_rusta-0.1.6+elad-2 (c (n "pasta_rusta") (v "0.1.6+elad-2") (h "0v9ifp28zxy03d1d6g3c6c2p922f6lf6knvqa5zv4b9yvxh59kj5")))

