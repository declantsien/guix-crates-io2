(define-module (crates-io pa st pasteboard) #:use-module (crates-io))

(define-public crate-pasteboard-0.1.0 (c (n "pasteboard") (v "0.1.0") (d (list (d (n "cocoa") (r "^0.19.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0cxmld7nfvdvx0d1faj6yrj1ska06n0nzdiba70pc83z2qzp56jc")))

(define-public crate-pasteboard-0.1.1 (c (n "pasteboard") (v "0.1.1") (d (list (d (n "cocoa") (r "^0.19.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0v33sjqzdnwzq6npcna78i5phhrlrgnk85lccg6r5zq4l592n1z3")))

(define-public crate-pasteboard-0.1.2 (c (n "pasteboard") (v "0.1.2") (d (list (d (n "cocoa") (r "^0.24.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "os_info") (r "^3.0.1") (d #t) (k 1)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "18mi8xg7xsgzkh5gz1h81wqvzfy57n2yr362d630pz9dijix50x0")))

(define-public crate-pasteboard-0.1.3 (c (n "pasteboard") (v "0.1.3") (d (list (d (n "cocoa") (r "^0.24.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "os_info") (r "^3.0.1") (d #t) (k 1)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1rjwq10dvhkqb8xxa669ygf6f4k6l836z7karyc2090sy3f0r733")))

