(define-module (crates-io pa st pasty-rs) #:use-module (crates-io))

(define-public crate-pasty-rs-0.1.0 (c (n "pasty-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1gzi24iqqswlg0cambf6w6cghrdwr7a1wfqb1p831344r7jzbvxy")))

