(define-module (crates-io pa st pastebin_rust_api) #:use-module (crates-io))

(define-public crate-pastebin_rust_api-0.1.0 (c (n "pastebin_rust_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)))) (h "1mfp1yiir0bx9zv1s0hxprky9y3bflb4qawjd8fn4n1q7jxlxp6w") (y #t)))

(define-public crate-pastebin_rust_api-0.1.1 (c (n "pastebin_rust_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)))) (h "1dcy0w7f2w16qaylvyb1sn76mgzgdhcyn2aqzl7f5ixcipd27snk") (y #t)))

(define-public crate-pastebin_rust_api-0.1.11 (c (n "pastebin_rust_api") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)))) (h "15nmf10hl0wv5xd5nl0wsagvdkbs8n4m2c4nsf6nnbfn950y7qz2") (y #t)))

(define-public crate-pastebin_rust_api-0.2.11 (c (n "pastebin_rust_api") (v "0.2.11") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)))) (h "1famd5wkyf5668p9sb383yy93m5p1w7476kvibig6jzjvp28jmb4") (y #t)))

(define-public crate-pastebin_rust_api-0.3.11 (c (n "pastebin_rust_api") (v "0.3.11") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)))) (h "04g3rx860476q23i1sfdy0nai31xrbxdkyd73fqjp43ankwl070d") (y #t)))

(define-public crate-pastebin_rust_api-0.4.11 (c (n "pastebin_rust_api") (v "0.4.11") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "1mvbk7fsrr9idw5dl46k73wsflz2rfak0ad81ah1dcd0zdnpvc1m") (y #t)))

(define-public crate-pastebin_rust_api-0.4.12 (c (n "pastebin_rust_api") (v "0.4.12") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "0lcmrmq62ajv1knxffxay52qzkjlh71bix4mvhrabgmjdf6dy0w2") (y #t)))

(define-public crate-pastebin_rust_api-0.5.12 (c (n "pastebin_rust_api") (v "0.5.12") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "12dc39a329011yq7p0kg6g06f49xv1qmyyj8h1hwjdxgrlsxlswy") (y #t)))

(define-public crate-pastebin_rust_api-0.5.13 (c (n "pastebin_rust_api") (v "0.5.13") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "1r1m77iln3nlz5d6hjy7pji0v1yqcdp46hjzkc13q0323lrzxqkj")))

(define-public crate-pastebin_rust_api-0.5.14 (c (n "pastebin_rust_api") (v "0.5.14") (d (list (d (n "reqwest") (r "^0.5.2") (d #t) (k 0)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "14la0zjwmf6516lgpw0sy4gcf5b2h4d3dgqj6xrb2czfs944rnqb")))

