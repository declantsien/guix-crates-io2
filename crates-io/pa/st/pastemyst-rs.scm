(define-module (crates-io pa st pastemyst-rs) #:use-module (crates-io))

(define-public crate-pastemyst-rs-0.1.0 (c (n "pastemyst-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "116zgym4jn8815qgzlsli3jdxnhfs0n0byp5wbd11bv0a06mls30") (y #t)))

(define-public crate-pastemyst-rs-0.1.1 (c (n "pastemyst-rs") (v "0.1.1") (h "1vskk97s0p9z21nrwjq1fl9k08xgsfi7n92qf74qxm4h727aag6f")))

