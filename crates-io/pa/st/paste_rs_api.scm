(define-module (crates-io pa st paste_rs_api) #:use-module (crates-io))

(define-public crate-paste_rs_api-0.1.0 (c (n "paste_rs_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0wbwgpyy40ra6vz00h09pk28yi6gqnvjxif0l0k84p9wkmifrd5c") (y #t)))

(define-public crate-paste_rs_api-0.1.1 (c (n "paste_rs_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1cfmqbcdlm5b7gy613jwz5l7drqm9078150yyw502zyb85y7da5x") (y #t)))

(define-public crate-paste_rs_api-0.1.2 (c (n "paste_rs_api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1cz2l6zlznaw76m5vy937mh8wy78n6c9yc4zkd0xfbsz7i4ki6vq") (y #t)))

