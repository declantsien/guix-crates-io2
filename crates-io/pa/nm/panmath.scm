(define-module (crates-io pa nm panmath) #:use-module (crates-io))

(define-public crate-panmath-0.1.1 (c (n "panmath") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "05zfly81439808abfyb2ks55dskaj34840jwqc224qhd5j3sbgmb")))

(define-public crate-panmath-0.1.2 (c (n "panmath") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "1axkyk6l82qcy2cdfhjyvlmdalsi3brgl2ysi22hwka4m0i4m3h0")))

