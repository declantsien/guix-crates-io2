(define-module (crates-io pa kr pakr) #:use-module (crates-io))

(define-public crate-pakr-0.1.0 (c (n "pakr") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)))) (h "09rcri1pmq6mpsad0g3s79csflhi52v8328hhpibzj14k02ca70s")))

