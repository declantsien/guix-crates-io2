(define-module (crates-io pa kr pakr-assert-size) #:use-module (crates-io))

(define-public crate-pakr-assert-size-1.0.0 (c (n "pakr-assert-size") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jigi9f86rkg6bbh9faffs1aqj019hyc7ggaib74vi0pgvg485i1") (y #t) (r "1.57.0")))

(define-public crate-pakr-assert-size-1.0.1 (c (n "pakr-assert-size") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s6a8kpii8ilmyizpyr01qgbiicjq6958zgniqxz7pa6wbvb7ay9") (r "1.57.0")))

