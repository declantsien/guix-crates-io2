(define-module (crates-io pa kr pakr-mio-afpacket) #:use-module (crates-io))

(define-public crate-pakr-mio-afpacket-0.2.0 (c (n "pakr-mio-afpacket") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("tcp" "os-util" "os-poll"))) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 2)))) (h "1ynj8kqxmhw44q4bz6g11nc9qycqlnxi18bfdmnzwrinm6qqfx30")))

