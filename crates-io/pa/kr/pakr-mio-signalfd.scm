(define-module (crates-io pa kr pakr-mio-signalfd) #:use-module (crates-io))

(define-public crate-pakr-mio-signalfd-1.0.0 (c (n "pakr-mio-signalfd") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-util"))) (d #t) (k 0)) (d (n "pakr-signals") (r "^1.0") (d #t) (k 0)))) (h "1v66xa0wa9x6aayjcsi8i6h4ph324l3lxbf0q6cbix37a8a6hm2x")))

