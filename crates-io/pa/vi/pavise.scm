(define-module (crates-io pa vi pavise) #:use-module (crates-io))

(define-public crate-pavise-0.1.0 (c (n "pavise") (v "0.1.0") (h "0sgv0w5agzjvvhqhp4p4frh4p9ran5ziddhz01lfxj99kpbqljpy") (y #t)))

(define-public crate-pavise-0.0.1 (c (n "pavise") (v "0.0.1") (h "0kb5n3khz299napfz0f64d4rdfbagsnwwlhy2k50n5fpcgdpn7nv")))

