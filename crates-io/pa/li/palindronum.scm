(define-module (crates-io pa li palindronum) #:use-module (crates-io))

(define-public crate-palindronum-0.0.1 (c (n "palindronum") (v "0.0.1") (h "04k8byifz3rzrmqxdzx9w13wnnq5afgqml25bkfnszgx1jpmr36q")))

(define-public crate-palindronum-0.0.2 (c (n "palindronum") (v "0.0.2") (h "0jvmhpc369am1jzwvdjs3w44ibyia9hsh4g2y829qxvhbg6yqv8k")))

(define-public crate-palindronum-0.0.3 (c (n "palindronum") (v "0.0.3") (h "16hvk40q07d12msbgrdn1l8vcrgncs4yjq809nrdlz0aqndbdy13")))

(define-public crate-palindronum-0.0.4 (c (n "palindronum") (v "0.0.4") (h "15547m7xjjms8qsdbj41gzn1w6bw06y8idswyvcg62hz3h09y8ba")))

(define-public crate-palindronum-1.0.0 (c (n "palindronum") (v "1.0.0") (h "1gwcg698fvdxwad0zrxg2s4m0mbs3fzcxwky3nvlg4ix2y65jp9i")))

(define-public crate-palindronum-1.0.1 (c (n "palindronum") (v "1.0.1") (h "0ss3qhhrb4vaafqqf8dzfs11mn52rkcixi6ns5qx4xyp7av2qnhk")))

(define-public crate-palindronum-1.0.2 (c (n "palindronum") (v "1.0.2") (h "1d87gigw08s1vj10sc2xnim474r0l8xp19dvdvylhyydlm4fx8cn")))

