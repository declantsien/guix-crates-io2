(define-module (crates-io pa li palindrome) #:use-module (crates-io))

(define-public crate-palindrome-0.1.0 (c (n "palindrome") (v "0.1.0") (h "101a4rccq9fpql68kqiv1pxfxz7bscbqhd0ry5k6qmgdvinyjphh")))

(define-public crate-palindrome-0.1.1 (c (n "palindrome") (v "0.1.1") (h "0687kzrv0si9sgkhwny2rfr2y5phx3rn4ma3izsg2qppjpc2s3md")))

