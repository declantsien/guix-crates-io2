(define-module (crates-io pa li palin) #:use-module (crates-io))

(define-public crate-palin-0.1.0 (c (n "palin") (v "0.1.0") (h "117ysvjzrin8z4g7l7mzagf4m5zqdj3wni9yri6mfb0w4024g0jg")))

(define-public crate-palin-0.2.0 (c (n "palin") (v "0.2.0") (h "12d64lfh6ar6kakwb4zrb2s4h9rvlalwr8qadgyqf96lgd5jhz2k")))

(define-public crate-palin-0.3.0 (c (n "palin") (v "0.3.0") (h "1g3vls1n8y0jc9q7aks293cvjrn85qjjwdi4y20nxbnm6bahi2xh")))

(define-public crate-palin-0.4.0 (c (n "palin") (v "0.4.0") (h "1m0hyz11z9g4i12qkvqz7k00769fk6n3xv07gwfwdxvmkb96a3sg")))

