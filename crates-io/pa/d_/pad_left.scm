(define-module (crates-io pa d_ pad_left) #:use-module (crates-io))

(define-public crate-pad_left-0.1.0 (c (n "pad_left") (v "0.1.0") (h "17ac48m419j03s54l30kfyq09jcmgcabsry7rkkr7lx5dnzdk8zj")))

(define-public crate-pad_left-0.1.1 (c (n "pad_left") (v "0.1.1") (h "1anwmgnpbkw98v65cfni43hh3bs2d3fc9cg1pggirgvl5rl15dlq")))

(define-public crate-pad_left-0.1.2 (c (n "pad_left") (v "0.1.2") (h "1sbpg4lncs2src71lqwd15i8axnkrs8g7q16p7c3d9br01l5n5r8")))

