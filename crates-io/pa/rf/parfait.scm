(define-module (crates-io pa rf parfait) #:use-module (crates-io))

(define-public crate-parfait-0.1.0 (c (n "parfait") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1silm8syhks06jm030c1xh2n48bhyjykvkb2rdm17rnc6l9s886a")))

(define-public crate-parfait-0.1.1 (c (n "parfait") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01vpvifnlsbkj0n29r7k92hj925f6h6fr198n0h9nhyizmsq2rcl")))

