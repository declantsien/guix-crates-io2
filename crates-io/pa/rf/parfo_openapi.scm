(define-module (crates-io pa rf parfo_openapi) #:use-module (crates-io))

(define-public crate-parfo_openapi-0.1.0 (c (n "parfo_openapi") (v "0.1.0") (h "14vq8q3j7f6vigcd6jjx8k7fh790fr15irp06r54bf6cbzxwhfa7") (y #t)))

(define-public crate-parfo_openapi-0.1.1-alpha.1 (c (n "parfo_openapi") (v "0.1.1-alpha.1") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 2)))) (h "0f29vhy7nd01x4qpx156dnzpv6dvv275di3h2v4y4sydpapwdlhf") (y #t)))

(define-public crate-parfo_openapi-0.1.1-alpha.2 (c (n "parfo_openapi") (v "0.1.1-alpha.2") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 2)))) (h "1kg97rqr8b8vj40r1pqsd9gf6phz3nq5flj94w90isznl2iic29s") (y #t)))

