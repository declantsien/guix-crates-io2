(define-module (crates-io pa hi pahi-olin) #:use-module (crates-io))

(define-public crate-pahi-olin-0.1.0 (c (n "pahi-olin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13") (d #t) (k 0)))) (h "064ly3n6si7jb0sgn6lp8pnpjn3bj0bm9mkv5yg48qqnni37rrnf")))

(define-public crate-pahi-olin-0.2.0 (c (n "pahi-olin") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (k 0)))) (h "0glzyjasik0h2ab4h38ak6fzfc0gdrkajim5v8v98kyxn0jv4vg3")))

