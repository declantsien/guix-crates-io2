(define-module (crates-io pa wb pawb) #:use-module (crates-io))

(define-public crate-pawb-0.1.0 (c (n "pawb") (v "0.1.0") (h "1dc8a7bimi729hib67lhm7skqhb8rgsjdrhn42wc97hs8lb753i5")))

(define-public crate-pawb-0.0.1 (c (n "pawb") (v "0.0.1") (h "1r0dfrm7fgwgx6a0n39bhpckr2gscxwqn6gj9y823hl8rrp2rln8")))

(define-public crate-pawb-0.1.1 (c (n "pawb") (v "0.1.1") (h "17ziqw6wmj6h4r52gdmklamq3k6mb7m39m1laxn5nbr6zr7asnjp")))

