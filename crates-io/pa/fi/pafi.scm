(define-module (crates-io pa fi pafi) #:use-module (crates-io))

(define-public crate-pafi-0.1.0 (c (n "pafi") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "05350r08l65ynyrghc123cycvarjv8nlvqgdpcdrjdzq441sfqpb")))

(define-public crate-pafi-0.1.1 (c (n "pafi") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "06xyydsnwi1rim4d74v6g94pxik733wnji302d7z74k3bpd9d6af")))

