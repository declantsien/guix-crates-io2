(define-module (crates-io pa pr paprika) #:use-module (crates-io))

(define-public crate-paprika-0.0.1 (c (n "paprika") (v "0.0.1") (h "0karr8nk5f4p2zh32zai4r0izh5is6bwhjah4igszzqiwss290n4")))

(define-public crate-paprika-0.0.2 (c (n "paprika") (v "0.0.2") (h "0jn7xad3k14ffdaaf7d2xjzfklxbjj87gapjp9126qblhbgn0wnx")))

(define-public crate-paprika-0.0.3 (c (n "paprika") (v "0.0.3") (h "1knfmhfd67z99y34ziijis4a70q4ll8xynlr2gl64b8bpbhmpjr3")))

(define-public crate-paprika-0.0.4 (c (n "paprika") (v "0.0.4") (h "1kxz9qak3pbzc1ks2w52hrklhn1zil06ckhb2xphl7izpc2p8plj")))

