(define-module (crates-io pa ko pako-tools) #:use-module (crates-io))

(define-public crate-pako-tools-0.1.0 (c (n "pako-tools") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.14.0") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1qcdal8sik9s16nrfk83ac6d86z6viakb8r3gy0rc2qvxarhg9sz")))

(define-public crate-pako-tools-0.1.1 (c (n "pako-tools") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.14.0") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0am3rd0y4w0y4r95jj5w6cdb2byddlv0bddjwjq7yrn34nr1jhq7")))

(define-public crate-pako-tools-0.1.2 (c (n "pako-tools") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.14.0") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1rvq9jfqrhn1rnfrf7jbmwqzlmlcifsxl4w9iv6lqszyjq02iwr9")))

