(define-module (crates-io pa rq parquetry) #:use-module (crates-io))

(define-public crate-parquetry-0.1.0 (c (n "parquetry") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^46") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0msq6ydzy41bnq9gn35qs9z2sj1ivw3mmzsgwd6kv34wrvndd7fc")))

(define-public crate-parquetry-0.2.0 (c (n "parquetry") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^46") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x19am8bz7d732rbf3k656i31k841d32sg7f0y1rkb9k39qyc5ky")))

(define-public crate-parquetry-0.3.0 (c (n "parquetry") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^46") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wzvswp6mg6lrlzxxn7g9yxflm342fzqmgy8cgd7sz3i1didibmc")))

(define-public crate-parquetry-0.3.1 (c (n "parquetry") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^46") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "101fha8qqdnvd84wygavbyv1g0jmgwgaih2c77prdb7brik00wh7")))

(define-public crate-parquetry-0.4.0 (c (n "parquetry") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^47") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18xfr2papp4y7ig8cfrrzdxx4fc7z387yicnrh6jnz3x4rk0fl8v")))

(define-public crate-parquetry-0.5.0 (c (n "parquetry") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^48") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1birsvy8p3r794waxjb92ildnz84jjpmi3dgaaav5xs3xlrm49pz")))

(define-public crate-parquetry-0.6.0 (c (n "parquetry") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^49") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dpp591zf5j5ib841z2va2dyxklpi9ml279db2ckvnymvl4ykkfg")))

(define-public crate-parquetry-0.7.0 (c (n "parquetry") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^50") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jdy1hwbc306l4i8glp1pr53l1v8ahm5rswgq1qybsx3lrdp5k1b")))

(define-public crate-parquetry-0.8.0 (c (n "parquetry") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^51") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qwm4p0qdhgykzqdjfkbx5xi50aamnvaxg7lk1lb26640d21bg98")))

(define-public crate-parquetry-0.9.0 (c (n "parquetry") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^51") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mgds11lb9ylzj8zjls9dzxjy5wixj87n1c0f8hxicl7dkfv06ln")))

