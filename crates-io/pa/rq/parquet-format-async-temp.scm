(define-module (crates-io pa rq parquet-format-async-temp) #:use-module (crates-io))

(define-public crate-parquet-format-async-temp-0.1.0 (c (n "parquet-format-async-temp") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (f (quote ("futures_async"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "19y7qxknmxjj1gmksxggkvyvnh1v376bcwlmgwhh2lbahpy5m0rf")))

(define-public crate-parquet-format-async-temp-0.1.1 (c (n "parquet-format-async-temp") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (f (quote ("futures_async"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1n5b9y6fp97hzzn6pd59fa4lq1wna0hcssvc2qqnqb0v8rs3xrnd")))

(define-public crate-parquet-format-async-temp-0.2.0 (c (n "parquet-format-async-temp") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (f (quote ("futures_async"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0i7mn4x64d8g6d046y9kpb55dz8v0z67cz7lhgncxs9zr3ww5aq3")))

(define-public crate-parquet-format-async-temp-0.3.0 (c (n "parquet-format-async-temp") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (f (quote ("futures_async"))) (d #t) (k 0)))) (h "1grhb4gh81rqjf5x7cbfs9gxmrfci37c1g74mngh27aj8dgqp328")))

(define-public crate-parquet-format-async-temp-0.3.1 (c (n "parquet-format-async-temp") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (f (quote ("futures_async"))) (d #t) (k 0)))) (h "16fvqr4vfsld44ac7r306lg8xsn15k9b51h2afv5wnry9k4759p1")))

