(define-module (crates-io pa rq parquet_derive) #:use-module (crates-io))

(define-public crate-parquet_derive-3.0.0 (c (n "parquet_derive") (v "3.0.0") (d (list (d (n "parquet") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1njxfp4mrj74wzdcflqrb60p9a9bvb04i62m7c178jlr5i1pbifc") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-4.0.0 (c (n "parquet_derive") (v "4.0.0") (d (list (d (n "parquet") (r "^4.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s09z5cz06q7bary1wawfs9k11yrd52m4wgxdrmxsgx2x1jc01qq") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-4.1.0 (c (n "parquet_derive") (v "4.1.0") (d (list (d (n "parquet") (r "^4.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xz373pcipxgcbx9inkm2ml6jg0gvx2y9z2z2kalmkb4qi6m9qy0") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-4.2.0 (c (n "parquet_derive") (v "4.2.0") (d (list (d (n "parquet") (r "^4.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vjl5yxd8n0b0cd89zx4h612arrmsxw8044wsqq08a554w5i1kkq") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-4.3.0 (c (n "parquet_derive") (v "4.3.0") (d (list (d (n "parquet") (r "^4.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0shrvc67z9nmm7pm23w704fs1bic60hp9amzrhbbm7j3pjjs6pcs") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-4.4.0 (c (n "parquet_derive") (v "4.4.0") (d (list (d (n "parquet") (r "^4.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05rgd9rsnhd53hn5mgwp1a865jz5sbgmdvl4hvy1jywbw3hbmyki") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.0.0 (c (n "parquet_derive") (v "5.0.0") (d (list (d (n "parquet") (r "^5.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mmc48nml8z6g2j5hk29vsiyvf8gcckyxap5rr6rhr5gclw85fzl") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.1.0 (c (n "parquet_derive") (v "5.1.0") (d (list (d (n "parquet") (r "^5.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cqh1xh191rp4x0zpmapfqmk7xmka9v3hany70rg56ssijrjvv3q") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.2.0 (c (n "parquet_derive") (v "5.2.0") (d (list (d (n "parquet") (r "^5.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wfrcbdvyaknkmjbvgbqs2bxy6g2yymh9w8wmvld69i3gmadyqss") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.3.0 (c (n "parquet_derive") (v "5.3.0") (d (list (d (n "parquet") (r "^5.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ygp4nvc36r9vx0sykf97bgn2ik5vad3in4l3rms6s714k1a4ivj") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.4.0 (c (n "parquet_derive") (v "5.4.0") (d (list (d (n "parquet") (r "^5.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r7a1b3aqmvk55prj8wdjyzz7sp90h4k6ha52zghpkhnl9cf98kw") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-5.5.0 (c (n "parquet_derive") (v "5.5.0") (d (list (d (n "parquet") (r "^5.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17kai9kms47z548gc99aar0kylp6lj8d3bw1acykprrbz7qwql5m") (f (quote (("uuid") ("chrono") ("bigdecimal"))))))

(define-public crate-parquet_derive-6.1.0 (c (n "parquet_derive") (v "6.1.0") (d (list (d (n "parquet") (r "^6.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15g5zv9j0n3jjmyywnxgl43ryb601kkwjy7990qs6jw62innmxj9")))

(define-public crate-parquet_derive-6.2.0 (c (n "parquet_derive") (v "6.2.0") (d (list (d (n "parquet") (r "^6.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16krykbbbskvpws0r1q67lvsaja26qdc9gz0bbs7nnqrs0rdc1lz")))

(define-public crate-parquet_derive-6.3.0 (c (n "parquet_derive") (v "6.3.0") (d (list (d (n "parquet") (r "^6.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15w7vmnvhyjg945r6yp4j72khv35536a8cxlhmdwr71rhldklgzh")))

(define-public crate-parquet_derive-6.4.0 (c (n "parquet_derive") (v "6.4.0") (d (list (d (n "parquet") (r "^6.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dqrdl1a3cccmlsk3aks43n79825zbrs9j2nflxrxbf6bc4plnb7")))

(define-public crate-parquet_derive-6.5.0 (c (n "parquet_derive") (v "6.5.0") (d (list (d (n "parquet") (r "^6.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vhd29ja0n6m7c5qsm1vayp5x0px7b9p482rhqlyfwnjgr81632y")))

(define-public crate-parquet_derive-7.0.0 (c (n "parquet_derive") (v "7.0.0") (d (list (d (n "parquet") (r "^7.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ga2fds5m8x48xmz9a2nxp4pfwfxj9nq0kzc4jqdx0yyirjjg9s0") (r "1.57")))

(define-public crate-parquet_derive-8.0.0 (c (n "parquet_derive") (v "8.0.0") (d (list (d (n "parquet") (r "^8.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c986y80b150pnv9pfbi819agfvlc7dn3qn8lfxg1g3bk5m0rfzw") (r "1.57")))

(define-public crate-parquet_derive-9.0.2 (c (n "parquet_derive") (v "9.0.2") (d (list (d (n "parquet") (r "^9.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r0djcl04a4jqk2fc0xl98jyicshlwkp6a4xaixbcxzrq7677yjk") (r "1.57")))

(define-public crate-parquet_derive-9.1.0 (c (n "parquet_derive") (v "9.1.0") (d (list (d (n "parquet") (r "^9.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fiix6nad8jygvinbkbqnqid1q4cp8c335mwny9rjsmdsnrnas17") (r "1.57")))

(define-public crate-parquet_derive-10.0.0 (c (n "parquet_derive") (v "10.0.0") (d (list (d (n "parquet") (r "^10.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zar7zh96dp8rrlmx1pwsx1v55jdrjd1c9zlmnhwidqz2i5w0552") (r "1.57")))

(define-public crate-parquet_derive-11.0.0 (c (n "parquet_derive") (v "11.0.0") (d (list (d (n "parquet") (r "^11.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k14a9cmcr152hc12gvxbp09acx2ip9hm4dvpxn1d7dwgfmz62hz") (r "1.57")))

(define-public crate-parquet_derive-11.1.0 (c (n "parquet_derive") (v "11.1.0") (d (list (d (n "parquet") (r "^11.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qn9rhs6fzibgp5153kdmnk76pamflyr1x19x4a6803bff30179b") (r "1.57")))

(define-public crate-parquet_derive-12.0.0 (c (n "parquet_derive") (v "12.0.0") (d (list (d (n "parquet") (r "^12.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fhdrcma7m3g5pi6853j4d50l216wnd1n5arnzbv99dkwzw7agnw") (r "1.57")))

(define-public crate-parquet_derive-13.0.0 (c (n "parquet_derive") (v "13.0.0") (d (list (d (n "parquet") (r "^13.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q6jamvlzha4jqvrlq7xx6i3a820b6a4v0vjm5k2q6y4dd2bbfpz") (r "1.57")))

(define-public crate-parquet_derive-14.0.0 (c (n "parquet_derive") (v "14.0.0") (d (list (d (n "parquet") (r "^14.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z51nk9gvrlrw8jbixcvsyw8q48fbav01gnajvarjkv71qibafbs") (r "1.57")))

(define-public crate-parquet_derive-15.0.0 (c (n "parquet_derive") (v "15.0.0") (d (list (d (n "parquet") (r "^15.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13c3mjxj3ayhwx2bh0a864laih5hys3b7lfl9y42wrn2fipzb9bg") (r "1.57")))

(define-public crate-parquet_derive-16.0.0 (c (n "parquet_derive") (v "16.0.0") (d (list (d (n "parquet") (r "^16.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06xihnp3hf8jyb9nyjm368zcafhj6r9bn4jav5bami9f71cd3kkn") (r "1.57")))

(define-public crate-parquet_derive-17.0.0 (c (n "parquet_derive") (v "17.0.0") (d (list (d (n "parquet") (r "^17.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "09lw3v8rw094wlkfb598k4kfi3lxl6ps948kcscir832dbjirzra") (r "1.57")))

(define-public crate-parquet_derive-18.0.0 (c (n "parquet_derive") (v "18.0.0") (d (list (d (n "parquet") (r "^18.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "19r7hmsifyzspkas1l5f4y79kd98cwx6m0dgcirkxwqmxdl3v325") (r "1.57")))

(define-public crate-parquet_derive-19.0.0 (c (n "parquet_derive") (v "19.0.0") (d (list (d (n "parquet") (r "^19.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "0fz1brl6scgyv0k9ghgnmmmfrqa5jhd8j58mkvhawwm47yjsmhzb") (r "1.57")))

(define-public crate-parquet_derive-20.0.0 (c (n "parquet_derive") (v "20.0.0") (d (list (d (n "parquet") (r "^20.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "0zafsq3kii0hgg41ldcihnvv5bxh2j0zbr8m8i0znrqg2xgzbsv6") (r "1.62")))

(define-public crate-parquet_derive-21.0.0 (c (n "parquet_derive") (v "21.0.0") (d (list (d (n "parquet") (r "^21.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1cwlcq9yx84k03hkxy0r5yfznji5ji4kj997ic95v0zy8yciczmi") (r "1.62")))

(define-public crate-parquet_derive-22.0.0 (c (n "parquet_derive") (v "22.0.0") (d (list (d (n "parquet") (r "^22.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1gxzc36l0n0fg60n1a709x6fm7kssgjvvsffdm8q0zlbhqvpc821") (r "1.62")))

(define-public crate-parquet_derive-23.0.0 (c (n "parquet_derive") (v "23.0.0") (d (list (d (n "parquet") (r "^23.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1llgnqyr0n7d3797sigdh52js41qmhvmacsgidwhsb9h84jwzdkm") (r "1.62")))

(define-public crate-parquet_derive-24.0.0 (c (n "parquet_derive") (v "24.0.0") (d (list (d (n "parquet") (r "^24.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1nb7yhpmfpsahxpsmxc5dnqs4p04gzwrzy4ph0fy205djvf9ppg7") (r "1.62")))

(define-public crate-parquet_derive-25.0.0 (c (n "parquet_derive") (v "25.0.0") (d (list (d (n "parquet") (r "^25.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "156imp2s823m23x3xy066rgq0668m7m731vji43sd6ydf2gifif6") (r "1.62")))

(define-public crate-parquet_derive-26.0.0 (c (n "parquet_derive") (v "26.0.0") (d (list (d (n "parquet") (r "^26.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1mwjnqpw6z2h0zq2kz2gs94gq461jlwys8n20mxv03zrqqffm20q") (r "1.62")))

(define-public crate-parquet_derive-27.0.0 (c (n "parquet_derive") (v "27.0.0") (d (list (d (n "parquet") (r "^27.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06q44yv1s855zsdhmflyclhnxp32qwhapc4i414m2zsaszas1s71") (r "1.62")))

(define-public crate-parquet_derive-28.0.0 (c (n "parquet_derive") (v "28.0.0") (d (list (d (n "parquet") (r "^28.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15wwg3g8hkv9d33sry987azl7pnqpz9vsxqlcyl057i1dzkpnb6l") (r "1.62")))

(define-public crate-parquet_derive-29.0.0 (c (n "parquet_derive") (v "29.0.0") (d (list (d (n "parquet") (r "^29.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "168525d2p335g2fcydwksi0i9jpkl4xdv9sanwc2rg1n3801zrjg") (r "1.62")))

(define-public crate-parquet_derive-30.0.0 (c (n "parquet_derive") (v "30.0.0") (d (list (d (n "parquet") (r "^30.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12wn1b6ncfpz306ydwl33shg54j9fc5r0hh6bdk22x4jzv2mbwdw") (r "1.62")))

(define-public crate-parquet_derive-30.0.1 (c (n "parquet_derive") (v "30.0.1") (d (list (d (n "parquet") (r "^30.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j9m6j3i2abd0zwmpvsz95bd07fkrs4ymv1z7sdjwc13vcl1v4wj") (r "1.62")))

(define-public crate-parquet_derive-31.0.0 (c (n "parquet_derive") (v "31.0.0") (d (list (d (n "parquet") (r "^31.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0imh75af7vjsqfh3j0wyh0mzpg4s4cy1zqwa7krbz27iagzmfi3g") (r "1.62")))

(define-public crate-parquet_derive-32.0.0 (c (n "parquet_derive") (v "32.0.0") (d (list (d (n "parquet") (r "^32.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ma1a9gvclsqj20b2w4fndn1y3y9cqpnc0rdilmy34pz4mvygx5n") (r "1.62")))

(define-public crate-parquet_derive-33.0.0 (c (n "parquet_derive") (v "33.0.0") (d (list (d (n "parquet") (r "^33.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wj9wq6x538xmprkbd4ddyc9lj1lbk75pljcki5vxmzn6s8rzvsp") (r "1.62")))

(define-public crate-parquet_derive-34.0.0 (c (n "parquet_derive") (v "34.0.0") (d (list (d (n "parquet") (r "^34.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10zhiqjzdjai6d3nvd9pflm7r2ij3fshs29jbgh8c02mp004sl3f") (r "1.62")))

(define-public crate-parquet_derive-35.0.0 (c (n "parquet_derive") (v "35.0.0") (d (list (d (n "parquet") (r "^35.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07jqh7dfymncg346dmg2k36gmq7v8dqh2fs1hznh1jj17b4vr461") (r "1.62")))

(define-public crate-parquet_derive-36.0.0 (c (n "parquet_derive") (v "36.0.0") (d (list (d (n "parquet") (r "^36.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14847z1hpnkw14g2lj31vavdi1hzd0wh0qqpdkifhgysgf3778kl") (r "1.62")))

(define-public crate-parquet_derive-37.0.0 (c (n "parquet_derive") (v "37.0.0") (d (list (d (n "parquet") (r "^37.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1y1bv4hgr4lws0klll5cy9cma0fcaw8f2c7kvfysz4cxjdhli95z") (r "1.62")))

(define-public crate-parquet_derive-38.0.0 (c (n "parquet_derive") (v "38.0.0") (d (list (d (n "parquet") (r "^38.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dhxqr5pb2amz4gr8yiq5x21hbkvfw36gwc206h9lhk965g7mwbp") (r "1.62")))

(define-public crate-parquet_derive-39.0.0 (c (n "parquet_derive") (v "39.0.0") (d (list (d (n "parquet") (r "^39.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "028y71xypcrg26hqf0nhz5dnv8g5fn8zdk54zl8l9s4497gnsrqj") (r "1.62")))

(define-public crate-parquet_derive-40.0.0 (c (n "parquet_derive") (v "40.0.0") (d (list (d (n "parquet") (r "^40.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qafrrbxi0d55vwziapv3w81rbwj1a5zmbrqj60j8v7n87sbdfba") (r "1.62")))

(define-public crate-parquet_derive-41.0.0 (c (n "parquet_derive") (v "41.0.0") (d (list (d (n "parquet") (r "^41.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zxj3ybvhrazl20cxcm73iinvr5s5g2vd1icflm3clsf4vzwjys6") (r "1.62")))

(define-public crate-parquet_derive-42.0.0 (c (n "parquet_derive") (v "42.0.0") (d (list (d (n "parquet") (r "^42.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lb4k4qccsffb1198pvgn3i0fl4apm673kydnc5g1r0nv9cxrl9j") (r "1.62")))

(define-public crate-parquet_derive-43.0.0 (c (n "parquet_derive") (v "43.0.0") (d (list (d (n "parquet") (r "^43.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06n1abgf61lf22gmbqxfddjp5h7wbf05vff91c5zca677yq339ib") (r "1.62")))

(define-public crate-parquet_derive-44.0.0 (c (n "parquet_derive") (v "44.0.0") (d (list (d (n "parquet") (r "^44.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ycb3gl4533c8dsvskm1sxi56ws9yn720ypw7cky1acvg842qx0a") (r "1.62")))

(define-public crate-parquet_derive-45.0.0 (c (n "parquet_derive") (v "45.0.0") (d (list (d (n "parquet") (r "^45.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x8agrw1g3nzpxadcy3g3m0ancjwvnd35pss6y7z9m65qx7bs86i") (r "1.62")))

(define-public crate-parquet_derive-46.0.0 (c (n "parquet_derive") (v "46.0.0") (d (list (d (n "parquet") (r "^46.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p9fz27qs9f3p1wfsyclradlx58bzysf7p7qh1mbgc4hcrdlqhzr") (r "1.62")))

(define-public crate-parquet_derive-47.0.0 (c (n "parquet_derive") (v "47.0.0") (d (list (d (n "parquet") (r "^47.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vp564vfz4y5srirmq0y54pcyz9zqqmjlwjsk5jx2y610glmqn77") (r "1.62")))

(define-public crate-parquet_derive-48.0.0 (c (n "parquet_derive") (v "48.0.0") (d (list (d (n "parquet") (r "^48.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vfgydk9jz2ybk9v9zqh6k4qabvbjvd7hi7ws7b0vnfhj66zfy0k") (r "1.62")))

(define-public crate-parquet_derive-48.0.1 (c (n "parquet_derive") (v "48.0.1") (d (list (d (n "parquet") (r "^48.0.1") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p4vsm72spa4b25zglja1bv20c3w0gpdrj9wy8yjck16rhvvi5fb") (r "1.62")))

(define-public crate-parquet_derive-49.0.0 (c (n "parquet_derive") (v "49.0.0") (d (list (d (n "parquet") (r "^49.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vq65fzqrnb7m0ivq9zympww3if8pkvm5jknca4f5sr72820ajmj") (r "1.62")))

(define-public crate-parquet_derive-50.0.0 (c (n "parquet_derive") (v "50.0.0") (d (list (d (n "parquet") (r "^50.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0alrpf4326iaq61bcqz2wzl2w7abrnr3w9i3mim28gajq2h78vr8") (r "1.62")))

(define-public crate-parquet_derive-51.0.0 (c (n "parquet_derive") (v "51.0.0") (d (list (d (n "parquet") (r "^51.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vbbfd3s4kdyxdnl86ihs8zc0wn6m5kh6zk0gkfcj4x2zjsxp2l8") (r "1.62")))

