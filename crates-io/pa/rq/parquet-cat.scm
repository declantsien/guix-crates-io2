(define-module (crates-io pa rq parquet-cat) #:use-module (crates-io))

(define-public crate-parquet-cat-0.1.0 (c (n "parquet-cat") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.48.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.48.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datafusion") (r "^11.0.0") (d #t) (k 0)) (d (n "object_store") (r "^0.4.0") (f (quote ("aws"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ql88aj0y6qyma1ks9s3s43zlgvm2pn9dj5r0hz05kq8wm9sf2b3") (y #t)))

(define-public crate-parquet-cat-0.1.1 (c (n "parquet-cat") (v "0.1.1") (d (list (d (n "aws-config") (r "^0.48.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.48.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datafusion") (r "^11.0.0") (d #t) (k 0)) (d (n "object_store") (r "^0.4.0") (f (quote ("aws"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1iia8hhgfhr7h2n6ms37arz5q1l0yjy0liyd2qx3nww0kij3zk5a")))

