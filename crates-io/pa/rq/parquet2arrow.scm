(define-module (crates-io pa rq parquet2arrow) #:use-module (crates-io))

(define-public crate-parquet2arrow-0.1.0 (c (n "parquet2arrow") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "arrow") (r "^9.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parquet") (r "^9.1.0") (d #t) (k 0)))) (h "0rdr6acmdcdw3rdd9ckn53aw9ph0kszxlpk5aqvhd72fdgfax84r")))

