(define-module (crates-io pa rq parquet-format) #:use-module (crates-io))

(define-public crate-parquet-format-2.4.0 (c (n "parquet-format") (v "2.4.0") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0yjvask3fiynbpfpdgqjbbsfs4xiawngaygk44z9v1p0h6cf8nm6")))

(define-public crate-parquet-format-2.5.0 (c (n "parquet-format") (v "2.5.0") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "113qnyfqr3vkilw3r6ni73zjpgmm5qy3cmy72i11878hkaa3a294")))

(define-public crate-parquet-format-2.6.0 (c (n "parquet-format") (v "2.6.0") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.12") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "1mrbdj60d8rq9y0mxz2ibkvazg64s9wdx7rwnc51390v0dwchlyy")))

(define-public crate-parquet-format-2.6.1 (c (n "parquet-format") (v "2.6.1") (d (list (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "0wavvaf5dkfas1ms5kz093hq4mayldc0grfcyv4fsp9vahinpg55")))

(define-public crate-parquet-format-2.7.0 (c (n "parquet-format") (v "2.7.0") (d (list (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "1av2d94f3a1c57bqqv7rqfxcgbakqv20nayn5g4r6w7vaqnn6qwf") (y #t)))

(define-public crate-parquet-format-3.0.0 (c (n "parquet-format") (v "3.0.0") (d (list (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "1ybvy17plq3w7icl6c6agqspd1vdg0mkgldgp93sxm2nrwzswirz")))

(define-public crate-parquet-format-4.0.0 (c (n "parquet-format") (v "4.0.0") (d (list (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "1df0kfymvsgsqwrhb6c1mmamjvvl46l4172z91y9cq2lrp6hc30z")))

