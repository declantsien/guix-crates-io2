(define-module (crates-io pa ds pads) #:use-module (crates-io))

(define-public crate-pads-0.2.2 (c (n "pads") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "pulsectl-rs") (r "^0.3.2") (d #t) (k 0)))) (h "1zxh8j2n3qjmm4ffixn6whd2nzxz3x6f5s6hjsqzvc2q5iidba0q")))

(define-public crate-pads-0.2.3 (c (n "pads") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "pulsectl-rs") (r "^0.3.2") (d #t) (k 0)))) (h "1v9xayh1i09yw44imgz8pw0szy5cx888bcxr3y93m6s24dgiw3g2")))

(define-public crate-pads-0.2.5 (c (n "pads") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.0.21") (d #t) (k 0)) (d (n "pulsectl-rs") (r "^0.3.2") (d #t) (k 0)))) (h "1dbabhv548dy092fh244qnq4s9d174zh3xh9a57x0xs1929bm4cz")))

(define-public crate-pads-0.2.6 (c (n "pads") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pulsectl-rs") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02v8gl3r4lcvl655by2qyqyiwfh7khqmwdc0pk9d82gg2l9r0pwg")))

(define-public crate-pads-0.2.7 (c (n "pads") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libpulse-sys") (r "^1.21.0") (d #t) (k 0)) (d (n "pulsectl-rs") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1in8rbmz9s49lc4l47adrr8h8aqiwvqzn3dmydlc5jgr9j8zgapw")))

