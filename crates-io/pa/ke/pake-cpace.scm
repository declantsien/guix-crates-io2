(define-module (crates-io pa ke pake-cpace) #:use-module (crates-io))

(define-public crate-pake-cpace-0.1.0 (c (n "pake-cpace") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "00s4vcdrqqvkx9iwkmlgdbny4nizah7a0m84p2dsxavcfm18mwjz")))

(define-public crate-pake-cpace-0.1.1 (c (n "pake-cpace") (v "0.1.1") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "1hlxddxfm1vpm8hwdnh1s0bwrd1nn7zywcapvgfd0bap1f0c6hlf")))

(define-public crate-pake-cpace-0.1.2 (c (n "pake-cpace") (v "0.1.2") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "0vgqfcbsqw621ykhy1p62zsmrwa76jw5a1j5zzr9zhf54n26mvgs")))

(define-public crate-pake-cpace-0.1.3 (c (n "pake-cpace") (v "0.1.3") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "0h4hisi6l0lvzn0w64sl3ka5kilrwgv3jgpi47fvg4vqawnza7ch")))

(define-public crate-pake-cpace-0.1.4 (c (n "pake-cpace") (v "0.1.4") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "1l4lir9dfm6375k87yl4v7hmlax5dk8bnxs94sikfs3y8sl4xw3v")))

(define-public crate-pake-cpace-0.1.5 (c (n "pake-cpace") (v "0.1.5") (d (list (d (n "curve25519-dalek") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "1y5g1r78h21v6pxf7capbmgap2mfxl22hwx9m365x8y995awk0yz")))

(define-public crate-pake-cpace-0.1.6 (c (n "pake-cpace") (v "0.1.6") (d (list (d (n "curve25519-dalek") (r "^3.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (k 0)) (d (n "hmac-sha512") (r "^0.1") (d #t) (k 0)))) (h "1maxgim1w2xhvmv3dm7wy73h76zcy1rpq5qk3n2apv7r6q8a5z7d")))

(define-public crate-pake-cpace-0.1.7 (c (n "pake-cpace") (v "0.1.7") (d (list (d (n "curve25519-dalek") (r "^4.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)) (d (n "getrandom") (r "^0.2") (t "cfg(not(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\")))") (k 0)) (d (n "hmac-sha512") (r "^1.1") (d #t) (k 0)))) (h "0nja4dbxxk9pimgplm7yd72bmxblbll3ikg1iak6wacyszyxr3dx")))

