(define-module (crates-io pa ct pact-crypto) #:use-module (crates-io))

(define-public crate-pact-crypto-0.1.0 (c (n "pact-crypto") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "pact") (r "^0.1.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "pact-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (k 0)))) (h "1j1vjng0fx4mcn1sjdmvddjhyh633il7c5gwn4zzxkwc0982vhxk")))

(define-public crate-pact-crypto-0.1.1 (c (n "pact-crypto") (v "0.1.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "pact") (r "^0.1.1") (f (quote ("toml"))) (d #t) (k 0)) (d (n "pact-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (k 0)))) (h "0d8dicbc63nzsdzr665n105fwry12x51c5dfm1cgakmbp1af80w2")))

(define-public crate-pact-crypto-0.2.0 (c (n "pact-crypto") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "pact") (r "^0.2.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "pact-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (k 0)))) (h "14pn4qwymshlrbaqdq4zg240whmsm1r7gkll86fj2ki0dc2ifa39")))

