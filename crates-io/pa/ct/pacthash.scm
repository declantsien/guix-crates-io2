(define-module (crates-io pa ct pacthash) #:use-module (crates-io))

(define-public crate-pacthash-0.2.0 (c (n "pacthash") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.5") (d #t) (k 0)))) (h "1z7rdys1w85gb9as1sdkgxib5jj7izj7rl422vx0b8vp2qnpdmj1")))

