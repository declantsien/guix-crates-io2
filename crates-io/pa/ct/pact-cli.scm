(define-module (crates-io pa ct pact-cli) #:use-module (crates-io))

(define-public crate-pact-cli-0.1.0 (c (n "pact-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "07i083kckgzk0wc83i8ni4lyplisf8svylbd13kn464c2hsg2wl2")))

