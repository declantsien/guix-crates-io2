(define-module (crates-io pa ct pact) #:use-module (crates-io))

(define-public crate-pact-0.0.0 (c (n "pact") (v "0.0.0") (h "0cajsgf5b6dpxnknwq77ff4nnhxmmpkxycqfz1ffmaj49aivbafb")))

(define-public crate-pact-0.1.0 (c (n "pact") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "bytes") (r "^1.3.0") (k 2)) (d (n "snafu") (r "^0.7.1") (k 0)) (d (n "toml_edit") (r "^0.14.4") (o #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (k 2)))) (h "113paxrax21cf9q1f969kvf20hda5x9vwii43pdmn03n23wfapm2") (f (quote (("default")))) (s 2) (e (quote (("toml" "dep:toml_edit"))))))

(define-public crate-pact-0.1.1 (c (n "pact") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "bytes") (r "^1.3.0") (k 2)) (d (n "snafu") (r "^0.7.1") (k 0)) (d (n "toml_edit") (r "^0.14.4") (o #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (k 2)))) (h "0plvl1wfbr5zbj4kmjgbj6is68a8wbx5svn4xz2865bbd9ydzrhg") (f (quote (("default")))) (s 2) (e (quote (("toml" "dep:toml_edit"))))))

(define-public crate-pact-0.2.0 (c (n "pact") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "bytes") (r "^1.3.0") (k 2)) (d (n "snafu") (r "^0.7.1") (k 0)) (d (n "toml_edit") (r "^0.14.4") (o #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (k 2)))) (h "0p84wk8lilf95kd8wklca7ynjlj2cjs0b0ka9mhlymzfg04q9yfy") (f (quote (("std") ("default")))) (s 2) (e (quote (("toml" "dep:toml_edit"))))))

