(define-module (crates-io pa ct pact-derive) #:use-module (crates-io))

(define-public crate-pact-derive-0.1.0 (c (n "pact-derive") (v "0.1.0") (d (list (d (n "pact") (r "^0.1.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "17bvakh81k573dinknjlyql3gcm0biqq81njfanv9bgxm42v14s9")))

(define-public crate-pact-derive-0.1.1 (c (n "pact-derive") (v "0.1.1") (d (list (d (n "pact") (r "^0.1.1") (f (quote ("toml"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0505hf53klpf2h0x487v00ygwjq49qfbfhq5n2nr4j50pi8fs0n2")))

(define-public crate-pact-derive-0.2.0 (c (n "pact-derive") (v "0.2.0") (d (list (d (n "pact") (r "^0.2.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0d1klbsl2vqi8h5cvgps4naz6j0xiglfg00zpbzl337drxv4ams5")))

