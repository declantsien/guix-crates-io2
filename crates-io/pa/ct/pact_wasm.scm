(define-module (crates-io pa ct pact_wasm) #:use-module (crates-io))

(define-public crate-pact_wasm-0.0.0 (c (n "pact_wasm") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "console_log") (r "^0.2") (f (quote ("color"))) (d #t) (k 0)) (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pact_models") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "19mh6247lc1dl3q1ggcxsyshga9cgmdwx830g1bywddnlb9jpjk7")))

