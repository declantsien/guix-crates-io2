(define-module (crates-io pa ys paysec) #:use-module (crates-io))

(define-public crate-paysec-0.0.1 (c (n "paysec") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "soft-aes") (r "^0.1.0") (d #t) (k 0)))) (h "1liga9bzc29r22hhc5c6ncsjsjm211pzv0ypz3vankjh1xqsr4a4")))

(define-public crate-paysec-0.0.2 (c (n "paysec") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "soft-aes") (r "^0.1.0") (d #t) (k 0)))) (h "1nf9pnw6w1s6xqkhinfprcjyy02362csyg6rbl05awbi11ipq5d6")))

(define-public crate-paysec-0.1.0 (c (n "paysec") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "soft-aes") (r "^0.2.2") (d #t) (k 0)))) (h "0dw02djjfmyridcrajnvkjl3vmm9h1kfccgpwpd5775iw4a9bkwi")))

(define-public crate-paysec-0.1.1 (c (n "paysec") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "soft-aes") (r "^0.2.2") (d #t) (k 0)))) (h "0arq45x0rwh27xz9gcwxga0vvsw6hljl3lnfckbr7nish64jqdgd")))

