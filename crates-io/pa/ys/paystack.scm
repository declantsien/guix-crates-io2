(define-module (crates-io pa ys paystack) #:use-module (crates-io))

(define-public crate-paystack-0.1.0 (c (n "paystack") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xyzybynqavl4dn5ixg5bs277ykv0a0bmkgclgp7h64r8dn8jz4a")))

(define-public crate-paystack-0.1.1 (c (n "paystack") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w0r78ix6wim82kypqhndmpidx0pyag7h7x5b86lq925f73i67lm")))

