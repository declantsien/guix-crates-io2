(define-module (crates-io pa cp pacpreview) #:use-module (crates-io))

(define-public crate-pacpreview-0.1.0 (c (n "pacpreview") (v "0.1.0") (d (list (d (n "alpm") (r "^1.1") (d #t) (k 0)) (d (n "pacmanconf") (r "^1.0.0") (d #t) (k 0)))) (h "100875vcqqlxqv5qpwpn4v0594j1kvzbykbq8l6dssq3pla1755p") (f (quote (("git" "alpm/git"))))))

(define-public crate-pacpreview-0.2.1 (c (n "pacpreview") (v "0.2.1") (d (list (d (n "alpm") (r "^2.2.1") (d #t) (k 0)) (d (n "clap") (r "~3.0.12") (d #t) (k 0)) (d (n "pacmanconf") (r "^1.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "15ywb7bkgph5igmww6an002hypn83p7k4x9jz0kyap3bbmd3z596") (f (quote (("git" "alpm/git"))))))

(define-public crate-pacpreview-0.2.2 (c (n "pacpreview") (v "0.2.2") (d (list (d (n "alpm") (r "^2.2.1") (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "textwrap") (r "~0.15.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "01b4v8k4cslpjnf22bbyksjj5n6v90skf3hd8cvhqzblnwwn69qc") (f (quote (("git" "alpm/git"))))))

(define-public crate-pacpreview-0.4.0 (c (n "pacpreview") (v "0.4.0") (d (list (d (n "alpm") (r "^3.0.5") (d #t) (k 0)) (d (n "pacmanconf") (r "^2.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)) (d (n "textwrap") (r "~0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "16p7kh09l5p46j7kbzx8j0fjva9lshynqc5jzy30dyafkq4k3c0f") (f (quote (("git" "alpm/git"))))))

