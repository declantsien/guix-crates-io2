(define-module (crates-io pa r- par-array-init) #:use-module (crates-io))

(define-public crate-par-array-init-0.0.4 (c (n "par-array-init") (v "0.0.4") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "03n574y1bk2s52cfs4pn4i4nxndh0xlzfqgb1xm5rsljrd5f63m6")))

(define-public crate-par-array-init-0.0.5 (c (n "par-array-init") (v "0.0.5") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "16ph3s3jg7pi26n0lqv2hhy6ysv1f1bnl2jk48gm84swb1lbm790")))

