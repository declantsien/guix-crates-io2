(define-module (crates-io pa r- par-map) #:use-module (crates-io))

(define-public crate-par-map-0.1.0 (c (n "par-map") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)))) (h "01w1xndj2ch54z6pbk6lszlq890ph2pllvcw3r69z2hww1wmsiik")))

(define-public crate-par-map-0.1.1 (c (n "par-map") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)))) (h "1cyfjj1gvmidcz5wq2x6d4bwjprpgsxvk4qxbnka0kjbqspdsnxx")))

(define-public crate-par-map-0.1.2 (c (n "par-map") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "pub-iterator-type") (r "^0.1") (d #t) (k 0)))) (h "1bzxaqyqd84bw01kf1v0s3zgyf8lrx9b6r51sia20065c95h8j51")))

(define-public crate-par-map-0.1.3 (c (n "par-map") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "pub-iterator-type") (r "^0.1") (d #t) (k 0)))) (h "1m4v3k3ihnz7iqqrpjx5x6g5j5c04c7wv9ipav4jgxd65n8vwbja")))

(define-public crate-par-map-0.1.4 (c (n "par-map") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "pub-iterator-type") (r "^0.1") (d #t) (k 0)))) (h "15800i4016dg0mrvk3a7zv3ixbmhh9dr2w6g820vn0hpk68b419g")))

