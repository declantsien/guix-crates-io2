(define-module (crates-io pa r- par-iter-sync) #:use-module (crates-io))

(define-public crate-par-iter-sync-0.1.0 (c (n "par-iter-sync") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "0v8wgdq05fk1x2m9ym38xg88yjzzibrwdd0lka062wihjcfpi1xw")))

(define-public crate-par-iter-sync-0.1.1 (c (n "par-iter-sync") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "0fgk5mdn0cbf3mha2apqzvd8xxnviwhci52j0sm4b4nc0b20766s")))

(define-public crate-par-iter-sync-0.1.3 (c (n "par-iter-sync") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "12z9yi42xbf85gmaiy6l0dpv5c8pxkr1m6skhcx7w1mgwavzcbil")))

(define-public crate-par-iter-sync-0.1.5 (c (n "par-iter-sync") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1580shvcbi03wsn0hkn47mfy9fp014r79f3116d3h223725hm8dd")))

(define-public crate-par-iter-sync-0.1.6 (c (n "par-iter-sync") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "00489447y0xcwz32sv08n4fibwqykpic35iyh4j21xf5yf2rxgf3") (f (quote (("bench"))))))

(define-public crate-par-iter-sync-0.1.7 (c (n "par-iter-sync") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "12pbb9vji1fgxja7mkql602cznw6v80xhj10af03zrg916a8q1vv") (f (quote (("bench"))))))

(define-public crate-par-iter-sync-0.1.8 (c (n "par-iter-sync") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "00xarxrf3vj42bc5xp700qqaikr8di8gj2lps211nh5240pb215n") (f (quote (("bench"))))))

(define-public crate-par-iter-sync-0.1.9 (c (n "par-iter-sync") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1k020gngmz9aq6blkf1099s3mf6hn11f3mw50z1pjkci9dwk6mxf") (f (quote (("bench"))))))

(define-public crate-par-iter-sync-0.1.10 (c (n "par-iter-sync") (v "0.1.10") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "08yjkqfvqi6bdzyg74iiy66ljbc45sjnbc6l6q57s1bv76xxafw3") (f (quote (("bench"))))))

(define-public crate-par-iter-sync-0.1.11 (c (n "par-iter-sync") (v "0.1.11") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1417arzihlv689iwsqnn3sqbqcwabqaj4jb43whmkgwlxnm83agg") (f (quote (("bench"))))))

