(define-module (crates-io pa hs pahs-snafu) #:use-module (crates-io))

(define-public crate-pahs-snafu-0.1.0-alpha.3 (c (n "pahs-snafu") (v "0.1.0-alpha.3") (d (list (d (n "easy-ext") (r "^0.2.4") (d #t) (k 0)) (d (n "pahs") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "051rg64k11q16lgkah6jj5mvvdpc7mig78hkpjck84dkvr9l9cyi")))

(define-public crate-pahs-snafu-0.1.0-alpha.4 (c (n "pahs-snafu") (v "0.1.0-alpha.4") (d (list (d (n "pahs") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1nk6svqm84mwsh0cfrjj9c26c852k33kql1prni6lz5lgz64hbrl")))

