(define-module (crates-io pa hs pahs) #:use-module (crates-io))

(define-public crate-pahs-0.1.0-alpha.0 (c (n "pahs") (v "0.1.0-alpha.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0bjgyk16vh2lmri5nsjjg96n42y39dqhxzvrj0pjcjnlzf79j2j9") (f (quote (("with_snafu"))))))

(define-public crate-pahs-0.1.0-alpha.3 (c (n "pahs") (v "0.1.0-alpha.3") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0c1bjkz4s9yi42s2pvzc6sggxy9nzviidqfhnwp0ai3qqqpcw81a") (f (quote (("loop_assert") ("default" "loop_assert"))))))

(define-public crate-pahs-0.1.0-alpha.4 (c (n "pahs") (v "0.1.0-alpha.4") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0695bm4nmlziylnwjshmxx3dyyq3rqwflzdpijbc77b9lvx7d1ax") (f (quote (("loop_assert") ("default" "loop_assert"))))))

(define-public crate-pahs-0.1.0-alpha.5 (c (n "pahs") (v "0.1.0-alpha.5") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0k5nn2zvcsj4d2xn3rq8j07xjnwnhs7bxwjslvg3l7ppfg7jnw6n") (f (quote (("loop_assert") ("default" "loop_assert"))))))

