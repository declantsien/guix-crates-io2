(define-module (crates-io pa vl pavlok) #:use-module (crates-io))

(define-public crate-pavlok-0.1.0 (c (n "pavlok") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "00ygh1ycm9h7p113k014bm2qv7b66damlfyh646klvnnd3bnlj1x")))

