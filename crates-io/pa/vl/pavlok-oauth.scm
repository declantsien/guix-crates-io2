(define-module (crates-io pa vl pavlok-oauth) #:use-module (crates-io))

(define-public crate-pavlok-oauth-0.1.0 (c (n "pavlok-oauth") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1mv44yr3wpc960hm4w2cb0dhm12c9qm5pmq614sd4zx7njla1l1r")))

(define-public crate-pavlok-oauth-0.1.1 (c (n "pavlok-oauth") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1rkz97wv6yxm3x81lhkvr79q8picmypyp0ldx89rnm5jnakdcqdh")))

(define-public crate-pavlok-oauth-0.1.2 (c (n "pavlok-oauth") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1y21ix6mcwpkkxh5hdnpa4dj40giqf77c8qdn46bxag896an91bz")))

(define-public crate-pavlok-oauth-0.1.3 (c (n "pavlok-oauth") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "05c47dwppybid1792j6fh4afyrhhpqwmzd4av380zi33a6wh4v2h")))

(define-public crate-pavlok-oauth-0.1.4 (c (n "pavlok-oauth") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1fcb218hl2b04r4da2vzy1dl3mzaxjz5wn9g6h35dshdnhalfjzq")))

(define-public crate-pavlok-oauth-0.1.5 (c (n "pavlok-oauth") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0kr3s16nanjzr9dqw234kvf4nichsfmgrsqrqc5435avb63qycw5")))

(define-public crate-pavlok-oauth-0.1.6 (c (n "pavlok-oauth") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1la9ilypn48i6r2g4b0af8h6shnj46yh8vpcb0m9zlkr77cspw7j")))

(define-public crate-pavlok-oauth-0.1.7 (c (n "pavlok-oauth") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1l74dkd39h4lww3r58l865s286mvd2qb28z0cb4rp82k6fsg3i5d")))

