(define-module (crates-io pa vl pavlok-cli) #:use-module (crates-io))

(define-public crate-pavlok-cli-0.1.0 (c (n "pavlok-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "pavlok") (r "^0.1.0") (d #t) (k 0)))) (h "1x0ahdc7hs5988d5iqr5h3flw0mmhigb9j0l06lx2hrsbvns1dxq")))

