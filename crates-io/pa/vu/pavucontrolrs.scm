(define-module (crates-io pa vu pavucontrolrs) #:use-module (crates-io))

(define-public crate-pavucontrolrs-0.1.0 (c (n "pavucontrolrs") (v "0.1.0") (d (list (d (n "libpulse-binding") (r "^2.27.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "tui") (r "^0.6.2") (d #t) (k 0)))) (h "0d2jpfbhvhkbaviv17v0704s20zgcgvg0jskblayrr74h621476z")))

(define-public crate-pavucontrolrs-0.1.1 (c (n "pavucontrolrs") (v "0.1.1") (d (list (d (n "libpulse-binding") (r "^2.28.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("termion"))) (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "12r4dibgsv1vsas12g8jiahya1cj49vmc02k7s19jdvxqb2kv9l6")))

