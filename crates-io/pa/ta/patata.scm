(define-module (crates-io pa ta patata) #:use-module (crates-io))

(define-public crate-patata-0.1.0 (c (n "patata") (v "0.1.0") (d (list (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "114ywpn4hclnnlp2ac1qw4m6wifg5mvfsq6zlgm54sgcgrlc26g8")))

(define-public crate-patata-0.0.1 (c (n "patata") (v "0.0.1") (d (list (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1970xc6mn4f6hsw5p347lzyb9km2ac0pv08aipbbmkcgynr0y9dn")))

