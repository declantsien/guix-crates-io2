(define-module (crates-io pa gg paggo) #:use-module (crates-io))

(define-public crate-paggo-0.1.0 (c (n "paggo") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("net" "bytes" "mio" "io-std" "io-util" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0y0qf4pn3iw31scq15w0920wv289icsfcxf57assl14xzsdd5yms") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-paggo-0.1.1 (c (n "paggo") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("net" "bytes" "mio" "io-std" "io-util" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "07mnd98q5g2j84pbkwmiwpizgdqgbx0s10frcpr2qy3pryysd70v") (f (quote (("default" "cli") ("cli" "clap"))))))

