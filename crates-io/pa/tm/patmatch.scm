(define-module (crates-io pa tm patmatch) #:use-module (crates-io))

(define-public crate-patmatch-0.1.0 (c (n "patmatch") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "1gnrhpbi96gk4z2ix76fcmp2hv7p5yxbnpf0adp7322zkq6dwr6k")))

(define-public crate-patmatch-0.1.1 (c (n "patmatch") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "1wb9ia88vdv14d4i2x86k1dh6smpd8lznaq6nv2fjy3w87mxcglg")))

(define-public crate-patmatch-0.1.2 (c (n "patmatch") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "0yqknm4sfj5dxv7qcnwn3mbjiqdqgqnsygmkkzvqq5xkx0vyya95")))

(define-public crate-patmatch-0.1.3 (c (n "patmatch") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "18l9x6npkibp6m0zxvq1pldhsga9y9pyvaibcngwkl9zvxv3ahs7")))

