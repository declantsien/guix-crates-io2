(define-module (crates-io pa tm patme) #:use-module (crates-io))

(define-public crate-patme-0.1.0 (c (n "patme") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (f (quote ("yaml_conf"))) (k 0)) (d (n "lettre") (r "^0.10.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "powershell_script") (r "^1.0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0cgz70zx4pmq119y941fpiiq6d3c9hs42lkyz084m1j5cqa1dg9n")))

