(define-module (crates-io pa co paco) #:use-module (crates-io))

(define-public crate-paco-0.1.0 (c (n "paco") (v "0.1.0") (h "01fvkhaqd6npkwaz9s1insdpq73rm59adb50gaimh1w100cq8v2m")))

(define-public crate-paco-0.1.1 (c (n "paco") (v "0.1.1") (h "05myklcpwjji3qic0lkpidbmr671i7py41hjpzqdg6hpwjp77pds")))

(define-public crate-paco-0.1.2 (c (n "paco") (v "0.1.2") (h "18mjklybpzm1x90m19rpc6n9py7qsl3ihaslalx6nbh3af2d9n97")))

(define-public crate-paco-0.1.3 (c (n "paco") (v "0.1.3") (h "1ca1flvdfsw3s1a2c4bkm4gn1zv2f0m5qzagfld9w5iq5xrci8p4")))

