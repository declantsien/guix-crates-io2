(define-module (crates-io pa ym payment_strings) #:use-module (crates-io))

(define-public crate-payment_strings-0.1.0 (c (n "payment_strings") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xfl4dyaw8zh4q5idasay8zs8r3m8k36l3qippbzlxvnc9h88868")))

(define-public crate-payment_strings-0.1.1 (c (n "payment_strings") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jvyypj8k4c4d6fc1zvywpcx29mza1yb9rssnprvs6hqqda6ms8z")))

