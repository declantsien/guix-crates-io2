(define-module (crates-io pa ym payment-program) #:use-module (crates-io))

(define-public crate-payment-program-0.1.0 (c (n "payment-program") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.6.9") (d #t) (k 0)))) (h "1vmw66k6ba3hjh2k282qfwa7jbfq4b20s883lj27kx6414yjnf4r") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.1 (c (n "payment-program") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.6") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0x9r2xjvs73fd8iywf9720imy4rykhdpfz226kn0ipjjgxwbqnr3") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.2 (c (n "payment-program") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0m6bd2y3sxvkk9hq6r6x04p5gwcaqqi2apqm7psgnx688f959nnq") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.3 (c (n "payment-program") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1zwn41yfxf55jhgzp6d17cd4w2xdn20mp4sgp0n2x2vkp5l7m7as") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.4 (c (n "payment-program") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1lvyzkjdp0d007m6sbmv2773hw1l4c6i0k2r7c2sgvrg8v3c85cv") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.5 (c (n "payment-program") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0wi50jk0x7p5894385rn69hcrspy6yl4q4vkcffpk32d6v6xzpmh") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.6 (c (n "payment-program") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "18ii9dxgw9h8jw87yjgvx5fzwg6zyxcs8xanqm0ws70v4cz2g86y") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1.7 (c (n "payment-program") (v "0.1.7") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "index-program") (r "^0.1.11") (f (quote ("cpi"))) (d #t) (k 0)))) (h "104ycddwjwk5blhy5g8q63486xckrhaghnymdh0phkr9l8xbii9a") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

