(define-module (crates-io pa pi papi-sys) #:use-module (crates-io))

(define-public crate-papi-sys-0.1.0 (c (n "papi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "15p227cj94yf8dhff930q80hqrw2vxy93hgch74691kp2lasbsvq") (l "papi_wrapper")))

(define-public crate-papi-sys-0.1.1 (c (n "papi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1dv2csxl0wr8ghmjp75k7jmhlhsf1hp61p0zc55k6mhxpmz2wkl9") (l "papi_wrapper")))

