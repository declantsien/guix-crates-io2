(define-module (crates-io pa pi papito_dom) #:use-module (crates-io))

(define-public crate-papito_dom-0.1.0 (c (n "papito_dom") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "16rv3scjvk1jggsp7zv7m38a7hj0haf6agszd3wv6gzrc12hxpm3")))

(define-public crate-papito_dom-0.1.1 (c (n "papito_dom") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "1v7zkxkizn3d1zrirxis4qa7ndqss7v518d01cg55sfz6pna7x5l")))

