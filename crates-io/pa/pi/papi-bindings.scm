(define-module (crates-io pa pi papi-bindings) #:use-module (crates-io))

(define-public crate-papi-bindings-0.4.0 (c (n "papi-bindings") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "1zi1n2msyq4yv2l4i5lkhhi9gq96jf8njxgn92wx4ism6w7jsrkk")))

(define-public crate-papi-bindings-0.5.0 (c (n "papi-bindings") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "1xdazkpv8gg6q05kgwpp648vzjmyygwid9igjvzaxnvl7xglfw4w")))

(define-public crate-papi-bindings-0.5.1 (c (n "papi-bindings") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0x9rj0ll0gwyaa932iab6yy3308vfmrvpzdyw6mc09rm2996ifnz")))

(define-public crate-papi-bindings-0.5.2 (c (n "papi-bindings") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.142") (d #t) (k 0)))) (h "022vzlph5d1jgl7h594j4vxbkbfdf6p5ragiywvwzdkwjd1lkxvw")))

