(define-module (crates-io pa pi papi) #:use-module (crates-io))

(define-public crate-papi-0.1.0 (c (n "papi") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "papi-sys") (r "~0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.79") (d #t) (k 0)) (d (n "toml") (r "~0.4.7") (d #t) (k 0)))) (h "07j3z31jcysxzhwdszm7v0b4cd8sh92xwm9440vakw4hiaryywxx")))

