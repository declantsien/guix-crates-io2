(define-module (crates-io pa pi papito) #:use-module (crates-io))

(define-public crate-papito-0.1.0 (c (n "papito") (v "0.1.0") (d (list (d (n "papito_dom") (r "^0.1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "1g5n5dr2n93rns3xxh541q8jm2r7jic4c3x1w0jyph8lsp8pb144")))

(define-public crate-papito-0.1.1 (c (n "papito") (v "0.1.1") (d (list (d (n "papito_dom") (r "^0.1.1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "0b1mvpgq3i62d0nkc8ajcx9b68zp3260sa295nr2qlgcgf5h63lx")))

