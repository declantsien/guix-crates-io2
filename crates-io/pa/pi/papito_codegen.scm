(define-module (crates-io pa pi papito_codegen) #:use-module (crates-io))

(define-public crate-papito_codegen-0.1.0 (c (n "papito_codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "papito") (r "^0.1.0") (d #t) (k 2)) (d (n "papito_dom") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 2)) (d (n "syn") (r "^0.12.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0992ydr41cl8gn5rnhl5mfa95lg07aimm4gskgw0ilc7057jagnk")))

(define-public crate-papito_codegen-0.1.1 (c (n "papito_codegen") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "papito") (r "^0.1.1") (d #t) (k 2)) (d (n "papito_dom") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 2)) (d (n "syn") (r "^0.12.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0985rb3gzbif31cpigh12si0k1xdk4pgg371qz4n6k0916sigg81")))

