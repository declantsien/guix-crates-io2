(define-module (crates-io pa le palestine-memorial-rs) #:use-module (crates-io))

(define-public crate-palestine-memorial-rs-0.1.0 (c (n "palestine-memorial-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hvgpx9sbwrcgmrk68kbi6lm5rrd0k1fh2whyj4qaw81h0g1wz40")))

(define-public crate-palestine-memorial-rs-0.1.1 (c (n "palestine-memorial-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01caxghdcsm8cy3p0nc48lvyz1jcrrg6ijza604njj5d6scd9yd3")))

