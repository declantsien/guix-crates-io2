(define-module (crates-io pa le palette_derive) #:use-module (crates-io))

(define-public crate-palette_derive-0.4.0 (c (n "palette_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "15npv479k3bbhl0abbr63v8ichm9pch8bs6q8nnfg2jgqy2kqlbb") (f (quote (("strict"))))))

(define-public crate-palette_derive-0.4.1 (c (n "palette_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "17hqjbvv04kxw3xkxl52hrqgiy5bqaa6mxzkgimpn5hj7qb2rg3n") (f (quote (("strict"))))))

(define-public crate-palette_derive-0.5.0 (c (n "palette_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x5icddb877923rpl27bg4cjsf1x0d3layxmgwa3mpb01rh5yjqb") (f (quote (("strict"))))))

(define-public crate-palette_derive-0.6.0 (c (n "palette_derive") (v "0.6.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b9vsd2sdx060kwsfy73ab7n6y2facps8yrw36hxi9m87q2w76bp")))

(define-public crate-palette_derive-0.6.1 (c (n "palette_derive") (v "0.6.1") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09z4nd4sbmzqd1pqr48vrdca3v2c03dzr70cmxs7zhp7m13dzvh5")))

(define-public crate-palette_derive-0.7.0 (c (n "palette_derive") (v "0.7.0") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "1pdhyvdvps1iinwb2r92v6h113vbxakl4b7sff7iww7bm54mli2g")))

(define-public crate-palette_derive-0.7.1 (c (n "palette_derive") (v "0.7.1") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0s6jxr54xfyhcm4q2zlihk98yc2vq17bk3mbmyd5l8k1j5q1w8jk")))

(define-public crate-palette_derive-0.7.2 (c (n "palette_derive") (v "0.7.2") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "1124vhfabb3zhvbaqq227nc0sx8zf3sinlx09x1zb2msnfkby0iw")))

(define-public crate-palette_derive-0.7.3 (c (n "palette_derive") (v "0.7.3") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "02ps239sxcr5v294qf89d87sv7fdm9pr2cziwj2l6ggzql703nxp")))

(define-public crate-palette_derive-0.7.4 (c (n "palette_derive") (v "0.7.4") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0nw8bjnfq89q3rbv05n26ljrp0hq24s699xg1n1ydzh1jf91qpg0") (r "1.60.0")))

(define-public crate-palette_derive-0.7.5 (c (n "palette_derive") (v "0.7.5") (d (list (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0vcxjslri6f24zgv3n6ixhzb21a8z23fa6h42s8ss2zcvc10g2g8") (r "1.60.0")))

(define-public crate-palette_derive-0.7.6 (c (n "palette_derive") (v "0.7.6") (d (list (d (n "by_address") (r "^1.2.1") (d #t) (k 0)) (d (n "find-crate") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0c0xhpk1nqyq4jr2m8xnka7w47vqzc7m2vq9ih8wxyjv02phs0zm") (r "1.60.0")))

