(define-module (crates-io pa le palett) #:use-module (crates-io))

(define-public crate-palett-0.0.1 (c (n "palett") (v "0.0.1") (d (list (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "veho") (r ">=0.0.3") (d #t) (k 0)))) (h "06mlvvf6cvy597nx7jw98cz7igfkwphxqa8l73q9lg9m3jd5qchb")))

(define-public crate-palett-0.0.2 (c (n "palett") (v "0.0.2") (d (list (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "veho") (r ">=0.0.3") (d #t) (k 0)))) (h "036azs8z276zh8g6wv02jkcn0jwyicw2p2admwqns4a7xw2h453a")))

(define-public crate-palett-0.0.3 (c (n "palett") (v "0.0.3") (d (list (d (n "aryth") (r ">=0.0.7") (d #t) (k 0)) (d (n "num") (r ">=0.3.1") (d #t) (k 0)) (d (n "veho") (r ">=0.0.9") (d #t) (k 0)))) (h "1ibj4wj13fv8valhn8xpz0ql701m69p1jrnrka54rdbph0q33n25")))

(define-public crate-palett-0.0.4 (c (n "palett") (v "0.0.4") (d (list (d (n "aryth") (r ">=0.0.7") (d #t) (k 0)) (d (n "num") (r ">=0.3.1") (d #t) (k 0)) (d (n "veho") (r ">=0.0.9") (d #t) (k 0)))) (h "0kaaanswyrs8va2c11mmjl6fag9b99xif3mcnm6svz3apfsfwpn2")))

(define-public crate-palett-0.0.5 (c (n "palett") (v "0.0.5") (d (list (d (n "aryth") (r ">=0.0.7") (d #t) (k 0)) (d (n "num") (r ">=0.3.1") (d #t) (k 0)) (d (n "veho") (r ">=0.0.15") (d #t) (k 0)))) (h "1wdszhrh58hrmq4h2kabfpfw2jpfnq6h915lghl0iw2ws4nviwa9")))

(define-public crate-palett-0.0.6 (c (n "palett") (v "0.0.6") (d (list (d (n "aryth") (r ">=0.0.7") (d #t) (k 0)) (d (n "num") (r ">=0.3.1") (d #t) (k 0)) (d (n "veho") (r ">=0.0.15") (d #t) (k 0)))) (h "0rzfgc3kr2hn2369hgx85ac011dnihl8fn7003wdrx57s5mbrvq9")))

(define-public crate-palett-0.0.7 (c (n "palett") (v "0.0.7") (d (list (d (n "aryth") (r ">=0.0.10") (d #t) (k 0)) (d (n "num") (r ">=0.4.0") (d #t) (k 0)) (d (n "veho") (r ">=0.0.19") (d #t) (k 0)))) (h "0m7cb23rrhxdxgcyl78zdb7llcg9zb5qn9ia89d3c045glf6g7rl")))

(define-public crate-palett-0.0.8 (c (n "palett") (v "0.0.8") (d (list (d (n "aryth") (r ">=0.0.11") (d #t) (k 0)) (d (n "num") (r ">=0.4.0") (d #t) (k 0)) (d (n "veho") (r ">=0.0.20") (d #t) (k 0)))) (h "0q88qn0s5l2v0gsx3myfc0y94n242psfqwacvf5wykyw1fsypkqz")))

