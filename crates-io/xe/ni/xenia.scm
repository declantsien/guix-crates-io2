(define-module (crates-io xe ni xenia) #:use-module (crates-io))

(define-public crate-xenia-0.1.0 (c (n "xenia") (v "0.1.0") (h "06n8igk1rh97m79yknlixgawaax3b55jld8ncpmam6ln9yl1y9xf") (y #t)))

(define-public crate-xenia-0.0.1 (c (n "xenia") (v "0.0.1") (h "01vmqkhniqqmhj0lmmpv41bx5b354ix6iyx4m2b28fsghji17afm")))

(define-public crate-xenia-0.0.2 (c (n "xenia") (v "0.0.2") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.6.4") (f (quote ("errno" "general" "system" "no_std"))) (k 0)))) (h "1pksch14c968g5pf05fiwdlg52qqswbmam8aajj39bibb5s367mc")))

