(define-module (crates-io xe r_ xer_add_one) #:use-module (crates-io))

(define-public crate-xer_add_one-0.0.1 (c (n "xer_add_one") (v "0.0.1") (h "1wmpr7za0srnbw4sf3787k6d0jkldfssqim62cvxaw39603mv497")))

(define-public crate-xer_add_one-0.1.0 (c (n "xer_add_one") (v "0.1.0") (h "1r4k3pmmy9lfjlmgay3nqdgzzvlwraabv9j5w54fcw9aiad95dr4")))

