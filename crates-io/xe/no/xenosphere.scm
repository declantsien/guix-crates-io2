(define-module (crates-io xe no xenosphere) #:use-module (crates-io))

(define-public crate-xenosphere-0.0.1-alpha.1 (c (n "xenosphere") (v "0.0.1-alpha.1") (d (list (d (n "xenosphere-core") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "xenosphere-macro") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "xenosphere-parser") (r "^0.0.1-alpha.1") (d #t) (k 0)))) (h "1a22vipmmyz1dpa0kfx1qs6svrmnbsvrs8ancd2vpf4vaz2xj450")))

