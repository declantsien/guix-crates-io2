(define-module (crates-io xe no xenon) #:use-module (crates-io))

(define-public crate-xenon-0.1.0 (c (n "xenon") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0dppg88bx2bm4qqw5iwap5c0kdwm45jyggpkn7j6a93agbjc1hin")))

