(define-module (crates-io xe no xenosphere-parser) #:use-module (crates-io))

(define-public crate-xenosphere-parser-0.0.1-alpha.1 (c (n "xenosphere-parser") (v "0.0.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1d1ac11n6i431sdzqjrndij9xd5h26bg2rcffflksssg8569drsy")))

