(define-module (crates-io xe no xenosphere-macro) #:use-module (crates-io))

(define-public crate-xenosphere-macro-0.0.1-alpha.1 (c (n "xenosphere-macro") (v "0.0.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0va28mypwqd2hz0r5i9ihvkh5agm0fp8lx5xl3591cf2x05f53sy")))

