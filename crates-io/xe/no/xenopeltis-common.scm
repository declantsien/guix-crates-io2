(define-module (crates-io xe no xenopeltis-common) #:use-module (crates-io))

(define-public crate-xenopeltis-common-0.1.0 (c (n "xenopeltis-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "127rga0xvbfy8mf0kv36zljhgk9fsjwk9az73slg4myx84h1pl27")))

(define-public crate-xenopeltis-common-0.2.0 (c (n "xenopeltis-common") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hgan80r6nji01c03i1kddvgr1kh5i4bp16i3rdmmsxjw551gh26")))

(define-public crate-xenopeltis-common-0.2.1 (c (n "xenopeltis-common") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gqshasns46d81ymy206idz827509vk43fxdnk7rj34kq8q1y230")))

