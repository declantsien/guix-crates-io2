(define-module (crates-io xe nf xenforeignmemory) #:use-module (crates-io))

(define-public crate-xenforeignmemory-0.1.0 (c (n "xenforeignmemory") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenforeignmemory-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0gwh4f5ffric4w3njlgp1acnb158pqwjjnz2va4np5swkcdmczcj")))

(define-public crate-xenforeignmemory-0.2.1 (c (n "xenforeignmemory") (v "0.2.1") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xenforeignmemory-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0yxiib2y01w471z4d0kphy63kfca7csylswpnbkbc1jfmm7acpl0")))

(define-public crate-xenforeignmemory-0.2.2 (c (n "xenforeignmemory") (v "0.2.2") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xenforeignmemory-sys") (r "^0.1.0") (d #t) (k 0)))) (h "02dlvrz6y1hqxgycpf97v98xfn7jjbgyb030jd20m4i86n9lcqxs")))

(define-public crate-xenforeignmemory-0.2.3 (c (n "xenforeignmemory") (v "0.2.3") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xenforeignmemory-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0xvckqqsm4s0dah3yydj6fy0pplfd237a6azf3pmx0h0adaiacri")))

