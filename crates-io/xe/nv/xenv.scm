(define-module (crates-io xe nv xenv) #:use-module (crates-io))

(define-public crate-xenv-0.1.0 (c (n "xenv") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)))) (h "0gnhmj61gz4d0h9nynw4m3y3k0ff522ylndszy7j2vgarz16mxxd")))

(define-public crate-xenv-0.2.0 (c (n "xenv") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.0") (d #t) (k 0)))) (h "0nf9qzrwmzgwkkw84rfvxzgkr0cvnsn7ysk3syh8gplj86f3d4lc")))

