(define-module (crates-io xe n- xen-sys) #:use-module (crates-io))

(define-public crate-xen-sys-0.0.0-pre (c (n "xen-sys") (v "0.0.0-pre") (h "11wvrggl6nj8f3k5vncs3grn0qlacp3was1rw3nd6jmiqv7m88z5")))

(define-public crate-xen-sys-0.0.0-pre1 (c (n "xen-sys") (v "0.0.0-pre1") (h "0p1ikkw8hays15n64c79jr70197f8i8n00kzcgm0sn4sy531qbmj")))

(define-public crate-xen-sys-0.0.0-pre2 (c (n "xen-sys") (v "0.0.0-pre2") (d (list (d (n "cty") (r "=0.2.0") (d #t) (k 0)))) (h "1gqz2c6ib0x177rrkhz55wngg5rf0ngy18rliqmirippgv8h80pa")))

