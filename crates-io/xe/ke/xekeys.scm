(define-module (crates-io xe ke xekeys) #:use-module (crates-io))

(define-public crate-xekeys-0.9.0 (c (n "xekeys") (v "0.9.0") (h "1k7hphw93qsmm744nsc7r66bpxg9frv2irfnq7gbgsmrikdmk0wh")))

(define-public crate-xekeys-1.0.0 (c (n "xekeys") (v "1.0.0") (h "1p9ap1hq6bjk6r1q3i8145yr9a28qk2s7jc5fgmb1yx25blym5hv")))

(define-public crate-xekeys-1.0.1 (c (n "xekeys") (v "1.0.1") (h "0wi44lc7f4im0b3v99nd1hn80k8lj16vx6dkwhr4piilqzfs1pk6")))

(define-public crate-xekeys-2.0.0 (c (n "xekeys") (v "2.0.0") (h "0r139mjzdy4h94mvbs1d91z47imq3z4l6p835qhf3wdjjkdp2q0p")))

