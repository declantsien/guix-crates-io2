(define-module (crates-io xe li xelis_hash) #:use-module (crates-io))

(define-public crate-xelis_hash-0.1.0 (c (n "xelis_hash") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("k12"))) (d #t) (k 0)))) (h "1jprc0lfbyh3jzpkxvsgcikv8w45sj4yncwrwi8zdp61zvayzxb6") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-xelis_hash-0.1.1 (c (n "xelis_hash") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("k12"))) (d #t) (k 0)))) (h "15nsqvqsrryz6fw4a4hl47ryh0dj84qnn8z76xf8ym3bs0nmzynw") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-xelis_hash-0.1.2 (c (n "xelis_hash") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.3") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("k12"))) (d #t) (k 0)))) (h "018fxyzyys2cw1ilkp69q2fcaazx7h5wvh6c1z3kvp3kk969hzjq") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-xelis_hash-0.1.3 (c (n "xelis_hash") (v "0.1.3") (d (list (d (n "aes") (r "^0.8.3") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("k12"))) (d #t) (k 0)))) (h "1p3inbpw42s9nymga08wzkgqnwlyc58h2dbc9x931h2mgbq0pfif") (f (quote (("nightly") ("default" "nightly"))))))

