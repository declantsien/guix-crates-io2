(define-module (crates-io xe nc xenctrl) #:use-module (crates-io))

(define-public crate-xenctrl-0.1.0 (c (n "xenctrl") (v "0.1.0") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0f2ijh4p1aklcnwivmx07yl2pj7kamrdblm7dil9jk1syv3c9ghw")))

(define-public crate-xenctrl-0.3.2 (c (n "xenctrl") (v "0.3.2") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0hghr7gp7g8yw1wkdfbk187rwfsjl1q5sw137qh1cy8s2dnxwqyd")))

(define-public crate-xenctrl-0.4.0 (c (n "xenctrl") (v "0.4.0") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1vxbpcz5d30316nzb223rjrz4b82rfnblgrcb1qwpvbidjp0y8d7")))

(define-public crate-xenctrl-0.4.1 (c (n "xenctrl") (v "0.4.1") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1jf7l201bariydz1lf1r97css6616yrwhpbwkz7np4gbksg6a9l5")))

(define-public crate-xenctrl-0.4.2 (c (n "xenctrl") (v "0.4.2") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "10q8899an4sh3ny03bja3aggwkj22fqb0l4gabvihhxbq3101cdl")))

(define-public crate-xenctrl-0.4.3 (c (n "xenctrl") (v "0.4.3") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "088pc71yffaxdxqgmkprvnmcm6qfzr0j9h7vbjjzyvnzyxrb1vcs")))

(define-public crate-xenctrl-0.4.4 (c (n "xenctrl") (v "0.4.4") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "010zbv7iy7jiz0x3qb9kqgf0ad2a1dazzhfahd9h7p355553ysil")))

(define-public crate-xenctrl-0.4.5 (c (n "xenctrl") (v "0.4.5") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0s63fy5il3mf2kki2v7b4391a18w4wvh05nckyi9sllb2v7xc949")))

(define-public crate-xenctrl-0.4.6 (c (n "xenctrl") (v "0.4.6") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1ymvg47w73zf3v145sqp0sg5lwdmxr4b4m32xaxwil40pxr6rllr")))

(define-public crate-xenctrl-0.4.7 (c (n "xenctrl") (v "0.4.7") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0hnd4pcq1mkwgchi13ik4905p7qcfq5aln2f8ds111gkm02gjwl7")))

(define-public crate-xenctrl-0.4.8 (c (n "xenctrl") (v "0.4.8") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1fz3z1nh6y2n3jjl4s86v448jsn0jkfz9rdfqvrw58h046900vq4")))

(define-public crate-xenctrl-0.4.9 (c (n "xenctrl") (v "0.4.9") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0nwcvd08y2ldvrfj28iyax3wbgz17dya62pzgljgpk1dh4q88s6a")))

(define-public crate-xenctrl-0.5.0 (c (n "xenctrl") (v "0.5.0") (d (list (d (n "enum-primitive-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1gbmc7qyxx4dq5yq6wrzfrqcinyzag2kmaynrn6a1xfw313anm7k")))

(define-public crate-xenctrl-0.6.0 (c (n "xenctrl") (v "0.6.0") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "xenctrl-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "xenvmevent-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1a8067gy21217p2zv107cbhf140b3kbbl9wy3rqzsxm0lylrrv3f")))

