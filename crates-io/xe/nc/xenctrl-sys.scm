(define-module (crates-io xe nc xenctrl-sys) #:use-module (crates-io))

(define-public crate-xenctrl-sys-0.1.0 (c (n "xenctrl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1n2dii3bjl7c4sq2nhygymzrphr88jzln8p9gs6hb4iihcqh3iyd") (l "xenctrl")))

(define-public crate-xenctrl-sys-0.1.1 (c (n "xenctrl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "1zqf7b2qp2s0wjzkgxk8jr8jh987m0h7k6k4pf2qcwmmqx577nw7") (l "xenctrl")))

