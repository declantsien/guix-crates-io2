(define-module (crates-io xe nc xencelabs-quick-keys) #:use-module (crates-io))

(define-public crate-xencelabs-quick-keys-0.1.0 (c (n "xencelabs-quick-keys") (v "0.1.0") (h "1dsw6xwk4mzyjpl57xh9wmb0r5a7pwd38pqa9n60cd3cak4agwx4")))

(define-public crate-xencelabs-quick-keys-0.2.0 (c (n "xencelabs-quick-keys") (v "0.2.0") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)))) (h "10mh7a89v5783ykg0pyg24abhqnb9r334cs52qm80kwkzck5pkvk")))

(define-public crate-xencelabs-quick-keys-0.2.1 (c (n "xencelabs-quick-keys") (v "0.2.1") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)))) (h "06r5kj27wx1wvlnnr6l7ymir1hwddc3d1yk87f4r7w0hli9y31gq")))

(define-public crate-xencelabs-quick-keys-0.3.0 (c (n "xencelabs-quick-keys") (v "0.3.0") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0s60cjb83z423v30nb3yzswy6li00vgkxm1hl0h5xl64s73a34pm")))

(define-public crate-xencelabs-quick-keys-0.3.1 (c (n "xencelabs-quick-keys") (v "0.3.1") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1yibpyvm8pi25sh1rayc0pbs148l93024wqqn4092xpx2mhidhx6")))

(define-public crate-xencelabs-quick-keys-0.3.2 (c (n "xencelabs-quick-keys") (v "0.3.2") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "002qwq547z073r4ypmhdpz03h557p99spiqcija97lq5kgs8j1xw") (s 2) (e (quote (("serde" "dep:serde"))))))

