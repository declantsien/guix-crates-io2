(define-module (crates-io xe rr xerr) #:use-module (crates-io))

(define-public crate-xerr-0.1.0 (c (n "xerr") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "15wirv4gmbb88k8yvcrmbc7w964xsrwan6j7iv54bh2zlgky9jip")))

(define-public crate-xerr-0.1.1 (c (n "xerr") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1xs5az2ba0sjvb42igzb6gkws2gh79b4afymfbg8j1glzrjz6gxb")))

(define-public crate-xerr-0.1.2 (c (n "xerr") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "03zaqlrwp3hxv84h1dj0783k399wvm4hpm5fiyy30lwv7jkmk6wc")))

(define-public crate-xerr-0.1.3 (c (n "xerr") (v "0.1.3") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0jywa858snsfwvkk9f0skday7ly5f4d2ipglbns74whj8fqqb9zx")))

(define-public crate-xerr-0.1.4 (c (n "xerr") (v "0.1.4") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0bl2fpn1bkanscmqqym2771k2pxgzw8fm04w2ypixlvka2ambdc0")))

(define-public crate-xerr-0.1.5 (c (n "xerr") (v "0.1.5") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0wmhgsnaj7jcmrk7n0jbwzy9783wsn3hx4gyni2njrnqs8vlpfca")))

(define-public crate-xerr-0.1.6 (c (n "xerr") (v "0.1.6") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1ws5vzgcnmg2sqhh79117iw4kr28kvi95gszra4y07mmrj7phr0b")))

(define-public crate-xerr-0.1.7 (c (n "xerr") (v "0.1.7") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0p15rnyq56d4h4wzb327xc8ihffcicxdwwkb2jdskb6snv2rl1wl")))

(define-public crate-xerr-0.1.9 (c (n "xerr") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0z03lyfd5cairqbzr92f6mq6ph9ycg3sgzfjrjw7ddxw601qgrm5")))

(define-public crate-xerr-0.1.10 (c (n "xerr") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0vy0hxdz09d20zv5zqn1awdrackmvx376z5i95qb2m8mnqpqgkxy")))

(define-public crate-xerr-0.1.11 (c (n "xerr") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0sy0gpyran5yvvsmhiks9jvhhba6mcs248jq2j1dhvi18vc6k2xf")))

