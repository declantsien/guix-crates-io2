(define-module (crates-io xe #{62}# xe621) #:use-module (crates-io))

(define-public crate-xe621-0.6.0 (c (n "xe621") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "mockito") (r "^0.20.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0lxck4nrl2j7gm6krbgzkl69pcc7i5hfz0gyx921a1c5xjyk31ln")))

(define-public crate-xe621-0.6.1 (c (n "xe621") (v "0.6.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "custom_error") (r "^1") (d #t) (k 0)) (d (n "mockito") (r "^0") (d #t) (k 2)) (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^1") (d #t) (k 0)))) (h "1g1vpqh8rrswd8ik077zfdjrq1radvlm2zpgqi793l0j0dahhk97")))

(define-public crate-xe621-0.6.2 (c (n "xe621") (v "0.6.2") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "custom_error") (r "^1") (d #t) (k 0)) (d (n "mockito") (r "^0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^1") (d #t) (k 0)))) (h "0fb1crrg7xlg8v927cvh36idgwlgwirzzhklmd83yds77n93irma")))

(define-public crate-xe621-0.6.3 (c (n "xe621") (v "0.6.3") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "custom_error") (r "^1") (d #t) (k 0)) (d (n "mockito") (r "^0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^1") (d #t) (k 0)))) (h "10y5njl6j0id9fjak75j7likkrv0sywaq2v6032ckf0bf1103a3j")))

