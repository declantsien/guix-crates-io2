(define-module (crates-io xe nu xenu-background) #:use-module (crates-io))

(define-public crate-xenu-background-0.1.1 (c (n "xenu-background") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "picto") (r "^0.1.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)) (d (n "xcb-util") (r "^0.1") (f (quote ("image" "shm"))) (d #t) (k 0)))) (h "1nwa1lbiraqzr69saxrpn0kic0a201k9ac7sdf14fj6yi5fiqii2") (f (quote (("nightly" "picto/nightly") ("default"))))))

