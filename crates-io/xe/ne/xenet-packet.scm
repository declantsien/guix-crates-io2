(define-module (crates-io xe ne xenet-packet) #:use-module (crates-io))

(define-public crate-xenet-packet-0.1.0 (c (n "xenet-packet") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xenet-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xenet-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.1.0") (d #t) (k 0)))) (h "1094crr8bp8vxmkvzzif8mk5vj5s0b1myfzwj4hqslys418vg8kr")))

(define-public crate-xenet-packet-0.2.0 (c (n "xenet-packet") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xenet-core") (r "^0.2.0") (d #t) (k 0)) (d (n "xenet-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.2.0") (d #t) (k 0)))) (h "1yc9mycf6zv15vqbimcbn44djr216bl5vzh54yqdplfm52avz93p")))

(define-public crate-xenet-packet-0.3.0 (c (n "xenet-packet") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xenet-core") (r "^0.3.0") (d #t) (k 0)) (d (n "xenet-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.3.0") (d #t) (k 0)))) (h "0i63wfah7yziss24718ipbr2p476zy1b12m5wpyh7hlh0mqr5bm9") (s 2) (e (quote (("serde" "dep:serde" "xenet-core/serde"))))))

(define-public crate-xenet-packet-0.4.0 (c (n "xenet-packet") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xenet-core") (r "^0.4.0") (d #t) (k 0)) (d (n "xenet-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.4.0") (d #t) (k 0)))) (h "1pbfngrp5yqgc9dvfimfd42cpb2shd1469w54sdlzxsn7hi80jq0") (s 2) (e (quote (("serde" "dep:serde" "xenet-core/serde"))))))

(define-public crate-xenet-packet-0.5.0 (c (n "xenet-packet") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xenet-core") (r "^0.5.0") (d #t) (k 0)) (d (n "xenet-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.5.0") (d #t) (k 0)))) (h "0gszihazcg2424ls41ab19svkgxv1fp8l01rlq3zqipbm1xqq9x1") (s 2) (e (quote (("serde" "dep:serde" "xenet-core/serde"))))))

