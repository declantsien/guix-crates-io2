(define-module (crates-io xe ne xenet-packet-builder) #:use-module (crates-io))

(define-public crate-xenet-packet-builder-0.1.0 (c (n "xenet-packet-builder") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xenet-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xenet-packet") (r "^0.1.0") (d #t) (k 0)))) (h "1pmyn0vgiy64jzrxc0z3vb2n7brzy7icw67qxk4aph5ha63pq038")))

(define-public crate-xenet-packet-builder-0.2.0 (c (n "xenet-packet-builder") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xenet-core") (r "^0.2.0") (d #t) (k 0)) (d (n "xenet-packet") (r "^0.2.0") (d #t) (k 0)))) (h "05rl93c9ypj2nlxxfv6qhm2h6pw5kkl47p55wqp6bl0856wq4aks")))

(define-public crate-xenet-packet-builder-0.3.0 (c (n "xenet-packet-builder") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xenet-core") (r "^0.3.0") (d #t) (k 0)) (d (n "xenet-packet") (r "^0.3.0") (d #t) (k 0)))) (h "0d6ig7417r64hs5hga3j058wxvqbi9kfgi57qy1hk7qq63lizcw4")))

(define-public crate-xenet-packet-builder-0.4.0 (c (n "xenet-packet-builder") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xenet-core") (r "^0.4.0") (d #t) (k 0)) (d (n "xenet-packet") (r "^0.4.0") (d #t) (k 0)))) (h "0rb458ivaqv6pxwiipzz628dnb9f32sfpr5h7vgwrfchy6i2143n")))

(define-public crate-xenet-packet-builder-0.5.0 (c (n "xenet-packet-builder") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xenet-core") (r "^0.5.0") (d #t) (k 0)) (d (n "xenet-packet") (r "^0.5.0") (d #t) (k 0)))) (h "1ldqayw41rc685p5q8p7fk9srv8mrb4frpcwydmg5as49x7qgnqy")))

