(define-module (crates-io xe ne xenevtchn) #:use-module (crates-io))

(define-public crate-xenevtchn-0.1.1 (c (n "xenevtchn") (v "0.1.1") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1ig9knysxqq4ill31l1dd5jf694r43mbas9rfjpvhnkpb3dlhhjs")))

(define-public crate-xenevtchn-0.1.2 (c (n "xenevtchn") (v "0.1.2") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "08zn45zqbhqh7iay6p63042mg235d36bji2kavlrqwlfw2m03xa8")))

(define-public crate-xenevtchn-0.1.3 (c (n "xenevtchn") (v "0.1.3") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "10qhvd2pc46aw0yx13vd7dp3i3s8i9zdz0vdvqlh0qvyfblpm5wc")))

(define-public crate-xenevtchn-0.1.4 (c (n "xenevtchn") (v "0.1.4") (d (list (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1d0g99janjbwfn67f7zcqiqqspsrss26m88000766yg2d87pbw0k")))

(define-public crate-xenevtchn-0.1.5 (c (n "xenevtchn") (v "0.1.5") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0wk51wqkh22b6zyn3k1spl7ir987j0x68kwg0amn4cjvs0p14rx2")))

(define-public crate-xenevtchn-0.1.6 (c (n "xenevtchn") (v "0.1.6") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "xenevtchn-sys") (r "^0.1.2") (d #t) (k 0)))) (h "16n1gk4fmqzfq18q947cgyixnbmmym4syrn5xz1k4a76dy9lq739")))

