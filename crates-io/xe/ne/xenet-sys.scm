(define-module (crates-io xe ne xenet-sys) #:use-module (crates-io))

(define-public crate-xenet-sys-0.1.0 (c (n "xenet-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ijba36rfdq0r805z0yyv0v67v1jvf37smsmzf5b1ch39x52yzbd")))

(define-public crate-xenet-sys-0.2.0 (c (n "xenet-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0aaay9ihqmpbxnkgz8bkm0y8p98k95wh95qga8rc30224bzavyq7")))

(define-public crate-xenet-sys-0.3.0 (c (n "xenet-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15lsr8xarlq4851pxmw61iyxakwb2bvqxhxya3k2ki01n2gb32v9")))

(define-public crate-xenet-sys-0.4.0 (c (n "xenet-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08s8rivzf32y7q0129q7b9s7icv0abf4k6c6nil4xid86ywvsclp")))

(define-public crate-xenet-sys-0.5.0 (c (n "xenet-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z5f2c45xysfk0xxdn2my1v9bc386vnihksdz99h40x9r51gzijw")))

