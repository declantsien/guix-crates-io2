(define-module (crates-io xe ne xenet-macro) #:use-module (crates-io))

(define-public crate-xenet-macro-0.1.0 (c (n "xenet-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0z7a6m65vaml43wihyvi4mzzj6aknd4p2qc3ldpja3dhmglzgkch")))

(define-public crate-xenet-macro-0.2.0 (c (n "xenet-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.2.0") (d #t) (k 2)))) (h "022wr4rja66g55m1n0yazgiaf503i555r4bcjhdi0r20nr8cal2z")))

(define-public crate-xenet-macro-0.3.0 (c (n "xenet-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.3.0") (d #t) (k 2)))) (h "0gmh3kbx0l7r1m6p8lsg1z14gbj50dm5i7nd12f3x4nly6h40crb")))

(define-public crate-xenet-macro-0.4.0 (c (n "xenet-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.4.0") (d #t) (k 2)))) (h "0qhxincraqssqcab3mqk4b97y3jxmj0blb8h78mxh2fyrbw1k3cv")))

(define-public crate-xenet-macro-0.5.0 (c (n "xenet-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xenet-macro-helper") (r "^0.5.0") (d #t) (k 2)))) (h "1a4cmqr61px69ssm84v27milrxrclz3sdp8qgfnjx40865yiw4ka")))

