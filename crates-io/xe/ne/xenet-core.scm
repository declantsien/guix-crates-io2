(define-module (crates-io xe ne xenet-core) #:use-module (crates-io))

(define-public crate-xenet-core-0.1.0 (c (n "xenet-core") (v "0.1.0") (d (list (d (n "default-net") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pfyxblwr6957apdkqczkkll4w93i1ifwiawmp5mdjn9jscq865q") (s 2) (e (quote (("serde" "dep:serde" "default-net/serde") ("default" "dep:default-net"))))))

(define-public crate-xenet-core-0.2.0 (c (n "xenet-core") (v "0.2.0") (d (list (d (n "default-net") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lijfmwzwckzf26spxc2zgh415j4sq6wix8c9qh0sbxmd2j1fx8s") (s 2) (e (quote (("serde" "dep:serde" "default-net/serde") ("default" "dep:default-net"))))))

(define-public crate-xenet-core-0.3.0 (c (n "xenet-core") (v "0.3.0") (d (list (d (n "default-net") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q2rdrpv62r24zinlvxdw5wb45dbd0rbbsnsb0217l2ivdmx2zm7") (s 2) (e (quote (("serde" "dep:serde" "default-net/serde") ("default" "dep:default-net"))))))

(define-public crate-xenet-core-0.4.0 (c (n "xenet-core") (v "0.4.0") (d (list (d (n "default-net") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mg75w41pajjvkw2jfa2yqywlrawsfqpvgla0g4jcyq55svvvc32") (s 2) (e (quote (("serde" "dep:serde" "default-net/serde") ("default" "dep:default-net"))))))

(define-public crate-xenet-core-0.5.0 (c (n "xenet-core") (v "0.5.0") (d (list (d (n "default-net") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16qnjnpq5n47k2k40brnpl85vq6nihyckphkm8fwnb6x9wg5483h") (s 2) (e (quote (("serde" "dep:serde" "default-net/serde") ("default" "dep:default-net"))))))

