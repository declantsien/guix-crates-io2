(define-module (crates-io xe ne xenet-macro-helper) #:use-module (crates-io))

(define-public crate-xenet-macro-helper-0.1.0 (c (n "xenet-macro-helper") (v "0.1.0") (d (list (d (n "xenet-core") (r "^0.1.0") (d #t) (k 0)))) (h "0vvp1bzhqmv98lzdvcd8j6q0pbh6wx49wxck89g615rmys4zrk97")))

(define-public crate-xenet-macro-helper-0.2.0 (c (n "xenet-macro-helper") (v "0.2.0") (d (list (d (n "xenet-core") (r "^0.2.0") (d #t) (k 0)))) (h "0bqqh878qii5zcycdwrwciisg43bd2lqfmbag7whrb02wrriz8i2")))

(define-public crate-xenet-macro-helper-0.3.0 (c (n "xenet-macro-helper") (v "0.3.0") (d (list (d (n "xenet-core") (r "^0.3.0") (d #t) (k 0)))) (h "0kxn1zq3fajbl44hwi04dxfsa5sr809pdcmvk1ph8d8svi5vy02b")))

(define-public crate-xenet-macro-helper-0.4.0 (c (n "xenet-macro-helper") (v "0.4.0") (d (list (d (n "xenet-core") (r "^0.4.0") (d #t) (k 0)))) (h "1gay9wy7p86w9vkxh54bfpfbqxig5qa6rzq50j5xi7f68j0vk0iv")))

(define-public crate-xenet-macro-helper-0.5.0 (c (n "xenet-macro-helper") (v "0.5.0") (d (list (d (n "xenet-core") (r "^0.5.0") (d #t) (k 0)))) (h "1g6pnam63dj574j16vhs934pg2jz99bwpwgs4lhpi9hkd456j2vl")))

