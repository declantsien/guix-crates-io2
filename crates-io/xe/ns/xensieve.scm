(define-module (crates-io xe ns xensieve) #:use-module (crates-io))

(define-public crate-xensieve-0.1.0 (c (n "xensieve") (v "0.1.0") (h "179ngamz72jxdd75wpgk1248za15wzhgxhz14dbrp87kpi9i3ips")))

(define-public crate-xensieve-0.2.0 (c (n "xensieve") (v "0.2.0") (h "1yc0kqszdyknlgwabv5q7abw8jqwgfq2n18psypc30g2c4m2c2kf")))

(define-public crate-xensieve-0.3.0 (c (n "xensieve") (v "0.3.0") (h "0abm7az8wzqf1sq66q30jh4gdmfpq6wj5p79ad97678wp35zs9fq")))

(define-public crate-xensieve-0.4.0 (c (n "xensieve") (v "0.4.0") (h "15cvj5496bw6g8nrsm7bk7p0hlarvj9jfwphqpgllip31qiq6sqw")))

(define-public crate-xensieve-0.5.0 (c (n "xensieve") (v "0.5.0") (h "1ncqbqxyx0xfybdcw4w3yf0l5y05g8kvqxgav2mzng2z4dyzh00i")))

(define-public crate-xensieve-0.6.0 (c (n "xensieve") (v "0.6.0") (h "0hpq5zmm2hppyc18nfq2aabzfyzlqsc9pnh988fg0gs8ic3l6mhq")))

(define-public crate-xensieve-0.7.0 (c (n "xensieve") (v "0.7.0") (h "0gmr4z4drnrjr1jg0aa4vkzxg93ncqxh8nzphw05z5sgakr1aszz")))

(define-public crate-xensieve-0.8.0 (c (n "xensieve") (v "0.8.0") (h "0ca4w0pl2f2vqfa71vk236hlc8sg2r9sk68qap2j6qx4rdccdlxq")))

