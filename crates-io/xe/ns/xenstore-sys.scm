(define-module (crates-io xe ns xenstore-sys) #:use-module (crates-io))

(define-public crate-xenstore-sys-0.1.0 (c (n "xenstore-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "1wkv6v1zj8lm3vx087rs2i1xbfql16rimv03vnzvv3ngzd219bhc") (l "xenstore")))

(define-public crate-xenstore-sys-0.1.1 (c (n "xenstore-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "0pdql6i0lx9abavqf7saxff78nk651wqsf11bkmsiafzs99hcyqn") (l "xenstore")))

(define-public crate-xenstore-sys-0.1.2 (c (n "xenstore-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0s8brjkgjzjxsv3zp0h017plhw4kvc0rm3yah87j9qvhpqb4wgvl") (l "xenstore")))

(define-public crate-xenstore-sys-0.1.3 (c (n "xenstore-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1hcdi5df49myrkblp8q317jn0549nwgmf6ri6ma88nbl7agfm2qb") (l "xenstore")))

(define-public crate-xenstore-sys-0.1.4 (c (n "xenstore-sys") (v "0.1.4") (d (list (d (n "bindgen") (r ">=0.49") (d #t) (k 1)))) (h "0hzmc3fl99nw0nkk1iafygn75gvin2x8sf3amvyxjd1nrigjsnn6") (l "xenstore")))

(define-public crate-xenstore-sys-0.2.0 (c (n "xenstore-sys") (v "0.2.0") (d (list (d (n "bindgen") (r ">=0.49") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1xrjy0d3cg9qrznfsfjrgha8si1dvdyfx3g7c76hbb7lnim5vmj7") (f (quote (("static")))) (l "xenstore")))

(define-public crate-xenstore-sys-0.3.0 (c (n "xenstore-sys") (v "0.3.0") (d (list (d (n "bindgen") (r ">=0.49") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "02dpapjj251z3b3qm3xlx71jjs0inbq55l2jcjh8g16yvz1a8r58") (f (quote (("static")))) (l "xenstore")))

