(define-module (crates-io xe d- xed-sys2) #:use-module (crates-io))

(define-public crate-xed-sys2-0.1.0 (c (n "xed-sys2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "07nd1ka1xpwsb96pp3ddnn17wfrmj4d5y9nmkzd1w6v09msw5pis") (l "xed")))

(define-public crate-xed-sys2-0.1.1 (c (n "xed-sys2") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0d9nfb19ng3d5b1r2cgd3wfkkkfc0lcj6n8z7ng1rhzlgcagcflq") (l "xed")))

