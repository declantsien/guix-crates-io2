(define-module (crates-io xe d- xed-sys) #:use-module (crates-io))

(define-public crate-xed-sys-0.1.0 (c (n "xed-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0riaxshj3922a7xhxg24avyb8d7mrrwfhyibkbi4l0j5hqwl3qvm")))

(define-public crate-xed-sys-0.1.1 (c (n "xed-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1i61bzxfingvnp2pl5vi50kfna6c5703c5jgndvb3wdzmjc73rw6")))

(define-public crate-xed-sys-0.2.0 (c (n "xed-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.2") (d #t) (k 1)))) (h "11z8vi237bjkwxgk66v1sknkzkzwbcxgqlvqp0v0lsr3hw17cpbb") (y #t)))

(define-public crate-xed-sys-0.2.1 (c (n "xed-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.2") (d #t) (k 1)))) (h "0xhrxvc3p12hjmigrpv3w9jg4av07w4l7zdsg34p3qg7k6xh7qva") (y #t)))

(define-public crate-xed-sys-0.2.2 (c (n "xed-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.2") (d #t) (k 1)))) (h "16l0rvwd4ha88qjvqrahfxmymbzi5wg51js4q87bgvyxkwy49xgh")))

(define-public crate-xed-sys-0.2.3 (c (n "xed-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.2") (d #t) (k 1)))) (h "1zhnh6mqd6d4cpyzcxfdsa7ibp2yhksrwl96v0r4shm1hdwrv87w")))

(define-public crate-xed-sys-0.2.4 (c (n "xed-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.8") (d #t) (k 1)))) (h "1rdhvll3g7iigqay3i97pkl0x2zi81azz2b30ilsj8ql1dfl6m9y")))

(define-public crate-xed-sys-0.3.0 (c (n "xed-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 1)))) (h "07b8vngjxz09wkg2yyrg9ksz4dx5py65m2h95mlayc6zacqcjpbm")))

(define-public crate-xed-sys-0.4.0 (c (n "xed-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 1)))) (h "1kkggjvs6n0phwp0wyxyl304cpcg82xv7fcl9bi570m3q5bmx64k")))

(define-public crate-xed-sys-0.5.0+xed-2023.12.19 (c (n "xed-sys") (v "0.5.0+xed-2023.12.19") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("experimental"))) (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 1)))) (h "0z87xgxg5jah5y5wgjhjvrsaa6nl7i4z84i19i50r8k3f6ncybsc") (s 2) (e (quote (("bindgen" "dep:bindgen")))) (r "1.64")))

