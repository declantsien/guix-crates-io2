(define-module (crates-io x- bo x-bow) #:use-module (crates-io))

(define-public crate-x-bow-0.1.0 (c (n "x-bow") (v "0.1.0") (d (list (d (n "observables") (r "^0.1.0") (k 0)) (d (n "x-bow-macros") (r "^0.1.0") (d #t) (k 0)))) (h "15phgl5ybmnk36vd12g2zcb8bgwgwcqbbm3b1yhrj7xcf1ffm7wm")))

(define-public crate-x-bow-0.2.0 (c (n "x-bow") (v "0.2.0") (d (list (d (n "async_ui_internal_utils") (r "^0.0.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)) (d (n "x-bow-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1b41h6gkwb0nhkxclh88b5dlbjhsym88qk6z5y13byngkg0frgmd")))

