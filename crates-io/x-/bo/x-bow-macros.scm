(define-module (crates-io x- bo x-bow-macros) #:use-module (crates-io))

(define-public crate-x-bow-macros-0.1.0 (c (n "x-bow-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fg6b19zkh9icmji7jw2fscf74h62agnrhs6ac59nih4fqd7wyf5")))

(define-public crate-x-bow-macros-0.2.0 (c (n "x-bow-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nhy6khhcsnprqbkh7j1prgpr5f906a8p8466zjgr3544d42xz58")))

