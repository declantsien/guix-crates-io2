(define-module (crates-io x- gi x-git-hooks) #:use-module (crates-io))

(define-public crate-x-git-hooks-0.0.1 (c (n "x-git-hooks") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "19xqc6z0zwpq57al09yapmgycvgnacc7nz0q5idhfq0f8lkclx1g")))

