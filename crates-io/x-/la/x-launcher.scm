(define-module (crates-io x- la x-launcher) #:use-module (crates-io))

(define-public crate-x-launcher-0.1.0 (c (n "x-launcher") (v "0.1.0") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0syr320r6pha9a99nwjixl436dy7cd1hkb3v6hy3xb80bl81wclw") (y #t)))

(define-public crate-x-launcher-0.1.1 (c (n "x-launcher") (v "0.1.1") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1rkj7yb98qdya9b8wzy741qkvkfm3k9zxwgk85gibn43ijrk5xhl") (y #t)))

(define-public crate-x-launcher-0.1.2 (c (n "x-launcher") (v "0.1.2") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "059r7jway0iwzf9xsk28wy09m4p3b7c3r6pc0rbvvm89azm0jniq")))

