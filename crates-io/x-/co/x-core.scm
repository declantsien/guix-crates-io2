(define-module (crates-io x- co x-core) #:use-module (crates-io))

(define-public crate-x-core-0.1.0 (c (n "x-core") (v "0.1.0") (d (list (d (n "camino") (r "^1.0.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "determinator") (r "^0.3.0") (d #t) (k 0)) (d (n "guppy") (r "^0.8.0") (d #t) (k 0)) (d (n "hakari") (r "^0.2.0") (f (quote ("summaries"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "rental") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "02xr6r9mlx8v8w9v0i9yxrwns9qbk2rsnnxkr61imn976ldvb1xr")))

