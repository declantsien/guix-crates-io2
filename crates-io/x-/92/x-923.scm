(define-module (crates-io x- #{92}# x-923) #:use-module (crates-io))

(define-public crate-x-923-0.1.0 (c (n "x-923") (v "0.1.0") (h "0nbg08asyi32a40y3bci5gnlzi56sdfy6b4fp25k9cww9l6vbbaz")))

(define-public crate-x-923-0.1.1 (c (n "x-923") (v "0.1.1") (h "0giw5bwyam6430q4cmkz200fij38cx0hwy1rf8289dm2xaafarmj")))

(define-public crate-x-923-0.1.2 (c (n "x-923") (v "0.1.2") (h "1d9n4gbhc7r9mgfw33iz9vrlnypn1q5q9g9hzdr984xy5ls77w5b")))

