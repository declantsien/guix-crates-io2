(define-module (crates-io x- lo x-log) #:use-module (crates-io))

(define-public crate-x-log-0.1.0 (c (n "x-log") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1fas5pqfp01nzlb06wa1c24d7c96m2bip3h3n7al97lx726bipcg")))

(define-public crate-x-log-0.1.1 (c (n "x-log") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1wmvs1rayvaaq98971x875spf6bsdly6hqs5dm8ppw44riiz9mm4")))

(define-public crate-x-log-0.2.0 (c (n "x-log") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1mcmhwvh5f81psfgiw95xghjxfp6qhbwdk00mcgccpdbj14j870v")))

(define-public crate-x-log-0.2.1 (c (n "x-log") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0y6cxa31hyvz7g5wl1nz67z0nqnxbypwzbb0ndk7n3ayi49p8l1l")))

(define-public crate-x-log-0.2.2 (c (n "x-log") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1b8g35ql9r30jys2vp7ssnp9bs5047ndbbh8as49l0kz6py0fc82")))

(define-public crate-x-log-0.2.3 (c (n "x-log") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0pn1ilq8ngh33cw64c5z8fhi6b5glfv1zjkplb2j15fljpz1p0i2")))

(define-public crate-x-log-0.2.4 (c (n "x-log") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "13ckp3x6z3ci5hmikmbag1p4lbaaadjjlk6m0mxmrkaxv7qgxjlq")))

(define-public crate-x-log-0.3.0 (c (n "x-log") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0lkv3ssz9zsvvpqpnf6dvxras0f2hqf0lwy65mrvjk0s89ircl9f")))

(define-public crate-x-log-0.3.1 (c (n "x-log") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1qfjjrhblnk3dlvvy8fhs4xdfpaffgdz56vdyzscalb4yc7i385s")))

(define-public crate-x-log-0.3.2 (c (n "x-log") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "19qmbdc75cr3y7dy7f5spw6w4bkg0wmvn8sqw3lgjch8d1kww0hb")))

(define-public crate-x-log-0.3.3 (c (n "x-log") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0lcmk5250ng5livfwjg9nj5and67ij4hxbqi3yvj957awbxvr7vq")))

(define-public crate-x-log-0.3.4 (c (n "x-log") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0ab2n82mr57m7faz4z4qmg0xgg3fji0ga8w603ql5m3brpnry98w")))

(define-public crate-x-log-0.3.5 (c (n "x-log") (v "0.3.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1fxnaa9ypyfqvbi6dwnac57282yvfshickmvsybzb0g80h0wm1yq")))

(define-public crate-x-log-0.3.6 (c (n "x-log") (v "0.3.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "13dnfqdyc1x06cckcdz2x1ks1m89g3j28y4sdkdxs53qxnl9pqhq")))

(define-public crate-x-log-0.3.7 (c (n "x-log") (v "0.3.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0bqdn8nbxcfqk3ja722cxqv89kv8gizhn4q1blac3amvvv84jjx6")))

(define-public crate-x-log-0.3.8 (c (n "x-log") (v "0.3.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0vx389mmak4igf29i74x7vi4q5137a6s47s7ikzvm0819ns97bkj")))

(define-public crate-x-log-0.4.0 (c (n "x-log") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "06r8cr9dc6xqy9j1xhsra7iw4fw20phynmjqzb0y5hpx83br2lsz") (f (quote (("wasm" "chrono/wasmbind"))))))

(define-public crate-x-log-0.4.1 (c (n "x-log") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "13r0dmjvn2xmic3m8flwxyczfryxka00jr1iix2n35b5q332hx4r") (f (quote (("wasm" "chrono/wasmbind"))))))

(define-public crate-x-log-0.4.2 (c (n "x-log") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0naami06vbdsx842g4mv87i5vb7nan5gjs5bzd3zhwr11rrjhn69") (f (quote (("wasm" "chrono/wasmbind"))))))

