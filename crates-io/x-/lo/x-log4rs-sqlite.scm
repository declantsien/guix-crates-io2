(define-module (crates-io x- lo x-log4rs-sqlite) #:use-module (crates-io))

(define-public crate-x-log4rs-sqlite-0.0.1 (c (n "x-log4rs-sqlite") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0k6cxsvxm9cqddrfgpank6mzi61ad52rcljv3k080qr3i2afar9f")))

(define-public crate-x-log4rs-sqlite-0.0.2 (c (n "x-log4rs-sqlite") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("fast-rng" "v4"))) (d #t) (k 0)))) (h "1mpaw386mxmr2rw1560r338lfi8fvqqzhc3bkhwi1vjv7h9xj5m2")))

(define-public crate-x-log4rs-sqlite-0.0.3 (c (n "x-log4rs-sqlite") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("fast-rng" "v4"))) (d #t) (k 0)))) (h "0z3qyini59b79ws5cpyqsvr0ny8dww1c09n5dh7nidh2wxqq0kl2")))

(define-public crate-x-log4rs-sqlite-0.0.4 (c (n "x-log4rs-sqlite") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("fast-rng" "v4"))) (d #t) (k 0)))) (h "0yil8smqa1lacahhq5wnpyg910gi9ymvias1wy3afprnlv7656ij")))

(define-public crate-x-log4rs-sqlite-0.1.0 (c (n "x-log4rs-sqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("fast-rng" "v4"))) (d #t) (k 0)))) (h "0mab7b2b15g8xwp83ag8f4pw52lbx22m86fz3k96ic4chv3ns3sc")))

