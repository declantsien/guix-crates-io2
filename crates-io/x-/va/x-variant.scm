(define-module (crates-io x- va x-variant) #:use-module (crates-io))

(define-public crate-x-variant-0.1.0 (c (n "x-variant") (v "0.1.0") (h "0m0q9swxm0cvnlnzr2micjsy246ainf7g83s55crf43snycx259h")))

(define-public crate-x-variant-0.1.1 (c (n "x-variant") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fb2v9wkianp7v18l3a18d2n6955da4w2jq7a13r9p72c0c03f5p") (f (quote (("serde-prefix-nums") ("default" "serde"))))))

