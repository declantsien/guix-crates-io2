(define-module (crates-io x- li x-lint) #:use-module (crates-io))

(define-public crate-x-lint-0.1.0 (c (n "x-lint") (v "0.1.0") (d (list (d (n "camino") (r "^1.0.1") (d #t) (k 0)) (d (n "guppy") (r "^0.8.0") (d #t) (k 0)) (d (n "hakari") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "x-core") (r "^0.1.0") (d #t) (k 0)))) (h "05zcn7nlijmq63n7yrsn7fgxh864n6yz94m697jz61czkqj6ybiq")))

