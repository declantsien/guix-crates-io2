(define-module (crates-io x- pi x-pixiv-lib) #:use-module (crates-io))

(define-public crate-x-pixiv-lib-0.1.0 (c (n "x-pixiv-lib") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0m8bmhdi0k1263w6g0ir5gvymazpy1a7mw8554d5l3d9ijsa4v")))

(define-public crate-x-pixiv-lib-0.1.1 (c (n "x-pixiv-lib") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0j1y518bslpds82ah67slirjszf4pp4p1hlwd9if1bp5hp961g")))

(define-public crate-x-pixiv-lib-0.1.2 (c (n "x-pixiv-lib") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i9z1b82pgk8zv4vrcgf5f23j84k3gx03n249b5ldm6kb05jnngr")))

