(define-module (crates-io x- pa x-path) #:use-module (crates-io))

(define-public crate-x-path-0.1.0-alpha.0 (c (n "x-path") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "markdown-includes") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_UI_Shell" "Win32_Foundation" "Win32_Globalization" "Win32_System_Com"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cmz7g9iaz96b8w6di97nhq1v8wcf52vxvnd9wmj96wa2k63bx2k") (f (quote (("strict"))))))

