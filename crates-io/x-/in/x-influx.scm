(define-module (crates-io x- in x-influx) #:use-module (crates-io))

(define-public crate-x-influx-0.9.0 (c (n "x-influx") (v "0.9.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "docopt") (r "~0.8") (d #t) (k 0)) (d (n "influent") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "1whgwjspdd5z0abb8x0slz7dwmwrfzb1jajppxda0kcjarv3nzrq")))

(define-public crate-x-influx-0.9.1 (c (n "x-influx") (v "0.9.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "docopt") (r "~0.8") (d #t) (k 0)) (d (n "influent") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "1lildkqykb68jjlqcx8zxfzia1pa8xxqm671b3p8mgy2k7xnz4y3")))

(define-public crate-x-influx-0.9.2 (c (n "x-influx") (v "0.9.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "docopt") (r "~0.8") (d #t) (k 0)) (d (n "influent") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "0r91n0a32f4d9zpdz4gwyvc5f7x3qq153bnjiqv1jzl868s57biq")))

