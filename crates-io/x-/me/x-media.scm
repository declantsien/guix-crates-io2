(define-module (crates-io x- me x-media) #:use-module (crates-io))

(define-public crate-x-media-0.1.0 (c (n "x-media") (v "0.1.0") (d (list (d (n "core-audio-types") (r "^0.1") (d #t) (k 0)) (d (n "core-media") (r "^0.2") (d #t) (k 0)) (d (n "core-video") (r "^0.2") (d #t) (k 0)) (d (n "x-variant") (r "^0.1") (d #t) (k 0)))) (h "1yhwzza6h805k97dwfi3cb9f9p408a2ykghyk3nwlic78a5yaw3f")))

