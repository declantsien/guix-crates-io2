(define-module (crates-io yf fi yffi) #:use-module (crates-io))

(define-public crate-yffi-0.1.0 (c (n "yffi") (v "0.1.0") (d (list (d (n "lib0") (r "^0.1") (d #t) (k 0)) (d (n "yrs") (r "^0.1") (d #t) (k 0)))) (h "0zfhh741lgb4r8saxqgi3s3zzv9b9ism2j05d95v66fv0fkp373j")))

(define-public crate-yffi-0.2.0 (c (n "yffi") (v "0.2.0") (d (list (d (n "lib0") (r "^0.2") (d #t) (k 0)) (d (n "yrs") (r "^0.2") (d #t) (k 0)))) (h "0ldqci62xnyz0l340ivriwp5a2qpgrh0gcfbymwc63j1vxw14s1v")))

(define-public crate-yffi-0.2.1 (c (n "yffi") (v "0.2.1") (d (list (d (n "lib0") (r "^0.2.1") (d #t) (k 0)) (d (n "yrs") (r "^0.2.1") (d #t) (k 0)))) (h "01v1xqjyzqcdfnavy5pl0qcgsprfm5ybzmv1fby4zn0298rcn8dm")))

(define-public crate-yffi-0.4.0 (c (n "yffi") (v "0.4.0") (d (list (d (n "lib0") (r "^0.4.0") (d #t) (k 0)) (d (n "yrs") (r "^0.4.0") (d #t) (k 0)))) (h "0gkw3j65y38ah3c61lf0w13ik2vj5xk76a7qka7z01khkwwx0wcm")))

(define-public crate-yffi-0.5.0 (c (n "yffi") (v "0.5.0") (d (list (d (n "lib0") (r "^0.5.0") (d #t) (k 0)) (d (n "yrs") (r "^0.5.0") (d #t) (k 0)))) (h "1kpbxrpiqzh1l20vw5jhqwf1g3iyimvnkryf8agy412aa0vh629n")))

(define-public crate-yffi-0.6.0 (c (n "yffi") (v "0.6.0") (d (list (d (n "lib0") (r "^0.6.0") (d #t) (k 0)) (d (n "yrs") (r "^0.6.0") (d #t) (k 0)))) (h "1p15790d2w8qy1353f3nslkl5vb812jh2s9w23mkf5k1za7b19v0")))

(define-public crate-yffi-0.6.1 (c (n "yffi") (v "0.6.1") (d (list (d (n "lib0") (r "^0.6.1") (d #t) (k 0)) (d (n "yrs") (r "^0.6.1") (d #t) (k 0)))) (h "02xbc9w80ry7nnw5i7p4791aq9krb6zzfxrlv2kmnh1g6581vspg")))

(define-public crate-yffi-0.6.2 (c (n "yffi") (v "0.6.2") (d (list (d (n "lib0") (r "^0.6.2") (d #t) (k 0)) (d (n "yrs") (r "^0.6.2") (d #t) (k 0)))) (h "1r5dwz7q3ciqww0dmcxasnf3dndzk371vid7a6xmcsc6x5lzn4gl")))

(define-public crate-yffi-0.7.0 (c (n "yffi") (v "0.7.0") (d (list (d (n "lib0") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.7.0") (d #t) (k 0)))) (h "109jzp538niqxndhhxl6fxr0nkgr6dx8j9xrw1m0aqcihglgm66c")))

(define-public crate-yffi-0.7.1 (c (n "yffi") (v "0.7.1") (d (list (d (n "lib0") (r "^0.7.1") (d #t) (k 0)) (d (n "yrs") (r "^0.7.1") (d #t) (k 0)))) (h "0n6fr1d8kia1l273xfwdxf89fbyk150pj32dyb8n6w6f4svkbjmw")))

(define-public crate-yffi-0.8.0 (c (n "yffi") (v "0.8.0") (d (list (d (n "lib0") (r "^0.8.0") (d #t) (k 0)) (d (n "yrs") (r "^0.8.0") (d #t) (k 0)))) (h "1cs6qzd602s2qsgb3rd8609bl35x9im7yq8bh1xd0gmk1h6id987")))

(define-public crate-yffi-0.8.1 (c (n "yffi") (v "0.8.1") (d (list (d (n "lib0") (r "^0.8.1") (d #t) (k 0)) (d (n "yrs") (r "^0.8.1") (d #t) (k 0)))) (h "0fwsqjf8g8hkkz72611213k5g2vky2a5hmhl6231z0hwn5dn0ang")))

(define-public crate-yffi-0.9.0 (c (n "yffi") (v "0.9.0") (d (list (d (n "lib0") (r "^0.9") (d #t) (k 0)) (d (n "yrs") (r "^0.9") (d #t) (k 0)))) (h "1fbgbplas58zs6lpyfhjwyazcf7gnh1nrlprxzzfg7fq9j1lz0cd")))

(define-public crate-yffi-0.9.1 (c (n "yffi") (v "0.9.1") (d (list (d (n "lib0") (r "^0.9.1") (d #t) (k 0)) (d (n "yrs") (r "^0.9.1") (d #t) (k 0)))) (h "02z3kw2akarijvcxsilbk6ykf6mkw0xl8nicqzpgjm4dffb0c3s8")))

(define-public crate-yffi-0.9.2 (c (n "yffi") (v "0.9.2") (d (list (d (n "lib0") (r "^0.9.2") (d #t) (k 0)) (d (n "yrs") (r "^0.9.2") (d #t) (k 0)))) (h "0qglcrnv717p839vm9fmvh8m27a5rnhs3h4cg5kd7h78fw5frhbc")))

(define-public crate-yffi-0.9.3 (c (n "yffi") (v "0.9.3") (d (list (d (n "lib0") (r "^0.9.3") (d #t) (k 0)) (d (n "yrs") (r "^0.9.3") (d #t) (k 0)))) (h "0j175gynv8fh9s3da0ibv8i4bj980951gcj216d5r8kla1ipzjxw")))

(define-public crate-yffi-0.10.0 (c (n "yffi") (v "0.10.0") (d (list (d (n "lib0") (r "^0.10.0") (d #t) (k 0)) (d (n "yrs") (r "^0.10.0") (d #t) (k 0)))) (h "0fxmrm3bfp1924j6f37223si0zp36rpkkg1sgxicdjcj9d7mc1p9")))

(define-public crate-yffi-0.10.1 (c (n "yffi") (v "0.10.1") (d (list (d (n "lib0") (r "^0.10.1") (d #t) (k 0)) (d (n "yrs") (r "^0.10.1") (d #t) (k 0)))) (h "103fggd64myqss0xav750j4gfmkp5ln21g7m380gp519gbdvjbi0")))

(define-public crate-yffi-0.10.2 (c (n "yffi") (v "0.10.2") (d (list (d (n "lib0") (r "^0.10.2") (d #t) (k 0)) (d (n "yrs") (r "^0.10.2") (d #t) (k 0)))) (h "1mlcng3bv30bs7ws50pkhm27jiv6x8yswa2bni5cr5i9y8mhvzb9")))

(define-public crate-yffi-0.10.3 (c (n "yffi") (v "0.10.3") (d (list (d (n "lib0") (r "^0.10.3") (d #t) (k 0)) (d (n "yrs") (r "^0.10.3") (d #t) (k 0)))) (h "02g37k96z1gdsqqc6i9l94xsgw0yifh5nb6bz76xn5f92nmly199")))

(define-public crate-yffi-0.10.4 (c (n "yffi") (v "0.10.4") (d (list (d (n "lib0") (r "^0.10.4") (d #t) (k 0)) (d (n "yrs") (r "^0.10.4") (d #t) (k 0)))) (h "19bxxppqjcqvgh8r0mi4vz531d9am42bldjl5k64axbmv28jpwr0")))

(define-public crate-yffi-0.10.5 (c (n "yffi") (v "0.10.5") (d (list (d (n "lib0") (r "^0.10.5") (d #t) (k 0)) (d (n "yrs") (r "^0.10.5") (d #t) (k 0)))) (h "190kbnrqwkw64k8p0a45lcrgi36m5a90sz7vgiy6kid8j7vb46hh")))

(define-public crate-yffi-0.10.6 (c (n "yffi") (v "0.10.6") (d (list (d (n "lib0") (r "^0.10.6") (d #t) (k 0)) (d (n "yrs") (r "^0.10.6") (d #t) (k 0)))) (h "1fp5382sxfgj5b0bzkk2k8p7ij395y236s862n3ayp7fj47bigvp")))

(define-public crate-yffi-0.11.0 (c (n "yffi") (v "0.11.0") (d (list (d (n "lib0") (r "^0.11.0") (d #t) (k 0)) (d (n "yrs") (r "^0.11.0") (d #t) (k 0)))) (h "1raqyycpmnldkhjzjw0yrdj7srmrr61cb67ly67cg889wn2giybl")))

(define-public crate-yffi-0.11.1 (c (n "yffi") (v "0.11.1") (d (list (d (n "lib0") (r "^0.11.1") (d #t) (k 0)) (d (n "yrs") (r "^0.11.1") (d #t) (k 0)))) (h "0ksgg8n0j7iafk9pqiqw6vc9hd0l28yh6xcjp8yajchd0ziw82s2")))

(define-public crate-yffi-0.11.2 (c (n "yffi") (v "0.11.2") (d (list (d (n "lib0") (r "^0.11.2") (d #t) (k 0)) (d (n "yrs") (r "^0.11.2") (d #t) (k 0)))) (h "0dl0hgwh9bg8j2sd3fp510bypc2kkz35vwr7xy8c18nacf4knvcc")))

(define-public crate-yffi-0.12.0 (c (n "yffi") (v "0.12.0") (d (list (d (n "lib0") (r "^0.12.0") (d #t) (k 0)) (d (n "yrs") (r "^0.12.0") (d #t) (k 0)))) (h "0w90d2kzhq19mxxk7c4wjdpp54y0v6k8f9yrzx0a1g06qq2d1rrf")))

(define-public crate-yffi-0.12.1 (c (n "yffi") (v "0.12.1") (d (list (d (n "lib0") (r "^0.12.1") (d #t) (k 0)) (d (n "yrs") (r "^0.12.1") (d #t) (k 0)))) (h "1anhd5aampjwhry7hb9jlaa0pnwcklslqf6j5zmzrzj81p392c27")))

(define-public crate-yffi-0.12.2 (c (n "yffi") (v "0.12.2") (d (list (d (n "lib0") (r "^0.12.2") (d #t) (k 0)) (d (n "yrs") (r "^0.12.2") (d #t) (k 0)))) (h "1xisb9y0cgzk4nb6rgr0b8bzbqnvh2a2kql0ghdhx15r156hkc6p")))

(define-public crate-yffi-0.13.0 (c (n "yffi") (v "0.13.0") (d (list (d (n "lib0") (r "^0.13.0") (d #t) (k 0)) (d (n "yrs") (r "^0.13.0") (d #t) (k 0)))) (h "0agdir6669nzjq27ixas7xxm05k6sdgykc92w4hqa81id31f6r1s")))

(define-public crate-yffi-0.14.0 (c (n "yffi") (v "0.14.0") (d (list (d (n "lib0") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.14.0") (d #t) (k 0)))) (h "1w5p9jns2zfnanzpd71hn1mpl7g1ixi1zy7xvl1qcn47k3wbwmqg")))

(define-public crate-yffi-0.14.1 (c (n "yffi") (v "0.14.1") (d (list (d (n "lib0") (r "^0.14.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.14.1") (d #t) (k 0)))) (h "1ynm16mmlmqjljhz2jmvgdb4pp0imhgihzl9pkwm0lisc5agzq3x")))

(define-public crate-yffi-0.15.0 (c (n "yffi") (v "0.15.0") (d (list (d (n "lib0") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.15.0") (d #t) (k 0)))) (h "1xb61bryal1rfp5r2b7x2qnjja3ssbw9m7mf64y7q1z9pn360d8a")))

(define-public crate-yffi-0.16.0 (c (n "yffi") (v "0.16.0") (d (list (d (n "lib0") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.0") (d #t) (k 0)))) (h "0qmqb3kjgqqm7jwbvc2v2p3zlwdwmk9gv4v6vx6b0bgbnddjzf5i")))

(define-public crate-yffi-0.16.1 (c (n "yffi") (v "0.16.1") (d (list (d (n "lib0") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.1") (d #t) (k 0)))) (h "0pjzddi8k82n5dlyqa3rhnqi5yzv2gapzcjqqk23yy0rcf5zp9vl")))

(define-public crate-yffi-0.16.2 (c (n "yffi") (v "0.16.2") (d (list (d (n "lib0") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.2") (d #t) (k 0)))) (h "0yf980yp3dpid5m08qpaqhb9h1fcbgs6rsy521i249kcgbadhvm5")))

(define-public crate-yffi-0.16.3 (c (n "yffi") (v "0.16.3") (d (list (d (n "lib0") (r "^0.16.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.3") (d #t) (k 0)))) (h "1bz3n92mrr8p9zn514bxarbzxhmcbq77dgsnyl3nkgqvhb3bk3fz")))

(define-public crate-yffi-0.16.4 (c (n "yffi") (v "0.16.4") (d (list (d (n "lib0") (r "^0.16.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.4") (d #t) (k 0)))) (h "01ji8xzmr8vih057b7yls69w2mmxx505rgf7n1v3435jck2m9j58")))

(define-public crate-yffi-0.16.5 (c (n "yffi") (v "0.16.5") (d (list (d (n "lib0") (r "^0.16.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.5") (d #t) (k 0)))) (h "09ahr1jmpgspjcvj5f44a1nvmjjdz3762x4ijkf3s1pnb7pg7xpj")))

(define-public crate-yffi-0.16.7 (c (n "yffi") (v "0.16.7") (d (list (d (n "lib0") (r "^0.16.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.7") (d #t) (k 0)))) (h "073j0ppkbz1lm5l4sz36b32zh0d9d231gb6ph760z29y32c1ac79")))

(define-public crate-yffi-0.16.8 (c (n "yffi") (v "0.16.8") (d (list (d (n "lib0") (r "^0.16.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.8") (d #t) (k 0)))) (h "11spgs168pakhdj7w3d4c6226dvipbn3j069kf766wwn47sqca04")))

(define-public crate-yffi-0.16.9 (c (n "yffi") (v "0.16.9") (d (list (d (n "lib0") (r "^0.16.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.9") (d #t) (k 0)))) (h "0s70igy5w59347x0g7g2vg6nii9bp76fd6av7vnfnbwpzcg5svg6")))

(define-public crate-yffi-0.16.10 (c (n "yffi") (v "0.16.10") (d (list (d (n "lib0") (r "^0.16.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.16.10") (d #t) (k 0)))) (h "0r0mf2wcxmrcl31wbyla4drqwcb3s5ppaczavqmp6brdfg1arki6")))

(define-public crate-yffi-0.17.0 (c (n "yffi") (v "0.17.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17") (f (quote ("weak"))) (d #t) (k 0)))) (h "0jgzzcp842abcp23a7p0kyz948kvm7zwqng4h8xm1j4p8mz5ky9v")))

(define-public crate-yffi-0.17.1 (c (n "yffi") (v "0.17.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17") (f (quote ("weak"))) (d #t) (k 0)))) (h "1w01vag16v2b2hyfdfcvnx3402arsras6p6hcy170mc6ra5ydy6w")))

(define-public crate-yffi-0.17.2 (c (n "yffi") (v "0.17.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17.2") (f (quote ("weak"))) (d #t) (k 0)))) (h "0f1qzvz6k1m0r0yv20jx4lzgv92skl0xhx6g1nia7q35izqxjhvl")))

(define-public crate-yffi-0.17.3 (c (n "yffi") (v "0.17.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17.3") (f (quote ("weak"))) (d #t) (k 0)))) (h "1qhjvrn6yg51k814pgy2g4765qwlszn1myx4aakhx0x06cj8sa2z")))

(define-public crate-yffi-0.17.4 (c (n "yffi") (v "0.17.4") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17.4") (f (quote ("weak"))) (d #t) (k 0)))) (h "0gz6pcbri0sgxkrigyzm261lf80r442xzbnwlx98x5b6ji5dxdv0")))

(define-public crate-yffi-0.18.0 (c (n "yffi") (v "0.18.0") (d (list (d (n "yrs") (r "^0.18") (f (quote ("weak"))) (d #t) (k 0)))) (h "15d0wvaa9crx7jkz323q5y12wlnby8kkbianzfxnyl9v96gb63mz")))

(define-public crate-yffi-0.18.1 (c (n "yffi") (v "0.18.1") (d (list (d (n "yrs") (r "^0.18.1") (f (quote ("weak"))) (d #t) (k 0)))) (h "1w0ky1jgzr8x8qjqgs07i5j1mxq25zyr597y6yy7cbhx9nb4vdpi")))

(define-public crate-yffi-0.18.2 (c (n "yffi") (v "0.18.2") (d (list (d (n "yrs") (r "^0.18.2") (f (quote ("weak"))) (d #t) (k 0)))) (h "08kc37alfb5wxzajnm2km0sw30yjr4qxknvm9ljk93z9i2jf4248")))

(define-public crate-yffi-0.18.3 (c (n "yffi") (v "0.18.3") (d (list (d (n "yrs") (r "^0.18.3") (f (quote ("weak"))) (d #t) (k 0)))) (h "038ppa6pihym3cyh6hbq76a2h4wm217809rvmpi8k0if7fjwq8bw")))

(define-public crate-yffi-0.18.4 (c (n "yffi") (v "0.18.4") (d (list (d (n "yrs") (r "^0.18.4") (f (quote ("weak"))) (d #t) (k 0)))) (h "15ndgffqrp59aj45rjqgyzf548gbnkbfnwblgnw3ryqq4k2l1ib6")))

(define-public crate-yffi-0.18.5 (c (n "yffi") (v "0.18.5") (d (list (d (n "yrs") (r "^0.18.5") (f (quote ("weak"))) (d #t) (k 0)))) (h "0bkwjrvimq6ziglh048iqahy40dfch8i73ld14rjs8spbyxvr35k")))

(define-public crate-yffi-0.18.6 (c (n "yffi") (v "0.18.6") (d (list (d (n "yrs") (r "^0.18.6") (f (quote ("weak"))) (d #t) (k 0)))) (h "0jzljq449394rbwc3f13lj0ljh8d36rqw280bw88yji6ffggqiv0")))

(define-public crate-yffi-0.18.7 (c (n "yffi") (v "0.18.7") (d (list (d (n "yrs") (r "^0.18.7") (f (quote ("weak"))) (d #t) (k 0)))) (h "1gjn8qbkx16xy6lra75680prh203bgxkbwy9dxg8i4hyh4p861rg")))

(define-public crate-yffi-0.18.8 (c (n "yffi") (v "0.18.8") (d (list (d (n "yrs") (r "^0.18.8") (f (quote ("weak"))) (d #t) (k 0)))) (h "1mf2vdc0mgj2swnx82gm49jkkhxcxd70xyncxcr8p5gnj5q319y5")))

