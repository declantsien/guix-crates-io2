(define-module (crates-io yf ft yfft) #:use-module (crates-io))

(define-public crate-yfft-0.1.0 (c (n "yfft") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.1.36") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.0") (d #t) (k 0)))) (h "1l6w4qvldhp8c9hi4ykkcll6vywyhxbsxlc1j31svnlpdzi0ir3n")))

