(define-module (crates-io gv fs gvfs) #:use-module (crates-io))

(define-public crate-gvfs-0.1.0 (c (n "gvfs") (v "0.1.0") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "zip") (r "^0.5") (k 0)))) (h "13gizvzmsn78cr6a0bcv7as453adplhqnwgfc7a7ckj8v31p6sdx")))

(define-public crate-gvfs-0.1.1 (c (n "gvfs") (v "0.1.1") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "zip") (r "^0.5") (k 0)))) (h "0yiiklvz94nwp4j2dd45ckrj2yb2216y4zfjacn124pdf0mi3xyx")))

(define-public crate-gvfs-0.1.2 (c (n "gvfs") (v "0.1.2") (d (list (d (n "directories") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "zip") (r "^0.5") (k 0)))) (h "0s6kfla380js9gkbjxy19qpnpcxpqzkwjplp8rjr8v3v3ad4na9r")))

