(define-module (crates-io gv r- gvr-sys) #:use-module (crates-io))

(define-public crate-gvr-sys-0.1.0 (c (n "gvr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "1zzv6gfsfc9gk5qxyf2c4bh3jkq3ir2lj57vh6wd5zngb4zqk9d3")))

(define-public crate-gvr-sys-0.2.0 (c (n "gvr-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "14vdl1hhk6g17g4msbx8w82xxwg5wbc6mkff99phzsrhw5q0pyh4")))

(define-public crate-gvr-sys-0.3.0 (c (n "gvr-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "01vd25skjndmv9pyvm1cd1kvmi7sn3b9wrvyi4vf24pw7byhj5bd")))

(define-public crate-gvr-sys-0.4.0 (c (n "gvr-sys") (v "0.4.0") (h "10p6qzxb8fv6dxrcb7mja4jk2hvfs00bwr56sh5cvdbdjfl8c796")))

(define-public crate-gvr-sys-0.5.0 (c (n "gvr-sys") (v "0.5.0") (h "0k0pprmxjxx0n7xj9nvxwqvfky2nz4jmaiv9ns3xw9fr7khsajz8")))

(define-public crate-gvr-sys-0.6.0 (c (n "gvr-sys") (v "0.6.0") (h "1cr4kmgs38mvjxmjwfad4320y41ab350vjrn9zh0rv91bx865plj")))

(define-public crate-gvr-sys-0.7.0 (c (n "gvr-sys") (v "0.7.0") (h "16jv13cxp0f960jmc4csc8w1b31xynp3srj4vjfk2ryfv2a4ncxi")))

(define-public crate-gvr-sys-0.7.1 (c (n "gvr-sys") (v "0.7.1") (h "12lmc4zij98if9srbrc9imnpxkg1vbr37cmypzk8qlvknd6l6zs9")))

(define-public crate-gvr-sys-0.7.2 (c (n "gvr-sys") (v "0.7.2") (h "15zpllq0g1lx7qsmf9pqjp7m17w3yvj1j06mv1grwvvm2bnyda6j")))

