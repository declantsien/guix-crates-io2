(define-module (crates-io gv ox gvox-rs) #:use-module (crates-io))

(define-public crate-gvox-rs-0.1.0 (c (n "gvox-rs") (v "0.1.0") (h "0gvdcvm6sslr909m5z68h3hbg3hr4qqfr7m7hdn60rym7xaaazjr")))

(define-public crate-gvox-rs-0.1.1 (c (n "gvox-rs") (v "0.1.1") (h "01zpcbkw74va0lbzgzpvkl42ln6x10avlzy14g4jphj336pq4q0l")))

(define-public crate-gvox-rs-0.1.2 (c (n "gvox-rs") (v "0.1.2") (h "0gaaawff3lz3am2a43pksqpjhgx3prba07cslfr40v1dc9aksp46")))

(define-public crate-gvox-rs-0.1.3 (c (n "gvox-rs") (v "0.1.3") (h "1h5gggm43vfcyvkmgmj5vq0pgim6c52hsyp5jg5qgjkmkwrsimkp")))

(define-public crate-gvox-rs-0.1.4 (c (n "gvox-rs") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1yj8jrz517w24x7kqia8kqpl2q4h5ndjf688a9s3yj49clah1h0q")))

(define-public crate-gvox-rs-0.1.5 (c (n "gvox-rs") (v "0.1.5") (d (list (d (n "gvox-sys") (r "^0.0.1") (k 0)))) (h "1g3zdflijnn6wjlgb99sisd51mxmr8ccqv1v9c8wbp9psl5sji9l")))

(define-public crate-gvox-rs-0.1.6 (c (n "gvox-rs") (v "0.1.6") (d (list (d (n "gvox-sys") (r "^0.1.0") (k 0)))) (h "1xrn95gadr3ghrjv38ry2w51hb6p4akx4i0mk95c1akqmf17845a")))

(define-public crate-gvox-rs-0.1.7 (c (n "gvox-rs") (v "0.1.7") (d (list (d (n "gvox-sys") (r "^1.0.0") (k 0)))) (h "1ymbszvrgsa9wkdzkhgf6fvhwgjf7brcbdd3743lzz265jq7cazq")))

(define-public crate-gvox-rs-1.0.0 (c (n "gvox-rs") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.0.1") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "0d5vw2nmffbnnck7qjks7sgz60wj2bkq0ijwnq79q8gal1mvqs8l")))

(define-public crate-gvox-rs-1.1.0 (c (n "gvox-rs") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.1.0") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1ch00cjxqjxkmxwc4m1ldmw3yqwhas2jq331wx5gn11dpksnfhms")))

(define-public crate-gvox-rs-1.2.0 (c (n "gvox-rs") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.0") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1xf3cackq38zc0di2v644x74af4crvfr4h9qr1l2n3zrngcg77fh")))

(define-public crate-gvox-rs-1.2.1 (c (n "gvox-rs") (v "1.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.1") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "07jhymkp4lza1vlh1516l46952s97knsb816m21lrrl2b56qpb4b")))

(define-public crate-gvox-rs-1.2.2 (c (n "gvox-rs") (v "1.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.2") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1ins2j5wsg401mnd9m9whn8adm3gkdn563zlnmrq6vv496nmqhbn")))

(define-public crate-gvox-rs-1.2.3 (c (n "gvox-rs") (v "1.2.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.3") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "0j4pw16nmjcmxxfnvs8y3qfwk9d4l0pva4bj314c9bx6x3p0iam4")))

(define-public crate-gvox-rs-1.2.4 (c (n "gvox-rs") (v "1.2.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.4") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1436wr8l3nlw74djcljgaw27441dgwdl01qvg8xbpvagmg1f7882")))

(define-public crate-gvox-rs-1.2.5 (c (n "gvox-rs") (v "1.2.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.5") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "0dczfpwzfrcwljxirq3s17g0f8vd7fyl957djfqb8qq5kppjsklk")))

(define-public crate-gvox-rs-1.2.6 (c (n "gvox-rs") (v "1.2.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.6") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "018xa0wizmp3n8znfmxap3hhhscic4icmmr8lg35iyigh5pqzgw7")))

(define-public crate-gvox-rs-1.2.7 (c (n "gvox-rs") (v "1.2.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.7") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "0nqqhc4j9mkdnx48ms67h5mjzkxzk5pws9a9rlw77x10c0w1q962")))

(define-public crate-gvox-rs-1.2.8 (c (n "gvox-rs") (v "1.2.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.8") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "15251ybwc0aiafw1fq2bsd43h936fjj2hmwkwa60ni618zvsj7cr")))

(define-public crate-gvox-rs-1.2.9 (c (n "gvox-rs") (v "1.2.9") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.9") (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "0d7a02g4x3bgcf0p085r42qqf5vmkkdyx18vsvhdizm9hwxxglgg")))

(define-public crate-gvox-rs-1.2.10 (c (n "gvox-rs") (v "1.2.10") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.10") (k 0)))) (h "13cl9yiwkf7z1lz5y5fjp4mhyglnwbpbig0pjvnkkpxp6m3dpvv1")))

(define-public crate-gvox-rs-1.2.11 (c (n "gvox-rs") (v "1.2.11") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.2.11") (k 0)))) (h "004kdx78xnf40wiynzn97s1r0g1r7i43gwjbk5r2bm13ngn15rw3")))

(define-public crate-gvox-rs-1.3.0 (c (n "gvox-rs") (v "1.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "gvox-sys") (r "^1.3.0") (k 0)))) (h "1r362y5shxz9qmbywg41jnlif4443jcl3pig85zxvssbwhnkjp8z")))

