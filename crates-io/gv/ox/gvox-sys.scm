(define-module (crates-io gv ox gvox-sys) #:use-module (crates-io))

(define-public crate-gvox-sys-0.0.1 (c (n "gvox-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "19q2qkkppnr55whxfvcdp597nfmdr9y3770x0cng8x2lacg8jgnd")))

(define-public crate-gvox-sys-0.1.0 (c (n "gvox-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0dy8xcr5jkwf0jc6xgaj5gr8pn938h0vszacj3cmiivayc7rzw93")))

(define-public crate-gvox-sys-1.0.0 (c (n "gvox-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0v8441dzacrznrivq50kdzsrvh0wx2czq2rnm35a56d7spij42s1")))

(define-public crate-gvox-sys-1.0.1 (c (n "gvox-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "15ysy1vczbj9jn0ylr6lbc899zk325s4d6swklrwwvymxgg05537")))

(define-public crate-gvox-sys-1.1.0 (c (n "gvox-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1xbqdgg2k8dabv5vc38rb5x4bjwqb8g7sh7nychs6hnyc1vz6g5l")))

(define-public crate-gvox-sys-1.2.0 (c (n "gvox-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "109dxjw0gdqdxyyk5r9m23f86wa8qpsy072nvcnzwnzh48cy4c0n")))

(define-public crate-gvox-sys-1.2.1 (c (n "gvox-sys") (v "1.2.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0dp9qgj4dbqi28cch8yi86k3zgfmf04walcrq4ic7gzgchk5zji2")))

(define-public crate-gvox-sys-1.2.2 (c (n "gvox-sys") (v "1.2.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "15rj5h7qhlak2jcap6jr5qkwndhqz1245z4d4d09abx58zvv2pva")))

(define-public crate-gvox-sys-1.2.3 (c (n "gvox-sys") (v "1.2.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "13fb7z98yxh44r8wnbaqrzn7q3w53yv6a3mm2c6dh7bpdppc98ss")))

(define-public crate-gvox-sys-1.2.4 (c (n "gvox-sys") (v "1.2.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "175rglxz2gl25wnhkizc7gdkqvchj5xjr97msq1shdfdif45607d")))

(define-public crate-gvox-sys-1.2.5 (c (n "gvox-sys") (v "1.2.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1xp6l4nj4l4ljmbm8a6pkd5ci37x722dpfbafgjhqdf28rb7gi88")))

(define-public crate-gvox-sys-1.2.6 (c (n "gvox-sys") (v "1.2.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1phbp5pn91ga7cswbk49xj9irxh12hbfvk5jhrrajlz1r5g32bvf")))

(define-public crate-gvox-sys-1.2.7 (c (n "gvox-sys") (v "1.2.7") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0qm571x6bmx94dsgwqrg4b3wsnmpycgb0pk4b8gsq8dgwzhv4556")))

(define-public crate-gvox-sys-1.2.8 (c (n "gvox-sys") (v "1.2.8") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "195qwb5cp54hpimaxy228bf6p29wz8q7rdwdhzykp9h3iwa8i0ay")))

(define-public crate-gvox-sys-1.2.9 (c (n "gvox-sys") (v "1.2.9") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "13c6243wi7ar7g2l4k7k6vlyg010mw43lwk84brpk959vdar100c")))

(define-public crate-gvox-sys-1.2.10 (c (n "gvox-sys") (v "1.2.10") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "13ccx4bz9s5bp0dskyna2as5i3abj7yphsj6rq4sjfq72rk29280")))

(define-public crate-gvox-sys-1.2.11 (c (n "gvox-sys") (v "1.2.11") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0nifc0vm4dj4ifm8yk98xm0mc7574lzcv30da8nj94vdnsrjmzj1")))

(define-public crate-gvox-sys-1.3.0 (c (n "gvox-sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1b8cx8fzkh4750gc705pgx77svql74fb8vpiww82n9sa5vi4niss")))

