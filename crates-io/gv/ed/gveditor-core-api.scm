(define-module (crates-io gv ed gveditor-core-api) #:use-module (crates-io))

(define-public crate-gveditor-core-api-0.1.0 (c (n "gveditor-core-api") (v "0.1.0") (h "0f9djnfr3v8i9z8p418kw4s3viynsfssacn18n89hx4jskas99b9")))

(define-public crate-gveditor-core-api-0.1.1 (c (n "gveditor-core-api") (v "0.1.1") (h "15scp38559mxagxd8qbh0rikvb8szv0fvb3xk5dp0q0y4qy5n15i")))

(define-public crate-gveditor-core-api-0.1.3 (c (n "gveditor-core-api") (v "0.1.3") (h "071bhvnh4c6r8r3fn1f6qjn5b66pmzp5cfa2751wcdj8knh6czhz")))

(define-public crate-gveditor-core-api-0.1.4 (c (n "gveditor-core-api") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "04xq6i4czh6r5a7hd65ymzxc2hb851ij4ndjcnxchbacb13i9dw0")))

(define-public crate-gveditor-core-api-0.1.5 (c (n "gveditor-core-api") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0k87p856aky5avcrg2919402w39sl5q94aibpln236zvgz7vsql2")))

(define-public crate-gveditor-core-api-0.1.6 (c (n "gveditor-core-api") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "11bb41xjj3bh0f7n5r1zsz6jxs1qd184byrv8f6khrq0d601rwvh")))

(define-public crate-gveditor-core-api-0.1.7 (c (n "gveditor-core-api") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (f (quote ("fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)))) (h "03x8pagip1l9rpdq49swbsj99s5qfpcpfyfms9mwicqhmkrdx4i3")))

