(define-module (crates-io gv nc gvnc) #:use-module (crates-io))

(define-public crate-gvnc-0.1.0 (c (n "gvnc") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "gvnc-sys")) (d (n "gio") (r "^0.14") (d #t) (k 0)) (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bkpa1bnlas1hdr5xr91x00iwymxalk7v1z329h5xhx8rlrnx5xc")))

(define-public crate-gvnc-0.2.0 (c (n "gvnc") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.2.0") (d #t) (k 0) (p "gvnc-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k19kx6jrc5q99x1cphhh1n24ijrfx93wqnkvv6vazxgxi52qg0f")))

(define-public crate-gvnc-0.3.0 (c (n "gvnc") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.3.0") (d #t) (k 0) (p "gvnc-sys")) (d (n "gio") (r "^0.16") (d #t) (k 0)) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jfbvgqwb5az2jnwp5y868mx5csslgvcs8axxrn4f0ga8v9s2gfa")))

(define-public crate-gvnc-0.4.0 (c (n "gvnc") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4.0") (d #t) (k 0) (p "gvnc-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13s185bsqajcgp07bncczhmrg5wzrhg66a77wm2sqkxxm10kp0dp")))

