(define-module (crates-io gv ar gvariant) #:use-module (crates-io))

(define-public crate-gvariant-0.1.0 (c (n "gvariant") (v "0.1.0") (d (list (d (n "gvariant-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1dx5avb27sx9qkv062chd2ca6wvr4xlz9yki2ac4l2nqc6ks4igl")))

(define-public crate-gvariant-0.2.0 (c (n "gvariant") (v "0.2.0") (d (list (d (n "gvariant-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0lm7cvw8c2jsdx41dlf8syf4ynw91xvsklyzj66pi2lg3bk08gfm") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.3.0 (c (n "gvariant") (v "0.3.0") (d (list (d (n "gvariant-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "10afypw3znjnzgd9f3m0vilb8ibs06pmv73qjan6y16101g32bk6") (f (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.4.0 (c (n "gvariant") (v "0.4.0") (d (list (d (n "gvariant-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0fp1h5vj035g2qpgzkksj6n1vdgmnw64h6qvxa027zrqdcmrhykz") (f (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.5.0 (c (n "gvariant") (v "0.5.0") (d (list (d (n "gvariant-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "03j6nmwxdhnyk9baappgpaaa1yladdign8ci6r4imfdkhsasb3mx") (f (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

