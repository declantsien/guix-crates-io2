(define-module (crates-io gv ar gvariant-macro) #:use-module (crates-io))

(define-public crate-gvariant-macro-0.1.0 (c (n "gvariant-macro") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g4qmgd0byic98f4dav8qign4vwaxp8hsj7a3qwiavdx46vnlbyv")))

(define-public crate-gvariant-macro-0.2.0 (c (n "gvariant-macro") (v "0.2.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f6sn874822hr73wc8bajf4x1czjcckyii8v81pn689ac4k79577")))

(define-public crate-gvariant-macro-0.3.0 (c (n "gvariant-macro") (v "0.3.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kqlsxla5mnpzyvcsacmslwwmi8mndmpk310labkbg1n5i6ll3dx")))

(define-public crate-gvariant-macro-0.4.0 (c (n "gvariant-macro") (v "0.4.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vgbsb34kfvxnirh5irparfs4dfgifkxpa7f0f0s4dblrnf725cr")))

(define-public crate-gvariant-macro-0.5.0 (c (n "gvariant-macro") (v "0.5.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wdr97syw6imvfb4n5w27h97ccaz1fvs822kfrn6blln6wxw7mi6")))

