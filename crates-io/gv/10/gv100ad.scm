(define-module (crates-io gv #{10}# gv100ad) #:use-module (crates-io))

(define-public crate-gv100ad-0.1.0 (c (n "gv100ad") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "13lq0giw0r1w84paaxmszaih1011fhy85kw86xhm77krm4l2hhl1")))

(define-public crate-gv100ad-0.2.0 (c (n "gv100ad") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0c90x7pxijlbczhv177vi6k9qmnmhva5i5wz107dgyhrlv441fxy")))

