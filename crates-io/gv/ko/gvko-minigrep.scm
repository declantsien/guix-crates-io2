(define-module (crates-io gv ko gvko-minigrep) #:use-module (crates-io))

(define-public crate-gvko-minigrep-0.1.0 (c (n "gvko-minigrep") (v "0.1.0") (h "1mxhshjl2s7shvq2g4i56gjkg30jawn8jmifi7mgh2gl2a5vpvfj")))

(define-public crate-gvko-minigrep-0.1.1 (c (n "gvko-minigrep") (v "0.1.1") (h "0avlplnjqvifflbw6886n80yy8vgymamm6bnn900sx4fmjz3lwa3")))

