(define-module (crates-io gv db gvdb-macros) #:use-module (crates-io))

(define-public crate-gvdb-macros-0.1.0 (c (n "gvdb-macros") (v "0.1.0") (d (list (d (n "gvdb") (r "^0.1.0") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1j1xj46z866wn05njqnmjdnylrrsy71pjp8byzx2qgswwlgl2n4b")))

(define-public crate-gvdb-macros-0.1.1 (c (n "gvdb-macros") (v "0.1.1") (d (list (d (n "gvdb") (r "^0.1.0") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0cnwyfwbvp8yhdfwwfvx0j9cpkkip4d7cp1if91nmkijgajvqpa6")))

(define-public crate-gvdb-macros-0.1.2 (c (n "gvdb-macros") (v "0.1.2") (d (list (d (n "gvdb") (r "^0.1.1") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0ixhizg37c2gbva34ja2wjsdx2lsn5b7rrrnrxz5pnc0r9ffnnka")))

(define-public crate-gvdb-macros-0.1.3 (c (n "gvdb-macros") (v "0.1.3") (d (list (d (n "gvdb") (r "^0.1.1") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "19z7zbvmar35kz2f7la4xkrvajgc69kadxl90kcwlqxblb0aqjl8")))

(define-public crate-gvdb-macros-0.1.4 (c (n "gvdb-macros") (v "0.1.4") (d (list (d (n "gvdb") (r "^0.2") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "169ll527n31syq7asjdgrss5xma3d4kj5scql8k4kbixrmibq68r")))

(define-public crate-gvdb-macros-0.1.5 (c (n "gvdb-macros") (v "0.1.5") (d (list (d (n "gvdb") (r "^0.3") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "008dvrgnwjdxyyxw0n0gij6vgb1kjbsiaa698vqil8z2b03ls3p1")))

(define-public crate-gvdb-macros-0.1.6 (c (n "gvdb-macros") (v "0.1.6") (d (list (d (n "gvdb") (r "^0.4") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "17jjqrjmjvyy7mf5ikdyqjw0wyx4m8r8axgwa7n0jq9cyp5nl5n5") (r "1.65")))

(define-public crate-gvdb-macros-0.1.7 (c (n "gvdb-macros") (v "0.1.7") (d (list (d (n "gvdb") (r "^0.4") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0akfhvibfj696l9mlba0c8bj7cgp7blqxir4rfvwj9p1zaf0gk1y") (r "1.65")))

(define-public crate-gvdb-macros-0.1.8 (c (n "gvdb-macros") (v "0.1.8") (d (list (d (n "gvdb") (r "^0.5") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0g674awgjipakb3mn4pd9m334z5d8pwcpg44fj6d7ajw2bismkdc") (r "1.65")))

(define-public crate-gvdb-macros-0.1.9 (c (n "gvdb-macros") (v "0.1.9") (d (list (d (n "gvdb") (r "^0.5") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1m2lmkv6qnryinvi6x9ma73qcbihx3cv2jjxvsa6h86za0mvv237") (r "1.65")))

(define-public crate-gvdb-macros-0.1.10 (c (n "gvdb-macros") (v "0.1.10") (d (list (d (n "gvdb") (r "^0.5") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0m6xzikcyw3j47mjfdi60rv1pyncw7sklw8l5yqw5vp9cn3ma28x") (r "1.65")))

(define-public crate-gvdb-macros-0.1.11 (c (n "gvdb-macros") (v "0.1.11") (d (list (d (n "gvdb") (r "^0.5") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0hw3xgcngvx1mm521iw8bhhga4jzwl50v0ddvvva9sakamgs8s4w") (r "1.70")))

(define-public crate-gvdb-macros-0.1.12 (c (n "gvdb-macros") (v "0.1.12") (d (list (d (n "gvdb") (r "^0.6") (f (quote ("gresource"))) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1spm5y74pbf6ck4yn8pfllx6j13czha1nc6c7c7rr16gipij9cnn") (r "1.75")))

