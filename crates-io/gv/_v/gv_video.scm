(define-module (crates-io gv _v gv_video) #:use-module (crates-io))

(define-public crate-gv_video-0.1.7 (c (n "gv_video") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "texture2ddecoder") (r "^0.0.5") (d #t) (k 0)))) (h "1vdp3fryf656qyb73wdhp7aafqb7crqc69j6fvaiw58nn4zavbni")))

(define-public crate-gv_video-0.1.8 (c (n "gv_video") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "texture2ddecoder") (r "^0.0.5") (d #t) (k 0)))) (h "1krzw2v7q3yhgljj88i3phgkff2m1hcj0919zzv3cim4wqg09a8h")))

(define-public crate-gv_video-0.1.9 (c (n "gv_video") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "texture2ddecoder") (r "^0.0.5") (d #t) (k 0)))) (h "09mvbls68xx73h3lrmlqg2wj3c32845lfkmm7znwfjl3r82y0b8s")))

(define-public crate-gv_video-0.1.10 (c (n "gv_video") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "texture2ddecoder") (r "^0.0.5") (d #t) (k 0)))) (h "1njg38d10q8s7597c7kly68yjpzbplikp273c7afavj0a0lc8b5h") (f (quote (("unsafe") ("default" "unsafe"))))))

