(define-module (crates-io gv c- gvc-sys) #:use-module (crates-io))

(define-public crate-gvc-sys-0.1.0 (c (n "gvc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "regex") (r "^1.3.7") (d #t) (k 1)) (d (n "util") (r "^0.1.0") (d #t) (k 0)))) (h "0wp11cphwywz4k55spvdpjyblvszk1h1s422csh5vqkwrdvhwx4y") (y #t)))

(define-public crate-gvc-sys-0.1.1 (c (n "gvc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "regex") (r "^1.3.7") (d #t) (k 1)))) (h "0xkmf3w2n7aw3xv6cdxlqv27b4jijc7j9zr5nfzbhg8y6nkfjxxv")))

