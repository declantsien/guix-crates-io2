(define-module (crates-io az al azalea-ecs-macros) #:use-module (crates-io))

(define-public crate-azalea-ecs-macros-0.6.0 (c (n "azalea-ecs-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "1ncqczvq30d5xd56r8dndqr835z9vax7301nk2r41lgg1c0qi17m")))

