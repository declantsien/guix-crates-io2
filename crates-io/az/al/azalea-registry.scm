(define-module (crates-io az al azalea-registry) #:use-module (crates-io))

(define-public crate-azalea-registry-0.1.0 (c (n "azalea-registry") (v "0.1.0") (d (list (d (n "azalea-registry-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1a13gqzqjcwi9vpwydck04b389rrfy7f1d2wjaxcxzvwa1r0rnpk")))

(define-public crate-azalea-registry-0.2.0 (c (n "azalea-registry") (v "0.2.0") (d (list (d (n "azalea-buf") (r "^0.2.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.2.0") (d #t) (k 0)))) (h "19c11gwv8spygc1ggqc5jky4apgybrfky8w106mygdvjx7wxwyz2")))

(define-public crate-azalea-registry-0.3.0 (c (n "azalea-registry") (v "0.3.0") (d (list (d (n "azalea-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1piy8yf4cwz6inmr1rfzglkxnf6mj9a26hvhis20scibfh9h5m0n")))

(define-public crate-azalea-registry-0.4.0 (c (n "azalea-registry") (v "0.4.0") (d (list (d (n "azalea-buf") (r "^0.4.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1hvxicq1xj4002k3mvaiz2zh1blffrd6gqw36zrxxqyncb62ydzc")))

(define-public crate-azalea-registry-0.5.0 (c (n "azalea-registry") (v "0.5.0") (d (list (d (n "azalea-buf") (r "^0.5.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.5.0") (d #t) (k 0)))) (h "0k7wp25d5cpyjav17rp8kyq6x3hzkpmfrlc60ys4ciim1i464nyx")))

(define-public crate-azalea-registry-0.6.0 (c (n "azalea-registry") (v "0.6.0") (d (list (d (n "azalea-buf") (r "^0.6.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)))) (h "13qr56h56hh68m2hmjj9kmh87khnd4x0dkqdn8ax2bcsjp0sbfmz")))

(define-public crate-azalea-registry-0.7.0 (c (n "azalea-registry") (v "0.7.0") (d (list (d (n "azalea-buf") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07al2gcj4h35mmm8ipkhl6kwkcdp9yld7npxarq9hih0w40481b8") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "azalea-registry-macros/serde"))))))

(define-public crate-azalea-registry-0.8.0 (c (n "azalea-registry") (v "0.8.0") (d (list (d (n "azalea-buf") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "064gw7yjanjwl59zbp80bbflsc2d6jcda0pqrcwi49jcbqny57jp") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "azalea-registry-macros/serde"))))))

(define-public crate-azalea-registry-0.9.0 (c (n "azalea-registry") (v "0.9.0") (d (list (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.3") (d #t) (k 0)))) (h "11vy531i8mgzdfxpxwhl43js8xl9iqqzcs69h4w7f2bin3syz1zx") (f (quote (("serde" "azalea-registry-macros/serde") ("default" "serde"))))))

(define-public crate-azalea-registry-0.9.1 (c (n "azalea-registry") (v "0.9.1") (d (list (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.4") (d #t) (k 0)))) (h "02cbxmrz6bzs6dbjbzwr5qgwnlf0f1j8kss3mr9z8f6iai2snncp") (f (quote (("serde" "azalea-registry-macros/serde") ("default" "serde"))))))

(define-public crate-azalea-registry-0.10.0 (c (n "azalea-registry") (v "0.10.0") (d (list (d (n "azalea-buf") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-registry-macros") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.4") (d #t) (k 0)))) (h "0n55qcm0p4xhgcjsr1nwxc4wkbximac1q2h6ni2j2a440n85kl2f") (f (quote (("serde" "azalea-registry-macros/serde") ("default" "serde"))))))

