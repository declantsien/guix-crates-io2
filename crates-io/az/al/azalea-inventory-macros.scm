(define-module (crates-io az al azalea-inventory-macros) #:use-module (crates-io))

(define-public crate-azalea-inventory-macros-0.7.0 (c (n "azalea-inventory-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "11g649grplfj511jazsdpk77b59pd1h7l9d09x01kh92dnclp40y")))

(define-public crate-azalea-inventory-macros-0.8.0 (c (n "azalea-inventory-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0dw5bxlrqqqf0f2v55jwsh9ig2g5qx10kaxdg1jw4v3m8myxcn64")))

(define-public crate-azalea-inventory-macros-0.9.0 (c (n "azalea-inventory-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1js1xapiqxwv5iwg4d5r0jhmkxzzgk3i0ffifiq7x35h0cym0a6x")))

(define-public crate-azalea-inventory-macros-0.9.1 (c (n "azalea-inventory-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "029fkzqi34432r7sv504l018g6hbbx9ilws7wwijirliz9n803nn")))

(define-public crate-azalea-inventory-macros-0.10.0 (c (n "azalea-inventory-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1wgmgv4746sfmav4zfna320iiqm2xck811492ngfxwl1a3db1n05")))

