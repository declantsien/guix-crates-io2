(define-module (crates-io az al azalea-protocol-macros) #:use-module (crates-io))

(define-public crate-azalea-protocol-macros-0.1.0 (c (n "azalea-protocol-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "16gacxq087sf00y97scd5lciixg5sd7vidw9w5s0krkvp3nyxnf9")))

(define-public crate-azalea-protocol-macros-0.2.0 (c (n "azalea-protocol-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1cvg9h0nwck4ppj9b1ya5672xmq2sq3ph39kgg8yczsyska5k2jh")))

(define-public crate-azalea-protocol-macros-0.3.0 (c (n "azalea-protocol-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1k2v25wi8g3f982nv4m5qhhlkid3wk97p9k6yxdd6j7gq0q4nrb7")))

(define-public crate-azalea-protocol-macros-0.4.0 (c (n "azalea-protocol-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0bwig00x7c4gx9ss7s861f5mw9bippvj1gam86kn8z1wvnc9ysag")))

(define-public crate-azalea-protocol-macros-0.5.0 (c (n "azalea-protocol-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1frpa59bz6xainzfcbq4fr0wgx047qqpm5jffk6nfcgsz3i2f45l")))

(define-public crate-azalea-protocol-macros-0.6.0 (c (n "azalea-protocol-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0rchym0cdd7yx6q75zahb9wv832zphzgm2pvi7b8qkvafmhvhwwq")))

(define-public crate-azalea-protocol-macros-0.7.0 (c (n "azalea-protocol-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1jar38lhppzkwbn9yrkwqdgj1pbjgaqccf6rwk104mk1kzs0ql9z")))

(define-public crate-azalea-protocol-macros-0.8.0 (c (n "azalea-protocol-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "138y4wh4l5910sbmqzach437mq83sx07k9rv1kh0dyr8dfq8qciv")))

(define-public crate-azalea-protocol-macros-0.9.0 (c (n "azalea-protocol-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1mpwk7vpzzb1lh415islrpvwxgpbif8zjjf0166hn515j66dwbxz")))

(define-public crate-azalea-protocol-macros-0.9.1 (c (n "azalea-protocol-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "1pkgam11ywnnvxg1g7qdv7h7qz6snrp71bs3gdclmz2a6ql7kxra")))

(define-public crate-azalea-protocol-macros-0.10.0 (c (n "azalea-protocol-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "00pspa6zk1c48czsi8jb370d4sgc7f35b5n2j7dm9ahz7frq3cm7")))

