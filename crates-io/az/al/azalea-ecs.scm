(define-module (crates-io az al azalea-ecs) #:use-module (crates-io))

(define-public crate-azalea-ecs-0.6.0 (c (n "azalea-ecs") (v "0.6.0") (d (list (d (n "azalea-ecs-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (k 0)) (d (n "iyes_loopless") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time"))) (d #t) (k 0)))) (h "1bc041b5b3ssyfpkiy21aljll8zs28iqhmpsz06gl1xcx5l701vf")))

