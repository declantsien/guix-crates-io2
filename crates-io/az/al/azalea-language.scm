(define-module (crates-io az al azalea-language) #:use-module (crates-io))

(define-public crate-azalea-language-0.1.0 (c (n "azalea-language") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "12z71cw20qb54iacg3whrfyglgbz4kqhlzngpys0vihqab8kfhz5")))

(define-public crate-azalea-language-0.2.0 (c (n "azalea-language") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0y1jdlyksccs6cizva3mkc27lci458qnrv21sdgfzmc5ln6qzdc6")))

(define-public crate-azalea-language-0.3.0 (c (n "azalea-language") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1zpkqyfs4s5nwsir89jc4ppis9iyry7gsnqx4v9k9rnhrvgw9zwx")))

(define-public crate-azalea-language-0.4.0 (c (n "azalea-language") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0x5cbind6pd563y9c0zcv3kf44zwrg3dadvqnwyz5blg33qn7xm3")))

(define-public crate-azalea-language-0.5.0 (c (n "azalea-language") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "01zgjm8cjzqs7pprwxn5hhvh4pf3phl3bgdxn2n9a29k68ac4ii7")))

(define-public crate-azalea-language-0.6.0 (c (n "azalea-language") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1x2509jmmh3iaxrff80l54swfzxmph5709kv36a0s7a0x64qcxm3")))

(define-public crate-azalea-language-0.7.0 (c (n "azalea-language") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0addxc7kckx9i974zrwyp81r099l7z6jhdp7hcaa7607bwrrngnl")))

(define-public crate-azalea-language-0.8.0 (c (n "azalea-language") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1bgrhg0v4w0qlhl7csb4kd4kkf2cg6wgk6gj8ki89radx6vf8mi1")))

(define-public crate-azalea-language-0.9.0 (c (n "azalea-language") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "158sx721ik4sps0a5rnngww16dcf828pk0k9pwssh8fpxrgjsgjd")))

(define-public crate-azalea-language-0.9.1 (c (n "azalea-language") (v "0.9.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1xi4wbr8pql26xai8pylvz1fm0qz5cz6i17h4iva0rwjx3yj0wa5")))

(define-public crate-azalea-language-0.10.0 (c (n "azalea-language") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0qqdhz7fv9c6ng9d8qbs3lkzfg5c7dpd32w1g8a936s5n8ijmw8i")))

