(define-module (crates-io az al azalea-block) #:use-module (crates-io))

(define-public crate-azalea-block-0.1.0 (c (n "azalea-block") (v "0.1.0") (d (list (d (n "azalea-block-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1f4ipr411cbv905zljvvz59grlm8jspmb2h6ps1mygdyx2vb0syf")))

(define-public crate-azalea-block-0.2.0 (c (n "azalea-block") (v "0.2.0") (d (list (d (n "azalea-block-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.2.0") (d #t) (k 0)))) (h "1z3xnlcch64g1v8l3x3in4iqns0dkp3a7ai5i509yi5p8a53jl6h")))

(define-public crate-azalea-block-0.3.0 (c (n "azalea-block") (v "0.3.0") (d (list (d (n "azalea-block-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.3.0") (d #t) (k 0)))) (h "0cgpbbjp2ayl1pl6svgvfb1wmhbhk962iqixqv5gc33la1nc6izz")))

(define-public crate-azalea-block-0.4.0 (c (n "azalea-block") (v "0.4.0") (d (list (d (n "azalea-block-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.4.0") (d #t) (k 0)))) (h "1lq4gm2cl7gyvh7051dpk06id4hlcg7bgbiqpysgrk090fz3wg46")))

(define-public crate-azalea-block-0.5.0 (c (n "azalea-block") (v "0.5.0") (d (list (d (n "azalea-block-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.5.0") (d #t) (k 0)))) (h "09grw8mvr7xw577qwnbz1hn6xw4dm3s2bxykmb1igf4hf8fb0j7n")))

(define-public crate-azalea-block-0.6.0 (c (n "azalea-block") (v "0.6.0") (d (list (d (n "azalea-block-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.6.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.6.0") (d #t) (k 0)))) (h "11w2n9x1754vylf2wqwh8x6z0k4ldyrx9f45z0agq58xbn5aiash")))

(define-public crate-azalea-block-0.7.0 (c (n "azalea-block") (v "0.7.0") (d (list (d (n "azalea-block-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.7.0") (d #t) (k 0)))) (h "0xj6s9z4bdxf0qr2mww69396cvxyhzpxyh9pzgpblqkmxjqixy71")))

(define-public crate-azalea-block-0.8.0 (c (n "azalea-block") (v "0.8.0") (d (list (d (n "azalea-block-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.8.0") (d #t) (k 0)))) (h "1rybp6h432bxihv8s79c6706kcz92nja5jd0hz70m1lqycvm48dd")))

(define-public crate-azalea-block-0.9.0 (c (n "azalea-block") (v "0.9.0") (d (list (d (n "azalea-block-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.9.0") (d #t) (k 0)))) (h "12gd15shp5bh6szflb9yysy7hqx150cidb6a77y80mjvdr5gmvww")))

(define-public crate-azalea-block-0.9.1 (c (n "azalea-block") (v "0.9.1") (d (list (d (n "azalea-block-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.9.0") (d #t) (k 0)))) (h "00fb64b03xh6n124a81djlwfrk57ps9mixmwkdaqbxpivck1k602")))

(define-public crate-azalea-block-0.10.0 (c (n "azalea-block") (v "0.10.0") (d (list (d (n "azalea-block-macros") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-buf") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.10.0") (d #t) (k 0)))) (h "125hcl2b4hlschr66hxcb122ycpps61pnyw783lbi6167adb05pq")))

