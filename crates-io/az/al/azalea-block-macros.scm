(define-module (crates-io az al azalea-block-macros) #:use-module (crates-io))

(define-public crate-azalea-block-macros-0.1.0 (c (n "azalea-block-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1n9wkdliwk7nsmcaaah9a23an57xrsa20r076s5valsaxn265hky")))

(define-public crate-azalea-block-macros-0.2.0 (c (n "azalea-block-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "11k0xm6wq28vfggs4f0j25h5m2ji3wkr8iagiwqmrkq3dgdjmj78")))

(define-public crate-azalea-block-macros-0.3.0 (c (n "azalea-block-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "11smwmx6jqg50sl48ff4hbssy8cy72y18x756hryn83n1hz2mn15")))

(define-public crate-azalea-block-macros-0.4.0 (c (n "azalea-block-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0nrxdwawsypw7iwx596xmb0zv8kgqgcjxrg7nywfpgg5cz0qpnil")))

(define-public crate-azalea-block-macros-0.5.0 (c (n "azalea-block-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0vg8xra7h1z6fjkjmb1z7kz3x1hq9k1lrc8hy8hgkaqjwc0irhhj")))

(define-public crate-azalea-block-macros-0.6.0 (c (n "azalea-block-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1f3d1h9250y3hfigv4v9kkzg1nm4c4icyfc48gm690wppazly70l")))

(define-public crate-azalea-block-macros-0.7.0 (c (n "azalea-block-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0ckks2iqs0c5c2py2gg837wl6f6a4qgjjhml6cmfl39abi4z7n2p")))

(define-public crate-azalea-block-macros-0.8.0 (c (n "azalea-block-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "14dswvx5jpm30i8wjx8al8k89prf5i80cql02kn9lzl8b5hiil5b")))

(define-public crate-azalea-block-macros-0.9.0 (c (n "azalea-block-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "049y9zmqmx14w47pzk1w9058xln01hgpx3xi6b6xys8yhwbjrsh4")))

(define-public crate-azalea-block-macros-0.9.1 (c (n "azalea-block-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "0bm80n670l1wqdkjqipqcgnfz2izsgraz70waa0zd81671vrnayx")))

(define-public crate-azalea-block-macros-0.10.0 (c (n "azalea-block-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1c4wbz1pfxdlalxn1w4b2i731a8w2sqf053hcqv4v1qi1zdqyvvc")))

