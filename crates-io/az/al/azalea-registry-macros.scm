(define-module (crates-io az al azalea-registry-macros) #:use-module (crates-io))

(define-public crate-azalea-registry-macros-0.1.0 (c (n "azalea-registry-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1f1a0cxcv2da26yyjqpin9i9lybnndrbmcbb2m38s4czbwpj3rrl")))

(define-public crate-azalea-registry-macros-0.2.0 (c (n "azalea-registry-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0g6ab7svkshipm11z47ifrsqgvlammnnjdfvdbzacr3s6xgm65lj")))

(define-public crate-azalea-registry-macros-0.3.0 (c (n "azalea-registry-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0gi4c5jidjqkwfgapv4rlp01nn1japj3nh8nn824sw0p3fcgrhq5")))

(define-public crate-azalea-registry-macros-0.4.0 (c (n "azalea-registry-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0dcvh9c26syl77azrf1alf25an80m1yggpa4ilnnqhixaz9awpca")))

(define-public crate-azalea-registry-macros-0.5.0 (c (n "azalea-registry-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1n15kyp9fmvyg93mjc22ng9k4j13x3pzfgga008x1j294irjy6qc")))

(define-public crate-azalea-registry-macros-0.6.0 (c (n "azalea-registry-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "16nc1fcnnwss82rlmhahlzb5qx6niqqymj0sv837ssb153hm2847")))

(define-public crate-azalea-registry-macros-0.7.0 (c (n "azalea-registry-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1k4z2ixndh438gmhmxj1x1nm9kxqsk4igqvg02wya50ln730mjcy") (f (quote (("serde"))))))

(define-public crate-azalea-registry-macros-0.8.0 (c (n "azalea-registry-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0gl1m5kh3chyws8lgrm5giaqz18sccsglmfcl63qxyj2lgwqp0y9") (f (quote (("serde"))))))

(define-public crate-azalea-registry-macros-0.9.0 (c (n "azalea-registry-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0y3x726i7i0c9p2hd8q15md1pkrkl9bg39ym4hm7g3bd0mwcqhli") (f (quote (("serde"))))))

(define-public crate-azalea-registry-macros-0.9.1 (c (n "azalea-registry-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "07kxz7h8h9nxs4b4rlsic2bfjg529p5wsnhlk2dizxvf4q4kphln") (f (quote (("serde"))))))

(define-public crate-azalea-registry-macros-0.10.0 (c (n "azalea-registry-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0njc8dal1qjsg1a7psigx8scy0qpnx1n7kyd1jbmxg88zizb9rx6") (f (quote (("serde"))))))

