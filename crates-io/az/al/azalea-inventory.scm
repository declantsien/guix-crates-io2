(define-module (crates-io az al azalea-inventory) #:use-module (crates-io))

(define-public crate-azalea-inventory-0.7.0 (c (n "azalea-inventory") (v "0.7.0") (d (list (d (n "azalea-buf") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-inventory-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-nbt") (r "^0.7.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.7.0") (d #t) (k 0)))) (h "14db2xl3nin5h42l5vbm02gi3cww0gcamd0knab6fp50jiji5zv6")))

(define-public crate-azalea-inventory-0.8.0 (c (n "azalea-inventory") (v "0.8.0") (d (list (d (n "azalea-buf") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-inventory-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-nbt") (r "^0.8.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.8.0") (d #t) (k 0)))) (h "1yd665nzwj3xb3ln1ricsxss5b7jysv63c7ndyhxvabdv4p5sm8b")))

(define-public crate-azalea-inventory-0.9.0 (c (n "azalea-inventory") (v "0.9.0") (d (list (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-inventory-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.9.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.3") (d #t) (k 0)))) (h "01zhh31y7ysbrxnfrmm8qvbk988asg6dag38vvbhrd5wxmz8l9bi")))

(define-public crate-azalea-inventory-0.9.1 (c (n "azalea-inventory") (v "0.9.1") (d (list (d (n "azalea-buf") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-inventory-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.9.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.4") (d #t) (k 0)))) (h "1pnq2d59x2gaxbsrkwkqib0vvarrs2kgp9qdrkh42f5jmkhndnga")))

(define-public crate-azalea-inventory-0.10.1 (c (n "azalea-inventory") (v "0.10.1") (d (list (d (n "azalea-buf") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-chat") (r "^0.10.0") (f (quote ("azalea-buf"))) (d #t) (k 0)) (d (n "azalea-core") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-inventory-macros") (r "^0.10.0") (d #t) (k 0)) (d (n "azalea-registry") (r "^0.10.0") (d #t) (k 0)) (d (n "simdnbt") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "00zs2an8f4gcaafqbssdcvjmvgqvslxlg82b3p88p5h5qlcbk9xa")))

