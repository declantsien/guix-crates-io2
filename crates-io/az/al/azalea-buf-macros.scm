(define-module (crates-io az al azalea-buf-macros) #:use-module (crates-io))

(define-public crate-azalea-buf-macros-0.1.0 (c (n "azalea-buf-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pgxlya6kg2yyhnszrcn4jhr8yhhyyyha0x0ijrqmmw3jl0lknf5")))

(define-public crate-azalea-buf-macros-0.2.0 (c (n "azalea-buf-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0y59vxch7i759aja9mjyaiiv7nx6m7azh30h0wsi8n0nij13djc5")))

(define-public crate-azalea-buf-macros-0.3.0 (c (n "azalea-buf-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vcm1fvw2i257yf59gh17hnmvc1nc5r12w0il8qr96mlab1nwgkq")))

(define-public crate-azalea-buf-macros-0.4.0 (c (n "azalea-buf-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10cxrq7yz0sg1jn443rjk2ka9y6jiv2jvk03m6vi9p31k3wc81a9")))

(define-public crate-azalea-buf-macros-0.5.0 (c (n "azalea-buf-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l9rw6ksmp8x09c24sm4y2rh9ihz10n0z5rhxfsaw3c1wi1bf0n0")))

(define-public crate-azalea-buf-macros-0.6.0 (c (n "azalea-buf-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rmbgg2qkc22dva1p5api61rawljzr1g5bhrflx67dhg7dn4mgpd")))

(define-public crate-azalea-buf-macros-0.7.0 (c (n "azalea-buf-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hlb265n82xars0zxisc4sfyagw9sdpzrbj77b35qb2q3k3sg70h")))

(define-public crate-azalea-buf-macros-0.8.0 (c (n "azalea-buf-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11zpi2cw3gyksxjb31fbn9q05rf7n804bf1b54dpqjp2i6wg6wzq")))

(define-public crate-azalea-buf-macros-0.9.0 (c (n "azalea-buf-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lhbxidlj6jxp0agc5xhgc7byq7m5j31b5midps091fic9mzgckp")))

(define-public crate-azalea-buf-macros-0.9.1 (c (n "azalea-buf-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mqjd372z9wkdkg0zgksglkaacfxafnjczci10hc1pqi059xx7wx")))

(define-public crate-azalea-buf-macros-0.10.0 (c (n "azalea-buf-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dmzhjjrnhs7lc155cpmihqgdc5i1icpbbix9wcmdlfacac21pl0")))

