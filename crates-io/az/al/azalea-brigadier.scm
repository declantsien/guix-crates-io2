(define-module (crates-io az al azalea-brigadier) #:use-module (crates-io))

(define-public crate-azalea-brigadier-0.1.0 (c (n "azalea-brigadier") (v "0.1.0") (h "15sbcf070n9z6816801k1rxzq5qsy94kvwc1kazhr2pmck8vz9ii")))

(define-public crate-azalea-brigadier-0.2.0 (c (n "azalea-brigadier") (v "0.2.0") (h "181md9z9bf5w0zd4fl64i4pzkshv16s2185mshqn7npxsnmkfxsp")))

(define-public crate-azalea-brigadier-0.3.0 (c (n "azalea-brigadier") (v "0.3.0") (h "1ikma5gdgf6knmlj8z4axx3ijcxqldnhk86x2rhax6m6ibw81n4y")))

(define-public crate-azalea-brigadier-0.4.0 (c (n "azalea-brigadier") (v "0.4.0") (h "0ihjd4hdqckp0b1rz3bjz3skffszcba71m730nw48sd02grz95va")))

(define-public crate-azalea-brigadier-0.5.0 (c (n "azalea-brigadier") (v "0.5.0") (d (list (d (n "azalea-buf") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "15gg86ixb60zin1gvf4g2rqwvrvmrx5s7x20yk30sx3bm9djakw5") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.6.0 (c (n "azalea-brigadier") (v "0.6.0") (d (list (d (n "azalea-buf") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "01z8q9xvwb29aixgmfmncxff6y2mx7zjqy3r9sk9bbanx5y972h9") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.7.0 (c (n "azalea-brigadier") (v "0.7.0") (d (list (d (n "azalea-buf") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1xx1zifmqk9zaap5aj2px7rpy8zrwxrnphxnq2swsy0wip54bvvw") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.8.0 (c (n "azalea-brigadier") (v "0.8.0") (d (list (d (n "azalea-buf") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0nmfdfr524vcyz4lkycwjlqmfa0vpvi4km5sdx34ldgxp71nzzaj") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.9.0 (c (n "azalea-brigadier") (v "0.9.0") (d (list (d (n "azalea-buf") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0zy4jfdn236z91nqimxg2xx9350shxz13xa363hg18ffyfdbncmx") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.9.1 (c (n "azalea-brigadier") (v "0.9.1") (d (list (d (n "azalea-buf") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1k6qdricxv7030pkj82aw9i8md7mzy2nvfvy72prn4svbysv271b") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

(define-public crate-azalea-brigadier-0.10.0 (c (n "azalea-brigadier") (v "0.10.0") (d (list (d (n "azalea-buf") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "azalea-chat") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0vqbry9mx7ip6m7y0w2siybb3crlqjhgv45d8mwlr90skkzh37q1") (s 2) (e (quote (("azalea-buf" "dep:azalea-buf" "dep:azalea-chat"))))))

