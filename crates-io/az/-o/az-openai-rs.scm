(define-module (crates-io az -o az-openai-rs) #:use-module (crates-io))

(define-public crate-az-openai-rs-0.1.0 (c (n "az-openai-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1ymlbdy617wl03dc25ld68rn5zqp7p333plryaf9vgg1xxvjnb7p")))

