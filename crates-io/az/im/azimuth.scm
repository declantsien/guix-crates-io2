(define-module (crates-io az im azimuth) #:use-module (crates-io))

(define-public crate-azimuth-0.0.0 (c (n "azimuth") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)))) (h "16agagfjjwr8k3pisn67kq5ng75liz52i5h6rzba45cpmzg38l7w") (y #t)))

(define-public crate-azimuth-0.0.1 (c (n "azimuth") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wxdy8bws89aj02zpqsg6sisw32k35sm2xnf8zp0hcgi6is7i8ip") (y #t)))

(define-public crate-azimuth-0.0.2-azimuth (c (n "azimuth") (v "0.0.2-azimuth") (h "10nlj0l8an8yvl1wxjf5zz5i4vh2s6f9xl8ascyhgqb01a6175rp") (y #t)))

(define-public crate-azimuth-0.0.0-- (c (n "azimuth") (v "0.0.0--") (h "0ddf5x2bgzjsr75h9wcfn8k34si6hr0kmpkjj6ypn5x7qkq7h77y") (y #t)))

