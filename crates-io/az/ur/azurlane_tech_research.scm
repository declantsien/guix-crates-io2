(define-module (crates-io az ur azurlane_tech_research) #:use-module (crates-io))

(define-public crate-azurlane_tech_research-0.1.0 (c (n "azurlane_tech_research") (v "0.1.0") (h "0asflld99j8lbcawblkjpxsi758p487rv7d46m7yrnpkwj58qd06")))

(define-public crate-azurlane_tech_research-0.1.1 (c (n "azurlane_tech_research") (v "0.1.1") (h "01nmzp6rpzz480xxqdp691gay2mxpfxk2hpkk3zcwyqvcv30siss")))

(define-public crate-azurlane_tech_research-0.1.2 (c (n "azurlane_tech_research") (v "0.1.2") (h "19knngk53g27p94lwhbx7ba88bapf02p6cpxz83c09i04wrk0ank")))

(define-public crate-azurlane_tech_research-0.2.0 (c (n "azurlane_tech_research") (v "0.2.0") (h "0dzlzrk98yvysj6rg3jh6bpysssb0q8znyfrimngalxqjiicb56d")))

(define-public crate-azurlane_tech_research-0.2.1 (c (n "azurlane_tech_research") (v "0.2.1") (h "0a5cxdz4326g4iyc51imwfpw870bw6jy4iyq29x8gdczvnvxl6mg")))

