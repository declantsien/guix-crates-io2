(define-module (crates-io az ur azur) #:use-module (crates-io))

(define-public crate-azur-0.1.0 (c (n "azur") (v "0.1.0") (d (list (d (n "lx") (r "^0.1.0") (d #t) (k 0)))) (h "1jqyf91qsnp9j5qx088vys87plwh3xgsqa56qknn7pgqc8nm1laj")))

(define-public crate-azur-0.3.0 (c (n "azur") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "lx") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0jd7skyahq89n9kphvjlvm4ci3nm5qci38xc23f9dw11nv00104p") (f (quote (("std"))))))

(define-public crate-azur-0.3.1 (c (n "azur") (v "0.3.1") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "lx") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0imz5ccdcn38y2zxid00xymh1l95l8fr1ci9dpkhjga3rsnz4dzs") (f (quote (("std"))))))

