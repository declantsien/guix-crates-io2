(define-module (crates-io az ur azure-iot-rs) #:use-module (crates-io))

(define-public crate-azure-iot-rs-0.1.0 (c (n "azure-iot-rs") (v "0.1.0") (d (list (d (n "azure-iot-rs-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "161rqljjjps0fwjay1kr4v0kjz9mmxrcxmmwq3ihi13w0apz4drp")))

(define-public crate-azure-iot-rs-0.2.0 (c (n "azure-iot-rs") (v "0.2.0") (d (list (d (n "azure-iot-rs-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0d73f8qs3jm4wnb77405jfa4f5k2gismij7np3vk4g6m0np1zl8y")))

(define-public crate-azure-iot-rs-0.3.0 (c (n "azure-iot-rs") (v "0.3.0") (d (list (d (n "azure-iot-rs-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1fk3sn9b5l3plnzyjfld2hi1zxfp5plh3i67gvadldbh2sms3ngy")))

(define-public crate-azure-iot-rs-0.4.0 (c (n "azure-iot-rs") (v "0.4.0") (d (list (d (n "azure-iot-rs-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0s01lsq68p2m0412xs4cykkrn3mlbb7c87ykxk0q89drmncj2zqj")))

