(define-module (crates-io az ur azure_password_sprayer) #:use-module (crates-io))

(define-public crate-azure_password_sprayer-0.1.0 (c (n "azure_password_sprayer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qz6dh2pvg257x1n7y3fd5hmn4zw5ijqr2sc7141aaksilpv3wsa")))

