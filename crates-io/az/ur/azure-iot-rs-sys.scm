(define-module (crates-io az ur azure-iot-rs-sys) #:use-module (crates-io))

(define-public crate-azure-iot-rs-sys-0.1.0 (c (n "azure-iot-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "10aa8yckdyjpx390f3wd3cm19w96c16g5jcx5sxpxl0s0ddkvh00")))

(define-public crate-azure-iot-rs-sys-0.1.1 (c (n "azure-iot-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0fpssqyii0g6ccyqqza1bjkj3lr5fmgzas9gnqzsx6zp0vy7mci4")))

(define-public crate-azure-iot-rs-sys-0.1.2 (c (n "azure-iot-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "16wsnabvbnig4c79lvv12iagiy32x2f77qninkzdqvv4wdzhvdar")))

(define-public crate-azure-iot-rs-sys-0.1.3 (c (n "azure-iot-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "16ay76vypj7r1440mrj1ggqhy4appcgmida7prgdyz4hnqc6s0za")))

(define-public crate-azure-iot-rs-sys-0.1.4 (c (n "azure-iot-rs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "152ajf349ja1bbg7wz6rslr4ybq5p6gsg02ks0q19ihqi93k6bdb")))

(define-public crate-azure-iot-rs-sys-0.1.5 (c (n "azure-iot-rs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1wyy7gyx6pkfxkymx0zz036q5hwvv9y5jjfd27g064mxhb7amqf1")))

(define-public crate-azure-iot-rs-sys-0.1.6 (c (n "azure-iot-rs-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0b9nsxl11bdv2p04slr38pr2jjdjl5z2wag8j09shv970nxrmsnr")))

(define-public crate-azure-iot-rs-sys-0.1.7 (c (n "azure-iot-rs-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0dbn509hq6w75945q55b5l33976zkwkdz9z66yz1g9wabf0njhbv")))

(define-public crate-azure-iot-rs-sys-0.1.8 (c (n "azure-iot-rs-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "003jhjr0r4licr03qpds1hbsi4yszkkxjjc3yhpaxjiv6pncvy7s") (f (quote (("prov_client") ("mqtt") ("http") ("default" "amqp" "mqtt" "http" "prov_client") ("amqp"))))))

