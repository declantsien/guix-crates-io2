(define-module (crates-io az ur azure-functions-shared-codegen) #:use-module (crates-io))

(define-public crate-azure-functions-shared-codegen-0.1.5 (c (n "azure-functions-shared-codegen") (v "0.1.5") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19zzkmlm8rhfrw59qwygvqaar7vrjmk5y4fnp8c6j98ilssacib2")))

(define-public crate-azure-functions-shared-codegen-0.1.6 (c (n "azure-functions-shared-codegen") (v "0.1.6") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0c7pj5z37n6jvr4xdgy8xyf7qzfhl4qb5925f177aj5jzazlki0y")))

(define-public crate-azure-functions-shared-codegen-0.2.0 (c (n "azure-functions-shared-codegen") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05f873syrkymglrq4y2w6nmggf1rl44rlb9yx0zq2x31w65yakg8")))

(define-public crate-azure-functions-shared-codegen-0.2.1 (c (n "azure-functions-shared-codegen") (v "0.2.1") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10q4q1k0362q935bij27l7svrwrs0jk6sw44jyn4md4bi6zbs2vy")))

(define-public crate-azure-functions-shared-codegen-0.2.2 (c (n "azure-functions-shared-codegen") (v "0.2.2") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1wchrwjnzvvpiwyfx68dg0imywa3bb9m4j4bc3xkshfsqhl110y6")))

(define-public crate-azure-functions-shared-codegen-0.2.3 (c (n "azure-functions-shared-codegen") (v "0.2.3") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0d079is0axpmp732wdjlwr48n1wi2ja6wzm8axc7x1jvjszy38hq")))

(define-public crate-azure-functions-shared-codegen-0.2.4 (c (n "azure-functions-shared-codegen") (v "0.2.4") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "05v2c98lnrl55l5yzrdsxskcd7mcs8bazrg06y24cnibmj6q21fi")))

(define-public crate-azure-functions-shared-codegen-0.2.5 (c (n "azure-functions-shared-codegen") (v "0.2.5") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1nvp3cndqfgcgg40zy7vd5wz1660jfmjs7gyg9y4vlg6zmx5siww")))

(define-public crate-azure-functions-shared-codegen-0.2.6 (c (n "azure-functions-shared-codegen") (v "0.2.6") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0c91bkahfn4p630d2h6rf8q297q8cwc249hln7n1f0ci01pgzf04")))

(define-public crate-azure-functions-shared-codegen-0.3.0 (c (n "azure-functions-shared-codegen") (v "0.3.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1hr7ws9n0qvdsrgvkhgbpwrmc3v4h3mwlacv3i8svlgisn22dppb")))

(define-public crate-azure-functions-shared-codegen-0.4.0 (c (n "azure-functions-shared-codegen") (v "0.4.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1nmap69zxk1690mg0fh2vqnnhsrl252fm3hmyx0f8ni4pnkgcsjw")))

(define-public crate-azure-functions-shared-codegen-0.4.1 (c (n "azure-functions-shared-codegen") (v "0.4.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1rllss0zcz6avh73078618mxw3gzbq0ic9qknz4npp8qmrg7cgsn")))

(define-public crate-azure-functions-shared-codegen-0.5.0 (c (n "azure-functions-shared-codegen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "16xnjr38zjkmlkqpsn7fpxhfr1pn2na9qxfz5qszl9djzsf1y7hg") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.5.1 (c (n "azure-functions-shared-codegen") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0cb15cz4fkl3wihlxa1jdlhvh9wycspj94g6pra53nh5j623nzqr") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.5.2 (c (n "azure-functions-shared-codegen") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0fs57fgxwkf031pvbz5mfrjqbagpgx06qygzdwm8kka8h132xjyp") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.6.0 (c (n "azure-functions-shared-codegen") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1p20af31rjnvdr5zq5pc301d9fqwcw5bwc1i3sgfg5kba9fpypbg") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.7.0 (c (n "azure-functions-shared-codegen") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1rnisfr0jk6grsnxlrqn9a3f10mpd97hdahv7jx2g0linkc2csm8") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.8.0 (c (n "azure-functions-shared-codegen") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxxw86cl132abhhdza2zywkdl3pi8pzkfgb11792dfj62clva5d") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.8.1 (c (n "azure-functions-shared-codegen") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0nh6234xcjw0rkwia34gzy5q7h6i0xaq90y0wjckxdwskiybs7km") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.9.0 (c (n "azure-functions-shared-codegen") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0gq7q9dnf3bkx1zhx8f1falxypivz18lz4xlnyxh5k5pbksyf42n") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.10.0 (c (n "azure-functions-shared-codegen") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0i9j19ma39a117wmc6pm9lyk8najkhp39zspm89z1z61y64s516p") (f (quote (("unstable"))))))

(define-public crate-azure-functions-shared-codegen-0.11.0 (c (n "azure-functions-shared-codegen") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0jjs6d8q7x0pvj8chzm2ya814x3g4lm7rzgfgw1pdd3i55pz7lfm") (f (quote (("unstable"))))))

