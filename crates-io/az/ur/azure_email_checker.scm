(define-module (crates-io az ur azure_email_checker) #:use-module (crates-io))

(define-public crate-azure_email_checker-0.1.1 (c (n "azure_email_checker") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w9np2fiwv13sjsivrkzys25fzrgb4hy7cipwdrm94dy1b9hb16q")))

