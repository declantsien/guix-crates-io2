(define-module (crates-io az ur azure_blob_uploader) #:use-module (crates-io))

(define-public crate-azure_blob_uploader-0.1.0 (c (n "azure_blob_uploader") (v "0.1.0") (d (list (d (n "azure_storage") (r "^0.17.0") (d #t) (k 0)) (d (n "azure_storage_blobs") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19h3h2jd0h123qkjpv988vakm2h3bgshpy9cbb74xryh3g03w3bj")))

(define-public crate-azure_blob_uploader-0.1.1 (c (n "azure_blob_uploader") (v "0.1.1") (d (list (d (n "azure_storage") (r "^0.17.0") (d #t) (k 0)) (d (n "azure_storage_blobs") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0036h12kxryarmpymwj7g7nk296pjvs7m9fs1pkj0hgg2f3l77ry")))

(define-public crate-azure_blob_uploader-0.1.2 (c (n "azure_blob_uploader") (v "0.1.2") (d (list (d (n "azure_storage") (r "^0.17.0") (d #t) (k 0)) (d (n "azure_storage_blobs") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lba6mj25az330bihnpqmxfw2gklw2f1bramjja9prjljjvg2f7y")))

(define-public crate-azure_blob_uploader-0.1.3 (c (n "azure_blob_uploader") (v "0.1.3") (d (list (d (n "azure_storage") (r "^0.17.0") (d #t) (k 0)) (d (n "azure_storage_blobs") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fjmri7vgs76nb8m0dciwnzh95bd5l00g3a90gzd9j1ix3w5hq7r")))

(define-public crate-azure_blob_uploader-0.1.4 (c (n "azure_blob_uploader") (v "0.1.4") (d (list (d (n "azure_storage") (r "^0.17.0") (d #t) (k 0)) (d (n "azure_storage_blobs") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y545nr0bkk0zwqiwbjv4lgwvjaq875w7l7paa33y58bf7fni8mi")))

