(define-module (crates-io az ur azurlane) #:use-module (crates-io))

(define-public crate-azurlane-1.0.0 (c (n "azurlane") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g9rn3ycj3027wz1vczww89nm3yi074xk1ahpaqrq8qxa6hdf49f")))

(define-public crate-azurlane-1.1.0 (c (n "azurlane") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k75j2ibciw544icplcxxr2wb6ys5myijvnwvphaicpwkk4j7g27")))

