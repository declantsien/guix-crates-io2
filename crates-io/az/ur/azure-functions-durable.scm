(define-module (crates-io az ur azure-functions-durable) #:use-module (crates-io))

(define-public crate-azure-functions-durable-0.11.0 (c (n "azure-functions-durable") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "hyper") (r "^0.13.0-alpha.4") (f (quote ("unstable-stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "mockito") (r "^0.19.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "14acpy1zimmkzjks9b2hq2bwk8m8zcxbkkrlini508079x52jzkf")))

