(define-module (crates-io az ur azure_app_config) #:use-module (crates-io))

(define-public crate-azure_app_config-0.1.0 (c (n "azure_app_config") (v "0.1.0") (d (list (d (n "azure_core") (r "^0.20.0") (d #t) (k 0)) (d (n "azure_identity") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1z4rp9vwb3vbgmajz4ki99vjs2bxf19b9fs66818c4apw9jh2148")))

(define-public crate-azure_app_config-0.1.1 (c (n "azure_app_config") (v "0.1.1") (d (list (d (n "azure_core") (r "^0.20.0") (d #t) (k 0)) (d (n "azure_identity") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w9dqbc2b1lf14gka4fpcal5nhm3nnbdj463mkh4k1hqp87afalr")))

