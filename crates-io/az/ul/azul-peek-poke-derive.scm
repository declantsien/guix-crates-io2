(define-module (crates-io az ul azul-peek-poke-derive) #:use-module (crates-io))

(define-public crate-azul-peek-poke-derive-0.2.1 (c (n "azul-peek-poke-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0y0i6raak7z56d2m6pg65wkgrq1i5frifjgficpswac62hnw6yhm")))

