(define-module (crates-io az ul azul-core) #:use-module (crates-io))

(define-public crate-azul-core-0.0.1 (c (n "azul-core") (v "0.0.1") (d (list (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css-parser") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "gleam") (r "^0.11.0") (d #t) (k 0)))) (h "036xvz8dxdfy1zkpg2ighvram539l24f0p823pgh5z84k0wp0c1r") (f (quote (("css_parser" "azul-css-parser"))))))

(define-public crate-azul-core-0.0.2 (c (n "azul-core") (v "0.0.2") (d (list (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css-parser") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "gleam") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "1d8iymkggs91hdcl5xdg7bdp5d94d1swwi8w7kbl178hsvgskki1") (f (quote (("opengl" "gleam") ("default" "opengl") ("css_parser" "azul-css-parser"))))))

