(define-module (crates-io az ul azul-peek-poke) #:use-module (crates-io))

(define-public crate-azul-peek-poke-0.2.0 (c (n "azul-peek-poke") (v "0.2.0") (d (list (d (n "euclid") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "peek-poke-derive") (r "^0.2") (o #t) (d #t) (k 0) (p "azul-peek-poke-derive")))) (h "1iy53z6fkg0m26mf2kk37p45mh6jrz0i3zczd11qzh3m23yxgsn9") (f (quote (("extras" "derive" "euclid") ("derive" "peek-poke-derive") ("default" "derive"))))))

