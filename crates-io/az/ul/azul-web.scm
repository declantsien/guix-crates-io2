(define-module (crates-io az ul azul-web) #:use-module (crates-io))

(define-public crate-azul-web-0.0.1 (c (n "azul-web") (v "0.0.1") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)))) (h "1l6bnjd0y78ckhl8px5llhbqfphh50h45vr11r088612nx0jbsgj")))

(define-public crate-azul-web-0.0.2 (c (n "azul-web") (v "0.0.2") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)))) (h "13cvcg51yb506gscmccy26aiqlmwnnhjnqh6fnwcvzlgl44pv3fz")))

(define-public crate-azul-web-0.0.3 (c (n "azul-web") (v "0.0.3") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)))) (h "0nqdlp8i78df10vwc3lsanjbglrkzh7m9izqs7dybkl2d5wfid3q")))

(define-public crate-azul-web-0.0.4 (c (n "azul-web") (v "0.0.4") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)))) (h "1qiq86xz5cd888giq9qa3cjdkqj462p8al623p33pj14pd6fxwfn")))

(define-public crate-azul-web-0.0.5 (c (n "azul-web") (v "0.0.5") (d (list (d (n "azul-core") (r "^0.0.2") (f (quote ("opengl"))) (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)))) (h "0dkxldscvp0cpc320ya0wnlh4ny0k8zl2xdzfgz2a8zhw6gdkivk")))

