(define-module (crates-io az ul azul) #:use-module (crates-io))

(define-public crate-azul-0.1.0 (c (n "azul") (v "0.1.0") (h "0n47ns3lppydwp247zdvcp3mazjswf1q5j7nr7khic70q1c4xyqq")))

(define-public crate-azul-1.0.0-alpha1 (c (n "azul") (v "1.0.0-alpha1") (h "1pxywqp4qlmicaz7w4m6h6i56dipyrbq3m6f9gl07ybmhb2r3crs") (l "azul")))

(define-public crate-azul-1.0.0-alpha2 (c (n "azul") (v "1.0.0-alpha2") (h "1bzlh73zql3y04xhlh9nz9gmgszn4kzlkgspdxxssr3z140vn0iz") (f (quote (("docs_rs")))) (l "azul")))

(define-public crate-azul-1.0.0-alpha3 (c (n "azul") (v "1.0.0-alpha3") (h "046nbw7da7wn9kjrrc72b2nfvcqgab7zfqvwhzj3vd11ck1yvxk2") (f (quote (("docs_rs")))) (l "azul")))

(define-public crate-azul-1.0.0-alpha4 (c (n "azul") (v "1.0.0-alpha4") (h "19zhakrswlni7wrph4q7g208lrdcbgy9s1vi3adira0bcxxfl2nj") (f (quote (("docs_rs")))) (l "azul")))

