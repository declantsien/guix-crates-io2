(define-module (crates-io az ul azul-simplecss) #:use-module (crates-io))

(define-public crate-azul-simplecss-0.1.0 (c (n "azul-simplecss") (v "0.1.0") (h "1vhp0mmiqfvm3vl136cay7xv164dq5d8hjf23xvf1f2ki4xmzvr7")))

(define-public crate-azul-simplecss-0.1.1 (c (n "azul-simplecss") (v "0.1.1") (h "1b3ll1wn3z6r7253qjbypvibibdlv7fmvcfr36yss4vlhpgvy0y3")))

