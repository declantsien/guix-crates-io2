(define-module (crates-io az ul azul-webrender-build) #:use-module (crates-io))

(define-public crate-azul-webrender-build-0.0.1 (c (n "azul-webrender-build") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mnbxmjk551spz43a5wz9c16vjpjjxzf0p89cz9api59jwbxg6w6") (f (quote (("serialize_program" "serde"))))))

(define-public crate-azul-webrender-build-0.0.2 (c (n "azul-webrender-build") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0dv8fwv6x293j5m0xrsyq2virhvgcnsb2mbll8ahflyk756kmdlp") (f (quote (("serialize_program" "serde"))))))

