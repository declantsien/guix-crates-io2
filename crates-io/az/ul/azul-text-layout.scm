(define-module (crates-io az ul azul-text-layout) #:use-module (crates-io))

(define-public crate-azul-text-layout-0.0.1 (c (n "azul-text-layout") (v "0.0.1") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)) (d (n "freetype") (r "^0.4.1") (k 0)) (d (n "harfbuzz-sys") (r "^0.3.4") (f (quote ("build-native-harfbuzz" "build-native-freetype"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (k 0)))) (h "0k032vm4rf5q29xg2slr60yqi2xamjpr5v676wvfgmny9n0vmf46")))

(define-public crate-azul-text-layout-0.0.2 (c (n "azul-text-layout") (v "0.0.2") (d (list (d (n "azul-core") (r "^0.0.1") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)) (d (n "freetype") (r "^0.4.1") (k 0)) (d (n "harfbuzz-sys") (r "^0.3.4") (f (quote ("build-native-harfbuzz" "build-native-freetype"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (k 0)))) (h "0a4n3z0la7ji6nnsrvd9jfw9hxjcj9c0x4rmwn36v8h3q2lx03b3")))

(define-public crate-azul-text-layout-0.0.3 (c (n "azul-text-layout") (v "0.0.3") (d (list (d (n "azul-core") (r "^0.0.2") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)) (d (n "freetype") (r "^0.4.1") (k 0)) (d (n "harfbuzz-sys") (r "^0.3.4") (f (quote ("build-native-harfbuzz" "build-native-freetype"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (k 0)))) (h "14f3w3q0xl357g52alfv076gkrqwz9l9364rsvvsng43g5l1xnza")))

(define-public crate-azul-text-layout-0.0.4 (c (n "azul-text-layout") (v "0.0.4") (d (list (d (n "azul-core") (r "^0.0.2") (k 0)) (d (n "azul-css") (r "^0.0.1") (k 0)) (d (n "freetype") (r "^0.4.1") (k 0)) (d (n "harfbuzz-sys") (r "^0.3.4") (f (quote ("build-native-harfbuzz" "build-native-freetype"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (k 0)))) (h "1giqdcjpfi1f66q4zrpm72r6fp25fp8wq0h7agfqqzkr7nwr1wi0")))

