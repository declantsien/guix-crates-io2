(define-module (crates-io az ul azul-layout) #:use-module (crates-io))

(define-public crate-azul-layout-0.0.1 (c (n "azul-layout") (v "0.0.1") (d (list (d (n "azul-core") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-text-layout") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "05h0lz0qdknxfqqdxdxhkwzh5zrij96lr6p2si0bh26fay2cb16i") (f (quote (("text_layout" "azul-text-layout") ("default"))))))

(define-public crate-azul-layout-0.0.2 (c (n "azul-layout") (v "0.0.2") (d (list (d (n "azul-core") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css-parser") (r "^0.0.1") (d #t) (k 2)) (d (n "azul-text-layout") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "azulc") (r "^0.0.1") (d #t) (k 2)))) (h "0q3412qwi4l1irx1d9wyfipyh204n0j7wz74g7f8lr05bimlw1x8") (f (quote (("text_layout" "azul-text-layout") ("default"))))))

(define-public crate-azul-layout-0.0.3 (c (n "azul-layout") (v "0.0.3") (d (list (d (n "azul-core") (r "^0.0.2") (f (quote ("opengl"))) (k 0)) (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css-parser") (r "^0.0.1") (d #t) (k 2)) (d (n "azul-text-layout") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "azulc") (r "^0.0.1") (d #t) (k 2)))) (h "08frd2ik5sfaq7981rmcspc4hnfnam9cvspbhrq6a9anyfbbcj28") (f (quote (("text_layout" "azul-text-layout") ("default"))))))

(define-public crate-azul-layout-0.0.4 (c (n "azul-layout") (v "0.0.4") (d (list (d (n "azul-core") (r "^0.0.2") (f (quote ("opengl"))) (k 0)) (d (n "azul-css") (r "^0.0.1") (d #t) (k 0)) (d (n "azul-css-parser") (r "^0.0.1") (d #t) (k 2)) (d (n "azul-text-layout") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "azulc") (r "^0.0.1") (d #t) (k 2)))) (h "1pffh55s1yl28xhksvar6vpi0vzxdvnvzhcpd75sfvjvwn85rrx8") (f (quote (("text_layout" "azul-text-layout") ("default"))))))

