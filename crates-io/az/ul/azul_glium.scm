(define-module (crates-io az ul azul_glium) #:use-module (crates-io))

(define-public crate-azul_glium-0.22.0 (c (n "azul_glium") (v "0.22.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "genmesh") (r "^0.5") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9") (d #t) (k 1)) (d (n "glutin") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "obj") (r "^0.8") (f (quote ("genmesh"))) (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1r3zg48sibvx6cn6iq7k6b6v3np8vxmib5pm8iy5w6182fcd1vnf") (f (quote (("unstable") ("test_headless") ("icon_loading" "glutin/icon_loading") ("default" "glutin"))))))

