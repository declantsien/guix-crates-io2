(define-module (crates-io az am azamcodec) #:use-module (crates-io))

(define-public crate-azamcodec-0.1.0 (c (n "azamcodec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "16fr4216l0q3wfjrns33h9qq1f903h45a95wyxcplgqq2f8hcr57")))

(define-public crate-azamcodec-0.1.1 (c (n "azamcodec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0jlmw9qgfdvh0rnyrnxygjsq2v91l8nrq6blsnq41sly8f3pzvqj")))

(define-public crate-azamcodec-0.1.2 (c (n "azamcodec") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0jx4sjxg6866wvrhvcidajyh34sqnxbr5haiwaawd6xchdnx8vny")))

(define-public crate-azamcodec-0.1.3 (c (n "azamcodec") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1rcnhyz68v153graj9c6n8hwii7qdjjl9467qsjbn41b947cjs58")))

(define-public crate-azamcodec-0.1.4 (c (n "azamcodec") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0flpsqvxsyw447q0a4q4ck15dszb4ax2w079p39wm6zfscvkfwsq")))

