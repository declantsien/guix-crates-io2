(define-module (crates-io az #{65}# az65) #:use-module (crates-io))

(define-public crate-az65-0.1.0 (c (n "az65") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z7am0y7czdn662g7ygvivvi6mspz7nkzwlpam4wrr9j4hrmpy9s")))

(define-public crate-az65-0.1.1 (c (n "az65") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jif90gafa0sc63f8y3wadlqbs7a4i3dksm80yygx3v1shc1pq8j")))

(define-public crate-az65-0.1.2 (c (n "az65") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ch5l38wxdv4ndcr6hdy9khkw64nfvb0dkghpkl867mda6b0scnk")))

(define-public crate-az65-0.1.3 (c (n "az65") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wii8cwwipb9px2i769il8wzyw2bs00qvfi39c4fpm58lfxgsd7p")))

(define-public crate-az65-0.1.4 (c (n "az65") (v "0.1.4") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b4vry7i6dq1916jh4piqlicg646p51dd5yg53cqrz2j35fhch45")))

(define-public crate-az65-0.1.5 (c (n "az65") (v "0.1.5") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m2y4w4vdjz7cv0n2382bnl54xnijzhijss66s3jzfqij2n4206d")))

(define-public crate-az65-0.1.6 (c (n "az65") (v "0.1.6") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0amxghzy47apns497c6f95ckmydgvva567w8xbqg8zbv2sfq14rq")))

(define-public crate-az65-0.1.7 (c (n "az65") (v "0.1.7") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z3i1qw1p8x91bwfk2gvhwbp2wr3a7rhsx8lwmiy3c8p6030d03p")))

(define-public crate-az65-0.1.8 (c (n "az65") (v "0.1.8") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01xz5k7qgfx66kdpdjkxfnc3h4m97q0f6c7adm0ywczl3g66v37k")))

(define-public crate-az65-0.1.9 (c (n "az65") (v "0.1.9") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "092l088av8jld032myibl98fxyxks2w9i5blv30r48dkdd87a58n")))

(define-public crate-az65-0.1.10 (c (n "az65") (v "0.1.10") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13423g1iv3b1y94dpwg75df2kp54a2qhaaf8pfa77cc26snw8d2i")))

(define-public crate-az65-0.1.11 (c (n "az65") (v "0.1.11") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01fxa2ypmx84zd1jc9nsh4iy8aiwf4xpvqwvdkmhz2h826cghz1f")))

