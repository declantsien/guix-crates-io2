(define-module (crates-io vy pe vyper-rs) #:use-module (crates-io))

(define-public crate-vyper-rs-0.2.0 (c (n "vyper-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "0p6gzfw67qbs5yi82gfj8fksl9plaplpkxn39k7rjacpwr8p36vn")))

(define-public crate-vyper-rs-1.0.0 (c (n "vyper-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1gxzm3ilhx3gf3cq88cz0sa06x37v01h9amqdd9d32iv93gw6sfd")))

(define-public crate-vyper-rs-2.0.0 (c (n "vyper-rs") (v "2.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1qsdbyc2404ngmwzia6qxycn2ynp2v2jhjl0wq0bn89l42xykpj2")))

(define-public crate-vyper-rs-2.0.1 (c (n "vyper-rs") (v "2.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "168v4ykhn2hq82apv39ba7zfr9vk6c6cj934bl2zhk1ii60vcsxp")))

