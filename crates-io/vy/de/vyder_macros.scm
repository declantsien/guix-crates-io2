(define-module (crates-io vy de vyder_macros) #:use-module (crates-io))

(define-public crate-vyder_macros-0.1.0 (c (n "vyder_macros") (v "0.1.0") (d (list (d (n "object-safe") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0b477shkq0y86g692rx0hk8jpz3h1imrvzh5170yn8difnrdcdar")))

(define-public crate-vyder_macros-0.1.1 (c (n "vyder_macros") (v "0.1.1") (d (list (d (n "object-safe") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0pq66n8pv1hzr7ibrl1cm6yxnwz75jpqi3vd3rckp4kmc523d0n2")))

