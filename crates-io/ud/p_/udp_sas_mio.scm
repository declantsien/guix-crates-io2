(define-module (crates-io ud p_ udp_sas_mio) #:use-module (crates-io))

(define-public crate-udp_sas_mio-0.1.2 (c (n "udp_sas_mio") (v "0.1.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "udp_sas") (r "^0.1.2") (d #t) (k 0)))) (h "08cj91hbyfm1v0lyshp1gnd4vpjgqzd35iniyl8plydvp4x6v60j")))

(define-public crate-udp_sas_mio-0.2.0 (c (n "udp_sas_mio") (v "0.2.0") (d (list (d (n "mio") (r "^0.7") (f (quote ("net"))) (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("net" "os-poll"))) (d #t) (k 2)) (d (n "udp_sas") (r "^0.1.2") (d #t) (k 0)))) (h "0gcjlngl4607yyiyf8kixr6dmldmw0z09fjdzz7147r6v354824g")))

