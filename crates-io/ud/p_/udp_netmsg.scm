(define-module (crates-io ud p_ udp_netmsg) #:use-module (crates-io))

(define-public crate-udp_netmsg-0.1.0 (c (n "udp_netmsg") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "19zykdw7lmzilnkn7q8hb16cm6dh7w2k5amcsrf9vixaxzy6cw6j")))

(define-public crate-udp_netmsg-0.1.1 (c (n "udp_netmsg") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "02h32cr1xvjsnnbqxgifsq2a92r3d8n2jjr99v442mcg5p63bdrx")))

(define-public crate-udp_netmsg-0.1.2 (c (n "udp_netmsg") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "16bm9kdz6rvda74s48amb0kdyhh84ysqi9pin8srj5qdharzls3l")))

(define-public crate-udp_netmsg-0.1.3 (c (n "udp_netmsg") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "12ndrdnajgcm9k897769wy8fq5l47jky02xam3l3wg9fv9jir1ly")))

(define-public crate-udp_netmsg-0.1.4 (c (n "udp_netmsg") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "1zf0s5zxj4x15m6fvfbin9gdc6jx016wczc5d3bzq5dzkmz2zlp0")))

(define-public crate-udp_netmsg-0.1.5 (c (n "udp_netmsg") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "10ffa2hs49l4d5cn8psyxwf934pvfgcm4js0rqxpc4aiw38czvrq")))

(define-public crate-udp_netmsg-0.1.6 (c (n "udp_netmsg") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "00bpb868s2d1xf90b2n6xp6d68lmfrfxzxgfsjh02nx434f2md01")))

(define-public crate-udp_netmsg-0.1.7 (c (n "udp_netmsg") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "1zicpw3zgz6zfkp65nhq1d2afn5vwqcw0i2vagvsaxz1j6drscyf")))

(define-public crate-udp_netmsg-0.1.8 (c (n "udp_netmsg") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "1bxqphc9g7kb84b75fx1v8jwl1hssm75ifqbf5r221hgx12w38vl")))

(define-public crate-udp_netmsg-0.2.0 (c (n "udp_netmsg") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "14z6zxshhjy3v8wvqv2zrvn84i257f38pi2g2vzh1shn0v9z29hp")))

