(define-module (crates-io ud p_ udp_server) #:use-module (crates-io))

(define-public crate-udp_server-0.1.0 (c (n "udp_server") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "net2") (r "^0.2.34") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01179y7paj91c6yzq1xzf5q2vasapl26f4rd1c4s00wr4n8j9g5k")))

(define-public crate-udp_server-0.1.1 (c (n "udp_server") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "net2") (r "^0.2.34") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y4j8jfdnbhmjm86v0z9232h82nd4pm2g6sbr48kigcgn71kh18p")))

(define-public crate-udp_server-0.1.2 (c (n "udp_server") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "net2") (r "^0.2.34") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bjhmhk0d4qnvpkyrw2w5m1h5k5zlm6llk3hqg3yp7pn5dxg6y85")))

(define-public crate-udp_server-0.1.3 (c (n "udp_server") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w66xfadadwfg283j4xb955cjdqx63ji6c24cvq8zrz7dw3wfxa2")))

(define-public crate-udp_server-0.1.4 (c (n "udp_server") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wl92i7rk1h3brwdbryw2y1k00dpq4ap06djd7rphz30fil3fkb7")))

(define-public crate-udp_server-0.2.0 (c (n "udp_server") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10qbcbhawx5924yd1qlgvhf1pz1z32fiqrirzyaahd54h50lg3jh")))

(define-public crate-udp_server-0.2.1 (c (n "udp_server") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdqrcbzvjznxkf60wgp78sk28dnkzwfabl2n52c1j7g0wg9hiav")))

(define-public crate-udp_server-0.2.2 (c (n "udp_server") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10vp70zij0sv54dzf3j8im5j4l4s75cj375rzm38qmxx4sc6j9rx")))

(define-public crate-udp_server-0.2.3 (c (n "udp_server") (v "0.2.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19m3kkxjjz95blvr0gh7m8kapx4x4k3305cy2p6j2yfrxz6bl38q")))

(define-public crate-udp_server-0.2.4 (c (n "udp_server") (v "0.2.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1y76448dyd4kbracyd8iv6mwm92akxv4kvmzppk0xaw1njgpfjvw")))

(define-public crate-udp_server-0.2.5 (c (n "udp_server") (v "0.2.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bc0h6z20zm6cbs3lzmp7q98jpxvzkqn9iwrs2jy1s7w1x5i7yqh")))

(define-public crate-udp_server-0.2.6 (c (n "udp_server") (v "0.2.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd9h55ll8gj1y8q1l19ls8h7xq8v05qdjzgrsrn8h0si3k5src5")))

(define-public crate-udp_server-0.2.7 (c (n "udp_server") (v "0.2.7") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1ayyjigvldx36ryrgjkh5pl1r22kfhm4gsq4g7ksxr3dvw7338")))

(define-public crate-udp_server-0.2.8 (c (n "udp_server") (v "0.2.8") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0sdhkibsl17c7z98xsxifip5zgvr5kzz2pnja3bwlrs9kgxlk9x7")))

(define-public crate-udp_server-0.2.9 (c (n "udp_server") (v "0.2.9") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "07wqvcw7hqlzbi6n5vb131sw3hb4ahl6njyp3li546d93i30a43s")))

(define-public crate-udp_server-0.3.0 (c (n "udp_server") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18dzccnvnlyn7z265nha19l4zmz8f3r5fzzwp3dxs7zibsdxk31p")))

(define-public crate-udp_server-0.3.1 (c (n "udp_server") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ifycn7szzsrkj9bm91fndp0yi3zdziw3vx2a8xhrg2cwi704jmx")))

(define-public crate-udp_server-0.3.2 (c (n "udp_server") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hghn66m3p90w6d5lsvni9jlrk020rw6nffx653r6mxalik3dxvj")))

(define-public crate-udp_server-0.3.3 (c (n "udp_server") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j1hf8xibav7pv4ki8z648pi8cixivy18rz59akiw6p8vqf1kgsf")))

(define-public crate-udp_server-0.3.4 (c (n "udp_server") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6ggp05jpan2iai7srf7z0qa15c9a915gx714lvnhsizk1ghi8f")))

(define-public crate-udp_server-0.3.5 (c (n "udp_server") (v "0.3.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nalqqcd0k0zkrq7kg7jn8f8nd6bqjmh1dsniwm58r6l5hdlwzca")))

(define-public crate-udp_server-0.3.6 (c (n "udp_server") (v "0.3.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zz5xz59900vbiar1ax3dgh5cj30363nf1qvs5lngir8dy95zzf4")))

(define-public crate-udp_server-0.4.0 (c (n "udp_server") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00hkbgss7wmasgj8xg26pvyzgvln1pkhj5zcjyfwzhai0qhql796")))

(define-public crate-udp_server-0.4.1 (c (n "udp_server") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4ap4m96h1idjlr43lyckv85kgdp1pdzmkn70pnx8dmzb7wd2p4")))

(define-public crate-udp_server-0.5.0 (c (n "udp_server") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "aqueue") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1abdqiz4zdpa6ypxxyszfbfh8wn4dbmy8l83671m83lxd798qn8i")))

(define-public crate-udp_server-0.5.1 (c (n "udp_server") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "aqueue") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksabdaqc5rd6kmcwhmjii369a4mapmk0f1jnpklflqki976yl9v")))

(define-public crate-udp_server-1.0.0 (c (n "udp_server") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "aqueue") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06r84qr1kqj6ikp693sqildhig844gdd87q4sn1pbdd2kvixaqcq")))

(define-public crate-udp_server-1.0.1 (c (n "udp_server") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "aqueue") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "130sck0xp5yj4ynpfbf6vg2zwv1a7kjy15phdw723qvl5l30p7ms")))

(define-public crate-udp_server-1.0.2 (c (n "udp_server") (v "1.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07vjzqnb86bhpf577h8pzy1fl9v54wwbwx10mvpfxrnzacri5zzi")))

(define-public crate-udp_server-1.0.3 (c (n "udp_server") (v "1.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqcllvhcgyd0khlarmx4jbl0226y7r13dqzaf5zs59kd7xyh74k")))

(define-public crate-udp_server-1.0.4 (c (n "udp_server") (v "1.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-lock") (r "^3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ry7v6i1x8i22ykz8czrs07ljb54xbyvnxn2m24la405qgdazv6d")))

