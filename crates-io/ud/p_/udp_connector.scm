(define-module (crates-io ud p_ udp_connector) #:use-module (crates-io))

(define-public crate-udp_connector-0.1.0 (c (n "udp_connector") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0yzypffv76knq65mnxgkbbc7hwn4c97ma2wx0gpiadhjalq02lpn")))

