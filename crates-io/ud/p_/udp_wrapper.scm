(define-module (crates-io ud p_ udp_wrapper) #:use-module (crates-io))

(define-public crate-udp_wrapper-0.1.0 (c (n "udp_wrapper") (v "0.1.0") (h "0zaxabd6qb2kz0f0gkwyhpcsx0yrnqncq2yk2cylxm6h6bl3ax3h")))

(define-public crate-udp_wrapper-0.1.2 (c (n "udp_wrapper") (v "0.1.2") (h "0ffplk4cd8826jqc2vb58vikrizlq7nhahz0w8pzm3lspyl6yb1y") (r "1.56")))

(define-public crate-udp_wrapper-0.2.0 (c (n "udp_wrapper") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19f0bcw1n7n2ayiqrsvgm512xfwg7skfin9cmdr266iqj892zvyc") (r "1.56")))

(define-public crate-udp_wrapper-0.2.1 (c (n "udp_wrapper") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p1jqd9wa1mqfwxwjqglzlv9x5clnm7i7kqhkdssf66phj4l7yfc") (r "1.56")))

