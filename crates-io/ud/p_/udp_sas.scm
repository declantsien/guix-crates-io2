(define-module (crates-io ud p_ udp_sas) #:use-module (crates-io))

(define-public crate-udp_sas-0.1.0 (c (n "udp_sas") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rjpl5k57pvy92m043skhcivnxgq5wqd5b5dwvawr698mzv7lg2n")))

(define-public crate-udp_sas-0.1.1 (c (n "udp_sas") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "060bys1kakrzhc5g74hf5m5p52jz7cz4f43i85z7sc3f7b2vaypg")))

(define-public crate-udp_sas-0.1.2 (c (n "udp_sas") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.1") (d #t) (k 0)))) (h "15x5bqah31cc3h8gs5ifwfwl1n9w0a1pscax4z381rmnl2gbfxb1")))

(define-public crate-udp_sas-0.1.3 (c (n "udp_sas") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.1") (d #t) (k 0)))) (h "0w6rzd7i6kc2wszc609842i1ffsnn5hrvjfq1zmfjygxxsrry8nb")))

(define-public crate-udp_sas-0.1.4 (c (n "udp_sas") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2.2") (d #t) (k 0)))) (h "03id4xvj2agz89bnbqs2vv4y26c14sf0dah18dn1llvw4pq856bk")))

