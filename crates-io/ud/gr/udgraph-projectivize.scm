(define-module (crates-io ud gr udgraph-projectivize) #:use-module (crates-io))

(define-public crate-udgraph-projectivize-0.6.0 (c (n "udgraph-projectivize") (v "0.6.0") (d (list (d (n "conllu") (r "^0.6") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.5") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.6") (d #t) (k 0)))) (h "11izcp5chbgbwx25l1gc4rnzfpar73f7g03f55x3bsr7p57r6jz7")))

(define-public crate-udgraph-projectivize-0.7.0 (c (n "udgraph-projectivize") (v "0.7.0") (d (list (d (n "conllu") (r "^0.7") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.7") (d #t) (k 0)))) (h "0d3kg083drm7bsv8yqm4cp2cshg8my6ni0kixz3mnsln1lv4ppsh")))

(define-public crate-udgraph-projectivize-0.8.0 (c (n "udgraph-projectivize") (v "0.8.0") (d (list (d (n "conllu") (r "^0.8") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.8") (d #t) (k 0)))) (h "1bkj426m8mr0f1z7045fyzb2i9a0c4i2cgbqny1jd26nbxkij9hl")))

