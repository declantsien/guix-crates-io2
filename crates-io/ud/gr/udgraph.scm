(define-module (crates-io ud gr udgraph) #:use-module (crates-io))

(define-public crate-udgraph-0.6.0 (c (n "udgraph") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.5") (k 0)))) (h "0hndmsgwc2m13d0caljmdvcwvpczi7w5dng6jrv89kkyp7nvx2ja")))

(define-public crate-udgraph-0.7.0 (c (n "udgraph") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (k 0)))) (h "11rp3bc56g34spawpf8jg90dwsmfzikva2770ddn8q5aqj0in0x7")))

(define-public crate-udgraph-0.8.0 (c (n "udgraph") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02hdyhdj8v5f1m6693cjs7g7ii1jz9dngll6w93mq5l13dx4xchg")))

