(define-module (crates-io ud p- udp-conditioner-proxy) #:use-module (crates-io))

(define-public crate-udp-conditioner-proxy-0.1.0 (c (n "udp-conditioner-proxy") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (f (quote ("async"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "process" "rt-multi-thread" "signal" "time"))) (d #t) (k 0)))) (h "1hzlqn2rv6i13x77mc8bg0g3p19ps2vncxf780cshs9qrwb3fxdd")))

(define-public crate-udp-conditioner-proxy-0.2.0 (c (n "udp-conditioner-proxy") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10") (f (quote ("async"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "process" "rt-multi-thread" "signal" "time"))) (d #t) (k 0)))) (h "0bmwfa59lr273b9s1h0ib9hva9zgwgq89fk86bz1zr65p0m8cazq")))

(define-public crate-udp-conditioner-proxy-0.3.0 (c (n "udp-conditioner-proxy") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10") (f (quote ("async"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "process" "rt-multi-thread" "signal" "time"))) (d #t) (k 0)))) (h "1hiqgnyrm39fnblyr0hw1mb4bl838qx3d61jnyz9dsp02r41rink")))

