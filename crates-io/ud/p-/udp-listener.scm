(define-module (crates-io ud p- udp-listener) #:use-module (crates-io))

(define-public crate-udp-listener-0.1.0 (c (n "udp-listener") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("udp" "rt-core"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0phn6rf9jan8ly1icin0wds00w1nkzin07qn6rhmsjlls97h3wxv")))

(define-public crate-udp-listener-0.1.1 (c (n "udp-listener") (v "0.1.1") (d (list (d (n "async-channel") (r "^1.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("udp" "rt-core"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1hgmayq1hh739iknrblil6cblc56wbj3gvy54mah5zy8cmj403gl")))

(define-public crate-udp-listener-0.1.2 (c (n "udp-listener") (v "0.1.2") (d (list (d (n "async-channel") (r "^1.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("udp" "rt-core"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1jjxgs7m697zvcqc5rs5bcy02xg1ajdq94qgs6r34m5l76nbb3cw")))

