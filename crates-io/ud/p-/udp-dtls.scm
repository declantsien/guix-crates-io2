(define-module (crates-io ud p- udp-dtls) #:use-module (crates-io))

(define-public crate-udp-dtls-0.0.0 (c (n "udp-dtls") (v "0.0.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "13d26vhlq7mpbkpp9b8pcnwvwypgfc4xccbdyxps3vxs0vcj9p35") (f (quote (("vendored" "openssl/vendored"))))))

(define-public crate-udp-dtls-0.1.0 (c (n "udp-dtls") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "0vry1907qmrxj791hwl5mvrdakipzciy50s567rzhdv51bvlyagp") (f (quote (("vendored" "openssl/vendored"))))))

