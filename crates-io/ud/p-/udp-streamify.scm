(define-module (crates-io ud p- udp-streamify) #:use-module (crates-io))

(define-public crate-udp-streamify-0.1.0 (c (n "udp-streamify") (v "0.1.0") (d (list (d (n "tokio") (r "^1.2.0") (f (quote ("net" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "0z217by4j18vw12fnv9f35dipxblc8hi4xz5z19p4192cd620yjk")))

(define-public crate-udp-streamify-0.1.1 (c (n "udp-streamify") (v "0.1.1") (d (list (d (n "tokio") (r "^1.5") (f (quote ("net" "sync" "rt" "macros" "time"))) (d #t) (k 0)))) (h "1iszv3i0kgk90q85nskjsfl5744rxhzsl51s3apchkypdm2wrnma")))

