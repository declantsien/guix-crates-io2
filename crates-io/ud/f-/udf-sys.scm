(define-module (crates-io ud f- udf-sys) #:use-module (crates-io))

(define-public crate-udf-sys-0.3.0 (c (n "udf-sys") (v "0.3.0") (h "0h05h4zj9jsygbq002r6yi53s8r2rnlk750jmr7rxfhh054l3h3x")))

(define-public crate-udf-sys-0.3.1 (c (n "udf-sys") (v "0.3.1") (h "13qk73mdnr7gdxb4r99z85gk0ajbk02ysyhnaay6ll70d41ivk7y")))

(define-public crate-udf-sys-0.3.2 (c (n "udf-sys") (v "0.3.2") (h "12cmgbhh1jkyckqhp3a26kr6xwmpkv5789w1pmynpn25hzwgs1r9")))

(define-public crate-udf-sys-0.3.3 (c (n "udf-sys") (v "0.3.3") (h "1xj08zgqx54m42mn1l7aiz5rkbb1v7scflvjilabqx3ylzsm1mz2")))

(define-public crate-udf-sys-0.3.4 (c (n "udf-sys") (v "0.3.4") (h "1waiqsyk5zk8qiqn900mivh86hrhcjxpcbmc8xxdncslcwrgkncs")))

(define-public crate-udf-sys-0.3.5 (c (n "udf-sys") (v "0.3.5") (h "1hdna6znyww34kx468ay9qnqn65k7z9cylxhdjqvvs0hy7qg3kxi")))

(define-public crate-udf-sys-0.3.6 (c (n "udf-sys") (v "0.3.6") (h "0hm28x16bqcp5zc0bw7hbknfqd6b9v3jnlgfkb2w0j3161s9ppdg")))

(define-public crate-udf-sys-0.3.7 (c (n "udf-sys") (v "0.3.7") (h "095y4zh7iy43ginqgddswq8lwpb5fak5lxkvd09mmsyy5j8lama6")))

(define-public crate-udf-sys-0.3.8 (c (n "udf-sys") (v "0.3.8") (h "00dnbzkv2y9binfj0bc6b09nvq9p1953hl8z15pdbbw1yvb886bd")))

(define-public crate-udf-sys-0.3.9 (c (n "udf-sys") (v "0.3.9") (h "10vli8a26pdq4jbl38pk68nbvv1j6lypyz3c5ykvd9viq4m8zc1f")))

(define-public crate-udf-sys-0.3.10 (c (n "udf-sys") (v "0.3.10") (h "0whaxjc55h1pv6g2r3q8jzzwnk4k9kbb9nvg5rpgahmx1m9yn7mi")))

(define-public crate-udf-sys-0.4.0-rc.1 (c (n "udf-sys") (v "0.4.0-rc.1") (h "0qy67a0yx67c1lsrax3jq6kv6b3anaa1jpdqv9shlwyayay53lvx")))

(define-public crate-udf-sys-0.4.0 (c (n "udf-sys") (v "0.4.0") (h "0gws8rngaj4sd2rq3ykz1n5v5yjgnjbvihd7miibq948pdc53kap")))

(define-public crate-udf-sys-0.4.1 (c (n "udf-sys") (v "0.4.1") (h "14c1nk162xrz2d7l4y6zkr344z2qcgi306bblz84nwfz9rqrf0lf")))

(define-public crate-udf-sys-0.4.2 (c (n "udf-sys") (v "0.4.2") (h "0jz7nhx2lqsbllqalg3xlfczsih3n3khx63widnq6621f6lc8fgk")))

(define-public crate-udf-sys-0.4.4 (c (n "udf-sys") (v "0.4.4") (h "0z6rrzc5s5rx8sqm45bz5g5n8128ca80ymynbj6j79h19hmkfa9b")))

(define-public crate-udf-sys-0.4.5 (c (n "udf-sys") (v "0.4.5") (h "10naa3ci9j8nmkdxy883cj6sfinsy2l9w6r4l3fnykn9567pi80x")))

(define-public crate-udf-sys-0.5.0-rc.1 (c (n "udf-sys") (v "0.5.0-rc.1") (h "130d3lpi4x5fmj7x22j75vhzwsab7zv34xq9jgvbb5w93ds99whz")))

(define-public crate-udf-sys-0.5.0-rc.2 (c (n "udf-sys") (v "0.5.0-rc.2") (h "1m94idjw9zz3v92mblz9yxbqvwzkg8ckm7g2vmh4g0hr0sji4idc")))

(define-public crate-udf-sys-0.5.0 (c (n "udf-sys") (v "0.5.0") (h "0142504z4iflz31s9gv95xchs9ajc8say4f5fs5a4rng8565klp5")))

(define-public crate-udf-sys-0.5.1 (c (n "udf-sys") (v "0.5.1") (h "1m3r2jv74hi9qmq2413j17qjwx1y45nbh302yzsfvzdqcfkrkm2r")))

(define-public crate-udf-sys-0.5.2 (c (n "udf-sys") (v "0.5.2") (h "1865474hl22bbbm5kz6i4lw05mbpbs9b02hvbnw0sj0cvnalnskn")))

(define-public crate-udf-sys-0.5.3 (c (n "udf-sys") (v "0.5.3") (h "0ql23kszyhyspw7a5kbj02d58cjgzcf296flvpv2v9jj7rbd9wpg")))

(define-public crate-udf-sys-0.5.4 (c (n "udf-sys") (v "0.5.4") (h "1qalcf87pzdiipjfzdycg1rdbscvpxzf7acx04p4p75m9i149kyf")))

(define-public crate-udf-sys-0.5.5 (c (n "udf-sys") (v "0.5.5") (h "09bwyc2m0bpqiyy3iqihm54h7kc8iql9n21w78s8wi1rk4zwac91")))

