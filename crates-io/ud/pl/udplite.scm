(define-module (crates-io ud pl udplite) #:use-module (crates-io))

(define-public crate-udplite-0.1.0 (c (n "udplite") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.59") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"android\"))") (k 0)) (d (n "mio_06") (r "^0.6.14") (o #t) (d #t) (k 0) (p "mio")) (d (n "mio_07") (r "^0.7.0") (f (quote ("os-util"))) (o #t) (d #t) (k 0) (p "mio")))) (h "1dzs5hjx7w2vv6i9zs4ac0y7kbskcm2w7mr5wp0r46nzpnqqkpdm")))

