(define-module (crates-io ud mp udmp-parser) #:use-module (crates-io))

(define-public crate-udmp-parser-0.1.2 (c (n "udmp-parser") (v "0.1.2") (h "1a6w9kz5dajwm32z2lcd6gxyn06h42ryf95q54szn4xska13k6ir") (r "1.65")))

(define-public crate-udmp-parser-0.2.0 (c (n "udmp-parser") (v "0.2.0") (h "1dbldwa5b9knzhipvxvm2ajcg8cmh3hlrgcvsg7dvjxp1pgc23d2") (r "1.65")))

