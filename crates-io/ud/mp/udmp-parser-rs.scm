(define-module (crates-io ud mp udmp-parser-rs) #:use-module (crates-io))

(define-public crate-udmp-parser-rs-0.1.0 (c (n "udmp-parser-rs") (v "0.1.0") (h "0k8mrfj60f7vw9xx5vljr4b4zcg5m05xgbgf5vsjq9ia1qy5kch3") (y #t) (r "1.65")))

(define-public crate-udmp-parser-rs-0.1.1 (c (n "udmp-parser-rs") (v "0.1.1") (h "1460lhbd6lvh538jnd18zb44nvh053icrh6pa8731k5a94f4zy3b") (y #t) (r "1.65")))

(define-public crate-udmp-parser-rs-0.1.2 (c (n "udmp-parser-rs") (v "0.1.2") (h "1g1nz5f9rlq79b47fvyll732mfchph5mclpdqsjr8mhjvd31dj9l") (y #t) (r "1.65")))

