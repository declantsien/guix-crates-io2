(define-module (crates-io ud ev udevrs) #:use-module (crates-io))

(define-public crate-udevrs-0.1.0 (c (n "udevrs") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1v2w9w79104igjrw5fys2w1npfsdq1zcsiq68mnxp7xzc68h0dmx")))

(define-public crate-udevrs-0.1.1 (c (n "udevrs") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "0jjlhvyrrz3n7yg5id09a4fmn8mz3iiy87il1519j2gykimkzwbz")))

(define-public crate-udevrs-0.2.0 (c (n "udevrs") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1asqn8fbbf7bw9crksv2llhp870qdmh8mx69135i24nv265vjizq")))

