(define-module (crates-io ud it udit) #:use-module (crates-io))

(define-public crate-udit-0.1.0 (c (n "udit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w6j9d1m9535z1wjsyi4srbviicid4mv2a1zpqhp18pl1ynlnda0")))

(define-public crate-udit-0.1.1 (c (n "udit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1viy1rgdyaxc7h6asj58q0iqk3sn6hj1gca4598ick8dy4cnsckc")))

