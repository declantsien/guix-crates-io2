(define-module (crates-io ud ps udpsec) #:use-module (crates-io))

(define-public crate-udpsec-0.1.0 (c (n "udpsec") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.2.0") (f (quote ("reusable_secrets"))) (d #t) (k 0)))) (h "0fx1dj4b3jfcrbh4q9adnsn455wg1dmbvdksqzv6pq7xw9n58i9h")))

(define-public crate-udpsec-0.2.0 (c (n "udpsec") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.5.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "wait_not_await") (r "^0.2.1") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.2.0") (f (quote ("reusable_secrets"))) (d #t) (k 0)))) (h "13xjqnysgdk10xpdx1x4dr80sj1c30vqz5xa5chmfb5hxx2ryqd3")))

