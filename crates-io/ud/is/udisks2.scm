(define-module (crates-io ud is udisks2) #:use-module (crates-io))

(define-public crate-udisks2-0.1.0 (c (n "udisks2") (v "0.1.0") (d (list (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 2)) (d (n "zbus") (r "^4.0.0") (d #t) (k 0)))) (h "18709rmckmil97g9z1qnv7n8qn0v43nyzy3r9k1v8agxjnani45i") (r "1.75")))

