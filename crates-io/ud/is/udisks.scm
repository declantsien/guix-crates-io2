(define-module (crates-io ud is udisks) #:use-module (crates-io))

(define-public crate-udisks-0.1.0 (c (n "udisks") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zbus") (r "^2.1.1") (d #t) (k 0)))) (h "0schsgz3q1qgxfzm260lp3jkxsc4jq7b1jwnghf3z9xhldyzxiam")))

