(define-module (crates-io ud p2 udp2p_node) #:use-module (crates-io))

(define-public crate-udp2p_node-0.1.0 (c (n "udp2p_node") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)) (d (n "udp2p_traits") (r "^0.1.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "09g325js1mmdlivxjhvz9n32h4ffipcsxg0bxrmy348kfs7wrzs8")))

