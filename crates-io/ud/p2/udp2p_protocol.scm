(define-module (crates-io ud p2 udp2p_protocol) #:use-module (crates-io))

(define-public crate-udp2p_protocol-0.2.0 (c (n "udp2p_protocol") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0dqdipyad3nxhmjqzwm98zynqkw6hv5rfm2mln3fipsrn7g2f8d4")))

