(define-module (crates-io ud p2 udp2p_gd_udp) #:use-module (crates-io))

(define-public crate-udp2p_gd_udp-0.2.0 (c (n "udp2p_gd_udp") (v "0.2.0") (d (list (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "118w3wdrm73qjiy6yc9d9qgmxxrpi0gk4iifmhqdw3ry7ymd20rx")))

(define-public crate-udp2p_gd_udp-0.2.1 (c (n "udp2p_gd_udp") (v "0.2.1") (d (list (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "08frr519wfzhdg310gn1yriwshgqhqgiglzd6hyc1iwx6wprv40v")))

(define-public crate-udp2p_gd_udp-0.2.2 (c (n "udp2p_gd_udp") (v "0.2.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "1gka4xnhwcidzn7yi5vmybrkmnzj2x6jygk7xabr7bs37h3lw4k4")))

