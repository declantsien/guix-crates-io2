(define-module (crates-io ud p2 udp2p_utils) #:use-module (crates-io))

(define-public crate-udp2p_utils-0.2.0 (c (n "udp2p_utils") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)))) (h "0x7hqshp9phq55ln1jhi5i2z40184gf8wxgb574vf6y1qvkcbypi")))

