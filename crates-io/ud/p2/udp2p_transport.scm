(define-module (crates-io ud p2 udp2p_transport) #:use-module (crates-io))

(define-public crate-udp2p_transport-0.2.0 (c (n "udp2p_transport") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "udp2p_gd_udp") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0a4579ca11qh4cr82y8fb0n90xvnrqnk2lxvikv9w89zhqqqrkib")))

(define-public crate-udp2p_transport-0.2.1 (c (n "udp2p_transport") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "udp2p_gd_udp") (r "^0.2.1") (d #t) (k 0)) (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "1i2rhyc4lnb4aypcqafqx07fhgnndgs80j7q89zl8b7jlycfkvjl")))

(define-public crate-udp2p_transport-0.2.2 (c (n "udp2p_transport") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "udp2p_gd_udp") (r "^0.2.1") (d #t) (k 0)) (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0csvvp4liyky8rngpb72xyfwr76b56vz3wjwkyb6an3ywwcdp89s")))

(define-public crate-udp2p_transport-0.2.3 (c (n "udp2p_transport") (v "0.2.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "udp2p_gd_udp") (r "^0.2.1") (d #t) (k 0)) (d (n "udp2p_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0q5mjj3ndybsjk9q46jwssb55z5qpnihnai8pqz4ngmxpwv4c5yh")))

