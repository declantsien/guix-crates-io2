(define-module (crates-io ud p2 udp2p_record) #:use-module (crates-io))

(define-public crate-udp2p_record-0.2.0 (c (n "udp2p_record") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "udp2p_node") (r "^0.1.0") (d #t) (k 0)) (d (n "udp2p_utils") (r "^0.2.0") (d #t) (k 0)))) (h "16yhw9gyjvdxn2bjj0g76kfcn481j2922fiagnhnxhlbxjgkr9d9")))

