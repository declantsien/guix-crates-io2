(define-module (crates-io ud bs udbserver) #:use-module (crates-io))

(define-public crate-udbserver-0.1.0 (c (n "udbserver") (v "0.1.0") (d (list (d (n "gdbstub") (r "^0.6") (d #t) (k 0)) (d (n "unicorn-engine") (r "^2.0.0-rc7") (f (quote ("use_system_unicorn"))) (d #t) (k 0)))) (h "1329fhzi22ll3h8hrqaaknq1w1f1smg5jhxp4jzlpjszq3by5yx1") (f (quote (("capi"))))))

