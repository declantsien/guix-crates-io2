(define-module (crates-io ud ic udict) #:use-module (crates-io))

(define-public crate-udict-0.1.0 (c (n "udict") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wdf8qxvllsvvq6x4ca9b78bynxb0bkb0qqiphi5a5ay6j77fsvf")))

(define-public crate-udict-0.1.1 (c (n "udict") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nkcw90gxiw6l0jdh94cpfvxm4abcywkl2n7w9yf04nk46rzbcji")))

(define-public crate-udict-0.1.2 (c (n "udict") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "termimad") (r "^0.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pcqapsalhrak4xv8sw5g6w9kj0bn958bf3wb16rgyx461hwx8sp")))

