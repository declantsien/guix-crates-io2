(define-module (crates-io ud sx udsx) #:use-module (crates-io))

(define-public crate-udsx-0.1.0 (c (n "udsx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12vncl7nqhmd30v9zvyzgj6rmmqiiyvf4iwq38s53h65nqk7mavh") (y #t)))

(define-public crate-udsx-0.2.0 (c (n "udsx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k68ja8v9hdzanvvq481p00p9hkrrn6jk5g41b4j3xamqzmrxcxs") (y #t)))

(define-public crate-udsx-0.3.0 (c (n "udsx") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01rqqc07hyvrcr9dn3vgrkmiy74ya2jh8a480qcwrlfsxgm3znb5") (y #t)))

(define-public crate-udsx-0.4.0 (c (n "udsx") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ngndhdx8y2g9sxgi1i874i0aqvy56nmdp50i60ai34j0kjwdla2") (y #t)))

(define-public crate-udsx-0.5.0 (c (n "udsx") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0flygfy8fa1bkjqy7rvnvrblp2knk8j6i2li6gq45cg4m4pz7d96") (y #t)))

(define-public crate-udsx-0.6.0 (c (n "udsx") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vy6srwa1y61khq0sa2abc7y9f9yin94ifr9nxnx60rfblxmajl3") (y #t)))

(define-public crate-udsx-0.7.0 (c (n "udsx") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xijw1vvmcg2mqwg2i0sv2qzzij7vm4fy4vyw7qigfr3fbxl0am2") (y #t)))

(define-public crate-udsx-0.7.1 (c (n "udsx") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j30i1sk3mhqhsjbvv0k7n820g123qr8947i0w6fadrhgy58ad1f") (y #t)))

(define-public crate-udsx-0.7.2 (c (n "udsx") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pcyyp0rl7mvz1hm9cd415varalvnb0dqwair6ihz5p8jxr1gq2v") (y #t)))

(define-public crate-udsx-0.8.0 (c (n "udsx") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x15kp1azpldc7y0nfnpcb9gw4ff93hva41qc8n0z31drcm3zd8y") (y #t)))

(define-public crate-udsx-0.8.1 (c (n "udsx") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gin448sp9fsnwj4jxyy8h96nc3xrwwm5gkbcrih89jlfvi9z2zi") (y #t)))

(define-public crate-udsx-0.9.0 (c (n "udsx") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k2nk2q1ymgmaf5ixx9yb6vjrmhg1q55y19yjhxllz0ham31hr3g") (y #t)))

(define-public crate-udsx-0.10.0 (c (n "udsx") (v "0.10.0") (h "0g8k3qhms0yd8fqq34cmi7nzcl4s0s3n659kg8g5qcgli06ymvzn") (y #t)))

(define-public crate-udsx-0.10.1 (c (n "udsx") (v "0.10.1") (h "196qqlnjydajs8al3lxmih7cz5x3z86n2hh93raahvp4i0iyydnf") (y #t)))

(define-public crate-udsx-0.11.0 (c (n "udsx") (v "0.11.0") (h "0d6x4gkn7w3lr2kv0l7dcv0s1glxv0vnp4577l5zs2rprxbzid0x") (y #t)))

(define-public crate-udsx-0.12.0 (c (n "udsx") (v "0.12.0") (h "1yzv1xs4kklvqfw760b6agz1vbdiyzlmwlms90gpgdr7wvm878vm") (y #t)))

(define-public crate-udsx-0.12.1 (c (n "udsx") (v "0.12.1") (h "02h89bmdfd2kngfig2xrqbjvclaj3zxmmyb43kg9iyclv23ld76x") (y #t)))

(define-public crate-udsx-0.13.0 (c (n "udsx") (v "0.13.0") (h "1p9c710a2k83ay7jyrz45gx65hfwyr1b3dka38cmhwy8lcj8xccz") (y #t)))

(define-public crate-udsx-0.14.0 (c (n "udsx") (v "0.14.0") (h "10h7gc80v9c0jrwhrk9v8h80gc30bhz7gcwlniv1fg8q593mvqmd") (y #t)))

