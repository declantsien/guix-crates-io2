(define-module (crates-io ud s_ uds_windows) #:use-module (crates-io))

(define-public crate-uds_windows-0.1.0 (c (n "uds_windows") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0pqln8x5likjwhn40782z3s35ppjgx324y8r3c2hfch1j8jp1y4q")))

(define-public crate-uds_windows-0.1.1 (c (n "uds_windows") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0z9z0fy6227v7j5msxqbp5rkw91cw84jqa4ziv1b8yhg3vy7401q")))

(define-public crate-uds_windows-0.1.2 (c (n "uds_windows") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1a3mlcddxs8d33zq939jdfarnmb9j5sww8gbn5wfm7gqcnj8qsxx")))

(define-public crate-uds_windows-0.1.3 (c (n "uds_windows") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "044s8v884808cr31r2c989nfv0ny335b4rz32f8np7cqs38nq4yx")))

(define-public crate-uds_windows-0.1.4 (c (n "uds_windows") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "18z56v7ajgg7qs7mxwgpvk1955l5f3iwxmj5rhzzcv6d91jh0qfg")))

(define-public crate-uds_windows-0.1.5 (c (n "uds_windows") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0mdv9xyrf8z8zr2py5drbilkncgrkg61axq6h7hcvgggklv9f14z")))

(define-public crate-uds_windows-0.1.6 (c (n "uds_windows") (v "0.1.6") (d (list (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2def" "minwinbase" "ntdef" "processthreadsapi" "handleapi" "ws2tcpip" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v7g9hl06s13xc4d2bn6khqn2cz6ha2f2d71j8l6mvlhypsjpch9")))

(define-public crate-uds_windows-1.0.0 (c (n "uds_windows") (v "1.0.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2def" "minwinbase" "ntdef" "processthreadsapi" "handleapi" "ws2tcpip" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yg4c97zmh0g0vh55p6kjmvlh0nn52qh8vl1kzgy8dc5zchr74k5")))

(define-public crate-uds_windows-1.0.1 (c (n "uds_windows") (v "1.0.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2def" "minwinbase" "ntdef" "processthreadsapi" "handleapi" "ws2tcpip" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jkyqinyh3gyrv65wnhxjfz23ianx0gr9w8ad1h03zphil894sa8")))

(define-public crate-uds_windows-1.0.2 (c (n "uds_windows") (v "1.0.2") (d (list (d (n "tempfile") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2def" "minwinbase" "ntdef" "processthreadsapi" "handleapi" "ws2tcpip" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03ckj6vnzvm4r5xd17dxyyqqqcfgs3xqj53hcswykk6k4i1n0rff")))

(define-public crate-uds_windows-1.1.0 (c (n "uds_windows") (v "1.1.0") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2def" "minwinbase" "ntdef" "processthreadsapi" "handleapi" "ws2tcpip" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fb4y65pw0rsp0gyfyinjazlzxz1f6zv7j4zmb20l5pxwv1ypnl9")))

