(define-module (crates-io ud s_ uds_seq) #:use-module (crates-io))

(define-public crate-uds_seq-0.1.0 (c (n "uds_seq") (v "0.1.0") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0ib93kcfg51s5cwbv57m9vfhc0p2b0vp49rx72lnab1dzhaqdph9")))

(define-public crate-uds_seq-0.1.2 (c (n "uds_seq") (v "0.1.2") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1ycs07jzjdnpqyb6xqhsff6slhwnmxqf9x26kv98vq3g8d83mffn")))

(define-public crate-uds_seq-0.1.3 (c (n "uds_seq") (v "0.1.3") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1j5bvs75h6888d510z1kqdwy5mfiwscxvmkb560yc2kx43fbhdrj")))

