(define-module (crates-io ud pe udpexchange) #:use-module (crates-io))

(define-public crate-udpexchange-0.1.0 (c (n "udpexchange") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "const-lru") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (o #t) (d #t) (k 0)) (d (n "static-alloc") (r "^0.2.4") (d #t) (k 0)))) (h "1lj2v5h6mrd637s0sq0jqvv5l9msl1szy1c7k94djwcgfm1h921l") (f (quote (("mini" "libc"))))))

(define-public crate-udpexchange-0.1.1 (c (n "udpexchange") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "const-lru") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (o #t) (d #t) (k 0)) (d (n "static-alloc") (r "^0.2.4") (d #t) (k 0)))) (h "0al88fr38jb5ibjy204g9fxv19xn4niyp8nx194001q3nj1wqsn4") (f (quote (("replay") ("mini" "libc"))))))

