(define-module (crates-io ud at udatatable) #:use-module (crates-io))

(define-public crate-udatatable-0.1.0 (c (n "udatatable") (v "0.1.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (f (quote ("std"))) (d #t) (k 2)))) (h "0sf55mlnrjlh00ma4dylyzlkv1gwzy2iv290ma5fxmfxm3nwk8kj") (f (quote (("plot"))))))

(define-public crate-udatatable-0.1.1 (c (n "udatatable") (v "0.1.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (f (quote ("std"))) (d #t) (k 2)))) (h "1ffknb9nzhwnnkckiki4ddnnqlkiwwsgiqd4wsz02ysd5hc8vgzs") (f (quote (("plot"))))))

