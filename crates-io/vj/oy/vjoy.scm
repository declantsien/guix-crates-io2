(define-module (crates-io vj oy vjoy) #:use-module (crates-io))

(define-public crate-vjoy-0.1.0 (c (n "vjoy") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1151r6c7lgkpabl4h6qjipq014yj2lranv1vcx52hfl19afy419b")))

(define-public crate-vjoy-0.2.0 (c (n "vjoy") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0586pg9r8w4swqxlciqcq6sa2phnrpy5b75yvcbsr7rqvc3kflsj")))

(define-public crate-vjoy-0.3.0 (c (n "vjoy") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1s21brwypvlw5hsqwp5p9mw1nmd17yijm588mjl0i5yaw3s6p7hd")))

(define-public crate-vjoy-0.3.1 (c (n "vjoy") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.3.0") (d #t) (k 0)))) (h "04w2za3jshs7ypb7p08dqpsrkb7d5ddqn3cdm44vj1rmwzm0r4nv")))

(define-public crate-vjoy-0.4.0 (c (n "vjoy") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "profiling") (r "^1.0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1xxv5hzvvb544cdanyrvrp8pl0qbql2340ngaxabhqyjlb2wq7k8")))

(define-public crate-vjoy-0.5.0 (c (n "vjoy") (v "0.5.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "profiling") (r "^1.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1f7r9xd7l34sigbfx9s6g10fl26gddii1nf7azg632dpllnarnhq")))

(define-public crate-vjoy-0.6.0 (c (n "vjoy") (v "0.6.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "profiling") (r "^1.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vjoy-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1fdjfvjbdpknd751v9l7zva47ripjrss2jqdkp60miylz5awmzzm")))

