(define-module (crates-io vj oy vjoy-sys) #:use-module (crates-io))

(define-public crate-vjoy-sys-0.1.0 (c (n "vjoy-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "0jagl95kma58yql96nqb4k0cxsxfg20pf60gfz5zdlj4wyx0f4fi")))

(define-public crate-vjoy-sys-0.1.1 (c (n "vjoy-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "19hrh94fqnsvdqq5lj2gsws39s46x6szkpsbfjs9jhaakj325z44")))

(define-public crate-vjoy-sys-0.1.2 (c (n "vjoy-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1p14yxmd15l3yhagdd3hbw0h916ab31m14nwpyvm0xfbmahh6krh")))

(define-public crate-vjoy-sys-0.2.0 (c (n "vjoy-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "0ch30nhpypyjsvmdsw1h0lsyicbi2bjw4zyngxd2dxr714fyymvd")))

(define-public crate-vjoy-sys-0.3.0 (c (n "vjoy-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1dvqalyz2dd1sfwh8a17j5rckagh8r708yh6fi8pjjnj3sjzblaq")))

(define-public crate-vjoy-sys-0.4.0 (c (n "vjoy-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1qlyjy2h7d81jaqzc8r1x8z3y8qbrd92cwygrzkyayhn52qvklnj")))

(define-public crate-vjoy-sys-0.4.1 (c (n "vjoy-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1jyj5ksbc3m9g7kgsydr1cl837wyxiln40178q3qmgdps4bbvw5i")))

