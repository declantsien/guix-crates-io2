(define-module (crates-io vj ou vjoule_api) #:use-module (crates-io))

(define-public crate-vjoule_api-1.1.0 (c (n "vjoule_api") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0ffbc9wpgjjzy5i0ha1r0qgdw7l6395m4qf1zxjpqc3fyxna4fjg") (y #t)))

(define-public crate-vjoule_api-1.1.1 (c (n "vjoule_api") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1j3qh16i00cczvpcrwzx2z8flr5pnlyf4pvwjzjx0qxdl5w1l95c")))

(define-public crate-vjoule_api-1.2.0 (c (n "vjoule_api") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "12g3wcsvhl7qffqyvb9s25czaqf7xpl8nj9bq3h62a8i8r4bk5s3")))

(define-public crate-vjoule_api-1.3.0 (c (n "vjoule_api") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1d0h87x0i6ax0fybwyc5x1p1yc4jlvw68s5ifw9l77lrvnma0j3z")))

(define-public crate-vjoule_api-1.3.1 (c (n "vjoule_api") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1iq6bpxmqgn29g2g83b3gsygamxmxdvhcb98spfq9av932qm0s26")))

