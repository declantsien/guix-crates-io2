(define-module (crates-io ht tp http-multipart) #:use-module (crates-io))

(define-public crate-http-multipart-0.1.0 (c (n "http-multipart") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.21") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 2)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "httparse") (r "^1.8") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "084jhj65rmfrkcqzhwhyng1myixrclndnk91kaza0m9vby739fka")))

