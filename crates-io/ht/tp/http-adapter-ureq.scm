(define-module (crates-io ht tp http-adapter-ureq) #:use-module (crates-io))

(define-public crate-http-adapter-ureq-0.1.0 (c (n "http-adapter-ureq") (v "0.1.0") (d (list (d (n "http-adapter") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (k 0)))) (h "0nfi2gg3sflns5aiin63jw2p1fgvgch2qgj5iv6ibr1xzhsg10pm") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("json" "ureq/json") ("gzip" "ureq/gzip") ("default" "tls" "gzip") ("cookies" "ureq/cookie") ("charset" "ureq/charset") ("brotli" "ureq/brotli"))))))

(define-public crate-http-adapter-ureq-0.1.1 (c (n "http-adapter-ureq") (v "0.1.1") (d (list (d (n "http-adapter") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (k 0)))) (h "01dp14kgymzkpqbdqaqmyrahhapza1gw9xcp902l91gas9cx2kaa") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("json" "ureq/json") ("gzip" "ureq/gzip") ("default" "tls" "gzip") ("cookies" "ureq/cookie") ("charset" "ureq/charset") ("brotli" "ureq/brotli"))))))

(define-public crate-http-adapter-ureq-0.2.0 (c (n "http-adapter-ureq") (v "0.2.0") (d (list (d (n "http-adapter") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (k 0)))) (h "0hpc0k71zi4vi79zzrn2hq551c4qcz4wyb5dkkvqsyga5nidyh5a") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("json" "ureq/json") ("gzip" "ureq/gzip") ("default" "tls" "gzip") ("cookies" "ureq/cookie") ("charset" "ureq/charset") ("brotli" "ureq/brotli"))))))

