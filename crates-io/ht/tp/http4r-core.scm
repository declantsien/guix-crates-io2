(define-module (crates-io ht tp http4r-core) #:use-module (crates-io))

(define-public crate-http4r-core-0.0.0 (c (n "http4r-core") (v "0.0.0") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0gmi8hpg5rvcp2h37nzhq3cg6qd7lrj2qclc4lwyy6kac1d2i15r")))

(define-public crate-http4r-core-0.0.1 (c (n "http4r-core") (v "0.0.1") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1h5pfddbi2pbwjly572c8vcqpqjykdsz704g4gih6nsm02h2jg9b")))

(define-public crate-http4r-core-0.0.2 (c (n "http4r-core") (v "0.0.2") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0i3vzhaip5k6by1g7wdmdjxqcy5i16lj4h9w9y3g99ljj0c147zl")))

(define-public crate-http4r-core-0.0.3 (c (n "http4r-core") (v "0.0.3") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "15l4f55i9f35nprxccsfih4hk8f6gmjljbm022hf26yv8an2jgdk")))

(define-public crate-http4r-core-0.0.4 (c (n "http4r-core") (v "0.0.4") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1ifc3i41ls4ma6zwi0166b8s1d91s4f64y3ry2lsv2mm8xq2yglk")))

(define-public crate-http4r-core-0.0.5 (c (n "http4r-core") (v "0.0.5") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1864d0w5h8qkndy0x5hr7gv9g00khyijd5hpmfcg60p7avy3821j")))

(define-public crate-http4r-core-0.0.6 (c (n "http4r-core") (v "0.0.6") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1l7faxad0hbm9vflcb4028p0bsq3qj4a457vw4kgi0zx2dcdaxnf")))

