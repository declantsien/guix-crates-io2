(define-module (crates-io ht tp http_status_code_searcher) #:use-module (crates-io))

(define-public crate-http_status_code_searcher-0.1.0 (c (n "http_status_code_searcher") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1lnpm90rk9nbv9hmaklgp6zck13bs743zjs026k6y5bixczg7q2w")))

(define-public crate-http_status_code_searcher-0.1.1 (c (n "http_status_code_searcher") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "061jzsppmdbzgn9ajd46mn06h1szi9ph40b4qkqrm4mfmlv26anr")))

(define-public crate-http_status_code_searcher-0.1.2 (c (n "http_status_code_searcher") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0hiai5sak70vmyz9w1lgv6c02z8vkv11miki9pzz07slks39y536")))

(define-public crate-http_status_code_searcher-0.1.3 (c (n "http_status_code_searcher") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1h4s7bzvndgsrbp64fq3arhq067ca8jznydz4305ayc0nxz11wld")))

