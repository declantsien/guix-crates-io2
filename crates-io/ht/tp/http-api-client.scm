(define-module (crates-io ht tp http-api-client) #:use-module (crates-io))

(define-public crate-http-api-client-0.1.0 (c (n "http-api-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "http-api-endpoint") (r "^0.1") (d #t) (k 0)))) (h "064anslmnw4p51967vw17ykx3q4av8wkdqib0y68mpiw56nmz8gy") (y #t)))

(define-public crate-http-api-client-0.1.1 (c (n "http-api-client") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "http-api-endpoint") (r "^0.1") (d #t) (k 0)))) (h "110s8pb8w193na9bbdacnfpcam1agn7qbwwkv2r1bimq7lvp0vki") (y #t)))

(define-public crate-http-api-client-0.1.2 (c (n "http-api-client") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "http-api-endpoint") (r "^0.1") (d #t) (k 0)))) (h "041nfvqh9cmg6233gcqsd05h5zqil1y8jw2m58xj8i50nhymxm2m")))

(define-public crate-http-api-client-0.2.0 (c (n "http-api-client") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)))) (h "02y41h2630hady0kf5arikdb5bv1mmivkg1jn3m3fkydwd2gjw55")))

(define-public crate-http-api-client-0.2.1 (c (n "http-api-client") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)))) (h "1qad9vnl9fc5aakw7nmvjzmmlg0jgvp7zfp35rj59gpc0ax57zfk")))

