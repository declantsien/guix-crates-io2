(define-module (crates-io ht tp http-bytes) #:use-module (crates-io))

(define-public crate-http-bytes-0.1.0 (c (n "http-bytes") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "httparse") (r "^1.3.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "proptest-http") (r "^0.1.0") (d #t) (k 2)))) (h "0mgdbgaciinv15g98q3wdrhw3ycqq0isir9iiid4h3a44imrhcik") (f (quote (("default" "basicauth") ("basicauth" "base64" "percent-encoding"))))))

