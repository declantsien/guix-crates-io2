(define-module (crates-io ht tp httptest_server) #:use-module (crates-io))

(define-public crate-httptest_server-0.1.0 (c (n "httptest_server") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httptest_core") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0dgaymjah7iy5zgbyi9i8s8qlvs47gs2wmdqak9mip7lja1gnjp3")))

