(define-module (crates-io ht tp httproxide-hyperlocal) #:use-module (crates-io))

(define-public crate-httproxide-hyperlocal-0.8.0 (c (n "httproxide-hyperlocal") (v "0.8.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-std" "io-util" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0grnm1cvb226fmylswy71p5nrm3xq1wb3sdiqsy0flmgp132gva1") (f (quote (("server" "hyper/http1" "hyper/server") ("default") ("client" "hyper/client" "hyper/http1"))))))

