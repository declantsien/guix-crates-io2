(define-module (crates-io ht tp httproxide-h3) #:use-module (crates-io))

(define-public crate-httproxide-h3-0.0.0 (c (n "httproxide-h3") (v "0.0.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.18") (d #t) (k 0)))) (h "15bfi1j3xdnl9crdqg59zkc4qmrgqxxjlsbnj02l2df01d9fas9n") (f (quote (("test_helpers"))))))

