(define-module (crates-io ht tp http-test-server) #:use-module (crates-io))

(define-public crate-http-test-server-1.0.0 (c (n "http-test-server") (v "1.0.0") (h "1373i61crx34nx0r1wdh18bxr27fmczih5v13lchb0536yaqr8jv")))

(define-public crate-http-test-server-1.0.1 (c (n "http-test-server") (v "1.0.1") (h "1hzs377xlq7wj55vbaalm7bv2lszhakcwc9d5fdps1xgk0hrpggf")))

(define-public crate-http-test-server-2.0.0 (c (n "http-test-server") (v "2.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jw5h11y2q85hvrw32n7b4k1c1bsdn2y0rqz6by1z78a9swsffaz")))

(define-public crate-http-test-server-2.0.1 (c (n "http-test-server") (v "2.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04cj9774rp9yl9pj26mkqlwyzrjzdxvabgxlgl0p4md434a7frgw")))

(define-public crate-http-test-server-2.1.0 (c (n "http-test-server") (v "2.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hx7vsp5yz1sq7y3vxpr05s39gvzp1m3chk326nrbil1ixqcb21j")))

(define-public crate-http-test-server-2.1.1 (c (n "http-test-server") (v "2.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hqaqkcjvw36lsph5ap2d16vq4i7j9zqqgj5xm8c06kvhcvfj1j3")))

