(define-module (crates-io ht tp http2hpack) #:use-module (crates-io))

(define-public crate-http2hpack-0.1.0 (c (n "http2hpack") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "12ky3166z06bixm4qy6jrrmhwhyjhd8mwkffsy7k0df6g78z5x7p") (y #t)))

(define-public crate-http2hpack-0.1.1 (c (n "http2hpack") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0dx4aii9r8ifpppslz34194rr9y1y037n23iiikzv4arh15h1xdn") (y #t)))

(define-public crate-http2hpack-0.1.3 (c (n "http2hpack") (v "0.1.3") (d (list (d (n "bitflags") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "lsio") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0jzvdrbw36j29r1wi1lrwk0bql9cz6bw7mj2cb7fl1anzbrbwpk2") (y #t)))

