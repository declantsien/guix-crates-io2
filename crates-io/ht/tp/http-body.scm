(define-module (crates-io ht tp http-body) #:use-module (crates-io))

(define-public crate-http-body-0.0.0 (c (n "http-body") (v "0.0.0") (h "14x9wbanjphrx1iycr56c6hrzzl5qy7flhqp4mghaai2is9zrhdb")))

(define-public crate-http-body-0.1.0 (c (n "http-body") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-buf") (r "^0.1.0") (k 0)))) (h "0b99404k4mw6a92hvyr0qwzkqv4f866ykg0x7913limjq5cwhhb7")))

(define-public crate-http-body-0.2.0-alpha.1 (c (n "http-body") (v "0.2.0-alpha.1") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)))) (h "12nrv6h7amr4lj1k619a6yypphx2q6k5i56kcpbasphmyshxzj0v")))

(define-public crate-http-body-0.2.0-alpha.2 (c (n "http-body") (v "0.2.0-alpha.2") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)))) (h "1mym89qi5ncz7q34j9zq0czzyw19y27wgjggmvhvv9jij5x04yd6")))

(define-public crate-http-body-0.2.0-alpha.3 (c (n "http-body") (v "0.2.0-alpha.3") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)))) (h "1hzf95bwj0zr037h0kbnnh04axxma3sz6rmkyn2qbgg27mpyyfhz")))

(define-public crate-http-body-0.2.0 (c (n "http-body") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0799aca5ymhxif9c7n7j87lk97kg98vz6j21gm88wh4bpscqk46l")))

(define-public crate-http-body-0.3.0 (c (n "http-body") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1dndma9g9fz3yj8swxr7d7a3w5rhxzrzfqwb5azqsdi84jnamx8i")))

(define-public crate-http-body-0.3.1 (c (n "http-body") (v "0.3.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "06qi0ni45lb92w3ml260c0bxbq5zd4snjmz0a9k69xq6021zzm8k")))

(define-public crate-http-body-0.4.0 (c (n "http-body") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "150rkibhyrcmsyi0qzwx4hp02c4lga1kkdg8j7l5wkh7xqkvsq98")))

(define-public crate-http-body-0.4.1 (c (n "http-body") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0dwpakklfqy17y0pgwkdjrzcqja99c5sxbk940mggqml4g0pgysx")))

(define-public crate-http-body-0.4.2 (c (n "http-body") (v "0.4.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1abmxi1mgv4qxawn4v1bxmp7b7ac835mg7isy2q6sy70w15s3nk0")))

(define-public crate-http-body-0.4.3 (c (n "http-body") (v "0.4.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1rc6c4n6wwmcfl6hg7fdilybqwyakqcsc888prh0qi3r54xmi71r")))

(define-public crate-http-body-0.4.4 (c (n "http-body") (v "0.4.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1imjszmk34603m7chfnhd3rq263bxbdlaxhlbzd06wv7354zix0z")))

(define-public crate-http-body-0.4.5 (c (n "http-body") (v "0.4.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1l967qwwlvhp198xdrnc0p5d7jwfcp6q2lm510j6zqw4s4b8zwym")))

(define-public crate-http-body-1.0.0-rc1 (c (n "http-body") (v "1.0.0-rc1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1w418xs9hgdj8cjm0d3262ksvhsl1xg9yy7axq95da55cd78hf7h")))

(define-public crate-http-body-1.0.0-rc.2 (c (n "http-body") (v "1.0.0-rc.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "038ml3ih3aw4sb3jq6iqkgf3b9h9f395pmn0j1ydc0mc68pgq7cm")))

(define-public crate-http-body-1.0.0 (c (n "http-body") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)))) (h "0hyn8n3iadrbwq8y0p1rl1275s4nm49bllw5wji29g4aa3dqbb0w")))

(define-public crate-http-body-0.4.6 (c (n "http-body") (v "0.4.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1lmyjfk6bqk6k9gkn1dxq770sb78pqbqshga241hr5p995bb5skw")))

