(define-module (crates-io ht tp http_stub) #:use-module (crates-io))

(define-public crate-http_stub-0.1.0 (c (n "http_stub") (v "0.1.0") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)))) (h "1ky3m1yixkfv0nxnqvbbha0d7kklvq30flagcwqxqrgajbwvalcr")))

(define-public crate-http_stub-0.1.1 (c (n "http_stub") (v "0.1.1") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.6") (d #t) (k 0)))) (h "16k35fbrfdqq0sx7k45ilqb856143xf75gz053w1n00g1r3pm96a")))

(define-public crate-http_stub-0.1.2 (c (n "http_stub") (v "0.1.2") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.6") (d #t) (k 0)))) (h "1k02y4gszfkhn1490fb0vcynh4l7a02mn5m1j0hkza4812hjm2mq")))

(define-public crate-http_stub-0.1.3 (c (n "http_stub") (v "0.1.3") (d (list (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.6") (d #t) (k 0)))) (h "1wa1s3f7lxcnzfw4krs85y5s1yya5r54n3glsq40j00drb5lnrzp")))

