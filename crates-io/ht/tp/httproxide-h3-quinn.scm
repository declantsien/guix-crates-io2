(define-module (crates-io ht tp httproxide-h3-quinn) #:use-module (crates-io))

(define-public crate-httproxide-h3-quinn-0.0.0 (c (n "httproxide-h3-quinn") (v "0.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "h3") (r "^0.0.0") (d #t) (k 0) (p "httproxide-h3")) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "quinn") (r "^0.8.0") (k 0)) (d (n "quinn-proto") (r "^0.8.0") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "02z3x9kjy38mgfsx7hj5mkkxg5xkfspgh6ik6yv45k34n4ihcglq")))

