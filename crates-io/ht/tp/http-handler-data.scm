(define-module (crates-io ht tp http-handler-data) #:use-module (crates-io))

(define-public crate-http-handler-data-0.1.0 (c (n "http-handler-data") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "dyn-clone") (r "^1.0") (k 0)))) (h "1lnnk5hlzg00m2nq3fr23r45fhmnv3avrbrrcb0yg75fjk5jw8sm") (f (quote (("std" "downcast-rs/std") ("default" "std"))))))

(define-public crate-http-handler-data-0.1.1 (c (n "http-handler-data") (v "0.1.1") (d (list (d (n "cloneable-any") (r "^0.1") (k 0)))) (h "1xvqh9zraf7qiy6s60cl5xlx9pbakfqrgsjn986h24zb6yrxj1ar") (f (quote (("std" "cloneable-any/std") ("default" "std"))))))

