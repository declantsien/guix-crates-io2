(define-module (crates-io ht tp http-api-endpoint) #:use-module (crates-io))

(define-public crate-http-api-endpoint-0.1.0 (c (n "http-api-endpoint") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "08avpmxpdx6mza4kyf8r6im5gy1b017s9ljpdl035vsq1jwz83b6") (y #t)))

(define-public crate-http-api-endpoint-0.1.1 (c (n "http-api-endpoint") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "1s71mpknmyspmjn7baa941p9ssw0c7rqikbk17gwinnhycirv94h") (y #t)))

(define-public crate-http-api-endpoint-0.1.2 (c (n "http-api-endpoint") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "1ba8qy0h91isbvmdyv05a0kl1zjvrclzx8gjjl52ckjcihvss8ng")))

(define-public crate-http-api-endpoint-0.2.0 (c (n "http-api-endpoint") (v "0.2.0") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)))) (h "05pvcyyi4kpa4zkjiz1zk9j8hc1bpzl4yx9r3dh8gv2jhf1dgmi7")))

