(define-module (crates-io ht tp http-service-hyper) #:use-module (crates-io))

(define-public crate-http-service-hyper-0.1.0 (c (n "http-service-hyper") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.1.4") (d #t) (k 0)) (d (n "hyper") (r "^0.12.24") (d #t) (k 0)))) (h "15byf41rlasgf38yav2z64mqanicgx5cwx1adkjzg2p7l8smp9ac")))

(define-public crate-http-service-hyper-0.1.1 (c (n "http-service-hyper") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (d #t) (k 0)))) (h "0df10ssr8zhypnyxjy0hvzbxamrvjqpkbb7gfvqvz91l1yjkq7nk")))

(define-public crate-http-service-hyper-0.2.0 (c (n "http-service-hyper") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (d #t) (k 0)))) (h "099qrly3b2zl73kn2pq3b8p2nv5sslf4hxrsyin49vq7b6ia2c0f")))

(define-public crate-http-service-hyper-0.3.0 (c (n "http-service-hyper") (v "0.3.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (k 0)) (d (n "romio") (r "^0.3.0-alpha.8") (d #t) (k 2)))) (h "0k4ijmax6aikxis4xch1j8ic4kw22j4c7aypbc3ygl8dr9k017ls") (f (quote (("runtime" "hyper/runtime") ("default" "runtime"))))))

(define-public crate-http-service-hyper-0.4.0 (c (n "http-service-hyper") (v "0.4.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.3.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (k 0)) (d (n "romio") (r "^0.3.0-alpha.8") (d #t) (k 2)))) (h "1nv7lijddf2lf0p82yshciw07gxbyc7qn443mjy51b73m7pqdvr7") (f (quote (("runtime" "hyper/runtime") ("default" "runtime")))) (y #t)))

(define-public crate-http-service-hyper-0.3.1 (c (n "http-service-hyper") (v "0.3.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (k 0)) (d (n "romio") (r "^0.3.0-alpha.8") (d #t) (k 2)))) (h "11wc9f68l7hf09lb31c4v23ibpv7qbf2qz7ask9ii5xmnjb86i71") (f (quote (("runtime" "hyper/runtime") ("default" "runtime"))))))

(define-public crate-http-service-hyper-0.4.1 (c (n "http-service-hyper") (v "0.4.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12.27") (k 0)) (d (n "romio") (r "^0.3.0-alpha.8") (d #t) (k 2)))) (h "1zwaxpyx7cycpv48qvaj9223v2s5k0mjzsi4jlpvizg0jjp5sgg3") (f (quote (("runtime" "hyper/runtime") ("default" "runtime"))))))

