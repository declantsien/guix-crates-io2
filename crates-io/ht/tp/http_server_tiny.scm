(define-module (crates-io ht tp http_server_tiny) #:use-module (crates-io))

(define-public crate-http_server_tiny-0.1.0 (c (n "http_server_tiny") (v "0.1.0") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1asv1lklg7n846rdfrb0blg390hy8px7d7ar21hi4jy2vfn4pgmj")))

(define-public crate-http_server_tiny-0.1.1 (c (n "http_server_tiny") (v "0.1.1") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1cwmds2xgdh1ak545slnglrkpwyb7i6m5bf7g6rd2lycs8k0v502")))

(define-public crate-http_server_tiny-0.1.2 (c (n "http_server_tiny") (v "0.1.2") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1z3icjx4bqj6pglk72m465n8jipzd8nrjnjbd4h3gczkw1m5gfja")))

(define-public crate-http_server_tiny-0.1.3 (c (n "http_server_tiny") (v "0.1.3") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0bifayda9nlganchg92vzscr7gp5gcy9plnabx1anz79djv62mk4")))

(define-public crate-http_server_tiny-0.1.4 (c (n "http_server_tiny") (v "0.1.4") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "18b19f631dwmkbmn38qcprf90mv88n7yqfixhrhm1z5xca7c8x9h")))

(define-public crate-http_server_tiny-0.1.5 (c (n "http_server_tiny") (v "0.1.5") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "06ygk5g3m0a36pycdjw9wnqjzivp0kjmx43mrkna30lanxlzwysn")))

(define-public crate-http_server_tiny-0.1.6 (c (n "http_server_tiny") (v "0.1.6") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "06xn4fwidbvic7y4phq35ylz5sllj6dm93v670igxr09q1lzmdmk")))

(define-public crate-http_server_tiny-0.2.0 (c (n "http_server_tiny") (v "0.2.0") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1ykw6ilzx3312d1sgiy7qk5grfgl5snqb4pax7s484iwccnf29dm")))

(define-public crate-http_server_tiny-0.2.1 (c (n "http_server_tiny") (v "0.2.1") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0zschxd7xn8yq62i4z3lnl3p16n7f12d2c9cgjpv8zqvpip15f3k")))

