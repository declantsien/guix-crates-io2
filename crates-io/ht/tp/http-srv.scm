(define-module (crates-io ht tp http-srv) #:use-module (crates-io))

(define-public crate-http-srv-0.1.0 (c (n "http-srv") (v "0.1.0") (d (list (d (n "html") (r "^0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "pool") (r "^0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r "^0.1.0") (d #t) (k 0) (p "url-utils")))) (h "12q3sjjh4vlc4wc31lf4gk8vnnbvqnys5zzbrc1917bzn9pr0vi8")))

(define-public crate-http-srv-0.1.1 (c (n "http-srv") (v "0.1.1") (d (list (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "023ajgp6ds8dzg0pw6m1rqjm4likvi0bflp2c2h1vsp4ngjqz7sv")))

(define-public crate-http-srv-0.1.2 (c (n "http-srv") (v "0.1.2") (d (list (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "mime") (r ">=0.1.0") (d #t) (k 0) (p "rmime")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "0dyagrqdi1fqd3y53v974h7sfnfw6nh1bvk7mbkg5n91hrinxa4x")))

(define-public crate-http-srv-0.1.3 (c (n "http-srv") (v "0.1.3") (d (list (d (n "base64") (r ">=0.1.0") (d #t) (k 0) (p "rb64")) (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "mime") (r ">=0.1.0") (d #t) (k 0) (p "rmime")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "0ns2n03v54k00qljygrbpc7yqsidvpgd41szhrkr1hqghq5420sh")))

(define-public crate-http-srv-0.1.4 (c (n "http-srv") (v "0.1.4") (d (list (d (n "base64") (r ">=0.1.0") (d #t) (k 0) (p "rb64")) (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "mime") (r ">=0.1.0") (d #t) (k 0) (p "rmime")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "1mi5zy9rfwss66lpxxibl9aqcxi1yqkn2wmlqcv0wr8c6pij9alj")))

(define-public crate-http-srv-0.1.5 (c (n "http-srv") (v "0.1.5") (d (list (d (n "base64") (r ">=0.1.0") (d #t) (k 0) (p "rb64")) (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "mime") (r ">=0.1.0") (d #t) (k 0) (p "rmime")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "0aayq5w6519sf6hh907fy9q844xy1v72846an93mg66xvfnggg6d")))

(define-public crate-http-srv-0.1.6 (c (n "http-srv") (v "0.1.6") (d (list (d (n "base64") (r ">=0.1.0") (d #t) (k 0) (p "rb64")) (d (n "html") (r ">=0.1.0") (d #t) (k 0) (p "rhtml")) (d (n "mime") (r ">=0.1.0") (d #t) (k 0) (p "rmime")) (d (n "pool") (r ">=0.1.0") (d #t) (k 0) (p "job-pool")) (d (n "url") (r ">=0.1.0") (d #t) (k 0) (p "url-utils")))) (h "0pjvys8iq78s368l25r5r6gq44vfpagnknj5srfwfyzkc0ib51a4")))

