(define-module (crates-io ht tp http-service) #:use-module (crates-io))

(define-public crate-http-service-0.0.0 (c (n "http-service") (v "0.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.9") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "0gww96n9r49w3qiq4qvscgjhvmyb3ndfr8ahkh1w2wd8fmqff3j5")))

(define-public crate-http-service-0.1.0 (c (n "http-service") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "1mwi6dzs0b20hw9pp4vzv2b1yspwc9jkkj10kh6wljawl4wnkp0x")))

(define-public crate-http-service-0.1.1 (c (n "http-service") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "0b1scbmmqdba60lvwdqv846jdzgmhj33xzz39bfljpk6948nq209")))

(define-public crate-http-service-0.1.2 (c (n "http-service") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "1j2wqna2fxaq7snp9qhjpq719g5cfi0f5vkqk61nzih9vg9gf934")))

(define-public crate-http-service-0.1.3 (c (n "http-service") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "1v5xpnbz4ynf95j616ss0vjyg0w3p7d7fx5nsqz66536zfvbh14q")))

(define-public crate-http-service-0.1.4 (c (n "http-service") (v "0.1.4") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "1zgzzgw81rzj0fw9mig9v034cb04fzlwkmaafi104jqk79aj5jx9")))

(define-public crate-http-service-0.1.5 (c (n "http-service") (v "0.1.5") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)))) (h "1hmnbdwqh3b8ya99chdf6yr08w8pqniy6yihx9rk0rn9d8lwbpj1")))

(define-public crate-http-service-0.2.0 (c (n "http-service") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.1.1") (d #t) (k 2)))) (h "108ilaif6dkrhl721j0h3mlpx75cyln9jlhghkxcpvd919malm4l")))

(define-public crate-http-service-0.3.0 (c (n "http-service") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.3.0") (d #t) (k 2)))) (h "1kw5scqkmn7wjd1ixg6h2nl04p8w7p3jd1zg9nac9s73nx4admld")))

(define-public crate-http-service-0.3.1 (c (n "http-service") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)))) (h "1rwdx127i13fq72hwmy68g4m0yqmpqbgh3kmy7vs8y6i1kngbj15")))

(define-public crate-http-service-0.4.0 (c (n "http-service") (v "0.4.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.0") (d #t) (k 0)))) (h "0267bbkwgncjk2j26iv58g42smhzwfwafi55g2zr9y7svl2zc9cn")))

(define-public crate-http-service-0.5.0 (c (n "http-service") (v "0.5.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)))) (h "0cndh0c6lmci771m1aa5x8mqvkh42xaanwch5svi4yjnm684ymrc")))

