(define-module (crates-io ht tp http-acl) #:use-module (crates-io))

(define-public crate-http-acl-0.1.0 (c (n "http-acl") (v "0.1.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0qccgpf2bkjavwiqycbhjfgx1mvr2mx9ki5lcd2dgwskyd8cpv2j")))

(define-public crate-http-acl-0.2.0 (c (n "http-acl") (v "0.2.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1vpbg2c93yk68r4jcfcvaaddq3q877nlbl7akwiyndh7d846207b")))

(define-public crate-http-acl-0.3.0 (c (n "http-acl") (v "0.3.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0v2imqg6s3cqh90mdm248zjzjbyxg10c7xhrxmlxqsdps2wmbssc")))

(define-public crate-http-acl-0.3.1 (c (n "http-acl") (v "0.3.1") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1syl1iv6cmj1rsxlm6k1myzmy781k57m6iqivv4kkjrvhn3brmzk")))

(define-public crate-http-acl-0.3.2 (c (n "http-acl") (v "0.3.2") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1hi9pbw8mmgljaa02zk05dy924i08r7ri0smrnf4m1xgzmm0d7zw")))

(define-public crate-http-acl-0.3.3 (c (n "http-acl") (v "0.3.3") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0cnailc3fa4nrwafa32a0ykddd257gx0888ccgx067cpan5l6h1l")))

(define-public crate-http-acl-0.4.0 (c (n "http-acl") (v "0.4.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1dv8zdjz6nqg7p2vn6rg1jqp0wd0b8b8g2zk05rs55wp5xr2r9g7")))

(define-public crate-http-acl-0.4.1 (c (n "http-acl") (v "0.4.1") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0mszq3i134zv92bp3w7ivc40filmqg8jxd56a29mmm2dlpzrf7c6")))

(define-public crate-http-acl-0.5.0 (c (n "http-acl") (v "0.5.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "139l1smdgp0dci17iq4ykf5hm9l2nq71d31vf9g6iaj50y8i34jg")))

(define-public crate-http-acl-0.5.1 (c (n "http-acl") (v "0.5.1") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0840ibg8vz3cl8g6lnh3mhzx7d682cs3yba95p56bpa9ykb5asgh") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

(define-public crate-http-acl-0.5.2 (c (n "http-acl") (v "0.5.2") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0q8j9nhw3204n15ifpwxyj6jdh0y4r1fa55racs80198qk61wyf1") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

(define-public crate-http-acl-0.5.3 (c (n "http-acl") (v "0.5.3") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1blc212d7vvfk3cak3c36va8dq8vq1jbxx88hkb5cvhfhc497q69") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

(define-public crate-http-acl-0.5.4 (c (n "http-acl") (v "0.5.4") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "03d5l8i505spbrjhsx24lq4c59smq6fsncln3c3fwksdvcan2ism") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

(define-public crate-http-acl-0.5.5 (c (n "http-acl") (v "0.5.5") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1dnj901h4z07l1rnc16xl7wycmr9l658v0nplnb3zlpdh5b9jpcz") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

(define-public crate-http-acl-0.5.6 (c (n "http-acl") (v "0.5.6") (d (list (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "14q1l9wma24fq8rvzbbkhyv1q01hd42mjmn6yld3d22vvjk4mf9s") (s 2) (e (quote (("serde" "dep:serde" "ipnet/serde"))))))

