(define-module (crates-io ht tp https-cert-info) #:use-module (crates-io))

(define-public crate-https-cert-info-0.1.0 (c (n "https-cert-info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 1)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "x509-parser") (r "^0.8.0") (d #t) (k 0)))) (h "04yag5baip8iq0k88wcyc1jyg00fsnm92w56f8hqx5wyd1g6bjmn")))

