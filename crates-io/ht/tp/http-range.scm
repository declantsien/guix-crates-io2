(define-module (crates-io ht tp http-range) #:use-module (crates-io))

(define-public crate-http-range-0.1.0 (c (n "http-range") (v "0.1.0") (h "1wxsd6dy5s7d3xc92s2r6g8vqa077l0r7khpfzg96dzbv4z20vwm")))

(define-public crate-http-range-0.1.1 (c (n "http-range") (v "0.1.1") (h "00jflbgwnwfpspr7ymiflh8za48bcxg4206v168yl1gxwq1l0bjz")))

(define-public crate-http-range-0.1.2 (c (n "http-range") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vp9xkn2l7m52ldppqnwn3wm5wd0wr0zi1gkaijyn8jbnsbgcb4i")))

(define-public crate-http-range-0.1.3 (c (n "http-range") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1slzlk47mqwv4rp79frpk03yzqzrcxcndbiza2bm80lmwly9xmq6")))

(define-public crate-http-range-0.1.4 (c (n "http-range") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1p4h9dj5vzwspqkz5g75hiw50f99d09j5czxhabc1dyrhd7nksgf")))

(define-public crate-http-range-0.1.5 (c (n "http-range") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wvm2p9jhbj6f9fbl1i7a0iz85nga37kx739v4p8fpqg27dwkpi1")))

