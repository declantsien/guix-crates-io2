(define-module (crates-io ht tp http-adapter) #:use-module (crates-io))

(define-public crate-http-adapter-0.1.0 (c (n "http-adapter") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0v4swxvxklbwgzqwbjs6zxyk1jqmmfyrccvm7ybav0h3xiwnfc3j")))

(define-public crate-http-adapter-0.1.1 (c (n "http-adapter") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "12by5l7qsckr7l386nrrkahk2h2gc5r2fs1qik219kn2yg2wjzqa")))

(define-public crate-http-adapter-0.2.0 (c (n "http-adapter") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)))) (h "1pcnf9r5hpyxlm9dz6r4iky9a3mzsz6xlxmxgf8aq85pf87jv82l")))

