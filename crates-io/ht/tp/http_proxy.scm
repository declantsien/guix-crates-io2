(define-module (crates-io ht tp http_proxy) #:use-module (crates-io))

(define-public crate-http_proxy-0.1.0 (c (n "http_proxy") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15b2i9cz4gj8ayh6zl1ndxw5fz2kyrrd53wv3gvk26spf843vvsw")))

(define-public crate-http_proxy-0.1.1 (c (n "http_proxy") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0v55qra8xdlra81rjfbmakibqi4zdgcn78g31cd74gwznm9v87nx")))

