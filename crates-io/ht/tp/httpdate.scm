(define-module (crates-io ht tp httpdate) #:use-module (crates-io))

(define-public crate-httpdate-0.1.0 (c (n "httpdate") (v "0.1.0") (h "1xqz8phnaldhik21xg28abkdxhhcmz3qsfg7r7bkp5rrrpbs1qbh") (f (quote (("unstable"))))))

(define-public crate-httpdate-0.2.0 (c (n "httpdate") (v "0.2.0") (h "141wcjkz0m75r8s72ahylkpv3c4l3krxkhdzrnjidr8m8ys5yn3f") (f (quote (("unstable"))))))

(define-public crate-httpdate-0.2.1 (c (n "httpdate") (v "0.2.1") (h "0vmlh5zgh5mbfxxx0dld4hlyjf6fnlx4k54h20d00dq51i3f5isx") (f (quote (("unstable"))))))

(define-public crate-httpdate-0.3.0 (c (n "httpdate") (v "0.3.0") (h "005f23dd7282p4d2xd32r22izxawwdbq5svb0wq9a7s2xvs6kfmf") (f (quote (("unstable"))))))

(define-public crate-httpdate-0.3.1 (c (n "httpdate") (v "0.3.1") (h "1l04s33s1dhr22ac30zbizjnay7jkjzwlx92nyc9s5x8awyhvrp9") (f (quote (("unstable"))))))

(define-public crate-httpdate-0.3.2 (c (n "httpdate") (v "0.3.2") (h "0izbd3sf0625wm4rrfv85xa4xa8j4n1ldxhwlkgff4cm6rh4sjs9") (f (quote (("unstable"))))))

(define-public crate-httpdate-1.0.0-alpha (c (n "httpdate") (v "1.0.0-alpha") (h "0vnj5d6qaj8k4736x6lyfly8k6waz80wr160ci3zg5f9s41kybky")))

(define-public crate-httpdate-1.0.0 (c (n "httpdate") (v "1.0.0") (h "1ffllpaayc3vfrxyj4h0ychjr4i60q7wn3h6nz626ar38c6jv105")))

(define-public crate-httpdate-1.0.1 (c (n "httpdate") (v "1.0.1") (h "0h240ck2xlp62dqfhfp6g45984avsmh1pkcgjmyywgzkr2kbhmk4")))

(define-public crate-httpdate-1.0.2 (c (n "httpdate") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "08bln7b1ibdw26gl8h4dr6rlybvlkyhlha309xbh9ghxh9nf78f4")))

(define-public crate-httpdate-1.0.3 (c (n "httpdate") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1aa9rd2sac0zhjqh24c9xvir96g188zldkx0hr6dnnlx5904cfyz") (r "1.56")))

