(define-module (crates-io ht tp http-encoding) #:use-module (crates-io))

(define-public crate-http-encoding-0.1.0 (c (n "http-encoding") (v "0.1.0") (d (list (d (n "brotli2") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1z12ih5p0sn14arhd9y0dv7f52b1wcdbiwirwb7jxp0x5p2l7b2c") (f (quote (("gz" "flate2") ("default") ("de" "flate2") ("br" "brotli2") ("all" "br" "gz" "de"))))))

(define-public crate-http-encoding-0.2.0 (c (n "http-encoding") (v "0.2.0") (d (list (d (n "brotli2") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0zmayqhd2wznm5cz3isc0kzn7j9wx7q6nw5wy8a9ybyqvl10yn8i") (f (quote (("gz" "flate2") ("default") ("de" "flate2") ("br" "brotli2") ("all" "br" "gz" "de"))))))

