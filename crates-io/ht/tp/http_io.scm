(define-module (crates-io ht tp http_io) #:use-module (crates-io))

(define-public crate-http_io-0.1.0 (c (n "http_io") (v "0.1.0") (h "0rra76x3gpn51q12gn05jk14rifdw3c25gbzx70h6j15kmgw9w21")))

(define-public crate-http_io-0.2.0 (c (n "http_io") (v "0.2.0") (h "1nbxabrv0yj3djqbnrq1ahjxxl7xdp54fs298wsmh5f0kzyk56i0")))

(define-public crate-http_io-0.2.1 (c (n "http_io") (v "0.2.1") (h "0pz08hhlm3wplw3zmihg9a5gx2i6cgb9603lrp7cvhvaa3vcz283")))

(define-public crate-http_io-0.2.2 (c (n "http_io") (v "0.2.2") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)))) (h "041qircgm65h3hvq1z2q8d6hamgh2i24m1hw10ym5rsg5rkvmdi6") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.3 (c (n "http_io") (v "0.2.3") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1dpvby4nd7dizalmvmh2hdpin0xh693ng292ij052vbs1j6zgsmk") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.4 (c (n "http_io") (v "0.2.4") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1ix9bhw2jnbn5pimysx245ciz1y3b1r8r73iliz27137madnz6fk") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.5 (c (n "http_io") (v "0.2.5") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "140cn50jk362n18x4hhja4q7wc10qp2cpyyl25p5pgnlf0hsdr1a") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.6 (c (n "http_io") (v "0.2.6") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "1zhrc9f56ihsfvqcfg3dy6nwaxxd65xayyd65sgk55r6p69z0g6n") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.7 (c (n "http_io") (v "0.2.7") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "0brmi61m4nbqqzcn5c0b7i49c72liziwmvr50ym11v0kn62h65py") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.8 (c (n "http_io") (v "0.2.8") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "0gm3i614kzn7pldzrx8kvb8iv1lib4hak4nn45i1jfxj8dr00gs4") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.9 (c (n "http_io") (v "0.2.9") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "02bwnam0jyzc9naqz0p67phc5qza44mxvjsfv6d45z667liph1zv") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.10 (c (n "http_io") (v "0.2.10") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "1gjbsv2wgik8zym04gb3dx4yxzj91jvi50311b5nzk43kj327qjl") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.11 (c (n "http_io") (v "0.2.11") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "1irha9mfwbxrxbvljxl8bnlaf6zdrxsi6r4lldwwcc396w7jn37z") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.12 (c (n "http_io") (v "0.2.12") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "03nc8w1pkr8dgb52f35llh8mpzwadyyngpn6i2vrvrs51c7sbyqk") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.13 (c (n "http_io") (v "0.2.13") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "0rw706rn6k3vzlybsbfsiy679gphrhl3nsjm6i9726nc5l2dr8ka") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.14 (c (n "http_io") (v "0.2.14") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "125mia3n9c9i8vr14yjpjbv6hvf873sgm169yhbbbnmr5ms4p879") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.15 (c (n "http_io") (v "0.2.15") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)))) (h "1bz8h45rvdhzzq6pmlniq0j59xnaqnjraykvwshkj4s6c0msczl5") (f (quote (("std") ("default" "std" "openssl"))))))

(define-public crate-http_io-0.2.16 (c (n "http_io") (v "0.2.16") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0wjmh0yq2cwi25xddh2zpbjm0bq5s1lmjwhiy7irvilkzhy9ndp5") (f (quote (("std") ("ssl-rustls" "ssl" "rustls" "webpki-roots" "rustls-pemfile") ("ssl-openssl" "ssl" "openssl") ("ssl-native-tls" "ssl" "native-tls") ("ssl") ("default" "std" "ssl-native-tls"))))))

(define-public crate-http_io-0.2.17 (c (n "http_io") (v "0.2.17") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "082cmvz07chpxhgar3lmxhgxjvdn4vh5shxmmybw5i512msnx3vh") (f (quote (("std") ("ssl-rustls" "ssl" "rustls" "webpki-roots" "rustls-pemfile") ("ssl-openssl" "ssl" "openssl") ("ssl-native-tls" "ssl" "native-tls") ("ssl") ("default" "std" "ssl-native-tls"))))))

(define-public crate-http_io-0.2.18 (c (n "http_io") (v "0.2.18") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 2)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0lsl5i6vx1fivfhy7piaw0nypafj06g7drd1krljx6h0y1sxrll3") (f (quote (("std") ("ssl-rustls" "ssl" "rustls" "webpki-roots" "rustls-pemfile") ("ssl-openssl" "ssl" "openssl") ("ssl-native-tls" "ssl" "native-tls") ("ssl") ("default" "std" "ssl-native-tls"))))))

