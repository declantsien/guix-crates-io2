(define-module (crates-io ht tp http_router_alt) #:use-module (crates-io))

(define-public crate-http_router_alt-0.2.0 (c (n "http_router_alt") (v "0.2.0") (d (list (d (n "hyper") (r ">= 0.12") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0clcins5nr84x6k9cc8j7xv9x6dg6m00b8q2hgrkckb0mw3vdd02") (f (quote (("with_hyper" "hyper") ("default" "with_hyper"))))))

