(define-module (crates-io ht tp http-auth-basic) #:use-module (crates-io))

(define-public crate-http-auth-basic-0.1.0 (c (n "http-auth-basic") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "1s4z4kvn0a1qp0nskvmc67fx48wbcgyrab9dv3sy3y6fmq5apy4m")))

(define-public crate-http-auth-basic-0.1.1 (c (n "http-auth-basic") (v "0.1.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "1hd5jg8sq3qbc3k7211zkzrz1q7cw1b299cgh0cgr3qwpwqdlkkl")))

(define-public crate-http-auth-basic-0.1.2 (c (n "http-auth-basic") (v "0.1.2") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "0ql5zqprbydhdz5sk98n05ydsy7jjjacnay0p0ip0phypzwlayl8")))

(define-public crate-http-auth-basic-0.1.3 (c (n "http-auth-basic") (v "0.1.3") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "1q3y7xar5rd5lg8r0041b5vlzpijkw9q977v6vs3bfblhjkbcsfz")))

(define-public crate-http-auth-basic-0.2.0 (c (n "http-auth-basic") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "1dzgyb584wvj6h10q2miy6yc0k26l91mykz7p85brwxakk16l0qr")))

(define-public crate-http-auth-basic-0.2.1 (c (n "http-auth-basic") (v "0.2.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "01wks751f8h1fdw0271mjkp3wwmdpa7j5c2ydjrnfnl16sm7in43")))

(define-public crate-http-auth-basic-0.2.2 (c (n "http-auth-basic") (v "0.2.2") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "0jmw6fl410gy7kf7ynizbr23g3yz5jy41p3i85q1q33j1p0wz2vr")))

(define-public crate-http-auth-basic-0.3.0 (c (n "http-auth-basic") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "0ryrigdb9yycg75lh26d4hb78b3fq0k3jbvxqr5lih8pmvqx42v0")))

(define-public crate-http-auth-basic-0.3.1 (c (n "http-auth-basic") (v "0.3.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)))) (h "0qjp98snpzlsv2xpj805nmlqf96l3bi4q21k9dlgldirf19wk4n6")))

(define-public crate-http-auth-basic-0.3.2 (c (n "http-auth-basic") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "013l4xy5qv7ldvw06j1a0n8n6y8lv0ybsf78h5izv7i20yyz0jfy")))

(define-public crate-http-auth-basic-0.3.3 (c (n "http-auth-basic") (v "0.3.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "1g0ykyy2gh4bbkflgbxxhw4kr22g9zzy567pvql28jkzrym1fbnx")))

