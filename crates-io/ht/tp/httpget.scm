(define-module (crates-io ht tp httpget) #:use-module (crates-io))

(define-public crate-httpget-0.1.9 (c (n "httpget") (v "0.1.9") (d (list (d (n "minreq") (r "^2.6.0") (d #t) (k 0)))) (h "1q8q9va1ll01k9s15ayalhj6pcfq2alwsc2j3zqvqgh7xbscq1x8") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.10 (c (n "httpget") (v "0.1.10") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "197pf4dphbwgffqly00nnxgm55dl7pkax5j5r6w6140r923c554m") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.12 (c (n "httpget") (v "0.1.12") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "1q41sijwjc7mil9hfdqysikkggdl4a4wp1pjr15k2167sfyr1lyy") (f (quote (("tls" "minreq/https-rustls") ("default")))) (y #t)))

(define-public crate-httpget-0.1.13 (c (n "httpget") (v "0.1.13") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "0vz6gbicpp2y9qm2zk88pwqb2ykg1avvjay4sbvr0ilr2fvsz99q") (f (quote (("tls" "minreq/https-rustls") ("default")))) (y #t)))

(define-public crate-httpget-0.1.14 (c (n "httpget") (v "0.1.14") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "0j9s5maq938kiiy38js87ihhaqd5l41qvr9mjv9a92dg7qxl70ma") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.15 (c (n "httpget") (v "0.1.15") (d (list (d (n "minreq") (r "^2.9.0") (d #t) (k 0)))) (h "04fa3f6cxvw9fnxa6arxk22z52x9fjpsrxmkvl3b6yphi400jc0s") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.16 (c (n "httpget") (v "0.1.16") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "0lgisrpyfr68y7ck6yc2x4i8ksg1gx2py9ai2qg5aa9d93cs0pjh") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.17 (c (n "httpget") (v "0.1.17") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "04f2294k36mdrn9y13c769gdl1mqyj9xdjmcmbi7zy6zvbxpz83j") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.18 (c (n "httpget") (v "0.1.18") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "1vp25wa4ddwfb8qs0k1nrl2f7551bsg44lx3dbk2q7jp52hvrfj9") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.19 (c (n "httpget") (v "0.1.19") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "0c22r9hgkdqsgzvppk9rfgsz1z1ls2bchrws15lzpx6y039rj2yp") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.20 (c (n "httpget") (v "0.1.20") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "1sqip94x8wyyfhzs20cg05hnp5pwy5z710p68vsjl4jc63qpjc5y") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

(define-public crate-httpget-0.1.22 (c (n "httpget") (v "0.1.22") (d (list (d (n "minreq") (r "^2.11.0") (d #t) (k 0)))) (h "1bbsb14jjk5qj8rmcc0i22mhw8f87pfyq3l91nklgjfrqzs9lzm7") (f (quote (("tls" "minreq/https-rustls") ("default"))))))

