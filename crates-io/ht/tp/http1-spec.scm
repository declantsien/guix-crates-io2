(define-module (crates-io ht tp http1-spec) #:use-module (crates-io))

(define-public crate-http1-spec-0.0.0 (c (n "http1-spec") (v "0.0.0") (h "1c05a0ipv6nphc29fyag706bfxjipm85a080rxbghmbhzz6421b5")))

(define-public crate-http1-spec-0.1.0 (c (n "http1-spec") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0j04dspbcjfy2m2l5h5yibblsxpawznqilfars7v8jf6z1fdm1m9")))

(define-public crate-http1-spec-0.1.1 (c (n "http1-spec") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0x2xypipi8fzmw80h6g5p8x28gwb88acy3rpbabz45wrvp6v29ny")))

(define-public crate-http1-spec-0.1.2 (c (n "http1-spec") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0hmkqwpfixlgzqbaagaz60zz22ximrnnrmfdjfgjyn63a5b62l5z")))

(define-public crate-http1-spec-0.1.3 (c (n "http1-spec") (v "0.1.3") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0cx2ccks3bxbicyjnkfw328hs3hycf2lwi7qdm53haj5j3312g5g")))

(define-public crate-http1-spec-0.2.0 (c (n "http1-spec") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "1m1b0a85r4x6iir82xfxswnkwmijyxc0pnwqfbw0iwax5hvy1mpf")))

(define-public crate-http1-spec-0.2.1 (c (n "http1-spec") (v "0.2.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "09iyczg3yiaw2hcq4y8d2lqbaz7mmhnd6fpmkdxxnkz7yds97ksj")))

(define-public crate-http1-spec-0.3.0 (c (n "http1-spec") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0prhs7rm7kbp8dnqwlpkghf1acbzw3k42i98kassv8cf9xicnm3r")))

(define-public crate-http1-spec-0.3.1 (c (n "http1-spec") (v "0.3.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "1dl00p2mrv97vg381g1b97k52lygls2v46hzv4z0yshynk9x4ndj")))

