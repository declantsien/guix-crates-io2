(define-module (crates-io ht tp http-box) #:use-module (crates-io))

(define-public crate-http-box-0.1.0 (c (n "http-box") (v "0.1.0") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "181lshv28pgjgx3qcyswxj7sng8cqyf63ngirbwi391mhv4ff3qm")))

(define-public crate-http-box-0.1.1 (c (n "http-box") (v "0.1.1") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "clippy") (r "^0.0.119") (d #t) (k 2)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 2)))) (h "11fb7k0s4i6wb8dzffm2ckg07vhpvz2p7jdg3q9frfq1b7da1pp0")))

(define-public crate-http-box-0.1.2 (c (n "http-box") (v "0.1.2") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "clippy") (r "^0.0.119") (d #t) (k 2)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 2)))) (h "0xw3hgg2nk8lb6ijjb5h0w6cps76g0zr58rn19d8km074wn03995")))

(define-public crate-http-box-0.1.3 (c (n "http-box") (v "0.1.3") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "clippy") (r "^0.0.119") (d #t) (k 2)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 2)))) (h "0vn7iz321plcd9ransy8f7mnlx54mlvl6d7s00fvl93m4zcyq06v")))

(define-public crate-http-box-0.1.4 (c (n "http-box") (v "0.1.4") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 2)))) (h "14v99dm0hxrifamh0fyjjddzlwivnkbavgnfkla8jv7hvnwmcy84")))

(define-public crate-http-box-0.1.5 (c (n "http-box") (v "0.1.5") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.7.1") (d #t) (k 2)))) (h "1z69pvjk0j6z7m2bxy1m2m73f7xk0v4p6m31yahgl322ldi7ib0n")))

(define-public crate-http-box-0.1.6 (c (n "http-box") (v "0.1.6") (d (list (d (n "byte-slice") (r "^0.1.11") (d #t) (k 0)) (d (n "byte-slice") (r "^0.1.11") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "1xbdgxjfvsnjk69jia1iyzv6k8fnxkkvwfvfvl388h6cs3j0yaq9")))

