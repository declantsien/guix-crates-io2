(define-module (crates-io ht tp httpd-util) #:use-module (crates-io))

(define-public crate-httpd-util-0.1.0 (c (n "httpd-util") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "1kgm00baxsm3f9hws24xkcl1vyixs32qphdqb6vl6jiv91gn5shn")))

(define-public crate-httpd-util-0.1.1 (c (n "httpd-util") (v "0.1.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "038y7kv7gb9sxzxqr8ashhd7j93322095qmm4p2nlq44hh3xjb03")))

(define-public crate-httpd-util-0.1.2 (c (n "httpd-util") (v "0.1.2") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "17g8n4qicf4pgnixwg4s82cixsz7m2q4smqmm8k6gbgh1ziw3da7")))

(define-public crate-httpd-util-0.1.3 (c (n "httpd-util") (v "0.1.3") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "1smaj532pdjxdvh00fni2jqaqmlqrdcc2im5i3ywjfnrw77ljxpq")))

(define-public crate-httpd-util-0.1.4 (c (n "httpd-util") (v "0.1.4") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.23.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "05sw1qdimz00y36637mcyddj6ga4hi0fbi27cpvckgzgbbjkvcdf")))

(define-public crate-httpd-util-0.1.5 (c (n "httpd-util") (v "0.1.5") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "pnet") (r "^0.23.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.7") (d #t) (k 0)))) (h "02x0jdz2jvwrpzf8yfzf5ir3qmqiaz5bap8zk2j7910xpg2h3683")))

(define-public crate-httpd-util-0.1.6 (c (n "httpd-util") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "pnet") (r "^0.26") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "1509z8k37k604r38g1cscsf0cjik44dr5lrmzymm08aggpa5hdrr")))

