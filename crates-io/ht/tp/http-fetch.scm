(define-module (crates-io ht tp http-fetch) #:use-module (crates-io))

(define-public crate-http-fetch-0.2.1 (c (n "http-fetch") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "0pss79ccj363xmgi745g18gncq8fq424f644lwwrf1fxg2adlvlj") (y #t)))

