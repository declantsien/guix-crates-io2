(define-module (crates-io ht tp http-dl-util) #:use-module (crates-io))

(define-public crate-http-dl-util-0.1.0 (c (n "http-dl-util") (v "0.1.0") (d (list (d (n "iced") (r "^0.6.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.5.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.7.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0yz6mn123sbjbjk9h4rfqrinigxrfxpn2ahcdmr1bh74qijx85x7") (y #t)))

(define-public crate-http-dl-util-0.1.1 (c (n "http-dl-util") (v "0.1.1") (d (list (d (n "iced") (r "^0.6.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.5.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.7.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0c58f0w0hw70i2xcch8c8xd1k426127mpw02bf4vxdsg9lzdqc8f")))

