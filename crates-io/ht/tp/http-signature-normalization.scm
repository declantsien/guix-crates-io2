(define-module (crates-io ht tp http-signature-normalization) #:use-module (crates-io))

(define-public crate-http-signature-normalization-0.1.0 (c (n "http-signature-normalization") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ycyjasypavsjzjc4pmfdfj9xwfmw6j6k65828s6gw4pid8k3000")))

(define-public crate-http-signature-normalization-0.1.1 (c (n "http-signature-normalization") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1681k0icm7jgi2d3ss595z22yizxz4q0k4p2gw2fjgdily0s4npm")))

(define-public crate-http-signature-normalization-0.2.0 (c (n "http-signature-normalization") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "01jk7jml41yv0671vilr2gr5qvkraxq274n6nasqzgc65rkkcxd5")))

(define-public crate-http-signature-normalization-0.3.0 (c (n "http-signature-normalization") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jmj27ms5y3c11qdfbv4h7gp0xj6y26zqcrla8kvpajaqsqrrnzj")))

(define-public crate-http-signature-normalization-0.4.0 (c (n "http-signature-normalization") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rj0l0a285kiipmkiprbr4iv2q8bj93qsgckbl87b4fnvqx882bg")))

(define-public crate-http-signature-normalization-0.4.1 (c (n "http-signature-normalization") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "052pbajv7iygyfwyf8b1h6i5vjklr1nyb41df7gcch2xbcjkay15")))

(define-public crate-http-signature-normalization-0.4.2 (c (n "http-signature-normalization") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "127095n7z6fxv1n15vpcfw0z4bzf7v3mwpcbq76s4pj63lrb1rpx")))

(define-public crate-http-signature-normalization-0.5.0 (c (n "http-signature-normalization") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00pdr71mvf3nv0lwb4x9n0slaasmy8xpfyhdhkcvmb6f4q4wdab7")))

(define-public crate-http-signature-normalization-0.5.1 (c (n "http-signature-normalization") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wz7npc76hl90x2l7xhhkk015qk18r8qm80ypiavprq36rak70k4")))

(define-public crate-http-signature-normalization-0.5.2 (c (n "http-signature-normalization") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "089zn8pk8nx7kfk1vg84nqmis33kcgxfrxla7awhvv1w86a754gf")))

(define-public crate-http-signature-normalization-0.5.3 (c (n "http-signature-normalization") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cawfsr9g4wawqddxzdbaqgnamc49ydrrbhgj5c253dl6w604fnb")))

(define-public crate-http-signature-normalization-0.5.4 (c (n "http-signature-normalization") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s891b32p8bb05k18f4xhwbsdvxgai2sm5f2lbfghc2mrb6rqj87")))

(define-public crate-http-signature-normalization-0.6.0 (c (n "http-signature-normalization") (v "0.6.0") (d (list (d (n "httpdate") (r "^1") (d #t) (k 0)))) (h "059mss8qg660vp3xraqlbb8dlibj9drwdg79swxymmw1xzdmmx7q")))

(define-public crate-http-signature-normalization-0.6.1 (c (n "http-signature-normalization") (v "0.6.1") (d (list (d (n "httpdate") (r "^1") (d #t) (k 0)))) (h "0vnaz5fkcl171p3nsjmqp625sl0pkqmsak8l3p0baw5vd67474wb")))

(define-public crate-http-signature-normalization-0.7.0 (c (n "http-signature-normalization") (v "0.7.0") (d (list (d (n "httpdate") (r "^1") (d #t) (k 0)))) (h "07hh9y1d82kkaibchfffz27rz5vphrmcqnr2spwz7rad354k2pmr")))

