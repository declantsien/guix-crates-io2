(define-module (crates-io ht tp http-request-derive-macros) #:use-module (crates-io))

(define-public crate-http-request-derive-macros-0.1.0 (c (n "http-request-derive-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "07q8vddgx7hzrfghf1znaz8d0sngs7818piq4p0rsplshx4j0vlm")))

(define-public crate-http-request-derive-macros-0.1.1 (c (n "http-request-derive-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "013p4py5jqbgdyb001y0j5r58x68g4mwixcjiq9f5yzidm4cbk40")))

(define-public crate-http-request-derive-macros-0.2.0 (c (n "http-request-derive-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1cm68gm2k240jd0shv8pjmi07p3j24ms21w3d9404aqg0cklbxpr")))

