(define-module (crates-io ht tp httparse) #:use-module (crates-io))

(define-public crate-httparse-0.0.1 (c (n "httparse") (v "0.0.1") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1mdvahv4jxzwjjvcralgjqpwxcwraimrsqr3jzm648f751kgchpf")))

(define-public crate-httparse-0.0.2 (c (n "httparse") (v "0.0.2") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "0cs3f8fiv89cf1dda60bimxb93z1bq077p4pq4d6nzc7blylqyzq")))

(define-public crate-httparse-0.0.3 (c (n "httparse") (v "0.0.3") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "06izxyy2mrfwmxanf0mc80ahgra8gdgwn44yf3mdda5ajpdp8xfp")))

(define-public crate-httparse-0.0.4 (c (n "httparse") (v "0.0.4") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "17dsnx8yi0p3cb40s1k4y6bhcy02a66rn612s36q3k7wa143s0lj")))

(define-public crate-httparse-0.0.5 (c (n "httparse") (v "0.0.5") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "0sl383930ivs3402x31bqmqm5mg3py9w4j02vy55fnz594p0lbvh")))

(define-public crate-httparse-0.0.6 (c (n "httparse") (v "0.0.6") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "07w16w3xg0v0g33dmgx7lf1g0s7xcibs61x2d20rfysl2sda0r9z")))

(define-public crate-httparse-0.1.0 (c (n "httparse") (v "0.1.0") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1k64vk57mpyaa77psdvk269vjg112vfgbkm45ysr4w9bniw6grn4")))

(define-public crate-httparse-0.1.1 (c (n "httparse") (v "0.1.1") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "09krsfczfzl22040ip8nxsg0qf6kqw51zxd03x9c00pgm4as43sd")))

(define-public crate-httparse-0.1.2 (c (n "httparse") (v "0.1.2") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "027wzdlrv6x4v4rrp5yj21lkk9skanmmqhgfav7n2pig84wf3hfn")))

(define-public crate-httparse-0.1.3 (c (n "httparse") (v "0.1.3") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1awz901q9glbiih0fmxvmsx33izgnc03nfg6ybljjvzbjqfjbvax")))

(define-public crate-httparse-0.1.4 (c (n "httparse") (v "0.1.4") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1736r8kpayxbs2gwbrnnvgfyjbw0bl38d6sibzz2vzjzkxvpg99c")))

(define-public crate-httparse-0.1.5 (c (n "httparse") (v "0.1.5") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "0lq3jrrqxxhfisi88sw4i3ccwkxvhq7sw7q4aizg1rgypmpjmm8r")))

(define-public crate-httparse-0.1.6 (c (n "httparse") (v "0.1.6") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "0vsr25abfhz5x8an6c2p3sj5d75z8yl3yiv027rlihxgbp9dfiv3")))

(define-public crate-httparse-1.0.0 (c (n "httparse") (v "1.0.0") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1y5ydnh0wy4pjj93a7rx0b94bmy71bc0q2p2487hs0jdy2kp5ga9")))

(define-public crate-httparse-1.1.0 (c (n "httparse") (v "1.1.0") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "1898c9zap8a5ifv7g53b8ad1igqg364dvbdgb4bb278nbpa5rsr7")))

(define-public crate-httparse-1.1.1 (c (n "httparse") (v "1.1.1") (d (list (d (n "pico-sys") (r "*") (d #t) (k 2)))) (h "0a8zmdh96khxw75iiy6pdvbp63d7ircssihwr3n2c1f2qn8f3n1y")))

(define-public crate-httparse-1.1.2 (c (n "httparse") (v "1.1.2") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "1bk77gvpw2l5d19akqca8c5a0qw9vp58xjx5c06hf2xqvds40ls6")))

(define-public crate-httparse-1.2.0 (c (n "httparse") (v "1.2.0") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "0knnrswwkqifix8i5zv2wgv0gkbmlnrlg57lg32347avf37bx2ka") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.2.1 (c (n "httparse") (v "1.2.1") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "0zdir7ycssappnnv4pv1k0q1lzbhp3xl2wbhmgxxz7hza4zadrx6") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.2.2 (c (n "httparse") (v "1.2.2") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "088fa1773jiyv2kz64wh0a3vfc9anial2kvpk3786fpfv6z5dxvp") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.2.3 (c (n "httparse") (v "1.2.3") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "01san131a43gp5l3gx1nds98kwva6kdj1965wwdgps2pfkcjsbxg") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.2.4 (c (n "httparse") (v "1.2.4") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "0drf5994937grw80fis51pf7yhipbm7bxzy0jny8mds5hw90gx62") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.2.5 (c (n "httparse") (v "1.2.5") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "1992y12c37qzw4hx7dhqskvhv9dxispba7p4nrnsvhsp543236i0") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.0 (c (n "httparse") (v "1.3.0") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "0hnybg8fr7s2hwj1gz7z7bpp6na5xysv9l94ij6bvgqdnj9mlba2") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.1 (c (n "httparse") (v "1.3.1") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "1zsj84ibdafn31382dh27mww79sbi4khb6vhwlc07v9fnjc1v013") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.2 (c (n "httparse") (v "1.3.2") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "0h75zqrql0bwn7grhxs5m43g5f6iib8d8gw72b5400qhvgbqhqkv") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.3 (c (n "httparse") (v "1.3.3") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "10vsfx1b8drhif08fbi0ha9d3v1f3h80w42rxh0y3hrvzl64nwz8") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.4 (c (n "httparse") (v "1.3.4") (d (list (d (n "pico-sys") (r "^0.0") (d #t) (k 2)))) (h "1yf23ldnjwfkkhkca7f4w15mky9961gjz28dlwyybhphc7l9l5yd") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.5 (c (n "httparse") (v "1.3.5") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "14anx05yjiwxd72kvmhxi085zvgl6m9r1k1csl9v6q1i5jzalp31") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.3.6 (c (n "httparse") (v "1.3.6") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "12cmrxq1vdgx4z93r68l3q8249w9qwjx89wsrxsc2gnrp6awjddw") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.4.0 (c (n "httparse") (v "1.4.0") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "0ds4grlxbbqnji8c30gr3yhrnhnc7mf31izxqa3lhxn9dw6y872a") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.4.1 (c (n "httparse") (v "1.4.1") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "0s2y7ki3sxyf1ynp0mm2k3mgafigy23d7g0rzci3rs9pdrhppa7k") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.5.0 (c (n "httparse") (v "1.5.0") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "1agy6xarsm3sm1wpshrbk59nrb6arzcvdngywvdax9pykr7dia3v") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.5.1 (c (n "httparse") (v "1.5.1") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "00smbrgb9vyzbbj3j5d66iarjl5x2wpfwkmhcy5nizylw7dlzndc") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.6.0 (c (n "httparse") (v "1.6.0") (d (list (d (n "pico-sys") (r "^0.0.1") (d #t) (k 2)))) (h "1i66wqc7lkfsi8h26sm8s6kirzzh1xgpx2dlrkzbfpz1h944204i") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.7.0 (c (n "httparse") (v "1.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1fmr81r28flk4fwn6zw077n4fwdv3y8q54vdzbrmkj6qdfiyhc33") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.7.1 (c (n "httparse") (v "1.7.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0k60q1hx96cvmjn6k3yjkff87fz0ga2a4z0g9ss8a9x5nndy4v29") (f (quote (("std") ("default" "std"))))))

(define-public crate-httparse-1.8.0 (c (n "httparse") (v "1.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "010rrfahm1jss3p022fqf3j3jmm72vhn4iqhykahb9ynpaag75yq") (f (quote (("std") ("default" "std"))))))

