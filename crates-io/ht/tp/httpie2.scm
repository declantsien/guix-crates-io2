(define-module (crates-io ht tp httpie2) #:use-module (crates-io))

(define-public crate-httpie2-0.1.0 (c (n "httpie2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jsonxf") (r "^1.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pzm1n1hf53hsr3x9khay9nbzfhjzg4j2ag46m36g4gniqqpvp8j")))

