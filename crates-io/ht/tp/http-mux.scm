(define-module (crates-io ht tp http-mux) #:use-module (crates-io))

(define-public crate-http-mux-0.1.0 (c (n "http-mux") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "tcp" "server"))) (d #t) (k 2)) (d (n "plumb") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1jhbd1qgzhd84vf7im2bqkmywkvg2jjj2wpx1fjjhgd416j0s6qw")))

(define-public crate-http-mux-0.1.1 (c (n "http-mux") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "tcp" "server"))) (d #t) (k 2)) (d (n "plumb") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18wwb4j9gzkz19g4j9245zcvq1m9xgc5x123gxmz8yafzi8rxr50")))

