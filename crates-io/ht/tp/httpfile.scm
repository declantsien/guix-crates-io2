(define-module (crates-io ht tp httpfile) #:use-module (crates-io))

(define-public crate-httpfile-0.1.0 (c (n "httpfile") (v "0.1.0") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "19vyhml5ma9jrcriayf19d2p7b0igfa1wyi4fkyvb4zmrrpz74hp")))

(define-public crate-httpfile-0.1.1 (c (n "httpfile") (v "0.1.1") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0i02c2bsysq1ni6d45k41jzhp1c0q3vknn3zsm5ndgw1l115r4hz")))

