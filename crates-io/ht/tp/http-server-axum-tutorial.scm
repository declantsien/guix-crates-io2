(define-module (crates-io ht tp http-server-axum-tutorial) #:use-module (crates-io))

(define-public crate-http-server-axum-tutorial-0.1.0 (c (n "http-server-axum-tutorial") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1j2sliflzw6axr3g08jp2wvn1wpgp7qlrf50w2z3wan61y6nllpb")))

