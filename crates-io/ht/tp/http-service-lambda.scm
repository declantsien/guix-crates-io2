(define-module (crates-io ht tp http-service-lambda) #:use-module (crates-io))

(define-public crate-http-service-lambda-0.1.0 (c (n "http-service-lambda") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http-service") (r "^0.3.0") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 2)) (d (n "simple_logger") (r "^1.3.0") (k 2)) (d (n "tide") (r "^0.2.0") (k 2)) (d (n "tokio") (r "^0.1.21") (d #t) (k 0)))) (h "013h69h044shhl2cvqnfbvg3qhzy6kql0pcpnwspfddbb2w65cj9")))

(define-public crate-http-service-lambda-0.3.1 (c (n "http-service-lambda") (v "0.3.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http-service") (r "^0.3.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 2)) (d (n "simple_logger") (r "^1.3.0") (k 2)) (d (n "tide") (r "^0.2.0") (k 2)) (d (n "tokio") (r "^0.1.21") (d #t) (k 0)))) (h "03srm8b4gmvajgx0jpmfynr738ap606fjzg02pqsd69i3kmdxfla")))

