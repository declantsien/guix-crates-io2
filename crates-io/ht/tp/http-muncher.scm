(define-module (crates-io ht tp http-muncher) #:use-module (crates-io))

(define-public crate-http-muncher-0.1.0 (c (n "http-muncher") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.5") (d #t) (k 1)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0j13si393ydj3rwmnmw9al2zhdn9hpnckg0pcdksp87167493mjs")))

(define-public crate-http-muncher-0.2.0 (c (n "http-muncher") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.5") (d #t) (k 1)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "10phg2f16gzygj59w7g19wzqqp07k6j52aalvxrzrzvnwmg442rk")))

(define-public crate-http-muncher-0.2.1 (c (n "http-muncher") (v "0.2.1") (d (list (d (n "gcc") (r "0.3.*") (d #t) (k 1)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0gxckd2y12ia95ldykl7zjvxwrdl3gbknaghm68hg4lwdhw4i4cm")))

(define-public crate-http-muncher-0.2.2 (c (n "http-muncher") (v "0.2.2") (d (list (d (n "gcc") (r "0.3.*") (d #t) (k 1)) (d (n "libc") (r "0.1.*") (d #t) (k 0)))) (h "1wq7s5dd0l354pjz2lph9dr8rrkqwbvhs0a3g5jxmy9qig7pxbr1")))

(define-public crate-http-muncher-0.2.3 (c (n "http-muncher") (v "0.2.3") (d (list (d (n "gcc") (r "0.3.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1gg4vsrqxk9yqjd57krgvadlfwm590viiwf591s62vjzg9hi95cr")))

(define-public crate-http-muncher-0.2.4 (c (n "http-muncher") (v "0.2.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y81m3q4w0ggnl25v44y2xahl4q94rlmpdc25f3rlhqn5g9k0q5g")))

(define-public crate-http-muncher-0.2.5 (c (n "http-muncher") (v "0.2.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f34ncq0xj5cbi13df8jis12irh1gcq5qcnwifm1gfwibfbk1563")))

(define-public crate-http-muncher-0.3.0 (c (n "http-muncher") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wvkc6ljlhmkhxydkf45crjgz75zhip6vqhqjxmyly3q2ddldni7")))

(define-public crate-http-muncher-0.3.1 (c (n "http-muncher") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hd7bl8rj7rbbl06gzlzkdxrc25ck87dkxpxzpsglnb4jjc8n38g")))

(define-public crate-http-muncher-0.3.2 (c (n "http-muncher") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10xjbxb14jczk0wd4nhdzyc1ggi3m4a1s8va88kxky44z2cg0pmd")))

