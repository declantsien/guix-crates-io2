(define-module (crates-io ht tp http-request-body) #:use-module (crates-io))

(define-public crate-http-request-body-0.0.0 (c (n "http-request-body") (v "0.0.0") (h "124cgl6jj8jw7sfbh9d8hfrkc2j876q9s9axxjgh0s10h45rnx5x")))

(define-public crate-http-request-body-0.1.0 (c (n "http-request-body") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper-request-body") (r "^0.2") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "warp-request-body") (r "^0.2") (o #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ag1cg72dc9rr1whhz387riw8fp0dq3x1kk2gy9k0pxlhnb84hdq") (f (quote (("default"))))))

