(define-module (crates-io ht tp http_app_tools) #:use-module (crates-io))

(define-public crate-http_app_tools-0.1.0 (c (n "http_app_tools") (v "0.1.0") (h "0yi48ivd756j7wxmparsb1nj0bwvw4sr5r2vxvb0bji2j4q8zd5c") (y #t)))

(define-public crate-http_app_tools-0.1.1 (c (n "http_app_tools") (v "0.1.1") (h "12xslxhxhygnv4jcn6mv8771k4qfiw7mpqv1gda9xw2519538v1d")))

(define-public crate-http_app_tools-0.1.3 (c (n "http_app_tools") (v "0.1.3") (h "14wpm6l8nl32sn541p6bx0rcy9ryzmyys22rhrvzxrfvgg9c51j8")))

(define-public crate-http_app_tools-0.1.4 (c (n "http_app_tools") (v "0.1.4") (h "1gzxlmq203n2qx0dw0w718mqbyy5rdln6r09r553g8a1id15rfs3")))

