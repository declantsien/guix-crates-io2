(define-module (crates-io ht tp httpstatus) #:use-module (crates-io))

(define-public crate-httpstatus-0.1.0 (c (n "httpstatus") (v "0.1.0") (h "13pyh769jpp22jj0q7xjzld0d66v7bha6dq2ihli3d936n31wg93")))

(define-public crate-httpstatus-0.1.1 (c (n "httpstatus") (v "0.1.1") (h "14vhx2gds8llpp032mahhm6grw7vb5nwm87lrary2hp3px52zy47")))

(define-public crate-httpstatus-0.1.2 (c (n "httpstatus") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00i0hcl12k42sqiz5968c2jqhs6s5h3vv6bcnkg97z3240d2ikyb")))

