(define-module (crates-io ht tp http_req) #:use-module (crates-io))

(define-public crate-http_req-0.1.0 (c (n "http_req") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "0bxd01299s441p7q3dq5952fdr7z4bn8hi5hj1kgvkadjbn5pycj")))

(define-public crate-http_req-0.1.1 (c (n "http_req") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "1rw2k3pw2sama2k3z8a2jnh3mia5l52f5g9fwg7x558irhlalhiq")))

(define-public crate-http_req-0.1.2 (c (n "http_req") (v "0.1.2") (d (list (d (n "native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "1niwpfj4f6dam1i252qpjhgpyw8qnbzcigzwxsw23xnw0avynakv")))

(define-public crate-http_req-0.2.0 (c (n "http_req") (v "0.2.0") (d (list (d (n "native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "1bwnn4gs9ykg3b5sk32ckyrdamrdkk29zbn144xmdw8kl5rj0z04")))

(define-public crate-http_req-0.2.1 (c (n "http_req") (v "0.2.1") (d (list (d (n "native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "04aww9iklgp988fb4mi6gwiq5qy7y7w3vndrzpkzbx8kb12b7z93")))

(define-public crate-http_req-0.3.0 (c (n "http_req") (v "0.3.0") (d (list (d (n "native-tls") (r "~0.2.2") (d #t) (k 0)))) (h "07czicz7jyabgpnhp50bxmzyr8dnlxzs6fkn1icnbqwimvbqzadh")))

(define-public crate-http_req-0.3.1 (c (n "http_req") (v "0.3.1") (d (list (d (n "native-tls") (r "~0.2.2") (d #t) (k 0)))) (h "0b3k6vcipsnqmma5ih9qv9as8x7v0zwm0jpa2ks0xa5r1y91qm0c")))

(define-public crate-http_req-0.4.0 (c (n "http_req") (v "0.4.0") (d (list (d (n "native-tls") (r "^0.2.2") (d #t) (k 0)))) (h "0xrisni9fz4x0fx8mv4rhbicwk4g8zcgg1xmpfcwb5wdxppg4j8i")))

(define-public crate-http_req-0.4.1 (c (n "http_req") (v "0.4.1") (d (list (d (n "native-tls") (r "^0.2.2") (d #t) (k 0)))) (h "12z5vzh5dxzfg0j6dgxg2fl3jmysbhax5bn6zp7kypivymwf998x")))

(define-public crate-http_req-0.4.2 (c (n "http_req") (v "0.4.2") (d (list (d (n "native-tls") (r "^0.2.2") (d #t) (k 0)))) (h "0n1klg2f37piavbp52a3a5prg6zl29mnrga4hnkkp1bir9kawjmb")))

(define-public crate-http_req-0.4.3 (c (n "http_req") (v "0.4.3") (d (list (d (n "native-tls") (r "^0.2.2") (d #t) (k 0)))) (h "0713fnl8mzr7phpxv12wdqzwkmd8j3sv8i2cj4amsv74fv64y7w4")))

(define-public crate-http_req-0.4.4 (c (n "http_req") (v "0.4.4") (d (list (d (n "native-tls") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1vbix21gm567kglz77j4hj99nzxl9vrbmcawb58ljm1qm8aafpv1") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.5 (c (n "http_req") (v "0.4.5") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "103rz1j5q05srj9qzlcc461q08mx7h129yrv7q28w8xpv1q4b2ra") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.6 (c (n "http_req") (v "0.4.6") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "131vilsr6100indlfjbxs12yxlswnaqwjwkajczpgn23dy83ys04") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.7 (c (n "http_req") (v "0.4.7") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1ph3bfs6ik3hgl78a5k1xqr6qpsvg4navp0pfj6fxazcgkxyjg5n") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.8 (c (n "http_req") (v "0.4.8") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1fzhpa8klfdh83xj6vspf02p00fkvyc8952pnbmsypfy1vlqnz3z") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.9 (c (n "http_req") (v "0.4.9") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1j94j44vmpzcsxxxrp378hbivx4lqxhs2k2g2qpp9hshfyhf7s17") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.10 (c (n "http_req") (v "0.4.10") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1qjrn5azq045791jq34zmdcm31ilcg29wf58rsmmip668wa9hwvp") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.4.11 (c (n "http_req") (v "0.4.11") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0cbcxl9kfzh2wyyq7rcvb773np801q3n1r24zinap8cjfa29k4lk") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.0 (c (n "http_req") (v "0.5.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "01kq2r87s4jcdjwf39aqfn88ccw5vnzkny1brf2jbq96j133rp06") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.1 (c (n "http_req") (v "0.5.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0npn92m1npv8yfa0nisnj7ag3rx6qhyjky07qbirf3mrlzj56w4v") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.2 (c (n "http_req") (v "0.5.2") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0vzjz6bcqvh33svv2jg5kdr0vxkabwacis3qk64b83bi395qyi22") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.3 (c (n "http_req") (v "0.5.3") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1v5kvanljbzks56bvymc9b6g2mbhmdb7k58r8jwfwfm9gf83acks") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.4 (c (n "http_req") (v "0.5.4") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0qaw43nwvvxbnqddxhb9fh9316dn64nmkzj08pq8n49qdy51xrys") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.5.5 (c (n "http_req") (v "0.5.5") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.4") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.18") (o #t) (d #t) (k 0)))) (h "18v3s249slh6fa2v5d8ykj58v446i907b8dxqvck01nq5idnp6pg") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.6.0 (c (n "http_req") (v "0.6.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1pkbc4n83rdk4kllkcijhixzw4i32v42ssmzk1swpg0zk0amj033") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.6.1 (c (n "http_req") (v "0.6.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.18") (o #t) (d #t) (k 0)))) (h "051gq1r172d4dm46w1zbq6p2a5p08dp2qaxsw5j9dl5454qkfprk") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.7.0 (c (n "http_req") (v "0.7.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0y64sipfhfh1cd2q8h1wln0w21h0h4adb8d4i0f0sa381wn9l5l3") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.7.1 (c (n "http_req") (v "0.7.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20") (o #t) (d #t) (k 0)))) (h "002zri361mnihr2hb72ppfr41iyllmqpdkjny4a6br91lq47qb55") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.7.2 (c (n "http_req") (v "0.7.2") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "17bflqmqlzn90gz8ns222hqsw9clxa8w3r05b2rbjqsikywizaqx") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.8.0 (c (n "http_req") (v "0.8.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0fmw063by838n1m6lnmnsxa6qn5w42988bbgq0mr29sph4wqz0kc") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.8.1 (c (n "http_req") (v "0.8.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0spxi41hdn8hbxqkididwc74d29cn2z89z826rak7zqd4xdd8v4y") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.9.0 (c (n "http_req") (v "0.9.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "09j9802bkbvg0jgcb77r9mfy34dg32mhg8qp1vykbcbha2llfaxs") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.9.1 (c (n "http_req") (v "0.9.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0gzhwhndprz2qfcliiigl1f0bvr9d1r741gnlwzg5by3jj5a69aq") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.9.2 (c (n "http_req") (v "0.9.2") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1y21j344705h4zb9ywydjyy0ypjss4ib61rxayysmr7by9vh2s4z") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.9.3 (c (n "http_req") (v "0.9.3") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1l1p044qx4yiic8zsiv13cfzychk5k3359935j7xcqn59v3k9kj2") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots") ("default" "native-tls"))))))

(define-public crate-http_req-0.10.0 (c (n "http_req") (v "0.10.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (o #t) (d #t) (k 0)))) (h "1850n241nn0k1aq17p22civzlq4xilxhc5238sqbvjbhrkd4x38m") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots" "rustls-pemfile") ("default" "native-tls"))))))

(define-public crate-http_req-0.10.1 (c (n "http_req") (v "0.10.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0346wxgmrks3yahk6y5mwhfj4xjsny8c8qqmpmx69xc9biqxhdm5") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots" "rustls-pemfile") ("default" "native-tls"))))))

(define-public crate-http_req-0.10.2 (c (n "http_req") (v "0.10.2") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (o #t) (d #t) (k 0)))) (h "042y0f4kgwhk32b4zan10fbdsanr9g34qrm0dk57w7yyx40lnfch") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots" "rustls-pemfile") ("default" "native-tls"))))))

(define-public crate-http_req-0.10.3 (c (n "http_req") (v "0.10.3") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0khh0f5cri7vi783fksgvk685zj5n04iq6xgfh3l98nhs8s9p6ix") (f (quote (("rust-tls" "rustls" "webpki" "webpki-roots" "rustls-pemfile") ("default" "native-tls"))))))

