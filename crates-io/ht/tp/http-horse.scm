(define-module (crates-io ht tp http-horse) #:use-module (crates-io))

(define-public crate-http-horse-0.1.0 (c (n "http-horse") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "fsevent") (r "^2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0chdmy9zpgprp02vzv5cmdk5ayvjb3y0r2vrhnk3dv1abb1ydzvx")))

