(define-module (crates-io ht tp http-timings) #:use-module (crates-io))

(define-public crate-http-timings-0.1.0 (c (n "http-timings") (v "0.1.0") (d (list (d (n "brotli") (r "^5.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "rustls-connector") (r "^0.20.0") (f (quote ("rustls-native-certs" "webpki-roots" "webpki-roots-certs"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1dfp7aydk72zmzhsy9ii2kxwi29c15r1hxw931k6hxcrcg16za6r") (y #t)))

(define-public crate-http-timings-0.1.1 (c (n "http-timings") (v "0.1.1") (d (list (d (n "brotli") (r "^5.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "rustls-connector") (r "^0.20.0") (f (quote ("rustls-native-certs" "webpki-roots" "webpki-roots-certs"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "05xq78i1fn014fp12l4h62kmj2fighcqp4l6cxvz1vkzsm3pyskk")))

(define-public crate-http-timings-0.1.2 (c (n "http-timings") (v "0.1.2") (d (list (d (n "brotli") (r "^5.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "rustls-connector") (r "^0.20.0") (f (quote ("rustls-native-certs" "webpki-roots" "webpki-roots-certs"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "184yql2b3ypk1ypigp0m6x0zcxmk2vrpi6p0qnfsb1wpbmf24k18")))

