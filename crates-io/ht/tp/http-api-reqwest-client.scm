(define-module (crates-io ht tp http-api-reqwest-client) #:use-module (crates-io))

(define-public crate-http-api-reqwest-client-0.1.2 (c (n "http-api-reqwest-client") (v "0.1.2") (d (list (d (n "http-api-client") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("time"))) (k 0)))) (h "0p07hm43wa46lb9rjwhq8053ddn23hckx24v7y87wkrxn246zca0")))

(define-public crate-http-api-reqwest-client-0.2.0 (c (n "http-api-reqwest-client") (v "0.2.0") (d (list (d (n "http-api-client") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("time"))) (k 0)))) (h "0cxnfw9c1hv0kkkfq3qncg66845rcw1vlm1zqxm0as11ia2zz72i")))

(define-public crate-http-api-reqwest-client-0.2.1 (c (n "http-api-reqwest-client") (v "0.2.1") (d (list (d (n "http-api-client") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (k 0)))) (h "0v35dg8x0n3fcsiv41ddngg41fjy8rjcmpdljjsjca0f8vf63rsn")))

