(define-module (crates-io ht tp https-everywhere-lib-wasm) #:use-module (crates-io))

(define-public crate-https-everywhere-lib-wasm-0.1.0 (c (n "https-everywhere-lib-wasm") (v "0.1.0") (d (list (d (n "https-everywhere-lib-core") (r "^0.1.0") (f (quote ("potentially_applicable"))) (k 0)) (d (n "js-sys") (r "^0.3.20") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "048d1y3vgxvxbwbmxh1vm6lk01h0g9vwgnf2nwsqw49vbh692fdj")))

(define-public crate-https-everywhere-lib-wasm-0.1.1 (c (n "https-everywhere-lib-wasm") (v "0.1.1") (d (list (d (n "https-everywhere-lib-core") (r "^0.1.1") (f (quote ("potentially_applicable"))) (k 0)) (d (n "js-sys") (r "^0.3.20") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "12ir8yh1q2fvrd4whpf1g56466xahcpzmv9k4ambrijhbqdyz7rf")))

(define-public crate-https-everywhere-lib-wasm-0.1.2 (c (n "https-everywhere-lib-wasm") (v "0.1.2") (d (list (d (n "https-everywhere-lib-core") (r "^0.1.1") (f (quote ("potentially_applicable"))) (k 0)) (d (n "js-sys") (r "^0.3.20") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0iqi61wiv5rynkfzqssmpg048rnib9k7hkvl9pcwd341dqqx1qkd")))

