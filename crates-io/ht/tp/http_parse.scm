(define-module (crates-io ht tp http_parse) #:use-module (crates-io))

(define-public crate-http_parse-1.0.0 (c (n "http_parse") (v "1.0.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "17hwhpg0r6j17shbdh6xr01v4f569fkvpykml0vyja2fbw85k10s")))

(define-public crate-http_parse-2.0.0 (c (n "http_parse") (v "2.0.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1bawss6hdhvf3fnpgscif9zz6a46d5lh2gl5x71245iwcbcyas45")))

(define-public crate-http_parse-2.0.1 (c (n "http_parse") (v "2.0.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "14kng6l01567qkw0raapxx3khh434ia9vrrr36hlmhdv6s4gg58s")))

