(define-module (crates-io ht tp http_cat) #:use-module (crates-io))

(define-public crate-http_cat-0.1.0 (c (n "http_cat") (v "0.1.0") (d (list (d (n "http") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg"))) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13hq80h0xi3hphzgcq32a3883c5b1hvz98dnmm773l1vkn7hy92s")))

