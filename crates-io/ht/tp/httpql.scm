(define-module (crates-io ht tp httpql) #:use-module (crates-io))

(define-public crate-httpql-0.1.0 (c (n "httpql") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hkdlp2rws299axhda7ziz2daxbn6q4v2p46600pvnqbcdnjrqm1")))

(define-public crate-httpql-0.1.1 (c (n "httpql") (v "0.1.1") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bjiqgf281ga0hvcwnk79r1aaywf9pyja9a5d7clzir1knqd4k5m")))

(define-public crate-httpql-0.1.2 (c (n "httpql") (v "0.1.2") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17rk34b0h6xr8ypr52rsrfzs029hsc9b04hvki4kwyy8s9451aiq")))

(define-public crate-httpql-0.1.3 (c (n "httpql") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jvlr1p89lcff23kl1pjwrh9k2k9rmh0yhvrwln375j03qd2d2p2")))

(define-public crate-httpql-0.1.4 (c (n "httpql") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "023dp1skwi8p0gb8b7jf4f51fpm4n650smfj59pgbxifks6c5v89")))

(define-public crate-httpql-0.1.5 (c (n "httpql") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rspw5gfi2mslqbcbnz34y2cn9iij9ic6314iby0vrnwfnb5v5yd")))

(define-public crate-httpql-0.1.6 (c (n "httpql") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jj6yhfdkym0x2h0ns8rvrcg8j6k15mvg816yzywsahnhrnzzyxc")))

(define-public crate-httpql-0.2.0 (c (n "httpql") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 2)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l1lknqvmq4qybk98mlslihlx74klr5la0p3csij4w2ln2wa1z0f")))

