(define-module (crates-io ht tp https_client) #:use-module (crates-io))

(define-public crate-https_client-0.0.1 (c (n "https_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "05xhq4x0h6amqlw5vzshj91lr9dvcjixc7pmmd82zv3ppf8ldk4c")))

(define-public crate-https_client-0.0.2 (c (n "https_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1nk5rmwnpvz0k2r5f4bzdmvi5lxxnwlyp442x55wyiwdhiaaqw3j")))

(define-public crate-https_client-0.0.3 (c (n "https_client") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1fidv51sli888gpsbig2nmh2s0n6iwy26829iwipj2nln9dvx54h")))

(define-public crate-https_client-0.0.4 (c (n "https_client") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.41") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0nkiyg8mb0rknrhjiwpqpxky7li4r2vlc6jvnxnk8miw6fyhr2zj")))

(define-public crate-https_client-0.0.6 (c (n "https_client") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "12i6h2pw73lqlk8v4jjargbdmi31q0iv703p30ciwbf0vz3r9fa1")))

