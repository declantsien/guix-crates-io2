(define-module (crates-io ht tp http-range-header) #:use-module (crates-io))

(define-public crate-http-range-header-0.2.1 (c (n "http-range-header") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "0xj66mb5qc0nnbr9zlxg2wnjli5hqbi29w36lhj36hsv2nfbnilk") (f (quote (("with_error_cause")))) (y #t)))

(define-public crate-http-range-header-0.2.2 (c (n "http-range-header") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "1k3bs3wpnm64qsskxyxx3d3qq3nh1wrjjqnv0xz6lsn6hdvqf3v5") (f (quote (("with_error_cause"))))))

(define-public crate-http-range-header-0.3.0 (c (n "http-range-header") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "0aas8c5dagfhcqpmqq9xw6a8nkl3lfg4g4mpddvyz1cj1bnqxzhb") (f (quote (("with_error_cause"))))))

(define-public crate-http-range-header-0.3.1 (c (n "http-range-header") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)))) (h "13vm511vq3bhschkw2xi9nhxzkw53m55gn9vxg7qigfxc29spl5d") (f (quote (("with_error_cause"))))))

(define-public crate-http-range-header-0.4.0 (c (n "http-range-header") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)))) (h "1zhigmvyrk50jzll6nmn7n1rxkfzh8xn0838dvdvnj52rlqyzr1w")))

(define-public crate-http-range-header-0.4.1 (c (n "http-range-header") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)))) (h "12hnsmb8kq3hk4z95ysc61gswnp91d4bxb8ic8ykwa7ckz29g8q8")))

