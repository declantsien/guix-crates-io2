(define-module (crates-io ht tp httpcodec) #:use-module (crates-io))

(define-public crate-httpcodec-0.1.0 (c (n "httpcodec") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1ag2pqsnviccn7aaaaqp6w2049gd3sm63b448jdwcj9crbdzl699")))

(define-public crate-httpcodec-0.1.1 (c (n "httpcodec") (v "0.1.1") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0yg1s2jbn33zmgwwdsy4nf0chixy4cdhfr16x782nf9d370lnpxr")))

(define-public crate-httpcodec-0.1.2 (c (n "httpcodec") (v "0.1.2") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0m1hv5nkchfi1vi776mgvi47v492bx00cw0zl4w00c8hg9a2s348")))

(define-public crate-httpcodec-0.1.3 (c (n "httpcodec") (v "0.1.3") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "14wgkly89k796r2ah6fkhyhqybaprmn9794k7cy4qv5di7gmfcmq")))

(define-public crate-httpcodec-0.1.4 (c (n "httpcodec") (v "0.1.4") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "18a2wfirfnk15gks9zfwh2zn1jspbx892ncf251qxvyii92xdalm")))

(define-public crate-httpcodec-0.1.5 (c (n "httpcodec") (v "0.1.5") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1ahnbb45yznjbi5kf9s005ikdlrqyvz43a7b4s6zi1qp25d9lwb2")))

(define-public crate-httpcodec-0.1.6 (c (n "httpcodec") (v "0.1.6") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "144rldn0wmi2pjzn1m5qswrx4h8gy73k6pfd0mh99b9yq0wjw3qh")))

(define-public crate-httpcodec-0.1.7 (c (n "httpcodec") (v "0.1.7") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1b1ac1hsf0wra1dwq4m4z5k2n94mjwzyj09zcgd2m2nvw1h3zia9")))

(define-public crate-httpcodec-0.1.8 (c (n "httpcodec") (v "0.1.8") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0z7ks7q2b6k1dmvq3mhzp03xd31mag778119ddb0d993j771vqij")))

(define-public crate-httpcodec-0.1.9 (c (n "httpcodec") (v "0.1.9") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "05kzz19nfv15gk9gg28b5fza5pdx9ka86kjm3pc132rwflx147l4")))

(define-public crate-httpcodec-0.1.10 (c (n "httpcodec") (v "0.1.10") (d (list (d (n "bytecodec") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "007n0862wmgkyfjzp2bi6dd7zjjykh52gc6cbl5qq4d32bdbjwsc")))

(define-public crate-httpcodec-0.2.0 (c (n "httpcodec") (v "0.2.0") (d (list (d (n "bytecodec") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "17v2m48g128y46cavvrnr51v8x7dkv10vdyr8bpv4bw6kkbf430l")))

(define-public crate-httpcodec-0.2.1 (c (n "httpcodec") (v "0.2.1") (d (list (d (n "bytecodec") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "15pkix3ah8dxfzynbw6d0862m1lx75vp1ld9vhq45rzflmbsh1jn")))

(define-public crate-httpcodec-0.2.2 (c (n "httpcodec") (v "0.2.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1pphxf9blyvd73n1fi6mzmyvvqfbx25nf3gya2q9fvrkrh2mwpr3")))

(define-public crate-httpcodec-0.2.3 (c (n "httpcodec") (v "0.2.3") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0x9788izsb2cfywgbk6r85dcj2xgiba7jfwl8gasn323a51xcjcz")))

