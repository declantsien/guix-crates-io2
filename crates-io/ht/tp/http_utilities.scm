(define-module (crates-io ht tp http_utilities) #:use-module (crates-io))

(define-public crate-http_utilities-0.1.0 (c (n "http_utilities") (v "0.1.0") (h "1gb56aw7qc6kkzw4rf0vb955cn99arayn28da5xws6kz4qah7i17")))

(define-public crate-http_utilities-0.1.1 (c (n "http_utilities") (v "0.1.1") (h "034kwaav08dvavyw2k4zd1s8mzadmvggirz5l0hb7jg9xd0cf5vh")))

(define-public crate-http_utilities-0.1.2 (c (n "http_utilities") (v "0.1.2") (h "1baggc1x6saz5h8f28046fqmpdgf6f85a7drpnk6f2kyglzhdnvp")))

