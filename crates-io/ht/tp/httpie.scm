(define-module (crates-io ht tp httpie) #:use-module (crates-io))

(define-public crate-httpie-0.0.1 (c (n "httpie") (v "0.0.1") (h "1yld7kzwmg1laq8i7bm57vj1yjwbwackvdfhs1jc3krcrzgscd1j")))

(define-public crate-httpie-0.0.2 (c (n "httpie") (v "0.0.2") (h "184fhwrlk9manhnm5ayfb7xrzxp9l1kh4j893m70g25kcdb34vif")))

