(define-module (crates-io ht tp http-basic-auth) #:use-module (crates-io))

(define-public crate-http-basic-auth-0.1.0 (c (n "http-basic-auth") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1q16gplx33w98qk87yhy6775y4myj28b6ijrj34lj7x3zvmgq7qp")))

(define-public crate-http-basic-auth-0.1.1 (c (n "http-basic-auth") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0gddbww2qgbh4rx16rfh7glgssx4nygi0rys31qk03rm7mqa8hw0")))

(define-public crate-http-basic-auth-0.1.2 (c (n "http-basic-auth") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "034kics7m2904prl73vb6y35ag3nipaa28bgh62p999k1rkcmyrc")))

