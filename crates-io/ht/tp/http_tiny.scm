(define-module (crates-io ht tp http_tiny) #:use-module (crates-io))

(define-public crate-http_tiny-0.9.0 (c (n "http_tiny") (v "0.9.0") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "0njfs2xlsh4gc78vw6n1hi0sm674m3bmc6fb309p4gsmldmdj3dj")))

(define-public crate-http_tiny-1.0.0 (c (n "http_tiny") (v "1.0.0") (d (list (d (n "ebacktrace") (r "^0.4") (d #t) (k 0)))) (h "1qfj2lzyvx0qplhhw527ycnqh79447m6wmmmhlfilp5jbfp7664l")))

(define-public crate-http_tiny-1.0.1 (c (n "http_tiny") (v "1.0.1") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "134w1ccsgklawb2c6pnjny967hm4vdjgcm9n3yd1q534bxqhy4i8") (y #t)))

(define-public crate-http_tiny-1.0.2 (c (n "http_tiny") (v "1.0.2") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "1mhy4f9zdqgq39xv9a54k2y09sh8ca62wrvwhrm25kn4bw1a2nxw")))

(define-public crate-http_tiny-1.0.3 (c (n "http_tiny") (v "1.0.3") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "04jdahdzxz66bmkbcadszc5pb4k25wgr6ncl3yyspd5xk4fjjskk")))

(define-public crate-http_tiny-1.1.0 (c (n "http_tiny") (v "1.1.0") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "1ch0c93bq27pyi58ga6c2qh453d07dnvsijz6hm4w959j240n4jp")))

(define-public crate-http_tiny-2.0.0-pre.0 (c (n "http_tiny") (v "2.0.0-pre.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fl2w89qbw3igr3669ssj5d4zjyd4q0kgchvjzsy20qc8wrq9ha6")))

(define-public crate-http_tiny-2.0.0-pre.1 (c (n "http_tiny") (v "2.0.0-pre.1") (h "1bicvapd55d6x2m73dcvjbbhkslsgl8pi8qwanxqyh64a4r1pcyw")))

(define-public crate-http_tiny-2.0.0-pre.2 (c (n "http_tiny") (v "2.0.0-pre.2") (h "1p2g5qsj8j5iq0dg0072zfd2bpgcfzdll72hgvcydrs3lwh6gvz5")))

(define-public crate-http_tiny-2.0.0-pre.3 (c (n "http_tiny") (v "2.0.0-pre.3") (h "1knxr12s3xz63vfs17y20rcc8qsqbxhfb7jmmygrksfq6q5972k8")))

(define-public crate-http_tiny-2.0.0-pre.5 (c (n "http_tiny") (v "2.0.0-pre.5") (h "092aj8jcnzwa6bdiykwj9nn65wz9ya9w2mynxn13i6zq8f9qwapi") (f (quote (("default" "convenience") ("convenience"))))))

(define-public crate-http_tiny-2.0.0-pre.6 (c (n "http_tiny") (v "2.0.0-pre.6") (h "0421ihibl51akcvya20apvap20cj9vybz50kp44c0r084w9c7pjk") (f (quote (("default" "convenience") ("convenience"))))))

