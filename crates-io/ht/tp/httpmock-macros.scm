(define-module (crates-io ht tp httpmock-macros) #:use-module (crates-io))

(define-public crate-httpmock-macros-0.1.0 (c (n "httpmock-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0hix4n3dxw81zbd2rv1skwyz6ql50v2nak8jd10mbxzif1fp1sjc")))

(define-public crate-httpmock-macros-0.2.0 (c (n "httpmock-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "02b33j5wwad9yi0s9x5v8wzfjsk4ms3bgifjrxr1rg1j5wmaxdbd")))

(define-public crate-httpmock-macros-0.3.0 (c (n "httpmock-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0sl8w3ncsh0zy17whg5l23b4icrh0hh1il615ka258370my5bg78")))

(define-public crate-httpmock-macros-0.4.0 (c (n "httpmock-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "183ni33znn1vcafw1bhlpqy8r5h41bv7wddqk2r9mfffj582lav4")))

