(define-module (crates-io ht tp http-rust) #:use-module (crates-io))

(define-public crate-http-rust-0.1.0 (c (n "http-rust") (v "0.1.0") (h "06a20q8mvzc97rjzw0hq1r8sn2y61nazd9xz8d9xv16v437jmaxi")))

(define-public crate-http-rust-0.1.1 (c (n "http-rust") (v "0.1.1") (h "1b91j4hicn7657g2dvbdwh7v5ds06s0al1vf9r535mfj208xmmvb")))

(define-public crate-http-rust-0.1.2 (c (n "http-rust") (v "0.1.2") (h "0gqg8w2js159z668kxvdc1v7iry6lx2ckqmzrfgw6ifc8m0dg9mv")))

