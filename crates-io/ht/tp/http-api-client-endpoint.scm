(define-module (crates-io ht tp http-api-client-endpoint) #:use-module (crates-io))

(define-public crate-http-api-client-endpoint-0.2.0 (c (n "http-api-client-endpoint") (v "0.2.0") (d (list (d (n "dyn-clone") (r "^1.0") (o #t) (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "16sb6a71ypwxm9kfyb55hadhqwsf46y54bn2sjlw6x7yf6mxh0fh") (f (quote (("default"))))))

(define-public crate-http-api-client-endpoint-0.2.1 (c (n "http-api-client-endpoint") (v "0.2.1") (d (list (d (n "dyn-clone") (r "^1") (o #t) (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "03himpy3rf4bz6nx2smpflbqcd25lr7p09damfxjmi75zkcfx8vp") (f (quote (("default"))))))

