(define-module (crates-io ht tp httpbox) #:use-module (crates-io))

(define-public crate-httpbox-0.1.0 (c (n "httpbox") (v "0.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "0hlckppvflr092z87c60b2wdpbk6c7zq2n2ak5lrm9zj742yr002")))

