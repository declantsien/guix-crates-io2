(define-module (crates-io ht tp http-link) #:use-module (crates-io))

(define-public crate-http-link-1.0.0 (c (n "http-link") (v "1.0.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1yq18mlaa5qcwkgcfrd9j195ffpbbrnid0gl7psp8j0lsnwh5jdw")))

(define-public crate-http-link-1.0.1 (c (n "http-link") (v "1.0.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "180pj6zw7g9hs0xxjafmvcs9blbz2s582jacj1bdkf5sj70iy3sh")))

