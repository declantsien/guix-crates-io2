(define-module (crates-io ht tp http-pull-parser) #:use-module (crates-io))

(define-public crate-http-pull-parser-0.1.0 (c (n "http-pull-parser") (v "0.1.0") (d (list (d (n "http-muncher") (r "^0.2.3") (d #t) (k 0)))) (h "0y0ifjg176w740rdw9mzzcx9ijxfpnbzyxnfqhdyw93g9jx56mn6")))

(define-public crate-http-pull-parser-0.1.1 (c (n "http-pull-parser") (v "0.1.1") (d (list (d (n "http-muncher") (r "^0.2.3") (d #t) (k 0)))) (h "0vh5fwf8cfhsvpyixihz4r3jrvhczqcbv8ly9q71yvg99iil56r8")))

(define-public crate-http-pull-parser-0.1.2 (c (n "http-pull-parser") (v "0.1.2") (d (list (d (n "http-muncher") (r "^0.2.3") (d #t) (k 0)))) (h "00a8drlvd3m096kv719ikr72almfn0m2vividzywyl62d6sqd613")))

(define-public crate-http-pull-parser-0.1.3 (c (n "http-pull-parser") (v "0.1.3") (d (list (d (n "http_parser") (r "^0.0.2") (d #t) (k 0)))) (h "0jlk0sv8m1mvcc9w1yywm6ln9jhqbds16vbd2nfr8dz3f4mppji8")))

(define-public crate-http-pull-parser-0.1.4 (c (n "http-pull-parser") (v "0.1.4") (d (list (d (n "http_parser") (r "^0.0.2") (d #t) (k 0)))) (h "1w8ir1nay4i05940za0386qfczgwx34ra4y26fphnv3j9q8h68ca")))

