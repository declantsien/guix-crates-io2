(define-module (crates-io ht tp httpserv) #:use-module (crates-io))

(define-public crate-httpserv-0.5.0 (c (n "httpserv") (v "0.5.0") (h "1xzajd6vqndxmp6ly64i9yxx1mfn13ysdpwdkj3s3s528r41slja")))

(define-public crate-httpserv-0.5.1 (c (n "httpserv") (v "0.5.1") (h "1m7qmlphb2jjzqbdyrncinyg8z92vd3gbzwpbd56rxq9jp2cmqna")))

(define-public crate-httpserv-0.5.2 (c (n "httpserv") (v "0.5.2") (h "0y978bjm8f37yj9nghjb8gllivbnm9bj99lyykha6qmwz261y4ym")))

(define-public crate-httpserv-0.5.3 (c (n "httpserv") (v "0.5.3") (h "1jvvpmsnifxj8b1d8fpbzjaj4in4hw82q09zvv7mk7a4a38pk5yy")))

(define-public crate-httpserv-0.5.4 (c (n "httpserv") (v "0.5.4") (h "1liwsb7xcgk757shv5i5h278vvcb8n9j1mj7l8gplagidzsl0m5h")))

(define-public crate-httpserv-0.6.0 (c (n "httpserv") (v "0.6.0") (h "05fphbm0k2zdf79n3dcgjih0lcrk8k4zqkp4ka0c7zm63fs8z6x7")))

(define-public crate-httpserv-0.6.1 (c (n "httpserv") (v "0.6.1") (h "1rinxi6si6dldi08x2l597k5hjl2z39m2hap1a2ms9yx05wsyvdq")))

(define-public crate-httpserv-1.0.0 (c (n "httpserv") (v "1.0.0") (h "1jvhd4srr5ckmqal6x2pvmykpddw6wxl0x63pvdjas9p6x4kh762")))

(define-public crate-httpserv-1.0.1 (c (n "httpserv") (v "1.0.1") (h "1lb976yajgz0nqna9z500fsi1h27gmyd3ds6xmkbvmmq8rix1llj")))

(define-public crate-httpserv-1.0.2 (c (n "httpserv") (v "1.0.2") (h "002jnyhl6jr5rvif02sz23n9y904lrql28rzyrbdxfsxnrkfkw9j")))

(define-public crate-httpserv-1.0.3 (c (n "httpserv") (v "1.0.3") (h "0x4fjx1l9f2y4628fxhzyf4kai5dl30y62gcpaqqhnvw19gg3sqs")))

