(define-module (crates-io ht tp http-request-uri) #:use-module (crates-io))

(define-public crate-http-request-uri-0.1.0 (c (n "http-request-uri") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "0c60s1p5algwf9nvr5c3avl9pkpb070qd6vl6krhj0gnxw82jc0m") (y #t)))

(define-public crate-http-request-uri-0.1.1 (c (n "http-request-uri") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "1dhg43nk4d7j0cdibdbb41n9aiksahb1kcvz6z904frn94as2dzj") (y #t)))

(define-public crate-http-request-uri-0.2.0 (c (n "http-request-uri") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (o #t) (k 0)))) (h "1vvb3py2hkqnm1fg0xy1x55j6kdavi4gdicmdjpbs7if25lacd00") (f (quote (("with-http" "http") ("default" "with-http"))))))

(define-public crate-http-request-uri-0.2.1 (c (n "http-request-uri") (v "0.2.1") (d (list (d (n "http") (r "^0.2") (o #t) (k 0)))) (h "13fzhpgzdwkarpiynam4p8dkh5zkfj79ccb4glpnn0zvf5vzv77s") (f (quote (("default" "http"))))))

