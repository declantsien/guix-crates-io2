(define-module (crates-io ht tp httpdir) #:use-module (crates-io))

(define-public crate-httpdir-0.1.0 (c (n "httpdir") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls" "gzip" "deflate"))) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "12580pqvd5jf9q44a9n11m0gqxlw1jnq5xd29dm128zmx73m36d4")))

