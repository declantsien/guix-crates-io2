(define-module (crates-io ht tp http_router) #:use-module (crates-io))

(define-public crate-http_router-0.1.0 (c (n "http_router") (v "0.1.0") (d (list (d (n "hyper") (r ">= 0.12") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1958ahql6l1yinnjwsxv6zxfx1zqv3xqhsq0pq9fcbs4bg7g3dbf") (f (quote (("with_hyper" "hyper") ("default" "with_hyper"))))))

