(define-module (crates-io ht tp httping) #:use-module (crates-io))

(define-public crate-httping-0.1.0 (c (n "httping") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pnd2rblja02cxv15mdkjaa81dmbqh8qpray5cr4vc4clxad7hr3") (y #t)))

(define-public crate-httping-0.1.1 (c (n "httping") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ivdqycrr9la1v7zzi49y27cqchnch6jf5jlhcilld0acg7fxcl2") (y #t)))

(define-public crate-httping-0.1.4 (c (n "httping") (v "0.1.4") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fvgs7d7mda6cswffb4lwp5ip477nq6b6s4qai5qrqy5hla11zi8") (y #t)))

(define-public crate-httping-0.1.5 (c (n "httping") (v "0.1.5") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01mqd66icnsazj5ssfir2gnvr872g9sgq0qmybyjywkpz71wds33") (y #t)))

(define-public crate-httping-0.1.6 (c (n "httping") (v "0.1.6") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l01mggzz04rdbl9s3lnx40q53h495g3af0kq4jzd75wgdvp3qmh") (y #t)))

(define-public crate-httping-0.1.7 (c (n "httping") (v "0.1.7") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10m9lyqhc9930p3jdh3zg4kgqc1w1aaa0nva76iiq6198ch0iggb") (y #t)))

(define-public crate-httping-0.1.8 (c (n "httping") (v "0.1.8") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gn8b7jpyj1bi8m06l8j3kdy5pcvyx4zmnynq8pbs5ahb7dk8p2q") (y #t)))

(define-public crate-httping-0.1.9 (c (n "httping") (v "0.1.9") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ryq4y1i49fsqjzmf6r54lagk2kh31mr1xxwir7y3cld10dfhyc2")))

