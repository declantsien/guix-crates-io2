(define-module (crates-io ht tp http-path-params) #:use-module (crates-io))

(define-public crate-http-path-params-0.1.0 (c (n "http-path-params") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "dyn-clone") (r "^1.0") (k 0)))) (h "1h700kgxj1r5z554bwyslpm9cgj9k88llz7sdr529rq9zsakjk2q") (f (quote (("std" "downcast-rs/std") ("default" "std")))) (y #t)))

(define-public crate-http-path-params-0.2.0 (c (n "http-path-params") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "dyn-clone") (r "^1.0") (k 0)))) (h "1k4kx8nq0ady1yl3ps3yj14b91zn78mxczm4zl0g06570j1wj6m6") (f (quote (("std" "downcast-rs/std") ("default" "std"))))))

(define-public crate-http-path-params-0.2.1 (c (n "http-path-params") (v "0.2.1") (d (list (d (n "downcast-rs") (r "^1") (k 0)) (d (n "dyn-clone") (r "^1") (k 0)))) (h "1jscv55gxv5wl7lcnlkfkbqnvpw9xxhadmjhjrqv3x7xqvq6jh7v") (f (quote (("std" "downcast-rs/std") ("default" "std"))))))

