(define-module (crates-io ht tp http_tools) #:use-module (crates-io))

(define-public crate-http_tools-0.1.0 (c (n "http_tools") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "15zrzrl0cxb0hwmrljk415w216d6yagxb451z3qnbpcrgbswmh49")))

(define-public crate-http_tools-0.1.1 (c (n "http_tools") (v "0.1.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "0snjq9lrnl9yfaj8a32c7x66f8kllcp146mrrdcmvr5139na58wp")))

(define-public crate-http_tools-0.1.2 (c (n "http_tools") (v "0.1.2") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "1d07mfk8b3ncv4rzibn6lr45139s3dl6j7py0hc6x9ny319cajqc")))

(define-public crate-http_tools-0.1.3 (c (n "http_tools") (v "0.1.3") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "1ywlbhmz6gf11v89ngn7pzwgmmzwnbzc97m5glha5jc2r0d5n2i3")))

