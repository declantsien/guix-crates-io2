(define-module (crates-io ht tp http_query_params) #:use-module (crates-io))

(define-public crate-http_query_params-0.1.0 (c (n "http_query_params") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1nzd1zc823xd77qydmy1jf8hvn3615d1wrydwiyn8ra6a9gx49w6")))

