(define-module (crates-io ht tp httpfile-build) #:use-module (crates-io))

(define-public crate-httpfile-build-0.1.0 (c (n "httpfile-build") (v "0.1.0") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04iv3csahqd3g64zgrd0f338m82cm86b5kqi0743i4sibmk6y467")))

(define-public crate-httpfile-build-0.1.1 (c (n "httpfile-build") (v "0.1.1") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s7x3i6q8crcxzc7m4jlhxfc2zyz9a6iwb993x2sc4983w9d5s77")))

