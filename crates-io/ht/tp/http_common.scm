(define-module (crates-io ht tp http_common) #:use-module (crates-io))

(define-public crate-http_common-0.1.0 (c (n "http_common") (v "0.1.0") (h "105h0y8awpxqy357hdkdayljz42jb10yfnn3x8rf5ih88cj32b0y")))

(define-public crate-http_common-0.1.1 (c (n "http_common") (v "0.1.1") (h "1cwyvp40m60fkgqnvc0s6zngsisf1bgssqmd77z0p6c7j1npv3sw")))

(define-public crate-http_common-0.2.0 (c (n "http_common") (v "0.2.0") (h "000amgqvrm2yyr9681by6w4bwck3sjnzb96qg47gzycqz3kv3k57")))

(define-public crate-http_common-0.2.1 (c (n "http_common") (v "0.2.1") (h "10771c2dsmch1rk6js94i72hx83qbs10zjn5r2m8fc4vyrzmvjgy")))

(define-public crate-http_common-0.2.2 (c (n "http_common") (v "0.2.2") (h "0qpcimwnqbm7il5rg4vm4wzksnyiv6g201gn3750i51i68bsfqqz")))

