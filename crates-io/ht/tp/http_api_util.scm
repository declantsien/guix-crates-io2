(define-module (crates-io ht tp http_api_util) #:use-module (crates-io))

(define-public crate-http_api_util-0.1.0 (c (n "http_api_util") (v "0.1.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0il5f2x88ims8asm152n462b78yyqkf7nrfsxy4n8sr84dyyplzn")))

(define-public crate-http_api_util-0.1.1 (c (n "http_api_util") (v "0.1.1") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1z6wmmhwg44l2s5p5yq4v6p0swqi5364m12q58fili0s8ra9sg7r")))

(define-public crate-http_api_util-0.1.2 (c (n "http_api_util") (v "0.1.2") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0apzggd9fb9aa55r061ln1sicbp029vkf244c0s48nqh1wa011mh")))

