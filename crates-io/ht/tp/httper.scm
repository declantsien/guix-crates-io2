(define-module (crates-io ht tp httper) #:use-module (crates-io))

(define-public crate-httper-0.1.0 (c (n "httper") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "http") (r "^0.1.8") (d #t) (k 0)) (d (n "hyper") (r "^0.12.7") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "1wh81b6fx8ka2m6spsn19l2fz8yxk2kgzdm60155plyjdrhgbhjv")))

