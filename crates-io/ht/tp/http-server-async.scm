(define-module (crates-io ht tp http-server-async) #:use-module (crates-io))

(define-public crate-http-server-async-0.1.0 (c (n "http-server-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xszgn97s171f0k33gca8p7rg0w9p4b27231cya9daaiyjz4yj85") (f (quote (("http") ("fbbin") ("default" "fbbin"))))))

