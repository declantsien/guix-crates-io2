(define-module (crates-io ht tp http_str) #:use-module (crates-io))

(define-public crate-http_str-0.1.0 (c (n "http_str") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1clasi04q7g3wh896fx6fkfi6fgkb047a5hznvcn74a0vn01nwxl") (y #t)))

(define-public crate-http_str-0.1.1 (c (n "http_str") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19idjb9m01wadhi9qhy2mycyrnkrhllmpymdairjgmffxbfqqkzs") (y #t)))

(define-public crate-http_str-0.1.2 (c (n "http_str") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0107fxhs1bsqhmg3fg48cryciqn490zn0n970csc7b2qcahyvn9f") (y #t)))

(define-public crate-http_str-0.1.3 (c (n "http_str") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07yf6csmd6798mmpmz5sqglzz5xgv6k1jk3dlia1wqdx55f1a7nv") (y #t)))

