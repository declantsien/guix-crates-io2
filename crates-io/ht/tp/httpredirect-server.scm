(define-module (crates-io ht tp httpredirect-server) #:use-module (crates-io))

(define-public crate-httpredirect-server-0.1.0 (c (n "httpredirect-server") (v "0.1.0") (d (list (d (n "globwalk") (r "^0.7") (d #t) (k 0)))) (h "0w2iyf072jh5cjiq50viv84f523ppj76nqj107d5i9s4yb5rbs4j") (y #t)))

(define-public crate-httpredirect-server-0.1.1 (c (n "httpredirect-server") (v "0.1.1") (d (list (d (n "axum") (r "^0.6.15") (d #t) (k 0)) (d (n "globwalk") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.4.0") (f (quote ("full" "trace"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0grbw5ryv8prl8ya7w4fljf1w3yjgnyma6ls9mqc6kgxhg484szv") (y #t)))

