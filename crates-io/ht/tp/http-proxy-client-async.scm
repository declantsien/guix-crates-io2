(define-module (crates-io ht tp http-proxy-client-async) #:use-module (crates-io))

(define-public crate-http-proxy-client-async-0.3.1 (c (n "http-proxy-client-async") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1.3") (d #t) (k 0)) (d (n "merge-io") (r "^0.3") (d #t) (k 2)))) (h "05vib7a23p0jw13q3j8wc0p1zsnmfmdb08r7i9b24hydmk74cjnd")))

