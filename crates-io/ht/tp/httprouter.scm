(define-module (crates-io ht tp httprouter) #:use-module (crates-io))

(define-public crate-httprouter-0.0.0 (c (n "httprouter") (v "0.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "hyper") (r "^0.13.8") (d #t) (k 0)))) (h "0hxxba74q58si21jj57an0fclq737xxd6kckf3p6rh10279a1adr")))

(define-public crate-httprouter-0.1.0 (c (n "httprouter") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.9") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.13.9") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0021bcmqyf9k7y2j5s55fg0q60l22yznqzkbcxf5lf4sb0cpfwxs") (f (quote (("hyper-server" "hyper") ("default" "hyper-server"))))))

(define-public crate-httprouter-0.2.0 (c (n "httprouter") (v "0.2.0") (d (list (d (n "hyper") (r "^0.13.9") (d #t) (k 0)) (d (n "hyper") (r "^0.13.9") (d #t) (k 2)) (d (n "matchit") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0irj3x62ri8pv1yhgrl605i1r2ps89ynxy6jjm0irknk3v8dv2ci")))

(define-public crate-httprouter-0.3.0 (c (n "httprouter") (v "0.3.0") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper") (r "^0.13.9") (d #t) (k 0)) (d (n "hyper") (r "^0.13.9") (d #t) (k 2)) (d (n "matchit") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0cgq9r07cm84d3cmrfkj8n4d36w81ydakl5g4sfkv0s9qqsri6j8")))

(define-public crate-httprouter-0.4.0 (c (n "httprouter") (v "0.4.0") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "matchit") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("full"))) (d #t) (k 2)))) (h "15ky01wwagsz0fai224jssmvfj54p5vy5jhnmbkyy62lqhxw7hg3")))

(define-public crate-httprouter-0.5.0 (c (n "httprouter") (v "0.5.0") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "http1" "tcp"))) (d #t) (k 2)) (d (n "matchit") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("full"))) (d #t) (k 2)))) (h "0vsrfzdsq4n8mxxspm3mmqyxx5h75fjbkq9naq4yk2q8fc59dcn2")))

