(define-module (crates-io ht tp http_request_parser) #:use-module (crates-io))

(define-public crate-http_request_parser-0.1.0 (c (n "http_request_parser") (v "0.1.0") (h "00yvvbxvwxmlxcyw33khk3p8c9x77wcy08x88cmyxa7x9nvavzln")))

(define-public crate-http_request_parser-0.1.1 (c (n "http_request_parser") (v "0.1.1") (h "0hz7771aqj542pygswyf1l5fkz4zhv4qficp5sfql5kkb0p6zmnn")))

(define-public crate-http_request_parser-0.1.2 (c (n "http_request_parser") (v "0.1.2") (h "0wl21c0dlx3d5hqi1w4151l5ay1ifrl0k1kbx8zaxzyd4p7bv0nf")))

(define-public crate-http_request_parser-0.1.3 (c (n "http_request_parser") (v "0.1.3") (h "0dbhp467xjsbj214jidqn2h32jk9y0vwmzqk1rq5akpn3ap9d7pi")))

(define-public crate-http_request_parser-0.2.0 (c (n "http_request_parser") (v "0.2.0") (h "072gld18l6yj911ra24spgbp25yh19rhc58rhv151xr45nrm9n30")))

