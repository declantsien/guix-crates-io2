(define-module (crates-io ht tp http-serve-folder) #:use-module (crates-io))

(define-public crate-http-serve-folder-0.1.0 (c (n "http-serve-folder") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "14g8f1vcdg73syr5p9z8x0pla53s7b3j09v9j9i7v10rv5x2dxyw")))

(define-public crate-http-serve-folder-0.2.0 (c (n "http-serve-folder") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1j7iv9xqfs54dxhpq7g21cjp4zlf6yxlngkfasiqicp6k63n3lfk")))

