(define-module (crates-io ht tp http-stream) #:use-module (crates-io))

(define-public crate-http-stream-0.9.0 (c (n "http-stream") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fyb57a78rzmbnbx9acvr97b988ckd413zm9kp4gs0n5ixnvrikp")))

(define-public crate-http-stream-0.9.1 (c (n "http-stream") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lk5m9xqim2c0wr6fl9nc4qqdry3pvsn6nmqqby5cmbw9xrcb63w")))

(define-public crate-http-stream-0.9.2 (c (n "http-stream") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hrax2mqvd9bsbdvc6bhcwvxv8hzkx92n1yw8r35iyq6c0gxnpw1")))

(define-public crate-http-stream-0.10.0 (c (n "http-stream") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14kli9d6sx9n6z3cngrl7s5j1b46bi7gri94r63fjmb6wm3pvi91")))

(define-public crate-http-stream-0.11.0 (c (n "http-stream") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ywd7c471153cqzk1j524nrjkvlzpdf12fihifbr3ia4z4pkhdyh")))

(define-public crate-http-stream-0.12.0 (c (n "http-stream") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rryv5hw8q51qa2kxxjxip2smaq8h5kzyr5p7gcy37s0y1ggz66n")))

(define-public crate-http-stream-0.12.1 (c (n "http-stream") (v "0.12.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "057cqxmvq70bs85xpppa9sd0w8dfkf5nps25m6zp7jxzjsxr95wp")))

(define-public crate-http-stream-0.12.2 (c (n "http-stream") (v "0.12.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r6ai1sw8rmwp9k8284y29xr3f8fnhaw9i67a2n62cn5sdgmz2v3")))

(define-public crate-http-stream-0.13.0 (c (n "http-stream") (v "0.13.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jkx06d8j90hgf799zw969sc7715x60w5hldgw1i2ngk8qdx244h")))

(define-public crate-http-stream-0.14.0 (c (n "http-stream") (v "0.14.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rsx0pnhkafqm5z4f56mnwz6p39ak8xhk9zyzz10rgp0ag5przz1")))

(define-public crate-http-stream-0.14.1 (c (n "http-stream") (v "0.14.1") (d (list (d (n "percent-encoding") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04679jh5ccjyf68vhp5bhvayah2hk0bsk53sivi5fda6igv91yj4")))

(define-public crate-http-stream-0.14.2 (c (n "http-stream") (v "0.14.2") (d (list (d (n "percent-encoding") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17wbrb2ccqmw1l21ab6csj3q031v11n4dcnr540x5jmkbzxhpfip")))

