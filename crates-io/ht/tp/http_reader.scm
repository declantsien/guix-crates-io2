(define-module (crates-io ht tp http_reader) #:use-module (crates-io))

(define-public crate-http_reader-0.1.0 (c (n "http_reader") (v "0.1.0") (d (list (d (n "lru-cache") (r ">=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.1.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0malzrfdq1czvx1rfxl2k5h8p5g7rsm7hbghh12zdrn3xkxhzf6n") (y #t)))

(define-public crate-http_reader-0.1.1 (c (n "http_reader") (v "0.1.1") (d (list (d (n "lru-cache") (r ">=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.1.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04zhl7ycp9sxgzf9cw4w7ccxrv2f0gzinszdkhcqmd5sknx13b6q") (y #t)))

(define-public crate-http_reader-0.1.2 (c (n "http_reader") (v "0.1.2") (d (list (d (n "lru-cache") (r ">=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.1.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0jy33g92xxj4s6wx6mzikpamjyg711b8yxjqn8dzbswndj445m52")))

(define-public crate-http_reader-0.2.0 (c (n "http_reader") (v "0.2.0") (d (list (d (n "log") (r ">=0.1.0") (d #t) (k 0)) (d (n "lru-cache") (r ">=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.1.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "07n3qx6pqmh0v2xmqnj43anwi7c6m69dfwz06kfwcd6079pjs5c3")))

(define-public crate-http_reader-0.2.1 (c (n "http_reader") (v "0.2.1") (d (list (d (n "log") (r ">=0.1.0") (d #t) (k 0)) (d (n "lru-cache") (r ">=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.1.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0jvhm2c90vw1l0y2b4lgx76jdspxhbv5wxijgffr9a32rffv789c")))

