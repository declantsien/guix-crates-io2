(define-module (crates-io ht tp http_req_wasi) #:use-module (crates-io))

(define-public crate-http_req_wasi-0.10.0 (c (n "http_req_wasi") (v "0.10.0") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.4.2") (d #t) (k 0)))) (h "0x6h2jaznj8qb8wqgf9v9ggikz42fs73xgrf88gx2kpinbsvf9lp") (f (quote (("wasmedge_ssl") ("default" "wasmedge_ssl"))))))

(define-public crate-http_req_wasi-0.10.1 (c (n "http_req_wasi") (v "0.10.1") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.4.2") (d #t) (k 0)))) (h "04dvikd96jz0k0ayxw6asgks9j26jm61rm5wicpjqx715ki0fr2g") (f (quote (("wasmedge_ssl") ("default" "wasmedge_ssl"))))))

(define-public crate-http_req_wasi-0.10.2 (c (n "http_req_wasi") (v "0.10.2") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.4.2") (d #t) (k 0)))) (h "06vg1kmv449ivb0aksifyhairbnqdxqchixq07w4j2b2jplv0mim") (f (quote (("wasmedge_ssl") ("default" "wasmedge_ssl"))))))

(define-public crate-http_req_wasi-0.11.0 (c (n "http_req_wasi") (v "0.11.0") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_rustls_api") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0d9y034vgj8x7yndvhyps3mhk0mszx62qlgm3bn5jb0rhka0xxwx") (f (quote (("wasmedge_rustls" "wasmedge_rustls_api") ("default" "wasmedge_rustls"))))))

(define-public crate-http_req_wasi-0.11.1 (c (n "http_req_wasi") (v "0.11.1") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_rustls_api") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.5.0") (d #t) (k 0)))) (h "1g8sbr5a18qbzhw1207mxbx552p6vlaxygzd1jq0rc80zg84vr5h") (f (quote (("wasmedge_rustls" "wasmedge_rustls_api") ("default" "wasmedge_rustls"))))))

