(define-module (crates-io ht tp http-content-range) #:use-module (crates-io))

(define-public crate-http-content-range-0.1.0 (c (n "http-content-range") (v "0.1.0") (h "0n0gh2jpjq3mwmwy17p8q063anr3crdaly7z04v0n0hz6gisfz0b")))

(define-public crate-http-content-range-0.1.1 (c (n "http-content-range") (v "0.1.1") (h "0c6vndw8m42wb2i9jqkfkmckyhzjsbl20x6qzpjpkz17bapxipg5")))

(define-public crate-http-content-range-0.1.2 (c (n "http-content-range") (v "0.1.2") (h "0bhx9j94raqynhmk2v2m0f98v5a6qi6b753p20b69a0qya71l3cz")))

