(define-module (crates-io ht tp http-echo-server) #:use-module (crates-io))

(define-public crate-http-echo-server-0.1.0 (c (n "http-echo-server") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.148") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00cfwczawrsdd0ggws2ga83lc05yjn5735sg95hhsqra7ny8l03x")))

(define-public crate-http-echo-server-0.1.1 (c (n "http-echo-server") (v "0.1.1") (d (list (d (n "axum") (r "^0.6.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.148") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w2akmalcl6iwxmpgka5dfmy3r1s5m8qnfc9k074z6zklgwbgiv8")))

