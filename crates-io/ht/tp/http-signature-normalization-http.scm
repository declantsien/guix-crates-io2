(define-module (crates-io ht tp http-signature-normalization-http) #:use-module (crates-io))

(define-public crate-http-signature-normalization-http-0.1.0 (c (n "http-signature-normalization-http") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-signature-normalization") (r "^0.1.0") (d #t) (k 0)))) (h "09jzjnxfclpq399vb5fs4y9cwdnj14rnh8bh07i8zja4rfzijw0s")))

(define-public crate-http-signature-normalization-http-0.2.0 (c (n "http-signature-normalization-http") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-signature-normalization") (r "^0.1.0") (d #t) (k 0)))) (h "1ccanjg5hgl236g2jkxx6jwmhw3xvmazpxcmadd7myacfsmfjdgj")))

(define-public crate-http-signature-normalization-http-0.3.0 (c (n "http-signature-normalization-http") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-signature-normalization") (r "^0.2.0") (d #t) (k 0)))) (h "1ph5rfyhw2k1i11bmnrxs96ydp23kqy3k20k6h4myr5gga3clqaa")))

(define-public crate-http-signature-normalization-http-0.4.0 (c (n "http-signature-normalization-http") (v "0.4.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-signature-normalization") (r "^0.6.0") (d #t) (k 0)))) (h "0v8x6cvmpc41imj3zl4ml304c2c30xk5cmch9qrqjxw480m4wkya")))

(define-public crate-http-signature-normalization-http-0.5.0 (c (n "http-signature-normalization-http") (v "0.5.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-signature-normalization") (r "^0.7.0") (d #t) (k 0)))) (h "1rmzdazz1fyavx17gwxmv07zxdpbja2vcpiwx7qbsipx1q9ria55")))

