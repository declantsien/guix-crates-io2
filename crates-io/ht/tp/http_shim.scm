(define-module (crates-io ht tp http_shim) #:use-module (crates-io))

(define-public crate-http_shim-0.0.1 (c (n "http_shim") (v "0.0.1") (d (list (d (n "http0_x") (r "^0.2") (d #t) (k 0) (p "http")) (d (n "http1_0") (r "^1") (d #t) (k 0) (p "http")))) (h "0ir30c8lpzn27h2gv4qxsfhpalv06ma1j35w8435dn1pq052m843") (f (quote (("old") ("new") ("default"))))))

