(define-module (crates-io ht tp httptin) #:use-module (crates-io))

(define-public crate-httptin-0.1.3 (c (n "httptin") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1l8aq495nibabs2qcsrk0v3r6i2505n06zjabd2vsxphxm69ynlb")))

(define-public crate-httptin-0.1.4 (c (n "httptin") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "05yhgr5qddl8v8b337249a02di6g1xjqhxfnksdy3mw6kq5sphzl")))

(define-public crate-httptin-0.1.5 (c (n "httptin") (v "0.1.5") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "09rnbq23vys7sq6i6nm4vs9zmj1m7x74k1yibzl37rdnhfjdawcg")))

(define-public crate-httptin-0.2.0 (c (n "httptin") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "slog") (r "^1.5") (d #t) (k 0)) (d (n "slog-term") (r "^1.5") (d #t) (k 0)))) (h "0fw0lmwfm3x78hlbj7yhajl38skrz42s53b68w31kf779yss10pw")))

(define-public crate-httptin-0.2.1 (c (n "httptin") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5") (d #t) (k 0)) (d (n "slog-term") (r "^1.5") (d #t) (k 0)))) (h "0lj2p2ff0k43hrksv42bdlpmzb9qz0dr832g6bafmmq9wch13nvn")))

(define-public crate-httptin-0.2.3 (c (n "httptin") (v "0.2.3") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1whivjqgby1mk7660ixvzk7hll2hrj5w63i9vp6frwj4c53gx2mp")))

