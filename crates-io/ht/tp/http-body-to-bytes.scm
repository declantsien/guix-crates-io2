(define-module (crates-io ht tp http-body-to-bytes) #:use-module (crates-io))

(define-public crate-http-body-to-bytes-0.0.0 (c (n "http-body-to-bytes") (v "0.0.0") (h "09i64hfpy2jnq87a71y3q2icyjm28m07z2p56j4fw35vbriqsp4n")))

(define-public crate-http-body-to-bytes-0.1.0 (c (n "http-body-to-bytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "http-body") (r "^0.4") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("stream"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1l74cby1ighx7w7za5rgvsz9fmgydi781wpvg5snrzd0f532425l")))

(define-public crate-http-body-to-bytes-0.2.0 (c (n "http-body-to-bytes") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 2)) (d (n "http-body") (r "^1") (k 0)) (d (n "http-body-util") (r "^0.1") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w2dacm2yyw583v4w2z0j29k77psz2n4a3w4bpljw77mqqv8580p")))

