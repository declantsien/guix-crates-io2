(define-module (crates-io ht tp http-codec) #:use-module (crates-io))

(define-public crate-http-codec-0.1.0 (c (n "http-codec") (v "0.1.0") (d (list (d (n "atoi") (r "^0.2.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "fake-stream") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.23") (d #t) (k 2)) (d (n "http") (r "^0.1.10") (d #t) (k 0)) (d (n "httparse") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.0") (d #t) (k 0)))) (h "18isnbwfrnidd0h7gfk7wsmls0lfghsj40cw37n2l33iwlpzzcji")))

(define-public crate-http-codec-0.1.1 (c (n "http-codec") (v "0.1.1") (d (list (d (n "atoi") (r "^0.2.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "fake-stream") (r "^0.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.23") (d #t) (k 2)) (d (n "http") (r "^0.1.10") (d #t) (k 0)) (d (n "httparse") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.0") (d #t) (k 0)))) (h "0l8jqn733if8c5y95gj3gsp4qsk1z5686dpnf5g7sv4v9zssbwg9")))

