(define-module (crates-io ht tp http-protocol) #:use-module (crates-io))

(define-public crate-http-protocol-0.1.0 (c (n "http-protocol") (v "0.1.0") (h "1azbfj054pllp6vd9xvfpfkawdawhr8yk6yl4g3m5l91qfd3rpdy") (y #t)))

(define-public crate-http-protocol-0.1.1 (c (n "http-protocol") (v "0.1.1") (h "1h4hgwpd8lndn9dj3h0ag81476kvdkbifzh9kylgl02yd1nhxvx6") (y #t)))

(define-public crate-http-protocol-0.1.2 (c (n "http-protocol") (v "0.1.2") (h "09jgawjsp3cyywp3irlv7xnc22l0kjcm4hw42gmcqcywb2l5s87p") (y #t)))

(define-public crate-http-protocol-0.1.3 (c (n "http-protocol") (v "0.1.3") (h "1ndwqbjl3nshx4i782yrjjh8rpl06wj2i8axiyrxvzfjh7i3xyry") (y #t)))

(define-public crate-http-protocol-0.1.4 (c (n "http-protocol") (v "0.1.4") (h "1kgclmvgs1cbsvrrcramp8y911712094ymsdpwhznyqijn4jvkb7") (y #t)))

(define-public crate-http-protocol-0.1.5 (c (n "http-protocol") (v "0.1.5") (h "19qvpq3rs0dinv5nd5bhykial9lih3flxlwa0nl35ilr7cqk8n8c") (y #t)))

(define-public crate-http-protocol-0.1.6 (c (n "http-protocol") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1sxi2nxzf5nzafiiad9fnlm325acjawhhb9xs9l82nlly6283mw5") (y #t)))

(define-public crate-http-protocol-0.1.7 (c (n "http-protocol") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dn8992jap88fbgi3fm8yginpilrc4q9rgbfl2kqcmz5axghipd8") (y #t)))

(define-public crate-http-protocol-0.1.8 (c (n "http-protocol") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fn2pjsi2ldklvswj6352asjj5b1w3yppy3ln7llkgg23rjd5aqh") (y #t)))

(define-public crate-http-protocol-0.1.9 (c (n "http-protocol") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vc3znxl8rmv4037qn6yjcdmsv9646srqkqff1pabvi5zdrmdhp7") (y #t)))

(define-public crate-http-protocol-0.1.10 (c (n "http-protocol") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0l2mzjf11c129wcvppk0psl4sqxfvq9xklryjny9rcqndwkl1692") (y #t)))

(define-public crate-http-protocol-0.1.11 (c (n "http-protocol") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hr4fwvhswiv1ignvqvcjvq5fzpf89hy7z9qyfwb0qdlrbrjsmc0") (y #t)))

(define-public crate-http-protocol-0.1.12 (c (n "http-protocol") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14cwl2msxj7kmbz9s016ii0znd3pcgyh5wxkby1gf8z47x3bxmxv") (y #t)))

(define-public crate-http-protocol-0.1.13 (c (n "http-protocol") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rbwhq116jzrzqf8v09v7c8lyy6bz0k27w9jcdnjjqp8mybhip6r") (y #t)))

(define-public crate-http-protocol-0.1.14 (c (n "http-protocol") (v "0.1.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0m1ldivkcy6mxfqv50z82hjpwwysj9fmikx8b7scx7w2cdby0j0q") (y #t)))

(define-public crate-http-protocol-0.1.15 (c (n "http-protocol") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05ig1inylck1sv7la4vgn5ywsyl1an8050d9an1asfd8p240csfj") (y #t)))

(define-public crate-http-protocol-0.1.16 (c (n "http-protocol") (v "0.1.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1g7zkgi36ds5v25g8d2hxir2ja08bmfad7j8i6fa4vhymd3m5q3x") (y #t)))

(define-public crate-http-protocol-0.2.0 (c (n "http-protocol") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yh6rv0ncqwx2q30rw7wc853rbbw4z8jy6ixmyrbgkif1gbn2hbz")))

(define-public crate-http-protocol-0.2.1 (c (n "http-protocol") (v "0.2.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ixs99h042vvngv8nhdaj491dr610wsymms8wh5kz1y52wkl8hcx")))

(define-public crate-http-protocol-0.2.2 (c (n "http-protocol") (v "0.2.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q8xbnkr1qp6dxjd48srawdipa3zmvcn0apm34r9lpnmhkvi629w")))

(define-public crate-http-protocol-0.2.3 (c (n "http-protocol") (v "0.2.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1cpjx09nk95yhl5l6pkblwdknjgjgxv3qci8bxhaafj34bwph628")))

(define-public crate-http-protocol-0.2.4 (c (n "http-protocol") (v "0.2.4") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0h9pss3q6b8h2hhxhja3mg7k8bqp346k112dvi15kw9ljracvgmr")))

(define-public crate-http-protocol-0.2.5 (c (n "http-protocol") (v "0.2.5") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1salp931k7c5gbh2mgfajqpvdfd5aab0b6pdh3sfa14pqcs1hbwi")))

(define-public crate-http-protocol-0.2.6 (c (n "http-protocol") (v "0.2.6") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0f8wg18baqk4b3rywm2w897wcjwxvalr5zpfa3z4i223hp5fw16d")))

(define-public crate-http-protocol-0.2.7 (c (n "http-protocol") (v "0.2.7") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "17n6yzjxzplqiqy5ga4x5lc0p7nmqqwkq66rfm12m8q7pfry3sfb")))

(define-public crate-http-protocol-0.2.8 (c (n "http-protocol") (v "0.2.8") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xac92ngqbjglzv1jcd00ll7rkbr6n23drlf7g7vr14dwh1y1bxw")))

(define-public crate-http-protocol-0.2.9 (c (n "http-protocol") (v "0.2.9") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ss85b6gspdckfx4l45s53jczjrd7xmij2afi1574sg8b88h9z85")))

(define-public crate-http-protocol-0.2.10 (c (n "http-protocol") (v "0.2.10") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1sqdyx6l42kcdn863c11684rpyz5lz90ab7z111215qc9f6p6lwc")))

(define-public crate-http-protocol-0.2.11 (c (n "http-protocol") (v "0.2.11") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01dq6ibs2v5jkx6apmhdp5vnxzlk66bcdzv0d09y2x5ap7ip1ns2")))

(define-public crate-http-protocol-0.2.12 (c (n "http-protocol") (v "0.2.12") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j6xf2pf6xdb9xih329j2j0xpnmkgw9bpvlpx4mi9xf1kgp3aq7v")))

(define-public crate-http-protocol-0.2.13 (c (n "http-protocol") (v "0.2.13") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08l0zvismhwz3lwnn2p3a9wyid12gg2w21m8w0xsbswabsgnq2n8")))

(define-public crate-http-protocol-0.2.14 (c (n "http-protocol") (v "0.2.14") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "055k844dknqhxn765kz70h0ck24k0mqr1xb9lldib3g5p2alqhxd")))

(define-public crate-http-protocol-0.2.15 (c (n "http-protocol") (v "0.2.15") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1fkgygnnl09c0ysg2hw95d4yy94cpkga6nk7r99d1h0lp46m4bhn")))

(define-public crate-http-protocol-0.2.16 (c (n "http-protocol") (v "0.2.16") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0lg3hvy21hv5yya43y6zc758if352521mymw17qrig94bpixhzn8")))

