(define-module (crates-io ht tp http-rate) #:use-module (crates-io))

(define-public crate-http-rate-0.1.0 (c (n "http-rate") (v "0.1.0") (d (list (d (n "all_asserts") (r "^2.2.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 2)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "02lwa70lalzjm6q7xm4nj6ndygr6cxpspkp0rc1zr53wixbpmn9q") (y #t)))

(define-public crate-http-rate-0.1.1 (c (n "http-rate") (v "0.1.1") (d (list (d (n "all_asserts") (r "^2.2.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 2)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0zi20894cfn3f58d8k62fgr37r3hzi5mvgrb0qsczq1424380lyv")))

