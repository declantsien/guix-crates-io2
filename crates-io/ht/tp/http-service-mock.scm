(define-module (crates-io ht tp http-service-mock) #:use-module (crates-io))

(define-public crate-http-service-mock-0.1.0 (c (n "http-service-mock") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http-service") (r "^0.1.4") (d #t) (k 0)))) (h "0bwj97i09dj3a2rgcb9xp35ldkkwis0a8wj13rb1ab6h4vyr1f97")))

(define-public crate-http-service-mock-0.1.1 (c (n "http-service-mock") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "http-service") (r "^0.1.5") (d #t) (k 0)))) (h "155240dk5acxpla8ivfi1mdlb802883g9nxna3f3n6hi7raclmnp")))

(define-public crate-http-service-mock-0.2.0 (c (n "http-service-mock") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "http-service") (r "^0.2.0") (d #t) (k 0)))) (h "0y5f7gi3afgxbfgzgq03lphk2cac6aiz2zzadxvxq6x6rycyq0df")))

(define-public crate-http-service-mock-0.3.0 (c (n "http-service-mock") (v "0.3.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "http-service") (r "^0.3.0") (d #t) (k 0)))) (h "13xjr3jdsqgdmz29lbvdndz2rid4516zwlf2vaxiwnfqxfa4v4c7")))

(define-public crate-http-service-mock-0.3.1 (c (n "http-service-mock") (v "0.3.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "http-service") (r "^0.3.1") (d #t) (k 0)))) (h "052f4aqmvr5923ys8jqi019iizm0ivhnhlfr2hwsvanlbqbj7m3f")))

(define-public crate-http-service-mock-0.4.0 (c (n "http-service-mock") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)))) (h "0idgsqczdzqy4xk84jm6n6i3jz9rlmzz2fncvxyp2lnmzjy6fgl9")))

(define-public crate-http-service-mock-0.5.0 (c (n "http-service-mock") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "http-service") (r "^0.5.0") (d #t) (k 0)))) (h "0cajy57l7csjrrywp5k4wsbf0p9i4p9r7d5lqlffbzpxqpm5mpbr")))

