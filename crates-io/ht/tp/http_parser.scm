(define-module (crates-io ht tp http_parser) #:use-module (crates-io))

(define-public crate-http_parser-0.0.1 (c (n "http_parser") (v "0.0.1") (h "12vjl9zqcsis38vvc7hm6ysra4079q5xc22rgjw3lhvk5cd4m22d")))

(define-public crate-http_parser-0.0.2 (c (n "http_parser") (v "0.0.2") (h "19gkp92j3hrj8hf3vj0k6r7w7wf75y55w2gjj8cw8455kg7hv4ji")))

