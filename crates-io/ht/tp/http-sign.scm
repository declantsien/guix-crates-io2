(define-module (crates-io ht tp http-sign) #:use-module (crates-io))

(define-public crate-http-sign-0.1.0 (c (n "http-sign") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "marching-buffer") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1z6s1y44b5lazh2vxmsw66as7qrwx2nazvj7ck04m3dyki75v333")))

(define-public crate-http-sign-0.2.0 (c (n "http-sign") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "marching-buffer") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1kkwlviqpvxx5z1iaa327y6cvz3xjhdm56f43vjgdw1bkznwqdj8")))

(define-public crate-http-sign-0.2.1 (c (n "http-sign") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "19wrppbpj6sh8bj6x5rxy3mmrgbdyvg2jvyzclkknmrvvxh9r48a")))

(define-public crate-http-sign-0.2.2 (c (n "http-sign") (v "0.2.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "16vbvwv038h334a4dk2rv0gi8k39zzq4r87j50q3jbmpmr87hxvq")))

(define-public crate-http-sign-0.3.0 (c (n "http-sign") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1rvk7vfqci9yg7afgjpchf7l0x1lh670n8jqfsxzhwnggki8dzcc")))

