(define-module (crates-io ht tp http-request-validator) #:use-module (crates-io))

(define-public crate-http-request-validator-0.1.0 (c (n "http-request-validator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0578fxkxwir26g24fns2c8qlyc0cvdrlcibh5khmi8ma2yz1ny2r")))

