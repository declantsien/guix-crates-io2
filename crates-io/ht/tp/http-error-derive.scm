(define-module (crates-io ht tp http-error-derive) #:use-module (crates-io))

(define-public crate-http-error-derive-0.0.1 (c (n "http-error-derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fy5xa203dc4ysqrv12nl93ckkmsh3wdi3ml41lnskwcjw76jky6")))

(define-public crate-http-error-derive-0.0.2 (c (n "http-error-derive") (v "0.0.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dmjn65qnv3qpads8aia8rrp05bv2qfc76la9524kvzspgk1bwyj")))

(define-public crate-http-error-derive-0.1.0 (c (n "http-error-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v3gl6lgphcpl9fnbfjn6i60wmr2bcp347hrigwlfrix6dg44sxx")))

(define-public crate-http-error-derive-0.1.1 (c (n "http-error-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0d03dzk977fd2n71g9y80ca5w06q7gjwwb87qz1w7qmng7iiknhl")))

(define-public crate-http-error-derive-0.2.0 (c (n "http-error-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1n5j983fidlq75cgxk4a4iq2j0klwnkwni7nx6zp5dk15m17rain")))

(define-public crate-http-error-derive-0.2.1 (c (n "http-error-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "081105cy0k3i07bqd5z9hb1r2chmqfbpkqali3im95g5rnc8mdmh")))

(define-public crate-http-error-derive-0.3.0 (c (n "http-error-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xzjnadmviksj3mpfknd5s9sksvbv3dxyxymcc8fl44kkaa1k9g0")))

(define-public crate-http-error-derive-0.3.1 (c (n "http-error-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0l0m5vv0z894mazqasbkphb7j9v11n585mx1gf24nsvsvffprhxc")))

(define-public crate-http-error-derive-0.3.2 (c (n "http-error-derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1y38rh10xvridhwbpsddq4rbqcyhqn0f53p7q2dqp07kcbb4nj9g")))

