(define-module (crates-io ht tp http-connection) #:use-module (crates-io))

(define-public crate-http-connection-0.0.0 (c (n "http-connection") (v "0.0.0") (h "0irv635x6i0axdy2v1bhrhswc1f249lj9v6ils3nalh6yzp8zv9n")))

(define-public crate-http-connection-0.1.0 (c (n "http-connection") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1") (o #t) (d #t) (k 0)))) (h "09xckc1zjy611d03wfc673lqqnbwg19dsilsvba72wvzlk780q2z") (f (quote (("tcp" "tokio-tcp") ("default" "tcp"))))))

