(define-module (crates-io ht tp http-typed) #:use-module (crates-io))

(define-public crate-http-typed-0.1.0 (c (n "http-typed") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1xh9jzlrzday66mf1hcgznqgk639dgsnbnc45j226lwgcchqvs3f")))

(define-public crate-http-typed-0.2.0 (c (n "http-typed") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "05jlvi10q53hd8c4l9q8lmnkfxdkjwd9jr9ssa3qvb9ksc6wd3ba")))

(define-public crate-http-typed-0.3.0 (c (n "http-typed") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1w6hsr7y2hzllgfivny8lzz72lm8ykzqm7npnyfdiw212lc1csqh")))

(define-public crate-http-typed-0.3.1 (c (n "http-typed") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "056il5ds8w0lka3jgy15kpx0vzcfvng2gkjcrbk3jsagzixq3vf6")))

(define-public crate-http-typed-0.3.2 (c (n "http-typed") (v "0.3.2") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1dax2ism3yjflhx2ynk6c3a0rnxwwn5fr8hk259mizig4chkhba6") (f (quote (("rustls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "client" "native-tls") ("client" "reqwest")))) (y #t)))

(define-public crate-http-typed-0.3.3 (c (n "http-typed") (v "0.3.3") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "09k7sfzhi5b1hykyljxf44qd5q45937rz3ncz0ijx7s7m8rkkwph") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "client" "native-tls") ("client" "reqwest"))))))

(define-public crate-http-typed-0.4.0 (c (n "http-typed") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.12.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "16x4536qp4xv5hdz5igdvf3i5yssyj1ycsi6qq57bvnxhwk3wr30") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "client" "native-tls") ("client" "reqwest"))))))

