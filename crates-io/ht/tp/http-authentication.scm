(define-module (crates-io ht tp http-authentication) #:use-module (crates-io))

(define-public crate-http-authentication-0.1.0 (c (n "http-authentication") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (k 0)) (d (n "http-auth") (r "^0.1.5") (k 0)))) (h "14gz2bbq7l9ml67fb0n7n824zkj52n42m4sb4h7sg368ppzg2ivj") (f (quote (("std") ("scheme-bearer") ("scheme-basic" "base64") ("default" "std" "http" "scheme-basic" "scheme-bearer"))))))

(define-public crate-http-authentication-0.2.0 (c (n "http-authentication") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (k 0)) (d (n "http-auth") (r "^0.1") (k 0)))) (h "0pnzsd63w8yxhpxi9izsiyc9yl2ykvxdapniq3dp48c12qi3d28h") (f (quote (("std") ("scheme-bearer") ("scheme-basic" "base64") ("default" "std" "http" "scheme-basic" "scheme-bearer"))))))

