(define-module (crates-io ht tp http-status-codes2) #:use-module (crates-io))

(define-public crate-http-status-codes2-0.1.0 (c (n "http-status-codes2") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0v18jmdp70xqzf2d2jzvasqh1yphnsfr5c5127sps0n8w565wywm")))

(define-public crate-http-status-codes2-0.1.1 (c (n "http-status-codes2") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0pky6f5qvzxzs1py79kjc51a6w038rr85a70phfq7caynwhrnj0m")))

(define-public crate-http-status-codes2-0.1.2 (c (n "http-status-codes2") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "1iq75mdd4dn7mz9154ydha89hg54sajhwbfl13qxkddnxhyvqzp1")))

(define-public crate-http-status-codes2-0.1.3 (c (n "http-status-codes2") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "1028ziagimnm530dmy961ypis1b6dhhnsv51nh8jgybapwvrpgr0")))

