(define-module (crates-io ht tp http-whatever) #:use-module (crates-io))

(define-public crate-http-whatever-0.1.0 (c (n "http-whatever") (v "0.1.0") (d (list (d (n "http") (r "^0.2.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "03n7is0dh79srd1jl66b719vmfjh1b9piy7hc1whz2z5kxb7ncc5")))

(define-public crate-http-whatever-0.1.1 (c (n "http-whatever") (v "0.1.1") (d (list (d (n "http") (r "^0.2.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0hrbhf18vsf0lysf2h636k0c7p01abkdcc6cqbxphf6c87icxb2j")))

(define-public crate-http-whatever-0.2.0 (c (n "http-whatever") (v "0.2.0") (d (list (d (n "http") (r "^0.2.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0hw08vljgr448lpqkcgf56y66q4gf8q56155l7yzq13r1f4i6jsj")))

(define-public crate-http-whatever-0.2.1 (c (n "http-whatever") (v "0.2.1") (d (list (d (n "http") (r "^0.2.10") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0va71mnw93k99pza5acmshwgggz2d94bniwhf844r4i99h2cfwbc")))

(define-public crate-http-whatever-0.3.0 (c (n "http-whatever") (v "0.3.0") (d (list (d (n "http") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (d #t) (k 0)))) (h "1ip7awc99y9gq7dvxdpqxwkh4iycfsm5mb71990ycdkpccmh01pr")))

