(define-module (crates-io ht tp http_header) #:use-module (crates-io))

(define-public crate-http_header-0.3.0 (c (n "http_header") (v "0.3.0") (h "0anvyy2y7kgxh3bfpw8dfspdzxkkj24rsd67kn1122yzk11wijff")))

(define-public crate-http_header-0.3.1 (c (n "http_header") (v "0.3.1") (h "0kfyksd6bd2dnpi0k1imb53d9icdnack3z06pqwdmihs07nxs94p")))

(define-public crate-http_header-0.3.2 (c (n "http_header") (v "0.3.2") (h "0h13mnfw9b4a361v54i5lmym1yn760512a3qd4fii1f7v3f5r0mq")))

(define-public crate-http_header-0.4.0 (c (n "http_header") (v "0.4.0") (h "0sc07jlcpbrkb2iqwnqjnb3vglwqhvgm5m2qcv92kdqalk40dlqc")))

(define-public crate-http_header-0.5.0 (c (n "http_header") (v "0.5.0") (h "1ix7ydmnj0kg9wkg2fm958sc0ix59z0bnh7fgwdghrwpi63j245n") (y #t)))

(define-public crate-http_header-0.5.1 (c (n "http_header") (v "0.5.1") (h "1f6wv5a1diqlky6jdccg4id31dhdfjwk1y5rfbkb0r1fjcbbfxqk")))

(define-public crate-http_header-0.6.0 (c (n "http_header") (v "0.6.0") (h "1jqz591fljzgdzpjbv7ffg9yh90ccza5zk732yw5hw6bg4mdv6is")))

(define-public crate-http_header-0.6.1 (c (n "http_header") (v "0.6.1") (h "1cbn8wg749n6p6gfvy376gnqj7i70dsg1nijhx47nzvm8rzi6w0q")))

(define-public crate-http_header-0.6.2 (c (n "http_header") (v "0.6.2") (h "1g0lndad4bm0zkwqsv1f97fkzjn0q83715yv8206qjcbfvvgn2nq")))

(define-public crate-http_header-0.6.3 (c (n "http_header") (v "0.6.3") (h "1ffxyjcpy45lhi909dgh5gl5cday8kdcp2zr1yyii4ljvks5ld2r")))

(define-public crate-http_header-0.7.0 (c (n "http_header") (v "0.7.0") (h "1r7z1lq685yshw7vf5x7w6vmd9sizsalfwyv0aly804q988zkikq")))

(define-public crate-http_header-0.8.0 (c (n "http_header") (v "0.8.0") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "16ccamkz254xjlsxzv164yn0wwnlv5xk4q4bz0p6yf9y4mf22a4b")))

(define-public crate-http_header-0.8.1 (c (n "http_header") (v "0.8.1") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "1ajhq5mxf01vqy5rzj5nj744jbhjq99dfnzqa7db34grpp823j3b")))

(define-public crate-http_header-0.8.2 (c (n "http_header") (v "0.8.2") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "1bag7n7rb7gwllnhhgw8b09iazm2b7x64yndgbyd7bg0v7rwa4xm")))

