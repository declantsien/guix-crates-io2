(define-module (crates-io ht tp http-service-h1) #:use-module (crates-io))

(define-public crate-http-service-h1-0.1.0 (c (n "http-service-h1") (v "0.1.0") (d (list (d (n "async-h1") (r "^1.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "http-service") (r "^0.5.0") (d #t) (k 0)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)))) (h "1pvi0aqf6ipra7qhfcgfzzy8skrh2d9qwikxmg6qwdw6iwl1235i") (f (quote (("runtime" "async-std/default") ("default" "runtime"))))))

