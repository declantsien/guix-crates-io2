(define-module (crates-io ht tp http-status-print) #:use-module (crates-io))

(define-public crate-http-status-print-0.1.0 (c (n "http-status-print") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)))) (h "0yaqnml44lmx75bs82kpq0q18n8lysgjr19brqxiihpk6qvp9z1f")))

(define-public crate-http-status-print-0.1.1 (c (n "http-status-print") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)))) (h "0ccn903nhm8kyx3cv1fq2d3fw8b8s95p1yy2nww9fpkqxfmxfihr")))

(define-public crate-http-status-print-0.1.2 (c (n "http-status-print") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "human-panic") (r "^0.4.0") (d #t) (k 0)))) (h "1spspa9r3zdk3rmlx892lzyb4w23jv588c04cw6vz985qk5nmz4l")))

(define-public crate-http-status-print-0.1.3 (c (n "http-status-print") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.0") (d #t) (k 0)))) (h "1dqs7sl57z3xv4y0xf23x7xbm87hgdvpr6y9hghi0qlbb3a3rbzp")))

(define-public crate-http-status-print-0.1.4 (c (n "http-status-print") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)))) (h "0fmsjp7vgbwc4zd3fdcrhd1i1l2kys6bqwd4hv4mjn0w9qzx4590")))

(define-public crate-http-status-print-0.1.5 (c (n "http-status-print") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)))) (h "1mb7pjyndnx96zg44yvxls7d3b2z1ij0ybza0li69x794c0ypj9m")))

