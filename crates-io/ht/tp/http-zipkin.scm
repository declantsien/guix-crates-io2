(define-module (crates-io ht tp http-zipkin) #:use-module (crates-io))

(define-public crate-http-zipkin-0.1.0 (c (n "http-zipkin") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.3") (d #t) (k 0)))) (h "1gxqkknm7v6791344xwlgir83gjscbazv2lxqcr52np8a3ynvb0d")))

(define-public crate-http-zipkin-0.1.1 (c (n "http-zipkin") (v "0.1.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.3") (d #t) (k 0)))) (h "00m2yy9377aaqwix6iaw3nq26p56l90imw4py4jqkm5qxc80vvlj")))

(define-public crate-http-zipkin-0.2.0 (c (n "http-zipkin") (v "0.2.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "zipkin") (r "^0.4") (d #t) (k 0)))) (h "0jv2azmb7qmbrd7m3x514slb23kf4c0ig6dzz4xc5sq41c8a7ph7")))

(define-public crate-http-zipkin-0.3.0 (c (n "http-zipkin") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "zipkin") (r "^0.4") (d #t) (k 0)))) (h "03mgfjndqxj7l6h80xx5x60hcghjwa9y4xkk0kz5kvxz0sxl01fi")))

(define-public crate-http-zipkin-0.4.0 (c (n "http-zipkin") (v "0.4.0") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "zipkin") (r "^0.4") (d #t) (k 0)))) (h "0lwwgwaxbv866ix6sxckasknqr7kyij7pka6q04manjjarmwjm8n")))

