(define-module (crates-io ht tp http-body-request-validator) #:use-module (crates-io))

(define-public crate-http-body-request-validator-0.1.0 (c (n "http-body-request-validator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "http-request-validator") (r "^0.1") (d #t) (k 0)))) (h "1jklv6ifsb8b5j9sxnhvvqin4303b8qk095zxrmn6as7fp3fvn9i")))

