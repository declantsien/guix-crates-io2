(define-module (crates-io ht tp http_digest_headers) #:use-module (crates-io))

(define-public crate-http_digest_headers-0.1.0 (c (n "http_digest_headers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0vcifl5f069w0w7aaghgq0z2v0fcxmgbsrd7gq5h1jy6d7vrmcvx") (f (quote (("use_ring" "ring") ("use_openssl" "openssl") ("default" "use_openssl"))))))

