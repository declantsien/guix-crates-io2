(define-module (crates-io ht s2 hts221) #:use-module (crates-io))

(define-public crate-hts221-0.1.0 (c (n "hts221") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "02z9az947dhhnz99k07mnw92dprc6d9agl2a8i1ka6cgzj0lm2vw")))

(define-public crate-hts221-0.1.1 (c (n "hts221") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.4.3") (f (quote ("cm7-r0p1"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.3.13") (f (quote ("abort-on-panic"))) (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.2.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "stm32f30x") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 2)) (d (n "stm32f30x-hal") (r "^0.1.2") (f (quote ("rt"))) (d #t) (k 2)))) (h "1gcs0aj1j07v13qgxmv1ks9vinli3v1y0mzpldgll781sv2x2fdv")))

(define-public crate-hts221-0.2.0 (c (n "hts221") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.5.0") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "stm32f30x") (r "^0.7.0") (f (quote ("rt"))) (d #t) (k 2)) (d (n "stm32f30x-hal") (r "^0.2.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "1rp9lsl4rr3lbyi1m41yvmwsipjbnp1754r2w3j486nbp79yyw7q")))

(define-public crate-hts221-0.3.0 (c (n "hts221") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "stm32f30x-hal") (r "^0.2.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "05qaakn1z2sn8lbb2rxq7s2akkbaxkssh11syd17fw7mvqkzj0f5")))

