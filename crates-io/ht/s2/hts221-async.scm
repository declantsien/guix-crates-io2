(define-module (crates-io ht s2 hts221-async) #:use-module (crates-io))

(define-public crate-hts221-async-0.1.0 (c (n "hts221-async") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "1nhd8l47hrfil4fi4l8pxav5pprhr925156l3lx28f6yjnc5lh9r")))

(define-public crate-hts221-async-0.1.1 (c (n "hts221-async") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "0rdaz3npk6mr5vddm4q1v616ih231gzc40qgdginmphw5q44vcrx")))

(define-public crate-hts221-async-0.2.0 (c (n "hts221-async") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1sc93m244mmdlx09ray4kz9r0x6ssp3dril09q8kfh5ig416vrm7")))

