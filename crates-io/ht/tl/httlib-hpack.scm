(define-module (crates-io ht tl httlib-hpack) #:use-module (crates-io))

(define-public crate-httlib-hpack-0.0.0 (c (n "httlib-hpack") (v "0.0.0") (h "18c58aj6gqk955qhw25pjf1wc3q9wgmjm7hxhrym3hjfr7rpncxm")))

(define-public crate-httlib-hpack-0.0.1 (c (n "httlib-hpack") (v "0.0.1") (h "1vzr1nkp1m67iljnzinpldw5znxkhcm8cybvxlhbz18bsiyd0y9c")))

(define-public crate-httlib-hpack-0.1.0 (c (n "httlib-hpack") (v "0.1.0") (d (list (d (n "httlib-huffman") (r "^0.3.3") (d #t) (k 0)))) (h "0s837vyf01kmglmmb5394w7xj4914vrb0qbv8jbiy8mzcv1cdv9z")))

(define-public crate-httlib-hpack-0.1.1 (c (n "httlib-hpack") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "httlib-huffman") (r "^0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0khj9psa9bi67pq31i17vy6ibgnz7k8wm2fln7sbp3chq79y5gvh")))

(define-public crate-httlib-hpack-0.1.2 (c (n "httlib-hpack") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "httlib-huffman") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0cmg9ifhh4z89bg05366khpa6x5nzkvr72yzsbwcdfzs05wgznk4")))

(define-public crate-httlib-hpack-0.1.3 (c (n "httlib-hpack") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "httlib-huffman") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1qxaabq6y2jp5rb8lcv0lmvr7pi1512z34552kwnyz2nx3jn1ks0")))

