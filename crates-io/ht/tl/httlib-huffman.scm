(define-module (crates-io ht tl httlib-huffman) #:use-module (crates-io))

(define-public crate-httlib-huffman-0.1.0 (c (n "httlib-huffman") (v "0.1.0") (h "078d9nc9dgs1b1z9dviaxlybhya30cb8ik4h1k0mhsbnm9iqd34p") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1.1 (c (n "httlib-huffman") (v "0.1.1") (h "13f58q0mwjk9c9yfiri1cfnnfvhxflngqchwm6374wkjx5acx83m") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1.2 (c (n "httlib-huffman") (v "0.1.2") (h "1h7qciq0kf49p36zvn2x5mga23ranr4i1zpkq8xms1hvs3wfjbi9") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1.3 (c (n "httlib-huffman") (v "0.1.3") (h "057zk22ysf80l124f23lnjssqp8zffg0idaa9m0rdh2624l3k43x") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1.4 (c (n "httlib-huffman") (v "0.1.4") (h "0nyh8z3yrgd8yk6a52krkbag083ia81rgqvxykn4lcpkjl64l74f") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.2.0 (c (n "httlib-huffman") (v "0.2.0") (h "1h48hkp4pix16c23h8fv47hlp54cnkcvl2mhjvi1nlns9af7dbjk") (f (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.2.1 (c (n "httlib-huffman") (v "0.2.1") (h "1y4l94782mnjhymj42h8hgd6bsc3j7sysgcf4ywbj8i9y9zj6vkx")))

(define-public crate-httlib-huffman-0.2.2 (c (n "httlib-huffman") (v "0.2.2") (h "0x2jil2h1rixpkx5clpfl1a9vk6y5wizg446sjz9i0bbxvbhich3")))

(define-public crate-httlib-huffman-0.2.3 (c (n "httlib-huffman") (v "0.2.3") (h "1gd8navg81cqrbjf2qc9bbjphhp43igsp7yajgrmnlsyfalhqngx")))

(define-public crate-httlib-huffman-0.3.0 (c (n "httlib-huffman") (v "0.3.0") (h "00rgpkxc03rfsnb3dlqllw5rhwg523sxjnsfxfl82hp2j455w1ds")))

(define-public crate-httlib-huffman-0.3.1 (c (n "httlib-huffman") (v "0.3.1") (h "1zh19i4fv41rdy72hnxxrnk0yahmr112wr318lf8vxhgjynar5dm")))

(define-public crate-httlib-huffman-0.3.2 (c (n "httlib-huffman") (v "0.3.2") (h "1yi2bgmxfm75zaigxqwns7wda570chwypk892cmcygql787pz3hg")))

(define-public crate-httlib-huffman-0.3.3 (c (n "httlib-huffman") (v "0.3.3") (h "006jrmflr7vyydrwxwr1q4pkilg8cc44in1fvn21p8mrilv828gq")))

(define-public crate-httlib-huffman-0.3.4 (c (n "httlib-huffman") (v "0.3.4") (h "0xxqvqkxq7mxdb3si83szg0nfybfr3jk9mc0mg1jcmcc836cp7qs")))

