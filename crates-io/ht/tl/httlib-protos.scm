(define-module (crates-io ht tl httlib-protos) #:use-module (crates-io))

(define-public crate-httlib-protos-0.1.0 (c (n "httlib-protos") (v "0.1.0") (h "07xsb8apbqz0lwrsbq81bkj8jbjqqdvxnh9m5d2mql8jhg2w9rmf")))

(define-public crate-httlib-protos-0.1.1 (c (n "httlib-protos") (v "0.1.1") (h "1dza90pzd90dn81llmmbhak6fppgar687nz85pk29qmr7skij6hj")))

(define-public crate-httlib-protos-0.1.2 (c (n "httlib-protos") (v "0.1.2") (h "0bxxcyll2qb424gawv9nz7ipdy7ddgjj86wpy58yqr9074ld3c7k")))

(define-public crate-httlib-protos-0.1.4 (c (n "httlib-protos") (v "0.1.4") (h "15k5xnpvrl6hax08f93xd7wfzcvvpw7dvijyp4xzl232k8j0gwyc")))

(define-public crate-httlib-protos-0.2.0 (c (n "httlib-protos") (v "0.2.0") (h "0kfdyvli17w6srqfcihjqsybmr4gsydza4vyymmvagc6yx2x7k6d")))

(define-public crate-httlib-protos-0.2.1 (c (n "httlib-protos") (v "0.2.1") (h "183i3jkqmqkzcgmswzjywb8dyxg48xwadnjwllg72lh961qx8qw2")))

(define-public crate-httlib-protos-0.3.0 (c (n "httlib-protos") (v "0.3.0") (h "0s03b85ypgbg5k582x6bip27c8x8bw4rw0aimjy19jdrkb2cym14")))

(define-public crate-httlib-protos-0.3.1 (c (n "httlib-protos") (v "0.3.1") (h "0i9hg78pjs4l928jgd0swhv7d24nwd6gvgbshmzndr73vrq25yv8")))

(define-public crate-httlib-protos-0.3.2 (c (n "httlib-protos") (v "0.3.2") (h "0f6asin26ga3z1byxmyqb5vsgysbd4nzslrrpzmxrgbqfj9wsznl")))

