(define-module (crates-io ht ai htai_elitist) #:use-module (crates-io))

(define-public crate-htai_elitist-1.1.0 (c (n "htai_elitist") (v "1.1.0") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1r3yrz9y8lf93hmviavrydb8kbq4i151k36ymw9zq3mhgxfsv4p4")))

