(define-module (crates-io ht re htree) #:use-module (crates-io))

(define-public crate-htree-0.1.0 (c (n "htree") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "00fgcahrdgag0cpqf835ainihgc2yvhbjs5r08h9g40gnhivcl0n")))

(define-public crate-htree-0.1.1 (c (n "htree") (v "0.1.1") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "126dx7df72qz5wn8iqbl2ms2ppsmx6mmaavlim0lcm14gb3k7hhy")))

(define-public crate-htree-0.1.2 (c (n "htree") (v "0.1.2") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0m24k20kyxfklfy4cisf798pz43nhf8wzlb5dfz4jgpz6zgyicis")))

(define-public crate-htree-0.1.3 (c (n "htree") (v "0.1.3") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0l4ggrr24kjkz79hrwzvjh0z74b14fd6p1annyaasdjiqifv4zml")))

