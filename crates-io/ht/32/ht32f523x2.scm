(define-module (crates-io ht #{32}# ht32f523x2) #:use-module (crates-io))

(define-public crate-ht32f523x2-0.1.0 (c (n "ht32f523x2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0rw3yqxanwfgznkd79nm196gzx6pcgpgf8jwy2qw2h2cbckcsxmv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ht32f523x2-0.2.0 (c (n "ht32f523x2") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "18hni3grzm9520md28wyy25j8zsdsjhsgsmmxa3358fh48v4ydq8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ht32f523x2-0.2.1 (c (n "ht32f523x2") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hpkdbv30gz8h935p97jnc4q8yhgbwa4i4bkjs96f0j5rwgkhfxp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ht32f523x2-0.3.0 (c (n "ht32f523x2") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "07bfnwaxgzlmm1vgw93ygmmw86wwzdljdhv116bvdfrswjsd44d8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ht32f523x2-0.4.0 (c (n "ht32f523x2") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hbdg6f6pxzcgzwvls8jwwjincisn643bx4mddijgwnwzq4d6krx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ht32f523x2-0.5.0 (c (n "ht32f523x2") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "161qd75qqbxr2ig0rrvd936ghhbj6aks44zag5jb36xsz40x71q8") (f (quote (("rt" "cortex-m-rt/device"))))))

