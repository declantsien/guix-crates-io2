(define-module (crates-io ht #{32}# ht32f5yyy) #:use-module (crates-io))

(define-public crate-ht32f5yyy-0.1.0 (c (n "ht32f5yyy") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "17lxwfmb9mf4z2vp6lrmfbj8wr242k2nk6zh9yc0jvbysd0jm0q4") (f (quote (("rt" "cortex-m-rt/device") ("ht32f5826") ("default"))))))

