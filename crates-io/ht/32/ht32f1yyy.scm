(define-module (crates-io ht #{32}# ht32f1yyy) #:use-module (crates-io))

(define-public crate-ht32f1yyy-0.1.0 (c (n "ht32f1yyy") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0g41z7jph6xb58qq7f6d49glzxj0jjhlij5jj93p2nkg6k1vs25m") (f (quote (("rt" "cortex-m-rt/device") ("ht32f175x") ("ht32f1655_56") ("ht32f1653_54") ("ht32f125x") ("default"))))))

(define-public crate-ht32f1yyy-0.1.1 (c (n "ht32f1yyy") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1w4x61syfmmw9akrz8w3gd1nq2g84xhcmnpy3f665yr1jhi9wvsn") (f (quote (("rt" "cortex-m-rt/device") ("ht32f175x") ("ht32f1655_56") ("ht32f1653_54") ("ht32f125x") ("default"))))))

(define-public crate-ht32f1yyy-0.2.0 (c (n "ht32f1yyy") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08xrbrjnv9dcjxpscqbdy5rn6xwfhkwlfg76nv7p89p9khdsl85h") (f (quote (("rt" "cortex-m-rt/device") ("ht32f175x") ("ht32f1655_56") ("ht32f1653_54") ("ht32f125x") ("default" "critical-section" "rt"))))))

