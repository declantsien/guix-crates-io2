(define-module (crates-io ht #{32}# ht32f2yyy) #:use-module (crates-io))

(define-public crate-ht32f2yyy-0.1.0 (c (n "ht32f2yyy") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0qcnriflglmvlspl2xzj0mlda62dqy4z2fi776c6wmjv93mzc1fn") (f (quote (("rt" "cortex-m-rt/device") ("ht32f275x") ("default"))))))

