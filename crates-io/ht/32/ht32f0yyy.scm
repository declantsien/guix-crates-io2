(define-module (crates-io ht #{32}# ht32f0yyy) #:use-module (crates-io))

(define-public crate-ht32f0yyy-0.1.0 (c (n "ht32f0yyy") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1drjxnmgnxrwj7hkgsn75y1d2g1f4dc8jgc0fy4xl6aivaaxpgdw") (f (quote (("rt" "cortex-m-rt/device") ("ht32f0008") ("ht32f0006") ("default"))))))

