(define-module (crates-io ht #{32}# ht32-usbd) #:use-module (crates-io))

(define-public crate-ht32-usbd-0.1.0 (c (n "ht32-usbd") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z1i2d2kz1lyjz643nrlljr9kfhag82rr86csmlj1hm90q2yi92j")))

(define-public crate-ht32-usbd-0.1.1 (c (n "ht32-usbd") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "usb-device") (r "^0.3.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pmfzqpcp2awpj11y29bdqvwnpwnnicfz5nzwjhw0fcd2b35nkqy")))

