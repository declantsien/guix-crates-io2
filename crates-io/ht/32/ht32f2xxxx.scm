(define-module (crates-io ht #{32}# ht32f2xxxx) #:use-module (crates-io))

(define-public crate-ht32f2xxxx-0.1.0 (c (n "ht32f2xxxx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fg7i5lyjh9z4r70khraaq9b7j9pn0f1fnxvhm9c8w09l8nffyhy") (f (quote (("rt" "cortex-m-rt/device") ("ht32f22366") ("default"))))))

