(define-module (crates-io ht qx htqx_minigrep) #:use-module (crates-io))

(define-public crate-htqx_minigrep-0.1.1 (c (n "htqx_minigrep") (v "0.1.1") (h "0xjkxylr51xybxmvlxg1w5yndnlcgzvy52jh02988z06qj8hxh72")))

(define-public crate-htqx_minigrep-1.0.0 (c (n "htqx_minigrep") (v "1.0.0") (h "0gxj2q69gvqp9lzjldhq0ppl476pjcdgk8snh6awixaxvajcfb7f")))

