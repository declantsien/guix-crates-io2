(define-module (crates-io ht r_ htr_cli) #:use-module (crates-io))

(define-public crate-htr_cli-0.5.24 (c (n "htr_cli") (v "0.5.24") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "htr") (r "^0.5.24") (k 0)))) (h "1pnvyf3g851b9lhdik72cvz2bh432h4jl4pbpffplp97j16hw0gl")))

(define-public crate-htr_cli-0.5.25 (c (n "htr_cli") (v "0.5.25") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "htr") (r "^0.5.25") (k 0)))) (h "0fjpsc2r1vv2zrf5mh1njz8d6y3pv8f4wpwhvgv0p8ah0vag7ilw")))

(define-public crate-htr_cli-0.5.26 (c (n "htr_cli") (v "0.5.26") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "htr") (r "^0.5.26") (k 0)))) (h "00max3fppg204063mw42rj56m38w8pkzh29snqhdima3r6jkfkzr")))

(define-public crate-htr_cli-0.5.27 (c (n "htr_cli") (v "0.5.27") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "htr") (r "^0.5.26") (k 0)))) (h "1kawgggmv11cir3lh6iz30vfx20q3vsp851xm9m4a4kkxf8zi2i3")))

