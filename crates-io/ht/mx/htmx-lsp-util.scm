(define-module (crates-io ht mx htmx-lsp-util) #:use-module (crates-io))

(define-public crate-htmx-lsp-util-0.1.0 (c (n "htmx-lsp-util") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (f (quote ("kv_unstable" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.1") (d #t) (k 0)))) (h "0khk07rypavq32g2ni4bbc2bvd6z62nfld2hm51d99ndnqmb8rv7")))

