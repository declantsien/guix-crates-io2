(define-module (crates-io ht mx htmx-macros) #:use-module (crates-io))

(define-public crate-htmx-macros-0.1.0 (c (n "htmx-macros") (v "0.1.0") (d (list (d (n "manyhow") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote-use") (r "^0.7.2") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1g2rly4520hr64ymydnww5m6wjg1bn2795zjz668b5flrl9wsf6b")))

