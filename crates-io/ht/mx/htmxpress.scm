(define-module (crates-io ht mx htmxpress) #:use-module (crates-io))

(define-public crate-htmxpress-0.0.0 (c (n "htmxpress") (v "0.0.0") (d (list (d (n "htmxpress_macros") (r "^0.0.0") (d #t) (k 0)))) (h "1w44ll27nkdmbqd3zr5fznnzg8709qz84q7j94vgpnxcy2z3r1rx")))

(define-public crate-htmxpress-0.1.0 (c (n "htmxpress") (v "0.1.0") (d (list (d (n "htmxpress_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (o #t) (d #t) (k 0)))) (h "1fszz9w3a0dx7jdvb9j5pz982074nq6p0js0sphjmdhlpify7l24") (f (quote (("full" "urlencoding") ("default" "full")))) (s 2) (e (quote (("urlencoding" "dep:urlencoding"))))))

