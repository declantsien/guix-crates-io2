(define-module (crates-io ht mx htmx) #:use-module (crates-io))

(define-public crate-htmx-0.1.0 (c (n "htmx") (v "0.1.0") (d (list (d (n "derive_more") (r "^1.0.0-beta.3") (f (quote ("as_ref" "deref" "deref_mut"))) (d #t) (k 0)) (d (n "html") (r "^0.6.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "htmx-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quote-use") (r "^0.7.1") (f (quote ("namespace_idents"))) (d #t) (k 0)))) (h "0cdls4g87d15lxb4vmrwzf8bbp3bcpbizfcblxhhn4ya3ladip2p")))

