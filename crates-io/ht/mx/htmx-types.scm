(define-module (crates-io ht mx htmx-types) #:use-module (crates-io))

(define-public crate-htmx-types-0.1.0 (c (n "htmx-types") (v "0.1.0") (d (list (d (n "claims") (r "^0.7") (d #t) (k 2)) (d (n "headers-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "http-serde") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10l8jia80hvglymkykxh2l5cfnf2h1fjshrpgpqch63rwcvd0x29")))

