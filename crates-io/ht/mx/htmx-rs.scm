(define-module (crates-io ht mx htmx-rs) #:use-module (crates-io))

(define-public crate-htmx-rs-0.1.0 (c (n "htmx-rs") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tower-livereload") (r "^0.9.2") (d #t) (k 0)))) (h "0cdkrdn3v422vap0mlgypbxxs8w7g6xnssambpb0xsyx9l19kh77")))

