(define-module (crates-io ht mx htmx-lsp) #:use-module (crates-io))

(define-public crate-htmx-lsp-0.1.0 (c (n "htmx-lsp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "htmx-lsp-server") (r "^0.1.0") (d #t) (k 0)) (d (n "htmx-lsp-util") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("kv_unstable" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "10dx1010yr7ah2j8kyibvxva0wf9dhq129dwasl39iw697y818gi")))

