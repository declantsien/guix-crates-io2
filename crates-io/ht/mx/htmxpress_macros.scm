(define-module (crates-io ht mx htmxpress_macros) #:use-module (crates-io))

(define-public crate-htmxpress_macros-0.0.0 (c (n "htmxpress_macros") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1bz8wh55an0b01sgr1l1i801giwjd1j186mcpnfq270i52m1r189")))

(define-public crate-htmxpress_macros-0.1.0 (c (n "htmxpress_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a6j954sfgdbd2hjb0m06bcrh2vlxyvsp09cv37jdfqy7nhdgjj7")))

