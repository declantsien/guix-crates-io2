(define-module (crates-io ht mx htmx-components) #:use-module (crates-io))

(define-public crate-htmx-components-0.3.0 (c (n "htmx-components") (v "0.3.0") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "axum-flash") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rscx") (r "^0.1.11") (d #t) (k 0)) (d (n "rscx-web-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 1)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.5.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 0)))) (h "031xdpbqabc8i1fgd6pniga24ds9w96l9xp96nkgv603ziyi5j37")))

