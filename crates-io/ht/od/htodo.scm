(define-module (crates-io ht od htodo) #:use-module (crates-io))

(define-public crate-htodo-0.1.0 (c (n "htodo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1bwim5rnz681z3h5v2jqkm6r0igfragad8ajs5591cz9b9dai5p1")))

(define-public crate-htodo-0.1.1 (c (n "htodo") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0z77rr5fbb70k4li1n0xh0cad6bdr2hy7h65wdbgzm5wh9alr03p")))

(define-public crate-htodo-0.1.3 (c (n "htodo") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1i0l7dv36i8bs3ir6w477q9r7zrszx29fq64shfa407x4zzn8dkw")))

(define-public crate-htodo-0.1.4 (c (n "htodo") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "127s0ah70ppgm34mg2n9jp477qq89cr4031yizxibv7702z3plnv")))

(define-public crate-htodo-0.1.5 (c (n "htodo") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0v1lwwy7i6azpfvi7jgqnmmczg2zvmh0xkgyz00qmbvglmkd3l26")))

