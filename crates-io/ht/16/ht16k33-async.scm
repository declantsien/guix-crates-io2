(define-module (crates-io ht #{16}# ht16k33-async) #:use-module (crates-io))

(define-public crate-ht16k33-async-0.0.0 (c (n "ht16k33-async") (v "0.0.0") (d (list (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "1b5xjlm20cp39174kql5a48ib8xdlkvabx3bsa0w3ac1nh29ar3y")))

(define-public crate-ht16k33-async-0.0.1 (c (n "ht16k33-async") (v "0.0.1") (d (list (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "0dc0spbhs0hmm2zcw04kmv9l15x07nfaxcm0c0gljd8mlssr05ha")))

(define-public crate-ht16k33-async-0.0.2 (c (n "ht16k33-async") (v "0.0.2") (d (list (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "embassy-executor") (r "^0.5.0") (f (quote ("nightly" "arch-cortex-m" "executor-thread" "executor-interrupt" "integrated-timers"))) (d #t) (k 2)) (d (n "embassy-rp") (r "^0.1.0") (f (quote ("time-driver" "critical-section-impl"))) (d #t) (k 2)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1mqfxspgf4cs74zrrpxpl9qwp614g2x7ipydyjwgs3mvr01zif87")))

