(define-module (crates-io ht #{16}# ht16k33-lite) #:use-module (crates-io))

(define-public crate-ht16k33-lite-0.0.1 (c (n "ht16k33-lite") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "enum_index") (r "^0.2") (d #t) (k 0)) (d (n "enum_index_derive") (r "^0.2") (d #t) (k 0)))) (h "0i0247p6jllm0gqsx2995jimlp6riyw5ksqz31ns258lxvrkcjzs")))

(define-public crate-ht16k33-lite-0.0.2 (c (n "ht16k33-lite") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "enum_index") (r "^0.2") (d #t) (k 0)) (d (n "enum_index_derive") (r "^0.2") (d #t) (k 0)))) (h "1i2q9nzgg50la0j5f85s9zkghakmwj3sbxn218a851y9jbp9fa4j")))

