(define-module (crates-io ht tt httt) #:use-module (crates-io))

(define-public crate-httt-0.1.0 (c (n "httt") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sl80jhyrhsa5smlq95wq9c12a7wqw0d9hjqd03x1n4h48g7s6fr")))

