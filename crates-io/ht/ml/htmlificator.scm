(define-module (crates-io ht ml htmlificator) #:use-module (crates-io))

(define-public crate-htmlificator-0.1.0 (c (n "htmlificator") (v "0.1.0") (h "0ng9iqz3p268vyzi3ka8crk7sq11kns4bd63s2njirwypzvrwaa0")))

(define-public crate-htmlificator-0.2.0 (c (n "htmlificator") (v "0.2.0") (h "1r4y8m2a8hi61gp8x36g4hl7398gn2dhpzjqgsl4441idhgzq4p4")))

(define-public crate-htmlificator-0.2.1 (c (n "htmlificator") (v "0.2.1") (h "0czkd0x4j0w0fv7gr6aqcaiyjqpgp0ivmd6av80a29qncdznqbzq")))

