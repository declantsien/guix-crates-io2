(define-module (crates-io ht ml html-languageservice) #:use-module (crates-io))

(define-public crate-html-languageservice-0.1.0 (c (n "html-languageservice") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lsp-textdocument") (r "^0.3.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0dn377cvw5f6ljcpxwacjmyfr0ls686pxyfw5k67y3cmdnc9nffl")))

