(define-module (crates-io ht ml htmli) #:use-module (crates-io))

(define-public crate-htmli-0.1.0 (c (n "htmli") (v "0.1.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0z3cfwj8g3r5m37r56y26waf4ifmwyd1ig8flvnwn09qir0bfnn1")))

(define-public crate-htmli-0.1.1 (c (n "htmli") (v "0.1.1") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "012j2hywxs7fnykwdkag8ng1wdsc5ph9b57p70zrv1acmqzc7s6b") (y #t)))

(define-public crate-htmli-0.1.2 (c (n "htmli") (v "0.1.2") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "minifier") (r "^0.0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1k1z8yk7b7m3vj9mdy9rr2gcrxc6w27wnwsrrpyxvpn4pjkqzx0w")))

(define-public crate-htmli-0.1.3 (c (n "htmli") (v "0.1.3") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "minifier") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1drypwvi2wcidx3bzbq0a34wbgj56hv5nzc4wz6v8wkbf3bdr4f1")))

