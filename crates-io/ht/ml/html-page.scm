(define-module (crates-io ht ml html-page) #:use-module (crates-io))

(define-public crate-html-page-0.1.0 (c (n "html-page") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)))) (h "0z21sz29x867zqb7bnl6pda638g82lis04w2ckyf7sapxaq190rz")))

(define-public crate-html-page-0.2.0 (c (n "html-page") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)))) (h "02cq6a3l5c0672k1imz6qjrkkc2rjan5s7c66gyqj0n3y789fp6w")))

(define-public crate-html-page-0.2.1 (c (n "html-page") (v "0.2.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "02crapmih7qbgi392b9jhiwzrcli5gbfba1jn55lsmvcmkqvildi")))

(define-public crate-html-page-0.3.0 (c (n "html-page") (v "0.3.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0ha3la0c7kbix3fs7hpcdjvg1m61kjvbwzlgzfna82swgsc8iply")))

