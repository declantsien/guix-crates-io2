(define-module (crates-io ht ml html_compile) #:use-module (crates-io))

(define-public crate-html_compile-0.1.0 (c (n "html_compile") (v "0.1.0") (h "1h02gch913b2q5l4ppways1wx3r7advgw2nmnhzj2wanklp31nx2") (y #t)))

(define-public crate-html_compile-0.1.1 (c (n "html_compile") (v "0.1.1") (h "0gm5fmgh4ayaislpf5xv4cr4w0msx7i0dck7xbwy1dg72rh9hh0b")))

(define-public crate-html_compile-0.2.0 (c (n "html_compile") (v "0.2.0") (h "08sqcpvd0dnsxzda5fc30w9l82dq6dmvdln1m1y05cn7r7mxfrja")))

