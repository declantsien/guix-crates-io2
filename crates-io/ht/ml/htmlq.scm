(define-module (crates-io ht ml htmlq) #:use-module (crates-io))

(define-public crate-htmlq-0.1.0 (c (n "htmlq") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.23.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.7.3") (d #t) (k 0)))) (h "1sqp91nnh3v1k4d4c4a5k6ry1cglyciy114kxmk1g6fqjkdxqac3")))

(define-public crate-htmlq-0.2.0 (c (n "htmlq") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.23.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.7.3") (d #t) (k 0)))) (h "0gmbsnvri1hff31fhrsv9lhsdz7xrwbdxw9iqb6n0pnswjqrxj4b")))

(define-public crate-htmlq-0.3.0 (c (n "htmlq") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0wx3gfihaiknyywasi7lcph23r05icclryrxrkras496r5bjlmkx")))

(define-public crate-htmlq-0.4.0 (c (n "htmlq") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0912cdkz5xji1hzfj1cf42zh1kd860b52xmwwhb7q2jhp6qk25jh")))

