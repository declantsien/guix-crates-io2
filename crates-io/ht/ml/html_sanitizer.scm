(define-module (crates-io ht ml html_sanitizer) #:use-module (crates-io))

(define-public crate-html_sanitizer-0.1.0 (c (n "html_sanitizer") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "0m7q29cwj3g4yc8c969ggfp7qw0vbwj5kr6j6qzy0vrxzq7m8c4b")))

(define-public crate-html_sanitizer-0.1.1 (c (n "html_sanitizer") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "1q74yd4zsslz11wm9074jjf7r9y9iv538cs2klrr69zbfvs5qwpp")))

