(define-module (crates-io ht ml html) #:use-module (crates-io))

(define-public crate-html-0.0.2 (c (n "html") (v "0.0.2") (h "1f65qhbl58ixbd4h3qig6qbffq2h3kw4456lhz8r1b7in951hysv")))

(define-public crate-html-0.1.0 (c (n "html") (v "0.1.0") (h "1f08bmggwgy9qz28clv5gmpdp1myf0wixpg1hyq1q21kphxbwsk8")))

(define-public crate-html-0.0.1 (c (n "html") (v "0.0.1") (h "1csc6bajslbc4q3183scqrl4aqzy00mb43vy7agnp81m106w1drz")))

(define-public crate-html-0.1.1 (c (n "html") (v "0.1.1") (h "1q3vz1r3mscqw92a62s0k9ji1r96w04vnmv57z2bw1b6f6qgbsp0")))

(define-public crate-html-0.3.0 (c (n "html") (v "0.3.0") (d (list (d (n "html-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1by0hiavghs1lc9cp7ivh09wprwja318jfd3414z74azmvpxpn6g")))

(define-public crate-html-0.3.1 (c (n "html") (v "0.3.1") (d (list (d (n "html-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1p3vxlh8242lm1kmvn25c364lk43m54d63k8rdhzrnv0jxjqcmpi")))

(define-public crate-html-0.4.0 (c (n "html") (v "0.4.0") (d (list (d (n "html-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1kb3m5ajwmnrqw5zschaw98w038qsf2zbip5pdmbw5bql28l9akz")))

(define-public crate-html-0.4.1 (c (n "html") (v "0.4.1") (d (list (d (n "html-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0x9vpc61cgdl0b6cj52r7w4jqawm3zrvf8jzjm4x6vhbmi6q8an6")))

(define-public crate-html-0.4.2 (c (n "html") (v "0.4.2") (d (list (d (n "html-sys") (r "^0.2.2") (d #t) (k 0)))) (h "098v1ji347r91i968467zkmywka91vlhav5dp1bkf4pwz0rf0kn1")))

(define-public crate-html-0.5.0 (c (n "html") (v "0.5.0") (d (list (d (n "html-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0mzd3j0v56q5yp2rqb34jv1pks42fpdrdm08q1ipiai9508l6m4z")))

(define-public crate-html-0.5.1 (c (n "html") (v "0.5.1") (d (list (d (n "html-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1hc6pm01lzzrhl1q6qp5wdhdrsq785qypzjf4dddpif7jijsqgxw")))

(define-public crate-html-0.5.2 (c (n "html") (v "0.5.2") (d (list (d (n "html-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1kya610bdy2x80xv11zlm5pg6rsw3qd5gnak9iqa4ac04dvpafmw")))

(define-public crate-html-0.6.0 (c (n "html") (v "0.6.0") (d (list (d (n "html-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "057cnakazg46y5bj66jjsch3kq6gdrxsw98jv5696lnv45rpwp77")))

(define-public crate-html-0.6.1 (c (n "html") (v "0.6.1") (d (list (d (n "html-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0bkvfmz784g26v6d9fbkqfgkv3vr2d7cqbaf3vvkm1bmy71i9sjc")))

(define-public crate-html-0.6.2 (c (n "html") (v "0.6.2") (d (list (d (n "html-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0apan5dhyndwpi3m0b5273d05fmwic4pjpyjdz3c9dlmj78a17jh")))

(define-public crate-html-0.6.3 (c (n "html") (v "0.6.3") (d (list (d (n "html-sys") (r "^0.4.3") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1lh3b5w3xxf4l7k9fbwh8sycznzy31203wq2jda13iki32w7skcl")))

