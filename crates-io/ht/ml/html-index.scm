(define-module (crates-io ht ml html-index) #:use-module (crates-io))

(define-public crate-html-index-0.1.0 (c (n "html-index") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1j9820ajaxx51flsg2w7mj4k5crjzzdlbwjcwaadld24nrsx947r")))

(define-public crate-html-index-0.1.1 (c (n "html-index") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1dks5ap14mm8i2w3f60j981fqqfvil86w158vhkasahmb2l2aq2m")))

(define-public crate-html-index-0.1.2 (c (n "html-index") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0q0l1ny4xrfxqqffxvxjshq6d6yvsfk863h59plgb1h6x6hznc26")))

(define-public crate-html-index-0.1.3 (c (n "html-index") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "000fa25vjp6hgklylg6nbwb9j81idnqpki551nfdjpi1pkzm7s59")))

(define-public crate-html-index-0.1.4 (c (n "html-index") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0xk4viz5sygcdpwl0rgy98cqsc78a1n2k71drnqykbcy7604dm20")))

(define-public crate-html-index-0.1.5 (c (n "html-index") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "04cr01fcwrgl0k08aa8p61s2ixniyd70na26m1da22qim9j09vys")))

(define-public crate-html-index-0.1.6 (c (n "html-index") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1mwrxk69izr11wkmc3084fixsinkdc9qd94h6kbj9vpsdr0pnkms")))

(define-public crate-html-index-0.1.7 (c (n "html-index") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0qwibmd9hg762d5myq77i88cmnl39qanc17xqjbh0qii7ws20wgw")))

(define-public crate-html-index-0.2.0 (c (n "html-index") (v "0.2.0") (d (list (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0vm5vppylw9adgjj1nnndj3w5kdbqm3rj5k9b3ks0syg4zg41xxh")))

(define-public crate-html-index-0.3.0 (c (n "html-index") (v "0.3.0") (d (list (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)))) (h "1r8v3j5gd460599kid7x5z3519vx1vca1yfk2nvlvvr3ryiiayic")))

(define-public crate-html-index-0.3.1 (c (n "html-index") (v "0.3.1") (d (list (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)) (d (n "http-types") (r "^2.1.0") (d #t) (k 0)))) (h "1vjpliawrrzskjv9xkc6ykdafi67s902vv1nlvna1y6fm2qwmzwr")))

(define-public crate-html-index-0.3.2 (c (n "html-index") (v "0.3.2") (d (list (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)) (d (n "http-types") (r "^2.1.0") (d #t) (k 0)))) (h "143ldv8laywxnry6inwni5fb290gmvncbmpv6bsrn5vdigpikxhd")))

(define-public crate-html-index-0.3.3 (c (n "html-index") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)) (d (n "http-types") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "12bqy81i1hh7c7mnp7nz31aw63ngjxkp7mwdijbfgp21albaz6xy") (f (quote (("default" "http-types"))))))

(define-public crate-html-index-0.3.4 (c (n "html-index") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "css-rel-preload") (r "^0.1.0") (d #t) (k 0)) (d (n "http-types") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "1p9x469336sdgn8gncgz2bawhpn2cf0zgxj324pxaf01lkffsj7h") (f (quote (("default" "http-types"))))))

