(define-module (crates-io ht ml html-compare) #:use-module (crates-io))

(define-public crate-html-compare-0.1.0 (c (n "html-compare") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "css-compare") (r "^0.1") (d #t) (k 0)) (d (n "htmlparser") (r "^0.1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "0wk8zs75n90bj6smrxqx9qy2npld3n2s328wn7c19icgsmaxrl9f")))

(define-public crate-html-compare-0.1.1 (c (n "html-compare") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "css-compare") (r "^0.1") (d #t) (k 0)) (d (n "htmlparser") (r "^0.1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "0y9z2f5qaimnllf1qvf6iyh9w0qrr6fncdhi8g4yisplsj9pmd4d")))

(define-public crate-html-compare-0.1.2 (c (n "html-compare") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "css-compare") (r "^0.1") (d #t) (k 0)) (d (n "htmlparser") (r "^0.1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "04vm5k9q1njqwmmbn3fh2b2vzh8yg6ly90g8xpq0djz78bvnnqjc")))

