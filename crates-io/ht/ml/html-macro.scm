(define-module (crates-io ht ml html-macro) #:use-module (crates-io))

(define-public crate-html-macro-0.0.1 (c (n "html-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fs15awyg3nixa97yimr7c12ix07294h057qb8xpkqkirp09370h")))

(define-public crate-html-macro-0.0.2 (c (n "html-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13xfvmg6pxph0inminyk55zqwf7rgav0szc70xpp5vr2fgclijb7")))

(define-public crate-html-macro-0.1.0 (c (n "html-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "164v7y8va9qh8ivi55dk7yprrm38k3caha1c921cvcj81kdzm5d4")))

(define-public crate-html-macro-0.1.1 (c (n "html-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xnmd4ak3fdlws937wsqzdb9fis5nfrlkhac5wqjvwbgpsy2qcx9")))

(define-public crate-html-macro-0.1.2 (c (n "html-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xqsxh159f0v33li1hfzdhmxd1fn9c9nh5rf5wyyv41ny1nybhcl")))

(define-public crate-html-macro-0.1.4 (c (n "html-macro") (v "0.1.4") (d (list (d (n "html-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nz9rdl5jp83v6wv7g2g4qm6ccjjjnd7mk4hx2r2lpwmi4rwisf2")))

(define-public crate-html-macro-0.1.5 (c (n "html-macro") (v "0.1.5") (d (list (d (n "html-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c5m7jacxjbkjm8z4np4v0i9m2krlfj97j4yiqgjy2an50pc1a37")))

(define-public crate-html-macro-0.1.6 (c (n "html-macro") (v "0.1.6") (d (list (d (n "html-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "010283r455rk10cq90d7pf4s3456japhbyb0yhqni1lpwnwga4c7")))

(define-public crate-html-macro-0.1.7 (c (n "html-macro") (v "0.1.7") (d (list (d (n "html-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g0qllwld8w03gg9p35awg6bqra8w4d88whqvs5k29y34ay95k7a")))

(define-public crate-html-macro-0.1.8 (c (n "html-macro") (v "0.1.8") (d (list (d (n "html-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18gymm6iw96q4yxpwwjdvkanxd4pz2yrgxjlgzgd4mph6kyyszvk")))

(define-public crate-html-macro-0.1.9 (c (n "html-macro") (v "0.1.9") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17k8z339d59qivff5bgkb6zraqbj90sarwh0x4h5z8xjpxlnin2p")))

(define-public crate-html-macro-0.1.10 (c (n "html-macro") (v "0.1.10") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02zxyqg2r8gv994jd1wv20368pjd7zanfd5brznh5hwpbyr0z9ag")))

(define-public crate-html-macro-0.1.11 (c (n "html-macro") (v "0.1.11") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bnf2fhpnsfjfgn4xgdsxszqc4wyizz0r86w82j449yhbp8p92ix")))

(define-public crate-html-macro-0.1.12 (c (n "html-macro") (v "0.1.12") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0smidlj5w6i97kdnxgmi7yv0qy5k4kj98s1yxl8p3zdgy6h2jw8q")))

(define-public crate-html-macro-0.1.13 (c (n "html-macro") (v "0.1.13") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rjaljw6745z5879v44szmw02q5cs3xl2gqjyqaj8pknmi9sd4aj")))

(define-public crate-html-macro-0.1.14 (c (n "html-macro") (v "0.1.14") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i1qgqfvgw91s2bjfsiaxz6pkb59qqjvsx8yi71cqmsci4l93h9q")))

(define-public crate-html-macro-0.1.15 (c (n "html-macro") (v "0.1.15") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x4rg2n1b97kn4653g329bwh0prhng2licmfd8p7sakps2c45mzp")))

(define-public crate-html-macro-0.2.0 (c (n "html-macro") (v "0.2.0") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c1s2grh6d0qp2icjmjal8yz6i03k3lihnhggwcr0a7km90adwkq")))

(define-public crate-html-macro-0.2.1 (c (n "html-macro") (v "0.2.1") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qq427dblhc97rc085ckgqkc8629l22zmvzd3gp7752s0a44h99k")))

(define-public crate-html-macro-0.2.2 (c (n "html-macro") (v "0.2.2") (d (list (d (n "html-validation") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10ygm09qdkn3ssh20m85m8cidw4awz6faqzyadwimixmn69f9n9j")))

