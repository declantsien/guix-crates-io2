(define-module (crates-io ht ml html2runes) #:use-module (crates-io))

(define-public crate-html2runes-1.0.0 (c (n "html2runes") (v "1.0.0") (d (list (d (n "clap") (r "~2.20.5") (d #t) (k 0)) (d (n "html5ever") (r "~0.13.0") (d #t) (k 0)) (d (n "html5ever-atoms") (r "~0.2.0") (d #t) (k 0)) (d (n "tendril") (r "~0.2.3") (d #t) (k 0)))) (h "06g41iy7qwyy56832f780imrxxclwpi7danmv2amgnpjdb82jmq7")))

(define-public crate-html2runes-1.0.1 (c (n "html2runes") (v "1.0.1") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.13.0") (d #t) (k 0)) (d (n "html5ever-atoms") (r "^0.2.0") (d #t) (k 0)) (d (n "tendril") (r "^0.2.3") (d #t) (k 0)))) (h "0nbm554zydv4mnkz0xkj37w4khlny149xs1ll9fwg73hs06n0qrw")))

