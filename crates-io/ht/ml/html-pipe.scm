(define-module (crates-io ht ml html-pipe) #:use-module (crates-io))

(define-public crate-html-pipe-0.1.0 (c (n "html-pipe") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1f3qpwpjir9xy97145457jvw3y427l7n0yj5p9rs64bljhkyvfmz")))

(define-public crate-html-pipe-0.1.1 (c (n "html-pipe") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0jij8n0wpcd7fnzrhmsn9c5q40smwfjhgjz1fr085j6722bgdh1k")))

