(define-module (crates-io ht ml html5minify-cli) #:use-module (crates-io))

(define-public crate-html5minify-cli-1.0.0 (c (n "html5minify-cli") (v "1.0.0") (d (list (d (n "html5minify") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i4b2rv0qhizmjxzi9crha6nmdm52p40n7qfwdar4ffzznck219w")))

