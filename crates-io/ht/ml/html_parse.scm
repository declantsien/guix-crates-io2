(define-module (crates-io ht ml html_parse) #:use-module (crates-io))

(define-public crate-html_parse-0.1.2 (c (n "html_parse") (v "0.1.2") (d (list (d (n "forest-ds") (r "^1.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)))) (h "1y19jfl8pzwnnh64kl6pynsvym7bpzxlxriqqcbzjmbqyn8fnm6k")))

(define-public crate-html_parse-0.1.3 (c (n "html_parse") (v "0.1.3") (d (list (d (n "forest-ds") (r "^1.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)))) (h "1dhaxrh2xqvrpgj07xcjjhxpqazw3s8d9pghyfjwqli4ml9bxc4v")))

(define-public crate-html_parse-1.0.0 (c (n "html_parse") (v "1.0.0") (d (list (d (n "forest-ds") (r "^1.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l8v5ymwhjd63ldr103if7yy57vnsqic94xbfwpbm274mjs4vsaf") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "forest-ds/serde" "indexmap/serde"))))))

(define-public crate-html_parse-1.1.1 (c (n "html_parse") (v "1.1.1") (d (list (d (n "forest-ds") (r "^1.1.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "17x3wncvjs56rwqwd2y9smi8pwpwgf146qvn1086cy6mbm1af2gl") (s 2) (e (quote (("serde" "dep:serde" "forest-ds/serde" "indexmap/serde"))))))

(define-public crate-html_parse-1.1.2 (c (n "html_parse") (v "1.1.2") (d (list (d (n "forest-ds") (r "^1.1.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0306hjlxkzj893s3kk27l38q6lf2j0n6pc23zbsfgp96n6als388") (s 2) (e (quote (("serde" "dep:serde" "forest-ds/serde" "indexmap/serde"))))))

