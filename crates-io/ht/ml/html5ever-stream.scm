(define-module (crates-io ht ml html5ever-stream) #:use-module (crates-io))

(define-public crate-html5ever-stream-0.1.0 (c (n "html5ever-stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1ckapd9m5zxwfp0znkdr7ismwzls6sa0sc519gxs1y3y5x29mqxw")))

