(define-module (crates-io ht ml html-types) #:use-module (crates-io))

(define-public crate-html-types-0.1.0 (c (n "html-types") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)))) (h "0rl1mayw2cw2br8jcfgxwgan3jjpw0pmh3q0igr6d8fw86rmhrc5")))

(define-public crate-html-types-0.2.0 (c (n "html-types") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)))) (h "1k72900zbkpp8fqzi2s10zpjlc4hc7s7wnq2syl75kpjd7l3d41d")))

(define-public crate-html-types-0.2.1 (c (n "html-types") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)))) (h "1lg84cdcb5xh1hw2mn4gnckcjgkg4cmjan1clf2b650fjxsw6crw")))

(define-public crate-html-types-0.3.0 (c (n "html-types") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)))) (h "17zpw1k4gs47vq884fcsz2yh01yqrd5mmkzijd5d5xj2g030b2a6")))

(define-public crate-html-types-0.4.0 (c (n "html-types") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)))) (h "16gjyj14hy4jr7lri5k4njzlbb0cac86gsqv5zqbr9dl1hqpkc15")))

