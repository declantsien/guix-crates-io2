(define-module (crates-io ht ml html-node-core) #:use-module (crates-io))

(define-public crate-html-node-core-0.1.12 (c (n "html-node-core") (v "0.1.12") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "12ck32l379kzfyhkasgzd6cvrc1v04cj41m27n0373rxg60skckw") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.13 (c (n "html-node-core") (v "0.1.13") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1lrdxcpcq1aqb17s5sz6c4wzlh4b97mvh93ajzl4051g2ac9vmfb") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.14 (c (n "html-node-core") (v "0.1.14") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "196c2w9lpa8l8jlbgy6iy46mcx6j55jhsd7wmr1cgs1p2w16px0k") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.15 (c (n "html-node-core") (v "0.1.15") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "017l7c4b46c9j4r2albmqr06cfjvab1drr2bzpmks2ka5k8936za") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.16 (c (n "html-node-core") (v "0.1.16") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0s7jal8pal7v4kgk0lxkfqac19c3wpz5qdm4ag0ymp43y1s21lw7") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.17 (c (n "html-node-core") (v "0.1.17") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0f5f5gx03gva551rjwy9bql4s9ijpp33achzbx1dh30qxhsq5wnl") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.1.18 (c (n "html-node-core") (v "0.1.18") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0v1mglnhbns1ig3rllfmzcjjbj83dkzjaa0s2f51g2hsis96cl6i") (f (quote (("typed")))) (s 2) (e (quote (("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.2.0 (c (n "html-node-core") (v "0.2.0") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kzrwj12bqqmirjd161klh2vrp9i31nxja9qgawr160wxy55srj6") (f (quote (("typed")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.2.1 (c (n "html-node-core") (v "0.2.1") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lgvnn5x6g3b6sfif7yq86gisgccrrh8fvqpchnrnfjlmwbxqhcd") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.2.2 (c (n "html-node-core") (v "0.2.2") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mniwgng1yf4vnklzar53hrnv4magkm0817n2zy3w695ynxa4q4n") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.2.3 (c (n "html-node-core") (v "0.2.3") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cq3jjvv065k2q9c7kf9al60cgr8fz3r21yavh3lhlqzp9yqx8sm") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.3.0 (c (n "html-node-core") (v "0.3.0") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0x6d6krhll1sxa4dfzc5k2gq7vw10hras7f5qmx1lna74z84lqb6") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.4.0 (c (n "html-node-core") (v "0.4.0") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dfw2g32396xc9apxlmkq9jqsvk0n9q8lnd4m9dv0r48h311kgr2") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

(define-public crate-html-node-core-0.5.0 (c (n "html-node-core") (v "0.5.0") (d (list (d (n "axum") (r "^0.6") (o #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h4bsadw9kf7m5mx0xpnikpxxn2r9vqwiyzwx85zcab7g4qqcdlp") (f (quote (("typed") ("pretty")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

