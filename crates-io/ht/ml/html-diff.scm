(define-module (crates-io ht ml html-diff) #:use-module (crates-io))

(define-public crate-html-diff-0.0.1 (c (n "html-diff") (v "0.0.1") (d (list (d (n "html5ever-atoms") (r "^0.2.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.4.3") (d #t) (k 0)))) (h "1hwxcmpljwa4hhl27r7sdbiqkrnd7vgjlkj8y1pn8v7dzdfb10ym")))

(define-public crate-html-diff-0.0.2 (c (n "html-diff") (v "0.0.2") (d (list (d (n "html5ever-atoms") (r "^0.2.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.4.3") (d #t) (k 0)))) (h "1mgy4q7p4fdd1pmys51kfajm6czdzikbgjjshl7zn8rnsn70kkcd")))

(define-public crate-html-diff-0.0.3 (c (n "html-diff") (v "0.0.3") (d (list (d (n "kuchiki") (r "^0.5.1") (d #t) (k 0)))) (h "1ix7zybybiqfcrb1lnk7yq881nycfs62xwlzrwjnkrfa8d4wdnqp")))

(define-public crate-html-diff-0.0.4 (c (n "html-diff") (v "0.0.4") (d (list (d (n "kuchiki") (r "^0.5.1") (d #t) (k 0)))) (h "0s4crd4wy5d8g42yy7xzh5fnqf1sn3fl0mwnrs7m0hm6h4qdd62j")))

(define-public crate-html-diff-0.0.5 (c (n "html-diff") (v "0.0.5") (d (list (d (n "kuchiki") (r "^0.6") (d #t) (k 0)))) (h "1ik4mmvmzvqj3wl3pqqr1559zwchqqriivghf7s7jdiw7cz78y4p")))

(define-public crate-html-diff-0.0.6 (c (n "html-diff") (v "0.0.6") (d (list (d (n "kuchiki") (r "^0.6") (d #t) (k 0)))) (h "1xs5adb5cc7a2ywj2dm613dnd4zr6wnmc3xqv703ljj85bvgsk7f")))

