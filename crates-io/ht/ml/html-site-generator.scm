(define-module (crates-io ht ml html-site-generator) #:use-module (crates-io))

(define-public crate-html-site-generator-0.0.1-alpha (c (n "html-site-generator") (v "0.0.1-alpha") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "new_string_template") (r "^1.4.0") (d #t) (k 0)))) (h "1a5m51pjn43f6nm37mgr87ih8zdhwy7pw652xngr48nyziz37930")))

(define-public crate-html-site-generator-0.0.1 (c (n "html-site-generator") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "html-site-generator-macro") (r "^0.0.1") (d #t) (k 0)))) (h "1jhi831hng5fp81rg3a8cgjc2hsp1qzlr8fas5pif528q0db87dr")))

