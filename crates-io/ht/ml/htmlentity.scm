(define-module (crates-io ht ml htmlentity) #:use-module (crates-io))

(define-public crate-htmlentity-0.1.0 (c (n "htmlentity") (v "0.1.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "grcov") (r ">=0.5.15, <0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r ">=0.2.68, <0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0b3c5lwyb9vaiprn346wdpckhmih9f2drdhg9bhbwi5zgbzmvgsa")))

(define-public crate-htmlentity-0.1.1 (c (n "htmlentity") (v "0.1.1") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "grcov") (r ">=0.5.15, <0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r ">=0.2.68, <0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0f0byipl4gsysvfj3q7d4g04m0v5lb7ya6nrlgw049xki4sldblm")))

(define-public crate-htmlentity-0.1.2 (c (n "htmlentity") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.5.15") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0s3lp86a7syi4h0fxdqz2h536khyrycfx5jx09cd4w4akn4j7hwa")))

(define-public crate-htmlentity-1.0.0 (c (n "htmlentity") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.5.15") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0anizgxdplc0fqaypizqwwfd08gn3d6096km1d4svv8cmpif426g")))

(define-public crate-htmlentity-1.0.1 (c (n "htmlentity") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.5.15") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0wwbqbbczh03vfya7im5fl4rq39syf4dbil2hi6hd2jcxcasv999")))

(define-public crate-htmlentity-1.0.2 (c (n "htmlentity") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.5.15") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "016i5wvyxgmpjclqhaa8rjgx6m4hp3jmgpvcg60krs330v9a2nza")))

(define-public crate-htmlentity-1.1.0 (c (n "htmlentity") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.6.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0wz1p8nv0glwzi65d7m2n3rl2visgv56sb842ymzna74vb78chi3")))

(define-public crate-htmlentity-1.1.1 (c (n "htmlentity") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.6.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1a29dps3awj00pvwy4ydhfr3c2r7k7z69fkayz789q2swby7zyy1")))

(define-public crate-htmlentity-1.1.2 (c (n "htmlentity") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "00x9k12zqr7xq600grlgm16lnvl85lzyl0i0wrvf7h7ggh685qd0")))

(define-public crate-htmlentity-1.1.3 (c (n "htmlentity") (v "1.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ixvv6i3ivc0wr30myvgb8ff8yllhrlyijg69yv2x0zl12n2852n")))

(define-public crate-htmlentity-1.2.0 (c (n "htmlentity") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "grcov") (r "^0.6.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0b1kl94zm0f8cys0mf2z12kmggp3vsv13zp9izsf6ifc801jsgav")))

(define-public crate-htmlentity-1.3.0 (c (n "htmlentity") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "grcov") (r "^0.8.16") (d #t) (k 2)))) (h "09mssg2b7wa9s7v85bqxclhl5yprbysi5xyahcays1pabx2m2xxf")))

(define-public crate-htmlentity-1.3.1 (c (n "htmlentity") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "grcov") (r "^0.8.16") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kcmpdnk1s8mywx3084qc28kh0xqkrrg1n5bgjviidln8x71vcds")))

