(define-module (crates-io ht ml html-extractor) #:use-module (crates-io))

(define-public crate-html-extractor-0.1.0 (c (n "html-extractor") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "0i82pdm5c3j8x4wym9r86ms913js7sbnnar20rf54l4sckbkz8cv")))

(define-public crate-html-extractor-0.1.1 (c (n "html-extractor") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "0i5zr50gyway2b41z0qrk1j0nmwywd4cdsnkmr9nn86b2bajmmwc")))

(define-public crate-html-extractor-0.2.0 (c (n "html-extractor") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "= 0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "0pzfw3lhi0yn3b5q9nm9chccwjz1j2mg255hrvws0mrjaqhpdgf0")))

(define-public crate-html-extractor-0.2.1 (c (n "html-extractor") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "= 0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "011mbcjna392qz2dcfbnakm5zrgbh17mnw69ms71fwqppc6ims3i")))

(define-public crate-html-extractor-0.3.0 (c (n "html-extractor") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "= 0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "14xcl7g3x283wllm7q9af81jhccx51l8jyxfhznkm3d8vdsqzi9q")))

(define-public crate-html-extractor-0.4.0 (c (n "html-extractor") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "html-extractor-macros") (r "= 0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "0pn02p2sv0rkh1al1bkwapmwhv9m4qjm1r1xh5xfpqdc21b34jsi")))

(define-public crate-html-extractor-1.0.0 (c (n "html-extractor") (v "1.0.0") (d (list (d (n "html-extractor-macros") (r "=1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0vvqq29k8ailmg0nd3j7xfb2zgsfxwsrrj3c3jamn5lnfdv0gpcr")))

