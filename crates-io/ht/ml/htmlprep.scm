(define-module (crates-io ht ml htmlprep) #:use-module (crates-io))

(define-public crate-htmlprep-0.0.2 (c (n "htmlprep") (v "0.0.2") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "039d84dfrlacf16bmpby41cv5gmpqmj0yyn5d18cprl9jhz153cr")))

(define-public crate-htmlprep-0.0.3 (c (n "htmlprep") (v "0.0.3") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0c8scgypmsvf4kyxgqnqnps92q4z370jgx0bkhj5jgli84h9pbv3") (r "1.70")))

(define-public crate-htmlprep-0.0.4 (c (n "htmlprep") (v "0.0.4") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1blrrfcl5gw8syn1hnr4p1npjjyl508axsmd4iyyrj7jhzhyp6xs") (r "1.70")))

(define-public crate-htmlprep-0.0.5-a (c (n "htmlprep") (v "0.0.5-a") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1db86hn9djvc2ny3lcwhbhj9g41wznchapiwk3xcn5l94dvgvpfz") (r "1.70")))

(define-public crate-htmlprep-0.0.5-c (c (n "htmlprep") (v "0.0.5-c") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08c03d95m9na58s8cvysd39vbb0nxk6idc323n3h53brd05prbxd") (r "1.70")))

(define-public crate-htmlprep-0.0.5 (c (n "htmlprep") (v "0.0.5") (d (list (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "11bdhlgdxvv26xjf4wcfmm6l0rj60708hasqwhkbc6csh1s2iq1i") (r "1.70")))

