(define-module (crates-io ht ml html5tokenizer) #:use-module (crates-io))

(define-public crate-html5tokenizer-0.1.0 (c (n "html5tokenizer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 2)))) (h "1jsrpl867q9i57hi6nqfi9dxqp8386dnmixx3yh61ngvzgjndx9g") (y #t)))

(define-public crate-html5tokenizer-0.2.0 (c (n "html5tokenizer") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (o #t) (d #t) (k 1)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 2)))) (h "086pjlki52czn2r9a2fcq2vlarx7kvcsi5gdvkrrrgfwicdl1fx3") (f (quote (("named-entities" "phf" "phf_codegen") ("default" "named-entities")))) (y #t)))

(define-public crate-html5tokenizer-0.3.0 (c (n "html5tokenizer") (v "0.3.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (o #t) (d #t) (k 1)))) (h "1nla3cvampqfgb46vlgdbflq3cidhml9yb767n8kvc7fblkawbyn") (f (quote (("spans") ("named-entities" "phf" "phf_codegen") ("default" "named-entities")))) (y #t)))

(define-public crate-html5tokenizer-0.4.0 (c (n "html5tokenizer") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "1cv0k8adq1mrhz43mn8cagffg99v65zf6y3xixhh2jhlks8igl94") (f (quote (("integration-tests")))) (y #t)))

(define-public crate-html5tokenizer-0.5.0 (c (n "html5tokenizer") (v "0.5.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "1dkpy2vac93l6681cz4cbxvv10kk3plslqyf52wdr65gbbnbwz51") (f (quote (("integration-tests"))))))

(define-public crate-html5tokenizer-0.5.1 (c (n "html5tokenizer") (v "0.5.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "0fkmv17fqhikhcbkz0gaqwsmvcab4cfad7fiby655isiy3fg40df") (f (quote (("integration-tests"))))))

(define-public crate-html5tokenizer-0.5.2 (c (n "html5tokenizer") (v "0.5.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "1zlsl86qwrzglhaplkcqcxrgb3iq7hawplk3xz6lbgqrgzwi62bz") (f (quote (("integration-tests"))))))

