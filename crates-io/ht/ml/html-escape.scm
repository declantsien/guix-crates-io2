(define-module (crates-io ht ml html-escape) #:use-module (crates-io))

(define-public crate-html-escape-0.1.0 (c (n "html-escape") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1xa4w548n0cdwfkbaajl3his0vqh7qpydn7x30iz4kg4amglnfzm") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.0 (c (n "html-escape") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1v8vdi4jf79ypri77jyivhjimhfavjnq7wgfdflyvsfbrr93w1ci") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.1 (c (n "html-escape") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "03l8jnn70hh6pp9936vnykcp704q4q02i49ms7a7sfd34l1bqzh9") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.2 (c (n "html-escape") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0i1kl2pjhc8dm9y1l5wyad83zma1nrfqcpcc2jn2d4l3b2kpfm9q") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.3 (c (n "html-escape") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1fw2qv8kmnq2c1qycx9jfn5simkhi72144y2wsszngpzkwv8rvwz") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.5 (c (n "html-escape") (v "0.2.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1yvzga88s3z8ziqnjxvy7vmbk6902lx398dm3v9b8xbkqnkmq63w") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.4 (c (n "html-escape") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0axyr5ygg3w4zppdvinlbjawckvcz2qna3r15a2r6i2yd4irc4vg") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.6 (c (n "html-escape") (v "0.2.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "13lqmbp8bkhnnkfkjabaaks7slasfg9jx4msjm1lgds1x4690j6k") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.7 (c (n "html-escape") (v "0.2.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0cr2r4zmc2rs6q60qifkwf78xkk9fdxpmm2f6dcy95sy4sr98iyn") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.8 (c (n "html-escape") (v "0.2.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0yypqkm59p0dn43z7v0cq3hzglgfw9182agbh0yf1jqgrazzx342") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.9 (c (n "html-escape") (v "0.2.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1dxw9lpckrqzzqgbkw64ckbajr4b7xxnjdn8adgzqf2mm40shvl1") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.10 (c (n "html-escape") (v "0.2.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1gsnhqjsr12f2faygphrh5g97gn2n48yv0wmi2zqfln7n914sn4v") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.11 (c (n "html-escape") (v "0.2.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0v62vdrz02p5sgpvhv7hcg8g4paipqkc8hkazd4ynf7gl6glgrxq") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.12 (c (n "html-escape") (v "0.2.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "01f2v3c6j2rk5h2lhdbh62j07cm1fvzqw4vplj2sms83jpx5qc8m") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-escape-0.2.13 (c (n "html-escape") (v "0.2.13") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0xml3hswv0205fbm5iq7dqiwjkr6d245xkfppwi7wqjdfr4x86kd") (f (quote (("std") ("default" "std"))))))

