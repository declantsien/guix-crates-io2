(define-module (crates-io ht ml html-validation) #:use-module (crates-io))

(define-public crate-html-validation-0.1.0 (c (n "html-validation") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "14q4xmh66n2rs99w0gyhai5wsr9bz4b8zlyin38cd6nnar207ag9")))

(define-public crate-html-validation-0.1.1 (c (n "html-validation") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0jwc78sq1w405fy3gngyym198x2cmaf0hryl0s0d8ff432i0dr4d")))

(define-public crate-html-validation-0.1.2 (c (n "html-validation") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1cb44j9r9rxf784gihlm2cipypgs0lygg3kyy2rdh2px2mgzdchx")))

