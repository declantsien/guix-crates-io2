(define-module (crates-io ht ml html5minify) #:use-module (crates-io))

(define-public crate-html5minify-0.1.0 (c (n "html5minify") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.23") (d #t) (k 0)))) (h "01q0ph8258ylh290fakpqmclwi4zszi2v0g3f4by4drx03fk4jyz")))

(define-public crate-html5minify-0.2.0 (c (n "html5minify") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "1xmdvs6bd2sphywqqqhk12mavz4q34nxs67m5vzr87blqbn87mg7")))

(define-public crate-html5minify-0.2.1 (c (n "html5minify") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "0fz6lxz1kkp8jrl91s7r7x552j79k73cvmv0m5b20zzah8s3jbp9")))

(define-public crate-html5minify-0.3.0 (c (n "html5minify") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "1a6knbj48f49yjdlwllkp04xk975d0xvpxznna2hrwqj288qbrr9")))

(define-public crate-html5minify-0.3.1 (c (n "html5minify") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "1si54p2y88m6jp59vqfq3cac7ivp28hjyywpqmbsy9iwqca8q6cw")))

(define-public crate-html5minify-0.3.2 (c (n "html5minify") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "1pv1a3m9kzb2s959bvx0jgck93lrlcj43fwalkks0636qi5pnvsg")))

(define-public crate-html5minify-0.3.3 (c (n "html5minify") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "05ssqvkgydmf8pxrid4fmbyig5cpywrm56ks2z0rzll76ycif1rz")))

