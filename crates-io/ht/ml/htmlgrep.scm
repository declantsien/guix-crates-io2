(define-module (crates-io ht ml htmlgrep) #:use-module (crates-io))

(define-public crate-htmlgrep-0.1.0 (c (n "htmlgrep") (v "0.1.0") (d (list (d (n "kuchiki") (r "^0.7") (d #t) (k 0)))) (h "1sv37xz991am0rvni9cpj1yy2nf8m5ml70g2mx17h2733yk0rfpq")))

(define-public crate-htmlgrep-0.2.0 (c (n "htmlgrep") (v "0.2.0") (d (list (d (n "kuchiki") (r "^0.7") (d #t) (k 0)))) (h "1x9cpaicw0q29ggjj3fyr37chjrzmlh8vgr0l6jx2mjyzikabb7k")))

(define-public crate-htmlgrep-0.2.2 (c (n "htmlgrep") (v "0.2.2") (d (list (d (n "kuchiki") (r "^0.7") (d #t) (k 0)))) (h "19mjmqba2gdxns05m5gxppi2wg9li48x1ripi7cvdq16l5hlcxzs")))

(define-public crate-htmlgrep-0.3.0 (c (n "htmlgrep") (v "0.3.0") (d (list (d (n "kuchiki") (r "^0.7") (d #t) (k 0)))) (h "1n6gpbarxfr4l7i8fgffb0hcd433hqsli8rcv4687rzp2iag84h4")))

(define-public crate-htmlgrep-0.3.1 (c (n "htmlgrep") (v "0.3.1") (d (list (d (n "kuchiki") (r "^0.7") (d #t) (k 0)))) (h "13zhnr1110axzvr6yxxd4qvvxzw9cqq68ls5whg92nnf6qycy6m3")))

