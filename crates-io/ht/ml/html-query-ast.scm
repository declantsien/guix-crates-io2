(define-module (crates-io ht ml html-query-ast) #:use-module (crates-io))

(define-public crate-html-query-ast-0.1.1 (c (n "html-query-ast") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "074x8j4i622jf1rjb9v8md76mbrds551388p4lq8af1w5gq3abqg")))

(define-public crate-html-query-ast-0.2.0 (c (n "html-query-ast") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)))) (h "0lzx9jgaxlk1cdbywapwx1pfvxhrfasmksqin3ki3pf2nbk5pv3w")))

(define-public crate-html-query-ast-0.2.1 (c (n "html-query-ast") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)))) (h "09ffgqr78x51xyz0i3wvfyixqrk1cb8k1by66a9qic27fxn5nz2p")))

(define-public crate-html-query-ast-0.2.2 (c (n "html-query-ast") (v "0.2.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)))) (h "1cdnb6l0il6m384jh60cdyf7q482khc7hmzp3fgadpqpyvvdh8jy")))

