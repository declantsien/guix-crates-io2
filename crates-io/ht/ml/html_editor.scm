(define-module (crates-io ht ml html_editor) #:use-module (crates-io))

(define-public crate-html_editor-0.1.0 (c (n "html_editor") (v "0.1.0") (h "187iffi0mlfmgfh0hi9g29vpj7xql2bkhfsj2wc5vn8418h17j32")))

(define-public crate-html_editor-0.2.0 (c (n "html_editor") (v "0.2.0") (h "0q8q79dzkh0j9p3brib6z2a0c4g5glj0vhgakmzrmmly5ybbjml2")))

(define-public crate-html_editor-0.3.0 (c (n "html_editor") (v "0.3.0") (h "15an4sfqy88dc9n7607zf69q8vcx3nb9s638fcp9x6lya8q5pb9a")))

(define-public crate-html_editor-0.4.0 (c (n "html_editor") (v "0.4.0") (h "0kik9kvddm8lk2aab5hibx9allvqk3rc1ar40s5p81saw73hb8hr")))

(define-public crate-html_editor-0.5.0 (c (n "html_editor") (v "0.5.0") (h "0qlfgdsj60ya84jp1ns1yqknsxyqrqpacf6xjc5cskg6pc46h0cr")))

(define-public crate-html_editor-0.5.1 (c (n "html_editor") (v "0.5.1") (h "03s1cn69dah6wlnjjgra32m8njsqzjz0a7dgbv1dd1mlr6vwagbv")))

(define-public crate-html_editor-0.5.2 (c (n "html_editor") (v "0.5.2") (h "0vgm3n8y08bjrfssvw6rg7301i8lpicvxxbzlb9745f5lza78qlm")))

(define-public crate-html_editor-0.5.3 (c (n "html_editor") (v "0.5.3") (h "1jma4finax519qcbdz917806dsi68nmrcw9wp7d7hplsgqj4imxh") (y #t)))

(define-public crate-html_editor-0.6.0 (c (n "html_editor") (v "0.6.0") (h "066mjxk80ggqbvhgk1c5xcczfw5msia9s2iv7x824hdr65bbgwf7") (y #t)))

(define-public crate-html_editor-0.6.1 (c (n "html_editor") (v "0.6.1") (h "08af4kd4rs5c2jdbxfxlvyh226wsvk88czilpnackkk437sx301d")))

(define-public crate-html_editor-0.7.0 (c (n "html_editor") (v "0.7.0") (h "1h2zdi0xjvbr0salqpgxcn5qqqycnbxk6bhmxl2i4nip716w64xc")))

