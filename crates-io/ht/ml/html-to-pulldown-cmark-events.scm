(define-module (crates-io ht ml html-to-pulldown-cmark-events) #:use-module (crates-io))

(define-public crate-html-to-pulldown-cmark-events-0.1.0 (c (n "html-to-pulldown-cmark-events") (v "0.1.0") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (k 0)))) (h "1i5a5xg6rw584rk7h1d9f7bbw3rwscy0wllmjb5hv9hza9jwb1fx")))

(define-public crate-html-to-pulldown-cmark-events-0.1.1 (c (n "html-to-pulldown-cmark-events") (v "0.1.1") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)))) (h "0pf27bakq70hha1rdsxj704rljsd0qxlv3wqpp4f1hamcrm0bymx")))

(define-public crate-html-to-pulldown-cmark-events-0.1.2 (c (n "html-to-pulldown-cmark-events") (v "0.1.2") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "182cvxvpnz92kff5z9c03m2rdr0v42is3kzmys8hjf3yp6q16p7n")))

(define-public crate-html-to-pulldown-cmark-events-0.1.3 (c (n "html-to-pulldown-cmark-events") (v "0.1.3") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1648v6qp6ivq2kv5shx1l3r4qr0xhwry9zziyld7lcaxa7s4jc72")))

(define-public crate-html-to-pulldown-cmark-events-0.1.4 (c (n "html-to-pulldown-cmark-events") (v "0.1.4") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "19nf93kcbiwn0blzb6q9v260a82a7gyvdbsfpzigpmciilgh2bbz")))

(define-public crate-html-to-pulldown-cmark-events-0.1.5 (c (n "html-to-pulldown-cmark-events") (v "0.1.5") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "0gwa274xqv28y3lm39akwbp3jyydmaqvj6h380dy2xq0ck8n7g3b")))

(define-public crate-html-to-pulldown-cmark-events-0.1.6 (c (n "html-to-pulldown-cmark-events") (v "0.1.6") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "166nq3sw4w54bbgwvc2zq67ymggs01d0hpdlh75r1g2rhffd388x")))

(define-public crate-html-to-pulldown-cmark-events-0.1.7 (c (n "html-to-pulldown-cmark-events") (v "0.1.7") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1cxd6b6a5g081r6n9g77918n20dwx3jsflkymvrkyc8nig0cazdf")))

(define-public crate-html-to-pulldown-cmark-events-0.1.8 (c (n "html-to-pulldown-cmark-events") (v "0.1.8") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1cg96bpzrvxwyjjmhh9z8zrqwbw29sbr5a7rwfqqzig5vwy361ca")))

(define-public crate-html-to-pulldown-cmark-events-0.1.9 (c (n "html-to-pulldown-cmark-events") (v "0.1.9") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "0yfx0bpqavjdwfpp8w46348qgjn6kqr50jvhrgsh4h6m5lh94vvp")))

(define-public crate-html-to-pulldown-cmark-events-0.1.10 (c (n "html-to-pulldown-cmark-events") (v "0.1.10") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)))) (h "0fkqnvgrcimhv17gf22n98pwcbg3gc14v225a964mwys3wrhs005")))

(define-public crate-html-to-pulldown-cmark-events-0.1.11 (c (n "html-to-pulldown-cmark-events") (v "0.1.11") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)))) (h "14f12wbjjn549c2q1r9irga9b7hp1qqh8767zc2gzb2yaqx3ffmh")))

(define-public crate-html-to-pulldown-cmark-events-0.1.12 (c (n "html-to-pulldown-cmark-events") (v "0.1.12") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)))) (h "1fn55f11cx3nq7b4f809zzvcn1hxfpis0ga5zcwnq6in7qmmv1cw")))

