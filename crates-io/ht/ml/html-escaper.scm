(define-module (crates-io ht ml html-escaper) #:use-module (crates-io))

(define-public crate-html-escaper-0.0.0 (c (n "html-escaper") (v "0.0.0") (h "1lnj0cakcz7cspas7qxxrvagda9w2a8vwlj7pyfnhds44wp48xwk")))

(define-public crate-html-escaper-0.1.0 (c (n "html-escaper") (v "0.1.0") (h "0xd055lc4dn0b47nxalvzania2gmjan3zg1cws79afbaha3217i1")))

(define-public crate-html-escaper-0.2.0 (c (n "html-escaper") (v "0.2.0") (h "0ammzkhjbpb2crrla61lnsgw1ldxsgr785xvlgh529g97sihr6j5")))

