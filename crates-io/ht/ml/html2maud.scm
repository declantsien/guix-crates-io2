(define-module (crates-io ht ml html2maud) #:use-module (crates-io))

(define-public crate-html2maud-0.1.0 (c (n "html2maud") (v "0.1.0") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0n93bvaa7h6zxdy1f18rhpixi5bvc060z1zqhnswcbp0q77ddw94")))

(define-public crate-html2maud-0.1.1 (c (n "html2maud") (v "0.1.1") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "08qi0ndc446m9cl9axlc13c9n8sigm7sn23is4g221zjrn5pjn12")))

(define-public crate-html2maud-0.1.2 (c (n "html2maud") (v "0.1.2") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1a20xc2kgf35s8rxisxqd38cm28m6sacfmccnjjch9b82h56cvy6")))

(define-public crate-html2maud-0.1.3 (c (n "html2maud") (v "0.1.3") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "09xv8x5nxdip1ydck8akmdv6lzaj76p8b6n9yk3mq3kiby2v7b3c")))

(define-public crate-html2maud-0.1.7 (c (n "html2maud") (v "0.1.7") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1g5cxp3cxsi9ijldk4jvw66hv8bkzv4nryl8m68ja6bry951jcnj")))

(define-public crate-html2maud-0.1.8 (c (n "html2maud") (v "0.1.8") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0qab57p0a8cd4ripg5y0c47y4jx2cmsa4lnxw8z9rr4v8my4ysmg")))

(define-public crate-html2maud-0.1.9 (c (n "html2maud") (v "0.1.9") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0xqhj0hcfgmh93prc4cxki39vd35s29php2cdii2wa1xh8qq6i63")))

(define-public crate-html2maud-0.1.10 (c (n "html2maud") (v "0.1.10") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "11qd5dmy17rwnpdxdzi912j6yzss0s1hsx664fn6hknv57qpzwk6")))

(define-public crate-html2maud-0.1.11 (c (n "html2maud") (v "0.1.11") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1w1xfv7bb0hcsvdg511insfbawa92i7s6dv7b9wk54r4f8r4g6c3")))

