(define-module (crates-io ht ml htmldom_read) #:use-module (crates-io))

(define-public crate-htmldom_read-0.1.0 (c (n "htmldom_read") (v "0.1.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1gdlrdxsjvn5jr1cq866rzliyks7i1va2vcdz9kipkvjqfzkk43l")))

(define-public crate-htmldom_read-0.2.0 (c (n "htmldom_read") (v "0.2.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "04chks8bb8d2jv97rz2k3lpz87m8cznczhjx3ldjb5mq7q6djhhg")))

(define-public crate-htmldom_read-0.3.0 (c (n "htmldom_read") (v "0.3.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1w6pxlwcy5kb85ss4xs6zn6bvgch5ism8fdxngi7r80g9b02ycsn")))

(define-public crate-htmldom_read-0.3.1 (c (n "htmldom_read") (v "0.3.1") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "09hhb36lmdn4x0iq43fq5i5nqcn3i78031igjjl3k02clplkn0ll")))

(define-public crate-htmldom_read-0.3.2 (c (n "htmldom_read") (v "0.3.2") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "0gd3bq0vzpxrb4cchg88j6x04d3laxrcn1ab3vprrr6jl00jc5hm")))

(define-public crate-htmldom_read-0.3.3 (c (n "htmldom_read") (v "0.3.3") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1gyvlcr5dx4xmq6wr57glkbmix840mbbnpk9p0dcqw0law0l7h9a")))

(define-public crate-htmldom_read-0.4.0 (c (n "htmldom_read") (v "0.4.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "0n0rnxqswl83hqkzpnz2sqirxl79xd1gbvxvjdhjfs5ia37sq5q4")))

(define-public crate-htmldom_read-0.5.0 (c (n "htmldom_read") (v "0.5.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1d4sgmcfka7042p6ydz1c1abpvvaxgyh9k6m0rd51gzwqnbc7phn")))

