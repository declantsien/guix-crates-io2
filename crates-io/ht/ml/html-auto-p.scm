(define-module (crates-io ht ml html-auto-p) #:use-module (crates-io))

(define-public crate-html-auto-p-0.1.0 (c (n "html-auto-p") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1bz14cvrhmnjg74dxip3fwkx9kplm8754dar1wlpxzdqav3257hk")))

(define-public crate-html-auto-p-0.1.1 (c (n "html-auto-p") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1dzx3mvs38ssingd4ghc57ac2dip60walq4n4zjqgjs2q0qyyggp")))

(define-public crate-html-auto-p-0.1.2 (c (n "html-auto-p") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0mfmwjl4klmyf3bzjzwdf9bg8mphzmk0yah2lv4209lp6pj9hdh4")))

(define-public crate-html-auto-p-0.1.3 (c (n "html-auto-p") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1nrzlvx2kj8213i2dwfsjvm0bhcqw7lwvah7vsxa8v3ym80dmnak")))

(define-public crate-html-auto-p-0.1.4 (c (n "html-auto-p") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1p8hzk86c95r39vpskxmsl252xl4lm26zp51gbv76i4hf3i2c81h")))

(define-public crate-html-auto-p-0.2.0 (c (n "html-auto-p") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "03ii9p07ncsgk4kzn22iiplkl3y6arvywvxwjvqdcaav9yziwycl")))

(define-public crate-html-auto-p-0.2.1 (c (n "html-auto-p") (v "0.2.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1cqjxkyd6ni4q38jxj1wpy0l2dps3fk4rfrmp7l48r43fngvgk0x")))

(define-public crate-html-auto-p-0.2.2 (c (n "html-auto-p") (v "0.2.2") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0j66g3i5ivyvcmlyyx6kmaljnqa1syhwgxr66cw3ykj91z7zz75i")))

(define-public crate-html-auto-p-0.2.3 (c (n "html-auto-p") (v "0.2.3") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0ibjpdingzx42yjnl1pgbakqnnyf5r49lnmq830la1xbpxz9fn4f")))

(define-public crate-html-auto-p-0.2.4 (c (n "html-auto-p") (v "0.2.4") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "185nbksivzi6c2rk45vpz4546ymyr36w7cyxggs1kwk08zvqkh5s") (r "1.61")))

