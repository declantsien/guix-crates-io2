(define-module (crates-io ht ml html5ever_ext) #:use-module (crates-io))

(define-public crate-html5ever_ext-0.21.0 (c (n "html5ever_ext") (v "0.21.0") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "05hbfslm8mz3n4iwgnx9bx9j26yhpmn45yzxqjzijnv84is55z70")))

(define-public crate-html5ever_ext-0.21.1 (c (n "html5ever_ext") (v "0.21.1") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "153pkg5db82pxphkqggw710ajavyfamrcip6lgzdclg5f5zv1zhb")))

(define-public crate-html5ever_ext-0.21.2 (c (n "html5ever_ext") (v "0.21.2") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1qn70pmsqmbzwi6mb2s5pgcmkv04wpxk85qfrhyyh7vl55vklhgd")))

(define-public crate-html5ever_ext-0.21.3 (c (n "html5ever_ext") (v "0.21.3") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "10xq0jzfblic4qdmlhfl09jpfcwb4kkykg8jwpphz70zfgvwggg7")))

(define-public crate-html5ever_ext-0.21.4 (c (n "html5ever_ext") (v "0.21.4") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0cdjsmgvfig40fxs50nimx5qb12ix0hww9ir0ywgadwlv7lx7qqb")))

(define-public crate-html5ever_ext-0.21.5 (c (n "html5ever_ext") (v "0.21.5") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0la447bg6m0yd2j1hirawd22rq7qrl94drnzx3z8ygsh8n3wvh3c")))

(define-public crate-html5ever_ext-0.21.6 (c (n "html5ever_ext") (v "0.21.6") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "18ji1vsahx6ph8izr2n295sdgvv1mw9hpn984hz8sicd3sd4491j")))

(define-public crate-html5ever_ext-0.21.7 (c (n "html5ever_ext") (v "0.21.7") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1dzd1k74pz26v3myy94frsgg5i0c3xsvc49nqcwv0swvcajzxbpp")))

(define-public crate-html5ever_ext-0.21.8 (c (n "html5ever_ext") (v "0.21.8") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1wazm71jikwvczb8n5gdrncy2j4i8kim21b4i7z5nc8yh60nx43m")))

(define-public crate-html5ever_ext-0.21.9 (c (n "html5ever_ext") (v "0.21.9") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "003sfrqfwmalflx4ha7sa2mvcamiwnk40yyria6fp53izcz7a9pd")))

(define-public crate-html5ever_ext-0.21.10 (c (n "html5ever_ext") (v "0.21.10") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1fd8lg9iz1mfcsh40yy261k1b2knjjv3lvxhysn91qr1809b9jrh")))

(define-public crate-html5ever_ext-0.21.11 (c (n "html5ever_ext") (v "0.21.11") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1im1f2cksainn2q8jwxbi88dvqabil41dhywlnklv4dypqqb7dhd")))

(define-public crate-html5ever_ext-0.21.12 (c (n "html5ever_ext") (v "0.21.12") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0j31bvap2fhmw3svgnmlk6r86bw9ipr0ra1lrhsg5k0k54hfcqaf")))

(define-public crate-html5ever_ext-0.21.13 (c (n "html5ever_ext") (v "0.21.13") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1w1wrb1i0af9bdiff4z6nsvl86jiibqzc43yhphwcjyi6b7p4px7")))

(define-public crate-html5ever_ext-0.21.14 (c (n "html5ever_ext") (v "0.21.14") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0cd4hyhaw3c9kyflys7drafw4fvgf97k6s0r4bnmy03n7anlw03c")))

(define-public crate-html5ever_ext-0.21.15 (c (n "html5ever_ext") (v "0.21.15") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0iixfbbnbws15wkr9vgdp9z72c2lixqw1wqc5ka78y84v2mj93ix")))

(define-public crate-html5ever_ext-0.21.16 (c (n "html5ever_ext") (v "0.21.16") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1ryj7nkwsr0242c9zxw12hwdpgm7rm91mmd19gp0p33jq5a65zfv")))

(define-public crate-html5ever_ext-0.21.17 (c (n "html5ever_ext") (v "0.21.17") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "1c1dlsv9ffpa66gvf0y17n78d5ay061r4rv1snz7370xqk4mai3w")))

(define-public crate-html5ever_ext-0.21.18 (c (n "html5ever_ext") (v "0.21.18") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0z9i2r2kq0bhfj8ayfna5r0a1lkvylpzvlcb2j4i5b8syq2l55l2")))

(define-public crate-html5ever_ext-0.21.19 (c (n "html5ever_ext") (v "0.21.19") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "050mqi82mszag4mjcbw926gi2ji6b373svhz2fzzq4yybjpx1ddl")))

(define-public crate-html5ever_ext-0.21.20 (c (n "html5ever_ext") (v "0.21.20") (d (list (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "either") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0wnky23yvvb94synavxp9z847sxvx9xrarc2ix3ry6s0l6yz675w")))

