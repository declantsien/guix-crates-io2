(define-module (crates-io ht ml html-builder) #:use-module (crates-io))

(define-public crate-html-builder-0.1.0 (c (n "html-builder") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1dqvh2glk3d1x65d36y90hqx1v7qygw3x1hyx7j5si6rzl04az50")))

(define-public crate-html-builder-0.2.0 (c (n "html-builder") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "0pb4sqd3lgn1vgzfjvkp9hyy0707crqrq4nflmpc2975ngr9qq2j")))

(define-public crate-html-builder-0.3.0 (c (n "html-builder") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1d5wn58g6r4c8k6jjah26vm9byqlh5j7n5wji38nndhqc06azvpr")))

(define-public crate-html-builder-0.4.0 (c (n "html-builder") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0yk98lw3xmp5wqm377rlgv94wrwssq1d598jnn25i6xr4in91b42")))

(define-public crate-html-builder-0.5.0 (c (n "html-builder") (v "0.5.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0c0hs4rjzbn3ifzbz1bd3005liyzrfhlh3yz8b8d4v679zywknfd")))

(define-public crate-html-builder-0.5.1 (c (n "html-builder") (v "0.5.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "13l9bbngy3jlma6yaqcq7zn3b55q1dl8pfdj722y4xyz99gzllwi")))

