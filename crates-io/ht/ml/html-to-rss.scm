(define-module (crates-io ht ml html-to-rss) #:use-module (crates-io))

(define-public crate-html-to-rss-0.1.0 (c (n "html-to-rss") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rss") (r "^2.0.1") (f (quote ("validation"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (f (quote ("deterministic"))) (d #t) (k 0)))) (h "1agq2n9wmbp5nh6nfnzzs1x4ygi0vmzp67305y5n7xs88xshp2q4")))

(define-public crate-html-to-rss-0.2.0 (c (n "html-to-rss") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rss") (r "^2.0.1") (f (quote ("validation"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (f (quote ("deterministic"))) (d #t) (k 0)))) (h "15b6an7b1jy4i1v9j2l5qqmgx8wdlq9kncfsmv38bw1p6q1myk5z")))

(define-public crate-html-to-rss-0.3.0 (c (n "html-to-rss") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rss") (r "^2.0.1") (f (quote ("validation"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (f (quote ("deterministic"))) (d #t) (k 0)))) (h "1z9c7crvfizr80ixhkq9f0grrpwfbxi3skvdhlpyn736rfx064rj")))

(define-public crate-html-to-rss-0.3.1 (c (n "html-to-rss") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "rss") (r "^2.0.1") (f (quote ("validation"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (f (quote ("deterministic"))) (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)))) (h "066by3kkslbmiilj63dknf7aczwj409k1ds2yggq848zpl1b590r")))

