(define-module (crates-io ht ml html_hasher) #:use-module (crates-io))

(define-public crate-html_hasher-0.1.0 (c (n "html_hasher") (v "0.1.0") (d (list (d (n "lol_html") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0i08l6m1vdsh2gl00dl4wyr8km675bqhhir1vzwxy0z8ix4lsgnz")))

(define-public crate-html_hasher-0.1.1 (c (n "html_hasher") (v "0.1.1") (d (list (d (n "lol_html") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1d42vp1crxb7mfhc7y3xw85fxcmyhz55iccagz4n8gmrnb59hwkd")))

