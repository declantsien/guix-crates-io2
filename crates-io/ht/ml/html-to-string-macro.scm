(define-module (crates-io ht ml html-to-string-macro) #:use-module (crates-io))

(define-public crate-html-to-string-macro-0.1.0 (c (n "html-to-string-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8") (d #t) (k 0)))) (h "1fg2q8pz60ps1r6ns1ym1blw206zz2jxzzam5qc05b67y45mwf6m")))

(define-public crate-html-to-string-macro-0.1.1 (c (n "html-to-string-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8") (d #t) (k 0)))) (h "0qcqd6kjk90rzhb9y9aa21mcmbygqc2manaph2ly1pwx0x0chqxq")))

(define-public crate-html-to-string-macro-0.1.2 (c (n "html-to-string-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8") (d #t) (k 0)))) (h "077h1vpq4dww8s4q06fgf04i6yr69852hj5rcbjfgvc567jx0v3j")))

(define-public crate-html-to-string-macro-0.2.0 (c (n "html-to-string-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "13da0fj0mzigkmfjdvgbip3bxdyrlp60i2w73zcp208j0p81b2p3")))

(define-public crate-html-to-string-macro-0.2.1 (c (n "html-to-string-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "1g0kwq61i7cvakns1jmg0lmbqn6wb3ivlsym24dfn4damc7gqy7z")))

(define-public crate-html-to-string-macro-0.2.2 (c (n "html-to-string-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "0bhig9cpiazsv9wp5swfbam7qxc57n67n1jnp5idywvs3gy4q1wy")))

(define-public crate-html-to-string-macro-0.2.3 (c (n "html-to-string-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "1h7m2j6gry3pnf1ipfnmyjf01c64a75pg6ci6n14jjqvxdppdb8a")))

(define-public crate-html-to-string-macro-0.2.4 (c (n "html-to-string-macro") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.1") (d #t) (k 0)))) (h "1lk9dfx49592iq6gm1pwrlflwm7bxwikq1szkkp02yr6l1307mkk")))

(define-public crate-html-to-string-macro-0.2.5 (c (n "html-to-string-macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.9.0") (d #t) (k 0)))) (h "1yq9aqqg7jbwlzinf8p60v1pvwxpyh213lza9d9ac1rihgw7fsaw")))

