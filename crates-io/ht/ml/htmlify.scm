(define-module (crates-io ht ml htmlify) #:use-module (crates-io))

(define-public crate-htmlify-1.0.0 (c (n "htmlify") (v "1.0.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "0q2qp4kh0i8g87zz3irhs4vy90nnpjbkwj1ilxhjcxbb0b6xaiaf")))

(define-public crate-htmlify-1.0.1 (c (n "htmlify") (v "1.0.1") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "1valx12qsmz0ic5kp38mwdqqkhb1zizapy9mplkg2v3yhdpzxsbh")))

(define-public crate-htmlify-1.0.2 (c (n "htmlify") (v "1.0.2") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "1xqz5v5ahrdw0a7jk2x8jx15fwwxp2f3h5x3jc6dr3aggwf8gas4")))

(define-public crate-htmlify-1.0.3 (c (n "htmlify") (v "1.0.3") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)))) (h "1cfj5aflgr1fqs8dih574x9lzv9pqmlsn0x5hxrqz6s6y7izaacv")))

(define-public crate-htmlify-1.1.0 (c (n "htmlify") (v "1.1.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1hc534491kminq9ahr7w54vggwacra7j5gxz41azbj6pjcy86rbb")))

(define-public crate-htmlify-1.1.1 (c (n "htmlify") (v "1.1.1") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0m9krkwls474svs5nmw86bq81q5msqh9axcncdpm52zxn643lwpd")))

(define-public crate-htmlify-1.2.0 (c (n "htmlify") (v "1.2.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1cy25fprn2jmghgxwyakk6x3md1k0bpmh1rxcfkpalzc71gn1mf6")))

(define-public crate-htmlify-1.2.1 (c (n "htmlify") (v "1.2.1") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "14zwsrp456g1p8a9z3ghl9hkrblpfnf8bvqsa1bc5icxh7ay98q8")))

(define-public crate-htmlify-1.2.2 (c (n "htmlify") (v "1.2.2") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1jx752j6knpdpi7q012sg5vkvr54gnnrk8ai519wvchmn619hciz")))

(define-public crate-htmlify-1.3.0 (c (n "htmlify") (v "1.3.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "HtmlElement"))) (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1alwv3lg8pai959008qbsyid61dl24f4xncdlwlpk799m6nd7pwg")))

