(define-module (crates-io ht ml html-sys) #:use-module (crates-io))

(define-public crate-html-sys-0.1.0 (c (n "html-sys") (v "0.1.0") (h "05v9lx7cdy5zb52bjgsqlaf3f7wgf2g9vpca59vglfx1ak1ln5am")))

(define-public crate-html-sys-0.2.0 (c (n "html-sys") (v "0.2.0") (h "10j3yza16w3g646dl087l0d0a4dm5fqpzlywb3ysgsi00ndi400h")))

(define-public crate-html-sys-0.2.1 (c (n "html-sys") (v "0.2.1") (h "00wx7iyimxaz3rb9dy5pi3h7ahjnf79raspg4dw5vx8lh5jylalw")))

(define-public crate-html-sys-0.2.2 (c (n "html-sys") (v "0.2.2") (h "0vzv2i5ls8b6xd4x69rysypnp4lfv85mpxn7bkz2iz6lrfx3ywxn")))

(define-public crate-html-sys-0.3.0 (c (n "html-sys") (v "0.3.0") (h "1cl1np1cshs18sjmbwab28ky9dxsp86y5zkn3lifvr6j235640gj")))

(define-public crate-html-sys-0.3.1 (c (n "html-sys") (v "0.3.1") (h "0vdm0y9vx7lxnkwq13dhbi63ks2s0siv3anxx1zgb73g3m5vzsh5")))

(define-public crate-html-sys-0.4.0 (c (n "html-sys") (v "0.4.0") (h "006jmg5vddfq6i8hiap4qbhjbd2accbiji1m8dlsgp0r93z43m5p")))

(define-public crate-html-sys-0.4.1 (c (n "html-sys") (v "0.4.1") (h "1av83jxyhawyw3r7fshv4gvbf43f3zwxmzarcp2963jq3dg4kwxy")))

(define-public crate-html-sys-0.4.2 (c (n "html-sys") (v "0.4.2") (h "0mphzgbncnzzjar7k8dpcgw9xh0ja8vrpymqi9i0gy42swvli3wa")))

(define-public crate-html-sys-0.4.3 (c (n "html-sys") (v "0.4.3") (h "0wr5vlrifijc5id5bs8kjhzhx0fiw9gprdvdp38psrd5cxbabv0k")))

