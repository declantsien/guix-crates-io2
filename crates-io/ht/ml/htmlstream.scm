(define-module (crates-io ht ml htmlstream) #:use-module (crates-io))

(define-public crate-htmlstream-0.1.0 (c (n "htmlstream") (v "0.1.0") (h "0lbla52phc3m1rfvsamk07vqhs0m5p5qkh5hryhnsydxqdwlqqnj")))

(define-public crate-htmlstream-0.1.1 (c (n "htmlstream") (v "0.1.1") (h "1xqfc0bl01pv3lmlycl70ddmgkdywvg7f6hirxr9mw0iwx73qxl7")))

(define-public crate-htmlstream-0.1.2 (c (n "htmlstream") (v "0.1.2") (h "0glw4k0v8h8pvzkv9cl1b2kc8zn4s1pr0pjcfghrv8i13km7m1vm")))

(define-public crate-htmlstream-0.1.3 (c (n "htmlstream") (v "0.1.3") (h "0k9j887p930chz2nyp4636zyr2inpzyyhfca2as56mshvz10zj1h")))

