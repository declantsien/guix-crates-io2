(define-module (crates-io ht ml html_to_epub) #:use-module (crates-io))

(define-public crate-html_to_epub-0.1.1 (c (n "html_to_epub") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "epub-builder") (r "^0.4.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "visdom") (r "^0.4.11") (d #t) (k 0)))) (h "0nadi8h34aqzvhr1igzg9zi84p8dxp4lsv3r54fpl3bf4r2sg9vw")))

(define-public crate-html_to_epub-0.1.3 (c (n "html_to_epub") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "epub-builder") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "visdom") (r "^0.5.10") (f (quote ("full"))) (d #t) (k 0)))) (h "00finvp9i2sady3937bhbrrnyzjw8975607akmikwc7zx6hlq22d")))

