(define-module (crates-io ht ml html-string) #:use-module (crates-io))

(define-public crate-html-string-0.1.0 (c (n "html-string") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yxlzm8kjs867b3h178gndrqn7ibn5ikvvy290fyj8786s30kbkm")))

(define-public crate-html-string-0.2.0 (c (n "html-string") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0074dmc0r6fm7cswld55jrd82lkh2qkcgybvzbx938vxxlhl06xw")))

(define-public crate-html-string-0.3.0 (c (n "html-string") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)))) (h "1ka43bm62yyyh0ahirj4jzb67n5k6gbfx5vay5g9fald8f7axbfy")))

