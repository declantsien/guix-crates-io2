(define-module (crates-io ht ml html-editor-cli) #:use-module (crates-io))

(define-public crate-html-editor-cli-0.1.0 (c (n "html-editor-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "html_editor") (r "^0.6.1") (d #t) (k 0)))) (h "1prphfqz024vdi63azs1p0cc5mvzrsq5bxn46k6d79c5q9nkziji")))

(define-public crate-html-editor-cli-0.1.1 (c (n "html-editor-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "html_editor") (r "^0.6.1") (d #t) (k 0)))) (h "1aicfhb9zsgnasb09h7ayf60k0hz5hb5z8w4afj8cyb4zwj081kc")))

