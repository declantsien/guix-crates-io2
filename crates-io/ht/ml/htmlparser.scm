(define-module (crates-io ht ml htmlparser) #:use-module (crates-io))

(define-public crate-htmlparser-0.1.0 (c (n "htmlparser") (v "0.1.0") (h "0q1h8mg5h12hs6lnn39if0kd9lhkkva8y73b9n3mjcms6z2p23vn") (f (quote (("std") ("default" "std"))))))

(define-public crate-htmlparser-0.1.1 (c (n "htmlparser") (v "0.1.1") (h "1yxvg7ks291fk896kpqarmg907rwc0ir8f0fmikdkd0qf5vl2g92") (f (quote (("std") ("default" "std"))))))

