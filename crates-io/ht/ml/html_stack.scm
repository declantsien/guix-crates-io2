(define-module (crates-io ht ml html_stack) #:use-module (crates-io))

(define-public crate-html_stack-1.0.0 (c (n "html_stack") (v "1.0.0") (h "1yf85d1bdxzkdr4821bh6kjlx0y4b08f5f597yvld70x8b6dvx8w") (r "1.66")))

(define-public crate-html_stack-1.0.1 (c (n "html_stack") (v "1.0.1") (h "0fnh8zn2fs1k9hwhw9nqr6nj31pfl80hx7fxdj40xmf4dxbaif45") (r "1.66")))

(define-public crate-html_stack-1.0.2 (c (n "html_stack") (v "1.0.2") (h "0rg6b4wbpfknjlfg37sb2m01z0mkyf9sjz96s45pzbdkm4frcal6") (r "1.66")))

