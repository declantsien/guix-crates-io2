(define-module (crates-io ht ml html_query_parser) #:use-module (crates-io))

(define-public crate-html_query_parser-0.1.0 (c (n "html_query_parser") (v "0.1.0") (h "19qq7ml4nggflgq4hv8v15s4z2v7wrcq1jy7zyp5ccfj2h1fdfg0")))

(define-public crate-html_query_parser-0.2.0 (c (n "html_query_parser") (v "0.2.0") (h "1nc96rra2ga7jrga58b7v98bzcrc1fbi1mija4j776c50wglia0z")))

(define-public crate-html_query_parser-0.2.1 (c (n "html_query_parser") (v "0.2.1") (h "1qfcpzxy5bkhjy2kmwsqanp299bgflqiqzmpz42qhyik0929s340")))

(define-public crate-html_query_parser-0.3.0 (c (n "html_query_parser") (v "0.3.0") (h "16kng4li29z6ia8ryxshl7x3haxb26nzgf3f74hfr3zrj9bkp4f5")))

(define-public crate-html_query_parser-0.3.1 (c (n "html_query_parser") (v "0.3.1") (h "01bsfyzmj9gimzvhyjcw7pmaygfwczcgyixsbfhvp31xnb74agjw")))

