(define-module (crates-io ht ml html-purifier) #:use-module (crates-io))

(define-public crate-html-purifier-0.1.0 (c (n "html-purifier") (v "0.1.0") (d (list (d (n "lol_html") (r "^0.1.0") (d #t) (k 0)))) (h "1ff0xmwi2qj76y6rvy4bzzzjszd6xa0077nj8m4c2r29yk57hj3v")))

(define-public crate-html-purifier-0.1.1 (c (n "html-purifier") (v "0.1.1") (d (list (d (n "lol_html") (r "^0.1.0") (d #t) (k 0)))) (h "0qhir5r073wzkqlb9zbwhb93l44wab4k7dixrw6ck16n725mwggs")))

(define-public crate-html-purifier-0.2.0 (c (n "html-purifier") (v "0.2.0") (d (list (d (n "lol_html") (r "^0.1.0") (d #t) (k 0)))) (h "10x791ih8h94qshjrp8hj3bbj28s61dkhif3ckm8dvjfgl5rh4x8")))

(define-public crate-html-purifier-0.3.0 (c (n "html-purifier") (v "0.3.0") (d (list (d (n "lol_html") (r "^1.2.0") (d #t) (k 0)))) (h "1s2npiff70c8wvn3jwb1rcrpap85g1gwaqnw1nrqczlsd9afr919")))

