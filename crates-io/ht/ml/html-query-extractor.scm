(define-module (crates-io ht ml html-query-extractor) #:use-module (crates-io))

(define-public crate-html-query-extractor-0.1.1 (c (n "html-query-extractor") (v "0.1.1") (d (list (d (n "html-query-ast") (r "^0.1.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07qkn5w0fpgfn0r0szdhwypk3ah1jdgwzxhymk6dg9y825539wkc")))

(define-public crate-html-query-extractor-0.2.0 (c (n "html-query-extractor") (v "0.2.0") (d (list (d (n "html-query-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1aqmq6cdl1pcl43p4lvjxw8fd47hffy1z3nmji612cjm19ifii2c")))

(define-public crate-html-query-extractor-0.2.1 (c (n "html-query-extractor") (v "0.2.1") (d (list (d (n "html-query-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1qmk34sjj4iy3734xyxr1gb4ssjgx429z4r2hw0zg609ija68ya7")))

(define-public crate-html-query-extractor-0.2.2 (c (n "html-query-extractor") (v "0.2.2") (d (list (d (n "html-query-ast") (r "^0.2.2") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1i678sx1pqy55b0sb2vhr40yc7x7lr1c62qnj1pvh6x1br1hwgai")))

