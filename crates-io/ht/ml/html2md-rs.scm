(define-module (crates-io ht ml html2md-rs) #:use-module (crates-io))

(define-public crate-html2md-rs-0.1.0 (c (n "html2md-rs") (v "0.1.0") (h "0pk4yyzvmiwizwk3ri51zwjamy70jd55v2s9zf1qahmyi36ihlrn")))

(define-public crate-html2md-rs-0.1.1 (c (n "html2md-rs") (v "0.1.1") (h "09rw8gw26w18xnp7kqwpns3wfvyxss07c0zld125kvrdmbv5y403")))

(define-public crate-html2md-rs-0.1.2 (c (n "html2md-rs") (v "0.1.2") (h "1g8df0k7bhz9ajcp097h9znkwddfjhhazdqi6jlndna3f31rbnag")))

(define-public crate-html2md-rs-0.1.3 (c (n "html2md-rs") (v "0.1.3") (h "16caagznhbgr48aqf53g9c6kfa4hz14qa7q36gvh362qg6pp365l")))

(define-public crate-html2md-rs-0.1.4 (c (n "html2md-rs") (v "0.1.4") (h "0jsxs7k7ix957wg2jkzpf799h09f8pi5gsrn8jba8lbs2qhk9rda")))

(define-public crate-html2md-rs-0.1.5 (c (n "html2md-rs") (v "0.1.5") (h "10kdajfngbkc64qhvs184hldb42ja4xbfz8i38zbg6r0jhph7if3")))

(define-public crate-html2md-rs-0.1.6 (c (n "html2md-rs") (v "0.1.6") (h "173a6vsj8sp52s16lisyvs9ccrf6qds7z78041g03ay38d317bpn")))

(define-public crate-html2md-rs-0.1.8 (c (n "html2md-rs") (v "0.1.8") (h "0xscrbykrfw76wnfaxxkb37776phi19sia12xyml7qs9wha187b6")))

(define-public crate-html2md-rs-0.2.0 (c (n "html2md-rs") (v "0.2.0") (h "1ycz0rzi9b63fvf2mw4f3r34hvkpavbqn9f8xnjnv0dy421afsrd")))

(define-public crate-html2md-rs-0.3.0 (c (n "html2md-rs") (v "0.3.0") (h "1h8h8g1rpi7295r117zib614aqld7mqlvlldgim2al23zdn80grs")))

(define-public crate-html2md-rs-0.4.0 (c (n "html2md-rs") (v "0.4.0") (h "0vrlgld15k0ppc7z4ib3kyvch5yb6j4dj2nqqlfld1sbq842896p")))

(define-public crate-html2md-rs-0.5.0 (c (n "html2md-rs") (v "0.5.0") (h "10dasiljqmgds8baxc3rppi9b34vs8ijz35s8fchpi40k8ksms8i")))

(define-public crate-html2md-rs-0.5.1 (c (n "html2md-rs") (v "0.5.1") (h "0s6y84d3xvp8prx6asml05zrijp6gn7prrv24bpsbjhdb5b31sij")))

(define-public crate-html2md-rs-0.6.0 (c (n "html2md-rs") (v "0.6.0") (h "11hcfkq5sskq6kk8whn2qlgz8nr4jlh939f0jygyvpq6f2dp7shc")))

(define-public crate-html2md-rs-0.7.0 (c (n "html2md-rs") (v "0.7.0") (h "0avswnypkvi0x60l46ipq813290q4sqzw2ixv2jbpa9hjc9fcixj")))

(define-public crate-html2md-rs-0.7.1 (c (n "html2md-rs") (v "0.7.1") (h "0xvis4siw5fis9fll3xxn3w89ns25zapvjzmh31hsnmg0msi6jz8")))

(define-public crate-html2md-rs-0.7.2 (c (n "html2md-rs") (v "0.7.2") (h "0pq698zjlnp8b891p8xp584r41mfrnw492cyfdnjswm9hfyfncxy")))

(define-public crate-html2md-rs-0.8.0 (c (n "html2md-rs") (v "0.8.0") (h "10gsif57n2nqr3i90j6v99l3pl8pr25lfh3v93dixpnbmfg44vip")))

(define-public crate-html2md-rs-0.8.1 (c (n "html2md-rs") (v "0.8.1") (h "0km9f55i7xiqz471kfbvk4b4lmi6w8rrys4pmb24v3l0idjlhkvq")))

(define-public crate-html2md-rs-0.8.2 (c (n "html2md-rs") (v "0.8.2") (h "18cl9w5v3sgknqsl6n0z7zwnrm6pkhxz0hpah5ydmxmnxxv9khrj")))

