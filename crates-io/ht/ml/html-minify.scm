(define-module (crates-io ht ml html-minify) #:use-module (crates-io))

(define-public crate-html-minify-0.1.0 (c (n "html-minify") (v "0.1.0") (d (list (d (n "minify-html") (r "^0.8.0") (f (quote ("js-esbuild"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)))) (h "1fds1sps0q41vgaqvlzmvvcap8d7i2xc8j338dh611smyhp80ryy")))

