(define-module (crates-io ht ml html-tags) #:use-module (crates-io))

(define-public crate-html-tags-0.0.1 (c (n "html-tags") (v "0.0.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 1)) (d (n "scraper") (r "^0.16.0") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "1saq301pr9zch621bdpmycwg9chfpnah9np58p79nal91c03i7bk")))

(define-public crate-html-tags-0.0.2 (c (n "html-tags") (v "0.0.2") (h "1dn33hisnfd8pidyd0l8ismwqrzxalrrlr53r4ryym66jipj15sp") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.3 (c (n "html-tags") (v "0.0.3") (h "1sj4gacyz41c2ddpnph3whjz3mfq4pi7y3ir7798dmy142k3ibnj") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.4 (c (n "html-tags") (v "0.0.4") (h "1ir67skhwdz4m5s74zqgngjrxmi916crdhsdfsf5ph5ibqh9f7az") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.5 (c (n "html-tags") (v "0.0.5") (h "1qfs1ksvca6f02asqv6fygpi4z3axylxpnnv10l2i4wjhfwzyag7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.6 (c (n "html-tags") (v "0.0.6") (h "1g3d5lgdmjzc307kx6s4w4b78cfhvgpcmnzs40bkbpjsw2aiphar") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.7 (c (n "html-tags") (v "0.0.7") (h "0jwganzdi1glmmzg480wrk81h3dblknisb3nax2yh67qak1gi8s8") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.8 (c (n "html-tags") (v "0.0.8") (h "0dn3ambgfwm9vm89pl15kk2sv6g4bdqjymfj74i8dkwdpknr5r8z") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.9 (c (n "html-tags") (v "0.0.9") (h "0fny67qydhcgmjv4p3vixn70mchbnbz5h951friz50jc0jydrgd5") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.10 (c (n "html-tags") (v "0.0.10") (h "1nf0kim7grr54mmz519q8ixnik5sjkvwshahd1m9gy45v1n5pbkh") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.11 (c (n "html-tags") (v "0.0.11") (h "131zqynrmg4j4kffdsz99sa0gbwg4z8af182fmmiv4cal832kq2a") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-html-tags-0.0.12 (c (n "html-tags") (v "0.0.12") (d (list (d (n "tendril") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0nzr8n0is66g48f547d71h22v9bkyw08z8sr2kpbig77dfvgwaky") (f (quote (("default" "alloc" "tendril") ("alloc")))) (s 2) (e (quote (("tendril" "dep:tendril" "alloc"))))))

