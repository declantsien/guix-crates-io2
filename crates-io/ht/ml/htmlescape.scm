(define-module (crates-io ht ml htmlescape) #:use-module (crates-io))

(define-public crate-htmlescape-0.3.1 (c (n "htmlescape") (v "0.3.1") (d (list (d (n "num") (r "^0.1.26") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0qria8paf19qy5sgzzk3iiii9fp2j7spbhqf0zjxwrg7v9c500p9")))

