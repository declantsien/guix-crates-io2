(define-module (crates-io ht ml html-to-react) #:use-module (crates-io))

(define-public crate-html-to-react-0.1.1 (c (n "html-to-react") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rn664jzbgh2xds8gxfmxgnf52bkf5wp9ik5sw0zs564bc375hdi")))

(define-public crate-html-to-react-0.1.2 (c (n "html-to-react") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j8k2l7vf42pxkwj8kr62yakkvbzk2zf8hc94sqskbalk1mxpaf9")))

(define-public crate-html-to-react-0.1.3 (c (n "html-to-react") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0s5w4275gsl3kqi6mw2ygg9fzi9k07p6hrcryqsn6qr7mc4swpql")))

(define-public crate-html-to-react-0.1.4 (c (n "html-to-react") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qb5hlqlqhmygp9p2xr3d8ma3j9nimivh77qdyr8vg4xbvgy1han")))

(define-public crate-html-to-react-0.2.0 (c (n "html-to-react") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dka31461a8xnz7ldr07gzb0ppvcngyzj8mlz39nfn432i2dwf02")))

(define-public crate-html-to-react-0.2.1 (c (n "html-to-react") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "00xfipb6wpirvs82b72p5zzpvmj9351bfai16zs42i9alkv0rfvq")))

(define-public crate-html-to-react-0.3.0 (c (n "html-to-react") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ixjh1swc6ihx08gf7d99cv7qxmn4qqzklqjz7vlkmiccavk07ff")))

(define-public crate-html-to-react-0.3.1 (c (n "html-to-react") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "075pwv5y26h457fkzb8scham0g490cingipsmsypi9nyq5dj2chz")))

(define-public crate-html-to-react-0.3.2 (c (n "html-to-react") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10c837ij15anblf9l7mc6lzbcldswfwksfzbgchd5jkpdzfjblyr")))

(define-public crate-html-to-react-0.4.0 (c (n "html-to-react") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g0s2r2c6zavfypdxcsmpm2mxggmi0sxsa6mxnksw4400sbccp3n")))

(define-public crate-html-to-react-0.4.1 (c (n "html-to-react") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "152dwn7jkrz1m1mjp7b9iqlssq4i97qdw5yj534d5ykia71ckdpi")))

(define-public crate-html-to-react-0.4.2 (c (n "html-to-react") (v "0.4.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fs82n4bn21kzllffxc3kca0isga8v6v4mn183228i162vgd9jn9")))

(define-public crate-html-to-react-0.5.0 (c (n "html-to-react") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n965w5p24jp4n69x8fwvbrkimcawbkaniy8rk39c0803ck848cn")))

(define-public crate-html-to-react-0.5.1 (c (n "html-to-react") (v "0.5.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0bnzmrk4r0m6l36shncc8cdj19wa8whii7gx4xw44vvzvcjfvnfh")))

(define-public crate-html-to-react-0.5.2 (c (n "html-to-react") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1d252iin39j0c91p3fv2wp1a0sp9sij351881xz8mifslmwflawn")))

