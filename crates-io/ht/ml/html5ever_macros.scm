(define-module (crates-io ht ml html5ever_macros) #:use-module (crates-io))

(define-public crate-html5ever_macros-0.1.0 (c (n "html5ever_macros") (v "0.1.0") (d (list (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "0pg828k3rywnjkxl5gpn33hka7iiya9412j1l0g5q58acnc1fd1r")))

(define-public crate-html5ever_macros-0.2.0 (c (n "html5ever_macros") (v "0.2.0") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "0qimn66ccsljh9ngc3knml1wjgjgl6sx1d2gq48cx18hi259nf19")))

(define-public crate-html5ever_macros-0.2.1 (c (n "html5ever_macros") (v "0.2.1") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "0hlsr6lil9f11fniyacrmagrfk5mngqbdd4qz619zb9ksfz3xpr6")))

(define-public crate-html5ever_macros-0.2.2 (c (n "html5ever_macros") (v "0.2.2") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "1dx5ly7lwl6y787vlvjpp5s91yc0yyjd7ax32hglzcckjky08rc8")))

(define-public crate-html5ever_macros-0.2.3 (c (n "html5ever_macros") (v "0.2.3") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "1wmk119vfjxw5pp7rh1n5bisnapn31kn4rn6pfcgj583wkjc4iy3")))

(define-public crate-html5ever_macros-0.2.4 (c (n "html5ever_macros") (v "0.2.4") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "1g8pay9xwzhzb46cnw8rjj2a41qjzxrdy6dw6ar2f8xddqq8ljn6")))

(define-public crate-html5ever_macros-0.2.5 (c (n "html5ever_macros") (v "0.2.5") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "1v7cz9n62cv9i1755lhbag317iqy248xsqkp1i7pgnqinc3dw0wx")))

(define-public crate-html5ever_macros-0.2.6 (c (n "html5ever_macros") (v "0.2.6") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "0k60662ynh72dg7mjqr5byhlf3y3rp9kz7y0gp4mlcckaamcxv9x")))

(define-public crate-html5ever_macros-0.2.7 (c (n "html5ever_macros") (v "0.2.7") (d (list (d (n "mac") (r "^0") (d #t) (k 0)))) (h "1x7yyyxvajr3mn01s5zj4ji5w09vs4hdyn35l9bknh76wrq6rx2j")))

