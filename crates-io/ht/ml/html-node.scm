(define-module (crates-io ht ml html-node) #:use-module (crates-io))

(define-public crate-html-node-0.1.0 (c (n "html-node") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0klaavnyqic4w3fnbrpj8kqmmnb5r9v5h282bmis1ff5lylhbmhh")))

(define-public crate-html-node-0.1.1 (c (n "html-node") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "1ihammxy62x0zknl6l45fx53c0f81fznb242r05q1ddym7pxgb18")))

(define-public crate-html-node-0.1.2 (c (n "html-node") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0bj61c8yz34xz9f539af38r51chvlqsd58m1bpxphn5ggi2f94zy")))

(define-public crate-html-node-0.1.3 (c (n "html-node") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0n43rd16imwz1xnmsxnzf05vfr14lr4h6nd5d527aw3ysr7j8cjk")))

(define-public crate-html-node-0.1.4 (c (n "html-node") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "047pqbpgqyh0n02pjs4fba52wfg7ks837pgma3s064g7s9vfw8s9")))

(define-public crate-html-node-0.1.5 (c (n "html-node") (v "0.1.5") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0xk26i6cjpdlg5a7jjvs76pa8izki792xsa6b5gy4l8z4s0n7wfa")))

(define-public crate-html-node-0.1.6 (c (n "html-node") (v "0.1.6") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0hf0ncswx8fphx6ccyws6hv7fl5n7fmxa9jdkhp2acpbz0dc8csf")))

(define-public crate-html-node-0.1.8 (c (n "html-node") (v "0.1.8") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "1xyxy67i4mspvbd4hyn5ihyb81jx3a4iw1d06p630swr3073sr0b")))

(define-public crate-html-node-0.1.9 (c (n "html-node") (v "0.1.9") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "1zz94p69n11r40qp18nadk78sdhdjyiw0d5f6arh40s83l7vrmaa")))

(define-public crate-html-node-0.1.10 (c (n "html-node") (v "0.1.10") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)))) (h "0jnnhlcagygj133z6xrlxs8wvsm9xsl5xcpc3g3r6mz1ki5ihwi7")))

(define-public crate-html-node-0.1.12 (c (n "html-node") (v "0.1.12") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09apbyvlh46gfagvgjhrx9120paf07jx7pv7c922k78nw2yhqqm5") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.1.13 (c (n "html-node") (v "0.1.13") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j5xjx68ivyhznq64fmj4vmvhqbh2sy7kpb98k92v464dr0h9pa9") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.1.14 (c (n "html-node") (v "0.1.14") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hf0ayf17xcj52404q558f73kn0i8a2dzwi4wf1y4p1z55cvkywv") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.1.15 (c (n "html-node") (v "0.1.15") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dhzqqhk89n522zxxwkkp792gq5bphy88gbip23daqx9mp17kdkl") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.1.16 (c (n "html-node") (v "0.1.16") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hhfxj5w0g3bgld7xr92y4cb8l9i49rriz07wd4n8dps3panasz3") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.1.17 (c (n "html-node") (v "0.1.17") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.1") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cf7ih7jpr72f24g1088gcwcd24fmqnfl3yhbl26av6mfifv81s6") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.2.0 (c (n "html-node") (v "0.2.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zqmld8llk59c1r5bmaf1j4b827v0apnf53mahb5djhw8bkibxfp") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.2.1 (c (n "html-node") (v "0.2.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1xwc7x731ndz1cxcb3zajfg68kvs1ilvx3ni9n5fa8qks2d05ijp") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.2.2 (c (n "html-node") (v "0.2.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09xqcsizq6k64avxchv53skyrdhn5cp9viv882xngy4xmv3rsjby") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.2.3 (c (n "html-node") (v "0.2.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.2") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0y8w2f4x2w1l4g7yfjdf2gy2apafrzd8rr9vcmphcd0i8l8cw1za") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.3.0 (c (n "html-node") (v "0.3.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.3") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hacr8dz3c3zx9bpncdrz9rwp39fija885ccrs236zm2hqadk6g3") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.4.0 (c (n "html-node") (v "0.4.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.4") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13abvdshw0i7vwwjv8a20zl0rxm438pqhcxq2dv6vxcqvkzl63dk") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

(define-public crate-html-node-0.5.0 (c (n "html-node") (v "0.5.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "html-node-core") (r "^0.5") (d #t) (k 0)) (d (n "html-node-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1z43y0r2cqxhv5ig6mrf7lwp4w8wjnvz78yhr8g2lxk7pjvav58m") (f (quote (("typed" "html-node-core/typed" "html-node-macro/typed") ("serde" "html-node-core/serde") ("pretty" "html-node-core/pretty") ("axum" "html-node-core/axum"))))))

