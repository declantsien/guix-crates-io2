(define-module (crates-io ht ml html5ever_dom_sink) #:use-module (crates-io))

(define-public crate-html5ever_dom_sink-0.1.1 (c (n "html5ever_dom_sink") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.1") (d #t) (k 0)) (d (n "html5ever_test_util") (r "^0.1.0") (d #t) (k 2)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "string_cache_plugin") (r "^0") (d #t) (k 2)))) (h "0yldhx4b1sn55y5zqvyrm0xbhkmyvb8zzd48qs17vp9vidx7s6xd")))

(define-public crate-html5ever_dom_sink-0.2.0 (c (n "html5ever_dom_sink") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.2") (d #t) (k 0)) (d (n "html5ever_test_util") (r "^0.1.0") (d #t) (k 2)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "string_cache_plugin") (r "^0") (d #t) (k 2)) (d (n "tendril") (r "^0.1") (d #t) (k 0)))) (h "19akd2birh5fqq1x4ab0lf2zlxkpg6fys52q6z5q4fiw88zv9kg8")))

