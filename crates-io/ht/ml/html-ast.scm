(define-module (crates-io ht ml html-ast) #:use-module (crates-io))

(define-public crate-html-ast-0.0.0 (c (n "html-ast") (v "0.0.0") (h "1a3v12hp8v86ksg254kvkc09jqb5vcji85zi0nlgggrpff8gxfpz") (f (quote (("default"))))))

(define-public crate-html-ast-0.1.0 (c (n "html-ast") (v "0.1.0") (h "197fy8wfrb7rkzxw5mlzp6g4fvfcirdja550bhkamda2ax2f7k2z") (f (quote (("default"))))))

