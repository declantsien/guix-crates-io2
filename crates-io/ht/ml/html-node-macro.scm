(define-module (crates-io ht ml html-node-macro) #:use-module (crates-io))

(define-public crate-html-node-macro-0.1.0 (c (n "html-node-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02930k0328xr9ab7n5mfpm6y7fv64gq5y1sr7lkq2nyv95zqjpjh")))

(define-public crate-html-node-macro-0.1.1 (c (n "html-node-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0iviswi7k2sgp4aj53g86x516x3jz9x0jvb9kdg1pg99fryxgwlr")))

(define-public crate-html-node-macro-0.1.2 (c (n "html-node-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zg6nazaddzqfl0yl66yn65b3pbxgknwvv0bi8mz5bds8nx56r8p")))

(define-public crate-html-node-macro-0.1.3 (c (n "html-node-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1l394pnwjl7hggiyab8jbgkinrlmi7kdwbica8cw269ypzy266bi")))

(define-public crate-html-node-macro-0.1.4 (c (n "html-node-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11ll7vzx3myzd8pk4z8sb5cyq9hilwvzx52n5zrri6z2iz7m46g7")))

(define-public crate-html-node-macro-0.1.5 (c (n "html-node-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1c7bx84whjnjb99c4fa13yrqy6smy2pm5wf42han2mc2jcxazayr")))

(define-public crate-html-node-macro-0.1.6 (c (n "html-node-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ca67nvwvsihan20ry892z0f4rg2pzrm839y23r5xm0bmbnrm0vf")))

(define-public crate-html-node-macro-0.1.8 (c (n "html-node-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1lljqqhnvgaz51bcb8qlk1zrcdisdfv5s70bssa8ihrip6h2aqvs")))

(define-public crate-html-node-macro-0.1.9 (c (n "html-node-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0nf3bwfm6l0v62fn2npjwicvqqspayxzk20i8hn89cvyk1i3bil9")))

(define-public crate-html-node-macro-0.1.10 (c (n "html-node-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "09mpf4q6dkjlpki1dx80ynyr7ipy7xa3bqs22jzzv1i3bxk7wmd4")))

(define-public crate-html-node-macro-0.1.12 (c (n "html-node-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0l4ngdln0hy1jpj0y4pgqnw9k8smcqryvxp64cby432xhxbv55s8") (f (quote (("typed"))))))

(define-public crate-html-node-macro-0.1.13 (c (n "html-node-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0j2fy7injd65a84hfra2vvxx07r1b2rlcg36bmv0grf5dwc4558d") (f (quote (("typed"))))))

(define-public crate-html-node-macro-0.1.14 (c (n "html-node-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1y3z9gs4g5h36g90ah0zccsy4cpr5vzrpzh6cvk2r2l7g33aal9y") (f (quote (("typed"))))))

(define-public crate-html-node-macro-0.1.15 (c (n "html-node-macro") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1h93vln7p3v55n1g03i6lkzvrill5wpswzih7f2237gj4cw6d26n") (f (quote (("typed"))))))

(define-public crate-html-node-macro-0.1.16 (c (n "html-node-macro") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1rgk8d8rxdwm83sh7kbl4m2bd4v46mkm61h2rfm6v2s38d0c2hpd") (f (quote (("typed"))))))

(define-public crate-html-node-macro-0.1.17 (c (n "html-node-macro") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hb2g4dja66p344zjgdbvq50bflganla20j5mi4f733w4iij0q9x") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.1.18 (c (n "html-node-macro") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0g72pz3gps1z5lhrmr297waimhpp5swn5500ykfwvhxqjkpd3ny1") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.2.0 (c (n "html-node-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wymrbpmq8r67qdjxk44nz6ify0qpnfjiyidjjnpcliz9km149g6") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.2.1 (c (n "html-node-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0mnagqd128h272qmcvf4jchgbhzipgm88p3fr9cr7y17kwr6bg56") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.2.2 (c (n "html-node-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1a2lj91m50cj2a2zjnja9rqy7lwzxcvwypimza91h2ccj92ry6v6") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.2.3 (c (n "html-node-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1i0xycdpw218gr03x93rjbci9r6cddr6y8bmirjnwnq0zm0smk1r") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.3.0 (c (n "html-node-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1qi8hcddmp1zgl8qdw1a1cglg6wb82cvvrs05rammgz67l36hz2y") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.4.0 (c (n "html-node-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nshpg0fili3kzlmzw1r4ialsbixgcj097zl2msxnrllb6j79m2r") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.4.1 (c (n "html-node-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0f6kiry3ry4d1fmaj7c5bnszjn30c6jx9x2hadsp9am2cw1wsf16") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

(define-public crate-html-node-macro-0.5.0 (c (n "html-node-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wsznq5dy8l3y580zn0ssbnwyl26xdy2xy0cp42wf8rg75qcrp14") (s 2) (e (quote (("typed" "dep:syn_derive"))))))

