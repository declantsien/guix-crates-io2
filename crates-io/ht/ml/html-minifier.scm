(define-module (crates-io ht ml html-minifier) #:use-module (crates-io))

(define-public crate-html-minifier-1.0.0 (c (n "html-minifier") (v "1.0.0") (d (list (d (n "minifier") (r "^0.0.19") (d #t) (k 0)))) (h "13m6bzrkr5n9l15c8jib7zd93lldz1k2av5b7gjdpr17599fvygl")))

(define-public crate-html-minifier-1.0.1 (c (n "html-minifier") (v "1.0.1") (d (list (d (n "minifier") (r "^0.0.19") (d #t) (k 0)))) (h "0kpfv0fmiqi0zl53k5pbq7fx6jgzhvq3xh4zp1i7zxkajvlka40a")))

(define-public crate-html-minifier-1.1.0 (c (n "html-minifier") (v "1.1.0") (d (list (d (n "minifier") (r "^0.0.19") (d #t) (k 0)))) (h "0sls93cnl2w8r18x8br5cf9hy4azgj0g4dihg8j34d21cacjzi3s")))

(define-public crate-html-minifier-1.1.1 (c (n "html-minifier") (v "1.1.1") (d (list (d (n "minifier") (r "^0.0.19") (d #t) (k 0)))) (h "0a23qn8qj15pab00zlgvhc56jnzqq4mfhd03ra3745nl403lllig")))

(define-public crate-html-minifier-1.1.2 (c (n "html-minifier") (v "1.1.2") (d (list (d (n "minifier") (r "^0.0.19") (d #t) (k 0)))) (h "13a6bvnd5d2i2y3b6xmpdxwp7kxii30f39kg4ry3868j928b357r")))

(define-public crate-html-minifier-1.1.3 (c (n "html-minifier") (v "1.1.3") (d (list (d (n "minifier") (r "^0.0.20") (d #t) (k 0)))) (h "1456l5k8m77ni6dac83944vp94a3qgk11kcixlx28pifpnrymm32")))

(define-public crate-html-minifier-1.1.4 (c (n "html-minifier") (v "1.1.4") (d (list (d (n "minifier") (r "^0.0.21") (d #t) (k 0)))) (h "1d2342y6aj5q4ww876gbpcj22vnzf44rl13s51a6d81230r107iy")))

(define-public crate-html-minifier-1.1.5 (c (n "html-minifier") (v "1.1.5") (d (list (d (n "minifier") (r "^0.0.30") (d #t) (k 0)))) (h "0lqagn8fv76wv1im4x9iw85hdm3xkbh9qbjh517jp667d56nsryg") (y #t)))

(define-public crate-html-minifier-1.1.6 (c (n "html-minifier") (v "1.1.6") (d (list (d (n "minifier") (r "^0.0.26") (d #t) (k 0)))) (h "1m0j580y72fccjbhplmbrs72ix0sjckw86brqwxhzgi5y4rvhm02")))

(define-public crate-html-minifier-1.1.7 (c (n "html-minifier") (v "1.1.7") (d (list (d (n "minifier") (r "^0.0.31") (d #t) (k 0)))) (h "178jjfybyhi6w92xqda2wqr9bqybpw40qxnqqf47l7hk0pfvyr0d")))

(define-public crate-html-minifier-1.1.8 (c (n "html-minifier") (v "1.1.8") (d (list (d (n "minifier") (r "^0.0.31") (d #t) (k 0)))) (h "0yvhf8xr02wyz50f88kc5xps64k5v630nbp1hqm9z37frznrm2mf")))

(define-public crate-html-minifier-1.1.9 (c (n "html-minifier") (v "1.1.9") (d (list (d (n "minifier") (r "^0.0.31") (d #t) (k 0)))) (h "12k9rb4smpplg7karp90vdlqirz3j4hispxc57aq5y3sj25zidrn")))

(define-public crate-html-minifier-1.1.10 (c (n "html-minifier") (v "1.1.10") (d (list (d (n "minifier") (r "^0.0.33") (d #t) (k 0)))) (h "0p7793imfi7a3hw9qc86acnwvlzxlzh4mpm37z0565nl7y0b9rjf") (y #t)))

(define-public crate-html-minifier-1.1.11 (c (n "html-minifier") (v "1.1.11") (d (list (d (n "minifier") (r "^0.0.33") (d #t) (k 0)))) (h "12gy5d3pw85rzf2ql53877a9xdpr1q7679da1dv781iwxfc4by22") (y #t)))

(define-public crate-html-minifier-1.1.12 (c (n "html-minifier") (v "1.1.12") (d (list (d (n "minifier") (r "^0.0.33") (d #t) (k 0)))) (h "0fbk50yjf8vazq49vdllf805zvm6sbdi273v27jsyzvklcfph57r") (y #t)))

(define-public crate-html-minifier-1.1.13 (c (n "html-minifier") (v "1.1.13") (d (list (d (n "minifier") (r "^0.0.33") (d #t) (k 0)))) (h "1lmmmj1d2yv5mbg32h07xlp1gqx80sdjnd1qgiibj2akrwbsvq33") (y #t)))

(define-public crate-html-minifier-1.1.14 (c (n "html-minifier") (v "1.1.14") (d (list (d (n "minifier") (r "^0.0.33") (d #t) (k 0)))) (h "07s9gfd08k75pw5whilyc867k7mfwv21c2l95alyl2v2sxf1dnsa") (y #t)))

(define-public crate-html-minifier-1.1.15 (c (n "html-minifier") (v "1.1.15") (d (list (d (n "minifier") (r "^0.0.35") (d #t) (k 0)))) (h "0abwfxvg8gl9p1nynx9g1b9sxcgshd4gmg9n8ar6x54a6ih07nm4") (y #t)))

(define-public crate-html-minifier-1.1.16 (c (n "html-minifier") (v "1.1.16") (d (list (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "1hmad0rk3cj3rlxlhwzv8k441dbdjsv8bh1z58cvd4cxk5jzcac6")))

(define-public crate-html-minifier-1.2.0 (c (n "html-minifier") (v "1.2.0") (d (list (d (n "educe") (r ">= 0.4") (f (quote ("Default"))) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "0q9m28sfwy33msjh95ipqlrabzrvf7m05wr91yaa5i62yv8qn46l")))

(define-public crate-html-minifier-1.2.1 (c (n "html-minifier") (v "1.2.1") (d (list (d (n "educe") (r ">= 0.4") (f (quote ("Default"))) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "1mlma2flp0qaia1wqy1x3qavxzknvzch9i2d42bbilh1m4jah0ps")))

(define-public crate-html-minifier-1.2.2 (c (n "html-minifier") (v "1.2.2") (d (list (d (n "educe") (r ">=0.4") (f (quote ("Default"))) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "11xykx8x74mzpz2xdqfgdwd0qn40zla28d0pjz3l84732wmw6lvk")))

(define-public crate-html-minifier-2.0.0 (c (n "html-minifier") (v "2.0.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0sakacdz5w0f3kjmdz76mlc9kyjm1zcbx2m2081kyx5z52gaf1na") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.1 (c (n "html-minifier") (v "2.0.1") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "04gf4dnrgbgr3457l4ywrgmyml8svfpdp420zd3h0w88gc4hzpc9") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.2 (c (n "html-minifier") (v "2.0.2") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "14b9csjlvfswfnkr38l5ag5z1c69mvvwc769wdv2s5wvgb7pgh39") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.3 (c (n "html-minifier") (v "2.0.3") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0qg7pi7isn8js9xxn1g6b4ddz7id93x0axwmqp16zvwsslww8zam") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.4 (c (n "html-minifier") (v "2.0.4") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1pqrr4m6iz58gbbrkfv2ngj21vd4xzbvkgxyyajg1z2xm4c0r897") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.5 (c (n "html-minifier") (v "2.0.5") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0ii62lsxic3c9bl04g63i7y6brgqwvzdgaihjx36ya99cinm16jz") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.6 (c (n "html-minifier") (v "2.0.6") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0ggxrzn4a0xgla3ymlwlkddv8gzs71m49d21y2phzf933b6cvl7i") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.7 (c (n "html-minifier") (v "2.0.7") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1xfpbyxk540jwsw9s9lr49p5bmy8lgnldxyyh1p8xpib1varg9pi") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.8 (c (n "html-minifier") (v "2.0.8") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1dm5myhpsfhd4ckfrbr4s3j5xjwk7dd5pzj24hd0m4cmh0vbqd04") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.9 (c (n "html-minifier") (v "2.0.9") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "08y9bvlr48pwylkqlkm30fvhm53hgjq0ybncq9afrma70bi2b4qd") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.10 (c (n "html-minifier") (v "2.0.10") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0hiidqz7klzz7p466l87hcxw53dw4j2iqmiqy01rh6vzhr80yhlc") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.11 (c (n "html-minifier") (v "2.0.11") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1kacff9snnw15cc1jdgkbw6ligj4m2wrxhzpphzqblm5gl6nb2hr") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.12 (c (n "html-minifier") (v "2.0.12") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0i16drsv8icgy5li1c2l4g2vjsk3z49c6k28qr1k3shi81vgjv37") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.13 (c (n "html-minifier") (v "2.0.13") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0wdlj0wrg1ipln4gjmjyayjlhzj1ppz6f33yfrnxdx2740gclp1v") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.0.14 (c (n "html-minifier") (v "2.0.14") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0dh34532qxrh2k598c891qcpwfk0g5mf1xrsrhka814zxn8wx8ym") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.1.0 (c (n "html-minifier") (v "2.1.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1rs0w8xss8g58qw145fknkfc9gn1s44swih2y8v5pd0k84jalriy") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.0 (c (n "html-minifier") (v "3.0.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "02qwqbwj6pprvjkrw9hlv8rl4z39qhfgj6hwx4isqmph66q8nyhf") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-2.1.1 (c (n "html-minifier") (v "2.1.1") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1sxv99185qciyq6cdzlv8j8yg2l9vbzn0x5bmihghayapwysih9h") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.1 (c (n "html-minifier") (v "3.0.1") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "0v33wylwdsym4m44r90gl1vm70lqwkgaia5rfhy3zsn6yphrygyg") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.2 (c (n "html-minifier") (v "3.0.2") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "03zd7b95hcn2x8qsybpa9w5nsawl635h766pz1z0fhz40dgfsw96") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.3 (c (n "html-minifier") (v "3.0.3") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "1z16vpqhdbz8nal5m3d2sy969zakf0qckkzj45qi38yfbnys34fc") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.4 (c (n "html-minifier") (v "3.0.4") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "1zha9vjppvx5fbx840dngg6bdiql822yf48rw6nwnm6g1jj3klb3") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.5 (c (n "html-minifier") (v "3.0.5") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "00c9x7v2yszkgbxjki0xbifc8n9km7bdday0cxgrviyfd30hnlyb") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.6 (c (n "html-minifier") (v "3.0.6") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "1vxmwd2zw9mzwfv0cp4na5dahjq3ys7llfzfni1z7nyjwnx419v8") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.7 (c (n "html-minifier") (v "3.0.7") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "18w82f20cdzripcx99xn12v5wmyb6nv3gwyxfbiysja19j3alpaz") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.8 (c (n "html-minifier") (v "3.0.8") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.36") (d #t) (k 0)))) (h "0rn70ikgspgwnhrp5z1578rz9wmrr1hz5ig3vk9a9m2vb36qzq0h") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.9 (c (n "html-minifier") (v "3.0.9") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.39") (d #t) (k 0)))) (h "12402pl4hbk07165b3zzg5hjp7gf84azc3z9ffcyf76hc9kv96df") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.10 (c (n "html-minifier") (v "3.0.10") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.39") (d #t) (k 0)))) (h "08p1sl33yxrqhvfhl838pmv01rvaf3y844p9rl80ay872dim510h") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.11 (c (n "html-minifier") (v "3.0.11") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.39") (d #t) (k 0)))) (h "1w4s8b4b0c7wbsqcb093kg7fnz58r358ns22mn5zgligmlnfsxlq") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.12 (c (n "html-minifier") (v "3.0.12") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.40") (d #t) (k 0)))) (h "1bwn96prvl5hpbv7i278dx4bfycf1fmd1z8k949v70kyccp8cfss") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.13 (c (n "html-minifier") (v "3.0.13") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.41") (d #t) (k 0)))) (h "1byvw2d2yh45sng2bzm3znnm8qvbjbaxds1j8nvy7zglxjyirwbh") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.14 (c (n "html-minifier") (v "3.0.14") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.41") (d #t) (k 0)))) (h "1i6g666bv3345gksyvjgfmv9zx847068w9pwvc20qyjdj49j8r4a") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.15 (c (n "html-minifier") (v "3.0.15") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.0.43") (d #t) (k 0)))) (h "12mq4nmqwc3mbpz3hrsi9zc5jyk3gcp4qs74ln2jl2ns0bz9p46r") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-3.0.16 (c (n "html-minifier") (v "3.0.16") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "185rh4lqvh4b41pg4h8pvcxw85n6zi6434jg9sfhfjhcgyzrblzy") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-html-minifier-3.0.17 (c (n "html-minifier") (v "3.0.17") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "11qfn0gjqmw6v8dzsp8zdn95z4ygdakmggwsr6srphc0cl5fd7d6") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-html-minifier-4.0.0 (c (n "html-minifier") (v "4.0.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "03xhk9j94fwdyxdp1i6m2gvcqbh0amfpkkwbyzm9f6xywhv41xqq") (f (quote (("std") ("default" "std"))))))

(define-public crate-html-minifier-5.0.0 (c (n "html-minifier") (v "5.0.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "educe") (r ">=0.4") (f (quote ("Debug" "Default"))) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "minifier") (r "^0.3") (d #t) (k 0)))) (h "1n0sk9bs21yslmjic0xb8nd58nmpg3xvpsrshpdbsyzfr3hn53yb") (r "1.60")))

