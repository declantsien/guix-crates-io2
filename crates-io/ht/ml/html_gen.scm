(define-module (crates-io ht ml html_gen) #:use-module (crates-io))

(define-public crate-html_gen-0.3.0 (c (n "html_gen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "1j3q3wbaw3qm5pnbnfxvd35m52y0708y8m90z0rjwb8s2c4w824d")))

(define-public crate-html_gen-0.3.1 (c (n "html_gen") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "1x23if4a05dgks0a47y5rahini39zlr7lir67hh2qw3rsxhcfm1p")))

(define-public crate-html_gen-0.3.2 (c (n "html_gen") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "0ds9jivx9rpxbglgam7ikfy56nmaqndsxb1sbm3ag30rz91qjkx8")))

(define-public crate-html_gen-0.3.3 (c (n "html_gen") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "11ljml3bn0qbj3jgrrpqsbjd1wxca347vbl46dr4dpqlnpxy2c28")))

(define-public crate-html_gen-0.5.0 (c (n "html_gen") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "1p3m89gbza0gs2lxbgmrwl764rmxd0w6zi1qbrfwjqv37r6cqdmj")))

(define-public crate-html_gen-0.5.1 (c (n "html_gen") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filepath") (r "^0.1.1") (d #t) (k 0)) (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rsass") (r "^0.13.0") (d #t) (k 0)))) (h "0lr8wzpnhvwr8d7j8vrn56fl80d6qzmp7ci2dgjkncqbffb3vgc7")))

