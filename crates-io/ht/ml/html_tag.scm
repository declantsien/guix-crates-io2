(define-module (crates-io ht ml html_tag) #:use-module (crates-io))

(define-public crate-html_tag-0.1.0 (c (n "html_tag") (v "0.1.0") (h "05aprvp00dbbfabbn3py8g50xjzvhc7yl7nj69zz9rpv227nw5hl")))

(define-public crate-html_tag-0.1.1 (c (n "html_tag") (v "0.1.1") (h "0qv6wwy6rrc0ka08qhzii7vkl2mq6zg5zr7gbws1h4q4fswip6pd")))

(define-public crate-html_tag-0.1.3 (c (n "html_tag") (v "0.1.3") (h "1vjxjg36h05g33q7nqb6hy9gnk9qdyhprky6sdb4hfga04jld315")))

