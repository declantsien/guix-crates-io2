(define-module (crates-io ht ml html_to_email) #:use-module (crates-io))

(define-public crate-html_to_email-0.1.0 (c (n "html_to_email") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.3") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "visdom") (r "^0.4.10") (d #t) (k 0)))) (h "1ybqg8mfr4bv34gyfwla6yi1fjn5195i5b6h50s0srpv0j9djy2d")))

(define-public crate-html_to_email-0.1.1 (c (n "html_to_email") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "visdom") (r "^0.4.10") (d #t) (k 0)))) (h "0imfsqjh4gcwn9556c6a9vks743rgs7mar7ijvymjzh7vr1vg6zb")))

(define-public crate-html_to_email-1.0.2 (c (n "html_to_email") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "visdom") (r "^0.4.13") (d #t) (k 0)))) (h "0jpvqqrvyflgjnia74lnni900q8n5jr2cynzla269bascp7rkmrl")))

(define-public crate-html_to_email-1.0.3 (c (n "html_to_email") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.5") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "visdom") (r "^0.5.10") (f (quote ("full"))) (d #t) (k 0)))) (h "1ykfrmhi857yvxcmyj6qr442k8515vjapywi70hn77hx3gqj99yn")))

