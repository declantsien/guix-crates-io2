(define-module (crates-io ht ml html_preprocessor) #:use-module (crates-io))

(define-public crate-html_preprocessor-0.0.1 (c (n "html_preprocessor") (v "0.0.1") (h "1gwy9x7lafmn09vq708jjmswbxz0qvjha1jhwwrwjkd4wd0zppc7")))

(define-public crate-html_preprocessor-0.0.2 (c (n "html_preprocessor") (v "0.0.2") (h "1g5in1v8sixb4rd4az7rk32p30xxldv4a874jk31h29l1km2nnmb")))

(define-public crate-html_preprocessor-0.0.3 (c (n "html_preprocessor") (v "0.0.3") (h "0sd211zc11qwzinpwcrpr7zwjgh84iphdshmdlqp89gi1lxq8d2r")))

