(define-module (crates-io ht ml html5ever-arena-dom) #:use-module (crates-io))

(define-public crate-html5ever-arena-dom-0.1.0 (c (n "html5ever-arena-dom") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "1x2srqawc8w08gc3ipad7lvq965fa714mrb7rr0rv0kqz54gs0i4")))

(define-public crate-html5ever-arena-dom-0.1.1 (c (n "html5ever-arena-dom") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "1w7k0l5yw6h5g4bflarmd99458wxx430lddp5w2nj4v54rf9a5qg")))

(define-public crate-html5ever-arena-dom-0.1.2 (c (n "html5ever-arena-dom") (v "0.1.2") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "18a6sw1kx3favb67vgm19hm2488rnbnny8axnq16ksx1rmcckf21")))

