(define-module (crates-io ht ml html-outliner) #:use-module (crates-io))

(define-public crate-html-outliner-0.1.0 (c (n "html-outliner") (v "0.1.0") (d (list (d (n "kuchiki") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vdxnavx094lspv97m078kk4dyr8lh6f2n1yxaqn2nakyqjz6ip0")))

(define-public crate-html-outliner-0.1.1 (c (n "html-outliner") (v "0.1.1") (d (list (d (n "kuchiki") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cp79iv6sybj5hi2a2h47vmx684167lkcpfkzfy7rv0kz02przcc")))

(define-public crate-html-outliner-0.1.2 (c (n "html-outliner") (v "0.1.2") (d (list (d (n "kuchiki") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yspj60l3vs8r8yvvhn04knfcb4f7wl3h5n5l4sxzi2sz15wg6xc")))

(define-public crate-html-outliner-0.1.3 (c (n "html-outliner") (v "0.1.3") (d (list (d (n "kuchiki") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xpj2n30y3jja6fm3c72hshpracm8svcpwj8n2q5c8di09vyw820")))

(define-public crate-html-outliner-0.1.4 (c (n "html-outliner") (v "0.1.4") (d (list (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r8fdn8zrc4j42v9v90mqqxakzwv8nyfwiwh2m1fgs5igx9g05bj")))

(define-public crate-html-outliner-0.1.5 (c (n "html-outliner") (v "0.1.5") (d (list (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0q3zqf52z6p2l7si1r60jb29rz6pana6pl1jh77y63kpij4v84q2")))

(define-public crate-html-outliner-0.1.6 (c (n "html-outliner") (v "0.1.6") (d (list (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14c1328a89566022327cj8q7gvwfv3iq56w26riakadwv4q175gx") (r "1.61")))

