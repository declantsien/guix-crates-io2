(define-module (crates-io ht ml html-datetime-local) #:use-module (crates-io))

(define-public crate-html-datetime-local-0.1.0 (c (n "html-datetime-local") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1lmm10vrfd5d6p1ccv7v5hsz46bx0i9cyrc0l0s5p6c48r557r9n")))

