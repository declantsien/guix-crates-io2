(define-module (crates-io ht ml htmlprops-to-react) #:use-module (crates-io))

(define-public crate-htmlprops-to-react-0.1.0 (c (n "htmlprops-to-react") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1k3b7z5aqfbm0vr1rv53a3rf6wfx7vs9ndf63f7c28p39mpxkg4s")))

(define-public crate-htmlprops-to-react-0.1.1 (c (n "htmlprops-to-react") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0p0ypgpb0pf6jrc2nbjwh76mjj3d3mypxxxfqwb52x067v60z8wn")))

