(define-module (crates-io ht ab htable2csv) #:use-module (crates-io))

(define-public crate-htable2csv-0.1.0 (c (n "htable2csv") (v "0.1.0") (d (list (d (n "isahc") (r "^1.2.0") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)))) (h "0azn21kiijp2877n7fipnqirijm82dnyj20hyvsrwfcqla0vrhk0")))

(define-public crate-htable2csv-0.1.1 (c (n "htable2csv") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "isahc") (r "^1.2.0") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)))) (h "1mzbj17im0jpcfp3dkzl73chx3ksswv7hqlyjjks09wcqfw1ai1k")))

(define-public crate-htable2csv-0.1.2 (c (n "htable2csv") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "isahc") (r "^1.2.0") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)))) (h "08n4w15jmbs44r18wwc23bk1fyrd3554s8r3z5qyldjdx8badm21")))

