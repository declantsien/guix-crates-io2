(define-module (crates-io ht ab htable) #:use-module (crates-io))

(define-public crate-htable-0.1.0 (c (n "htable") (v "0.1.0") (h "0kcipy394fm7z7nbcxkwxw5lnikgmd45rjc5ycf538km5hjgg61r")))

(define-public crate-htable-0.2.0 (c (n "htable") (v "0.2.0") (h "0mzw2lmfa4cmfd0ci8d1bvi06sv4mi97gm949pvsrbdqv77kgpb2")))

(define-public crate-htable-0.2.1 (c (n "htable") (v "0.2.1") (h "1b81sii23vxza3h7x6m0a6mmx700zhvzsh6vc9pdlm3x1pskl1lx")))

(define-public crate-htable-0.3.1 (c (n "htable") (v "0.3.1") (h "018hiqzmdkqpa4pdhrh0ri5dh2ri5y7cq1wwhg85alkfj3s9xxj6")))

(define-public crate-htable-0.3.2 (c (n "htable") (v "0.3.2") (h "0riwzrqj6f5xqzqrhmyll6rlhnsadvnycmyq91wmmwisviv4yw74")))

