(define-module (crates-io ht u2 htu21df-sensor) #:use-module (crates-io))

(define-public crate-htu21df-sensor-0.1.0 (c (n "htu21df-sensor") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1vygl8i4jp11hvvrynlsb1fxrc2km76dpx4p8dwiyjsi15xw7wfc")))

(define-public crate-htu21df-sensor-0.1.1 (c (n "htu21df-sensor") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0w6c4z5najlw93gcvd5rydj252hydd6dwq5chi0m1f2rlp8bq8kv")))

(define-public crate-htu21df-sensor-0.1.2 (c (n "htu21df-sensor") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1ar7mxhkv7g18jpzph54ga0wbmld254l6wbx0c7x5f3vfgkphicl")))

(define-public crate-htu21df-sensor-0.1.3 (c (n "htu21df-sensor") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1zw195rv5barsbwalvzpz49bgldkf48mwcmykk26ylnh3cnrijsw")))

(define-public crate-htu21df-sensor-0.1.4 (c (n "htu21df-sensor") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1v9mcbm6b0fj9aw22fwcyrh7a5zrwspzvmnfgwzq22phxld37yll") (f (quote (("std") ("default"))))))

