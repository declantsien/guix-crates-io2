(define-module (crates-io ht u2 htu21d) #:use-module (crates-io))

(define-public crate-HTU21D-0.1.0 (c (n "HTU21D") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 0)))) (h "0qxfxj3z2xxj59kqhkdi92fjqihwh2nr9a08nqa5y852vrw7gvrb")))

(define-public crate-HTU21D-0.1.1 (c (n "HTU21D") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 0)))) (h "0bgyfbcvcwbdqbybi94kxvk9gn2aspakkj3mncssyqgiyn5vwbwl")))

