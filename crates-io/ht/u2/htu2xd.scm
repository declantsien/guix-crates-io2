(define-module (crates-io ht u2 htu2xd) #:use-module (crates-io))

(define-public crate-htu2xd-0.1.0 (c (n "htu2xd") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0a56qipjwwph44wqd3harhglgx2qwid61y48yf3rb9wkkac87x4f")))

