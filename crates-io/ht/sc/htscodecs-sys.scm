(define-module (crates-io ht sc htscodecs-sys) #:use-module (crates-io))

(define-public crate-htscodecs-sys-1.2.1 (c (n "htscodecs-sys") (v "1.2.1") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "0j4hwlpfdrivyrgl8wc6j5yyihlbn2pm9isspwjj1z519b94v67b") (l "htscodecs")))

