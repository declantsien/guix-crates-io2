(define-module (crates-io ht es htesterp) #:use-module (crates-io))

(define-public crate-htesterp-0.1.0 (c (n "htesterp") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k84k3alz7bgqr2f00gcjw58zg78qpw80vhxf7ma3ajn0c5di23h")))

(define-public crate-htesterp-0.1.1 (c (n "htesterp") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "04xrsp7rirdl9zzf830f0wwlbbgby8jddd5bqx0w3i5crrk71pql")))

(define-public crate-htesterp-0.2.0 (c (n "htesterp") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h4023xyh613bnbrid7p5hhn3lc1dmalknqs0mc0d15nlfiqmrx0")))

(define-public crate-htesterp-0.2.1 (c (n "htesterp") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yid539c7l2zhkh01g21c7ls6sj0fv6l0wz0ws259sbc44csl692")))

(define-public crate-htesterp-0.2.2 (c (n "htesterp") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbqswxpc9dnsc60g0bi8a8w9n13awnii350zib1c0nvjs0kcnx2")))

