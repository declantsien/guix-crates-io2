(define-module (crates-io ht wd htwdresden) #:use-module (crates-io))

(define-public crate-htwdresden-0.1.0 (c (n "htwdresden") (v "0.1.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.3") (d #t) (k 0)))) (h "0yy744y19g1q7nfrpdhaglcvny3s306hvcq4hkbpy47qc7xc5mqs")))

(define-public crate-htwdresden-0.1.1 (c (n "htwdresden") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1y44sm0jbj04xixkf20smxzp11sfp9hlxzfvpp52mx36sydv9s2v")))

(define-public crate-htwdresden-0.2.0 (c (n "htwdresden") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "04nrljd49gdlxmvbkzc662gdm7v2g2xkyhf3yya79w17ljclhzfj")))

(define-public crate-htwdresden-0.3.1 (c (n "htwdresden") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1sn24ds78zcy0zq3wgzy69rw0gzysqsyj94n1c2h4da8i8cr22ra")))

(define-public crate-htwdresden-0.3.2 (c (n "htwdresden") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0qpjhwxgql70gpxmqd4kfs2an51fy9spvi6w33ibfm7c7g4cra0k")))

(define-public crate-htwdresden-0.3.3 (c (n "htwdresden") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "13k59dvx65q6r3w292pj76z9wkrzcxni92gs6mqay19r4s86816l")))

(define-public crate-htwdresden-0.3.4 (c (n "htwdresden") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1zj5jwa41mkanmvviqxihiw7ymgs6mnckgd8xc471cm70fc0bil9")))

