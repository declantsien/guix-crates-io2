(define-module (crates-io ht m_ htm_md) #:use-module (crates-io))

(define-public crate-htm_md-0.1.0 (c (n "htm_md") (v "0.1.0") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "htmlize") (r "^1.0.4") (f (quote ("unescape_fast"))) (d #t) (k 0)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "md_htm") (r "^0.1.0") (d #t) (k 0)) (d (n "mdka") (r "^1.2.1") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0fd69hz0cvg6s1mld31qan5j9fdn7nibq15kg77072zas905m6xv")))

