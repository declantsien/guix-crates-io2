(define-module (crates-io ht ti httimple) #:use-module (crates-io))

(define-public crate-httimple-0.1.0 (c (n "httimple") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "1kl75n8zrz4amvck7wf4rpmnyvcd6j02fh4j859lrbrk62z21c16")))

(define-public crate-httimple-0.1.1 (c (n "httimple") (v "0.1.1") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "1bh4xhiwikh2r0fry34kmq6rlj4fa2cpzklhjzdg0v0kwyybv88i")))

(define-public crate-httimple-0.1.2 (c (n "httimple") (v "0.1.2") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "1h5snj9xll5p83ghmwlaz3c4qdjfvlhdhjz8knvdyjn39xsigg9z")))

(define-public crate-httimple-0.1.3 (c (n "httimple") (v "0.1.3") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "0pw1wpak3a2mr68kmnjayjcagxpikylf8zsnfka9ah6lb57rpl2k")))

(define-public crate-httimple-0.1.4 (c (n "httimple") (v "0.1.4") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "1zsks3s9v6vr1508grgd7h2d7iiga4zwm741hwpd4jrqn8vb1gik")))

(define-public crate-httimple-0.1.5 (c (n "httimple") (v "0.1.5") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "0hk6g2iafra09hz96fsf1s6pgxj4dv66km0xdawc14mxc30fj86h")))

(define-public crate-httimple-0.1.6 (c (n "httimple") (v "0.1.6") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "09s54yyw85n7qcxsq09yvq4b59g15zmyiqfmb12j4wi3zy6c82a2")))

(define-public crate-httimple-0.1.7 (c (n "httimple") (v "0.1.7") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "hpack_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "rustls") (r "^0.12.0") (d #t) (k 0)))) (h "0x3576mvkjbl28n1pli7pirgrg6qd1y2p8chdvyq3ks2f7a0gg2h")))

