(define-module (crates-io ht im htime) #:use-module (crates-io))

(define-public crate-htime-0.0.1 (c (n "htime") (v "0.0.1") (h "1h43zcmwnyqhldkrga9y51vgl9wkb9cylap6jm2mlf50pa6fqhdc")))

(define-public crate-htime-0.0.2 (c (n "htime") (v "0.0.2") (h "1089y3fggis341c052158jz7w8mkk3dm2hl45ig2wb4n6n19cfd2")))

(define-public crate-htime-0.0.3 (c (n "htime") (v "0.0.3") (h "1hf8jml7gwj3avpj79jlgbywq7fqpg4dlrxmw1qcpsz6gld7s8jd")))

