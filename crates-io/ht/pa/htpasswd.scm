(define-module (crates-io ht pa htpasswd) #:use-module (crates-io))

(define-public crate-htpasswd-0.1.0 (c (n "htpasswd") (v "0.1.0") (h "1n3mxm7vbpghja22yqz76gyrqz5s807mcwf98zpqi41gzdva9bvr") (y #t)))

(define-public crate-htpasswd-0.2.0 (c (n "htpasswd") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "1cqvnv8mhcamyzii8dn8xp5gjvcznk5lc3gb3bfs2lbrhbhdnixy") (y #t)))

