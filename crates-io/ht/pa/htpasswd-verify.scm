(define-module (crates-io ht pa htpasswd-verify) #:use-module (crates-io))

(define-public crate-htpasswd-verify-0.1.0 (c (n "htpasswd-verify") (v "0.1.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bcrypt") (r "^0.6") (d #t) (k 0)) (d (n "pwhash") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "022sdnds67h4hyr2vf0sfbwzrd5rkr3dlhkr2zxmxhl6gsck5ylk")))

(define-public crate-htpasswd-verify-0.2.0 (c (n "htpasswd-verify") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bcrypt") (r "^0.6") (d #t) (k 0)) (d (n "pwhash") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1vb983jlc807npjcxabyrghbr3skfkq597aqh9hmqmy7yy0nqy8h")))

(define-public crate-htpasswd-verify-0.2.1 (c (n "htpasswd-verify") (v "0.2.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bcrypt") (r "^0.10.1") (d #t) (k 0)) (d (n "pwhash") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1igk9nd0n6yk4a3yyxmvp4lgdhzihi0k3iw1h8sx9az1kjdy4f7z")))

(define-public crate-htpasswd-verify-0.3.0 (c (n "htpasswd-verify") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bcrypt") (r "^0.13") (d #t) (k 0)) (d (n "pwhash") (r "^1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1g7ww23r9kf70zsvycx2knrxyrgv0lifmlxpf4h660yjpi9a3yi3")))

