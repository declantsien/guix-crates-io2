(define-module (crates-io p3 dt p3dtxt) #:use-module (crates-io))

(define-public crate-p3dtxt-0.1.0 (c (n "p3dtxt") (v "0.1.0") (d (list (d (n "armake2") (r "^0.2.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.7") (d #t) (k 0)))) (h "0xx5lk3nhz3sb7604jj2lhjmi0nqjd8vj8s4ch8r12x72iadwm73")))

(define-public crate-p3dtxt-0.2.0 (c (n "p3dtxt") (v "0.2.0") (d (list (d (n "armake2") (r "^0.3.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.7") (d #t) (k 0)))) (h "10iz0pvfbn87v3zzwxii5a8x114n0mh8daq89c619b6vf52sdy1f")))

