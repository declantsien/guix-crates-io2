(define-module (crates-io xk b- xkb-data) #:use-module (crates-io))

(define-public crate-xkb-data-0.1.0 (c (n "xkb-data") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0r67sdks6zy164ml85rp86z53mwzp1hcbmpm8jgkr976r6gmjji9")))

