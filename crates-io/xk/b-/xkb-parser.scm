(define-module (crates-io xk b- xkb-parser) #:use-module (crates-io))

(define-public crate-xkb-parser-0.1.0 (c (n "xkb-parser") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "from-pest") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (f (quote ("pretty-print"))) (d #t) (k 0)) (d (n "pest-ast") (r "^0.3.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 2)) (d (n "shrinkwraprs") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 2)))) (h "1cc3g76imaxc2f9b63x736mqdwjq0h9i4zgbgpvr6p82y25688jx")))

