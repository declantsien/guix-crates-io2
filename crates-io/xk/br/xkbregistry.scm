(define-module (crates-io xk br xkbregistry) #:use-module (crates-io))

(define-public crate-xkbregistry-0.1.0 (c (n "xkbregistry") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "12lvaxd0vy87fy3x6lk0p88i9qkvsslrwl8v9b54b247aiqp4x4q") (f (quote (("static")))) (y #t)))

(define-public crate-xkbregistry-0.1.1 (c (n "xkbregistry") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1y5w1k4ir8ia8mn15kna05c5m85w6lh40q9scb7mbggd8mfk10bn") (f (quote (("static"))))))

