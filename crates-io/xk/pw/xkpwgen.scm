(define-module (crates-io xk pw xkpwgen) #:use-module (crates-io))

(define-public crate-xkpwgen-0.1.0 (c (n "xkpwgen") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 1)) (d (n "sha2") (r "^0.4") (d #t) (k 1)))) (h "13drnpci5p751yl802fv4fzdcdk13fsj2h8448fczhpcv8qvfli1")))

(define-public crate-xkpwgen-0.2.0 (c (n "xkpwgen") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 1)) (d (n "sha2") (r "^0.4") (d #t) (k 1)))) (h "1pkgyxv9ybiksvy5kim2plwylwqraa2m4j7ri8rki391l5wdwxfs")))

(define-public crate-xkpwgen-0.3.0 (c (n "xkpwgen") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1sxmrhc96j39q4vbwcl7ax92ckvy8jd5pwqwx3ypib1wh9agiv9r")))

(define-public crate-xkpwgen-0.4.0 (c (n "xkpwgen") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1aqzbv8jwpdzn9pck55q4h8i12cg6d65gkli850ijw3mhhampqqa") (y #t)))

(define-public crate-xkpwgen-0.4.1 (c (n "xkpwgen") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "179kf18mkziqybbgq5yhji0k2d9hbbs24d3jxxpz23ypixxf0mla")))

(define-public crate-xkpwgen-0.5.0 (c (n "xkpwgen") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "062j87j4yqmanix8z06vdpmx1spnqqyydxc22hm8v7m4r1db6kn0") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.5.1 (c (n "xkpwgen") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p2ra59gcdvilp4ilfbmrkhz8hw7cypr2v0yp7ccv6bj2wk37pfm") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.6.0 (c (n "xkpwgen") (v "0.6.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)))) (h "012fvsff1ixb1z5hgclf5r4bmxdcbaz0dkn7fsliprgsnfibfv4n") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.6.1 (c (n "xkpwgen") (v "0.6.1") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)))) (h "016mmq2af38mliac954jwyyh25qpwdar4lzm7q4c7viifsyzb27c") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.7.0 (c (n "xkpwgen") (v "0.7.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)))) (h "14g47iz8s9540c2j9m9m1djrphg0c9i9q8fw43bwx61n2dy2idvx") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.8.0 (c (n "xkpwgen") (v "0.8.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)))) (h "1yz735n8fnqp149h1b2v658p04b6wdh96rr1ipsgvrqvx311k5gs") (f (quote (("default"))))))

(define-public crate-xkpwgen-0.8.1 (c (n "xkpwgen") (v "0.8.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1pbci2zshbnyjc0nbxmnv42a9xi467b1af8ay6rpsy368nnxkl8y") (f (quote (("default"))))))

(define-public crate-xkpwgen-1.0.0 (c (n "xkpwgen") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1k42p6khnjwfgjvz1bs4wrirmhiwhdr1bm8shg3kw5zmql4c35sc") (f (quote (("default"))))))

(define-public crate-xkpwgen-1.0.1 (c (n "xkpwgen") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02xv1w73z47n1j68qn0dl42dvyk7mc8a3fl4gv04vl45v0vdmvsw") (f (quote (("default"))))))

