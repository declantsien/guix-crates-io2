(define-module (crates-io xk cd xkcdpass) #:use-module (crates-io))

(define-public crate-xkcdpass-0.1.0 (c (n "xkcdpass") (v "0.1.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1sq86wgamnxqghcsxh2260nrwg17ny5rqrnnynjsigjxjb4y63l5")))

(define-public crate-xkcdpass-0.2.0 (c (n "xkcdpass") (v "0.2.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "11nkzgjkvnfahy0alp14jq8szb3p5dsfkhpp5gdj23201xh4csba")))

(define-public crate-xkcdpass-0.3.0 (c (n "xkcdpass") (v "0.3.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1z29drsnsxf9b22cxd753qhimckwa17g44impz3db6hcy8zkm0mv")))

(define-public crate-xkcdpass-0.4.0 (c (n "xkcdpass") (v "0.4.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1gjd2kzsjsw4igh4w95bagjwqz9g9nqz847wfaa6h3w3l64bzw2s")))

(define-public crate-xkcdpass-0.5.0 (c (n "xkcdpass") (v "0.5.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qvg77y9pz6fdqq06m4hpwywqfja3wm89mm70c2x8yxp4izkcpza")))

