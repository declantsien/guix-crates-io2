(define-module (crates-io xk cd xkcd_get) #:use-module (crates-io))

(define-public crate-xkcd_get-0.1.0 (c (n "xkcd_get") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "1wxva5vn1h9816y3nl0qwbysjikcqkivglxlcdwbx50gf6rr31na")))

(define-public crate-xkcd_get-0.1.1 (c (n "xkcd_get") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "16nrnz6isaark9889vdkfr8wsz1hipp2cpxs6jlj74xij3l2wcwx")))

(define-public crate-xkcd_get-0.1.2 (c (n "xkcd_get") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0q5gpmp1awcszm375xx4qb2bwh3krd7dcyhk47l8br0q1b40kf51")))

(define-public crate-xkcd_get-0.2.0 (c (n "xkcd_get") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "161z8fslymmz7lalaiyw064r9kdya3bkp32g2ncsaxir2qkbfxyy")))

