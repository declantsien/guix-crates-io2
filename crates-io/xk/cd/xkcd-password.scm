(define-module (crates-io xk cd xkcd-password) #:use-module (crates-io))

(define-public crate-xkcd-password-0.1.0 (c (n "xkcd-password") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "06jyjlppk7i25yybrqk81x836c2vyz90kfb7q5wsmj73wfk6rmhm") (f (quote (("default" "built_in_dicts") ("built_in_dicts"))))))

(define-public crate-xkcd-password-0.1.1 (c (n "xkcd-password") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1y2pzw4nqyhfz4jjdwmdq44wfvs993znyasis89z9ihrvsn6g98d") (f (quote (("default" "built_in_dicts") ("built_in_dicts"))))))

