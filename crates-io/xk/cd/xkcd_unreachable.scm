(define-module (crates-io xk cd xkcd_unreachable) #:use-module (crates-io))

(define-public crate-xkcd_unreachable-0.1.0 (c (n "xkcd_unreachable") (v "0.1.0") (h "0p3zagdq5vjp3mpdhb3fzysza07kzl9bkkzzmjxxj8i2kd6c9z7z")))

(define-public crate-xkcd_unreachable-0.1.1 (c (n "xkcd_unreachable") (v "0.1.1") (h "0vbcd9yyicay577x3l2pr44zqkdbl5khrqad89gx3yg87fjxf3zx")))

