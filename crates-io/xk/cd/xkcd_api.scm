(define-module (crates-io xk cd xkcd_api) #:use-module (crates-io))

(define-public crate-xkcd_api-0.1.0 (c (n "xkcd_api") (v "0.1.0") (d (list (d (n "nanoserde") (r "^0.1.22") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "1kd9zb31g34swgqnk37jscwihxin2ynfg97bnv9mccnnnmw3wznn")))

(define-public crate-xkcd_api-1.0.0 (c (n "xkcd_api") (v "1.0.0") (d (list (d (n "nanoserde") (r "^0.1.22") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "1l4j8r14rdg19gw361xv1hy3v3wrq51dsk4xkr30pl4ky3iykxxw")))

