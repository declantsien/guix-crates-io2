(define-module (crates-io xk cp xkcp-sys) #:use-module (crates-io))

(define-public crate-xkcp-sys-0.0.0 (c (n "xkcp-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "133ajgymgyxln4x9rgx5wpnkc48bl4vjz5pnjxj4ly7c6kvwpf7i") (r "1.65")))

(define-public crate-xkcp-sys-0.0.1 (c (n "xkcp-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0h4g6930g914gd054jrxqzz9mrq5ls8xwh8m9n0i22599gjg9ky1") (f (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (r "1.65")))

(define-public crate-xkcp-sys-0.0.2 (c (n "xkcp-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "058kia0i2ysc50jfwkxjk7q83a6izaxyaaypzprnmddiggqfr5d0") (f (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (r "1.65")))

(define-public crate-xkcp-sys-0.0.3 (c (n "xkcp-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "11ahgh51crq29hwrnrh00d4rv36maayw40nlglg7xz8spcrg2i7s") (f (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (r "1.65")))

