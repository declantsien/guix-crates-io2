(define-module (crates-io xk cp xkcp-rs) #:use-module (crates-io))

(define-public crate-xkcp-rs-0.0.0 (c (n "xkcp-rs") (v "0.0.0") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "xkcp-sys") (r "^0.0.0") (d #t) (k 0)))) (h "0a2yr47cpnpfzz7j9jizjf8jfciz52nxcw1rw36vcdwqqmmf1dhi") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-xkcp-rs-0.0.1 (c (n "xkcp-rs") (v "0.0.1") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "xkcp-sys") (r "=0.0.1") (d #t) (k 0)))) (h "1yacap84a8rsmw5c5p4qcgn0yp4f1nk8v6s6090b20z15i5f328x") (f (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (r "1.65")))

(define-public crate-xkcp-rs-0.0.2 (c (n "xkcp-rs") (v "0.0.2") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "xkcp-sys") (r "=0.0.2") (d #t) (k 0)))) (h "1fq5p5gas8r1b2wl0iyr12lpqp6znp84rdfkfc0g19f9ww67cqh0") (f (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (r "1.65")))

(define-public crate-xkcp-rs-0.0.3 (c (n "xkcp-rs") (v "0.0.3") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "xkcp-sys") (r "=0.0.3") (d #t) (k 0)))) (h "1d4hw3n28fkji0cfwf6gbd42w347g3w871ks1dmfwy3xxy0bscs8") (f (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (r "1.65")))

