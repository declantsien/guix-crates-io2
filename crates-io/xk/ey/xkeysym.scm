(define-module (crates-io xk ey xkeysym) #:use-module (crates-io))

(define-public crate-xkeysym-0.1.0 (c (n "xkeysym") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 2)) (d (n "x11rb") (r "^0.11.0") (d #t) (k 2)))) (h "0dnydfq3h8hzi0i0sqgwp9n2z5az44cwmrqg9s65gybil30jlmfd") (r "1.32.0")))

(define-public crate-xkeysym-0.1.1 (c (n "xkeysym") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 2)) (d (n "x11rb") (r "^0.11.0") (d #t) (k 2)))) (h "0s32yq56vsz3jigbdk2b2m0qgzy8cfg5vrrrhgc1mr1rzaigr7sg") (r "1.32.0")))

(define-public crate-xkeysym-0.2.0 (c (n "xkeysym") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (o #t) (k 0)) (d (n "bytemuck") (r "^1.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (k 0)) (d (n "x11rb") (r "^0.11.0") (d #t) (k 2)))) (h "0886dn1rlkiazcp5n6ayqfg0ibpiny62dlbiyr9v4l32nxl8wjh5") (r "1.48.0")))

