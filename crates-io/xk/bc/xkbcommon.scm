(define-module (crates-io xk bc xkbcommon) #:use-module (crates-io))

(define-public crate-xkbcommon-0.1.0 (c (n "xkbcommon") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.6") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0g8vsb4zmqxpfzkkrj1f3kji3nz8pqkc28kxwa8mjnlr4dq7b3nb") (f (quote (("x11" "xcb"))))))

(define-public crate-xkbcommon-0.2.0 (c (n "xkbcommon") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.6") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "15xbz3yxsy5rfmcdx23g4dz7w4jr7j7ngjjmsv8cyv9w4c1m63ba") (f (quote (("x11" "xcb"))))))

(define-public crate-xkbcommon-0.2.1 (c (n "xkbcommon") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0m1ysgyanrw1cwhqdn1dyp9pn3m61c71yjhicvhfvbcmfgd6q0jq") (f (quote (("x11" "xcb"))))))

(define-public crate-xkbcommon-0.3.0 (c (n "xkbcommon") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0mn8x2pmzjgf5gkfczp11ascqakv46yjvdg9bafx80nrhpb7bjbs") (f (quote (("x11" "xcb"))))))

(define-public crate-xkbcommon-0.2.2 (c (n "xkbcommon") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "1dzi38clqr6x8lc1lxlchbbnmmlld3ln36w34yfq7mlrs948w5sb") (f (quote (("x11" "xcb") ("compose")))) (y #t)))

(define-public crate-xkbcommon-0.2.3 (c (n "xkbcommon") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0cfawq6qyq4jdls6igvwyv8kwgbdmd61xiqcicbgf18wlm9xi2a3") (f (quote (("x11" "xcb") ("compose")))) (y #t)))

(define-public crate-xkbcommon-0.4.0 (c (n "xkbcommon") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "1kig3drxm3jbjlyxv9vxawpi2x32w2z9jxysxvg53gfsgmgym87x") (f (quote (("x11" "xcb") ("wayland" "memmap"))))))

(define-public crate-xkbcommon-0.5.0-beta.0 (c (n "xkbcommon") (v "0.5.0-beta.0") (d (list (d (n "evdev") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.102") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "xcb") (r "^1.0.0-beta") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0a5jpmgybpdcxx0w9l7z4rhsv9yvkibldbbz3kp7hd54s8287d77") (f (quote (("x11" "xcb") ("wayland" "memmap"))))))

(define-public crate-xkbcommon-0.4.1 (c (n "xkbcommon") (v "0.4.1") (d (list (d (n "evdev") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "xcb") (r "^0.9.0") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "1kr8ik92ckz12s9my66a7grip84n87xgkannpchi5hsmqw6d0bh3") (f (quote (("x11" "xcb") ("wayland" "memmap"))))))

(define-public crate-xkbcommon-0.5.0 (c (n "xkbcommon") (v "0.5.0") (d (list (d (n "evdev") (r "^0.11.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "xcb") (r "^1.1.1") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "0bm9qyw9hmdn65vskphajjxy0ivzvdzj1002v8ianya3f4vf3gmc") (f (quote (("x11" "xcb") ("wayland" "memmap2") ("default" "wayland"))))))

(define-public crate-xkbcommon-0.5.1 (c (n "xkbcommon") (v "0.5.1") (d (list (d (n "as-raw-xcb-connection") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "evdev") (r "^0.11.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "123b96fbp693z43z2f2gbadb9dzf523n2d21j3izcaz9k6sjbnsj") (f (quote (("x11" "as-raw-xcb-connection") ("wayland" "memmap2") ("default" "wayland"))))))

(define-public crate-xkbcommon-0.6.0 (c (n "xkbcommon") (v "0.6.0") (d (list (d (n "as-raw-xcb-connection") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "evdev") (r "^0.11.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "xkeysym") (r "^0.2.0") (d #t) (k 0)))) (h "1avq087w3zj4zfp4sjfn0ivpvx7zm0lw35lin0cjsmxk8hf3g1n2") (f (quote (("x11" "as-raw-xcb-connection") ("wayland" "memmap2") ("default" "wayland"))))))

(define-public crate-xkbcommon-0.7.0 (c (n "xkbcommon") (v "0.7.0") (d (list (d (n "as-raw-xcb-connection") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "evdev") (r "^0.11.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "memmap2") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "xkeysym") (r "^0.2.0") (d #t) (k 0)))) (h "07n9shhcls66wjvmk5pzqql46ipfdv7b8hbc384wgv9hk4jpv1hk") (f (quote (("x11" "as-raw-xcb-connection") ("wayland" "memmap2") ("default" "wayland"))))))

