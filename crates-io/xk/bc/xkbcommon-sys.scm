(define-module (crates-io xk bc xkbcommon-sys) #:use-module (crates-io))

(define-public crate-xkbcommon-sys-0.7.0 (c (n "xkbcommon-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1adx8pfafrk55bniiybr8yxhq9cg46qxdhrjrlxzkxm3jf329ihq") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-0.7.1 (c (n "xkbcommon-sys") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1khg8jrmkl4y12wbha33lhyyggb927dc81zzw3fkd6167jrr5lvl") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-0.7.2 (c (n "xkbcommon-sys") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r1nv7gsgchvijdml6y5a5g983fvzvaryxmvqvaxwkliv2m2wr8d") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-0.7.3 (c (n "xkbcommon-sys") (v "0.7.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19kjsf76kp7aaxg5l4fp7yk4ms7pbl4d92jdlv2cy3jsxqyp4xf5") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-0.7.4 (c (n "xkbcommon-sys") (v "0.7.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12yp8s1by5v1zmz70a52jp81d0xpvdqfawaxg4lbybm0vj04jhzs") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-0.7.5 (c (n "xkbcommon-sys") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1daaan0j7yp8zkvy6zmzfswmvczjfw8m12i23iab9c25knvh382r") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-1.3.0 (c (n "xkbcommon-sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03g57hppr213pcxwja17sjjc2jr2lk50c524kvh4rlil3fb204c0") (f (quote (("x11") ("static"))))))

(define-public crate-xkbcommon-sys-1.4.1 (c (n "xkbcommon-sys") (v "1.4.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "142ww452qq0q47fmc4khzsd0fbwmb71fjl7pci573zf83fvdpxsn") (f (quote (("x11") ("static"))))))

