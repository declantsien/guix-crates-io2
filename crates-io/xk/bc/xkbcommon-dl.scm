(define-module (crates-io xk bc xkbcommon-dl) #:use-module (crates-io))

(define-public crate-xkbcommon-dl-0.1.0 (c (n "xkbcommon-dl") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dlib") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "x11-dl") (r "^2.17") (d #t) (k 0)))) (h "11mmdnhnlvyzhm322sbavjyxr1kf5dsiy5fhb46696k79ccn0grd")))

(define-public crate-xkbcommon-dl-0.2.0 (c (n "xkbcommon-dl") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xnp0kqr1wrgsg594yrwqr4i4z0ciib6q1wndd8wvrh9y9lr406d") (f (quote (("x11"))))))

(define-public crate-xkbcommon-dl-0.3.0 (c (n "xkbcommon-dl") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ww1y1zl9x8b07m22xj6nwdrskc7ar1gj4188jp94fsxvqvgiydd") (f (quote (("x11"))))))

(define-public crate-xkbcommon-dl-0.4.0 (c (n "xkbcommon-dl") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "dlib") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "xkeysym") (r "^0.2.0") (d #t) (k 0)))) (h "0rc1la66lfm4rmppg275yx81manalw7wld0azc23p85yr9cjq3k4") (f (quote (("x11")))) (r "1.64")))

(define-public crate-xkbcommon-dl-0.4.1 (c (n "xkbcommon-dl") (v "0.4.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "dlib") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "xkeysym") (r "^0.2.0") (d #t) (k 0)))) (h "16c6kmgqbffdsnw5w9q97p55d824ss3fqzif2lrh33648j2nc939") (f (quote (("x11")))) (r "1.64")))

(define-public crate-xkbcommon-dl-0.4.2 (c (n "xkbcommon-dl") (v "0.4.2") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "dlib") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "xkeysym") (r "^0.2.0") (d #t) (k 0)))) (h "1iai0r3b5skd9vbr8z5b0qixiz8jblzfm778ddm8ba596a0dwffh") (f (quote (("x11")))) (r "1.64")))

