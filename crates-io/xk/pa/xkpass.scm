(define-module (crates-io xk pa xkpass) #:use-module (crates-io))

(define-public crate-xkpass-0.1.0 (c (n "xkpass") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11819j8yjvra1xv4lzimmzmbpyrw70v9ax24b7k8n91bq5rlqhhb")))

