(define-module (crates-io qi ng qingke-rt) #:use-module (crates-io))

(define-public crate-qingke-rt-0.1.7 (c (n "qingke-rt") (v "0.1.7") (d (list (d (n "qingke") (r "^0.1.7") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1.7") (d #t) (k 0)))) (h "08jv3vkg80pd9bbgg88rg482xzbp3kgvyz3xpl62c9g0gx8gpvyi") (f (quote (("highcode"))))))

(define-public crate-qingke-rt-0.1.8 (c (n "qingke-rt") (v "0.1.8") (d (list (d (n "qingke") (r "^0.1") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1") (d #t) (k 0)))) (h "1mbc1pyj43ml5wnx2bf9h49ic2frsp6d79ajf3395la3ky9wmg9p") (f (quote (("highcode"))))))

(define-public crate-qingke-rt-0.1.9 (c (n "qingke-rt") (v "0.1.9") (d (list (d (n "qingke") (r "^0.1") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1") (d #t) (k 0)))) (h "0awmrmw41im2lvldjqd396jnvdwwwrpl54hhjc0srl66nmpmpy2v") (f (quote (("highcode"))))))

(define-public crate-qingke-rt-0.1.10 (c (n "qingke-rt") (v "0.1.10") (d (list (d (n "qingke") (r "^0.1") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1") (d #t) (k 0)))) (h "0y41b3dg92dhh4a1lmfjnhmyhvacwiqxwknnsc3y0ydhfix4yl1m") (f (quote (("highcode"))))))

(define-public crate-qingke-rt-0.1.11 (c (n "qingke-rt") (v "0.1.11") (d (list (d (n "qingke") (r "^0.1") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1") (d #t) (k 0)))) (h "1l5ajnish7lagmfwxya2qdyq0dnq2phy37sqjj2hflaz9517x2rj") (f (quote (("highcode"))))))

(define-public crate-qingke-rt-0.1.12 (c (n "qingke-rt") (v "0.1.12") (d (list (d (n "qingke") (r "^0.1") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "^0.1") (d #t) (k 0)))) (h "1aa4p1gyzlirvaab7lwpizqf2r3d48vkd4ryi5a92242h4jhm654") (f (quote (("v2") ("highcode"))))))

(define-public crate-qingke-rt-0.2.0 (c (n "qingke-rt") (v "0.2.0") (d (list (d (n "qingke") (r "=0.2.0") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "=0.2.0") (d #t) (k 0)))) (h "0y9gljwzg24zbx19vqppi2azc287q3h4mq7rjwm2p4ls64vw26x8") (f (quote (("v4") ("v3") ("v2") ("highcode"))))))

(define-public crate-qingke-rt-0.2.1 (c (n "qingke-rt") (v "0.2.1") (d (list (d (n "qingke") (r "=0.2.0") (d #t) (k 0)) (d (n "qingke-rt-macros") (r "=0.2.1") (d #t) (k 0)))) (h "025nnxxcrc34pppkn21jfxcz9cml8q3vzhjqj5vjvg64c1zx4xdi") (f (quote (("v4") ("v3") ("v2") ("highcode"))))))

