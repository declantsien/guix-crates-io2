(define-module (crates-io qi ng qingke) #:use-module (crates-io))

(define-public crate-qingke-0.1.0 (c (n "qingke") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "144rcrg91a1rfapm1dqadbh1l50hqg65mx7zb3v3r183waxivls9")))

(define-public crate-qingke-0.1.1 (c (n "qingke") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)))) (h "0qdmw2fz99sksyfiw1nd9l1v7lzpcsxxnisnwkcaa9amn3xb14q1")))

(define-public crate-qingke-0.1.2 (c (n "qingke") (v "0.1.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)))) (h "16gny4jp6ggk879is151kc7g133xf80364mr73c9cqzarhv4hhsg")))

(define-public crate-qingke-0.1.3 (c (n "qingke") (v "0.1.3") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "18dzbzyh381lk4qvmjx1vgkyq1ikplksbsrqzh8ghw6xd42pyn04")))

(define-public crate-qingke-0.1.4 (c (n "qingke") (v "0.1.4") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1d4r9cb2j5ajzdzd9bjnikinpi7zab151pvby05lc8dpnrq7i6xb")))

(define-public crate-qingke-0.1.5 (c (n "qingke") (v "0.1.5") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "16jwb947sjj5390rqvd4xl94ahd7g2kclkvh8zyvm2z57vfj7s0w")))

(define-public crate-qingke-0.1.6 (c (n "qingke") (v "0.1.6") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1kpxq32gg0jcqx522xfngr0fxi8lplj8bw63823d3h09m63zym3d") (s 2) (e (quote (("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.7 (c (n "qingke") (v "0.1.7") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1inmzjqhijdivxh8nqh423h09s0rv0v41y5phgsqmc9j3q6b839i") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.8 (c (n "qingke") (v "0.1.8") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "01yxz0g0rihncnq641v7zj72b6k92cg8swgf0nb60fbgcddp6jwc") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.9 (c (n "qingke") (v "0.1.9") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "0yc76hv5j1gqd7lfis4r7l1q13k6fh9p4akjr5dlb61ri2cfsa8p") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.10 (c (n "qingke") (v "0.1.10") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "1vw2p396xjacbqhg2gnp2kq9pj8r0xpahmzvisa7x669yv5vniji") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.11 (c (n "qingke") (v "0.1.11") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "157ily0r4sjh4vqi2jw019y86s92i5xv31faalzbw67fck4xa5w9") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.1.12 (c (n "qingke") (v "0.1.12") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "1c35lclvxgpk7yzhg799fybqkh4nmc4s29iisjnsn7f9fn00gyyq") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

(define-public crate-qingke-0.2.0 (c (n "qingke") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-u8"))) (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)))) (h "1w97l5brjlv50ppn4fs230njs0sxjjc1phnwfdyxv7pg67dvpaa2") (s 2) (e (quote (("defmt" "dep:defmt") ("critical-section-impl" "dep:critical-section"))))))

