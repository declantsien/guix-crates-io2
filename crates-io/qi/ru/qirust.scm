(define-module (crates-io qi ru qirust) #:use-module (crates-io))

(define-public crate-qirust-0.1.0 (c (n "qirust") (v "0.1.0") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1ry1s7hp53kb546y7fv7synms0w0zwxcfg5zx56gxz00z36pfsc6")))

(define-public crate-qirust-0.1.1 (c (n "qirust") (v "0.1.1") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "18vlfgmmx329nzd3zw81nx1b0wly84kv8ys3yi0gfg0fqijzh7iv")))

(define-public crate-qirust-0.1.2 (c (n "qirust") (v "0.1.2") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1lkc5an0nljyzfmbvd1dza9iwjijkahiaq8nchb0iyhwh6ysiqld")))

(define-public crate-qirust-0.1.3 (c (n "qirust") (v "0.1.3") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1lp0wdpb5w2mhpncqrsj7slsxzcfhzlqv63v9jcgp5fmbmhwi2yh")))

(define-public crate-qirust-0.1.4 (c (n "qirust") (v "0.1.4") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0q1bgyb2w7lb7l681r53siivdcnlfdjgzwg8sfq5isi59c4ay2xa")))

(define-public crate-qirust-0.1.5 (c (n "qirust") (v "0.1.5") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "189v0ni1by02s5zn1pa0cfnkc5ghbsdh8zpfr4fhibch9dlarp3w")))

(define-public crate-qirust-0.1.6 (c (n "qirust") (v "0.1.6") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0zgv6nyz9waq92lrz3qg4m8z3km3yfa4jy4nnb2sr6z135gvirv3")))

(define-public crate-qirust-0.1.7 (c (n "qirust") (v "0.1.7") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "15d3781nar99pj1zxcpiajwd9i2dcbms8qaxgidwcrhbfybm7qca")))

(define-public crate-qirust-0.1.8 (c (n "qirust") (v "0.1.8") (d (list (d (n "image") (r "^0.25.0") (d #t) (k 0)))) (h "0mp090122amyqd6qnzrinmcq8g281srnjbla3xlxz2l7mbgdr68a")))

