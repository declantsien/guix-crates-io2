(define-module (crates-io qi ne qinetic_utils_macros) #:use-module (crates-io))

(define-public crate-qinetic_utils_macros-0.1.0 (c (n "qinetic_utils_macros") (v "0.1.0") (d (list (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "156242wwa698qnz6vziyk8yj7i3lgjbhk1nsqjb1dja85fzkvz45") (y #t)))

