(define-module (crates-io qi ne qinetic_asset) #:use-module (crates-io))

(define-public crate-qinetic_asset-0.1.0 (c (n "qinetic_asset") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "02acrz5ar023qb6h2v55vsfwwma1lfjv358p5rrb35fcmrl8ivrf") (y #t)))

(define-public crate-qinetic_asset-0.1.1 (c (n "qinetic_asset") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0vvlr9bj0h7gbkkgchq47d2k5zx1dqr87129h7mbsyprvfcjk8g3") (y #t)))

(define-public crate-qinetic_asset-0.1.2 (c (n "qinetic_asset") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "04zqjbx5djsy90nlsb3xkjvj9cy712wpinyps0bza27rhx4m4gn1") (y #t)))

(define-public crate-qinetic_asset-0.1.3 (c (n "qinetic_asset") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "01g9qi6sknjwbg83fm46rvl53csw7ch7fxhyh10hkga0v02l0xf9") (y #t)))

