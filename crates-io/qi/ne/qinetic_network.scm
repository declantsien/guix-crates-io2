(define-module (crates-io qi ne qinetic_network) #:use-module (crates-io))

(define-public crate-qinetic_network-0.1.0 (c (n "qinetic_network") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "0r7jaxjrjlld58il9dfv8k5yq6yqc9r8xv0zi3qai0ssi8fwm4b3") (y #t)))

(define-public crate-qinetic_network-0.1.1 (c (n "qinetic_network") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1ancsz53l27irry99clikza4g7qfkabz8npjqfgfs7ajmzy1a9ph") (y #t)))

(define-public crate-qinetic_network-0.1.2 (c (n "qinetic_network") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "10x2cjxrki2c1sjb475f59bahs5skqmaaczdh1qzdswkn5fizbqr") (y #t)))

(define-public crate-qinetic_network-0.1.3 (c (n "qinetic_network") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "0b71fxz2x10kkpw2hnvyk77q9b4708lnl50np68sl50d10jbywq6") (y #t)))

