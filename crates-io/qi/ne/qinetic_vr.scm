(define-module (crates-io qi ne qinetic_vr) #:use-module (crates-io))

(define-public crate-qinetic_vr-0.1.0 (c (n "qinetic_vr") (v "0.1.0") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "02fl2l0m58llrnajw32d0z22v8v7dblkbb4zrsgcqga73zdg72d1") (y #t)))

(define-public crate-qinetic_vr-0.1.1 (c (n "qinetic_vr") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)) (d (n "qinetic_xr") (r "0.*") (d #t) (k 0)))) (h "0fsax570k6x7j9fkdq7cqy5ngzgz6840naf99vb14p34d8snk49q") (y #t)))

