(define-module (crates-io qi ne qinetic_physics) #:use-module (crates-io))

(define-public crate-qinetic_physics-0.1.0 (c (n "qinetic_physics") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "106b7m5366b8h6x16qasdvnykvg1fd63jssqa03dwf7lxhwall49") (y #t)))

(define-public crate-qinetic_physics-0.1.1 (c (n "qinetic_physics") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0p7wkaa9z28pw0ymmb3v5kqa3aimaj2y3y865mg93j6vv7s4dmc0") (y #t)))

(define-public crate-qinetic_physics-0.1.2 (c (n "qinetic_physics") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1a2gd38jzpb0js1ic8w1nwap69fimfax0ph5i0zzqx3fvy9kc9s7") (y #t)))

(define-public crate-qinetic_physics-0.1.3 (c (n "qinetic_physics") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "1a7p2r9g9gy314ci1jcsfmn1kx50rq3amjdwpkfvvfm1pgnpv3l1") (y #t)))

