(define-module (crates-io qi ne qinetic_animation) #:use-module (crates-io))

(define-public crate-qinetic_animation-0.1.0 (c (n "qinetic_animation") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "0fn867c35vvn0gs7lrhndgv2vdcxsw2kah69938q1zxkaxx7rdd4") (y #t)))

(define-public crate-qinetic_animation-0.1.1 (c (n "qinetic_animation") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0p3k41y5irs5i4j24d5x51p1sfv5yvyaafxc5w9hkz7d383y8vyq") (y #t)))

(define-public crate-qinetic_animation-0.1.2 (c (n "qinetic_animation") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0h6kl02nh6imvvv84021zdaysm77clp0jwyjpnszwig1xgbai97s") (y #t)))

(define-public crate-qinetic_animation-0.1.3 (c (n "qinetic_animation") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "0irdn1yhhir8b8nnzh3cmpflr5gwkfnw82a8925srymqy2qlh6bj") (y #t)))

