(define-module (crates-io qi ne qinetic_app) #:use-module (crates-io))

(define-public crate-qinetic_app-0.1.0 (c (n "qinetic_app") (v "0.1.0") (h "19zaligp4f4j0mm07vrs8f04lp451q96aj6sxw79fxnf99nkdczp") (y #t)))

(define-public crate-qinetic_app-0.1.1 (c (n "qinetic_app") (v "0.1.1") (h "1b59mnximl0bj6wfmd9r1qrhzka1ys6iwfvkp68p1xf1nm3qyzzq") (y #t)))

(define-public crate-qinetic_app-0.1.2 (c (n "qinetic_app") (v "0.1.2") (h "0s077iiiagfdl7wzgg1lxxf4r4lma3s9ya0s3sr2vcvncin7a2sr") (y #t)))

(define-public crate-qinetic_app-0.1.3 (c (n "qinetic_app") (v "0.1.3") (h "1z6bv5dsygd5n442dhv34nnznsli982cj4n64k920chkrp6aq3i5") (y #t)))

(define-public crate-qinetic_app-0.1.4 (c (n "qinetic_app") (v "0.1.4") (h "10ma5n4vm1kl0a17fdsnqhybmjh4i6nsff592gcxpdgva7m1d4p3") (y #t)))

(define-public crate-qinetic_app-0.1.5 (c (n "qinetic_app") (v "0.1.5") (h "0rjk8wss3z4dss36jdr2rq4myf89h671g4bs3k440paqf441jfql") (y #t)))

(define-public crate-qinetic_app-0.2.0 (c (n "qinetic_app") (v "0.2.0") (d (list (d (n "downcast-rs") (r "1.*") (d #t) (k 0)) (d (n "qinetic_app_macros") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "1x6agfa344sr6j557a1hhpzx590ifssb08075bf0w4aq7d1v6hia") (y #t)))

