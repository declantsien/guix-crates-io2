(define-module (crates-io qi ne qinetic_utils) #:use-module (crates-io))

(define-public crate-qinetic_utils-0.1.0 (c (n "qinetic_utils") (v "0.1.0") (d (list (d (n "qinetic_utils_macros") (r "0.*") (d #t) (k 0)))) (h "1hi353bykdq0jbv265mbyga84csvj47la2wa7dpqaiwnn6f60s7n") (y #t)))

(define-public crate-qinetic_utils-0.1.1 (c (n "qinetic_utils") (v "0.1.1") (d (list (d (n "qinetic_utils_macros") (r "0.*") (d #t) (k 0)))) (h "1fzl4887vp4n36yi2w5pjcbc2g7x9inkzxcsbl7qj191q2ic8dcr") (y #t)))

