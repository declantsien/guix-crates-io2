(define-module (crates-io qi ne qinetic_app_macros) #:use-module (crates-io))

(define-public crate-qinetic_app_macros-0.1.0 (c (n "qinetic_app_macros") (v "0.1.0") (d (list (d (n "qinetic_utils_macros") (r "0.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "01xi1y641rqdirds64yldw7j8q83vn2aad76b59z68n69hxp5zsd") (y #t)))

