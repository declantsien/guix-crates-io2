(define-module (crates-io qi ne qinetic_core) #:use-module (crates-io))

(define-public crate-qinetic_core-0.1.0 (c (n "qinetic_core") (v "0.1.0") (h "0fh17m9ix9h7h7023q2ixhf6dnbcyy1zv9hv5qyl7s1ylwv7c9d7") (y #t)))

(define-public crate-qinetic_core-0.1.1 (c (n "qinetic_core") (v "0.1.1") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "1904p9m9bwk46mys5q4fjzs9rjw714fvqc7xspa5n3p5v77j4jii") (y #t)))

(define-public crate-qinetic_core-0.1.2 (c (n "qinetic_core") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "17y9c9fq7jn98208hrspca6jzn8bh9z08ifr6cx5nnby9hxrspnz") (y #t)))

(define-public crate-qinetic_core-0.1.3 (c (n "qinetic_core") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1igbkkwz1h5j8913x8nivxz1ib8hps2gd9az4p1rabvbr35h57h7") (y #t)))

(define-public crate-qinetic_core-0.1.4 (c (n "qinetic_core") (v "0.1.4") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "0p91ajwdnhmsj2wn7sz2xv08lfk1ypsqi8vyl587kirs4pm66lrp") (y #t)))

