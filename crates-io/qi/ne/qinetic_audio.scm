(define-module (crates-io qi ne qinetic_audio) #:use-module (crates-io))

(define-public crate-qinetic_audio-0.1.0 (c (n "qinetic_audio") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "1scxim58i80c7q42bqfv72s5nnr3vq82dzm3xqd8qky486lq9bkr") (y #t)))

(define-public crate-qinetic_audio-0.1.1 (c (n "qinetic_audio") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1wpb9b5fbvrg3gd7z8xdlvr6bykymyxsf7a05q2zh6fmvf459j01") (y #t)))

(define-public crate-qinetic_audio-0.1.2 (c (n "qinetic_audio") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1wvc7hkd5lgqgahwm9ka7vmh92zrmssy4lv953fsf3j9hdj19109") (y #t)))

(define-public crate-qinetic_audio-0.1.3 (c (n "qinetic_audio") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "1n1r54iy1w2yclpsfz0zzhzi988ai6hbi16i4g1p00r85m0ls6hs") (y #t)))

