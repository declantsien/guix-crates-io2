(define-module (crates-io qi ne qinetic_input) #:use-module (crates-io))

(define-public crate-qinetic_input-0.1.0 (c (n "qinetic_input") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "0p8sgw2bqgq3hais8sjgjccn4n7sb74wb4s83lz33ampnbaj0h02") (y #t)))

(define-public crate-qinetic_input-0.1.1 (c (n "qinetic_input") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "14i68bv9y0ca60gs02yss7wfvq4vgfch3yffnzddgpj0dd80aywy") (y #t)))

(define-public crate-qinetic_input-0.1.2 (c (n "qinetic_input") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "00cbqi9q6mf8vlgdgr0p5khgf3h8dxzq6ah53ws6jk33pmvxyfxg") (y #t)))

(define-public crate-qinetic_input-0.1.3 (c (n "qinetic_input") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "14zs38qgdcs9i4npygnp6g3ln1kw55268b2w387kh48arnq3xcva") (y #t)))

