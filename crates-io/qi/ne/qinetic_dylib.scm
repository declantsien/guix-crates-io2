(define-module (crates-io qi ne qinetic_dylib) #:use-module (crates-io))

(define-public crate-qinetic_dylib-0.1.0 (c (n "qinetic_dylib") (v "0.1.0") (d (list (d (n "qinetic_internal") (r "^0.1.0") (d #t) (k 0)))) (h "0n44jfpypp8j4sd8jzjqqzx5r5qjpn1dvzmkjhj73vc8mnc1ih80")))

(define-public crate-qinetic_dylib-0.1.1 (c (n "qinetic_dylib") (v "0.1.1") (d (list (d (n "qinetic_internal") (r "^0.1.1") (d #t) (k 0)))) (h "139y10l7r6q438bfnjailqx8295b76an35i3xgpm4fsfnvrpdklv")))

(define-public crate-qinetic_dylib-0.1.2 (c (n "qinetic_dylib") (v "0.1.2") (d (list (d (n "qinetic_internal") (r "0.*") (d #t) (k 0)))) (h "0lvfmk4qn8b65dppdwv3m0pwfx786hawfsik891vclg5x612gspr")))

(define-public crate-qinetic_dylib-0.1.3 (c (n "qinetic_dylib") (v "0.1.3") (d (list (d (n "qinetic_internal") (r "0.*") (d #t) (k 0)))) (h "0inh3al178d04fcrzqjdgypp5yhm4aj040dr1vfs6p9kgxs4s74h")))

