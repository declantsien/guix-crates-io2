(define-module (crates-io qi ne qinetic_log) #:use-module (crates-io))

(define-public crate-qinetic_log-0.1.0 (c (n "qinetic_log") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "0qw12z8d57wmv64cnqnda4f1ny5sd4vpj5kbpq4xvxgnxic9yqad") (y #t)))

(define-public crate-qinetic_log-0.1.1 (c (n "qinetic_log") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "09qkd239vmkyxpf7rp8h65wgfxwwx4jcpa92rrj3swyvzm2pr21l") (y #t)))

(define-public crate-qinetic_log-0.1.2 (c (n "qinetic_log") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0nd4khqx4xl1v0fxlxfaqvvfs5zl4mzvxh7f7393i06qbcrz95qk") (y #t)))

(define-public crate-qinetic_log-0.1.3 (c (n "qinetic_log") (v "0.1.3") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "0hjpzsskvp0xyhjglzwgrywncr0xwv3npnxln7m5qg7bhsp83cwf") (y #t)))

