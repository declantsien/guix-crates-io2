(define-module (crates-io qi ne qinetic_math) #:use-module (crates-io))

(define-public crate-qinetic_math-0.1.0 (c (n "qinetic_math") (v "0.1.0") (h "19rdkl5p8qy6q8wd2bdgaknc8zgqbx6mr4hllnn4zya5hrn2zadp") (y #t)))

(define-public crate-qinetic_math-0.1.1 (c (n "qinetic_math") (v "0.1.1") (h "1lrnbjqp0wifnw9n85i0kbx6brjkzq0k44p3213j7nd1c17x9ih1") (y #t)))

(define-public crate-qinetic_math-0.1.2 (c (n "qinetic_math") (v "0.1.2") (h "0l6g8bp8hsdhmjh3sx5llzjsn72fwi7dbfj15nddbdwkk9869rmx") (y #t)))

(define-public crate-qinetic_math-0.1.3 (c (n "qinetic_math") (v "0.1.3") (h "02qdlyhrrn21awl3md2nby92h0csy6q9ag1g973y6swpbh95gsbz") (y #t)))

(define-public crate-qinetic_math-0.2.0 (c (n "qinetic_math") (v "0.2.0") (h "1xvlxlmamdadbbil7qzhkbv7sbbrcc3m53gx9m8p87xhj8sp8a0f") (y #t)))

(define-public crate-qinetic_math-0.2.1 (c (n "qinetic_math") (v "0.2.1") (h "1c3xkjb4zrqdsxr1f28ixzg5jzpmdzi8r7cbfdqadyqnbziczrvj") (y #t)))

(define-public crate-qinetic_math-0.2.2 (c (n "qinetic_math") (v "0.2.2") (h "0sdns9x2q5cjr2ck6155b8nkhl56lw3vc6j469yah42z7wdcijlh") (y #t)))

(define-public crate-qinetic_math-0.2.3 (c (n "qinetic_math") (v "0.2.3") (h "0q5zpm9pqnayqdq73rlqrk6lryvxs6i5zp8yzwjifpw1998ib2ci") (y #t)))

(define-public crate-qinetic_math-0.2.4 (c (n "qinetic_math") (v "0.2.4") (h "194qxjp0q9aphndz5zc1pq4msk77zwhs509559ixh9wwmyqvays3") (y #t)))

(define-public crate-qinetic_math-0.3.0 (c (n "qinetic_math") (v "0.3.0") (h "120pik2insgarzf2kv0qdwf4rf2zv7kn3wnxlcqx7vbxs6yxj6fn") (y #t)))

(define-public crate-qinetic_math-0.3.1 (c (n "qinetic_math") (v "0.3.1") (h "0yj773rs2srjs4xnvd320999v936c7x701hvyds7hcya4xw1b1kl") (y #t)))

(define-public crate-qinetic_math-0.4.0 (c (n "qinetic_math") (v "0.4.0") (h "1dgyyjlcfxrdc0d142pqy1x1hlbg8vipavjiiygyivwglzm29fnf") (y #t)))

(define-public crate-qinetic_math-0.4.1 (c (n "qinetic_math") (v "0.4.1") (h "0vcwadbrirb0zrmlwiz7s7hicqnhvm7zz910rvxhlgl6jx9w3ypa") (y #t)))

(define-public crate-qinetic_math-0.4.2 (c (n "qinetic_math") (v "0.4.2") (h "0qzqnwha1lay8237rc8gkba9aa75yb1h3zp6yq02xk67174a9hdk") (y #t)))

(define-public crate-qinetic_math-0.4.3 (c (n "qinetic_math") (v "0.4.3") (h "0plncq1namqfm2im5qa0iii9vaqxvv3nmhissw01yr0111q9w4ig") (y #t)))

(define-public crate-qinetic_math-0.4.4 (c (n "qinetic_math") (v "0.4.4") (h "0hjxbrpmb0cc3nfna7fd0ccmxv7f1p95rqbs2gmxa1cr5sjqfhxd") (y #t)))

(define-public crate-qinetic_math-0.5.0 (c (n "qinetic_math") (v "0.5.0") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "0igi77h5y0q80djbg4k5q9mf6by2kgvwdwmmnixnjfnkdnscr3s6") (y #t)))

(define-public crate-qinetic_math-0.6.0 (c (n "qinetic_math") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "1zbr5x74c6g8zz1jcgis77mfg8k1r53dccgsdw7pah9i6pm6m8jd") (y #t)))

