(define-module (crates-io qi ne qinetic_window) #:use-module (crates-io))

(define-public crate-qinetic_window-0.1.0 (c (n "qinetic_window") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "00pvz7bsm237xfgklkn11yr1yzyyn5s2kb75p8r45cdzl01bzar8") (y #t)))

(define-public crate-qinetic_window-0.1.1 (c (n "qinetic_window") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0a74q16ffml6lnmg04nj42bmk6zjj0biw40v4j9wz6j2i2hzrifl") (y #t)))

(define-public crate-qinetic_window-0.1.2 (c (n "qinetic_window") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0mgihg2k3nzy3aqqlxbqmmr9icli1rxzfir6pzpafzcyds6fb969") (y #t)))

(define-public crate-qinetic_window-0.2.0 (c (n "qinetic_window") (v "0.2.0") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)) (d (n "qinetic_ecs") (r "0.*") (d #t) (k 0)))) (h "1bl44sbam487jwdx8v7b8nx5m611d0waa6z6g21cwqi54cnacyxb") (y #t)))

