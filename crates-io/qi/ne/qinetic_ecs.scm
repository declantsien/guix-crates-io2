(define-module (crates-io qi ne qinetic_ecs) #:use-module (crates-io))

(define-public crate-qinetic_ecs-0.1.0 (c (n "qinetic_ecs") (v "0.1.0") (d (list (d (n "qinetic_app") (r "^0.1.0") (d #t) (k 0)))) (h "0c4cga8amipakqgrsykfkj72pkaj6vpcyiqfy5dvhsz0zb13cz2b") (y #t)))

(define-public crate-qinetic_ecs-0.1.1 (c (n "qinetic_ecs") (v "0.1.1") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "1p43vq6wfc2fx95mixyp2cwapr5hdjd3qvcgpra76aa4rv54pdai") (y #t)))

(define-public crate-qinetic_ecs-0.1.2 (c (n "qinetic_ecs") (v "0.1.2") (d (list (d (n "qinetic_app") (r "0.*") (d #t) (k 0)))) (h "0mqdk2fbcllr255pshj9yx01pxdh3fn37aq0y31dqypy7i63hkpr") (y #t)))

(define-public crate-qinetic_ecs-0.2.0 (c (n "qinetic_ecs") (v "0.2.0") (d (list (d (n "qinetic_ecs_macros") (r "0.*") (d #t) (k 0)))) (h "1jz1cqnr95f03x4n60azxvd3sbmgab8zlrkjw0vh8a8znb4cm339") (y #t)))

