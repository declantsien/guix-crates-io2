(define-module (crates-io qi pp qippy) #:use-module (crates-io))

(define-public crate-qippy-0.0.1 (c (n "qippy") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.30.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "161awxgk8mdi2d8rafbjg936lhyldxkff967agd9kvchzs272bd9")))

