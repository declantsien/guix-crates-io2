(define-module (crates-io qi f_ qif_parser) #:use-module (crates-io))

(define-public crate-qif_parser-0.0.1 (c (n "qif_parser") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "071y0lqacvixna96rqy3q3pj4v7zls71c4rs6cgz73f8ydylg878")))

(define-public crate-qif_parser-0.0.2 (c (n "qif_parser") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0y38ypxhzn91rzqhvwbl1x61ww5h6a8hq8kfyk20ni0zq46b2vns")))

(define-public crate-qif_parser-0.0.3 (c (n "qif_parser") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0r93p86f0afl271ka5mwpkvy8a1bwycycgsvvhpkpjs7hji8hayj")))

(define-public crate-qif_parser-0.0.4 (c (n "qif_parser") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1k9h0ngrn9d3i2wxiw1yg0v96pi7a38210xavl19ngahqcs115iw")))

(define-public crate-qif_parser-0.0.5 (c (n "qif_parser") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1x7nlrvrfhrij4w8gx1vn4dmff1ckfxw8g5kxadxxxba2xcmnfx7")))

(define-public crate-qif_parser-0.0.6 (c (n "qif_parser") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qpdk1sai6c3xpl3mh72z13r2y0a11mnx0ciqq5qh4p5avn5ml7s")))

(define-public crate-qif_parser-0.1.0 (c (n "qif_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "14lpiz9503pr1d4ablfwi2f3mvg15cnbv8b8kc0xwcivn6a4kbcs")))

(define-public crate-qif_parser-0.2.0 (c (n "qif_parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w9w4syigg2n4bz5m9plp1ip5kfnfknqrv2g7zx3rcaa2hljpifr")))

(define-public crate-qif_parser-0.3.0 (c (n "qif_parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pajb5rslhw5wnjldjia99pv10x6arqi95alayywb45b85glmpv3")))

(define-public crate-qif_parser-0.4.0 (c (n "qif_parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m0cbxikj43f474az01277b64g2mi3p2892a3cxxwhgszgc4pbb8")))

