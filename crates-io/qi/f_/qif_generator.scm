(define-module (crates-io qi f_ qif_generator) #:use-module (crates-io))

(define-public crate-qif_generator-0.1.0 (c (n "qif_generator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "15l6l448hsb68kxc630gp7h88604qird9vk7h29nbyms3051fmxa")))

(define-public crate-qif_generator-0.1.1 (c (n "qif_generator") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0gsls9vshfbkw5x171jign9mn78bv79fv8jbx0jgfvndnr1vyj8s")))

(define-public crate-qif_generator-0.1.2 (c (n "qif_generator") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0rvpgdq1ym27008mppfixs0y06k8mr9fjwjv8dzbl1fmnqfiwm42")))

(define-public crate-qif_generator-0.1.3 (c (n "qif_generator") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06hmk3m5wcqlzfckmfb5v9jzly1jqlglqbnwpahdh6rb0qmbmsbr")))

(define-public crate-qif_generator-0.1.4 (c (n "qif_generator") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1a9cj72i363g08s5kbqw91p0c89zwn9jsh4h6jsvg41nrqwk2dzm")))

(define-public crate-qif_generator-0.1.5 (c (n "qif_generator") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1f4cw5m1nffhsg6lfx2cpykc3i6fn2m54dlalhfnj96p2wgnk2hq")))

(define-public crate-qif_generator-0.1.6 (c (n "qif_generator") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1fl9q5jmk11d2zksm8blynzm206hl962ni3qzhrw3lz29nfdgjv7")))

(define-public crate-qif_generator-0.1.7 (c (n "qif_generator") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0s6sihg27mci9grx32ni3pyhij781ap4y54nmn8d84d11n5vw50h")))

