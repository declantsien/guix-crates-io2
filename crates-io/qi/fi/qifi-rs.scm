(define-module (crates-io qi fi qifi-rs) #:use-module (crates-io))

(define-public crate-qifi-rs-0.3.0 (c (n "qifi-rs") (v "0.3.0") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.0") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1482vwy0qkpfhqgxii6fmgmhxr75jbz4gaw9yyn7gpa1f00qllpz")))

