(define-module (crates-io qi _o qi_openapi) #:use-module (crates-io))

(define-public crate-qi_openapi-0.1.0 (c (n "qi_openapi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1rnv3vxp3lrcz3k19xzqys44pgr8h9cp5m8ynbkf68gn2vacq4b0") (f (quote (("std" "serde/std") ("default" "std"))))))

