(define-module (crates-io qi rc qirc) #:use-module (crates-io))

(define-public crate-qirc-0.0.1 (c (n "qirc") (v "0.0.1") (h "1r23bkj81w4ms2mcqaibcwq42983wfrry9wlyzhp901vmfiz6x1i") (y #t)))

(define-public crate-qirc-0.0.2 (c (n "qirc") (v "0.0.2") (h "1dzalba7yhsfvkk6ghj9fq9dg969w09fq46nz93823lvmcnhy7sr")))

