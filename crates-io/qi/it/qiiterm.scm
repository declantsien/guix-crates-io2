(define-module (crates-io qi it qiiterm) #:use-module (crates-io))

(define-public crate-qiiterm-0.0.1 (c (n "qiiterm") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.8") (d #t) (k 0)))) (h "0nhyrcxmfcm0vcrznck0inp6vhc54561rkcfqdz791yvwn7pmz18")))

