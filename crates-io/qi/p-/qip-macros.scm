(define-module (crates-io qi p- qip-macros) #:use-module (crates-io))

(define-public crate-qip-macros-1.0.0-beta.0 (c (n "qip-macros") (v "1.0.0-beta.0") (h "14jfh8rmwizark85kc9cy32ybz4z4ry6bh939nzp694y4b2bwm7g")))

(define-public crate-qip-macros-1.0.0 (c (n "qip-macros") (v "1.0.0") (h "1lhqjsb6i3dj92ksm4mibridn5v6wbhv51pl1v2a45xyamix66pk")))

