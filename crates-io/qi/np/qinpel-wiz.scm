(define-module (crates-io qi np qinpel-wiz) #:use-module (crates-io))

(define-public crate-qinpel-wiz-0.1.0 (c (n "qinpel-wiz") (v "0.1.0") (d (list (d (n "liz") (r "^0.1.5") (d #t) (k 0)) (d (n "octocrab") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7cj7rnfvmh47j1qjd8x0w1ifyydi67id5vazhq0asp1rf61c7n")))

