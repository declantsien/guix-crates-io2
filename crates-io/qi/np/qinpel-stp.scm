(define-module (crates-io qi np qinpel-stp) #:use-module (crates-io))

(define-public crate-qinpel-stp-0.1.0 (c (n "qinpel-stp") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("tokio-native-tls" "blocking" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1xv1ai9w4g4zy1fafp7vba6h0ggyiq9nfynrngypy579gj4ci2b2")))

