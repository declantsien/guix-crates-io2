(define-module (crates-io qi ni qiniu-utils) #:use-module (crates-io))

(define-public crate-qiniu-utils-0.0.5 (c (n "qiniu-utils") (v "0.0.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0db3xv47whg451lg8axhz499y6kx1ix6fbnsa27dyqlnhsw2a7zg") (y #t) (r "1.56.0")))

(define-public crate-qiniu-utils-0.0.6 (c (n "qiniu-utils") (v "0.0.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "01cl4wvffrmbyhlgirjci9cz8cmmqyn033ckc9ijqdjvq0zhdxry") (y #t) (r "1.56.0")))

(define-public crate-qiniu-utils-0.0.7 (c (n "qiniu-utils") (v "0.0.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0alpp7mb0hvyd3m90691515qhsh6d3ps4rc9fp4ls8vx2073js42") (y #t) (r "1.56.0")))

(define-public crate-qiniu-utils-0.0.8 (c (n "qiniu-utils") (v "0.0.8") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0jspm9anwh1cqx0fi08hw46w95rvqhr3rp0qrc2dqg5b2azzp564") (y #t) (r "1.56.0")))

(define-public crate-qiniu-utils-0.0.9 (c (n "qiniu-utils") (v "0.0.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "092mqpa9i0prsp6bg30m1zwwzgjc9bliywpwsiwp9inh0p8rjwiw") (y #t) (r "1.60.0")))

(define-public crate-qiniu-utils-0.1.0 (c (n "qiniu-utils") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0nywyh6bccr6xck0g7bvmqzkdab449grdr33q94j20rc97pfjbkm") (r "1.60.0")))

(define-public crate-qiniu-utils-0.1.1 (c (n "qiniu-utils") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "17gwvylxhn2k5m5vyl7l9a6r7hrxfh1a7ji3axfay01hmhdc1nbz") (r "1.60.0")))

(define-public crate-qiniu-utils-0.1.2 (c (n "qiniu-utils") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "1m6rwjfm9hha4jnscw0b9ysxjn5iq4944afrh1b4yx8h1fldz4sl") (r "1.60.0")))

(define-public crate-qiniu-utils-0.1.3 (c (n "qiniu-utils") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0b4853zwrzd8ph995byz4nv7y5ij79j4qx1nqmg0d6h4qhpvfyvi") (r "1.60.0")))

(define-public crate-qiniu-utils-0.1.4 (c (n "qiniu-utils") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "1sp3k4hpgx2wgdsjj86bsh1bhac7nk5frps4rd2g9zi245xvm52y") (r "1.60.0")))

(define-public crate-qiniu-utils-0.2.0 (c (n "qiniu-utils") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "171518a8klf8m8sj0007jwihman79zhih1z4yf3cz2ki3gp45him") (r "1.60.0")))

(define-public crate-qiniu-utils-0.2.1 (c (n "qiniu-utils") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0j75shrzf7cl5ijkqhr5i8n4r6fpsafz2h0a3aighb409djy8s2c") (r "1.60.0")))

(define-public crate-qiniu-utils-0.2.2 (c (n "qiniu-utils") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "1bi7s8ml2i5d3dhw0kq29mlbkw03hi4ys8wcpw0ls1h167j71lmn") (r "1.60.0")))

(define-public crate-qiniu-utils-0.2.3 (c (n "qiniu-utils") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0k54yrra7b7gy27n2nci8spbwphn9hl1kl5hs0giyi9xnfrq7y8b") (r "1.60.0")))

(define-public crate-qiniu-utils-0.2.4 (c (n "qiniu-utils") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "102gr35v1da64mz5g92w3fzfwyp4f96zp4gaih27aai40vxy15ys") (r "1.70.0")))

