(define-module (crates-io qi ni qiniu-ureq) #:use-module (crates-io))

(define-public crate-qiniu-ureq-0.0.5 (c (n "qiniu-ureq") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0dvqi1icgl7b578zdamxkgnizlkb9ars1hslqdf2cg5ng0hzzk34") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (y #t) (r "1.56.0")))

(define-public crate-qiniu-ureq-0.0.7 (c (n "qiniu-ureq") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1zqqfgh7gdma0g200fdhndz430b8mcmsjkhk7wnsrqygz5nnn7sc") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (y #t) (r "1.56.0")))

(define-public crate-qiniu-ureq-0.0.8 (c (n "qiniu-ureq") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1aqsqlmkha7s7asfhgr7mdcd6knbvx3rxfwfsbh87cxgrqb4wi5b") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (y #t) (r "1.56.0")))

(define-public crate-qiniu-ureq-0.0.9 (c (n "qiniu-ureq") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1hml3xvjw586d1hv8k9nkfxqhhf67c6bbdbz1jwl6xn9sjvsjrjm") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (y #t) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.1.0 (c (n "qiniu-ureq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1i18pq0xnaigjk18f17qs5705lzlgv7ipqcczlfc8ynjk5kkp8dn") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.1.1 (c (n "qiniu-ureq") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1kk8bpskf2j7zfrn0h1cd383iwkb6nqhiq59aga5ksxian5h6m34") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.1.2 (c (n "qiniu-ureq") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "03xsjn1lii7gl7g2syp4jwvvgl88xwmh6q5zk6yrnf2jjknhf5lf") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.1.3 (c (n "qiniu-ureq") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "1lkcl87d9fms7rvxr9m5qipzdzyh0l4zdy08biarb6ainhgjlrak") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.1.4 (c (n "qiniu-ureq") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "02aj2q5rgpyfs9ncrkq4fpi218gdlirlbkwfcckqf3x4b8mdiq2d") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.2.0 (c (n "qiniu-ureq") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0w89b1m01gk4zrig8gi0liaw3j5ri6lmnpina2rxvxnvlasspxn0") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.2.1 (c (n "qiniu-ureq") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0p9wbim3vdrsv1wnhnnx6nwg69x2v7z8cic494jk6m5hi45vp1v8") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.2.2 (c (n "qiniu-ureq") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0rbfgmzyr7rxazr6bgsimvg6n6l8wg9hv1vnk9vzclknswc358pb") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.2.3 (c (n "qiniu-ureq") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0rykpr3yn1hx6wf0sxkribvrv0nmm1b43pf9dzd1wrw52ayh2isn") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.60.0")))

(define-public crate-qiniu-ureq-0.2.4 (c (n "qiniu-ureq") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "md-5") (r "^0.9.1") (d #t) (k 2)) (d (n "qiniu-http") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 2)))) (h "0dlabn6i5ibwfirpvar8wzw475hz9calxq7b0xplb362k2p1w8j7") (f (quote (("tls" "ureq/tls") ("socks-proxy" "ureq/socks-proxy") ("native-certs" "ureq/native-certs") ("docs") ("cookies" "ureq/cookies") ("charset" "ureq/charset") ("async" "qiniu-http/async")))) (r "1.70.0")))

