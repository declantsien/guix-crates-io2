(define-module (crates-io qi ni qiniu-upload-rs) #:use-module (crates-io))

(define-public crate-qiniu-upload-rs-0.0.1 (c (n "qiniu-upload-rs") (v "0.0.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ag02kfxnjd1mnvp43vlisx40z12aq920nccpqdqrcyfgxl0i42x")))

