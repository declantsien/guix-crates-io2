(define-module (crates-io qi ni qiniu_v1) #:use-module (crates-io))

(define-public crate-qiniu_v1-0.0.1 (c (n "qiniu_v1") (v "0.0.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0afig3gwk4g8kshzz2fbfp334bssallj21mgfzv64dqxmyw19rs8") (y #t)))

