(define-module (crates-io qi ni qiniu) #:use-module (crates-io))

(define-public crate-qiniu-0.1.0 (c (n "qiniu") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "064lpm8075lifyc6h54kyg8z6rh1cbkks47p8zmz2npsc848r07h")))

(define-public crate-qiniu-0.1.1 (c (n "qiniu") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0vxqn0k0gk80anmsvmm98jibxlngwlmna4lgsa3dy8090i3nsjd5")))

(define-public crate-qiniu-0.1.2 (c (n "qiniu") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "06zrqdvr05ykr4wiivsfycczdd9m1mcd32zcsw6dsp41myxd21fy")))

