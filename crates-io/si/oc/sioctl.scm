(define-module (crates-io si oc sioctl) #:use-module (crates-io))

(define-public crate-sioctl-0.0.1 (c (n "sioctl") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "sndio-sys") (r "^0.0.1") (d #t) (k 0)))) (h "08hi6ax2ldkcbzcknk3sx5xlykck376sgrk797z648qljs9fihzg")))

(define-public crate-sioctl-0.0.2 (c (n "sioctl") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "sndio-sys") (r "^0.0.1") (d #t) (k 0)))) (h "15ijavg8qim9pf2jpycrf0qdv9zig1dn0lsdx8d0d4zqlhm0wkfi")))

