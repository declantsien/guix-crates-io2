(define-module (crates-io si ra siraph) #:use-module (crates-io))

(define-public crate-siraph-0.1.0 (c (n "siraph") (v "0.1.0") (d (list (d (n "dynvec") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0v3fsxajdpqx0nnp4yvcddrg444v14m8h2x5x47ba1n9y93cj6mx") (f (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

(define-public crate-siraph-0.1.1 (c (n "siraph") (v "0.1.1") (d (list (d (n "dynvec") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "12ky2fpjqk8d54s0474qqjb0flk58qrdir6mgs1mf8cg0jc7l5zv") (f (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

(define-public crate-siraph-0.1.2 (c (n "siraph") (v "0.1.2") (d (list (d (n "dynvec") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1a7vqp56hhd57jnxp6xx9ddmgrzn6brimwwncp96xmxfjm082hvz") (f (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

