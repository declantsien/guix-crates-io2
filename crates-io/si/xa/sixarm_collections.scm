(define-module (crates-io si xa sixarm_collections) #:use-module (crates-io))

(define-public crate-sixarm_collections-1.1.0 (c (n "sixarm_collections") (v "1.1.0") (h "0vp23lhqnd29dx5zbhbm9mmcsn8nmvw94m61z4l6yj91hjh5pp3h")))

(define-public crate-sixarm_collections-1.1.1 (c (n "sixarm_collections") (v "1.1.1") (d (list (d (n "sixarm_assert") (r "^1") (d #t) (k 0)))) (h "0qf9wj70gazidmjrii5r6n8plzd57zp77hag43zybz870agxnhrv")))

