(define-module (crates-io si xa sixarm_assert) #:use-module (crates-io))

(define-public crate-sixarm_assert-1.1.0 (c (n "sixarm_assert") (v "1.1.0") (h "14avs8j9q7a6hfnac4ppm5109fh07m2xv1di5c0395s6vhghh564")))

(define-public crate-sixarm_assert-1.1.1 (c (n "sixarm_assert") (v "1.1.1") (h "0c69z638amm12pkil8bhji4wjy54x01js6z68zf3ka8y27xggg4z")))

