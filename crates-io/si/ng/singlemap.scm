(define-module (crates-io si ng singlemap) #:use-module (crates-io))

(define-public crate-singlemap-0.1.0 (c (n "singlemap") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0y1xm5c1m2x04cxhslldf2m91jblkbwh7db2v22z5kg25ikzj0f4") (y #t)))

(define-public crate-singlemap-0.1.1 (c (n "singlemap") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "1siq4a55s3gz4vs7yvcz9n8ghn4i6gnr8a0wgll1xpc450jdvqk7") (y #t)))

(define-public crate-singlemap-0.1.3 (c (n "singlemap") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1mbd7aaz2v9f74f0478m4kmkbqpkn85xi6d1669zkx4z5p1m9qsa") (y #t)))

(define-public crate-singlemap-0.1.4 (c (n "singlemap") (v "0.1.4") (d (list (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0ggkqymc85iidvfx2fpyhky02jfyncd6xpyka5j0k5if45yjy5qs") (y #t)))

(define-public crate-singlemap-0.1.5 (c (n "singlemap") (v "0.1.5") (d (list (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "1fggwlf7z4dpk83syqzslva0kirir4xx2973sh8vl60a91bh97hp")))

(define-public crate-singlemap-0.1.6 (c (n "singlemap") (v "0.1.6") (d (list (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "1bgdy662l7cm31sq0pq27jkga644fkqwapbqa722yl7l5gv51fhc")))

