(define-module (crates-io si ng singletonthread) #:use-module (crates-io))

(define-public crate-singletonThread-1.0.0 (c (n "singletonThread") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.15.1") (d #t) (k 0)))) (h "09z4iw12igd3llj4w2gjbr3m5c76pkigxxn0fxdyxdb3l7idh18x")))

