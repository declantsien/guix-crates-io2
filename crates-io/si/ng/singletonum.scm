(define-module (crates-io si ng singletonum) #:use-module (crates-io))

(define-public crate-singletonum-0.1.0 (c (n "singletonum") (v "0.1.0") (d (list (d (n "once_cell") (r "^0.1.6") (d #t) (k 0)) (d (n "singletonum-derive") (r "^0.1.0") (d #t) (k 0)))) (h "03zy1260c6myar8lask5dwwf8mk7q69564bnnpmkqwlndkxxpdi7")))

(define-public crate-singletonum-0.2.0 (c (n "singletonum") (v "0.2.0") (d (list (d (n "once_cell") (r "^0.1.6") (d #t) (k 0)) (d (n "singletonum-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0yqcd4y1a2nc9aq72iq6z0vz4y7hr2iy9f6ysmgj0xp4hfnbaxpv")))

