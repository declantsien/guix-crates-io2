(define-module (crates-io si ng singleton-trait) #:use-module (crates-io))

(define-public crate-singleton-trait-0.1.0 (c (n "singleton-trait") (v "0.1.0") (h "04r4nqa3wn9b64zxvd5hhwqccfqag2diy0d8x73pad7crn14x7n9")))

(define-public crate-singleton-trait-0.1.1 (c (n "singleton-trait") (v "0.1.1") (h "1s1q61zg1k1liky9mwwjs4ps97lb4m200yb5kfvx8z709srm4gdb")))

(define-public crate-singleton-trait-0.1.2 (c (n "singleton-trait") (v "0.1.2") (h "0s5f3z86avpi0zqjbbmrv45da9378g5pslg2aln6rqkrgg193vz7")))

(define-public crate-singleton-trait-0.2.0 (c (n "singleton-trait") (v "0.2.0") (h "0bj7md8a818sv4x805v18d87wqf61z26yssxivxawzah6kh6w9vj")))

(define-public crate-singleton-trait-0.3.0 (c (n "singleton-trait") (v "0.3.0") (h "0bqrgml30xadkdfk94rqarn4vl72by7c1gzhdayfgx7hphx94d1x")))

(define-public crate-singleton-trait-0.4.0 (c (n "singleton-trait") (v "0.4.0") (h "1sawyvb3lmqaz35xdc1yzg614dahfsjaw9xgas4nhpq46zh4gqwh")))

