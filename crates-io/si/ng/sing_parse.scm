(define-module (crates-io si ng sing_parse) #:use-module (crates-io))

(define-public crate-sing_parse-0.1.0 (c (n "sing_parse") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sing_util") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06cryfi8jpidwhqkfaqr1yx0lbn3fkav8971sq9jhn6c93sj3xrs")))

(define-public crate-sing_parse-0.1.1 (c (n "sing_parse") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sing_util") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gdkfb50dxgywy2hgzz9g5jbfz2726lbwi0429gpi197s6b15klw")))

(define-public crate-sing_parse-0.1.2 (c (n "sing_parse") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sing_util") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0x91yzvjzfz7pmclwwhypdnf70w909vb772dpkhp4njaxyaxzhb5")))

(define-public crate-sing_parse-0.1.3 (c (n "sing_parse") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sing_util") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "041gjkn2ginhpyb5yb40i0xfvxbww46ay6mbccv6fncag3mfqz0x")))

