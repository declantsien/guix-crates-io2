(define-module (crates-io si ng single-gomoku) #:use-module (crates-io))

(define-public crate-single-gomoku-0.8.3 (c (n "single-gomoku") (v "0.8.3") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "pallet-balances") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 2)) (d (n "sp-io") (r "^2.0.0") (k 2)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "0v731a4knyrydfw319w5g221s3r3a7q0y6sclzkfb7dxkr9z8ysr") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std" "pallet-balances/std") ("default" "std"))))))

