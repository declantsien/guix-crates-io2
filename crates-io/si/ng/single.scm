(define-module (crates-io si ng single) #:use-module (crates-io))

(define-public crate-single-0.1.0 (c (n "single") (v "0.1.0") (h "1zc28a84yd8jmrn261awvxcqbdqbnr03fsr0d9gjrdkj6h3bd7d7")))

(define-public crate-single-0.2.0 (c (n "single") (v "0.2.0") (h "0md3ywfjq72b3k02mszyz92dj83mwsr0rz36l7vyblg1hq7qhv0n")))

(define-public crate-single-0.2.1 (c (n "single") (v "0.2.1") (d (list (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "040rck819pbbdjzdrisp4sa5pi1lv3vmj92p582swjxvgk7i74yv")))

(define-public crate-single-0.3.0 (c (n "single") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)))) (h "1gqmr8npglz9819w1p1y3gmb130ynh4ymr4xlkayzxmpsziwdkjx") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-single-1.0.0 (c (n "single") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)))) (h "18ly6qq1z6b57j0wb3g0cfsi0lyg741vb8cian28kdhs59rxsnmx") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-single-1.0.1 (c (n "single") (v "1.0.1") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)))) (h "0fsl54fhl7aiy2jgh99ii5gawvn7vb0dqw82gv1yx1mlhnv5pd4x") (f (quote (("std" "failure/std") ("default" "std"))))))

