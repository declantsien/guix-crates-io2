(define-module (crates-io si ng singly) #:use-module (crates-io))

(define-public crate-singly-0.1.0 (c (n "singly") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1j3mbli99svjrfw0ik3al7zg45zcpv0vfydd6f7jn22rd97952ff") (y #t)))

(define-public crate-singly-0.1.1 (c (n "singly") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08gwi27c3g3skglfrmkmjhqiify0gb58087jfsvqra84rss0xfv0")))

