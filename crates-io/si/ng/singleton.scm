(define-module (crates-io si ng singleton) #:use-module (crates-io))

(define-public crate-singleton-0.1.0 (c (n "singleton") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z9x081v5f9fk6i8ns2p5yh1kq8hkw70296wiwd688gxsx3fvh94")))

(define-public crate-singleton-0.1.1 (c (n "singleton") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "041c3dxkaywlsq3yazspbpmbd5pxzh4n9w0p9s1qhhfbf1fvy9gh")))

(define-public crate-singleton-0.2.0 (c (n "singleton") (v "0.2.0") (h "0lygznz4id6lapj6cb52jdchc2ahidvf49rsn6ny2iyk3n1c31ii") (f (quote (("const_fn"))))))

(define-public crate-singleton-0.2.1 (c (n "singleton") (v "0.2.1") (h "1a66fyjx991mpm29jbnapmsm40p15w1m37wx1mjlpajhg2f5s8m4") (f (quote (("const_fn"))))))

(define-public crate-singleton-0.2.2 (c (n "singleton") (v "0.2.2") (h "1xiyrkhyq33wq14wncs4i6hg125wq3pz3bwvbgqlkmqxmpl6253g") (f (quote (("const_fn"))))))

