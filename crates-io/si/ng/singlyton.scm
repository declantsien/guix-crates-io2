(define-module (crates-io si ng singlyton) #:use-module (crates-io))

(define-public crate-singlyton-1.0.0 (c (n "singlyton") (v "1.0.0") (h "0n8jsy6fjr3w0g2l4r84i2m522kx3hjpg7h0llg63im7i89csq6j") (f (quote (("std") ("default" "std"))))))

(define-public crate-singlyton-1.1.0 (c (n "singlyton") (v "1.1.0") (h "1nrvrdd2nrcwz4klmfyrk1zsb5d6pwb2lxp17ay8jqy6rk0w4nxw") (f (quote (("std") ("default" "std"))))))

(define-public crate-singlyton-1.2.0 (c (n "singlyton") (v "1.2.0") (h "1wc5phnrz0fz4mll172ybvs5w4p5bwgdx9m9d8g7bvli46k86yi1") (f (quote (("std") ("default" "std"))))))

(define-public crate-singlyton-1.2.1 (c (n "singlyton") (v "1.2.1") (h "090y0q4sclgzsxdm34x2haqci1iqj88b6jh6ivkvqyrxrckag4h1") (f (quote (("std") ("default" "std"))))))

(define-public crate-singlyton-2.0.0 (c (n "singlyton") (v "2.0.0") (d (list (d (n "atomic_refcell") (r "^0.1.7") (d #t) (k 0)))) (h "1h9wpybdd68mpg5p5jhmdbml5wkd5hj48vs23990ldy6bqsrinq4")))

(define-public crate-singlyton-3.0.0 (c (n "singlyton") (v "3.0.0") (d (list (d (n "atomic_refcell") (r "^0.1.7") (d #t) (k 0)))) (h "0cx6pxncxgpm16vf3falzx591a60y6n03jrsiy9qrxyb0w8vkjv2")))

(define-public crate-singlyton-4.0.0 (c (n "singlyton") (v "4.0.0") (d (list (d (n "atomic_refcell") (r "^0.1.7") (d #t) (k 0)))) (h "1r7xwdqj7hggd9bq24cb5s67ayjd8kp9sf3bzmpb5l4y7j60mpli")))

(define-public crate-singlyton-4.1.0 (c (n "singlyton") (v "4.1.0") (d (list (d (n "atomic_refcell") (r "^0.1.7") (d #t) (k 0)))) (h "171dnd3p8lnpa4r54n2kipp2k46bs1flyszlx0lrfanb7nyxk983")))

(define-public crate-singlyton-4.1.1 (c (n "singlyton") (v "4.1.1") (d (list (d (n "atomic_refcell") (r "^0.1.7") (d #t) (k 0)))) (h "1v1mvdk25zn6x5xgs6cw1lvi7l1y5wjiwh78z4cy5wi4n7fbw86r")))

