(define-module (crates-io si ng single-use-dns) #:use-module (crates-io))

(define-public crate-single-use-dns-0.1.0 (c (n "single-use-dns") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (d #t) (k 0)) (d (n "trust-dns-client") (r "^0.19.3") (d #t) (k 0)) (d (n "trust-dns-server") (r "^0.19.3") (d #t) (k 0)))) (h "1mw6dapkm27wkq314dx9cbxiwmsr0fd1m43yqiginah5i6l8m27s")))

