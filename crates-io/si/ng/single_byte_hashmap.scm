(define-module (crates-io si ng single_byte_hashmap) #:use-module (crates-io))

(define-public crate-single_byte_hashmap-0.1.0 (c (n "single_byte_hashmap") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("inline-more"))) (o #t) (k 0)))) (h "0k72py37drkvcxy8k2pr3fdb1nxxicwm6r6izc2brp9dyhq7ab14") (f (quote (("serde" "faster_hashmap" "hashbrown/serde") ("rayon" "faster_hashmap" "hashbrown/rayon") ("nightly" "faster_hashmap" "hashbrown/nightly") ("faster_hashmap" "hashbrown") ("default" "faster_hashmap"))))))

(define-public crate-single_byte_hashmap-0.1.1 (c (n "single_byte_hashmap") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("inline-more"))) (o #t) (k 0)))) (h "1ny46ds6jhnrgl6x3bfcbawb6rh3p4w2vhsysv9x52a07wsibrdn") (f (quote (("serde" "faster_hashmap" "hashbrown/serde") ("rayon" "faster_hashmap" "hashbrown/rayon") ("nightly" "faster_hashmap" "hashbrown/nightly") ("faster_hashmap" "hashbrown") ("default" "faster_hashmap"))))))

(define-public crate-single_byte_hashmap-0.1.2 (c (n "single_byte_hashmap") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("inline-more"))) (o #t) (k 0)))) (h "0lbm08m9dg38f1m4hlc4p77r9pmpfnnxv3n5kna6ym2600mmrbqc") (f (quote (("serde" "faster_hashmap" "hashbrown/serde") ("rayon" "faster_hashmap" "hashbrown/rayon") ("nightly" "faster_hashmap" "hashbrown/nightly") ("faster_hashmap" "hashbrown") ("default" "faster_hashmap") ("allow_unsafe_sizes"))))))

