(define-module (crates-io si ng single_executor) #:use-module (crates-io))

(define-public crate-single_executor-0.1.0 (c (n "single_executor") (v "0.1.0") (d (list (d (n "atomic_swapping") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "concurrency_traits") (r "^0.5.1") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.1") (f (quote ("alloc"))) (k 0)))) (h "1a19n0lflxda681f6gqz7ivjy3404n59p6grrsnllr7gv47cn1sb") (f (quote (("std" "concurrency_traits/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.1.1 (c (n "single_executor") (v "0.1.1") (d (list (d (n "atomic_swapping") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "concurrency_traits") (r "^0.5.1") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.1") (f (quote ("alloc"))) (k 0)))) (h "0v5b6a710m93s3szwk9wvf5xdccz63rdx56dadsqg4p141m9a0rf") (f (quote (("std" "concurrency_traits/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.0 (c (n "single_executor") (v "0.3.0") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "13m7s3268f4flsadhkr9dw4bljcqa9ipkyargrjfcbxvbwcavj4d") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.1 (c (n "single_executor") (v "0.3.1") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "0znh25rrclj7vwc5567k592v6n9wd3vdi3drf5r9lx4fxbmsphkv") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.2 (c (n "single_executor") (v "0.3.2") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "1afs7fdrhmv94qvv94mpv7hpjb30zq29124db04112fc8k0xnyp6") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.3 (c (n "single_executor") (v "0.3.3") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "0r4wmqzdvbms0zk1rrkpnlvjxm3s3sj5609a085h3s4cgjl8aj05") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.4 (c (n "single_executor") (v "0.3.4") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "0kq6a51k7lfys89ippijzhmd3y0s07bmda6bmrlmx9r9p9kazdwz") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.3.5 (c (n "single_executor") (v "0.3.5") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "1c86jnzc00j3hcvsbmp9a2x0sa0y37z3140i4hqi0a4fmbx3vnlk") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

(define-public crate-single_executor-0.4.0 (c (n "single_executor") (v "0.4.0") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "0461cp8gfjpgwvr92gqyfp6y3syz5jsqdx3c7jj73lb9x98rzm8j") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std")))) (y #t)))

(define-public crate-single_executor-0.4.1 (c (n "single_executor") (v "0.4.1") (d (list (d (n "concurrency_traits") (r "^0.7.0") (f (quote ("alloc"))) (k 0)) (d (n "simple_futures") (r "^0.1.2") (f (quote ("alloc"))) (k 0)))) (h "19crsh33a00syb6j46sw16mbb1c6rghb0n50afnd9gl38qxh48y2") (f (quote (("std" "concurrency_traits/std" "simple_futures/std") ("nightly" "concurrency_traits/nightly") ("default" "std"))))))

