(define-module (crates-io si ng singer) #:use-module (crates-io))

(define-public crate-singer-0.1.0 (c (n "singer") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03dl88li3wp0fvb8q5p1n03058xmaasjhwp21pryhxf0zc99nx9h")))

(define-public crate-singer-0.1.1 (c (n "singer") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gi75qkfaiksf25h66irl632nziizg0025d449sl0bdv44wn6irn") (y #t)))

(define-public crate-singer-0.1.2 (c (n "singer") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0db55hg7lv8n8vf53kyf5nvkpfagpgnmx3sfbb4zh115yhcfhyj2")))

(define-public crate-singer-0.2.0 (c (n "singer") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nv7b9z8bi1hgjbhwbp1diw82d14288bj1s6p7lbbwacsy01xa55")))

(define-public crate-singer-0.3.0 (c (n "singer") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w2513qclpx37di4ycjzrxkak0islshc4xik3rbjsnqwlryj9jg4")))

