(define-module (crates-io si ng singleton-manager) #:use-module (crates-io))

(define-public crate-singleton-manager-0.1.0 (c (n "singleton-manager") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ly6i8zgmvpf63659czm7jfby4vwjyd167cxkv65s4fckrr77q3j") (y #t)))

(define-public crate-singleton-manager-0.1.1 (c (n "singleton-manager") (v "0.1.1") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pyvfipbhsvi7r2k303jhk0bwrn5cbr0nmdz3ignnnf686a6mxcg")))

(define-public crate-singleton-manager-0.1.2 (c (n "singleton-manager") (v "0.1.2") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1295nri5bsz4y8aga7mxv9xym5x5d011bjxip54jxiabapa3qvvk")))

(define-public crate-singleton-manager-0.1.3 (c (n "singleton-manager") (v "0.1.3") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0fscihk942g9zp85n1vaafr1xba75pgjlv5i11bsznpz75v216vx")))

(define-public crate-singleton-manager-0.1.4 (c (n "singleton-manager") (v "0.1.4") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "14li5rljzwdcxvf5d6wcx5xig3mfy26dsbqdxz7x04lbs4dwdxa2")))

