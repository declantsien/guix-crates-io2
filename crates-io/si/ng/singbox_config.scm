(define-module (crates-io si ng singbox_config) #:use-module (crates-io))

(define-public crate-singbox_config-0.17.0 (c (n "singbox_config") (v "0.17.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yks621l58qz6a8hbz9sk9fw5q74mqww80sb5pglw4nayi7lkjkm")))

