(define-module (crates-io si ng singleflight) #:use-module (crates-io))

(define-public crate-singleflight-0.1.0 (c (n "singleflight") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1avqfyrq3zhdxf4v37l4h6hpi5s3d5qz7d56cd8fajq550sf469s")))

(define-public crate-singleflight-0.2.0 (c (n "singleflight") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "06cg97skqwkl06dcghfad11c6wprnw93vglw2jkxs8awaqwdcjcg")))

(define-public crate-singleflight-0.3.0 (c (n "singleflight") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0kg2dxda6849nlxg4638jq648mbrb1r5vshchvwq3i375sn3b5mw")))

