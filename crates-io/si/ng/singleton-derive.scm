(define-module (crates-io si ng singleton-derive) #:use-module (crates-io))

(define-public crate-singleton-derive-0.1.0 (c (n "singleton-derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1axaykbzqm102mlvm94s7q6c3d825cpdy1gbnlxdm8gww2cizk53")))

