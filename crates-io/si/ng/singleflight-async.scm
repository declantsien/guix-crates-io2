(define-module (crates-io si ng singleflight-async) #:use-module (crates-io))

(define-public crate-singleflight-async-0.1.0 (c (n "singleflight-async") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "sync" "time" "parking_lot"))) (d #t) (k 2)))) (h "15x2d6jhqm4k7m35g1j6xzaipf8gk0q45a9pb2pvz13c8h2qqa7f") (f (quote (("hardware-lock-elision" "parking_lot/hardware-lock-elision") ("default"))))))

(define-public crate-singleflight-async-0.1.1 (c (n "singleflight-async") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "sync" "time" "parking_lot"))) (d #t) (k 2)))) (h "0qd8is200yx32x4w4kfvs55lybnn4009rqhipm2ig97g9rfy10va") (f (quote (("hardware-lock-elision" "parking_lot/hardware-lock-elision") ("default"))))))

