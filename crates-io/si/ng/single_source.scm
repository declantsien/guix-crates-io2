(define-module (crates-io si ng single_source) #:use-module (crates-io))

(define-public crate-single_source-0.1.0 (c (n "single_source") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1097drw48m4vx5zknng5ndfih753yf2cahkcc1al4mfwwla5zaqc")))

(define-public crate-single_source-0.1.1 (c (n "single_source") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0nxa1f6ki5rv4y4irqv4nxmf3v1rkfcvdpf7awalfb305mj26d35")))

(define-public crate-single_source-0.1.2 (c (n "single_source") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "18dsyhh356rsb68spb2mb12izcjj9xg68c70r5a09035lc03j5ss")))

(define-public crate-single_source-0.1.3 (c (n "single_source") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1cl4ycv78k2z2sqn23yxqwf7zjasyzvrpqqhbpcglff3rgd03678")))

(define-public crate-single_source-0.1.4 (c (n "single_source") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1wng176cvjpixiybv2dpg7xpdclhj2j7yxxvbavf8gjdi384c6ai")))

(define-public crate-single_source-0.1.5 (c (n "single_source") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1v97zb8yd7kgk4zwmqyp40g5l5m6bvfb3xzljszmcm8xx231h7fi")))

