(define-module (crates-io si ng single-trait-impl) #:use-module (crates-io))

(define-public crate-single-trait-impl-0.1.0 (c (n "single-trait-impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xi8j3v4w2c51r34rpp34xnry1fq6qqdvkn8z2y5h82kzml5h5ni")))

