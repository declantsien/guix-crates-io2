(define-module (crates-io si ng single_use_seals) #:use-module (crates-io))

(define-public crate-single_use_seals-0.5.0 (c (n "single_use_seals") (v "0.5.0") (d (list (d (n "amplify_derive") (r "^2.7") (d #t) (k 0)))) (h "0n3aqs25kbybwkh70i5f17ypaa9gcsw4sxf7mqq4lr2d6rymz3pl")))

(define-public crate-single_use_seals-0.5.1 (c (n "single_use_seals") (v "0.5.1") (d (list (d (n "amplify_derive") (r "^2.7") (d #t) (k 0)))) (h "1sfa7lh3dx4v50d7d2s4djgpd4zwbgcxq4a1syx9kkkdqc4cgq0j") (f (quote (("default") ("all"))))))

(define-public crate-single_use_seals-0.5.2 (c (n "single_use_seals") (v "0.5.2") (d (list (d (n "amplify_derive") (r "^2.7.2") (d #t) (k 0)))) (h "0nh9aw2qkdjib02nj8bzzvy6nnd7j2syldqa8l6qyjcl58gf64yf") (f (quote (("default") ("all"))))))

(define-public crate-single_use_seals-0.5.3 (c (n "single_use_seals") (v "0.5.3") (d (list (d (n "amplify_derive") (r "^2.8") (d #t) (k 0)))) (h "1vwyd9yjzs4j63i66gxflk8nyf5pdlbz9dm28sqp1xd6r3hx6h6w") (f (quote (("default") ("all"))))))

(define-public crate-single_use_seals-0.5.4 (c (n "single_use_seals") (v "0.5.4") (d (list (d (n "amplify_derive") (r "^2.8") (d #t) (k 0)))) (h "1wpikhdnlnp54p487vsg7wz9blxb3n8a29sr4f9nj55qyrh6j1zn") (f (quote (("default") ("all"))))))

(define-public crate-single_use_seals-0.5.5 (c (n "single_use_seals") (v "0.5.5") (d (list (d (n "amplify_derive") (r "^2.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "0ibz26d9pdv96z41r7si3i30kx6lcm872pz0yi4s1ddqxj876fmi") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.6.0 (c (n "single_use_seals") (v "0.6.0") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "1lkz4778l0y4jjbjcnkfnz5jjrbibnsqwvrqpdaa70gm5hi9xwal") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.7.0-alpha.1 (c (n "single_use_seals") (v "0.7.0-alpha.1") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "1igi9lsvlsiz05rvqsy94lyrdfksx7ychs305yvnjcaydf3fb6y1") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.6.1 (c (n "single_use_seals") (v "0.6.1") (d (list (d (n "amplify_derive") (r "^2.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "14k27awrmdb836cb4h91zf40n844an6pcr7wxc669k5a4n9mdjqb") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.7.0-beta.1 (c (n "single_use_seals") (v "0.7.0-beta.1") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "1bydhpi1l5jv6yii6hyby0x2w1sj61igc50yqwd7mzrk684r109n") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.7.0-beta.2 (c (n "single_use_seals") (v "0.7.0-beta.2") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "1h1k8cnvmbbf2ba0xxpg62vmhgm2ljnys0hhhz53yshaxhzw5rm7") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.7.0-rc.1 (c (n "single_use_seals") (v "0.7.0-rc.1") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "0vwchf6za8wy18wj15ym1whjbnrx0k4vpky3givfcjf39hyc02gs") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.7.0 (c (n "single_use_seals") (v "0.7.0") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "09h31ibdnyw1v7z1lp1ys44fd7hv1bcdyr8rw7zsb0072fq3npp1") (f (quote (("default") ("async" "async-trait") ("all" "async"))))))

(define-public crate-single_use_seals-0.8.0 (c (n "single_use_seals") (v "0.8.0") (d (list (d (n "amplify_derive") (r "^2.9") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)))) (h "1lmhiljapykmq8la3kpkkxdsp5hpyc53dhqn232dm61303asaxfn") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.59.0")))

(define-public crate-single_use_seals-0.9.0-alpha.1 (c (n "single_use_seals") (v "0.9.0-alpha.1") (d (list (d (n "amplify_derive") (r "^2.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "010f99xq9xcsh7disyvrrlprhr1k82idgh448bv87qy610fncgy9") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.59.0")))

(define-public crate-single_use_seals-0.9.0-rc.1 (c (n "single_use_seals") (v "0.9.0-rc.1") (d (list (d (n "amplify_derive") (r "^2.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "0d37m4k22n4kwiq8zvwfrywbyyid5qw9fa2srb5q7gjg2h65hbfc") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.59.0")))

(define-public crate-single_use_seals-0.9.0 (c (n "single_use_seals") (v "0.9.0") (d (list (d (n "amplify_derive") (r "^2.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "03psbgx1yfyy7ny235shpnhha3nb18ld1mvwqrp831hl87az34p0") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.59.0")))

(define-public crate-single_use_seals-0.10.0-beta.1 (c (n "single_use_seals") (v "0.10.0-beta.1") (d (list (d (n "amplify_derive") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)))) (h "1mwffk5h9hr5763hxd27dwb9ld37mb2akaaxsdl7079y4ibaxb10") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.66.0")))

(define-public crate-single_use_seals-0.10.0-beta.2 (c (n "single_use_seals") (v "0.10.0-beta.2") (d (list (d (n "amplify_derive") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)))) (h "0jhvkagi4zx19h6lga7vykbyq3j06v42sgaw7ck2i0dr2wa6b9rf") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.66")))

(define-public crate-single_use_seals-0.10.0-rc.1 (c (n "single_use_seals") (v "0.10.0-rc.1") (d (list (d (n "amplify_derive") (r "^4.0.0-alpha.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)))) (h "0rgr37bjlvwvc96a4sfqrqh1j0p7azf5kzphxjnf7jfcwkzdqqnp") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.66")))

(define-public crate-single_use_seals-0.10.0 (c (n "single_use_seals") (v "0.10.0") (d (list (d (n "amplify_derive") (r "^4.0.0-alpha.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)))) (h "1qmyndrmwjw1xlihik87a6bdwvhcg6mlf2m7fjbckkv8db5zgrvs") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.66")))

(define-public crate-single_use_seals-0.10.1 (c (n "single_use_seals") (v "0.10.1") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "15vi3kngynmb1kwyrpib6i2pm671zqyrsmzp5h6s3z4pnns5axpd") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.67")))

(define-public crate-single_use_seals-0.11.0-beta.1 (c (n "single_use_seals") (v "0.11.0-beta.1") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "19gm934pmpn12bnrnv1c5x9pfwa3miaql12gp7khdp2hjhl0ysx4") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.67")))

(define-public crate-single_use_seals-0.11.0-beta.2 (c (n "single_use_seals") (v "0.11.0-beta.2") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "1zp35pdpbn7y9k42gs24cyw36rdignpdbp61lxfc8h966jhlf1n3") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.67")))

(define-public crate-single_use_seals-0.11.0-beta.3 (c (n "single_use_seals") (v "0.11.0-beta.3") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "02pyby7xw9dh9nd81yvx3xdq6vclkyscxwm61010f4hypvkfn50v") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.67")))

(define-public crate-single_use_seals-0.11.0-beta.4 (c (n "single_use_seals") (v "0.11.0-beta.4") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "0ms950vkhxg5dy1n2nzsmv5dyhgwy9qarmmcng99a4q342y2wf48") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.69")))

(define-public crate-single_use_seals-0.11.0-beta.5 (c (n "single_use_seals") (v "0.11.0-beta.5") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)))) (h "0nh328ccy05z28f4ahl5aq61fbkbwa3af4vrpfahhadmxa8xm4w8") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.69")))

(define-public crate-single_use_seals-0.11.0-beta.6 (c (n "single_use_seals") (v "0.11.0-beta.6") (d (list (d (n "amplify_derive") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.80") (o #t) (d #t) (k 0)))) (h "0xqv8if33yhrrns4p80iapa3ayjj1gd1yy66zs4y4bk41lx7ndjm") (f (quote (("default") ("async" "async-trait") ("all" "async")))) (r "1.70.0")))

