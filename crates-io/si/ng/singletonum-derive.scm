(define-module (crates-io si ng singletonum-derive) #:use-module (crates-io))

(define-public crate-singletonum-derive-0.1.0 (c (n "singletonum-derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "13ghkx9ar1wrm6zm3b9xn02b1qz71c22jkqv57mkfqkaar7xvzcg")))

(define-public crate-singletonum-derive-0.2.0 (c (n "singletonum-derive") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0cwb01909f1pwcd8f0mp1kw88788n6bpp0lfqlagzcvl253bshf1")))

