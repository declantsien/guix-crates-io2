(define-module (crates-io si ng sing_util) #:use-module (crates-io))

(define-public crate-sing_util-0.1.0 (c (n "sing_util") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06043hkf4h1yz4z0648r7ywdzwi7i2hdh7qxisvr6ljjxf7zy5p6")))

(define-public crate-sing_util-0.1.1 (c (n "sing_util") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13s2x8l8wav2bw4312bh5jl20wbg2hxchcv4lrxfpig4jz2znc41")))

(define-public crate-sing_util-0.1.2 (c (n "sing_util") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nbzspf6xfza02q09x5ildis63krbrriz9kklyclvra09aigni6j")))

(define-public crate-sing_util-0.1.3 (c (n "sing_util") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l286a5sc84m2z7f3hhs6nzbfhcwm6dmr2p18r54djii6ry1py9h")))

