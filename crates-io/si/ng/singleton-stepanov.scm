(define-module (crates-io si ng singleton-stepanov) #:use-module (crates-io))

(define-public crate-singleton-stepanov-0.1.0 (c (n "singleton-stepanov") (v "0.1.0") (h "1ck15s5ppnm10j8bnrlms3ix557sm0w13bdgafzxbjn2ysp4x587")))

(define-public crate-singleton-stepanov-0.1.1 (c (n "singleton-stepanov") (v "0.1.1") (h "0p8nnciwkkgr4gnj703aslc92phfabgcjsq2r7pg5vjrcp5zx298")))

(define-public crate-singleton-stepanov-0.2.0 (c (n "singleton-stepanov") (v "0.2.0") (h "1nc48idn7hcda8n113x4h54wi73zr04p72r12zvxwy81y20y0vxv")))

(define-public crate-singleton-stepanov-0.3.0 (c (n "singleton-stepanov") (v "0.3.0") (h "06mvvnz43qxkxgws718imy8ql2x6k1a14dip6abphx04950mxdix")))

(define-public crate-singleton-stepanov-1.0.0 (c (n "singleton-stepanov") (v "1.0.0") (h "1frxzvik8jw04nx0fnpbi9z3lg7pg8xhckrxkjj4qxrgap4pmjkh")))

(define-public crate-singleton-stepanov-1.0.1 (c (n "singleton-stepanov") (v "1.0.1") (h "04f9kdmv056crkj35nc2mgfj738mkhsnkr22gn9prswyx1jwzybp")))

(define-public crate-singleton-stepanov-1.1.0 (c (n "singleton-stepanov") (v "1.1.0") (h "14l70b0hrp4rjy872afjz8p95vkv17f9w7f57lsxzkbr6b54mpv6")))

