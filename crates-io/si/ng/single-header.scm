(define-module (crates-io si ng single-header) #:use-module (crates-io))

(define-public crate-single-header-0.1.0 (c (n "single-header") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 1)))) (h "065v6ly3wdgxdf04vjd50rvpabix9i8zgf22wyldv5rzj7dpsm3p")))

(define-public crate-single-header-0.1.1 (c (n "single-header") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 1)))) (h "1c9bwxbs3a5xxbcif3x5wjs8xk086mqfnmknfc70szdmryrkc8hh")))

(define-public crate-single-header-0.1.2 (c (n "single-header") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 1)))) (h "1bpvl16kiqpqw885b33fhw2s8579ya494h89zc2i1z34rs6gqxxv")))

(define-public crate-single-header-0.1.3 (c (n "single-header") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 1)))) (h "0zasgfarby65i45652g71vhwss1d3q5s0fsgznd75dv7g8iw4x1r")))

(define-public crate-single-header-0.1.4 (c (n "single-header") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 1)))) (h "13m9x440bx1xf5n7k02xi6gsr2g7jf4cii5vn0f8d5kacl56h2wx")))

(define-public crate-single-header-0.1.5 (c (n "single-header") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0qvj8a5d7lqqdfycjc89fz7l7ln9dagrcfpaln523ps8ppldzy01")))

(define-public crate-single-header-0.1.6 (c (n "single-header") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0m7mxy0rk952dmbyzqba4vrrwv0i7yz3vvmrmg3bqsskyy5fxaxl")))

(define-public crate-single-header-0.1.7 (c (n "single-header") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "1d2salirwrlfvy4byjamb9g67aic6yfyvn1wfj9vdjsi4id12qr3")))

(define-public crate-single-header-0.1.8 (c (n "single-header") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0m77h16dx07myhidhgdrg6r03r0ym8hgw2a2ff3nys3p7akhwxx4")))

(define-public crate-single-header-0.1.9 (c (n "single-header") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0gsmwnyldmz78g1gx1q3yjf091zim32smfsp28vrswl9n8wbd6vx")))

(define-public crate-single-header-0.2.0 (c (n "single-header") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "13lb40srp0gwbm09mdpk1lclx5c2by29ar34grmjb2hglxkfdjbd")))

(define-public crate-single-header-0.2.1 (c (n "single-header") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "012qa1hqgrg2yq4jfjcg2fdgjfg4w42fbdvzmn2kzrbggaac2c9l")))

