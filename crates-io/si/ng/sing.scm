(define-module (crates-io si ng sing) #:use-module (crates-io))

(define-public crate-sing-0.1.0 (c (n "sing") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rodio") (r "^0.10") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)))) (h "0q7q83mfh0awrjg1jyg0zkb6zch24wfiyp2sxy3ydx8ym7fr70mg")))

(define-public crate-sing-0.1.1 (c (n "sing") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rodio") (r "^0.10") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)))) (h "0zsr45v80025wr9xai0xzxslq7fq99aijbxq0bs9yb3h7nv4lmvl")))

