(define-module (crates-io si lt silt-lua) #:use-module (crates-io))

(define-public crate-silt-lua-0.1.0 (c (n "silt-lua") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1x29gm6p3vkjd5a10kb14p3bzcg8qwxj5hk45v66vqgshgpv3776") (f (quote (("under-number") ("silt" "bang" "under-number" "global" "implicit-return" "short-declare") ("short-declare") ("implicit-return") ("global") ("dev-out") ("default") ("bang"))))))

(define-public crate-silt-lua-0.1.1 (c (n "silt-lua") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1imxa85j78xx4m3q95yyri775fxwpagn8kaqw48qkjxm5mpyyw1f") (f (quote (("under-number") ("silt" "bang" "under-number" "global" "implicit-return" "short-declare") ("short-declare") ("implicit-return") ("global") ("dev-out") ("default") ("bang"))))))

