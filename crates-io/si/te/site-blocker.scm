(define-module (crates-io si te site-blocker) #:use-module (crates-io))

(define-public crate-site-blocker-0.1.0 (c (n "site-blocker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1qkfz8gb4dfzlr1zgz42yv87zk9h9ykg76n5kdmmjl4d7njxhrsv")))

(define-public crate-site-blocker-0.1.1 (c (n "site-blocker") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0r6isqwvd239wsspdlwb052qgg8cjpa3nw13sg0cv9k18i4nn9im")))

(define-public crate-site-blocker-0.1.2 (c (n "site-blocker") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0jn1zjiji0rc1y7z82kcikln44kz07rv2p8ys5wgh2fh9ignifhv")))

