(define-module (crates-io si te sitemaps-cli) #:use-module (crates-io))

(define-public crate-sitemaps-cli-0.1.0 (c (n "sitemaps-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sitemaps") (r "^0.1.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.4.0") (d #t) (k 0)))) (h "1v6gv514bnzzcqllzxq0i8d20qb4cn1r2b5k5b1qkd1r00kqa7jl") (r "1.61.0")))

