(define-module (crates-io si te sitewriter) #:use-module (crates-io))

(define-public crate-sitewriter-0.1.0 (c (n "sitewriter") (v "0.1.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "07f9ff0767ki1v7ly2vvc7siy7zq9d92fav7vp7vjiyj60pk2pwn") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.1.1 (c (n "sitewriter") (v "0.1.1") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "0xj56rql76dhfrr9wk6gw3a3pirkv5sh7kjgqyxc7syg99a599l1") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.2.0 (c (n "sitewriter") (v "0.2.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "1ixslp6izijxsdkyw2j8c8cinzrn6ss0sfjlvlsr48jrr05459nz") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.2.1 (c (n "sitewriter") (v "0.2.1") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "11ipp1bb8maciqhym7bjkmsrw2bcdxms5f88fww1fg3fk29z3cmy") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.2.2 (c (n "sitewriter") (v "0.2.2") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "0xl4jhiwchw5f9d1mz3ipgisrhgssjdmfik7g7vib9djpqr01pgc") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.2.3 (c (n "sitewriter") (v "0.2.3") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "0pvhzsh3x6g888amq299k8zpk0sw7psh322g9apx4c1r2yv42vv6") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.2.4 (c (n "sitewriter") (v "0.2.4") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "0711bgl8xmsycmmdr2wk8m60v9bg8lx4vz0k07lbkfa8i3ss9s11") (f (quote (("default" "chrono"))))))

(define-public crate-sitewriter-0.3.0 (c (n "sitewriter") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "1v7girvi5vrv17bri22ajyfqw3kbb9hz16zqig0zj7mllg5w1vjq")))

(define-public crate-sitewriter-0.3.1 (c (n "sitewriter") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "1bw5xzdxy3qxx61j9iqhmq9kch7kkmwv29kfqs6x577vilc4310p")))

(define-public crate-sitewriter-0.3.2 (c (n "sitewriter") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quick-xml") (r "^0.21.0") (d #t) (k 0)))) (h "08qlmh4p4wpf6wcp2r76y73lkxqngv356594c2nbixssxlm1y7xq")))

(define-public crate-sitewriter-0.4.0 (c (n "sitewriter") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1qrbc8mdw0wm4lr75p7p913cn1srr00dgccdyaak8hhmlxji65hp")))

(define-public crate-sitewriter-0.4.1 (c (n "sitewriter") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "186w49m297kc56lij4a3x73ja41jvc44rz2kb6gx259kn11962bf")))

(define-public crate-sitewriter-0.4.2 (c (n "sitewriter") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "12kny9mazlixya6f4iwc1aijyk44fnfa4xaivfhvx57f8g5ga0j2")))

(define-public crate-sitewriter-0.5.0 (c (n "sitewriter") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0b8gkxplwr18ny7kicjm4cf7p3n3dvz7ikmpn6pac9nl44rlylvv")))

(define-public crate-sitewriter-0.5.1 (c (n "sitewriter") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0zi292hpsgw7djpnvwdqnj9fsl46yihpfinsfr07rf149zkhrajh")))

(define-public crate-sitewriter-0.5.2 (c (n "sitewriter") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.11.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "012fpq5j2w3iql3r27c1qy3hi3njj2kgrlgmqci7dhyxp7aj3ipc")))

(define-public crate-sitewriter-0.5.3 (c (n "sitewriter") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.24.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0mdyqplsvy9iimwa5qii8gx3ahw6bs4wngwd0mpl1zc9i6s7bphs")))

(define-public crate-sitewriter-0.5.4 (c (n "sitewriter") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1jsckpw9zr4vb9xz4mirnqy6s3cpid33nhpf55jzpa2444mnmd4f")))

(define-public crate-sitewriter-0.5.5 (c (n "sitewriter") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "00hcc3hsgraj1vl23fxldjq91nlan19lwkz6pxyrfdzva5jvl5gp")))

(define-public crate-sitewriter-1.0.0 (c (n "sitewriter") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1qjrjf4k3pa03r6gfpm1xhkhk341glcdx692mam6va8v62q68mrz")))

(define-public crate-sitewriter-1.0.1 (c (n "sitewriter") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0k1min5bpyy68cl7xbyb3sxfp06jpgigmm9jbyhca8jgppj2zsdb")))

(define-public crate-sitewriter-1.0.2 (c (n "sitewriter") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("std" "clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29.0") (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0lp2zfrmlpfxd14zjjzb9837i6hrr7f6gai9m40sl9f08zc54i0g")))

(define-public crate-sitewriter-1.0.3 (c (n "sitewriter") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "156jvihffzj0vifw335gy5gz8dnjkgp29nx3zmbfr7q0zzbnf03q")))

(define-public crate-sitewriter-1.0.4 (c (n "sitewriter") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1ssl2jhb17rfj02asw44ilzk60scd6igs619z6p7qh3jjxpkg7zl")))

(define-public crate-sitewriter-1.0.5 (c (n "sitewriter") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("std" "clock"))) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1xbwyax5b03jf3dx7f1fqj8wks3z0s0gh8lll128xk9phxfg94v0")))

