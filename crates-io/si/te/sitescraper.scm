(define-module (crates-io si te sitescraper) #:use-module (crates-io))

(define-public crate-sitescraper-0.1.0 (c (n "sitescraper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "1mzldac66rh3rnry37rqd0pkxaim8xyc8851f926djfvjsbbwad4") (y #t)))

(define-public crate-sitescraper-0.1.1 (c (n "sitescraper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "0yfqahdfa6k1adp1nwyymvd67mzm7r38xhycjib5rpys5fi7g0yi") (y #t)))

(define-public crate-sitescraper-0.1.2 (c (n "sitescraper") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "18f0xnkzailrbs58y2s4gdf1ifxlgqq8c1qbr1576qpkbp9533qp") (y #t)))

(define-public crate-sitescraper-0.1.3 (c (n "sitescraper") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "03fa5fr2f55g55i5p2rds69fhz6a523yf8x5j3yx2m99nrzx690h") (y #t)))

(define-public crate-sitescraper-0.1.4 (c (n "sitescraper") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "17yh1ihww67x2d44aa4dm3sxr8dbk8cjg59bfrscpvfgkxssys0j") (y #t)))

(define-public crate-sitescraper-0.1.5 (c (n "sitescraper") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "1xpi61fxnjdqcmqdpsycq6pdcj4wx8ybx9701x3p6zq33vk6qvak") (y #t)))

(define-public crate-sitescraper-0.1.51 (c (n "sitescraper") (v "0.1.51") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "0xkbqrd4amyladfk3vwq6zhcr7mp0n07rcvqyd9r8px2pd0lbaqn") (y #t)))

(define-public crate-sitescraper-0.1.52 (c (n "sitescraper") (v "0.1.52") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "19pz7c8b73vz54vc4gl522dcvnxa0dcs3wn2lqkkhwrn9gz6n28h") (y #t)))

(define-public crate-sitescraper-0.1.53 (c (n "sitescraper") (v "0.1.53") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "0hyy9isgsir1aivrrbaa0mj62npq9djvd43zj5cbqbjisr0wfjr1")))

(define-public crate-sitescraper-0.1.54 (c (n "sitescraper") (v "0.1.54") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "0w3s70av1vkc8fxsrmr6d8l06kmvck09irac0fvfsr3si0q5yhca") (y #t)))

(define-public crate-sitescraper-0.1.55 (c (n "sitescraper") (v "0.1.55") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "03pry76zmv0c86q7w7gc4pb7mxzpq70r2zy2c11mfd8zb5mmrbq1")))

(define-public crate-sitescraper-0.2.0 (c (n "sitescraper") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "1a8cc6z3vb5lncaxzxmygnxn3c7v2lgqza0q10rzh13q01qisd84") (y #t)))

(define-public crate-sitescraper-0.2.1 (c (n "sitescraper") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "slicestring") (r "^0.1.0") (d #t) (k 0)))) (h "0yzn6v0lwvlwh5fmm53z9r9vc1zsmx4kgn1j9a74n6wzbnmjnd41")))

