(define-module (crates-io si te sitemap) #:use-module (crates-io))

(define-public crate-sitemap-0.1.0 (c (n "sitemap") (v "0.1.0") (d (list (d (n "chrono") (r ">= 0.2") (d #t) (k 0)) (d (n "chrono_utils") (r ">= 0.1.0") (d #t) (k 0)) (d (n "url") (r ">= 0.5.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.3") (d #t) (k 0)))) (h "0i9ajbg0pyn0r4k7m2i5l6hxbwd0xc99l1azfagl2sk9lzriayks") (y #t)))

(define-public crate-sitemap-0.1.1 (c (n "sitemap") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "chrono_utils") (r ">= 0.1.0") (d #t) (k 0)) (d (n "url") (r ">= 0.5.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.3") (d #t) (k 0)))) (h "0wm3gc4aa46wp5prfq5bq6qaiyfnwwzg6di5dx20yrpw43y276pr")))

(define-public crate-sitemap-0.2.0 (c (n "sitemap") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono_utils") (r "^0.1.3") (d #t) (k 0)) (d (n "url") (r ">= 0.5.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.3") (d #t) (k 0)))) (h "03g56bv1rxa99x1s7r5xmqif6wlrmcxdbcjzkjh9f53i92vp75xa")))

(define-public crate-sitemap-0.3.0 (c (n "sitemap") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono_utils") (r "^0.1.3") (d #t) (k 0)) (d (n "url") (r ">= 0.5.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.3") (d #t) (k 0)))) (h "0cvi2gmfbdwf3c0wfsbxrydqbkrf029wh37d136p2syyfzcwm8qp")))

(define-public crate-sitemap-0.4.0 (c (n "sitemap") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono_utils") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.3") (d #t) (k 0)))) (h "1dqn211a7vv01clarhrhsv2fq5nlpiqz4n6xbg4gwqa4igjwg5aw")))

(define-public crate-sitemap-0.4.1 (c (n "sitemap") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono_utils") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.3") (d #t) (k 0)))) (h "0kgbzp5b10lfpqcv9yj9ix5ilrj1wcrbbs05v3bnckw5i0fpwsbw")))

