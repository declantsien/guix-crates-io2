(define-module (crates-io si te sitegen) #:use-module (crates-io))

(define-public crate-sitegen-0.0.1 (c (n "sitegen") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "09y795x8lkd3fvwfkh92n4rx50b09mrxx2w2yhac182yr7h9qdi5")))

