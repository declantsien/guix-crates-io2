(define-module (crates-io si te sitemap-xml-writer) #:use-module (crates-io))

(define-public crate-sitemap-xml-writer-0.1.0 (c (n "sitemap-xml-writer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0il7inc1m70289fkgiy0al3pnfirhy22hvl3nkszf7w0vz6x938m") (f (quote (("default")))) (s 2) (e (quote (("url" "dep:url") ("time" "dep:time") ("chrono" "dep:chrono"))))))

