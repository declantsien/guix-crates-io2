(define-module (crates-io si te sitemaps) #:use-module (crates-io))

(define-public crate-sitemaps-0.1.0 (c (n "sitemaps") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "114lzsxasn83ns0yn9ydpqsigx5bqrw322ha16n9f1lad19g5nml") (r "1.61.0")))

