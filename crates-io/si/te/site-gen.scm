(define-module (crates-io si te site-gen) #:use-module (crates-io))

(define-public crate-site-gen-2.0.0 (c (n "site-gen") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "comrak") (r "^0.21.0") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)) (d (n "truncate_string_at_whitespace") (r "^1.0.1") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15.2") (d #t) (k 0)))) (h "0pckhk1ahyhrbr8whmzfnwmbmjv9b9fnmq771n0mrnd31zm3g097")))

