(define-module (crates-io si te sitemap-writer) #:use-module (crates-io))

(define-public crate-sitemap-writer-0.1.0 (c (n "sitemap-writer") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "09h26drrlvdd63598jqbd8jz7hch17l9ljja4rkq7qmpzn4pvcgb")))

(define-public crate-sitemap-writer-0.1.1 (c (n "sitemap-writer") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "0ymg5n141xalwnbxa03ja1yl3q81ygrppimg2i6lqqjfldb6pfap")))

