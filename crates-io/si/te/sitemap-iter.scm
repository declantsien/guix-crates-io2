(define-module (crates-io si te sitemap-iter) #:use-module (crates-io))

(define-public crate-sitemap-iter-0.1.0 (c (n "sitemap-iter") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)))) (h "1j3xrj4ivpmzryqdbqvq20w4cc7fmfqc0wzwxg58prg3scpqxfza") (r "1.56")))

