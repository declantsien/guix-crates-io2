(define-module (crates-io si te siter) #:use-module (crates-io))

(define-public crate-siter-0.1.0 (c (n "siter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap_conf") (r "^0.1.5") (d #t) (k 0)) (d (n "gobble") (r "^0.6.1") (d #t) (k 0)) (d (n "templito") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1bas3mz16n5212aa6fmyvq0lag29j6zccnmx38pciv3vax6zgcwk")))

(define-public crate-siter-0.1.1 (c (n "siter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "card_format") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clap_conf") (r "^0.1.5") (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "gobble") (r "^0.6.1") (d #t) (k 0)) (d (n "templito") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1dgyynksffq7kiybxqpp364yskx40751jn6icc52xg7n1f0a7kw4")))

