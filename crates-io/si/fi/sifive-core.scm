(define-module (crates-io si fi sifive-core) #:use-module (crates-io))

(define-public crate-sifive-core-0.0.1 (c (n "sifive-core") (v "0.0.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "087yc883qmgrvbnxwm42718pd1705xv23z0ry1f44jzwr3fqypni")))

(define-public crate-sifive-core-0.0.2 (c (n "sifive-core") (v "0.0.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1pah9rpr5y7zb1rsinzb53b5vz5wdk5afzgsmypgbl45fxxpa9hs")))

(define-public crate-sifive-core-0.1.0 (c (n "sifive-core") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1wpqrh1i699digf9nwn1a50w6x3ysxrqr1dim6vsfpsdjk32ncqq")))

