(define-module (crates-io si fi sifive-fe310-g002) #:use-module (crates-io))

(define-public crate-sifive-fe310-g002-0.1.0 (c (n "sifive-fe310-g002") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "0m1lwcmn4jw6ja58s67azfigv1c24mr9sx4hyv5175pl3kyq7d67")))

(define-public crate-sifive-fe310-g002-0.1.1 (c (n "sifive-fe310-g002") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "07hm7zzks3vdsn6a033xpafb12wy39rrmi9cvw6dqphyvpki3vdb")))

(define-public crate-sifive-fe310-g002-0.1.2 (c (n "sifive-fe310-g002") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1vb1qxbblj0jp9y5a3xfzxkd7812sf9fagnl28b975lzycab245s")))

(define-public crate-sifive-fe310-g002-0.1.3 (c (n "sifive-fe310-g002") (v "0.1.3") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1clz1ssi55wvyhnv303jam8bc0mrgh14yb4fgyl01hjcdbj4csxi")))

(define-public crate-sifive-fe310-g002-0.1.4 (c (n "sifive-fe310-g002") (v "0.1.4") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1928f7p5mdxdy2nbwi9fvqjgb572pn1sr3g3ghpq91xkv7lk8xys")))

(define-public crate-sifive-fe310-g002-0.1.5 (c (n "sifive-fe310-g002") (v "0.1.5") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1sxizngnyx5dw7d5r4mmazrjsxyav9sa2llby34wbq0wd4ysgy7v")))

