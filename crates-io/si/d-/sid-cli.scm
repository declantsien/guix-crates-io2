(define-module (crates-io si d- sid-cli) #:use-module (crates-io))

(define-public crate-sid-cli-0.2.0 (c (n "sid-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sid2") (r "^0.2.0") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "02z09wazbi7pvgpqf0fd361b4kjqvwp6i951cm2ynmr3jak2kpz4")))

(define-public crate-sid-cli-0.3.0 (c (n "sid-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sid2") (r "^0.3.0") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1aar2ahagrpfq0y8dib83xhgpp67x2hafbp4ifn06bp6spbr5xlv")))

