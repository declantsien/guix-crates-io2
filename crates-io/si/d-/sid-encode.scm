(define-module (crates-io si d- sid-encode) #:use-module (crates-io))

(define-public crate-sid-encode-0.1.0 (c (n "sid-encode") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1885j8hlaqwl0r5z852x5qxywzn94pq9fvd28bfcfpld53616ri6")))

(define-public crate-sid-encode-0.1.1 (c (n "sid-encode") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bvm1gd6sl4isdcvjyapay4sxbr5y0vq3kq6ncfbpc6nabgy58x5")))

(define-public crate-sid-encode-0.2.0 (c (n "sid-encode") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1q22v0044zmcjkgv2dgcmr9mk88ln28p4a83qwwidi8m1v9g3lj8")))

