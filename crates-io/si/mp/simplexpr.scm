(define-module (crates-io si mp simplexpr) #:use-module (crates-io))

(define-public crate-simplexpr-0.1.0 (c (n "simplexpr") (v "0.1.0") (d (list (d (n "eww_shared_util") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.7") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m2y3l90fa885vkgcr7fsgy434q0xpzcf18gb4r2lv9ldb4prsxn")))

