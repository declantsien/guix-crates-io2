(define-module (crates-io si mp simplex_method) #:use-module (crates-io))

(define-public crate-simplex_method-0.1.0 (c (n "simplex_method") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "05zwr4qp86l92fx6py9pm8nf7fgi6pd05d4yz5bw3smwq966sqg4")))

(define-public crate-simplex_method-0.1.1 (c (n "simplex_method") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "0pa43lb27hw8xpd6bns1vk319ji4wskdnm68bc2l2li4gid8x6xx")))

(define-public crate-simplex_method-0.1.2 (c (n "simplex_method") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "1f25g781lisw6rswy403vrj2xi7z196hmy2x154xcw056is1y49a")))

(define-public crate-simplex_method-0.1.3 (c (n "simplex_method") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "038jqhwqxkmak0ps606w816dh89b96wx909cbxw7p9j5m61my6m2")))

