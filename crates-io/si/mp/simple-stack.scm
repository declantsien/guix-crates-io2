(define-module (crates-io si mp simple-stack) #:use-module (crates-io))

(define-public crate-simple-stack-0.1.0 (c (n "simple-stack") (v "0.1.0") (h "0d2gbin0pk9jja25mqxsbbwk31zvbjyzn6l5fgn28c382pswy8x7")))

(define-public crate-simple-stack-0.2.0 (c (n "simple-stack") (v "0.2.0") (h "0c1msimmraf8kwz0agwbcsby815v2d1lcdhy4pqrs41im8skncd4")))

