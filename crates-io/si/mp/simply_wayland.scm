(define-module (crates-io si mp simply_wayland) #:use-module (crates-io))

(define-public crate-simply_wayland-0.1.0 (c (n "simply_wayland") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0fa6n70qbm948bffhilrhqgrg0i84p93j25px43r8hvg9mcpfw1y")))

(define-public crate-simply_wayland-0.1.1 (c (n "simply_wayland") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1lmxc03nksyylximyn18cz6507mdqshzbpv0bfg5cjfl79ww4hnd")))

(define-public crate-simply_wayland-0.1.2 (c (n "simply_wayland") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1jb6mkcp9sccvi6sg4jjd8mcm42bq95wyadsv69avx5csqrnnfis")))

(define-public crate-simply_wayland-0.1.3 (c (n "simply_wayland") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "02ald7awskv23pvnn0ix0mkfv6z7gqv3wj43z0w89xl2wbfyn8bf")))

(define-public crate-simply_wayland-0.1.5 (c (n "simply_wayland") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "0ldi0xbqlvcjrg0a8bf9q7khaflphrs7ww977jrl8s1dqj1vqvrj")))

(define-public crate-simply_wayland-0.1.7 (c (n "simply_wayland") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "0z9clsbxpwqrib7n42dzb7d6bhf3w8v2h67a2z903nd9sk94hk0r")))

(define-public crate-simply_wayland-0.1.8 (c (n "simply_wayland") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "0fgmpj2rffdka084qgn3q2vv4n8ig9dasrbiid1sqcnx3wgak8jb")))

(define-public crate-simply_wayland-0.1.9 (c (n "simply_wayland") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "1vzhx1nxhhqaslw9f5gqanxhwb0zw7a298adg0n6j0v21a4vv596")))

(define-public crate-simply_wayland-0.1.10 (c (n "simply_wayland") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "1x11rk6g9y31fqpvadlgn66g34l394xbpbkw330cd9jdgmmndq07")))

(define-public crate-simply_wayland-0.1.11 (c (n "simply_wayland") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "04frm9y97s9vgq5rrq4lxi3wg6m91x5z7q08h5bc64az5fqxvfai")))

(define-public crate-simply_wayland-0.1.12 (c (n "simply_wayland") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "0q8y27cf3f721wp3nw04b0syjhj5bflyp3d15sibpmd3rik4sqv5")))

(define-public crate-simply_wayland-0.1.13 (c (n "simply_wayland") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)))) (h "12ni8iwvsk6g77l2hbgpgiblxrr7fg5mig4mhj0n26hcs456y2mr")))

(define-public crate-simply_wayland-0.1.14 (c (n "simply_wayland") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 1)) (d (n "parking_lot") (r "^0.3.8") (d #t) (k 1)))) (h "00lcppkpbrjsrdaq1yisx6b8pqwwv03wybh8adkv9ll5ps8nlc2b")))

