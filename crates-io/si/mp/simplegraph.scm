(define-module (crates-io si mp simplegraph) #:use-module (crates-io))

(define-public crate-simplegraph-0.1.0 (c (n "simplegraph") (v "0.1.0") (h "1cjrizg7r1931fh9f1slr5gwb4yhsiws0wn16nnafbj4jl12sjy0")))

(define-public crate-simplegraph-0.2.0 (c (n "simplegraph") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0clq87wy1k8vjawlq2xfq0wmncgq2db2i08ckv9ns66fhs2fafbx")))

(define-public crate-simplegraph-0.2.1 (c (n "simplegraph") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0ng7hjj8jpajxngsvjk9hc176pa06lj0sqjsgmzz07r0pmm8524b")))

(define-public crate-simplegraph-0.2.2 (c (n "simplegraph") (v "0.2.2") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0psffgjqmbfh4m0razgri8mlhygv78lyld63pl7yhf070rbp2rhg")))

