(define-module (crates-io si mp simple-dht11) #:use-module (crates-io))

(define-public crate-simple-dht11-0.1.0 (c (n "simple-dht11") (v "0.1.0") (d (list (d (n "rppal") (r "^0.14.0") (d #t) (k 0)))) (h "0735fmjvyy62rx9n083j9ld0yjlqh7vg8cchrh27vcb75r1f33rz")))

(define-public crate-simple-dht11-0.1.1 (c (n "simple-dht11") (v "0.1.1") (d (list (d (n "rppal") (r "^0.14.0") (d #t) (k 0)))) (h "0pr075hxb6k3fhxzpg4zrslcglx12wl1fanc4ymawp7blg0g3k1g")))

(define-public crate-simple-dht11-0.1.2 (c (n "simple-dht11") (v "0.1.2") (d (list (d (n "rppal") (r "^0.14.0") (d #t) (k 0)))) (h "1cqcqz62bpd9v90605bgknh4h2yl5y475b2x1w91xz5v2rrmaihn")))

