(define-module (crates-io si mp simplestats) #:use-module (crates-io))

(define-public crate-simplestats-0.1.2 (c (n "simplestats") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0r7fn6vi51pl0c1akjlldzjdmfvwwwgfar2iffkvjpnk29gpm98i") (f (quote (("build_deps" "man"))))))

(define-public crate-simplestats-0.1.3 (c (n "simplestats") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0gmlfmn4fnshhs3ww5a7hafprfks9j7iny3v3s96xdn0fksglbm2") (f (quote (("build_deps" "man"))))))

