(define-module (crates-io si mp simple_ringbuf) #:use-module (crates-io))

(define-public crate-simple_ringbuf-0.1.0 (c (n "simple_ringbuf") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0y170r9s1cdpc29cnd49w861996j1f03kivail000gwf3i5pii46") (f (quote (("default"))))))

(define-public crate-simple_ringbuf-0.1.1 (c (n "simple_ringbuf") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1ksn4wyfs0m7h15jbs9b6gavf6wq4ajgrw3w8iprrrfmcvnkkcqf") (f (quote (("default"))))))

(define-public crate-simple_ringbuf-0.1.2 (c (n "simple_ringbuf") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1nr3mmc28i36zngk1jpfbp166z8g5lx4v5csk8s8fdh71vqssj4j") (f (quote (("default"))))))

