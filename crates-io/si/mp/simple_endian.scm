(define-module (crates-io si mp simple_endian) #:use-module (crates-io))

(define-public crate-simple_endian-0.1.1 (c (n "simple_endian") (v "0.1.1") (h "03kl0qsivx7ibvrb3diaqdzq1zrj305g8jqc6mb7hc4aqwk63r6p")))

(define-public crate-simple_endian-0.1.2 (c (n "simple_endian") (v "0.1.2") (h "0l7sz1nvyd80xklc3gjd0jx35lrd8rpyvlkqicq1brmddy99ja2y")))

(define-public crate-simple_endian-0.1.3 (c (n "simple_endian") (v "0.1.3") (h "1vf0xlyj10kfm4xqdzkdlkmqpwxz7ppk8h9xzbm5z689cdyp65m6")))

(define-public crate-simple_endian-0.1.4 (c (n "simple_endian") (v "0.1.4") (h "1ad7y2liynk7d6rd4j1kr4b3rbr45iq7vjb8k00z7r339km7ad9s")))

(define-public crate-simple_endian-0.1.5 (c (n "simple_endian") (v "0.1.5") (h "1psr0687nba47y54dkn4d3fjmyfbnk22a76xv4n1wfq8q2zscxzj")))

(define-public crate-simple_endian-0.1.6 (c (n "simple_endian") (v "0.1.6") (h "069gw4iqy53s34qvmvikarryjk4gq1r044sqnfyj746cs8z815xa")))

(define-public crate-simple_endian-0.1.7 (c (n "simple_endian") (v "0.1.7") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "17l2bgpqmvfx0slxzrqn00k64qlbq5yvk4w8qygv5m79i6g3iz95")))

(define-public crate-simple_endian-0.2.0 (c (n "simple_endian") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "0ffg7b2nq1v4wpi5ln8fr0h3m1pik0167100qwx6km0j5y7vjkjh") (f (quote (("shift_ops") ("neg_ops") ("math_ops") ("little_endian") ("integer_impls") ("format") ("float_impls" "integer_impls") ("default" "bitwise" "comparisons" "format" "math_ops" "neg_ops" "shift_ops" "both_endian" "float_impls" "integer_impls" "byte_impls") ("comparisons") ("byte_impls") ("both_endian" "big_endian" "little_endian") ("bitwise" "integer_impls") ("big_endian"))))))

(define-public crate-simple_endian-0.2.1 (c (n "simple_endian") (v "0.2.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "1bk9nk6cmy24ggqawav2dnv09xjf346zcnn7fdzz6k654v15kfrm") (f (quote (("shift_ops") ("neg_ops") ("math_ops") ("little_endian") ("integer_impls") ("format") ("float_impls" "integer_impls") ("default" "bitwise" "comparisons" "format" "math_ops" "neg_ops" "shift_ops" "both_endian" "float_impls" "integer_impls" "byte_impls") ("comparisons") ("byte_impls") ("both_endian" "big_endian" "little_endian") ("bitwise" "integer_impls") ("big_endian"))))))

(define-public crate-simple_endian-0.3.0 (c (n "simple_endian") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "1i3734ywrmz4gnkly6xipkb6mdwnqmy1vn254d0qhm623mqjvhsj") (f (quote (("shift_ops") ("neg_ops") ("math_ops") ("little_endian") ("integer_impls") ("format") ("float_impls" "integer_impls") ("default" "bitwise" "comparisons" "format" "math_ops" "neg_ops" "shift_ops" "both_endian" "float_impls" "integer_impls" "byte_impls") ("comparisons") ("byte_impls") ("both_endian" "big_endian" "little_endian") ("bitwise" "integer_impls") ("big_endian"))))))

(define-public crate-simple_endian-0.3.1 (c (n "simple_endian") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "0g0kadv57ssavi6jy18kc579diz5f5phvn0d15j8a7smwm4hyg3k") (f (quote (("shift_ops") ("neg_ops") ("math_ops") ("little_endian") ("integer_impls") ("format") ("float_impls" "integer_impls") ("default" "bitwise" "comparisons" "format" "math_ops" "neg_ops" "shift_ops" "both_endian" "float_impls" "integer_impls" "byte_impls") ("comparisons") ("byte_impls") ("both_endian" "big_endian" "little_endian") ("bitwise" "integer_impls") ("big_endian"))))))

(define-public crate-simple_endian-0.3.2 (c (n "simple_endian") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 2)))) (h "13s5gj1b7bqspg3198k4bpd1qs3vdccvg853sl1gv12xjai1naw7") (f (quote (("shift_ops") ("neg_ops") ("math_ops") ("little_endian") ("integer_impls") ("format") ("float_impls" "integer_impls") ("default" "bitwise" "comparisons" "format" "math_ops" "neg_ops" "shift_ops" "both_endian" "float_impls" "integer_impls" "byte_impls") ("comparisons") ("byte_impls") ("both_endian" "big_endian" "little_endian") ("bitwise" "integer_impls") ("big_endian"))))))

