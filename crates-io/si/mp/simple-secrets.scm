(define-module (crates-io si mp simple-secrets) #:use-module (crates-io))

(define-public crate-simple-secrets-0.1.0 (c (n "simple-secrets") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)))) (h "0bsb8wiywlkiqmkrq2wvgynw74a2w0db7gwbnvp37wxli5lgvas0") (y #t)))

(define-public crate-simple-secrets-0.1.1 (c (n "simple-secrets") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)))) (h "0j5w5qgs8fw9ksalqp9vvxfx4z9rv7gwn7vi2bz1fdnk0rp8dq97")))

