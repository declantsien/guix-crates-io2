(define-module (crates-io si mp simple_vector2) #:use-module (crates-io))

(define-public crate-simple_vector2-1.0.0 (c (n "simple_vector2") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kjj0ahkwxqwb6l104g4sz481i8hzfcwa1b2f11nf2nndg17j380")))

(define-public crate-simple_vector2-1.0.1 (c (n "simple_vector2") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cbmxz2vh0hgb5fy48qs2nqk6m3bhp66vsc4nfpgfbkfqvlvvvjz")))

