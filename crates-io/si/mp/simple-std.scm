(define-module (crates-io si mp simple-std) #:use-module (crates-io))

(define-public crate-simple-std-0.1.0 (c (n "simple-std") (v "0.1.0") (h "15bglqjxda85wv465988kq6fmj8p4dgxq1pxp4q0zg5bbgy7ppbp")))

(define-public crate-simple-std-0.1.1 (c (n "simple-std") (v "0.1.1") (h "0hj47ldy0sq9ifhff09ml1aqdlxxvhrk2mr9gjx8q2lh56ylszbb")))

