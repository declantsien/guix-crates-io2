(define-module (crates-io si mp simple-rate-limiter) #:use-module (crates-io))

(define-public crate-simple-rate-limiter-1.0.0 (c (n "simple-rate-limiter") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 2)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("parking_lot" "rt-multi-thread"))) (k 2)))) (h "0slfwd0vd4h2g4xwkba8rn9l74sw5hc9ka7qhi5mz8h04pi30syh")))

