(define-module (crates-io si mp simpl_actor_macros) #:use-module (crates-io))

(define-public crate-simpl_actor_macros-0.1.0 (c (n "simpl_actor_macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13z14aw0crk1fzlnnbavh6qscl5lys4jsr1y8hrsdnykbl479xsi")))

(define-public crate-simpl_actor_macros-0.1.1 (c (n "simpl_actor_macros") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pi2pr9qj2ly8czqdk9h65b89k0ivlh692s015bhxm1aqw4i8m8b")))

(define-public crate-simpl_actor_macros-0.2.0 (c (n "simpl_actor_macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pff65shr3z1bwgfj9qf2kbhdck2h0q8z4x0vs617b1i6k4c3h7v")))

(define-public crate-simpl_actor_macros-0.2.1 (c (n "simpl_actor_macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xgl1pcdf2z2pi3v43avalglxzf2dqsgi2nzgdpnhsh21yjbll99")))

(define-public crate-simpl_actor_macros-0.2.2 (c (n "simpl_actor_macros") (v "0.2.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12dr8djbyy9srqj24bjxnrx4q71ls1lpx04dalnjy8psxlr7plqv")))

(define-public crate-simpl_actor_macros-0.2.3 (c (n "simpl_actor_macros") (v "0.2.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zhsk4s6hl2zjijm5lscp8xasyax5bd89wpx4qjqd2ypbiwzfsrc")))

(define-public crate-simpl_actor_macros-0.2.4 (c (n "simpl_actor_macros") (v "0.2.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05y349vmp1037j9lbmdp4ns3qbq8vqydm84appn0y77h0k2mb01k")))

(define-public crate-simpl_actor_macros-0.2.5 (c (n "simpl_actor_macros") (v "0.2.5") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vnk32ip9a6y1ngsi8l6qkpcgnr7ag1dnib8sl92pd55cpsnippz") (y #t)))

(define-public crate-simpl_actor_macros-0.2.6 (c (n "simpl_actor_macros") (v "0.2.6") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bhcwn865qy5ck57als1nxinyr3z1xmdp3iyyzsxvybsykirdsj5")))

(define-public crate-simpl_actor_macros-0.2.7 (c (n "simpl_actor_macros") (v "0.2.7") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rsb6vs4sq38i5vmqbwnhafyif134g1z9y5nw6h6rvh88s0fvgsy")))

