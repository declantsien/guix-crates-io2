(define-module (crates-io si mp simple-scheduler) #:use-module (crates-io))

(define-public crate-simple-scheduler-0.1.0 (c (n "simple-scheduler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (f (quote ("executor"))) (d #t) (k 0)))) (h "0qck9svsxnsdinlccjj01b8jv47da99wqdim97lrzyi7bw2xi1ng")))

