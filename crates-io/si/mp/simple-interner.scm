(define-module (crates-io si mp simple-interner) #:use-module (crates-io))

(define-public crate-simple-interner-0.1.0 (c (n "simple-interner") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j7fl36m5a3v3ihibazykr5d3jki2d8s43fmc8rhj8i9jqjn7bff")))

(define-public crate-simple-interner-0.2.0 (c (n "simple-interner") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nhvpgm3y0d2vfr2qj5qhsvnmhvb80pm9xvkb4w3awbwqvv0h6my")))

(define-public crate-simple-interner-0.3.1 (c (n "simple-interner") (v "0.3.1") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.2") (o #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (k 0)))) (h "0smxz2sxwm6mk4smpbpk6syjkq8pg7ii38p5plf948bvwghc1xgq") (f (quote (("raw" "hashbrown" "hashbrown/raw"))))))

(define-public crate-simple-interner-0.3.2 (c (n "simple-interner") (v "0.3.2") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (o #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (k 0)))) (h "0qkfdsys6az00hbp1znywgq5mkjpj4m8rsnx9b5pkby2h21b5l89") (f (quote (("raw" "hashbrown" "hashbrown/raw")))) (y #t) (r "1.60.0")))

(define-public crate-simple-interner-0.3.3 (c (n "simple-interner") (v "0.3.3") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (o #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (k 0)))) (h "0z93h6yg9232aap66px7d3sfww5d51y3agdwrk8a102kpyxm5zjm") (f (quote (("raw" "hashbrown" "hashbrown/raw")))) (y #t) (r "1.60.0")))

(define-public crate-simple-interner-0.3.4 (c (n "simple-interner") (v "0.3.4") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (o #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (k 0)))) (h "1s6y9b762ya1kb3gjll1sb2jk1qfr8yb9iiz6szvc62k5brcnxzb") (f (quote (("raw" "hashbrown" "hashbrown/raw")))) (r "1.60.0")))

