(define-module (crates-io si mp simple-pagerank) #:use-module (crates-io))

(define-public crate-simple-pagerank-0.1.0 (c (n "simple-pagerank") (v "0.1.0") (h "0d2na68qn7pzkmkdw96d4nwah9j9qzj3gcfgfygbas8hl44y49nf")))

(define-public crate-simple-pagerank-0.1.1 (c (n "simple-pagerank") (v "0.1.1") (h "08syn0rridpfwl6nlqfhw2y99h74f0rasyfzxhry93k5a1qljh24")))

(define-public crate-simple-pagerank-0.1.2 (c (n "simple-pagerank") (v "0.1.2") (h "1fhlx227ykab2kahq30i2i2rnwzhm7k6hjyylnac5rmf325267fd")))

(define-public crate-simple-pagerank-0.2.0 (c (n "simple-pagerank") (v "0.2.0") (h "03w72jkfdcy78csfkxi7pb98bkwlnk025mx92bnjirzsbp8vdlj7")))

