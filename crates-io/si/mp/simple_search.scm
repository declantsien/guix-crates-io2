(define-module (crates-io si mp simple_search) #:use-module (crates-io))

(define-public crate-simple_search-0.1.0 (c (n "simple_search") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "1z3d31sfvckzbfx3qpqxjc1x99sqa6f4razhz0f3wx2z03w7kc4s") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-simple_search-0.1.1 (c (n "simple_search") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "116dbb528a5br3dkrl9ahfbshv7qzhzaj8vq7smnhmn6nyv5pyhk") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-simple_search-0.1.2 (c (n "simple_search") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "1h47cb67ldb59vcd6mw2x89hxkhyxhhmn52v3hw50zgzwvj3mwv5") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-simple_search-0.2.0 (c (n "simple_search") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "19rmvrd5m034nprrvyrm1j9406xinfaz2rmmirc8p5pcw4017c1q") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-simple_search-0.2.1 (c (n "simple_search") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "0l929g6hm3khr2mrqin8n5vhfjs7xdy5xf6fbzg57nszya9gvmly") (s 2) (e (quote (("rayon" "dep:rayon"))))))

