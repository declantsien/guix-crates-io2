(define-module (crates-io si mp simple_ml) #:use-module (crates-io))

(define-public crate-simple_ml-0.1.0 (c (n "simple_ml") (v "0.1.0") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lh5zvdq1kwa20v3cc6izaha8z2l6y64mrdjvpa8dbx41dm7ik7l") (y #t)))

(define-public crate-simple_ml-0.1.1 (c (n "simple_ml") (v "0.1.1") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1zh2vv6l2vpniawyj22zi8vsyqb5j06cw6i6j4z57rzr9jv222g4") (y #t)))

(define-public crate-simple_ml-0.1.2 (c (n "simple_ml") (v "0.1.2") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "10rv7wmc65sbkf0wv5j1nmqprhi61j2c252np99cmaqcj64ibb83") (y #t)))

(define-public crate-simple_ml-0.1.3 (c (n "simple_ml") (v "0.1.3") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0wf01y5a98j0d1mj1mk1yk648k1h5a5v22a45gfziqgam2zg3cl7") (y #t)))

(define-public crate-simple_ml-0.1.4 (c (n "simple_ml") (v "0.1.4") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "085fi6zpg5vy3s0dl1dz24i66kpva832dx7qpfqarfgnhak08y2n") (y #t)))

(define-public crate-simple_ml-0.1.5 (c (n "simple_ml") (v "0.1.5") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ijwlf7ls34fzs5paspf6c0a18h95swgp8mcz3lj1jkwb57i5w3n") (y #t)))

(define-public crate-simple_ml-0.1.6 (c (n "simple_ml") (v "0.1.6") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1fks94y4mn83npq5a5hmfb60d37dvqf46rx1m4mg0xq0wf7i2x0w")))

(define-public crate-simple_ml-0.1.7 (c (n "simple_ml") (v "0.1.7") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nl1rax9k8vp3y7z1li7f2jan55z0490vidacznd4jxwlnv5ax1c")))

(define-public crate-simple_ml-0.1.9 (c (n "simple_ml") (v "0.1.9") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0h3capk1b8wmmnmmw83n291wqxfnkpnsb1imii4v8xy5s5hsk4qz")))

(define-public crate-simple_ml-0.1.10 (c (n "simple_ml") (v "0.1.10") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vysy2j2i9hshsswfvahwy7plvppwbcsjwakhq843c3v3cbmfw4c")))

(define-public crate-simple_ml-0.1.11 (c (n "simple_ml") (v "0.1.11") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lqjhslk7q35jz4hqzyckw070hqvm62kqpdybh5f1p9rph5fckx4")))

(define-public crate-simple_ml-0.1.12 (c (n "simple_ml") (v "0.1.12") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0hxaz75rhf9v3sx7a60ry80w6lv6j99d6m7glizvyd26hz05x3g7")))

(define-public crate-simple_ml-0.1.13 (c (n "simple_ml") (v "0.1.13") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0mx507rjh8nfg5lmlvcfyp9xp66fb4dvw2srwfgmm323lkc8317g")))

(define-public crate-simple_ml-0.1.14 (c (n "simple_ml") (v "0.1.14") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ff7bfgk160ndfmsx5lay5psir7kkys56cnyl5i5ym4lhw4p5b0q")))

(define-public crate-simple_ml-0.2.0 (c (n "simple_ml") (v "0.2.0") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06pqybhjzj4qalrp26207rl3mx9l93ccgi8bx96wvdpykqwjvbfn")))

(define-public crate-simple_ml-0.2.1 (c (n "simple_ml") (v "0.2.1") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qd1gikblp3j5lqqdicv2ns55d4ywhg07x4zz6qpa8nm3xsqj747")))

(define-public crate-simple_ml-0.2.2 (c (n "simple_ml") (v "0.2.2") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "037myy69z55lb5dfckkqlixrb4rd04cikjynsy4wvgimrzwa61y1")))

(define-public crate-simple_ml-0.2.3 (c (n "simple_ml") (v "0.2.3") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "012ssiyslg9aqhz9bya628ldq593a4hjh9lig10s38xak4q18yl1")))

(define-public crate-simple_ml-0.2.4 (c (n "simple_ml") (v "0.2.4") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0h48kajmnrkaj70ldg44rlivdqh1cl35d8kkq4amigxk99ykyg26")))

(define-public crate-simple_ml-0.2.5 (c (n "simple_ml") (v "0.2.5") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ah4b9yhvdnhs0hmab24ncmqfipdfkg67023rm66l4jdl7m3n9n8")))

(define-public crate-simple_ml-0.2.6 (c (n "simple_ml") (v "0.2.6") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sd59lx2d1fjqvc1f9zw9qgxb0a270hgrx1dy0pxf0pf36qxywy3")))

(define-public crate-simple_ml-0.2.7 (c (n "simple_ml") (v "0.2.7") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16r1cbbb5k1l10rmskviyfjv47lsbb8ds2pqc11wbwrwxh1siigx")))

(define-public crate-simple_ml-0.2.8 (c (n "simple_ml") (v "0.2.8") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06skfywgij0asav59fglbrhf53jbwz5564b4qdsxlqrl8nrdfr54")))

(define-public crate-simple_ml-0.2.9 (c (n "simple_ml") (v "0.2.9") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "03x9k6r38yaljswxfvigdh9l3zacpk16lhz8gv20fi7vhfrzdsbp")))

(define-public crate-simple_ml-0.2.10 (c (n "simple_ml") (v "0.2.10") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0c4halgq5qnd6cbg7y23vw7r0q0sn9bflfmb9xmh7n385j1dgg3s")))

(define-public crate-simple_ml-0.2.11 (c (n "simple_ml") (v "0.2.11") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00kwzvqpk6g4admly2vlvpnl97qv0k8npwvq7qxsmk4ag1mkkp50")))

(define-public crate-simple_ml-0.3.0 (c (n "simple_ml") (v "0.3.0") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1r43f4fhpq0g3mb8dkkzaqr54ym35ykz6r8hidx5zsmbniaradw4")))

(define-public crate-simple_ml-0.3.1 (c (n "simple_ml") (v "0.3.1") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "183810dd2kb4hwzj66878ipk5ghxkgacic3h4s22xl4d01c4fi3i")))

(define-public crate-simple_ml-0.3.2 (c (n "simple_ml") (v "0.3.2") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "18amyvn9khckic1v4kq8zs3pxml79w5sjcs9j935jrx7vzxkg97g")))

