(define-module (crates-io si mp simpleterm) #:use-module (crates-io))

(define-public crate-simpleterm-0.2.0 (c (n "simpleterm") (v "0.2.0") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "1k88bw4pn046hc5cnm1rjgvnkvip0fflzycx7kqsmcq723vz4rfy")))

(define-public crate-simpleterm-0.2.1 (c (n "simpleterm") (v "0.2.1") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "0rwkkj4z7dai7vpgzgfhxd3g75xmvjzyhq1wv161j3afxml6yknq")))

(define-public crate-simpleterm-0.2.2 (c (n "simpleterm") (v "0.2.2") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "0hs888zfkw4hcwkac3wwsqdznfwpxmryiph57c1x842cz7p6irqx")))

(define-public crate-simpleterm-0.2.3 (c (n "simpleterm") (v "0.2.3") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "11x1rqkk58vxqgx4ia26ggb4rn1m0nv9bsyifyz3x7p4msap5pq3")))

(define-public crate-simpleterm-0.2.4 (c (n "simpleterm") (v "0.2.4") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "1jwziyrkz1s84nk4zdh75rmiswp43b99dl1m3gjig8hb6kzckini")))

(define-public crate-simpleterm-0.2.5 (c (n "simpleterm") (v "0.2.5") (d (list (d (n "piston_window") (r "^0.109.0") (d #t) (k 0)))) (h "0qsxnpsgadq9frhx6mjjjzq2ckc8jf7mn0gwgaal1cr0pkqvgj6a")))

