(define-module (crates-io si mp simple-lexer) #:use-module (crates-io))

(define-public crate-simple-lexer-0.1.0 (c (n "simple-lexer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regular-expression") (r "^0.1.0") (d #t) (k 0)) (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-lexer-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-parser-bootstrap") (r "^0.1.0") (d #t) (k 0)))) (h "0z5zg4m4ix9pfblvww5yjm7mvd2ghsdkwqpqsbphslszi2xc4x4d")))

