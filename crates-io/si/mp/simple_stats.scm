(define-module (crates-io si mp simple_stats) #:use-module (crates-io))

(define-public crate-simple_stats-0.0.1 (c (n "simple_stats") (v "0.0.1") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1k1vhfxsp23warby48caypxwz2y9j35drlcijli14ys9ycqci25d")))

(define-public crate-simple_stats-0.0.2 (c (n "simple_stats") (v "0.0.2") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "18jq206bnkp8mlzv1cqs7pcixy3gw5scrda180rdzzhc0vacv95j")))

(define-public crate-simple_stats-0.0.3 (c (n "simple_stats") (v "0.0.3") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "187c5l7aycprbpfqlx3y8rxkawfd5cs76i81yccibhldrbi7wky1")))

(define-public crate-simple_stats-0.0.4 (c (n "simple_stats") (v "0.0.4") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "0llb50klhzvb3bqn7wgp8y7wxlzxfy7qnk5m7x5za9j1ng187602")))

(define-public crate-simple_stats-0.1.0 (c (n "simple_stats") (v "0.1.0") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "0189gis032v1f96rfvdv4kq9np4biakjh5rpirn0wjrw22y7rgqj")))

(define-public crate-simple_stats-0.1.1 (c (n "simple_stats") (v "0.1.1") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "0awhi4hypi9snz0z523hnwmb8qr5vpk1qx361r215fahhqgmai7f")))

