(define-module (crates-io si mp simplog) #:use-module (crates-io))

(define-public crate-simplog-1.0.0 (c (n "simplog") (v "1.0.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0sgvp8kvyhzdd323ygabipa11x682q7mciraybk3pmslbmsq20gl")))

(define-public crate-simplog-1.0.1 (c (n "simplog") (v "1.0.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0k9mb412xwh0qin4df3hyv7z3fmzhidxi7liqwplb5jxnfbax9qc")))

(define-public crate-simplog-1.0.2 (c (n "simplog") (v "1.0.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0n4dz4axx61hiy19pyh3qpcj8bkrj7mh4mp2s3ia6970hh9j1944")))

(define-public crate-simplog-1.0.3 (c (n "simplog") (v "1.0.3") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "05nrc29jqprdysy0zmlrcgx4h9qf3ygky1ijs9yrjz0lhmhf0xm3")))

(define-public crate-simplog-1.1.0 (c (n "simplog") (v "1.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "02bia8wbfa1qhhiwfc5a08fcsdl3dl5c0hamg3kq2fwmziih446h")))

(define-public crate-simplog-1.1.1 (c (n "simplog") (v "1.1.1") (d (list (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "0x31qjg5fv59hl1i6jc3v5684gf50wibn0m6v101r4jwp85wxl3h")))

(define-public crate-simplog-1.2.1 (c (n "simplog") (v "1.2.1") (d (list (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0inwl7vaflxl93ijanqj3x7id7j6y6k3sa7dqbvb19fclrdlw3wd")))

(define-public crate-simplog-1.2.2 (c (n "simplog") (v "1.2.2") (d (list (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "09gw92nazbvj5112am9ixrjciypvdjrd93xyyx1n2rcxczl9k3g7")))

(define-public crate-simplog-1.3.0 (c (n "simplog") (v "1.3.0") (d (list (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0lvd29ybs98zkrv35rz6yxp7l3w3qv38cgx70pivb2xvx0kq556i")))

(define-public crate-simplog-1.4.0 (c (n "simplog") (v "1.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0ilq85dgr51zvzhchqdskm3vg8yihwipn3kyf399aqagcij00s8v")))

(define-public crate-simplog-1.5.0 (c (n "simplog") (v "1.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0nl918qldi9hcx3kb0i20fksf2klx65l8pmgmhz528jca3r5z0zy")))

(define-public crate-simplog-1.6.0 (c (n "simplog") (v "1.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "05vaj9wzbafmpv5cv7jcjn707gam851wj4fhmpg8ba4q291kr50x")))

