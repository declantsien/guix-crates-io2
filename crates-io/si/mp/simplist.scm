(define-module (crates-io si mp simplist) #:use-module (crates-io))

(define-public crate-simplist-0.0.1 (c (n "simplist") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0v82957sznx6x04lzlkh6s006jvqrhng32r61r2dsfhz3hwqkg6n") (f (quote (("scramble"))))))

(define-public crate-simplist-0.0.2 (c (n "simplist") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0iqw2y6qrm8c3nra1yp5nncksljzv7a5hq6nprills18c3sm5a12") (f (quote (("scramble"))))))

(define-public crate-simplist-0.0.3 (c (n "simplist") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0y87vimwqvsd9ypbbhrk20b1m0xrdbr31s0hrryzc80z81i3b21b") (f (quote (("scramble"))))))

(define-public crate-simplist-0.0.4 (c (n "simplist") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0jds2ysq13397y4q443373824w5b02341jp6hs7dc8y4c968khkl") (f (quote (("scramble") ("nightly"))))))

(define-public crate-simplist-0.0.5 (c (n "simplist") (v "0.0.5") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 0)))) (h "1hvlqc5i14mnq9hism2s8i116raqgy78mmpz845by1dz1gpvy35j") (f (quote (("scramble") ("nightly"))))))

