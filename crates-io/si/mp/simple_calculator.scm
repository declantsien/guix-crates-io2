(define-module (crates-io si mp simple_calculator) #:use-module (crates-io))

(define-public crate-simple_calculator-0.1.0 (c (n "simple_calculator") (v "0.1.0") (h "1gg2cc34jl162ihzjzr3crbx3sfls12vsir9i9qzcx44fxc46152") (y #t)))

(define-public crate-simple_calculator-0.1.1 (c (n "simple_calculator") (v "0.1.1") (h "03q5rm8jnx7l79salby38a0vrchwi3a7z73cp24i9wz0i46s98yy") (y #t)))

(define-public crate-simple_calculator-0.1.2 (c (n "simple_calculator") (v "0.1.2") (h "06ahngwvrkqydq3x5fbq2g9255x0bvkd514c28ca12gxnfh9mlls") (y #t)))

(define-public crate-simple_calculator-0.1.3 (c (n "simple_calculator") (v "0.1.3") (h "1fwny85kyj87qvidp4vn2i7mnd44bm4ppg895hd83yrfpgr9nm9h") (y #t)))

(define-public crate-simple_calculator-0.1.4 (c (n "simple_calculator") (v "0.1.4") (h "1y6r3ai3lyixzcn13x2hn4r3rs6whbr4dg0ijpgh5a00qa0ilys1") (y #t)))

(define-public crate-simple_calculator-0.1.5 (c (n "simple_calculator") (v "0.1.5") (h "0pnzz5nlcklvy3inn6k3y2266cg2inkkjszqip9ld7dpb9c6s1n0")))

