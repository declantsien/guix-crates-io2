(define-module (crates-io si mp simple_bpm) #:use-module (crates-io))

(define-public crate-simple_bpm-0.1.0 (c (n "simple_bpm") (v "0.1.0") (d (list (d (n "itertools") (r "^0.6.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0fis10lyx5yybcd31fmqi87l4xjvlfwv34xf0g8dry482y30xg6s")))

(define-public crate-simple_bpm-0.2.0 (c (n "simple_bpm") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hodges") (r "^0.1.0") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0pknx9bhipi6x1xls9yxmhll8mi51g8pma7pqgrjlm883xjc0bkb")))

(define-public crate-simple_bpm-0.2.1 (c (n "simple_bpm") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hodges") (r "^0.1.0") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0mj1324kmgyxaqbx4slz1v7m0va8jmarx2c98axvsaxaqanmmkiv")))

