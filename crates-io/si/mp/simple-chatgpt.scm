(define-module (crates-io si mp simple-chatgpt) #:use-module (crates-io))

(define-public crate-simple-chatgpt-0.1.0 (c (n "simple-chatgpt") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1prbpmspdbz3k2d6p73zk3kcf9khj2xcsjrxiq7ygcv5d8rp9jvy") (r "1.71.0")))

(define-public crate-simple-chatgpt-1.0.0 (c (n "simple-chatgpt") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "03agjj8da4h3ywsrgiafmbwzjwqjyfb1i1v1whacqn8bk9md3n63") (r "1.71.0")))

(define-public crate-simple-chatgpt-1.0.1 (c (n "simple-chatgpt") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1lz61fi035mwcsrs7xn1cnv75jdmjcimjay89d7gh9h05lzrnfh6") (r "1.71.0")))

