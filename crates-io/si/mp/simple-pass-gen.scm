(define-module (crates-io si mp simple-pass-gen) #:use-module (crates-io))

(define-public crate-simple-pass-gen-0.1.0 (c (n "simple-pass-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12b214ml5llp833i4380ipzdrfgcqmw143z9a52mgpb437w077yp")))

(define-public crate-simple-pass-gen-0.1.1 (c (n "simple-pass-gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w6wy7d9j0ql00zs09xpbbsixi420kw2kywyccpfbnh3vvhz9fml")))

(define-public crate-simple-pass-gen-0.1.2 (c (n "simple-pass-gen") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lh393i9vrhpwv8xbw63k3v0x43qivhqk56nwakdqazx7ag8nd7p")))

(define-public crate-simple-pass-gen-0.1.3 (c (n "simple-pass-gen") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1122mziidzaqq9l7mf8xkncghpxw9syzrhca2zkc0by30glpqphj")))

(define-public crate-simple-pass-gen-0.1.4 (c (n "simple-pass-gen") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rg5s8gv2dlmdvjr7l4z4y9n4szgcf6l9fig7lmdl9skr1wa4r7j")))

(define-public crate-simple-pass-gen-0.1.5 (c (n "simple-pass-gen") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "113np2q1w4dwbsk42970d62r1spfdig73h2ys7z093j4irapm61v")))

(define-public crate-simple-pass-gen-0.1.7 (c (n "simple-pass-gen") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jf6h28vcrs8wy1gq3dl7qrmsg8piycjkfka6af12gzsqzyvczar")))

(define-public crate-simple-pass-gen-0.1.8 (c (n "simple-pass-gen") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kghmd79x0dl09x5lz29c9g5hd09kphllxgqh6h6bnqj8mrghys5")))

