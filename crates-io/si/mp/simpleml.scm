(define-module (crates-io si mp simpleml) #:use-module (crates-io))

(define-public crate-simpleml-0.1.0 (c (n "simpleml") (v "0.1.0") (d (list (d (n "tree_iterators_rs") (r "^1.1.4") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.0") (d #t) (k 0)))) (h "1fa9wwc1f7pwn8brcr7pp98xhg3xflfai98kwsar71hcix6a9if6")))

(define-public crate-simpleml-0.2.0 (c (n "simpleml") (v "0.2.0") (d (list (d (n "tree_iterators_rs") (r "^1.1.4") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.0") (d #t) (k 0)))) (h "0dc3x4gpm4za98a2jgp8i7s5bvy8a3gj4w8qhsxhlabgrmzihd2b")))

(define-public crate-simpleml-0.2.1 (c (n "simpleml") (v "0.2.1") (d (list (d (n "tree_iterators_rs") (r "^1.1.4") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.0") (d #t) (k 0)))) (h "0g3frq2k6zxha7wq1k9a9vzf5sva2ck1w9w34l7rqv6ab7qg6qj6")))

(define-public crate-simpleml-0.2.2 (c (n "simpleml") (v "0.2.2") (d (list (d (n "tree_iterators_rs") (r "^1.1.4") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.1") (d #t) (k 0)))) (h "0rhkpf1f61029g8vh49h3nvcddq5rkq1bcdcpah3wdsgcgn9741m")))

(define-public crate-simpleml-0.3.0 (c (n "simpleml") (v "0.3.0") (d (list (d (n "tree_iterators_rs") (r "^1.2.1") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.1") (d #t) (k 0)))) (h "1s18rq21bhql6himfgzd91zzvsffm17fcn1cpzf3ba6vng1fg6fd")))

(define-public crate-simpleml-1.0.0 (c (n "simpleml") (v "1.0.0") (d (list (d (n "tree_iterators_rs") (r "^1.2.1") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.1") (d #t) (k 0)))) (h "1j7s6pjlf95gj6jsw51blzwn0hxdgg8ga4dqppm3429bcv1jy98b")))

(define-public crate-simpleml-1.0.1 (c (n "simpleml") (v "1.0.1") (d (list (d (n "tree_iterators_rs") (r "^1.2.1") (d #t) (k 0)) (d (n "whitespacesv") (r "^1.0.2") (d #t) (k 0)))) (h "0z6b4vqhss8f371avc452zcghgdw7ryh2z54nxpq5h3yn2b91q2n")))

