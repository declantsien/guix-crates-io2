(define-module (crates-io si mp simple-vec-collections) #:use-module (crates-io))

(define-public crate-simple-vec-collections-0.1.0 (c (n "simple-vec-collections") (v "0.1.0") (h "00y2hqp320ld3gmrm107132m1ff9x0skrzqlnkb8x08rs8ir8qvl")))

(define-public crate-simple-vec-collections-0.1.1 (c (n "simple-vec-collections") (v "0.1.1") (h "1n66cc1i70dkqhff5n05w3n2pp2p5hlr89g4sqz1lkywr6jkh5hj") (r "1.65.0")))

(define-public crate-simple-vec-collections-0.1.2 (c (n "simple-vec-collections") (v "0.1.2") (h "1lg10zf1sglkwglb06dsmwxhrd63lw46zs9isjg9zbrhmaiqs1nl") (r "1.65.0")))

(define-public crate-simple-vec-collections-0.2.0 (c (n "simple-vec-collections") (v "0.2.0") (h "0qmy3q8f1kpgxp7cdhhmf4vy4940d85qijg6aakc0xfn41kl7r7g") (r "1.65.0")))

(define-public crate-simple-vec-collections-0.3.0 (c (n "simple-vec-collections") (v "0.3.0") (h "0pm0q0w0lr61ahafdzkjmx15jvjh1l5giziw9jqddvyw3g1llxkj") (r "1.65.0")))

(define-public crate-simple-vec-collections-0.4.0 (c (n "simple-vec-collections") (v "0.4.0") (h "138gvhhi6m90lzlv8hiz38asdx169cchh1f7988b8ig7yvh3wlfp") (r "1.65.0")))

