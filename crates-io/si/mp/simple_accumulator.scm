(define-module (crates-io si mp simple_accumulator) #:use-module (crates-io))

(define-public crate-simple_accumulator-0.1.0 (c (n "simple_accumulator") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1wis27bd51x3rz3j23mnwywwi0j0bspv7kcq1y4d089583v6q2bn")))

(define-public crate-simple_accumulator-0.2.1 (c (n "simple_accumulator") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1nkjfz7vd8y5q84vvzhwk5b5n46pfk2l6dwsk1g0hdrqhwqnk8zi") (y #t)))

(define-public crate-simple_accumulator-0.2.2 (c (n "simple_accumulator") (v "0.2.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ckp7qv6cgmfp65kpq0ipx7gf3sbnig8qn7bxabpm3zf30q4ggjk")))

(define-public crate-simple_accumulator-0.3.0 (c (n "simple_accumulator") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1z9ghq1ilwvb2g1lklgzb1100nj7vjwdh89ssbkza5cnzyypszrq")))

(define-public crate-simple_accumulator-0.3.1 (c (n "simple_accumulator") (v "0.3.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1k8diw81ix2rikfclqa39221ppvzwff1lvpz959i5q496h4csj2q")))

(define-public crate-simple_accumulator-0.3.2 (c (n "simple_accumulator") (v "0.3.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lc4waxj6b7h15rra5ankxsy62mkkz0f83h24mdn3fspd1l1arp4")))

(define-public crate-simple_accumulator-0.4.0 (c (n "simple_accumulator") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02g4xx6mzbiik3x90019aszahg83razdb3nd2h5mzms5cwzysccr")))

(define-public crate-simple_accumulator-0.4.2 (c (n "simple_accumulator") (v "0.4.2") (d (list (d (n "float_eq") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 2)) (d (n "plotly") (r "^0.8.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01x06mnxjs037nypfxz9hbwjnyjdm8q66qf3sp5n2n1fz0hmsvnh") (y #t)))

(define-public crate-simple_accumulator-0.5.0 (c (n "simple_accumulator") (v "0.5.0") (d (list (d (n "float_eq") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 2)) (d (n "plotly") (r "^0.8.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "016ydl4bxd9cf4fb1riqw9ps9qc6qvjwdqdj84js58rahsx9v46i") (y #t)))

(define-public crate-simple_accumulator-0.5.1 (c (n "simple_accumulator") (v "0.5.1") (d (list (d (n "float_eq") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 2)) (d (n "plotly") (r "^0.8.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ar7f80qwbzpd6cn46f1dmxa61r5x7gpdclw085pzksvndkmiv71")))

(define-public crate-simple_accumulator-0.6.0 (c (n "simple_accumulator") (v "0.6.0") (d (list (d (n "float_eq") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 2)) (d (n "plotly") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "watermill") (r "^0.1.1") (d #t) (k 0)) (d (n "watermill") (r "^0.1.1") (d #t) (k 2)))) (h "0kfn85h9dnk4cydr04dzg5cgbii7jhhylxn66m254gpsnx3wln1h") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-simple_accumulator-0.7.0 (c (n "simple_accumulator") (v "0.7.0") (d (list (d (n "float_eq") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "histogram") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 2)) (d (n "plotly") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 2)) (d (n "watermill") (r "^0.1.1") (d #t) (k 0)) (d (n "watermill") (r "^0.1.1") (d #t) (k 2)))) (h "08ywzbxawcmgx2rg3y1izig039iqjdmr8q63fapjs42amlip0sks") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("histogram" "dep:histogram"))))))

