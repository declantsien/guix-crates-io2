(define-module (crates-io si mp simple-discord-rpc) #:use-module (crates-io))

(define-public crate-simple-discord-rpc-0.1.0 (c (n "simple-discord-rpc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)))) (h "05ybs64imhpbqfsj3fkfjqwbqk1mivwy75qsmwmyrkssvi3gjbxv")))

(define-public crate-simple-discord-rpc-0.1.1 (c (n "simple-discord-rpc") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)))) (h "0795hr2fd5ynj0p5pck6rvz3w1dc5hpj2whj48rdgm6561a36zyb")))

(define-public crate-simple-discord-rpc-0.1.2 (c (n "simple-discord-rpc") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)))) (h "1rfvnzmm87d6bs011cf61lnf1chd3kyrlqqx8aw41xww09l9pdc2")))

