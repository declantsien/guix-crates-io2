(define-module (crates-io si mp simple_generators_macros) #:use-module (crates-io))

(define-public crate-simple_generators_macros-0.1.3 (c (n "simple_generators_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "simple_generators_adapter") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "199lqga8585r24175aqikcvblb9x3pc18jns1a40gvkpcmads47i")))

