(define-module (crates-io si mp simple-interpreter) #:use-module (crates-io))

(define-public crate-simple-interpreter-0.1.0 (c (n "simple-interpreter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zhm84gpvqf8qf9vygyviss10h0z9357yfxlki1w0kafadkxi11a")))

(define-public crate-simple-interpreter-0.2.0 (c (n "simple-interpreter") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1xbpkrx3jv911mkabbf3r7cs2ajy8bbg3iqz3sf33qkqnyyw9i6p")))

