(define-module (crates-io si mp simple_jwt) #:use-module (crates-io))

(define-public crate-simple_jwt-0.1.0 (c (n "simple_jwt") (v "0.1.0") (d (list (d (n "openssl") (r "^0.8.3") (f (quote ("hmac"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0iy48ml3q9nbns6y0w6m1c15qn971xa01q4x1cfylda5av6kg4kx") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-simple_jwt-0.9.0 (c (n "simple_jwt") (v "0.9.0") (d (list (d (n "openssl") (r "^0.8.3") (f (quote ("hmac"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1ylxind8s1np74446vzs4cqkbsbdcvxp2napfpqc2j569x6yrywb") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-simple_jwt-1.0.0 (c (n "simple_jwt") (v "1.0.0") (d (list (d (n "base64") (r "~0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fsffg4gbm4frajg1y3kc71y09k3sn9znp1h7fq650phkq69rxa6")))

(define-public crate-simple_jwt-1.1.0 (c (n "simple_jwt") (v "1.1.0") (d (list (d (n "base64") (r "~0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18p25bqi5xiw28fq6n62bpgczzrnc96zzknqfw9msp20jmgmc2y8")))

(define-public crate-simple_jwt-1.2.0 (c (n "simple_jwt") (v "1.2.0") (d (list (d (n "base64") (r "~0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ckw55gv1vkyzydhhmk9li3052n4rk0w10hykgf86f8r9pxbjwzr")))

(define-public crate-simple_jwt-1.2.1 (c (n "simple_jwt") (v "1.2.1") (d (list (d (n "base64") (r "~0.7.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aj0lkbw4kshjfpg6fsxpc2kdh1kqvfvh4f8d8xjsrjxl38hkw7q")))

