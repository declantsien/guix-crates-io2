(define-module (crates-io si mp simple_cache_core) #:use-module (crates-io))

(define-public crate-simple_cache_core-1.0.0-beta (c (n "simple_cache_core") (v "1.0.0-beta") (h "0gv5zvg9fmdnxr8138nccjcra2x7yxy1ixz4qbr6jcxbz4i5rk1y")))

(define-public crate-simple_cache_core-2.0.0-beta (c (n "simple_cache_core") (v "2.0.0-beta") (h "1kdzj7x0p2nkdpy9v2brdhn17qawdksb3y3xlrrr40gz5aqmczkq")))

