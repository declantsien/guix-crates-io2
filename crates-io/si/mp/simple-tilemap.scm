(define-module (crates-io si mp simple-tilemap) #:use-module (crates-io))

(define-public crate-simple-tilemap-0.1.0 (c (n "simple-tilemap") (v "0.1.0") (d (list (d (n "fast-srgb8") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "simple-blit") (r "^0.7") (d #t) (k 0)))) (h "0rw69fddv5kzy9h6k85sygjmfdcdclysdq62p7snalwhfjap88nb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "rgb/serde"))))))

(define-public crate-simple-tilemap-0.1.1 (c (n "simple-tilemap") (v "0.1.1") (d (list (d (n "fast-srgb8") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "simple-blit") (r "^0.7.1") (d #t) (k 0)))) (h "05g70kibdi65qzfbs163bkiw5xj9g014xxf4ykzxr33yp2s3njxh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "rgb/serde" "simple-blit/serde"))))))

(define-public crate-simple-tilemap-0.2.0 (c (n "simple-tilemap") (v "0.2.0") (d (list (d (n "fast-srgb8") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "simple-blit") (r "^0.7.1") (d #t) (k 0)))) (h "0fpj767w7niy47nlnj53q3i9gj4mcj68hjrwxz0yq3dqrqnyydm6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "rgb/serde" "simple-blit/serde"))))))

(define-public crate-simple-tilemap-0.3.0 (c (n "simple-tilemap") (v "0.3.0") (d (list (d (n "fast-srgb8") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "simple-blit") (r "^0.7.1") (d #t) (k 0)))) (h "00mkxm9gf75f5mww3dkjc5m4pxw6mx0qfcfn8ma44706yv12jh5d") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "rgb/serde" "simple-blit/serde"))))))

(define-public crate-simple-tilemap-0.4.0 (c (n "simple-tilemap") (v "0.4.0") (d (list (d (n "fast-srgb8") (r "^1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "simple-blit") (r ">=0.7.1") (d #t) (k 0)))) (h "105vd34n8x9i0r12hbdwcmj24d4wcys2vc5cdrpx96icjxzg7dc4") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "rgb/serde" "simple-blit/serde"))))))

