(define-module (crates-io si mp simple-cache) #:use-module (crates-io))

(define-public crate-simple-cache-0.1.0 (c (n "simple-cache") (v "0.1.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0sg8rl726yrsz0mv5cw7wl774k6pspryvwqls13k594yrx4zz8mx")))

(define-public crate-simple-cache-0.1.1 (c (n "simple-cache") (v "0.1.1") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwjhggrz0rz0rc4vb8gwb6k4789diazcqvx2swg2caj16l31s1j")))

(define-public crate-simple-cache-0.1.2 (c (n "simple-cache") (v "0.1.2") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0wj5r7xwn1f5y090jmsd0ag0xbmi6ghzxdfn0xh878ryjl6nhlx6")))

(define-public crate-simple-cache-0.1.3 (c (n "simple-cache") (v "0.1.3") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0mnbidaxn89i1wimppnvbsaqglgn65grf9d8hcdxjfxi285pajdx")))

(define-public crate-simple-cache-0.1.4 (c (n "simple-cache") (v "0.1.4") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "05zckxgfi64jxc4sdwi1fniv9lkwc9pawlj9n2z7x7aq3j2cjkzp")))

(define-public crate-simple-cache-0.2.0 (c (n "simple-cache") (v "0.2.0") (d (list (d (n "arc-swap") (r "^1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 2)))) (h "13vhkahklh9mp4nxbzk4bf03kqppwyai2za18m0ab9g7w3inz6vw")))

