(define-module (crates-io si mp simpath) #:use-module (crates-io))

(define-public crate-simpath-1.0.0 (c (n "simpath") (v "1.0.0") (h "12zfxhn71qx4k806q835c7ivhg206ppbh3yslzs0fk3mxg8i6xz8")))

(define-public crate-simpath-1.1.0 (c (n "simpath") (v "1.1.0") (h "03i3qzl7pmj8p7w5k01i63khj54w9s1wih3zzylsl131f4k7vbfa")))

(define-public crate-simpath-1.2.0 (c (n "simpath") (v "1.2.0") (h "0aji37gp4395lsxqiir4cl0ps59hpbks7v33348yzh2h5p22jk7a")))

(define-public crate-simpath-1.3.0 (c (n "simpath") (v "1.3.0") (h "1w9kjnny2h0k9xgz19m07r0cijx2wp1lrcd3zq0a8y4i8xqrxwaz")))

(define-public crate-simpath-1.3.1 (c (n "simpath") (v "1.3.1") (h "0fjz6slbxsl3dj6xrzmfv3fc2ia57r4ncghgmcjv902wj6agfqn8")))

(define-public crate-simpath-1.4.1 (c (n "simpath") (v "1.4.1") (d (list (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)))) (h "1jn62971vrnmw4vrawa8cw0rcnvrys9q8g9dq3brjnjbr40ngszv")))

(define-public crate-simpath-1.5.0 (c (n "simpath") (v "1.5.0") (d (list (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)))) (h "19px1gnanq1f40gy9is78c6d2l6ll98ilxv3rbgj60sgbc94mds2")))

(define-public crate-simpath-2.0.0 (c (n "simpath") (v "2.0.0") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 2)))) (h "1a6yblrdg6wipqj2fkcl712j3jky3l9d0wlaz3cknwxidqn8rr4x") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.0.1 (c (n "simpath") (v "2.0.1") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 2)))) (h "1k64gjxdp6j3jw5nj1678npbw44xvfdnzsb3hcx9qj14pawhwb4v") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.1.2 (c (n "simpath") (v "2.1.2") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 2)))) (h "02aa5jw05y1ciw4p1yxjjlpf720j7rjh5hmi06cnqfhsrbhsn70g") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.1.4 (c (n "simpath") (v "2.1.4") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 2)))) (h "02i1lzmp1glspfgzp5h1i6cmmh2jrvwpxnkd6nmw8crpf51x3ckq") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.1.5 (c (n "simpath") (v "2.1.5") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 2)))) (h "1biirw2vwravp59dwdbl20ps2mvhwymzz8iqzhpd0nmia5ihjq89") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.2.0 (c (n "simpath") (v "2.2.0") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "0vnzfx29mm4zp3q8ybwwkjllscdpj7m6g85azfikc7lyd6snwxsc") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.2.1 (c (n "simpath") (v "2.2.1") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "0kbkhs3154hj2c57njnn3cp2wciz6gc61yki7ypr8laap8hiakds") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.3.0 (c (n "simpath") (v "2.3.0") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "1ajfp9m17jvgx8vlf960rkdj5298mrfdvsv7hq8wm1zswzlfmcyd") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.4.0 (c (n "simpath") (v "2.4.0") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "137hzj1ys9kaz2351zv4jh8zq1s80ha2sirg65phfs4zd94v2sv8") (f (quote (("urls" "url" "curl") ("default"))))))

(define-public crate-simpath-2.5.0 (c (n "simpath") (v "2.5.0") (d (list (d (n "curl") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)) (d (n "url") (r "~2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "14zz1546xzdw6m7g1ral3yqcxbd9xvc5arrzz52v427y0wqwpl73") (f (quote (("urls" "url" "curl") ("default"))))))

