(define-module (crates-io si mp simple-pipeline) #:use-module (crates-io))

(define-public crate-simple-pipeline-0.6.0 (c (n "simple-pipeline") (v "0.6.0") (d (list (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01jgbkf7b37ghs6j3qnl9wg7qzgahfxbaxy7v71n510bmsphg3bd")))

