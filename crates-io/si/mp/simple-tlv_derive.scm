(define-module (crates-io si mp simple-tlv_derive) #:use-module (crates-io))

(define-public crate-simple-tlv_derive-0.1.0 (c (n "simple-tlv_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0wdvk4gqzhiv3ikv4anwa6c64k9xlzxjcyr4v2grwiyp3nb122qf")))

