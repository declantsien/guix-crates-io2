(define-module (crates-io si mp simplecc) #:use-module (crates-io))

(define-public crate-simplecc-0.1.0 (c (n "simplecc") (v "0.1.0") (h "0kkhbcln47pajhh6r1dj7hfa930r2rk59m3cf3dzrg2nvsyzg0wg")))

(define-public crate-simplecc-0.1.1 (c (n "simplecc") (v "0.1.1") (h "18awkkymij7g4aph500wsylgzi0hmqkcnwnl5pkrv26f83ka231l") (f (quote (("default") ("builtin_dicts"))))))

(define-public crate-simplecc-0.2.0 (c (n "simplecc") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0g4svpg0wqghfy3438qc33g2ns29q91n3lajh2b5s4sv7akgjdg6") (f (quote (("default") ("builtin_dicts" "lazy_static"))))))

(define-public crate-simplecc-0.2.1 (c (n "simplecc") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0bc6nvnrq8ch00avgqsi93s7diwvhn16cvfa0nyh5pbvsjyqcjpk") (f (quote (("default") ("builtin_dicts" "lazy_static"))))))

(define-public crate-simplecc-0.2.2 (c (n "simplecc") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "18jgy7wnqql0j82hapak55nk71afd9g5llfm0qzng5vbicr7q901") (f (quote (("default") ("builtin_dicts" "lazy_static"))))))

