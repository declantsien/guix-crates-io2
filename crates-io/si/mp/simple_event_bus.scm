(define-module (crates-io si mp simple_event_bus) #:use-module (crates-io))

(define-public crate-simple_event_bus-0.0.1 (c (n "simple_event_bus") (v "0.0.1") (h "0lm1520308bhdk19fypp1l8imcrfndxnwrd84z37arhqd44ggf3d") (y #t)))

(define-public crate-simple_event_bus-0.0.2 (c (n "simple_event_bus") (v "0.0.2") (h "0vljgxbnncqy9467l228ar42pynf2ix91jfcm2yc0qrd5mjav520")))

(define-public crate-simple_event_bus-0.0.3 (c (n "simple_event_bus") (v "0.0.3") (h "1m8yls64v30410dcxivq6xrxlq4gfp43cid3iyd10sa85vysl7gy")))

(define-public crate-simple_event_bus-0.0.4 (c (n "simple_event_bus") (v "0.0.4") (h "113xpnasmgigbyn3l4cdsxqi85069yv0qmjr84ydnyvbi56qnv08")))

(define-public crate-simple_event_bus-0.0.5 (c (n "simple_event_bus") (v "0.0.5") (h "0irj20w79xxflykqckj362x6q5mxqmc7psm6y06c6lvj7q2qgzvd")))

