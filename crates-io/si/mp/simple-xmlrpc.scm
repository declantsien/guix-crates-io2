(define-module (crates-io si mp simple-xmlrpc) #:use-module (crates-io))

(define-public crate-simple-xmlrpc-0.1.0 (c (n "simple-xmlrpc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "iso8601") (r "^0.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "052r2qgg80hhfqd3rzfvbiji69c1f8yslz322s1hkb5bv9jc14h1")))

(define-public crate-simple-xmlrpc-0.1.1 (c (n "simple-xmlrpc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "iso8601") (r "^0.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dknjx5ma1hh9gsglkbvwky86amwrjb5vvs1blfzmhs0gvqs66j1")))

(define-public crate-simple-xmlrpc-0.1.2 (c (n "simple-xmlrpc") (v "0.1.2") (d (list (d (n "serde_xmlrpc") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xcr3wcpx4r1sfap00m2j238rkiipihasfbg3gl5xg9471pl0yd9")))

