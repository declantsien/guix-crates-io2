(define-module (crates-io si mp simp_linalg) #:use-module (crates-io))

(define-public crate-simp_linalg-0.1.0 (c (n "simp_linalg") (v "0.1.0") (h "1ilcj3c9hfbslzkfl9f219gilmzka6plrnz2p59lfm204v3xk542")))

(define-public crate-simp_linalg-0.1.1 (c (n "simp_linalg") (v "0.1.1") (h "06xjmkd498a9hd1bvh93f60a5qfbcqbkn5v4s65qky2zb7qvgbn9")))

(define-public crate-simp_linalg-0.1.2 (c (n "simp_linalg") (v "0.1.2") (h "0xn76gf8h2p2zjfgw876xna93g2b18bsdzngcqwlnwb8hdf59c2g")))

(define-public crate-simp_linalg-0.1.3 (c (n "simp_linalg") (v "0.1.3") (h "0k7qcbp3w4hgsvlyh1hwwvnnyk8m1ii93za68lpfna48q2ls9zqy")))

(define-public crate-simp_linalg-0.1.4 (c (n "simp_linalg") (v "0.1.4") (h "1ibx951nvpm3xfpr8dmgmgnls152d0ijm9d42kvd6i7v2qlvbylz")))

(define-public crate-simp_linalg-0.2.0 (c (n "simp_linalg") (v "0.2.0") (h "0lviz7fnkq3q60kqygv1j8jahd5nxyc9qf75bnadlw1g4d3l0x2n")))

