(define-module (crates-io si mp simply-fuse) #:use-module (crates-io))

(define-public crate-simply-fuse-0.0.3 (c (n "simply-fuse") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "polyfuse") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.9") (d #t) (k 0)))) (h "1gwcl27yfkpsprjnbvvkif5ywghsdl0bv8dad4swlk1if4mff3ds")))

(define-public crate-simply-fuse-0.0.4 (c (n "simply-fuse") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "polyfuse") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "typed-builder") (r "^0.9") (d #t) (k 0)))) (h "0zxb5q6f0x7gxfxc7a7vkbffr3fm11asq05zfpp0ka3x9964dy9y")))

