(define-module (crates-io si mp simple_redis) #:use-module (crates-io))

(define-public crate-simple_redis-0.1.0 (c (n "simple_redis") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1rwv13gy1bv46hzppqrqxf44nfisvayzyk1ff6xrn1gm1s2g8mz1") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.1 (c (n "simple_redis") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1kp6vbmqi9kbikd79wd8x1hcbv9xl7zp52a6clmrw2756kdnarbm") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.2 (c (n "simple_redis") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1hi56n027rq5vjn17g5m78qgrhaykjpjx8q8nx2lw852x0c0kw13") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.3 (c (n "simple_redis") (v "0.1.3") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0xgg92kzl00z20p4xlpkhc4ah51xln2n4w2rdy1c4wsnwgnm438j") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.4 (c (n "simple_redis") (v "0.1.4") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0qkpcc47y47lzww1wyj42jj3rmkyrxk5nxislffnjqnnkqz6kp50") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.5 (c (n "simple_redis") (v "0.1.5") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1z4qqymv75hyis7djdqqfc14ksq991cgc6k1h5d20c8gb54hcjdg") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.6 (c (n "simple_redis") (v "0.1.6") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "03xf5g639al5wf04ils28h1qi556c14r6dxhzzcmici1aga9933b") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.7 (c (n "simple_redis") (v "0.1.7") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0papzxyhw706a7hqfjmc4skw017zqygbnisr1zqwkq2aqhjcwx4i") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.8 (c (n "simple_redis") (v "0.1.8") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0gqzycsvk93rxl1axg0vdg0z03ql2ccvc68jkrrjq5isi3hfqa4k") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.9 (c (n "simple_redis") (v "0.1.9") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "17694f0sl4q3j37iiz2z5l0gb7c28zfl072a48cps23gjgr6iwjk") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.10 (c (n "simple_redis") (v "0.1.10") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "08azsy3ipd7fccdafphwg92fj25jn0yb7c8chi1izaqrdk7vbrq6") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.11 (c (n "simple_redis") (v "0.1.11") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1pyhcry0cnrabbcn1kzjbpfmy8fb53yfqw2kr5mbxlc08fz3j0pa") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.12 (c (n "simple_redis") (v "0.1.12") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0rl7mfgjmnm0vlhvngrw141f616rvsq97ad4yzmzsrlz9lwy5vfq") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.13 (c (n "simple_redis") (v "0.1.13") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "18viv3pr3izqg8waaja6cmpgck7grzwyaz0jrrv1hhlnaqisw5i5") (f (quote (("default"))))))

(define-public crate-simple_redis-0.1.14 (c (n "simple_redis") (v "0.1.14") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "08sg5w7d61c28a434q6g68akhcrjs8c6zqz5vhck4rbf0swcmclr") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.1 (c (n "simple_redis") (v "0.2.1") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1m75c2kpr0iilzxrqhb8ilxf7b2nmdfx0aclcs2d1isa3i8zg6p0") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.2 (c (n "simple_redis") (v "0.2.2") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1sbl6bqfrm4b2yrl4d4rkxx7446bcm1f424n6964p001g0bmnc79") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.3 (c (n "simple_redis") (v "0.2.3") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1lq7qiiqriizi2r1fxvj5j318mvf4bzk37jb6n76sklw2z5lk0sf") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.4 (c (n "simple_redis") (v "0.2.4") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "092nqv429pdw8q21sffiffm7w0b8c4qqib8z94im5mnkc35aj81q") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.5 (c (n "simple_redis") (v "0.2.5") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0ivbvwcm9clz0cf9cg7bzkn9cmcycakfr5b9ixqiv6naqsgp5m3n") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.6 (c (n "simple_redis") (v "0.2.6") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0jra8gxg87w1yg4lhn3nkmq1pc5aayfmala413fa1iaaxfnx0kqs") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.7 (c (n "simple_redis") (v "0.2.7") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0x7kn7zmzv5643fbgkziy458nm1sbajrc1kfirlqis15shh7phb8") (f (quote (("default"))))))

(define-public crate-simple_redis-0.2.8 (c (n "simple_redis") (v "0.2.8") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0wlhy2prv12hdij3rdzag7fvg2az4l7gaj4kcki8afqwdbh4zx7k") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.0 (c (n "simple_redis") (v "0.3.0") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0a8iddjsi3l8q8iv4nhk2k3xxnd7la4g3pkfa66g7rk9fl8wl72j") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.1 (c (n "simple_redis") (v "0.3.1") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0655i4bqhmq13grin0zg7bcrh5pl4g9b2cj9diwxrz8drk0851dx") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.2 (c (n "simple_redis") (v "0.3.2") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0q189zy008fc4gjp4gv098zfb3fxxbk0y8prishlymq5rrcygk3x") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.3 (c (n "simple_redis") (v "0.3.3") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1ymf81k4cf9iprqj8qj4g2hb39q0l695jnb3la2b8xgqj23nhv0l") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.4 (c (n "simple_redis") (v "0.3.4") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "10msb1pswm85w6pyxlhq4g974ipzv3vz0664xrc3j54j212qz4x2") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.5 (c (n "simple_redis") (v "0.3.5") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "046rzf547gzvj38bynx92ar2g7rsyz8ibb54kvm7av66n3q92pim") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.6 (c (n "simple_redis") (v "0.3.6") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1b2ply08pna691ymxk2hivs686w4qs7k5g26ayljzljhg3ajps5m") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.7 (c (n "simple_redis") (v "0.3.7") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1pfrpxh9rf6vpc5rz4rmp56lb71gk1cx63bhr166w2d8ddmcv6n4") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.8 (c (n "simple_redis") (v "0.3.8") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "06wgknwiv3hyc2d0fvj1a845l0cs3jpqm0s8n45fi79blfngglvk") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.9 (c (n "simple_redis") (v "0.3.9") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1s3487df5qikyjhk5qglrrpmnnn5nxlhnbx543382kbwlnhxjrnc") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.10 (c (n "simple_redis") (v "0.3.10") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1jyyy4qjyrf3c0gz30dnih07q1b76j903x6nl94bzpbdqi801n00") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.11 (c (n "simple_redis") (v "0.3.11") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0fw0mjjpa7andracj3g6dnwg39rxq7q11r988b4pldjj7rbbxg8h") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.12 (c (n "simple_redis") (v "0.3.12") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "11bf7i3rca432dlqnd73kg98xh3ggvjd2kdsx8z4nqqqnf80km2i") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.13 (c (n "simple_redis") (v "0.3.13") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "151jirwwnp00cifzcp36rm1vidp1xr0z5jb549r5d3s4q1bq4j40") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.14 (c (n "simple_redis") (v "0.3.14") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "13wr7ldfzllj7qbd8rz87dmpjk4kz1dmgsz648l1cdzqfyrcg4wh") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.15 (c (n "simple_redis") (v "0.3.15") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0g74d45sq57wklsz7vz79hp36dlfn0lfiz0px8ncsmkszd4asyg0") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.16 (c (n "simple_redis") (v "0.3.16") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "18kxy9qggrhg37fwlrdsks9shqr2a5a65p4czli59vc6g5bkl8n0") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.17 (c (n "simple_redis") (v "0.3.17") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "10mr4iix0l3cxdq9z1sfdpb069as39wr4qs3hpizfv4c0b8d83j3") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.18 (c (n "simple_redis") (v "0.3.18") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1j2ilw22mzrqzgjddwz8y132ffhwy4swxxgq883v1yjhi56i2gdz") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.19 (c (n "simple_redis") (v "0.3.19") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "04c0qhaasabsnl847288rcvhgkg6g71ngam6gp5ikh8hqiv9dsx1") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.20 (c (n "simple_redis") (v "0.3.20") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1y3z2f4pr8wavpwl7v9hmn5c5yn1y26khv93qbnm59b67pwzblsp") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.21 (c (n "simple_redis") (v "0.3.21") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0fsgd86x85bp86s1a8g9p51g7wpsxv1n6v2nwxhlwl6h8fxsnxh4") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.22 (c (n "simple_redis") (v "0.3.22") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1k8a7iy334c3d4nk8pdp1m490amhfssy8ync6jzjqi7k19bdhdrk") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.23 (c (n "simple_redis") (v "0.3.23") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1rd08vh7c69grhm5vijc4641ki6gsb5gb32d2ibwmsxv8ssxzn84") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.24 (c (n "simple_redis") (v "0.3.24") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1pvaq1498z81a9hczs7mbjxivlnig5qvikwdrkzk84rfg88wklnl") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.25 (c (n "simple_redis") (v "0.3.25") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0r2z8vz2qlwychqy2lljk7inaqjk7fid5il6kqwk8c6gxxh59ab2") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.26 (c (n "simple_redis") (v "0.3.26") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1hp8f6lzr4nlnq6sgklc28zgnicffw550kl94yh3nzdw4kmcc754") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.27 (c (n "simple_redis") (v "0.3.27") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0d146l7b3cmdk10x3gsqakv3w4rvflcq4xajsjmh3v93b87n5842") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.28 (c (n "simple_redis") (v "0.3.28") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0awn1v8ffa4gx6pk53rab33476q3rsks3854qhh12wawi81bvmqm") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.29 (c (n "simple_redis") (v "0.3.29") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "16gdlk0h65wvh8lgq8sdc3v2mgh4s0d8rzn1sc0dm4402xg5qs5i") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.30 (c (n "simple_redis") (v "0.3.30") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "05hyakrqca07bik10h89563x8xf7kkfs1vwydvk1jrayl8b8kmvs") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.31 (c (n "simple_redis") (v "0.3.31") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "06i7fssh078cy346qjgw30ypi7sxslyivqihd2p764wbwzf1pxy2") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.32 (c (n "simple_redis") (v "0.3.32") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0hgb3bl2a4wpywd5qsn5rlgvn5r6p4vwjsiadq6cq695jrj26315") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.33 (c (n "simple_redis") (v "0.3.33") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1s45d8nnwlgjapql5vs8dhpswrlrx6b84zwza51x6b70qzf0ynbz") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.34 (c (n "simple_redis") (v "0.3.34") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1cvc13p55pqxp3q0c6jzf1wg21m44693j4fnkchfx876gp89478b") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.35 (c (n "simple_redis") (v "0.3.35") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1qhs1w0yc6hsqvj1qqy94qg94rck4h6xqq0662kxmb1lv3d6g3lf") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.36 (c (n "simple_redis") (v "0.3.36") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1wcprhg6if999lzwvz3xa5im3s3akgdqnqd5yccdcl14vk094a8z") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.37 (c (n "simple_redis") (v "0.3.37") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "18wjdqsmnv2iwybwci8h3n1ri8nh88kjg5wljkwq3apq1dnbwgp4") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.38 (c (n "simple_redis") (v "0.3.38") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0hx0zin81lx0sp2pnljmfl2j8xb5rkz0c3fsfsfh28l2qq7gpm80") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.39 (c (n "simple_redis") (v "0.3.39") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1ggn7yy822m0cix26lyq9ja0340nvm8m9aavyw8njwv62jsm3jhx") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.40 (c (n "simple_redis") (v "0.3.40") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "02cd0qigz0ghqsp05dp8llzcb20yn80lsab07hcsb892d9z8dab9") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.41 (c (n "simple_redis") (v "0.3.41") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "14y2dpxz94wi0gwbjwn6r40kxhfm4lxd7zrx1jz15cqpv1n565yc") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.42 (c (n "simple_redis") (v "0.3.42") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0ga5zzxz9k89ihwsl6jzm0wwgm6dpfr92yz0gckiw7qr3b0x89wz") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.43 (c (n "simple_redis") (v "0.3.43") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "14lg8fyhbsdyr6pj682mwk9p1n21gikiijfzgaqk4gcc9d2xhc22") (f (quote (("default"))))))

(define-public crate-simple_redis-0.3.44 (c (n "simple_redis") (v "0.3.44") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0hifydcip4x1bbx1sbmwbhvm56i4wmcxd2ia792nraz528zlfzqz") (f (quote (("default"))))))

(define-public crate-simple_redis-0.4.0 (c (n "simple_redis") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.10") (d #t) (k 2)))) (h "12k0iickfsv29rdj1y67w08khw9p1nqn7wlyss8z1zc6rlm5v1xc")))

(define-public crate-simple_redis-0.5.0 (c (n "simple_redis") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.15") (k 0)) (d (n "rusty-hook") (r "^0.10") (d #t) (k 2)))) (h "1jx6p56jpn1nk7zr40h25fa7mc9pv972v2lv18k0fyrqk2b27ak1")))

(define-public crate-simple_redis-0.5.1 (c (n "simple_redis") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.16") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0kqqlmd7sw2nm2npg4fgmhcv5b8kml24sz4nvazmdbmfr7dacc9d")))

(define-public crate-simple_redis-0.5.2 (c (n "simple_redis") (v "0.5.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.17") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "1v393ysj9czka72s6nbbvb5mihss5nx8m93cs5iaxwb1b4k0nvd3")))

(define-public crate-simple_redis-0.5.3 (c (n "simple_redis") (v "0.5.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.18") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "02bddg80j6z38s5f82r0q77vf2c48y3fwvb984j9lzivqrm88hmf")))

(define-public crate-simple_redis-0.5.4 (c (n "simple_redis") (v "0.5.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.18") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0jinlwqg6p0p6qqx9mf9qbxxbwkrr6v6fwl1hksqmwjg6qy8jarg")))

(define-public crate-simple_redis-0.5.5 (c (n "simple_redis") (v "0.5.5") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.19") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0kxa2j1nv9i6c618nnznc37dbpkdhhn66651fmrk1fcxzfd7xnyp")))

(define-public crate-simple_redis-0.6.0 (c (n "simple_redis") (v "0.6.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.20") (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "18k9hsr3vx7xnkfcpgr6n490kg1fkibjjcyv119bia1cvg421p4y")))

(define-public crate-simple_redis-0.6.1 (c (n "simple_redis") (v "0.6.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.21") (k 0)))) (h "0ja0pc4a5gj21f6wzqjs4ksfw27z9h19wrql1l39xa1aizzb36fx")))

(define-public crate-simple_redis-0.6.2 (c (n "simple_redis") (v "0.6.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.23") (k 0)))) (h "1y3np59zsq5ca8kprk4z2271jga1i97ysm807ik3gqyk2dn89b88")))

(define-public crate-simple_redis-0.6.3 (c (n "simple_redis") (v "0.6.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.25") (k 0)))) (h "0v0pf1fib5h8is1f7cla3i8bhkvilw6sq4rg8pffm0b6s788q5i5")))

