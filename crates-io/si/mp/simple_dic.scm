(define-module (crates-io si mp simple_dic) #:use-module (crates-io))

(define-public crate-simple_dic-0.1.0 (c (n "simple_dic") (v "0.1.0") (h "1vb6njbndljhpzxzgjxv35z35bx2spkqphrm7mmbmy1sf328ar6s")))

(define-public crate-simple_dic-0.1.1 (c (n "simple_dic") (v "0.1.1") (h "0sq15ys9q98bjs1yg3i044ljqnb4c8imba3frq5j9zik7lfr3q0z")))

(define-public crate-simple_dic-0.1.2 (c (n "simple_dic") (v "0.1.2") (h "11y4gg9pidrp1yyhncn1bv8d9p2pxackbcnzk6sc8zk5kgzq9d3i")))

(define-public crate-simple_dic-0.1.3 (c (n "simple_dic") (v "0.1.3") (h "1csgvb0yqw877rnfx1i12nf9jkbplsp59m3fgpf8n73icl085bvb")))

(define-public crate-simple_dic-0.1.4 (c (n "simple_dic") (v "0.1.4") (h "1yhpqhmsrp15anb84qzzp58b2agpyhlcsf51i544dbp6y90gxpdj")))

