(define-module (crates-io si mp simplerand) #:use-module (crates-io))

(define-public crate-simplerand-0.0.1 (c (n "simplerand") (v "0.0.1") (h "0anr0v7jk32rj2hd076in83dzh27540640i4v0yn4v7p7s0ba2sc")))

(define-public crate-simplerand-1.0.0 (c (n "simplerand") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1b22m1shh6a87v5d04f2gxf0x7ydxi990gi1l7lxw1qw5xr4bh8r")))

(define-public crate-simplerand-1.0.1 (c (n "simplerand") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09sf6zpd2gi3kw946bmikncypm8gpm3abbx20fm1kdm99l6h6l9c")))

(define-public crate-simplerand-1.1.0 (c (n "simplerand") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1lpl6r2gny9k41nq3wr2pfp8mfd4f2mf8if2lb8nd77x1zrvzfc6")))

(define-public crate-simplerand-1.2.0 (c (n "simplerand") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1n62rk20clxikd29ypfqnwf44fqn5azjhwpsyj5fb7pxwqj8jfc8")))

(define-public crate-simplerand-1.3.0 (c (n "simplerand") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yw9p3sk5c57gwz60ydpvsbx9kkwyccwlcv0z3q4v66b4455libh")))

