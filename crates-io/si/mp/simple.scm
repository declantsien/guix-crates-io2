(define-module (crates-io si mp simple) #:use-module (crates-io))

(define-public crate-simple-0.0.1 (c (n "simple") (v "0.0.1") (d (list (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "029qa0m9mbh8f1b46ph6rwd4fix4fmbll9q043s7bgj33bhv4fhk")))

(define-public crate-simple-0.0.2 (c (n "simple") (v "0.0.2") (d (list (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "1x0h36p4syfxd3ahm0bcyvwhdvadyrkyk17j4gznxs8g67yrryxf")))

(define-public crate-simple-0.0.3 (c (n "simple") (v "0.0.3") (d (list (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "168jk35z86mb7kp3iqlshpwkav6hq975m7vpkp48m8pl539k71g7")))

(define-public crate-simple-0.0.4 (c (n "simple") (v "0.0.4") (d (list (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "1di9i8s523yb3wsr92kl1f5586nrjhiaj3q5zbsg0in3wg3gs7fc")))

(define-public crate-simple-0.0.5 (c (n "simple") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "0jqag6b05qxivpmc41h8cr8fyc2by94mkhydn9dgwv0dm265wwbb")))

(define-public crate-simple-0.0.6 (c (n "simple") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "1pyzsmn2q1ik92sbkzbqbm9syd4cqcaccikziximan930w2i9q70")))

(define-public crate-simple-0.0.8 (c (n "simple") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)) (d (n "sdl2_image") (r "*") (d #t) (k 0)))) (h "1xj8n7rj1s0nlpi6x1mk6glcif5wcxjrldhjz618gvgx3zv4k5wr")))

(define-public crate-simple-0.1.0 (c (n "simple") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)) (d (n "sdl2_image") (r "*") (d #t) (k 0)))) (h "0q2mcx2pnvxynr702add7gvw6dv2d9b1j0bjf3gd5d14s3808msc")))

(define-public crate-simple-0.2.0 (c (n "simple") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "*") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)) (d (n "sdl2_image") (r "*") (d #t) (k 0)))) (h "0fpjwd9mlld4iyyw5nkdippahphm2ixdg14xbawrxmgc6vhlbzp3")))

(define-public crate-simple-0.2.1 (c (n "simple") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.24") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.2.1") (d #t) (k 0)))) (h "1asjdxj0wf7ay927r525ax6mllr819qqznfr4s51xdyg8cy20rhk")))

(define-public crate-simple-0.3.0 (c (n "simple") (v "0.3.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.24") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.32.1") (f (quote ("image" "unsafe_textures"))) (k 0)))) (h "11v0d1ncmma16343j7ivjb31skp37pn0xm0mil44khyg1mw1z0ma")))

