(define-module (crates-io si mp simple-task-timer) #:use-module (crates-io))

(define-public crate-simple-task-timer-0.1.0 (c (n "simple-task-timer") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1ba20hl9n36j7i6kskcrkfv5xy61plkz4qsfwhn6iav67wzgy0cq")))

(define-public crate-simple-task-timer-0.2.0 (c (n "simple-task-timer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1wcm3c20457dx6vqpgzk8pk2mvfdmq7c6qwpnp8crlsj0vq40rg5")))

