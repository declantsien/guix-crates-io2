(define-module (crates-io si mp simple-ref-fn) #:use-module (crates-io))

(define-public crate-simple-ref-fn-0.1.0 (c (n "simple-ref-fn") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "104ixww6qk2qk6y4x673b3bzz9ja669sw7jxh0h2i34rljk4bs9l")))

(define-public crate-simple-ref-fn-0.1.1 (c (n "simple-ref-fn") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1aif9nz3rmzy7x1a8lr8x8bmj6a2s3k9z581vvpc9qc60019ifqg")))

(define-public crate-simple-ref-fn-0.1.2 (c (n "simple-ref-fn") (v "0.1.2") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0fphgqkq969gin0npxzi4g5mppll8nximzdm3dzrinlk12zzlk74")))

