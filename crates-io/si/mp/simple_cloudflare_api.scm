(define-module (crates-io si mp simple_cloudflare_api) #:use-module (crates-io))

(define-public crate-simple_cloudflare_api-0.1.0 (c (n "simple_cloudflare_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vcygl90zsydqhcjf2ydas34anz8dng62w8dzxbhknlf0nl6zgfv")))

