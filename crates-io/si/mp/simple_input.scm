(define-module (crates-io si mp simple_input) #:use-module (crates-io))

(define-public crate-simple_input-0.1.0 (c (n "simple_input") (v "0.1.0") (h "0arg0hmwl2s5y32h7wxwyajk6ifjly4b7kvxv53559gykzxfbs6h")))

(define-public crate-simple_input-0.2.0 (c (n "simple_input") (v "0.2.0") (h "1zxrsb0krhz50dfc57myccmvfpy63wrm8r8b7gzyr9qb5s958ymp")))

(define-public crate-simple_input-0.3.0 (c (n "simple_input") (v "0.3.0") (h "0wjblba8r2hc47la57mzcknnmhw9yq4nwkz55zv9x11pwnsl797v")))

(define-public crate-simple_input-0.4.0 (c (n "simple_input") (v "0.4.0") (h "06wqpd0ahsvln4hzmxk9zvg9x6f54m3y4zbanqr20irgyh4sjhgs")))

