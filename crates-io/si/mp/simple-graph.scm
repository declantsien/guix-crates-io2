(define-module (crates-io si mp simple-graph) #:use-module (crates-io))

(define-public crate-simple-graph-0.1.0 (c (n "simple-graph") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zsqsw9vzinv08bri9i75rybqilg4w1njvhk2b7ji7ldq91sncpz")))

(define-public crate-simple-graph-0.1.1 (c (n "simple-graph") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zypm8j9r2vhhvm2zwz8j9b09fvr6pv08qwafnpg0ky3j4q5haw8")))

