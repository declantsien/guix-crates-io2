(define-module (crates-io si mp simple-geom) #:use-module (crates-io))

(define-public crate-simple-geom-0.1.0 (c (n "simple-geom") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1amw6d203lc3yz3f2kiny18mmg2v51da483vyschyg7j9p2vd21k")))

(define-public crate-simple-geom-0.1.1 (c (n "simple-geom") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0fwck67p93r7f1lckfxbddl0zviv2zc8w4282qx9clysybm7h4av")))

