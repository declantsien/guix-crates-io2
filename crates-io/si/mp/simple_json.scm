(define-module (crates-io si mp simple_json) #:use-module (crates-io))

(define-public crate-simple_json-0.1.0 (c (n "simple_json") (v "0.1.0") (h "079rkk3a8f019jkkp0fp8hmv5npwvhl6lgb6abhqph6jx9gq8d2d")))

(define-public crate-simple_json-0.2.1 (c (n "simple_json") (v "0.2.1") (h "101zzkr7nvcmsicz0dqcravpbs8zdxn4s06803d61xp0zmvscn79")))

(define-public crate-simple_json-0.2.2 (c (n "simple_json") (v "0.2.2") (h "1la4hx8amxs9bcahkq91qhg4j4y9r0fa2af1ydfnaaaf03dyzwgs")))

(define-public crate-simple_json-0.2.3 (c (n "simple_json") (v "0.2.3") (h "1hm53q7zqlwhdkb1nzyajc90n7lswprm37lxmb15p4shl4dm85ba")))

