(define-module (crates-io si mp simple_hll) #:use-module (crates-io))

(define-public crate-simple_hll-0.0.1 (c (n "simple_hll") (v "0.0.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "borsh") (r "^1.2.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kx1gyhdjf8y031yjd55xv55rswvawbhmk72awgjjsmx2ds57p5v") (f (quote (("serde_borsh" "borsh" "serde") ("default"))))))

