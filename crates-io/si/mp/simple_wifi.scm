(define-module (crates-io si mp simple_wifi) #:use-module (crates-io))

(define-public crate-simple_wifi-0.1.0 (c (n "simple_wifi") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "16rf4hqa06br82p84r3pqnlrsxdwr2lw3qqs062nks4gsi7x07mm") (y #t)))

(define-public crate-simple_wifi-0.1.1 (c (n "simple_wifi") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "195r39nbrl8nw55a292j8827kg28bba6w4d5aj47x12ma2adyvxc") (y #t)))

(define-public crate-simple_wifi-0.1.2 (c (n "simple_wifi") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "0sn6fjwdrf489s3mrk803cf63x0svi4fsm6lv4pg67pza0m8zlr6") (y #t)))

(define-public crate-simple_wifi-0.1.3 (c (n "simple_wifi") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "01gccfg7403676mmby988yc0fbz27bnlzx68qqjviwklbmli3ykc")))

(define-public crate-simple_wifi-0.1.4 (c (n "simple_wifi") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "0y36bmzsc2z7f5x300psm9b16zkvzjsxrkvsvwprpn5hsj5501w4")))

(define-public crate-simple_wifi-0.1.5 (c (n "simple_wifi") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "1j7k1g64w5l3g76nmp907fnsf7sjn16nhw9l2ikqil4b5s8fnmpp") (y #t)))

(define-public crate-simple_wifi-0.1.6 (c (n "simple_wifi") (v "0.1.6") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "165qbcaw5f01cwhgxx3hh982k7jv6r2b58l79byhkq0c1aighdh8")))

