(define-module (crates-io si mp simpler-git) #:use-module (crates-io))

(define-public crate-simpler-git-0.1.0 (c (n "simpler-git") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.17") (d #t) (k 0)))) (h "03d91jirhpv3ng7cx1i9y7881pvcdkcqdr4ycxpx93rhycbbl92v")))

(define-public crate-simpler-git-0.2.0 (c (n "simpler-git") (v "0.2.0") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "1xbslcaklqw5fczmzc5lczk7l4x52l09j2acrrgcwcbv3h3z3fkp")))

(define-public crate-simpler-git-0.2.1 (c (n "simpler-git") (v "0.2.1") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "1cbj9s219ai2ii4nzj03jbjkp6lcphxk5bf5zx1r2faggvyn6fln")))

