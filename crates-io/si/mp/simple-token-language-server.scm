(define-module (crates-io si mp simple-token-language-server) #:use-module (crates-io))

(define-public crate-simple-token-language-server-0.0.1 (c (n "simple-token-language-server") (v "0.0.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.19.0") (f (quote ("proposed"))) (d #t) (k 0)))) (h "18rfkqf1xy1zmbk2alnnvh5i5qa0qf2r72n245zw6n8hcjj7w1gw")))

