(define-module (crates-io si mp simple-tlv) #:use-module (crates-io))

(define-public crate-simple-tlv-0.1.0 (c (n "simple-tlv") (v "0.1.0") (d (list (d (n "heapless") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "simple-tlv_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1y43bxgnr8zy1an363h8vx6cjjrdqwvmh05l05vincm92k9fs5mm") (f (quote (("std" "alloc") ("derive" "simple-tlv_derive") ("alloc"))))))

