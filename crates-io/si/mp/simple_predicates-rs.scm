(define-module (crates-io si mp simple_predicates-rs) #:use-module (crates-io))

(define-public crate-simple_predicates-rs-0.1.0 (c (n "simple_predicates-rs") (v "0.1.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mria3riz4szjf6vsiw6m081gjlp44gp8qdf3s5wg2ir02c7w6nb") (f (quote (("default")))) (y #t)))

