(define-module (crates-io si mp simple-anchor-idl-ts) #:use-module (crates-io))

(define-public crate-simple-anchor-idl-ts-0.1.0 (c (n "simple-anchor-idl-ts") (v "0.1.0") (d (list (d (n "anchor-idl") (r "^0.3.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0g38glyg97dvsfsdvn2izp526yq6y1lyyznnf7z2hc226rnlg1qx")))

(define-public crate-simple-anchor-idl-ts-0.1.1 (c (n "simple-anchor-idl-ts") (v "0.1.1") (d (list (d (n "anchor-idl") (r "^0.3.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0ld4nq3nb56zij7d05xw2rfnhb1s7cxvxrn64ac093qxr0zgbmji")))

