(define-module (crates-io si mp simple-repl) #:use-module (crates-io))

(define-public crate-simple-repl-0.1.0 (c (n "simple-repl") (v "0.1.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "10plcsg50fn2w4mxbh6h2nychzhnzm0fv1gzfhrld1p1zp8s8bil")))

(define-public crate-simple-repl-0.1.1 (c (n "simple-repl") (v "0.1.1") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "110ym424nm7q5qfhaybnlgricda6jr4q3z7c23s6mvax4l82ky20")))

(define-public crate-simple-repl-0.1.2 (c (n "simple-repl") (v "0.1.2") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0zhlcr7hmxk158b6hfvvwvsngcpxvq75d99dd468knawwvf96y80")))

(define-public crate-simple-repl-0.1.4 (c (n "simple-repl") (v "0.1.4") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0fj2vw0vh7w6cap6x1cr5x9jsg3pqxk0cgfsqn7kz48n8ch5id7i")))

