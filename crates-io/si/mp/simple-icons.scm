(define-module (crates-io si mp simple-icons) #:use-module (crates-io))

(define-public crate-simple-icons-0.1.0 (c (n "simple-icons") (v "0.1.0") (h "0w9j8h2gdbr79k928fd43ssncijbcxnsyhmiwzr3gn1m76lj0hh4")))

(define-public crate-simple-icons-0.1.1 (c (n "simple-icons") (v "0.1.1") (h "1wsvfjcnas1ymfpxxb5n9l5042f7jrljl610ccla7ngxaww6i3hp")))

