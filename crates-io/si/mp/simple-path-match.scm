(define-module (crates-io si mp simple-path-match) #:use-module (crates-io))

(define-public crate-simple-path-match-0.2.0 (c (n "simple-path-match") (v "0.2.0") (d (list (d (n "beef") (r "^0.5.2") (k 0)) (d (n "snafu") (r "^0.7.4") (k 0)))) (h "1hwjmm3x07mxnr1sbahrxfc5ry9kmy4wi6dba3hl6k7wrvwgk5vm")))

