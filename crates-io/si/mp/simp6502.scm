(define-module (crates-io si mp simp6502) #:use-module (crates-io))

(define-public crate-simp6502-0.1.0 (c (n "simp6502") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1vchn20bqvcn7l3r0512pi57hwxk5gan519g2g2fw86qr1wr4adb")))

(define-public crate-simp6502-0.1.1 (c (n "simp6502") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)))) (h "1ai4xny7cwwnpimahis11ssk2iqfhfa5srsj16r3avhrlp7irs23")))

