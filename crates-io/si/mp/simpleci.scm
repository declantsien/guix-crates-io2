(define-module (crates-io si mp simpleci) #:use-module (crates-io))

(define-public crate-simpleci-0.1.0 (c (n "simpleci") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0dj8wf5kyh6djdrah3as6i2rwk0wxy8fs3l1qgl4mp2ihf5issra") (y #t)))

(define-public crate-simpleci-0.0.1 (c (n "simpleci") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0ic462sf5mnj7mpzvmmspy0nafcxbwg8kkm6pvzs2vnkhz47jx19")))

(define-public crate-simpleci-0.0.2 (c (n "simpleci") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "02q58ga3ww2q1y023119344zlwbwk6jiav106mlim8x0jgnpsfzf")))

(define-public crate-simpleci-0.0.3 (c (n "simpleci") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0dj0wljxy1aj9r44a1bszikk3dwv2qdrpyyyzhfkd7lrxj4xq4gr") (y #t)))

(define-public crate-simpleci-0.0.4 (c (n "simpleci") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0qq120hryn5qrbhg0ngrapypv912d30f2fv7r10hjg99rfj37zgd")))

