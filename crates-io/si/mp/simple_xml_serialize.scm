(define-module (crates-io si mp simple_xml_serialize) #:use-module (crates-io))

(define-public crate-simple_xml_serialize-0.1.0 (c (n "simple_xml_serialize") (v "0.1.0") (h "04zac12qa3r790z0491hr4fsk2yfj7ady8adla4z191595qa3n7i")))

(define-public crate-simple_xml_serialize-0.2.0 (c (n "simple_xml_serialize") (v "0.2.0") (h "07j9kvq1s7mrccllfqncp43pwz68i5aj053by2lna22dg66mqs4w")))

(define-public crate-simple_xml_serialize-0.2.1 (c (n "simple_xml_serialize") (v "0.2.1") (h "1qd9rbh68z5p5k0y9b3xjrdqy1g3nn15rpn01sr2f1rmdjg7agyd")))

(define-public crate-simple_xml_serialize-0.2.3 (c (n "simple_xml_serialize") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0marm3adviifp5dcpvhgvn38qdif46z4h2dxd1ls412rfb1d3pgs")))

(define-public crate-simple_xml_serialize-0.3.0 (c (n "simple_xml_serialize") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "05dcb970xzx43r56wqwxq8hpb7zvv71g0naymq3ij1bgd71rd21g")))

