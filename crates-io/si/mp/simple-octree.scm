(define-module (crates-io si mp simple-octree) #:use-module (crates-io))

(define-public crate-simple-octree-0.1.0 (c (n "simple-octree") (v "0.1.0") (h "06pay4dgj8sx1m36qsp259wqhfkdn866jl33b35zmg1391ndqlr2")))

(define-public crate-simple-octree-0.1.1 (c (n "simple-octree") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0if08zpxrw2fk5rz8xciibd6ly4n8gglnvjmm4x3qn4ndjbdn23s")))

