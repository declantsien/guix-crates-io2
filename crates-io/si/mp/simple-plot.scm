(define-module (crates-io si mp simple-plot) #:use-module (crates-io))

(define-public crate-simple-plot-0.1.0 (c (n "simple-plot") (v "0.1.0") (d (list (d (n "plotly") (r "^0.7.0") (d #t) (k 0)))) (h "0h6sk2w3yq2zbzcv4x8a79svi7kqmgnjmx583v0bf1hfjdl7yhhd")))

(define-public crate-simple-plot-0.1.1 (c (n "simple-plot") (v "0.1.1") (d (list (d (n "plotly") (r "^0.7.0") (d #t) (k 0)))) (h "0zrn1wpcwmh6vqksvy2fbn7kyh5q4vwa0a7138z5xpl9cpwr9nzd")))

(define-public crate-simple-plot-0.1.2 (c (n "simple-plot") (v "0.1.2") (d (list (d (n "plotly") (r "^0.7.0") (d #t) (k 0)))) (h "1gn24iijlyc582ns4nc6lm6s0y009373f3rgpv52c2ly7wcy4jyx")))

