(define-module (crates-io si mp simple-aes) #:use-module (crates-io))

(define-public crate-simple-aes-1.0.0 (c (n "simple-aes") (v "1.0.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1zmnxg3ginciyn1ic72pgcy36dd8lxcjglmbjyj2b3bh14ivqdf4")))

(define-public crate-simple-aes-1.0.1 (c (n "simple-aes") (v "1.0.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1d9l14992k0kkydhw7r5rbfz7pd1qjkisvh6sxnfza37l9b65dfj")))

