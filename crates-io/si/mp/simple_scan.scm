(define-module (crates-io si mp simple_scan) #:use-module (crates-io))

(define-public crate-simple_scan-0.1.0 (c (n "simple_scan") (v "0.1.0") (h "1m2gvm3bxhl2111m05y582fjcwcq9m9fdrxyzxs4f4nr1m0apyx5")))

(define-public crate-simple_scan-0.2.0 (c (n "simple_scan") (v "0.2.0") (h "178kvc2bbzs6a7r9409kn71pysc3f8sz5brxwidv06h3s1f26mk3")))

(define-public crate-simple_scan-0.2.1 (c (n "simple_scan") (v "0.2.1") (h "129217x1827v2ik351f1farbpxfkqc698ndh42298zczszzh0y4b")))

(define-public crate-simple_scan-0.2.2 (c (n "simple_scan") (v "0.2.2") (h "0a1r3pym50b0bdycr62ib4xr0i3y0fi9b9lmlzv12p9rp28pwm7x")))

(define-public crate-simple_scan-0.3.0 (c (n "simple_scan") (v "0.3.0") (h "0b0ik7j4gs89bhf3prk5ifbx86873rgr6nqzwiv0pqig8i2fxdym")))

(define-public crate-simple_scan-0.3.1 (c (n "simple_scan") (v "0.3.1") (h "0iknixl61ynv1j411i0qhsj2h7m4l2c74sfy251ax0myimgn0zd8")))

