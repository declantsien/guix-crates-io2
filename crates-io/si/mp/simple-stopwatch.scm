(define-module (crates-io si mp simple-stopwatch) #:use-module (crates-io))

(define-public crate-simple-stopwatch-0.1.0 (c (n "simple-stopwatch") (v "0.1.0") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1lb9y5vc236rncb1flhsfwvhy2lajdgs9g3b8gflm5yg0vivdym8")))

(define-public crate-simple-stopwatch-0.1.1 (c (n "simple-stopwatch") (v "0.1.1") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0r5ks7xhxydfr4iq43c0xmhvsz6gg1vnarbv41gl3i8m2smkvrir")))

(define-public crate-simple-stopwatch-0.1.2 (c (n "simple-stopwatch") (v "0.1.2") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1qfw0w3nz42018mg8y5qkgjacs69cj835c5r1n8cl7lp9lvr0w4z")))

(define-public crate-simple-stopwatch-0.1.3 (c (n "simple-stopwatch") (v "0.1.3") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "12b0nbyf9fak3piwj4g7cb1gqfzjmpyp1psihba0qyz93jdvzdb9")))

(define-public crate-simple-stopwatch-0.1.4 (c (n "simple-stopwatch") (v "0.1.4") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0l9bd74xmpqrwgpnvsb5nbvrg2prjhmgbydl7s1cbfxzkz0kn2za")))

