(define-module (crates-io si mp simple-async-pipe) #:use-module (crates-io))

(define-public crate-simple-async-pipe-0.1.0 (c (n "simple-async-pipe") (v "0.1.0") (d (list (d (n "tokio") (r "^1.6.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 2)))) (h "19vqq9psai0vxxgv1axwix879gcan878r42a3l7lzxh3dy9n668a")))

(define-public crate-simple-async-pipe-0.1.1 (c (n "simple-async-pipe") (v "0.1.1") (d (list (d (n "tokio") (r "^1.6.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 2)))) (h "15d3svihlx85dii3g56jrfj9k4c58d2plw3lzp9fvbahnkkb6376")))

