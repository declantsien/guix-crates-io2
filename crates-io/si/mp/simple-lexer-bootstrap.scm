(define-module (crates-io si mp simple-lexer-bootstrap) #:use-module (crates-io))

(define-public crate-simple-lexer-bootstrap-0.1.0 (c (n "simple-lexer-bootstrap") (v "0.1.0") (d (list (d (n "finite-automata") (r "^0.1.1") (d #t) (k 0)) (d (n "regular-expression-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lar4hbnpmirpw0pr1q62qlp7s4d4ldj9r56i77gbyy6z8vd7jaj")))

