(define-module (crates-io si mp simple_yaml_parser_kma) #:use-module (crates-io))

(define-public crate-simple_yaml_parser_kma-0.1.0 (c (n "simple_yaml_parser_kma") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0gr93v2038fcb3bmdmxr8pjpxb83cif9rchip89bhrvsi1767h9y")))

