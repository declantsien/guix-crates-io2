(define-module (crates-io si mp simple_url_parser) #:use-module (crates-io))

(define-public crate-simple_url_parser-0.0.1 (c (n "simple_url_parser") (v "0.0.1") (d (list (d (n "nom") (r "^6.2.1") (d #t) (k 0)))) (h "1ghy1p682sgszyls59x03xrlqv23whqfns8ikzkxvq86qsaczq29")))

(define-public crate-simple_url_parser-0.0.2 (c (n "simple_url_parser") (v "0.0.2") (d (list (d (n "nom") (r "^6.2.1") (d #t) (k 0)))) (h "0f2h62s8fs8jmp8wm62gd8whvga6csjgqjg7rh6yy0phpa6z0lxs")))

(define-public crate-simple_url_parser-0.0.3 (c (n "simple_url_parser") (v "0.0.3") (d (list (d (n "nom") (r "^6.2.1") (d #t) (k 0)))) (h "0kakip7hgx0ahgbwkm6yd1qnb9k8avg3kfqznpb51vd34xj9xvnm")))

(define-public crate-simple_url_parser-0.0.4 (c (n "simple_url_parser") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1jkmbr93jh9y1xca1qxizvqan6vp4i3hgph057fwkgc2z6ap6p41")))

