(define-module (crates-io si mp simple-async-local-executor) #:use-module (crates-io))

(define-public crate-simple-async-local-executor-0.1.0 (c (n "simple-async-local-executor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0bm4k5cfd15yzr9rhp5nrx4vdgv9sqj8wzc710l88fj9ybayxk6b") (f (quote (("default" "futures"))))))

