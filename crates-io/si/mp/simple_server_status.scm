(define-module (crates-io si mp simple_server_status) #:use-module (crates-io))

(define-public crate-simple_server_status-0.1.0 (c (n "simple_server_status") (v "0.1.0") (h "0vmfcfmrpfacvy9jylgif4f1m9hh5kbprqb9d5b3xg220spwmbf1") (f (quote (("tcp") ("ram") ("default" "cpu" "ram" "tcp") ("cpu"))))))

(define-public crate-simple_server_status-0.2.0 (c (n "simple_server_status") (v "0.2.0") (h "09cmw175mwkc6b7ifwd4zql3kpamdnxj1avrqhw02l6sar8mvdvc") (f (quote (("tcp") ("ram") ("net") ("default" "cpu" "net" "ram" "tcp") ("cpu"))))))

(define-public crate-simple_server_status-0.2.1 (c (n "simple_server_status") (v "0.2.1") (h "0a8ifkds4rhwgqwp9rh34rpk4j9yzaxl499lg6zwlz8xifvhn6aa") (f (quote (("tcp") ("ram") ("net") ("default" "cpu" "net" "ram" "tcp") ("cpu"))))))

