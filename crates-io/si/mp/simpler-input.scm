(define-module (crates-io si mp simpler-input) #:use-module (crates-io))

(define-public crate-simpler-input-0.1.0 (c (n "simpler-input") (v "0.1.0") (h "1i9amzkdlivq8hy1qg48hz5l70s9nimzli3iicld01binz7jaa30")))

(define-public crate-simpler-input-0.2.0 (c (n "simpler-input") (v "0.2.0") (h "0bqbw99ghjj535yp2lshw1mkc37b9fp0gp8lynlqlr9gl56sf21l")))

