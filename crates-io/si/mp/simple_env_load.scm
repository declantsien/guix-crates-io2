(define-module (crates-io si mp simple_env_load) #:use-module (crates-io))

(define-public crate-simple_env_load-0.1.0 (c (n "simple_env_load") (v "0.1.0") (h "0fdk5f5mfm06d1gwgr6x7ldmc7k5dfcs8vr9yjj1mpcf3881rpsn")))

(define-public crate-simple_env_load-0.2.0 (c (n "simple_env_load") (v "0.2.0") (h "1qx53ii6a300pw47hhw3kk556rxz2lqybsp0islnycxqicx0cbq6")))

