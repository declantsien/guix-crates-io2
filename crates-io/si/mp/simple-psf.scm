(define-module (crates-io si mp simple-psf) #:use-module (crates-io))

(define-public crate-simple-psf-0.1.1 (c (n "simple-psf") (v "0.1.1") (h "185qnm1q949b1vihxy9xlccy2pzvhgqrs0xanvllzyq9yiw6la6m")))

(define-public crate-simple-psf-0.1.2 (c (n "simple-psf") (v "0.1.2") (h "079zn351p0cxmzl5ihlij7b95ssl6ivl10b0gvfidkihy4j5vywd")))

