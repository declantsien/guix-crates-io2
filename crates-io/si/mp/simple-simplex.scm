(define-module (crates-io si mp simple-simplex) #:use-module (crates-io))

(define-public crate-simple-simplex-0.1.0 (c (n "simple-simplex") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0gyvk8xgm597w1lawzcm23mmanp9hynkas4iyv7qa9xx1bx3kykl")))

(define-public crate-simple-simplex-1.0.0 (c (n "simple-simplex") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "13lamc7qblyzycdmx8sz7gl1ci64fpb87jibz6zbmda2l84yh1ha") (y #t)))

(define-public crate-simple-simplex-1.0.1 (c (n "simple-simplex") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0zld2xlyr9linc340rdix0mynqgkw3m4s3y2ghj9nx0sn6zw5yhi") (y #t)))

(define-public crate-simple-simplex-1.0.2 (c (n "simple-simplex") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1jzwiplhbsxxqxam5pbf268wvdwgbqybz9m9az96avdx3r3mvkrs") (y #t)))

(define-public crate-simple-simplex-1.0.3 (c (n "simple-simplex") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1krkmjvd87z4wdw4hp0yprw756vqa8slx5vwqr8ajhzxa4lf9cbm")))

