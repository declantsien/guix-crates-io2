(define-module (crates-io si mp simpledb) #:use-module (crates-io))

(define-public crate-simpledb-0.1.0 (c (n "simpledb") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r "^0.14") (d #t) (k 0)))) (h "17fz1k2fnzpj6s8x4kjh9s2hz1r2w17jz0df2zgwsizib44y0apl")))

(define-public crate-simpledb-0.1.1 (c (n "simpledb") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r "^0.14") (d #t) (k 0)))) (h "0x4n46yw3ahcb3clzmsmxs9bg7wz6wafj7qcck1jldqgw3sxj2g2")))

(define-public crate-simpledb-0.1.2 (c (n "simpledb") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r "^0.14") (d #t) (k 0)))) (h "0fik25c6mv204l69yb27zcbrpvm93kh8z17dlygjpgz4zbswfs2q")))

(define-public crate-simpledb-0.1.3 (c (n "simpledb") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r "^0.14") (d #t) (k 0)))) (h "1kvijcpxk8hriirlx9hb3cyji1xaraslnx97xrc75wgy846jmzg5")))

(define-public crate-simpledb-0.1.4 (c (n "simpledb") (v "0.1.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rocksdb") (r "^0.15") (d #t) (k 0)))) (h "1jvymg67p4ja5df2nrvnxm794g19qw1r5if8yg4ph8n67mg30kv7")))

(define-public crate-simpledb-0.1.5 (c (n "simpledb") (v "0.1.5") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rocksdb") (r "^0.15") (d #t) (k 0)))) (h "1cl54qd94igmrdnhi7mvs3dplqaff5di67idpy8d74nifrdlqq2y")))

(define-public crate-simpledb-0.1.6 (c (n "simpledb") (v "0.1.6") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)))) (h "1xyilbzq63m0h644q4k1fsphyzndb50n0a256q1gplijjv8jsa2f")))

(define-public crate-simpledb-0.1.7 (c (n "simpledb") (v "0.1.7") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.20.1") (f (quote ("zstd" "lz4"))) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lsg8d1rw40gq1i360nwmqc7pc724898c88m08n9hf3h778xikap")))

