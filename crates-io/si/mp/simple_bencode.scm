(define-module (crates-io si mp simple_bencode) #:use-module (crates-io))

(define-public crate-simple_bencode-0.1.0 (c (n "simple_bencode") (v "0.1.0") (h "09c4f5q42lz6kpmz46k9vd2ns9p79pkpn8kvzwnzvkag9b6wf9a0")))

(define-public crate-simple_bencode-0.1.1 (c (n "simple_bencode") (v "0.1.1") (h "1pn6xxix5qs3dwgrfdk8najsl5zpq1310ryhm1fwb9lkgd57bq0s")))

(define-public crate-simple_bencode-0.1.2 (c (n "simple_bencode") (v "0.1.2") (h "0xhhv2x21s348d0y3xgzrr7m6sk6rrq5sp7x48vc8lk1scm593xr")))

(define-public crate-simple_bencode-0.1.3 (c (n "simple_bencode") (v "0.1.3") (h "172nfwlj0narrws8gvzk6lsxx0lnnp69s03b0lmxzikgapiwirj6")))

(define-public crate-simple_bencode-0.1.4 (c (n "simple_bencode") (v "0.1.4") (h "1qfzm1k3pm1w5axa23jz1f5r339d03cgkz5wdz6sv6pl0lb5467s")))

