(define-module (crates-io si mp simple_bar) #:use-module (crates-io))

(define-public crate-simple_bar-0.1.0 (c (n "simple_bar") (v "0.1.0") (h "0m7h9s3sjv7ba2amigimv9ir7cdfmjkqdgispz01rm82nfx3p96a") (y #t)))

(define-public crate-simple_bar-0.1.1 (c (n "simple_bar") (v "0.1.1") (h "0dg44k5j2p4hfiy1dvqy3sdzz0i9w25xslfgp401fx72gb328n6l") (y #t)))

(define-public crate-simple_bar-0.1.2 (c (n "simple_bar") (v "0.1.2") (h "1cdw8x0z5g3000zzf4alpvkcchwbkapipqkzqn0862bivp6qf0lm") (y #t)))

(define-public crate-simple_bar-0.1.3 (c (n "simple_bar") (v "0.1.3") (h "1769v675bmw059xjnlvw1lgn9kczb218nvhafvs6vck9kz7g98xn") (y #t)))

(define-public crate-simple_bar-0.1.4 (c (n "simple_bar") (v "0.1.4") (h "13nzb02rfqxb6k4h00b0zfapsxhv0r6akaf4jyk8yi8bcvprak31")))

(define-public crate-simple_bar-0.1.5 (c (n "simple_bar") (v "0.1.5") (h "0nnrzpzs5yn7dc1zr9l7zgc2igp2r3ijhr5n17qcqhyyk35js7g5")))

(define-public crate-simple_bar-0.1.6 (c (n "simple_bar") (v "0.1.6") (h "0rzipi80b69204ajxc0ayhkcfmn63z6z04j0y5yhp1j5i7m9lxah") (y #t)))

(define-public crate-simple_bar-0.2.0 (c (n "simple_bar") (v "0.2.0") (h "0m07q2swxiyxbqxkw4h2f9h8q15zjyp45kmhnkp2cgg6dj20gfi0")))

(define-public crate-simple_bar-0.2.1 (c (n "simple_bar") (v "0.2.1") (h "1ycfmzciw5xh7cz0c15abp64njm5f5303ll52lsi8pmgrh10xylp")))

(define-public crate-simple_bar-0.2.2 (c (n "simple_bar") (v "0.2.2") (h "1igadas5z50h2cqvpf657mr29zd0i86n05xm6sn0852n3gbzxpyc")))

