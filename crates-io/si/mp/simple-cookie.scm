(define-module (crates-io si mp simple-cookie) #:use-module (crates-io))

(define-public crate-simple-cookie-0.1.0 (c (n "simple-cookie") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (f (quote ("aes"))) (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)))) (h "05n168ycxvmvnfkz6s59yqgrrwz1dlm4g5nl4qp2s48v00zds9ds")))

(define-public crate-simple-cookie-0.1.1 (c (n "simple-cookie") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (f (quote ("aes"))) (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)))) (h "0dqq6y2i8y5fii75pb8xrz39c0aisz32dziy3hy4v5dkq2jqch7y")))

(define-public crate-simple-cookie-1.0.0 (c (n "simple-cookie") (v "1.0.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (f (quote ("aes"))) (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)))) (h "05lw2byhvywd8c850l9ap943vc5napvyla2w0a7ki0z9n2hsc55y")))

