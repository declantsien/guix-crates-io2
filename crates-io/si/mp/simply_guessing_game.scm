(define-module (crates-io si mp simply_guessing_game) #:use-module (crates-io))

(define-public crate-simply_guessing_game-0.1.0 (c (n "simply_guessing_game") (v "0.1.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "emojis") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18qccq9b161zylj9bzrlhp60is8myinszba3f06qfmahx45s8q1c") (y #t)))

(define-public crate-simply_guessing_game-0.2.0 (c (n "simply_guessing_game") (v "0.2.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "emojis") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mggz3pf0jprv49arrsnrc7myjlwb4la9v3sa909rq2p4vl2zqmq") (y #t)))

