(define-module (crates-io si mp simpler_timer) #:use-module (crates-io))

(define-public crate-simpler_timer-0.1.0 (c (n "simpler_timer") (v "0.1.0") (h "1pf5mhsnc5ab42a86pl01547d5wj7gxzrby4z2xifcz9hkx6crb8")))

(define-public crate-simpler_timer-0.1.1 (c (n "simpler_timer") (v "0.1.1") (h "1v8a646xvigvijs0sab9b0pwf02skzyhbw6shv56538nclvir6f0")))

(define-public crate-simpler_timer-0.2.0 (c (n "simpler_timer") (v "0.2.0") (h "0w85y1ys4mjjyy65znw7g3716wxphbhad03kd64ppa3w9h1zk17i")))

