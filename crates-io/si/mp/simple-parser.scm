(define-module (crates-io si mp simple-parser) #:use-module (crates-io))

(define-public crate-simple-parser-0.1.0 (c (n "simple-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regular-expression-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-lexer-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-parser-bootstrap") (r "^0.1.0") (d #t) (k 0)))) (h "0r7yjv00sqr37brxhk1mcsqx5va415s5gd1y2nzfdad2shfxmpzl")))

