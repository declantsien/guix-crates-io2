(define-module (crates-io si mp simple_generators) #:use-module (crates-io))

(define-public crate-simple_generators-0.1.3 (c (n "simple_generators") (v "0.1.3") (d (list (d (n "adapter") (r "^0.1.3") (d #t) (k 0) (p "simple_generators_adapter")) (d (n "macros") (r "^0.1.3") (d #t) (k 0) (p "simple_generators_macros")))) (h "0ndy3rpilp0x11n4k3x0mjh5s2gk2kikcml74vqwj9jgl01s9pc0")))

