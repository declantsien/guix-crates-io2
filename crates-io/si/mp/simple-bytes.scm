(define-module (crates-io si mp simple-bytes) #:use-module (crates-io))

(define-public crate-simple-bytes-0.1.1 (c (n "simple-bytes") (v "0.1.1") (h "0i0q1k8d73kq7c3pxr7z4lbl8ijlp35x5d193nsgl41jpf4ac29n")))

(define-public crate-simple-bytes-0.2.0 (c (n "simple-bytes") (v "0.2.0") (h "0q6bpmfb4gr4qa3y1rmvgi6rdfkqirgwwcsf1g24fs0saswqxgq6")))

(define-public crate-simple-bytes-0.2.1 (c (n "simple-bytes") (v "0.2.1") (h "0g0abxjlbdc0xhadwg58jr0m7m8jcl22qnycaswcwhn5myljdl1m") (y #t)))

(define-public crate-simple-bytes-0.2.2 (c (n "simple-bytes") (v "0.2.2") (h "15cw8x1rpvdp68d96pa6iyq9274qs5k798sri87bz9as8d75prf0")))

(define-public crate-simple-bytes-0.2.3 (c (n "simple-bytes") (v "0.2.3") (h "059bghsb84bzdz2jmh9q0akqnjziq1nk8pqzixixnk3w87369asr")))

(define-public crate-simple-bytes-0.2.4 (c (n "simple-bytes") (v "0.2.4") (h "05pj8r3r23ry2js7am2dw59cflwxc1l22adxk46r10x9kddkkwr6")))

(define-public crate-simple-bytes-0.2.5 (c (n "simple-bytes") (v "0.2.5") (h "1ncccd7pjz1yp8fkzqz18qikidbinb73f0m3w3v58qmhv4950pjs")))

(define-public crate-simple-bytes-0.2.6 (c (n "simple-bytes") (v "0.2.6") (h "03wrlz5pn2403l2brckmkbaf60b51wlxw2c6l2mp5jvrggms9gjc")))

(define-public crate-simple-bytes-0.2.7 (c (n "simple-bytes") (v "0.2.7") (h "04n5v8p28z2c5k4zsp05ah4m2d5lwajlx4h76zb7qwc6hapfkqgn")))

(define-public crate-simple-bytes-0.2.8 (c (n "simple-bytes") (v "0.2.8") (h "0z6p62d4wvhmxdqskiz8p51hdpc1vnvgll6ljsdwyhkmaqwypdi9")))

(define-public crate-simple-bytes-0.2.9 (c (n "simple-bytes") (v "0.2.9") (h "0xh33lyy0659ajpq0m2lwvfgcnxh0kmw3kgwh74qrxavvihazrnv")))

(define-public crate-simple-bytes-0.2.10 (c (n "simple-bytes") (v "0.2.10") (h "1fnbyk14c8mb69lv6bwd48av84fc33qfbm7hkfcqm6nysba7ri0k")))

(define-public crate-simple-bytes-0.2.11 (c (n "simple-bytes") (v "0.2.11") (h "12dckdnr7lasqyp04mr3d37mmayhd5g2g74r64ss9jmjdiw1hada")))

(define-public crate-simple-bytes-0.2.12 (c (n "simple-bytes") (v "0.2.12") (h "0hg5jlz8hkx7zjf8yk3nycljf75jh1s1a5k6csllmab7ldf9z0kx") (r "1.56")))

(define-public crate-simple-bytes-0.2.13 (c (n "simple-bytes") (v "0.2.13") (h "1j0qhlvgqs0y6s6g7aqjp3s142xq3iyc0yv7mkd6c0pfqhpcj5vf") (r "1.56")))

(define-public crate-simple-bytes-0.2.14 (c (n "simple-bytes") (v "0.2.14") (h "1i2cnhcvlx1d03bh2nsdm4ki82wkmz6mvww5a44lz421sbck45f1") (r "1.56")))

