(define-module (crates-io si mp simple-abns) #:use-module (crates-io))

(define-public crate-simple-abns-0.1.0 (c (n "simple-abns") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "1nq1i37b7zzxjjgq0lcspjzijq04hcd56qbla6wkspm2qngvpnzj")))

