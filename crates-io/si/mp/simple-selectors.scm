(define-module (crates-io si mp simple-selectors) #:use-module (crates-io))

(define-public crate-simple-selectors-0.1.0 (c (n "simple-selectors") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "0j0xfwpwxv2ig0slj3hzcwyzxr31wpm146g895k0xn34fh4v1siv")))

