(define-module (crates-io si mp simple-life) #:use-module (crates-io))

(define-public crate-simple-life-0.1.0 (c (n "simple-life") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1x25bdpdkragmyzgsza0did74jba6za5245z321hbjln7r6g062v") (f (quote (("tracing"))))))

(define-public crate-simple-life-0.1.1 (c (n "simple-life") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1pz7dihi8vq6idykw0jps0prp1w23gi4h6ml5ss5q3946aqjs397") (f (quote (("tracing"))))))

(define-public crate-simple-life-0.1.2 (c (n "simple-life") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1h9fr8f0cjgx63z4khl99pjvbahsqq7fkxyrh893dan3w5xp51pg") (f (quote (("tracing"))))))

(define-public crate-simple-life-0.1.3 (c (n "simple-life") (v "0.1.3") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "0yqb5wnh8ddf6q9l85v6kwgmjx94kj9v6lmbypx40mmdx11iydn0")))

(define-public crate-simple-life-0.1.4 (c (n "simple-life") (v "0.1.4") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "0inh3hnng9cqb5hd13v2zgqc1hj4kfva49bphf7c33dw7322xcqd")))

(define-public crate-simple-life-0.1.5 (c (n "simple-life") (v "0.1.5") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "0wxwgzb0mwzg7yr1la21762wfjnmykl0zfrli7dbghsbilp78pyn")))

(define-public crate-simple-life-0.1.6 (c (n "simple-life") (v "0.1.6") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "1mjldir0mzqv518b9n2h3v6hjskfkzz1as3mv8zg7xj0wgsivaaq")))

(define-public crate-simple-life-0.1.7 (c (n "simple-life") (v "0.1.7") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "04lv7klw9gagkxmjyj879gr7s6wk69as9jhvnkrafh81gddp9p9v")))

(define-public crate-simple-life-0.1.8 (c (n "simple-life") (v "0.1.8") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "09spdyil9bnb544bmxvvb2ky86v12vxzfi981qslnlap4606vjmb")))

(define-public crate-simple-life-0.2.0 (c (n "simple-life") (v "0.2.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "0xwd1n3d4v6azwyd1y29v2m4yr4kjf36vis3w0gcl69rp1xh1zi5")))

(define-public crate-simple-life-0.2.1 (c (n "simple-life") (v "0.2.1") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "1bcm6rlz3274l07jl164lfjh3jc7c5zblss1rblinsxq5gw402zr")))

(define-public crate-simple-life-0.2.2 (c (n "simple-life") (v "0.2.2") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "1hc1v9vnpg6vzdascv76ppzv6y8chw4c95cxv9yygy5fdccysfpz")))

(define-public crate-simple-life-0.2.3 (c (n "simple-life") (v "0.2.3") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "18ip676c8w83104b98gin6vwi38s93ig9yahpwafkn2iiczqwx4d")))

(define-public crate-simple-life-0.3.0 (c (n "simple-life") (v "0.3.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "1am0jyrhbhan3wmm5daxcy56piih1mgqqxmqr45q00vqi2sgcxyw")))

(define-public crate-simple-life-0.4.0 (c (n "simple-life") (v "0.4.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "08s7shbhc2sr3c104dk5pm5x9af4idmaibdrjv64rl763qa1qb64")))

(define-public crate-simple-life-0.5.0 (c (n "simple-life") (v "0.5.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros" "signal"))) (d #t) (k 0)))) (h "1r4r1ikx1gi19y85hykl9dzy5ipv5psc897p9nbgj0iadf36ylcv")))

