(define-module (crates-io si mp simple-ntp) #:use-module (crates-io))

(define-public crate-simple-ntp-0.1.0 (c (n "simple-ntp") (v "0.1.0") (h "1szdwflmx82qb7jysjl4zlavm08rcdz2aggwlh3yy9ns7kclfv54")))

(define-public crate-simple-ntp-0.1.1 (c (n "simple-ntp") (v "0.1.1") (h "17y3fsbapg60kwx4km797vx4xixqll8npmx0bfivzcyknyrmh3g6")))

