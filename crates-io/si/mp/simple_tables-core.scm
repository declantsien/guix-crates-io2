(define-module (crates-io si mp simple_tables-core) #:use-module (crates-io))

(define-public crate-simple_tables-core-0.1.0 (c (n "simple_tables-core") (v "0.1.0") (h "0kn5rwrh7f37kinhzvb63maybwj9zr7vc731ka312mnjadj725wf")))

(define-public crate-simple_tables-core-0.1.1 (c (n "simple_tables-core") (v "0.1.1") (h "148pkjb2v0ws3g1mjm73bahdjraffd8bv25y6wac7b18bv8qbcg1")))

(define-public crate-simple_tables-core-0.2.0 (c (n "simple_tables-core") (v "0.2.0") (h "083agqnxmhn6r0gbjxy64p0hwmcxyc95dgpbsqjxwxc72qmznayi")))

(define-public crate-simple_tables-core-0.2.1 (c (n "simple_tables-core") (v "0.2.1") (h "1x50l7ygwiqc1rh67qbzfq5vr8hafdlg22rcjbb2mn0bs2fk5l18")))

(define-public crate-simple_tables-core-0.3.0 (c (n "simple_tables-core") (v "0.3.0") (h "0lbm2n6c12kddss0m02w8jkixx1qpd4ipbwydxlhmryx7cbicz9y")))

