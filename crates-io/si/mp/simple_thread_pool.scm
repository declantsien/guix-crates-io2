(define-module (crates-io si mp simple_thread_pool) #:use-module (crates-io))

(define-public crate-simple_thread_pool-0.1.0 (c (n "simple_thread_pool") (v "0.1.0") (h "0jpjz3k35n8jgwaqw0crb900hgj1c8nhd149b13k8373759kkj2d") (y #t)))

(define-public crate-simple_thread_pool-0.1.1 (c (n "simple_thread_pool") (v "0.1.1") (h "0q66g2sb3q0m08m1kl7c5kz8lxljnc702z67jawaqsij6j521swv")))

