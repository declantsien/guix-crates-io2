(define-module (crates-io si mp simple_config_parser) #:use-module (crates-io))

(define-public crate-simple_config_parser-0.1.0 (c (n "simple_config_parser") (v "0.1.0") (h "0xw3blnv2fvh25vrkfs1b2kzkjmqvriqccijr35nyvf16nl8wz34") (y #t)))

(define-public crate-simple_config_parser-0.1.1 (c (n "simple_config_parser") (v "0.1.1") (h "059jw927xdw3z2j4r51nzqiv3n8acshvqrkh82gjm8np4p2k8kp5") (y #t)))

(define-public crate-simple_config_parser-0.1.2 (c (n "simple_config_parser") (v "0.1.2") (h "1mq0bj14vp30gmh9k3rx4mcpr4gfcy31wrrbpi5zvixyy8hqn833") (y #t)))

(define-public crate-simple_config_parser-0.1.3 (c (n "simple_config_parser") (v "0.1.3") (h "07mfzj1ixzvy0xxzwva1js4fxbdxy3i3r2njxn1vjp8wp7qw9awa")))

(define-public crate-simple_config_parser-0.1.4 (c (n "simple_config_parser") (v "0.1.4") (h "1ldnzv4ynwn7khfa9ibx2sjhc08v6l09i5vnws6n25j5hm7s1rdn")))

(define-public crate-simple_config_parser-0.1.5 (c (n "simple_config_parser") (v "0.1.5") (h "06mylcj6vgj136vjs3ixjr8y7276419150nd5ays3jcizyj7iljc")))

(define-public crate-simple_config_parser-0.1.6 (c (n "simple_config_parser") (v "0.1.6") (h "1n536n1g06qzqjimhv6l3j9ankdim8cb3l4nwm2kz5v12lrs2jna")))

(define-public crate-simple_config_parser-1.0.0 (c (n "simple_config_parser") (v "1.0.0") (h "049vcm60fbj1c7c2linqrrjjcd2m89kq55g5aw1d7wyfnxy2nmix")))

