(define-module (crates-io si mp simple-json-parser) #:use-module (crates-io))

(define-public crate-simple-json-parser-0.0.1 (c (n "simple-json-parser") (v "0.0.1") (h "16716547wssmrbxyra6aac220mfqq8sf36vzxs3mzxhijwil6p4k")))

(define-public crate-simple-json-parser-0.0.2 (c (n "simple-json-parser") (v "0.0.2") (h "0af60mbgxg82f7cvsx4d5a6gwy973vpm1kyxiand7axfcs45yz43")))

(define-public crate-simple-json-parser-0.0.3 (c (n "simple-json-parser") (v "0.0.3") (h "04kx4123fvwnpq05zawp7hzvfms65bzsqniafah3405pb9p9s60d")))

