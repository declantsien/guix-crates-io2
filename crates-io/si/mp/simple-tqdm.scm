(define-module (crates-io si mp simple-tqdm) #:use-module (crates-io))

(define-public crate-simple-tqdm-0.1.1 (c (n "simple-tqdm") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "038jc24933d33dq9xsmnqffm374mn1bsxmd3997a9l08h1qll6hg") (s 2) (e (quote (("rayon" "dep:rayon" "indicatif/rayon"))))))

(define-public crate-simple-tqdm-0.1.2 (c (n "simple-tqdm") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "00pbb8dkrz60c2z2334ah0iv4wzplps3ansmsxmsriv99dg21qlz") (s 2) (e (quote (("rayon" "dep:rayon" "indicatif/rayon"))))))

(define-public crate-simple-tqdm-0.1.3 (c (n "simple-tqdm") (v "0.1.3") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1v36ilza2mp65drr4kxlayq478mhp9xbx59safjnjgn1mz9pmywm") (s 2) (e (quote (("rayon" "dep:rayon" "indicatif/rayon"))))))

(define-public crate-simple-tqdm-0.1.4 (c (n "simple-tqdm") (v "0.1.4") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1xr8xffqlyklzk96flmkwm3i3qacq0cvbgkycz2iwcq730724hy5") (s 2) (e (quote (("rayon" "dep:rayon" "indicatif/rayon"))))))

(define-public crate-simple-tqdm-0.2.0 (c (n "simple-tqdm") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0jayf8n2p4ws5yz6l3dcp8lnxr1hkj5ixa8jcw45xrx23xwjd8ri") (s 2) (e (quote (("rayon" "dep:rayon" "indicatif/rayon"))))))

