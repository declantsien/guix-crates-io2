(define-module (crates-io si mp simple-vectors) #:use-module (crates-io))

(define-public crate-simple-vectors-0.1.0 (c (n "simple-vectors") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vector-space") (r "^0.1.0") (d #t) (k 0)))) (h "1cypwyi92dlsnrv0av2h4gk0s0x3jh4pc3w2bdnjal9m6b036afz") (f (quote (("parsable" "token-parser"))))))

(define-public crate-simple-vectors-0.2.0 (c (n "simple-vectors") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vector-basis") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "1s1pn2c80h4cdr2k8f50vga741c8gr9v8sc918748nc2jqkr1d6q") (f (quote (("parsable" "token-parser"))))))

