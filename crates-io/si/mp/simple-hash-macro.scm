(define-module (crates-io si mp simple-hash-macro) #:use-module (crates-io))

(define-public crate-simple-hash-macro-0.1.0 (c (n "simple-hash-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1l568agki91gn1c6784rasdj3saz1sz6hqavxgkw9qaj87szmhdf")))

(define-public crate-simple-hash-macro-0.1.1 (c (n "simple-hash-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "158rjdlcn167arlzchys754m4z4hmvakk4ygj6n2rvgb2x43bzsr")))

