(define-module (crates-io si mp simple-middleware) #:use-module (crates-io))

(define-public crate-simple-middleware-0.1.0 (c (n "simple-middleware") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1wlhkl3ygd6x62gix2px6rmvlk8kslhmdx6krrsm9xifx20y0972") (r "1.78")))

(define-public crate-simple-middleware-0.1.1 (c (n "simple-middleware") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0fdkk71xdm8h5rwrhmp2yimi4n3vb1gq2ydddp0krzvb0w8rvbkj") (r "1.78")))

