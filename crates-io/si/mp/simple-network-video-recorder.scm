(define-module (crates-io si mp simple-network-video-recorder) #:use-module (crates-io))

(define-public crate-simple-network-video-recorder-0.1.0 (c (n "simple-network-video-recorder") (v "0.1.0") (h "0rn2ng75zhlbkr12ng2d2dhqi0pcw7vdz3hky3zv7jwkf2d1jgkg")))

(define-public crate-simple-network-video-recorder-0.1.1 (c (n "simple-network-video-recorder") (v "0.1.1") (h "0x5rfsj9sncdjrvggwjfxndknynhf24yqbkg20bzhwjgr331q3hf")))

(define-public crate-simple-network-video-recorder-0.1.2 (c (n "simple-network-video-recorder") (v "0.1.2") (h "0xkx9jdcpqlaly90sc8zlygk3nxfpv1bdl35fwc5gvn8ln1h5f4i")))

