(define-module (crates-io si mp simple-async-cache-rs) #:use-module (crates-io))

(define-public crate-simple-async-cache-rs-0.1.0 (c (n "simple-async-cache-rs") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "10w4i2sy67wwswajs447r4dva1k651hipcb8l7jgqczlwa6w6f2m")))

(define-public crate-simple-async-cache-rs-0.1.1 (c (n "simple-async-cache-rs") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "0xr6svdfjpqkcpwcs2kyjlqr0923rvmxpjbg6189iizv8j9c0cz6")))

(define-public crate-simple-async-cache-rs-0.1.2 (c (n "simple-async-cache-rs") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "0ck7fx9ngnqnr0zgrn6hyb1b2jmmyqvgmb6hzh4nxar0cs0cw9v1")))

(define-public crate-simple-async-cache-rs-0.1.3 (c (n "simple-async-cache-rs") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "16xdch6k8df1wa3vivvvrp25z0nbjzz68gbqp087j2ipj67cgbjs")))

(define-public crate-simple-async-cache-rs-0.2.0 (c (n "simple-async-cache-rs") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "16qy5a32cys5k0ypa12s5nq731drhpwsb3wl29qkm4d4gpx1725b")))

(define-public crate-simple-async-cache-rs-0.3.0 (c (n "simple-async-cache-rs") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "0mfkh3kx9wmy7jj9rakgq4chjrd2bs9giyr9xav7mrz7ajawz1qd")))

(define-public crate-simple-async-cache-rs-0.3.1 (c (n "simple-async-cache-rs") (v "0.3.1") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "1vyppcr8r75x065g22r7yfzjh5lr749yq91c8fir87xrppmcfhiv")))

(define-public crate-simple-async-cache-rs-0.3.2 (c (n "simple-async-cache-rs") (v "0.3.2") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "1baz7199wmiq48djzh6p9r9xcz6fg0d2613inyygm08gb08ky3nh")))

(define-public crate-simple-async-cache-rs-0.3.3 (c (n "simple-async-cache-rs") (v "0.3.3") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "1dk5igkpm8ay19f3b2d31f27q6b60pys2dvi2adpghx5l90i8asn")))

