(define-module (crates-io si mp simple_parse_derive) #:use-module (crates-io))

(define-public crate-simple_parse_derive-0.1.0 (c (n "simple_parse_derive") (v "0.1.0") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "14na45swgn2pjyg7ph7k1z3ka8z0ckgg3cj6plxkjx4nwal27hii")))

(define-public crate-simple_parse_derive-0.1.1 (c (n "simple_parse_derive") (v "0.1.1") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "17wgkn9bfc6r2y8w1i22ja6riqgaywznz504isnnfnrqdvh57lin")))

(define-public crate-simple_parse_derive-0.4.0 (c (n "simple_parse_derive") (v "0.4.0") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "0jvqpixyinm753x04dsclidjvcnyh9n5s68cbp738mykxbcx9j19")))

(define-public crate-simple_parse_derive-0.4.1 (c (n "simple_parse_derive") (v "0.4.1") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "1lsf9472rh1rr8sj7im8j9czzfig8rhvlkn5dk08x91n0ycd25k4")))

(define-public crate-simple_parse_derive-0.5.0 (c (n "simple_parse_derive") (v "0.5.0") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "1kw8rf83x2cqx3693w1314zpslm9fhliy1qww68qnp1pr1zdsyiq") (f (quote (("verbose" "log") ("print-generated") ("default"))))))

(define-public crate-simple_parse_derive-0.6.0 (c (n "simple_parse_derive") (v "0.6.0") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "1y7g19kmajq062k6mak2l6ga39ys0azsi0scmcj9j6s56a2xalwj") (f (quote (("verbose" "log") ("print-generated") ("default"))))))

(define-public crate-simple_parse_derive-0.6.1 (c (n "simple_parse_derive") (v "0.6.1") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "040p8kwi04cpgwp9h3529b2sr8xjaqmlaalks0nzrmqqbpmr0h6y") (f (quote (("verbose" "log") ("print-generated") ("default"))))))

(define-public crate-simple_parse_derive-0.6.2 (c (n "simple_parse_derive") (v "0.6.2") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "0bgld8k9qgg6n8lqcl54dlcydw7z49pnpgr8vxzykkvc43xnpwd0") (f (quote (("verbose" "log") ("print-generated") ("default"))))))

(define-public crate-simple_parse_derive-0.6.3 (c (n "simple_parse_derive") (v "0.6.3") (d (list (d (n "darling") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "02sz8y1cyzzjbzg62gd7751i7bn8s8v63ymjrp3xw5b3b7p4ffh4") (f (quote (("verbose" "log") ("print-generated") ("default"))))))

