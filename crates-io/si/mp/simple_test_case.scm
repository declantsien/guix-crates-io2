(define-module (crates-io si mp simple_test_case) #:use-module (crates-io))

(define-public crate-simple_test_case-1.0.0 (c (n "simple_test_case") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07b2fxhan3lig3cqhaq2dz0023ivg2v80rxrl73vjhq00svabk6m")))

(define-public crate-simple_test_case-1.1.0 (c (n "simple_test_case") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0npshpvgrqn57f5yajabc6bcaz5a68fv6xhfw1wqdjim2jqg13p2")))

(define-public crate-simple_test_case-1.2.0 (c (n "simple_test_case") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02jh4rclsv4aclziqvnspdzhqsvvsj7h3gfal76wl3dq83x4j1jx")))

