(define-module (crates-io si mp simple_s3) #:use-module (crates-io))

(define-public crate-simple_s3-0.1.0 (c (n "simple_s3") (v "0.1.0") (d (list (d (n "aws_auth") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (d #t) (k 0)))) (h "11bzaxw6aa00pziy6g9r89x5wd6mgn09w6axb0l8b51g6f0kah5p")))

