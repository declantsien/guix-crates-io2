(define-module (crates-io si mp simple-cgi) #:use-module (crates-io))

(define-public crate-simple-cgi-0.0.1 (c (n "simple-cgi") (v "0.0.1") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "0clhdpzf0m6fgb9hphc4y046fw2h02bagb170xvq4vm9x68lkbk9") (y #t)))

(define-public crate-simple-cgi-0.0.2 (c (n "simple-cgi") (v "0.0.2") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "17gsyj20dljj3pvasgn640pfdqs17xsnb0p8k2qsa3gc38vmnrpy") (y #t)))

(define-public crate-simple-cgi-0.0.3 (c (n "simple-cgi") (v "0.0.3") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "1bgvk3sjl4x7hbc9ikvwfvhrc4yz2qv41kazb1la6g5gnlla5zla") (y #t)))

(define-public crate-simple-cgi-0.0.4 (c (n "simple-cgi") (v "0.0.4") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "1i739scffnv5fzqkzdywl9cnz4g048m2fi1n530k6n1vdl8qja5j") (y #t)))

(define-public crate-simple-cgi-0.0.5 (c (n "simple-cgi") (v "0.0.5") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "0fihm7j89g73hywcwsd73ddfdydr4ahl163i9w7bbs7xbmc4478w") (y #t)))

