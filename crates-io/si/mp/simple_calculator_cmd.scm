(define-module (crates-io si mp simple_calculator_cmd) #:use-module (crates-io))

(define-public crate-simple_calculator_cmd-0.1.0 (c (n "simple_calculator_cmd") (v "0.1.0") (d (list (d (n "simple_calculator_deliver") (r "^0.1.0") (d #t) (k 0)))) (h "1qkj2lkyymf1pw70252h1p78ql98z3h4nhka9l6sgqc2wbkf2b2w")))

(define-public crate-simple_calculator_cmd-0.1.1 (c (n "simple_calculator_cmd") (v "0.1.1") (d (list (d (n "simple_calculator_deliver") (r "^0.1.1") (d #t) (k 0)))) (h "1iw2yv8ms5diianh2jnkzllz80kikq8bpg7s0zrwlzclp5afzkvm")))

