(define-module (crates-io si mp simple_parse) #:use-module (crates-io))

(define-public crate-simple_parse-0.1.0 (c (n "simple_parse") (v "0.1.0") (d (list (d (n "simple_parse_derive") (r "^0") (d #t) (k 0)))) (h "1my06g02hzch66rcclijh0015yn938f58cp3f1blwifb514bdm5p")))

(define-public crate-simple_parse-0.2.0 (c (n "simple_parse") (v "0.2.0") (d (list (d (n "simple_parse_derive") (r "^0") (d #t) (k 0)))) (h "1yp98g0p58k3rbrasgbrswxjcmjv4j854jzxpdgkkk0c2m2qmhcc")))

(define-public crate-simple_parse-0.2.1 (c (n "simple_parse") (v "0.2.1") (d (list (d (n "simple_parse_derive") (r "^0") (d #t) (k 0)))) (h "1v1azly7mmwzsvj0gxllvki0sswfr831kwrpjjq8nr7r0kc0javn")))

(define-public crate-simple_parse-0.3.0 (c (n "simple_parse") (v "0.3.0") (d (list (d (n "simple_parse_derive") (r "^0") (d #t) (k 0)))) (h "1mazr3s80lch30l5gbhm4k9ggf3xfklaikbdrv2lnbfy14hx2dr0")))

(define-public crate-simple_parse-0.4.0 (c (n "simple_parse") (v "0.4.0") (d (list (d (n "simple_parse_derive") (r "^0") (d #t) (k 0)))) (h "05iarq92yxnb45cady739l7r6qv2a42jh287v4502p6p8401v819")))

(define-public crate-simple_parse-0.4.1 (c (n "simple_parse") (v "0.4.1") (d (list (d (n "simple_parse_derive") (r "^0.4") (d #t) (k 0)))) (h "06iw6l5h8lz29bjr9z1k3x8ihfbwvwwawrqb39ha47mn2an5iqa4")))

(define-public crate-simple_parse-0.5.0 (c (n "simple_parse") (v "0.5.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "simple_parse_derive") (r "^0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "08bn2j0pbxnll43mr6s5rgxj3b4xmqqwr1wdh0s86i6i33k0v3lg") (f (quote (("verbose" "simple_parse_derive/verbose" "log") ("print-generated" "simple_parse_derive/print-generated") ("default"))))))

(define-public crate-simple_parse-0.6.0 (c (n "simple_parse") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "simple_parse_derive") (r "^0.6") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0xs1qn9gm3xvdw9na3csmsin4iiaay1bim6x0p4bf2hd3jd449pn") (f (quote (("verbose" "simple_parse_derive/verbose" "log") ("print-generated" "simple_parse_derive/print-generated") ("default"))))))

(define-public crate-simple_parse-0.6.1 (c (n "simple_parse") (v "0.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "simple_parse_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0dwfbqaz3a4lx10yfifycjws23b48iaf4asbka7jhzkj6c1qa03i") (f (quote (("verbose" "simple_parse_derive/verbose" "log") ("print-generated" "simple_parse_derive/print-generated") ("default"))))))

(define-public crate-simple_parse-0.6.2 (c (n "simple_parse") (v "0.6.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "simple_parse_derive") (r "^0.6.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0v91z2alk8ahw556c0qaqz0xj2w108rch773d6z42f2bjfkjh6pb") (f (quote (("verbose" "simple_parse_derive/verbose" "log") ("print-generated" "simple_parse_derive/print-generated") ("default"))))))

(define-public crate-simple_parse-0.6.3 (c (n "simple_parse") (v "0.6.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (o #t) (d #t) (k 0)) (d (n "simple_parse_derive") (r "^0.6.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0fiaalvbniyp9y3n0r383ddkzj57qbxpihpf443hgihwy6c9p0ky") (f (quote (("verbose" "simple_parse_derive/verbose" "log") ("print-generated" "simple_parse_derive/print-generated") ("default"))))))

