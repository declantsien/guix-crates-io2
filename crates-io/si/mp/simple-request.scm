(define-module (crates-io si mp simple-request) #:use-module (crates-io))

(define-public crate-simple-request-0.0.1 (c (n "simple-request") (v "0.0.1") (d (list (d (n "base64ct") (r "^1") (f (quote ("alloc"))) (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "tcp" "client" "backports" "deprecated"))) (k 0)) (d (n "hyper-rustls") (r "^0.24") (f (quote ("http1" "native-tokio"))) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "02fjh8cjddsgbnydnr6c70jz3ih1i2ci61hic03gjri6d2rbyczw") (f (quote (("basic-auth" "zeroize" "base64ct"))))))

