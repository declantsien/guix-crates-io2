(define-module (crates-io si mp simple_math) #:use-module (crates-io))

(define-public crate-simple_math-0.1.0 (c (n "simple_math") (v "0.1.0") (h "0pmpwrw7g48czn04v6869r3x3if5yzj6xk6iqhk0hcs7yfkrd68x")))

(define-public crate-simple_math-0.1.1 (c (n "simple_math") (v "0.1.1") (h "11l75qqx0b57ff34y3d1ksw52bxk0vz1ymy0r2fvxjds2s9gvmg3")))

(define-public crate-simple_math-0.1.2 (c (n "simple_math") (v "0.1.2") (h "0iz3pzcrqb3gcj4gb2qbfk5qlayhwdlx0rfyc3x5lvkg3sv8sbrw")))

