(define-module (crates-io si mp simple-tokenizer) #:use-module (crates-io))

(define-public crate-simple-tokenizer-0.1.0 (c (n "simple-tokenizer") (v "0.1.0") (h "1y45vlbfbkf6h6rbdp4dj8p1iljyp9xz8yf0nklcw313idyvlb57")))

(define-public crate-simple-tokenizer-0.2.0 (c (n "simple-tokenizer") (v "0.2.0") (h "0hlzbrvh8r6rfpbdgbgb3kqndxjf4z411bvwjvf8g7zd1asmx6f7")))

(define-public crate-simple-tokenizer-0.4.0 (c (n "simple-tokenizer") (v "0.4.0") (d (list (d (n "yap") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1f8z01z4r6rlf2i64c5l15bcfk4h75wfgqcf8hv96l7zwxbyn2hi") (f (quote (("default"))))))

(define-public crate-simple-tokenizer-0.4.1 (c (n "simple-tokenizer") (v "0.4.1") (d (list (d (n "yap") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0qfyk67i2p5vl4n90s4j8x5s6src0z5hdbb9pk3k8svs6r2jzfdm") (f (quote (("default"))))))

