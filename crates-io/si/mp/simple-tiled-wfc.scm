(define-module (crates-io si mp simple-tiled-wfc) #:use-module (crates-io))

(define-public crate-simple-tiled-wfc-0.7.4 (c (n "simple-tiled-wfc") (v "0.7.4") (d (list (d (n "bitsetium") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "00wh43m9d33zyvys94kd5l83i06sn281snjsqwsaliy7jf1fh96y")))

(define-public crate-simple-tiled-wfc-0.7.5 (c (n "simple-tiled-wfc") (v "0.7.5") (d (list (d (n "bitsetium") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1p5nmyw8v53xp0yyv00clz3ak5956vk1d1fdkv0kfkrrfcf3piri")))

(define-public crate-simple-tiled-wfc-0.7.7 (c (n "simple-tiled-wfc") (v "0.7.7") (d (list (d (n "bitsetium") (r "^0.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05nk90zkd50d0808dhv1crfdibaidgc9fwhh1vvzh7s27z105fs1")))

