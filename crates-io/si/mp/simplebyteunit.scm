(define-module (crates-io si mp simplebyteunit) #:use-module (crates-io))

(define-public crate-simplebyteunit-0.1.0 (c (n "simplebyteunit") (v "0.1.0") (h "1m3bwvx0m6vdr2wi2wxyzmi9zgyp7hw55fn62whjg3rj3vg3am9i")))

(define-public crate-simplebyteunit-0.1.1 (c (n "simplebyteunit") (v "0.1.1") (h "1nhjbqnjr0xcaxb0983whckiqwmlc8d0531l9vc15v1w9igb2x09")))

(define-public crate-simplebyteunit-0.2.0 (c (n "simplebyteunit") (v "0.2.0") (h "14flakahb2j3ycw2vsv96l9q1n420a844r8slxj4w8fhp8fdpp6s")))

(define-public crate-simplebyteunit-0.2.1 (c (n "simplebyteunit") (v "0.2.1") (h "0419g1m68bf0s7pg4k68rfjk9kll1dqj5hmsn7y0ch20s86il02k")))

