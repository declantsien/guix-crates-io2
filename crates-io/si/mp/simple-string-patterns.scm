(define-module (crates-io si mp simple-string-patterns) #:use-module (crates-io))

(define-public crate-simple-string-patterns-0.1.0 (c (n "simple-string-patterns") (v "0.1.0") (h "0wdgcp0g1xhc5b2sfs9ibgynhkh0hvdcwdzplzb1va7yj49airbi") (y #t)))

(define-public crate-simple-string-patterns-0.1.1 (c (n "simple-string-patterns") (v "0.1.1") (h "1d4vhz5vjv92rpvb7idhscgzvfcjb7j7h5zr6346828lcsgqwf8l") (y #t)))

(define-public crate-simple-string-patterns-0.1.2 (c (n "simple-string-patterns") (v "0.1.2") (h "0nyqmlanq6lvdwzirycaa7k4fgdhwswxx19yxazllh785kxgr558") (y #t)))

(define-public crate-simple-string-patterns-0.1.3 (c (n "simple-string-patterns") (v "0.1.3") (h "0h5ry8dpbdkqib04fq95r81wsz0zwr50w75q5r7hbhf298lxbkcz") (y #t)))

(define-public crate-simple-string-patterns-0.1.4 (c (n "simple-string-patterns") (v "0.1.4") (h "1mxadb3n35abn6xhx9sk6kgga66s5y3szvkqjyvka734kvcy5nbd") (y #t)))

(define-public crate-simple-string-patterns-0.1.5 (c (n "simple-string-patterns") (v "0.1.5") (h "1vvl18247ybc3q2ynzyn402xznpy8x1h36pgsq638n6rknhmz2pg") (y #t)))

(define-public crate-simple-string-patterns-0.1.6 (c (n "simple-string-patterns") (v "0.1.6") (h "11smlmmmfrnn4g2y13k9cvrlmjm2pqsrm2sq51icdka4kf02si55") (y #t)))

(define-public crate-simple-string-patterns-0.1.7 (c (n "simple-string-patterns") (v "0.1.7") (h "1s9kgwp5yaynyirhxw7i9yiqsgg0inclnr6wa8q4794bx0b325is") (y #t)))

(define-public crate-simple-string-patterns-0.2.0 (c (n "simple-string-patterns") (v "0.2.0") (h "0i6rj9raspjkicccq18dckndicixqap4503v7q3nrk9rxjqraril") (y #t)))

(define-public crate-simple-string-patterns-0.2.1 (c (n "simple-string-patterns") (v "0.2.1") (h "1fc270clmnzq323fin9hlyhw817h5c957sk7vc2c4f2yr90141b6") (y #t)))

(define-public crate-simple-string-patterns-0.2.2 (c (n "simple-string-patterns") (v "0.2.2") (h "0rbzbih96cj9s62kgcsxqynq373khdzd5x5hjb3ywqhc9xwwnc2v") (y #t)))

(define-public crate-simple-string-patterns-0.2.3 (c (n "simple-string-patterns") (v "0.2.3") (h "0lpfw51hs2yrb2v3544m8jffc699n7w5icw1spchc0in1xnk70g6") (y #t)))

(define-public crate-simple-string-patterns-0.2.4 (c (n "simple-string-patterns") (v "0.2.4") (h "01136rhwbk3sf4n80jid3nq9kglmnpf9rnnl37ncm9i0k8j6i4dc") (y #t)))

(define-public crate-simple-string-patterns-0.2.5 (c (n "simple-string-patterns") (v "0.2.5") (h "17w1i8c8xyyw69r8chjgnpfb77qbrnwjx3j431laig3pidkl2cwz")))

(define-public crate-simple-string-patterns-0.3.0 (c (n "simple-string-patterns") (v "0.3.0") (h "1vnzczw13isrk6jb2whaxpsr2g605ddpgpma1agf1zj2jyn869dn") (y #t)))

(define-public crate-simple-string-patterns-0.3.1 (c (n "simple-string-patterns") (v "0.3.1") (h "0clfkmqskh3kby8dhnfcm8x5yxp4mq9p0knlx8b2xqfgdbkzf2k1") (y #t)))

(define-public crate-simple-string-patterns-0.3.2 (c (n "simple-string-patterns") (v "0.3.2") (h "08x6sbq56v026dhhgixc78k4lg4xpq196d66c9pwfhci5m64bd0s") (y #t)))

(define-public crate-simple-string-patterns-0.3.3 (c (n "simple-string-patterns") (v "0.3.3") (h "0rb5j3irbm8xhxggdq1qqn9prjyywcsygicha4iwbycypl217hdw") (y #t)))

(define-public crate-simple-string-patterns-0.3.4 (c (n "simple-string-patterns") (v "0.3.4") (h "0y0adm0cm1sx7n58q4spbn5mnfbmny6zjjnd5cqc3w3x2d0xxc4r") (y #t)))

(define-public crate-simple-string-patterns-0.3.5 (c (n "simple-string-patterns") (v "0.3.5") (h "1s7iavaq7cxq5lfvcliidnf3iiqlhn1hp8y0031y4n71p5pzk6vb") (y #t)))

(define-public crate-simple-string-patterns-0.3.6 (c (n "simple-string-patterns") (v "0.3.6") (h "1gycrws9gq8s2bmix2m1igfw9mlqihkz5kh12x1ly5bfnmp148n5") (y #t)))

(define-public crate-simple-string-patterns-0.3.7 (c (n "simple-string-patterns") (v "0.3.7") (h "1s4wys5nkzr6g09l53pv9qhb5i26v8a3jzbckx1y4izv9zd5y4ad") (y #t)))

(define-public crate-simple-string-patterns-0.3.8 (c (n "simple-string-patterns") (v "0.3.8") (h "1qdps191558fg2ch7n8vfl4vd0g569mqln4s7q7fahanzzipwcch") (y #t)))

(define-public crate-simple-string-patterns-0.3.9 (c (n "simple-string-patterns") (v "0.3.9") (h "15myyrnwxfrk2zzcz0p3bpq1g1v4i1zaiffa89ndk7h9mkxds6xw") (y #t)))

(define-public crate-simple-string-patterns-0.3.10 (c (n "simple-string-patterns") (v "0.3.10") (h "1xyxl0zdd0f6k5nc8bz2rz16gqwgc0g19frlwl7wfkq8d6m7d82w") (y #t)))

(define-public crate-simple-string-patterns-0.3.11 (c (n "simple-string-patterns") (v "0.3.11") (h "03vby7r031biacmc8mk4ng37gx64jfxf56xbiaw9fhabln8fybv0") (y #t)))

(define-public crate-simple-string-patterns-0.3.12 (c (n "simple-string-patterns") (v "0.3.12") (h "030db0r4gk33mw9c16iphkrlh1xj92nj4cjs8qq0w7ishvch0asq") (y #t)))

(define-public crate-simple-string-patterns-0.3.13 (c (n "simple-string-patterns") (v "0.3.13") (h "0c1zvwyqv983x7f36vr769rh706bxpinvhajivzjh35hssi38hbc") (y #t)))

(define-public crate-simple-string-patterns-0.3.14 (c (n "simple-string-patterns") (v "0.3.14") (h "14pdr9jvs074q34jxwf3fppyzv045j1b78rnjiq5lqn4dhdybhi1")))

