(define-module (crates-io si mp simple-test-bbarekas) #:use-module (crates-io))

(define-public crate-simple-test-bbarekas-0.1.0 (c (n "simple-test-bbarekas") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.2.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xkzpmf4nf6wcj9655f3bf36sfvzdykjv3mzaxmlikai2sgdzzwa")))

(define-public crate-simple-test-bbarekas-0.1.1 (c (n "simple-test-bbarekas") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.2.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07qji4y1bsjb0aw813ayci04661w1mf323148f315djc0c4n2ig6")))

(define-public crate-simple-test-bbarekas-0.1.2 (c (n "simple-test-bbarekas") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "1piimlidn7aww9vd4afn6w6aj6rzvgqwagadg943iylapa4k6smf")))

(define-public crate-simple-test-bbarekas-0.1.3 (c (n "simple-test-bbarekas") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 2)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "15i26rxpbgqqw4ddd1jb42k0bk7jwgwm3z7jy96hjy2sd84s92bg")))

