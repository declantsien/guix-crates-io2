(define-module (crates-io si mp simple_dot) #:use-module (crates-io))

(define-public crate-simple_dot-0.1.0 (c (n "simple_dot") (v "0.1.0") (d (list (d (n "unique_id") (r "^0.1.5") (d #t) (k 0)))) (h "05a3rkv725jrrd0vkxq5753w9ib9c1phcrsvgfxjvnkm6yq6248g")))

(define-public crate-simple_dot-0.1.1 (c (n "simple_dot") (v "0.1.1") (d (list (d (n "unique_id") (r "^0.1.5") (d #t) (k 0)))) (h "0ylvqcislhvs7hmcrxyl6bg0788cdhpdvzdcnhxqp4nsxj7rlxm1")))

