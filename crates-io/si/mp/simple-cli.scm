(define-module (crates-io si mp simple-cli) #:use-module (crates-io))

(define-public crate-simple-cli-0.1.0 (c (n "simple-cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0hir1rkypkwiqljs5r8q2y1lz7gidiay4wi0ivb4hbdw8fqrzdgm")))

(define-public crate-simple-cli-0.1.1 (c (n "simple-cli") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "086gckblm74m0a6icc8q9jfqcgwi47nqznrix4nx4w3gkq5ci1zm")))

