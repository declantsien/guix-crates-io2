(define-module (crates-io si mp simple-soft-float) #:use-module (crates-io))

(define-public crate-simple-soft-float-0.1.0 (c (n "simple-soft-float") (v "0.1.0") (d (list (d (n "algebraics") (r ">= 0.1.2, < 0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("num-bigint"))) (o #t) (d #t) (k 0)))) (h "06cncfv8h1rcvqivph9l4v7jr7rnfgs59cr2ywz75a3p2yqki0sx") (f (quote (("python-extension" "python" "pyo3/extension-module") ("python" "pyo3" "once_cell") ("default"))))))

