(define-module (crates-io si mp simple_asn1) #:use-module (crates-io))

(define-public crate-simple_asn1-0.1.0 (c (n "simple_asn1") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "14ffca44l18wm5s3p90ax6kp8kpzibs7v3wg6x215zhra2q8g9cg")))

(define-public crate-simple_asn1-0.2.0 (c (n "simple_asn1") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0mw1jya8ykm8vd8h3x2vd3pljfd3jfylc59w9xbp0crf7blg5vh7")))

(define-public crate-simple_asn1-0.2.1 (c (n "simple_asn1") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0l5kyzxj7m873rdhz67y2nl2kanj7fhipj97kfn0s75rxyybpxjp")))

(define-public crate-simple_asn1-0.3.0 (c (n "simple_asn1") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0mirjz1ifsmsjkyy35mm5z96qvxaznj78x6pg1iw4qzyc3qif0pn")))

(define-public crate-simple_asn1-0.3.1 (c (n "simple_asn1") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "15l80lpl39jvky4y716r560464b93c8mdmdj6jccidfd0kinp8vh")))

(define-public crate-simple_asn1-0.4.0 (c (n "simple_asn1") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0676jbw1h7pjv8s0mlhq9dih6ahjn5j2mhnnjw64y9b5f6xfq99b")))

(define-public crate-simple_asn1-0.4.1 (c (n "simple_asn1") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0jxy9as8nj65c2n27j843g4fpb95x4fjz31w6qx63q3wwlys2b39")))

(define-public crate-simple_asn1-0.5.0 (c (n "simple_asn1") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0r9hhwcp9kfb4j8nqjxwl3nzsd0607ivglb9bzwscsp8lgg5niir")))

(define-public crate-simple_asn1-0.5.1 (c (n "simple_asn1") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)))) (h "0l5c1xkpn0lmcl8x95xgmqhvlk05wib9k6qjvlchzsv6rrzmk3fv")))

(define-public crate-simple_asn1-0.5.2 (c (n "simple_asn1") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)))) (h "0qsjcnvkdlvakhynvy4823hbrblwx9wb8k45isjzabr4wmv903kf")))

(define-public crate-simple_asn1-0.5.3 (c (n "simple_asn1") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)))) (h "04wfr96ncv7ic9wsflzp352k37j2kfa39ydqlb9j2hxd6k7yccfw")))

(define-public crate-simple_asn1-0.5.4 (c (n "simple_asn1") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)))) (h "103s209pwzfwqmqx2ibkmdsx6i8hay0gcg8izhfwh79hzdhfmd4f")))

(define-public crate-simple_asn1-0.6.0 (c (n "simple_asn1") (v "0.6.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing" "quickcheck"))) (k 0)))) (h "0m8327mhinhbd74d23yavavjb5897amap3b0k5riy5wgisv95qv9")))

(define-public crate-simple_asn1-0.6.1 (c (n "simple_asn1") (v "0.6.1") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing" "quickcheck"))) (k 0)))) (h "0iapyiaf0ipwkjivrrmarslycb1pwfmzihmrjk391fdr70f2nxja")))

(define-public crate-simple_asn1-0.6.2 (c (n "simple_asn1") (v "0.6.2") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1") (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing"))) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing" "quickcheck"))) (k 2)))) (h "11d0l3l7lppzr1wxhvsbmjmw6s2vy3v7b8ygz500z4di9qhfbi5d")))

