(define-module (crates-io si mp simple_clustering) #:use-module (crates-io))

(define-public crate-simple_clustering-0.1.0 (c (n "simple_clustering") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("std" "suggestions" "derive"))) (o #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("jpeg" "png"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "palette") (r "^0.6") (f (quote ("std"))) (k 0)))) (h "1y6js44gf4fvhbyj3i8i6zdw0v5gamg72ry7qqbnm3jvwgw13wdn") (f (quote (("default" "app") ("app" "clap" "image"))))))

(define-public crate-simple_clustering-0.1.1 (c (n "simple_clustering") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("std" "suggestions" "derive"))) (o #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("jpeg" "png"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "palette") (r "^0.6") (f (quote ("std"))) (k 0)))) (h "0l42vg9vqj2n7g3hg2cd8bp80a60f11dm15x1x2gc7h44hdiznbz") (f (quote (("default" "app") ("app" "clap" "image"))))))

(define-public crate-simple_clustering-0.2.0 (c (n "simple_clustering") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("std" "suggestions" "derive"))) (o #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("jpeg" "png"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("std"))) (k 0)) (d (n "palette") (r "^0.7.2") (f (quote ("std"))) (k 0)))) (h "0k0an5x3flimz6yy9hzkmjjcsdk64nyqvx3z4j87g5nws2fzkv26") (f (quote (("default" "app") ("app" "clap" "image"))))))

