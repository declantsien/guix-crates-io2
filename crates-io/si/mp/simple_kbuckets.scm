(define-module (crates-io si mp simple_kbuckets) #:use-module (crates-io))

(define-public crate-simple_kbuckets-0.1.0 (c (n "simple_kbuckets") (v "0.1.0") (h "0pp5khklbd3gikczjbkrsj039m8sf57kh92xhaqw5bbpwzndjdjf")))

(define-public crate-simple_kbuckets-0.1.1 (c (n "simple_kbuckets") (v "0.1.1") (h "1w50x368naw4hi32zk2nw73i8hs6jiplf0izqvizd61kc95jzlpq")))

(define-public crate-simple_kbuckets-0.2.0 (c (n "simple_kbuckets") (v "0.2.0") (h "0qadz5ca01b11dfvf74mffrhpgfxs37r83cgb0gwh2ykhszrb75w")))

