(define-module (crates-io si mp simple-aes256-gcm) #:use-module (crates-io))

(define-public crate-simple-aes256-gcm-0.1.0 (c (n "simple-aes256-gcm") (v "0.1.0") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.9.0") (d #t) (k 0)) (d (n "grcov") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1sb5jhlki1iz1in029r9k3bkd787csimid3wb76nnh12mncvr3zq")))

(define-public crate-simple-aes256-gcm-0.1.1 (c (n "simple-aes256-gcm") (v "0.1.1") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "grcov") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1gm75xl4kk2pql4ac4g9kw0w2q2g52l24c049xnqkply2fkxgr8l")))

(define-public crate-simple-aes256-gcm-0.2.0 (c (n "simple-aes256-gcm") (v "0.2.0") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "grcov") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1vsn8by6qf4xgrs4f12i84v9d5c6zrj6p03vf046s6ih0wapngm6")))

(define-public crate-simple-aes256-gcm-0.2.1 (c (n "simple-aes256-gcm") (v "0.2.1") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06kcyfnrpfninb6lmfr4yjvkvxixvh461l5kh6490la97b7b9cx4")))

(define-public crate-simple-aes256-gcm-0.2.2 (c (n "simple-aes256-gcm") (v "0.2.2") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1a2bmpw0cgf28imn1az02fx678srp4vwqcn6qqwca4lpgp2nbr7r")))

