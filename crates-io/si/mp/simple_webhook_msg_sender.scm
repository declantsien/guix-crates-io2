(define-module (crates-io si mp simple_webhook_msg_sender) #:use-module (crates-io))

(define-public crate-simple_webhook_msg_sender-0.0.1 (c (n "simple_webhook_msg_sender") (v "0.0.1") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "18wqqbl3m5cxmlxxvaiik4lzzz7x045dkhqg5s4913pcqb162dzb")))

