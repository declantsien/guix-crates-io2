(define-module (crates-io si mp simple_delaunay_lib) #:use-module (crates-io))

(define-public crate-simple_delaunay_lib-0.1.0 (c (n "simple_delaunay_lib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "robust") (r "^1.1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 2)))) (h "1hjf616bksvfija3v5njchb5740pd8r1p91ahjsalpbmkcxlximn")))

(define-public crate-simple_delaunay_lib-0.2.0 (c (n "simple_delaunay_lib") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "robust") (r "^1.1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 2)))) (h "1xpx8j4v3wngygkx6h0vkb2ybnpw2n7si806cdlg0jlv8wzk6ywn")))

