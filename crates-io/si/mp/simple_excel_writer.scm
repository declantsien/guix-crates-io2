(define-module (crates-io si mp simple_excel_writer) #:use-module (crates-io))

(define-public crate-simple_excel_writer-0.1.0 (c (n "simple_excel_writer") (v "0.1.0") (h "1azrcb82mc3ccgsvxg36l7i353wqvrb2l2dlc8pwa8agbnnn53g2")))

(define-public crate-simple_excel_writer-0.1.1 (c (n "simple_excel_writer") (v "0.1.1") (h "1a38ln99h9yyv2i1jjxjq4qnyflfdxjav3f1y7w9xg3l6ikbc1ry")))

(define-public crate-simple_excel_writer-0.1.2 (c (n "simple_excel_writer") (v "0.1.2") (h "117fqszc3v1j41555hgyqlk3ax99vy01d26fdj92cw14dz5mkwi1")))

(define-public crate-simple_excel_writer-0.1.3 (c (n "simple_excel_writer") (v "0.1.3") (h "0gcn9j5dnl70fyy8ipv9879isdq0f8fgim3p8s8igni6zw5dw06s")))

(define-public crate-simple_excel_writer-0.1.4 (c (n "simple_excel_writer") (v "0.1.4") (h "016zdfs8gx8jr95rvrm5ficncijzrlmpcjmxh0xxskjijhir41wa")))

(define-public crate-simple_excel_writer-0.1.5 (c (n "simple_excel_writer") (v "0.1.5") (d (list (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "067nb8b3pls9p6sf7pcjvsnsnb24i80nn5ab6jq2jvlzczmpdp1g")))

(define-public crate-simple_excel_writer-0.1.7 (c (n "simple_excel_writer") (v "0.1.7") (d (list (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "144j90dw6nwpqj0vwhfbzyv2kzsxywp1mwlarfihlwppmmlk2b5q")))

(define-public crate-simple_excel_writer-0.1.8 (c (n "simple_excel_writer") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1b8ihs0rcmlb4dv7aw11n07gm4rfhi406bwmcadcfziz5agrndgk")))

(define-public crate-simple_excel_writer-0.1.9 (c (n "simple_excel_writer") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (k 0)) (d (n "zip") (r "^0.5.13") (f (quote ("deflate" "time"))) (k 0)))) (h "0p6bx1fr8vlmjcvi31aa8mqd67wy2dwdwlvszrwdya5pqbw31ckd")))

(define-public crate-simple_excel_writer-0.2.0 (c (n "simple_excel_writer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (k 0)) (d (n "zip") (r "^0.5.13") (f (quote ("deflate"))) (k 0)))) (h "1a3n06hr5p5ylj4fs0aly6rx16l89k4ac0gb1dg14pg1a0iy65ar")))

