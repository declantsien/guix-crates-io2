(define-module (crates-io si mp simple_prompts) #:use-module (crates-io))

(define-public crate-simple_prompts-0.1.0 (c (n "simple_prompts") (v "0.1.0") (d (list (d (n "rustyline") (r "^6.1") (d #t) (k 0)))) (h "0wb0ljsy57mp142kqqlibfj8b33f3cwdwsfllh4h372nazvfix6c")))

(define-public crate-simple_prompts-0.1.1 (c (n "simple_prompts") (v "0.1.1") (d (list (d (n "rustyline") (r "^6.1") (d #t) (k 0)))) (h "1rdrnmg7qw7p9x6dk5pmsx0hqx8ilml8mpg1lhkdk9xnpgi543f2")))

(define-public crate-simple_prompts-0.1.2 (c (n "simple_prompts") (v "0.1.2") (d (list (d (n "rustyline") (r "^6.1") (d #t) (k 0)))) (h "0idddajs51n2m9nv1lzlc6gk21g6gpgiqli3jfgsd15rk1lblhg1")))

(define-public crate-simple_prompts-0.1.3 (c (n "simple_prompts") (v "0.1.3") (d (list (d (n "rustyline") (r "^6.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "0pw7y3hc562wqrq2r85mcny5kv74hv9l3159q5mkfg5farya0aqm")))

