(define-module (crates-io si mp simple_mdns_client) #:use-module (crates-io))

(define-public crate-simple_mdns_client-1.0.0 (c (n "simple_mdns_client") (v "1.0.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "154npfj5pbc4jzqfc411ssq7sgk7d51r9ns6620pcsryfvqqsshs")))

(define-public crate-simple_mdns_client-1.1.0 (c (n "simple_mdns_client") (v "1.1.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1d4ydba1zhajxpldxpm8m9863fw4zx0llm7rf3s57m8c3w6cvgq5")))

(define-public crate-simple_mdns_client-2.0.0 (c (n "simple_mdns_client") (v "2.0.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1z5kga6q1gnfi3mwaklpw1whpvsg6w2ndvh665gd2h1dcp9sm614")))

(define-public crate-simple_mdns_client-3.0.0 (c (n "simple_mdns_client") (v "3.0.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1f6mcim308692hawk3a463wq7s6r6yq5zx57cp04dh66vl4gp01w")))

