(define-module (crates-io si mp simplemad_sys) #:use-module (crates-io))

(define-public crate-simplemad_sys-0.1.0 (c (n "simplemad_sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0y3innncdnab8ngf3qckz98dcwjahly0b1h759j8kiga8yxd80ww")))

(define-public crate-simplemad_sys-0.2.0 (c (n "simplemad_sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "009cgffxcchy8jfbi0zr98xsndkaagjcqz3326zgmb1039kyb89w")))

(define-public crate-simplemad_sys-0.3.0 (c (n "simplemad_sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0a5l0mz3kwlwm551h04scr5bs2l86shhqk7w5wwam7k5pk9z8271")))

(define-public crate-simplemad_sys-0.4.0 (c (n "simplemad_sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08r0bb7jk8cv6riq577bimrbfj8l9jxg6sccc66wj6blgqjxj8pp")))

(define-public crate-simplemad_sys-0.5.0 (c (n "simplemad_sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19wksvh6rh35zx5429z07lcw9z0q8ic5ry5722swq1dqprq6snsn")))

