(define-module (crates-io si mp simple_http) #:use-module (crates-io))

(define-public crate-simple_http-0.1.0 (c (n "simple_http") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.6") (f (quote ("async-await"))) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 0)))) (h "1bzvlv03ya86mfkx54zg2mrgxs15nqwnj4mh2r65g8xal0c2zcnh")))

