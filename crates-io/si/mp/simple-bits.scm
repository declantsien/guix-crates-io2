(define-module (crates-io si mp simple-bits) #:use-module (crates-io))

(define-public crate-simple-bits-1.0.0 (c (n "simple-bits") (v "1.0.0") (h "0s41c3x7zjy18hzs1yz0khz1jv3abi1bcl861wagmxns8vxi35if")))

(define-public crate-simple-bits-1.0.1 (c (n "simple-bits") (v "1.0.1") (h "0ba3h5x26dsvdjb0wwxa3q3kwfn8wwjpxqphkxwl1nzwyymkdbfd")))

