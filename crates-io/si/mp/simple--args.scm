(define-module (crates-io si mp simple--args) #:use-module (crates-io))

(define-public crate-simple--args-1.0.0 (c (n "simple--args") (v "1.0.0") (h "1cx002rlqvkaijx7yhrf91w2n84vdf8vqgiw02z1vm4g9bv7b79x")))

(define-public crate-simple--args-1.1.0 (c (n "simple--args") (v "1.1.0") (h "1afkiywgy5mzswprqjp9yx4sgvyqixh8lbwxhdc6rpwd8zvy9c9z")))

(define-public crate-simple--args-1.1.1 (c (n "simple--args") (v "1.1.1") (h "11sp9j9l67fbk2422ywlpbagzkbcbgszqnsa2pbacbwx4vblwf18")))

