(define-module (crates-io si mp simpl) #:use-module (crates-io))

(define-public crate-simpl-0.1.0-alpha.1 (c (n "simpl") (v "0.1.0-alpha.1") (h "01byh64a83xdm1fx6ncp82iq06cdnqmlyz2x7xfp05qk4vklf7zj")))

(define-public crate-simpl-0.1.0 (c (n "simpl") (v "0.1.0") (h "04f7f6hfz5dksmabk445vy6abi7gjs0albqw1zw5a0qwj46g2c1a")))

