(define-module (crates-io si mp simple-semver) #:use-module (crates-io))

(define-public crate-simple-semver-0.1.0 (c (n "simple-semver") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1b6n24gf1fb0s0shix6n73bxld9wmw1g6hqifh9j700xmkzhxpkm")))

