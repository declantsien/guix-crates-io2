(define-module (crates-io si mp simplisp_extensions) #:use-module (crates-io))

(define-public crate-simplisp_extensions-0.3.0 (c (n "simplisp_extensions") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.4.2") (d #t) (k 0)) (d (n "simplisp") (r "^0.3") (d #t) (k 0)))) (h "0hyvgxcmxqg600rspcnhzf44477rdj1hrk1jrn24anacmwdjqbkm")))

(define-public crate-simplisp_extensions-0.4.0 (c (n "simplisp_extensions") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "simplisp") (r "^0.4") (d #t) (k 0)))) (h "1g8q20rsky85krfhisy1ry1zh7726zf3i35vb3fphlilbrc7nm55")))

