(define-module (crates-io si mp simplify-rs) #:use-module (crates-io))

(define-public crate-simplify-rs-0.1.0 (c (n "simplify-rs") (v "0.1.0") (h "1wl6b4h7wf3cfshjvs9x8pk60gaxi5wzz6cigskf20ggr5c569xl") (f (quote (("double_precision") ("default"))))))

(define-public crate-simplify-rs-0.1.1 (c (n "simplify-rs") (v "0.1.1") (h "14k0cd2grsdi3mjx645vnz3kinqkgr6lnarmjjl825wbmkcfnr70") (f (quote (("double_precision") ("default"))))))

(define-public crate-simplify-rs-0.1.2 (c (n "simplify-rs") (v "0.1.2") (h "12wx50ksadwmcd14qfkjaclg3k54nnzaw4ygzidvd2kinic8c572") (f (quote (("double_precision") ("default"))))))

(define-public crate-simplify-rs-0.1.3 (c (n "simplify-rs") (v "0.1.3") (h "0raf6i38c33fw55qh67g9p031912cjllr5iblp5kd63bsphz1ahr") (f (quote (("double_precision") ("default"))))))

(define-public crate-simplify-rs-0.1.4 (c (n "simplify-rs") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "10zsnjvsszgly4c6j0s7pn9c9706i7hl55c27xqy368n4rqyhwwq") (f (quote (("double_precision") ("default"))))))

(define-public crate-simplify-rs-0.1.5 (c (n "simplify-rs") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1wclj8ia75xq9isw7s7ij6kprh6n7cwgd25f1yjjbyska767jsgq") (f (quote (("double_precision") ("default"))))))

