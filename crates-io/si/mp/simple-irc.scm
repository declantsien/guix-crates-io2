(define-module (crates-io si mp simple-irc) #:use-module (crates-io))

(define-public crate-simple-irc-0.1.0 (c (n "simple-irc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ghdc2ir4kv3kj55sm4ib9n6hqsh1ml924kiib8azad97p30bjlm")))

(define-public crate-simple-irc-0.2.0 (c (n "simple-irc") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c70ryqahbjhw1lqzfcc04aakjnym6xx5yxz0z9nvjrhy72bkni9")))

(define-public crate-simple-irc-0.2.1 (c (n "simple-irc") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12dps6w0x7xhsm55ig7r4pv48xd17pcsn8fyy9x4l9mm8fyngr37")))

(define-public crate-simple-irc-0.3.0 (c (n "simple-irc") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vx4425fyqmfk8calv197g75s688cqxiy6ff88vr1ji14p33h95g")))

(define-public crate-simple-irc-0.3.1 (c (n "simple-irc") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lkacbx4bj8cs2vicyjdm5hp4qggb6c2c5c3q8afqiyc8xl7x47n")))

(define-public crate-simple-irc-0.3.2 (c (n "simple-irc") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08dp48bpd1vj7wz8bxwcv0am5jjxwdd3gqdmc4ys9gawkpih679i")))

