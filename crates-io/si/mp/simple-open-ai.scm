(define-module (crates-io si mp simple-open-ai) #:use-module (crates-io))

(define-public crate-simple-open-ai-0.1.0 (c (n "simple-open-ai") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nfi27xkjv93nlwgalkrva3azz2b4zkp4zmjyalbyvhhnnxrdim8")))

