(define-module (crates-io si mp simple_trie) #:use-module (crates-io))

(define-public crate-simple_trie-0.1.0 (c (n "simple_trie") (v "0.1.0") (h "026rbk589v2dkspcbv94xilq4byndijl6nk09n7gjb5zlvcsfycl")))

(define-public crate-simple_trie-0.1.1 (c (n "simple_trie") (v "0.1.1") (h "0bg7fg562xpyza91ngz15ynn67n5a36lapq75k2h24dca8q5hfqr")))

(define-public crate-simple_trie-0.1.2 (c (n "simple_trie") (v "0.1.2") (h "05cd8mdivxbkf303593dz1612g2nswh58w2gsmlh8n0si7ma7z6a")))

(define-public crate-simple_trie-0.1.3 (c (n "simple_trie") (v "0.1.3") (h "0f7snda0j6bdnavbzkrx78yywh113j8wlii1grym02h7gma1575d")))

(define-public crate-simple_trie-0.1.4 (c (n "simple_trie") (v "0.1.4") (h "1qx5czv1dqc1j6gvvchx6yswfwvg6xlw185w64zm8ixrvj98rpxr")))

(define-public crate-simple_trie-0.1.5 (c (n "simple_trie") (v "0.1.5") (h "0bplfmq19480v2syq2bqs07pm26cyf3zvgnddw2xx8yiavg7ajqg")))

(define-public crate-simple_trie-0.1.6 (c (n "simple_trie") (v "0.1.6") (h "0yh70clhsqf3izci8lxkbngpcn7f7ldrq6qfzrqs179smf9p7h80")))

(define-public crate-simple_trie-0.1.7 (c (n "simple_trie") (v "0.1.7") (h "0axhlph3lm1r4hlyim9igmxb7qgy0w8v2badwdlq4wvja55hl5rp")))

(define-public crate-simple_trie-0.1.8 (c (n "simple_trie") (v "0.1.8") (h "0ia16bkffgnphw6qhfrl5q9dyk12f8lsjs7kpc7dqxdq3qpv2823")))

(define-public crate-simple_trie-0.1.9 (c (n "simple_trie") (v "0.1.9") (h "1w6pimx1z3xjwliz366a1y2f1qilmya0i6pi9m5fl6pwd3s7yhn2")))

(define-public crate-simple_trie-0.1.10 (c (n "simple_trie") (v "0.1.10") (h "02vdlrkqci0ydh33m8qbn9rz1fkpg25c5ln2iyvv8651bn4879ba")))

(define-public crate-simple_trie-0.1.11 (c (n "simple_trie") (v "0.1.11") (h "17igx89q1ygv3y3ls0anhgxi9y0flfvp9k3ccapqrv6km08w99xa")))

(define-public crate-simple_trie-0.1.12 (c (n "simple_trie") (v "0.1.12") (h "1q6ig0rh4flnbg4zfj3q17q2r4n84015wl198fr2mv7z33vhabda")))

(define-public crate-simple_trie-0.1.13 (c (n "simple_trie") (v "0.1.13") (h "0l6f8m4vczgc24whqb33w43gwjlxd569d5qp1cr7idfnishq51cb")))

(define-public crate-simple_trie-0.1.14 (c (n "simple_trie") (v "0.1.14") (h "03mxj87ph170crmidami1nx8ndjs9zn45s8pgsr57lmagh5j5qr6")))

(define-public crate-simple_trie-0.1.15 (c (n "simple_trie") (v "0.1.15") (h "0rc6fhg2xd2pnihd4pdrci6jwfn7xva9xrvvnix2wriwqng3hs23")))

(define-public crate-simple_trie-0.1.16 (c (n "simple_trie") (v "0.1.16") (h "1645kwj3zqvj78dm4cidv5hga0d8yi4dzc7h01y7914a493fnxyh")))

(define-public crate-simple_trie-0.1.17 (c (n "simple_trie") (v "0.1.17") (h "0603g9q5cgq74a553sq1c527g9ccq3616ry51r9pn0f6im763nn5")))

(define-public crate-simple_trie-0.1.18 (c (n "simple_trie") (v "0.1.18") (h "1kgmrqm94i98k9rjbfb8kw8zj6c42hbmdcgikjvds4587psab30k")))

(define-public crate-simple_trie-0.1.19 (c (n "simple_trie") (v "0.1.19") (h "1gpbv285wc35bya9pn1p3mkc8sr4i5sqzcpmi69xsgwc1jc6r4d8")))

(define-public crate-simple_trie-0.1.20 (c (n "simple_trie") (v "0.1.20") (h "1w64y96wa8jf8yq7rrmbws6aidxj07lgq60a04ayn9706yrc0aw5")))

(define-public crate-simple_trie-0.1.21 (c (n "simple_trie") (v "0.1.21") (h "0qhps709wmzzcw16v3ss3la0z72nzdx9dw4r763ndh6y104nw73l")))

(define-public crate-simple_trie-0.1.22 (c (n "simple_trie") (v "0.1.22") (h "0z2zxckhc9vda9v00yz6c30yq6nj8aipkmlklbl92xppiyhzga29")))

