(define-module (crates-io si mp simple-cards) #:use-module (crates-io))

(define-public crate-simple-cards-1.0.0 (c (n "simple-cards") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dx3q1mxkmzmkkgsr68v1ldnzv0p1fx7mhh3q0f1nzkgk68xr514")))

(define-public crate-simple-cards-1.0.1 (c (n "simple-cards") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fi3l0wmdym117pcdqblk1lgm6aw5a2izb40g0z3fn1wilvai8ga")))

(define-public crate-simple-cards-1.0.2 (c (n "simple-cards") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "074dyjrhblx0azyanbi4brdv62zzvb2c35f2wi0k6qs6mh9iz27m")))

(define-public crate-simple-cards-1.0.3 (c (n "simple-cards") (v "1.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xi1wm9i47i9yhp1852n09jr4nbmn92wp2wrhqz71rh7bh00r684")))

(define-public crate-simple-cards-1.0.4 (c (n "simple-cards") (v "1.0.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1006cyjyi6jn0jxn4d7636zkhp0ypn80apmxxfpl4ixggmsmsi64")))

(define-public crate-simple-cards-1.0.5 (c (n "simple-cards") (v "1.0.5") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0vgpl9wi0l4qj6h4h4vv61w96hh7aymmbqv3afxhd61cypp02g6y")))

(define-public crate-simple-cards-1.0.6 (c (n "simple-cards") (v "1.0.6") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0ii22qppc6hl6k6q792ivn363i0fr375qmr41mvxg3x46r1grcwp")))

(define-public crate-simple-cards-1.0.7 (c (n "simple-cards") (v "1.0.7") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0bds3j3yca9pccbxd2c1wjbmf5h68f875grqsaxfzb52z6xln5cf")))

(define-public crate-simple-cards-1.1.0 (c (n "simple-cards") (v "1.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0y2r1qavqbpgpwm9s20lxjqblirb0lvqr2swcpnh4wpvqvsqqxmm")))

