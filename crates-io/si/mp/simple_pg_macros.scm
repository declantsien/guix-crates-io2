(define-module (crates-io si mp simple_pg_macros) #:use-module (crates-io))

(define-public crate-simple_pg_macros-0.1.0 (c (n "simple_pg_macros") (v "0.1.0") (h "12n7w56517p7h5mfn23wvahllhg91g1pfn5xs79bvbyvk7zj2gbs")))

(define-public crate-simple_pg_macros-0.2.0 (c (n "simple_pg_macros") (v "0.2.0") (h "0pbcqyhc8814q9s8950lw6c5kda1dxz5xc5nyjw6lf5cd2ald1qx")))

(define-public crate-simple_pg_macros-0.3.0 (c (n "simple_pg_macros") (v "0.3.0") (h "0wyxcz7mjanzmkjfn0mcywbmhgdjd9izxvvdlmgadvszkbxqq95g")))

(define-public crate-simple_pg_macros-0.4.0 (c (n "simple_pg_macros") (v "0.4.0") (h "0h2sz452aggfphhfbjzpm59ry5qah8qir2fq4yd6s6m5mjhjv9sc")))

