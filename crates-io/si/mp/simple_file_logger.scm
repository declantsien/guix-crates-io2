(define-module (crates-io si mp simple_file_logger) #:use-module (crates-io))

(define-public crate-simple_file_logger-0.1.0 (c (n "simple_file_logger") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.1") (d #t) (k 0)))) (h "180xy0z0rlhas3mwwc72jzrg7pn5hdy3fkc13k9q9i388h7424cg")))

(define-public crate-simple_file_logger-0.1.1 (c (n "simple_file_logger") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.1") (d #t) (k 0)))) (h "18nfxzy6xk9bgq2r6iddk6zrkpaz273qva6h7jpk5iyi0fi6rizk")))

(define-public crate-simple_file_logger-0.2.0 (c (n "simple_file_logger") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.1") (d #t) (k 0)))) (h "0zwnkj2f1f9acvz8pnhz0klsd05l3r89vzv406126zmk54hyk6ff")))

(define-public crate-simple_file_logger-0.3.0 (c (n "simple_file_logger") (v "0.3.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24.1") (d #t) (k 0)))) (h "1fn3qyvwd2jjffbpjbjhrlghgax33mrxljh2sqrzp0w6vfp79hkd")))

(define-public crate-simple_file_logger-0.3.1 (c (n "simple_file_logger") (v "0.3.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24.1") (d #t) (k 0)))) (h "13y181mmdw43kknvwp6lfaivz9dbbrzp3fq72l8ihckrggwj9j1g")))

(define-public crate-simple_file_logger-0.4.0 (c (n "simple_file_logger") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mp56pwsqjvwvnhn6m1x08g3vr8lj6wq36dbdflw4di2gyrjg9cx") (s 2) (e (quote (("serde" "dep:serde") ("clap" "dep:clap"))))))

(define-public crate-simple_file_logger-0.4.1 (c (n "simple_file_logger") (v "0.4.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mcq6kd8a1lfrzips55k5jjh2gmbhfd3vgiirlknw1zm6s780b8y") (s 2) (e (quote (("serde" "dep:serde") ("clap" "dep:clap"))))))

