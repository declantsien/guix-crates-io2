(define-module (crates-io si mp simple_colatz) #:use-module (crates-io))

(define-public crate-simple_colatz-0.1.0 (c (n "simple_colatz") (v "0.1.0") (h "19kianjmg2zr0rv4dbl7f8dsmszikw0ph1gk8q4ypzphvbcc7xf4")))

(define-public crate-simple_colatz-0.1.1 (c (n "simple_colatz") (v "0.1.1") (h "11minn91s8s5l6s16cjjhimqbl7b5yp8q3p3biash54rd0zhlfff")))

(define-public crate-simple_colatz-0.1.2 (c (n "simple_colatz") (v "0.1.2") (h "0yhk3zd87yp76asq252284icq9grjw2gymws26j48ji6dpfhd821")))

(define-public crate-simple_colatz-0.1.4 (c (n "simple_colatz") (v "0.1.4") (h "0zjj23bzsbp8cg5mrpl0bhg6sqz9vfwc21a9yqrpa1pzsmhjlr2k")))

