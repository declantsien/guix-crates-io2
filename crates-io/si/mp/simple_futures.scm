(define-module (crates-io si mp simple_futures) #:use-module (crates-io))

(define-public crate-simple_futures-0.1.0 (c (n "simple_futures") (v "0.1.0") (d (list (d (n "atomic_swapping") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "04jy2dmk6ra3d4yp718siz6508ydsqpc34ckccnxwyxrkq7dvs8j") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-simple_futures-0.1.1 (c (n "simple_futures") (v "0.1.1") (d (list (d (n "atomic_swapping") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0wc8ay8cygsbgb5givyh88jmfs2k4z4ii9krpr82x4z7ci8yzgj2") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-simple_futures-0.1.2 (c (n "simple_futures") (v "0.1.2") (d (list (d (n "atomic_swapping") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0xm34hrfpmnqrskqks58prvyrxhh8mamyvihhnglpq866ykp27sk") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

