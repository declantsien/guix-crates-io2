(define-module (crates-io si mp simplestore) #:use-module (crates-io))

(define-public crate-simplestore-0.0.1 (c (n "simplestore") (v "0.0.1") (h "1il6b2028bsj1i9mnfn9zwycivml1qks8mjnadv7km1imc44w7nm")))

(define-public crate-simplestore-0.0.2 (c (n "simplestore") (v "0.0.2") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)))) (h "1g3rzbcs2qh6qpwa0n947d3yc11qg204p4xkbphpx9kb4cnaw9hv")))

(define-public crate-simplestore-0.0.3 (c (n "simplestore") (v "0.0.3") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)))) (h "0mm1h30f3xbq6dl5fg5617jb4mxambd231cm01hd00awqhkiyr5b")))

(define-public crate-simplestore-0.0.4 (c (n "simplestore") (v "0.0.4") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)))) (h "0rdx3y4m24i2bfz6l1wlfzvzryg81sv2vl2krmn4f9pmxmlsr2w6")))

