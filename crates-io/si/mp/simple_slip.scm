(define-module (crates-io si mp simple_slip) #:use-module (crates-io))

(define-public crate-simple_slip-0.1.0 (c (n "simple_slip") (v "0.1.0") (h "17z45k7p5cav5jklmhf72n5r74qdcsm2xv1x75b3x3wbyx4cdk23")))

(define-public crate-simple_slip-0.1.1 (c (n "simple_slip") (v "0.1.1") (h "0xk35a2afga9nczxx9pmc3bhl1pzilhkzh37qllma4i4w15f03ml")))

(define-public crate-simple_slip-0.1.2 (c (n "simple_slip") (v "0.1.2") (h "0qq7jrzzs69yp3hlf7g6lp53x9z3p3jbi1xybw0dh7w7xldv6bfk")))

