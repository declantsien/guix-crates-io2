(define-module (crates-io si mp simple-color) #:use-module (crates-io))

(define-public crate-simple-color-0.1.0 (c (n "simple-color") (v "0.1.0") (d (list (d (n "data-stream") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0lx4kchqc1ilm3p9wdc2hj8d3j627hrkpcrfs7n6vm7zqvd6yqbv") (f (quote (("parser" "token-parser"))))))

(define-public crate-simple-color-0.2.0 (c (n "simple-color") (v "0.2.0") (d (list (d (n "data-stream") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)))) (h "139p1fkmk1ma6dly7f9d2bm0a4nb6b0vyj8kambjh3dlv9k9i1mj") (f (quote (("parser" "token-parser"))))))

(define-public crate-simple-color-0.2.1 (c (n "simple-color") (v "0.2.1") (d (list (d (n "data-stream") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0129k141amjfw01ryzs9bkgmxd1y9k75sr95q51xq7fr9b67zgv2") (f (quote (("parser" "token-parser"))))))

