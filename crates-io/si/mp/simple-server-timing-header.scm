(define-module (crates-io si mp simple-server-timing-header) #:use-module (crates-io))

(define-public crate-simple-server-timing-header-0.1.0 (c (n "simple-server-timing-header") (v "0.1.0") (h "1j9hcq4pcqa6h75g67157dzc33y5m7rmmnhsz7qxq4wfg5kvdai4")))

(define-public crate-simple-server-timing-header-0.1.1 (c (n "simple-server-timing-header") (v "0.1.1") (h "0vh82k18a2c32gj30633hlasv25l0k8llnj37l98x6sww0cqkrqn")))

