(define-module (crates-io si mp simple-pso-demo-rs) #:use-module (crates-io))

(define-public crate-simple-pso-demo-rs-0.1.0 (c (n "simple-pso-demo-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dwxkszlzdrbr7vm0q4g1gh9xzvh8syw8frxqwy7qxm51x8096w5")))

(define-public crate-simple-pso-demo-rs-0.1.1 (c (n "simple-pso-demo-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0amrxjxlg898f7nk3cz1hkaclfppd7046n098748cdi75pwdyyrw")))

(define-public crate-simple-pso-demo-rs-0.1.2 (c (n "simple-pso-demo-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q1dkzpdchabhiwxxk3qx09q3f4ajxn6mix4iiy3j8z9r3ndrqbg")))

