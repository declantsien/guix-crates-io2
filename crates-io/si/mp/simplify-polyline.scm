(define-module (crates-io si mp simplify-polyline) #:use-module (crates-io))

(define-public crate-simplify-polyline-0.1.0 (c (n "simplify-polyline") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sdplm5bfw8n0lha5c5wy8xvpmqgzyqaydp1pxdjf19h7rhb5k5q") (s 2) (e (quote (("tests" "dep:serde" "dep:serde_json"))))))

(define-public crate-simplify-polyline-0.2.0 (c (n "simplify-polyline") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13c4wjkk3139cmajhia2s1qc4sz7gydihy45mlndsvh7dqj8k0rm") (s 2) (e (quote (("tests" "dep:serde" "dep:serde_json"))))))

(define-public crate-simplify-polyline-0.2.1 (c (n "simplify-polyline") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vdxb7fy713ivckk5nbzgli7ak4aqjmkz8wh6v3kqf6zf0xg08x6") (s 2) (e (quote (("tests" "dep:serde" "dep:serde_json"))))))

(define-public crate-simplify-polyline-0.3.0 (c (n "simplify-polyline") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "19gqps6sbxxvyz6d6p8bbf9gbz4mf4iyrbyg6dv9hsll0sip6nci") (s 2) (e (quote (("tests" "dep:serde" "dep:serde_json"))))))

(define-public crate-simplify-polyline-0.4.0 (c (n "simplify-polyline") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yb9awvls8injxjcq0p6v0986ygwkrn117v3nc9kkpjz1y1i2i0g") (s 2) (e (quote (("tests" "dep:serde" "dep:serde_json"))))))

(define-public crate-simplify-polyline-0.5.0 (c (n "simplify-polyline") (v "0.5.0") (d (list (d (n "criterion") (r "=0.4.0") (d #t) (k 2)) (d (n "memchr") (r "=2.6.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "=6.5.1") (d #t) (k 2)) (d (n "rayon-core") (r "=1.11.0") (d #t) (k 2)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.107") (d #t) (k 2)))) (h "17nn1xzcrhybmg0y8wp4iiajcv4byhif4sq21j60jpk9d3bzfdnj") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

