(define-module (crates-io si mp simple-dns) #:use-module (crates-io))

(define-public crate-simple-dns-0.1.0 (c (n "simple-dns") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "10a2d982jscv40svaafcril23bqnx40rzchv9npfc8jy7nz9cm0r")))

(define-public crate-simple-dns-0.1.1 (c (n "simple-dns") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "01jhcaih5gi414fwp05gj6jr8m4bzlvw1w7mdnggw8vrrnji8yyc")))

(define-public crate-simple-dns-0.2.0 (c (n "simple-dns") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1fls1xwwcwlsxmzy7g6mx4mnblljg8ccv9hnblaz0n9dar30zr12") (y #t)))

(define-public crate-simple-dns-0.2.1 (c (n "simple-dns") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0yqz3744yrzgch5w2is998ykq248ki62s5aicxp76qnrr4ckskny")))

(define-public crate-simple-dns-0.3.0 (c (n "simple-dns") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "04m30ksqx7fppc880k8kmrqaryivds49p684lxi6v4rll6sr3zw2")))

(define-public crate-simple-dns-0.4.0 (c (n "simple-dns") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ckfig98x3dwn7lpskzwhdq0gxvz0dwxrs9f2dz4lnq929jp4637")))

(define-public crate-simple-dns-0.4.1 (c (n "simple-dns") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qyf31h8qb35jfs6a14sl9dg7h8clxlkbxmqnjwwq4pwxih01m9g")))

(define-public crate-simple-dns-0.4.2 (c (n "simple-dns") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1r2vj7p3dygdbq98a87k8d941vcrihpfbmwb8kg5c15j2n2kn97c")))

(define-public crate-simple-dns-0.4.3 (c (n "simple-dns") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1j4qgvnr3p9fwidbz9wa2n7w07n16y4nzzg342pylbfjlhgdczp0")))

(define-public crate-simple-dns-0.4.4 (c (n "simple-dns") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0kl18ccd3gall5k7vkkff6zrg6ilimgsis4szc3jq3akzyvg807s")))

(define-public crate-simple-dns-0.4.5 (c (n "simple-dns") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0z00x8vjiqgwlfj993rav5y4ykxwihm0qrzvpwv20w4x9zcgcl71")))

(define-public crate-simple-dns-0.4.6 (c (n "simple-dns") (v "0.4.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1pjjshylcisv1fk88xg02r23v99c2nx3qavnfl8m3hr10yxp2x4w")))

(define-public crate-simple-dns-0.4.7 (c (n "simple-dns") (v "0.4.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04p23pf7qkdkhxz3k0wq5fwqp348vkn5zid3f668gpba8wsjxk40")))

(define-public crate-simple-dns-0.5.0 (c (n "simple-dns") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0kpgwy490q041rpnrsijgbf9yk2ihjax08z0md5xba5sjc27bapn")))

(define-public crate-simple-dns-0.5.1 (c (n "simple-dns") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0kzlxk9fl24maafwn4836vmd4jxp4znl3g6g25nb183lck2rgqpv")))

(define-public crate-simple-dns-0.5.2 (c (n "simple-dns") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "17vmfng0iy3qmwwv98sf0dr3b8snx1y2p0q27ib3g3k4rxvlixa4")))

(define-public crate-simple-dns-0.5.3 (c (n "simple-dns") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hf1m5j4nysr9yx5qc7i2qi9sjgdajs2qpnk3mym0g07b951afsf")))

(define-public crate-simple-dns-0.5.4 (c (n "simple-dns") (v "0.5.4") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "07hpgp2xh91fyh45583rsz321xpxkchmymsznyx8qmvr8fddpx1y")))

(define-public crate-simple-dns-0.5.5 (c (n "simple-dns") (v "0.5.5") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1pbybil7qzj0fifmzfwmic11iqgm268xfbs9xygv8gr4rd9y9izc")))

(define-public crate-simple-dns-0.5.6 (c (n "simple-dns") (v "0.5.6") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1620szyf4s7k5agrwhy2vw5d5y6ma0wzcmkszsfzdfkahbm4nlqp")))

(define-public crate-simple-dns-0.5.7 (c (n "simple-dns") (v "0.5.7") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "154n8yhv7a7krg1m1xhry7h4w4bv4kj0gq60yjbnvyysvbya7sfa")))

(define-public crate-simple-dns-0.6.0 (c (n "simple-dns") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0hr3xkishqgzcwkm1pk6x3pn4ynd32gmrpcd638xyp1mfard1l18")))

(define-public crate-simple-dns-0.6.1 (c (n "simple-dns") (v "0.6.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1smhnigklc8h3p1rrfm8vbvfz0n6f1s349vnk2m71hfahivzzpps")))

(define-public crate-simple-dns-0.6.2 (c (n "simple-dns") (v "0.6.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0n4rai0s78bndm5rwgcsnw4gnwxhmc1n3cn0dn64d50qwvi7yq01")))

(define-public crate-simple-dns-0.7.0 (c (n "simple-dns") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0mriqflnx0ccjjbv4rjx35kh4p6vj9fdzhi5f45b87nj6xrra99n")))

