(define-module (crates-io si mp simple-observable) #:use-module (crates-io))

(define-public crate-simple-observable-0.1.0 (c (n "simple-observable") (v "0.1.0") (h "0bazf5pyhgk32wglwrk061kb7gbypxj3bm8kgjiq5j6rwqid0p76")))

(define-public crate-simple-observable-0.1.1 (c (n "simple-observable") (v "0.1.1") (h "003pjahrrqrrgqb72bd4gslr53xg1727gz2s5qvwdbzh4c05hf2y")))

(define-public crate-simple-observable-0.2.0 (c (n "simple-observable") (v "0.2.0") (h "181wirwwrw8c61j7fgp04s968q8bs2ymq8yk86b0j9y50ymvif0h")))

(define-public crate-simple-observable-0.2.1 (c (n "simple-observable") (v "0.2.1") (h "0b8ja1zm8y90yxpqvc7r26a4l63wpag0xcwbgw63ya6wlijgk7db")))

(define-public crate-simple-observable-0.2.2 (c (n "simple-observable") (v "0.2.2") (h "14p9fv7p9nb48vw1wknkfqh61h045qc411r1jxyzlnk692rsviqg")))

