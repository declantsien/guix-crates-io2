(define-module (crates-io si mp simple_random_numbers) #:use-module (crates-io))

(define-public crate-simple_random_numbers-0.1.0 (c (n "simple_random_numbers") (v "0.1.0") (h "1bjg13knhssir308rrk8f5hrf2sqakyhi0cjz4p1wrpisvdb1zib")))

(define-public crate-simple_random_numbers-0.1.1 (c (n "simple_random_numbers") (v "0.1.1") (h "044wngmszp6xj4gk2g7jhy9bklyszdv5z0w48g1vq4ys5ik42hnc")))

