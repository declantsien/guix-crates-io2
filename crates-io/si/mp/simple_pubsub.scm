(define-module (crates-io si mp simple_pubsub) #:use-module (crates-io))

(define-public crate-simple_pubsub-0.5.0 (c (n "simple_pubsub") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12dsxdnzwjbfjzza9sfwg72m1fwf5apzn3sgmx5i2jbcc62xd896") (y #t)))

(define-public crate-simple_pubsub-0.5.1 (c (n "simple_pubsub") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wk2j6y8cw4002pga6pbgiqdw9wxi2wri0p81q9i3aqcy6r8pmjm") (y #t)))

(define-public crate-simple_pubsub-0.5.2 (c (n "simple_pubsub") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1pj8znr75db545wzj0bwp6nwglh4pcq090vxwyfq9il8m5ahn6q1")))

(define-public crate-simple_pubsub-0.5.3 (c (n "simple_pubsub") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0vifhmidy2jf10pfnlsb1klghj52z7311gq3r5i7m3a2s2zkgghq")))

