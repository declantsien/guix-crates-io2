(define-module (crates-io si mp simple-jsonrpc-client) #:use-module (crates-io))

(define-public crate-simple-jsonrpc-client-0.1.0 (c (n "simple-jsonrpc-client") (v "0.1.0") (d (list (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h67f8vkds3msbg3gkn0is96yfpm4xv4fna965x1g7h7vm8y6dxp")))

