(define-module (crates-io si mp simple-logging) #:use-module (crates-io))

(define-public crate-simple-logging-1.0.0 (c (n "simple-logging") (v "1.0.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "thread-id") (r "^3.2.0") (d #t) (k 0)))) (h "0r9p99h3lgdy8aqvplg3hnl7lnkzlij2hxn8gqkrj4c1yhbjh3wx")))

(define-public crate-simple-logging-1.0.1 (c (n "simple-logging") (v "1.0.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "thread-id") (r "^3.2.0") (d #t) (k 0)))) (h "1dwkmpn1xv5wk0p8q7bpq2nr0frwiaz2gzp0ba9dhrcf3zymanwh")))

(define-public crate-simple-logging-2.0.0 (c (n "simple-logging") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "thread-id") (r "^3.2.0") (d #t) (k 0)))) (h "0ldwdwnlq2bzdarzp3b8s4d08mpy6jc21s857wnw1hamsp7983r0")))

(define-public crate-simple-logging-2.0.1 (c (n "simple-logging") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "thread-id") (r "^3.2.0") (d #t) (k 0)))) (h "0gf5vnaii7p6h3qwgc1050rzvk52q1p1iykkabdfpw3k1k9s99v4")))

(define-public crate-simple-logging-2.0.2 (c (n "simple-logging") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "thread-id") (r "^3") (d #t) (k 0)))) (h "0hmm523f0ax76yljf3z178rn9cm0q6knwa52haqnnckmavl4h3dh")))

