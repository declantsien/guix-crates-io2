(define-module (crates-io si mp simpdiscover) #:use-module (crates-io))

(define-public crate-simpdiscover-0.1.0 (c (n "simpdiscover") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "0f5w9sah3dp9i703wkqi49snbs3p0z3hrsab82f2n2hm0l5qsfjb")))

(define-public crate-simpdiscover-0.1.1 (c (n "simpdiscover") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "0mzmggs57a6nd39gr4d631q6ply8jyin564n1aqqxvx4mm774vps")))

(define-public crate-simpdiscover-0.1.2 (c (n "simpdiscover") (v "0.1.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "06ab8zd29p0ppfblas5bbpjc3bc6x2xwqxdrg66dqjvpbfkx09pb")))

(define-public crate-simpdiscover-0.1.3 (c (n "simpdiscover") (v "0.1.3") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "0k0hqff0b2asaxvfrrjnwzq9s3i38sh2qifxm5z799c4mn0ijaaf")))

(define-public crate-simpdiscover-0.1.4 (c (n "simpdiscover") (v "0.1.4") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "198wpp46q5cmaa2g9sriki5z7ngh1z94bwd82bqsih0adahwiq1p")))

(define-public crate-simpdiscover-0.1.5 (c (n "simpdiscover") (v "0.1.5") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "01diaj4sdjap88x1wjvcb6d31r291vc9gymrzfg97swn1yv20nix")))

(define-public crate-simpdiscover-0.1.6 (c (n "simpdiscover") (v "0.1.6") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "0m1aqz4m7jm53655m478p4xr3gbkw1rkmrmm0la9znyilrmx639w")))

(define-public crate-simpdiscover-0.2.0 (c (n "simpdiscover") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "190liyqfavsxq9lhc5w5fh7ylfwwsrvxdqmah1ngf7r3xicz7pqk")))

(define-public crate-simpdiscover-0.2.1 (c (n "simpdiscover") (v "0.2.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.3") (d #t) (k 0)))) (h "1dqh4kjh5piignc16rlxarm6lr61xk7p3abr0gjvz42gmjr1s743")))

(define-public crate-simpdiscover-0.3.0 (c (n "simpdiscover") (v "0.3.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.4") (d #t) (k 0)))) (h "021rka0a0c5ra6w5srxz97771ap5001s8qv1z0df6iwzk9snk5r1")))

(define-public crate-simpdiscover-0.3.1 (c (n "simpdiscover") (v "0.3.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simplog") (r "~1.5") (d #t) (k 0)))) (h "1d6y2jfhjl4zmkf2z4lgn89y566kr04ppmgb8yivjxlbfs3sgi3v")))

(define-public crate-simpdiscover-0.4.0 (c (n "simpdiscover") (v "0.4.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 0)) (d (n "simplog") (r "~1.5") (d #t) (k 0)))) (h "1q4jv49x0x1ql9x5zzjzs46kbn45s22aqgzgy151hxxjpw1vajkp")))

(define-public crate-simpdiscover-0.4.1 (c (n "simpdiscover") (v "0.4.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 0)) (d (n "simplog") (r "~1.5") (d #t) (k 0)))) (h "1diyrkw83bs4bga9gc3cvn6rcxlq5gzhlb2wxm4km0km0ba74ljq")))

(define-public crate-simpdiscover-0.5.0 (c (n "simpdiscover") (v "0.5.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "simplog") (r "~1.5") (d #t) (k 0)))) (h "1xp38aclz9p4a2v5fpnv974xqmkgdrwsmxmdj65d8h3m55kh4w23")))

(define-public crate-simpdiscover-0.5.1 (c (n "simpdiscover") (v "0.5.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "simplog") (r "~1.5") (d #t) (k 0)))) (h "1ll1vxmhkyrn5s98a19xvypmxqdcyb83lkvgn7qh6zg77780aqjn")))

(define-public crate-simpdiscover-0.6.0 (c (n "simpdiscover") (v "0.6.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "simplog") (r "~1.6") (d #t) (k 0)))) (h "1cyhhnsjccvkv9mjcpb6m49cfyhw0pq5qh938dpjwh3q332cv6ks")))

(define-public crate-simpdiscover-0.6.1 (c (n "simpdiscover") (v "0.6.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "simplog") (r "~1.6") (d #t) (k 0)))) (h "1wj2x5xxl5hkxz8hjmpqq9nkg5symcwdww8p7lzs5znbjcmz7wx9")))

(define-public crate-simpdiscover-0.6.2 (c (n "simpdiscover") (v "0.6.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "simplog") (r "~1.6") (d #t) (k 0)))) (h "1kf92ngp65iss7dwvbsph1ap2n29yj42x6fc47jicmn619f350qr")))

(define-public crate-simpdiscover-0.7.0 (c (n "simpdiscover") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)))) (h "11j5dq7d8k9736pjhpdc7filwkqlzdidxfd4xkfpsp6w7f1v6m1j")))

