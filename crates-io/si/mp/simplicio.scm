(define-module (crates-io si mp simplicio) #:use-module (crates-io))

(define-public crate-simplicio-0.1.0 (c (n "simplicio") (v "0.1.0") (h "06cnkw85dgaqb1hsvfsihk8khjaslsjkwgfjhlfh3cjzibpch975")))

(define-public crate-simplicio-0.1.1 (c (n "simplicio") (v "0.1.1") (h "0lh8m6505g2i2lzln440maz1ard4444hasl6j6dwa4kbk4qs3n9g")))

(define-public crate-simplicio-0.1.2 (c (n "simplicio") (v "0.1.2") (h "1l38fll406pgflmni483yml7v5rssz7b6kz51zv6axj7rr4af91j")))

(define-public crate-simplicio-0.1.3 (c (n "simplicio") (v "0.1.3") (h "0pdvlw88dksgw7k5m2kj9mlcwzfvh9gx9aa6108wcas4k66aiiw5")))

