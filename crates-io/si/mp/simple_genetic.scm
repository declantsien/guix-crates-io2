(define-module (crates-io si mp simple_genetic) #:use-module (crates-io))

(define-public crate-simple_genetic-0.1.0 (c (n "simple_genetic") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00ix2nsf7ifk5hpf64ysq87dmqidnsv5gmclw7a91164ma1f3gyh")))

(define-public crate-simple_genetic-0.1.1 (c (n "simple_genetic") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02cb8yq7ndcvkk1mmwmd8lhv0p7nz6csand3w3y0j8p5fz2rnkl2")))

(define-public crate-simple_genetic-0.1.2 (c (n "simple_genetic") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x3lqhfpcrlli1clj5v6jmza25i6i8gijy6xzb3zd1vy1y27f6pk")))

(define-public crate-simple_genetic-0.2.0 (c (n "simple_genetic") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s1yw6hwackf26ihk754s4f7kmj2wwc2iw2ibz2ja3dv60w9rxn4")))

