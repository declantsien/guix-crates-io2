(define-module (crates-io si mp simple-si-units-core) #:use-module (crates-io))

(define-public crate-simple-si-units-core-0.2.0 (c (n "simple-si-units-core") (v "0.2.0") (h "0d5a51i5gj61fjyl8zr8rvjijwwz2yaf98zbd7wcvpxhfbva37gc")))

(define-public crate-simple-si-units-core-0.2.1 (c (n "simple-si-units-core") (v "0.2.1") (h "13vjd9pwk66kk5rs03pi5vl9msq4qzr1p45xifjgki8x6b504bvc")))

(define-public crate-simple-si-units-core-0.3.0 (c (n "simple-si-units-core") (v "0.3.0") (h "146pzcd1f9s464ni7w1iprdzvdwwxg7idzzfr8lp5px8zn6fwvgn")))

(define-public crate-simple-si-units-core-0.3.1 (c (n "simple-si-units-core") (v "0.3.1") (h "1qcy78vp6n00lgy1x9b4fy4vijfzq82j6k090nkvfb0yz1q6ra5z")))

(define-public crate-simple-si-units-core-0.3.2 (c (n "simple-si-units-core") (v "0.3.2") (h "1m0hzxdnmnsahfx9y2wirgacjry9j64rva5k40f10jgq1clqwwwg")))

(define-public crate-simple-si-units-core-0.3.3 (c (n "simple-si-units-core") (v "0.3.3") (h "0hibgg1z3nqaym8s028my3yi08f4fnzj7gdp6q07i7m6pzl2pf4v")))

(define-public crate-simple-si-units-core-0.3.4 (c (n "simple-si-units-core") (v "0.3.4") (h "0fhbhnk63i9bcnx3mrka8n3i0g8wmw3zfrw097lrdynq14azwdc1")))

(define-public crate-simple-si-units-core-0.3.5 (c (n "simple-si-units-core") (v "0.3.5") (h "1pw26jgrq9rq124k6iq8bgdplsnvg08jfzw3985lxwka23s9wn1s")))

(define-public crate-simple-si-units-core-0.4.0 (c (n "simple-si-units-core") (v "0.4.0") (h "0cvzqys2m40l26gzlamffjh6bs5w30336wl7ic3rvhak1f9v4jvy")))

(define-public crate-simple-si-units-core-0.4.2 (c (n "simple-si-units-core") (v "0.4.2") (h "0lz694f67c3grbmmgdpbckl9hblflx9pqgsyxpslbzl9zrp3izkr")))

(define-public crate-simple-si-units-core-0.5.0 (c (n "simple-si-units-core") (v "0.5.0") (h "1qkl2d9fh8hv63lgcbzzy3xmx9r1m6y16s9c6649dnac1rlvzi9p")))

(define-public crate-simple-si-units-core-0.7.1 (c (n "simple-si-units-core") (v "0.7.1") (h "07wwcqbjkc0q39hzfsc6hnq2qb1cx8hv3cgbwjhxysnm9qk8s4gs")))

(define-public crate-simple-si-units-core-0.7.3 (c (n "simple-si-units-core") (v "0.7.3") (h "0mr2c27m6rkgmb6mrng65j49q6il9xi19qrfhjjqa50m5wm6mac0")))

(define-public crate-simple-si-units-core-0.7.4 (c (n "simple-si-units-core") (v "0.7.4") (h "0fx3a9lrvxxa28hwlim88hjv3h7cpyyc5qmwh9yklgx6jm1c2va2")))

(define-public crate-simple-si-units-core-0.7.5 (c (n "simple-si-units-core") (v "0.7.5") (h "1kxm5sjjsqb297bqvfdjxpfls1744dm7h3lcgmrzllw3gwklbiv6")))

(define-public crate-simple-si-units-core-0.7.7 (c (n "simple-si-units-core") (v "0.7.7") (h "1dkcaj4sj2aq70k93z2bj31n7fmyb187i193gc16rixc3va0yp61")))

(define-public crate-simple-si-units-core-0.8.0 (c (n "simple-si-units-core") (v "0.8.0") (h "1aqnmqphrbx8jcaghqvr64kh17s81fvcvn76dg31ihr5hx23xqaa")))

(define-public crate-simple-si-units-core-0.8.5 (c (n "simple-si-units-core") (v "0.8.5") (h "1kbji3c9igh23hbbrvdk2hia09j0lax0v5bqcislhwlyk14ww5lj")))

(define-public crate-simple-si-units-core-0.9.0 (c (n "simple-si-units-core") (v "0.9.0") (h "0xnhflxy75p3lyv76qgk69qqfgy2lc6zi51plc7hnl33pjsxqkgn")))

(define-public crate-simple-si-units-core-1.0.0 (c (n "simple-si-units-core") (v "1.0.0") (h "078wqff7j125hjw22acb59rqp3wsrxr3pb6xfx8j45ipryyw0dwi")))

(define-public crate-simple-si-units-core-1.0.1 (c (n "simple-si-units-core") (v "1.0.1") (h "1rc29p5g7ag1mwi7rcsx2jlgacfbwjfkrxjzvsmmrw9qv902x4i0")))

