(define-module (crates-io si mp simple_tables) #:use-module (crates-io))

(define-public crate-simple_tables-0.1.0 (c (n "simple_tables") (v "0.1.0") (d (list (d (n "simple_tables-core") (r "^0.1.0") (d #t) (k 0)) (d (n "simple_tables-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)))) (h "0dwfm727jday95kqh5pbr6l90w4a4hylns5k0pi93f6ngzgr2cg3")))

(define-public crate-simple_tables-0.1.1 (c (n "simple_tables") (v "0.1.1") (d (list (d (n "simple_tables-core") (r "^0.1.1") (d #t) (k 0)) (d (n "simple_tables-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)))) (h "1gb0ayv5mbxr86c95pa41r0qr01g3ab2rq1iya4fmpr7c3pqlxzb")))

(define-public crate-simple_tables-0.2.0 (c (n "simple_tables") (v "0.2.0") (d (list (d (n "simple_tables-core") (r "^0.2.0") (d #t) (k 0)) (d (n "simple_tables-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)))) (h "1vqjg5vd51z5m0frl49l039480y5wi96g6rfax1581nsk2ym41g4")))

(define-public crate-simple_tables-0.2.1 (c (n "simple_tables") (v "0.2.1") (d (list (d (n "simple_tables-core") (r "^0.2.1") (d #t) (k 0)) (d (n "simple_tables-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)))) (h "0yibzd08n2wfgbpng9dcmq25br25gd7qhr7q5ywqh6hmy8ypbabx")))

(define-public crate-simple_tables-0.3.0 (c (n "simple_tables") (v "0.3.0") (d (list (d (n "simple_tables-core") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_tables-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0k6fdhsp8ynf9l8jywnvzhr5s019lkl0w2ddqn9k2kvp2fhql951")))

