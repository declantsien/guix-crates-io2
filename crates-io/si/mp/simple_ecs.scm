(define-module (crates-io si mp simple_ecs) #:use-module (crates-io))

(define-public crate-simple_ecs-0.1.2 (c (n "simple_ecs") (v "0.1.2") (h "0spswnm01f6gzzpz2l079vljjq266kxcc6irqq23kki85rqdvffv")))

(define-public crate-simple_ecs-0.1.3 (c (n "simple_ecs") (v "0.1.3") (h "0j7sp581ahpbj2iddaaqv2xbp2di5xndxcx60axjvjksndr8d89h")))

(define-public crate-simple_ecs-0.1.4 (c (n "simple_ecs") (v "0.1.4") (h "115yn0w6x4ggw13303i7rhai6rizmm25y1wz4r0537jmgdlgvyd2")))

(define-public crate-simple_ecs-0.1.5 (c (n "simple_ecs") (v "0.1.5") (h "1a0dgg87lsv94nhhrhcjqlpijc7nsz9fifs937xak871663znbc0")))

(define-public crate-simple_ecs-0.2.0 (c (n "simple_ecs") (v "0.2.0") (h "0isny8qdmkkd8d6x66djfml4g33gs4qw5x58zp8m5qsfgh1pskz5")))

(define-public crate-simple_ecs-0.2.1 (c (n "simple_ecs") (v "0.2.1") (h "1qqdblhjb6ha08w9rxgfw0s9ic4n9ygwqib9sk3cn6v7scaa99zk")))

(define-public crate-simple_ecs-0.2.2 (c (n "simple_ecs") (v "0.2.2") (d (list (d (n "dynamic") (r "^0.2.1") (d #t) (k 0)))) (h "1ia0n75zyvy9x0g3hv93kgwcj1p99qrxh8c3h1m45r0rqqh32669")))

(define-public crate-simple_ecs-0.3.0 (c (n "simple_ecs") (v "0.3.0") (d (list (d (n "dynamic") (r "^0.2") (d #t) (k 0)))) (h "03ca6f99la8ydj7xbfy59hwgk44qh8f4x00da421pkv453cgrxiq")))

(define-public crate-simple_ecs-0.3.1 (c (n "simple_ecs") (v "0.3.1") (d (list (d (n "dynamic") (r "^0.2") (d #t) (k 0)))) (h "1p2w875hpr6nzb6isknw8lwlryc5klhhvzgiqjbyaghy1f12yvjq")))

(define-public crate-simple_ecs-0.3.2 (c (n "simple_ecs") (v "0.3.2") (d (list (d (n "dynamic") (r "^0.2") (d #t) (k 0)))) (h "1ii3pv23w2wcrsfca2bnxwxvm9skq16dh4zkdh3knlx8ry0xdsly")))

