(define-module (crates-io si mp simple_statemachine) #:use-module (crates-io))

(define-public crate-simple_statemachine-1.0.0 (c (n "simple_statemachine") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xhqq7q3mr7z98znbqxm7fsj6bvy2252gbknf3swa8gkj8il869b")))

