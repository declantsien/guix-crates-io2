(define-module (crates-io si mp simple-builder-macro) #:use-module (crates-io))

(define-public crate-simple-builder-macro-0.0.2 (c (n "simple-builder-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fg1q32vr7kcbjb3brd2vv1ahlmy42i4c7yfm42jww6s2gwb6szw")))

