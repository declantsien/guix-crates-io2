(define-module (crates-io si mp simplex-23d-rs) #:use-module (crates-io))

(define-public crate-simplex-23d-rs-0.1.0 (c (n "simplex-23d-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hips0w7zvn71pns4faq3306b4lbiyznnvnmcwfg38390yawrc6m")))

(define-public crate-simplex-23d-rs-0.2.0 (c (n "simplex-23d-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12mqay1bvvy6jcin51p1yxi6c31j9nkc5wy3b5r6869jgpazdjjl")))

(define-public crate-simplex-23d-rs-0.2.1 (c (n "simplex-23d-rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01mlj3s37c1a12bnmbagmz1vzsrvz1i8wmqwy9zcz6narmfmh61n")))

