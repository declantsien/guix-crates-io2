(define-module (crates-io si mp simple-message-channels) #:use-module (crates-io))

(define-public crate-simple-message-channels-0.0.1 (c (n "simple-message-channels") (v "0.0.1") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "1jjbvk6vjn9mbd2mc46p4xq4m6r47pvpmj6hj3q5k66vqn3ali0l")))

(define-public crate-simple-message-channels-0.0.2 (c (n "simple-message-channels") (v "0.0.2") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "18xbi0j92qvys8v36b095lgdzddrii102ixxl9a1vbl3hpmym3ah")))

(define-public crate-simple-message-channels-0.1.0 (c (n "simple-message-channels") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "0idvl7l1hc5pimyrp7nqb3ncpnlwp55kjdz0vvigc493vx1n1bsz")))

(define-public crate-simple-message-channels-0.1.1 (c (n "simple-message-channels") (v "0.1.1") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "0vv4grfcvivip4f7v4qf6hzj84scq43zmabsjk9yhk6kc1x6fbnv")))

(define-public crate-simple-message-channels-0.2.0 (c (n "simple-message-channels") (v "0.2.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "07n44mdr3d48fy5fbw63axl53m63mi0kdiz17lsmh5z0q6kpipb4")))

