(define-module (crates-io si mp simple_behavior_tree) #:use-module (crates-io))

(define-public crate-simple_behavior_tree-0.1.0 (c (n "simple_behavior_tree") (v "0.1.0") (h "0vpih9i8n0gh2wc3xcy6csis3js4jvyq7krbszqixkppq5rl814c")))

(define-public crate-simple_behavior_tree-0.1.1 (c (n "simple_behavior_tree") (v "0.1.1") (h "0zwsbcanpjs4s0svkk0h3z2fyz1108az1s9k06bavnla7addkxjh")))

(define-public crate-simple_behavior_tree-0.1.2 (c (n "simple_behavior_tree") (v "0.1.2") (h "1msf89ial2c3hlq5qrsqsbs5h3rhkyd87pvv40yavpc937kxxp46")))

(define-public crate-simple_behavior_tree-0.1.3 (c (n "simple_behavior_tree") (v "0.1.3") (h "1b3yjgrzy3sc4q16pwkmzmabhl69id7sx8ggfisgkbvagn45k7cr")))

