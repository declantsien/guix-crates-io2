(define-module (crates-io si mp simple-chart) #:use-module (crates-io))

(define-public crate-simple-chart-0.7.7 (c (n "simple-chart") (v "0.7.7") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "04pdv0r4442f2nfa6vpc4qhwpi1iz7xnikfmbvq4gw9h98l12dbk") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-simple-chart-0.7.8 (c (n "simple-chart") (v "0.7.8") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "13n5v2nyd11g890q8k9cnwqfaaif3igi7h97yjfhz1d05rh6c421") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-simple-chart-0.7.9 (c (n "simple-chart") (v "0.7.9") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.96") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1p6sm1nxff9rwsnrv56xyskgrh5w14znlybmwpymbaxyd4ra3frq") (f (quote (("dev" "clippy") ("default"))))))

