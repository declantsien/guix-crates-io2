(define-module (crates-io si mp simple_tcp_serializer) #:use-module (crates-io))

(define-public crate-simple_tcp_serializer-0.1.0 (c (n "simple_tcp_serializer") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ym8n7ggjsx61sjpm0d1ny3bn7r2xxfja67ci3nhpz7jx2xpbn4i")))

