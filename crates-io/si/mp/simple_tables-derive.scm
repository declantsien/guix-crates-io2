(define-module (crates-io si mp simple_tables-derive) #:use-module (crates-io))

(define-public crate-simple_tables-derive-0.1.0 (c (n "simple_tables-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "simple_tables-core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "1mqb6qvblccffqyg5jsqjddwd0wi3x9vk6031pgkskpb5l1z9p1l")))

(define-public crate-simple_tables-derive-0.1.1 (c (n "simple_tables-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "simple_tables-core") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "0jixmvp702a8af2i16w0aiaz84gz8j82a5d6w3a7bmdv0c2qizg0")))

(define-public crate-simple_tables-derive-0.2.0 (c (n "simple_tables-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "simple_tables-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvcv0ay1islk6qx5l42nsgn6yjfx4dc1vhm3r9bg7nvkpcr91p7")))

(define-public crate-simple_tables-derive-0.2.1 (c (n "simple_tables-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "simple_tables-core") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "0xflrhpn8aprrawxyvv0lfv9lhv8yr8vsn8jyig8v1y0xml1g31f")))

(define-public crate-simple_tables-derive-0.3.0 (c (n "simple_tables-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "simple_tables-core") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "1ryhif2wb9y1dszhfka42affmfqfay7il5gzs6mpliv3pakxvlmd")))

