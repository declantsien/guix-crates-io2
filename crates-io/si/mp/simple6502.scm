(define-module (crates-io si mp simple6502) #:use-module (crates-io))

(define-public crate-simple6502-1.0.0 (c (n "simple6502") (v "1.0.0") (h "07xphi62k9q9nrrrigq8m6bv43if91mbbizflxdxallx4gh328zf")))

(define-public crate-simple6502-1.1.0 (c (n "simple6502") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y37kjdjknxd5n6nvh1vq0vi5dyfyyiy2afkykaxa0wsq7x5nr9h") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-simple6502-2.0.0 (c (n "simple6502") (v "2.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cl36k334ffba1dlv96anchh89vc8ny419l9p6kq6z4a3gv7xhgz") (s 2) (e (quote (("serde" "dep:serde"))))))

