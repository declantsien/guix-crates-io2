(define-module (crates-io si mp simple_bitfield) #:use-module (crates-io))

(define-public crate-simple_bitfield-0.1.0 (c (n "simple_bitfield") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1dqdrxdn3wvs2hx1a1a3bpx8234g4haxdc8bq1nkqvlhmxazd1zc")))

(define-public crate-simple_bitfield-0.1.1 (c (n "simple_bitfield") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "01kg1c61856aqqmazbnqhphfv7lgmhkgca6n7c4a67fxgkxr614k")))

(define-public crate-simple_bitfield-0.1.2 (c (n "simple_bitfield") (v "0.1.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "043x50vbp0n5x4mbffqblb1nnxy5b5s8g218w7by09pk1i8v4pay")))

(define-public crate-simple_bitfield-0.1.3 (c (n "simple_bitfield") (v "0.1.3") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0yvvpb1c2a0c6sd2s41gx7yxbnb565vc9y0vdpia5r50ay3slflj")))

(define-public crate-simple_bitfield-0.1.5 (c (n "simple_bitfield") (v "0.1.5") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1nxb96z0gnww4cr8faid4051aysn66b2qizdg16911w1hmamjvdy")))

(define-public crate-simple_bitfield-0.1.6 (c (n "simple_bitfield") (v "0.1.6") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "19xbf7gf5lx162nyn2iz93kp2lmi5xqncm7436zsq1vh87w6aia9")))

(define-public crate-simple_bitfield-0.1.7 (c (n "simple_bitfield") (v "0.1.7") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1cs3z1pka66ca7vbk0qrqyk283mlpphw3zg5mszwcjyxy1cfmbns")))

(define-public crate-simple_bitfield-0.1.8 (c (n "simple_bitfield") (v "0.1.8") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1h4lmh80xdaali1fm17wgnliwwgp95jvzlwmrayibmrw1pr66xwl")))

