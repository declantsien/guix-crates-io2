(define-module (crates-io si mp simple_calculator_deliver) #:use-module (crates-io))

(define-public crate-simple_calculator_deliver-0.1.0 (c (n "simple_calculator_deliver") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1lpfy4w7w2im1696hsi2crgp6g8ji5bbdq70m53j5xf8pymnf3ap")))

(define-public crate-simple_calculator_deliver-0.1.1 (c (n "simple_calculator_deliver") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0zyc4h54hm9qk4zkcg4di6jplcszq4p81gkbjjbpsl5y7ycp0rhz")))

