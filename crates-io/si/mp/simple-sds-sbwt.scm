(define-module (crates-io si mp simple-sds-sbwt) #:use-module (crates-io))

(define-public crate-simple-sds-sbwt-0.3.1 (c (n "simple-sds-sbwt") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)))) (h "04k3vabnckywndzamg0dhm801l1n97f7k0cbiqqq2zyb2jzl4nq5") (f (quote (("binaries" "getopts" "rand" "rand_distr"))))))

