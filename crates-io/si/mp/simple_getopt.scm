(define-module (crates-io si mp simple_getopt) #:use-module (crates-io))

(define-public crate-simple_getopt-0.1.0 (c (n "simple_getopt") (v "0.1.0") (d (list (d (n "arraylist") (r "^0.1.5") (d #t) (k 0)))) (h "1b58i4lcg4v1iigz1647j14n5834caqks8sj9gar3z97w9gvpk5a") (y #t)))

(define-public crate-simple_getopt-0.1.1 (c (n "simple_getopt") (v "0.1.1") (d (list (d (n "arraylist") (r "^0.1.5") (d #t) (k 0)))) (h "0kjsf97yacsm4h2n907bh2g9swj4ik73dbsbsqbd74xchkiqq4qk")))

