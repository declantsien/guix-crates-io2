(define-module (crates-io si mp simple-merkle-tree) #:use-module (crates-io))

(define-public crate-simple-merkle-tree-0.1.0 (c (n "simple-merkle-tree") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "150yx8jmzn4si76fwyn1km2ypil226sf2j6aknfpiws0kq74d2bi")))

