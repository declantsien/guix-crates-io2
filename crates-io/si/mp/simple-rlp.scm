(define-module (crates-io si mp simple-rlp) #:use-module (crates-io))

(define-public crate-simple-rlp-0.1.0 (c (n "simple-rlp") (v "0.1.0") (d (list (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xbzfwxinynzrya5xfvi2lq0v6kjgx3ib7svn9cyn3q6bmh5wfj8")))

