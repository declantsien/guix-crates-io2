(define-module (crates-io si mp simple-chunk-allocator) #:use-module (crates-io))

(define-public crate-simple-chunk-allocator-0.1.0 (c (n "simple-chunk-allocator") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.46") (d #t) (k 2)))) (h "1g47i8yb5fk4rr0mw18ln98agiplni3yaknk40lzswwz5b8v8mpw")))

(define-public crate-simple-chunk-allocator-0.1.1 (c (n "simple-chunk-allocator") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.46") (d #t) (k 2)))) (h "11dx9bqc9jqfhikiw67npzswc5kkcpdrnbakz3craz3x9p0528av")))

(define-public crate-simple-chunk-allocator-0.1.2 (c (n "simple-chunk-allocator") (v "0.1.2") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.46") (d #t) (k 2)))) (h "0v3k481v7sh7fh6294zlj2id5jzkbviirmp4d6i2jk0gfdnvkvn3")))

(define-public crate-simple-chunk-allocator-0.1.3 (c (n "simple-chunk-allocator") (v "0.1.3") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.46") (d #t) (k 2)))) (h "0z2cyl44x5hgpl9gy9wyd3rcyv6vhihi3raqiragzdp1i6mjm7zr")))

(define-public crate-simple-chunk-allocator-0.1.4 (c (n "simple-chunk-allocator") (v "0.1.4") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.46") (d #t) (k 2)))) (h "129iw0cnbddxp067gxravlqrk8gg486wdihjrcy61j5yx6971cml")))

(define-public crate-simple-chunk-allocator-0.1.5 (c (n "simple-chunk-allocator") (v "0.1.5") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("use_spin_nightly" "const_mut_refs" "alloc_ref"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "x86") (r "^0.51") (d #t) (k 2)))) (h "1dc57ga3l71imkwsbyz2cp8bkz3idk95qs5bk3vkcqyzakym4glp")))

