(define-module (crates-io si mp simply_2dpga) #:use-module (crates-io))

(define-public crate-simply_2dpga-0.1.1 (c (n "simply_2dpga") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1bixanjrl82hdwf4x05lk8gm5y0pmdjsivyqjh111lfi66r0kp8y")))

(define-public crate-simply_2dpga-0.1.2 (c (n "simply_2dpga") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "02q98wrmyhzddm25h0pqx0wpkv1if21m6gfwnv2llqghdl5k5abm")))

