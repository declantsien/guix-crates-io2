(define-module (crates-io si mp simple_login_rs) #:use-module (crates-io))

(define-public crate-simple_login_rs-0.1.0 (c (n "simple_login_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mmyawbfh93ab2x0badi1pyvyn0hm29f5542hkxmwdhbnrn9dn5w")))

(define-public crate-simple_login_rs-0.1.1 (c (n "simple_login_rs") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1wqiw1as8f36xq6vyy9j5krfsc9d7cf909psj6s1wskbmr7a53i2")))

(define-public crate-simple_login_rs-0.1.2 (c (n "simple_login_rs") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "090swb3a0kz38p8kdyvbymsfmf29d9qyr5mv4791rkwsdrib7dfy")))

