(define-module (crates-io si mp simple_spawn_blocking) #:use-module (crates-io))

(define-public crate-simple_spawn_blocking-1.0.0 (c (n "simple_spawn_blocking") (v "1.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (k 0)))) (h "0kl4yprf5i5a2ln95nmzb0sifp7vcjisrdvcqyf14gjrs6bfsc8v")))

