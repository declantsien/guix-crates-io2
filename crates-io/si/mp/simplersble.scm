(define-module (crates-io si mp simplersble) #:use-module (crates-io))

(define-public crate-simplersble-0.6.0-alpha1 (c (n "simplersble") (v "0.6.0-alpha1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1zmyvdqzjv3gjzjcf15s8frc47s8y3a8cn47mn31sdni4233sqcg") (y #t) (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha2 (c (n "simplersble") (v "0.6.0-alpha2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gm8jm85w62knywh5rzci5pnsg9r0j0z905qjd6hcr6qis0997by") (y #t) (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha3 (c (n "simplersble") (v "0.6.0-alpha3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0kmcvvqpl2zrvw9kgcwrjrz93d9jx419c3231c0vwlwfv9lyv8li") (y #t) (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha4 (c (n "simplersble") (v "0.6.0-alpha4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0xhx8jajb9vga9s88sgzx6hlksac6k770k56whbsmsqslkf8j1ck") (y #t) (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha5 (c (n "simplersble") (v "0.6.0-alpha5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gmwp1wz0j9n9z0sr56qkj0xngf813wy2i9iv6a72wb8ipnhjy17") (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha6 (c (n "simplersble") (v "0.6.0-alpha6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0976xpxfypz1gw41dq01i44axdx03ynfxfhizd5k3gp0xx71miad") (l "simpleble")))

(define-public crate-simplersble-0.6.0-alpha7 (c (n "simplersble") (v "0.6.0-alpha7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0avdsinmch8gqb4z0s80rhld0h7l6yyb6vdxsvbq9c5mdy0zwnzm") (l "simpleble")))

(define-public crate-simplersble-0.6.0 (c (n "simplersble") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17cwid0ia6lwjn1gwxwsj4nn647zx85g3cwc0xxs237msl46j4yy") (l "simpleble")))

(define-public crate-simplersble-0.6.1-alpha1 (c (n "simplersble") (v "0.6.1-alpha1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1hzi0lda35074avf5vhvc50sd494gr3pzqxkczphcm1phd57m7hk") (l "simpleble")))

(define-public crate-simplersble-0.6.1 (c (n "simplersble") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08d561rb8gfx80jazi7igjzmj7qds4mljpi6hc6xj911zsj1vkrv") (l "simpleble")))

(define-public crate-simplersble-0.7.1 (c (n "simplersble") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1xisy4pwh3ka7c9x5p5h1gklgkgr64xhppvkm8z9fxk4xmwjqdcc") (y #t) (l "simpleble")))

(define-public crate-simplersble-0.7.2 (c (n "simplersble") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0cn7v2sl8sg0sg52b7r99k4k7jnwvmqlbffhng47ncg0vnxws0aj") (l "simpleble")))

(define-public crate-simplersble-0.7.3 (c (n "simplersble") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0rs1yvphakvr39x2cwk7jnmf8g23mpc9x6jkizyic3lfz52fahvc") (l "simpleble")))

