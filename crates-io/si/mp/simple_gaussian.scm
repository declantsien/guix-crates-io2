(define-module (crates-io si mp simple_gaussian) #:use-module (crates-io))

(define-public crate-simple_gaussian-0.2.4 (c (n "simple_gaussian") (v "0.2.4") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "10vyn5qflj94xjvpba10rkr5j6ijfsbvapv2bbrzpdk9bd11acjc")))

(define-public crate-simple_gaussian-0.2.5 (c (n "simple_gaussian") (v "0.2.5") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "1sbihay0h1j95asqf3gy7hsdd8xrnv21xazchsz9k979f4sjvbjn")))

(define-public crate-simple_gaussian-0.2.6 (c (n "simple_gaussian") (v "0.2.6") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1s3cdmwxji9rs2sbs3gn4dqiw95rwms0rd4hrpjwkvm3s13x4pgs")))

(define-public crate-simple_gaussian-0.3.0 (c (n "simple_gaussian") (v "0.3.0") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0zgkb50zfwh0c058j3x9r0f5yi8j3hr8ffd36xiriq61g6fnwx0n")))

(define-public crate-simple_gaussian-0.4.1 (c (n "simple_gaussian") (v "0.4.1") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0n6y3kw453sr8bbbrhim4jrmv9y55jxysmjpzqmlx8lsjvc6ag6l")))

(define-public crate-simple_gaussian-0.4.2 (c (n "simple_gaussian") (v "0.4.2") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "167z10wmx36q5b35ha63rzm62cm5cv8zdvr8r5nbrc89rxxpsnn1")))

(define-public crate-simple_gaussian-0.4.3 (c (n "simple_gaussian") (v "0.4.3") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1i183326imf8gr11cz8qwfmyrslafxjxxr4rb213w83qfpn6gynp")))

(define-public crate-simple_gaussian-0.4.4 (c (n "simple_gaussian") (v "0.4.4") (d (list (d (n "mindtree_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1acpzaxm5q2xb2klh1s2j7zy1m4p0gasfvhbz6bym31397bbz9gv")))

(define-public crate-simple_gaussian-0.4.5 (c (n "simple_gaussian") (v "0.4.5") (d (list (d (n "mindtree_utils") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1c4fl1bflxmq0j47g2jc46mdwamczy7012c16fm2h985jhknn7l5")))

(define-public crate-simple_gaussian-0.5.0 (c (n "simple_gaussian") (v "0.5.0") (d (list (d (n "mindtree_utils") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0b34pz3nrch3hvalsahdzh68k9qzby0n1ixbbj8yx7ws0n0mgzkl")))

(define-public crate-simple_gaussian-0.6.0 (c (n "simple_gaussian") (v "0.6.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "0rqjk1pkmpz2bkr7262qwp08pfy3vd3m62x3ws3ay0hr647w88yd")))

