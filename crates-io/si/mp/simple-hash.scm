(define-module (crates-io si mp simple-hash) #:use-module (crates-io))

(define-public crate-simple-hash-0.1.0 (c (n "simple-hash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "simple-hash-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0nvxh7lzyimgwhl52b87p3kpinq4f74c6b88lh87q47w4zv14fck")))

(define-public crate-simple-hash-0.1.1 (c (n "simple-hash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "simple-hash-macro") (r "^0.1.1") (d #t) (k 0)))) (h "17zwr2p5f7l1ppx4h5nh1r87k9gx5xip8b4vjg2rsa0c73n3gaxk")))

