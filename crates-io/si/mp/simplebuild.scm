(define-module (crates-io si mp simplebuild) #:use-module (crates-io))

(define-public crate-simplebuild-0.1.0 (c (n "simplebuild") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "045dhbpvnsq5vmzg6b2cpjz1c03r8ziav8kgaamgnkm1yqymvys0")))

(define-public crate-simplebuild-0.1.1 (c (n "simplebuild") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1c0dmfzgf3hxwdi6vkpfwvgj08zillbz36pl8bdhjvhfwsag6dl6")))

(define-public crate-simplebuild-0.1.2 (c (n "simplebuild") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "19m4iiawlr1lz931iggq543r61ka8k0605d0l1xk4zgjahr0ryh9")))

