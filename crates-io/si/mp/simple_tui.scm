(define-module (crates-io si mp simple_tui) #:use-module (crates-io))

(define-public crate-simple_tui-0.1.0 (c (n "simple_tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0kfya1zwkcsrqr7dznjp4kvy9zbiisz22jldgcgwd1aqc6760vl5") (y #t)))

(define-public crate-simple_tui-0.1.1 (c (n "simple_tui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "14hy8g40hgdqjjih6z3n6ya96yb23gazmnw9wggj2cbzpbbrzhw5")))

