(define-module (crates-io si mp simple_func_timer) #:use-module (crates-io))

(define-public crate-simple_func_timer-0.1.0 (c (n "simple_func_timer") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cvs1yk13nna6l5y00flmzhl325qx0mid3vln7r0kwfrxf0nf527")))

