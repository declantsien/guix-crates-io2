(define-module (crates-io si mp simple-pool) #:use-module (crates-io))

(define-public crate-simple-pool-0.0.1 (c (n "simple-pool") (v "0.0.1") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0f1ymszn2d2ik74rz66xqd5yy7fji77kywbhw4qzi8zay4nj26vc")))

(define-public crate-simple-pool-0.0.2 (c (n "simple-pool") (v "0.0.2") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0ranil9qhhhq6zqrr1qsmk4zcba3k1xrihxyycnffl8ns0b151gp")))

(define-public crate-simple-pool-0.0.3 (c (n "simple-pool") (v "0.0.3") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0hzns08xfa2xjigdpp9yyp43sm5jb398vdvdxq889spk9f212y8y")))

(define-public crate-simple-pool-0.0.4 (c (n "simple-pool") (v "0.0.4") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "17jfarhfyhf20apyx114f7y9h88rl432k1cz5dr16kxc587n8q0c")))

(define-public crate-simple-pool-0.0.5 (c (n "simple-pool") (v "0.0.5") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0arph4pd3rnrmqyhfgn5jj28i8dd96xdys5wrn3xawaggrx5cz7s")))

(define-public crate-simple-pool-0.0.6 (c (n "simple-pool") (v "0.0.6") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0a3qscpyx48n0ns2y9qq67xb1z02x7a74bbpx3fzf55p0ssl7kiq")))

(define-public crate-simple-pool-0.0.7 (c (n "simple-pool") (v "0.0.7") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "1g8dy9xl32lc3rysz7awkwycqvg42xgv73nlxgssfjlbs7ylapk2")))

(define-public crate-simple-pool-0.0.8 (c (n "simple-pool") (v "0.0.8") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "1mi75g8v81cwqpc2c41vbjjwjd4xiiibp542lba7hngplb780hin")))

(define-public crate-simple-pool-0.0.9 (c (n "simple-pool") (v "0.0.9") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "1387j4xzcc0y7wbwln4xw0vw1c15a8bjqiwfw84fcliyn19dwbb4")))

(define-public crate-simple-pool-0.0.10 (c (n "simple-pool") (v "0.0.10") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "1clm4rmf559lxyv1hq5iiwcn4n23qznvnl9sly0kgij3jwnm2xva")))

(define-public crate-simple-pool-0.0.11 (c (n "simple-pool") (v "0.0.11") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "156270pswwgk9x9s3dx8f9ml7r2cwb4kfdgm58kb3am3lxx4s712")))

(define-public crate-simple-pool-0.0.12 (c (n "simple-pool") (v "0.0.12") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt"))) (d #t) (k 2)))) (h "0cxpd5510fxgfwvbkqzfy369vsh79xgi768fs7w1awfih9bxynz0") (y #t)))

(define-public crate-simple-pool-0.0.13 (c (n "simple-pool") (v "0.0.13") (d (list (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt" "macros" "time"))) (d #t) (k 2)))) (h "0x0245h8xz2mmdl9889wwlriqqgm451myfy57l31j03f1vhpcd97") (y #t)))

(define-public crate-simple-pool-0.0.14 (c (n "simple-pool") (v "0.0.14") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0l77klqy9jgzkj86l0fm6rkfdw7405d3jp2sxibjgcah6jbcb20z")))

(define-public crate-simple-pool-0.0.15 (c (n "simple-pool") (v "0.0.15") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1mg9qnxpv66jma2m3i1wnl2fnhk5g041zlyj2r12ci245kdrrkjw")))

(define-public crate-simple-pool-0.0.16 (c (n "simple-pool") (v "0.0.16") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1ycvhy4r0z0qqfrr3g6cq3s1fl5i2y17xdf89zcslcivrv25kklz")))

(define-public crate-simple-pool-0.0.17 (c (n "simple-pool") (v "0.0.17") (d (list (d (n "object-id") (r "^0.1.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("net" "sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0nnl62rb49xkchsk3njg57cnyi1nx5pxgws521z8m2kp1fpcrvcy")))

