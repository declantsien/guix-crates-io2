(define-module (crates-io si mp simple_host_networks_manager_cli) #:use-module (crates-io))

(define-public crate-simple_host_networks_manager_cli-0.0.4 (c (n "simple_host_networks_manager_cli") (v "0.0.4") (d (list (d (n "simple_host_networks_manager_lib") (r "^0.0.3") (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "04z7lfzz21zgxkrk6vwnxvcfy7agr1q3banrxqlrrcicdc3z4caz")))

