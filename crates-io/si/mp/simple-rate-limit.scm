(define-module (crates-io si mp simple-rate-limit) #:use-module (crates-io))

(define-public crate-simple-rate-limit-0.1.0 (c (n "simple-rate-limit") (v "0.1.0") (d (list (d (n "ring-vec") (r "^0.1") (d #t) (k 0)))) (h "1dqzbfyjvcbxcprv8335aw038s0agih0cdibzb16j8hz5q3nyv7s")))

(define-public crate-simple-rate-limit-0.2.0 (c (n "simple-rate-limit") (v "0.2.0") (d (list (d (n "ring-vec") (r "^0.1") (d #t) (k 0)))) (h "0w0jxlgns4xiviyl0yacmqccdpbzggs02jbr70fb7f48rb6v9jhw")))

