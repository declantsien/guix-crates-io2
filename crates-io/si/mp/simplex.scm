(define-module (crates-io si mp simplex) #:use-module (crates-io))

(define-public crate-simplex-0.1.0 (c (n "simplex") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "19d868akr3pifjfj2jdcgpi3bixd4zzdfdl6yhj7ppp3y4b8l7k9")))

(define-public crate-simplex-0.9.0 (c (n "simplex") (v "0.9.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)))) (h "1sv770lc87ax17qw15d0fks3sivy4yjklkl9di6bfibrdb5bgypn")))

(define-public crate-simplex-1.0.0 (c (n "simplex") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)))) (h "0gb93hm0fa4i98akllfz28kjbh6nvdfg7jhz9hr0gxrh5njsb83p")))

