(define-module (crates-io si mp simple-zip) #:use-module (crates-io))

(define-public crate-simple-zip-1.0.0 (c (n "simple-zip") (v "1.0.0") (d (list (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "14d7s4l1vyp3bliyxb0fmrnw54whdlqyqzciskqzz27dw85xdvq9")))

(define-public crate-simple-zip-1.0.1 (c (n "simple-zip") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1j8mp2ifxn8ydy668872fybm26ndslln3l9b6kzy6q51sldp8l10") (f (quote (("zip") ("default" "zip"))))))

