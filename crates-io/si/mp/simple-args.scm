(define-module (crates-io si mp simple-args) #:use-module (crates-io))

(define-public crate-simple-args-0.1.0 (c (n "simple-args") (v "0.1.0") (d (list (d (n "multimap") (r "^0.8") (d #t) (k 0)))) (h "098fzpr9zq1x90cq9pv7qfj52wqzavfipyazhzzddl5h8dv10297")))

(define-public crate-simple-args-0.2.0 (c (n "simple-args") (v "0.2.0") (d (list (d (n "multimap") (r "^0.8") (d #t) (k 0)))) (h "1s25pr25lhvckphkkw8zpv4glirzd5kcfff1xpwkdrj5xn1xsisy")))

