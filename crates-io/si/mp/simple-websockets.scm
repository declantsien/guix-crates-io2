(define-module (crates-io si mp simple-websockets) #:use-module (crates-io))

(define-public crate-simple-websockets-0.1.0 (c (n "simple-websockets") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14") (d #t) (k 0)))) (h "0q0q64v6binmb2ivmjiv9hir5h8245c71gmih8jshbpx99q5pzqa")))

(define-public crate-simple-websockets-0.1.1 (c (n "simple-websockets") (v "0.1.1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14") (d #t) (k 0)))) (h "0xb1yfjb80ilsr6pm6xwrx8j9arcjkh7n9giiqdp09gga2d5bzvz")))

(define-public crate-simple-websockets-0.1.2 (c (n "simple-websockets") (v "0.1.2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15") (d #t) (k 0)))) (h "1380yj1wm204dlygybd4wcbxchh2w7s3rvfg4j2pm7wpy965lr14")))

(define-public crate-simple-websockets-0.1.3 (c (n "simple-websockets") (v "0.1.3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15") (d #t) (k 0)))) (h "03643gaqljvsd7ksnmvxxxw9nb5a51jzjfhrh18a01nkc7mw5b13")))

(define-public crate-simple-websockets-0.1.4 (c (n "simple-websockets") (v "0.1.4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15") (d #t) (k 0)))) (h "0qrcza60s4dl50gzvjjsiqw5bg89krhdnlr19fz935n178dgb12x")))

(define-public crate-simple-websockets-0.1.5 (c (n "simple-websockets") (v "0.1.5") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 2)))) (h "1zg1mj059xcy7phbg14nb10dysdwp65aidm57qa8j6qzqv69h2l7")))

(define-public crate-simple-websockets-0.1.6 (c (n "simple-websockets") (v "0.1.6") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.19") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19.0") (f (quote ("native-tls"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 2)))) (h "0z7f13c120gvz1vgih6jq10gbs240grzzd4v1v8j9dkvf4acqf3z")))

