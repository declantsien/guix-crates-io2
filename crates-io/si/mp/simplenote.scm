(define-module (crates-io si mp simplenote) #:use-module (crates-io))

(define-public crate-simplenote-0.1.0 (c (n "simplenote") (v "0.1.0") (d (list (d (n "base64") (r "^0.2.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "07wf1mpiwpvwamdkliijxqipanh3ifpz703bw4sh2pmyf8bqgngs")))

(define-public crate-simplenote-0.2.0 (c (n "simplenote") (v "0.2.0") (d (list (d (n "base64") (r "^0.2.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)))) (h "07v69v0c4ky1x18y3axr3fxjbchv8jxqrjm4dj7d8f69cavrn529")))

