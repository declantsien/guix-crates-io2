(define-module (crates-io si mp simpleini) #:use-module (crates-io))

(define-public crate-simpleini-0.1.0 (c (n "simpleini") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wgs41rl3yqb4jf744bzdsh853s5mj53mv9vjdq53qgy4w4l99js")))

(define-public crate-simpleini-0.1.1 (c (n "simpleini") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "13nrq3bfvlpw7lr3jszgzr5phcgi31mzk7jvfn11zv33rg33pcj3")))

(define-public crate-simpleini-0.1.2 (c (n "simpleini") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0bxv7k900krc629b81zrn936savfblpikyz2fiajnki1hirv0b90")))

