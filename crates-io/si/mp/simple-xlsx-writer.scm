(define-module (crates-io si mp simple-xlsx-writer) #:use-module (crates-io))

(define-public crate-simple-xlsx-writer-0.1.0 (c (n "simple-xlsx-writer") (v "0.1.0") (d (list (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 0)))) (h "0miqwsf04gwxr1r17p39h27vgpzrmh7z812kkwchaqvazfvahzn8")))

(define-public crate-simple-xlsx-writer-0.1.1 (c (n "simple-xlsx-writer") (v "0.1.1") (d (list (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 0)))) (h "0qk8a3p9555jwav1gdiycp0pkad025dkvn42jnaixlfxz5lj9993")))

(define-public crate-simple-xlsx-writer-0.1.2 (c (n "simple-xlsx-writer") (v "0.1.2") (d (list (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 0)))) (h "1bjd0s1bf9njgzi7k761ywq1c80q20d70604qy4zhjb0n96mlcrw")))

