(define-module (crates-io si mp simple-fcl-sys) #:use-module (crates-io))

(define-public crate-simple-fcl-sys-0.0.1 (c (n "simple-fcl-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "076wa9k2v57wvbsvdj1g5cz6xavayzjk4j8q2c1pzpg4099zzzg0")))

(define-public crate-simple-fcl-sys-0.0.2 (c (n "simple-fcl-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "10rqn5qs0dd8hl8sjcszy1zrrfkn1h4dkr24w4vrfjlhzf4s50fs") (f (quote (("skip_cc"))))))

(define-public crate-simple-fcl-sys-0.0.3 (c (n "simple-fcl-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1iczl1xis76gv4a0bszbg45fgwi4c0y7jmjvrlxkdrybysgk70zc") (f (quote (("skip_cc"))))))

(define-public crate-simple-fcl-sys-0.0.4 (c (n "simple-fcl-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0h1k9fiw479752pl3kwy3whnkwvp6vrbvpx4h02wd4yw0chlsv7c")))

(define-public crate-simple-fcl-sys-0.0.5 (c (n "simple-fcl-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0fizggswq8l5s41d0kagf9x76maim5j69yjg8cbc8nvlvhzvkmk9")))

