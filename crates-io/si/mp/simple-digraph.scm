(define-module (crates-io si mp simple-digraph) #:use-module (crates-io))

(define-public crate-simple-digraph-0.1.0 (c (n "simple-digraph") (v "0.1.0") (h "0q2m81k1kcfwdxrw0gr26afsjfmn8cbnj6flj04l8j9l5jvpmgr0")))

(define-public crate-simple-digraph-0.2.0 (c (n "simple-digraph") (v "0.2.0") (h "0i324gd5bwzingvix6xhaklf5vrxfvic4c8621ybdci0bw4sxrkm")))

(define-public crate-simple-digraph-0.3.0 (c (n "simple-digraph") (v "0.3.0") (h "0cl3bqc9a0w989ghmrkk9asfs58b1nnf4c20lp08a56406n4mp37")))

(define-public crate-simple-digraph-0.3.1 (c (n "simple-digraph") (v "0.3.1") (h "0yr7wwzsxicndxcnyi9qqlpk3f13q6z6n027cbdwla5jimf46jnw")))

(define-public crate-simple-digraph-0.3.3 (c (n "simple-digraph") (v "0.3.3") (h "1v2ln55f6wncrkirri6v0gx8x35m53wy92rkm9a8j9p1rxmnliz6")))

(define-public crate-simple-digraph-0.3.4 (c (n "simple-digraph") (v "0.3.4") (h "0w7rkla8cpm6spy2ng6yv453hyj795g83nxgpzbzkhimjc58dyw0")))

(define-public crate-simple-digraph-0.3.5 (c (n "simple-digraph") (v "0.3.5") (h "1wkha990bckd1xx5bxpmi6ragqm7ax8ybcbsl40mrpc7zqnx81vk")))

(define-public crate-simple-digraph-0.4.0 (c (n "simple-digraph") (v "0.4.0") (h "1p9y8w7r9f9v29w36zi5av2p8d7v304mk3qx262ahmsvbjpsifj0")))

