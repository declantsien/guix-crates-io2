(define-module (crates-io si mp simple-builder) #:use-module (crates-io))

(define-public crate-simple-builder-0.0.2 (c (n "simple-builder") (v "0.0.2") (d (list (d (n "simple-builder-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 0)))) (h "1hxq5wn8xyw1ny6z4kv4kv9wwsnb0qg37l7vjp26vx9gadij70yn")))

