(define-module (crates-io si mp simpletemplate) #:use-module (crates-io))

(define-public crate-simpletemplate-0.1.0 (c (n "simpletemplate") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-join") (r "^0.1.2") (d #t) (k 0)))) (h "0sls3hbhvznxfmi52xwny6rbamm7lakfhgqm2vps7006rw2a94i0")))

