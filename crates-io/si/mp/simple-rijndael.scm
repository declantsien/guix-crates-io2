(define-module (crates-io si mp simple-rijndael) #:use-module (crates-io))

(define-public crate-simple-rijndael-0.1.1 (c (n "simple-rijndael") (v "0.1.1") (h "1h5by4bh10gsk3v2k77k6zbbijfh0b2l887kc05rmzgpbwygqrcn") (y #t)))

(define-public crate-simple-rijndael-0.2.0 (c (n "simple-rijndael") (v "0.2.0") (h "0b9liiygi82ggarbswhqabbqc6gi8ri7m6p0942q0b6anczpmqfj")))

(define-public crate-simple-rijndael-0.3.0 (c (n "simple-rijndael") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0cplyxfca89j04xlpq80qsw581vrly10f75ls4a48kxplkapbb3a")))

(define-public crate-simple-rijndael-0.3.1 (c (n "simple-rijndael") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ggxlspamp62d1c83lwi2m3m4vvzxvj4ni9m85hx3373xf4fi1q0")))

(define-public crate-simple-rijndael-0.3.2 (c (n "simple-rijndael") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1192pchzq9r6g2vs2plccbxg4blydpbzgsmpiyhjk9h8bfacjgw4") (f (quote (("std") ("default" "std"))))))

