(define-module (crates-io si mp simple-game-engine) #:use-module (crates-io))

(define-public crate-simple-game-engine-0.5.2 (c (n "simple-game-engine") (v "0.5.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0s9lcy8bmixzwbl41nfbwv5spg15nal3cvcdzn27amzly75rqhj4")))

(define-public crate-simple-game-engine-0.6.0 (c (n "simple-game-engine") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1illfk7mdppm7066j409ang1pvgvvnmc71pdfzdxkd4bjd4mmi8s")))

(define-public crate-simple-game-engine-0.6.1 (c (n "simple-game-engine") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "137wd0np61smazkkm6ab399jdkc6by5vsijas49rfwz3ji5k3igs")))

(define-public crate-simple-game-engine-0.8.0 (c (n "simple-game-engine") (v "0.8.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.4") (d #t) (k 0)) (d (n "sdl2-unifont") (r "^1.0.2") (d #t) (k 0)))) (h "0qhyp523zdksq2a54yxnvpywlrlvybgbiixgqjhccb1yn8nbafam") (f (quote (("static_sdl" "sdl2/static-link") ("bundled_sdl" "sdl2/bundled"))))))

(define-public crate-simple-game-engine-0.8.1 (c (n "simple-game-engine") (v "0.8.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.4") (d #t) (k 0)) (d (n "sdl2-unifont") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1lsfymw5s48zghdi82q9xkjcg0iph5cpw9swsdajnnr6dmazzzqk") (f (quote (("unifont" "sdl2-unifont") ("static_sdl" "sdl2/static-link") ("bundled_sdl" "sdl2/bundled"))))))

(define-public crate-simple-game-engine-0.8.2 (c (n "simple-game-engine") (v "0.8.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.4") (d #t) (k 0)) (d (n "sdl2-unifont") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0zw94qqai1nhxki0wnsjh05k45msxy4390bl9wf45hcrc0svrb1q") (f (quote (("unifont" "sdl2-unifont") ("static_sdl" "sdl2/static-link") ("bundled_sdl" "sdl2/bundled"))))))

(define-public crate-simple-game-engine-0.8.3 (c (n "simple-game-engine") (v "0.8.3") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.4") (d #t) (k 0)) (d (n "sdl2-unifont") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "194nq15anb2hjj6jcm4xfam2lk6caaa81k3y9pydxl32ymq5zi1v") (f (quote (("unifont" "sdl2-unifont") ("static_sdl" "sdl2/static-link") ("bundled_sdl" "sdl2/bundled"))))))

