(define-module (crates-io si mp simplistis) #:use-module (crates-io))

(define-public crate-simplistis-0.1.0 (c (n "simplistis") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml-datetime-compat") (r "^0.3.0") (f (quote ("chrono"))) (d #t) (k 0)))) (h "19q3s2q9sn8zrbvzv1b52rg7qq8r0xb5y0fzz6bjq1nrp7zz41z4")))

