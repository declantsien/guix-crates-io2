(define-module (crates-io si mp simplewebserver) #:use-module (crates-io))

(define-public crate-simplewebserver-0.1.0 (c (n "simplewebserver") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.0") (f (quote ("colors"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "http1" "http2" "tcp"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "fs"))) (k 0)))) (h "0czhnix3lcvi55jwq02382ljcwi39hf90n6a5kk91j80gpxb2d77")))

(define-public crate-simplewebserver-0.1.1 (c (n "simplewebserver") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.0") (f (quote ("colors"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "http1" "http2" "tcp"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "fs"))) (k 0)))) (h "0j6kqj9vjcji8n1wqhap2ij8d6zplx73fkl0js2fkyvlc8xm1wcj")))

(define-public crate-simplewebserver-0.1.2 (c (n "simplewebserver") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.0") (f (quote ("colors"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "http1" "http2" "tcp"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "fs"))) (k 0)))) (h "0jd58snr7vfj90dag9mxiaxa4hxzyhj1ib8acaf94xq2qxbhsxwp")))

