(define-module (crates-io si mp simple-grid) #:use-module (crates-io))

(define-public crate-simple-grid-0.1.0 (c (n "simple-grid") (v "0.1.0") (h "0ajrzhn13y155d22r9jlrqmbcpnvflpn432l02xd4ysrzfdjyl41")))

(define-public crate-simple-grid-0.1.1 (c (n "simple-grid") (v "0.1.1") (h "0532aifxiwkk9mzlcq48fp0741q8bllqqx15asgr4apxzdp63ii1")))

(define-public crate-simple-grid-0.1.2 (c (n "simple-grid") (v "0.1.2") (h "12lfll9bmdapi27hjf0zdhzdsrc6mr1sz1rnjf69i6p46dqzzdqn")))

(define-public crate-simple-grid-1.0.0 (c (n "simple-grid") (v "1.0.0") (h "013vpish26ydzjhza1iyz71psnp6lql78vabgqxszsqw4v9wx7kz")))

(define-public crate-simple-grid-1.0.1 (c (n "simple-grid") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "0f95qiq4cn3wqkax7ziv3fpyp19q46k7bss8mw6bj1jy8xhkgpdl") (y #t)))

(define-public crate-simple-grid-1.0.2 (c (n "simple-grid") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "0jsjk2x73bvw25llym63gbgrdydsnnsacsgm3lfnz82k20nhzfag") (y #t)))

(define-public crate-simple-grid-1.0.3 (c (n "simple-grid") (v "1.0.3") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "0kd99q5pp2vwy8z302fcy3922wpw55ks0blpdf7a20fdhgqm3h4q") (f (quote (("linalg" "num-traits")))) (y #t)))

(define-public crate-simple-grid-1.0.4 (c (n "simple-grid") (v "1.0.4") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "1477bim5dn653cdfya26ygfqchp2zawf6qaw94xb6fnvi70pacf6") (f (quote (("linalg" "num-traits"))))))

(define-public crate-simple-grid-2.0.0 (c (n "simple-grid") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "0pv7gx2b3rgq5va2xs01lkwsg7bx0gnzjn0lzg079j6cbc04zss4") (f (quote (("linalg" "num-traits"))))))

(define-public crate-simple-grid-2.1.1 (c (n "simple-grid") (v "2.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "1wnzvrr8j7vxxlz87zcfbimjr1v5xlf77icfiqprfqxb80l9v6i4") (f (quote (("linalg" "num-traits"))))))

(define-public crate-simple-grid-2.2.1 (c (n "simple-grid") (v "2.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)))) (h "0bkgvrf536965jwqqb5fd8rj9rzqvd4i2g7lraxii12vl67rjkvj") (f (quote (("linalg" "num-traits"))))))

