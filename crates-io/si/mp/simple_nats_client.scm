(define-module (crates-io si mp simple_nats_client) #:use-module (crates-io))

(define-public crate-simple_nats_client-0.1.0 (c (n "simple_nats_client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yg964k1pi6k23aran37aj5xlgqg252bblkr4gq88lqdrxl5wq03")))

