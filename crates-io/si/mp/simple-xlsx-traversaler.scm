(define-module (crates-io si mp simple-xlsx-traversaler) #:use-module (crates-io))

(define-public crate-simple-xlsx-traversaler-0.1.0 (c (n "simple-xlsx-traversaler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (f (quote ("deflate"))) (k 0)))) (h "1fac7b1r2z9mqdmi7dppgcch8a1s051hcz6b14qg9grf05m1d1jb") (y #t)))

