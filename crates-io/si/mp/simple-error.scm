(define-module (crates-io si mp simple-error) #:use-module (crates-io))

(define-public crate-simple-error-0.1.0 (c (n "simple-error") (v "0.1.0") (h "07241zldyzc79vy17gdlq0n59xfw95x5hjfmxycr7hhlkar04lhp")))

(define-public crate-simple-error-0.1.1 (c (n "simple-error") (v "0.1.1") (h "1pvgvjsdzs7qhwqdpd12chs6qpbn9k776ay49b0svf47hi9wnwy9")))

(define-public crate-simple-error-0.1.2 (c (n "simple-error") (v "0.1.2") (h "02vrb1lcbqvxizmy3kdkhf8vn8jwbkk5157lg5zghllxg932pwll")))

(define-public crate-simple-error-0.1.3 (c (n "simple-error") (v "0.1.3") (h "05n7axsh08whycj2xzm4rpzdagx50nn7x2vfa3fn45lk26sy3la4")))

(define-public crate-simple-error-0.1.4 (c (n "simple-error") (v "0.1.4") (h "18hqbw3j4xai41s7jkwkjrqssh60gpaxllz7sdskbzb944g8d5q3")))

(define-public crate-simple-error-0.1.5 (c (n "simple-error") (v "0.1.5") (h "07bsb2nc812c4q2i9lgx9lwqhxqx0q45xg7k8kr82rjsjb6264b9")))

(define-public crate-simple-error-0.1.6 (c (n "simple-error") (v "0.1.6") (h "0333h3rhh9np9240hqa5nk3cj46hfkma6639ddcabjbgmxv3xdaw")))

(define-public crate-simple-error-0.1.7 (c (n "simple-error") (v "0.1.7") (h "1cqbwm5mcpbsga5w9im1la89rx96kgbha84ai780ban6kgkcy2n9")))

(define-public crate-simple-error-0.1.8 (c (n "simple-error") (v "0.1.8") (h "1r4x7jrs29115c02g6gcdgjw1pq0n7v7yq5ha58i1h54nj9683r4")))

(define-public crate-simple-error-0.1.9 (c (n "simple-error") (v "0.1.9") (h "0rssn3n7wr9bzsdvl731yrpi5hzy8jckn1pxxq4kq2xlmi9jjydn")))

(define-public crate-simple-error-0.1.10 (c (n "simple-error") (v "0.1.10") (h "0v1m6ba7is96nsbpr69g336y7yz2958h5cx5q99k6xdng1ghyc79")))

(define-public crate-simple-error-0.1.11 (c (n "simple-error") (v "0.1.11") (h "15fqbwfm74vqwnryzn6n816bqr6c988mf2j3pfz507lygabx2ybp")))

(define-public crate-simple-error-0.1.12 (c (n "simple-error") (v "0.1.12") (h "0n9j1pa0nsxjj65mzdfbz5mr43k72xdqnzsyssqr6pyrxpnw5h81")))

(define-public crate-simple-error-0.1.13 (c (n "simple-error") (v "0.1.13") (h "0yr5fg4vlqp5y9bqi4bw48najg5y4nrc8y5jkw42hm19ch98f3n0") (y #t)))

(define-public crate-simple-error-0.2.0 (c (n "simple-error") (v "0.2.0") (h "04yjn5gx44ffxk2rwrxx7zhlh0b16fc3z8vgcwilnqqcl12cipih")))

(define-public crate-simple-error-0.2.1 (c (n "simple-error") (v "0.2.1") (h "0lggl41nm4ar5fymr6x242zhqyd737wfha5v6294p11dmz4l961k")))

(define-public crate-simple-error-0.2.2 (c (n "simple-error") (v "0.2.2") (h "1wabf4v1b5ry0ssbf9gr877fblpw2zdw2i8vrda82s8m2fygfjv3")))

(define-public crate-simple-error-0.2.3 (c (n "simple-error") (v "0.2.3") (h "06wh4dk25mz290r0qcvzw1j6vcb6931vlxbzj9fclwkpx6fa4iyc")))

(define-public crate-simple-error-0.3.0 (c (n "simple-error") (v "0.3.0") (h "0xf1s6md8s8nsqsk21402l7v61v9idl2viyj96kcvhq0i25vchl5")))

