(define-module (crates-io si mp simple-locale) #:use-module (crates-io))

(define-public crate-simple-locale-0.1.0 (c (n "simple-locale") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xb7c9q496m62lyy812zxbyf2lfdncf0ap2hdnqiinzk890fjfiy")))

(define-public crate-simple-locale-0.2.0 (c (n "simple-locale") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rc76pg7gnvfca06c2ngqy4g3mfky988ki1zwv882ykns03n62qf")))

