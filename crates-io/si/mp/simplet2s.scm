(define-module (crates-io si mp simplet2s) #:use-module (crates-io))

(define-public crate-simplet2s-0.1.0 (c (n "simplet2s") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0crmbmllv7bl1pddgw3cbp17hw5dlvm9d7x6vdjpmn06sx3rx6xn")))

(define-public crate-simplet2s-0.1.1 (c (n "simplet2s") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0v9wj5573lmha9pffc2vzli3cvlhinmc9i9pw826clfjx9baj1v7") (f (quote (("unstable"))))))

(define-public crate-simplet2s-0.1.2 (c (n "simplet2s") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1xmlp9avpmpyk6rs527bsm7brg91j6ii0qp10s35ivv779h04wdb") (f (quote (("unstable"))))))

(define-public crate-simplet2s-0.1.3 (c (n "simplet2s") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1qqcni6flw1s2vdyckdx2b938fa7498xvxrfh47m82zi2glly5bq") (f (quote (("unstable"))))))

(define-public crate-simplet2s-0.1.4 (c (n "simplet2s") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0a9dd03p6hqbhfk7l584h1hjjc3wk55g99df7x9lssiyxgh16qi0") (f (quote (("unstable"))))))

(define-public crate-simplet2s-0.1.5 (c (n "simplet2s") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1v75al3gq29nxswcm423qyiypx2f8yrx49jpp2qm62q6c8p165kl") (f (quote (("unstable"))))))

(define-public crate-simplet2s-0.2.0 (c (n "simplet2s") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "09wyqgv18pakv4hh1by2d5mggikniz7hbh7w4cva1l2wgx80qq3f") (f (quote (("unstable"))))))

