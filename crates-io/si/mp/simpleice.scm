(define-module (crates-io si mp simpleice) #:use-module (crates-io))

(define-public crate-simpleice-0.1.0 (c (n "simpleice") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "console") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "lettre") (r "^0.6.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r7x6vds2kqydrzfwp0hi1wi6ny55dzyh97kzi3k3w0i4vh24pp3")))

