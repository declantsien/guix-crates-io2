(define-module (crates-io si mp simple_pdf_generator_derive) #:use-module (crates-io))

(define-public crate-simple_pdf_generator_derive-0.1.0 (c (n "simple_pdf_generator_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "13np7v10mwvcd9qq55y7li1xjwiz1ph7nzqqxmambph6l6fzy6l1")))

(define-public crate-simple_pdf_generator_derive-0.1.1 (c (n "simple_pdf_generator_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1z6sqrngglqxvnjhmwb77nwlmpxkcd83j24v1vk24xqrn9l0g52p")))

(define-public crate-simple_pdf_generator_derive-0.2.0 (c (n "simple_pdf_generator_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1b5v60gnmvrb9a2ni3939dnzfp2r8iqdgryc9gg974zhdapdpdi1")))

(define-public crate-simple_pdf_generator_derive-0.2.1 (c (n "simple_pdf_generator_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1iv2fy2kd0i7qyb6gnn4ngnv8g7msjdpsd987jy23240qjyi92g0")))

