(define-module (crates-io si mp simple_password_generator) #:use-module (crates-io))

(define-public crate-simple_password_generator-1.0.0 (c (n "simple_password_generator") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xigzgdwhh9wmryxn3sxc0bx0cpksif62dp058ija9rvfhx8rac9")))

(define-public crate-simple_password_generator-1.0.1 (c (n "simple_password_generator") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hn3rbfj4qnc7f2dq65a14i23hfharx1c85g3g2rzl8ldq8phja0")))

