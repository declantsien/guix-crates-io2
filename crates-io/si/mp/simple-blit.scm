(define-module (crates-io si mp simple-blit) #:use-module (crates-io))

(define-public crate-simple-blit-0.1.0 (c (n "simple-blit") (v "0.1.0") (h "1kjslz5bfp3dbl8wg6zzlh343r6ccjbmlz3j8105qii2slizig5j")))

(define-public crate-simple-blit-0.2.0 (c (n "simple-blit") (v "0.2.0") (h "1141472nxx7raa2jfhi72sf7jxa8rc560xp1ivlhn7lnqvwfaam2")))

(define-public crate-simple-blit-0.3.0 (c (n "simple-blit") (v "0.3.0") (h "05g6m3195jkywm2jfnjcgxcp52w6pck9k6ic9y6a8fix21b2siw0")))

(define-public crate-simple-blit-0.4.0 (c (n "simple-blit") (v "0.4.0") (h "1a6jv75i52kbzhfg5bj7z0wa9pqkmapfxrkwwc8d1jz7jh2mwy8j")))

(define-public crate-simple-blit-0.5.0 (c (n "simple-blit") (v "0.5.0") (h "19r3fx9jpnxwcbhhflp7b446jzq1yphb08zsvy4b9bllgljwxa5x")))

(define-public crate-simple-blit-0.6.0 (c (n "simple-blit") (v "0.6.0") (h "0vhi320ikcs12rpwyq2vldhzhxmifij33sddgcydpanl914ndwli")))

(define-public crate-simple-blit-0.6.1 (c (n "simple-blit") (v "0.6.1") (d (list (d (n "pixels") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0nqm48rdf9bqvxcgjhj2akxcd2jjbg9vm60s90jlrs8jvymp9a08") (f (quote (("pixels-integration" "pixels" "rgb") ("default"))))))

(define-public crate-simple-blit-0.7.0 (c (n "simple-blit") (v "0.7.0") (d (list (d (n "pixels") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)))) (h "06pic8m8zkam9bz6dzzgny5g450bq4kx7jvmbc8zwbjjqfhrpwg4") (f (quote (("pixels-integration" "pixels" "rgb") ("default"))))))

(define-public crate-simple-blit-0.7.1 (c (n "simple-blit") (v "0.7.1") (d (list (d (n "pixels") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0lh6xpnib6yd8rznrv8gvr3rcv7xim80fpskih7a4sry9zr4pxyw") (f (quote (("pixels-integration" "pixels" "rgb") ("default"))))))

(define-public crate-simple-blit-0.8.0 (c (n "simple-blit") (v "0.8.0") (d (list (d (n "pixels") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0gyxnp7qki1hcxyln452y9sz79m01cgy4z31cqc502kvfi5dfwgk") (f (quote (("pixels-integration" "pixels" "rgb") ("default"))))))

(define-public crate-simple-blit-1.0.0 (c (n "simple-blit") (v "1.0.0") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "pixels") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4") (f (quote ("std"))) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1askmx1h3q220chp9nkk4zclyq2r5hzypm605578s1x20c25j0xy") (f (quote (("pixels-integration" "pixels" "rgb") ("image-integration" "image") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "mint/serde"))))))

