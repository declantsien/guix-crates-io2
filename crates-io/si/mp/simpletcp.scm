(define-module (crates-io si mp simpletcp) #:use-module (crates-io))

(define-public crate-simpletcp-0.1.0 (c (n "simpletcp") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0kplkl3whqivahhdglifd5fxja8wchv3zdn00ihn14hqw8w60rns")))

(define-public crate-simpletcp-0.2.0 (c (n "simpletcp") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1b1gc4qjdgydv1k29x2n07i1m8mw30kignlb8xxy1mhq0sxmkvdn")))

(define-public crate-simpletcp-0.3.0 (c (n "simpletcp") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bmd4ss7sysqbx86nks1pwg3fmwh4542843grnnbfzizpdp421cx")))

(define-public crate-simpletcp-0.3.1 (c (n "simpletcp") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "069xhv78clm0ng4xncj1jjkylz9vkz3lyfbvpz4r85w5p2ayap3l")))

(define-public crate-simpletcp-0.4.0 (c (n "simpletcp") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "040f3adp96dv3pf0mkd4grm2q7wkm53q7cx3f5m7anbvaflwbmay")))

(define-public crate-simpletcp-0.4.1 (c (n "simpletcp") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1jjf37cqih7z5wr49gmxl8x4lh9zxlc72irzfbrpwap3lbckybf4")))

(define-public crate-simpletcp-0.5.0 (c (n "simpletcp") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gp2d6404r4612kr3rpn9vpxb52l4nl77vwa6pg01vkvrfbv5jr1")))

(define-public crate-simpletcp-0.5.1 (c (n "simpletcp") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lf69gbvmdcldjprqjg9pnqcwflmpsb09fwcnlvqchfb3gnlfc0q")))

(define-public crate-simpletcp-1.0.0 (c (n "simpletcp") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1mnrxdsnfrwlg0kl3ybl8dj3vbq87gbmxa1860b732dhmlf1f733")))

(define-public crate-simpletcp-1.1.0 (c (n "simpletcp") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nvn4x34ij0sxw4r7hngvr6dr11kn9x762sfgi6wqwf03603nd39")))

(define-public crate-simpletcp-1.2.0 (c (n "simpletcp") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xn6d88i9h2fngfd6qkiqvgickkm0inwx7ai4gisr8063ydryx30")))

(define-public crate-simpletcp-1.2.1 (c (n "simpletcp") (v "1.2.1") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "18rpdskaaidri56i59j8jmdi6n9l5cginbnac36jiwd24gcbb1jw")))

