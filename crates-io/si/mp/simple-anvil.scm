(define-module (crates-io si mp simple-anvil) #:use-module (crates-io))

(define-public crate-simple-anvil-0.1.0 (c (n "simple-anvil") (v "0.1.0") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "0x4kshj6f9aw45fzcvflqc37m5zv6q18p2jhcd4lwhim3vxyjiqs")))

(define-public crate-simple-anvil-0.2.0 (c (n "simple-anvil") (v "0.2.0") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "06a6qwhcjnp5sr4wmvwn2v19lxk7baq0iy993xrcyfz6i0zzniz7")))

(define-public crate-simple-anvil-0.3.0 (c (n "simple-anvil") (v "0.3.0") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "0cw2j5f6xl470dig6znv03h07hp9hkkl7lmpxjv4bz6s1m525a89")))

(define-public crate-simple-anvil-0.3.1 (c (n "simple-anvil") (v "0.3.1") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "191y2nnyim12biq1qkk8cd4h6l51pm58ddmzd6vja93vpsv89b4w")))

(define-public crate-simple-anvil-0.3.2 (c (n "simple-anvil") (v "0.3.2") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "1irk41bg9lrzdhvi38vds5ssk52vshh8gfjfhs38k3jb0jjdw5i5")))

(define-public crate-simple-anvil-0.3.3 (c (n "simple-anvil") (v "0.3.3") (d (list (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)))) (h "15lmlps2shfr1vmjrd4djg75ndx27k541nasdkyyfmq1s4633ayw")))

