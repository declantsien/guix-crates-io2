(define-module (crates-io si mp simple-term-renderer) #:use-module (crates-io))

(define-public crate-simple-term-renderer-0.4.1 (c (n "simple-term-renderer") (v "0.4.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1fcxsjm6j7gfr3c7cs8z8gyr4amh0xdf0rdg0z2jhzfnvm309hfg")))

(define-public crate-simple-term-renderer-0.5.0 (c (n "simple-term-renderer") (v "0.5.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.6") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0l139cf3fkhph4mhq1brrqnxs9qsmk6jaqvlwf17z3c8y42jbvi9")))

