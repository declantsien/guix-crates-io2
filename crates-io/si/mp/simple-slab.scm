(define-module (crates-io si mp simple-slab) #:use-module (crates-io))

(define-public crate-simple-slab-0.1.0 (c (n "simple-slab") (v "0.1.0") (h "1l7fh0shl4z4w6nbxvn58abkpzclm3ycc3gwgvm2m75my2dv63s1")))

(define-public crate-simple-slab-0.2.0 (c (n "simple-slab") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1aygpp56bm9al1k7mnlfcfhc8n7acz9j30k1hli565fdsyv0xswn")))

(define-public crate-simple-slab-0.2.1 (c (n "simple-slab") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "04fwyhsdkjh7bzgxj3pxbh7cjrgp172wj797cpw4y2md90dmb755")))

(define-public crate-simple-slab-0.2.2 (c (n "simple-slab") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "08rrbxzj1sysdiziw2nwmihwpchzpmfzcw6d03qxshydypvlrx2h")))

(define-public crate-simple-slab-0.2.3 (c (n "simple-slab") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xg4iqzzs8mmhvxqjk0s95qf3y5l9z6k930klx3c6mfk9qdl4xfw")))

(define-public crate-simple-slab-0.3.0 (c (n "simple-slab") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06q0h997c1cv9p44qyxsy1kcl21civgqcmk49cmz04kqds23s65a")))

(define-public crate-simple-slab-0.3.1 (c (n "simple-slab") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ggfaa0b8mlillwqmkbqiwisf84hddckff9kipw8vf18z15zwih2")))

(define-public crate-simple-slab-0.3.2 (c (n "simple-slab") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "090hm99lkp59p7pdic3bw6sv84dgv1jdl9g30ywp53171hdz1815")))

(define-public crate-simple-slab-0.3.3 (c (n "simple-slab") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m3axcsdghnh7pwclw5pwji5qa5wgmgk3nsvhk8s3vnw263b4d6l")))

