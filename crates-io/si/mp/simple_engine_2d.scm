(define-module (crates-io si mp simple_engine_2d) #:use-module (crates-io))

(define-public crate-simple_engine_2d-0.1.0 (c (n "simple_engine_2d") (v "0.1.0") (d (list (d (n "image") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.14") (d #t) (k 0)))) (h "1xvm308cv09k81rmv6m6q8s22s4hxyp62mz7n2lxn2cr12ijy0ws") (r "1.74")))

(define-public crate-simple_engine_2d-0.1.1 (c (n "simple_engine_2d") (v "0.1.1") (d (list (d (n "image") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.14") (d #t) (k 0)))) (h "173kl6dpfhm19dfgmdvkcrn0rff1bcn6jajgbdv7viwsalr05zz4") (r "1.74")))

