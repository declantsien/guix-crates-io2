(define-module (crates-io si mp simple_modbus) #:use-module (crates-io))

(define-public crate-simple_modbus-0.1.0 (c (n "simple_modbus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1mhdfwkr9l0d0583v8lgdqw9ry894gxam6g7fb6ap1455rwghg6q")))

(define-public crate-simple_modbus-0.1.1 (c (n "simple_modbus") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "06llda6cz1bxnxq5yifshd5pswqwgb1r0i30zldwkwlvv1zpr0n5")))

