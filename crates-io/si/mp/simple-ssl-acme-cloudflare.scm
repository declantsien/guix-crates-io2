(define-module (crates-io si mp simple-ssl-acme-cloudflare) #:use-module (crates-io))

(define-public crate-simple-ssl-acme-cloudflare-1.0.0 (c (n "simple-ssl-acme-cloudflare") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1230dy44g9rjmq1pxfc83r4km4pg9163iinv3agrgv9gv97apazr")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.1 (c (n "simple-ssl-acme-cloudflare") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "06g5kaxkd6lq11f4mvqafrl5wklcb8ak762sk7fn6k2kziw5788h")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.2 (c (n "simple-ssl-acme-cloudflare") (v "1.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1sar6x2ik5y9xv5am4hl4wgcmflycbjg0ih2vz2vf2ypjan4bjq1")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.3 (c (n "simple-ssl-acme-cloudflare") (v "1.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "0jn85ibnbhzvppcc7i5bii2rv8zj0imr1s3iwc9vm946qdmd5zfl")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.4 (c (n "simple-ssl-acme-cloudflare") (v "1.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "0h6290h3rvbwsgdpyazdp479clfnjrjhzpvxsz31m5csfxzf79h2")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.5 (c (n "simple-ssl-acme-cloudflare") (v "1.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "0kyc064gb5q1crs0nzg0y7pjz3jsbg6q75xf7xjczz9q72vy8dsr")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.6 (c (n "simple-ssl-acme-cloudflare") (v "1.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1djfpbmmwij6kc7450j855kdc6mihrndspjbrhzq4a34z90hw9wb")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.7 (c (n "simple-ssl-acme-cloudflare") (v "1.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1wml3ig6pg0n3ha8qsik048334w9i7zifz807cs93jlr3a6gq2ym")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.8 (c (n "simple-ssl-acme-cloudflare") (v "1.0.8") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.3") (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "19nbysx9rpk2jpfphclx4cpx4nb2x312arksc9536rhzd4nbfb5x")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.9 (c (n "simple-ssl-acme-cloudflare") (v "1.0.9") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "15lm2mkg66q0n5c8vldxifigj7f11f0m3qz2az31hczcb2klkss0")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.10 (c (n "simple-ssl-acme-cloudflare") (v "1.0.10") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1pb1laanx2hylqdswflvmf5pxr48p6yap1hq3vb4pg54wc1j3fxb")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.11 (c (n "simple-ssl-acme-cloudflare") (v "1.0.11") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1bzcm5kjca1jz5ss88crpxzi21p41lx0acr23z5091z6q1qhibap")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.12 (c (n "simple-ssl-acme-cloudflare") (v "1.0.12") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0z8qzxh3as35nhkcy6wjbqjx25ywhh1h2aq2nvlp4q9kf27nrh1f")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.13 (c (n "simple-ssl-acme-cloudflare") (v "1.0.13") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("once_cell_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0zxmxqf2d73ar99k1khxdwj1wdn7g6nicpxf7am88g1iv9vj0q7i")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.14 (c (n "simple-ssl-acme-cloudflare") (v "1.0.14") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("once_cell_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "0787cm9js55df2smvdi594sld4pm7i83szgvk26l7mw6anginh4m")))

(define-public crate-simple-ssl-acme-cloudflare-1.0.15 (c (n "simple-ssl-acme-cloudflare") (v "1.0.15") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("once_cell_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "13sv4hp0m3vf5nipgj5dpp3dn2bwkpz7czka03hl705s2rhwhpk9") (r "1.60")))

(define-public crate-simple-ssl-acme-cloudflare-1.1.0 (c (n "simple-ssl-acme-cloudflare") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "execute") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "0r8m7xdy3g1534x7y3wrq8ac343g2pxn9hy07xqkvgv49zzfv5bf") (r "1.70")))

