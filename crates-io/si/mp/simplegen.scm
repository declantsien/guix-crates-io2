(define-module (crates-io si mp simplegen) #:use-module (crates-io))

(define-public crate-simplegen-0.1.1 (c (n "simplegen") (v "0.1.1") (h "0lm0j028m18zid8j5am1j2nlqrrjii9fxa6xx21y873k0lb7g7na") (y #t)))

(define-public crate-simplegen-0.1.2 (c (n "simplegen") (v "0.1.2") (h "0q3cpm734zskh5c9nlv85g56q574010nyqknpipdmv31g5fwgdcs")))

(define-public crate-simplegen-0.2.0 (c (n "simplegen") (v "0.2.0") (h "16l91b7c90gj5vkaqndsf622vbvq0z6dxvhi0q51f448jriav7cj")))

(define-public crate-simplegen-0.2.1 (c (n "simplegen") (v "0.2.1") (h "128gisyqxw1i2c9f8xn1a097hmy8cphw7iipn3nk46izmwfwzgyb")))

(define-public crate-simplegen-0.2.2 (c (n "simplegen") (v "0.2.2") (h "0zyn3iw1dy8f4qvic5ry7s71bgq4x0x1nx6c5hj3agxfha7l2pql")))

(define-public crate-simplegen-0.2.3 (c (n "simplegen") (v "0.2.3") (h "18gk8wa1jyqazvxfsij6za46vyj4c12dj6mpbjn9rpc15hz06qn1")))

