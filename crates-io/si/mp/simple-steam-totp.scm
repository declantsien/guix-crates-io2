(define-module (crates-io si mp simple-steam-totp) #:use-module (crates-io))

(define-public crate-simple-steam-totp-0.1.0 (c (n "simple-steam-totp") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "13l34c6alsh91zyvmq3bp0261iwmdr1ai8cqai03zyxh325lszdh")))

