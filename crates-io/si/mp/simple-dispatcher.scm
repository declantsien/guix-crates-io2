(define-module (crates-io si mp simple-dispatcher) #:use-module (crates-io))

(define-public crate-simple-dispatcher-0.1.0 (c (n "simple-dispatcher") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1j62q3g8lxm6j7lzgv97y3gndzrmhpcjw7xr7mq62z5nzpg45f89") (y #t)))

(define-public crate-simple-dispatcher-0.1.1 (c (n "simple-dispatcher") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1hn78q8c390a84lhfxaj3cy5kj49i8h1fy5zygyiavswlfhz4dgm") (y #t)))

(define-public crate-simple-dispatcher-0.1.2 (c (n "simple-dispatcher") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "152k269zgz3yzcdjnc490vklp5zh6935vyfi3qq24fw2cz4fdhym") (y #t)))

(define-public crate-simple-dispatcher-0.2.0 (c (n "simple-dispatcher") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1f7vrqzwsqsbllwimmqrxxdv48680hqzi0s509hrzdgbdpfi309f") (y #t)))

(define-public crate-simple-dispatcher-0.2.1 (c (n "simple-dispatcher") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0svwgvb764klrz7ivxack7r9ma27wygsbyk4jjlhsjcp6w5r6ncs") (y #t)))

(define-public crate-simple-dispatcher-0.3.0 (c (n "simple-dispatcher") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1d8bk554fpdi90kkvimnmy5wy332ghjw5vhljir4s1v7iw6cx9sa") (y #t)))

(define-public crate-simple-dispatcher-0.3.1 (c (n "simple-dispatcher") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1l8fhx0hypbmdzv58v11y3algsfrygpa9l1pzfhsgllij8rvxzvl") (y #t)))

(define-public crate-simple-dispatcher-0.3.2 (c (n "simple-dispatcher") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1y923wl3wqshwrggvhdjckjxmipcpxf8mgj0vc1xxf3jnpkw6ziy") (y #t)))

(define-public crate-simple-dispatcher-0.3.3 (c (n "simple-dispatcher") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "00ygjw242rn76sgbppzp4h51k0m41gbl5jb6qj46si6hln952c3d") (y #t)))

(define-public crate-simple-dispatcher-0.3.4 (c (n "simple-dispatcher") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1xihd7m4zn893f0dj4dg1dmx3dnnh270rl61gzgql64vbfbvvhkr") (y #t)))

(define-public crate-simple-dispatcher-0.3.5 (c (n "simple-dispatcher") (v "0.3.5") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1w7zzfky32dlabaamgrnpyk62j5xv7hy3skyalira87r5jlscsj0") (y #t)))

(define-public crate-simple-dispatcher-0.4.0 (c (n "simple-dispatcher") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "09bi0zsja19r41canbwp3qq0hvrwjgawxlagh50a1c8hv3aii4kj") (y #t)))

(define-public crate-simple-dispatcher-0.4.1 (c (n "simple-dispatcher") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0m4b74r6sgbw0q2rr7armxa1fg90c1yhk5nm4p2rqvyn00hrbxpj") (y #t)))

(define-public crate-simple-dispatcher-0.5.0 (c (n "simple-dispatcher") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "09cz67s8x2vx571p7wj7hpl5q96ilprkvr2g35my3aqdlh5zx52n") (y #t)))

