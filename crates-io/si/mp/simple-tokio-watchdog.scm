(define-module (crates-io si mp simple-tokio-watchdog) #:use-module (crates-io))

(define-public crate-simple-tokio-watchdog-0.1.0 (c (n "simple-tokio-watchdog") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1nni20m2mj6ja9f8d831msnj1358dhr8471d2bph98jcq87caj0w") (s 2) (e (quote (("serde" "dep:serde") ("cli" "dep:clap"))))))

(define-public crate-simple-tokio-watchdog-0.2.0 (c (n "simple-tokio-watchdog") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "141spxhcycj2cdhl9y6nqy8sb7yp96x3zyngjvsw6cqv0mkb8250") (s 2) (e (quote (("serde" "dep:serde") ("cli" "dep:clap"))))))

