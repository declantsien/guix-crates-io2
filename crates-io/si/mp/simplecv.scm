(define-module (crates-io si mp simplecv) #:use-module (crates-io))

(define-public crate-simplecv-0.0.1 (c (n "simplecv") (v "0.0.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "02ariwgpqi9r7fb5n5jmm9bym5ch5sr3h8xbvj1vvcw0z17rgixk")))

