(define-module (crates-io si mp simplecss) #:use-module (crates-io))

(define-public crate-simplecss-0.1.0 (c (n "simplecss") (v "0.1.0") (h "03hc4qab1vilpvh7q219v1v4zacl7qj8mqinvxkl19l5g84qamhk")))

(define-public crate-simplecss-0.2.0 (c (n "simplecss") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7") (d #t) (k 2)))) (h "054hzai3yzg7ww0k482539gwfqfa89il30fnvfh5dmcn6pk58rar")))

(define-public crate-simplecss-0.2.1 (c (n "simplecss") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.6") (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7") (d #t) (k 2)))) (h "17g8q1z9xrkd27ic9nrfirj6in4rai6l9ws0kxz45n97573ff6x1")))

