(define-module (crates-io si mp simplers_optimization) #:use-module (crates-io))

(define-public crate-simplers_optimization-0.1.0 (c (n "simplers_optimization") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "02fa9mzngx3iqbs46g10qs8i0qknvh7gqpyph8qw62bmdgdpfqwh")))

(define-public crate-simplers_optimization-0.1.1 (c (n "simplers_optimization") (v "0.1.1") (d (list (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "0ar3v9xxlqxr68m1a0l8s8b98w3z9waqx1gbb1c0jpzl3y5hcafq")))

(define-public crate-simplers_optimization-0.1.2 (c (n "simplers_optimization") (v "0.1.2") (d (list (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "0rdfibpsag1hdridnjl8q53d5pkmj13l13qak9hm9yyanci56n54")))

(define-public crate-simplers_optimization-0.2.0 (c (n "simplers_optimization") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "19k6vchm0vd0rikkc5rzlr8zk0chf34wfbr0mfa00sx9v9gv8xs6") (y #t)))

(define-public crate-simplers_optimization-0.2.1 (c (n "simplers_optimization") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "13yh8l6xlcxn1kgi41h93xs5lfnaz0khwnahyblma3clyb89nv64")))

(define-public crate-simplers_optimization-0.3.0 (c (n "simplers_optimization") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.9") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "1zagn6j9fzrxgvmdzh793rs7l1mm86a9zwz9wjzy0hv7pi238lb4")))

(define-public crate-simplers_optimization-0.4.0 (c (n "simplers_optimization") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "1972v3ivw0w11knq63gl3n3mhk49834dy7jkchpv9lig5kz4989g")))

(define-public crate-simplers_optimization-0.4.1 (c (n "simplers_optimization") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)))) (h "1jy0qvyjm362qnb0m8d7ncz6yk97159cac8nghw5aik743h412n9")))

(define-public crate-simplers_optimization-0.4.2 (c (n "simplers_optimization") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.1") (d #t) (k 0)))) (h "0jkjm2ngy2lr9lmwl9x2dxbz7v65zm6np65rbdbn5b61bdy19ylx")))

(define-public crate-simplers_optimization-0.4.3 (c (n "simplers_optimization") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.0") (d #t) (k 0)))) (h "0hzcmn32vhfcw1rxvbff4xw552mc5cpjlb335id5f5iapc97kn9c")))

