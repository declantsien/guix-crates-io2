(define-module (crates-io si mp simple_on_shutdown) #:use-module (crates-io))

(define-public crate-simple_on_shutdown-0.1.0 (c (n "simple_on_shutdown") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0gdhabx1cb5d1h2n94dzn2zsm201wn1hminfdi2wsai99ghgmy7c")))

(define-public crate-simple_on_shutdown-0.1.1 (c (n "simple_on_shutdown") (v "0.1.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0374czlhqfdnjb8k3i6z3nd6lm30zvawic6mxgmkb84r00gz3vvi")))

(define-public crate-simple_on_shutdown-0.1.2 (c (n "simple_on_shutdown") (v "0.1.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1xivg4drvdbf6hw1d3h555h5lbfgrzc08jn98j2jiddp0cjxksr7")))

(define-public crate-simple_on_shutdown-0.1.3 (c (n "simple_on_shutdown") (v "0.1.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0qhf1syngfbp1b1bvppd5k421iplbbnhnlgzms76haqa16xfn2k5")))

(define-public crate-simple_on_shutdown-0.1.4 (c (n "simple_on_shutdown") (v "0.1.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1m78cgk663s4s5c6gwpzvrhk834qz3j3xr99n1p012iik8g272pr")))

(define-public crate-simple_on_shutdown-0.1.5 (c (n "simple_on_shutdown") (v "0.1.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0s745kspkmyiy3w1mvqarl0ih6iaj3rcziqpmbr447sxbvxdf6h1")))

(define-public crate-simple_on_shutdown-0.1.6 (c (n "simple_on_shutdown") (v "0.1.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1rnxfi9grzvmdx35mqrpcaxj7s8p87cf321isyrmj24fc15dwwpm")))

(define-public crate-simple_on_shutdown-0.1.7 (c (n "simple_on_shutdown") (v "0.1.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1sysfa8pfw4zzkfhq1mv32vi0rk6n4s3rn0239z06c1vkgyq88r4")))

(define-public crate-simple_on_shutdown-0.1.8 (c (n "simple_on_shutdown") (v "0.1.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0zni2lsb9iwb5qfv3d6c5y4h6wliyjlm7rd5pxfzhrzc5jl9swvn")))

(define-public crate-simple_on_shutdown-0.2.0 (c (n "simple_on_shutdown") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "02d57kas4i0gmplhk3gb6wb8h9wyif35w2ynsi9afihmddhvcy93") (y #t)))

(define-public crate-simple_on_shutdown-0.2.1 (c (n "simple_on_shutdown") (v "0.2.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "007ll4vrag97wr8k9li3cf9q1w3gavbqvrd4ffa31n3dbqn4n7b8")))

(define-public crate-simple_on_shutdown-0.2.2 (c (n "simple_on_shutdown") (v "0.2.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1kjav0cz0xgf8f2s99n9v9h5f7fjzqk4n7xg1k56ckczndj5zgv4")))

(define-public crate-simple_on_shutdown-0.3.0 (c (n "simple_on_shutdown") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1mxl9qdm3rvnylksmzirid6mv93d3n6v7g0dypvqk9l5r1wyg5lm")))

(define-public crate-simple_on_shutdown-1.0.0 (c (n "simple_on_shutdown") (v "1.0.0") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)))) (h "1qn61822jj4mfzpblfaigpdbz02xbksak6zj7vl3497lcpkndpq8")))

