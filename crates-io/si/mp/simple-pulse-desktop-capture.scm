(define-module (crates-io si mp simple-pulse-desktop-capture) #:use-module (crates-io))

(define-public crate-simple-pulse-desktop-capture-0.1.0 (c (n "simple-pulse-desktop-capture") (v "0.1.0") (d (list (d (n "libpulse-binding") (r "^2.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "08ns9yx66ab4iakk4bg44y6maq5z73hkj0fdzydii3nji37qi69m")))

(define-public crate-simple-pulse-desktop-capture-0.1.1 (c (n "simple-pulse-desktop-capture") (v "0.1.1") (d (list (d (n "libpulse-binding") (r "^2.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0s4pzhn7ilgdzg0hql08p6m4gkxwic1v874r4z1rahvz7w0r4q7h")))

