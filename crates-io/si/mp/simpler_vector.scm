(define-module (crates-io si mp simpler_vector) #:use-module (crates-io))

(define-public crate-simpler_vector-0.1.0 (c (n "simpler_vector") (v "0.1.0") (h "0wj0ycsizf2yc4bmn8hps527b18ykp082c05ks9pif529xr8n23z")))

(define-public crate-simpler_vector-0.1.1 (c (n "simpler_vector") (v "0.1.1") (h "1jif3ms3x7f4znd4jifan3w5nq7gh5s29ix64j5jl59z4ryxzzx1")))

(define-public crate-simpler_vector-0.1.2 (c (n "simpler_vector") (v "0.1.2") (h "00mvy8a8sfdh185bxdy7r6n07j7kh10lyyh63y2sswz2gi1n7ijl")))

