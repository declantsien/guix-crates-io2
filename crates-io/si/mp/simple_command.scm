(define-module (crates-io si mp simple_command) #:use-module (crates-io))

(define-public crate-simple_command-0.1.0 (c (n "simple_command") (v "0.1.0") (h "10kkr60q4xs6prvfa5azsd0g86lxdcqcn6wn6dm056jh7z32ya24")))

(define-public crate-simple_command-0.1.1 (c (n "simple_command") (v "0.1.1") (h "0bvn188hbv2pa8va3y91017glyk7lks5fdg3qxwascbf1hwgjf14")))

(define-public crate-simple_command-0.1.2 (c (n "simple_command") (v "0.1.2") (h "0ppkad7lw05w2h4aa1k7bkhjbnkyy627kl4lyx27kzzdrxjijagn")))

