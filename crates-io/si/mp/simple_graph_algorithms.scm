(define-module (crates-io si mp simple_graph_algorithms) #:use-module (crates-io))

(define-public crate-simple_graph_algorithms-1.0.0 (c (n "simple_graph_algorithms") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pzx1g1g14h686hcg0h48fjxf75fzv9338w20n29vhg5741figrs") (f (quote (("from_instruction")))) (s 2) (e (quote (("serde" "dep:serde"))))))

