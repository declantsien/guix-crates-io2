(define-module (crates-io si mp simplicity) #:use-module (crates-io))

(define-public crate-simplicity-0.1.0 (c (n "simplicity") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "166i7psrg4fqq9rl54ky0aykb01b74a9ri7ppvkgn3xb7s5bsgk2")))

(define-public crate-simplicity-0.2.0 (c (n "simplicity") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "04ax11zl7fw576ndpmjh7jqipy12y0ji9657xshagn0wdzi90a4l")))

(define-public crate-simplicity-0.2.1 (c (n "simplicity") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0vwc1zn4chc5mskapz0f568lzb4slinzw2y3qzn6biiaxgjndksz")))

(define-public crate-simplicity-0.3.0 (c (n "simplicity") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.5") (d #t) (k 0)) (d (n "simplicity_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0p97ix4r61dx99jkjv29pis14cvw5w64r033k892wzrvahgwmj32")))

(define-public crate-simplicity-0.4.0 (c (n "simplicity") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.5") (d #t) (k 0)) (d (n "simplicity_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "1mmfrpjj5y8x068p9r1w9c45hdws0cnzdvaa52nv7fr51wpg57iv")))

(define-public crate-simplicity-0.4.1 (c (n "simplicity") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.6") (d #t) (k 0)) (d (n "simplicity_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0s4sdhiyiqqphvqi4h1cxh7cd6k7h413d3nvg9c5i6mh9q10vqpg")))

(define-public crate-simplicity-0.4.2 (c (n "simplicity") (v "0.4.2") (d (list (d (n "nalgebra") (r "^0.24.0") (d #t) (k 0)) (d (n "robust-geo") (r "^0.1.7") (d #t) (k 0)) (d (n "simplicity_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0z4znpv5dxx77gcwgg09xzqs4cvqy9n20y4d6321dykm8k9wkgdv")))

