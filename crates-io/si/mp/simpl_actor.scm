(define-module (crates-io si mp simpl_actor) #:use-module (crates-io))

(define-public crate-simpl_actor-0.1.0 (c (n "simpl_actor") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06l0h9y8jvy10zns6f8j65wm0i5ac6c5pg13mz0f8y3daan4hdq9")))

(define-public crate-simpl_actor-0.1.1 (c (n "simpl_actor") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rhbpkwvnkdf7avhh415ag3q3x1aldnblgm9bbjbyprjgxjbr849")))

(define-public crate-simpl_actor-0.2.0 (c (n "simpl_actor") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0xski8xbs8a7plm5mdsqhwynpw4mf2f7x92h24s0s4ba2f9fsnjl")))

(define-public crate-simpl_actor-0.2.1 (c (n "simpl_actor") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j9d2lw4hr7a99l0p3hyd9yj6fhp3jnhf92ljzhkcd00iah7pqrf")))

(define-public crate-simpl_actor-0.2.2 (c (n "simpl_actor") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0asr125rsy54rr8q6232p7fp5s0a20mmr9igd8wks5gmh3wajp0p")))

(define-public crate-simpl_actor-0.2.3 (c (n "simpl_actor") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05qgig5k89xpv1m68kn6vqpnx6p8v8dybha7cnq6v8xxpggl0w8m")))

(define-public crate-simpl_actor-0.2.4 (c (n "simpl_actor") (v "0.2.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lfadl7j5bp398sjlvi1zbdfap6l6hv1bjfil186w3p8m814x1d0")))

(define-public crate-simpl_actor-0.2.6 (c (n "simpl_actor") (v "0.2.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ijjw06gjy0xq354wxw125k3vb7r8s50rkxijlmrz5kcnsvddra6")))

(define-public crate-simpl_actor-0.2.7 (c (n "simpl_actor") (v "0.2.7") (d (list (d (n "criterion") (r "^0.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "simpl_actor_macros") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05bp4maq7q9wcnqqjwx7krgkm9n4xab0vgl1wqbgw02n2kjzs60w")))

