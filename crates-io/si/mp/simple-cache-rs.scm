(define-module (crates-io si mp simple-cache-rs) #:use-module (crates-io))

(define-public crate-simple-cache-rs-0.1.0 (c (n "simple-cache-rs") (v "0.1.0") (h "01xlgfqfzl07i1axzjxd8x4v776dxmhaf4p5p1zxg67ipxcmd563")))

(define-public crate-simple-cache-rs-0.2.0 (c (n "simple-cache-rs") (v "0.2.0") (h "16297m58z8j2l9phs9jhcw3ns21l4ysb51yp4v93cyk4vv9cpd3g")))

(define-public crate-simple-cache-rs-0.3.0 (c (n "simple-cache-rs") (v "0.3.0") (h "1c66bxb1f9ysigqcsij3cywhfwryyriqndmmmp7fv004zn6hy3cy")))

(define-public crate-simple-cache-rs-0.3.1 (c (n "simple-cache-rs") (v "0.3.1") (h "1c5wn0qcky3kn85v70sfg4c0kbm2s5pisr2nyb9xqy9bngwr719w")))

(define-public crate-simple-cache-rs-0.4.0 (c (n "simple-cache-rs") (v "0.4.0") (h "1z1l44z68vf40kj3qfyh8a72kslhj36v1l51qlz095zrymzfvq4k")))

