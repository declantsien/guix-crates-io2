(define-module (crates-io si mp simple2fa) #:use-module (crates-io))

(define-public crate-simple2fa-0.1.0 (c (n "simple2fa") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (f (quote ("image"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "totp-lite") (r "^1.0.3") (d #t) (k 0)))) (h "037zcgkpjbdmj465r4xxa0mnzi5qiiz5icfnc21c6zw7n5lfgwp5")))

