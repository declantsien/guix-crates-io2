(define-module (crates-io si mp simple-vector2d) #:use-module (crates-io))

(define-public crate-simple-vector2d-0.1.0 (c (n "simple-vector2d") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "0vzw6xm8av9qa2h0m50j5mb7wcnk83m2w8nspwx7mzywyn51873f")))

(define-public crate-simple-vector2d-0.1.1 (c (n "simple-vector2d") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1cq1m65vwxxk9m0fqdb4v1ca6cygbp2r7918iw67hb1vzzyi6wk0")))

(define-public crate-simple-vector2d-0.1.2 (c (n "simple-vector2d") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0540h39r92rczqakv8b479lyvhf7ickgkpknh1p726j5p9n30d59") (f (quote (("serde-serial" "serde" "serde_derive") ("default"))))))

(define-public crate-simple-vector2d-0.1.3 (c (n "simple-vector2d") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.9.0, ~1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9.0, ~1") (o #t) (d #t) (k 0)))) (h "04nbx0dnfxipjz6w8qibf2hppbaciri17r1y1mbl6h148b610b99") (f (quote (("serde-serial" "serde" "serde_derive") ("default"))))))

(define-public crate-simple-vector2d-0.1.4 (c (n "simple-vector2d") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.9.0, ~1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9.0, ~1") (o #t) (d #t) (k 0)))) (h "07wkj23bhd60xwgwdpscpyvi0b8cfclwsvg7hl32zcz2css08fif") (f (quote (("serde-serial" "serde" "serde_derive") ("default"))))))

