(define-module (crates-io si mp simple-web-server) #:use-module (crates-io))

(define-public crate-simple-web-server-0.1.0 (c (n "simple-web-server") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-utils") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "18hxgxzb6zdap2ggqbf5nm4qvkwwqk204r6p4czihla5cc87gi7z")))

(define-public crate-simple-web-server-0.2.0 (c (n "simple-web-server") (v "0.2.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-utils") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "12ql0xikbmw6as93sqx99glykm29c38xn8f3wpqfj8s6kklhafky")))

