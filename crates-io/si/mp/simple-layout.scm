(define-module (crates-io si mp simple-layout) #:use-module (crates-io))

(define-public crate-simple-layout-0.0.0 (c (n "simple-layout") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "1a0c0rns3x32r9j3lyspcyzvcicslcssbfa4npc3qdhqcjx0wlbx") (f (quote (("simulate-example" "embedded-graphics-simulator" "log"))))))

(define-public crate-simple-layout-0.0.2 (c (n "simple-layout") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "0qb700rawxqs69fzv4vay3fkqw18i5hn5b0hvin2jhnbg7sw4xy0") (f (quote (("simulate-example" "embedded-graphics-simulator" "log"))))))

