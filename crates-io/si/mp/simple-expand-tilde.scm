(define-module (crates-io si mp simple-expand-tilde) #:use-module (crates-io))

(define-public crate-simple-expand-tilde-0.1.0 (c (n "simple-expand-tilde") (v "0.1.0") (d (list (d (n "simple-home-dir") (r "^0.3") (d #t) (k 0)))) (h "1zx8bf1fs7zikr17lzz87ib8r12r5p6n1qvg7r1a7hpvv6i7483m")))

(define-public crate-simple-expand-tilde-0.1.1 (c (n "simple-expand-tilde") (v "0.1.1") (d (list (d (n "simple-home-dir") (r "^0.3") (d #t) (k 0)))) (h "0z22a8iqlcn96d2hfl84jr4b8izspm1gwyfg7pvk0amba6gxkgax")))

(define-public crate-simple-expand-tilde-0.1.2 (c (n "simple-expand-tilde") (v "0.1.2") (d (list (d (n "simple-home-dir") (r "^0.3") (d #t) (k 0)))) (h "082p3dyjss0jshhxnb6jr0350laqb556h0nwn44b4gn9nb0igd3b")))

(define-public crate-simple-expand-tilde-0.1.4 (c (n "simple-expand-tilde") (v "0.1.4") (d (list (d (n "simple-home-dir") (r "^0.3.2") (d #t) (k 0)))) (h "0haa1bkni034gb08glyaqvplip35c93npzddgmnda26iq831178l")))

(define-public crate-simple-expand-tilde-0.1.5 (c (n "simple-expand-tilde") (v "0.1.5") (d (list (d (n "simple-home-dir") (r "^0.3.3") (d #t) (k 0)))) (h "07jwml9w68j1qgsqvd6rbgz074yx451ryhm3vivjw9g0jpyx29a8")))

(define-public crate-simple-expand-tilde-0.1.6 (c (n "simple-expand-tilde") (v "0.1.6") (d (list (d (n "simple-home-dir") (r "^0.3.4") (d #t) (k 0)))) (h "0jlpkd5gmylxmkvxj8gbv1k7rw1xga3nks2xdr18a3wrnwbx9ffv")))

