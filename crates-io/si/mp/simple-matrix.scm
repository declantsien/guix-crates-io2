(define-module (crates-io si mp simple-matrix) #:use-module (crates-io))

(define-public crate-simple-matrix-0.1.0 (c (n "simple-matrix") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "110anj34pijqhlffh9i7qzpl46j18jvmfma54k9bw6z67cl9yiqm") (f (quote (("impl_from"))))))

(define-public crate-simple-matrix-0.1.1 (c (n "simple-matrix") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "06yxb0r1nbaxn3wr38gqc6m92xkysabaa9l2dcb4abvb7d4lcmqk") (f (quote (("impl_from"))))))

(define-public crate-simple-matrix-0.1.2 (c (n "simple-matrix") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0z5ab16wx658gxqcpqc7l9yg98i9iq83x4kvik1i0vb0xb9a69v9") (f (quote (("impl_from"))))))

