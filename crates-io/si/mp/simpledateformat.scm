(define-module (crates-io si mp simpledateformat) #:use-module (crates-io))

(define-public crate-simpledateformat-0.0.0 (c (n "simpledateformat") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "087qknni9mdy4wx925wh3j0sn6fcm468d46dpw5np00wvxrgh6vp")))

(define-public crate-simpledateformat-0.1.0 (c (n "simpledateformat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "1cjvvz0dvrz4bqz4bx4bx31r2h6bpwjznixlyqavg7fjr3xa83a0")))

(define-public crate-simpledateformat-0.1.1 (c (n "simpledateformat") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "1100gv0qqrmpzssch7a997577d238andy1nmz3mg89hrwakajvyh")))

(define-public crate-simpledateformat-0.1.2 (c (n "simpledateformat") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "1zq4pxckz24jmkizb5raavgrk3yj1kl5agkyn7cmpsdyv960311a")))

(define-public crate-simpledateformat-0.1.3 (c (n "simpledateformat") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "05g1b9iy4r9cip9cbqv66zbm5b7i3i9appnnqi2ykfw10ha3c96d")))

(define-public crate-simpledateformat-0.1.4 (c (n "simpledateformat") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "0gia0z03sxzyxvrkba8a9ij0p6xb812ic73h50a3kwj1bwswx6nr")))

