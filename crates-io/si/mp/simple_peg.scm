(define-module (crates-io si mp simple_peg) #:use-module (crates-io))

(define-public crate-simple_peg-0.1.0 (c (n "simple_peg") (v "0.1.0") (h "0vms1ydrl2mfp7mb97ps861lm1kksb9nl66y25mgk5fa9827b6jq")))

(define-public crate-simple_peg-0.2.0 (c (n "simple_peg") (v "0.2.0") (h "0jzcrlibbyjnq1r8ppiyb383pizsrvwzj5r6v4gzrhlqhh1l8rkr")))

(define-public crate-simple_peg-0.3.0 (c (n "simple_peg") (v "0.3.0") (h "00sq162k4zpp19d6nsivwig41apr2pvazhvzkbd6r08jr6g2i3gp")))

