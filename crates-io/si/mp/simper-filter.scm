(define-module (crates-io si mp simper-filter) #:use-module (crates-io))

(define-public crate-simper-filter-0.1.0 (c (n "simper-filter") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14q549f758fzn8l63819m7gc6j8xhbaa3gmfk983lnw3aic15fbj")))

