(define-module (crates-io si mp simplelog) #:use-module (crates-io))

(define-public crate-simplelog-0.1.0 (c (n "simplelog") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0vr5pv4ccbw4383j21n6cijiwqxswnvgpscfnnk1d8akd1ji2lg9") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.2.0 (c (n "simplelog") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0djh16rf4nw9dpy3sp15v61bhp9fhhwm0ml61f9bsljzw2bf4xs3") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.3.0 (c (n "simplelog") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1abanh1zr4jvnvzswhqkwhwwimhixkrmfdmkagnqpysmigj6i1af") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.3.1 (c (n "simplelog") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "13794j130kjvm9p405q4ib1l267mp84knlyx6krj9a4bpngy7lyi") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.3.2 (c (n "simplelog") (v "0.3.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zf8a500yxh8c9yhr42575ydqyb41m9x2w468vpkvcz76mkin3fi") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.4.0 (c (n "simplelog") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "13sgax8h547gk5n2zf6zgdrva1z463kmhqd4fbk04gg4n0naggnz") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.4.1 (c (n "simplelog") (v "0.4.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0drgnxgy5z7g5kqv77hrqjabsjz238y52l4q3sm65sk0miry98yq") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.4.2 (c (n "simplelog") (v "0.4.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jr36ba8kkc03l62mxn4cahi03nc68pac385ssg3bhsbnzr66fic") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.4.3 (c (n "simplelog") (v "0.4.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1s8344v9scidg9xfkqjlsikybi000bazh3vi9lpgz1zi311b3wsq") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.4.4 (c (n "simplelog") (v "0.4.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09fij81dpkc4lbn89nksfdj7vzj9rk7x18frcnjzylfclfqibdi4") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.5.0 (c (n "simplelog") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1f84lzsvrjlbhk8d6fh7najczdiq5mn7fba2z7yj96x47z08428v") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.5.1 (c (n "simplelog") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0kk21hgk4mga5xgvmiy9br7cr7rywiwr1sa1gc2mxdrlvqbm2nff") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.5.2 (c (n "simplelog") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1r992kgzlqffxq7lnyh7mkaqw4gh53bc5ymzi1zs1jglzlwjphcw") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.5.3 (c (n "simplelog") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "04wsvria53jd85smpkvcrjaf4m4nr4nrsickxjwdwnjx31gk959f") (f (quote (("default" "term"))))))

(define-public crate-simplelog-0.6.0 (c (n "simplelog") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1yx3hhybpqmyac2bd3j0892cx9f8m1bsbsnv1ls3ik96ba59975a") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.0 (c (n "simplelog") (v "0.7.0") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "1bnqm88yi0kmmib2aabcji9g2kny6z4zk62j2r3py6if5mv5xibr") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.1 (c (n "simplelog") (v "0.7.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "09z0c2jx6rw430rjik774llr4qbsv976712p43pfgk312248rgpb") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.2 (c (n "simplelog") (v "0.7.2") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "17x7fgndka0any41jr7wrbg9k2c33x2c6v40dlczxz332zavjkvj") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.3 (c (n "simplelog") (v "0.7.3") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "1dhlfn97sgqji1dy1w1zr573fwr1c8wswzyr1l0qw7psxfxilvib") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.4 (c (n "simplelog") (v "0.7.4") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "1i87w72skvzp4lhpwn233sfp9q9kday2z7ly1akb1bg6mh1y78q5") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.5 (c (n "simplelog") (v "0.7.5") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "0rylmiqi6wpjgmiwnl95fnsbaan5pqyb5pr1g51hv2cs6jbsrb5w") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.7.6 (c (n "simplelog") (v "0.7.6") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)))) (h "039zfayrjpdjj4g2jabjhr4n4hr9i2ibik9wdc37swffrh1a1y9w") (f (quote (("test") ("default" "term"))))))

(define-public crate-simplelog-0.8.0 (c (n "simplelog") (v "0.8.0") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "0g6dqa9pknk9viddf696wwrbhl0898ziv5jri128lac7h3skc9rb") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.9.0 (c (n "simplelog") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "087pr7n3z7myaxi0ay430bsjj6xddpwspz1ssi8v5a8lk3bgzh2b") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.10.0 (c (n "simplelog") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "1q21gia998q9mbv7avf2h84h1p9dzhidqhl0ln41rv8cd8qgxl2r") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.10.1 (c (n "simplelog") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "13hl8zw438kzdhkgh81mp60rx3gb147isnb36a5b5r5q1ynh3zpm") (f (quote (("test") ("default" "termcolor")))) (y #t)))

(define-public crate-simplelog-0.10.2 (c (n "simplelog") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "0ndr5qfj34crkxqsc31a9w5c2lvqn1pw70pf03pj0hhm8bk4ml45") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.11.0 (c (n "simplelog") (v "0.11.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "^1.5.7") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "1587mwd5q19g1kl1r89fvc9wlzzn177r8vy7jyb2s2gk4pg29alb") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.11.1 (c (n "simplelog") (v "0.11.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "^1.5.8") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "11h8q8lykc829f4bd5llw35p4qi9g6mp0mk1p281fq8qi48w1azc") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.11.2 (c (n "simplelog") (v "0.11.2") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "~1.5") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)))) (h "0700ssb3xbr5qy3n7wfrqwibdpdhmgd8nlz42q0wlwkg8mj82d61") (f (quote (("test") ("default" "termcolor"))))))

(define-public crate-simplelog-0.12.0-alpha1 (c (n "simplelog") (v "0.12.0-alpha1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "~1.5") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1ijzlj4myqa73cc7gsgyca4b1c7mx7v3hz82brb0d783mmdiw0ii") (f (quote (("test") ("local-offset" "time/local-offset") ("default" "termcolor" "local-offset"))))))

(define-public crate-simplelog-0.12.0 (c (n "simplelog") (v "0.12.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "~1.5") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "11m7f2djih4413fdjk9lkkhwxq7lsqf86z00bd4xsx6ym82gzps8") (f (quote (("test") ("local-offset" "time/local-offset") ("default" "termcolor" "local-offset"))))))

(define-public crate-simplelog-0.12.1 (c (n "simplelog") (v "0.12.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "~1.5") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0sa3hjdifxhcb9lnlg549fr2cc7vz89nygwbih2dbqsx3h20ivmc") (f (quote (("test") ("local-offset" "time/local-offset") ("default" "termcolor" "local-offset"))))))

(define-public crate-simplelog-0.12.2 (c (n "simplelog") (v "0.12.2") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)) (d (n "paris") (r "~1.5") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1h59cp84gwdmbxiljq6qmqq1x3lv9ikc1gb32f5ya7pgzbdpl98n") (f (quote (("test") ("local-offset" "time/local-offset") ("default" "termcolor" "local-offset"))))))

