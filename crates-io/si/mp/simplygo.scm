(define-module (crates-io si mp simplygo) #:use-module (crates-io))

(define-public crate-simplygo-0.1.0 (c (n "simplygo") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (f (quote ("color" "color-auto"))) (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.26") (f (quote ("serde" "std"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j124jpxm6i7ymzv4m6ljbpb3agxvphndy2vd5vq90nsm7fapms6")))

