(define-module (crates-io si mp simple_hasher) #:use-module (crates-io))

(define-public crate-simple_hasher-0.0.1 (c (n "simple_hasher") (v "0.0.1") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "01r2ndlgv3qlzpa4wq53amxvyjlqzj6in7rkjqpv03rzbf6frr06")))

(define-public crate-simple_hasher-0.1.0 (c (n "simple_hasher") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "00xd39hjginmf2ay5jkgjna6pwb6s8yl70vf11cs0g31522pvf9q")))

(define-public crate-simple_hasher-1.0.0-alpha (c (n "simple_hasher") (v "1.0.0-alpha") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "03j9yxgx1xray0wkb6ak7ik6vxa6wf16igx9xj495q8vjhwpzvj7")))

(define-public crate-simple_hasher-1.0.0-alpha.1 (c (n "simple_hasher") (v "1.0.0-alpha.1") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "036kdfsw4xwyl9xvjxfkrrp5izi157w40am76svvki228wwzdhnz")))

(define-public crate-simple_hasher-1.0.0-rc (c (n "simple_hasher") (v "1.0.0-rc") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1spd2mzj0d8sy2fm3hfyij74kgh6byb6xllvk8yzlr9fywmg7w3p")))

(define-public crate-simple_hasher-1.0.0 (c (n "simple_hasher") (v "1.0.0") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "09s5a91p54gq27mjj6d99j8k5b8adaajm4aifkhw22wd53bxilz1")))

(define-public crate-simple_hasher-1.0.1 (c (n "simple_hasher") (v "1.0.1") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "127bxjjffpb9g2r7hl2nwvr5qknjiql9kcpqh049ak74krdpisk1")))

