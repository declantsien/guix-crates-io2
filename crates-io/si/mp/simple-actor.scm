(define-module (crates-io si mp simple-actor) #:use-module (crates-io))

(define-public crate-simple-actor-0.1.0 (c (n "simple-actor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0i1lg5idbccsnazwrnql19krnsm003qmqynl6vsr5bfdg990mhzz")))

(define-public crate-simple-actor-0.1.1 (c (n "simple-actor") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0n0ar4a1v0p4mx1f7lh6jhzy5nkh7n47k4b5d4scsxmfjiglgy3q")))

(define-public crate-simple-actor-0.1.2 (c (n "simple-actor") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0lqg3rskdll2l9lmlhk9bwvk6fcb6jqd3idmpin6g2dd5jaqs2na")))

(define-public crate-simple-actor-0.2.0 (c (n "simple-actor") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1n6kfpv2l47px68h5ija89w55cw08f2rmwy06xr5w2qqy0ky2p7s")))

(define-public crate-simple-actor-0.3.0 (c (n "simple-actor") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "19csk7ilx7k4gx57aw9n6crpdyv5wyam2gfb7435ds7avyr3pfa7")))

(define-public crate-simple-actor-0.4.0 (c (n "simple-actor") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1xlddys61yvia07fxmfr39wjpa4m1mdr5jmcvnm25lbn1qbcgx13")))

(define-public crate-simple-actor-0.4.1 (c (n "simple-actor") (v "0.4.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0icdpcwjqb8gp9gjb5mfchsx31fa7h21m1sshrybf03spmg25w9y")))

