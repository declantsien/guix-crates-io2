(define-module (crates-io si mp simple-bmp) #:use-module (crates-io))

(define-public crate-simple-bmp-0.1.0 (c (n "simple-bmp") (v "0.1.0") (h "00p4dc79fbx0s28261a5drcba28zhrn4hyr6y4l884grir6v9my9")))

(define-public crate-simple-bmp-0.2.0 (c (n "simple-bmp") (v "0.2.0") (h "1ypf7b65v3y8mxa2bb7zcpc92m7ypws9iblawjks470h26771vk4")))

(define-public crate-simple-bmp-0.2.1 (c (n "simple-bmp") (v "0.2.1") (h "1r0d18hjcrg0rgdq7rh7f5c1zgm2cds7bq2bs0dsb8mpwmvdr639")))

