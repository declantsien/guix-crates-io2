(define-module (crates-io si mp simple-file-rotation) #:use-module (crates-io))

(define-public crate-simple-file-rotation-0.1.0 (c (n "simple-file-rotation") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)))) (h "00j19l0lvzkrj9mqy9y6q5fz2bpn20bb2q85hbhr520a599zw6gf") (y #t)))

(define-public crate-simple-file-rotation-0.2.0 (c (n "simple-file-rotation") (v "0.2.0") (h "1fcd0pl5jas20f25616cgxhf4pz1q7hc3v1gh8wn8fxi33nj90hn")))

(define-public crate-simple-file-rotation-0.2.1 (c (n "simple-file-rotation") (v "0.2.1") (h "1v118q2kj4cbmcv76ym5iqabbsdhx85an1mry6pk28dqmkcsdsq6")))

(define-public crate-simple-file-rotation-0.3.0 (c (n "simple-file-rotation") (v "0.3.0") (h "0zndjjrqc35ylvw84rq0kh1y36jqhcba26i3mlj3ajm5gfl1bnlk")))

(define-public crate-simple-file-rotation-0.3.1 (c (n "simple-file-rotation") (v "0.3.1") (h "03kl6d8y34phw81hramw0zfwy4zqbnkwl6k5gy5ikssg39lsq3fl")))

(define-public crate-simple-file-rotation-0.3.2 (c (n "simple-file-rotation") (v "0.3.2") (h "18nb58a99h10yn6wh7xdzjcnwzk9d65asaj945dm4byrdi39ny65")))

(define-public crate-simple-file-rotation-0.3.3 (c (n "simple-file-rotation") (v "0.3.3") (h "1n1s4pds85g716h09fgs3r90zjwhb0c0cbb00nq4v6jbhbdlj3gm")))

(define-public crate-simple-file-rotation-0.3.4 (c (n "simple-file-rotation") (v "0.3.4") (h "0x3v3w2j76xgi0rhvflwwb0c81sf9n169mcn7j7svxzm5la1n1fn")))

