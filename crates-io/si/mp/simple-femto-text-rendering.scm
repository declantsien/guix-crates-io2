(define-module (crates-io si mp simple-femto-text-rendering) #:use-module (crates-io))

(define-public crate-simple-femto-text-rendering-0.1.0 (c (n "simple-femto-text-rendering") (v "0.1.0") (d (list (d (n "femtovg") (r "^0.3") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)))) (h "1rf8fdy469dbqa3ba4hpkcjdrzlx6bh2va7p7s51ph5lvmya3fhb")))

(define-public crate-simple-femto-text-rendering-0.2.0 (c (n "simple-femto-text-rendering") (v "0.2.0") (d (list (d (n "femtovg") (r ">=0.4, <=0.6") (d #t) (k 0)))) (h "18q8h3ya46ayhx0qrrncik3ggz1vsjw7i4a89b8yrrk3k8bly7nn")))

