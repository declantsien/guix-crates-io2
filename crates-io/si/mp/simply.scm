(define-module (crates-io si mp simply) #:use-module (crates-io))

(define-public crate-simply-0.1.0 (c (n "simply") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1rq3cc49nf3x83131r5x4bvcvxarwv3yk8zf5wg943sn6px22vbl")))

(define-public crate-simply-0.2.0 (c (n "simply") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1w9jjg6m0br9cvrwpcmkrhmpxznb23rjn12j8iarx8hcryfr33d0")))

(define-public crate-simply-0.3.0 (c (n "simply") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1bapx836x42a5sy00hcg80x39mbad1xyx58ghs7l028fjfvxnfrc")))

(define-public crate-simply-0.3.1 (c (n "simply") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "16payr1940rawvxx80vm4g2x3zj455grp0fk7iprl49y16hzgw73")))

(define-public crate-simply-0.4.0 (c (n "simply") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0gwqvlccqihjvgkzh46pys6c9jzk77vb3kd5iyakycr9lvlpcs8c")))

