(define-module (crates-io si mp simple_parallel) #:use-module (crates-io))

(define-public crate-simple_parallel-0.0.1 (c (n "simple_parallel") (v "0.0.1") (h "1gxxr8p0w8r319r003p77vbnkgc5b6i8v032l43zv7rw9dj9af0y")))

(define-public crate-simple_parallel-0.1.0 (c (n "simple_parallel") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "0174im1c1rzx592mflkizb10vllzsm4h9s5drfypms51p03sx8a9")))

(define-public crate-simple_parallel-0.1.1 (c (n "simple_parallel") (v "0.1.1") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "1qs6saivkrf0bhqvnqdh1lbiygfih8mym9l647kh50mqf3fpjxa5")))

(define-public crate-simple_parallel-0.1.2 (c (n "simple_parallel") (v "0.1.2") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "135m86hw7cclwfjbl6vf4nvp7n4r7r9v8lmzpdi5llfkxnbrzkm9")))

(define-public crate-simple_parallel-0.1.3 (c (n "simple_parallel") (v "0.1.3") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "1x429aqav839p6lf90xvhbqc4n4kpl7yzgsbi9ifpd3pc6alv2vh")))

(define-public crate-simple_parallel-0.2.0 (c (n "simple_parallel") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "num_cpus") (r "^0.2") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "1y5ngjk69gqiq7cb7rn1cyiimskdv67yg43pfjzn5jyayjiw7mj4") (f (quote (("unstable"))))))

(define-public crate-simple_parallel-0.2.1 (c (n "simple_parallel") (v "0.2.1") (d (list (d (n "num") (r "*") (d #t) (k 2)) (d (n "num_cpus") (r "^0.2") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "0nim76wm8h6312ilc5kq1nlgc0y8bp85w225rar64lybajwdnbq0") (f (quote (("unstable"))))))

(define-public crate-simple_parallel-0.3.0 (c (n "simple_parallel") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)) (d (n "num_cpus") (r "^0.2") (d #t) (k 2)) (d (n "strided") (r "*") (d #t) (k 2)))) (h "0qqwpiic5v0gl3lwj4q6fgqk0pmz6wk2qrk2zyh2fd7ywj0qhbip") (f (quote (("unstable"))))))

