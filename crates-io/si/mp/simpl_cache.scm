(define-module (crates-io si mp simpl_cache) #:use-module (crates-io))

(define-public crate-simpl_cache-1.0.0-beta (c (n "simpl_cache") (v "1.0.0-beta") (d (list (d (n "simple_cache_macros") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0vvzyv43vnmhf3p2xd1549vhj64llhv00zmrday36gfj3x22lzk6")))

(define-public crate-simpl_cache-2.0.0-beta (c (n "simpl_cache") (v "2.0.0-beta") (d (list (d (n "simple_cache_macros") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1x38lc2s31x91bjhqma4hpwbp6sbk8db6x1djnawh6q2happaj5q")))

(define-public crate-simpl_cache-2.0.1-beta (c (n "simpl_cache") (v "2.0.1-beta") (d (list (d (n "simple_cache_macros") (r "^2.0.0-beta") (d #t) (k 0)))) (h "19i3flxs88nj884bxzgbsqijlnbic4kbpbjwxm1a9z2p0r45xrjw")))

(define-public crate-simpl_cache-2.1.0-beta (c (n "simpl_cache") (v "2.1.0-beta") (d (list (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.1.0-beta") (d #t) (k 0)))) (h "0fn2rg74wlggq0d5n5w233klp0r6w49s1gi6531r57x0pfb4ynnk")))

(define-public crate-simpl_cache-2.1.1-beta (c (n "simpl_cache") (v "2.1.1-beta") (d (list (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.1.0-beta") (d #t) (k 0)))) (h "1c09q48rkili8v5m1bq6in11gi02p5w5xjl498w4qsvk2q3n16dj")))

(define-public crate-simpl_cache-2.2.0-beta (c (n "simpl_cache") (v "2.2.0-beta") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.1.0-beta") (d #t) (k 0)))) (h "0228x9lj9pqkj6l2fglgb62351y89hv11hn8r2r576dimwrcfjfn")))

(define-public crate-simpl_cache-2.3.0-beta (c (n "simpl_cache") (v "2.3.0-beta") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.3.0-beta") (d #t) (k 0)))) (h "1mkmlj09k6qil887nhmm5hfvg52l08594i48gbyvndamjj7478rs")))

(define-public crate-simpl_cache-2.4.0-beta (c (n "simpl_cache") (v "2.4.0-beta") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.4.0-beta") (d #t) (k 0)))) (h "0iqi2y2x7x2hr7ag8szv1c6017hv6zxsyx7rx5m9ipf1bw1njyfl")))

(define-public crate-simpl_cache-2.4.1-beta (c (n "simpl_cache") (v "2.4.1-beta") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "simple_cache_core") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "simple_cache_macros") (r "^2.4.1-beta") (d #t) (k 0)))) (h "0sybyj2s6n3i18x96nbkwl6nsg699wrlywhrg4akny34jipljnss")))

