(define-module (crates-io si mp simple_arguments) #:use-module (crates-io))

(define-public crate-simple_arguments-0.1.0 (c (n "simple_arguments") (v "0.1.0") (h "1886fbkvac0vh152xc1npl8kb26mgwhway0yqrq69ykx3w25cgbd")))

(define-public crate-simple_arguments-0.1.1 (c (n "simple_arguments") (v "0.1.1") (h "1ifg559ydlpks12zlg1lmayzjr8p87c8ziav285r8v6hqgjgdnid")))

