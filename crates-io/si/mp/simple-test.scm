(define-module (crates-io si mp simple-test) #:use-module (crates-io))

(define-public crate-simple-test-0.3.1 (c (n "simple-test") (v "0.3.1") (d (list (d (n "cfgrammar") (r "^0.12") (d #t) (k 0)) (d (n "cfgrammar") (r "^0.12") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lrlex") (r "^0.12.0") (d #t) (k 0)) (d (n "lrlex") (r "^0.12.0") (d #t) (k 1)) (d (n "lrpar") (r "^0.12.0") (d #t) (k 0)) (d (n "lrpar") (r "^0.12.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14vbg36lfi50grnp9m968m3xlq7bxygi6hv1niq1vq65rp42mqrg") (y #t)))

