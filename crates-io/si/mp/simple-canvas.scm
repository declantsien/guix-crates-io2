(define-module (crates-io si mp simple-canvas) #:use-module (crates-io))

(define-public crate-simple-canvas-0.1.0 (c (n "simple-canvas") (v "0.1.0") (h "0ljip7aznkv38wpfbs0jal696znd2sfigryffga8aqvwwjvdyvb9")))

(define-public crate-simple-canvas-0.1.1 (c (n "simple-canvas") (v "0.1.1") (h "15g05f0x212nar7acrf78ks425y0gz1x1gkmz7x5rzf425r14b59")))

