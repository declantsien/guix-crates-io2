(define-module (crates-io si mp simple-cursor) #:use-module (crates-io))

(define-public crate-simple-cursor-0.1.0 (c (n "simple-cursor") (v "0.1.0") (h "03gvcg0kbf59hs0phpr3l1bxwngqm0g0v24mh6vrkdhng22nz1yy")))

(define-public crate-simple-cursor-0.1.1 (c (n "simple-cursor") (v "0.1.1") (h "08c79dhxk3glyip86l4r83d06fad7zf6q30gk52riz14wj9d3k72")))

