(define-module (crates-io si mp simpleml_macro) #:use-module (crates-io))

(define-public crate-simpleml_macro-1.0.0 (c (n "simpleml_macro") (v "1.0.0") (d (list (d (n "simpleml") (r "^1.0.0") (d #t) (k 0)) (d (n "tree_iterators_rs") (r "^1.2.1") (d #t) (k 0)))) (h "138gan99ph50hzna6fmk5qpylw5n238ix991qdp3sr0g70v2dcq6")))

(define-public crate-simpleml_macro-1.0.1 (c (n "simpleml_macro") (v "1.0.1") (d (list (d (n "simpleml") (r "^1.0.1") (d #t) (k 0)) (d (n "tree_iterators_rs") (r "^1.2.1") (d #t) (k 0)))) (h "0zlgglvd67bnz2j8kpj9irv3k9rlzn2f5aqpmka9cls5kdfkx7fm")))

