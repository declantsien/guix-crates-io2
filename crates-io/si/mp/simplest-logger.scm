(define-module (crates-io si mp simplest-logger) #:use-module (crates-io))

(define-public crate-simplest-logger-1.0.0 (c (n "simplest-logger") (v "1.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0s6f08xf85b4l0rpp2128vxij3hd71wbfmdc1jxhxicjfbaznc58") (y #t)))

(define-public crate-simplest-logger-0.2.0 (c (n "simplest-logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1xzzy85c9iydm6lg96zlylnmr18rdi83vmwkkn94by3crmmdfpgh") (y #t)))

