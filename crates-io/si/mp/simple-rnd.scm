(define-module (crates-io si mp simple-rnd) #:use-module (crates-io))

(define-public crate-simple-rnd-0.9.0 (c (n "simple-rnd") (v "0.9.0") (h "0r6qv5grj7qj2prfp2yahhqw18l50zn0ii87pblkjjyl30yhfipc")))

(define-public crate-simple-rnd-0.9.1 (c (n "simple-rnd") (v "0.9.1") (h "1rhqirfh9r5agalwyh5bxfas4vymh4nc03jkxvrfz87yp8dgn6xa")))

