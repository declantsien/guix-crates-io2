(define-module (crates-io si mp simplify) #:use-module (crates-io))

(define-public crate-simplify-0.1.0 (c (n "simplify") (v "0.1.0") (h "1fx3jk1yj5r6bgvhx4fv8ilmk3jw8n0dzklc32i4y0g1ygswl4af")))

(define-public crate-simplify-0.1.1 (c (n "simplify") (v "0.1.1") (h "1csgym43mgrqpgk9l21bllx0g01kl4sc4irc2s691n5x4sk7kwjk")))

(define-public crate-simplify-0.1.2 (c (n "simplify") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "0iqlbb5yvwcnk4jqn61pjlfildyp0y2g80i5gjk87mg1gsj2y804")))

