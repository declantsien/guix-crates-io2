(define-module (crates-io si mp simple-ssdp-client) #:use-module (crates-io))

(define-public crate-simple-ssdp-client-0.1.0 (c (n "simple-ssdp-client") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "07p130ibfxrz4knpkckyjqwmkffk59nbic7qv0d2l4n5f45vji99") (y #t)))

(define-public crate-simple-ssdp-client-0.1.1 (c (n "simple-ssdp-client") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0bips1xi8xhpxdh3g87xwmkg70ynk2v3llb6hwk4wccmd4zn30gd")))

(define-public crate-simple-ssdp-client-0.1.2 (c (n "simple-ssdp-client") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1h97k0nckd6l1gxxkar6lx0az4lwvhvpp4hyrwrcdjpipy6k3gkc")))

(define-public crate-simple-ssdp-client-0.1.3 (c (n "simple-ssdp-client") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1g49kbyydn5p3mrsxgvk9ac8dpgkfblzihs00kv55h851p9n62m6")))

(define-public crate-simple-ssdp-client-0.1.4 (c (n "simple-ssdp-client") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0vj2chjmf9j640msqcb6498ls0wm9mp0j2imxwf0lvda0vwsy0i5")))

(define-public crate-simple-ssdp-client-0.1.5 (c (n "simple-ssdp-client") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0fmkm9y0p3j7099b8b3qbp61xfgdvnz3zlmd2x6hibaksvwvij3i")))

