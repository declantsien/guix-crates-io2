(define-module (crates-io si mp simple_xml_serialize_macro) #:use-module (crates-io))

(define-public crate-simple_xml_serialize_macro-0.1.0 (c (n "simple_xml_serialize_macro") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nm5x1qmvgb09v9sahk4h3bphj6fqvhfs2svz87lfjp32lrnb412")))

(define-public crate-simple_xml_serialize_macro-0.2.0 (c (n "simple_xml_serialize_macro") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.2.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19mafxzasqalxfba0fpdw7a2m003ij6h85hh9amd3h60ddx2b60v") (f (quote (("process_options"))))))

(define-public crate-simple_xml_serialize_macro-0.2.1 (c (n "simple_xml_serialize_macro") (v "0.2.1") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.2.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "161hpwlmsfmnn9r0s5ifsyyxpb56bgczdgfjlgpy9najp61q1j4n") (f (quote (("process_options"))))))

(define-public crate-simple_xml_serialize_macro-0.3.0 (c (n "simple_xml_serialize_macro") (v "0.3.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qfnk4dap984qcc8r2p4aljddmw4gvc50zirm25v8hxv57av5dcv") (f (quote (("process_options"))))))

