(define-module (crates-io si mp simple-pcf) #:use-module (crates-io))

(define-public crate-simple-pcf-0.1.0 (c (n "simple-pcf") (v "0.1.0") (h "1jpqbs543a0ys0cwd53m3avjz1pfiw293j9a5p4r1cwac6pd7q8w")))

(define-public crate-simple-pcf-0.1.1 (c (n "simple-pcf") (v "0.1.1") (h "05abryn2n0s0palf52dxff6amv00qb8c1r6x7m907mv8dkizhv86")))

