(define-module (crates-io si mp simplelog-config) #:use-module (crates-io))

(define-public crate-simplelog-config-0.1.0 (c (n "simplelog-config") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "08j5vqgrn8v3lzns8bswy9rrg66ms806bsvaiw52gpq7r2xj85mq")))

