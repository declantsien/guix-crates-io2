(define-module (crates-io si mp simple-notion) #:use-module (crates-io))

(define-public crate-simple-notion-0.1.0 (c (n "simple-notion") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikv668a200rxwwfi1vphmivg5jvzlj84al6sw9a541yhhdzalqw")))

(define-public crate-simple-notion-0.1.1 (c (n "simple-notion") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mgzd13pxvif8ydxf2f1n6s4sz83xxahf47wm0kwqp9r6xq6nk4q")))

