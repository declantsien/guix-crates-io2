(define-module (crates-io si mp simple-stream) #:use-module (crates-io))

(define-public crate-simple-stream-0.1.0 (c (n "simple-stream") (v "0.1.0") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1h9igbd5hizm2k2b03bk82d0idzpk7pr1y8512r9yn4c9kiv5142")))

(define-public crate-simple-stream-0.2.0 (c (n "simple-stream") (v "0.2.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)))) (h "16r60wck6q83cl29hj4y2d0ifar5va0j3674z8wnkxrrb59yig70")))

(define-public crate-simple-stream-0.2.1 (c (n "simple-stream") (v "0.2.1") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)))) (h "0dixrrv1qdrhcwjl5kb5agl1wi83ic91w9rg1wnsd12bjrv0d3db")))

(define-public crate-simple-stream-0.2.2 (c (n "simple-stream") (v "0.2.2") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)))) (h "0vr87spg65bislalxp5cxy2fczdb69bbcfacd7vp3irjy61fv36s")))

(define-public crate-simple-stream-0.2.3 (c (n "simple-stream") (v "0.2.3") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)))) (h "1gafc1zlfx5pf5z8bsz78v2n80k0lik8vaglxrn66sdb9ixqrqy4")))

(define-public crate-simple-stream-0.2.4 (c (n "simple-stream") (v "0.2.4") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "0ir3gmx1l0bz11iyrrr4rhmnjrmsxwfjfdfsc54xwx92z14nv3nm")))

(define-public crate-simple-stream-0.2.5 (c (n "simple-stream") (v "0.2.5") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "1danxcm2hfw65rk7hwxhci0fr1d0bblnjfayvfr6sj4770yn3c4l")))

(define-public crate-simple-stream-0.2.6 (c (n "simple-stream") (v "0.2.6") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "0jz4x75f4vqakp83gpb8zh7yjkcv6qwxh96hq79walyaabi6ay0g")))

(define-public crate-simple-stream-0.2.7 (c (n "simple-stream") (v "0.2.7") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "1p500glx85scyg74p1r6c8cv22v6dcbwj4k2c0idghkmzf46av1r")))

(define-public crate-simple-stream-0.2.9 (c (n "simple-stream") (v "0.2.9") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "0x2f16bn63145wyc4wckrfzqw93bra16ql1q38273lgfjxqir3m1")))

(define-public crate-simple-stream-0.3.1 (c (n "simple-stream") (v "0.3.1") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.8") (d #t) (k 0)))) (h "1557ih80anpb6c2adj89gvj5h6psh96avxkp320hz6izzar872gw")))

(define-public crate-simple-stream-0.3.2 (c (n "simple-stream") (v "0.3.2") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.8") (d #t) (k 0)))) (h "0369ibrijs5sx9zrxc4g90jd5xzmrrxpk6aw7bcd74q34yx20myl")))

(define-public crate-simple-stream-0.3.3 (c (n "simple-stream") (v "0.3.3") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.8") (d #t) (k 0)))) (h "1007jhc6bi9qjmcwxxx8fy7kzmrmwr0di72fjd6i368giqww97rs")))

(define-public crate-simple-stream-0.3.4 (c (n "simple-stream") (v "0.3.4") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.8") (d #t) (k 0)))) (h "1vw5dvm59vwryc8gql7q9wmwskki26lg8gmbxkff6h98ia3z7yy9")))

(define-public crate-simple-stream-0.3.5 (c (n "simple-stream") (v "0.3.5") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.8") (d #t) (k 0)))) (h "1r102c2n3p3azyv2kjcxz6j5v8xs2xj8cnxdsqiqi6m6an9hgm4v")))

(define-public crate-simple-stream-0.4.0 (c (n "simple-stream") (v "0.4.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1izjdwpv6xaly29r05rk8xh7105r9k5hby6kwa6mk0f3kkm07i8s")))

(define-public crate-simple-stream-0.5.0 (c (n "simple-stream") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "0cddh3nad7wm7yxd698ff6csafydnhdf9m24spi4vm81bmgbz3xm")))

(define-public crate-simple-stream-0.5.1 (c (n "simple-stream") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "12gfpqv53nph7pi4s69832qkdp1icaasxv4659xxbj7znx4aklk7")))

(define-public crate-simple-stream-0.5.2 (c (n "simple-stream") (v "0.5.2") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "0r4msxxdvassz5zcbwdal3qivd203blp02fwwi51klgacqivzm1s")))

(define-public crate-simple-stream-0.5.3 (c (n "simple-stream") (v "0.5.3") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "0r740ixqnhr0085mrijqw6qqrg2ikjfxxr4fa1ds3lbscsn3bbk9")))

(define-public crate-simple-stream-0.5.4 (c (n "simple-stream") (v "0.5.4") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1qmv8vwiypvznca1qrzdnj48w0qw3lxfdmzzxcjcw9lba0cb76kk")))

(define-public crate-simple-stream-0.6.0 (c (n "simple-stream") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "0vfh4blbry9nrz35mpcxz99gnl2bkzd2rfzyjjdhp6h1vzv3dxy1")))

(define-public crate-simple-stream-0.6.1 (c (n "simple-stream") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1qr5zxg745kmszn2mjqb78dpzil3ymdl4vx8xgpqkm45i53vb546")))

(define-public crate-simple-stream-0.7.0 (c (n "simple-stream") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "0lmsdlnfb6krm0pa508y42i2j337vmarlakvq7ijwl5fnamh6x71")))

(define-public crate-simple-stream-0.8.0 (c (n "simple-stream") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "157va2k2x14qgxr9l2lv586q6vfjpa2hqy9yr6r165g3d194yy1n")))

(define-public crate-simple-stream-0.9.0 (c (n "simple-stream") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "07idk253rwvpc30anccc7i1zrvl0ghamcyd9r1d0q17wl4crb4d6")))

(define-public crate-simple-stream-0.9.1 (c (n "simple-stream") (v "0.9.1") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1p9s77x7sqady0vxl53j3a703762w0r1syw8c7561fdi4fli7h9a")))

(define-public crate-simple-stream-0.9.2 (c (n "simple-stream") (v "0.9.2") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "07xjz7nfv7x8zkam1r1jcmcxjb28np4yy2kic9ikm3irqkp3d523")))

(define-public crate-simple-stream-0.9.3 (c (n "simple-stream") (v "0.9.3") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1nsv4a8cll1689gp22l34fk4n7f5zxp8rz5p3w0k6xxx6vy1m6i6")))

(define-public crate-simple-stream-0.9.5 (c (n "simple-stream") (v "0.9.5") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1m2ryqm15xa84b98zvn426yc95lwr8wc7vpfynzdnypz0lq7x4s8")))

(define-public crate-simple-stream-0.9.6 (c (n "simple-stream") (v "0.9.6") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "openssl") (r "^0.7.10") (d #t) (k 0)))) (h "1clz980svc6akjc9cz6rqxvw8gd35h3qziv93iyzagx8jvppbijq")))

(define-public crate-simple-stream-0.10.0 (c (n "simple-stream") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)))) (h "1yn1bh7lxrfnwklcvkyy1prxs0dq2bmryxq9n8mbvr66bjv8faqj")))

(define-public crate-simple-stream-0.10.1 (c (n "simple-stream") (v "0.10.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)))) (h "14bk8q4s0prm1f80s871cd700bbgb4gwza93sba7f0c55kiqrq9v")))

(define-public crate-simple-stream-0.11.0 (c (n "simple-stream") (v "0.11.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0az4fsph2f82bz2nilc3fk1cvvc00rgnf3mfnf3zndxhfaxmgnh5") (f (quote (("default" "openssl"))))))

