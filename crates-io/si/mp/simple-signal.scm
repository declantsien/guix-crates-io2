(define-module (crates-io si mp simple-signal) #:use-module (crates-io))

(define-public crate-simple-signal-1.0.3 (c (n "simple-signal") (v "1.0.3") (d (list (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v3s1agdaw7j65ld5qd5nx694bli1q1n60q6npaamv1g10pi8v5a") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

(define-public crate-simple-signal-1.0.4 (c (n "simple-signal") (v "1.0.4") (d (list (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "012zrcwhl61a5x65zi6i10n770ip52pm7rb8bxljxrzr2gxdynni") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

(define-public crate-simple-signal-1.0.5 (c (n "simple-signal") (v "1.0.5") (d (list (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0albfjswccr7192c04s42wnicmskcpfsl58v9j2ls6n3vf21zdax") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

(define-public crate-simple-signal-1.0.6 (c (n "simple-signal") (v "1.0.6") (d (list (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sswcw6f3ag9y0gkijqlllqbfgz970afck8hw0b05c32v8fj0wr8") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

(define-public crate-simple-signal-1.1.0 (c (n "simple-signal") (v "1.1.0") (d (list (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11v6qlyzap0zcgi1lmrpb574j4n8x8w31rw44vjvjbfiqah03sy1") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

(define-public crate-simple-signal-1.1.1 (c (n "simple-signal") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12r82dpipdkkfmslp04pd3b2fpr9h4zxjfs8axynchncmm2dmxsk") (f (quote (("stable" "lazy_static") ("nightly") ("default" "stable"))))))

