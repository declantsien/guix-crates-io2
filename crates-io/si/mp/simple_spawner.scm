(define-module (crates-io si mp simple_spawner) #:use-module (crates-io))

(define-public crate-simple_spawner-0.3.0-alpha.17 (c (n "simple_spawner") (v "0.3.0-alpha.17") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1bmd31wpq77lhvpg8ij9zp6rf2n24d6jrikl0rqcpvrq0wgfl3xq")))

