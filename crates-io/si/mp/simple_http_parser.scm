(define-module (crates-io si mp simple_http_parser) #:use-module (crates-io))

(define-public crate-simple_http_parser-0.2.0 (c (n "simple_http_parser") (v "0.2.0") (h "10c72gb0njl85h0n7krmddqjg8kpyj955xg2qx86f05lnck6jzh4")))

(define-public crate-simple_http_parser-0.2.1 (c (n "simple_http_parser") (v "0.2.1") (h "124cscq9msiv92l0brz16b8cggqs8z7rhnp5z2w6w27fghwcgl2w")))

(define-public crate-simple_http_parser-0.2.2 (c (n "simple_http_parser") (v "0.2.2") (h "0djp4gb7dzs2vd227ip4fbbp6gkh7ih3y0zy2pffgswn127x5ibg")))

(define-public crate-simple_http_parser-0.2.3 (c (n "simple_http_parser") (v "0.2.3") (h "13pjq9c8fvlr1xykffc5vkfmyns7lnisziag01l7lzfbibhqmm3y")))

(define-public crate-simple_http_parser-0.2.4 (c (n "simple_http_parser") (v "0.2.4") (h "1pwv0bfl5g2bzkg5wsrvl1z54z2xfmg4qaap4vrni3mi9z45msc7")))

(define-public crate-simple_http_parser-0.2.5 (c (n "simple_http_parser") (v "0.2.5") (h "13kn76qlfkzz7w8jlf60qvfmxi6wwiza1j1agb9l03j5g7dg83p3")))

(define-public crate-simple_http_parser-0.3.0 (c (n "simple_http_parser") (v "0.3.0") (h "1jyrvq50bq9lzh1b5wjc246czb6w1bmkp24sszyfix4bz45y3qlp")))

(define-public crate-simple_http_parser-0.3.1 (c (n "simple_http_parser") (v "0.3.1") (h "1sm513zh0y00q4gj19xdd0y89na4468jjskwwff9phb0zsdhy8ah")))

(define-public crate-simple_http_parser-0.3.2 (c (n "simple_http_parser") (v "0.3.2") (h "0x71g694ji616hwz0vbhxilaaxs0phg4x31bg5krklv2cvvbh3a7")))

(define-public crate-simple_http_parser-0.3.3 (c (n "simple_http_parser") (v "0.3.3") (h "13plsvar3x8gwrac8dwl6q0zll2xyqax3cshnvx800lz20fkdmzr")))

(define-public crate-simple_http_parser-0.3.4 (c (n "simple_http_parser") (v "0.3.4") (h "09f4gb0ss9g3xr3q7xgj92azzbh7lgfp9i8baag3lq92xgb1myr9")))

(define-public crate-simple_http_parser-0.3.5 (c (n "simple_http_parser") (v "0.3.5") (h "0p7p0blg77ag8kzhrfmdzaswxhdjl7xavidni7snrsb6adl8qa25")))

