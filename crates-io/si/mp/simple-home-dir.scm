(define-module (crates-io si mp simple-home-dir) #:use-module (crates-io))

(define-public crate-simple-home-dir-0.1.0 (c (n "simple-home-dir") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1dln3p9s7nczaf32v3hh0vvw66nnrdc8v0adh13naa0hv5chjg10")))

(define-public crate-simple-home-dir-0.1.1 (c (n "simple-home-dir") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1h3z0s0flashfbmi5ji11yl6a8jnkgaipsk2gnc3xc7v8af4f7n3") (y #t)))

(define-public crate-simple-home-dir-0.1.2 (c (n "simple-home-dir") (v "0.1.2") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "0p8hz3vckmzisaydfd323pa9y79rsjkgsflm41h6qnpkxrad7jm8") (f (quote (("expand_tilde")))) (y #t)))

(define-public crate-simple-home-dir-0.1.3 (c (n "simple-home-dir") (v "0.1.3") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "04lg7lr0kc80vqwh5i9lkyasxdwwg77ypjz3k66m3ljw77cjq3xs") (f (quote (("expand_tilde")))) (y #t)))

(define-public crate-simple-home-dir-0.1.4 (c (n "simple-home-dir") (v "0.1.4") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "09gd4kkqyag5j4rpm0g7c7l8z9paaiy3qfh53cs5xjrvmgz0vlr3") (f (quote (("expand_tilde"))))))

(define-public crate-simple-home-dir-0.1.5 (c (n "simple-home-dir") (v "0.1.5") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1r2kx87ygqvscgsl94k3808rdw6105bys4cpszgrqn3gdq9rb222") (f (quote (("test" "dirs" "expand_tilde") ("expand_tilde")))) (y #t)))

(define-public crate-simple-home-dir-0.1.6 (c (n "simple-home-dir") (v "0.1.6") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1ph9gv5f046mpsqwv07a1417kak0g6kj63xck1k3s92vmms54mfl") (f (quote (("test" "dirs" "expand_tilde") ("expand_tilde")))) (y #t)))

(define-public crate-simple-home-dir-0.1.7 (c (n "simple-home-dir") (v "0.1.7") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1x0s9bgyhm9nafcy2wapnagacb1zdkp7g6qbp30n4m3lvccib847") (f (quote (("test" "dirs" "expand_tilde") ("expand_tilde")))) (y #t)))

(define-public crate-simple-home-dir-0.2.0 (c (n "simple-home-dir") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Com_CallObj" "Win32_Foundation" "Win32_Globalization" "Win32_UI_Shell_Common"))) (t "cfg(windows)") (k 0)))) (h "1bnskz0j535kprj4ak7rf3s5f0nwg72bnmy0j45vaq44ybccp7sh") (f (quote (("test" "dirs" "expand_tilde" "home_dir") ("home_dir") ("expand_tilde") ("default" "home_dir"))))))

(define-public crate-simple-home-dir-0.2.1 (c (n "simple-home-dir") (v "0.2.1") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (t "cfg(windows)") (k 0)))) (h "06bn549q2284f5p8zpkh37v6xvj3q1y6n11yimx937c36vxvyj12") (f (quote (("test" "dirs" "expand_tilde" "home_dir") ("home_dir") ("expand_tilde") ("default" "home_dir"))))))

(define-public crate-simple-home-dir-0.2.2 (c (n "simple-home-dir") (v "0.2.2") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (t "cfg(windows)") (k 0)))) (h "15xgrmgs471cmvxnfr50plvlxhd43j9a4za9zfrja2821binwrlx") (f (quote (("test" "dirs" "expand_tilde") ("home_dir") ("expand_tilde" "home_dir") ("default" "home_dir")))) (y #t)))

(define-public crate-simple-home-dir-0.2.3 (c (n "simple-home-dir") (v "0.2.3") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (t "cfg(windows)") (k 0)))) (h "1xprzh48l6klv944jrw7i5swa1cr2p3f21adb1qx1pm15nzv31fl") (f (quote (("test" "dirs" "expand_tilde") ("home_dir") ("expand_tilde" "home_dir") ("default" "home_dir"))))))

(define-public crate-simple-home-dir-0.3.0 (c (n "simple-home-dir") (v "0.3.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1l8kkvwgf8k9v7lvvzraz242pyxqah0qi4y9i3sil9sx2nw1hvqn")))

(define-public crate-simple-home-dir-0.3.1 (c (n "simple-home-dir") (v "0.3.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0grvnak72bsjk7k1lyaj6m97qng7ysm3ybn8crxnxfmbns3sp12n")))

(define-public crate-simple-home-dir-0.3.2 (c (n "simple-home-dir") (v "0.3.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12z35j6cyjm0g2pqbm0qbdknhy2681ha4k2p1fid4mx2h07jjar1")))

(define-public crate-simple-home-dir-0.3.3 (c (n "simple-home-dir") (v "0.3.3") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kgmhv83q24na7s7nxvfkmjc37lrqfg84qflz6zmll8h0py82lb0")))

(define-public crate-simple-home-dir-0.3.4 (c (n "simple-home-dir") (v "0.3.4") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System" "Win32_System_Com" "Win32_UI" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s7gp09x04fzbhiaf28gyzk3dnlbm64fp6z8fhll01q8j2756cy4")))

