(define-module (crates-io si mp simple_huffman) #:use-module (crates-io))

(define-public crate-simple_huffman-0.1.0 (c (n "simple_huffman") (v "0.1.0") (h "0bqnd88935lyl7d1qwgxqry5pd3v9pv072xg934k72lcj76l0zp7")))

(define-public crate-simple_huffman-0.1.1 (c (n "simple_huffman") (v "0.1.1") (h "05r15lsk8mr12kzryrzbn2iay5h905wzzpnxs3zkk1fhrpjgvgwr")))

(define-public crate-simple_huffman-0.1.2 (c (n "simple_huffman") (v "0.1.2") (h "163msvfag4fg055wkjhcxyxsqg65ny4biyzi7y34bc4fjy244p5a")))

(define-public crate-simple_huffman-0.1.3 (c (n "simple_huffman") (v "0.1.3") (h "0j69xw0kapnx1xq0f27xr7xgj8cslcfkjj9i06z8v9qrz20k5jms")))

