(define-module (crates-io si mp simple_image_interface) #:use-module (crates-io))

(define-public crate-simple_image_interface-0.1.0 (c (n "simple_image_interface") (v "0.1.0") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "0q86lv5vakrjr7gnisilam11ad3r5ybpzsv509z17ladxzrvaqrl")))

(define-public crate-simple_image_interface-0.1.1 (c (n "simple_image_interface") (v "0.1.1") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "11iixgvhg5sj0wd95074kiqr0nwl9yp6p9akapmlv0k8i186vzjr")))

(define-public crate-simple_image_interface-0.1.2 (c (n "simple_image_interface") (v "0.1.2") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "1x37aal6iz1ybsj1k6h01bbd97zf7bn3zhk7kk4gx0nxw7w3m08g")))

(define-public crate-simple_image_interface-0.1.3 (c (n "simple_image_interface") (v "0.1.3") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "07ihwrfw5zzz4m930m7vizfg04mif96qddb5cnkflspakc6ipj1n")))

(define-public crate-simple_image_interface-0.1.4 (c (n "simple_image_interface") (v "0.1.4") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "15r5vfw6ais7h42k9ikf3x96x9mdr1c7cgk5bi1k7mbkrhqg2k4f")))

(define-public crate-simple_image_interface-0.1.5 (c (n "simple_image_interface") (v "0.1.5") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "02iazhab8yn9qsffjg8rik1kpmn4zggzakak6zhagy1gfi0v1mbc")))

(define-public crate-simple_image_interface-0.1.6 (c (n "simple_image_interface") (v "0.1.6") (d (list (d (n "ffmpeg-next") (r "^6.1.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rscam") (r "^0.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "1g6qgzyjfcdvhffdad9rw3pams2g8kx1r8qqgli6457l1dcm2fl6")))

