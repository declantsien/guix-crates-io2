(define-module (crates-io si mp simple-easing) #:use-module (crates-io))

(define-public crate-simple-easing-1.0.0 (c (n "simple-easing") (v "1.0.0") (h "06nlvfq7aq9fyq546jl6vwpw8j23n8yj4qba6f0bx44hb40cw9rd")))

(define-public crate-simple-easing-1.0.1 (c (n "simple-easing") (v "1.0.1") (h "188sag9a9nw9x9yzq6lgrgal41wfgh5k6g4p7gcnz3fry1yxsbc3")))

