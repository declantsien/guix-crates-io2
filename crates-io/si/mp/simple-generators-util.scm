(define-module (crates-io si mp simple-generators-util) #:use-module (crates-io))

(define-public crate-simple-generators-util-0.1.0 (c (n "simple-generators-util") (v "0.1.0") (d (list (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)))) (h "06pb81qhd3a1yvy1vj5cvr5564kivifp0qzwivbvb75p5ky3jzky") (f (quote (("alloc"))))))

(define-public crate-simple-generators-util-0.1.1 (c (n "simple-generators-util") (v "0.1.1") (d (list (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)))) (h "1vyqprbcdixzcmwwm01fw9qvxc9ips5gq291i6hsn62nk4jxb8sj") (f (quote (("alloc"))))))

