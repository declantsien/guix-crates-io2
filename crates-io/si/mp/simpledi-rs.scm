(define-module (crates-io si mp simpledi-rs) #:use-module (crates-io))

(define-public crate-simpledi-rs-0.1.0 (c (n "simpledi-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)))) (h "17bn1hp67php8iq75qxki943ppphds34ms9z9959mrwvdcvy9qhw")))

