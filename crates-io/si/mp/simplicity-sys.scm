(define-module (crates-io si mp simplicity-sys) #:use-module (crates-io))

(define-public crate-simplicity-sys-0.2.0 (c (n "simplicity-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "hashes") (r "^0.13") (d #t) (k 0) (p "bitcoin_hashes")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vcry2v0pwafyhg56cy8rwdqvkzi0xmgwcb879sy0qr13bc84a3a") (f (quote (("test-utils")))) (r "1.58.0")))

