(define-module (crates-io si mp simple-storage) #:use-module (crates-io))

(define-public crate-simple-storage-0.0.1 (c (n "simple-storage") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ajjj15b11aw85y7b7p0laqps3xnmzm0nss7qf4q3q13idmfx4hb")))

(define-public crate-simple-storage-0.0.2 (c (n "simple-storage") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0szvw4p3c0nxkbih5sfmziq4yka5b4g2301iwqfmzfcicq8bynzc")))

