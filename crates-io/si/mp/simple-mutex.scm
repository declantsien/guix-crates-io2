(define-module (crates-io si mp simple-mutex) #:use-module (crates-io))

(define-public crate-simple-mutex-1.0.0 (c (n "simple-mutex") (v "1.0.0") (d (list (d (n "event-listener") (r "^1.0.1") (d #t) (k 0)))) (h "0xsck0jrb4j1l89vlbmiifsaiah9jfjml8b4l3wn8yhfwq598k8z")))

(define-public crate-simple-mutex-1.0.1 (c (n "simple-mutex") (v "1.0.1") (d (list (d (n "event-listener") (r "^1.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "0s5cm2x2nf8j2fb2cpqgryxfcrj1y42rpga3208ryg121k47l7al")))

(define-public crate-simple-mutex-1.1.0 (c (n "simple-mutex") (v "1.1.0") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "1cs9kszm4rfxkfpviacvvkpjj43n0c19k210r7pb4zmf0irvnlds")))

(define-public crate-simple-mutex-1.1.1 (c (n "simple-mutex") (v "1.1.1") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "0xl54ygpzp8nxmjhnxihdss5dwgiyibyv1gkq6vmrq7m9g7kwicj")))

(define-public crate-simple-mutex-1.1.2 (c (n "simple-mutex") (v "1.1.2") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "01mzipbl7p2lmb3qrkcz6ljmn4yx96pmdvwq2yv07hnrhpjcmvsr")))

(define-public crate-simple-mutex-1.1.3 (c (n "simple-mutex") (v "1.1.3") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "05fs6s06x9gbswdysxz3p1fgfwq96wjsz6imrwbri91xjw3g1d5b")))

(define-public crate-simple-mutex-1.1.4 (c (n "simple-mutex") (v "1.1.4") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "0b722fgadds726vl30gg0wvhj6ny0m0f1j0w3hqlalkl4vqsn708")))

(define-public crate-simple-mutex-1.1.5 (c (n "simple-mutex") (v "1.1.5") (d (list (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 2)))) (h "1mnwlgjajqmxjfgsdcr9imf23yg1zblny95zrvcflvbgzbmbpaiq")))

