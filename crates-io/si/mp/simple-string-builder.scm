(define-module (crates-io si mp simple-string-builder) #:use-module (crates-io))

(define-public crate-simple-string-builder-0.1.0 (c (n "simple-string-builder") (v "0.1.0") (h "0mdc6z8wyihr2db8ll4s5j9w5rjgkl97f8l580if3fadgs89yvy9")))

(define-public crate-simple-string-builder-0.1.1 (c (n "simple-string-builder") (v "0.1.1") (h "187mqfckbhwjib4qpqn5989xks86c8b5hmag407np90k83j4dmvw")))

(define-public crate-simple-string-builder-0.2.0 (c (n "simple-string-builder") (v "0.2.0") (h "030lznbk8i60r5ziyabg3l40kchprigqw7hiphm0hqi3bwngq5mx")))

