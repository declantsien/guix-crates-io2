(define-module (crates-io si mp simplestcrypt) #:use-module (crates-io))

(define-public crate-simplestcrypt-0.1.0 (c (n "simplestcrypt") (v "0.1.0") (d (list (d (n "aes-siv") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zmai2mz71gswbqhzbq1ghq7fgcdyaz1f6zv19329z2x7rnfgk1g")))

(define-public crate-simplestcrypt-0.1.1 (c (n "simplestcrypt") (v "0.1.1") (d (list (d (n "aes-siv") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbsbwyjdj9f45kr45wzg3m6pb2vrdi88zai9s9d6i3vq6rkskw2")))

(define-public crate-simplestcrypt-0.1.2 (c (n "simplestcrypt") (v "0.1.2") (d (list (d (n "aes-siv") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bs7s53afhwn5nnf29d7clkqjhc3b73084881daa5jzh25c19n1z")))

(define-public crate-simplestcrypt-0.1.3 (c (n "simplestcrypt") (v "0.1.3") (d (list (d (n "aes-siv") (r "^0.6") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "084zj9p0jvg1k0k5c2wlbzg8by3ns3v2zkrykfsan0zn03rr2b95")))

