(define-module (crates-io si mp simple-async) #:use-module (crates-io))

(define-public crate-simple-async-0.1.0 (c (n "simple-async") (v "0.1.0") (h "05m309py5cdwndp4qi17pxmsv3jaz13am8kdnnaxckfic85s8hgh")))

(define-public crate-simple-async-0.2.0 (c (n "simple-async") (v "0.2.0") (h "1h18is05kgasxgblilc94jffg27xx7s9hh1llsf7izni34j3ampw")))

