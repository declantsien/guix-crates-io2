(define-module (crates-io si mp simple-window) #:use-module (crates-io))

(define-public crate-simple-window-0.1.0 (c (n "simple-window") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "raw-window-handle") (r "=0.6.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_UI" "Win32_UI_WindowsAndMessaging" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "15ika30n6cr82dj9qpwcylhm3xp2zm8nzngda8mh9zvl847v5ryy") (y #t)))

(define-public crate-simple-window-0.1.1 (c (n "simple-window") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "raw-window-handle") (r "=0.6.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_UI" "Win32_UI_WindowsAndMessaging" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0xcvrcclawf54vy66wl7pfj4zz2birihm7wwjggwyzimcmkdmb5n")))

