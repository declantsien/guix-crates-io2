(define-module (crates-io si mp simple-bitrange) #:use-module (crates-io))

(define-public crate-simple-bitrange-0.1.0 (c (n "simple-bitrange") (v "0.1.0") (h "1bwvq0zqzn0g32q835g2grv80q74s66hy4k03kb7wmpff6pjygns") (f (quote (("never-inline") ("enable-inline") ("default" "enable-inline")))) (y #t)))

(define-public crate-simple-bitrange-0.1.1 (c (n "simple-bitrange") (v "0.1.1") (h "1vihlnbdlbcj03vbh8i96vg4niyv80bljf6jxw9xmkhzfj50hqii") (f (quote (("never-inline") ("enable-inline") ("default" "enable-inline")))) (y #t)))

(define-public crate-simple-bitrange-0.1.2 (c (n "simple-bitrange") (v "0.1.2") (h "00pn15njqn2nwjj9p6wv3hyj8sc468sw1b2qqqh49dyayzyv6gll") (f (quote (("never-inline") ("enable-inline") ("default" "enable-inline")))) (y #t)))

(define-public crate-simple-bitrange-0.1.3 (c (n "simple-bitrange") (v "0.1.3") (h "1sy6jz9m7zhiv9xv0vhj3xsnvmsc34nprh1zycaixdfq3fkzzqah") (f (quote (("never-inline") ("enable-inline") ("default"))))))

