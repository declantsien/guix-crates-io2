(define-module (crates-io si mp simple-cheatsheet) #:use-module (crates-io))

(define-public crate-simple-cheatsheet-0.1.0 (c (n "simple-cheatsheet") (v "0.1.0") (h "0g0nlny5w5h5qs6vdmkn9iv65ynfs9bnmxr4s5pjm0a1z42f2b2p") (y #t)))

(define-public crate-simple-cheatsheet-0.2.0 (c (n "simple-cheatsheet") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0wjj1vjwasgspfwaf3mk6sag4spza1nq16c9la8rb17cb4s87m8s")))

