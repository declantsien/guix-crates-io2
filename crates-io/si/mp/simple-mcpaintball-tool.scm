(define-module (crates-io si mp simple-mcpaintball-tool) #:use-module (crates-io))

(define-public crate-simple-mcpaintball-tool-0.1.0 (c (n "simple-mcpaintball-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zrmbsgzzv1qa3sfyvnmv72rmfh28x6ssrjn4k58b145nfzscjcg")))

(define-public crate-simple-mcpaintball-tool-0.1.1 (c (n "simple-mcpaintball-tool") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06cgggv182fh3ndhd6ah1dqwn9s2vmvi89gdhk2f2fy1dg262b8b")))

