(define-module (crates-io si mp simple_function_logger) #:use-module (crates-io))

(define-public crate-simple_function_logger-0.1.0 (c (n "simple_function_logger") (v "0.1.0") (h "0rhhj95x04i3vdz3bcqjx5srjqhb8vgqkw757m7c6k76kr7hsjcr")))

(define-public crate-simple_function_logger-0.1.1 (c (n "simple_function_logger") (v "0.1.1") (h "0fk90nmda9p9yqzd91l0yq7rydgp51l0ddbprym1px9866brqzwj")))

(define-public crate-simple_function_logger-0.1.2 (c (n "simple_function_logger") (v "0.1.2") (h "0izsnapwhxcfwmsagf44q9as5606sm3lgbm036mx1h2g5x81781m")))

(define-public crate-simple_function_logger-0.1.3 (c (n "simple_function_logger") (v "0.1.3") (h "0f49xfv28w83zdif9jjrsvgf843qlfidxwa920gw3fkkgvlksn6i")))

(define-public crate-simple_function_logger-0.1.4 (c (n "simple_function_logger") (v "0.1.4") (h "13y1s6j82ggzzjai9r6sjrzlrkyhxd2i7sjaq8ln7ng40fyq191b")))

