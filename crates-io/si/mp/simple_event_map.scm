(define-module (crates-io si mp simple_event_map) #:use-module (crates-io))

(define-public crate-simple_event_map-0.1.0 (c (n "simple_event_map") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.10") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)))) (h "0m85hgkdrrz1jzag7l487swacqkxk0szfshivnw8aw8gdiqij5jw")))

(define-public crate-simple_event_map-0.2.0 (c (n "simple_event_map") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)))) (h "0pz818ing6pzb69aby9lbvrybdvdxzg9z2ygn4pw9vsccmr0g72h")))

