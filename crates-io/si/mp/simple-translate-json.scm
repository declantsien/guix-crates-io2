(define-module (crates-io si mp simple-translate-json) #:use-module (crates-io))

(define-public crate-simple-translate-json-0.1.0 (c (n "simple-translate-json") (v "0.1.0") (d (list (d (n "cargo-release") (r "^0.25.3") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "sys-locale") (r "^0.3.1") (d #t) (k 0)))) (h "129ahmxilfp77igr5imhyizybsd6xmxk1w45lfjxlxhh5lpw05ry")))

(define-public crate-simple-translate-json-0.1.1 (c (n "simple-translate-json") (v "0.1.1") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "sys-locale") (r "^0.3.1") (d #t) (k 0)))) (h "154lx912jw4z3fc9z5kqg0k1yf189d5jy6h9ya2xkxxybhgaq4hj")))

