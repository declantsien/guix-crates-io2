(define-module (crates-io si mp simple-rw-global) #:use-module (crates-io))

(define-public crate-simple-rw-global-0.1.0 (c (n "simple-rw-global") (v "0.1.0") (h "18jgcjzfhi8bhfkn9yimj20swmmsgm964wr9adg5si8b49g2jijn")))

(define-public crate-simple-rw-global-0.2.0 (c (n "simple-rw-global") (v "0.2.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0whqnkiqn8bcmwmszprxw6sjj620glm2x8bq0cijq80skkh70dhi") (f (quote (("stdsync") ("default" "stdsync")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-simple-rw-global-0.2.1 (c (n "simple-rw-global") (v "0.2.1") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0fhakj5gb2zhsw9cxyvfqsrh7z74lhlrpbbfrxks9q83rqrxzxmv") (f (quote (("stdsync") ("default" "stdsync")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-simple-rw-global-0.3.0 (c (n "simple-rw-global") (v "0.3.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "16g7ll1zlnags189bkdk2ncha1dkk0a6dlzkmjns7qgypg7pg2wf") (f (quote (("stdsync") ("default" "stdsync")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-simple-rw-global-0.4.0 (c (n "simple-rw-global") (v "0.4.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "09lh65d9xgm9mwzx2f7k60srj88li3zand5065rg8rcjx34vip6p") (f (quote (("stdsync") ("default" "stdsync")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-simple-rw-global-0.5.0 (c (n "simple-rw-global") (v "0.5.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1rv20wi7mk5cfmwx39anclql0v2pxnyb2mvgax06f4n0dshbxm4h") (f (quote (("stdsync") ("default" "stdsync")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

