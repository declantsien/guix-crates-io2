(define-module (crates-io si mp simple_wal) #:use-module (crates-io))

(define-public crate-simple_wal-0.1.0 (c (n "simple_wal") (v "0.1.0") (d (list (d (n "advisory-lock") (r "^0.2.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08ladqddrf60plxza6d7qhffhxbw2l20sm0ah770bp4wnaxgdsbf")))

(define-public crate-simple_wal-0.2.0 (c (n "simple_wal") (v "0.2.0") (d (list (d (n "advisory-lock") (r "^0.2.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nr3id0xxxznxddz6k20azyh5cm2qzpmyjs5c6vhk8hc26x505vn")))

(define-public crate-simple_wal-0.3.0 (c (n "simple_wal") (v "0.3.0") (d (list (d (n "advisory-lock") (r "^0.2.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s27fxcilpbnxl23g87ik6mli5akkdrzggcss25vj59qim9fpy2z")))

