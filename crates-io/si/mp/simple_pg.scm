(define-module (crates-io si mp simple_pg) #:use-module (crates-io))

(define-public crate-simple_pg-0.1.0 (c (n "simple_pg") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.1.0") (k 0)) (d (n "simple_pg_macros") (r "^0.1") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.12") (d #t) (k 0)))) (h "193f4jnfl1n5kpsh2ih5d3n1c3y0r8khxbbndvd5rii3zc16asww")))

(define-public crate-simple_pg-0.2.0 (c (n "simple_pg") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.2") (k 0)) (d (n "simple_pg_macros") (r "^0.2") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.13") (d #t) (k 0)))) (h "1ah18kqpwhni1z3dcqkq0bgilby11gk3jkarv8lnhh2g6gqzdi49") (f (quote (("serde" "simple_pg_pool/serde"))))))

(define-public crate-simple_pg-0.3.0 (c (n "simple_pg") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.3") (k 0)) (d (n "simple_pg_macros") (r "^0.3") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.14") (d #t) (k 0)))) (h "0vq4ymqwp9aqky97r7r7v9jfhcbqy8847y39l048cjkdrx4sbx3s") (f (quote (("serde" "simple_pg_pool/serde"))))))

(define-public crate-simple_pg-0.3.1 (c (n "simple_pg") (v "0.3.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.3.1") (k 0)) (d (n "simple_pg_macros") (r "^0.3") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.14") (d #t) (k 0)))) (h "120p0653yhy9lg7y1zr6zxz94ls2hyx4hil19f8ifzxrmmqzwlmm") (f (quote (("serde" "simple_pg_pool/serde"))))))

(define-public crate-simple_pg-0.4.0 (c (n "simple_pg") (v "0.4.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.4") (k 0)) (d (n "simple_pg_macros") (r "^0.3") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.15") (d #t) (k 0)))) (h "13r3m2wfr5jhvyzj1jg6h43w9r3ldjb0zzzn4a1vj163v4n606nb") (f (quote (("serde" "simple_pg_pool/serde"))))))

(define-public crate-simple_pg-0.4.1 (c (n "simple_pg") (v "0.4.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.4") (k 0)) (d (n "simple_pg_macros") (r "^0.3") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.15") (d #t) (k 0)))) (h "111wa4kpmyyb87x4xkam8wpz5lrgc26ck1944v586f7mq31xlypg") (f (quote (("serde" "simple_pg_pool/serde"))))))

(define-public crate-simple_pg-0.5.0 (c (n "simple_pg") (v "0.5.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "simple_pg_client") (r "^0.5") (k 0)) (d (n "simple_pg_macros") (r "^0.4") (d #t) (k 0)) (d (n "simple_pg_pool") (r "^0.16") (d #t) (k 0)))) (h "0kzxr892wr740523khmj2pslhlcan4zrkv7cy8l9rdzypb4m566g") (f (quote (("serde" "simple_pg_pool/serde"))))))

