(define-module (crates-io si mp simple-shutdown) #:use-module (crates-io))

(define-public crate-simple-shutdown-0.1.0 (c (n "simple-shutdown") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (o #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "sync" "time" "macros"))) (k 2)))) (h "1q4av8abbcwchp48gxn091s0qxd5gzqlg35y4njmfay4ql00zvzq") (f (quote (("std" "critical-section/std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("tokio" "std" "dep:tokio"))))))

