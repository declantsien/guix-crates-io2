(define-module (crates-io si mp simplisp) #:use-module (crates-io))

(define-public crate-simplisp-0.3.0 (c (n "simplisp") (v "0.3.0") (d (list (d (n "define_error") (r "^4") (d #t) (k 0)))) (h "0hrdh13kchghzjzagps3k1iyikm3sgqqs79qm4m5sfqf3d8rq0z2")))

(define-public crate-simplisp-0.4.0 (c (n "simplisp") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1pybckixbndffh6civkq6fqvzcc4cl2yzbdwd8bxym6jy099a9r1")))

