(define-module (crates-io si mp simple-shunting) #:use-module (crates-io))

(define-public crate-simple-shunting-0.1.2 (c (n "simple-shunting") (v "0.1.2") (d (list (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "lexers") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 2)))) (h "16iyr9gj6nqp9zdv3nkmdkszk3xkf5zxk4qswxyqhp2h597ap07v")))

