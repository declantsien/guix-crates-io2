(define-module (crates-io si mp simple-xml) #:use-module (crates-io))

(define-public crate-simple-xml-0.1.6 (c (n "simple-xml") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "063xb4wn4njl22jkprq4dn2sbjvf1air6s8wvmg87qj8m6dacalj")))

(define-public crate-simple-xml-0.1.7 (c (n "simple-xml") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "08gc5c01zp0n2wrl9sfxkbapnzsz3l4v2gh7gakg1sqv2ljlny86")))

(define-public crate-simple-xml-0.1.8 (c (n "simple-xml") (v "0.1.8") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "15qw7xfy9wxac9050hk5vd8jgh8imilzmmby1icw4jm3nqfg2nw4")))

(define-public crate-simple-xml-0.1.10 (c (n "simple-xml") (v "0.1.10") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1sn0hfib6lpxc6hp35nv0cj0c3saqkdsgs28nqdzblfjxpd00fg7")))

