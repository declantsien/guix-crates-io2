(define-module (crates-io si mp simplicity_derive) #:use-module (crates-io))

(define-public crate-simplicity_derive-0.1.0 (c (n "simplicity_derive") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "permutator") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "1jpjj35yac7r0gcifjfrfi463y0sdyngdxwz194kmr99vi0qj3ji")))

(define-public crate-simplicity_derive-0.2.0 (c (n "simplicity_derive") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "permutator") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "05kywa9ril0wxria4xz152lsz6sa82v79bsywif1p345qi75p8rd")))

