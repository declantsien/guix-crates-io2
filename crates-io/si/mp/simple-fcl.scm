(define-module (crates-io si mp simple-fcl) #:use-module (crates-io))

(define-public crate-simple-fcl-0.0.1 (c (n "simple-fcl") (v "0.0.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "simple-fcl-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1j2ri011zpk60s35mp770il86p9bpjwxj9casvh8drq4v9r9ag09")))

(define-public crate-simple-fcl-0.0.2 (c (n "simple-fcl") (v "0.0.2") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "simple-fcl-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0za5isvj0fdl2bgmdvgi2zr5zpnxiivdm2r6vr471hrxw8nggyjy")))

(define-public crate-simple-fcl-0.0.3 (c (n "simple-fcl") (v "0.0.3") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "simple-fcl-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0hx4k5dnq8rmvw9xn86sndjyg05vbypphb3ma8348sky7iy5gjrf")))

(define-public crate-simple-fcl-0.0.5 (c (n "simple-fcl") (v "0.0.5") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "simple-fcl-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1ish0vja48iqvi16mrc2m30bs663m14kd3xy540nzh7agb2ayhns")))

