(define-module (crates-io si mp simple_game_of_life) #:use-module (crates-io))

(define-public crate-simple_game_of_life-1.0.0 (c (n "simple_game_of_life") (v "1.0.0") (d (list (d (n "rand") (r "^0.3.23") (d #t) (k 0)) (d (n "simple") (r "^0.3.0") (d #t) (k 0)))) (h "0jys1k9i517rsma1dyrrsaswx11v9ckfm9rj8y671q5dww6bvkrq")))

(define-public crate-simple_game_of_life-1.1.0 (c (n "simple_game_of_life") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple") (r "^0.3.0") (d #t) (k 0)))) (h "08cs3c1dhf2nl4qrfafkzi486bw8w61rs7hwz49z13s1mil90s0r")))

(define-public crate-simple_game_of_life-1.1.1 (c (n "simple_game_of_life") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple") (r "^0.3.0") (d #t) (k 0)))) (h "11c3rmc5r70kcdlh4winixfz28abw9m3a475lh6ijn164bfqnq1y")))

