(define-module (crates-io si mp simple-pixels) #:use-module (crates-io))

(define-public crate-simple-pixels-0.1.0 (c (n "simple-pixels") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.12") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "10isnrwn613w535dvnr5lqcr6xy6gi4c76x6s2bad725mcdpcz4i")))

(define-public crate-simple-pixels-0.2.0 (c (n "simple-pixels") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.12") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0pc7539ddpdbmpckjlj3g0l6p1m0j5hla94wr3fah2n6vjxlps1s")))

(define-public crate-simple-pixels-0.2.1 (c (n "simple-pixels") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.13") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0dcs5v4qvbhc36n2r3gw86ivx0bsvc56wn298ydk8x41f0klwjia")))

(define-public crate-simple-pixels-0.2.2 (c (n "simple-pixels") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.13") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "09f3val6yjqhlk56725i597krkpphk1jj8671zmgswls257v4lmy")))

(define-public crate-simple-pixels-0.2.3 (c (n "simple-pixels") (v "0.2.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.13") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1lvl9anbpb63c03xjnchkh61c6hjcfrwr45lkbpb1hjx4i7jd6m3")))

(define-public crate-simple-pixels-0.2.4 (c (n "simple-pixels") (v "0.2.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0a1n16ad64hy0hgp48nfmpl5kzrgvc4v96235dcnj626rqgfnwrv")))

(define-public crate-simple-pixels-0.3.0 (c (n "simple-pixels") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "19mawyh9yqj4964ls2ql1sph2qr214ng7am4wfl3csi9afkh8zmg")))

(define-public crate-simple-pixels-0.4.0 (c (n "simple-pixels") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0mfwah6mhf8y91qphanfrs0iwrsm0ab5nxhpbz1qk5zcg6j0lgll")))

(define-public crate-simple-pixels-0.4.1 (c (n "simple-pixels") (v "0.4.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0h1y2x222rl21qix2bfp1i9563jhampsyh2716j21l7h61dvflc3")))

(define-public crate-simple-pixels-0.5.0 (c (n "simple-pixels") (v "0.5.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "08fy3fpmdnsgz7yrmvh3vinpk6cfmxrdcg3f1vkk63320rcjjdkw")))

(define-public crate-simple-pixels-0.6.0 (c (n "simple-pixels") (v "0.6.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "15m6jd36p8nd2gfbm4di5pj5n11h7m3ww7d6lszdfl0ydgln9sj2")))

(define-public crate-simple-pixels-0.7.0 (c (n "simple-pixels") (v "0.7.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "simple-blit") (r "^0.4") (d #t) (k 0)))) (h "0ff36bnv538qm7kli9rwlrm1bm3bm8masajl0z2ypky12m48xaky")))

(define-public crate-simple-pixels-0.8.0 (c (n "simple-pixels") (v "0.8.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "simple-blit") (r "^0.6") (d #t) (k 0)))) (h "02q5il33x2q78ggyqr0dh4pihpz955b79d2a7wailhzwpl97fzkb")))

(define-public crate-simple-pixels-0.8.1 (c (n "simple-pixels") (v "0.8.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.16") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "simple-blit") (r "^0.7") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)))) (h "1ljipc870y6nb98l2lksivdqpqdnnb0hr9wkkrjagchb1waf9ba3")))

(define-public crate-simple-pixels-0.9.0 (c (n "simple-pixels") (v "0.9.0") (d (list (d (n "miniquad") (r "=0.4.0-alpha.10") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^0.7") (d #t) (k 0)))) (h "0qgwq8vw4hy8imzff2hd3s6gybvqg2gx5d9r5w60ad9c4b9665cj")))

(define-public crate-simple-pixels-0.10.0 (c (n "simple-pixels") (v "0.10.0") (d (list (d (n "miniquad") (r "=0.4.0-alpha.10") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^0.8") (d #t) (k 0)))) (h "1g66j1qi5a4z7k92n5a1fzrcwcnh5hha2xnnh5rc2kkfmpfz70rx")))

(define-public crate-simple-pixels-0.11.0 (c (n "simple-pixels") (v "0.11.0") (d (list (d (n "miniquad") (r "=0.4.0-alpha.10") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^0.8") (d #t) (k 0)))) (h "1x81w91nxpaxh4syma6vb5dcphbwq3d8az57n89r3qs3wih7kr9b")))

(define-public crate-simple-pixels-0.12.0 (c (n "simple-pixels") (v "0.12.0") (d (list (d (n "miniquad") (r "=0.4.0-alpha.10") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^1") (d #t) (k 0)))) (h "1806iyfqx5i3z6w43yi8mnvyzw46rqq098p4d7166hw4wc4hjx97")))

(define-public crate-simple-pixels-0.12.1 (c (n "simple-pixels") (v "0.12.1") (d (list (d (n "miniquad") (r "^0.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^1") (d #t) (k 0)))) (h "0zgvwk5xazw53lnznwm9wbsvc4vjj058vsrj6rhb6ax6i1wqkbg5") (f (quote (("log-impl" "miniquad/log-impl") ("default"))))))

(define-public crate-simple-pixels-0.12.2 (c (n "simple-pixels") (v "0.12.2") (d (list (d (n "miniquad") (r "^0.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^1") (d #t) (k 0)))) (h "19bxirfssyxq972lsqjl2cf2bwc0jhr72i7qn1yscsr0byjdvryf") (f (quote (("log-impl" "miniquad/log-impl") ("default"))))))

(define-public crate-simple-pixels-0.13.0 (c (n "simple-pixels") (v "0.13.0") (d (list (d (n "miniquad") (r "^0.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^1") (d #t) (k 0)))) (h "0q1240xx5yzxfzi3bp1gkwbqrmrqyrfc4zlpqf60h1yyfzfhvzf9") (f (quote (("log-impl" "miniquad/log-impl") ("default"))))))

(define-public crate-simple-pixels-0.14.0 (c (n "simple-pixels") (v "0.14.0") (d (list (d (n "miniquad") (r "^0.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "simple-blit") (r "^1") (d #t) (k 0)))) (h "0mlzjv2svap9dh9yc47245myw738y4kk8xw257hh0zgzyfaxw414")))

