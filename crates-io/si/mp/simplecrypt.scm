(define-module (crates-io si mp simplecrypt) #:use-module (crates-io))

(define-public crate-simplecrypt-1.0.0 (c (n "simplecrypt") (v "1.0.0") (d (list (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1b1iki52n0imnrjsh3icjizmqvlfw6ldlhadan1dgfg2hzxyzf86")))

(define-public crate-simplecrypt-1.0.1 (c (n "simplecrypt") (v "1.0.1") (d (list (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1ckg3fswjdavvqf2m6dlysrwknn5z5yq928r37k4b7x1q90klfik")))

(define-public crate-simplecrypt-1.0.2 (c (n "simplecrypt") (v "1.0.2") (d (list (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hhgrqvwkia5nf4hdxhnm80d7rvh5jqdplcdwlbz5c864widdap7")))

