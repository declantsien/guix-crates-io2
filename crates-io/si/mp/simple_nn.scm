(define-module (crates-io si mp simple_nn) #:use-module (crates-io))

(define-public crate-simple_nn-0.1.0 (c (n "simple_nn") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "199fd8y2b97giiahp440fl6dl09grmd4d8m7lyha3zsfgc2y5833")))

(define-public crate-simple_nn-0.1.1 (c (n "simple_nn") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0qdxl8g5c424nnsd6lzcagb1sxwilwiz0p18bvvfvh90wjff3y26")))

(define-public crate-simple_nn-0.1.2 (c (n "simple_nn") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "06d9qa31srjcfjip7y7d7fg6f5qnkby8bscy8c13ss9nnja2dn67")))

