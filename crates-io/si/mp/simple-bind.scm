(define-module (crates-io si mp simple-bind) #:use-module (crates-io))

(define-public crate-simple-bind-0.1.0 (c (n "simple-bind") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0095ki7rpsvppc88yaz4lsn9q5k132f1myyy3cpg2gnpdicihv6k")))

(define-public crate-simple-bind-0.1.1 (c (n "simple-bind") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "13vs69vy5gr53sdxjpgqf9r3132f4cnqfp7d3yv63p9hvhlwd5y8")))

(define-public crate-simple-bind-0.1.2 (c (n "simple-bind") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0sxy8z3wsx6bhdc5rcidrmj2rahs9i6f7pj76568g0gwdi1ds470")))

(define-public crate-simple-bind-0.1.3 (c (n "simple-bind") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1c14ma3inrcyjyd7yahv0z589rji3phzfg3xh7bim322c8y9q2cs")))

(define-public crate-simple-bind-0.1.4 (c (n "simple-bind") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mv1pc4g4yngi0zr09gk1in2ydyz7pi0w5r4anv2l0i034czsh80")))

(define-public crate-simple-bind-0.1.5 (c (n "simple-bind") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1plgqnl4q8kyk4n19ij61v04qwpq5b6r3184h2mfcbdrz2dw6316")))

(define-public crate-simple-bind-0.1.6 (c (n "simple-bind") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.3.8") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02afxmdfq1rlwjvzdslhqiywl23cl7050k67cygar95inm4hk6v2")))

