(define-module (crates-io si mp simplemad) #:use-module (crates-io))

(define-public crate-simplemad-0.1.0 (c (n "simplemad") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "0svpnh342cixwvl12z5hmqlc75l2knz66riczryjhi23gll64p2z")))

(define-public crate-simplemad-0.2.0 (c (n "simplemad") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "03hpm5ki8qvlqrzknki5vypm465sx1dhgz6qfs4hnwhc5dhd21cn")))

(define-public crate-simplemad-0.2.1 (c (n "simplemad") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "0chn058h3w4qgxajs1bz8lbqni9vfxpig7i5j11jfcfvfa8n7bjk")))

(define-public crate-simplemad-0.2.2 (c (n "simplemad") (v "0.2.2") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "0w1gq7p1ll0yn4jcapainal7dmkyqff63cjn8zxcdv432rxq2ch8")))

(define-public crate-simplemad-0.2.3 (c (n "simplemad") (v "0.2.3") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "073p812np0nh8ih4sjzkycgkpl7j2fdgagn2p6831h7rs97lhpm9")))

(define-public crate-simplemad-0.3.0 (c (n "simplemad") (v "0.3.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "134xa7dm1ch82kgz3xxhi1zmhl0mbh80xnlxajfmpmm2gmqnpnkd")))

(define-public crate-simplemad-0.4.0 (c (n "simplemad") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "0f9ivflaalv33h0ss5874n4v49v1dzbl4cbgqysigja4nws87vg0")))

(define-public crate-simplemad-0.5.0 (c (n "simplemad") (v "0.5.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "simplemad_sys") (r "^0.1.0") (d #t) (k 0)))) (h "161b389bpax0m60166q8bp7rrngzwg83sp3zs4vd3ki2bvdrvili")))

(define-public crate-simplemad-0.5.1 (c (n "simplemad") (v "0.5.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "simplemad_sys") (r "^0.1.0") (d #t) (k 0)))) (h "013kni85xpi02cr05adkx9x98krkf62chskms2z8657xfij3s5rp")))

(define-public crate-simplemad-0.5.2 (c (n "simplemad") (v "0.5.2") (d (list (d (n "simplemad_sys") (r "^0.2.0") (d #t) (k 0)))) (h "1w1qkj622a3cib2j6hf0i78qzhf17z8dzrbndnrzkn7vy4rc2xli")))

(define-public crate-simplemad-0.6.0 (c (n "simplemad") (v "0.6.0") (d (list (d (n "simplemad_sys") (r "^0.3.0") (d #t) (k 0)))) (h "03ajixcf4basfdhlxrc5mn1fzhmvh1c4sryyhvn4wc2gn60hs3nb")))

(define-public crate-simplemad-0.6.1 (c (n "simplemad") (v "0.6.1") (d (list (d (n "simplemad_sys") (r "^0.3.0") (d #t) (k 0)))) (h "15lm9hx9p6hci5szrsix12v8lh1dy1qs2sbvx1x8nc19bf9kly93")))

(define-public crate-simplemad-0.6.2 (c (n "simplemad") (v "0.6.2") (d (list (d (n "simplemad_sys") (r "^0.3.0") (d #t) (k 0)))) (h "1byv10zwr9pb8q8qrqlr1jzc97l21wawai20jrw44w2w9fskmkva")))

(define-public crate-simplemad-0.6.3 (c (n "simplemad") (v "0.6.3") (d (list (d (n "simplemad_sys") (r "^0.4.0") (d #t) (k 0)))) (h "1q7sjd4q6p48bqpmgn11asknzhd44s4q5gijp0aj9xsg75ygj9cm")))

(define-public crate-simplemad-0.7.0 (c (n "simplemad") (v "0.7.0") (d (list (d (n "simplemad_sys") (r "^0.5.0") (d #t) (k 0)))) (h "04b9a4iiy01bmxh761s150w78zcmvf6r2f0hfj1k88xilwygkywc")))

(define-public crate-simplemad-0.7.1 (c (n "simplemad") (v "0.7.1") (d (list (d (n "simplemad_sys") (r "^0.5.0") (d #t) (k 0)))) (h "0ip7xfvlnp5kcg9nn75rcsykzywasbx25wmgsz4bbdhqci30rj11")))

(define-public crate-simplemad-0.8.0 (c (n "simplemad") (v "0.8.0") (d (list (d (n "simplemad_sys") (r "^0.5.0") (d #t) (k 0)))) (h "16dlcbvg9x270lv90p1cyn755p41rvjdwpf2yi6k20pd37na8z16")))

(define-public crate-simplemad-0.8.1 (c (n "simplemad") (v "0.8.1") (d (list (d (n "simplemad_sys") (r "^0.5.0") (d #t) (k 0)))) (h "01lfjrbdaiz39bj00yhi79bc7mj1hyf03k8pgfzw2c0nka1qbnr3")))

(define-public crate-simplemad-0.9.0 (c (n "simplemad") (v "0.9.0") (d (list (d (n "simplemad_sys") (r "^0.5.0") (d #t) (k 0)))) (h "0r2qdg5cyhxssmfkxahjc5dz87zm3ll8gmckx4acryqvjxk6hrj3")))

