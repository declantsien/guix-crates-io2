(define-module (crates-io si mp simple-json2) #:use-module (crates-io))

(define-public crate-simple-json2-0.1.1 (c (n "simple-json2") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "1paz7ca6nz7j2k58nv7jid8liaqfbvqblirv9mi0yibm9m6adz90") (f (quote (("std") ("default" "std"))))))

(define-public crate-simple-json2-0.1.2 (c (n "simple-json2") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "0k9kd8xcahhmf8v0k98p2wxgkica0vw5y7pfq3ak5bpl1qnvc51s") (f (quote (("std") ("default" "std"))))))

