(define-module (crates-io si mp simple-terminal-app) #:use-module (crates-io))

(define-public crate-simple-terminal-app-0.0.1 (c (n "simple-terminal-app") (v "0.0.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "054xwq7nz766xrpm0ndw60fyjymn63agcrrcv017w6bvxkvfqk53")))

(define-public crate-simple-terminal-app-0.0.2 (c (n "simple-terminal-app") (v "0.0.2") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1d0q1yqvr18vf6kybwhv6d1bhp2a1rj2w5y2ijiwq6zhyw5bd71w")))

(define-public crate-simple-terminal-app-0.0.3 (c (n "simple-terminal-app") (v "0.0.3") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1pjg7nk5df2f27jzvgalnkydfiirsnb5fhnhfqv7g3q85igzbgm5")))

(define-public crate-simple-terminal-app-0.0.4 (c (n "simple-terminal-app") (v "0.0.4") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1n719xbqps7i61lpgr14ppl1xj9vlgmig1jc9jrszf6r91lr0f8m")))

(define-public crate-simple-terminal-app-0.1.0 (c (n "simple-terminal-app") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1fgywbjcni47nl07c2fa7glgmbbwphg9yphi7lfry19r75wcnfjl") (y #t)))

(define-public crate-simple-terminal-app-0.1.1 (c (n "simple-terminal-app") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0qrgbz3qiig8dp0sqqw5hmf2gzmadl3pcafrkgz4q132787828i7")))

