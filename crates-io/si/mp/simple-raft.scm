(define-module (crates-io si mp simple-raft) #:use-module (crates-io))

(define-public crate-simple-raft-0.2.0 (c (n "simple-raft") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.7") (f (quote ("prost-derive"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "033vzzvdayknh7ks2l8znsyl8fwcw8f3n8s4qmps2pz0bd8iycpy") (f (quote (("default" "prost"))))))

