(define-module (crates-io si mp simple-16) #:use-module (crates-io))

(define-public crate-simple-16-0.1.0 (c (n "simple-16") (v "0.1.0") (h "1bdsd4f5nvfy8ilaqbz6wh458i52izz8ai5qlhsqdrc69dzy16l6")))

(define-public crate-simple-16-0.1.1 (c (n "simple-16") (v "0.1.1") (h "10dxhc640x2nj9j9x28q6lxnfbqy3526rnspsmwp73izd5l7i9ac")))

(define-public crate-simple-16-0.2.0 (c (n "simple-16") (v "0.2.0") (d (list (d (n "firestorm") (r "^0.3.0") (d #t) (k 0)))) (h "1b90iviycg7y9m1fkzrsi1x78k3pqmiajanlgjz16shjzd3kgssx")))

