(define-module (crates-io si mp simpleio) #:use-module (crates-io))

(define-public crate-simpleio-0.1.0 (c (n "simpleio") (v "0.1.0") (h "04dgk6b3ik010whf9912vy4gp0qxx47mv2pbld0pf7nr6z4vj3xp")))

(define-public crate-simpleio-0.1.1 (c (n "simpleio") (v "0.1.1") (h "1ysq9hbfrx33cxy94x6waii2j2w53q6fw2a36iavkc4bhzvp110m")))

(define-public crate-simpleio-0.1.2 (c (n "simpleio") (v "0.1.2") (h "0zyqx3qid6hjnm59c2pq35fmimzsy7vrm323299wykjyg24mrhzk")))

(define-public crate-simpleio-0.1.3 (c (n "simpleio") (v "0.1.3") (h "1bbzbxkg05ylbsyh4pgndsjxgibni7nkf2ddynwqpf82cwa2m8yf")))

(define-public crate-simpleio-0.1.4 (c (n "simpleio") (v "0.1.4") (h "14angkr9aylxmb608r79mbl38npgjv41y99nqwqkc8bjmpb4j35m")))

(define-public crate-simpleio-0.2.1 (c (n "simpleio") (v "0.2.1") (h "0zxryqvy3w7bnrx66f3l8597i2li4gmhpcipfjyvqpq5jifjzdyb")))

(define-public crate-simpleio-0.2.2 (c (n "simpleio") (v "0.2.2") (h "05mkz2clj4rmy3nx8six9qj886bnkxh29pjr23cq3zy9fw6gbw6l")))

(define-public crate-simpleio-0.2.3 (c (n "simpleio") (v "0.2.3") (h "1rb39a9h318n919ivag9r5sp9r1l7gyprdxcrk9fwjgaapsxjpcx")))

