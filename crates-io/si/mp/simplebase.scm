(define-module (crates-io si mp simplebase) #:use-module (crates-io))

(define-public crate-simplebase-0.1.0 (c (n "simplebase") (v "0.1.0") (h "0mlll15jml94j0z8qpljmw4sfw4j5524xs6mkl62j2ad8wycj369")))

(define-public crate-simplebase-0.2.0 (c (n "simplebase") (v "0.2.0") (h "0cz2jg5a9347r2hy07a0kmwfwg2f3schlg41r2ass71yyj5z3ln5")))

(define-public crate-simplebase-0.2.1 (c (n "simplebase") (v "0.2.1") (h "0gr5jl2xsymqkiygrl0lxpb5shgvinlmkv41fiv3jnj5qg6rsh2h")))

(define-public crate-simplebase-0.2.2 (c (n "simplebase") (v "0.2.2") (h "196lagih5sigxkvf3xix81k05h22rqm38fw4g00szw02psmx0g07")))

(define-public crate-simplebase-0.2.3 (c (n "simplebase") (v "0.2.3") (h "04vv7f4rrsihiaf46rymcvrwwngf32kma40xr38k830x517hf2ha")))

(define-public crate-simplebase-0.2.4 (c (n "simplebase") (v "0.2.4") (h "1c4gm3nwbas7dymn628pjmv55nlp890y53nsn5d43kkdjs9pw404")))

(define-public crate-simplebase-0.2.5 (c (n "simplebase") (v "0.2.5") (h "1hl6qlb8rwr732z5k6f5bmbj091bg2fbkxy62n2s5m975d5sk75f")))

(define-public crate-simplebase-0.2.6 (c (n "simplebase") (v "0.2.6") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1b9hfq2r5hwqb8dalgw1364w14x1834xgd1pq5zs3dffp5qa2x4q")))

(define-public crate-simplebase-0.3.0 (c (n "simplebase") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1rrck43wgwm8g8nf81kkci6jw0hl2jqgiyndrpgrwhcwvszf3ksi")))

(define-public crate-simplebase-0.3.1 (c (n "simplebase") (v "0.3.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1ckckl60qhf29dhgj808x7hsn2bhysv4yxv6zpm3gwzq436cfs8b")))

(define-public crate-simplebase-0.3.2 (c (n "simplebase") (v "0.3.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1addy8vvh261vplgfmz1vkk3blmb2gmx3l9iii3ccdphincgkj56")))

(define-public crate-simplebase-0.3.3 (c (n "simplebase") (v "0.3.3") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "046gcq8hj30v06yrl9bgwz7fpw1rvb9d9wc3qawgb6kfdag758f8")))

(define-public crate-simplebase-0.3.31 (c (n "simplebase") (v "0.3.31") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1ksamwyy1b8s4ynwkj3clksp4j92247j1nk5zqb2s7mbmbwvxi2y")))

(define-public crate-simplebase-0.3.32 (c (n "simplebase") (v "0.3.32") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0mzky7lamn3i7in2xs2x0izwml4036rq17dbi31ry78czk0rqhi2")))

(define-public crate-simplebase-0.3.33 (c (n "simplebase") (v "0.3.33") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0pq6rdamknsnz21k92v36q79kvfkylachrnq8wfpcrv2rc4j2i1v")))

(define-public crate-simplebase-0.3.34 (c (n "simplebase") (v "0.3.34") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1ws28n4pip7xj47xkb20mja8x8jaridhanxrzj01qp3vlrrmhxxk")))

(define-public crate-simplebase-0.3.35 (c (n "simplebase") (v "0.3.35") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0xrv2yz5dss6ky5a9vfimx25fikf7rw0xs83a28baigsg08swhw6")))

