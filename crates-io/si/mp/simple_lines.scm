(define-module (crates-io si mp simple_lines) #:use-module (crates-io))

(define-public crate-simple_lines-0.0.1 (c (n "simple_lines") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linereader") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k752s1s1rx76hghm13rcdqhzalk16mf2vhl9szhsrzfcz3imc95")))

