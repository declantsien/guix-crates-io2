(define-module (crates-io si mp simple-text-editor) #:use-module (crates-io))

(define-public crate-simple-text-editor-0.1.0 (c (n "simple-text-editor") (v "0.1.0") (h "1xw9b0ggjrk6kv5jk6ymr71s1fpjysjgrx6dwjhr62r6d7bd4gqh")))

(define-public crate-simple-text-editor-0.2.0 (c (n "simple-text-editor") (v "0.2.0") (h "0ff072x31rqwcy3janl8vb45lf6lp372z0g1j7rpmym2ndryc2db")))

(define-public crate-simple-text-editor-0.2.1 (c (n "simple-text-editor") (v "0.2.1") (h "196a60s6yiv2csa2kd075jk03iwjri5cfym7pik6zglsdrhx4pv9")))

(define-public crate-simple-text-editor-0.2.2 (c (n "simple-text-editor") (v "0.2.2") (h "1hpkhkcxlxddrpj3107fayvk62cir436dqr2v5ydz97sz17l9xlc")))

(define-public crate-simple-text-editor-0.2.3 (c (n "simple-text-editor") (v "0.2.3") (h "0yciq2ymwaxpp8vjhc17snvwkp8f4f18m05yy6sicq3qzcsyac0a")))

