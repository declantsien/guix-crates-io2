(define-module (crates-io si mp simple-word-count) #:use-module (crates-io))

(define-public crate-simple-word-count-0.1.0 (c (n "simple-word-count") (v "0.1.0") (h "0822379gvpkzjz2qzgjaxrwjhzf1gjj3n20256f1shfhbzjcw0a7")))

(define-public crate-simple-word-count-0.1.1 (c (n "simple-word-count") (v "0.1.1") (h "0bv4hdj4gywjdk5l08m6xclkvaqqyllixhn7j01axglmy04jhgfj")))

