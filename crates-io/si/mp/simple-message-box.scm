(define-module (crates-io si mp simple-message-box) #:use-module (crates-io))

(define-public crate-simple-message-box-0.0.1 (c (n "simple-message-box") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "176mbdg019wbrwr3zp0dqzpmc8fa19zmszfnp8bka2avbxwiga98")))

(define-public crate-simple-message-box-0.0.2 (c (n "simple-message-box") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1wh3dim5svz3ynxj9mjygc43aii3l18xdhnp17jvmxgnklvg9iy8")))

