(define-module (crates-io si mp simple-mermaid) #:use-module (crates-io))

(define-public crate-simple-mermaid-0.1.0 (c (n "simple-mermaid") (v "0.1.0") (h "094lizpq38y8xlbsd1891mcxvjhz5a6ypvwvy0416zbhw0926agr")))

(define-public crate-simple-mermaid-0.1.1 (c (n "simple-mermaid") (v "0.1.1") (h "061wrz7jr6fdw8ni5v52qywksf2gsi8syad98qfjn50fsx1is2k2")))

