(define-module (crates-io si mo simoncli) #:use-module (crates-io))

(define-public crate-simoncli-1.1.0 (c (n "simoncli") (v "1.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0gsdkd81gcb87fbkjgn1r5yrv8mq7dlfjjc1jva2w4dq9a07wrkr")))

(define-public crate-simoncli-1.1.1 (c (n "simoncli") (v "1.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0xvmmr1jm5j90mbk0fp9vzfzk4yvhlk4qx1glyv46byk33k53g0z")))

