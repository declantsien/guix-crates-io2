(define-module (crates-io si f- sif-kdtree) #:use-module (crates-io))

(define-public crate-sif-kdtree-0.1.0 (c (n "sif-kdtree") (v "0.1.0") (d (list (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s9nvph6acrff5lhmprgc8a5kiwhdkc20s201k4rxldc6wpj97l6") (r "1.55")))

(define-public crate-sif-kdtree-0.1.1 (c (n "sif-kdtree") (v "0.1.1") (d (list (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fvbq751qy0jlsmr1pibggh87amyl7dp47xvj280wpq9vch28vv9") (r "1.55")))

(define-public crate-sif-kdtree-0.1.2 (c (n "sif-kdtree") (v "0.1.2") (d (list (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nnp3dv4kvm0z7f7b80nfhpbbic9zy4x3k2y3pkdxvq3h16i3nwz") (r "1.55")))

(define-public crate-sif-kdtree-0.2.0 (c (n "sif-kdtree") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rmfkaydzmijcldpqjfjmycs5dljcnqkzv5qarv5vcqnjy2l7cc8") (r "1.55")))

(define-public crate-sif-kdtree-0.3.0 (c (n "sif-kdtree") (v "0.3.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h16ahlpm1k49mx958xd9bjzhkwj5ln5q8y66ys5bw4srx7ixpqm") (r "1.55")))

(define-public crate-sif-kdtree-0.4.0 (c (n "sif-kdtree") (v "0.4.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06hq4yajawb3h6h9a8w1cipzan5xhxvyawpj82zi1wxlg7p9mv66") (r "1.55")))

(define-public crate-sif-kdtree-0.5.0 (c (n "sif-kdtree") (v "0.5.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lrglgvjfg5pd41z0dfx3nc681q01w5xi803m8glaaymv3igacpa") (r "1.55")))

(define-public crate-sif-kdtree-0.5.1 (c (n "sif-kdtree") (v "0.5.1") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xnnzz2vhjjzh9inqq5la7ggd45iwn216c73c9pcjaqphn4im04b") (r "1.55")))

(define-public crate-sif-kdtree-0.5.2 (c (n "sif-kdtree") (v "0.5.2") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "002an57bdi0qmncgq4bd5380bgxxmb44qhjg0ys0wsxwcibnzajz") (r "1.55")))

(define-public crate-sif-kdtree-0.6.0 (c (n "sif-kdtree") (v "0.6.0") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0037ib6hqx3f0kl2lrxnzyhbaiykdjx0dip3ffryjrs2gfrjl25z") (r "1.55")))

