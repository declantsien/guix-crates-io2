(define-module (crates-io si f- sif-rtree) #:use-module (crates-io))

(define-public crate-sif-rtree-0.1.0 (c (n "sif-rtree") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17yhw1nm7gxp202wflsgwgrsjfpkmyrg2p890z2550da6cwnjkhd") (r "1.55")))

(define-public crate-sif-rtree-0.1.1 (c (n "sif-rtree") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18q9j4s43mlv580bi4y8ag2krq9d5kh0qi1952j10l6clnw3pv2s") (r "1.55")))

(define-public crate-sif-rtree-0.2.0 (c (n "sif-rtree") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n9f4a6f9ajrb3vj8cr38hrdsp8lacv10jgq605ca73baps3qrgr") (r "1.55")))

(define-public crate-sif-rtree-0.2.1 (c (n "sif-rtree") (v "0.2.1") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ksc72zs26dvy9p6i0qs8np5by37zr7r7aa7ha756z7im5n9h9x0") (r "1.55")))

(define-public crate-sif-rtree-0.2.2 (c (n "sif-rtree") (v "0.2.2") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02d79wxbnvahx85ig3b8a33xg0z4gqhbr113w95v9wvqa2jpnc0v") (r "1.55")))

(define-public crate-sif-rtree-0.2.3 (c (n "sif-rtree") (v "0.2.3") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0msghjg6mh6rc3rys1ki8jw2y11l55jyp2l1q4g7ykbkcb56cjmb") (r "1.55")))

(define-public crate-sif-rtree-0.2.4 (c (n "sif-rtree") (v "0.2.4") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m0awv3q2rbdvfnir049m5c1g8am0rq3zyrqcclzgm0czprm9qz2") (r "1.55")))

(define-public crate-sif-rtree-0.2.5 (c (n "sif-rtree") (v "0.2.5") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "168w7ya7m4c4dra561vkyxnabm0y1xccp0axxm7ajc9vnnsf2pdr") (r "1.55")))

(define-public crate-sif-rtree-0.2.6 (c (n "sif-rtree") (v "0.2.6") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mfjwq88pgqn67l6rlrlimpliw60lc3jva90g4xa5smy81m731b9") (r "1.55")))

(define-public crate-sif-rtree-0.2.7 (c (n "sif-rtree") (v "0.2.7") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rpxbnxxrijk44yfkg1l9yzv0fklsss5w9j522q2sbm1x60n9yws") (r "1.65")))

(define-public crate-sif-rtree-0.2.8 (c (n "sif-rtree") (v "0.2.8") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1adxijya0f4k1n5rg2f4qmdzj9a6ysf7xzk0jiv4rs6hh23jcr86") (r "1.65")))

(define-public crate-sif-rtree-0.2.9 (c (n "sif-rtree") (v "0.2.9") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i8lngk5l7q0r9xv9cd3zqw9iy42c37l74kikjhxalv59plgf07y") (r "1.65")))

(define-public crate-sif-rtree-0.2.10 (c (n "sif-rtree") (v "0.2.10") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09avm2w4lv0fkpbfs7a1l63ai0m1wkdn6xlgl54z86ch89jh12cj") (r "1.65")))

(define-public crate-sif-rtree-0.2.11 (c (n "sif-rtree") (v "0.2.11") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rstar") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b9vii4075hz78vaahqfl8p222kdp0njvbn9dhn2jcjrgp3v8jk9") (r "1.65")))

