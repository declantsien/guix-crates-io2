(define-module (crates-io si f- sif-itree) #:use-module (crates-io))

(define-public crate-sif-itree-0.1.0 (c (n "sif-itree") (v "0.1.0") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)))) (h "172v8rvcl4gq8d9lsx04kv0sysqdnxypfz9pwirq661qf1crmflj")))

(define-public crate-sif-itree-0.1.1 (c (n "sif-itree") (v "0.1.1") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)))) (h "00yyaj3648pk95j5mzfraag10h8n0xxxxj4y5zx5x02v1n1nvv2j")))

(define-public crate-sif-itree-0.1.2 (c (n "sif-itree") (v "0.1.2") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)))) (h "0bcqbm8k8p9nzhynhz606lpahwi86rwh8fps8vh3dngsij4wi7ri")))

(define-public crate-sif-itree-0.2.0 (c (n "sif-itree") (v "0.2.0") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02yllg0z0mws5dcsjgybxsz1jgcn7ihrw2vnlp2lziwbzbl3nl3g")))

(define-public crate-sif-itree-0.2.2 (c (n "sif-itree") (v "0.2.2") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vbz36al87w25mqwakdmr7fb9lwy5c101a3qfmmd0nwmabiq032a")))

(define-public crate-sif-itree-0.2.3 (c (n "sif-itree") (v "0.2.3") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x934h9d0xdx62is13cnbxzmla9k1j4q6rp9f6y3pfq3fk22q271")))

(define-public crate-sif-itree-0.2.4 (c (n "sif-itree") (v "0.2.4") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05f10pqa5kqazvncd079rwmqja6gmf195k5mhqwb582wzhm0z68q")))

