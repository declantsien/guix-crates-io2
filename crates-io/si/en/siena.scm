(define-module (crates-io si en siena) #:use-module (crates-io))

(define-public crate-siena-1.0.0 (c (n "siena") (v "1.0.0") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1j5r7r0c4zrlfnf3nm65r6picabnb063zb2y1i33v9ga58qhphwb")))

(define-public crate-siena-1.1.0 (c (n "siena") (v "1.1.0") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1dfq2yzihw6yfjv18k9jwpjnb6bq665wwgmmg33q0x02f6vyq2fd")))

(define-public crate-siena-1.1.1 (c (n "siena") (v "1.1.1") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0z5542pf6p795rcakcfjj40ycymcx1dilqa4vgwl5dwwsw28gqvf")))

(define-public crate-siena-1.1.2 (c (n "siena") (v "1.1.2") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1f87is5bvm9h7d0zmpfwll97d6xdlcf94vgj85fc99cli9a0dnmf")))

(define-public crate-siena-1.1.3 (c (n "siena") (v "1.1.3") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0ki6dskzw6qd2rrh08lc998qbnrdik316s5wgr8fhwr31gw7vl7n")))

(define-public crate-siena-1.1.4 (c (n "siena") (v "1.1.4") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1lhdq7nzr2j2jkqz74sv76gkgyfnwkb71q5wfzyzhpd55zpiv35w")))

(define-public crate-siena-1.2.0 (c (n "siena") (v "1.2.0") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0iijcaqh8sa12z2rynhcy2kkpcl0vbfiwq34818cgs7c8i8md6pa")))

(define-public crate-siena-1.2.1 (c (n "siena") (v "1.2.1") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0rnvbys04i5x61kgmalnpvm6pf5my6cgaxfjk1rx6686fhzdpz6q")))

(define-public crate-siena-1.2.2 (c (n "siena") (v "1.2.2") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "04n9vi43m0p8z60qf0xkf3c00szi06nmh7x6n0vdg979q2x4d6ja")))

(define-public crate-siena-1.3.0 (c (n "siena") (v "1.3.0") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1fayz6rvyx4xl0wsdm5qhdvq9cvc9pz5jyp8pk52wdmk2z5gyhp8")))

(define-public crate-siena-1.3.1 (c (n "siena") (v "1.3.1") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "11f07fdxk2dzcwnbl5f9v62jvb6m7a9m6q4y4jqcs5a8kd4p8y80")))

(define-public crate-siena-1.3.2 (c (n "siena") (v "1.3.2") (d (list (d (n "comrak") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0lni5c44yi26hgyd1jg43n2d40wjydlxh61z6fkp6f90f590hn4b")))

(define-public crate-siena-1.4.0 (c (n "siena") (v "1.4.0") (d (list (d (n "comrak") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "00sf4nxgdrl2ggv09471yvhz82wc7pxgf8kpbssv7j23k162j4ga")))

(define-public crate-siena-1.4.1 (c (n "siena") (v "1.4.1") (d (list (d (n "comrak") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "00y9qz40kzfl1psyflv1bv9rcivxdj0gkckphhfyncxjik5jq2rh")))

(define-public crate-siena-2.0.0 (c (n "siena") (v "2.0.0") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1nnc4zgf2crlfpw77knv59zib1np7q9x9r62w5ypdbh565dqlc6b")))

(define-public crate-siena-2.0.1 (c (n "siena") (v "2.0.1") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0mvifflg0y55cmcw13v1finz7nz6q8smivy3pvwj15ffmi841yll")))

(define-public crate-siena-2.0.2 (c (n "siena") (v "2.0.2") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "137vn6cmlycdv08id5r6ksd3w1aaz75wjnih1pinlhnddgsf9a7h")))

(define-public crate-siena-3.0.0 (c (n "siena") (v "3.0.0") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l1qqjjykrwmknynya1bhpc5359ni9zkz1z7fmq1sfcb0jk738fa")))

(define-public crate-siena-3.0.1 (c (n "siena") (v "3.0.1") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ic98x99jy8nba53zdpw6mdg5dksvi6c6hnincqhlmy826m7n9i4")))

(define-public crate-siena-3.1.0 (c (n "siena") (v "3.1.0") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14wqwszk58wr0pj3dalwn33f6j564fywzbnrj01678njdw98f6xh")))

(define-public crate-siena-3.2.0 (c (n "siena") (v "3.2.0") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fwvi4x5bmz2y5gpqd0ljqr4jrknqq87mfvs4d6nznxq65jn6iic")))

(define-public crate-siena-3.2.1 (c (n "siena") (v "3.2.1") (d (list (d (n "comrak") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g8yvmp2nbgkb39v2q52yq1k0kq4dsmxmvddp562rspw6hp7fx72")))

