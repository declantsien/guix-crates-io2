(define-module (crates-io si go sigo_macro_derive) #:use-module (crates-io))

(define-public crate-sigo_macro_derive-0.1.0 (c (n "sigo_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)))) (h "0wwrjasb20nwkhix3z5gy1hb0i9r4zplfjaclpnh5mmq5r4xygl3")))

(define-public crate-sigo_macro_derive-0.1.2 (c (n "sigo_macro_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)))) (h "0rlfhw104kvqgnkx9kv95q6cwic6cfjnrqm7vknxa6d63wjvrzhg")))

