(define-module (crates-io si go sigop) #:use-module (crates-io))

(define-public crate-sigop-0.1.0 (c (n "sigop") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "17yfn3kaqdmp6zbi4rb1350hig023694lspm52qv1vg2vn8mm60q")))

