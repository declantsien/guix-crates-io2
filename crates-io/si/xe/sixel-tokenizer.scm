(define-module (crates-io si xe sixel-tokenizer) #:use-module (crates-io))

(define-public crate-sixel-tokenizer-0.1.0 (c (n "sixel-tokenizer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xnlg3vfmh96bqj1fnj6qdgjdnl0zc6v07ww2xh4v5mc55k7y6xp")))

