(define-module (crates-io si xe sixel-sys-static) #:use-module (crates-io))

(define-public crate-sixel-sys-static-0.3.2 (c (n "sixel-sys-static") (v "0.3.2") (d (list (d (n "curl-sys") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)) (d (n "mozjpeg-sys") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1ygl11lhwl4dacpc5a8h0x1n0fi0ig9qblvx1a42lf4ra1n89219") (f (quote (("static") ("python_interface") ("png") ("pixbuf" "gdk-pixbuf-sys") ("jpeg" "mozjpeg-sys") ("gd") ("default" "static") ("curl" "curl-sys")))) (l "sixel")))

