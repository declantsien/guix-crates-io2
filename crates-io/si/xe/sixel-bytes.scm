(define-module (crates-io si xe sixel-bytes) #:use-module (crates-io))

(define-public crate-sixel-bytes-0.1.0 (c (n "sixel-bytes") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0l40xri1g8in0rf192dwpd731dnf15wfan70x3nijym91s1g1m5a") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-sixel-bytes-0.2.0 (c (n "sixel-bytes") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1vskdf7xs7mxvq8p7lb03lbzp9xdwnj9q6fjj766dzgfzsw6b30p") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-sixel-bytes-0.2.1 (c (n "sixel-bytes") (v "0.2.1") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.1") (d #t) (k 0)))) (h "06kif0c82m6kwrmg0i9i6vhl4sca5k97r6v66vj05bzz6wknb9ph") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-sixel-bytes-0.2.2 (c (n "sixel-bytes") (v "0.2.2") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.1") (d #t) (k 0)))) (h "175q5akdgg8fw01knm6hh9jp8c4agp2m6zq98zhmplyv6n4m71fc") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-sixel-bytes-0.2.3 (c (n "sixel-bytes") (v "0.2.3") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.2") (d #t) (k 0) (p "sixel-sys-static")))) (h "1z10lzy2g56jrz2sgsad4qz8kbswlplrcd42ac4yhw95lybd5jj5") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image"))))))

