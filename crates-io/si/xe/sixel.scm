(define-module (crates-io si xe sixel) #:use-module (crates-io))

(define-public crate-sixel-0.3.2 (c (n "sixel") (v "0.3.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "semver-parser") (r "^0.6.2") (d #t) (k 0)) (d (n "sixel-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1apak88v07zdf9cwh8jjvnq08syzfxvkygdq0n2168jisv9brr2i")))

