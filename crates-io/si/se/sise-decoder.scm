(define-module (crates-io si se sise-decoder) #:use-module (crates-io))

(define-public crate-sise-decoder-0.1.0 (c (n "sise-decoder") (v "0.1.0") (d (list (d (n "sise") (r "= 0.1.0") (d #t) (k 0)))) (h "0r2fknbbs4xg0pbhs7nxf2vczikap8hgf3c13gsqfigqql3gvs9j") (y #t)))

(define-public crate-sise-decoder-0.2.0 (c (n "sise-decoder") (v "0.2.0") (d (list (d (n "sise") (r "= 0.2.0") (d #t) (k 0)))) (h "169by4m5g8f99f67vpx9za6mfc1258x6n25rrbpbj9dh9rfhxpxv") (y #t)))

