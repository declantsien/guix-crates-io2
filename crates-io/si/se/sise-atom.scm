(define-module (crates-io si se sise-atom) #:use-module (crates-io))

(define-public crate-sise-atom-0.1.0 (c (n "sise-atom") (v "0.1.0") (h "1vmdbk3bi8ricqg3n3q5nj4ldxb4z5wqif64zk7m4skh0x9wiqci") (y #t)))

(define-public crate-sise-atom-0.2.0 (c (n "sise-atom") (v "0.2.0") (h "0vxlkv1s72sqclsm1x42x7v021i5vm6nfq1525l9vw6rbzxs9xp5") (y #t)))

(define-public crate-sise-atom-0.3.0 (c (n "sise-atom") (v "0.3.0") (h "12x4cn0ig8c6d8kixm6yxnzxcyhzgyiqvbgjpidx71bb2zfishj7")))

(define-public crate-sise-atom-0.3.1 (c (n "sise-atom") (v "0.3.1") (h "0l1vd2m8flzryigf0gyskx5zcckdzh5g518srlswg1lr8zbycam9")))

(define-public crate-sise-atom-0.3.2 (c (n "sise-atom") (v "0.3.2") (h "1p7ys4i2fsag8qrkbdn8z0x0za22b2xn7mif2v4l0x6rf5a90l9q")))

(define-public crate-sise-atom-0.3.3 (c (n "sise-atom") (v "0.3.3") (h "1ffacnpn9cyiw15z2k9lcdin847rww6sym0s1c90b9z8llk5mh5l")))

(define-public crate-sise-atom-0.3.4 (c (n "sise-atom") (v "0.3.4") (h "0fyd04wqrras2lqs9wn29iwm46l1994jy0jylydzmpyz03ns1drd")))

(define-public crate-sise-atom-0.4.0 (c (n "sise-atom") (v "0.4.0") (h "00cmirga9ps2f4ymzhlnmxfjjzif4cbszwq2m6wz3943i8zs481m") (y #t)))

(define-public crate-sise-atom-0.5.0 (c (n "sise-atom") (v "0.5.0") (h "0g6kfs9683l8m4l4pwghvippv3h85vbw0rfd496hiv076k8m4935")))

