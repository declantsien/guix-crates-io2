(define-module (crates-io si se sise) #:use-module (crates-io))

(define-public crate-sise-0.1.0 (c (n "sise") (v "0.1.0") (h "1icrx5l845n3waibgk4za0dd35j5cygp106zf0813avrbx3qvxiv") (y #t)))

(define-public crate-sise-0.2.0 (c (n "sise") (v "0.2.0") (h "0qqs7brm8ckz26digh7sx3whdf1faj3fd39gcw3i2hawvqxlydkv") (y #t)))

(define-public crate-sise-0.3.0 (c (n "sise") (v "0.3.0") (h "03zfz2j5hh11c9rx8083z6sv2khcshwzv6di0wi2yj2vkz7srx5m")))

(define-public crate-sise-0.3.1 (c (n "sise") (v "0.3.1") (h "0n6hqy64fcqfl3l9iwgmckkiyw2crlikmmxl9q1w9sbrj2dv93zh")))

(define-public crate-sise-0.4.0 (c (n "sise") (v "0.4.0") (h "0m5qqkpcs0hr3p91p6w3sjx2ivhgss23jhbg6vd5ys3i372wi848")))

(define-public crate-sise-0.5.0 (c (n "sise") (v "0.5.0") (h "1ha8rpr2p7jibvrfnz34y0l219i4ripkvya1hbpyxpgi09c0j6pz")))

(define-public crate-sise-0.5.1 (c (n "sise") (v "0.5.1") (h "13lia96qfp11dc2zpns9gjkhnwv1b4ys1piq0an5rl9vfgj0gpy6")))

(define-public crate-sise-0.6.0 (c (n "sise") (v "0.6.0") (h "1dqs8g4zl4y1a0p9bhb5sr7d4jgw6lpwzpy5l5q627a6jf2lgqam")))

(define-public crate-sise-0.7.0 (c (n "sise") (v "0.7.0") (h "0danqks5r5flc236w4lqy6dachvng631d723j3rrfsca15kdfjkc") (f (quote (("std") ("default" "std"))))))

(define-public crate-sise-0.8.0 (c (n "sise") (v "0.8.0") (h "170dmhp1byjkf28d89nqwmn46wyq682v9cxx8mda2hy8n6y43gmn") (f (quote (("std") ("default" "std")))) (r "1.56")))

