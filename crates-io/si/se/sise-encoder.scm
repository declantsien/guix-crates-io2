(define-module (crates-io si se sise-encoder) #:use-module (crates-io))

(define-public crate-sise-encoder-0.1.0 (c (n "sise-encoder") (v "0.1.0") (d (list (d (n "sise") (r "= 0.1.0") (d #t) (k 0)) (d (n "sise-decoder") (r "= 0.1.0") (d #t) (k 2)))) (h "08nhr8in4jdh5ffq0fac7v383avg8vfg3qp3kzhgy41d5a9br9b7") (y #t)))

(define-public crate-sise-encoder-0.2.0 (c (n "sise-encoder") (v "0.2.0") (d (list (d (n "sise") (r "= 0.2.0") (d #t) (k 0)) (d (n "sise-decoder") (r "= 0.2.0") (d #t) (k 2)))) (h "10v6i788nwbh4v553xgpmj1r9l56iqisz0iwmzsp1dpigp0i124y") (y #t)))

