(define-module (crates-io si mt simt_rocblas_sys) #:use-module (crates-io))

(define-public crate-simt_rocblas_sys-0.1.0 (c (n "simt_rocblas_sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "0aq0v9cq56wb5cl0nwk8cf03byqd43sh7xnc75mgh7l6wrr9wsv6")))

(define-public crate-simt_rocblas_sys-0.2.0 (c (n "simt_rocblas_sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "04pdhihlnqmm3vwxi04c1iiz084bdzs5cprb78lwpdk3arx1ld7s")))

