(define-module (crates-io si mt simt_hip_sys) #:use-module (crates-io))

(define-public crate-simt_hip_sys-0.1.0 (c (n "simt_hip_sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1xvxk7bmaz87rz3qc6x8hfd77fbdly86bd93s65pw9xqvrxj40rs")))

(define-public crate-simt_hip_sys-0.1.1 (c (n "simt_hip_sys") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1931lzp34apsw7i6q3zm45yvrkknsd7jvlnavjl8600lma4v5i2f")))

(define-public crate-simt_hip_sys-0.1.2 (c (n "simt_hip_sys") (v "0.1.2") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "0xf3vd1i83grphf5g73ksaz35zc9kj3s4l2c1mi0d1chm2i0k1d1")))

(define-public crate-simt_hip_sys-0.2.0 (c (n "simt_hip_sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "0mmif5l37xizfx6khl51pbzidq7rdf82qf04ys6mqajs3rdkd864")))

