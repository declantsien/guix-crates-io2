(define-module (crates-io si mt simt) #:use-module (crates-io))

(define-public crate-simt-0.0.1 (c (n "simt") (v "0.0.1") (h "10q7h2jfbx6fqkwksbpm3bjimlinkrxlgss06kgsaiw8h960k6id")))

(define-public crate-simt-0.2.0 (c (n "simt") (v "0.2.0") (d (list (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2") (d #t) (k 0)) (d (n "simt_hip") (r "^0.2") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y1dg6pa3h9pb25likahrrnnwvvra07wq5rgfph8wwmdi875y244")))

(define-public crate-simt-0.2.1 (c (n "simt") (v "0.2.1") (d (list (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2") (d #t) (k 0)) (d (n "simt_hip") (r "^0.2") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rhrxd1ccg5flszai2wzq0d0w6qxbw3kgzn4js1kqkqasbz3f55d")))

(define-public crate-simt-0.2.2 (c (n "simt") (v "0.2.2") (d (list (d (n "simt_core") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_cuda") (r "^0.2.1") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_hip") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b1ychcm3s6m2vbscgwhc9knmhi1jjhgp9054yrr2b7c2lq3ml3b")))

(define-public crate-simt-0.2.3 (c (n "simt") (v "0.2.3") (d (list (d (n "simt_core") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_cuda") (r "^0.2.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_hip") (r "^0.2.0") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jszw3klnjfhnpnbffyx5hxya9qiifyi48dmph7gz2gikw8ra266")))

