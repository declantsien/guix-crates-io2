(define-module (crates-io si mt simt_hip) #:use-module (crates-io))

(define-public crate-simt_hip-0.1.0 (c (n "simt_hip") (v "0.1.0") (d (list (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)))) (h "01sr9arv7smw6gdvraac9994a1dm3lxg8r4pmq0nszimc59mafj5")))

(define-public crate-simt_hip-0.1.1 (c (n "simt_hip") (v "0.1.1") (d (list (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yigxyn3fjcg3iaici7pcchxsgk8p0345wy0x7ax71ws7ajbaapc")))

(define-public crate-simt_hip-0.1.2 (c (n "simt_hip") (v "0.1.2") (d (list (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1shcpakp74kb1svs5w5x40xnvq87mndbda1dp0msc89lwg184irm")))

(define-public crate-simt_hip-0.1.3 (c (n "simt_hip") (v "0.1.3") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08rhmv62b1z9s269nz3hijfq2czniw1gc5cwqr43xrnra2sp94s9")))

(define-public crate-simt_hip-0.1.4 (c (n "simt_hip") (v "0.1.4") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1y030k8kz4wpn6nf2nn4p10v8grx73d7jfdqffw8nkjgqzzwlwj2")))

(define-public crate-simt_hip-0.1.5 (c (n "simt_hip") (v "0.1.5") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0apvp0ypi60qv9gi24wk0fnpb3h0j397y650f7s49jpjmriy8mbw")))

(define-public crate-simt_hip-0.1.6 (c (n "simt_hip") (v "0.1.6") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xzc6hbgzfbm2dazxafg4rgldfznyymvz41d8vn157d9dpvrx4y4")))

(define-public crate-simt_hip-0.2.0 (c (n "simt_hip") (v "0.2.0") (d (list (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_hip_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "188hdv57rpdw0yyz2ri6z7jl4dpfmqs3w6rnxbxd2b56w23jxy68")))

