(define-module (crates-io si mt simt_cuda_sys) #:use-module (crates-io))

(define-public crate-simt_cuda_sys-0.1.0 (c (n "simt_cuda_sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1gq5f2zwwlyqfvm6br9cya54lc65p8hh1m7pbwz16cb8fzcvmkfi")))

(define-public crate-simt_cuda_sys-0.1.1 (c (n "simt_cuda_sys") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "0kjzgmincas2d3mz5gf4zlg2rpjjz1viij288jfas4j4p9kxacb7")))

(define-public crate-simt_cuda_sys-0.2.0 (c (n "simt_cuda_sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1mmf7z6k0fjapfmrr6slkx6zbkl5a9l12hqknm2x3mlzmkydklz7")))

