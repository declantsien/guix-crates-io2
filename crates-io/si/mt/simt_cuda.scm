(define-module (crates-io si mt simt_cuda) #:use-module (crates-io))

(define-public crate-simt_cuda-0.1.0 (c (n "simt_cuda") (v "0.1.0") (h "0fh2rws57i7q6vdq1l0s59ssnbzla430dpl06vklygi3h2nqps9l")))

(define-public crate-simt_cuda-0.2.0 (c (n "simt_cuda") (v "0.2.0") (d (list (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qyvh4fdc0vpaw7grwhddz8822kj9l1rxfqq9z0dzgxq7l6x7n2z")))

(define-public crate-simt_cuda-0.2.1 (c (n "simt_cuda") (v "0.2.1") (d (list (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17wnzs8s24lkkf3l5rsd5fl216gdr392p9iyxjvhpfi9fbqvd6pk")))

(define-public crate-simt_cuda-0.2.2 (c (n "simt_cuda") (v "0.2.2") (d (list (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "simt_core") (r "^0.2") (d #t) (k 0)) (d (n "simt_cuda_sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p4ixwmxi8bar26nlhpkf1kbqwzmp93v9kz5mfjkfdqpg1yh8p74")))

