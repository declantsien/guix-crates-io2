(define-module (crates-io si m- sim-rust) #:use-module (crates-io))

(define-public crate-sim-rust-0.1.0 (c (n "sim-rust") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0r1rwdcsrihyizdkk3dhqd6hhgjhkgf1vfzx3nl1bp05j92f9fml")))

(define-public crate-sim-rust-0.2.0 (c (n "sim-rust") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0xjjcjypcmhb6swbb36qwlfzh9wvlpk7gipnm6z2h0a325ibrv2j")))

(define-public crate-sim-rust-0.2.1 (c (n "sim-rust") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0026jmsrv52w6dmfvk2d40p9j3bqs64f93mvsnwzaqfy18q9dsb8")))

(define-public crate-sim-rust-0.2.2 (c (n "sim-rust") (v "0.2.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1srzg5lnaz9vnqrwh3khmgffs5lq0n06dx2gg0wlqcdd44p6hm8l")))

(define-public crate-sim-rust-0.3.0 (c (n "sim-rust") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0ri193fas9n0dgj9hl68zxpyp66gvhzs0gld8frnpwy235axjlh1")))

(define-public crate-sim-rust-0.3.1 (c (n "sim-rust") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "10v4915sf2nhhlqs3xdjw04zflh1sn9a36s21bdfj3kkx2vwq542")))

(define-public crate-sim-rust-0.4.0 (c (n "sim-rust") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1kbd8bi14xvp0n760zvy1d495y7d3rbcq7lxdmmlbj04pyxi8874")))

(define-public crate-sim-rust-0.4.1 (c (n "sim-rust") (v "0.4.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0sn9v98ms39iikaj5k2p6vny22gq8cyx31j06rkcysk263477pck")))

(define-public crate-sim-rust-0.4.2 (c (n "sim-rust") (v "0.4.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0m9qlsdqkjwilapaqk237ya7h9vmb4praybn68is70jginc1154i")))

(define-public crate-sim-rust-0.4.3 (c (n "sim-rust") (v "0.4.3") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0k7gc2d2x49j01y8b70ip5h6kb27yzha2vyvflhdvdg4983h78kc")))

(define-public crate-sim-rust-0.5.0 (c (n "sim-rust") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0ikq01x6ff0846c7bznakvr2q4776vgwgqnb3khs9xcx2n5s2pra")))

(define-public crate-sim-rust-0.5.1 (c (n "sim-rust") (v "0.5.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1msc7sna1pk7blbl7hj17zcqwv73hq7mnj5h4g4xc5z14ajhf1yi")))

(define-public crate-sim-rust-0.6.0 (c (n "sim-rust") (v "0.6.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1ffgyxh35yhmjdqcxw06740yqcsk35llzs48cif0gp2wrdh40lw0")))

(define-public crate-sim-rust-0.7.0 (c (n "sim-rust") (v "0.7.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0d234piif77j49kkrr7spdsvs7rpmxd8jwd33gy89269nyw75ifv")))

(define-public crate-sim-rust-0.8.0 (c (n "sim-rust") (v "0.8.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "13j6njrigaqmcpjd3mqxr9fa5hx8xvcd911nyp7s1amjd7nlirl8")))

(define-public crate-sim-rust-0.8.1 (c (n "sim-rust") (v "0.8.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "16x7cq4wi3lv4s6ix0f4yy7p31sm157x5rz9qxqh6cpdy1ld09iw")))

(define-public crate-sim-rust-0.8.2 (c (n "sim-rust") (v "0.8.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1nhqc1anclr8crmrx7kac46kfiwvwyswnfdvg8j7lnwlmlc9q508")))

(define-public crate-sim-rust-0.8.3 (c (n "sim-rust") (v "0.8.3") (h "12w5yypwmhqxszya38dp7v1lzllapm51c6x5y12swy5j7gkhaps3")))

(define-public crate-sim-rust-0.8.4 (c (n "sim-rust") (v "0.8.4") (h "19z9lfmfhrvmrg2mf3n118k4ja6v1cssin40c3vpzgi9n3hb60ks")))

(define-public crate-sim-rust-0.9.0 (c (n "sim-rust") (v "0.9.0") (h "148wkv2a22i00xm26jc5pdiszyg6nphqdp1zj3d0kfrrf1anzzgw")))

