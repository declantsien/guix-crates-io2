(define-module (crates-io si m- sim-by-fired-event) #:use-module (crates-io))

(define-public crate-sim-by-fired-event-0.1.0 (c (n "sim-by-fired-event") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "084q19975yij1f4h7363hpxvg5axlf5i9zglnnrzd7h90ry69xxs")))

(define-public crate-sim-by-fired-event-0.1.1 (c (n "sim-by-fired-event") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1shs54hqmvpkabbb8d6sv9k809im56crg6sacdgcxd28czrwqh5r")))

