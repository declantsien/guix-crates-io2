(define-module (crates-io si es siesta) #:use-module (crates-io))

(define-public crate-siesta-0.1.0 (c (n "siesta") (v "0.1.0") (h "0p8v6gsyk66rcwjmqvcv3fa1ndh93sf2f2lvdkjllirqklpammxq")))

(define-public crate-siesta-0.1.1 (c (n "siesta") (v "0.1.1") (h "11gaanisrbgdkq006zqpypg18b0ghi2dp0dh2rc35xn117s02zab")))

(define-public crate-siesta-0.1.2 (c (n "siesta") (v "0.1.2") (h "15g9i6s0mjpfhqaixh9b40svba867f6zl0z6vr4xarvzjhw8yljq")))

(define-public crate-siesta-0.1.3 (c (n "siesta") (v "0.1.3") (h "0g6a5hvh9089pcdaqbbn6im2fhd8aki9y3009w0lri3bpc8zsr4s")))

(define-public crate-siesta-0.1.4 (c (n "siesta") (v "0.1.4") (h "1pn6qyaadn20h5k613cqw3pyqq937v5idycgcbx7yqfra566x129")))

(define-public crate-siesta-0.1.5 (c (n "siesta") (v "0.1.5") (h "09nikk0408qyl484hcz4msz4b88x8ypdbaf83j6r7v7nbxq3nywb")))

(define-public crate-siesta-0.1.6 (c (n "siesta") (v "0.1.6") (h "18q4vlxwwl04l7hljrlk2h9p4gbrc7mh93c114s7ngvg42la48xk")))

(define-public crate-siesta-0.1.7 (c (n "siesta") (v "0.1.7") (h "014mhq6hcvp4a3vzih6bdnqqpqncmxzkfk6aafgcm8c60i196z0h")))

(define-public crate-siesta-0.1.8 (c (n "siesta") (v "0.1.8") (h "0hnwvni3b13y6czrhwqf8qnj82ifqbfcq555zjc617pmrwghs98h")))

