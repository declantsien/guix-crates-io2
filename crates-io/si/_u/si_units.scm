(define-module (crates-io si _u si_units) #:use-module (crates-io))

(define-public crate-si_units-0.1.0 (c (n "si_units") (v "0.1.0") (h "0i270pimvsr2wrr4k6lh2dva18isx5axha11dlwamaxpcwzaj01y")))

(define-public crate-si_units-0.1.1 (c (n "si_units") (v "0.1.1") (d (list (d (n "prse") (r "^1.2.0") (d #t) (k 0)))) (h "13pmdvs65wrl9q5sx8scxxz2c7wg6fifximij1l53sw2wrdkx7mm")))

(define-public crate-si_units-0.1.2 (c (n "si_units") (v "0.1.2") (d (list (d (n "prse") (r "^1.2.0") (d #t) (k 0)))) (h "0lx2nh5xh6wzvvj7b6b8by9s21awjzgfj87vfg3dwdqldc4wi2pi")))

