(define-module (crates-io si tr sitronix-touch) #:use-module (crates-io))

(define-public crate-sitronix-touch-0.0.1 (c (n "sitronix-touch") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "02rgf3d9xardqw5sl53jjzyb24yr0dgfxlqsjlf2xxhnpzhh6kvk")))

