(define-module (crates-io si tr sitrep) #:use-module (crates-io))

(define-public crate-sitrep-0.1.0 (c (n "sitrep") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1qkb6pancwishv6rba9s6iq2mp1mm2r5900l5c2kq06srrk4v6a3") (f (quote (("test-utils") ("deadlocks-debugger" "parking_lot/deadlock_detection")))) (r "1.70.0")))

(define-public crate-sitrep-0.2.0 (c (n "sitrep") (v "0.2.0") (d (list (d (n "clap_builder") (r "=4.4.18") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1in0mbfwwmv13zkgzisqgv5j1a6m2adn16hz0rqiwpzsc1pfwr5p") (f (quote (("test-utils") ("deadlocks-debugger" "parking_lot/deadlock_detection")))) (r "1.70.0")))

(define-public crate-sitrep-0.3.0 (c (n "sitrep") (v "0.3.0") (d (list (d (n "clap_builder") (r "=4.4.18") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0bhvzw83d850cwz4ryvc2inw12g506ygznxv2n087vlj3k2jiakx") (f (quote (("test-utils") ("deadlocks-debugger" "parking_lot/deadlock_detection")))) (r "1.70.0")))

(define-public crate-sitrep-0.3.1 (c (n "sitrep") (v "0.3.1") (d (list (d (n "bumpalo") (r "^3.16.0") (d #t) (k 2)) (d (n "clap_builder") (r "=4.4.18") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)))) (h "1mscw0ibxwv005l14bj6i1x5xjcrykkzwn2z6891wp93jwvrc5di") (f (quote (("test-utils") ("deadlocks-debugger" "parking_lot/deadlock_detection")))) (r "1.70.0")))

