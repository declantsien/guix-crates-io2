(define-module (crates-io si gn signatory-client-lib-rs-mg) #:use-module (crates-io))

(define-public crate-signatory-client-lib-rs-mg-0.1.0 (c (n "signatory-client-lib-rs-mg") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "12vzgr0ilix2gc8fq7d4wyv6sdrs9gs5wm58ccj0hzrcxwxsyk4d")))

