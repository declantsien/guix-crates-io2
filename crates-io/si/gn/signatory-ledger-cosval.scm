(define-module (crates-io si gn signatory-ledger-cosval) #:use-module (crates-io))

(define-public crate-signatory-ledger-cosval-0.9.0 (c (n "signatory-ledger-cosval") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ledger-cosmos") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signatory") (r "^0.9") (f (quote ("digest" "ed25519" "generic-array" "test-vectors"))) (d #t) (k 0)))) (h "0w7cm18fbfqw694frl68dz26n69fld1npj95f1cz7z3qiamvdx83") (y #t)))

(define-public crate-signatory-ledger-cosval-0.10.0 (c (n "signatory-ledger-cosval") (v "0.10.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ledger-cosmos") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signatory") (r "^0.10") (f (quote ("digest" "ed25519" "generic-array" "test-vectors"))) (d #t) (k 0)))) (h "1gbd5ss281zh38i431j99jjnsp6f6g74c9b5f33yrpxkv0r9kv7x") (y #t)))

(define-public crate-signatory-ledger-cosval-0.99.0 (c (n "signatory-ledger-cosval") (v "0.99.0") (h "1fdgmas2n9gmp9ail2nrkc6ycnl617g4w1yicq4yvs67pk4f25a0") (y #t)))

