(define-module (crates-io si gn signed-distance-field) #:use-module (crates-io))

(define-public crate-signed-distance-field-0.6.0 (c (n "signed-distance-field") (v "0.6.0") (d (list (d (n "half") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0z7bzpdvskx5l22a9giapx0w93mil7x56dqngxs6nhhcmld9wkfw") (f (quote (("piston_image" "image"))))))

(define-public crate-signed-distance-field-0.6.2 (c (n "signed-distance-field") (v "0.6.2") (d (list (d (n "half") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0vrprrwz76sfhzy52lxp30nppmyn8axgk4459av9f9rr1ff7n7d2") (f (quote (("piston_image" "image"))))))

(define-public crate-signed-distance-field-0.6.3 (c (n "signed-distance-field") (v "0.6.3") (d (list (d (n "half") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "05gdr5b0yrxpcm4lcdvy1imsk2v6cn1sr32caxwgxag5djdbqiph") (f (quote (("piston_image" "image"))))))

