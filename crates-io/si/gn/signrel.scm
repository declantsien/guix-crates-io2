(define-module (crates-io si gn signrel) #:use-module (crates-io))

(define-public crate-signrel-1.0.0 (c (n "signrel") (v "1.0.0") (h "0hvzd3av1warxs0vi30p34s1fd8nfg7cczkmmwsxy9w7wfyvqp5a")))

(define-public crate-signrel-1.0.1 (c (n "signrel") (v "1.0.1") (h "00b6zn4bdjmp859h67mpj3jcgrfgqndxnrp4dpwc8ijnq1jd92pk")))

(define-public crate-signrel-1.0.2 (c (n "signrel") (v "1.0.2") (h "0d8jhav2hphzwddh8l2n99y0n4jsn4fwrlrx2p4hsg1jlzy11q81")))

(define-public crate-signrel-2.0.0 (c (n "signrel") (v "2.0.0") (h "17ywcaq765916a7jjjlsl0ahgikvqhn8lqf6pdl8yw42pxxwd0zy")))

