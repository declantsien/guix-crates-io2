(define-module (crates-io si gn signin) #:use-module (crates-io))

(define-public crate-signin-0.1.0 (c (n "signin") (v "0.1.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0c92y8n11yy0ikkab0c709qyjsx3rpw6pxnli5wlqs1imj7dvm7r")))

(define-public crate-signin-0.1.1 (c (n "signin") (v "0.1.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "05jiiz5hg1mwvj99mskggvnnpyi7f6y18kk0v6nvgcnqlw4bl6b0")))

(define-public crate-signin-0.1.2 (c (n "signin") (v "0.1.2") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1n6ns43r60g3hbd7n81bhh111lfjk0mby0ljg5z0rqy7zz3rr83b")))

(define-public crate-signin-0.1.3 (c (n "signin") (v "0.1.3") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1h9s4css7a2hs2frd0ha50zbpibirzhj2sfskb91vh71c1r0h406")))

(define-public crate-signin-0.1.4 (c (n "signin") (v "0.1.4") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1x00yzxfj5szn6lil7japkl87ia2rfmn35b65pflms8d5b0gp39l")))

(define-public crate-signin-0.1.5 (c (n "signin") (v "0.1.5") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "debug_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0h5xj80p258vg62dafp5vylhdczyw4z1rkcs7pyribza2im705ra")))

(define-public crate-signin-0.1.6 (c (n "signin") (v "0.1.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "debug_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0px0hhh95snzcx3gnx7b2pwf11axfc9j6na3cq3408w8ciacnnqi")))

