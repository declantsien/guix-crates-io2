(define-module (crates-io si gn signum) #:use-module (crates-io))

(define-public crate-signum-0.1.0 (c (n "signum") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (o #t) (k 0)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w8sgrbrqqwg0d1yx2r3nwaq1vkl7qcdkcxzmzm4jra6sfspn3hb")))

(define-public crate-signum-0.2.0 (c (n "signum") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (o #t) (k 0)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17gzmc5f7xdkydj425ff862vgc2f3ljvy2yyyb6rwjg767aa2539")))

(define-public crate-signum-0.2.1 (c (n "signum") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (o #t) (k 0)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zx1abz2d4kl4xcpm62412r6ylrnnv62ad4h9jjqqxl9714p853f")))

