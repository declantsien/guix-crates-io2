(define-module (crates-io si gn signal-notify) #:use-module (crates-io))

(define-public crate-signal-notify-0.1.0 (c (n "signal-notify") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10yzgn26nrvkp9j2prsz21c2d4b438jc2zyacyk8c65alxf5cvba")))

(define-public crate-signal-notify-0.1.1 (c (n "signal-notify") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13gg6z3mjlgimafqgc53jw24vkprxjdd1hrnnnl96qiazydvcykl")))

(define-public crate-signal-notify-0.1.2 (c (n "signal-notify") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h85xygaqnqys2p2n85xcyvcg5rypm2984b2y928q6ch6idr6662")))

(define-public crate-signal-notify-0.1.3 (c (n "signal-notify") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kigfhj00jmrdkk9ybdfx9c6ccx6c7xa3vvv0yvgprn2szdbwzw4")))

