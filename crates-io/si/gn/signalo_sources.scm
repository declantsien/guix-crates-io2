(define-module (crates-io si gn signalo_sources) #:use-module (crates-io))

(define-public crate-signalo_sources-0.4.0 (c (n "signalo_sources") (v "0.4.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.3") (k 0)))) (h "14wpkq24al94qs247a903qdc8qjbzsh18bda14105jbmrsxranih") (f (quote (("std" "signalo_traits/std" "num-traits/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sources-0.5.0 (c (n "signalo_sources") (v "0.5.0") (d (list (d (n "dimensioned") (r "~0.7") (o #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.4") (k 0)))) (h "0z81mnjb6kp3qmnnmlkzbihhjgxqyxfagln7hfg44dbh1sk2sl0j") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sources-0.5.1 (c (n "signalo_sources") (v "0.5.1") (d (list (d (n "dimensioned") (r "~0.7") (o #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "nearly_eq") (r "~0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.4") (k 0)))) (h "0b1wcfy33d0ljlfkkzzybhmxy2dvl4y8p4iq6dbd3mmbffydwxx7") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sources-0.5.2 (c (n "signalo_sources") (v "0.5.2") (d (list (d (n "dimensioned") (r "^0.7") (o #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "signalo_traits") (r "^0.5") (k 0)))) (h "1gffizwkb0jbvrs2pjjryx4ydaan00c864i8lm3s7h514fvxsb6g") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("default"))))))

