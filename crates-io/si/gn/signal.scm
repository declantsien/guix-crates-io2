(define-module (crates-io si gn signal) #:use-module (crates-io))

(define-public crate-signal-0.1.0 (c (n "signal") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)))) (h "0n9lwnplwil58n4gy8zzkzwib8d1pbpkzjacplpkym71jcvgc6gc")))

(define-public crate-signal-0.1.1 (c (n "signal") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "10yz675j4r45l84isw1d5dgvf4c0vxbx99ssax3lyqnxj1g96pb7")))

(define-public crate-signal-0.1.2 (c (n "signal") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "10xmmczp1labc217gnyls9j6g6m4gzzrpc3szglqhrdgla4zm8kw")))

(define-public crate-signal-0.1.3 (c (n "signal") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "04qzag10m0hy4fj3hrf4cnymkmqhx8zxahc6f74w15ah6ldnpv6h")))

(define-public crate-signal-0.1.4 (c (n "signal") (v "0.1.4") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "19pzpg5ml6b0gwa08dgf5azzhffzi5bfw66iwqgrwqqlr6yqn3i8")))

(define-public crate-signal-0.2.0 (c (n "signal") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0bai32r5a3vb2r7wymhhbjaj926qy8hgaz17cdh5nqwri7jibdmy")))

(define-public crate-signal-0.3.0 (c (n "signal") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "1zmc8b24n9g4l16chb3xbi04lvvcva8bwz459xdsa1l39194w94n") (y #t)))

(define-public crate-signal-0.3.1 (c (n "signal") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0gcyz5z10xjhgbq2cmskmlv68gp41wa8ibfv97w4vwga4n8n1fm1") (y #t)))

(define-public crate-signal-0.3.2 (c (n "signal") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0d5aa7isb0vahhj9vapm7pzfm72a11cqzv3yg8xsbrz8c2x4njlh")))

(define-public crate-signal-0.4.0 (c (n "signal") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "11fsqhh43qy2ka248yyslv80n658zpd0hmph9l8x3l3b1bv5yls0")))

(define-public crate-signal-0.4.1 (c (n "signal") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "12h8qr0g269l813w21m8gqsm03w4v3d6qb35h2vwn1dx3a29ij02")))

(define-public crate-signal-0.5.0 (c (n "signal") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0yd6ym63pw67x9mykhc989bzw1h2zx17x7lwyhphz1yx74h9j5gb")))

(define-public crate-signal-0.5.1 (c (n "signal") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1djkjdm4yw4lwacz7bxjw1ni35fw5bcp3mcibvqm4a1x2cfg4m2d")))

(define-public crate-signal-0.6.0 (c (n "signal") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "15zqjawl2kmlky8g5k70l66w6a2fiami7310qpgfqh38v7cjhr0h")))

(define-public crate-signal-0.7.0 (c (n "signal") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)))) (h "1nbfnm19jcwqr7wf7wizs5a8gd3j949rbx0r4i6ridls2lxyhv1g")))

