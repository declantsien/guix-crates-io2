(define-module (crates-io si gn signatory-sodiumoxide) #:use-module (crates-io))

(define-public crate-signatory-sodiumoxide-0.7.0-alpha1 (c (n "signatory-sodiumoxide") (v "0.7.0-alpha1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.7.0-alpha1") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0vfzn0v1qflzc1hb9sxccfd1adqdflr0f9lxskyf1i6j75j0af38") (y #t)))

(define-public crate-signatory-sodiumoxide-0.7.0 (c (n "signatory-sodiumoxide") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.7") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "072s8q4i4fc9mzy7ggih2bwxninlf118gk1g2vkzlqm25yf253fq") (y #t)))

(define-public crate-signatory-sodiumoxide-0.8.0 (c (n "signatory-sodiumoxide") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.8") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0mllzvijpg9p01lnfn722hjv3dsaiia192v5p5dnngkmb412ay06") (y #t)))

(define-public crate-signatory-sodiumoxide-0.9.0-alpha2 (c (n "signatory-sodiumoxide") (v "0.9.0-alpha2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.9.0-alpha2") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0xc94p7391wkx8ix889l9ashk6wsnjslklbjwj86ickgcbjnnj9q") (y #t)))

(define-public crate-signatory-sodiumoxide-0.9.0-alpha3 (c (n "signatory-sodiumoxide") (v "0.9.0-alpha3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.9.0-alpha3") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0qppzismcvz2hx025zjyxjb7ciwx3an69afq8wcph2vsb7lwgl1y") (y #t)))

(define-public crate-signatory-sodiumoxide-0.9.0 (c (n "signatory-sodiumoxide") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.9") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0f97xgb6myplj86kn97b3mch2jly2ykh8cprvvm4bgvh2gc5dgma") (y #t)))

(define-public crate-signatory-sodiumoxide-0.10.0 (c (n "signatory-sodiumoxide") (v "0.10.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.10") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "1w6dsfjkbp84lxpp70pdcdnbqbkmyys3yrvz3wlf962nvc2d3ql1") (y #t)))

(define-public crate-signatory-sodiumoxide-0.11.0 (c (n "signatory-sodiumoxide") (v "0.11.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.11") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1ypazlf50pd5ykfwciy6wb206fmlz3a6a9pvbdmqwlkpz5ib8h95") (y #t)))

(define-public crate-signatory-sodiumoxide-0.12.0 (c (n "signatory-sodiumoxide") (v "0.12.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.12") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1zr54zrzpz9fmg8bzp9qql7ypyybzsqmgax7fa12cribx1pyp921") (y #t)))

(define-public crate-signatory-sodiumoxide-0.13.0 (c (n "signatory-sodiumoxide") (v "0.13.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.13") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0ksdwp1dsyffb8myj977297zq96xnm4v9jbdysyxdr52p2lc0mjb") (y #t)))

(define-public crate-signatory-sodiumoxide-0.14.0 (c (n "signatory-sodiumoxide") (v "0.14.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.14") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0s40qf7m0s08fpyqxg3za0bhrqmqf6qc13c9w7zdc94w651li81z") (y #t)))

(define-public crate-signatory-sodiumoxide-0.15.0 (c (n "signatory-sodiumoxide") (v "0.15.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.15") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "10m2lfgqmavl97bvaby2z5sni4hc561hbamj8p33p9qbjcldj18r") (y #t)))

(define-public crate-signatory-sodiumoxide-0.16.0 (c (n "signatory-sodiumoxide") (v "0.16.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.16") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1sa226pqhlbs82gylb2d4fd8ckdzxzm548wckg9zvfa3jmqpfqg7") (y #t)))

(define-public crate-signatory-sodiumoxide-0.17.0 (c (n "signatory-sodiumoxide") (v "0.17.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.17") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0g997a15r9wp9fhb2blzxd7gq9ki0wgb5i4yxv56yh4xpql8ba7r") (y #t)))

(define-public crate-signatory-sodiumoxide-0.18.0 (c (n "signatory-sodiumoxide") (v "0.18.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "signatory") (r "^0.18") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "04h325wwf2rwb0bp3rba0szqcg979ib8zwldbapyf61qj8syp0m2") (y #t)))

(define-public crate-signatory-sodiumoxide-0.18.1 (c (n "signatory-sodiumoxide") (v "0.18.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "signatory") (r "^0.18") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "16lsx2w4zmfbs43ylkqxlpydxq9ckkmmpjmm3mk4m9b8xhvyapa7") (y #t)))

(define-public crate-signatory-sodiumoxide-0.19.0 (c (n "signatory-sodiumoxide") (v "0.19.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "signatory") (r "^0.19") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "15klbnj9w0q4hxwn9wk867gmvrxp5d8c0ambvvx87s5v2sf43p7q") (y #t)))

(define-public crate-signatory-sodiumoxide-0.20.0 (c (n "signatory-sodiumoxide") (v "0.20.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "signatory") (r "^0.20") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1gpjii0ig7dgnvhxbim8lhwzc65d38cmdhiq9j84sm06dshvk5qw") (y #t)))

(define-public crate-signatory-sodiumoxide-0.21.0 (c (n "signatory-sodiumoxide") (v "0.21.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "signatory") (r "^0.21") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "signatory") (r "^0.21") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "19lxwnx9lp4b578x4xn9vzc497ccj4s1if145n685fhk72hfk26l") (y #t)))

(define-public crate-signatory-sodiumoxide-0.22.0 (c (n "signatory-sodiumoxide") (v "0.22.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "signatory") (r "^0.22") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "signatory") (r "^0.22") (f (quote ("ed25519" "test-vectors"))) (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0fis2s4k25xrf4472vvwj6nk2a9hlygsgg0l7px990c2qjsa676w") (y #t)))

(define-public crate-signatory-sodiumoxide-0.99.0 (c (n "signatory-sodiumoxide") (v "0.99.0") (h "0b2bfddbr5v8r1hs0fr1p51qx3msgyc6nslg5cjp70xpdr47n5yw") (y #t)))

