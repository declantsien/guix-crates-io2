(define-module (crates-io si gn signed) #:use-module (crates-io))

(define-public crate-signed-0.1.0 (c (n "signed") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q8q0zl6b2qqhzdsy6dgp7rnmrixkh13cwa8n15k423wcb7d1zg0")))

(define-public crate-signed-0.1.1 (c (n "signed") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0qcxcari7sjgwqyg14c69rb6l73fvx7dwlqspiphzn7aqxkf561x")))

