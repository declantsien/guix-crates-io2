(define-module (crates-io si gn signal-child) #:use-module (crates-io))

(define-public crate-signal-child-1.0.0 (c (n "signal-child") (v "1.0.0") (h "1kfpd5hv2mf2ic76kxnj8m9i20a9jladj85wi52nxxvzr42hgfsl")))

(define-public crate-signal-child-1.0.1 (c (n "signal-child") (v "1.0.1") (h "1ivh7chm1x4m1w4l07jw4lfc7r8zm1mdqc1v8nmygz2yyam4kfap")))

(define-public crate-signal-child-1.0.2 (c (n "signal-child") (v "1.0.2") (h "0djf6k3s5d0d6ibj6v7m9h04dqxqp0qgl4bxx3r1igc6r22fmx19")))

(define-public crate-signal-child-1.0.3 (c (n "signal-child") (v "1.0.3") (h "1dyvy9rw06cybvv1f4q03rl42m9av1lg99vrdba7dd68snw3p5yp")))

(define-public crate-signal-child-1.0.4 (c (n "signal-child") (v "1.0.4") (h "1aha1k0dx7f64xfmlppnwgwxzb1csj21psw59fq2s5k95mpjbdxl")))

(define-public crate-signal-child-1.0.5 (c (n "signal-child") (v "1.0.5") (h "0vs2zsjwjdyqry2zakzlhvvi443mpc4033mbf2246f5fqpafx95j")))

