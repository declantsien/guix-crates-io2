(define-module (crates-io si gn signifix) #:use-module (crates-io))

(define-public crate-signifix-0.1.0 (c (n "signifix") (v "0.1.0") (h "0yvw8hxsjfgj2vk6drkm4b6w1vdzyxlg3iaks5g5r1x8nj50q199")))

(define-public crate-signifix-0.2.0 (c (n "signifix") (v "0.2.0") (h "1w86ihz5dy8cry5zscp7jq3842y10j80kx40zp4hlf7nb6garmg8")))

(define-public crate-signifix-0.3.0 (c (n "signifix") (v "0.3.0") (h "1w4hpz8njbmmv8nvgg12m91mf29hpdamm692c954pj6jvg0d3w77")))

(define-public crate-signifix-0.4.0 (c (n "signifix") (v "0.4.0") (h "1rrck5ppfds7rxd0wvmdm67wg2dpafszlsfl4nicf3lh7nwsgigs")))

(define-public crate-signifix-0.4.1 (c (n "signifix") (v "0.4.1") (h "133gn2ffv3ymsywicd33rmz44xhgqkzdis54pqq1rh67wrlkhdzj")))

(define-public crate-signifix-0.5.0 (c (n "signifix") (v "0.5.0") (h "0qp1aipan0278kcs4hsmbsajrmfswjksb2rmp2jvsf8mkyzr66nr")))

(define-public crate-signifix-0.6.0 (c (n "signifix") (v "0.6.0") (h "0y2ps422p9wwnw9pyzh9i3rgdm2x9igyqmz0h02rds386ih22kyg")))

(define-public crate-signifix-0.7.0 (c (n "signifix") (v "0.7.0") (h "07dclnc551calg0kwdxq9nwink2ig8vfw336ahs284k3fwgd57rd")))

(define-public crate-signifix-0.8.0 (c (n "signifix") (v "0.8.0") (h "08sgva4yn2awyvvw5jla0i6mn5l02f4mfs7b8haiqzjgx4sncxzs") (f (quote (("nightly"))))))

(define-public crate-signifix-0.9.0 (c (n "signifix") (v "0.9.0") (h "00agry4r9fr5797jxp1w0cqx47hz4ya7zys74mirckns1w17qkps") (f (quote (("nightly"))))))

(define-public crate-signifix-0.10.0 (c (n "signifix") (v "0.10.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)))) (h "1036cscc831a9a1x75zhgf50zghlg7dbgn4hrqbb73w4xv9j94mj")))

(define-public crate-signifix-0.10.1 (c (n "signifix") (v "0.10.1") (d (list (d (n "err-derive") (r "^0.2") (d #t) (k 0)))) (h "0cm1m8mq52i0pv1hwxfrsr843d8wginrpfrlp9yw33jiwbsk6ivb")))

