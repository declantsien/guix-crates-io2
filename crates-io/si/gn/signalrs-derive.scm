(define-module (crates-io si gn signalrs-derive) #:use-module (crates-io))

(define-public crate-signalrs-derive-0.1.0 (c (n "signalrs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "00qdpmi2c60jhzzkzkhv8di73cshwdjdnyfq3r2skpfn6bp8sy4w")))

