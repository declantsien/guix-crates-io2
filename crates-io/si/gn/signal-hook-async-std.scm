(define-module (crates-io si gn signal-hook-async-std) #:use-module (crates-io))

(define-public crate-signal-hook-async-std-0.1.0 (c (n "signal-hook-async-std") (v "0.1.0") (d (list (d (n "async-std") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "async-std") (r ">=1.0.0, <2.0.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "serial_test") (r ">=0.5.0, <0.6.0") (d #t) (k 2)) (d (n "signal-hook") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1iz6aw1dlkkywqgyrpsg81ki7ayaj8hnza1f171428yvww6qj1hn")))

(define-public crate-signal-hook-async-std-0.2.0 (c (n "signal-hook-async-std") (v "0.2.0") (d (list (d (n "async-std") (r "~1") (d #t) (k 0)) (d (n "async-std") (r "~1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serial_test") (r "~0.5") (d #t) (k 2)) (d (n "signal-hook") (r "~0.3") (d #t) (k 0)))) (h "1hrrj90ih6snbys5a7xpif8m6j9g0cs10zw88m24ipcg3zch1wj5")))

(define-public crate-signal-hook-async-std-0.2.1 (c (n "signal-hook-async-std") (v "0.2.1") (d (list (d (n "async-io") (r "~1") (d #t) (k 0)) (d (n "async-std") (r "~1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-lite") (r "~1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serial_test") (r "~0.5") (d #t) (k 2)) (d (n "signal-hook") (r "~0.3") (d #t) (k 0)))) (h "1w8s6ynnggrks7m3qsz5dffxw02bv97ksbi172dsfs8wcds6wllh")))

(define-public crate-signal-hook-async-std-0.2.2 (c (n "signal-hook-async-std") (v "0.2.2") (d (list (d (n "async-io") (r "~1") (d #t) (k 0)) (d (n "async-std") (r "~1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-lite") (r "~1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serial_test") (r "~0.5") (d #t) (k 2)) (d (n "signal-hook") (r "~0.3") (d #t) (k 0)))) (h "0mp5mic65cp3raqamr7hfzxmr4syg3abixfgnzskl0p2jx1sjjhc")))

