(define-module (crates-io si gn signtool) #:use-module (crates-io))

(define-public crate-signtool-1.0.0 (c (n "signtool") (v "1.0.0") (d (list (d (n "forensic-rs") (r "^0.5.2") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1pmriqqx79al7wn8rgpjh8f4y8d2x6kvvj0chvk9rl2d1dzysp0m")))

(define-public crate-signtool-1.0.1 (c (n "signtool") (v "1.0.1") (d (list (d (n "forensic-rs") (r "^0.5.2") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1c520h26fr3avbg0l4c3qn6m3ad4s6sh6bdl3bb8n30p41vlv9fj")))

(define-public crate-signtool-1.0.2 (c (n "signtool") (v "1.0.2") (d (list (d (n "forensic-rs") (r "^0.5.2") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0nmsi4lqik0psxj60blpxd7nf6cj586qqwy9kxlfx86x3vhq4cq2")))

(define-public crate-signtool-1.0.3 (c (n "signtool") (v "1.0.3") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bjwx03xvr218r6wxalldl43is1j04bvlrn9ngh4wigl7rbvxnxk")))

(define-public crate-signtool-1.0.4 (c (n "signtool") (v "1.0.4") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1pdf0npm2nmbrdqpk5n0ywy32sh2rbqsb35rxbl7215sd1jbprfv")))

(define-public crate-signtool-1.1.0 (c (n "signtool") (v "1.1.0") (d (list (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "13sv7vsgdj982lw4bcqv9mi39smx7qgna5r8c3s4hp123iqav4n3")))

(define-public crate-signtool-1.2.0 (c (n "signtool") (v "1.2.0") (d (list (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0227hnf3jij5a1177ks0mrm7z408ikkp7fbhlqdpqa2zaphkh7kr")))

