(define-module (crates-io si gn signal-msg) #:use-module (crates-io))

(define-public crate-signal-msg-0.1.0 (c (n "signal-msg") (v "0.1.0") (d (list (d (n "simple-signal") (r "^1.1.1") (d #t) (k 0)))) (h "02xj6hw4afxn3148raicwcy2pg63ngg6fmxlqg25rfcmaqdwm01s")))

(define-public crate-signal-msg-0.2.0 (c (n "signal-msg") (v "0.2.0") (d (list (d (n "simple-signal") (r "^1.1.1") (d #t) (k 0)))) (h "1q2wfzz1212dsrjd7wydf3163hqc7gqcw0gikcsnd1f5aizljdmb")))

(define-public crate-signal-msg-0.2.1 (c (n "signal-msg") (v "0.2.1") (d (list (d (n "simple-signal") (r "^1.1.1") (d #t) (k 0)))) (h "0q3xsfv1d9lzsb157k1bsl89wcrcn9azsjrgnhpiyzm3dvspanv6")))

