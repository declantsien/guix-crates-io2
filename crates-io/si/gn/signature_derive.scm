(define-module (crates-io si gn signature_derive) #:use-module (crates-io))

(define-public crate-signature_derive-0.2.0 (c (n "signature_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0ikprvcml7mdyabix9d17vznk2yck0gpsa3acq4wgrv42ax7nbzq") (y #t)))

(define-public crate-signature_derive-0.2.1 (c (n "signature_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0pl1j6dbp2hl2flaz8bl8sl3lr2n6wjn0jb71bv6frsrb05idpi1") (y #t)))

(define-public crate-signature_derive-0.3.0 (c (n "signature_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1qqig5i92pi74wly1a87ma3pkhaagjgj2vm4m0xb0s4jb5k9q2rl") (y #t)))

(define-public crate-signature_derive-1.0.0-pre.0 (c (n "signature_derive") (v "1.0.0-pre.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "101npzmw9yi1knssfqnpsxw8mlm3dw7pcz0w5f15qq63mqry7l1h")))

(define-public crate-signature_derive-1.0.0-pre.1 (c (n "signature_derive") (v "1.0.0-pre.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1sml6n77s12ymkyhkkygq4j58pzr7wjrn7vvd8hmvkhlqhw6sn66")))

(define-public crate-signature_derive-1.0.0-pre.2 (c (n "signature_derive") (v "1.0.0-pre.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0wp8b8ald7qixrcvvclhdcpmn8hkx049jlc29g57ql0304c6qrdh")))

(define-public crate-signature_derive-1.0.0-pre.3 (c (n "signature_derive") (v "1.0.0-pre.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "140dc985ah274xz4y362lry51z6m0205lcngsa07dy3kbc4pdfpz")))

(define-public crate-signature_derive-1.0.0-pre.4 (c (n "signature_derive") (v "1.0.0-pre.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0p1x0wv6grrgdn226m9rsqpcnpfwkpji7rjpalkbk1ynv0xpvf57")))

(define-public crate-signature_derive-1.0.0-pre.5 (c (n "signature_derive") (v "1.0.0-pre.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.2") (d #t) (k 0)))) (h "0rpp7dpf38wzxb28ica6x7jam5gs1bhf52ksvhbxv1wrpfd2vc5l") (r "1.56")))

(define-public crate-signature_derive-1.0.0-pre.6 (c (n "signature_derive") (v "1.0.0-pre.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jbyddfm370qwqplklf70rw153xnhv741g9qk6lyn830s2criyj8") (r "1.56")))

(define-public crate-signature_derive-1.0.0-pre.7 (c (n "signature_derive") (v "1.0.0-pre.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03wj342zvljknqwg3qbc9acrcsrzhdp1d2d6pfrh4p1b087k3rln") (r "1.56")))

(define-public crate-signature_derive-2.0.0-pre.0 (c (n "signature_derive") (v "2.0.0-pre.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00li49g73q0gcq8rfhkzlqvgagvkaggyb3yypcsb8ny70cljy4jk") (r "1.56")))

(define-public crate-signature_derive-2.0.0 (c (n "signature_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nnrb70cni3ba177m3823ph96mm56h4myyz3ywyklknakis31sgd") (r "1.56")))

(define-public crate-signature_derive-2.0.1 (c (n "signature_derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1z0mjjg3fpj08kc3nkax4lczgp7sfzbcm8q2qgim865510wkgpxc") (r "1.56")))

(define-public crate-signature_derive-2.1.0 (c (n "signature_derive") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11h4z3bql9pzj0mf7bv30q9c3rldk9n03520pk3z9siyj78q20xb") (r "1.60")))

