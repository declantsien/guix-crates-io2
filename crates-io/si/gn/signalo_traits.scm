(define-module (crates-io si gn signalo_traits) #:use-module (crates-io))

(define-public crate-signalo_traits-0.1.0 (c (n "signalo_traits") (v "0.1.0") (d (list (d (n "missing_mpl") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1hqk2ykjjsddjq3q1v1ffijq3asi18zbg6bl4fyl91npdi50sykm") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.1.1 (c (n "signalo_traits") (v "0.1.1") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0hrz9irci0bkxn2hh5bsia2akjyxcz2pp1z9shq7mk8qv5dzy8dz") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.1.2 (c (n "signalo_traits") (v "0.1.2") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "1n4cxhab7a8m2h22k38f5bxzfxi46rpcgcrn6zlvn6h4axniidrx") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.1.3 (c (n "signalo_traits") (v "0.1.3") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0qxdawblch3b6xmy6kj8j34gnvgc706g4is00gv46liqz94ca9gi") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.1.4 (c (n "signalo_traits") (v "0.1.4") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "19g8yhjghj43wiwzspmfnqwnkkb0xw2nrp19rjnaj727sfnlkg4p") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.2.0 (c (n "signalo_traits") (v "0.2.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "08vv5v3qk3i6xglf7cqx3bfjcz6n0904dc863x25bwpmkysgaybq") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.2.1 (c (n "signalo_traits") (v "0.2.1") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0hj00yiiim1hsi4nfsivfjkhy66vw4sx23j9i2r95005y13023i9") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.2.2 (c (n "signalo_traits") (v "0.2.2") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0a1rjb4zl0nkdjxycksc7hprjgdz545rv03abllk9lajhwhiy1cg") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.3.0 (c (n "signalo_traits") (v "0.3.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "1nm31bkd3sp1za4xhi1gibkf1nmj6x5lifsm080rq4i7b18594dd") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.4.0 (c (n "signalo_traits") (v "0.4.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0c3c9n5bf427r92qfgw1lpp8rpwn5v0nbbwyh1z2d9faxakyy629") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_traits-0.5.0 (c (n "signalo_traits") (v "0.5.0") (d (list (d (n "guts") (r "^0.1.1") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.5") (k 0)))) (h "075hjrimx82mb2wbs07yhv9lx6ifj19yd32h9y51544pm164r4dq") (f (quote (("std" "replace_with/std") ("panic_abort" "replace_with/panic_abort") ("derive_reset_mut") ("default"))))))

