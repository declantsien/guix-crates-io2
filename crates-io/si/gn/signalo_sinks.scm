(define-module (crates-io si gn signalo_sinks) #:use-module (crates-io))

(define-public crate-signalo_sinks-0.4.0 (c (n "signalo_sinks") (v "0.4.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "nearly_eq") (r "~0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.3") (k 0)))) (h "0z866nspq3xsa1dm32cmvsvhy5vf26fa3mg2727paks4hwinsdc7") (f (quote (("std" "signalo_traits/std" "num-traits/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sinks-0.5.0 (c (n "signalo_sinks") (v "0.5.0") (d (list (d (n "dimensioned") (r "~0.7") (o #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "nearly_eq") (r "~0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.4") (k 0)))) (h "1kw0lhxv3qyzzfzg58bvi4vjkz9nwm8qfcrdw4lvvmjlainxpiik") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sinks-0.5.1 (c (n "signalo_sinks") (v "0.5.1") (d (list (d (n "dimensioned") (r "~0.7") (o #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "nearly_eq") (r "~0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.4") (k 0)))) (h "0ll8s03xbznjd2px5152hcd697029ji9mg66hqabbkrdv4lxxa58") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_sinks-0.5.2 (c (n "signalo_sinks") (v "0.5.2") (d (list (d (n "dimensioned") (r "^0.7") (o #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "signalo_traits") (r "^0.5") (k 0)))) (h "1rl1jkdpdpggv3k3rh3i7viv382p3d5qnzygm9i9si1dplr19g92") (f (quote (("std" "signalo_traits/std" "num-traits/std" "dimensioned/std") ("default"))))))

