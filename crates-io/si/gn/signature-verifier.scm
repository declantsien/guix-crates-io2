(define-module (crates-io si gn signature-verifier) #:use-module (crates-io))

(define-public crate-signature-verifier-1.0.0 (c (n "signature-verifier") (v "1.0.0") (d (list (d (n "bs58") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nacl") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.9") (o #t) (d #t) (k 0)) (d (n "web3") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1x7m9vqlzawfyzqscg8x6swvsywkc3hphfdh4b5dldrbl7d4acb4") (s 2) (e (quote (("solana" "dep:solana-sdk" "dep:nacl" "dep:bs58") ("ethereum" "dep:web3"))))))

