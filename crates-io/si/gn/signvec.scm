(define-module (crates-io si gn signvec) #:use-module (crates-io))

(define-public crate-signvec-0.1.0 (c (n "signvec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fastset") (r "^0.2.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "14wan5q8k02aipmn6k9x05xg1zq2hna1fz9cd4qhc8hzc9mrpbwc") (f (quote (("default"))))))

(define-public crate-signvec-0.1.1 (c (n "signvec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fastset") (r "^0.2.1") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kvm114dav2wl6dg7iq2jq5712hh6cz3yvrqnfywk5cxszn8xy9z")))

(define-public crate-signvec-0.2.0 (c (n "signvec") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fastset") (r "^0.2.1") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sz4vkzfizxcjkqv14rsg36dxswzlhl656zmxgy1318knsxijpa6")))

(define-public crate-signvec-0.3.0 (c (n "signvec") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fastset") (r "^0.2.1") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dgh735a6xsz05qkr374rri3vz579dmv11v8xympyk90nnq1q2qn")))

