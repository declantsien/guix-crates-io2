(define-module (crates-io si gn sign_mail) #:use-module (crates-io))

(define-public crate-sign_mail-0.1.0 (c (n "sign_mail") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0vf5xagx4yidqlxis91qv6xyj0a8k6c5ag0cmyvyd2h7lhfncxl8") (y #t)))

(define-public crate-sign_mail-0.2.0 (c (n "sign_mail") (v "0.2.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0xvdybp9vyacpkfjrm9n7p4ivppavp3g394ld9mq6dinmvpr10x1") (y #t)))

(define-public crate-sign_mail-0.2.1 (c (n "sign_mail") (v "0.2.1") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "13d30i2lq7fbd19w8rpp666b2pysvmaz4y74db8fpbhka3qdr072") (y #t)))

(define-public crate-sign_mail-0.2.2 (c (n "sign_mail") (v "0.2.2") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0bnkfzafp4bdchla99fmsy7kgm027xjykfg5fcvgbdgvidf4pkzq")))

