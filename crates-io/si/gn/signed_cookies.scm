(define-module (crates-io si gn signed_cookies) #:use-module (crates-io))

(define-public crate-signed_cookies-0.1.0 (c (n "signed_cookies") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac-sha1") (r "~0.1.3") (d #t) (k 0)) (d (n "hyper") (r "~0.11.18") (d #t) (k 0)) (d (n "lazy_static") (r "~1.2.0") (d #t) (k 0)) (d (n "time") (r "~0.1.39") (d #t) (k 0)))) (h "1hqs0s180irc9klrksapsd7va2igx1bg46qk1lxms7ifh5x4sksc")))

(define-public crate-signed_cookies-0.1.1 (c (n "signed_cookies") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac-sha1") (r "~0.1.3") (d #t) (k 0)) (d (n "hyper") (r "~0.11.18") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "time") (r "~0.1.39") (d #t) (k 0)))) (h "01xs8hj2qdslzxm1z6c99xgnyrk0h2a8dmlnncrblzdagcldq5wn")))

(define-public crate-signed_cookies-0.1.2 (c (n "signed_cookies") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac-sha1") (r "~0.1.3") (d #t) (k 0)) (d (n "hyper") (r "~0.11.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "time") (r "~0.1.39") (d #t) (k 0)))) (h "1llp9pbigrd1j4q6ilz6l3swad77srrwqsfbx81naq9dh6i1fr0h")))

