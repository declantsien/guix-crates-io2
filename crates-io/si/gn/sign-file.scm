(define-module (crates-io si gn sign-file) #:use-module (crates-io))

(define-public crate-sign-file-0.1.0 (c (n "sign-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.22") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)))) (h "13g5pi10kxqw37dwmcfj6p7jrqcls0favfm5jxvcky7brjdldh65")))

