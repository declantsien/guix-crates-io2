(define-module (crates-io si gn signal-hook-registry) #:use-module (crates-io))

(define-public crate-signal-hook-registry-1.0.0 (c (n "signal-hook-registry") (v "1.0.0") (d (list (d (n "arc-swap") (r "~0.3.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "0qngw4amg5zh4q8425pnlg6w464rv68a2250cf7rc4i4bbpf47sr")))

(define-public crate-signal-hook-registry-1.0.1 (c (n "signal-hook-registry") (v "1.0.1") (d (list (d (n "arc-swap") (r "~0.3.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1mw5v909fn99h5qb96ma4almlik80lr1c7xbakn24rql6bx4zvfd")))

(define-public crate-signal-hook-registry-1.1.0 (c (n "signal-hook-registry") (v "1.1.0") (d (list (d (n "arc-swap") (r "~0.3.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1c9r77d0vrs10rnrgd2lgzndx90lg9x3wg2ad0wix9j8i2n62dli")))

(define-public crate-signal-hook-registry-1.1.1 (c (n "signal-hook-registry") (v "1.1.1") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1p2q8cdrkq6xcjjj097vrsrz9y98k7kkakmiif8465pr727x95qp")))

(define-public crate-signal-hook-registry-1.2.0 (c (n "signal-hook-registry") (v "1.2.0") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "0haz828bif1lbp3alx17zkcy5hwy15bbpmvks72j8iznx7npix4l")))

(define-public crate-signal-hook-registry-1.2.1 (c (n "signal-hook-registry") (v "1.2.1") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "0dgh2l7diyhkf74jjyqz1jfsyqsvxgssls30cix6b7jkph823qd3")))

(define-public crate-signal-hook-registry-1.2.2 (c (n "signal-hook-registry") (v "1.2.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1ar0dd9q1w952knhs0fx7qfh4iq5jvcvwkw1xg5fmmandh6flcnf")))

(define-public crate-signal-hook-registry-1.3.0 (c (n "signal-hook-registry") (v "1.3.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "19hirq0h33jjyh505s8hf9q5dq0ky80ygivkl3vshjv0y7zd1w8n")))

(define-public crate-signal-hook-registry-1.4.0 (c (n "signal-hook-registry") (v "1.4.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.3") (d #t) (k 2)))) (h "1c2mhijg54y6c1zi4630yki1vpq3z96ljfnsrdy0rb64ilr767p5")))

(define-public crate-signal-hook-registry-1.4.1 (c (n "signal-hook-registry") (v "1.4.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.3") (d #t) (k 2)))) (h "18crkkw5k82bvcx088xlf5g4n3772m24qhzgfan80nda7d3rn8nq")))

(define-public crate-signal-hook-registry-1.4.2 (c (n "signal-hook-registry") (v "1.4.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "signal-hook") (r "~0.3") (d #t) (k 2)))) (h "1cb5akgq8ajnd5spyn587srvs4n26ryq0p78nswffwhv46sf1sd9")))

