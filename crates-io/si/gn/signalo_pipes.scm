(define-module (crates-io si gn signalo_pipes) #:use-module (crates-io))

(define-public crate-signalo_pipes-0.1.0 (c (n "signalo_pipes") (v "0.1.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "^0.1.0") (d #t) (k 0)) (d (n "signalo_traits") (r "^0.1.0") (d #t) (k 0)))) (h "1wqz7asm1dhfzcdyx15pf52pv9rdjk9xd795j5dibadvyjkpmfnd") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.1.1 (c (n "signalo_pipes") (v "0.1.1") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "^0.1.0") (d #t) (k 0)) (d (n "signalo_traits") (r "^0.1.0") (d #t) (k 0)))) (h "0ihpdmjgb4cmxdpgbs7bnhmsppinny614c1lxxvrvlf51wmqyp6h") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.1.2 (c (n "signalo_pipes") (v "0.1.2") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "~0.1") (d #t) (k 0)) (d (n "signalo_traits") (r "~0.1") (d #t) (k 0)))) (h "1xag514y60dhvyyfkmrifrakd82k45m4ls9rlcj6lgg7fy9pbzvq") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.1.3 (c (n "signalo_pipes") (v "0.1.3") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "~0.1") (d #t) (k 0)) (d (n "signalo_traits") (r "~0.1") (d #t) (k 0)))) (h "13fa4702j96a7q2srsmlvh9z2pskg3jphi0bz7kl384qkkxvmsjd") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.1.4 (c (n "signalo_pipes") (v "0.1.4") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "^0.1.6") (k 0)) (d (n "signalo_traits") (r "^0.1.3") (k 0)))) (h "0ifc05l4whg3677rnmnn0cma2kqlp4qr0pan4c7kffp0725r6vqa") (f (quote (("std" "signalo_traits/std" "signalo_filters/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.1.5 (c (n "signalo_pipes") (v "0.1.5") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "^0.1.6") (k 0)) (d (n "signalo_traits") (r "^0.1.3") (k 0)))) (h "0950jicm3qdrf7kcv12f3h0i4isa16qbsggm1wqvv3vn2q5jn95x") (f (quote (("std" "signalo_traits/std" "signalo_filters/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.2.0 (c (n "signalo_pipes") (v "0.2.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "~0.2") (k 0)) (d (n "signalo_traits") (r "~0.2") (k 0)))) (h "0j68wdafsrs7mclgn13m9ayg65brryvqapxxjbksmiw4n5yranjw") (f (quote (("std" "signalo_traits/std" "signalo_filters/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.3.0 (c (n "signalo_pipes") (v "0.3.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "~0.3") (k 0)) (d (n "signalo_traits") (r "~0.2") (k 0)))) (h "0f2ag63cxf4z30hz8mh0bws4h338mqk4pfg8q7kccclpmzjijwfz") (f (quote (("std" "signalo_traits/std" "signalo_filters/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.3.1 (c (n "signalo_pipes") (v "0.3.1") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_filters") (r "~0.3") (k 0)) (d (n "signalo_traits") (r "~0.2") (k 0)))) (h "152my1x1wmdf5i26na51g1gzxkyflr7951j5nfgri4rzm15c4sk3") (f (quote (("std" "signalo_traits/std" "signalo_filters/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.4.0 (c (n "signalo_pipes") (v "0.4.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_traits") (r "~0.3") (k 0)))) (h "01cdbqmrcyawwa823p380a83ac50c6p29d6rmmqjr79dzxnsi00i") (f (quote (("std" "signalo_traits/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.5.0 (c (n "signalo_pipes") (v "0.5.0") (d (list (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "signalo_traits") (r "~0.4") (k 0)))) (h "19w66bqyakf5gfjkkzn9hrm9pnhx53c9yf1j851jh34d7rq4kyc1") (f (quote (("std" "signalo_traits/std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-signalo_pipes-0.5.1 (c (n "signalo_pipes") (v "0.5.1") (d (list (d (n "signalo_traits") (r "^0.5") (k 0)))) (h "1qj20wrpbbq8bmrhvq1qygl4fldiybg91bay0c10ldgb3v2f759i") (f (quote (("std" "signalo_traits/std") ("default"))))))

