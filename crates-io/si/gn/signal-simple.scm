(define-module (crates-io si gn signal-simple) #:use-module (crates-io))

(define-public crate-signal-simple-0.1.0 (c (n "signal-simple") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r2bxmhm9w8vx6m6cd54nyq216dk9cpicxakrnjppq6775q0svvr")))

(define-public crate-signal-simple-0.1.1 (c (n "signal-simple") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vc11wrm0vx2najjdsl8aspbjll1wcynsqm7yrl7sijdaxfsh5fb")))

