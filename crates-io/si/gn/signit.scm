(define-module (crates-io si gn signit) #:use-module (crates-io))

(define-public crate-signit-0.1.0 (c (n "signit") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.11") (d #t) (k 0)))) (h "1qhqgnhdhsjsf3kf0famhcs5plqvkxb9niwzajjhp4kzn9vjdmf9")))

