(define-module (crates-io si gn signald-rust) #:use-module (crates-io))

(define-public crate-signald-rust-0.1.0 (c (n "signald-rust") (v "0.1.0") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1w43xp3gw6p3p91m8slp3ax7k61na0hrapxhzgj8v323vgkhz1pl")))

(define-public crate-signald-rust-0.1.1 (c (n "signald-rust") (v "0.1.1") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1jnh4218pmn2hpsv4i40irnfnddppzxanzrylx1fpfjgq7c3vazl")))

(define-public crate-signald-rust-0.1.2 (c (n "signald-rust") (v "0.1.2") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 0)))) (h "0bkymmaw0ik9256133i8bqnm8y9p8fhqkiylqygi1b2nxi71zb2v")))

(define-public crate-signald-rust-0.1.3 (c (n "signald-rust") (v "0.1.3") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 0)))) (h "0rzv6jv0a9jrkvn37i4larpdincxl1k9kfb03299swvyjx64xfz6")))

(define-public crate-signald-rust-0.1.4 (c (n "signald-rust") (v "0.1.4") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 0)))) (h "15xzpwgv9c6p4gpr9dv6k0xbrbbq553a7yi0fnwf46r8fgslicnx")))

(define-public crate-signald-rust-0.1.5 (c (n "signald-rust") (v "0.1.5") (d (list (d (n "bus") (r "^2.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "rt-threaded" "sync"))) (d #t) (k 2)))) (h "0a2dk6v8ar8zy9ly9mcs59das70n056990018x56ibyiqi374ryy")))

