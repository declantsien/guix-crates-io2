(define-module (crates-io si gn signal-future) #:use-module (crates-io))

(define-public crate-signal-future-0.0.1 (c (n "signal-future") (v "0.0.1") (h "0zvcainarjfm6571rgwxqxmqdri90bwwj9cgyz625hyiw6n56zzx")))

(define-public crate-signal-future-0.1.0 (c (n "signal-future") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "10p10p3d4dpmzddnrby7ggdwb2gf0n3b2xkq49klp5a908gh92mw")))

(define-public crate-signal-future-0.1.1 (c (n "signal-future") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0n2ndagqdyhksqqjs8a33f0m13kbvln8hgp1fw9ik7jnfcbxvh9f")))

