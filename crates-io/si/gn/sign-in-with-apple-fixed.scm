(define-module (crates-io si gn sign-in-with-apple-fixed) #:use-module (crates-io))

(define-public crate-sign-in-with-apple-fixed-0.3.0 (c (n "sign-in-with-apple-fixed") (v "0.3.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("http1"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "net" "macros"))) (d #t) (k 0)))) (h "1kibnq8732y183sc1k5d9z8jx0lm8687bkg0m9j36x57mxswh6li")))

