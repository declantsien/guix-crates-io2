(define-module (crates-io si gn signum-sign) #:use-module (crates-io))

(define-public crate-signum-sign-0.1.0 (c (n "signum-sign") (v "0.1.0") (h "1jhfx5xgfifxih1mspavk1c8p1jgsd3qkqivbhmndgk50q7g2b02")))

(define-public crate-signum-sign-0.1.1 (c (n "signum-sign") (v "0.1.1") (h "0lacw7b4mxb3pwvpp171iqnh6l00g42i91mz6y40wlkf0sjhq46f")))

(define-public crate-signum-sign-0.1.2 (c (n "signum-sign") (v "0.1.2") (h "1lhssp5c2di5s2drcp69jy2r74h31hcwvdsda2l9i53iirz4npdj")))

(define-public crate-signum-sign-0.1.3 (c (n "signum-sign") (v "0.1.3") (h "0r6mx0v4qyn6mj73nwph7956a2aw39mm0z5yxhpvldkp2wy317xn")))

(define-public crate-signum-sign-0.1.4 (c (n "signum-sign") (v "0.1.4") (h "17xcfwhf294qfds2cdl8cvlbzphg0jpzqd2hsda18k3whx2q6yl8")))

(define-public crate-signum-sign-0.1.4-beta.0 (c (n "signum-sign") (v "0.1.4-beta.0") (h "04s5ilbsfys16mchndmdbi1j1wnj52jdbadqq2rpyamng65wgpd2")))

(define-public crate-signum-sign-0.1.4-beta.1 (c (n "signum-sign") (v "0.1.4-beta.1") (h "0ff75mxn16i0n9kxk532m624k2dardfxah6r7hz28xfn31m1k08j")))

(define-public crate-signum-sign-0.1.4-beta.2 (c (n "signum-sign") (v "0.1.4-beta.2") (h "0bdcf6drzjni5b4pgv3kwmwnbv37k5v54zy98i1fj2c89mxh2wqc")))

