(define-module (crates-io si gn signed_distance_fields) #:use-module (crates-io))

(define-public crate-signed_distance_fields-1.0.0 (c (n "signed_distance_fields") (v "1.0.0") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "1s43f8v5chs3z6w5533285875z5y50wjwc7z3pjv0caai33j4d98")))

(define-public crate-signed_distance_fields-1.0.1 (c (n "signed_distance_fields") (v "1.0.1") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "0p4w004i73qh62skq3c968h50sf1wpjliyz4imam77ixgb2mm7mj")))

(define-public crate-signed_distance_fields-1.0.2 (c (n "signed_distance_fields") (v "1.0.2") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "0gci135m6y5lf661avpjf2ld9lp6q3m77v7f3h9sbkzvpgn8h098")))

(define-public crate-signed_distance_fields-1.0.3 (c (n "signed_distance_fields") (v "1.0.3") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "0hwwff150b75l4x277mkpb8zbhliz447zzk14cf05d82c7bh7gin")))

(define-public crate-signed_distance_fields-1.0.4 (c (n "signed_distance_fields") (v "1.0.4") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "16qm5mijv1nybz6n22qd3jdf4lf13iw4zkj8qa0cxa8xqqcfbhcw")))

(define-public crate-signed_distance_fields-1.0.5 (c (n "signed_distance_fields") (v "1.0.5") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "13i411jfzrv6zgfmpgxji2h4vph1mgcvh115pfzk700yixlqy62v")))

(define-public crate-signed_distance_fields-1.0.6 (c (n "signed_distance_fields") (v "1.0.6") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "math_vector") (r "^0.2.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "1l1jqirf0rhzimr70yhhcbn0sa1hg7ksxn6zfcizccz9qkvjcdzl")))

