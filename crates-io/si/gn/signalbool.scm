(define-module (crates-io si gn signalbool) #:use-module (crates-io))

(define-public crate-signalbool-0.1.0 (c (n "signalbool") (v "0.1.0") (d (list (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "0bdw9s305d53gg04cgd4x9rszxcsa43cxhi9xk3flx4d76ls5qqp")))

(define-public crate-signalbool-0.1.1 (c (n "signalbool") (v "0.1.1") (d (list (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "0prqgpvkb3ks5wy6q1hfbgwwi2iw5m4ffl9gg18gwb95zimlbq9f")))

(define-public crate-signalbool-0.1.2 (c (n "signalbool") (v "0.1.2") (d (list (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "1w0llqh697q04qvh5s5h9mzsi65ai1wswmil0kdgj2dj9zpkd98w")))

(define-public crate-signalbool-0.1.3 (c (n "signalbool") (v "0.1.3") (d (list (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "0zlirakmrw1is0jqgg66760vlvj5wd0c2hhhgmz1cq76781s6zwc")))

(define-public crate-signalbool-0.1.4 (c (n "signalbool") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1y1wgvayigvdanlmn1bkc2sbabzy2smy8f280sry38rgqj2kb63v")))

(define-public crate-signalbool-0.2.0 (c (n "signalbool") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0vf2461n40hcr58g0l5fynf5l5cg8gwkf0j703kpq0fxvmg6yl2r")))

(define-public crate-signalbool-0.2.1 (c (n "signalbool") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0rvzfr3hs63nhvmmi250kiash1vmp65lmlrjh4zi0k535wj6n6qb")))

(define-public crate-signalbool-0.2.2 (c (n "signalbool") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.11") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fx75vwk2v9iqydi07xrvzkx42rgs0amkc5wixkq37ql35gnp5z5")))

(define-public crate-signalbool-0.2.3 (c (n "signalbool") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.14") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jg1v3w84cjq8rsrilnrr12040n08rnvzn8qp0mmzaz5ij29yias")))

(define-public crate-signalbool-0.2.4 (c (n "signalbool") (v "0.2.4") (d (list (d (n "nix") (r "^0.17") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nmgr8hi9agdp0slsnw1wc5zzpb9ijdvf59f0674a3rj00c85j96")))

(define-public crate-signalbool-0.2.5 (c (n "signalbool") (v "0.2.5") (d (list (d (n "nix") (r "^0.24") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fz3wpjg6qy26376al4nwcjhq6rxzyr069qpnzv4nhlgxkh2kkh0")))

