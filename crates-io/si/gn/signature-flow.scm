(define-module (crates-io si gn signature-flow) #:use-module (crates-io))

(define-public crate-signature-flow-1.0.0 (c (n "signature-flow") (v "1.0.0") (d (list (d (n "digest") (r "^0.9") (o #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "sha3") (r "^0.9.1") (k 2)) (d (n "signature_derive") (r "=1.0.0-pre.3") (o #t) (d #t) (k 0)))) (h "1jl8qah45c6l5iilc1pkqzyvjgysiyf0rkrsrcqb07786f0rhix5") (f (quote (("std") ("rand-preview" "rand_core") ("digest-preview" "digest") ("derive-preview" "digest-preview" "signature_derive") ("default" "std"))))))

