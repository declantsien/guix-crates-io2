(define-module (crates-io si gn signal-stack) #:use-module (crates-io))

(define-public crate-signal-stack-0.1.0 (c (n "signal-stack") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0wdfz2z6pyxl08gcgrck09j3qkzzrgk8bx9fp0if7g9ddjhz17cw")))

