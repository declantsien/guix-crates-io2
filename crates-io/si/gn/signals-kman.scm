(define-module (crates-io si gn signals-kman) #:use-module (crates-io))

(define-public crate-signals-kman-0.0.3 (c (n "signals-kman") (v "0.0.3") (h "15frddk5f9sc70yrzy3yggykmyl9gnwivq8f1nwjc4qzllkad9gg") (f (quote (("advanced"))))))

(define-public crate-signals-kman-0.0.4 (c (n "signals-kman") (v "0.0.4") (h "05px7xqzn278k49nds2wcf69yf9isygwb80db429rbjz58n8cd2n") (f (quote (("advanced"))))))

(define-public crate-signals-kman-0.0.5 (c (n "signals-kman") (v "0.0.5") (h "14rf5qn4l9vlz8psg87q0wf56f6wy9s20mf40k34m0blbsiz6fy7") (f (quote (("advanced"))))))

