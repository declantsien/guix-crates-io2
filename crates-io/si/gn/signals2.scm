(define-module (crates-io si gn signals2) #:use-module (crates-io))

(define-public crate-signals2-0.1.0 (c (n "signals2") (v "0.1.0") (h "00vda3is1jhpha7dbnpmixzd9znixcmn15kl3h3ywx7451n69056")))

(define-public crate-signals2-0.2.0 (c (n "signals2") (v "0.2.0") (h "11c34gw9lm8a9q45vd4d8kpw4dzm9ilinkswmcvrly4zi7mkrr6r")))

(define-public crate-signals2-0.2.1 (c (n "signals2") (v "0.2.1") (h "0hkb6zrjlip7m5bnpmwkbpkskyhj0qxpiknrk06jl2jbswn5iky0")))

(define-public crate-signals2-0.3.0 (c (n "signals2") (v "0.3.0") (h "04ihnrq1sw27ccxnll6v82d9nbb3x6nxyi9lgdlmacf5bgisjpy5")))

(define-public crate-signals2-0.3.1 (c (n "signals2") (v "0.3.1") (h "1qqlyspmy4rlyy77jd7673r907w1by851ljjh3vkz30931cfrrnn")))

(define-public crate-signals2-0.3.2 (c (n "signals2") (v "0.3.2") (h "04yi25ab8smill25gmpm060qkq4rq54cmhvisn5xyrl7kgm86xjh")))

(define-public crate-signals2-0.3.3 (c (n "signals2") (v "0.3.3") (h "0g3w4khw9qilybp2qlmj9vrbp586a8054hq7g0i9xhjq80i8q46w")))

