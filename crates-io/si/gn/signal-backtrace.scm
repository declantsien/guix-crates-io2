(define-module (crates-io si gn signal-backtrace) #:use-module (crates-io))

(define-public crate-signal-backtrace-0.1.0 (c (n "signal-backtrace") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "sig") (r "^1.0.0") (d #t) (k 0)))) (h "188c517jg6ffcb2bknrkgpz50g2b980xgiy0jkfb81jmcfpb5ay4")))

