(define-module (crates-io si ev sieve-cache) #:use-module (crates-io))

(define-public crate-sieve-cache-0.1.0 (c (n "sieve-cache") (v "0.1.0") (h "1yh1j9pmmdrni2qip8mk2351qmqkfnf14gsz76rxraxq2dn263w4")))

(define-public crate-sieve-cache-0.1.1 (c (n "sieve-cache") (v "0.1.1") (h "14zg1km7qqzkwhqizfrh70faa1mywa6pj0yi55axvhmmhxkr4f8l")))

(define-public crate-sieve-cache-0.1.2 (c (n "sieve-cache") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "0fk7igg0xd88bbsqxdb3kswq8082clp8w6s5h5fyz0mcfb19r4db")))

(define-public crate-sieve-cache-0.1.3 (c (n "sieve-cache") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "0myvny82ja6d2q1x0x106ps434ja132hzqjxgi3hw7qaz46zs65a")))

(define-public crate-sieve-cache-0.1.4 (c (n "sieve-cache") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "0x8g325w5zkv28pmyxmg7i23cny8hnj4km352jzpkh7jrjfkmgsi")))

(define-public crate-sieve-cache-0.2.0 (c (n "sieve-cache") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "1zdgb8bpm86wxr8y0j3ikq9xh82bysrz78iqsmb0xkxdsvwyp5k5")))

