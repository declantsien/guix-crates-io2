(define-module (crates-io si ev sieve2workers) #:use-module (crates-io))

(define-public crate-sieve2workers-0.1.0 (c (n "sieve2workers") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sieve") (r "^0.4.0") (d #t) (k 0) (p "xtuc-sieve-rs")))) (h "1iwzxzcjdggywms23vfivvy44zr0yyxhmy0d1gpfqdz0vaisfq5d")))

(define-public crate-sieve2workers-0.1.1 (c (n "sieve2workers") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sieve") (r "^0.4.1") (d #t) (k 0) (p "xtuc-sieve-rs")))) (h "1cf8bcsa410b38f5jibidh6vbblz598k0xj7m0v2i08px1v0h9da")))

(define-public crate-sieve2workers-0.1.2 (c (n "sieve2workers") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sieve") (r "^0.4.2") (d #t) (k 0) (p "xtuc-sieve-rs")))) (h "1ddyc6dciy70bckpz3zfwn89s0s75jd0flrxbjn48inpkgbr728a")))

