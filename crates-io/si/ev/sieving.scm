(define-module (crates-io si ev sieving) #:use-module (crates-io))

(define-public crate-sieving-0.1.0 (c (n "sieving") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0n1az6kanwpfbyqg6qqvkxgbwr0dsmzf9bjzjahxlbd4d924w3qs")))

(define-public crate-sieving-0.2.0 (c (n "sieving") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rp28c4yq44d6r8nc6qa4criv3fhmkkidbbbw3flaii1wfzjqv83")))

