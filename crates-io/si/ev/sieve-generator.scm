(define-module (crates-io si ev sieve-generator) #:use-module (crates-io))

(define-public crate-sieve-generator-0.3.0 (c (n "sieve-generator") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indentasy") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mr7y710pxrnj76wxs6sqv0msh192hiyq0x4d85bf4c879b7dz2w") (y #t)))

(define-public crate-sieve-generator-0.3.1 (c (n "sieve-generator") (v "0.3.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indentasy") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08g4am07vzhp77kwxbzadxvasy7ngc96a7jrcxfd64qld7n0288c") (y #t)))

(define-public crate-sieve-generator-0.3.2 (c (n "sieve-generator") (v "0.3.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indentasy") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s3qwq4fcp8l2hs1aqvmw9xy6fhs4gznr6z37sckmfgscp1gg8rs")))

