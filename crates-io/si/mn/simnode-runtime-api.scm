(define-module (crates-io si mn simnode-runtime-api) #:use-module (crates-io))

(define-public crate-simnode-runtime-api-1.6.0 (c (n "simnode-runtime-api") (v "1.6.0") (d (list (d (n "codec") (r "^3.1.3") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^26.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0v2hn7cqbmgbb7canyiybcgyssf8hqcrwmb38jcl9c1l8sfngzyn") (f (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-simnode-runtime-api-1.8.0 (c (n "simnode-runtime-api") (v "1.8.0") (d (list (d (n "codec") (r "^3.1.3") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^28.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0gf44gi3g14ajx9fn6cnmidck0pw09jcfyyh7nnanr8428z1v8jw") (f (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-simnode-runtime-api-1.9.0 (c (n "simnode-runtime-api") (v "1.9.0") (d (list (d (n "codec") (r "^3.1.3") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^29.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "02rdpxkfyaqijljaid9xpc5an161w6h5jid91c2cmhmydslriz5h") (f (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

