(define-module (crates-io si c_ sic_testing) #:use-module (crates-io))

(define-public crate-sic_testing-0.10.0 (c (n "sic_testing") (v "0.10.0") (h "0vx5f6d7i82wf8vs64kd7qm6ssp2bf87szpr1fsys5v0q598d5xr")))

(define-public crate-sic_testing-0.11.0 (c (n "sic_testing") (v "0.11.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.11") (d #t) (k 0)))) (h "13q2mqak3y0nlcq5wnnzinqn7hjwnp5czrkarcfpv5g9m3b4r198")))

(define-public crate-sic_testing-0.12.0 (c (n "sic_testing") (v "0.12.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.12.0") (d #t) (k 0)))) (h "0752a6qbih5j36vpxv7kg8iaqfjshwpf3y9ydc07c56y8sn5zr00")))

(define-public crate-sic_testing-0.13.0 (c (n "sic_testing") (v "0.13.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)) (d (n "sic_core") (r "^0.13.0") (d #t) (k 0)))) (h "1dn4k6b00bvsmkhg6jmwix51nl0rhva4cv5vip3254nqkick9n4b") (y #t)))

(define-public crate-sic_testing-0.14.0 (c (n "sic_testing") (v "0.14.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)) (d (n "sic_core") (r "^0.14.0") (d #t) (k 0)))) (h "1gazqcxac4pgm9ndl1n1mkldbn4qa5h30v6s4w9cfgqvvf5mx9xy")))

(define-public crate-sic_testing-0.15.0 (c (n "sic_testing") (v "0.15.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)) (d (n "sic_core") (r "^0.15.0") (d #t) (k 0)))) (h "1gxk18ngcpj14746idg5lkhqkcrcf2ch3cvk23cv6nx0az1l14nk")))

(define-public crate-sic_testing-0.16.0 (c (n "sic_testing") (v "0.16.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.16.0") (d #t) (k 0)))) (h "124kikh1qgjdb7hn028p23k6qnidg1kjrjibip8zjn6dsggr17lb")))

(define-public crate-sic_testing-0.17.0 (c (n "sic_testing") (v "0.17.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.17.0") (d #t) (k 0)))) (h "13xjnl42j00bg84x3i6m2i58l05x9ynq2ps361q4wp62f1pzacjl")))

(define-public crate-sic_testing-0.18.0 (c (n "sic_testing") (v "0.18.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.18.0") (d #t) (k 0)))) (h "1k79cdblngw4988025br16y8bxyzl0l046i2247yyzx0sfvbbmw9")))

(define-public crate-sic_testing-0.19.0 (c (n "sic_testing") (v "0.19.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.19.0") (d #t) (k 0)))) (h "10w6042gp929jp7fpfj1szy3iswn18jlm4pwc0b1m10gli1v6mrv")))

(define-public crate-sic_testing-0.19.1 (c (n "sic_testing") (v "0.19.1") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.19.0") (d #t) (k 0)))) (h "10vylmnfsymkhg4dgw2ixia3jih4n8x1j9micq34k89akw2969jk") (r "1.56")))

(define-public crate-sic_testing-0.20.0 (c (n "sic_testing") (v "0.20.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.20.0") (d #t) (k 0)))) (h "0srajr7x486m9jy134ma0f0falgla759kcvih0zw05n3bnva9jql") (r "1.56")))

(define-public crate-sic_testing-0.22.0 (c (n "sic_testing") (v "0.22.0") (d (list (d (n "parameterized") (r "^1.0.1") (d #t) (k 0)) (d (n "sic_core") (r "^0.22.0") (d #t) (k 0)))) (h "0yanq49mc5d3fzchnbdghn95hnxa7dxii7c1pz91cb78sspaphln") (r "1.61")))

