(define-module (crates-io si c_ sic_io) #:use-module (crates-io))

(define-public crate-sic_io-0.10.0 (c (n "sic_io") (v "0.10.0") (d (list (d (n "sic_core") (r "^0.10.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.10.0") (d #t) (k 2)))) (h "105v0i8p5ahk15ri4km2xiy5373gydibrrgr4yzd908sva58k3pp")))

(define-public crate-sic_io-0.11.0 (c (n "sic_io") (v "0.11.0") (d (list (d (n "sic_core") (r "^0.11") (d #t) (k 0)) (d (n "sic_testing") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1mx32ajvrv2nv4wfsp2n6d8577k1nyhhj3n0kdjvyfjxn0zx672r")))

(define-public crate-sic_io-0.12.0 (c (n "sic_io") (v "0.12.0") (d (list (d (n "sic_core") (r "^0.12.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.12.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "13i7dyxdzp9bjvhkw6f0k7ibmlmacrhl6w2r9m3f6afw9a3si29k")))

(define-public crate-sic_io-0.13.0 (c (n "sic_io") (v "0.13.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "sic_core") (r "^0.13.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.13.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "13s8qa3458316bf9lx632fciwm6qvk7cyknrng0q8vyl4b9z07ci") (y #t)))

(define-public crate-sic_io-0.14.0 (c (n "sic_io") (v "0.14.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "sic_core") (r "^0.14.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.14.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1bcxpjzjki65asdzi03v01sgsb7q0s7wjsn1drh24aqsvp0g2axg")))

(define-public crate-sic_io-0.15.0 (c (n "sic_io") (v "0.15.0") (d (list (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "sic_core") (r "^0.15.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.15.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0mnairqn7gblwfvkmbnyiza859il9kphg823f7ym8qwn7hixm600")))

(define-public crate-sic_io-0.16.0 (c (n "sic_io") (v "0.16.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.16.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1h1nk1rzrclqmi5ribg0kvq3bqyw5fi9srmljqk51x3sslz9c5l4")))

(define-public crate-sic_io-0.17.0 (c (n "sic_io") (v "0.17.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.17.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.17.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "047khvqx6mmyrpp1f3di3qlkdpvssg12hs49rq58bxpxk89nr885")))

(define-public crate-sic_io-0.18.0 (c (n "sic_io") (v "0.18.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.18.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.18.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0c8za1j4lw1k2qxnv4fr74h6n9541nbagilxh0x4bg3819rsjrbr")))

(define-public crate-sic_io-0.19.0 (c (n "sic_io") (v "0.19.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.19.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.19.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1sgbrrr2hpcnf4r8ydq4r6iy3ry2f9cr94gqabwi8kjrf9iwf104")))

(define-public crate-sic_io-0.19.1 (c (n "sic_io") (v "0.19.1") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.19.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.19.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0khghz4z4jvi5m2adk2jxp0gfnw9jgfvl4jra79qc41g13ih4xmn") (r "1.56")))

(define-public crate-sic_io-0.20.0 (c (n "sic_io") (v "0.20.0") (d (list (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.20.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.20.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02kz9h1q7cka6r9sbwvxma86hq23i03vdnbc0hrk5i0y0f2m3mqg") (r "1.56")))

(define-public crate-sic_io-0.20.1 (c (n "sic_io") (v "0.20.1") (d (list (d (n "parameterized") (r "^1.0.0") (d #t) (k 2)) (d (n "sic_core") (r "^0.20.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.20.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0c7p548q3a97jfkkdill9cpjrv3ha3shy87pwb962dgnsasb1wzi") (r "1.56")))

(define-public crate-sic_io-0.21.0 (c (n "sic_io") (v "0.21.0") (d (list (d (n "parameterized") (r "^1.0.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.21.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.20.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p95mgjh62khnavsnpyvx176wc6awl45jh9q67h2ydy0cs6f9qnl") (r "1.61")))

(define-public crate-sic_io-0.22.0 (c (n "sic_io") (v "0.22.0") (d (list (d (n "parameterized") (r "^1.0.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.22.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.22.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fsgpbzwirj9qqspqmw4bc8kv2h15fnbcbir2rfbrxdys2wbabn8") (r "1.61")))

(define-public crate-sic_io-0.22.1 (c (n "sic_io") (v "0.22.1") (d (list (d (n "parameterized") (r "^1.0.1") (d #t) (k 2)) (d (n "sic_core") (r "^0.22.0") (d #t) (k 0)) (d (n "sic_testing") (r "^0.22.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13qjdsbfpm1pcwfrcsm8wc408b62lm0qwvpwfgh3b2ina1i789rv") (r "1.61")))

