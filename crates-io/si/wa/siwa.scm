(define-module (crates-io si wa siwa) #:use-module (crates-io))

(define-public crate-siwa-0.1.0 (c (n "siwa") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1n14ns5wmfgvya7f53zgh5jr8x5cm7qhhln90i7bgk64gxvj7vj2")))

(define-public crate-siwa-0.1.1 (c (n "siwa") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10nyy40ivyazmxfixfmljpv8q4jr52y1hx1aqzf85ih00sbgahi2")))

(define-public crate-siwa-0.1.2 (c (n "siwa") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vakapknbxvicdrjph1id1c2jn28s8jf1mwrzyg3xmw7kwmprbnd")))

