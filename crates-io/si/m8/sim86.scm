(define-module (crates-io si m8 sim86) #:use-module (crates-io))

(define-public crate-sim86-0.1.0 (c (n "sim86") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "console") (r "=0.15.5") (d #t) (k 2)) (d (n "similar") (r "=2.2.1") (d #t) (k 2)) (d (n "uuid") (r "=1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1vpwv6hqbqn175y3pd3ah9wkkxnm85g3sv8k14va80i6dbk6iwfa")))

