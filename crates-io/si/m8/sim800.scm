(define-module (crates-io si m8 sim800) #:use-module (crates-io))

(define-public crate-sim800-0.0.1 (c (n "sim800") (v "0.0.1") (h "1c5b3jdza6nkpy7f9r6qig6wlspr74z2kxh8wzxnh78214shlpv4")))

(define-public crate-sim800-0.0.2 (c (n "sim800") (v "0.0.2") (h "1f48dw1fzjqgdqg4ksx5ldn19dl60vccmfwgd5d8zkb1bhrhg8ch")))

