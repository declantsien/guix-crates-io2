(define-module (crates-io si nf sinfo) #:use-module (crates-io))

(define-public crate-sinfo-0.1.0 (c (n "sinfo") (v "0.1.0") (h "0zhqmn860hklifr8wb544bkb3p64jdn0kfj67vymha01pyfljf5j")))

(define-public crate-sinfo-0.1.1 (c (n "sinfo") (v "0.1.1") (h "0xgx3gvxjypyi093nhzcbrll8i0s36jqwi6qb8jmn5zwh86yg543")))

(define-public crate-sinfo-0.1.2 (c (n "sinfo") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "0dzmdgs6s5992lsm70x8fyrpqklmab2pgknr6fsg2d8n5wvg43jv")))

(define-public crate-sinfo-0.2.0 (c (n "sinfo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)))) (h "0dl9v4a5zrglgsq14cjwi1yhgrdcrdamk3cisl8c4g9jg4h3qx71")))

