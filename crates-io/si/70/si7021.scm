(define-module (crates-io si #{70}# si7021) #:use-module (crates-io))

(define-public crate-si7021-0.1.0 (c (n "si7021") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.3") (d #t) (k 0)))) (h "138bb7rskmz92xrcsc343mcz3qir6f9a5acl9z5acf65kra09zgb")))

(define-public crate-si7021-0.2.0 (c (n "si7021") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.1") (d #t) (k 0)) (d (n "i2csensors") (r "^0.1.3") (d #t) (k 0)))) (h "00drkg6b3kg4lw1x2124rbn9zk4rpdd61l5fnsqcynv3xywh3nix")))

