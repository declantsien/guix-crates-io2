(define-module (crates-io si #{70}# si70xx) #:use-module (crates-io))

(define-public crate-si70xx-0.1.0 (c (n "si70xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "0yf7rd9idm4v384wfxy0knh5ad7fq8insvgqsl453gxb4ik0abcw") (f (quote (("si7013") ("default")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-si70xx-0.2.0 (c (n "si70xx") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.3") (o #t) (d #t) (k 0)))) (h "02p8sfwi3nnck93mbvb51nmamgp5vwb61m6dnas190ibkxrdgmm5") (f (quote (("si7013") ("default")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-si70xx-0.3.0 (c (n "si70xx") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00hvyak3i8xdng9w8m7lwcjy3nwdgsr6raxpi23c59b1799i4gqf") (f (quote (("si7013") ("default")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

