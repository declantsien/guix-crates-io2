(define-module (crates-io si co sicompiler) #:use-module (crates-io))

(define-public crate-sicompiler-0.1.0 (c (n "sicompiler") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "10rshipzj5rip8lzp5i5s7wq0fv3jxp03rkqpalnkf6a5lgrwj32")))

(define-public crate-sicompiler-0.1.1 (c (n "sicompiler") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ifzz90324cq239qv7asbmp80lvq7yyl9npak0cnpndyy06r7np")))

(define-public crate-sicompiler-1.0.0 (c (n "sicompiler") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "09nbn2zgxidc4xqd9zp5c9i7hc9pds35mfr7ckw9cm5gmcb9w818") (y #t)))

(define-public crate-sicompiler-1.0.1 (c (n "sicompiler") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "03zmmb5x8wnpslpqbsza6n1ic1ykv0zvylx2aq31bdixb34w4hrr")))

