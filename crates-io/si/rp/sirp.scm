(define-module (crates-io si rp sirp) #:use-module (crates-io))

(define-public crate-sirp-0.1.0 (c (n "sirp") (v "0.1.0") (h "1xdg2dqdfmxhwyin978541av4szz1w89vc0fjrlnik822xm10nhr")))

(define-public crate-sirp-0.1.1 (c (n "sirp") (v "0.1.1") (d (list (d (n "number-theory") (r "^0.0.1") (d #t) (k 0)))) (h "0av4h7vw6yn7yk2irwa0x1wl5n8dp1bcf395pyannncn4qycxb2a")))

