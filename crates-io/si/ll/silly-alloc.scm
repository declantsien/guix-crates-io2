(define-module (crates-io si ll silly-alloc) #:use-module (crates-io))

(define-public crate-silly-alloc-0.1.0 (c (n "silly-alloc") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "silly-alloc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (d #t) (k 2)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 2)) (d (n "xorshift") (r "^0.1.3") (d #t) (k 2)))) (h "0wwsans9vm7zavdc33ji7h37yabqyljaz2b1bv5k7316kh05f1l2")))

