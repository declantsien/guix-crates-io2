(define-module (crates-io si ll silly-alloc-macros) #:use-module (crates-io))

(define-public crate-silly-alloc-macros-0.1.0 (c (n "silly-alloc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1jyzyy901h4x0xqxmnvcs2j4acp5hg542c9iv3dajpa8i40paxbx")))

