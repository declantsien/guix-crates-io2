(define-module (crates-io si ll silly-png) #:use-module (crates-io))

(define-public crate-silly-png-1.0.0 (c (n "silly-png") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "1n1g0yz2y2zrcvmbidkx07izscv0fqq5szl7wr4f96220zr6rfxw")))

(define-public crate-silly-png-1.0.1 (c (n "silly-png") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "14rrvrvbbq82wvcrc815fcq8chwvqlk70s453ldxrd6l3659djh0")))

(define-public crate-silly-png-1.0.2 (c (n "silly-png") (v "1.0.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "0647bs65q2l5m6alyrz7pk99s9s9rqkzc7hh9561qf4kln7d26lh")))

(define-public crate-silly-png-1.1.0 (c (n "silly-png") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "0hriad4jqrcnz96ngvyscx9j0kkdlqvc2yk1mg703d29wmxypq6q")))

(define-public crate-silly-png-1.2.0 (c (n "silly-png") (v "1.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divisors") (r "^0.2.1") (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "1ykl1dscqs2h0gbxjxqyz5cr8q2zsq1qf5pillnf3zqabsdpx284")))

(define-public crate-silly-png-1.2.1 (c (n "silly-png") (v "1.2.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divisors") (r "^0.2.1") (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "03w6iryrkj8ws4nhx7m5d6f3q4f65cmf7yhqxp1bf3kl263n19sp")))

