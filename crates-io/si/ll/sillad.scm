(define-module (crates-io si ll sillad) #:use-module (crates-io))

(define-public crate-sillad-0.1.0 (c (n "sillad") (v "0.1.0") (d (list (d (n "async-io") (r "^2.3.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (f (quote ("io"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.4") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "1blr2qbrb1mjzpqn4s0ssfwjk7kpb7a5agsz6wkmdcrmsmfsx6ja")))

(define-public crate-sillad-0.1.1 (c (n "sillad") (v "0.1.1") (d (list (d (n "async-io") (r "^2.3.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (f (quote ("io"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.4") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "1sz8bmq4ddxcbxvdfzhk6iqgxhnwnf7aj5bwyfil0jjv0322y4vx")))

