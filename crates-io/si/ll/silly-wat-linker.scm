(define-module (crates-io si ll silly-wat-linker) #:use-module (crates-io))

(define-public crate-silly-wat-linker-0.1.0 (c (n "silly-wat-linker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cwk3728azplwdm2kpw4ysh5lyly0lvyis4g1a7j5xf17sy4s4wp")))

(define-public crate-silly-wat-linker-0.2.0 (c (n "silly-wat-linker") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "09w6ai4dvw1wkdjsgdfgwhf7nhw93dp75z1kapmx8gz5wddqr24a")))

(define-public crate-silly-wat-linker-0.2.1 (c (n "silly-wat-linker") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zzx70hfrkjz4fb1rz7ira8ljcrlz18rnin7k6b8rz1q60yd6ran")))

(define-public crate-silly-wat-linker-0.3.0 (c (n "silly-wat-linker") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1a14jywij7i0rlf6ndayl78056dv2dz4m2y0f8k087pv0gqwq6rk")))

(define-public crate-silly-wat-linker-0.4.0 (c (n "silly-wat-linker") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "1pv3laq09f6avdmhijsrchn1zcis15jzyifr14zp6a508bsw2nsg")))

(define-public crate-silly-wat-linker-0.5.0 (c (n "silly-wat-linker") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "0n8l1fzw0ykq37aw8aip5cvhcbrsga8yg4hb2zsk2sj2n5rmxq8p")))

(define-public crate-silly-wat-linker-0.6.0 (c (n "silly-wat-linker") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "05n251kj54a89yz22drhhnjchbp4lyaa1lg4q9dmbzy13ssb5dmg")))

(define-public crate-silly-wat-linker-0.6.1 (c (n "silly-wat-linker") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "0jz15pbbr7gwmalnavn3mmb47965j2cxd1pk8rg21dbmg94v7x0b")))

(define-public crate-silly-wat-linker-0.7.0 (c (n "silly-wat-linker") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "0ijh7583fryj4bkcrsdldyrlqk7p5zg4h1bc36mxhqcrgalwfl06")))

(define-public crate-silly-wat-linker-0.7.1 (c (n "silly-wat-linker") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (d #t) (k 0)) (d (n "wat") (r "^1.0.48") (d #t) (k 0)))) (h "0gndnqpyngiv95rxk6vrpg945d76ak2d6dvikldpj52r4an0vyfr")))

