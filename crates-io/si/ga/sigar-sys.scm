(define-module (crates-io si ga sigar-sys) #:use-module (crates-io))

(define-public crate-sigar-sys-1.0.0 (c (n "sigar-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g8za08wxvw37nh4sj1y772hlhgdp3bwd0wv59qg63cglvzb4bz4")))

(define-public crate-sigar-sys-1.0.1 (c (n "sigar-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05wfgfk77nv8d4pii33yr11g5vkvx1rb6706mdwb7z46dp2m23jx")))

(define-public crate-sigar-sys-1.0.3 (c (n "sigar-sys") (v "1.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0854i0lqq2k2qj2jgsgqxi2lrfxpmj9vwivcmfs8m92v6hj833n8")))

