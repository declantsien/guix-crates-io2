(define-module (crates-io si ga sigalign-impl) #:use-module (crates-io))

(define-public crate-sigalign-impl-0.1.0 (c (n "sigalign-impl") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "capwriter") (r "^0.2.0") (d #t) (k 0)) (d (n "lt-fm-index") (r "^0.7.0-alpha.2") (d #t) (k 0)) (d (n "lt-fm-index") (r "^0.7.0-alpha.2") (f (quote ("fastbwt"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sigalign-core") (r "^0.1.0") (d #t) (k 0)) (d (n "sigalign-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "06r0rv4vrjj9y784kb7bcgwkyz6kvx2zh8fj3x24vvdsrj4bnir5")))

