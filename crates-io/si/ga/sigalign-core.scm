(define-module (crates-io si ga sigalign-core) #:use-module (crates-io))

(define-public crate-sigalign-core-0.1.0 (c (n "sigalign-core") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "capwriter") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0iqcy5cmygfywdqfnq819mgk403qzvcgp655xfk2yhghgybpi28b") (f (quote (("short_key"))))))

