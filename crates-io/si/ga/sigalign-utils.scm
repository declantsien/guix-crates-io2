(define-module (crates-io si ga sigalign-utils) #:use-module (crates-io))

(define-public crate-sigalign-utils-0.1.0 (c (n "sigalign-utils") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)))) (h "199kaj4rgd4hxv616lkjgmvgmbyjsnvfd7646imzgnad9qanqg6s")))

