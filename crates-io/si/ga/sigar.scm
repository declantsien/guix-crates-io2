(define-module (crates-io si ga sigar) #:use-module (crates-io))

(define-public crate-sigar-0.0.1 (c (n "sigar") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0ha6j57i9jjfxx4b7302ydfgrmc09z687fxfjvsif0k22y4dy33f")))

(define-public crate-sigar-0.0.2 (c (n "sigar") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0zclgflmk77fzsn29wjm22mckzdq5r1ffsk1bpz3i76yvnvz57zw")))

