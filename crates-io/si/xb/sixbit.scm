(define-module (crates-io si xb sixbit) #:use-module (crates-io))

(define-public crate-sixbit-0.1.0 (c (n "sixbit") (v "0.1.0") (h "0qf058b3iyzb75kr0zxvvdphjcp0rmz6mvfq2bd62zk7wpjmgvw1")))

(define-public crate-sixbit-0.2.0 (c (n "sixbit") (v "0.2.0") (h "013xp1whdyj1zd5ryhk3lcs7rnbprlwisq0bhbzw7gd95z5y4958")))

(define-public crate-sixbit-0.3.0 (c (n "sixbit") (v "0.3.0") (h "1ac5p75v1j3d5ln841aixh1fl9266sgnsa2yfcm7iz9xj2wl6b91")))

(define-public crate-sixbit-0.4.0 (c (n "sixbit") (v "0.4.0") (h "0qngvmzxj4kfzngcbagy7dwk4l0nhmprlab5ilmhk00apj88py1v")))

(define-public crate-sixbit-0.5.0 (c (n "sixbit") (v "0.5.0") (d (list (d (n "arbitrary") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c1rzp7iwmi8b3j625r7bn447zs3pk6chhk9pvzf00bsqsivpl8n")))

