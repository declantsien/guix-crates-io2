(define-module (crates-io si eg siege-editor) #:use-module (crates-io))

(define-public crate-siege-editor-0.1.0 (c (n "siege-editor") (v "0.1.0") (d (list (d (n "direct-gui") (r "^0.1") (d #t) (k 0)) (d (n "line_drawing") (r "^0.7") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 0)) (d (n "siege") (r "^0") (d #t) (k 0)))) (h "0rjj7grn78qcxcds2xirfnysglnrvczwfbhxr8dr6zv1xcyi3zp9")))

(define-public crate-siege-editor-0.1.1 (c (n "siege-editor") (v "0.1.1") (d (list (d (n "direct-gui") (r "^0.1") (d #t) (k 0)) (d (n "line_drawing") (r "^0.7") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 0)) (d (n "siege") (r "^0") (d #t) (k 0)))) (h "0414r8j9kizw6d6gjj542svz4a0fzhz3cj215y9ns42jwvdmlhz6")))

(define-public crate-siege-editor-0.1.2 (c (n "siege-editor") (v "0.1.2") (d (list (d (n "direct-gui") (r "^0.1") (d #t) (k 0)) (d (n "line_drawing") (r "^0.7") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 0)) (d (n "siege") (r "^0") (d #t) (k 0)))) (h "0ysb8bk2vhgsml7njkj8zvpaxy20lp18l0s6kk1pjdp4jjpmwi57")))

