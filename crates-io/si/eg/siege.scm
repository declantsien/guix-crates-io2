(define-module (crates-io si eg siege) #:use-module (crates-io))

(define-public crate-siege-0.1.0 (c (n "siege") (v "0.1.0") (d (list (d (n "line_drawing") (r "^0.7") (d #t) (k 0)))) (h "040ygh0q69f8bfgdgqp4d9y9lfahgidrpr2wq4hzsmxlc0ch7mkg")))

(define-public crate-siege-0.1.1 (c (n "siege") (v "0.1.1") (d (list (d (n "line_drawing") (r "^0.7") (d #t) (k 0)))) (h "110jkk83hyglvlqx6mq20zfybj0gmqpy11z5xx6r1dgmyhb23wly")))

(define-public crate-siege-0.1.2 (c (n "siege") (v "0.1.2") (d (list (d (n "line_drawing") (r "^0.7") (d #t) (k 0)))) (h "1217ipfyn7dmxrh8zga05zkirpxylg4rg6xffqy29mxkp4a6ydg2")))

