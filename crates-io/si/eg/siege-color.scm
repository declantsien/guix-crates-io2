(define-module (crates-io si eg siege-color) #:use-module (crates-io))

(define-public crate-siege-color-0.4.0 (c (n "siege-color") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "siege-math") (r "^0.5") (d #t) (k 0)))) (h "0jkyn8j5p6g82y7ykjn09y163i9jkkifd80scw22qdvsiyn8ank6")))

