(define-module (crates-io si eg siege-math) #:use-module (crates-io))

(define-public crate-siege-math-0.5.0 (c (n "siege-math") (v "0.5.0") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "13z4fxc8mylm8i79fq3kxpy1jgky3693zqk2jlzpdpdvjld0i1nz")))

(define-public crate-siege-math-0.5.1 (c (n "siege-math") (v "0.5.1") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06qy6pb4n940ig5sfb3zbicir0lzn1pfx2pqm16zqnj1rsdw51xl")))

