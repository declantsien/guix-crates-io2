(define-module (crates-io si gh sighashdb) #:use-module (crates-io))

(define-public crate-sighashdb-0.1.0 (c (n "sighashdb") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0lny96h8skybggv1dd8vg6bang38lgdks3396yvg0zs82650fwn5")))

(define-public crate-sighashdb-0.1.1 (c (n "sighashdb") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1hmr4n04ll1xc6qigbh123da503v7h70b3kfkakf43vg8k16yrbs")))

(define-public crate-sighashdb-0.1.2 (c (n "sighashdb") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0i921y5cm1l1wbadff37zwzwnygawmn633lhrr796d9dpvnf5w67")))

(define-public crate-sighashdb-0.1.3 (c (n "sighashdb") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "112qhr0xa4gfcjajgcygyf8xgb7bsqw0fb33ny7qfc07hy0k3s18")))

(define-public crate-sighashdb-0.1.5 (c (n "sighashdb") (v "0.1.5") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "10agmqqr8783z91ik6n1ji4w842yyzss461jvpwk10mkwz5ch1fx")))

(define-public crate-sighashdb-0.1.6 (c (n "sighashdb") (v "0.1.6") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0zb6xh4j1mxfxibyjfns8qkjhyshd9cysrr5zyn0a8nakg8lfffl") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.7 (c (n "sighashdb") (v "0.1.7") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "15asdbj5j5yh3fq9hxirsk4a7b7q3n13jfq6kn6b4x4qp84kc1n3") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.8 (c (n "sighashdb") (v "0.1.8") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0jiivw7d61r19wzwbfal158qb2avj142bxfdhqcw692yz37zql9l") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.9 (c (n "sighashdb") (v "0.1.9") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0jzz6chbkahi8a2yhd1w1pv320r6g04lfbg2lyygikvdjs3pb8py") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.10 (c (n "sighashdb") (v "0.1.10") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0vjb9074hkc971gp645c7nar2an2mqavl02nk4ljq5rbbix56ccr") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.11 (c (n "sighashdb") (v "0.1.11") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0ql4c26ynhqg31f8xyph4awmbn9zza2nh6wkinl63v0fi7kf92vi") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.12 (c (n "sighashdb") (v "0.1.12") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "17rcq0l6w41grq6lpvrm34adp825l3nn5pjb3nx7d5scqjqzir8s") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.13 (c (n "sighashdb") (v "0.1.13") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1zl7zwzmyglhfl0m0aqm5r9lmrlmga6wcxsm5wzs613h80zkm4f2") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.14 (c (n "sighashdb") (v "0.1.14") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1d9x8kna68fa12g3807r7z6l0p7i3qkj7ngvxq5090ij7q0db8yv") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.16 (c (n "sighashdb") (v "0.1.16") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "19w13v44k7z6hdf0vipx407ry0w032c06b543d07mwvqz28g2mbi") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.17 (c (n "sighashdb") (v "0.1.17") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1rhsrmqp3sizbgkqg6cvz5kykr9fylb8l4saa9q8q51dbydiwji3") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.18 (c (n "sighashdb") (v "0.1.18") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1isqqzdpa9ni2n7arjigq53lk4jfap7pvvkggh6ckapk4bjxd3dw") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.19 (c (n "sighashdb") (v "0.1.19") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0rm41asz05bmg0ybirhw6szww9rlzjnwh864780mhcw6fjmwwx28") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.20 (c (n "sighashdb") (v "0.1.20") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "15w7ssyhyq47mf7znhz3njxpx9a0smpa7hhz9511yh3ga6cnzdbw") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.21 (c (n "sighashdb") (v "0.1.21") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1p3zwf3aabqrb4w31pmx4cgxx6lnmvx3p7ravs61p6x3zpw55f7d") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.22 (c (n "sighashdb") (v "0.1.22") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1439dffszhxwhs02jrkd23fn8c302rmvighz1spvwsk2vaz38ykx") (f (quote (("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.23 (c (n "sighashdb") (v "0.1.23") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "10r2y0q7ybcmnvr1vzgq2yfc6i3l6q9yhsrjz8ckmamjpz92rw67") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.24 (c (n "sighashdb") (v "0.1.24") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "07x110b7q5b5v70f3h23a3860yzpy6cf35wglsfwwl0x6yr6621j") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.25 (c (n "sighashdb") (v "0.1.25") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1q8jd27rsrylywklliw7pag3lwksh92abbs2hc8lrjknnn80lag0") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.26 (c (n "sighashdb") (v "0.1.26") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0gn8r2cxmiy65a6h6yhmpfw8h0z749zf24yal3bcx7qpdlidhwn8") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.27 (c (n "sighashdb") (v "0.1.27") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1c075d4wm4azksfs723ww5izgf2bc0xzi4bgpj76ixy52mj97303") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.28 (c (n "sighashdb") (v "0.1.28") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1n3lp7b907bg92d6fshrj74cdkc5y6q4vnjbqj5ky27di4dcxhy1") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.29 (c (n "sighashdb") (v "0.1.29") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1qs8zz7nhki690cclqvjx6xiryq6qa9gpfk4dalrpqp1rz0pyy93") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.30 (c (n "sighashdb") (v "0.1.30") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1wxavj9car4imblyi0xnqrs2q87kq932s7g740pr55ybh5xg6qfy") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.31 (c (n "sighashdb") (v "0.1.31") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1df67gpbch7ic6cnmij096qjvmbly2dzp5yzk3zf2isr5ynaq6az") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.32 (c (n "sighashdb") (v "0.1.32") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0rvqrzsyy2jfd38f16mdvrh4fkiy8rzjlks9la361mh9y50w2cg9") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.33 (c (n "sighashdb") (v "0.1.33") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0dd2qr05ra1dmlri4ll6rihpn0v9awakm829h92mv965vvplbcy8") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.34 (c (n "sighashdb") (v "0.1.34") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1ybnxwachh9k3lza43r7nrigvvv7cxkir2w41hjm2fpqd8dyjn2b") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.35 (c (n "sighashdb") (v "0.1.35") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "02bqxmcb0sg05ja680v4mzp7r6lr0mw8r0rass1za6z5k58a34jm") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.36 (c (n "sighashdb") (v "0.1.36") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "05v72zg0i2i93nvgvv5n7iwmvwiz7lgbj2axng8nqfxfcp84lwwr") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.37 (c (n "sighashdb") (v "0.1.37") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "11fr5jb9k9bh5g3vrb5f7h1v2dfamjyvi09c89c2j2cffxly8frr") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.38 (c (n "sighashdb") (v "0.1.38") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0njbp7cdxh48ns22zfds8nqwsbxaac0lmwy8bjc53z37c6qf3q9m") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.39 (c (n "sighashdb") (v "0.1.39") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0ibxlzmnm0hcb0p1na2lax2avj27xihxdwmdb3f0bkzkpkw0sgad") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.40 (c (n "sighashdb") (v "0.1.40") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "1y8qmgldahq91ilqalpzrsxn3715wnk1giga9wl9lfg26l5jjcrm") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.41 (c (n "sighashdb") (v "0.1.41") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "15ba3cpv2i3rjzfpq726n0gpq57vf9rxm48r8k390n1rq8h7280b") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

(define-public crate-sighashdb-0.1.42 (c (n "sighashdb") (v "0.1.42") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)))) (h "0xm4kaqjjmhvjr4wpij52qs1b67rhi3dfkds0mn31cb7mq6bry41") (f (quote (("tulipv2") ("tulipv1-leverage-farm") ("reverse-get") ("default" "reverse-get" "tulipv1-leverage-farm" "tulipv2") ("atrix-farm"))))))

