(define-module (crates-io si gh sigh) #:use-module (crates-io))

(define-public crate-sigh-1.0.0 (c (n "sigh") (v "1.0.0") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ihifzpjw0i2qs3gyd24cf4zx8xbw1kic95qjmr9f57wkx75zxpa")))

(define-public crate-sigh-1.0.1 (c (n "sigh") (v "1.0.1") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xrv3a86yz00pjqk8k0fcwixnycdi0xw6hbdchi01pxajhr7z9hp")))

(define-public crate-sigh-1.0.2 (c (n "sigh") (v "1.0.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18l9id58y7622q03g3ad72ipzkgwcdyf47bd187kysn48k6b9ga6")))

