(define-module (crates-io si gh sight) #:use-module (crates-io))

(define-public crate-sight-0.0.0 (c (n "sight") (v "0.0.0") (h "0p30pjr2y2j5ilgn3ma8pcn9d7k30q9xrlcqv70ha3zd2ly34jfr")))

(define-public crate-sight-0.0.1 (c (n "sight") (v "0.0.1") (h "12yy07l74jc84b4r735mgpjrignsh1snvr56bck5qqmhw590phar")))

