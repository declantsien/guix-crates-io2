(define-module (crates-io si ld sild_test_doc_build_fine) #:use-module (crates-io))

(define-public crate-sild_test_doc_build_fine-2024.3.0 (c (n "sild_test_doc_build_fine") (v "2024.3.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "16aggp4msmwnpd2cngrlwkwxvzi4cx07x4q2i0bcgx8mm8liks8h") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (y #t)))

(define-public crate-sild_test_doc_build_fine-2024.3.2 (c (n "sild_test_doc_build_fine") (v "2024.3.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "1wzkc7fmy88aqy7nsb9czadl52f4860h0sf6wn80hgjwi405ssbr") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (y #t)))

(define-public crate-sild_test_doc_build_fine-2024.3.3 (c (n "sild_test_doc_build_fine") (v "2024.3.3") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "0faqv7p2m2jv9pbcq8dn6q2z4fiinsvqmwmfsq08wvbahqjc5r47") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (y #t)))

