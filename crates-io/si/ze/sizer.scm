(define-module (crates-io si ze sizer) #:use-module (crates-io))

(define-public crate-sizer-0.1.0 (c (n "sizer") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0fr6lbidnhyigxmfnvbcckx54vz8fswc5w1madqwflsc4v4bq0r5")))

