(define-module (crates-io si ze sizes) #:use-module (crates-io))

(define-public crate-sizes-0.1.0 (c (n "sizes") (v "0.1.0") (h "04yzckm2f1yh5iknv7i0ssd76im9s2zyxxraxl6jc3hmfakg8hvw")))

(define-public crate-sizes-0.1.1 (c (n "sizes") (v "0.1.1") (h "1x2flqvzv3ka00h10qq9y39fb42chxvavmjjsk6zs19x12iq8w7v")))

(define-public crate-sizes-0.1.2 (c (n "sizes") (v "0.1.2") (h "1mk36bkj12hkmcykyil640bq58hvgclfw28alq94nw4dmqf6l08n")))

(define-public crate-sizes-0.1.3 (c (n "sizes") (v "0.1.3") (h "021624d00qk1170wr7sflj7035ds2989zmqvd0km20lq5xskfc0p")))

