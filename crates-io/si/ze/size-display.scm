(define-module (crates-io si ze size-display) #:use-module (crates-io))

(define-public crate-size-display-0.1.0 (c (n "size-display") (v "0.1.0") (h "1pwvzg9kbrh9p3lg2xizhmam00xpqlsaczc3mpffhsjvjrm8idxg")))

(define-public crate-size-display-0.1.1 (c (n "size-display") (v "0.1.1") (h "1q756wdxnasbh7275n4f8qkbd3a8n04km3i53cr9pasbdjzidhp4")))

(define-public crate-size-display-0.1.2 (c (n "size-display") (v "0.1.2") (h "18d805b213799kwry1xb5g3z7glan28j8cjmrsx69r9fkymm4yjg")))

(define-public crate-size-display-0.1.3 (c (n "size-display") (v "0.1.3") (h "14xcrs7fmg9i2703015isnjbxj84vipl5zrxq87ij1vkqg9n6h75")))

(define-public crate-size-display-0.1.4 (c (n "size-display") (v "0.1.4") (h "1qg103x2a8mwda2agrvq337hgg45acpqpl0crmc4yk8wkvq1bx4x")))

