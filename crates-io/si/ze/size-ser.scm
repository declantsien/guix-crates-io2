(define-module (crates-io si ze size-ser) #:use-module (crates-io))

(define-public crate-size-ser-0.1.0 (c (n "size-ser") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)))) (h "19nc6lrcl0bpj4g3crxwzr472dnw475r9sahl3a6d7s0hy78avxb")))

