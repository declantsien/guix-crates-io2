(define-module (crates-io si ze sized-chunks) #:use-module (crates-io))

(define-public crate-sized-chunks-0.1.0 (c (n "sized-chunks") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1xr5icbqz3i6jdn6lzppwrdjmcfnk0l02jvypcd1g5xj1nx9kn16")))

(define-public crate-sized-chunks-0.1.1 (c (n "sized-chunks") (v "0.1.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "11yffn7333nzrri4jh84shpg5bp23kp9xxhdv92sfrmlcd9qh5cg")))

(define-public crate-sized-chunks-0.1.2 (c (n "sized-chunks") (v "0.1.2") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0l6b6lbv3lq7q6n36c3zpsi28vrixjxps6yfhfxzcbpnqsy7h9l8")))

(define-public crate-sized-chunks-0.1.3 (c (n "sized-chunks") (v "0.1.3") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1wmr50cm05ywn2j0kcz8yfw0gsr7xiqf9xa61x76kmnjp8ipyglx")))

(define-public crate-sized-chunks-0.2.0 (c (n "sized-chunks") (v "0.2.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1j4k7x2vhgnxm09nmg2yzzh1a09sg4jwgd0zh3jkpzmpfx63fwh2")))

(define-public crate-sized-chunks-0.2.1 (c (n "sized-chunks") (v "0.2.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0lbzhwwfqf9g4s0a0v410s72djddbicyrcys2838sa264c857iys")))

(define-public crate-sized-chunks-0.2.2 (c (n "sized-chunks") (v "0.2.2") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "14c3drvpmy9rydl1x2va734n44k89nsm1n3baxx5ifk3yhw85ag2")))

(define-public crate-sized-chunks-0.3.0 (c (n "sized-chunks") (v "0.3.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "09icn65zcrkf79m35ij7nrhxcd2d76dqzxvrnkpnx5slwhzyp8m2")))

(define-public crate-sized-chunks-0.3.1 (c (n "sized-chunks") (v "0.3.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "11c7nywx8cwwfj41qf0z6448bk5681qbgpj5682qx778gryva7gh")))

(define-public crate-sized-chunks-0.3.2 (c (n "sized-chunks") (v "0.3.2") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0qhrbcic9dsrclhd0r5qjnlmbv9xb8f9cmf85qdaf863m2g7c0hw") (y #t)))

(define-public crate-sized-chunks-0.4.0 (c (n "sized-chunks") (v "0.4.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "12m8nqcddg4zhkcnq0aglhaah4aivviqz1zwbmw2bp5r0fh8na0f")))

(define-public crate-sized-chunks-0.5.0 (c (n "sized-chunks") (v "0.5.0") (d (list (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "03ykvb21mjvhwqxqgcx8ipzb78jccdsc45p2n4a47ddkjbfn9nv2")))

(define-public crate-sized-chunks-0.5.1 (c (n "sized-chunks") (v "0.5.1") (d (list (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "refpool") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1zvnzcvg2j1wd45k1azkk1vn6wnqdg8misa84h6mhfl3r4gghnbg")))

(define-public crate-sized-chunks-0.5.2 (c (n "sized-chunks") (v "0.5.2") (d (list (d (n "arbitrary") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "refpool") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "14kbaayd0x4pqnvrpg1h4s1lx7mxhdn6b5gzhgzrnhzqswdcrsyg") (y #t)))

(define-public crate-sized-chunks-0.5.3 (c (n "sized-chunks") (v "0.5.3") (d (list (d (n "arbitrary") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "refpool") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "063p5g0szd5gy8a43agd6kl800cz1d40cyvgjzzq3mqs6zm4946m")))

(define-public crate-sized-chunks-0.6.0 (c (n "sized-chunks") (v "0.6.0") (d (list (d (n "arbitrary") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "refpool") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1z0afy0yj1z4187np6d73f67rwv7m2q5fg7lpiz89mdjmrms3420") (f (quote (("ringbuffer" "array-ops"))))))

(define-public crate-sized-chunks-0.6.1 (c (n "sized-chunks") (v "0.6.1") (d (list (d (n "arbitrary") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "refpool") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "062id0sdvia8iny01jlhkzip00a939gqvjf06fky839wclka755i") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-sized-chunks-0.6.2 (c (n "sized-chunks") (v "0.6.2") (d (list (d (n "arbitrary") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "refpool") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0bqdhdqycdlk1wxnhwbwdzdlczxni25m8xyc8kaackv4lpn1rhqy") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-sized-chunks-0.6.3 (c (n "sized-chunks") (v "0.6.3") (d (list (d (n "arbitrary") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.1.0") (d #t) (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "06hl0985jmshqzzl0nvaj4sc97sq1ppx6ba91w4x8f2aaz1kv95w") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-sized-chunks-0.6.4 (c (n "sized-chunks") (v "0.6.4") (d (list (d (n "arbitrary") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.1.0") (d #t) (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "01yjydh1a45yp4b07fi1lkfprmj57bsjqnpac7rpik8kkxm5vrk5") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-sized-chunks-0.6.5 (c (n "sized-chunks") (v "0.6.5") (d (list (d (n "arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^2.1.0") (d #t) (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "07ix5fsdnpf2xsb0k5rbiwlmsicm2237fcx7blirp9p7pljr5mhn") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-sized-chunks-0.7.0 (c (n "sized-chunks") (v "0.7.0") (d (list (d (n "arbitrary") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1rz5q0g90bfg96y1dbv4xh6wsmd970za4gyshwfr1lb60k3z12ki") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std")))) (r "1.51.0")))

