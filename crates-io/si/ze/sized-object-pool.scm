(define-module (crates-io si ze sized-object-pool) #:use-module (crates-io))

(define-public crate-sized-object-pool-0.1.0 (c (n "sized-object-pool") (v "0.1.0") (d (list (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "077c8ax0h4nx56vicfk79q3hmxkpdjnzy209v0c9r6amg2czmsi3")))

(define-public crate-sized-object-pool-0.1.1 (c (n "sized-object-pool") (v "0.1.1") (d (list (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s82bq6a11vi8020zsc6kcdjpn2hay204zz8jgw2zvkw7qwsh311")))

(define-public crate-sized-object-pool-0.1.2 (c (n "sized-object-pool") (v "0.1.2") (d (list (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fcyyl4yjfjd63dwf5hxkcqmabmzsaqj7vy9d6j5s41m5dpghxai")))

(define-public crate-sized-object-pool-0.1.3 (c (n "sized-object-pool") (v "0.1.3") (d (list (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d7qf32sksnw6ls5yzn8ghvw20q0dl05sg9h83isdw48ny0arx7s")))

(define-public crate-sized-object-pool-0.2.0 (c (n "sized-object-pool") (v "0.2.0") (d (list (d (n "dynamic-pool") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lnykrfmzi0400z23kig6sdn3pfhj26xgi65qf04w622i3y5qx5x") (y #t)))

(define-public crate-sized-object-pool-0.2.1 (c (n "sized-object-pool") (v "0.2.1") (d (list (d (n "dynamic-pool") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1izbhngncf6blj8cmwnl5q2rxq6421z3yppr5yhcm1ns6f7bmpaa")))

(define-public crate-sized-object-pool-0.2.2 (c (n "sized-object-pool") (v "0.2.2") (d (list (d (n "dynamic-pool") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1l5aj5wg560bsxdmhs5khc1brnvqjn5x47217mfgvknfdpfg1wfy")))

