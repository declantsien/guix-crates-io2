(define-module (crates-io si ze size) #:use-module (crates-io))

(define-public crate-size-0.1.0 (c (n "size") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hhqf2zwc6iwvkqypbk4vgzda1kj4h6k5nxf686npr2bs75q5i3f")))

(define-public crate-size-0.1.1 (c (n "size") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1m019pll3vybkl1pvak8bhkhpqbv0byclx70rd1gvcbw315pkwds")))

(define-public crate-size-0.1.2 (c (n "size") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0miaxqmh1lb3c0f7j5y9wkfw4ki75s9lbdcz02fmfw4fiqbj2l1y")))

(define-public crate-size-0.2.0 (c (n "size") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "03isrsqjkzdkr5wxaq56vwvwi0n8sba4f2yx9a4za3a1lhfgdcsg") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.2.1 (c (n "size") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0sd00jxk2hsa78ngwhx32wams6n8fvhnz97fp795s63d2hcsazmy") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.3.0 (c (n "size") (v "0.3.0") (h "1nwccic0b8a51m67sh19qvh4rr8s4fla74csw42nvsb91plx8cas") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.3.1 (c (n "size") (v "0.3.1") (h "1wdhmig8a4v1bbqpk6bjdbhchp9a4b688lg67qhjapxcqr045zd2") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.4.0 (c (n "size") (v "0.4.0") (h "0svm9p671hlyny8h2vbhd83xy90kbzspviwxs0vv1dl404swijb3") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.4.1 (c (n "size") (v "0.4.1") (h "0vdxh3h5zjx8zb5sk8fydqvcwzsrzb44yiljif36v1djgx691vcz") (f (quote (("std") ("default" "std"))))))

(define-public crate-size-0.5.0-preview1 (c (n "size") (v "0.5.0-preview1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "1r46wg2c8a625ha5lqwn3g9hahwblny27zncww4722m6jnlqciza") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-size-0.5.0-preview2 (c (n "size") (v "0.5.0-preview2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "1masw2g0v3lwbrrbrdf7h0dhzgk5acrybvxnyb78xzwy119nm4f6") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

