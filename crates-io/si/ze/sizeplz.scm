(define-module (crates-io si ze sizeplz) #:use-module (crates-io))

(define-public crate-sizeplz-0.0.1 (c (n "sizeplz") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1n8mi4xvkg26bcavjfi2l8rbsc2n6qnz0nr9jwp95bg5sqdrabcr")))

