(define-module (crates-io si ze size-of-derive) #:use-module (crates-io))

(define-public crate-size-of-derive-0.1.0 (c (n "size-of-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("printing" "extra-traits"))) (d #t) (k 0)))) (h "1miacashkfhy08jhbzmqskhnsa0mz01jmx6pf10i5b4nqb0jcnd1")))

(define-public crate-size-of-derive-0.1.2 (c (n "size-of-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("printing" "extra-traits"))) (d #t) (k 0)))) (h "0s2yjjknz28is95rfk7cpbxbdyxqppw3lmnsydvx822k1y4z9zzf")))

