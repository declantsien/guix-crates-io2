(define-module (crates-io si ze sizef) #:use-module (crates-io))

(define-public crate-sizef-0.1.0 (c (n "sizef") (v "0.1.0") (h "10i3vp4dlbm6qgj2hhnsxrgba16c29fai62z1324qj8hmkrivy4y")))

(define-public crate-sizef-0.1.1 (c (n "sizef") (v "0.1.1") (h "1ky19mq7vncyjyfag1saycb2miv7smdxs2ipx24mrw65lfq6f51q")))

(define-public crate-sizef-0.1.2 (c (n "sizef") (v "0.1.2") (h "1yi8qc7f7gswwbslpifxzxf46gm0lvy3f2hfrlsbiisp2fb46y0r")))

(define-public crate-sizef-0.1.3 (c (n "sizef") (v "0.1.3") (h "1s66xbb04kkfkm9j2gd6p9s38y9kxhcf9sscvw0bnj1bz91rybvw")))

(define-public crate-sizef-0.1.4 (c (n "sizef") (v "0.1.4") (h "0mvd2nw39wlas8q8dxbjf5p4h0nhwlbrc5jh4xpm3l1bs2ap3hn9")))

(define-public crate-sizef-0.2.0 (c (n "sizef") (v "0.2.0") (h "1za8pc75cn5hhs5pn8j1prgrhjcv4c8r2kb4b63n80xmnxkc2i85")))

