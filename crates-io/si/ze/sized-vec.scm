(define-module (crates-io si ze sized-vec) #:use-module (crates-io))

(define-public crate-sized-vec-0.1.0 (c (n "sized-vec") (v "0.1.0") (d (list (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1q9bz8zzzbwwg62xq5j98c23f5j14j3pqf4dqgvvk4mjpbi3q1ax")))

(define-public crate-sized-vec-0.2.0 (c (n "sized-vec") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "06y53q476aihsd9d58fyq8d28jhpxxkx5q5salph0hm98m6dh3jn")))

(define-public crate-sized-vec-0.2.1 (c (n "sized-vec") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0qfg1h3pvh1wqgn189wk6cd46mm21svj2bppn1phrsa8ki2mm0jf")))

(define-public crate-sized-vec-0.2.2 (c (n "sized-vec") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "11ybc5whf37b97cwcqgpg34fx96r0b832pqqlzslk5xhjzcj2gmn")))

(define-public crate-sized-vec-0.2.3 (c (n "sized-vec") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0p6fpbi9y76sn35g02vynv0qs9a7jw4d359w0dicjqmyn53pc1wm")))

(define-public crate-sized-vec-0.3.0 (c (n "sized-vec") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0rqky78j537j38baglsg69ky1f3amqg6gs4v0nr7591p8jaf71ar")))

