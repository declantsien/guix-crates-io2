(define-module (crates-io si ze sized_matrix) #:use-module (crates-io))

(define-public crate-sized_matrix-0.1.0 (c (n "sized_matrix") (v "0.1.0") (d (list (d (n "init_trait") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1hz6ls6hyi6y8i68p065bsdk9mvccmyih4a9bwci6knb4swg2g39") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.0 (c (n "sized_matrix") (v "0.2.0") (d (list (d (n "higher_order_functions") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "16fbb1hrw9pncv0wgkqzd0k40v8cn1xiywrqbsdrirxsmbcf9s6q") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.1 (c (n "sized_matrix") (v "0.2.1") (d (list (d (n "higher_order_functions") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1l2k4iyvpshsk05j8jydsp50s0jmimdnnbjdpsaba0i3j729d601") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.2 (c (n "sized_matrix") (v "0.2.2") (d (list (d (n "higher_order_functions") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "00sf0xwpzjwr4701xks20g2xaszdl6d2jaf8y1pl5b9f1pjcjgrk") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.3 (c (n "sized_matrix") (v "0.2.3") (d (list (d (n "higher_order_functions") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "088s5chvabyyjnsgpjac271qvzmbsnjwqig9bpqfa5k0ynjp6wsr") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.4 (c (n "sized_matrix") (v "0.2.4") (d (list (d (n "higher_order_functions") (r "^0.1.2") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "01zkn3za8l9adnr425bi26dvd84hfr9qsyff2qx8i83jjrvwaqd9") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.2.5 (c (n "sized_matrix") (v "0.2.5") (d (list (d (n "higher_order_functions") (r "^0.1.2") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "03zsmf639bpk8ni67b7s4jjq2mf5xp9sgmx35yz29jy40cmn97cx") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-sized_matrix-0.3.0 (c (n "sized_matrix") (v "0.3.0") (d (list (d (n "higher_order_functions") (r "^0.2.0") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0dkk99yxmyk10dq0lri0bznfhsq0ffx3xc8wqq7kihrh5ggzsdrr") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

