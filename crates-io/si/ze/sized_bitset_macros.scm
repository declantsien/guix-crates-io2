(define-module (crates-io si ze sized_bitset_macros) #:use-module (crates-io))

(define-public crate-sized_bitset_macros-0.1.0 (c (n "sized_bitset_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0sz4iv57fzrd45pz9ryg2m7j3lrgxwwfv2nx2civ0b6wavp4xwbb")))

(define-public crate-sized_bitset_macros-0.2.0 (c (n "sized_bitset_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2s7dqa4sfz9lifygljcqjn5adb2z6sa3jgjngypii060bbymk1")))

(define-public crate-sized_bitset_macros-0.3.0 (c (n "sized_bitset_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0jya96z6hk3bgww67pcypnb2m5v1hwrsx7pi2zx0cvasj0gl2w3y")))

