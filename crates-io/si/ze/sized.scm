(define-module (crates-io si ze sized) #:use-module (crates-io))

(define-public crate-sized-0.1.0 (c (n "sized") (v "0.1.0") (h "0nh3znh80jin6vzfp6g4xi1m0rsnjxmqx4xx6m8skjkwyck4yr4r")))

(define-public crate-sized-0.2.0 (c (n "sized") (v "0.2.0") (h "0050g6hgh3499y5karhmjpcwkn2r40z1lv02kfcgmqlz2xcnxlnl")))

