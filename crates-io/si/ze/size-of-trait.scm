(define-module (crates-io si ze size-of-trait) #:use-module (crates-io))

(define-public crate-size-of-trait-1.0.0 (c (n "size-of-trait") (v "1.0.0") (h "14k71hq3hsliknzizqmiq32i0hd21bhv08gq4cz7z2231vxmvbkl")))

(define-public crate-size-of-trait-1.1.0 (c (n "size-of-trait") (v "1.1.0") (h "0xrgcwq447sjlvaiv9mdpwzgp21wc03zzjgjfrlqjdw045gwpcv4")))

(define-public crate-size-of-trait-1.1.1 (c (n "size-of-trait") (v "1.1.1") (h "0r8b64y5slifjy1ll7rva56dzw6knarf5y54kk1krj16vikdw1vg")))

(define-public crate-size-of-trait-1.1.2 (c (n "size-of-trait") (v "1.1.2") (h "0rqziz9kd56w9fc49mqs4c6iaqa0qada3bbplqg1i9j83dwswmhm")))

(define-public crate-size-of-trait-1.1.3 (c (n "size-of-trait") (v "1.1.3") (h "005srblkcmywknp8v7b03flagr4l9m249qxh151y8kc7ki99z2mz")))

