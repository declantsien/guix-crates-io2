(define-module (crates-io si ze size_hint) #:use-module (crates-io))

(define-public crate-size_hint-0.1.0 (c (n "size_hint") (v "0.1.0") (h "1lppj1qqzyf60l5aqdqn51cncixsix25phg5bjmbqcfxqz8gqyq3")))

(define-public crate-size_hint-0.1.1 (c (n "size_hint") (v "0.1.1") (h "1249pibkyh8bqsfw6dvdszj2xcwx8axld1yykq0knk30kkd385il")))

(define-public crate-size_hint-0.1.2 (c (n "size_hint") (v "0.1.2") (h "15rcf1m948f3fcf5l27hn9gyzcqlib9hnyn28xf5ywm5jc68bgs2")))

(define-public crate-size_hint-0.1.3 (c (n "size_hint") (v "0.1.3") (h "0ybpk519z0sgimxn84lf1zy3ph5min9r317qyb6s5q19gyd10mav")))

