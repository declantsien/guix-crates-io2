(define-module (crates-io si ze size_of_const_macro) #:use-module (crates-io))

(define-public crate-size_of_const_macro-0.1.0 (c (n "size_of_const_macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("fold"))) (d #t) (k 0)))) (h "10jdzq5k6i4yr0ysbmfdsdi852bh5633bakynwwixz0jd66lb5w4") (r "1.56")))

