(define-module (crates-io si ni sinit) #:use-module (crates-io))

(define-public crate-sinit-0.1.0 (c (n "sinit") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "01ymqqyk92i7nry7q6zjk7h7qvpkxbzzmc90gph2amp4c7n62xcz")))

(define-public crate-sinit-0.1.1 (c (n "sinit") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1syy409hkdicqxrjhk39q4cmfdpwhvbm2bq7nkc2m7sh7kh4260n")))

(define-public crate-sinit-0.1.2 (c (n "sinit") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "11bazg2gkxycx0x83zd0k4jh832bgal1vv1s45qh7212i1c9zcl1")))

