(define-module (crates-io si x_ six_viewer) #:use-module (crates-io))

(define-public crate-six_viewer-0.1.0 (c (n "six_viewer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.45.1") (d #t) (k 0)))) (h "0m1w4wdrimbrv8mz6cdjbfwi61ip7ir0a60vmhcgbzq47b0jf5dl")))

(define-public crate-six_viewer-0.1.1 (c (n "six_viewer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.45.1") (d #t) (k 0)))) (h "15sd98a3ab314a1qa5xcdnll2ci8s05pi5viqznlcr34w7rixc5c")))

