(define-module (crates-io si -u si-unit-prefix) #:use-module (crates-io))

(define-public crate-si-unit-prefix-1.0.0 (c (n "si-unit-prefix") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "133mvnk63m7987fqmj6m8d78dmyiflwm1iq2kg031jh5g1sgfw4y")))

