(define-module (crates-io si xt sixtyfps-build) #:use-module (crates-io))

(define-public crate-sixtyfps-build-0.0.1 (c (n "sixtyfps-build") (v "0.0.1") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.1") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pmqnqhjqffcrvk9a08jd41z9ih5finjbf9zn35hg88xa8wrvgci")))

(define-public crate-sixtyfps-build-0.0.2 (c (n "sixtyfps-build") (v "0.0.2") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.2") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ir40sg8zq4786wcbn94qnk0nphm5s34hmyyzs7vsg614xdfq28f")))

(define-public crate-sixtyfps-build-0.0.3 (c (n "sixtyfps-build") (v "0.0.3") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.3") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cdvxvlhzjnc7c01d4v52xrn4b1d5jiaxd4n9kjijn9xbhvfvrs8")))

(define-public crate-sixtyfps-build-0.0.4 (c (n "sixtyfps-build") (v "0.0.4") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.4") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "155bgijp9q0d0i868sy6pia0qqmrihiwxdwrczq379lrlgknkbas")))

(define-public crate-sixtyfps-build-0.0.5 (c (n "sixtyfps-build") (v "0.0.5") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.5") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03jbxl2hna6jlfygm0x3qm8cvcazv6m192a5zycdj2vzr5mlsid3")))

(define-public crate-sixtyfps-build-0.0.6 (c (n "sixtyfps-build") (v "0.0.6") (d (list (d (n "sixtyfps-compilerlib") (r "=0.0.6") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xp67514yk42mgqi9a7sxb2fjq0jbvwkdp5qpzd066ai7mwn0zzb")))

(define-public crate-sixtyfps-build-0.1.0 (c (n "sixtyfps-build") (v "0.1.0") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.0") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hi2acrvpph09pnbp06z56322bi96gv27ky044ghs3dmlyzfkkc3")))

(define-public crate-sixtyfps-build-0.1.1 (c (n "sixtyfps-build") (v "0.1.1") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.1") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08clan7lk9xspwbppa2v4vvfxgv3641pr35mbkgzax15fiij6xch")))

(define-public crate-sixtyfps-build-0.1.2 (c (n "sixtyfps-build") (v "0.1.2") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.2") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ifv61l2yam2am65gn08icv6xmaqzhqqgw2lgd5a36yi0rc7pkpz")))

(define-public crate-sixtyfps-build-0.1.3 (c (n "sixtyfps-build") (v "0.1.3") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.3") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v4mvb4p0ggzhv8504kb1j1h5yphga6j0ax66xivdh5mg625wk7k")))

(define-public crate-sixtyfps-build-0.1.4 (c (n "sixtyfps-build") (v "0.1.4") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.4") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vzdmgzh6ln96i8fnzl67nclgg9h2k696gpi1q14xw0n8v62aip6")))

(define-public crate-sixtyfps-build-0.1.5 (c (n "sixtyfps-build") (v "0.1.5") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.5") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wf45h6ksf33f8sgbpqazizm9p2w0dk3f7m3rz31qjfq9430a07c")))

(define-public crate-sixtyfps-build-0.1.6 (c (n "sixtyfps-build") (v "0.1.6") (d (list (d (n "sixtyfps-compilerlib") (r "=0.1.6") (f (quote ("rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xza86js2w12asakwj3kdqpxaacimxdp43a5wdvsidha5729xhyj")))

(define-public crate-sixtyfps-build-0.2.0 (c (n "sixtyfps-build") (v "0.2.0") (h "1qpwm2iviqzj09pxwggbyl1f7hvvrxmrkbw4w18407cixcmzzd23")))

