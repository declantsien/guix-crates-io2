(define-module (crates-io si xt sixth) #:use-module (crates-io))

(define-public crate-sixth-0.1.0 (c (n "sixth") (v "0.1.0") (d (list (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0llvvyn1x5b88k0fy7wr393cjlivcf2vk74sb9gs3gm3rnzywpfv")))

(define-public crate-sixth-0.1.1 (c (n "sixth") (v "0.1.1") (d (list (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "1kxkxkji3s0p8m3kqi4sc9fsry4xccrf2w6j0nzfk46bydz3n0qv")))

(define-public crate-sixth-0.1.2 (c (n "sixth") (v "0.1.2") (d (list (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "1yryhknh0dmnvkhwjrnnpihqm2a81fihpvr54g6nwmg29bxjxfds")))

