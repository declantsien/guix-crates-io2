(define-module (crates-io si xt sixtyfps-macros) #:use-module (crates-io))

(define-public crate-sixtyfps-macros-0.0.1 (c (n "sixtyfps-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.1") (f (quote ("proc_macro_span" "rust"))) (d #t) (k 0)))) (h "1cg8r2wwpr12s6xdhjsiddf4iq77jn1kkysg2xb2ysi4cprll2q4")))

(define-public crate-sixtyfps-macros-0.0.2 (c (n "sixtyfps-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.2") (f (quote ("proc_macro_span" "rust"))) (d #t) (k 0)))) (h "17dv4zr7apb74sxjx86v8j4sjjby59h830mx7lykyzzqyvkai0b7")))

(define-public crate-sixtyfps-macros-0.0.3 (c (n "sixtyfps-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.3") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1z2iff9lsiphh4jvpy5523vyjhdjsdk711wd8mmq0yfngjqazyrx")))

(define-public crate-sixtyfps-macros-0.0.4 (c (n "sixtyfps-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.4") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1isvppcv133wm9ripw88hqndhjm7ffic23qqryz9j03smf4hq89l")))

(define-public crate-sixtyfps-macros-0.0.5 (c (n "sixtyfps-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.5") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0xa5xkm3i7iw2lmkn091qqq8a14irarw6d9k854m96c4x1l8mjns")))

(define-public crate-sixtyfps-macros-0.0.6 (c (n "sixtyfps-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.0.6") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "08fk0bwph4pdjrf0sbrgnry67gifaw3d4l8dmm1ki4zvwgn0n5q9")))

(define-public crate-sixtyfps-macros-0.1.0 (c (n "sixtyfps-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.0") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0d06ikrgi50i3kxdz6m7gs4b422c49p6ifbhll2jkgcrwzyzf3ry")))

(define-public crate-sixtyfps-macros-0.1.1 (c (n "sixtyfps-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.1") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "08a0izqcn0cxlfvly7biw9wk0d2xaljhhg4ff3cjz9f9v8al285h")))

(define-public crate-sixtyfps-macros-0.1.2 (c (n "sixtyfps-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.2") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "13242lzkj4y2j93lnqpq4wd7yhd82paf5jhydh4hd43qmasmy0xa")))

(define-public crate-sixtyfps-macros-0.1.3 (c (n "sixtyfps-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.3") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "0q6za8kvg8l4iqbzd40m5vk7xlmranpljvmsyfm2q4hl31yhp9kg")))

(define-public crate-sixtyfps-macros-0.1.4 (c (n "sixtyfps-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.4") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "03yh91s536ghp2d9ai3fkwjm24pqndnhkvs059h5h48v6m8jb65b")))

(define-public crate-sixtyfps-macros-0.1.5 (c (n "sixtyfps-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.5") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1xizf4pi508v11f019ixas3qf04lwv0jcchgck1vq3rd96a4hrjm")))

(define-public crate-sixtyfps-macros-0.1.6 (c (n "sixtyfps-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sixtyfps-compilerlib") (r "=0.1.6") (f (quote ("proc_macro_span" "rust" "display-diagnostics"))) (d #t) (k 0)) (d (n "spin_on") (r "^0.1") (d #t) (k 0)))) (h "1khz62frniw7hblngb0a8vdbghhnz2qvhvhfx6wrfprhnpxqjmc4")))

