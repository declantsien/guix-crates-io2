(define-module (crates-io si xt sixth_database) #:use-module (crates-io))

(define-public crate-sixth_database-0.1.5 (c (n "sixth_database") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xv14rkk7brbckdv3ficp3d2jffv0a3p6q02n9c36zg99n52xyf1") (y #t)))

(define-public crate-sixth_database-0.1.6 (c (n "sixth_database") (v "0.1.6") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01wvnfqk5sz6h8jdjfal048qay3zhqjq5bv1ik9daraf8zcj75p2") (y #t)))

(define-public crate-sixth_database-0.1.7 (c (n "sixth_database") (v "0.1.7") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dhm9lw6bx17srbf5qgaja9bh912nan45djj5cm8f8n0axk6vgwh") (y #t)))

(define-public crate-sixth_database-0.1.8 (c (n "sixth_database") (v "0.1.8") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1nrgdiqz3l8mihpbg9mh88826xnwzbn9199q5102m2qczhfi6c0n") (y #t)))

(define-public crate-sixth_database-1.0.0 (c (n "sixth_database") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fpdwxpahib152n0zlaf1fw9f4c8w25xkzsrph56iyphyvdpjg9f") (y #t)))

(define-public crate-sixth_database-1.0.1 (c (n "sixth_database") (v "1.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ixdjfjzbawra9yblmsrrj8r6q8al8ayy5950rxhddsxijhwzg9b") (y #t)))

(define-public crate-sixth_database-1.0.2 (c (n "sixth_database") (v "1.0.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "173ymm4f789kqv5r6553l9g3q10swk9sg4ql0ij9wdh2lkasvd9h") (y #t)))

(define-public crate-sixth_database-1.0.4 (c (n "sixth_database") (v "1.0.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)))) (h "09rw3a5whcqj09w3qwfpnibr27vbgqcf33gicjarimfihwgqk40n") (y #t)))

(define-public crate-sixth_database-1.0.5 (c (n "sixth_database") (v "1.0.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)))) (h "1v8894rpz2gn7f1w2ib6jld7189msrc6q7zdzyg3a4n0ax1bryvz") (y #t)))

(define-public crate-sixth_database-1.0.6 (c (n "sixth_database") (v "1.0.6") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)))) (h "1vyx9nc570rb6zw9aqqxdnyzxq04iw3bsmxckkfjc94976wdip10") (y #t)))

(define-public crate-sixth_database-1.0.8 (c (n "sixth_database") (v "1.0.8") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)))) (h "05mw2za1dybr5cvs2s2in4s1j6jbdjwq50ll3xhlivpn4s932b3r") (y #t)))

(define-public crate-sixth_database-1.0.9 (c (n "sixth_database") (v "1.0.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)))) (h "0c1gyh1arl8d1wgzdi52g6hxa4sbqakc3y1sbwhr4kbwp5l0j5h1") (y #t)))

(define-public crate-sixth_database-1.0.10 (c (n "sixth_database") (v "1.0.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)))) (h "0145r6rr6xj1c31wn00i926wyi07dm4l9k1vpb97nnyphk9ryq98")))

