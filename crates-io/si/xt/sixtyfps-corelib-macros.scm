(define-module (crates-io si xt sixtyfps-corelib-macros) #:use-module (crates-io))

(define-public crate-sixtyfps-corelib-macros-0.0.1 (c (n "sixtyfps-corelib-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gs68v81j8g2a1jxn37zgvx5lfr6gch8d217dl4d1ff75y6ppwh0")))

(define-public crate-sixtyfps-corelib-macros-0.0.2 (c (n "sixtyfps-corelib-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m2hbn3wccrfr5kxvimv1vfcxby5hvgidwda3hc52kf23abf437i")))

(define-public crate-sixtyfps-corelib-macros-0.0.3 (c (n "sixtyfps-corelib-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hqg97n27px7x5wr3f6cvdqzwhw8325plbjnf8k37icrcggpggi5")))

(define-public crate-sixtyfps-corelib-macros-0.0.4 (c (n "sixtyfps-corelib-macros") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16v64aj2sn8z952pyy4i8m4n6jjgyg2ldm3nispz3ia8r2gr0ps3")))

(define-public crate-sixtyfps-corelib-macros-0.0.5 (c (n "sixtyfps-corelib-macros") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p43an8bfj0cs3szh1yzsd1ywnyk0386c3s7x858mzb1cf2vdqxr")))

(define-public crate-sixtyfps-corelib-macros-0.0.6 (c (n "sixtyfps-corelib-macros") (v "0.0.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wj9n8gisff6dd8gc41l6j8z9ixgcw81nqnavvjdlg95cmxc7867")))

(define-public crate-sixtyfps-corelib-macros-0.1.0 (c (n "sixtyfps-corelib-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18ig4695ip6hsvj1x401126n9zflpp7l3pb4v31asd1xwn9jnlq2")))

(define-public crate-sixtyfps-corelib-macros-0.1.1 (c (n "sixtyfps-corelib-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01rjqdjmq50npfzp9w1l6s86ppg5wr2pnciqgrr8bq823f9w2vr0")))

(define-public crate-sixtyfps-corelib-macros-0.1.2 (c (n "sixtyfps-corelib-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kn4a3s5v460jc6iapvqv3cl71g6d569rrmkiin5v06mh45qrap1")))

(define-public crate-sixtyfps-corelib-macros-0.1.3 (c (n "sixtyfps-corelib-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r0cyxlwzd76s4vxfh270lg86xq9daxl4k4dz3lb533f6b1xlakk")))

(define-public crate-sixtyfps-corelib-macros-0.1.4 (c (n "sixtyfps-corelib-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nf9y46qaf57j03inw1nfsk30h28xr8rcya18gywis3qxz1ripbv")))

(define-public crate-sixtyfps-corelib-macros-0.1.5 (c (n "sixtyfps-corelib-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l29k4bl2q5p3ym5fqg8aamynbccqwz2v394y9qqghv0j4gzlnk7")))

(define-public crate-sixtyfps-corelib-macros-0.1.6 (c (n "sixtyfps-corelib-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06389gbiqd175bl25daj6i0c5afhf7vg79lr9q2xp4dnd10lbjzi")))

