(define-module (crates-io si p2 sip2) #:use-module (crates-io))

(define-public crate-sip2-0.1.0 (c (n "sip2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0827ls82s8jsgg4xi8sml6cv8d1vz94gis1msx59yd3rl1231fih")))

(define-public crate-sip2-0.1.1 (c (n "sip2") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17wm86jvxzmh2qy5vaa4rqqljk3xl3vswh75vpcpj8nm8z7q2z2h")))

(define-public crate-sip2-0.1.2 (c (n "sip2") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1y6q4mix48yqfc9yw4a6csfgbp24rwx62vfg1c6ilaf293milqk7")))

(define-public crate-sip2-0.2.0 (c (n "sip2") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "17sz7k3rvip0ifkjbpbba8vakv27dq1j65qg4ppzi1kv8rxlv67n") (f (quote (("json" "serde_json"))))))

(define-public crate-sip2-0.2.1 (c (n "sip2") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "19wphldlkazqcc7bciwlhzr408ad25p356dqj4174bvvv8fr7306") (f (quote (("json" "serde_json"))))))

(define-public crate-sip2-0.2.2 (c (n "sip2") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ndgik9xz79ywch41ixvnky9zbp616b2aiszz6k2aj3qnisfndd0")))

(define-public crate-sip2-0.3.0 (c (n "sip2") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "deunicode") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0yjf5091s64r8nvx1q98cnxbbapjkhjf4xfbh1s5isnkgy7fyld0")))

(define-public crate-sip2-0.3.1 (c (n "sip2") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "deunicode") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xh63f1c1mm7ac4whq0i0ir6417128lw8v13sqykb5cg3g5jwqd4")))

