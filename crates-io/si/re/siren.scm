(define-module (crates-io si re siren) #:use-module (crates-io))

(define-public crate-siren-1.0.0 (c (n "siren") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0pwzhk6qkrbam95233j2fgjcldw12nwdpc3v0jvmy1wjiiikfp5k")))

(define-public crate-siren-1.0.1 (c (n "siren") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0rprslnl7v8z1zzlxnl46s8f9gi6akaxcap64q8mxrcwc0drjh66")))

(define-public crate-siren-1.0.2 (c (n "siren") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1zl64hix65a3r35d9jyr2m2zp3li8c0rbqgp4vp9bh13zhxlyslf")))

(define-public crate-siren-1.0.3 (c (n "siren") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "18vqwm3qsq1s55hz48iki1xgl1p523cylmbc3nb4l919xracdldm")))

(define-public crate-siren-1.1.0 (c (n "siren") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0iinayw3azzyrqc35s24bcz24f6n57nfv6ashhnplphlx4sf76cn")))

(define-public crate-siren-1.1.1 (c (n "siren") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0j2z8d9wnwvlrpk4m0gkqcr2al5acfyvh0j2354mpbwz1q6svdx3")))

(define-public crate-siren-1.1.2 (c (n "siren") (v "1.1.2") (d (list (d (n "ansi_term") (r "~0.12.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.40") (d #t) (k 0)))) (h "0l0lqwm2cmbh7d6iv6y895awdyc5mg3lrjgc9k15iq0ca859i7nb")))

(define-public crate-siren-1.1.3 (c (n "siren") (v "1.1.3") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.44") (d #t) (k 0)))) (h "0yh5fi0q2bd8m53s8q50lrxc9qv6wn2zh82xwbjalf8wqd8n468a")))

(define-public crate-siren-1.2.0 (c (n "siren") (v "1.2.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.44") (d #t) (k 0)))) (h "17aj57x0y63fv0l2ci7jyrvv4z51rv2ac1mw3nxxsyf5yzz455rp")))

(define-public crate-siren-1.3.0 (c (n "siren") (v "1.3.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.56") (d #t) (k 0)))) (h "1ifpdxisc6s7gk8vla12hiy84ik6wj8hqi8fifz9x734i6d45xbi")))

(define-public crate-siren-1.3.1 (c (n "siren") (v "1.3.1") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.56") (d #t) (k 0)))) (h "1cd4jq62f71n63n2cnji9mbx004cg6s9fzbccg8npvzfdk28iwg5")))

(define-public crate-siren-1.4.0 (c (n "siren") (v "1.4.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~3.0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.74") (d #t) (k 0)))) (h "1p00wpajr9i8kq94wd3xzyarnrb0ivv71yifh6s8lk7sjs80a54i")))

(define-public crate-siren-1.5.0 (c (n "siren") (v "1.5.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~3.2.4") (d #t) (k 0)) (d (n "serde") (r "~1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.81") (d #t) (k 0)))) (h "1xj7brjsjpdpxzr3jfx1ll20059w2k7fqkma97qz46hccwdghd2r")))

(define-public crate-siren-1.5.1 (c (n "siren") (v "1.5.1") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "clap") (r "~3.2.4") (d #t) (k 0)) (d (n "serde") (r "~1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.81") (d #t) (k 0)))) (h "1bhwh8bfaf20nwna9z1h7dvdvrdhij06krih71ficlbxp3d22gf8")))

