(define-module (crates-io si re sirena) #:use-module (crates-io))

(define-public crate-sirena-0.0.1 (c (n "sirena") (v "0.0.1") (h "1kvclcnnnbjnaf6cvhdp89d23n1457539gpyad67jy6zs6f90ppv")))

(define-public crate-sirena-0.1.0 (c (n "sirena") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "microfft") (r "^0.5") (f (quote ("size-1024"))) (k 0)) (d (n "micromath") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qnww4pb509r1ghxdw3015yy26kxhf6klc2523smin2s98dw3x5a") (s 2) (e (quote (("defmt" "dep:defmt"))))))

