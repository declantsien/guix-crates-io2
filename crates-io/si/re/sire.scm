(define-module (crates-io si re sire) #:use-module (crates-io))

(define-public crate-sire-0.0.1-alpha (c (n "sire") (v "0.0.1-alpha") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "1fcx6lzww28dmrpmyfcdmjvjj88xg8j8pf16fzv88i16i2lp2xhj")))

(define-public crate-sire-0.0.1-alpha1 (c (n "sire") (v "0.0.1-alpha1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "0ws5yh8rvq3nzshl70y5czv2hb6c73fm2hv1ypmnzj2lkglg31f9")))

(define-public crate-sire-0.0.1-alpha2 (c (n "sire") (v "0.0.1-alpha2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "15nmn2s64jwcyyr5haydal6k9l0ag7p8s30zgkqz73bz3ri94zfj")))

(define-public crate-sire-0.0.1-alpha3 (c (n "sire") (v "0.0.1-alpha3") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "1ahshrvvh87k1ifq12a1vg0dw9118b8ab0pr8ip7nynnrj9f37rd")))

(define-public crate-sire-0.0.1-alpha4 (c (n "sire") (v "0.0.1-alpha4") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "1f4iimjnlp1c0b3lma1f6viwzqyxg5c6j0yl1gha4lcdcwmggvgk")))

(define-public crate-sire-0.0.1-alpha6 (c (n "sire") (v "0.0.1-alpha6") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "12wh3pmjg9ifc3ldf19963i2kx68f2fxnirccizg1058kjgb56jn")))

(define-public crate-sire-0.0.1-alpha7 (c (n "sire") (v "0.0.1-alpha7") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)))) (h "11lmwj43x98bp2rq8ar357lv701zpxp8a2qflk58fks1g8vckwja")))

