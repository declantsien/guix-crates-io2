(define-module (crates-io si re siren-types) #:use-module (crates-io))

(define-public crate-siren-types-0.1.0 (c (n "siren-types") (v "0.1.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "readonly") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "19lh8pqw55w2697xik0b93daza3p2pan5bf70pzhn5pil79842cj")))

(define-public crate-siren-types-0.1.1 (c (n "siren-types") (v "0.1.1") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "readonly") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1zd7w2hnm8rdr2yk0gx8nqx039bc9919pc88wfs4l3mpajkypwvj")))

(define-public crate-siren-types-0.2.0 (c (n "siren-types") (v "0.2.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "readonly") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0w68hibfw81mi2nvizqkajax8ynxdgg0pmqzd2l2463nv9hx7597")))

(define-public crate-siren-types-0.2.1 (c (n "siren-types") (v "0.2.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "readonly") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "05ml71szjx0d1az22yvlwfv80l9n8fafaddznjr15wsvmhj8pwp4")))

(define-public crate-siren-types-0.2.2 (c (n "siren-types") (v "0.2.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0rni1i42nm9gl8j5nx0a4jj1xi3a0604bx4pnqiiw117wwp199i3")))

