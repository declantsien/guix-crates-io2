(define-module (crates-io si re sirena-types) #:use-module (crates-io))

(define-public crate-sirena-types-0.1.0 (c (n "sirena-types") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0rhyyb7virn6hfcivhpk7a7rpzndpgbj5c91njxpf1yi7yr6qmrq")))

(define-public crate-sirena-types-0.1.1 (c (n "sirena-types") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "03g04g50gpmgjiln0m3xa40zcm1mhfqfzvbxvkpw2dnrdxx3d9q5")))

(define-public crate-sirena-types-0.1.2 (c (n "sirena-types") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1b29jv86xfy51nmzi2q1wwpwxq5xws3p29q2fpv5wk49v498l3v1")))

(define-public crate-sirena-types-0.1.3 (c (n "sirena-types") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "13sl2a4w3cphr2i63dswc083sz78s2hya7lc211xl5bhz8yhnf4h")))

