(define-module (crates-io si on sion-rs) #:use-module (crates-io))

(define-public crate-sion-rs-0.1.0 (c (n "sion-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "074zl79x6525rhbyng2k814h6v8yf5bvss2631a8mi239akmymsj")))

(define-public crate-sion-rs-0.1.1 (c (n "sion-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0jfs56z876n2dx56s6qn88sjbz6mhzy9cb88gzwdwn6n7cqarxyx")))

