(define-module (crates-io si g- sig-rs) #:use-module (crates-io))

(define-public crate-sig-rs-1.0.0 (c (n "sig-rs") (v "1.0.0") (h "13xl9ba2m2n0wc028m8jay88f67d1p64zh9l3wly9zw4mnmasqxf")))

(define-public crate-sig-rs-1.0.1 (c (n "sig-rs") (v "1.0.1") (h "1jvbiixspza55gf3pdirskjhxpgd1wd9iyi38m9yaa7wjfbqmfww")))

