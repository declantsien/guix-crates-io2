(define-module (crates-io si g- sig-bitmap) #:use-module (crates-io))

(define-public crate-sig-bitmap-0.0.1 (c (n "sig-bitmap") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "0zqcnwx2ns74hcvpl0f089900z8dsghs8rbv1d0lby9pyha1mag7") (y #t)))

(define-public crate-sig-bitmap-0.0.2 (c (n "sig-bitmap") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "1xcmin23ay69yr2iia3dq12aa4yjby6i7xxqha0jm1fs9rr9n4h8") (y #t)))

(define-public crate-sig-bitmap-0.0.3 (c (n "sig-bitmap") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "02dr9sl6dzmbm66v9vn282sf1i09abmfpaj24clx1xxxknbl7xsa") (y #t)))

(define-public crate-sig-bitmap-0.0.4 (c (n "sig-bitmap") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "1jx4hzrcjrrd7l4p1y0lrwih7xr6ag40svgp5xl54pcbgn771hvj") (y #t)))

(define-public crate-sig-bitmap-0.0.5 (c (n "sig-bitmap") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "0d8dnn6drb1vdxsdvb8wmgq32x40r4qaspa2rahbj17gc9k6nhsx") (y #t)))

(define-public crate-sig-bitmap-0.0.6 (c (n "sig-bitmap") (v "0.0.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "1kjrqq1lkjcjxc3gx4yspn1mcx34r3m2a8d1d4c8rlbwdkmzq380") (y #t)))

(define-public crate-sig-bitmap-0.0.7 (c (n "sig-bitmap") (v "0.0.7") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "13vyly4yjhvgwc7x09kka0l63q6bc5gb4i3n1hazl69n97zfkia8") (y #t)))

(define-public crate-sig-bitmap-0.0.8 (c (n "sig-bitmap") (v "0.0.8") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "1pzryyqnvaz2cjdmgg4al7s7i29zhxwxiib1fkh7flr5r3lr2w13") (y #t)))

(define-public crate-sig-bitmap-0.0.9 (c (n "sig-bitmap") (v "0.0.9") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "0w440q339z6db1746qi795k7dd7h8fxvln7b6q780px6lzgyghwz")))

