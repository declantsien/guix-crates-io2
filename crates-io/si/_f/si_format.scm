(define-module (crates-io si _f si_format) #:use-module (crates-io))

(define-public crate-si_format-0.1.0 (c (n "si_format") (v "0.1.0") (d (list (d (n "bounded-integer") (r "^0.5.7") (f (quote ("types"))) (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "01gd7yiv2w6fazyk1vmxwlxnddih76fimi7zva5j20y8pp4zhm10") (f (quote (("std") ("float64" "float32") ("float32") ("default" "std" "float64"))))))

(define-public crate-si_format-0.1.1 (c (n "si_format") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "0plnnk647g1aq62mm63k6bmh6hm3srl0idz0la5vpm9nnyvlsq4n") (f (quote (("std") ("float64" "float32") ("float32") ("default" "std" "float64"))))))

