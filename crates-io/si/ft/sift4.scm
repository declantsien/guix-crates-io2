(define-module (crates-io si ft sift4) #:use-module (crates-io))

(define-public crate-sift4-0.1.0 (c (n "sift4") (v "0.1.0") (h "16cpqb5lr5ifs3jzrv29g4570kgvwxz9v2kpz307nh8qdhgb60rh")))

(define-public crate-sift4-0.1.1 (c (n "sift4") (v "0.1.1") (h "1k1zfzyispgr81b2zfl1h91gjy2i5n3dcf104gx45s0fiihll4pz")))

(define-public crate-sift4-0.1.2 (c (n "sift4") (v "0.1.2") (h "1jclg69533904ffd36905b8xzi69j94l16fmxgqyh9n99dd1bbx3")))

(define-public crate-sift4-0.1.3 (c (n "sift4") (v "0.1.3") (h "0li2ywghr6ikbivbriq08ys9ijv3kjnk7yih8wfbv6h18w5xnm38")))

(define-public crate-sift4-0.1.4 (c (n "sift4") (v "0.1.4") (h "0h2gp1jcdb3466wg3j9hyvbvxfy81ncxldm1w3k82kqkqc6pkp01")))

