(define-module (crates-io si ft sift) #:use-module (crates-io))

(define-public crate-sift-0.1.0 (c (n "sift") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "simd-json") (r "^0.2.2") (d #t) (k 0)))) (h "0w8bjyjra2jrxxh30svywsy3hh6fmrpnfmdp0yi5cwa80igdqykl")))

