(define-module (crates-io si ii siiir_points) #:use-module (crates-io))

(define-public crate-siiir_points-0.1.0-beta (c (n "siiir_points") (v "0.1.0-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "09n5kfsc94142i0gnra5ywjkr9zv58jfzj024hdczmgqygylkfyq") (y #t)))

(define-public crate-siiir_points-0.1.1-beta (c (n "siiir_points") (v "0.1.1-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "0pxyswi3a5x39r8kakmgp31i563nc8c5h6pzx8n64kwsnzq85rpi")))

(define-public crate-siiir_points-0.1.2-beta (c (n "siiir_points") (v "0.1.2-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "106899rvagwml1cpg2hgf7dy8yzi1z1c2xihbns4kj65naf5397f")))

(define-public crate-siiir_points-0.1.3-beta (c (n "siiir_points") (v "0.1.3-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "0aqyd8ff6jq5jzajjwdwqlclx6jfqzwpbdckky4cq8hmynb9cpmn")))

(define-public crate-siiir_points-0.1.4-beta (c (n "siiir_points") (v "0.1.4-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "0ip4w7y98bjbl4yjh196ghjnylsm7j037yyavmckiv2n6l51xvqs")))

(define-public crate-siiir_points-0.1.5-beta (c (n "siiir_points") (v "0.1.5-beta") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "1vjmsnd944k4xiygcadlb0z97gjfv1zqbg4p0jg0sy0an9hvaypm")))

