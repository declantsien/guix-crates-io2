(define-module (crates-io si ii siiir_modular_power) #:use-module (crates-io))

(define-public crate-siiir_modular_power-1.0.6 (c (n "siiir_modular_power") (v "1.0.6") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ss4pag1wqqyrgi52pvc556i928r72rkq8w0hgkaghwzrmcyim8x")))

(define-public crate-siiir_modular_power-1.0.7 (c (n "siiir_modular_power") (v "1.0.7") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ci0v3mybbxv4a4z0xnn0agdy7sssp2mg6j6sl4kjjzflkj36l48")))

