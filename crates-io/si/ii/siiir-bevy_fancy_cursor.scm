(define-module (crates-io si ii siiir-bevy_fancy_cursor) #:use-module (crates-io))

(define-public crate-siiir-bevy_fancy_cursor-0.4.0 (c (n "siiir-bevy_fancy_cursor") (v "0.4.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "04f8zn25wn2j8xwa9l17cr4hvwirqxbkw52i54kb4fld38dqbs5i") (y #t)))

(define-public crate-siiir-bevy_fancy_cursor-0.4.1 (c (n "siiir-bevy_fancy_cursor") (v "0.4.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "028p89b63ib7z0s4hbba028zrd0zccv28mgcrvyh3kx34w6k8kz1")))

(define-public crate-siiir-bevy_fancy_cursor-0.4.2 (c (n "siiir-bevy_fancy_cursor") (v "0.4.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1nrx9dybnb5rnj4pwsgcg3c870br6sxqwn5axppm10r7zwxjplk1") (y #t)))

(define-public crate-siiir-bevy_fancy_cursor-0.4.3 (c (n "siiir-bevy_fancy_cursor") (v "0.4.3") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "17yl97nk6cvk20waip4phiv56fnilrcprghns2rq6b9099ik7xgg")))

