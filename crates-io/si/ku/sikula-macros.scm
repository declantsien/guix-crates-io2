(define-module (crates-io si ku sikula-macros) #:use-module (crates-io))

(define-public crate-sikula-macros-0.2.0 (c (n "sikula-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "05mcj02fxx1lng6h3jf2nh7yliy6rl4c88fv6kj97c0jr4p4bd4v")))

(define-public crate-sikula-macros-0.3.0 (c (n "sikula-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pg29la1vdsg8n7v3cabkchxwf95hjz4f6c9wl1nlllpk236dkw9")))

(define-public crate-sikula-macros-0.3.1 (c (n "sikula-macros") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0a57wnls7mqb80x9f7zk132fj26myvr26l87zhrhlcjqs4jk617n")))

(define-public crate-sikula-macros-0.4.0-alpha.1 (c (n "sikula-macros") (v "0.4.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0amdb9lqdfyin5zkm6bzkm9ywfvsmbf086wyx9jsspjfz790pfgg")))

(define-public crate-sikula-macros-0.4.0-alpha.2 (c (n "sikula-macros") (v "0.4.0-alpha.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0khngkfqx83ks7bhddsmlkmyagb7fmy2cjqsin5jwanwha0gj9zg")))

(define-public crate-sikula-macros-0.4.0-alpha.3 (c (n "sikula-macros") (v "0.4.0-alpha.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03lmqmw2w6psw7x8mwrd753n3z2s6kfydy0krqvannv4l8wiiv13")))

(define-public crate-sikula-macros-0.4.0-alpha.4 (c (n "sikula-macros") (v "0.4.0-alpha.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19xpz56pf8y7wmk39gg074n8g8mpn3wrf8kkj61yyc72ibl9fs1m")))

(define-public crate-sikula-macros-0.4.0 (c (n "sikula-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1545g2y1h8s1lgasx5mb2rm94kqi3viik998mr1jqm8pv89s3ysd")))

(define-public crate-sikula-macros-0.4.1 (c (n "sikula-macros") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6jbj4plkpl9315maq71fx5smif3kkgphsnf08flvlyxc4a1pq7")))

(define-public crate-sikula-macros-0.4.2 (c (n "sikula-macros") (v "0.4.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ljcgwq9il2kg5ds3vch83f1pa3plmqck7x7xfcrjl1v5hkgn791")))

