(define-module (crates-io si gi sigil) #:use-module (crates-io))

(define-public crate-sigil-0.1.0 (c (n "sigil") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0sxjqfarzdschkkcxp4pkymrbhsq2lvyabq66fgqigvy9lnca3hh")))

(define-public crate-sigil-0.1.1 (c (n "sigil") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "10j3b6xf6frqmjkmxiz8yz9w5ysl6ayfyms6xcjr5iyqp0rbkfsb")))

(define-public crate-sigil-0.2.0 (c (n "sigil") (v "0.2.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "1anrnlb7jwn4dlhlhcq960rrn4lcmkw538b8cb5x1yln3dn8jnsp")))

