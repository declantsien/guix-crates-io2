(define-module (crates-io si #{53}# si5351) #:use-module (crates-io))

(define-public crate-si5351-0.1.2 (c (n "si5351") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)))) (h "1i5ixr5cc6vvrkc89zwsvmyy0qldblzx5n0ym5gyks5qgyl25z4d")))

(define-public crate-si5351-0.1.3 (c (n "si5351") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)))) (h "04s992bywcrva30hakf1gji3nqzf03hj0bnsqb76mrjdaz44zgyv")))

(define-public crate-si5351-0.1.4 (c (n "si5351") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)))) (h "0rxd78qmzdn7aa8g5an9cbppnacy9gprw84kr4nrhajvdc9ln838")))

(define-public crate-si5351-0.1.5 (c (n "si5351") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1") (d #t) (k 0)))) (h "17441l616yxla835vgin90fnzlg27zs6vv0bpkxivfj5ddgdrp3g")))

(define-public crate-si5351-0.2.0 (c (n "si5351") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "1f5zx86bfvci6gns2fkj5dmiib03acx9bzzxwly9cr8150giyv60")))

