(define-module (crates-io si pc sipcore) #:use-module (crates-io))

(define-public crate-sipcore-0.0.0 (c (n "sipcore") (v "0.0.0") (h "06845gmxpal1d50ivz1sakgisj29nlhvscs6y29yb63y1n9mgsdn")))

(define-public crate-sipcore-0.0.1 (c (n "sipcore") (v "0.0.1") (d (list (d (n "sipmsg") (r "^0.1.0") (d #t) (k 0)))) (h "09p3ma2g9g0vf4pc1vgl2yd43w9in8lq59fz6lk4wkvvlh1y041h")))

(define-public crate-sipcore-0.0.2 (c (n "sipcore") (v "0.0.2") (d (list (d (n "sipmsg") (r "^0.2.0-beta") (d #t) (k 0)))) (h "004is97ypg4ln5db1dkvg2va395xg0mrfnhxsq0b89a9ks301npm")))

