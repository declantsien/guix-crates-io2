(define-module (crates-io si _t si_trace_print) #:use-module (crates-io))

(define-public crate-si_trace_print-0.1.0 (c (n "si_trace_print") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "1cmwhhyc3d8b0gkm8cxp6yg2m5v59dm35qr4748a2lq6zb5bhlaw")))

(define-public crate-si_trace_print-0.1.1 (c (n "si_trace_print") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "08rpivm69a2ghv8nnddsizdngq7h4vq05dbllawdz8h39fgnpdby")))

(define-public crate-si_trace_print-0.1.2 (c (n "si_trace_print") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0sni1k44g712372b5kgi56jf58jc7zpbzhr0652s155mg1y30dn6")))

(define-public crate-si_trace_print-0.1.3 (c (n "si_trace_print") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0a6c55nl83hj4bpbnav93p2km6j0y9by38xn35iafy998djb11x9")))

(define-public crate-si_trace_print-0.2.4 (c (n "si_trace_print") (v "0.2.4") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "042g5jfsrwnlyrb5xlmc37n0r8bwpja27xmlz2ng22kqg5y6bqis")))

(define-public crate-si_trace_print-0.2.5 (c (n "si_trace_print") (v "0.2.5") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "16lyah7gkyihxx192qkj2nxsd7zfakkbir24jyk650cp7m594r3d")))

(define-public crate-si_trace_print-0.3.6 (c (n "si_trace_print") (v "0.3.6") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "1qdfc459jmmnzxzjgrm8vng1bxvwyi76f6lf2jkgwdr1gr17dspv") (r "1.54.0")))

(define-public crate-si_trace_print-0.3.7 (c (n "si_trace_print") (v "0.3.7") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "1gsynwcxl4bj43hj38jw4wfdmwzkf175ix59ynddgznfgsmaq3p0") (r "1.54.0")))

(define-public crate-si_trace_print-0.3.8 (c (n "si_trace_print") (v "0.3.8") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0hk99xzcbd3knwqmd4h20lh34mv39gn0c9kbrpfpcywn239a4k2f") (r "1.54.0")))

(define-public crate-si_trace_print-0.3.9 (c (n "si_trace_print") (v "0.3.9") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "18jnaxz9r5v46z097ip9h119alx8108wplg4c78kh49ahppc0apb") (r "1.54.0")))

(define-public crate-si_trace_print-0.3.10 (c (n "si_trace_print") (v "0.3.10") (d (list (d (n "backtrace") (r "^0.3.68") (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "1659w26nc4c8437ll6m9jiz3j9jx988dbr98fsdj4bgl5filygp9") (r "1.56.0")))

(define-public crate-si_trace_print-0.3.11 (c (n "si_trace_print") (v "0.3.11") (d (list (d (n "backtrace") (r "^0.3.68") (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0rxl621a12p7yqhaknm62p6y07xz8vd10gabvvnalaaifyw1k8qh") (r "1.56.0")))

(define-public crate-si_trace_print-0.3.12 (c (n "si_trace_print") (v "0.3.12") (d (list (d (n "backtrace") (r "^0.3.68") (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "1mb510zzfsrdbr26pvml6k5sx1qd87wnvypr6icx7bwfc37m3r3i") (r "1.56.0")))

