(define-module (crates-io si #{47}# si4703) #:use-module (crates-io))

(define-public crate-si4703-0.1.0 (c (n "si4703") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "004f5xzqnacjn8jw9pbx9y16lqf8cmwna851snjj5dhc6y8qqxnx")))

