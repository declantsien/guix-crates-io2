(define-module (crates-io si lh silhouette) #:use-module (crates-io))

(define-public crate-silhouette-0.1.0 (c (n "silhouette") (v "0.1.0") (d (list (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "try_default") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0cspmanij7nxnyyyc3zr5906cf6n43zwjmgxj0ba0v4s721lkn1j") (f (quote (("default")))) (s 2) (e (quote (("nightly" "dep:try_default"))))))

