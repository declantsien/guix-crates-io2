(define-module (crates-io si l_ sil_first) #:use-module (crates-io))

(define-public crate-sil_first-0.1.0 (c (n "sil_first") (v "0.1.0") (h "184pg4xq60pai7fvvmld81kpjaawmqcnm4p00c55h43qa7vc4hwr") (y #t)))

(define-public crate-sil_first-0.1.1 (c (n "sil_first") (v "0.1.1") (h "1h7ywqzbqfi6g9vy2529acvmlcvvx9afrss4q0g13bz2d2xs24pn")))

