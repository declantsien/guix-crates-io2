(define-module (crates-io si no sino) #:use-module (crates-io))

(define-public crate-sino-0.1.0 (c (n "sino") (v "0.1.0") (d (list (d (n "llvm-sys") (r "^160") (d #t) (k 0)))) (h "1v8scykjami37pyzf7krz2fraw6mf8bb9igmhjwm7wgbmrrq8m9d") (y #t)))

(define-public crate-sino-0.0.1 (c (n "sino") (v "0.0.1") (d (list (d (n "llvm-sys") (r "^160") (d #t) (k 0)))) (h "0gndyq0k1ks9k5as2rr2xpcqgf4wxbz1wjwz2xbhgldxkx74x0hq")))

(define-public crate-sino-0.0.2 (c (n "sino") (v "0.0.2") (d (list (d (n "inkwell") (r "^0.4.0") (f (quote ("llvm17-0"))) (d #t) (k 0)))) (h "0ca8vg9yai7yi16b8gim6nlb1xdl87fqwraqyzmiynkd2fq5wpw8")))

