(define-module (crates-io si n_ sin_cos_ln_sqrt) #:use-module (crates-io))

(define-public crate-sin_cos_ln_sqrt-0.1.0 (c (n "sin_cos_ln_sqrt") (v "0.1.0") (h "1gfwpsy3c66xrbxf0l431v0fj9pcl5gscj5mr4d4xddz7n7b8v2n")))

(define-public crate-sin_cos_ln_sqrt-0.1.1 (c (n "sin_cos_ln_sqrt") (v "0.1.1") (h "1mmq4mkcb4d0pb1gv2fgzhfsj11fyac00j8pjnl618hbmff5ii73")))

