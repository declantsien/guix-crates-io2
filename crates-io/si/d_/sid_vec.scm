(define-module (crates-io si d_ sid_vec) #:use-module (crates-io))

(define-public crate-sid_vec-0.1.0 (c (n "sid_vec") (v "0.1.0") (d (list (d (n "sid") (r "^0.1.0") (d #t) (k 0)))) (h "18b86vfvlgxxqgazj2ryg86ac44mr24z7vq5zsvxmkblzpqg9wv2")))

(define-public crate-sid_vec-0.2.0 (c (n "sid_vec") (v "0.2.0") (h "11sgjc44560pyzijvihyr5lfjid53wwy61y9hz91sndmdhpii2lj")))

(define-public crate-sid_vec-0.2.1 (c (n "sid_vec") (v "0.2.1") (h "1wvhfc3qngi91n13hyxq12nk11izvlcqa9ma5pqnf96xx5dhs9iq")))

