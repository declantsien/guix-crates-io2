(define-module (crates-io si f_ sif_macro) #:use-module (crates-io))

(define-public crate-sif_macro-0.1.0 (c (n "sif_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0p68j8vsh6f7mnnfffylv00p0vxnlrwpx29l3gzbvxwjhrqma9b3")))

