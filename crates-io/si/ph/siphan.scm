(define-module (crates-io si ph siphan) #:use-module (crates-io))

(define-public crate-siphan-0.1.0 (c (n "siphan") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "1372l7qwjlqsv1ds6zfxppxvxva1avh67947aacnb7diy9vp5nqa")))

(define-public crate-siphan-0.1.1 (c (n "siphan") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "0mg7dvigdqx0yh7ms29lgr5pa0j2ky0b3czirm64hn7ih3hd1mk6")))

