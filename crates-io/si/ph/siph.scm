(define-module (crates-io si ph siph) #:use-module (crates-io))

(define-public crate-siph-0.1.0 (c (n "siph") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "19r3dvljfp9d7v4yx4gxmzi7yphp9shs0np64vssfa6xgg1kqh90")))

