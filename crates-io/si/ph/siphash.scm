(define-module (crates-io si ph siphash) #:use-module (crates-io))

(define-public crate-siphash-0.0.0 (c (n "siphash") (v "0.0.0") (h "1rf3y3g3bxzys9vc5ipc3q1crack8z6f2l94rpf8s7lg7sjg1706")))

(define-public crate-siphash-0.0.2 (c (n "siphash") (v "0.0.2") (h "0n2h8cgm1zjrlgrbwysfdqxnsz5p3wvmjwk68cy40ixasg9g39a7")))

(define-public crate-siphash-0.0.4 (c (n "siphash") (v "0.0.4") (h "02anvappm6vsz22m9d8sra8bvilc1fha9y6j69sjh31idviry47n")))

(define-public crate-siphash-0.0.5 (c (n "siphash") (v "0.0.5") (h "1bqmkb2d9yrjzrvzzbahyc335c1nksb3ddfj4p9ypr6q63m51clp")))

