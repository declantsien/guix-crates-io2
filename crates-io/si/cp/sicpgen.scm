(define-module (crates-io si cp sicpgen) #:use-module (crates-io))

(define-public crate-sicpgen-0.1.0 (c (n "sicpgen") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1c3flqv94gaabg8xkvlz9gc0z4kkq6dnljjghsdldk4s75s6f1cb")))

(define-public crate-sicpgen-0.1.1 (c (n "sicpgen") (v "0.1.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1392ln2bmw8dnjc40mkzhp4wgqb8gplw0vwbzs9za9jl0acj2anz")))

