(define-module (crates-io si gt sigtransplant) #:use-module (crates-io))

(define-public crate-sigtransplant-0.1.0 (c (n "sigtransplant") (v "0.1.0") (d (list (d (n "goblin") (r "^0.2.1") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scroll_derive") (r "^0.10.1") (d #t) (k 0)))) (h "08hn6v5j6x78nmhr6j6k9z0ym4aa7r27iya5qh7dnrvcgsm9zw0j")))

