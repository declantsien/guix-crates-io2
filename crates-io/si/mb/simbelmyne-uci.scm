(define-module (crates-io si mb simbelmyne-uci) #:use-module (crates-io))

(define-public crate-simbelmyne-uci-0.1.0 (c (n "simbelmyne-uci") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chess") (r "^0.1.0") (d #t) (k 0) (p "simbelmyne-chess")))) (h "0yvnzh55502dvpsa9dwzmzr7x1iw7sw68bajp1qwsdddxgnf53a5")))

