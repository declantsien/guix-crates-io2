(define-module (crates-io si mb simble) #:use-module (crates-io))

(define-public crate-simble-0.1.0 (c (n "simble") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wjc7s563qllgpcfn69zvf88jpwm0qsbcim1vkz6w19w04zj6n1c") (f (quote (("nightly"))))))

(define-public crate-simble-0.1.1 (c (n "simble") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qs30gkci320dhrla9rfbjskv0lcrizbxd98bh795m2w52wd2lyw") (f (quote (("nightly"))))))

(define-public crate-simble-0.1.2 (c (n "simble") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qq09wf2gimn40z6krpjfvi3mifi4d5qawqqf0s16bhllhaswwgg") (f (quote (("nightly"))))))

(define-public crate-simble-0.1.3 (c (n "simble") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nyl7ra86v7x73vr79v09q5fsympwvk2gds458zk9q4a33in9r1z") (f (quote (("nightly"))))))

