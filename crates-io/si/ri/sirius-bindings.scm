(define-module (crates-io si ri sirius-bindings) #:use-module (crates-io))

(define-public crate-sirius-bindings-0.1.0 (c (n "sirius-bindings") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0fkk5inn71zq05ir1bh4yvm3ilg8bdzlwcwy2vhs166a0qak95i7") (s 2) (e (quote (("fuzz" "dep:arbitrary"))))))

