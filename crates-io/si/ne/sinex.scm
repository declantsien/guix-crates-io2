(define-module (crates-io si ne sinex) #:use-module (crates-io))

(define-public crate-sinex-0.0.1 (c (n "sinex") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinex") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10bnkmly8f48swfs63s5v9pmm7lhcam6jbb9nkc61dgl92lydwgs")))

(define-public crate-sinex-0.1.0 (c (n "sinex") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinex") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1541bwj915b6g6kjvdhwbdpyvrcq9d07yl0m765vq0br4b7kg36j")))

(define-public crate-sinex-0.1.1 (c (n "sinex") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinex") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r6ds0rs90kbkxlvy17fh6p1c8x75hhg45j3qnw92hzzk81xn50j")))

(define-public crate-sinex-0.2.0 (c (n "sinex") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinex") (r "^0.10.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lvjgzpv5gz8hfg2b11y1pk9sc9n0iqr8yjvx0lwbd9c28wg960v")))

(define-public crate-sinex-0.2.1 (c (n "sinex") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinex") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14la52mzgaknva8h34j8221gawd2hkrgc3n2pa6i8nh914hh6sza")))

(define-public crate-sinex-0.2.2 (c (n "sinex") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gnss-rs") (r "=2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p2b4xfxkc1akvydhpyj92ih1ffb0zxrdd7qdpkka0j64ipbh8f7")))

(define-public crate-sinex-0.2.3 (c (n "sinex") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gnss-rs") (r "^2.1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zfm79vpifd5vdi6zy0a31gsx3yz6ydlx4g2zhmmcbsckqkds4zb")))

