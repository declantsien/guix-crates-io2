(define-module (crates-io si na sinais) #:use-module (crates-io))

(define-public crate-sinais-0.1.0 (c (n "sinais") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)) (d (n "random_name_generator") (r "^0.3.6") (d #t) (k 2)) (d (n "sinais_macro") (r "^0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.15") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0whr557y34fxb96aw5f0v702w5wm0xcba41s5g82yrilnmb0bnd6")))

