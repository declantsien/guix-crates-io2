(define-module (crates-io si na sinais_macro) #:use-module (crates-io))

(define-public crate-sinais_macro-0.1.0 (c (n "sinais_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bg5cq449g4kmxg4bs07gm9kiwqm9n9n7gf23ba59bv8nlsyiqbi")))

