(define-module (crates-io si ms simsimd) #:use-module (crates-io))

(define-public crate-simsimd-0.1.0 (c (n "simsimd") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zzcib7lnbsrsr2l5bw7vys2mi2zk93lw201z8qlgalminwff0j0") (y #t)))

(define-public crate-simsimd-0.1.1 (c (n "simsimd") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ad73a0qrlmwzgbghblzisayibiyz8bhzdcjzfg22jy3ky4dc1c5") (y #t)))

(define-public crate-simsimd-0.1.2 (c (n "simsimd") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04bw2v1zbsc82dvpmg33267l47kvj36bjmhrda8qz8haglx9lavf") (y #t)))

(define-public crate-simsimd-3.7.3 (c (n "simsimd") (v "3.7.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13l5swc1ivy0jamx4prw2hgxn332kkw3139jmd8faimljyypkfxb")))

(define-public crate-simsimd-3.7.5 (c (n "simsimd") (v "3.7.5") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1qdvyz79gxcfdaa42clq2pn0j0i47rbj64sqp01idbkkzmwm8az3")))

(define-public crate-simsimd-3.7.7 (c (n "simsimd") (v "3.7.7") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vny70zifgjnpns01m1p4nbm4zxpl8fk0vg1z0gcxfc8mhz4j0i9")))

(define-public crate-simsimd-3.8.0 (c (n "simsimd") (v "3.8.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0syfg63v5mlm2cx8xf9kal3rkm0x8m3z8dl2qqlvqi99pyqpid6d")))

(define-public crate-simsimd-3.8.1 (c (n "simsimd") (v "3.8.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1d8prl8xkri98ir1qc05jswbgjlpdhrnjhqb02cnriz91pkjj365")))

(define-public crate-simsimd-3.9.0 (c (n "simsimd") (v "3.9.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kl8nqc090wka9dlk26jwmpj35q3n3nxm8w7c7iy3q4bfphvx5k1")))

(define-public crate-simsimd-4.0.0 (c (n "simsimd") (v "4.0.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jfvrdgawwr5nhf17619jgxl7s1lpympmqz5nc27rrhjwdsc3d03")))

(define-public crate-simsimd-4.1.0 (c (n "simsimd") (v "4.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nc6sdrm97ibc3d3j5zbrzjb6i2y9bkycmp44qjiscdqs441j3nw")))

(define-public crate-simsimd-4.1.1 (c (n "simsimd") (v "4.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b25zj7dhz750m697lf7i4y0alkxabidyps9hahy8knar6l0n3bs")))

(define-public crate-simsimd-4.2.0 (c (n "simsimd") (v "4.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vlnig0jmy6cp9jdk99q3rr34ffngpagpym9sgw9dvq8ljaf10vr")))

(define-public crate-simsimd-4.2.1 (c (n "simsimd") (v "4.2.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02rihj6hi1w4wriwx728fk7yyd54223kzsmsw2n6fbmbqvrggfzl")))

(define-public crate-simsimd-4.2.2 (c (n "simsimd") (v "4.2.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kpsmn95ny9wz6mf1cbm5y9xxg1896y8yd91rm3wkpd1500mpsix")))

(define-public crate-simsimd-4.3.0 (c (n "simsimd") (v "4.3.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ymzdvsrfwmib0hm8rs58qwrignq139zw613i293yw01yvw8pjv5")))

(define-public crate-simsimd-4.3.1 (c (n "simsimd") (v "4.3.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kkmljfcj564dp25dv8k7aax3n36qiz7vk23ifqjbvdyr8kp96yc")))

