(define-module (crates-io si ms sims-far) #:use-module (crates-io))

(define-public crate-sims-far-1.0.0 (c (n "sims-far") (v "1.0.0") (h "1fx497xmb8474fp37fgssrv94hi5a6hzw2as19qi2v6zcdp6plc8")))

(define-public crate-sims-far-1.1.0 (c (n "sims-far") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0l1vn9bfyq32m1hy7yz0889cx8hr46s2pr2i426wnzhha2p1qrgz")))

(define-public crate-sims-far-1.2.0 (c (n "sims-far") (v "1.2.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zjbkdsxwky258snh7c8xhfldwwdzpslv1cbmhnm95hwm9sfy983")))

(define-public crate-sims-far-1.3.0 (c (n "sims-far") (v "1.3.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0vfjzsvxzg4v6ngwlz2jzm1z24c85irwhgsavp7n4s099rxf38w3")))

