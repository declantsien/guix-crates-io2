(define-module (crates-io si ms sims) #:use-module (crates-io))

(define-public crate-sims-0.1.0 (c (n "sims") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0c6iqn9xyl9pkhh224290p90iv9ynbcxnxvbknyqm9lc572vw26r")))

(define-public crate-sims-0.1.1 (c (n "sims") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "08ry04i000v1w1hlf50cd83lyrg2m95bfrz6jwfy8yh4q3m2v3xi")))

