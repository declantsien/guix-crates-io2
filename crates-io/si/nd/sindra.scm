(define-module (crates-io si nd sindra) #:use-module (crates-io))

(define-public crate-sindra-0.1.0 (c (n "sindra") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "wee-peg") (r "^0.5") (d #t) (k 1)))) (h "1y219ln2wy5wp7iis6ih49jshm1dsglnrf4qv34lv59284rjapqx")))

