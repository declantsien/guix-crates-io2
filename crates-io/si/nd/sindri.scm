(define-module (crates-io si nd sindri) #:use-module (crates-io))

(define-public crate-sindri-0.0.1 (c (n "sindri") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("default" "blocking" "multipart" "json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.35") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0657v2q64l4hvnlw8d49pifgdbj053sfcrpvp95r5nd3lkjv2lr4")))

