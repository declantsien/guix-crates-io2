(define-module (crates-io si r- sir-macro) #:use-module (crates-io))

(define-public crate-sir-macro-0.1.0 (c (n "sir-macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsass") (r "^0.24.0") (d #t) (k 0)))) (h "08p4jq3whpn1h99v4m6wdwrchliqbbdzxz4z8i1l9vshn0bb50cl")))

(define-public crate-sir-macro-0.2.0 (c (n "sir-macro") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsass") (r "^0.24.0") (d #t) (k 0)))) (h "1pvs7yvdwkil98v8vrr0v1gwdhd74l7knjsiz8ym9wf0pxjkpwsg")))

(define-public crate-sir-macro-0.3.0 (c (n "sir-macro") (v "0.3.0") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 0)))) (h "17j24zn9b3n0fb4vshav9kl98xj3wwqwbvm9rzxhjafiz4i8hlmm")))

(define-public crate-sir-macro-0.5.0 (c (n "sir-macro") (v "0.5.0") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsass") (r "^0.28.8") (d #t) (k 0)))) (h "14zkmbv7713nrgzyamyfykbrwcac8x9f5r6bhny2mnxgk95lrzwb")))

