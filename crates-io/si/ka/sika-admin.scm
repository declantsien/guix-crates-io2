(define-module (crates-io si ka sika-admin) #:use-module (crates-io))

(define-public crate-sika-admin-0.0.0 (c (n "sika-admin") (v "0.0.0") (h "145jbzdhq44zc4nr1qxrx7b04r0dckgws3wchs10pcvf7d8dvbwv") (y #t)))

(define-public crate-sika-admin-0.0.1 (c (n "sika-admin") (v "0.0.1") (h "0yz12z148nxj9yld33kjs416y6c7bnpdks5kpfpsvg2p9a0w5hnh") (y #t)))

