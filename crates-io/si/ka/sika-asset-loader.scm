(define-module (crates-io si ka sika-asset-loader) #:use-module (crates-io))

(define-public crate-sika-asset-loader-0.1.0 (c (n "sika-asset-loader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "walker") (r "^1.0.1") (d #t) (k 0)))) (h "0z0d1filrdnsmsyqg6n4rjz2fqhri57prhcjvwr84qxshv34aqsf") (f (quote (("default" "actix-web") ("actix-web")))) (y #t)))

