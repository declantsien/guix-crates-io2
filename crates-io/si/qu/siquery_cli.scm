(define-module (crates-io si qu siquery_cli) #:use-module (crates-io))

(define-public crate-siquery_cli-1.2.0 (c (n "siquery_cli") (v "1.2.0") (d (list (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "siquery") (r "^1.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "13bk8nfvrhb7jrp10bkagqb793ihdr5b3hl4f8c5p72lzrwyw0a0")))

(define-public crate-siquery_cli-1.2.5 (c (n "siquery_cli") (v "1.2.5") (d (list (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "siquery") (r "^1.2.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0nxrm5kcffcrmyiwq9d3iajzk7p8y6smpfcpfwyac3hq7p1h0811")))

