(define-module (crates-io si ff siffra) #:use-module (crates-io))

(define-public crate-siffra-0.2.2 (c (n "siffra") (v "0.2.2") (d (list (d (n "astro-float") (r "^0.9.3") (f (quote ("std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "periodic-table-on-an-enum") (r "^0.3.2") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)))) (h "09x3nv1q4b0aw30j0ngp9c7gvs8ynpjfdvpd7vgikz1pw34a1p6z")))

