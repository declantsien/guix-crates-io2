(define-module (crates-io si gp sigproc_filterbank) #:use-module (crates-io))

(define-public crate-sigproc_filterbank-0.1.0 (c (n "sigproc_filterbank") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "1g6cayzv1pygyibfj6iy33rqhkmjclnn0asc32qlcmk5aa1il7aq") (r "1.57.0")))

(define-public crate-sigproc_filterbank-0.1.1 (c (n "sigproc_filterbank") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "0ql9zcpr2639hcjr8g4yxpdbj11m3lfwvvxnf40r0njn5ncr7fnb") (r "1.57.0")))

(define-public crate-sigproc_filterbank-0.2.0 (c (n "sigproc_filterbank") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "1z06gy2n873fiia9vcf9whh6a0avhhxlndknm9pgpzxgjl48wpfm") (r "1.57.0")))

(define-public crate-sigproc_filterbank-0.2.1 (c (n "sigproc_filterbank") (v "0.2.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "12z7q993q9nxx80i5p2vgilbvmm4sgdkidl5sqgslka5f0xkf4l9") (r "1.57.0")))

(define-public crate-sigproc_filterbank-0.3.0 (c (n "sigproc_filterbank") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "06fq0g63ah094qsv3g58cd8x7q17nkcnd7yci1pyhs01mzq435ak") (r "1.57.0")))

(define-public crate-sigproc_filterbank-0.4.0 (c (n "sigproc_filterbank") (v "0.4.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "1y38f70aa2kdxq1m925gfiilpsannlcjsvfpq5xqg4xd9g33z0cb") (r "1.57.0")))

