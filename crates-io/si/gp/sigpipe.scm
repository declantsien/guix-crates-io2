(define-module (crates-io si gp sigpipe) #:use-module (crates-io))

(define-public crate-sigpipe-0.1.1 (c (n "sigpipe") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lykrkgag291i5ypgv6z6jvp327qnk8dm7284zrp3flx83z9havs")))

(define-public crate-sigpipe-0.1.2 (c (n "sigpipe") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03jsj05x84m5zkw2p4656a8wlr2m391vi5y7yhrqrgsgwbz8b4sg")))

(define-public crate-sigpipe-0.1.3 (c (n "sigpipe") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rnqcgbx2mv3w11y6vf05a8a3y6jyqwmwa0hhafi6j6kw2rvz12m")))

