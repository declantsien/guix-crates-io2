(define-module (crates-io si mi similarity) #:use-module (crates-io))

(define-public crate-similarity-0.1.0 (c (n "similarity") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1mwnc67z7jmax41vdf7bqvd921km72ybsfkkfjc709g1kpyhlcb0")))

(define-public crate-similarity-0.1.1 (c (n "similarity") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "038s9ilv6kb9q1mm24vs8q0ns8w2dzqy6wnjvj33m1kczz6fp1pd")))

(define-public crate-similarity-0.1.2 (c (n "similarity") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "08r31dgksdi7g81zgg567m9g6rj863nb7q7qz55nrz0w8z4dr080")))

