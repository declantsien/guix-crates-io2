(define-module (crates-io si mi simi-test) #:use-module (crates-io))

(define-public crate-simi-test-0.1.0 (c (n "simi-test") (v "0.1.0") (d (list (d (n "simi") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Node" "Element"))) (d #t) (k 0)))) (h "0xlz8d28avzxav6cbgd3w2jgsmhzrvi67qiahks495vj2fgan11b")))

