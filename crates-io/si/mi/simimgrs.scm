(define-module (crates-io si mi simimgrs) #:use-module (crates-io))

(define-public crate-simimgrs-0.1.0 (c (n "simimgrs") (v "0.1.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "16v2cynax4pyppjagqppn5akpcdwzzn31khlkf2w7g8gkhq731fh")))

(define-public crate-simimgrs-0.1.1 (c (n "simimgrs") (v "0.1.1") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1i8khkmsqlp5zy1wikc0frx1fwa5na9b4i3f14nshljqg3splka3")))

(define-public crate-simimgrs-0.1.2 (c (n "simimgrs") (v "0.1.2") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0q0c09v0z0rh2s2q5q9qmb3k3bq266jyxd3r773smc1kfhq9r3d9")))

(define-public crate-simimgrs-0.1.3 (c (n "simimgrs") (v "0.1.3") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1sb69k8v3d57hkyla6rc6nvd1z34cq3njf04bwm07c2n0jcjblai")))

