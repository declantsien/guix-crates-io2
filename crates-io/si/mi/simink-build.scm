(define-module (crates-io si mi simink-build) #:use-module (crates-io))

(define-public crate-simink-build-0.1.0 (c (n "simink-build") (v "0.1.0") (h "1aap4jffb1q57abx4wayzsm47k75c83mvjzilp639sr9zxgd5sv0") (r "1.71")))

(define-public crate-simink-build-0.1.2 (c (n "simink-build") (v "0.1.2") (h "0xs419ki619pm38f20ls4lyh6kxw8614xz4g4a3dv8iylqp0nhyi") (r "1.71")))

(define-public crate-simink-build-0.1.3 (c (n "simink-build") (v "0.1.3") (h "0lfyp63mprvwnzzsa4mrscxwlvy5jd2p859i2v6qmrkch73sd0fi") (r "1.71")))

