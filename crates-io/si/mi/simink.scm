(define-module (crates-io si mi simink) #:use-module (crates-io))

(define-public crate-simink-0.1.0 (c (n "simink") (v "0.1.0") (h "1rad4qhx0kfnlj54y8x70jqnvx0w1x68qcs6vi1f6krfn31pdjpg") (y #t) (r "1.71")))

(define-public crate-simink-0.1.1 (c (n "simink") (v "0.1.1") (d (list (d (n "simink-macro") (r "^0.1") (d #t) (k 0)))) (h "04cpj2nxmcg572jxaikgqxsk4nbhfwrzhvcszddgnl4nk4nkfhmp") (y #t) (r "1.71")))

(define-public crate-simink-0.1.2 (c (n "simink") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)))) (h "0mc6613irmbiix010dxzp069rv9cibn647dglkcd6yj2m6d6dfnw") (y #t) (r "1.71")))

(define-public crate-simink-0.1.3 (c (n "simink") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)))) (h "0rh00bnsfcpsyrlwa18mam8xc66yrx17rlm9z9cb2f0ksj4a02m7") (y #t) (r "1.71")))

(define-public crate-simink-0.1.4 (c (n "simink") (v "0.1.4") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)))) (h "0qjpkqjgcgf69fdcw3hf5q9npq5jvjcsbkahb6ssdzqd6kcxacpa") (y #t) (r "1.71")))

(define-public crate-simink-0.1.5 (c (n "simink") (v "0.1.5") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "17rc3jg1n74prfn90jn9pxd3ac5d0pz3qi3iq910xhxicq2fc0zd") (f (quote (("print_pl011") ("default")))) (y #t) (r "1.71")))

(define-public crate-simink-0.1.7 (c (n "simink") (v "0.1.7") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "0c8dcvfqd26d5skyl7zhh9gc6lad9cdwx998k9m0vahrrljldz99") (f (quote (("print_pl011") ("default")))) (y #t) (r "1.71")))

(define-public crate-simink-0.1.8 (c (n "simink") (v "0.1.8") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "0qgsvp554rdzqdj0m1fpdk0f30f5xdrnam2j7zfplb475vbisy79") (f (quote (("print_pl011") ("default")))) (y #t) (r "1.71")))

(define-public crate-simink-0.1.9 (c (n "simink") (v "0.1.9") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "1fkiw7kzgnz42lz51zcxw13z9b004zhcm3a2xdiwfr3s6zlyrvs5") (f (quote (("print_pl011") ("default")))) (y #t) (r "1.71")))

(define-public crate-simink-0.1.10 (c (n "simink") (v "0.1.10") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "simink-macro") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "1iplgxgrz0njnrs2xq5spwab5gk603jadll569jqbg1wbpvpazfy") (f (quote (("print_pl011") ("default")))) (y #t) (r "1.71")))

(define-public crate-simink-0.1.11 (c (n "simink") (v "0.1.11") (d (list (d (n "barrier") (r "^0.1.0") (d #t) (k 0)))) (h "0r53ngyhvfqm5173mcmah3baj048m1pflmjsz8zy2ydspyg1ap4s") (r "1.73")))

(define-public crate-simink-0.1.12 (c (n "simink") (v "0.1.12") (d (list (d (n "srcu") (r "^0.1.0") (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00a9jxmp9wkpfrp5jl7cjb3zi6bjani3jbpd5grrqn84b8lpa9zh") (r "1.73")))

(define-public crate-simink-0.1.13 (c (n "simink") (v "0.1.13") (d (list (d (n "srcu") (r "^0.1") (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0f1r27989q15c9rkghrbyplfn139kvvx127nl6j09ic8nbrm4vp2") (r "1.73")))

