(define-module (crates-io si mi simink_entry) #:use-module (crates-io))

(define-public crate-simink_entry-0.1.0 (c (n "simink_entry") (v "0.1.0") (h "1zkd7zi27mzqmywhb4zlimfl8584ymjgkib0dkdamdzvgr6v34cv") (r "1.71")))

(define-public crate-simink_entry-0.1.1 (c (n "simink_entry") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1.91") (f (quote ("mem"))) (d #t) (k 0)) (d (n "simink_entry_marco") (r "^0.1") (d #t) (k 0)))) (h "1r603wf4a0qq7063gs9x3ms7b3z21vmcbzvh3npvmm7anlhkl2ac") (r "1.71")))

(define-public crate-simink_entry-0.1.2 (c (n "simink_entry") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1.91") (f (quote ("mem"))) (d #t) (k 0)) (d (n "simink_entry_marco") (r "^0.1") (d #t) (k 0)))) (h "1x9pv65yx6832dknvmvlnkl13jl6bdcgzpsy1csjbs39zkj044bj") (r "1.71")))

