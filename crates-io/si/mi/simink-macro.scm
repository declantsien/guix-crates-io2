(define-module (crates-io si mi simink-macro) #:use-module (crates-io))

(define-public crate-simink-macro-0.1.0 (c (n "simink-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bnv9yk6x699bspv3zznlb8m0xbg0m6343nfc21bhc8jrh7x2145") (r "1.71")))

(define-public crate-simink-macro-0.1.1 (c (n "simink-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "050xsrwv1cnm27cycj0qhrsc2sp4hgqi7g2cxh7qxxm0iap9hsp2") (r "1.71")))

(define-public crate-simink-macro-0.1.2 (c (n "simink-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1g52w9625nv3jvfrm9xn4v2vk3znxb9290qfp5bx1hxwdr3dqa9f") (r "1.71")))

(define-public crate-simink-macro-0.1.3 (c (n "simink-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fsnryhl4gr9c979lfvf19dif5qrxiqzgpyzj4nvgcm0mr7x8l58") (r "1.71")))

(define-public crate-simink-macro-0.1.4 (c (n "simink-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0w2fp4f899ia3ygyz2mhh7kkb0yn3zlazj9nnv9dh0vgzabnsql0") (r "1.71")))

(define-public crate-simink-macro-0.1.5 (c (n "simink-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1aiaxlax09j87d2l1d103g95v4ph09ihglqdj1xih1kbpmm51qap") (r "1.71")))

