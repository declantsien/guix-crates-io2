(define-module (crates-io si mi simino) #:use-module (crates-io))

(define-public crate-simino-1.0.0 (c (n "simino") (v "1.0.0") (d (list (d (n "any-object-storage") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "132dgk52vfkz4isj7dgfwwfsic0qwgzb3cy3067w2xvqz2x3amic")))

