(define-module (crates-io si mi simi-html-tags) #:use-module (crates-io))

(define-public crate-simi-html-tags-0.0.1 (c (n "simi-html-tags") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "00zcb8rz69kcm76n665k6m5bvf05izxmw3422njmwdv6jcbhklfj") (y #t)))

(define-public crate-simi-html-tags-0.1.0 (c (n "simi-html-tags") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1kdq7fdsdikfx3w08fiw4kcjs9kdd4h9szp8hi2q6hf39x5vc3gx") (y #t)))

