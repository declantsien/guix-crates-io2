(define-module (crates-io si mi similarity-metrics) #:use-module (crates-io))

(define-public crate-similarity-metrics-0.1.0 (c (n "similarity-metrics") (v "0.1.0") (d (list (d (n "polars") (r "^0.25.1") (f (quote ("lazy" "timezones" "dtype-datetime" "strings" "lazy_regex" "concat_str" "fmt"))) (d #t) (k 0)))) (h "1fil3xzwihpp2i60fgligkr7phv5kmnyilmaq155ckzri6772ppc")))

