(define-module (crates-io si mi similar-string) #:use-module (crates-io))

(define-public crate-similar-string-0.1.0 (c (n "similar-string") (v "0.1.0") (h "0rm7v2yfb1gbsppw6xm2c0j4ah4ra8xa5nfapijlysbr2pcqwc3a")))

(define-public crate-similar-string-0.1.1 (c (n "similar-string") (v "0.1.1") (h "0xvcb6gh58673wblrkaqki969kw1l99dy5phz5ls2cbn1as1ck5c")))

(define-public crate-similar-string-0.1.2 (c (n "similar-string") (v "0.1.2") (h "15xs59g150684hwcq4pyf6gkhk80yizz0rgd3qkp1i3mcmlaw1nw")))

(define-public crate-similar-string-1.3.0 (c (n "similar-string") (v "1.3.0") (h "0qg7ib9hs19lq4b6fx6blb1zxc8730dywfxx665h1r5p2xi7b8js")))

(define-public crate-similar-string-1.4.0 (c (n "similar-string") (v "1.4.0") (h "1ayi3rxqx66smagcbxdg34an9gw4sza2in4l8y38kdgzij822ja5")))

(define-public crate-similar-string-1.4.1 (c (n "similar-string") (v "1.4.1") (h "10ib7v4jgl2fa3cxxybfpy0b4c1143kism799saqrqcasn879n1m")))

(define-public crate-similar-string-1.4.2 (c (n "similar-string") (v "1.4.2") (h "0k2lxnb9i93qfwqv7jbaz1vb1ca86ficc1xnh1bz4ikvncdfh8sc")))

(define-public crate-similar-string-1.4.3 (c (n "similar-string") (v "1.4.3") (h "18vzij0c6jmvslsvnnhrmr8nyda3f5mp85m85kygkxr8bx2l5b6k")))

