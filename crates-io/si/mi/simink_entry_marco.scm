(define-module (crates-io si mi simink_entry_marco) #:use-module (crates-io))

(define-public crate-simink_entry_marco-0.1.0 (c (n "simink_entry_marco") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8d4idbcjn2bd7ihzvdawkjm68l3x0rccv89askf79lw3b80ilw") (r "1.71")))

(define-public crate-simink_entry_marco-0.1.1 (c (n "simink_entry_marco") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1is5nb0ycwy4wsvf06vd3qdvndqplp4hd3s23majhys50vnrmmf0") (r "1.71")))

