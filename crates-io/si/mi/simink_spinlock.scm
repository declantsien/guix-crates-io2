(define-module (crates-io si mi simink_spinlock) #:use-module (crates-io))

(define-public crate-simink_spinlock-0.1.0 (c (n "simink_spinlock") (v "0.1.0") (d (list (d (n "processor") (r "^0.1.0") (d #t) (k 0)))) (h "1gc0pzpmdr47hy6q19j0qdc4lrc7pa106zz5sqm6c22n5cnjq3yx") (r "1.71")))

(define-public crate-simink_spinlock-0.1.1 (c (n "simink_spinlock") (v "0.1.1") (d (list (d (n "processor") (r "^0.1") (d #t) (k 0)))) (h "0x8gjfjjr789jcjq1mdsvvjr552y9v5f1k38hr2ghdbgd8xypdxq") (r "1.71")))

