(define-module (crates-io si mi similar-structs-macros) #:use-module (crates-io))

(define-public crate-similar-structs-macros-0.1.0 (c (n "similar-structs-macros") (v "0.1.0") (h "0309j54n2chb2q2vvrxk1b5cilh0hfhhl9v6alk48lcv1p6alxyr")))

(define-public crate-similar-structs-macros-0.2.0 (c (n "similar-structs-macros") (v "0.2.0") (h "0cwbv2ysmb6yb22gnbix1pxy494p5ly2kphy3gjikfz5wdjxi57w")))

