(define-module (crates-io si mi simid) #:use-module (crates-io))

(define-public crate-simid-0.1.0 (c (n "simid") (v "0.1.0") (d (list (d (n "mac_address") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "16fy6nd543q2kgipkd85z512l9ja4gws4w5vnig4035rrybyvmya") (f (quote (("random" "rand") ("mac" "mac_address" "rand") ("hash_sha1" "sha1") ("hash_md5" "md5"))))))

