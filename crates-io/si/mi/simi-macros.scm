(define-module (crates-io si mi simi-macros) #:use-module (crates-io))

(define-public crate-simi-macros-0.0.1 (c (n "simi-macros") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.0.1") (d #t) (k 0)))) (h "16ry5gzrjh6jzl5p7sa393vrigvdrb9nx44l32rrl227q5zmz0ka")))

(define-public crate-simi-macros-0.1.0 (c (n "simi-macros") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.0.1") (d #t) (k 0)))) (h "0xi5a7vm7w2sffpqw6glq7anbl0gsgq6f1s3hnr2ri4y982pcp9y")))

(define-public crate-simi-macros-0.1.1 (c (n "simi-macros") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.1") (d #t) (k 0)))) (h "0d3sf1r0d65n0g281najr6wygrwnz2mb9s7jay6v2dj8sryfhppj")))

(define-public crate-simi-macros-0.1.2 (c (n "simi-macros") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.1") (d #t) (k 0)))) (h "1p57s54akfhqdmwkzkmr10kmm54b61qvcigbd0x414wxljfrivl1")))

(define-public crate-simi-macros-0.1.3 (c (n "simi-macros") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.1") (d #t) (k 0)))) (h "0x9rm6l7lrgy4pd4y4qn043qyzp2bcl1v0b0xd3m04qmapj8cq2p")))

(define-public crate-simi-macros-0.1.4 (c (n "simi-macros") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simi-html-tags") (r "^0.1") (d #t) (k 0)))) (h "1q56757jxsbi3vqb4gckvsn8ns2xvjqxdm8in1g7wpmcvyl552hz")))

(define-public crate-simi-macros-0.2.0 (c (n "simi-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "17f2gr7i38bq2y5wgwlcgjkw7yjsx7sb1606953vm0cl26xg28dn")))

(define-public crate-simi-macros-0.2.1 (c (n "simi-macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1i1yw803gjcs7sj8b7bb4ghm76ppkykivn0q4hry83cqgp3qyfb0") (f (quote (("nightly") ("default" "proc-macro-hack"))))))

