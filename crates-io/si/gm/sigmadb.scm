(define-module (crates-io si gm sigmadb) #:use-module (crates-io))

(define-public crate-sigmadb-0.1.0 (c (n "sigmadb") (v "0.1.0") (h "12hjqh6gml98q1x2k2bgfhsh9z4innpy7ib9h8nfps8aqhkrr57g")))

(define-public crate-sigmadb-0.0.0 (c (n "sigmadb") (v "0.0.0") (h "17as6l8ack6vih17pwcqbrlw0yyfk620c2b8kkjbg2chqf0f58k3")))

