(define-module (crates-io si gm sigma-util) #:use-module (crates-io))

(define-public crate-sigma-util-0.2.0 (c (n "sigma-util") (v "0.2.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)))) (h "10a70agmv5fnsbadsw17jjsjifh9isq4b7hglqln4fcrs2cwpd5m")))

(define-public crate-sigma-util-0.2.1 (c (n "sigma-util") (v "0.2.1") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0w5wjjkgqxq2agc4m67c210xafmx99cv2rahywhp681mpiqhsalr")))

(define-public crate-sigma-util-0.3.0 (c (n "sigma-util") (v "0.3.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0ddnhg7g92r612acipxq87lb0gpiavjwd5kgd1hak1n06sy6mlxz")))

(define-public crate-sigma-util-0.4.0 (c (n "sigma-util") (v "0.4.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1hrg4y6s9155aix3bmsjs4idj5mn80f949akxpa83kljrphcv5fz")))

(define-public crate-sigma-util-0.5.0 (c (n "sigma-util") (v "0.5.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0njk7s3w6xgl0c86hs3cyjnimp99bpm73j5wwkfpg665ci4ym99l")))

(define-public crate-sigma-util-0.6.0 (c (n "sigma-util") (v "0.6.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "151nziacdxz60ljy4acfrkf7z1dj0llmbpdyaiqglmkdmmhw06ss")))

(define-public crate-sigma-util-0.7.0 (c (n "sigma-util") (v "0.7.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1imh88qf5ca926l3ak6z5vadd8y2291frn67q9b2lqb7jndsq72c")))

(define-public crate-sigma-util-0.8.0 (c (n "sigma-util") (v "0.8.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1s7abw9likb3p80pwwrp28wid6zmba18mgnjvy5rhsrrv9s43gjz")))

(define-public crate-sigma-util-0.9.0 (c (n "sigma-util") (v "0.9.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "05xswsaazkrpn548fifdkgp88fsamj1sqq05fy0gihkmz96807hd")))

(define-public crate-sigma-util-0.9.1 (c (n "sigma-util") (v "0.9.1") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1d87kf0dcsmxrz3iv5dwdxk37x61x30fbz8gi4dw3zmp4sjd99hk")))

(define-public crate-sigma-util-0.10.0 (c (n "sigma-util") (v "0.10.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0l6yvl25csg00gh0vgp0dnrc51f3amb0h5ni1g5dhb1gpmg54h5v")))

(define-public crate-sigma-util-0.11.0 (c (n "sigma-util") (v "0.11.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "170rj68j94nqwcsmm361c92ddrjrax2iy5nr2xm5s0g29dwvyfgn")))

(define-public crate-sigma-util-0.12.0 (c (n "sigma-util") (v "0.12.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0sh9rv51kd61pq57iligaanavmkbq5ba6a4392hfgcraa00pysc0")))

(define-public crate-sigma-util-0.12.1 (c (n "sigma-util") (v "0.12.1") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1x64834f8x73is9qvkckh6q45q1n0wdn4ihi162zj7hkbgc34y95")))

(define-public crate-sigma-util-0.13.0 (c (n "sigma-util") (v "0.13.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0fdm47p0vqp2zzrhaf9p7ksxds9mmgcrlh4h6aivgjdljmr6rcra")))

(define-public crate-sigma-util-0.14.0 (c (n "sigma-util") (v "0.14.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "15yvp93kq9949gr01k9m4z7fspf04ql499xkfra7p0qqa89kw113")))

(define-public crate-sigma-util-0.15.0 (c (n "sigma-util") (v "0.15.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0w9y9x20bwplqmhmzqz833apziz6ddkpdhpha56q2sw47vgr84bi") (y #t)))

