(define-module (crates-io si gm sigma-rs) #:use-module (crates-io))

(define-public crate-sigma-rs-0.1.0 (c (n "sigma-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob-match") (r "=0.2.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0iksv48sc1bxaalkpdjislzj0bzgr5l5h8x7idqsh6597fdzkid4")))

