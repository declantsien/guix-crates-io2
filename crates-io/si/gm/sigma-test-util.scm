(define-module (crates-io si gm sigma-test-util) #:use-module (crates-io))

(define-public crate-sigma-test-util-0.2.0 (c (n "sigma-test-util") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0m22gh1yam4jwjf6vx0lkv5jg2yh4zpsrmvcmni02qnchv2mn2f8")))

(define-public crate-sigma-test-util-0.3.0 (c (n "sigma-test-util") (v "0.3.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "12wdnflz5hm0ylc54awwg56zr6l4ffxr6w1jmqwfrn47bnmsyxsf")))

