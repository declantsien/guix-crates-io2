(define-module (crates-io si gm sigma) #:use-module (crates-io))

(define-public crate-sigma-0.0.0 (c (n "sigma") (v "0.0.0") (h "0xpsqjjlhry86zpw9wi9kw70yi8zdvh8g67aviw8v9qhy8v4b59f")))

(define-public crate-sigma-0.1.0 (c (n "sigma") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.8.0") (d #t) (k 0)))) (h "16xxrx5a7lsr1bi4dxg9h9w5sl6scwa9hki801asw2940ysj8l08")))

(define-public crate-sigma-0.1.1 (c (n "sigma") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.8.0") (d #t) (k 0)))) (h "0vyfn68pfz0jlc4jr3wmnbvkmj7aw2dfgj6bx9j865p66ld5qgv4")))

