(define-module (crates-io si gm sigma-ser) #:use-module (crates-io))

(define-public crate-sigma-ser-0.1.0 (c (n "sigma-ser") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zrmvvhpqjdfa4ibipyr9mcy3p2mh7vp2p4d0yqkzy2fw7qic1ih")))

(define-public crate-sigma-ser-0.2.0 (c (n "sigma-ser") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02mpmcsl8yljs5cf7gd4g31xf0d1fnwmp25zdpbc1dm83r86sfkd")))

(define-public crate-sigma-ser-0.2.1 (c (n "sigma-ser") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ay7xpfwxhmyi660iy363g7g7nqa278spxih3qw0wjwdswa958vz")))

(define-public crate-sigma-ser-0.2.2 (c (n "sigma-ser") (v "0.2.2") (d (list (d (n "bitvec") (r "^0.20.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p1kn100fdaa2yir19b7vxaiyc2v9v065r8zxwsjhpmxl576xqyw")))

(define-public crate-sigma-ser-0.2.3 (c (n "sigma-ser") (v "0.2.3") (d (list (d (n "bitvec") (r "^0.20.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hnl3pd7cgjc42pc2zgfp3qb2kqbf7c4kp7ial4z50p4d2znjahn")))

(define-public crate-sigma-ser-0.3.0 (c (n "sigma-ser") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cnnx9gcz3rj9q5cf60kzn57h8z60iq9f8lw1w0wxrplimbxzw56")))

(define-public crate-sigma-ser-0.4.0 (c (n "sigma-ser") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w14prjm48nifkisicaq2afy4vq3qmqrryjvx40691qhji5khzkb")))

(define-public crate-sigma-ser-0.5.0 (c (n "sigma-ser") (v "0.5.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09x3nx313mb5x45sffi76m9ii3a8j93anwldbskpfmqrd5nn1sa0")))

(define-public crate-sigma-ser-0.6.0 (c (n "sigma-ser") (v "0.6.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xyai6rycxjk4307ckmgzp423q65mgndpz8xl6c6fyknk6sagh21")))

(define-public crate-sigma-ser-0.7.0 (c (n "sigma-ser") (v "0.7.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01qh0rxbcdisqskkiqyxrd2x4li63nxdbaklh965mlqcb74mcify")))

(define-public crate-sigma-ser-0.8.0 (c (n "sigma-ser") (v "0.8.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13hq3x5yv5y5vmhhwha7110mh8dsly509khkycmbz7qwcvih2yzf")))

(define-public crate-sigma-ser-0.9.0 (c (n "sigma-ser") (v "0.9.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n7yb9f5lz7y9n95dxvlk34nzgy7smpsqppfbp39fng1j2dyyn03")))

(define-public crate-sigma-ser-0.10.0 (c (n "sigma-ser") (v "0.10.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x21vn6ifzgc2zr6f0ndbzb2ivb16ivpmfdywqqmxaiif0v0hy4r")))

(define-public crate-sigma-ser-0.10.1 (c (n "sigma-ser") (v "0.10.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dh2spaq84az62302427zdjgf3mgpnh8z36xlc85mp6kaq79an07")))

(define-public crate-sigma-ser-0.11.0 (c (n "sigma-ser") (v "0.11.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h73sik474p4dyk1nwxkmsah47qjdl8gsqjf14bhmk7b4kz97fml")))

(define-public crate-sigma-ser-0.12.0 (c (n "sigma-ser") (v "0.12.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0synw827aqps10nklx32xlfya5ba0m3b5kcw3j71vqwci437k2r2")))

(define-public crate-sigma-ser-0.13.0 (c (n "sigma-ser") (v "0.13.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1akff3jlxzvdsxhf7nw81yqab992051zaa8nv6vwsp5rcx7v5623")))

(define-public crate-sigma-ser-0.13.1 (c (n "sigma-ser") (v "0.13.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vdzg21aj9iimmxxgrx1ffk7g7ij9vjxzzjsqzmqghxs8vcyfmgw")))

(define-public crate-sigma-ser-0.14.0 (c (n "sigma-ser") (v "0.14.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06pgrvdrpfnjh846krj3pjmlmgxn3y8v2vxpknaniqka4azjv2c2")))

(define-public crate-sigma-ser-0.15.0 (c (n "sigma-ser") (v "0.15.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lwsyvrmvbsc3h91p47qy5qjfa61i7k48m57r1fs0nzydspvwp8s")))

(define-public crate-sigma-ser-0.16.0 (c (n "sigma-ser") (v "0.16.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bounded-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h32s659khq49ldai5q2rh3n4d4p3szgjlnnnfffxdq5ky1qn1nq") (y #t)))

