(define-module (crates-io si la sila) #:use-module (crates-io))

(define-public crate-sila-0.1.0 (c (n "sila") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0snwy8axc6wmvnsq2c79wqw6rrj9skx993zydll1klvwyxrsnrsq")))

(define-public crate-sila-0.1.2 (c (n "sila") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0388dl5fk8whx45zl9ads826zcf4n8y66rl8gc6lng9mdxnh78q3")))

(define-public crate-sila-0.2.0 (c (n "sila") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "1is86fkqk8sx3230qnm4wifjabr072ckf9s4skkx1phxncb5ziqz")))

(define-public crate-sila-0.3.0 (c (n "sila") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "0r87djax6905klf9pb8ah8hzkkybn10yb2x0l4q4apcacyi8wgcm")))

(define-public crate-sila-0.3.1 (c (n "sila") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "1d5mawr52ivhna4w1rgwis887lchjryj5rkprrbijzxmfiaxd77b")))

(define-public crate-sila-0.3.2 (c (n "sila") (v "0.3.2") (d (list (d (n "enum-iterator") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "1ann3hf3r3qd2rbq7h4vg7drzs0dnss8snd822s9bs8inkx91yby")))

