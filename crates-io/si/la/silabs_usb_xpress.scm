(define-module (crates-io si la silabs_usb_xpress) #:use-module (crates-io))

(define-public crate-silabs_usb_xpress-0.1.0 (c (n "silabs_usb_xpress") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02jqib5xnbs9bqxdywiimdnmvskrzd29zim5qqvchybs21favvms")))

(define-public crate-silabs_usb_xpress-0.1.1 (c (n "silabs_usb_xpress") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v34srb8mrlrbq5rsjvawf6y334a6xj7450p5x843dvsg2c39gz2")))

(define-public crate-silabs_usb_xpress-0.2.0 (c (n "silabs_usb_xpress") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jhbi9111w070dfi1n8px9wfq2kn19slciddblmxgn4ri696hvsr")))

(define-public crate-silabs_usb_xpress-0.2.1 (c (n "silabs_usb_xpress") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "18qnrymyxi9z33zs8dz9jmkl70bgzcylsgw7yn82nb240hgzc3xb")))

(define-public crate-silabs_usb_xpress-0.3.0 (c (n "silabs_usb_xpress") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1wiv2j90v0fkdizc8bfpzd3x85aapn4ibl807v0zrqk38y85jvf7")))

(define-public crate-silabs_usb_xpress-0.3.1 (c (n "silabs_usb_xpress") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "07b06694db2kzk5574vzxkc7c4l848cgh0ii2p93sc9kvjn5lvil")))

