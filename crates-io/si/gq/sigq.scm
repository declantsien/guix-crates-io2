(define-module (crates-io si gq sigq) #:use-module (crates-io))

(define-public crate-sigq-0.9.0 (c (n "sigq") (v "0.9.0") (h "09804gf84hllr84smmi9b9l7s9l4spyqgm813ma511pq2banqgv3")))

(define-public crate-sigq-0.9.1 (c (n "sigq") (v "0.9.1") (h "1g0ckys33i2b03hnwqkqxiadq7n56lc1cb6jv1rljs3hrfkyjvjv")))

(define-public crate-sigq-0.10.0 (c (n "sigq") (v "0.10.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1hwzwgffrwnkn3cdn378sbb8kjc6gvzhjkg5wwglxhslzakk0m86") (y #t)))

(define-public crate-sigq-0.10.1 (c (n "sigq") (v "0.10.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0chxcqbi8r76gxxjkny82wcb5hwpgyj7szws5qg355aln6kw5jis")))

(define-public crate-sigq-0.10.2 (c (n "sigq") (v "0.10.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "19v2fjy8ikp6inzniz04ir6cdrwsyfn3iq2m4vp7f0syfadla7fz")))

(define-public crate-sigq-0.11.0 (c (n "sigq") (v "0.11.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "09vk3cxrfi68iyqcpc71d62p8hjysm8zn6r6dsnh9k9lvmprkjwn") (r "1.36")))

(define-public crate-sigq-0.12.0 (c (n "sigq") (v "0.12.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0h5gnjpbx4ckc0pxynawy04jgxyj8b561y6g0jm79djk2dd2k3cs") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

(define-public crate-sigq-0.13.0 (c (n "sigq") (v "0.13.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "108xjmdcrfxpippbphh0ym64g13m3r3c4abh5adb063v33g6divc") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

(define-public crate-sigq-0.13.1 (c (n "sigq") (v "0.13.1") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0i9rlmi2b4smpwjf8dpv8bxmgsnq3rdaa4vq0zwkp5b0qyx9143d") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

(define-public crate-sigq-0.13.2 (c (n "sigq") (v "0.13.2") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0n4kz7b95bx4w0hf8giidczgnr3iwbakihpb5wbfqva9pjh9r5ql") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

(define-public crate-sigq-0.13.3 (c (n "sigq") (v "0.13.3") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1p0sf63jlpn2f49jldzkihf6j04lqvpa99vy9r41x9sc20wqwaf0") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

(define-public crate-sigq-0.13.4 (c (n "sigq") (v "0.13.4") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0hq6m1j4gfc5y1lmb1aq8z9pq4q9zy4qynbk3ckzfp62lf7nqhlr") (f (quote (("inline-more") ("default" "inline-more")))) (r "1.64")))

