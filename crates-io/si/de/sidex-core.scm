(define-module (crates-io si de sidex-core) #:use-module (crates-io))

(define-public crate-sidex-core-0.1.0 (c (n "sidex-core") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1") (d #t) (k 0)) (d (n "chumsky") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "00idjb17p3acygjjhbc2rclxcfq32lxyzinvpw20kr8498ylxr0s")))

