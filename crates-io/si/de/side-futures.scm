(define-module (crates-io si de side-futures) #:use-module (crates-io))

(define-public crate-side-futures-0.1.0 (c (n "side-futures") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("rt-core" "rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0kjjc07pdn3wb842mdf2352s5yhn5ibafjp4yacb3nr4maix9pwq")))

(define-public crate-side-futures-0.1.1 (c (n "side-futures") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("rt-core" "rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0xr7d0r5i46z483bdpkbwilpax6x6qn54wgpidfia42vgd8j2scj")))

(define-public crate-side-futures-0.1.2 (c (n "side-futures") (v "0.1.2") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "09qf7719xkkvkaf32kabj80hdpr2l8ghspdd56z3jmy2ap0c7i75")))

