(define-module (crates-io si de sidex-cli) #:use-module (crates-io))

(define-public crate-sidex-cli-0.1.0 (c (n "sidex-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "sidex") (r "^0.1") (d #t) (k 0)))) (h "1lsxfq34v73p2na2kpdhkb7jb6kg9w3whx3x7x5b5gpjgbngrwgr")))

