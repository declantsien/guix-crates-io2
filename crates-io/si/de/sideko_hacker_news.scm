(define-module (crates-io si de sideko_hacker_news) #:use-module (crates-io))

(define-public crate-sideko_hacker_news-0.1.0 (c (n "sideko_hacker_news") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "03s8gv6ppj3ab4nw3m6a165vad03j4qkjg17byh3wgf2qkqkw667")))

(define-public crate-sideko_hacker_news-0.2.0 (c (n "sideko_hacker_news") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1r6x62nnv110gngxi1r493xrbrw4mvlqgwzhj57pgns5q5pdj8l7")))

