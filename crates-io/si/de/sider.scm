(define-module (crates-io si de sider) #:use-module (crates-io))

(define-public crate-sider-0.0.1 (c (n "sider") (v "0.0.1") (h "1wdv1kyrxzc63b1x5kndgc6sana34ybbbmq54p1yrl6d2ig228sr")))

(define-public crate-sider-0.1.0 (c (n "sider") (v "0.1.0") (h "08al8810f7ywlszvf8h6km09fk377028c9zy1hgpc341a999qfpf")))

(define-public crate-sider-0.1.1 (c (n "sider") (v "0.1.1") (h "0k049v5raksy38s5sbrz63y9q43h0h2wzrpps47q91gcaslb0vr5")))

(define-public crate-sider-0.1.2 (c (n "sider") (v "0.1.2") (h "1r2njxfydx8y7jk4k8d6xwi0nx2p6spb6zbsjfy9myxn8k111lwb")))

(define-public crate-sider-0.1.3 (c (n "sider") (v "0.1.3") (h "1ysr3grvqff1fxz5bkihpyyq7zdfqdl8i8qkx0hlh1w0ws0ng0jc")))

(define-public crate-sider-0.1.4 (c (n "sider") (v "0.1.4") (h "08jqng0siycxcl38n1g9nfsf16225wb2z863av416hv50lg4mb7c")))

