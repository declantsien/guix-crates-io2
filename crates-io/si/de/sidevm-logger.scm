(define-module (crates-io si de sidevm-logger) #:use-module (crates-io))

(define-public crate-sidevm-logger-0.1.1 (c (n "sidevm-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.16") (f (quote ("std"))) (d #t) (k 0)) (d (n "sidevm-env") (r "^0.1.0") (d #t) (k 0)))) (h "0cl52nvshjqki797b3nnwhl3cm303svggkdb7c4qb9vax7pxsdrw")))

(define-public crate-sidevm-logger-0.2.0-alpha.0 (c (n "sidevm-logger") (v "0.2.0-alpha.0") (d (list (d (n "log") (r "^0.4.16") (f (quote ("std"))) (d #t) (k 0)) (d (n "sidevm-env") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1xcgf5vjrx2g74w8wz3aa2m95av8spzhhav0gj5qsxv6dwh9mzrz")))

(define-public crate-sidevm-logger-0.2.0-alpha.1 (c (n "sidevm-logger") (v "0.2.0-alpha.1") (d (list (d (n "log") (r "^0.4.16") (f (quote ("std"))) (d #t) (k 0)) (d (n "sidevm-env") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1xrkswskf8n4203593r2nglbjw5194kjyh9m1sjr79kab2az07ld")))

(define-public crate-sidevm-logger-0.2.0-alpha.2 (c (n "sidevm-logger") (v "0.2.0-alpha.2") (d (list (d (n "log") (r "^0.4.16") (f (quote ("std"))) (d #t) (k 0)) (d (n "sidevm-env") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0qrc5sv5dh5mqipkaa24k245ac3wl3qxqcr20s96lfy4rvz1vsdz")))

(define-public crate-sidevm-logger-0.2.0-alpha.3 (c (n "sidevm-logger") (v "0.2.0-alpha.3") (d (list (d (n "log") (r "^0.4.16") (f (quote ("std"))) (d #t) (k 0)) (d (n "sidevm-env") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "0cvx9lw0143c3ncgz792v4c6rz3rbhrs2yfnpdn58yr32ad624gq")))

