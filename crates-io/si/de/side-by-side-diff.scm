(define-module (crates-io si de side-by-side-diff) #:use-module (crates-io))

(define-public crate-side-by-side-diff-0.1.0 (c (n "side-by-side-diff") (v "0.1.0") (d (list (d (n "similar") (r "^2.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1r1fyxmjhwbjyzi1hkdwrq34778g7628wyv5k4jms0sxg0xq0ybj")))

(define-public crate-side-by-side-diff-0.1.1 (c (n "side-by-side-diff") (v "0.1.1") (d (list (d (n "similar") (r "^2.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1qraafqvxbkivm0n0xq8xxyx9m7gpkzmsjmjzivrjky8wddd4bp9")))

(define-public crate-side-by-side-diff-0.1.2 (c (n "side-by-side-diff") (v "0.1.2") (d (list (d (n "similar") (r "^2.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0qwnl3b6glmld56yfw703aakq71qpw96pjwi7v8xrmglr073132h")))

