(define-module (crates-io si de sidefuzz) #:use-module (crates-io))

(define-public crate-sidefuzz-0.1.0 (c (n "sidefuzz") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1i0z72zgkn5xpnszczqjl16cwvlbpki7pa543ahglm25dpbd306i")))

(define-public crate-sidefuzz-0.1.1 (c (n "sidefuzz") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "007klcm19393fmipbkd058s0pcalqaiqk6bvkmx0rx54qrkmrr3z")))

(define-public crate-sidefuzz-0.1.2 (c (n "sidefuzz") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1y4snfcjfmdcx179g3nd15s4bzms2m77yyf6g802j4m64ava3sr9")))

