(define-module (crates-io si mr simrs) #:use-module (crates-io))

(define-public crate-simrs-0.1.0 (c (n "simrs") (v "0.1.0") (h "1dsa1mpsj1cvmmln1y53gfj97pyahv1j8yvgp5ds17ikbnx18j18")))

(define-public crate-simrs-0.1.1 (c (n "simrs") (v "0.1.1") (h "0x3z6b7cqbg7mpln6yp5jqcrrsgkwj116xj86d1b9hh1s3dg6y9c")))

(define-public crate-simrs-0.1.2 (c (n "simrs") (v "0.1.2") (h "1wl5alykxszwpzn2zbaz4jrgdhbjywjdaxavjy28y34fn1d2gjg5")))

(define-public crate-simrs-0.2.0 (c (n "simrs") (v "0.2.0") (h "1gc2wqgkh1jwkn8f0zjmna87zzshlbyqn2wqxv5pqjhycs8774z8")))

