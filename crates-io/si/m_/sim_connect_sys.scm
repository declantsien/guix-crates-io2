(define-module (crates-io si m_ sim_connect_sys) #:use-module (crates-io))

(define-public crate-sim_connect_sys-0.1.0 (c (n "sim_connect_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1ln1x45brnnnrbbgqhz8gjwncy44nhpi3rizbb34dwpf7f6phviw") (f (quote (("static_link"))))))

(define-public crate-sim_connect_sys-0.2.0 (c (n "sim_connect_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "059nhf4984k0vs7n46j9dbw3h86ffh4kykfiwv02l64bhycr1b69") (f (quote (("static_link")))) (l "SimConnect")))

