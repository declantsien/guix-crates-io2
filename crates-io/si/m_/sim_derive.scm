(define-module (crates-io si m_ sim_derive) #:use-module (crates-io))

(define-public crate-sim_derive-0.7.0 (c (n "sim_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fzhbpxfyrw26rb4xy46ivj6c265zzck20h6kyq0yljsf8mvbnjl")))

(define-public crate-sim_derive-0.8.0 (c (n "sim_derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fd15i1bpcip39b6cjqmj6w8j9dvczx9q8mkb0qksbvnfvnzhqbf")))

(define-public crate-sim_derive-0.9.0 (c (n "sim_derive") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r35p0nf40qb0qh7111rkgkai2jn8fqgpsxwf1iygj6506vywv4s")))

(define-public crate-sim_derive-0.9.1 (c (n "sim_derive") (v "0.9.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wfxnr33vsb18aw6qhk6lf0ngbpq42mcgmli5jwhfn2cvshgnbwx")))

(define-public crate-sim_derive-0.9.2 (c (n "sim_derive") (v "0.9.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ci9s9farrihynykrlfl0v0jjgbxi79cr71is1v0xfn6am2vv9sk")))

(define-public crate-sim_derive-0.10.0 (c (n "sim_derive") (v "0.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pj4vwpyv6wpkmyfr70kvrqbz0r1ggryw7iqibc132m4c7g0rkr7")))

(define-public crate-sim_derive-0.11.0 (c (n "sim_derive") (v "0.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1imjaxl3v2c9a22mwi784qndnsvlf4iq0pf87pawqvr61n19r80n")))

(define-public crate-sim_derive-0.12.0 (c (n "sim_derive") (v "0.12.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13s0l3mwc96g88fmrmi6kwn1bdfjpn89bdpblbw17y8qdj9mpyjk")))

(define-public crate-sim_derive-0.13.0 (c (n "sim_derive") (v "0.13.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bnmibqh753mgsbz0ap5xxcwraaiab577sjh3h9n988dfsh5jl2q")))

