(define-module (crates-io si m_ sim_connect_macros) #:use-module (crates-io))

(define-public crate-sim_connect_macros-0.1.0 (c (n "sim_connect_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "0x216igz4lp3dxdm82zl5dsk2z95zsvqpwkc218gfqga0krgykkh")))

(define-public crate-sim_connect_macros-0.1.2 (c (n "sim_connect_macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvyaxdmzd604fnhqyjpxdqz4zwqn104pacdgbbmwwqm86lqdwwx")))

(define-public crate-sim_connect_macros-0.1.3 (c (n "sim_connect_macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "02nwvzvp7x3bndys0bxwxh9l3d571vzj1il6wdw4i2nfqz20s16i")))

(define-public crate-sim_connect_macros-0.1.4 (c (n "sim_connect_macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "06b45rj7i7whx54919l3lzmrqvckq1pdj9l31q9k411xsfkzvlp4")))

