(define-module (crates-io si m_ sim_connect_data) #:use-module (crates-io))

(define-public crate-sim_connect_data-0.1.0 (c (n "sim_connect_data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1zq6vm2ph52q1drdlwamq01cqxb24ia7nchrkgdan4dl6lpaz170")))

(define-public crate-sim_connect_data-0.1.1 (c (n "sim_connect_data") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.2.0") (d #t) (k 0)))) (h "0kqjixkw75x812idr7i92q6jp1pb4jbxd46n5r71v478ivwnqrlx")))

(define-public crate-sim_connect_data-0.1.3 (c (n "sim_connect_data") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.2.0") (d #t) (k 0)))) (h "0gp6qhch117hq9qfdhn4ilyv7c2jxwmhl9ljk1dp1q0mdnmr6x3n")))

(define-public crate-sim_connect_data-0.2.0 (c (n "sim_connect_data") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.2.0") (d #t) (k 0)))) (h "1c5l0agrswimbgai75nfwwcy0f6f264n4x7dh795w4sikvcm0kjn")))

(define-public crate-sim_connect_data-0.2.1 (c (n "sim_connect_data") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.2.0") (d #t) (k 0)))) (h "1pd00rqxzi0wdxqfmkr57k109swk0xbyqcyx4g48ispwnjgs0alw")))

(define-public crate-sim_connect_data-0.2.2 (c (n "sim_connect_data") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sim_connect_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "sim_connect_sys") (r "^0.2.0") (d #t) (k 0)))) (h "1vs337m2p8yrlnid7x8ag40h6z8ccqgl5r6n9hmb6481lq3ynqsc")))

