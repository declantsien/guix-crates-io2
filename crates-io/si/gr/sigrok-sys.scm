(define-module (crates-io si gr sigrok-sys) #:use-module (crates-io))

(define-public crate-sigrok-sys-0.1.0 (c (n "sigrok-sys") (v "0.1.0") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1fqm09530bha3fsbpqf9xy0q7jrx0ygb3phx0nh00yd6yk9zph48")))

(define-public crate-sigrok-sys-0.1.1 (c (n "sigrok-sys") (v "0.1.1") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1gsdf2fpa75ydsdcgs9kpgzacz2izn56brqsc79vaz6i5amr5rpd")))

(define-public crate-sigrok-sys-0.2.0 (c (n "sigrok-sys") (v "0.2.0") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0snhiz7ybw5p7yy15zvdskvjycjd0mgcxys59fmnl37yiqxddgzg")))

