(define-module (crates-io si gr sigrok) #:use-module (crates-io))

(define-public crate-sigrok-0.1.0 (c (n "sigrok") (v "0.1.0") (d (list (d (n "sigrok-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1gij3aha7f3rr5d7cwmmwq4dir66r41ym4kqi90phr70mk45bx5g")))

(define-public crate-sigrok-0.2.0 (c (n "sigrok") (v "0.2.0") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "sigrok-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1s1almxbplggjh55xbkv727xr24q26y05h1fdr1nhgl3c559hvdy")))

(define-public crate-sigrok-0.3.0 (c (n "sigrok") (v "0.3.0") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "sigrok-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0vdg130jcjgvbpbjmaby51y2g39hcj76y5ijpjzf0vn6rmx328xq")))

(define-public crate-sigrok-0.3.1 (c (n "sigrok") (v "0.3.1") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "sigrok-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lwxsv2mjr4mph49xy5574xdq1q6fbdi4zlww11r36jzl8vcjp7z")))

