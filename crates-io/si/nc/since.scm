(define-module (crates-io si nc since) #:use-module (crates-io))

(define-public crate-since-0.11.0 (c (n "since") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "062ch7yk2qqbj6i595b2lspzqqq8llqnmxsyi4avzcjp1323gypx")))

(define-public crate-since-0.11.1 (c (n "since") (v "0.11.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0v5ahrrkcvrb13bnl6xdmvyj09cx6kwgj891d9d6zikkn00krdk2")))

