(define-module (crates-io si er siera-logger) #:use-module (crates-io))

(define-public crate-siera-logger-0.1.0 (c (n "siera-logger") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0lki1wn66k9nvlhz8irdm50kvfgv40w71idjd4ib8xyijdhdwapj")))

(define-public crate-siera-logger-0.2.0 (c (n "siera-logger") (v "0.2.0") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1881v1mgvbxzvdv2xmhws3a5rr0m94n2djv388lqyzrvgrmgbx2d")))

