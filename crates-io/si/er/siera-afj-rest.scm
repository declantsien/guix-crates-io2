(define-module (crates-io si er siera-afj-rest) #:use-module (crates-io))

(define-public crate-siera-afj-rest-0.1.0 (c (n "siera-afj-rest") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "siera-agent") (r "0.*") (d #t) (k 0)) (d (n "siera-logger") (r "0.*") (d #t) (k 0)))) (h "0gzja0hl1vn84q0gpvd7lhj539179ccyccr4kzlj439my5i2pql5")))

(define-public crate-siera-afj-rest-0.2.0 (c (n "siera-afj-rest") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "siera-agent") (r "0.*") (d #t) (k 0)) (d (n "siera-logger") (r "0.*") (d #t) (k 0)))) (h "147036y8r84dpdmgwin3r469v3wrlidl3lnql82krv8lkkv982ah")))

