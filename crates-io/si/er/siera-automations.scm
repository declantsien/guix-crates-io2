(define-module (crates-io si er siera-automations) #:use-module (crates-io))

(define-public crate-siera-automations-0.1.0 (c (n "siera-automations") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "siera-agent") (r "0.*") (d #t) (k 0)) (d (n "siera-cloudagent-python") (r "0.*") (d #t) (k 0)) (d (n "siera-logger") (r "0.*") (d #t) (k 0)))) (h "0gmva94c9kkmkxavdqzy6das7dnlwn17mzgx48sqn1l6f4pyxc94")))

(define-public crate-siera-automations-0.2.0 (c (n "siera-automations") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "siera-agent") (r "0.*") (d #t) (k 0)) (d (n "siera-cloudagent-python") (r "0.*") (d #t) (k 0)) (d (n "siera-logger") (r "0.*") (d #t) (k 0)))) (h "1xkk29hzrb73y0kz0salfmnqpm2qxmh8jsas2zir5ki58bi3csjv")))

