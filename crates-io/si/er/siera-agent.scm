(define-module (crates-io si er siera-agent) #:use-module (crates-io))

(define-public crate-siera-agent-0.1.0 (c (n "siera-agent") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1gvjwxd5zgmcj38mx5rhjh718i76ij9c3z7niki8vsa8dhjalxq9")))

(define-public crate-siera-agent-0.2.0 (c (n "siera-agent") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1dxs54fzsras8c0r8h8l4ryymkw050s9m6hh25p2v9fd9j24azxc")))

