(define-module (crates-io si er sierp) #:use-module (crates-io))

(define-public crate-sierp-1.0.0 (c (n "sierp") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1j0dsdfm2q6czwdh2xvd4m7xb3ladjk34g9q4504qh3mhs96hjvh")))

(define-public crate-sierp-1.0.1 (c (n "sierp") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "02al0qnjy7hfv4g52vxkp1xzm5m9a0jdpkwp4z7060iyibk5qkvx")))

