(define-module (crates-io si er sierra-proc-demo) #:use-module (crates-io))

(define-public crate-sierra-proc-demo-0.1.0 (c (n "sierra-proc-demo") (v "0.1.0") (d (list (d (n "sierra") (r "^0.1.0") (d #t) (k 0)) (d (n "sierra-proc") (r "^0.1.0") (f (quote ("verbose-docs"))) (d #t) (k 0)))) (h "1slf9i0kwbk41hd0a93psn416grszvcl94rvkrjhv5q17wybqldz")))

(define-public crate-sierra-proc-demo-0.2.0 (c (n "sierra-proc-demo") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.6") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "sierra") (r "^0.2.0") (f (quote ("proc-verbose-docs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-error") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "0fbia7cq6g70vq7yq82bj70gifqp9gkzhg60zpch4rgjj806vzmx")))

