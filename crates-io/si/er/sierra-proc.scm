(define-module (crates-io si er sierra-proc) #:use-module (crates-io))

(define-public crate-sierra-proc-0.1.0 (c (n "sierra-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9vn57n292pp39mdgfp1b6ihq3flzj2b580812n2aqw08plpmxh") (f (quote (("verbose-docs"))))))

(define-public crate-sierra-proc-0.2.0 (c (n "sierra-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "139g6zqwr4hxww6r76kzqich91dxzq435682j05nk30v8zly5lg0") (f (quote (("verbose-docs"))))))

(define-public crate-sierra-proc-0.3.0 (c (n "sierra-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vrwpkkjzvs2yw11sjmyca97bzj0yv5b8zjalj9d6biaz4qmlfcs") (f (quote (("verbose-docs"))))))

(define-public crate-sierra-proc-0.4.0 (c (n "sierra-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xx9hz6bb3a0b28wzxxz47mbhr3p0g4f34kjv6mg33mngisshfy5") (f (quote (("verbose-docs"))))))

(define-public crate-sierra-proc-0.6.0 (c (n "sierra-proc") (v "0.6.0") (d (list (d (n "proc-easy") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cijg5m9083bwwnf9b74b5ishrwx80c89dip1jvby01zwvzxijrg") (f (quote (("verbose-docs"))))))

