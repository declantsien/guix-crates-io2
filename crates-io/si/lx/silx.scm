(define-module (crates-io si lx silx) #:use-module (crates-io))

(define-public crate-silx-0.0.0 (c (n "silx") (v "0.0.0") (h "0z41hzxpk9bh5a35j5dyal7j7404rqclji2xbipfwwcyaqm8ybwa") (y #t)))

(define-public crate-silx-0.0.1 (c (n "silx") (v "0.0.1") (h "09mbj213lvz493iradrwni22y4p84n0fnvpjl4msz3zlnn1bka1l")))

