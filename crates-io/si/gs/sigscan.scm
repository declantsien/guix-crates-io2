(define-module (crates-io si gs sigscan) #:use-module (crates-io))

(define-public crate-sigscan-0.1.0 (c (n "sigscan") (v "0.1.0") (h "0cpd1s2ckvza3kkc61w7nkilr6lji0a2bpqlq6i9yc2n1bx2v7g3")))

(define-public crate-sigscan-0.1.1 (c (n "sigscan") (v "0.1.1") (h "03ch8kmnpr70a07nqn7qvvwr7snmj1ryx9hxmg5l31b9gn81jmkj")))

