(define-module (crates-io si gs sigs-slots) #:use-module (crates-io))

(define-public crate-sigs-slots-0.1.0 (c (n "sigs-slots") (v "0.1.0") (h "1adgjnahvp30w8r8bbdmff4h32v2ahalwlv06nqhgrldgwy3v1dg")))

(define-public crate-sigs-slots-0.1.1 (c (n "sigs-slots") (v "0.1.1") (h "01jcnprfi3s3lwgs6wpwgi0dakdd0ak9qa1gx781zllmgrdh57dv")))

(define-public crate-sigs-slots-0.2.0 (c (n "sigs-slots") (v "0.2.0") (h "1x6jppq4nfs473aqcmk7gp67s54miplw0y6pkbnng65s3giqpfli") (f (quote (("threadsafe") ("default"))))))

