(define-module (crates-io si mc simctl) #:use-module (crates-io))

(define-public crate-simctl-0.1.0 (c (n "simctl") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "0hq4ac4237yx0gv2vdmn1b8axaz8xz97y0xwn0yf2g5jp1bz003n")))

(define-public crate-simctl-0.1.1 (c (n "simctl") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "1jq2avvrjz3rhx5c6idq9vcx3c6kaq8lf4s696bz19ab9vxa4fsp")))

