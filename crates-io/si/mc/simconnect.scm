(define-module (crates-io si mc simconnect) #:use-module (crates-io))

(define-public crate-simconnect-0.1.0 (c (n "simconnect") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "0znljsk2gxnsipw8z52iddzp0z14mhmglfpw760bzn3sd126izni")))

(define-public crate-simconnect-0.1.1 (c (n "simconnect") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "029r327f2xczk0s2amzv6p448jlwa39ria3dyc8knzhf6dd67llq")))

(define-public crate-simconnect-0.1.2 (c (n "simconnect") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "0r7b1yrlj7smwlasdpkz6ab62bdn8z6bmhdd73axjrm5b3irrq5f")))

(define-public crate-simconnect-0.1.3 (c (n "simconnect") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "0pgzm3jjh38lnc7rc4xb14sw20nvazfky501qczh4lgsm5hlb379")))

(define-public crate-simconnect-0.1.4 (c (n "simconnect") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "thread_local") (r "~1.0.1") (d #t) (k 1)))) (h "1djwaar1x331z6kghbnrccyinbgghillsxdbdriii8qg5m6r9xx6")))

(define-public crate-simconnect-0.2.0 (c (n "simconnect") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "1fk7jmz1ic3v2wapr9mc1my4vacqwiw8pvg0szkbhxg7q4wx1bmj")))

(define-public crate-simconnect-0.3.0 (c (n "simconnect") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "1ld08mbsyxismzixxl210764ag2gsdlm6syf1b65jr16jq66c0ra")))

