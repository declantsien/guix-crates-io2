(define-module (crates-io si mc simconnect-sdk-derive) #:use-module (crates-io))

(define-public crate-simconnect-sdk-derive-0.1.0 (c (n "simconnect-sdk-derive") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0l8iwx5lijcnd9rp6vd9fzz5zrjmbhx6rjp762g6jcyiq4dmag6v")))

(define-public crate-simconnect-sdk-derive-0.1.1 (c (n "simconnect-sdk-derive") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1si7mqvz5j375zc01ij2aw0fypsjnh72l9rg78y45frm0i0idgyf")))

(define-public crate-simconnect-sdk-derive-0.1.2 (c (n "simconnect-sdk-derive") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "120ga78qb5vb0bh2ymjbjllmfa8q0vfq7l6fi6b25b5fv767sq3a")))

(define-public crate-simconnect-sdk-derive-0.1.3 (c (n "simconnect-sdk-derive") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hywvkjiqs4fagyg0p8852hkw0yr88zixxxdmn91samf5xpb79x3")))

(define-public crate-simconnect-sdk-derive-0.2.0 (c (n "simconnect-sdk-derive") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "05ajl3q3b3kmsvby4vzjb8n1ynbnkyf0431a7rsg2xrj8i3345zk")))

(define-public crate-simconnect-sdk-derive-0.2.1 (c (n "simconnect-sdk-derive") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "16gvr1z41rq5lcz3n23dk0w7fzlzjlcgbk5viq3y9bk9faz4rlli")))

(define-public crate-simconnect-sdk-derive-0.2.2 (c (n "simconnect-sdk-derive") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0p6chis6ksbagssyd873h51v92kvidfbycq8wrhg37ng4blk217s")))

