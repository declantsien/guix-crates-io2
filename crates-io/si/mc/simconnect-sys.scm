(define-module (crates-io si mc simconnect-sys) #:use-module (crates-io))

(define-public crate-simconnect-sys-0.1.0 (c (n "simconnect-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "0fr4g0d1xvl0jlw9wvv3ixb5v3w82xpapsgrqzv868w7igif6x4r") (l "SimConnect")))

(define-public crate-simconnect-sys-0.22.3 (c (n "simconnect-sys") (v "0.22.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1n8njg9wg5h2z83mlcp2k6w4f1p2rr646666h1had121blip631g") (f (quote (("vendored") ("static")))) (l "SimConnect")))

(define-public crate-simconnect-sys-0.23.1 (c (n "simconnect-sys") (v "0.23.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "0nia8xdwsip4qnkp51ina9c4fs5xik7p336331mfy8yvshgpi4ps") (f (quote (("vendored") ("static")))) (l "SimConnect")))

(define-public crate-simconnect-sys-0.23.1-latest.0 (c (n "simconnect-sys") (v "0.23.1-latest.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "0w2mimsr8h00i1yvi7m4zmd90myz33bha1774m1ha9dy6qhls0vz") (f (quote (("vendored") ("static")))) (l "SimConnect")))

(define-public crate-simconnect-sys-0.23.1-latest.1 (c (n "simconnect-sys") (v "0.23.1-latest.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1nzrpmdmcfl769cg48n3cqkwxvfim9z819xmxidm5mln368jy5dw") (f (quote (("vendored") ("static")))) (l "SimConnect")))

(define-public crate-simconnect-sys-0.23.1-latest.2 (c (n "simconnect-sys") (v "0.23.1-latest.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1spr7c57015gmgk9rza4wz58j8sjbmy8zk0665q0z67jbgb9n8cp") (f (quote (("vendored") ("static")))) (l "SimConnect")))

(define-public crate-simconnect-sys-0.23.2-pre.1 (c (n "simconnect-sys") (v "0.23.2-pre.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "10v5kfkkpfv3wb31hax86lvz1gjd0avxzqr5cksq7dcv4f1y834l") (f (quote (("vendored") ("static")))) (l "SimConnect")))

