(define-module (crates-io si lk silkworm_sourcemap) #:use-module (crates-io))

(define-public crate-silkworm_sourcemap-0.1.0-dev.0 (c (n "silkworm_sourcemap") (v "0.1.0-dev.0") (h "0zmbcc535mkrp0w1bhyfy069nsn8r200vy89ppcylva8h2p90krr")))

(define-public crate-silkworm_sourcemap-0.1.0-dev.1 (c (n "silkworm_sourcemap") (v "0.1.0-dev.1") (h "0l719qhb4gxkyg727f3x21kfa7mbmg034miv0yhmffz9jm7phr21")))

