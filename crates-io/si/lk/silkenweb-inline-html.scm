(define-module (crates-io si lk silkenweb-inline-html) #:use-module (crates-io))

(define-public crate-silkenweb-inline-html-0.8.0 (c (n "silkenweb-inline-html") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "silkenweb") (r "^0.8.0") (d #t) (k 0)) (d (n "silkenweb") (r "^0.8.0") (d #t) (k 2)) (d (n "silkenweb-parse") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "0jblk354kfbgjlr9n2b6vbhc905nl1wsxyaf5q7gli8d80s96xh9")))

