(define-module (crates-io si lk silkrust) #:use-module (crates-io))

(define-public crate-silkrust-0.0.1 (c (n "silkrust") (v "0.0.1") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "blowfish") (r "^0.8.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13kaz00bj6m7s4wcqlk1r01qsawp3n41lmdd86dnwr5c01n75xic")))

(define-public crate-silkrust-0.0.2 (c (n "silkrust") (v "0.0.2") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "blowfish") (r "^0.8.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12q7izsnxycbn708vh9vfw1zch3slg1fg13rxy227awcpf3igd18")))

(define-public crate-silkrust-0.0.3 (c (n "silkrust") (v "0.0.3") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "blowfish") (r "^0.8.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9a6a68ynn07b2rkh6js6fw085j1myqgd3xd5vif18fc7jv8hsn")))

