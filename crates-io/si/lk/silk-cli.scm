(define-module (crates-io si lk silk-cli) #:use-module (crates-io))

(define-public crate-silk-cli-0.2.0 (c (n "silk-cli") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "silk-rs") (r "^0.2.0") (d #t) (k 0)))) (h "1c864sfvv2inwmx9dqi6vnyz7qjsr48praz38pcxjcvwxa7vjx06")))

(define-public crate-silk-cli-0.2.1 (c (n "silk-cli") (v "0.2.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "silk-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0bdc13v2hw3q6v0k8nisckzr3pf7fr4ifddjwynpd4x58slbi9q0")))

(define-public crate-silk-cli-0.2.2 (c (n "silk-cli") (v "0.2.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "silk-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0scwibjf6dmaxl3dwf25ma7mm3mc95dwr98nc89ni5j2bc3pi32s")))

(define-public crate-silk-cli-0.2.3 (c (n "silk-cli") (v "0.2.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "silk-rs") (r "^0.2.0") (d #t) (k 0)))) (h "1d79r2cmvfk0gkliz00flwgz08qa2jayfr4rf9rs5i2vh8d1n25j")))

(define-public crate-silk-cli-0.2.4 (c (n "silk-cli") (v "0.2.4") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "silk-rs") (r "^0.2.0") (d #t) (k 0)))) (h "063gbcyjjx27391x5i4sxacw930c0gjam35hmk3v8qcxdd01crwp")))

