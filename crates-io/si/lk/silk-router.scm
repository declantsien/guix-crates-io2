(define-module (crates-io si lk silk-router) #:use-module (crates-io))

(define-public crate-silk-router-0.1.0 (c (n "silk-router") (v "0.1.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "19w5mfi4jvdk1xzxwdvksyh8hmhb9d7aqj0594dm0cskwyf8v8rb")))

(define-public crate-silk-router-0.1.1 (c (n "silk-router") (v "0.1.1") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "1s88ymvl6zprlk1vmirrrcvi5zij5akixzam7z2qfkwnnqz62rzs")))

