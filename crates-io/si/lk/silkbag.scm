(define-module (crates-io si lk silkbag) #:use-module (crates-io))

(define-public crate-silkbag-0.0.1 (c (n "silkbag") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "cairo-rs") (r "*") (d #t) (k 0)) (d (n "imagefmt") (r "*") (d #t) (k 0)) (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 0)) (d (n "tar") (r "*") (d #t) (k 0)) (d (n "threadpool") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1n3vlm7b8yc9f6rw5jj77403szjjb5b18y5lh1j7y7ql8l80w8fv")))

