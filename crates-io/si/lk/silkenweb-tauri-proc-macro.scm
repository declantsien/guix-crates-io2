(define-module (crates-io si lk silkenweb-tauri-proc-macro) #:use-module (crates-io))

(define-public crate-silkenweb-tauri-proc-macro-0.3.0 (c (n "silkenweb-tauri-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1857266ngm4r23kj63g434vdv1r46phsc2vlpz2f8zm8hlmfq5ws")))

(define-public crate-silkenweb-tauri-proc-macro-0.4.0 (c (n "silkenweb-tauri-proc-macro") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1wdy9m0hsqd2510wffk45h5w26nad1nflg58ggnkyj51fwxz5qg5")))

(define-public crate-silkenweb-tauri-proc-macro-0.5.0 (c (n "silkenweb-tauri-proc-macro") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1w693jji3b05f6shhm399palqmkczv6r2lam49bbz4yppwbx8nkc")))

(define-public crate-silkenweb-tauri-proc-macro-0.6.0 (c (n "silkenweb-tauri-proc-macro") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9cc6lniad9j4ygnzcrb60dy4xbsccbz4mj3fpz61glssfc0nhf")))

(define-public crate-silkenweb-tauri-proc-macro-0.7.0 (c (n "silkenweb-tauri-proc-macro") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0k71vbjl7zxh1qllrn40064sk2ii1rn7frb98ni6p52z7cmyh1y5")))

(define-public crate-silkenweb-tauri-proc-macro-0.7.1 (c (n "silkenweb-tauri-proc-macro") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "16n8zgbdw41525jjwfhkg8ylfq8sl35ingkgpdsa7kmvhdkd3rlj")))

(define-public crate-silkenweb-tauri-proc-macro-0.8.0 (c (n "silkenweb-tauri-proc-macro") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "16dv936fcfrjask0gdaxjrijsv39yhnl5yxhb5lc4gp7k2zj836q")))

