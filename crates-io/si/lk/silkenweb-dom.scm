(define-module (crates-io si lk silkenweb-dom) #:use-module (crates-io))

(define-public crate-silkenweb-dom-0.1.0 (c (n "silkenweb-dom") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "silkenweb-reactive") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Text" "Element" "HtmlInputElement"))) (d #t) (k 0)))) (h "10lw2psl2c3db9dn7s3v078d7xr8c5vdb5gc42bc5xj3bz5sq4gr")))

(define-public crate-silkenweb-dom-0.1.1 (c (n "silkenweb-dom") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "silkenweb-reactive") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Text" "Element" "HtmlInputElement"))) (d #t) (k 0)))) (h "1h853saw461nk3r94ncgdc0fk0hikby5nkd6z9ir3bbb7djhfc45")))

(define-public crate-silkenweb-dom-0.1.2 (c (n "silkenweb-dom") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "silkenweb-reactive") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Text" "Element" "HtmlInputElement"))) (d #t) (k 0)))) (h "0w123mvwnvr6pmj22qhy6iqdp9f2bwz4syk98rhzniavddikda8l")))

(define-public crate-silkenweb-dom-0.1.3 (c (n "silkenweb-dom") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "silkenweb-reactive") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Text" "Element" "HtmlInputElement"))) (d #t) (k 0)))) (h "0cvhib83yh2zbnqlxpzcawzgh4h2crcj4g50n93azi8mdr16ikgq")))

