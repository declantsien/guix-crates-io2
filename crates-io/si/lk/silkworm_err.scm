(define-module (crates-io si lk silkworm_err) #:use-module (crates-io))

(define-public crate-silkworm_err-0.1.0-dev.0 (c (n "silkworm_err") (v "0.1.0-dev.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "silkworm_sourcemap") (r "^0.1.0-dev.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0") (d #t) (k 0)))) (h "01lsfipzi3b3fq3wmhjmbpr38d212a44x29n65c6nlgrh5x0agg7")))

(define-public crate-silkworm_err-0.1.0-dev.1 (c (n "silkworm_err") (v "0.1.0-dev.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "silkworm_sourcemap") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0") (d #t) (k 0)))) (h "00xlc4i9l7gvqgwa9l5jfc6b8y7p9zgas55l0lwadx2yibyapc5b")))

