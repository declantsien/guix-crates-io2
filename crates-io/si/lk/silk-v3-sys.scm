(define-module (crates-io si lk silk-v3-sys) #:use-module (crates-io))

(define-public crate-silk-v3-sys-0.1.0 (c (n "silk-v3-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)))) (h "0nal0n648f7aq0bhry4cs0rgwr3wzk34n5y7hmv8lf6nkpr0kljv")))

