(define-module (crates-io si lk silk-sys) #:use-module (crates-io))

(define-public crate-silk-sys-0.1.0 (c (n "silk-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)))) (h "1hyrxlrqwfypcal82g93xq2wn1a9qw743mysl7swvxzpbmhx8532")))

