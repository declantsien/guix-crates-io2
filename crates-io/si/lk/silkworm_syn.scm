(define-module (crates-io si lk silkworm_syn) #:use-module (crates-io))

(define-public crate-silkworm_syn-0.1.0-dev.0 (c (n "silkworm_syn") (v "0.1.0-dev.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (f (quote ("raw"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1xm137vx7dydg1cgb59pj5pmc8j3h77518xmxmdjfy6n5x7bvbh6")))

(define-public crate-silkworm_syn-0.1.0-dev.1 (c (n "silkworm_syn") (v "0.1.0-dev.1") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (f (quote ("raw"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "silkworm_err") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "silkworm_sourcemap") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0fkmqyqaz1w92n3amgjrnxdz2rnckkwyryknvcbxkz8fcqvfn8aw")))

