(define-module (crates-io si lk silkenweb-signals-ext) #:use-module (crates-io))

(define-public crate-silkenweb-signals-ext-0.2.0 (c (n "silkenweb-signals-ext") (v "0.2.0") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)))) (h "0r0kzqj6rknqbw4jraqsw7narhybv8fl3cgk8nyg4jyvblbrvhyr")))

(define-public crate-silkenweb-signals-ext-0.3.0 (c (n "silkenweb-signals-ext") (v "0.3.0") (d (list (d (n "futures-signals") (r "^0.3.31") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0lr08jr2mbhp306msqc36hj3ja8myxwh1pgy11npzg0822jjszan")))

(define-public crate-silkenweb-signals-ext-0.4.0 (c (n "silkenweb-signals-ext") (v "0.4.0") (d (list (d (n "futures-signals") (r "^0.3.31") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1rzmjsyrhxp40bwvk176a7z6vk436dk5zp1kg7mibhcjjcfkiy3v")))

(define-public crate-silkenweb-signals-ext-0.5.0 (c (n "silkenweb-signals-ext") (v "0.5.0") (d (list (d (n "futures-signals") (r "^0.3.31") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "180s5gwadyzld0gi2lnjnppr8ar16c09hzsy60hp3vpqbb7a5ab6")))

(define-public crate-silkenweb-signals-ext-0.6.0 (c (n "silkenweb-signals-ext") (v "0.6.0") (d (list (d (n "futures-signals") (r "^0.3.31") (f (quote ("debug"))) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1phkwpmwx4s6h1vdhbiwdqsy537n6y1d4zfcvsg2nn9nl4z01d0n")))

(define-public crate-silkenweb-signals-ext-0.7.0 (c (n "silkenweb-signals-ext") (v "0.7.0") (d (list (d (n "futures-signals") (r "^0.3.31") (f (quote ("debug"))) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "07csmy2fbb83zchkkyc4l01iqz9acpy83zj4b8ywhs87d7wrqmvk")))

(define-public crate-silkenweb-signals-ext-0.7.1 (c (n "silkenweb-signals-ext") (v "0.7.1") (d (list (d (n "futures-signals") (r "^0.3.31") (f (quote ("debug"))) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1qs5cm35davx0picafi71hclm1xkdip2c4im2hi6ibh81isj912q")))

(define-public crate-silkenweb-signals-ext-0.8.0 (c (n "silkenweb-signals-ext") (v "0.8.0") (d (list (d (n "futures-signals") (r "^0.3.31") (f (quote ("debug"))) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1d3qrlyqsfjx9swwygm2iy8607hmaw5anyfa8nnwfr8ag09prlh3")))

