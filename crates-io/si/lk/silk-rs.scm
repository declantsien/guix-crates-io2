(define-module (crates-io si lk silk-rs) #:use-module (crates-io))

(define-public crate-silk-rs-0.1.0 (c (n "silk-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gw44yzckphrj6hi3n96aad49l2912cjq6yc9ljnm4wk1qfi50h3")))

(define-public crate-silk-rs-0.1.1 (c (n "silk-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iwsmd5fvzbmf0p8rxhwshndqhg9p1yziw3ka5af2w58vl1ar7qi")))

(define-public crate-silk-rs-0.2.0 (c (n "silk-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zhwgc4m0aqrf2n1rq8f3q1k2sy3p2h763jp93w5yf2syccnckh1")))

