(define-module (crates-io si lk silkenweb-reactive) #:use-module (crates-io))

(define-public crate-silkenweb-reactive-0.1.0 (c (n "silkenweb-reactive") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vrcxipmf8x61b4w8l4490cln3wx7x4qc0i7y7mm5r09j9aypack")))

(define-public crate-silkenweb-reactive-0.1.1 (c (n "silkenweb-reactive") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m6i4vgrj1n7ndqf3jfhv2xnbrx3x4fcsdrh9mycnp3asbymiawl")))

(define-public crate-silkenweb-reactive-0.1.2 (c (n "silkenweb-reactive") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q6ccs41zwzvxb0fqjc41hq72a68ac8s9s431iy4nszwcd51fpla")))

