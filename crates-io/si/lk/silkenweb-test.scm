(define-module (crates-io si lk silkenweb-test) #:use-module (crates-io))

(define-public crate-silkenweb-test-0.5.0 (c (n "silkenweb-test") (v "0.5.0") (d (list (d (n "silkenweb") (r "^0.5.0") (d #t) (k 0)) (d (n "silkenweb-base") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (d #t) (k 0)))) (h "0iq2aw3mh5vfjckzh9jnjjnnv4y4hkxbvj7362r0k78zgxlq3008")))

(define-public crate-silkenweb-test-0.6.0 (c (n "silkenweb-test") (v "0.6.0") (d (list (d (n "silkenweb") (r "^0.6.0") (d #t) (k 0)) (d (n "silkenweb-base") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (d #t) (k 0)))) (h "07gazgi38q362qhg3rqdhmax7kq0p11gjhk4rxs57qc22rwjvx0y")))

(define-public crate-silkenweb-test-0.7.0 (c (n "silkenweb-test") (v "0.7.0") (d (list (d (n "silkenweb") (r "^0.7.0") (d #t) (k 0)) (d (n "silkenweb-base") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (d #t) (k 0)))) (h "1q87s7acsfkmrg88f45z90jy333990rimxjyw8hibgpym11wgfml")))

(define-public crate-silkenweb-test-0.7.1 (c (n "silkenweb-test") (v "0.7.1") (d (list (d (n "silkenweb") (r "^0.7.1") (d #t) (k 0)) (d (n "silkenweb-base") (r "^0.7.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.65") (d #t) (k 0)))) (h "1i0mm5d6qi1q1p4h1mjy6nkkxpvhjxqaizymgi6s4ir0g5fh81l8")))

(define-public crate-silkenweb-test-0.8.0 (c (n "silkenweb-test") (v "0.8.0") (d (list (d (n "silkenweb") (r "^0.8.0") (d #t) (k 0)) (d (n "silkenweb-base") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.65") (d #t) (k 0)))) (h "10cynj2nayvl51slcg4df3d4ld3chrj7ypw4crl3i58bk9451cj6")))

