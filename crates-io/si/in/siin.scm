(define-module (crates-io si in siin) #:use-module (crates-io))

(define-public crate-siin-0.1.0 (c (n "siin") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "trauma") (r "^2") (d #t) (k 0)) (d (n "validator") (r "^0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a5nnfc8pg1w31pjyaakks3lkym4i6rm59s5ybkvj6plab2fqxwj")))

