(define-module (crates-io si le silent-headers) #:use-module (crates-io))

(define-public crate-silent-headers-0.1.0 (c (n "silent-headers") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "httpdate") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "silent-headers-core") (r "^0.1") (d #t) (k 0)))) (h "1py66m9lgi81n6cgblb2vfi3r6z92sxih56zf9iknqrx2bv5z308") (f (quote (("nightly")))) (y #t) (r "1.56")))

