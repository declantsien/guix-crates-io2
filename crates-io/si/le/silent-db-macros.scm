(define-module (crates-io si le silent-db-macros) #:use-module (crates-io))

(define-public crate-silent-db-macros-0.1.0 (c (n "silent-db-macros") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "0ci1mbz7rm99c7lagadldll5nd40knqpk4bfdvjjp0bp26rhpcn5")))

(define-public crate-silent-db-macros-0.2.0 (c (n "silent-db-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "1mvz922dynmz3jxpjwim403lxyfjzgx7n13bx43w5xnby7c64psc")))

