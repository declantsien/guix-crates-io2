(define-module (crates-io si mm simmons_rooms) #:use-module (crates-io))

(define-public crate-simmons_rooms-0.1.0 (c (n "simmons_rooms") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.16") (d #t) (k 0)))) (h "18ygv7jz2brgmrhqpfrh4d9sz1vik0azj9h44kpxybzwj36kch71") (f (quote (("dev" "clippy") ("default")))) (y #t)))

(define-public crate-simmons_rooms-0.1.1 (c (n "simmons_rooms") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.16") (d #t) (k 0)))) (h "1hnl3s65p661s6bnd988hrlfc67fwsdj46vg9bpapfs50mmd16y2") (f (quote (("dev" "clippy") ("default"))))))

