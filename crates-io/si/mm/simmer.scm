(define-module (crates-io si mm simmer) #:use-module (crates-io))

(define-public crate-simmer-0.1.0 (c (n "simmer") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)) (d (n "ufmt_float") (r "^0.2.0") (d #t) (k 0)))) (h "07hiqia1mli7xc6g9md8zhs8il6bqjhp4kjj6mprvmc0aq4anlkp") (f (quote (("f32") ("default"))))))

(define-public crate-simmer-0.2.0 (c (n "simmer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "onlyerror") (r "^0.1") (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (d #t) (k 0)) (d (n "ufmt_float") (r "^0.2") (d #t) (k 0)))) (h "16kbc0zv27aknq5l9s91ig9vj3q4s4x8p1f4463qy05y472y3nnq") (f (quote (("f32") ("default") ("checked"))))))

(define-public crate-simmer-0.3.0 (c (n "simmer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "onlyerror") (r "^0.1") (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (d #t) (k 0)) (d (n "ufmt_float") (r "^0.2") (d #t) (k 0)))) (h "18p0lkq0hdja8avyi2d261hm702699f69vybrqzr9mwkdqnq3rh4") (f (quote (("f32") ("default") ("checked"))))))

