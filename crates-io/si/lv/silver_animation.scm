(define-module (crates-io si lv silver_animation) #:use-module (crates-io))

(define-public crate-silver_animation-0.1.0-alpha0.1 (c (n "silver_animation") (v "0.1.0-alpha0.1") (d (list (d (n "quicksilver") (r "^0.4.0-alpha0.4") (d #t) (k 0)))) (h "05b33xk9mh4lxrl5j77pdby5gxdma2407kxb20x8b0f91hgsvslm")))

(define-public crate-silver_animation-0.1.0-alpha0.2 (c (n "silver_animation") (v "0.1.0-alpha0.2") (d (list (d (n "quicksilver") (r "^0.4.0-alpha0.5") (d #t) (k 0)))) (h "0dj7q04nzxczjz33qlqc8g7pmizyzkvdmimifwnvmgmm40hglpgv") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-silver_animation-0.1.0-alpha0.3 (c (n "silver_animation") (v "0.1.0-alpha0.3") (d (list (d (n "quicksilver") (r "^0.4.0-alpha0.5") (d #t) (k 0)))) (h "0r51yi00j7z63bi861rhm06c9ylqczzlfc2kprn909xih4gi4vhw") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-silver_animation-0.1.0-alpha0.4 (c (n "silver_animation") (v "0.1.0-alpha0.4") (d (list (d (n "quicksilver") (r "^0.4.0-alpha0.6") (d #t) (k 0)))) (h "16wdlrl4sfi5m26scsf7x58s99w00bpk53hzg9jcbwxirzv7s3np") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-silver_animation-0.1.0 (c (n "silver_animation") (v "0.1.0") (d (list (d (n "quicksilver") (r "^0.4.0") (d #t) (k 0)))) (h "0gh44382ffxd4ry9v0q4anvqnqw7h5anyryr609j1yys20ikzivv") (f (quote (("stdweb" "quicksilver/stdweb"))))))

