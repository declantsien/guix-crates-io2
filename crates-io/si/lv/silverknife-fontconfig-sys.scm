(define-module (crates-io si lv silverknife-fontconfig-sys) #:use-module (crates-io))

(define-public crate-silverknife-fontconfig-sys-0.1.0 (c (n "silverknife-fontconfig-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dr9hmjk676qx4yv6dlpyag1q7ncxvay4wfjck2lhp8zfc8y6np1")))

