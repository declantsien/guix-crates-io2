(define-module (crates-io si mu simulacrum_mock) #:use-module (crates-io))

(define-public crate-simulacrum_mock-0.1.0 (c (n "simulacrum_mock") (v "0.1.0") (d (list (d (n "debugit") (r "^0.1.0") (d #t) (k 0)) (d (n "handlebox") (r "^0.3.0") (d #t) (k 0)) (d (n "simulacrum_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "simulacrum_user") (r "^0.1.0") (d #t) (k 2)))) (h "0jpcpaaswn0pz98ijdc5izgfnj9rgyizmqbvxqppjz81mf0vx8xh")))

