(define-module (crates-io si mu simulate) #:use-module (crates-io))

(define-public crate-simulate-0.1.0 (c (n "simulate") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "16kkhl0vpvxlyjrk1m97kmkp27sw4ms142n85bmylgq8clqpqqn8") (y #t)))

(define-public crate-simulate-0.1.1 (c (n "simulate") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "0k0ywakc0p8c43qhzg5zmsxacpkf5wk1sbmfr4d5fqj31mpg5xzp") (y #t)))

(define-public crate-simulate-0.1.2 (c (n "simulate") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "1xqcmi4szqzc9rxn9jblra8i0l5qmj65sjwzai14cx2cmlcki1va") (y #t)))

(define-public crate-simulate-0.1.3 (c (n "simulate") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "0pimak7iqkqxcyibgy4lw3shj3ngbbicij2mmz2bwr27blh8hmd6")))

(define-public crate-simulate-0.2.0 (c (n "simulate") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "1vlxfhqxx8vnp7f318smmwrkh3yrqh9gp4akdcrzfij50s51nczr")))

(define-public crate-simulate-0.2.1 (c (n "simulate") (v "0.2.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (t "cfg(windows)") (k 0)))) (h "17571pwvyb81fk8v43zfz3k77fmxifckvy2rmky7kg9rlb3d11yi")))

(define-public crate-simulate-0.3.0 (c (n "simulate") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "errhandlingapi" "winbase"))) (t "cfg(windows)") (k 0)))) (h "14angyc1ir5jdvyk7vy528n85syy676x5136yhw9m51r2m1idvmv")))

