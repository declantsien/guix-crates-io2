(define-module (crates-io si mu simulated_annealing) #:use-module (crates-io))

(define-public crate-simulated_annealing-0.1.0 (c (n "simulated_annealing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "1r4amvmnzpg979nmbrz9rssx13zq5g6694ikbpmrffgdwhs7nk6l") (r "1.63")))

(define-public crate-simulated_annealing-0.1.1 (c (n "simulated_annealing") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "1xzs8sdmpkjn1gxbf0n843xrb2isngjb092s3g7m48fgzksrz51x") (r "1.63")))

(define-public crate-simulated_annealing-0.2.0 (c (n "simulated_annealing") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "16a3aa1b2jxp87j3icdsnb96l4k03brf42c4ybv6gizafp1mid67") (r "1.63")))

(define-public crate-simulated_annealing-0.2.1 (c (n "simulated_annealing") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "0rvm1hq8r8iygakwgfd11yp358j6pxflaw0f1zyq8hz6i9kzf3zi") (r "1.67.1")))

