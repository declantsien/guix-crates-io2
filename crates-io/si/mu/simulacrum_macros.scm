(define-module (crates-io si mu simulacrum_macros) #:use-module (crates-io))

(define-public crate-simulacrum_macros-0.1.0 (c (n "simulacrum_macros") (v "0.1.0") (d (list (d (n "simulacrum") (r "^0.1") (d #t) (k 0)))) (h "0i7yj7r1ds71x153yi5ixvgig91xnyaw7pz467jrp7g5gvf98i9m")))

(define-public crate-simulacrum_macros-0.1.1 (c (n "simulacrum_macros") (v "0.1.1") (d (list (d (n "simulacrum") (r "^0.1") (d #t) (k 0)))) (h "1c0hvrl9k3rpx3lx6xybmwrmkpw4y3nwyglc1bxalb3z58p1n60g")))

(define-public crate-simulacrum_macros-0.2.0 (c (n "simulacrum_macros") (v "0.2.0") (d (list (d (n "simulacrum") (r "^0.2") (d #t) (k 0)))) (h "02qd09fs3lp3ps8898qdhr9im52aa2dib00b9v84apym3bcfrvy0")))

(define-public crate-simulacrum_macros-0.2.1 (c (n "simulacrum_macros") (v "0.2.1") (d (list (d (n "simulacrum") (r "^0.2") (d #t) (k 0)))) (h "0ib6gwfxkcq9m0h9b12flg3gslprr366zvs27ryf2a7admpp4xb1")))

(define-public crate-simulacrum_macros-0.2.2 (c (n "simulacrum_macros") (v "0.2.2") (d (list (d (n "simulacrum") (r "^0.2") (d #t) (k 0)))) (h "15fc1l7pqkghkl4iii5fgcv2qmx7v0i9dkr81llc21q32zlipskb")))

(define-public crate-simulacrum_macros-0.3.0 (c (n "simulacrum_macros") (v "0.3.0") (d (list (d (n "simulacrum_mock") (r "^0.1.0") (d #t) (k 0)))) (h "0y245509l8r4zwzpi5ai7izyc7mn0lvjnp8w59zdkwcxlvj1jb12")))

(define-public crate-simulacrum_macros-0.3.1 (c (n "simulacrum_macros") (v "0.3.1") (d (list (d (n "simulacrum_mock") (r "^0.1.0") (d #t) (k 0)))) (h "1jmd14yi3p033f9bzn4x67v978xf8kankxkm2sjdq5n3gfk5mp6z")))

