(define-module (crates-io si mu simulink-rs) #:use-module (crates-io))

(define-public crate-simulink-rs-0.1.0 (c (n "simulink-rs") (v "0.1.0") (h "1gvh9ydg8n3l39pykb83ydd7j0kwc6xnr49wmw2r482bb52ykyjs")))

(define-public crate-simulink-rs-1.0.0 (c (n "simulink-rs") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "simulink-binder") (r "^2.0.0") (d #t) (k 0)))) (h "0468f274scj1r4h170dwwd2sz87awrnv1jyxp5570zc073kvf4jz")))

(define-public crate-simulink-rs-2.0.0 (c (n "simulink-rs") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "03i0sqi9qb9sh0z7v9c5qjmny6svj52g67igckp3k291qhz1ngn7")))

(define-public crate-simulink-rs-2.0.1 (c (n "simulink-rs") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0cyxh07zmhg73lqix6qizjyq1dy6c4dvgl2j47cimlaygzyvmgkm")))

(define-public crate-simulink-rs-3.0.0 (c (n "simulink-rs") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0wi07jwag3yb9459wb37px69qnn2xxn9ygggz5qykqfsp47cnsl1")))

(define-public crate-simulink-rs-3.0.1 (c (n "simulink-rs") (v "3.0.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "19dbdp2pa91yyrzvlfm77wlf4vsnbb6mmchdmj61yg18rsrvhnfh")))

(define-public crate-simulink-rs-3.0.2 (c (n "simulink-rs") (v "3.0.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "08dapprmj7f4pvk5fk2nk86ylc0ykkjq24hjz5zlhh4f7j34j6cw")))

(define-public crate-simulink-rs-3.1.0 (c (n "simulink-rs") (v "3.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1jpprgq78k4lmxj1xxcgz31a8fx58iqvm6qfl5frzixx4dw2fzac") (y #t)))

(define-public crate-simulink-rs-4.0.0 (c (n "simulink-rs") (v "4.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0snfq6mbbis1a657f9a624xfyklv7gxcn0f7ag1jh8jj5ncb577i") (y #t)))

(define-public crate-simulink-rs-4.0.1 (c (n "simulink-rs") (v "4.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0fqgx0p1ypasc1vn6ill5wg6aqg9jqsqibjxdvx1kf2z5kgxn2m4")))

(define-public crate-simulink-rs-4.0.2 (c (n "simulink-rs") (v "4.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0004yrc23fkwiyxmmgxzhwl82p572w3s8ibx0y85x2hpnagpdyz6")))

