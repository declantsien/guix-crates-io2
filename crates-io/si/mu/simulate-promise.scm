(define-module (crates-io si mu simulate-promise) #:use-module (crates-io))

(define-public crate-simulate-promise-0.0.1 (c (n "simulate-promise") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "time" "macros" "full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1q3cga4d736pf0vy77l80cb0x7is72gag4iblyja1q546vh9cdig")))

(define-public crate-simulate-promise-1.0.0 (c (n "simulate-promise") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "time" "macros" "full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1k2slhwf4mjjnrncbs5y2f7qgr2wlzqyv5cljh4vrlzkikn2v1lh")))

