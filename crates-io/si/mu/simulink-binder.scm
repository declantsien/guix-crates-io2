(define-module (crates-io si mu simulink-binder) #:use-module (crates-io))

(define-public crate-simulink-binder-0.1.0 (c (n "simulink-binder") (v "0.1.0") (d (list (d (n "license") (r "^2.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "18d6klzr6h601z2srvfy57m7gds06iy3znk1z23pf3hmy0i7h74g")))

(define-public crate-simulink-binder-0.1.1 (c (n "simulink-binder") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0gk0qlvmvy31lr9705k9saii3b8krh21kqxxvcyw41wrpfxba6i8")))

(define-public crate-simulink-binder-1.0.0 (c (n "simulink-binder") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0j7z2a6rhyjm246yc6x67hki85yf69fsl4sb0l64fm1diasdkrz8")))

(define-public crate-simulink-binder-2.0.0 (c (n "simulink-binder") (v "2.0.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0w4x6zdpik99ryqfrsgcak0x84cihkbfksw4swirqm2fbys9nsci")))

