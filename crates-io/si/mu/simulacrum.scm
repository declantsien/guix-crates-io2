(define-module (crates-io si mu simulacrum) #:use-module (crates-io))

(define-public crate-simulacrum-0.1.0 (c (n "simulacrum") (v "0.1.0") (d (list (d (n "debugit") (r "^0.1") (d #t) (k 0)) (d (n "handlebox") (r "^0.3") (d #t) (k 0)))) (h "1yi7l6ry9cjlmks3zh14j0rkpryczrlvbn4swwrsd2ph6wp3lglm")))

(define-public crate-simulacrum-0.1.2 (c (n "simulacrum") (v "0.1.2") (d (list (d (n "debugit") (r "^0.1") (d #t) (k 0)) (d (n "handlebox") (r "^0.3") (d #t) (k 0)))) (h "0hxmnkd2qqr0zjnrr430bx67bdwr8rhdl9kxnhj9q5b5hmw9wsbk")))

(define-public crate-simulacrum-0.2.0 (c (n "simulacrum") (v "0.2.0") (d (list (d (n "debugit") (r "^0.1") (d #t) (k 0)) (d (n "handlebox") (r "^0.3") (d #t) (k 0)))) (h "08yvzp9r6awvp8agf741j5h3ygfm8j4c1kgc2faavnbr81yjg0yz")))

(define-public crate-simulacrum-0.2.1 (c (n "simulacrum") (v "0.2.1") (d (list (d (n "debugit") (r "^0.1") (d #t) (k 0)) (d (n "handlebox") (r "^0.3") (d #t) (k 0)))) (h "1sbmcjhl9yjbswmyk8i9341kfq7ybhz071mqlf9bc3mqwyspgv4c")))

(define-public crate-simulacrum-0.2.2 (c (n "simulacrum") (v "0.2.2") (d (list (d (n "debugit") (r "^0.1") (d #t) (k 0)) (d (n "handlebox") (r "^0.3") (d #t) (k 0)))) (h "1q21lyyprjlhqdnw67fyjvh1509lavfnf34wbiins66a3mb1sflg")))

(define-public crate-simulacrum-0.3.0 (c (n "simulacrum") (v "0.3.0") (d (list (d (n "simulacrum_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "simulacrum_mock") (r "^0.1.0") (d #t) (k 0)) (d (n "simulacrum_user") (r "^0.1.0") (d #t) (k 0)))) (h "1ypfpipwdyzvbk62bwdn6lckc7j2x298prv2plcvsdia67mrfdqk")))

(define-public crate-simulacrum-0.3.1 (c (n "simulacrum") (v "0.3.1") (d (list (d (n "simulacrum_macros") (r "^0.3.1") (d #t) (k 0)) (d (n "simulacrum_mock") (r "^0.1.0") (d #t) (k 0)) (d (n "simulacrum_user") (r "^0.1.0") (d #t) (k 0)))) (h "1kjq1794025xjwbkmr9gp1fvrgzg3wi27ifmw3vb4rhz24bbqv7w")))

