(define-module (crates-io si mu simult) #:use-module (crates-io))

(define-public crate-simult-0.1.0 (c (n "simult") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gx1rplxzipv3gmwslggdyxyggblxx6zxz7fjvki3sbgpjgjzdn0")))

