(define-module (crates-io si np sinput) #:use-module (crates-io))

(define-public crate-sinput-0.1.0 (c (n "sinput") (v "0.1.0") (h "1swi6kap66jqgjm7asv457s5lng5ww405v2pf6avvk7fkwf8qlv2")))

(define-public crate-sinput-0.1.1 (c (n "sinput") (v "0.1.1") (h "0lwddb1c336qdd1g267241di3303mx7a2ryx2nyb5k0llcwnsldq")))

(define-public crate-sinput-0.1.2 (c (n "sinput") (v "0.1.2") (h "0fmjfm9r5sl4wp9abx931hc1safd388i2kl2icaja1ixw7x7kxxi")))

(define-public crate-sinput-0.1.3 (c (n "sinput") (v "0.1.3") (h "0dbspaf1qf2kdhdrv344ipmmd4xiaznb4xbii2vvh2lnr57vsv4s")))

(define-public crate-sinput-0.1.4 (c (n "sinput") (v "0.1.4") (h "19l0mc7yjyslq7k3f8bj546hzznn1fc9p6qilda3ppmikq0dxml4")))

(define-public crate-sinput-0.1.5 (c (n "sinput") (v "0.1.5") (h "1fdm6dck3j6nfd0npx3ihvdrj67f5zf755sbqygmz38kf52lfrbq")))

