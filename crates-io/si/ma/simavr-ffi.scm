(define-module (crates-io si ma simavr-ffi) #:use-module (crates-io))

(define-public crate-simavr-ffi-0.1.0 (c (n "simavr-ffi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "fs_extra") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "00477c07lqdvrbrvj0s663qz3habs8j0plh8m7c5rnqwkkn1p3sq") (l "simavr")))

