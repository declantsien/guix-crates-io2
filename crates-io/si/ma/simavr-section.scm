(define-module (crates-io si ma simavr-section) #:use-module (crates-io))

(define-public crate-simavr-section-0.1.0 (c (n "simavr-section") (v "0.1.0") (d (list (d (n "simavr-section-macro") (r "^0.1") (d #t) (k 0)) (d (n "simavr-section-sys") (r "^0.1") (d #t) (k 0)))) (h "1yrxqc1fcva1wc09gyvsgv041zlnc33906wc268mgvxzb1awnyh1")))

(define-public crate-simavr-section-0.1.1 (c (n "simavr-section") (v "0.1.1") (d (list (d (n "simavr-section-macro") (r "^0.1") (d #t) (k 0)) (d (n "simavr-section-sys") (r "^0.1") (d #t) (k 0)))) (h "0vsyil8i87ly8qdbm3qph4knb8vw545ddh1sv47d0a4rjylwrpdc")))

