(define-module (crates-io si ma simavr-sim) #:use-module (crates-io))

(define-public crate-simavr-sim-0.1.0 (c (n "simavr-sim") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "simavr-sys") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "vsprintf") (r "^2.0") (d #t) (k 0)))) (h "0xxlffwrmfwzi5d2firk5zb0pcqv9cvlxdj4cq302m66cs6fkg1p")))

(define-public crate-simavr-sim-0.1.1 (c (n "simavr-sim") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "simavr-sys") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "vsprintf") (r "^2.0") (d #t) (k 0)))) (h "0dw6p6kq3nd5x75hmw0s43ckmc7bvbgm973fkp64dc6kjpi8kpjh")))

