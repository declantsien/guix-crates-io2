(define-module (crates-io si ma simavr-section-macro) #:use-module (crates-io))

(define-public crate-simavr-section-macro-0.1.0 (c (n "simavr-section-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y24awm9rx7m1ddh3cjigz13vq85phab27c7jpr5l0ac05mqrpbm")))

