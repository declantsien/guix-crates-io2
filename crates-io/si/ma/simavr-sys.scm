(define-module (crates-io si ma simavr-sys) #:use-module (crates-io))

(define-public crate-simavr-sys-1.5.1 (c (n "simavr-sys") (v "1.5.1") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1w9xmds37iizp440qxlaxpfc67ff5l4jdnzn3pqggbr9w1jcl48c") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.1-1 (c (n "simavr-sys") (v "1.5.1-1") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1y8sjkjmxd3nlfw6183xb610q16wwqhh44zdx1awv04q3qjj970q") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.2 (c (n "simavr-sys") (v "1.5.2") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "17ciap8bjx61qnxa4fx7w9h594pnxz70nsirav9lxxdyzna7p02n") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.3-0 (c (n "simavr-sys") (v "1.5.3-0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1gh053dbqmfc98sy6dsxv0nj8mkc9y44wzfsk8mwj2g3ylha1q1d") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.4 (c (n "simavr-sys") (v "1.5.4") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "0a4w6ycv0gmd3bw7zf3l4kj5z0s9mdapgp5akdzyzwdbmvwl32yw") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.5 (c (n "simavr-sys") (v "1.5.5") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1qf9c16da0vbqsbx76szsz61wnzfzs0d9qbmyw2i1cs6wijiazpx") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.6 (c (n "simavr-sys") (v "1.5.6") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1gmrravnfk3vfsjxl0zncmqwa8gi8xv9yhv25q4x7p6vk0fixfj5") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.7 (c (n "simavr-sys") (v "1.5.7") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "1gv6b8z7nklm3r845hz6z8x5lwmy7r1jlprn71fi37n094fvh6ng") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.8 (c (n "simavr-sys") (v "1.5.8") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "0lhfsh95x5rxa8i3fqgp8xccswi7x8qhdh8y8rra1zabc9sv9488") (f (quote (("trace") ("default"))))))

(define-public crate-simavr-sys-1.5.9 (c (n "simavr-sys") (v "1.5.9") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "walkdir") (r "^1.0") (d #t) (k 1)))) (h "0zk0l72j3nxqjldmr3x81z45zndcb21nf2wbc9fq15225dw2vg9z") (f (quote (("trace") ("default"))))))

