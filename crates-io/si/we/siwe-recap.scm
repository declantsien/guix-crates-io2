(define-module (crates-io si we siwe-recap) #:use-module (crates-io))

(define-public crate-siwe-recap-0.1.0 (c (n "siwe-recap") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12sw5nmsks5j5502f41wgag96l0mi7lwb90b8i5jg6gyf14jbrmg")))

(define-public crate-siwe-recap-0.2.0 (c (n "siwe-recap") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "cid") (r "^0.10") (d #t) (k 0)) (d (n "iri-string") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_jcs") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (d #t) (k 0)) (d (n "siwe") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ucan-capabilities-object") (r "^0.1") (d #t) (k 0)))) (h "1na0rirpr6a2r3ybv16mg58wxvzb09k5fa9h5lhl9ji7abm5lk05")))

