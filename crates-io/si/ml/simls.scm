(define-module (crates-io si ml simls) #:use-module (crates-io))

(define-public crate-simls-0.1.0 (c (n "simls") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d5bzfycknghaibc1b4axgl8biny15mxhirnhhyyz45f59icgq1f") (y #t)))

(define-public crate-simls-0.0.2 (c (n "simls") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19ywxj388104107plf9y5w2nrcrikfbg4ki9gq6xd0fdgyyx66kf")))

