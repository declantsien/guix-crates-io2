(define-module (crates-io si ml simlog) #:use-module (crates-io))

(define-public crate-simlog-1.0.0 (c (n "simlog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "05wzaqvbz28ygl58sxcljkvcn91ff96k7sjp06xhm054i4k44yl5") (y #t)))

(define-public crate-simlog-1.0.1 (c (n "simlog") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "152flxmq2qc7gyd4chwk04hmsvg2ja1yd7j9l4qiy9ximm9k6rgn") (y #t)))

(define-public crate-simlog-1.0.2 (c (n "simlog") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "00y859ymnhigndm3cadjlvi5zx2fcikqfj9v6k7pw33xvkcnhj60") (y #t)))

(define-public crate-simlog-1.0.3 (c (n "simlog") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "00fi7xivsbzp5k3dkmys5w7gddmggvd0sljssx6dn61fk3vbf09p") (y #t)))

(define-public crate-simlog-2.0.0 (c (n "simlog") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "1c65pnrmdb9zsl7gbr7gwn961dqr0ci8ak9svd17h6pc9wil6y0l") (y #t)))

(define-public crate-simlog-2.0.1 (c (n "simlog") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "0z17bvdfabfdf10yara2rhlb5kldzj1819nsxq44kscjqwgjjj0d") (y #t)))

(define-public crate-simlog-2.0.2 (c (n "simlog") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "10if92l3y14gfy3951vixmjszaa273fnj2jk4pj3fp8xjfic1xc6") (y #t)))

(define-public crate-simlog-3.0.0 (c (n "simlog") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 0)))) (h "0ria9p2hysszkhgdlayqgya9lyqazdy33j09lxzdcg8sz0nc6vd3")))

