(define-module (crates-io si nk sinkhole-core) #:use-module (crates-io))

(define-public crate-sinkhole-core-0.0.1 (c (n "sinkhole-core") (v "0.0.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^2.0.0") (f (quote ("serde"))) (k 0)) (d (n "elgamal_ristretto") (r "^0.2.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (k 0)))) (h "0wjmz8njdwqj32dkqmvf2pb5pbm97950rc9892lc75djgnwvq2sk")))

