(define-module (crates-io si nk sink-rotate) #:use-module (crates-io))

(define-public crate-sink-rotate-1.0.0 (c (n "sink-rotate") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0g1yczq050r84qbic993czqq7ff43v6mfk93x54ziij4z1avvjvq")))

(define-public crate-sink-rotate-1.0.2 (c (n "sink-rotate") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1c1nws4sac60qmw9bn709qx494r97mpnjs09aml2b2by4pm76pxg")))

(define-public crate-sink-rotate-1.0.3 (c (n "sink-rotate") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1kd4c952vrlhy4a9c0sf2svhvvig6n4hlf58c1jh3rsbd065ahi0")))

(define-public crate-sink-rotate-1.0.4 (c (n "sink-rotate") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "058ds2kx7ln9wi7w4nz79rgw1mwp1rn1fq093sh3wggzxvhqi197")))

