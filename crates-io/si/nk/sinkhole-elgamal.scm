(define-module (crates-io si nk sinkhole-elgamal) #:use-module (crates-io))

(define-public crate-sinkhole-elgamal-0.0.1 (c (n "sinkhole-elgamal") (v "0.0.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "elgamal_ristretto") (r "^0.2.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sinkhole-core") (r "^0.0.1") (d #t) (k 0)))) (h "122bhhvx4w3q1m0hz44c623l2q8m4cnamjjcf504602y879j3r3b")))

