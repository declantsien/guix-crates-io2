(define-module (crates-io si dx sidx) #:use-module (crates-io))

(define-public crate-sidx-0.1.0 (c (n "sidx") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10xv4xn955fcghdcy7rrcxmfzfx22l6gsdhlv3gbvccqcpqk4lzz") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sidx-0.2.0 (c (n "sidx") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q42ayh5xxpsi5a47lppicrx7m5zsjx4h4x19ji7wjj7diwv9cih") (s 2) (e (quote (("serde" "dep:serde"))))))

