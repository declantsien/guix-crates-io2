(define-module (crates-io si md simdjson-rust) #:use-module (crates-io))

(define-public crate-simdjson-rust-0.2.0-alpha (c (n "simdjson-rust") (v "0.2.0-alpha") (d (list (d (n "cxx") (r "^1.0.74") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.74") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gr26ycrmw22x52x2jw9508frw0v8kld5dz1z63bxpybkawzpx94") (f (quote (("serde_impl" "serde" "serde_json") ("default" "serde_impl")))) (l "simdjson-rust")))

(define-public crate-simdjson-rust-0.3.0-alpha (c (n "simdjson-rust") (v "0.3.0-alpha") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "simdjson-sys") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pwxsxi0ji5fgn865pky7nd4gkz2z3n1c2cjhbzw9hz7bzn2i7pl") (f (quote (("serde_impl" "serde" "serde_json") ("default"))))))

(define-public crate-simdjson-rust-0.3.0-alpha.1 (c (n "simdjson-rust") (v "0.3.0-alpha.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "simdjson-sys") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11kl7wmfqx2yzh8rm8sjafa1192qsszqcmv1wa7an4p449ac8zqw") (f (quote (("serde_impl" "serde" "serde_json") ("default"))))))

(define-public crate-simdjson-rust-0.3.0-alpha.2 (c (n "simdjson-rust") (v "0.3.0-alpha.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "simdjson-sys") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0559rvqbls9i46z2fzjx1rkrri7l72997v4sy35vsqjk53p912w9") (f (quote (("serde_impl" "serde" "serde_json") ("default"))))))

