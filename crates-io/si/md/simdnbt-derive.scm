(define-module (crates-io si md simdnbt-derive) #:use-module (crates-io))

(define-public crate-simdnbt-derive-0.2.0 (c (n "simdnbt-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0s0c17z8qg518fqjm2gmhw40ykwghknp8ja5f84y0qy9db82gip2")))

(define-public crate-simdnbt-derive-0.2.1 (c (n "simdnbt-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0kjbi6asjwpqlfc1nbpl3fd40wpm6f56bd6cnjf36n02y2rjf2c4")))

(define-public crate-simdnbt-derive-0.3.0 (c (n "simdnbt-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0k6mx33myhh1krqc5lr8v7lfvmgm1hnsy8cd4smm1kx7njpg6hb2")))

(define-public crate-simdnbt-derive-0.4.0 (c (n "simdnbt-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "08vw5099y59j2szafi0iz6bk65ivi221y1shjprf3fxfa9ca009p")))

(define-public crate-simdnbt-derive-0.4.1 (c (n "simdnbt-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1hps2j24m2ijprz1db4kiprsx8y8pjyrpjcqfdpf8b80h77lhiii")))

(define-public crate-simdnbt-derive-0.4.3 (c (n "simdnbt-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "1ys17rjm3cizmllk9x0nyfyrq7gkvnqcq84aw3l1w9hh4dpdrl0y")))

(define-public crate-simdnbt-derive-0.5.2 (c (n "simdnbt-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "188zvllv875c02svy46gny99lsz39zzaxircn0s3f3id2naqmqmz")))

