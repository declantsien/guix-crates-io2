(define-module (crates-io si md simd_bytes) #:use-module (crates-io))

(define-public crate-simd_bytes-0.1.0 (c (n "simd_bytes") (v "0.1.0") (h "084hvv3a796dk9si03qvm8k0znqj4qac8dffh341vrz1dziigymv")))

(define-public crate-simd_bytes-0.1.1 (c (n "simd_bytes") (v "0.1.1") (h "147s8j9pc65nda8r9hk3qz395y8dbvfwvy0m5hmqcmpl1pw9ysg7")))

