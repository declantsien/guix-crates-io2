(define-module (crates-io si md simdnoise) #:use-module (crates-io))

(define-public crate-simdnoise-1.0.0 (c (n "simdnoise") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "1z9jqfbsjla70yd9z7whxy8qgv50dpxvy9x73navz72acmsya3is")))

(define-public crate-simdnoise-1.0.1 (c (n "simdnoise") (v "1.0.1") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "1p7r0bjdsn63pcqalgdws5ai4li2wn5fxn9zx1v2vskf97zpjna9")))

(define-public crate-simdnoise-1.0.2 (c (n "simdnoise") (v "1.0.2") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "1pn5wy0z4wdlr9wblbid7jhvh8zh88r6qapgsamws17wrkjbhd2d")))

(define-public crate-simdnoise-2.0.0 (c (n "simdnoise") (v "2.0.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "0zbj7v2c05zwinczlkj900hpfgcmq56a8wzkjikksmkmyjbvnsr7")))

(define-public crate-simdnoise-2.0.1 (c (n "simdnoise") (v "2.0.1") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "12zdag3ivfswgccz9jijnxaj50vvcd5952p0vifadraihgyfgrmd")))

(define-public crate-simdnoise-2.1.0 (c (n "simdnoise") (v "2.1.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "0nzdrwp6cacqdhpw1c2hr8hbaz8sc1ld5nk010m4vs3wkx3m9gda")))

(define-public crate-simdnoise-2.2.0 (c (n "simdnoise") (v "2.2.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 0)))) (h "007w7sgamjsd9z6kdcb0chj76zgvf646kb7cyhqi10bi02v0mh4l")))

(define-public crate-simdnoise-2.2.1 (c (n "simdnoise") (v "2.2.1") (h "048zf50gb60n5yhl958dfhkq89xb8nkdrh60phmfpgn1s91b19gp")))

(define-public crate-simdnoise-2.2.2 (c (n "simdnoise") (v "2.2.2") (h "1x28ky4pr243x9x9ymf3m3anshab78xviwc1h7br1fmk2jis37sf")))

(define-public crate-simdnoise-2.2.3 (c (n "simdnoise") (v "2.2.3") (h "1mndq14m6mzwq7cgzzp3119kaykj2if6jcgypij59jypmi25l7ls")))

(define-public crate-simdnoise-2.3.0 (c (n "simdnoise") (v "2.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.2.0") (d #t) (k 0)))) (h "09sa10q26i2j7nhz4h26qjxhslh8v09fp31nlyrs114jv8lhfflb")))

(define-public crate-simdnoise-2.3.1 (c (n "simdnoise") (v "2.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.2.2") (d #t) (k 0)))) (h "1r4czkw3c2rdx9dm2lvyvjckbwhrzlxfykjgncda8rwb2j4cyb6l")))

(define-public crate-simdnoise-2.3.2 (c (n "simdnoise") (v "2.3.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.2.2") (d #t) (k 0)))) (h "0p9b5ivxkcg9rqp9608kgczb1ahc74g5jh8vmpnx72jypkkdy2jm")))

(define-public crate-simdnoise-2.3.3 (c (n "simdnoise") (v "2.3.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.2.3") (d #t) (k 0)))) (h "0l78fbbplfl8w8x7z3b77j1399vqjn3xbxz399kybyphhxia050v")))

(define-public crate-simdnoise-2.3.4 (c (n "simdnoise") (v "2.3.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.2.3") (d #t) (k 0)))) (h "1v24yr7fm9lz7aqkd4phcychckkqrrp6l68sfl7v2c8w40xany93")))

(define-public crate-simdnoise-2.3.5 (c (n "simdnoise") (v "2.3.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.3.1") (d #t) (k 0)))) (h "183xgz28qnxhkg0d0ngx9g59xwjcwrngmjk0bsn4z4gqxxl39znj")))

(define-public crate-simdnoise-2.3.6 (c (n "simdnoise") (v "2.3.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.4.0") (d #t) (k 0)))) (h "0w7qsygnns2vdvv0gp6dymv5r1w2j460ivsw4fjx8pndaj3y3v3w")))

(define-public crate-simdnoise-2.3.7 (c (n "simdnoise") (v "2.3.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "simdeez") (r "^0.5.0") (d #t) (k 0)))) (h "1sg9ccpsq9nzjnhgjwmjwpzwpr2l0gqv36hpgg7d7l0zgf4rr4f2")))

(define-public crate-simdnoise-3.0.0 (c (n "simdnoise") (v "3.0.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "simdeez") (r "^0.6.1") (d #t) (k 0)))) (h "1n69mpfdff9r1jghb2x74wkyqpfcm4mi52537asllfgwklmcxg37")))

(define-public crate-simdnoise-3.0.1 (c (n "simdnoise") (v "3.0.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "simdeez") (r "^0.6.4") (d #t) (k 0)))) (h "0gk8mv8l282phjjlf165i9349azwr1sgw0lajy7frgrgrbg6hnh4")))

(define-public crate-simdnoise-3.1.1 (c (n "simdnoise") (v "3.1.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "simdeez") (r "^0.6.4") (d #t) (k 0)))) (h "1cyp9vrmgkxgw6r7r1v31bnjz501ls11azmc0mqsp7inqg0y9ac6")))

(define-public crate-simdnoise-3.1.2 (c (n "simdnoise") (v "3.1.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "simdeez") (r "^0.6.4") (d #t) (k 0)))) (h "045pg73vcqkah4l1659a08rvx61fikxpw0b9ivfdq2yxp2s12wp5")))

(define-public crate-simdnoise-3.1.3 (c (n "simdnoise") (v "3.1.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "simdeez") (r "^0.6.6") (d #t) (k 0)))) (h "0v4gdbk1l16sl72dixrya5f9glnp2aihvp917bz9p797z8xl1780")))

(define-public crate-simdnoise-3.1.4 (c (n "simdnoise") (v "3.1.4") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "simdeez") (r "^1.0.0") (d #t) (k 0)))) (h "00nvmp8f2m8kyhb7i4k1y1szz2q62svcf033shvq5gsm9wgc24hd")))

(define-public crate-simdnoise-3.1.5 (c (n "simdnoise") (v "3.1.5") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "simdeez") (r "^1.0.1") (d #t) (k 0)))) (h "1a4c26x8lzm5qn3wzx01s855hlz8hdjqwhr5ysbm18pdc1slacba")))

(define-public crate-simdnoise-3.1.6 (c (n "simdnoise") (v "3.1.6") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "simdeez") (r "^1.0.3") (d #t) (k 0)))) (h "0qm4bwa8780lm1g6ki17afwrqb6xbg2yqyp2zw32kz1il0zrmxnr")))

