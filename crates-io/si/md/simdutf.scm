(define-module (crates-io si md simdutf) #:use-module (crates-io))

(define-public crate-simdutf-0.1.0 (c (n "simdutf") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qcvdrmna4rbgi02yk35n9zw9hgnb2a6jw81mwb7qxd3v6jm4zrz") (y #t)))

(define-public crate-simdutf-0.1.1 (c (n "simdutf") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0if0gpi6bmy4p8gx4l722g36ydiw3d6mb1zgscp1pjy23jr2z1lb") (y #t)))

(define-public crate-simdutf-0.2.0 (c (n "simdutf") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17yr0jvvnigna2b4za20wszlnh4q3320gx0k01y43n99h999c9g6") (y #t)))

(define-public crate-simdutf-0.2.1 (c (n "simdutf") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jlg5cqhfsazzpbjms012il0arwwb5lcmhk5j8gk34cxg1mv61kq") (y #t)))

(define-public crate-simdutf-0.3.0 (c (n "simdutf") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ww1hdx1i73wardlflgycs1ads9vdmj465z7jahjqlzyx9mwyi5d") (y #t)))

(define-public crate-simdutf-0.4.0 (c (n "simdutf") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dzkrrc4l37qf3w288m5y7qpvxxlmcv5g0mihasyrpxh854zjck4") (y #t)))

(define-public crate-simdutf-0.4.1 (c (n "simdutf") (v "0.4.1") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i4x0n1v2y8d3lsac5fpv6adf5izj1fzkdz6pyirsk36bw37ss5k")))

(define-public crate-simdutf-0.4.2 (c (n "simdutf") (v "0.4.2") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p74slh8326cwmg89k34q76qav9m66wgwzm7167gl1s3c3p2nh9s")))

(define-public crate-simdutf-0.4.3 (c (n "simdutf") (v "0.4.3") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16zl5pbnxfcbr7z181nbijc988h0y152qxfqjwd58kmpdjq3phn9")))

(define-public crate-simdutf-0.4.4 (c (n "simdutf") (v "0.4.4") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1w9l9i70fgmiwrwbnx58wpp4qp0cx2w9h8ggi5wib29czpninjz6") (y #t)))

(define-public crate-simdutf-0.4.5 (c (n "simdutf") (v "0.4.5") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1pmw27isz4zsfc5l1z2dz7rgp3f712mbs31agvp7lsinfbmk6fj6") (y #t)))

(define-public crate-simdutf-0.4.6 (c (n "simdutf") (v "0.4.6") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0825y9awvp70qldg2x1w6z0z80g6ihkysxyzha38hlnas1p8n7gy")))

(define-public crate-simdutf-0.4.7 (c (n "simdutf") (v "0.4.7") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1y2412n9hj0qsnp8p61gd909ya50gvn0d69r59sxn0471cisxdgi")))

(define-public crate-simdutf-0.4.8 (c (n "simdutf") (v "0.4.8") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ak8d11crcwr23k68y74qc05r8fcyaz9shl6a5q50al52mqk2ab2")))

(define-public crate-simdutf-0.4.9 (c (n "simdutf") (v "0.4.9") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dwfwqs8y59zblwqsp0179x853v6zdxsg955ynwn8xc8ry7z9n5c")))

(define-public crate-simdutf-0.4.11 (c (n "simdutf") (v "0.4.11") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1iikq9s1gkdplncpll5a3f7265b9mm7dgvd23zkb3yjh6ckmnfvf")))

(define-public crate-simdutf-0.4.12 (c (n "simdutf") (v "0.4.12") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0cfyr5fjw0zy870rp5sz04nzky6b4r6c0h9s7ksvv0lsq7zivq3c")))

(define-public crate-simdutf-0.4.13 (c (n "simdutf") (v "0.4.13") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gmhj6bg2z2wfwhpfah4b9lylikh8pc2z37np336162yvx0c70nc")))

(define-public crate-simdutf-0.4.14 (c (n "simdutf") (v "0.4.14") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09bg90s1m9z2v81rlnyzam56qyns6mm92gvk0jrpm5vf0ap7n7zx")))

(define-public crate-simdutf-0.4.15 (c (n "simdutf") (v "0.4.15") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gr2khgxnlvfpar8wxyk8mxndwb27w6fd4jjgyhwqpfs69iaf9sl")))

(define-public crate-simdutf-0.4.16 (c (n "simdutf") (v "0.4.16") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1665wlkkzx8ny1q5i936lzyvf5f7skin3fg8fzv230amadl5y4sc")))

(define-public crate-simdutf-0.4.17 (c (n "simdutf") (v "0.4.17") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18l6qpdill28sy5ynd66044lhyzafixzpdw78lnizz1gc2dd53yg")))

