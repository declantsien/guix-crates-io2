(define-module (crates-io si md simd) #:use-module (crates-io))

(define-public crate-simd-0.0.1 (c (n "simd") (v "0.0.1") (h "05ixnq0l1hl8nq6b9qc5ag0sgyza7hk9z77km9xpfzv1ldad5843")))

(define-public crate-simd-0.0.2 (c (n "simd") (v "0.0.2") (d (list (d (n "llvmint") (r "^0") (d #t) (k 0)) (d (n "simdty") (r "^0") (d #t) (k 0)))) (h "1akw4hc0ljbwghpszf4h8vcfk881625f764z8hvrzgqwwkr3n2js") (f (quote (("xop") ("ssse3" "sse3") ("sse42" "sse41") ("sse41" "ssse3") ("sse3" "sse2") ("sse2" "sse") ("sse" "mmx") ("shims") ("neon") ("mmx") ("fma") ("default" "shims") ("avx512" "avx2") ("avx2" "avx") ("avx" "sse42"))))))

(define-public crate-simd-0.0.3 (c (n "simd") (v "0.0.3") (d (list (d (n "llvmint") (r "^0") (d #t) (k 0)) (d (n "simdty") (r "^0") (d #t) (k 0)))) (h "0pvx04v83civrizyz44cricx7xb9amg5idvs7bqk4ggvs11slfq2") (f (quote (("xop") ("ssse3" "sse3") ("sse42" "sse41") ("sse41" "ssse3") ("sse3" "sse2") ("sse2" "sse") ("sse" "mmx") ("shims") ("neon") ("mmx") ("fma") ("default" "shims") ("avx512" "avx2") ("avx2" "avx") ("avx" "sse42"))))))

(define-public crate-simd-0.1.0 (c (n "simd") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17hqvvhnwjfybpy9v1cdsw8gjxizqjvsn2whw180arlpkwgj0v77") (f (quote (("doc"))))))

(define-public crate-simd-0.1.1 (c (n "simd") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.4") (o #t) (d #t) (k 0)))) (h "08vhhz1w5m7amfp1d9lvfyyzl0jqjm82hrr7fb7afv3n5my89db3") (f (quote (("doc"))))))

(define-public crate-simd-0.2.0 (c (n "simd") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "183vwg7c5z4pl0wifzhw31h2adwlwilzprbxjc8g3wg1595d353s") (f (quote (("with-serde" "serde" "serde_derive") ("doc"))))))

(define-public crate-simd-0.2.1 (c (n "simd") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wv6dr4ryzwp5yyc8mm7xagk9l3f9g91lld156l53av3fdf81l1x") (f (quote (("with-serde" "serde" "serde_derive") ("doc"))))))

(define-public crate-simd-0.2.2 (c (n "simd") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cgzxdkydkmvyn8sz6g87zjq21mbvrbaxh504qxcrsqqjkfqcdpd") (f (quote (("with-serde" "serde" "serde_derive") ("doc"))))))

(define-public crate-simd-0.2.3 (c (n "simd") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "116sbzq3zxx4lsfbvja1xb5v9ps1kdaw718xqr2wayjpp5zb2j00") (f (quote (("with-serde" "serde" "serde_derive") ("doc"))))))

(define-public crate-simd-0.2.4 (c (n "simd") (v "0.2.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1dgpmfzd4favsckd5m0p6bna1dcgw19hjigkqcgwfhc4d05hxczj") (f (quote (("with-serde" "serde" "serde_derive") ("doc"))))))

