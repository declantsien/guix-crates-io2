(define-module (crates-io si md simd-abstraction) #:use-module (crates-io))

(define-public crate-simd-abstraction-0.1.0 (c (n "simd-abstraction") (v "0.1.0") (h "05z53p7hypdfvng716r16l35yyqbcw9s748i608y0qs1wfmr729h") (f (quote (("std")))) (y #t)))

(define-public crate-simd-abstraction-0.1.1 (c (n "simd-abstraction") (v "0.1.1") (h "01r1pxmnmrm7q8lp6wwklz3lx06yn7pz9c34izg8rdjxpk8i6n0w") (f (quote (("std")))) (y #t)))

(define-public crate-simd-abstraction-0.1.2 (c (n "simd-abstraction") (v "0.1.2") (h "1rkgsqb614dhvgiy8y5qliav7rvc1vafhr4xw6ipcmp7mpfmmblh") (f (quote (("std") ("hex")))) (y #t)))

(define-public crate-simd-abstraction-0.3.0 (c (n "simd-abstraction") (v "0.3.0") (h "0199k5m9zd8iyqck9lcgin10g9vy6j1kfm5xmmqb7wqpglcld39h") (f (quote (("std") ("hex")))) (y #t)))

(define-public crate-simd-abstraction-0.5.0 (c (n "simd-abstraction") (v "0.5.0") (h "14z8192jhb42bp9fff6k4hq3a2kzxq7gxgq1fffxarrikgfglw6z") (f (quote (("unstable") ("std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-simd-abstraction-0.5.1 (c (n "simd-abstraction") (v "0.5.1") (h "1qwlc01wrnd0y06w24ywwwijvjhl3wsmp75w01mfjv3mh84zdjis") (f (quote (("unstable") ("std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-simd-abstraction-0.6.0 (c (n "simd-abstraction") (v "0.6.0") (h "0z20nwyp2f1w9knc81b0vmq62264xbwf3dqjw0jid4n44f5z20p8") (f (quote (("unstable") ("std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-simd-abstraction-0.6.1 (c (n "simd-abstraction") (v "0.6.1") (h "0q03izar72pjvc65xihxq1g3spy2sfv0qwrsxpb2h9zbn7lxfkfx") (f (quote (("unstable") ("std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-simd-abstraction-0.6.2 (c (n "simd-abstraction") (v "0.6.2") (h "1n16qb3gkwgfnjwims9hda66k3ndc4cnik5vcpp26a1rgczhz262") (f (quote (("unstable") ("std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-simd-abstraction-0.7.0 (c (n "simd-abstraction") (v "0.7.0") (d (list (d (n "outref") (r "^0.1.0") (d #t) (k 0)))) (h "0q8w076vkr5rc7lm9f34g2q11ddv7nhsd2945l83bsrkiirg1ls9") (f (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (y #t) (r "1.61")))

(define-public crate-simd-abstraction-0.7.1 (c (n "simd-abstraction") (v "0.7.1") (d (list (d (n "outref") (r "^0.1.0") (d #t) (k 0)))) (h "11v9hy8qg0b4qypz2p75ijv41ln1rssk6qilz0gwbbfaayfb5bcw") (f (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (r "1.61")))

