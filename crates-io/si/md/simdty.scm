(define-module (crates-io si md simdty) #:use-module (crates-io))

(define-public crate-simdty-0.0.1 (c (n "simdty") (v "0.0.1") (h "1dznwvyhrc106sbs97rfv0v6jc1ylz107xkbawl6l8gdlsymjrrb")))

(define-public crate-simdty-0.0.2 (c (n "simdty") (v "0.0.2") (h "0bkvilibh4f6dx8wlc9cm70vjwxfr269mbarc226zrcvpgmizs6a")))

(define-public crate-simdty-0.0.3 (c (n "simdty") (v "0.0.3") (h "04y3m8j83wc0bclpr1d3mqpplbklrh5j98xk2rhndwh4chqpya91")))

(define-public crate-simdty-0.0.4 (c (n "simdty") (v "0.0.4") (h "155kz84hjiidbrjbr3clb7j307qjal7jffd41mp6qx2kwxv6qkh7") (f (quote (("unstable"))))))

(define-public crate-simdty-0.0.5 (c (n "simdty") (v "0.0.5") (h "0z44vj8bnkzxgnn8kcymblrwi95bl25r5hibjbgpwchn2pqv7310") (f (quote (("unstable"))))))

(define-public crate-simdty-0.0.6 (c (n "simdty") (v "0.0.6") (h "18fsvml71v6vhidz64lg04r1hna1466npwbp6r9wkxqc0h799gjn") (f (quote (("unstable"))))))

