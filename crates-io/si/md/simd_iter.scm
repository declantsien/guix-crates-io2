(define-module (crates-io si md simd_iter) #:use-module (crates-io))

(define-public crate-simd_iter-0.1.0 (c (n "simd_iter") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0nzgi2bpf84il9kqiijrf9kpjdr8kqgp39k4vbj2grmwi1ciwr3d")))

(define-public crate-simd_iter-0.2.0 (c (n "simd_iter") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1d02xk0lw6zw3qbq3vk9mvhiqlnlg68502dn10f59nv9yl5ikflx")))

