(define-module (crates-io si md simd-adler32) #:use-module (crates-io))

(define-public crate-simd-adler32-0.1.0 (c (n "simd-adler32") (v "0.1.0") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0f8p2arl9147bizrhqk9c44j0yddq9z7k059ca4spq4lihnyh0ai") (f (quote (("std") ("default" "std"))))))

(define-public crate-simd-adler32-0.3.0 (c (n "simd-adler32") (v "0.3.0") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rnkmh0wdalvz4ibw3n83pfq4amds2pbyslrfk4s3g4yskicgj1x") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-simd-adler32-0.3.1 (c (n "simd-adler32") (v "0.3.1") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05nlk7rxxzw4d667c68m93x6n88vksc1l3bnkxbbldd38cv8b5bn") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.2 (c (n "simd-adler32") (v "0.3.2") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1mgl03vhw4glzxmj8zspp0ksd6yk92nsr98wscqlvsq857svq21n") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.3 (c (n "simd-adler32") (v "0.3.3") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "11mdzr679dizk4ng5jsgc5p3yz6z2j8rch13www8sr8l9klz0d1d") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.4 (c (n "simd-adler32") (v "0.3.4") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "060b9v15s3miq06582cj2ywam92ph6xs34s62mc8az3xc4wxz98l") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.5 (c (n "simd-adler32") (v "0.3.5") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0bsj39d3xrqlcxjwj8i6fzh6ks38idh6b14nml8534f1fyxvz2i3") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.6 (c (n "simd-adler32") (v "0.3.6") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05nqqf8z68ac9fsaypccah6n1wjn999css55z91x4qpbqv6kabf7") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-simd-adler32-0.3.7 (c (n "simd-adler32") (v "0.3.7") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 2)) (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zkq40c3iajcnr5936gjp9jjh1lpzhy44p3dq3fiw75iwr1w2vfn") (f (quote (("std") ("nightly") ("default" "std" "const-generics") ("const-generics"))))))

