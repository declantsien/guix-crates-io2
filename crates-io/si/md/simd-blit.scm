(define-module (crates-io si md simd-blit) #:use-module (crates-io))

(define-public crate-simd-blit-1.0.0 (c (n "simd-blit") (v "1.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "0vavnvqfkngln8k8w8dnnrl7psnqdc2gx7qw4ldz56bd58cxnhnc") (f (quote (("simd") ("default"))))))

(define-public crate-simd-blit-1.0.1 (c (n "simd-blit") (v "1.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "1j6ys3bh39c28acx98lqx95q2wb7n7769a0rvr5d7bp0igfn7i8f") (f (quote (("simd") ("default"))))))

