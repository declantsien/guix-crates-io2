(define-module (crates-io si md simd-trick) #:use-module (crates-io))

(define-public crate-simd-trick-0.1.0 (c (n "simd-trick") (v "0.1.0") (h "0nkqnl8zsr3zp4rsil5js3b9zif44iwbz16xylxpsvj3zydkjk9s")))

(define-public crate-simd-trick-0.1.1 (c (n "simd-trick") (v "0.1.1") (h "0a1gjqmhx45hkp212lwrz4scv4lpgn9i9hp6h07b5vfd1irc0m7a")))

(define-public crate-simd-trick-0.2.0 (c (n "simd-trick") (v "0.2.0") (h "1grj25vm3vwr6kafpqmllra1dd8r9jpvd3s42gnal0c7qw8vxg81")))

(define-public crate-simd-trick-0.2.1 (c (n "simd-trick") (v "0.2.1") (h "17dh0slkimcf8b5qhddp3s3x0gmn4ipdx3g3xi8r2dr74fsajn8j")))

(define-public crate-simd-trick-0.3.0 (c (n "simd-trick") (v "0.3.0") (h "04mk0wkwk2p16qj7rhmp6dmky9hcjsnbwcjwfl9p93grxzigbah0")))

