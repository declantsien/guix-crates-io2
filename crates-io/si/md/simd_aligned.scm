(define-module (crates-io si md simd_aligned) #:use-module (crates-io))

(define-public crate-simd_aligned-0.1.0 (c (n "simd_aligned") (v "0.1.0") (d (list (d (n "packed_simd") (r "^0.1") (d #t) (k 0)))) (h "15f4350gri6xlhli0qjsxv73gsvq4b2b4r4j89h0lmpaanxv75cq")))

(define-public crate-simd_aligned-0.1.1 (c (n "simd_aligned") (v "0.1.1") (d (list (d (n "packed_simd") (r "^0.1") (d #t) (k 0)))) (h "0b1ikwahlgh5fp4f3ynrx16mc26c79bdjmig9xf6ldwnz2z8arxj")))

(define-public crate-simd_aligned-0.1.2 (c (n "simd_aligned") (v "0.1.2") (d (list (d (n "packed_simd") (r "^0.1") (d #t) (k 0)))) (h "0q9nvifg41nhh055k4856nh0p0gkvqk1h14mf44v6996nf0cszvc")))

(define-public crate-simd_aligned-0.1.3 (c (n "simd_aligned") (v "0.1.3") (d (list (d (n "packed_simd") (r "^0.3") (d #t) (k 0)))) (h "1r802nqigkhnk6pdfx1z59099swmycnnnbaviii4x2n412xz8vic")))

(define-public crate-simd_aligned-0.2.0 (c (n "simd_aligned") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3") (d #t) (k 0)))) (h "0whl95ngz3c0q4incs1vg0c4ggkdr653zzxwi8y3szzivjhmw3gz")))

(define-public crate-simd_aligned-0.2.1 (c (n "simd_aligned") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3") (d #t) (k 0)))) (h "0yq21gb37w3m1fdhw7dj3v0nm1jc4axrw0mj10bfwmylav37g1a0")))

(define-public crate-simd_aligned-0.4.0 (c (n "simd_aligned") (v "0.4.0") (h "1nmj96zxb6m294w9hgqn4clhjvc18m305xijli5mbzpi2vjiv5zw")))

