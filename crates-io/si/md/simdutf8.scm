(define-module (crates-io si md simdutf8) #:use-module (crates-io))

(define-public crate-simdutf8-0.0.1 (c (n "simdutf8") (v "0.0.1") (h "105xv7b0lphcg8b5x4x2cbigxfql3djf4zi8s1nldgb0pjhaayd1") (f (quote (("std") ("public_imp") ("hints") ("default" "std"))))))

(define-public crate-simdutf8-0.0.2 (c (n "simdutf8") (v "0.0.2") (h "0vm3x291qk4vv8hv4kgcgqq6q2schj13g05rj7srlj0k9pnr2h38") (f (quote (("std") ("public_imp") ("hints") ("default" "std"))))))

(define-public crate-simdutf8-0.0.3 (c (n "simdutf8") (v "0.0.3") (h "17wamapm1bcrasas43mimrpb60kgzvah20ikff1wfxczs75zcy0m") (f (quote (("std") ("public_imp") ("hints") ("default" "std"))))))

(define-public crate-simdutf8-0.1.0 (c (n "simdutf8") (v "0.1.0") (h "1ivf6q69kq3klbiadgxlp4fcgpwzz67n9793q4j64b4m8z3lp4d4") (f (quote (("std") ("public_imp") ("hints") ("default" "std"))))))

(define-public crate-simdutf8-0.1.1 (c (n "simdutf8") (v "0.1.1") (h "1c8j08fdwlnikxhfxbq78brm93agm2rs7i6ff9gcy5chbr23skvz") (f (quote (("std") ("public_imp") ("hints") ("default" "std"))))))

(define-public crate-simdutf8-0.1.2 (c (n "simdutf8") (v "0.1.2") (h "1zgimr47nv3x0yx3rxarfr00cq368m5fh7had32kbnbra5waj0ih") (f (quote (("std") ("public_imp") ("hints") ("default" "std") ("aarch64_neon"))))))

(define-public crate-simdutf8-0.1.3 (c (n "simdutf8") (v "0.1.3") (h "0nama0xqzbl8x72fqd8nhgvwjhg2vqj0gkv1la8gm0n6wwbdlw69") (f (quote (("std") ("public_imp") ("hints") ("default" "std") ("aarch64_neon"))))))

(define-public crate-simdutf8-0.1.4 (c (n "simdutf8") (v "0.1.4") (h "0fi6zvnldaw7g726wnm9vvpv4s89s5jsk7fgp3rg2l99amw64zzj") (f (quote (("std") ("public_imp") ("hints") ("default" "std") ("aarch64_neon_prefetch") ("aarch64_neon"))))))

