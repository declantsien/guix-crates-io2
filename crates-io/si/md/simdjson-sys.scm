(define-module (crates-io si md simdjson-sys) #:use-module (crates-io))

(define-public crate-simdjson-sys-0.1.0-alpha (c (n "simdjson-sys") (v "0.1.0-alpha") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0nfvv6g0c85dhi8b01y351x631z5fnqj5d0q38rsv8jk91h6nak5")))

(define-public crate-simdjson-sys-0.3.0-alpha (c (n "simdjson-sys") (v "0.3.0-alpha") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "022ljxqly0gvqx05f5vap6xc2rr0ngikjbyaifxy557jbx0nkslj")))

(define-public crate-simdjson-sys-0.1.0-alpha.1 (c (n "simdjson-sys") (v "0.1.0-alpha.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0sapd8yj3qmkn33ds1p4f8wckhlsx26rdpk8x7kl9vfl2i431cdq")))

(define-public crate-simdjson-sys-0.1.0-alpha.2 (c (n "simdjson-sys") (v "0.1.0-alpha.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1r2drq3gs4crf5j9cbh56s9mp5h50k8aki9gw3yxxnld8hzydvkg")))

