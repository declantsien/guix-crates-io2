(define-module (crates-io si md simdop) #:use-module (crates-io))

(define-public crate-simdop-0.1.0 (c (n "simdop") (v "0.1.0") (d (list (d (n "llvmint") (r "^0.0.2") (d #t) (k 0)) (d (n "simdty") (r "^0.0.3") (d #t) (k 0)))) (h "1wfh4fh6wqlg104xqqkfgjhbal4xhyb2f604qrjz5q8p6gpg1gwg")))

