(define-module (crates-io si md simdeez) #:use-module (crates-io))

(define-public crate-simdeez-0.2.0 (c (n "simdeez") (v "0.2.0") (h "14ivsxffzcy2hnv0gw3wkfngr1r74dsrh5d9ibc9970gaz0dkkjz")))

(define-public crate-simdeez-0.2.1 (c (n "simdeez") (v "0.2.1") (h "1s0avxcidy0lwkdk1axxirihv7p8m7x4yk0a33nk44q5zpvicq16")))

(define-public crate-simdeez-0.2.2 (c (n "simdeez") (v "0.2.2") (h "1xmhgvmfxl1wv5l9n50v3bvavqxrvykwa9hl8iy8ry22hs2q4nj5")))

(define-public crate-simdeez-0.2.3 (c (n "simdeez") (v "0.2.3") (h "0aa4ly5svlgdhzarq0h9mwwpzh4kcw6bpn344426npdni3xxlagb")))

(define-public crate-simdeez-0.2.4 (c (n "simdeez") (v "0.2.4") (h "09f7v1sadf1h7bzzl1dqw312w2nl1ziq10mzdh52ln9viwkk6h31")))

(define-public crate-simdeez-0.3.0 (c (n "simdeez") (v "0.3.0") (h "106jgp2pjhib5k36fkrhrx08kkzjyf7jicx8rb12909zwfbwgh0g")))

(define-public crate-simdeez-0.3.1 (c (n "simdeez") (v "0.3.1") (h "09gmlwnmfwnmng623fwiif4h1ayfyw91h84adnrmr9v864vxpvnw")))

(define-public crate-simdeez-0.4.0 (c (n "simdeez") (v "0.4.0") (h "078v1gjbyghxxmnhgjw8d38phmm607fv8bk3p07jjqplmixrg5fy")))

(define-public crate-simdeez-0.4.1 (c (n "simdeez") (v "0.4.1") (h "13zgp2rcwqmvs4x2b6m496cicj30ddnl1gvlkk9anxgy6p9iyrap")))

(define-public crate-simdeez-0.4.2 (c (n "simdeez") (v "0.4.2") (h "0qxaxws7pbsixf2g0rdqxmbb3ik2pmwvcr345mygli4wxslf9lak")))

(define-public crate-simdeez-0.5.0 (c (n "simdeez") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1nkcf0hj8sw5fp2npr0qqqq3ns28xfxly04x0vi0pw44l15malsp")))

(define-public crate-simdeez-0.5.1 (c (n "simdeez") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "02q4p0877gxkxv8kgvmf36yrjjq6jrv4z23h7c3bxp98b37d7f7i")))

(define-public crate-simdeez-0.5.2 (c (n "simdeez") (v "0.5.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1b4z3v1bh8pahiqx8q9sgrcs8hpl2h90pg8v0i58qir1w1giw35k")))

(define-public crate-simdeez-0.6.0 (c (n "simdeez") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1lxaz653ykcbcjqmq30iz81winjmq146xiaj865ypp4fsphwd3kb")))

(define-public crate-simdeez-0.6.1 (c (n "simdeez") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0dm192gra7m8w6bhjivk4vhdzs4d50hmqz6x65y5jqpiddg1vsf8")))

(define-public crate-simdeez-0.6.2 (c (n "simdeez") (v "0.6.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1jyfm7r746azfv2canwi4blws5cbxmf909ryb2yn8h7qm0xgdvcx")))

(define-public crate-simdeez-0.6.3 (c (n "simdeez") (v "0.6.3") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1iad7l275rk90qxz58qlh1hrxc9hsp2321z3a5arhk8bj8higld9")))

(define-public crate-simdeez-0.6.4 (c (n "simdeez") (v "0.6.4") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1p04jnr3cjh9jcynygi6lqzl2l72bc06yhi0vhlg8wd8n94aw122")))

(define-public crate-simdeez-0.6.5 (c (n "simdeez") (v "0.6.5") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1nvq03gi3rhw12ws3b8w1ndl0cg8lc8xkplzlyhx53vnlyva3akf")))

(define-public crate-simdeez-0.6.6 (c (n "simdeez") (v "0.6.6") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1lm7ymvsw5s0zg129vkwnggczwlpk52vv08v0nlz5vfy2mc7nm6j")))

(define-public crate-simdeez-1.0.0 (c (n "simdeez") (v "1.0.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0pwwvcxbkx5afr1nns1wag302faa5wivbwkdvgk3zsrzf5ad6658") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.1 (c (n "simdeez") (v "1.0.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1qvzb3qrkpfq15qm3wy2b4bx8qxykzpzs7qj79i43iq6wf1i8702") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.2 (c (n "simdeez") (v "1.0.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "03rim2z0bihh7w14gh00asdn8cmx39dnawk5f5lyiln7s03iik50") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.3 (c (n "simdeez") (v "1.0.3") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "00r42m5id9yjswinvrjbmkaj5bpqn9ciwb4rggppb2d7hd4dzs4v") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.4 (c (n "simdeez") (v "1.0.4") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0s89iq1fs0x850lz8522gich39z5q4haaf10g8i2g24fmbmr0m7r") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.5 (c (n "simdeez") (v "1.0.5") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0jc1nvz81hkl785bgzjsarx2agi6izq5kmda4fxixyzknf3w4zlk") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.6 (c (n "simdeez") (v "1.0.6") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0zb6b7zkxra5dsxqh9amw1p88czb3mp9n8k3kf426kk9vfwa94qq") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.7 (c (n "simdeez") (v "1.0.7") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "11q8ymcd133f1llnbsgiy7mgdckrza5wii0806f5xfm4zng9acj0") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-1.0.8 (c (n "simdeez") (v "1.0.8") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0qjnfi2mnmrhrr4d2jidk4dmmjsfr4pzz12anpjfw5z73f78kv7n") (f (quote (("sleef" "sleef-sys"))))))

(define-public crate-simdeez-2.0.0-dev1 (c (n "simdeez") (v "2.0.0-dev1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0jpw019an9271l3zk20mhvq3b5p8ijqickdg72b56nlc5md089ck") (f (quote (("sleef" "sleef-sys") ("default")))) (s 2) (e (quote (("no_std" "dep:libm"))))))

(define-public crate-simdeez-2.0.0-dev2 (c (n "simdeez") (v "2.0.0-dev2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0hawl15zsjkhw2ycw4maxdspm817h3sy79ryv3fyjpiq5b8556vl") (f (quote (("sleef" "sleef-sys") ("default")))) (s 2) (e (quote (("no_std" "dep:libm"))))))

(define-public crate-simdeez-2.0.0-dev3 (c (n "simdeez") (v "2.0.0-dev3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sleef-sys") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0gwxwf53n416zjpqvzmj1v8mhzmjdzajl31m6zdlllsfp3s3qgph") (f (quote (("sleef" "sleef-sys") ("default")))) (s 2) (e (quote (("no_std" "dep:libm"))))))

