(define-module (crates-io jt d- jtd-derive) #:use-module (crates-io))

(define-public crate-jtd-derive-0.1.0 (c (n "jtd-derive") (v "0.1.0") (d (list (d (n "jtd-derive-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "05h4wg1m5mrdxqvrj5skwyjq7b5m0r0l2sdsiax5dc8spyvnn55z")))

(define-public crate-jtd-derive-0.1.1 (c (n "jtd-derive") (v "0.1.1") (d (list (d (n "jtd-derive-macros") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0d6zfrmk6a22fgrfi59xs224fzr4m6b0hwc7mc8xnjkzfq6f4f6w")))

(define-public crate-jtd-derive-0.1.2 (c (n "jtd-derive") (v "0.1.2") (d (list (d (n "jtd-derive-macros") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "08bxm6a0sa4r0fkgd22l4c9g7pijbk1y7j0d3mi1hdiabj195wvx") (y #t)))

(define-public crate-jtd-derive-0.1.3 (c (n "jtd-derive") (v "0.1.3") (d (list (d (n "jtd-derive-macros") (r "=0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "1sqw97v5cmyf2zcdk9v3p34ykk4333ap2k524bdcn0qm2p4y25q2")))

(define-public crate-jtd-derive-0.1.4 (c (n "jtd-derive") (v "0.1.4") (d (list (d (n "jtd-derive-macros") (r "=0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "10lli801i4yn7vlfnzvn4rs3v1z206p0zck19372v4i2qvfh8b8x")))

