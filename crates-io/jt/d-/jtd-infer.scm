(define-module (crates-io jt d- jtd-infer) #:use-module (crates-io))

(define-public crate-jtd-infer-0.1.0 (c (n "jtd-infer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jtd") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vhz4wk6dc3nvzy0i4qws58kh43lfibw9sri0qx55zakrpp8wa3h")))

(define-public crate-jtd-infer-0.2.0-test.2 (c (n "jtd-infer") (v "0.2.0-test.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "jtd") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01zgizk8j32y5kg25bbi2n712ddb03yvmjyjys1scnyffva9rgfn")))

(define-public crate-jtd-infer-0.2.1 (c (n "jtd-infer") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "jtd") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03qk5mnxb62nkyfbvs5vdyy7h82im42y35v7cd53lgjah8c7hp83")))

