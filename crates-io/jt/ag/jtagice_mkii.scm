(define-module (crates-io jt ag jtagice_mkii) #:use-module (crates-io))

(define-public crate-jtagice_mkii-0.1.0 (c (n "jtagice_mkii") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0zbhlrq0k1p0j633isy5sjiszdn3257g7a9v7wwrmbasfass0ljs")))

(define-public crate-jtagice_mkii-0.1.1 (c (n "jtagice_mkii") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "02xsdgz1mnrjpfrknmfx30f84gxm28512phss7r2y65h3ajll2rh")))

