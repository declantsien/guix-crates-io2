(define-module (crates-io jt ag jtag) #:use-module (crates-io))

(define-public crate-jtag-0.0.1 (c (n "jtag") (v "0.0.1") (d (list (d (n "ftdi") (r "^0.1.3") (d #t) (k 0)) (d (n "ftdi-mpsse") (r "^0.1.0") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "0yswzkf433riki79dxips6ss4cj3bqjlm1sp6vvjxcm95ibkhkgb")))

(define-public crate-jtag-0.0.2 (c (n "jtag") (v "0.0.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ftdi") (r "^0.1.3") (d #t) (k 0)) (d (n "ftdi-mpsse") (r "^0.1.0") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "17a4129dmqjjkkbdsqpq3j87s34bd3rmsi7yv9i444w15gymd30h")))

(define-public crate-jtag-0.0.3 (c (n "jtag") (v "0.0.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "ftdi") (r "^0.1.3") (d #t) (k 0)) (d (n "ftdi-mpsse") (r "^0.1.1") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "0y07798pqij47srnv475fhn6ix5acj8yac7v0a5adckkd32zxz61")))

