(define-module (crates-io jt ag jtag-taps) #:use-module (crates-io))

(define-public crate-jtag-taps-0.1.0 (c (n "jtag-taps") (v "0.1.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)))) (h "17970bnlqdj7d3krvq7mkfii4x3v6wy8hk782s5165irmv8g48sj")))

(define-public crate-jtag-taps-0.1.1 (c (n "jtag-taps") (v "0.1.1") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)))) (h "0hmkc5vdawrdl3ccx7x38p54yy56gy5236qd9vp0j7zciai8f61y")))

(define-public crate-jtag-taps-0.2.0 (c (n "jtag-taps") (v "0.2.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)))) (h "08pmv7k48lnzywfzd2nmm7bgq6rvy31993dqf4scm5zldqv5j46s")))

(define-public crate-jtag-taps-0.2.1 (c (n "jtag-taps") (v "0.2.1") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)))) (h "0zw41z11rmskrvlmmvkif40vy1fssyrsk0743ffg5l1yxfa7wlfh")))

(define-public crate-jtag-taps-0.2.2 (c (n "jtag-taps") (v "0.2.2") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)))) (h "0q6gbmp5p7fmlc3d3lfj0ds3v3y9spppavi9kilx5yinh6nafx5m")))

(define-public crate-jtag-taps-0.2.3 (c (n "jtag-taps") (v "0.2.3") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)))) (h "0s2d3xw5d91mbzj8sm87lr9jis9x5cwvprw9vzivyhz09n6d070i")))

(define-public crate-jtag-taps-0.3.0 (c (n "jtag-taps") (v "0.3.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)))) (h "0gdniwjajm6wf80a32lf7ygm97h14ypp32zxmx5f1ir3b5384i79")))

(define-public crate-jtag-taps-0.4.0 (c (n "jtag-taps") (v "0.4.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)))) (h "1qqgr22wxy8c5cmga1jd3xk95mka5z5zkl0xvs5pb0il0g4xl0r3")))

(define-public crate-jtag-taps-0.5.0 (c (n "jtag-taps") (v "0.5.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)))) (h "14r3mf8farzgyfp2yb479j1vhxcc2xib5f272h6ljkrl83zc8rpl")))

