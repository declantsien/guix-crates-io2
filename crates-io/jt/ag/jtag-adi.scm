(define-module (crates-io jt ag jtag-adi) #:use-module (crates-io))

(define-public crate-jtag-adi-0.1.0 (c (n "jtag-adi") (v "0.1.0") (d (list (d (n "jtag-taps") (r "^0.3.0") (d #t) (k 0)))) (h "099whzvax42fsi5yrswgz5gbm598iic4pb3pffm18lw4dm23ap6n")))

(define-public crate-jtag-adi-0.2.0 (c (n "jtag-adi") (v "0.2.0") (d (list (d (n "jtag-taps") (r "^0.4") (d #t) (k 0)))) (h "1pmnjgcwdd3la44cdq39sk327cgq92brzk390x9mifsqbd6k3iba")))

(define-public crate-jtag-adi-0.3.0 (c (n "jtag-adi") (v "0.3.0") (d (list (d (n "jtag-taps") (r "^0.5") (d #t) (k 0)))) (h "0mabgi1alpg6xcdns7f2kw36hz9pbrml4qwvnrsfqixpi6g6di7x")))

