(define-module (crates-io jt ag jtagdap) #:use-module (crates-io))

(define-public crate-jtagdap-0.1.0 (c (n "jtagdap") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "rusb") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1b7gnmxyw6s9kh6w13xwcy5cwhysg0l0lhlnvz82him0iaa4dv74")))

(define-public crate-jtagdap-0.1.1 (c (n "jtagdap") (v "0.1.1") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "rusb") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "15fp5nj7nqcz23vji6kr1xggny4bvks304svylnlr2n335nlygz9")))

