(define-module (crates-io jt #{10}# jt1078) #:use-module (crates-io))

(define-public crate-jt1078-0.1.0 (c (n "jt1078") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jt808") (r "^0.1") (d #t) (k 0)) (d (n "jt_util") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec" "compat"))) (d #t) (k 0)))) (h "1kkwmw0y8ld4mp236wvpmfazsz7b7hgicjx3c52x80wy679zj7d5")))

