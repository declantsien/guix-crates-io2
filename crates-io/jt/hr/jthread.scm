(define-module (crates-io jt hr jthread) #:use-module (crates-io))

(define-public crate-jthread-0.1.0 (c (n "jthread") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fpkxji08wzz9yr930nwiz00bqgb9pp4si1kmwhwxi0sgknch5rq")))

(define-public crate-jthread-0.1.1 (c (n "jthread") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hx7wpzi4g1sjpy7s1jnbiy7vqnj3zvlrwfi3vpc9azzyf72fsy1")))

