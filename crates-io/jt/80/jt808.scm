(define-module (crates-io jt #{80}# jt808) #:use-module (crates-io))

(define-public crate-jt808-0.1.0 (c (n "jt808") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "jt_util") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec" "compat"))) (d #t) (k 0)))) (h "0hk5r01y9gcy1xkf1vb0b2vrc41sl4dsgh87vxx6dr6gw280z735")))

(define-public crate-jt808-0.1.1 (c (n "jt808") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "jt_util") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec" "compat"))) (d #t) (k 0)))) (h "021b2qgzs5lcz99hrib3mm83a9hfancspcyvsl6pccmw9kwmdgx6")))

