(define-module (crates-io jt _u jt_util) #:use-module (crates-io))

(define-public crate-jt_util-0.1.0 (c (n "jt_util") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zp8827g29hxgg0vaf1fy38x3jl2yk8pimmf6sn8lybh12h9z9zi")))

(define-public crate-jt_util-0.1.1 (c (n "jt_util") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rhrwj664dda6ha8p4p5j9bm193k9vki77kyf2gp79a7mxqc9wpi")))

