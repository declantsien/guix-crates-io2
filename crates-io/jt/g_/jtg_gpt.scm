(define-module (crates-io jt g_ jtg_gpt) #:use-module (crates-io))

(define-public crate-jtg_gpt-0.1.0 (c (n "jtg_gpt") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0gc0vwccr2gyqgbf23ms1ad3pa121wd3v6hs4w4z7swy3p83kcbc")))

(define-public crate-jtg_gpt-0.1.1 (c (n "jtg_gpt") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0nphaysvwrl9h682h9aj0bhwgxqf7rcf928c9nddzgw23lb3nizh")))

(define-public crate-jtg_gpt-0.1.2 (c (n "jtg_gpt") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1vbvn37ada7y780rg36i08rjvhzxsz28ymvkfsbwm6i1nnkxak2f")))

(define-public crate-jtg_gpt-0.1.3 (c (n "jtg_gpt") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1z3knlb3lrr46x9d7x7xs6nb1g0r99jd5xgjaxs1vryng0vabphq")))

(define-public crate-jtg_gpt-0.2.0 (c (n "jtg_gpt") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1azikgj2dq7fvdmgjbzr835mwl581bv4c2msw1qqy5ibx259yf50")))

(define-public crate-jtg_gpt-0.2.1 (c (n "jtg_gpt") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "113ivlyfidp7335fj7caxw8nyk2a72b5siggqwqpkc531skghlnp")))

(define-public crate-jtg_gpt-0.2.2 (c (n "jtg_gpt") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0s17ibxna64all6smppgiwijykzzcsi3fi491cnw86mmgg0g02h7")))

(define-public crate-jtg_gpt-0.2.3 (c (n "jtg_gpt") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "148y747fvd79s7iymivi2r6gxj2b46ri4v1aqx22s304narrh0rb")))

(define-public crate-jtg_gpt-0.2.4 (c (n "jtg_gpt") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "16fpk4121s4scgf7rn7kxrqr7xjlfipc3yilrfcmxkghb2in63q2")))

(define-public crate-jtg_gpt-0.2.5 (c (n "jtg_gpt") (v "0.2.5") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "00xlk1jh317bybzsjzcabfyw0bsml2n7xyp9ldn4c7jp4gk06pa3")))

(define-public crate-jtg_gpt-0.2.6 (c (n "jtg_gpt") (v "0.2.6") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1c1rd6cfcra47ikj5pvbfcr3v9zn3zzadik63zhsj5nzhqxiv9wx")))

(define-public crate-jtg_gpt-0.2.7 (c (n "jtg_gpt") (v "0.2.7") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0kp05bxfj8zc4z02vhsxsgngyn5pslrmhl3iindf53lb031db6vv")))

(define-public crate-jtg_gpt-0.2.8 (c (n "jtg_gpt") (v "0.2.8") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0pnyx7z5l2si05psdh8qdn2fplp7i6i5bv3d7pik92idwkky74hm")))

