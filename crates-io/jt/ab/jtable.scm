(define-module (crates-io jt ab jtable) #:use-module (crates-io))

(define-public crate-jtable-0.1.0 (c (n "jtable") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0vg93i8nxxbl13g1vvcb55r6ch1mg5yvv2vbf0mbnkkjarlmmq48")))

(define-public crate-jtable-0.1.1 (c (n "jtable") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0x0zqrpf361zfd7aa9cff48j5v98pq1yh3mdd5dpy4d6rqngvw4w")))

(define-public crate-jtable-0.2.0 (c (n "jtable") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0bgqbiidmjss7xi16g4r9d97gskk5ag1xdkgi1m2di0wvvc1w6px")))

