(define-module (crates-io jt ar jtar) #:use-module (crates-io))

(define-public crate-jtar-0.1.0 (c (n "jtar") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "145g5s44yqczlhymsw3nd8n6m7rmsk8982jnid8wlqk2r0ragiqa")))

