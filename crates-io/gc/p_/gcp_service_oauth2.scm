(define-module (crates-io gc p_ gcp_service_oauth2) #:use-module (crates-io))

(define-public crate-gcp_service_oauth2-0.1.0 (c (n "gcp_service_oauth2") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hmyidak5hsmk1ww4jry4hgwhldlgljhsx3ha105q0612i0l6wl0")))

(define-public crate-gcp_service_oauth2-0.1.1 (c (n "gcp_service_oauth2") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l6096zcbj7pv7h2grpvr2r2clzv5jxvdyfvss2syl3w3r73szw2")))

