(define-module (crates-io gc ca gccarch) #:use-module (crates-io))

(define-public crate-gccarch-0.1.0 (c (n "gccarch") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.0") (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("std" "derive"))) (k 0)) (d (n "libc") (r "^0.2.124") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "137dy59ssyw0z3vvkyac7nhj5sy109xp1r23nb4a7zjin3ikf1ng")))

(define-public crate-gccarch-0.1.1 (c (n "gccarch") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.0") (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("std" "derive"))) (k 0)) (d (n "libc") (r "^0.2.124") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19d06jck4h6hpzf95xzd8z41316q5gn68i4dal73rsw1q1z5svf6")))

