(define-module (crates-io gc mo gcmodule) #:use-module (crates-io))

(define-public crate-gcmodule-0.1.0 (c (n "gcmodule") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1ba4vlq5p09prvfmzd0pvpwrh4qm7v6l5d9kcl3m2gz8ycqh9dy2") (y #t)))

(define-public crate-gcmodule-0.1.1 (c (n "gcmodule") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1qvnpkgzdvhx3bslxm7kx4b8acy353h4xynrb11blrhyjdm3llli") (y #t)))

(define-public crate-gcmodule-0.1.2 (c (n "gcmodule") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "07xhazjpchyj25dgk68kfgmrgjb0s5mvm1x6nkmlv26v7nvkphif") (f (quote (("nightly"))))))

(define-public crate-gcmodule-0.2.1 (c (n "gcmodule") (v "0.2.1") (d (list (d (n "gcmodule_derive") (r "= 0.2.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "1xn6d4fhwl9lkrzf80ms2wh50ch209j8ch6lnx2p26y4nqlj8zll") (f (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.2.2 (c (n "gcmodule") (v "0.2.2") (d (list (d (n "gcmodule_derive") (r "= 0.2.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "1m7pr4wk2zymm4z5gnfg6xjhvnxk1rp9payxw0qp68w5s3qwrky2") (f (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.2.3 (c (n "gcmodule") (v "0.2.3") (d (list (d (n "gcmodule_derive") (r "= 0.2.3") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "1h1rk8pipwqx74zcnbiyqr4vvgxyhyhx43mylj67n42dsr0c50ij") (f (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.3.0 (c (n "gcmodule") (v "0.3.0") (d (list (d (n "gcmodule_derive") (r "= 0.2.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "0zvdgh32qpz07sl60slyzcz4gnqf8z0kcrqg8r01965min7bw0v7") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3.1 (c (n "gcmodule") (v "0.3.1") (d (list (d (n "gcmodule_derive") (r "= 0.2.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "06p5vyly0b4qmlzciqgnf5644gzymlnj73pz0dkffy37rlfw13yk") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3.2 (c (n "gcmodule") (v "0.3.2") (d (list (d (n "gcmodule_derive") (r "= 0.3.2") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "0f8fiizv92hhwp4drrhfj814djxsa6c7q1xdnnn2l10zhz6b5p32") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3.3 (c (n "gcmodule") (v "0.3.3") (d (list (d (n "gcmodule_derive") (r "= 0.3.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "09gdn29n3k2gz3la6nzrwhzqs2m26r4wf7s09bs28wa33agkx95f") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync") ("debug"))))))

