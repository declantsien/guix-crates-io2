(define-module (crates-io gc mo gcmodule_derive) #:use-module (crates-io))

(define-public crate-gcmodule_derive-0.2.1 (c (n "gcmodule_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p6k223snbixm1whii4zrggbyi866pwx103c8navm0in8s1nxyw1")))

(define-public crate-gcmodule_derive-0.2.2 (c (n "gcmodule_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17x1a08w2hqz7z92x1sl0hynlvpkkzf6lhdw8qpnw190nfscrsr0")))

(define-public crate-gcmodule_derive-0.2.3 (c (n "gcmodule_derive") (v "0.2.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zi5ah05jhcmshx59mvjn6whs3c4bnpba047bxiyqs7ycwjwki79")))

(define-public crate-gcmodule_derive-0.3.2 (c (n "gcmodule_derive") (v "0.3.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yqdvwx4smziyf9mam8b806rflv9818xihlmkjj5ric2wxpr0xcj")))

(define-public crate-gcmodule_derive-0.3.3 (c (n "gcmodule_derive") (v "0.3.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bhq08ywp332gccb6rf6z9bmbi869fx4jqxsixd2qcbm7p3n8mq")))

