(define-module (crates-io gc ac gcache) #:use-module (crates-io))

(define-public crate-gcache-0.0.1 (c (n "gcache") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0g56lgwr9k3mvi6hidps9sxr1gylh1brl8qcldf6kw93l2xh16v6")))

