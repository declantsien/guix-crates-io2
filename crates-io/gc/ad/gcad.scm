(define-module (crates-io gc ad gcad) #:use-module (crates-io))

(define-public crate-gcad-0.1.0 (c (n "gcad") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (f (quote ("const_prec_climber"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "13g4sd953imlncnlczqjkfqamm0nl3c8vn4zzm0w9csppggjl398")))

(define-public crate-gcad-0.2.0 (c (n "gcad") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libgcad") (r "^0.2.0") (d #t) (k 0)))) (h "0fc5arx0h82swyahf0lv1fc9lhwjh7v6sxy33i809cd1znzb69kx")))

(define-public crate-gcad-0.2.1 (c (n "gcad") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libgcad") (r "^0.2.1") (d #t) (k 0)))) (h "10m1hg0c8wvxjlsbq3dkgci8732p4scgdq2f6kfl28dyp1q8bvnq")))

(define-public crate-gcad-0.2.2 (c (n "gcad") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libgcad") (r "^0.2.2") (d #t) (k 0)))) (h "1dl65n5l5aws9sdrnxq1ckx9nz7cilhglmbwr5dflqkzfxyqxmcn")))

(define-public crate-gcad-0.3.0 (c (n "gcad") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libgcad") (r "^0.3.0") (d #t) (k 0)))) (h "1w6s0jqkjs8n60m384dpxn18z8jcyk4wj9grn94386y6y9qn2wrj")))

