(define-module (crates-io gc ad gcad_proc_macros) #:use-module (crates-io))

(define-public crate-gcad_proc_macros-0.2.0 (c (n "gcad_proc_macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (f (quote ("const_prec_climber"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1j65nx6jf87vjs043h7r5p9c1fp4jam08ljhkp1k3cska54ga42y")))

(define-public crate-gcad_proc_macros-0.3.0 (c (n "gcad_proc_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "165jl93cawxdvf34rl5vin1wxf7k03xrbc1pa3pc1jbkiy8ll51w")))

