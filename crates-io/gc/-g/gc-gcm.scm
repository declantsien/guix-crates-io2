(define-module (crates-io gc -g gc-gcm) #:use-module (crates-io))

(define-public crate-gc-gcm-0.8.0 (c (n "gc-gcm") (v "0.8.0") (d (list (d (n "binread") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xa2zasrnp9q2jvzkgh4f4s6ffbij7r36cx9n3xwsl2ls4n2424p") (f (quote (("bin" "structopt" "memmap" "rayon"))))))

(define-public crate-gc-gcm-0.9.0 (c (n "gc-gcm") (v "0.9.0") (d (list (d (n "binread") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vaw4x0kn2bisyl48ljm3pip4xz0g8c9yn630hwy5fcmnnkzzk4w") (f (quote (("no_std") ("bin" "structopt" "memmap" "rayon"))))))

(define-public crate-gc-gcm-0.9.1 (c (n "gc-gcm") (v "0.9.1") (d (list (d (n "binread") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "149i06qyfcr8ly4cv4lh00h5rlsz56mv7zqf1v0hab9afmsb9kmy") (f (quote (("no_std") ("bin" "structopt" "memmap" "rayon"))))))

(define-public crate-gc-gcm-0.10.0 (c (n "gc-gcm") (v "0.10.0") (d (list (d (n "binread") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "104z4lr4g2adfkd670fzh07y67mlyj6slk09v6gywg9gjdfkk5gc") (f (quote (("no_std") ("bin" "structopt" "memmap" "rayon"))))))

