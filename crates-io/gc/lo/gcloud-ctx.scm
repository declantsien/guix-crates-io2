(define-module (crates-io gc lo gcloud-ctx) #:use-module (crates-io))

(define-public crate-gcloud-ctx-0.1.0 (c (n "gcloud-ctx") (v "0.1.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_ini") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l3rhvd8mc9j0ifv3ld5j4pyc1xq39qbccvzk67brkypkylpix31")))

(define-public crate-gcloud-ctx-0.2.0 (c (n "gcloud-ctx") (v "0.2.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_ini") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01j16sssa4ckm2f4d5iyd4fhhlxqac92zkszg572xa0zcq17dh8q")))

(define-public crate-gcloud-ctx-0.3.0 (c (n "gcloud-ctx") (v "0.3.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_ini") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0130mhnlsvifrljla833vh5dxqj72753rip23v91viznf0ba2lqa")))

(define-public crate-gcloud-ctx-0.4.0 (c (n "gcloud-ctx") (v "0.4.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_ini") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mzc5z0klqqw9z33ywgxyw9by1k78xv1w88m2nfchfmyvhz1xkjc")))

