(define-module (crates-io gc lo gclog) #:use-module (crates-io))

(define-public crate-gclog-0.2.1 (c (n "gclog") (v "0.2.1") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "histogram") (r "^0.6.9") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "19vai4qyhwf8qsmgdb5g97f7yhhxhrx28wjhblyjnkq36zfcim7w")))

