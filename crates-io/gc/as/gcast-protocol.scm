(define-module (crates-io gc as gcast-protocol) #:use-module (crates-io))

(define-public crate-gcast-protocol-0.1.1 (c (n "gcast-protocol") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "gcast-wire") (r "^0.1.1") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1idria1raz794ianl6kwk57kkr6n6syf5zjzkb6a2flgqxfccqm6")))

(define-public crate-gcast-protocol-0.1.2 (c (n "gcast-protocol") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "gcast-wire") (r "^0.1.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0ijcaag3lfprrijcyhnrpdjxfgmb2gsrwpbilb6w7xm7jwkjr5bs")))

(define-public crate-gcast-protocol-0.1.3 (c (n "gcast-protocol") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "gcast-wire") (r "^0.1.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0nmszzdw2yykdbxkzq38sbysymj2x637xqn3c5p886hp0hhs4kac")))

(define-public crate-gcast-protocol-0.1.4 (c (n "gcast-protocol") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "gcast-wire") (r "^0.1.4") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "00p1i6z60a18hyb8lna5xwlv7ldxql60alay76n5pyj4g56zdzjs")))

(define-public crate-gcast-protocol-0.1.5 (c (n "gcast-protocol") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "gcast-wire") (r "^0.1.5") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0a036h3l9xy3abrm1ni14r2ngiisbc5621njh7qk7w55yb6m7s3v")))

