(define-module (crates-io gc as gcast-wire) #:use-module (crates-io))

(define-public crate-gcast-wire-0.1.1 (c (n "gcast-wire") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "01amv5dyr9sxla7mv7k2xlaakcb1a2wmgbyyzb48gxa429ffn3w9")))

(define-public crate-gcast-wire-0.1.2 (c (n "gcast-wire") (v "0.1.2") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "177knkbzs0nk241xpqcvkd4wi95166mxsx5mhxfgxpmkdzfdz5d0")))

(define-public crate-gcast-wire-0.1.3 (c (n "gcast-wire") (v "0.1.3") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1kgclfz0x5z3yxfpa62jwlisfpmy38zlbvr8x639n0341xlmabiq")))

(define-public crate-gcast-wire-0.1.4 (c (n "gcast-wire") (v "0.1.4") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "0a3nmzbf27vk7d9bsniy6hhbrlfd1jk4yv7451cd3v88s764f9am")))

(define-public crate-gcast-wire-0.1.5 (c (n "gcast-wire") (v "0.1.5") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1ybq4cj8i785wnsyd2s9msfy12f1cv2c4hmrbw3hlav1184wlqf1")))

