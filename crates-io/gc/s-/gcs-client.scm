(define-module (crates-io gc s- gcs-client) #:use-module (crates-io))

(define-public crate-gcs-client-0.1.0 (c (n "gcs-client") (v "0.1.0") (d (list (d (n "actix-web") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "bytes") (r ">=0.5.6, <0.6.0") (d #t) (k 0)) (d (n "chrono") (r ">=0.4.10, <0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0256isfh7344i239ld0rqf72vc2956xhpxv17hc19lmg3rdfd744") (y #t)))

