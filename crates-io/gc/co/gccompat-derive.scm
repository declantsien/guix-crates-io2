(define-module (crates-io gc co gccompat-derive) #:use-module (crates-io))

(define-public crate-gccompat-derive-0.1.0 (c (n "gccompat-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0fgpn7zqsgw4fan7k73kd3hq3cis4jnk08f8gn5vh38i7x9722y6")))

(define-public crate-gccompat-derive-0.1.1 (c (n "gccompat-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1bidhxdfk8b8m7w01l860s4b9xy19j8qiw4q0ldwn7iagqrkwacm")))

(define-public crate-gccompat-derive-0.1.2 (c (n "gccompat-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0x1snvbndi3blfp1g0wi1yi462qccmcc5nk01y19y083yrw7hd49")))

