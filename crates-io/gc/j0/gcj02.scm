(define-module (crates-io gc j0 gcj02) #:use-module (crates-io))

(define-public crate-gcj02-0.1.0 (c (n "gcj02") (v "0.1.0") (d (list (d (n "geo") (r "^0.24.0") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)))) (h "15kcg91jxgd38blzd07psw2sypiwij2k3y5ni1gq8afw2lj9cg0l")))

