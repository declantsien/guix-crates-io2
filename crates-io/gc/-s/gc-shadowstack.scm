(define-module (crates-io gc -s gc-shadowstack) #:use-module (crates-io))

(define-public crate-gc-shadowstack-0.1.0 (c (n "gc-shadowstack") (v "0.1.0") (d (list (d (n "mopa") (r "^0.2.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0li0daqiadj3gp24c4zlhipgnx91ynbw17nqp9ln666lf5igzasr")))

(define-public crate-gc-shadowstack-1.0.0 (c (n "gc-shadowstack") (v "1.0.0") (d (list (d (n "mopa") (r "^0.2.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1jrqrl30anq6adlbn27v1qkw1f3n5kcf75yg1ws1rgg0dlhcms70")))

(define-public crate-gc-shadowstack-1.1.0 (c (n "gc-shadowstack") (v "1.1.0") (d (list (d (n "mopa") (r "^0.2.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1dm3lnby2hb7hycv5bckmj5w30xxzgc871wbm79hz1y569sray7z") (y #t)))

(define-public crate-gc-shadowstack-1.1.1 (c (n "gc-shadowstack") (v "1.1.1") (d (list (d (n "mopa") (r "^0.2.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1w7ar5l3p07r853kg51n3w8pg2ib5799k5l78h65324f3cmj16ya")))

