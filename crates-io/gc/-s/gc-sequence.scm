(define-module (crates-io gc -s gc-sequence) #:use-module (crates-io))

(define-public crate-gc-sequence-0.1.0 (c (n "gc-sequence") (v "0.1.0") (d (list (d (n "gc-arena") (r "^0.1") (d #t) (k 0)))) (h "02wz0frbzj23s0q41innys07qifaq9jsn7yssd308gk2wsaf2k4y")))

(define-public crate-gc-sequence-0.1.1 (c (n "gc-sequence") (v "0.1.1") (d (list (d (n "gc-arena") (r "^0.1") (d #t) (k 0)))) (h "0dgk58vy8c679ffjlvgai61w0vr6cmdm5a2vw7im3ria9j5bcmdb")))

(define-public crate-gc-sequence-0.2.0 (c (n "gc-sequence") (v "0.2.0") (d (list (d (n "gc-arena") (r "^0.2") (d #t) (k 0)))) (h "1qhq3gmmcd8j84lyr566asl4nr3pm49h52sc1bxrgmy7wjdbhx8n")))

(define-public crate-gc-sequence-0.2.1 (c (n "gc-sequence") (v "0.2.1") (d (list (d (n "gc-arena") (r "^0.2") (d #t) (k 0)))) (h "0apx9spj1skq4bwh5fvb3h1gkxrf9ng3adlmcvm3akpaaqyll4b8") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-sequence-0.2.2 (c (n "gc-sequence") (v "0.2.2") (d (list (d (n "gc-arena") (r "^0.2.2") (d #t) (k 0)))) (h "0kwl9dbihwisv18fg5byw1rjqgrjwpdg3wjz6hrlh8gays3cvr6r") (f (quote (("std") ("default" "std"))))))

