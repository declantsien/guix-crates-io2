(define-module (crates-io gc #{-i}# gc-image) #:use-module (crates-io))

(define-public crate-gc-image-0.0.1 (c (n "gc-image") (v "0.0.1") (d (list (d (n "encoding_rs") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0i0gq6vrhyv32n8qgg3yhv3mbxip0qzwvjl7mmbyffkx2hc3ilhy") (y #t)))

(define-public crate-gc-image-0.0.2 (c (n "gc-image") (v "0.0.2") (d (list (d (n "encoding_rs") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0gjrb9rir46cn3540ngljsdc0xphc1crbgamrjkkzyqwz2ynnfl5")))

