(define-module (crates-io gc _d gc_derive) #:use-module (crates-io))

(define-public crate-gc_derive-0.2.0 (c (n "gc_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.2") (d #t) (k 0)))) (h "0cvf54hvfr2yvplhn1im85hidi2xksiiysqw54q9jnm8cp5chypx")))

(define-public crate-gc_derive-0.2.1 (c (n "gc_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.4") (d #t) (k 0)))) (h "011fch698q5l37lygmp7ln9m7pir22j5sqbsai1mc0hpxh2r77hg")))

(define-public crate-gc_derive-0.3.0 (c (n "gc_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.4") (d #t) (k 0)))) (h "19syhpy3i9nvnlhdsdxkbxidj4x0ia09qacj71wcpx8359fwbf23")))

(define-public crate-gc_derive-0.3.1 (c (n "gc_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "1xlkc3jbyb988qdc8dyk0lpxg2yi7mc1i6nvm9kkm9cphxxksljd")))

(define-public crate-gc_derive-0.3.2 (c (n "gc_derive") (v "0.3.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)))) (h "19hf9qv5bj3irpk7z4ypc4qr6alqa59shyk12hi0r2pjp9fc2095")))

(define-public crate-gc_derive-0.3.4 (c (n "gc_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "04wkqgj2w9g6v6mlfs38sq1gbjw9fvjjv9bi4spik8a4h269cnva")))

(define-public crate-gc_derive-0.3.5 (c (n "gc_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1f39r2zhdrlwxvclciy76xpkn2984iba959nsb46xz5j8k3zqkq3")))

(define-public crate-gc_derive-0.3.6 (c (n "gc_derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0lb1x02jh8bxi84pi3nsysjcj271qvbkrj221v3zqlqx9nwk9isj")))

(define-public crate-gc_derive-0.4.0 (c (n "gc_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0p31d81xnq0pwn99wm1cvvq7p8kqvrfl4g3g6xq784iz085vpvv5")))

(define-public crate-gc_derive-0.4.1 (c (n "gc_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0hdr1m8f8mk2n2msm3d9c4ikr36qny7fg00xcf2pizwly1289pv0")))

(define-public crate-gc_derive-0.5.0 (c (n "gc_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "07pnbg2bkqr8b3apwzy11gy395ham38vlr08yq32i2v5d46iwvzv")))

