(define-module (crates-io gc cj gccjit_sys) #:use-module (crates-io))

(define-public crate-gccjit_sys-0.0.1 (c (n "gccjit_sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "14d2fxl9jkdshi8lgcx3sxq3lqb34nn9sxm4ykpd8nkwqblz4y8g")))

(define-public crate-gccjit_sys-0.1.0 (c (n "gccjit_sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n271fswii6clh5ffn3wgdk3mpckaqz49ygjc4ka1w85l3xncsj0") (f (quote (("master"))))))

