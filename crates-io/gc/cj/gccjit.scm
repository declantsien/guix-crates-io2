(define-module (crates-io gc cj gccjit) #:use-module (crates-io))

(define-public crate-gccjit-0.0.1 (c (n "gccjit") (v "0.0.1") (d (list (d (n "gccjit") (r "^0.0.1") (d #t) (k 0)))) (h "0kysb9hwhqaw8gs72lb9nh0d90nc2cy9acaalzxbp1aaivh5ji9g") (y #t)))

(define-public crate-gccjit-0.0.2 (c (n "gccjit") (v "0.0.2") (d (list (d (n "gccjit_sys") (r "^0.0.1") (d #t) (k 0)))) (h "12ibmi493f8g33affm1wkxc4z53w006yzfn96l928d6l5p65zqln")))

(define-public crate-gccjit-1.0.0 (c (n "gccjit") (v "1.0.0") (d (list (d (n "gccjit_sys") (r "^0.0.1") (d #t) (k 0)))) (h "1p2pkcypjs9nggyywab8qmayn3fijnmn0dq9ym67b5ibx3c49xj1")))

(define-public crate-gccjit-2.0.0 (c (n "gccjit") (v "2.0.0") (d (list (d (n "gccjit_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1khpchi4vc5k2hk8x7d9v4i0a7db95fzgbsg3fcilk6pl8ylrapc") (f (quote (("master" "gccjit_sys/master"))))))

