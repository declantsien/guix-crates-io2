(define-module (crates-io gc om gcom) #:use-module (crates-io))

(define-public crate-gcom-0.1.0 (c (n "gcom") (v "0.1.0") (d (list (d (n "inquire") (r "^0.6.2") (f (quote ("editor"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1yvlzb5bb2c71dpwc13rzjh5bsnd256l8z1zrx7gqgmlq59vn2m3")))

