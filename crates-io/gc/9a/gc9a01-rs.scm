(define-module (crates-io gc #{9a}# gc9a01-rs) #:use-module (crates-io))

(define-public crate-gc9a01-rs-0.1.0 (c (n "gc9a01-rs") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "10sl9qfb8vgfyllqx9mnhwyylm482qlk52k9m38l1vzlycqzxviy") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-gc9a01-rs-0.2.0 (c (n "gc9a01-rs") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1n50mc5xxvs714837pk1cd948dp1w5vpy1fiarw0qjz9ziybmdnq") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-gc9a01-rs-0.2.1 (c (n "gc9a01-rs") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1nc7i963qa2jlg8d0bah5qzpi72imcksk7pghjkncxs4qhbn702v") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

