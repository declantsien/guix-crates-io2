(define-module (crates-io gc #{9a}# gc9a01) #:use-module (crates-io))

(define-public crate-gc9a01-0.1.0 (c (n "gc9a01") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-probe") (r "^0.3") (f (quote ("print-defmt"))) (d #t) (k 0)))) (h "170x6jrrms7z4hx22lpv7hq04b32h54n6r3x1mm5jmfjyds4dwlc")))

(define-public crate-gc9a01-0.2.0 (c (n "gc9a01") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-probe") (r "^0.3") (f (quote ("print-defmt"))) (d #t) (k 0)))) (h "122gzqvw2ny8qwxvma9mh3a2m9qc3j33jc5z1ihs0bcv4f7z2xa3")))

