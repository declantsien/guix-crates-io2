(define-module (crates-io gc _a gc_api) #:use-module (crates-io))

(define-public crate-gc_api-0.1.0 (c (n "gc_api") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "0fvpp10ympigcqjlfbs6blkq3fwwpy1badsnbbpy9s0mz2x32kip")))

(define-public crate-gc_api-0.3.0 (c (n "gc_api") (v "0.3.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "0hxp5lny4p8xz9mqfh7md9wp217vrhs3xzlsj70k4msy2y45fc3y")))

(define-public crate-gc_api-0.4.0 (c (n "gc_api") (v "0.4.0") (d (list (d (n "lock_api") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1fkl3dc6c0qba4ancip2whxfjsd33ncpp9zqf89gzq1pa1321qap")))

(define-public crate-gc_api-0.5.0 (c (n "gc_api") (v "0.5.0") (d (list (d (n "lock_api") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0my76fy2irz2r6i0rqclgp61p49qp5bp07svamxblr5l4ik5d36a")))

