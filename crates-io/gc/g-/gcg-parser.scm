(define-module (crates-io gc g- gcg-parser) #:use-module (crates-io))

(define-public crate-gcg-parser-0.1.0 (c (n "gcg-parser") (v "0.1.0") (h "1j93h0c55rv3c4j3sc6h59yx0g8c7aqy0ya349cx7hdl1wl1l5kn")))

(define-public crate-gcg-parser-0.1.1 (c (n "gcg-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1s1idl4r61hindmqi3alda95axjxqmdc0p703qmzid9658iw66w3")))

(define-public crate-gcg-parser-0.1.2 (c (n "gcg-parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1j3ypka85rgah4086p5g1gcszzziahmdqgz2xd7kr314mlr9fc77")))

(define-public crate-gcg-parser-0.1.3 (c (n "gcg-parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1a33pnqig6lpz6g79c7n9xnzg7sn6r96d9h4wfnbqkg86izgp9vw")))

(define-public crate-gcg-parser-0.1.4 (c (n "gcg-parser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0vz50qwv7bykpfvrnmabbylnmwrllim9k35c2p9r3flkg1y1z017")))

(define-public crate-gcg-parser-0.2.0 (c (n "gcg-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0d4i42wi4mdjmhglwmz13n95l8q01fd5v5f83jm9zcjif6znhmq1")))

(define-public crate-gcg-parser-0.3.0 (c (n "gcg-parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fdxvsd76xab43zb5fwwcjv8xy96b5fyf82c9a78svj0dl033rqy")))

(define-public crate-gcg-parser-0.4.0 (c (n "gcg-parser") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1m3xs0ymch60wgb6k049a4smn9py3nnsffkr20hx994rx7m1yy4w")))

(define-public crate-gcg-parser-0.5.0 (c (n "gcg-parser") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "11wprdf0n7363mi23lgyvkg6bh3n043gni9c00xr81wrijm8fcsz")))

(define-public crate-gcg-parser-0.5.1 (c (n "gcg-parser") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0qj6c1x3nwr58v8l4zrbva2jqqshbc2xsv8crml315pxab5jch8g")))

