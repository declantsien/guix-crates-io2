(define-module (crates-io gc he gchemol-lattice) #:use-module (crates-io))

(define-public crate-gchemol-lattice-0.0.4 (c (n "gchemol-lattice") (v "0.0.4") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "gchemol-gut") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.0.8") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1r402an39q4nfmw8vkv9mqg8n6s6zj1b2v1ql09v61pccf6v0g4f") (f (quote (("adhoc"))))))

(define-public crate-gchemol-lattice-0.0.5 (c (n "gchemol-lattice") (v "0.0.5") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "gchemol-gut") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.0.8") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "09wir8953sh4a5x172vdzgdzdhvzcdiv37mbwc5gnb8knifdw1ky") (f (quote (("adhoc"))))))

(define-public crate-gchemol-lattice-0.0.6 (c (n "gchemol-lattice") (v "0.0.6") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "gchemol-gut") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.0.8") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1yralq9ydn0wc0jlgcb4i58x3r3yw3fvw5qygvcrbiq3g5qhg6ka") (f (quote (("adhoc"))))))

(define-public crate-gchemol-lattice-0.1.0 (c (n "gchemol-lattice") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "gchemol-gut") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1.2") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1ykn9f1di3vzfa8qci989k5pzcsiirjimsg72xqycx8j7ngs3f70") (f (quote (("adhoc"))))))

(define-public crate-gchemol-lattice-0.1.1 (c (n "gchemol-lattice") (v "0.1.1") (d (list (d (n "gchemol-gut") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1.2") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "04l1bsb2fkf8a68b56wcv1qcypsz2vjl9fxmr6hcyd7mq6rgdr7q") (f (quote (("adhoc"))))))

(define-public crate-gchemol-lattice-0.1.2 (c (n "gchemol-lattice") (v "0.1.2") (d (list (d (n "gchemol-gut") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1.2") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1px370n4v0vjn6jbmxkwxg9dql5hcmb5s5dkdic91l1riaf6vqgp") (f (quote (("adhoc"))))))

