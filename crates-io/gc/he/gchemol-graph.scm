(define-module (crates-io gc he gchemol-graph) #:use-module (crates-io))

(define-public crate-gchemol-graph-0.0.3 (c (n "gchemol-graph") (v "0.0.3") (d (list (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c2g94ifn6ffv248xj5x3wxl9b592vbd63bhplyi61b1y2h1357w") (y #t)))

(define-public crate-gchemol-graph-0.0.4 (c (n "gchemol-graph") (v "0.0.4") (d (list (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jvhzjhlbb4kmpwln2wga6x4q7yj4mypjva5zzzmz1vin7q686q4") (y #t)))

(define-public crate-gchemol-graph-0.1.0 (c (n "gchemol-graph") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i39v987y74vzfkvyfv1la4mmxfz75dnbifw35limf9z3dxzvi30") (f (quote (("adhoc"))))))

(define-public crate-gchemol-graph-0.1.1 (c (n "gchemol-graph") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w2595x2gkbpzz5mzgc5jhgrsjqr548a9j3kx0y32jp8yfafpz9z") (f (quote (("adhoc"))))))

(define-public crate-gchemol-graph-0.1.2 (c (n "gchemol-graph") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hdqmk6ryq6klzdlgd14l469fza1rwzyb78mz5vqcp9x2d6nd3gd") (f (quote (("adhoc"))))))

(define-public crate-gchemol-graph-0.1.3 (c (n "gchemol-graph") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json_any_key") (r "^2.0.0") (d #t) (k 0)))) (h "0yzqz9kjnk6fs1l20hby5sw5j0lk1ry1x9n30yn88fn4gbhxna0x") (f (quote (("adhoc")))) (y #t)))

(define-public crate-gchemol-graph-0.1.4 (c (n "gchemol-graph") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13bikrrhq7mr1p1542pmgfwazh7yw41ql44sc8zvdb6vdpj0qhd9") (f (quote (("adhoc"))))))

(define-public crate-gchemol-graph-0.1.5 (c (n "gchemol-graph") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gzql8qjc563v0d7x654cn4dvzl8g9bgf8dhjwrh5c680v6m6cvz") (f (quote (("adhoc"))))))

(define-public crate-gchemol-graph-0.1.6 (c (n "gchemol-graph") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7a97gx7fzl4mwydn2jpvwyrmn7m2sgm3hnwkg7r5gaj2nxpb1a") (f (quote (("adhoc"))))))

