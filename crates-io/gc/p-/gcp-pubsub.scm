(define-module (crates-io gc p- gcp-pubsub) #:use-module (crates-io))

(define-public crate-gcp-pubsub-0.1.0 (c (n "gcp-pubsub") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "goauth") (r "^0.6.0-beta1") (d #t) (k 0)) (d (n "nanoid") (r "^0.2.0") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "smpl_jwt") (r "^0.4.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.2") (f (quote ("native-client"))) (d #t) (k 0)))) (h "0ychs8b6bbgr455h1dgdbz460z8z2i8hp8qbp4njfgpknqfz8ppm")))

