(define-module (crates-io gc p- gcp-vertex-ai-generative-ai) #:use-module (crates-io))

(define-public crate-gcp-vertex-ai-generative-ai-0.1.0 (c (n "gcp-vertex-ai-generative-ai") (v "0.1.0") (d (list (d (n "gcp-vertex-ai-generative-language") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0yvprjaqg46qnvnyfi7jlvyv9sx977mniml5krrmhdm6sqsbvpv6")))

