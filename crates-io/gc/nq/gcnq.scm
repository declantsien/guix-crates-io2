(define-module (crates-io gc nq gcnq) #:use-module (crates-io))

(define-public crate-gcnq-0.0.1 (c (n "gcnq") (v "0.0.1") (h "1y17lrmgjdq0b3mlgvdr0mia7k6hmj3nrr6zvk6pqn1wk2bhlnnw")))

(define-public crate-gcnq-0.0.2 (c (n "gcnq") (v "0.0.2") (h "1blw31ncbak4wgmmjfl5n5vymcgmmln569yfxqiz5r9pwm3pdwbm")))

(define-public crate-gcnq-0.0.3 (c (n "gcnq") (v "0.0.3") (h "1j2r89r3awyx7cifqr68vzch5n0q2ng59vanprss55b4sv1ypwfk")))

