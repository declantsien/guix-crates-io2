(define-module (crates-io gc #{91}# gc9106) #:use-module (crates-io))

(define-public crate-gc9106-0.9.0 (c (n "gc9106") (v "0.9.0") (d (list (d (n "embedded-graphics") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "13akm793ycqb6fllpw7w8rrmi63558qs4ffnjnalq5dzmp8by1yn") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

