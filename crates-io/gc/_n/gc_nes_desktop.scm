(define-module (crates-io gc _n gc_nes_desktop) #:use-module (crates-io))

(define-public crate-gc_nes_desktop-0.1.0 (c (n "gc_nes_desktop") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "gc_nes_core") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "minifb") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1f5bqa74ldcihysgjbcqj9yfhyzs7zimh9ml0lrz5yfkpmd2yp24")))

