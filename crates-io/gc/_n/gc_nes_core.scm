(define-module (crates-io gc _n gc_nes_core) #:use-module (crates-io))

(define-public crate-gc_nes_core-0.1.0 (c (n "gc_nes_core") (v "0.1.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "emulator_6502") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)))) (h "13b8pndrg571lh9nk6fyrfzld1lybxmp0w5q64r6fjy4lah2cfrk") (f (quote (("web-frame-format") ("default"))))))

