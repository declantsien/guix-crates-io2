(define-module (crates-io gc nd gcnd) #:use-module (crates-io))

(define-public crate-gcnd-0.0.1 (c (n "gcnd") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.9.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "14qzk54c79vdgdmkiaxh792g01xvx1jhj3jc9pxlskp9qr5kxbgg")))

