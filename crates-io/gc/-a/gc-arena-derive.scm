(define-module (crates-io gc -a gc-arena-derive) #:use-module (crates-io))

(define-public crate-gc-arena-derive-0.1.0 (c (n "gc-arena-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0n5sz6x3zairy1w33nrxqy0c483fwn45p4pwnx2zfjjprq92wa03")))

(define-public crate-gc-arena-derive-0.1.1 (c (n "gc-arena-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1zrv7nss0di9v06pqy38i8cxyq13gq585c33cwj8ws79zh9vy7rl")))

(define-public crate-gc-arena-derive-0.2.0 (c (n "gc-arena-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0905hnlngwkamdxvchxxymmi7y7wlaj8zzkkfrzj62cpa2fzv8gm")))

(define-public crate-gc-arena-derive-0.2.1 (c (n "gc-arena-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1j2amzff77qa890wp4c2jcx2vz7a8gp44v6rflp2xh4r25vlbnyw")))

(define-public crate-gc-arena-derive-0.2.2 (c (n "gc-arena-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ah7ibym25k1a7nh0cd4s89abvw08nill8ddx27gn1g7llcq95s0")))

(define-public crate-gc-arena-derive-0.3.0 (c (n "gc-arena-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1gsz4w00l1bjy7inif2ygr318xx1pzvz1bkazaf490873h12psqa")))

(define-public crate-gc-arena-derive-0.3.1 (c (n "gc-arena-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0q2rqr6b388kxvic2y539yi2j32fq3nnkyy3lsip1iiq0mhanr1q")))

(define-public crate-gc-arena-derive-0.3.2 (c (n "gc-arena-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0krf274ljny7v34i6hzfi5w7apaipdbmpr05wmixrs0kmva2mny1")))

(define-public crate-gc-arena-derive-0.3.3 (c (n "gc-arena-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1schby9vd4hzbpnkz3zc9vq8lk828qrwlh9kll59g9p1ybhndz9m")))

(define-public crate-gc-arena-derive-0.4.0 (c (n "gc-arena-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0f4hhxvgsfi05ms7mfd4h8sgsddd7lhz5qg3sb17xr64r5dx0x5y")))

(define-public crate-gc-arena-derive-0.5.0 (c (n "gc-arena-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "05sg1v3xkd7hq442n382yk1g2d27n6kjqzsg6claksi9sj3w9wwn")))

(define-public crate-gc-arena-derive-0.5.1 (c (n "gc-arena-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0c7d53canf19x80giiy7x09vramfxrxwsb9qmk2b35j8lql2v5cc")))

(define-public crate-gc-arena-derive-0.5.2 (c (n "gc-arena-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0a03q1qxigb4a0isw7bmpvf179jjq8afckk20qaiz9d4mzdmbcvq")))

(define-public crate-gc-arena-derive-0.5.3 (c (n "gc-arena-derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("default" "visit-mut"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1pfdsd5m1vy52791q5s2z1vp1rvghg90hx3snx3118apangsc4n6")))

