(define-module (crates-io gc -a gc-adapter) #:use-module (crates-io))

(define-public crate-gc-adapter-0.1.0 (c (n "gc-adapter") (v "0.1.0") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0h68f6yn5i4q8972dkadr62c977zw08xnz9zwb4c55pphvah3815") (f (quote (("libusb" "rusb"))))))

(define-public crate-gc-adapter-0.1.1 (c (n "gc-adapter") (v "0.1.1") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (o #t) (d #t) (k 0)))) (h "19jfkav63w5qkkdcj0pgyk1ljy07i1wxqbxcs159mmlay5lrdbwd") (f (quote (("libusb" "rusb"))))))

(define-public crate-gc-adapter-0.1.2 (c (n "gc-adapter") (v "0.1.2") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1nygaavmyyj8xcc2k0d0icccpkfa030f0iqmmikmw2akn9qrpz4c") (f (quote (("libusb" "rusb"))))))

