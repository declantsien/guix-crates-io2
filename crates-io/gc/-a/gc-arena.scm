(define-module (crates-io gc -a gc-arena) #:use-module (crates-io))

(define-public crate-gc-arena-0.1.0 (c (n "gc-arena") (v "0.1.0") (d (list (d (n "gc-arena-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1dmk2jki4rgnsic9qlz6gszrap94pg2yxl10a7y3q92rc920xmav")))

(define-public crate-gc-arena-0.1.1 (c (n "gc-arena") (v "0.1.1") (d (list (d (n "gc-arena-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0mpmhkizvvygyfdp486q5kpzqs4yv43pc004fpgzn5q7zr7qpipp")))

(define-public crate-gc-arena-0.2.0 (c (n "gc-arena") (v "0.2.0") (d (list (d (n "gc-arena-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1s631xvjz7rfsxmvdshahvv7h8pysm3m2yizs50ysrnq1czjl9p7")))

(define-public crate-gc-arena-0.2.1 (c (n "gc-arena") (v "0.2.1") (d (list (d (n "gc-arena-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0kyl8r3xiy4za2bcgvlzyxn2m6ms5dj3akrjgn44pz0w5m270rjg") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.2.2 (c (n "gc-arena") (v "0.2.2") (d (list (d (n "gc-arena-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0z96ivn6q2z9vln0p4x91599fvblp43x3r4fb1i13dn3a6q2b8wc") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.3.0 (c (n "gc-arena") (v "0.3.0") (d (list (d (n "gc-arena-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0f1dzi0b5spkjz1j94qgb2z1z2ws4w3gbvn06ch5yp48j16z92sv") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.3.1 (c (n "gc-arena") (v "0.3.1") (d (list (d (n "gc-arena-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1kdrjpmqya2069hm4f9jhs860sdmb15y92mlyqm57g2mavjhllfr") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.3.2 (c (n "gc-arena") (v "0.3.2") (d (list (d (n "gc-arena-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0jd71zq20zbp8hlpl8drsjg7p5yi0bilh2j22b3b719pf9jhdadp") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.3.3 (c (n "gc-arena") (v "0.3.3") (d (list (d (n "gc-arena-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13lfivyh3jwsqwym08ibf9pl64nsikv0v599qs1jzq2yp3c6p0iz") (f (quote (("std") ("default" "std"))))))

(define-public crate-gc-arena-0.4.0 (c (n "gc-arena") (v "0.4.0") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "gc-arena-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1anfd728ns8j1nr44z83x20y672qdmp0a3kba5avqjbbwhkv8r9k") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tracing" "dep:tracing") ("allocator-api2" "dep:allocator-api2" "hashbrown?/allocator-api2"))))))

(define-public crate-gc-arena-0.5.0 (c (n "gc-arena") (v "0.5.0") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "gc-arena-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "020j9isaqc929nl04capb4caxgyw6131rvcwrh09j6y57wxy9cjp") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tracing" "dep:tracing") ("allocator-api2" "dep:allocator-api2" "hashbrown?/allocator-api2"))))))

(define-public crate-gc-arena-0.5.1 (c (n "gc-arena") (v "0.5.1") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "gc-arena-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rairh6lha18z0dii8kxjkwi6ahh7vjq7hxgp00l7vy1qgcakm14") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tracing" "dep:tracing") ("allocator-api2" "dep:allocator-api2" "hashbrown?/allocator-api2"))))))

(define-public crate-gc-arena-0.5.2 (c (n "gc-arena") (v "0.5.2") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "gc-arena-derive") (r "^0.5.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0nhfk2mxs6lhjyjvcwmhcdazj4a4iqgl277zrv5fli48061swdlx") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tracing" "dep:tracing") ("allocator-api2" "dep:allocator-api2" "hashbrown?/allocator-api2"))))))

(define-public crate-gc-arena-0.5.3 (c (n "gc-arena") (v "0.5.3") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "gc-arena-derive") (r "^0.5.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rl2x26lmn9gfvbwahhcz9kr8pcvavr4yqg9m8s7i4rjibw0rmrw") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tracing" "dep:tracing") ("allocator-api2" "dep:allocator-api2" "hashbrown?/allocator-api2"))))))

