(define-module (crates-io gc v_ gcv_spline) #:use-module (crates-io))

(define-public crate-gcv_spline-0.1.0 (c (n "gcv_spline") (v "0.1.0") (h "15kmj43n3g3r3bb04p6aah58b1cvfsp0abpnalgp47rxxgglvvhk")))

(define-public crate-gcv_spline-0.2.0 (c (n "gcv_spline") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.17") (f (quote ("libm"))) (k 0)))) (h "0bbgam5c1ambcpk1qfx96ak3j2yjg51kk4w0nmh183aw8smnjygz")))

