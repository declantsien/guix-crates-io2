(define-module (crates-io gc dx gcdx) #:use-module (crates-io))

(define-public crate-gcdx-0.1.0 (c (n "gcdx") (v "0.1.0") (h "08s1amr9kcjl11z7im9m6y464cr9agjwhgiy86va7k0hkv3cgf7w")))

(define-public crate-gcdx-0.1.1 (c (n "gcdx") (v "0.1.1") (h "07zn90jd53sd79s6jcwvgyx17jf9q1brvwalgw4kcglya3ndhw8r")))

(define-public crate-gcdx-0.1.3 (c (n "gcdx") (v "0.1.3") (h "041dh7c7j4s6ck5d5nrxyr6abjx4mq1mamnszifqm18wdzckidwk")))

(define-public crate-gcdx-0.1.4 (c (n "gcdx") (v "0.1.4") (h "1686id4kcw0zp19d77lhln1mdryj7dlpp7j5if3dlyzshhk2wkya")))

(define-public crate-gcdx-0.1.5 (c (n "gcdx") (v "0.1.5") (h "13l3ph4lb4g4v6gi8il6a456a0cg9n60gq4q9r7g0plwhdk47njv")))

(define-public crate-gcdx-0.1.6 (c (n "gcdx") (v "0.1.6") (h "0zh2lfpldlsxd1kqpvfc1wm212fq5cx5rkid723y9acd2fhzardk")))

(define-public crate-gcdx-0.1.7 (c (n "gcdx") (v "0.1.7") (h "035akxzhi9pw1bxwjn87lh5r2rqb7p91p2wnzx5b163i1l0jifxk")))

(define-public crate-gcdx-0.1.8 (c (n "gcdx") (v "0.1.8") (h "1gi28mc1svlhxw1fqhvqjw7qay032k6z1a5kdsak4856glds4kf4")))

