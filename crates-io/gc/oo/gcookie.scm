(define-module (crates-io gc oo gcookie) #:use-module (crates-io))

(define-public crate-gcookie-0.0.3 (c (n "gcookie") (v "0.0.3") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_Security_Cryptography"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lvgy9z7c0hh9wmbd0aggg0hf7rk286rf4da3yyb750s3cjf2q9i")))

