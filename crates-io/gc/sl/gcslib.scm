(define-module (crates-io gc sl gcslib) #:use-module (crates-io))

(define-public crate-gcslib-0.1.1 (c (n "gcslib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "google_auth") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xap6z3rqh12x82ycw9vjy6bi3jyqz1kmp30n8f822z2a8fcyhrw")))

(define-public crate-gcslib-0.2.1 (c (n "gcslib") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "google_auth") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04ar3padc7qghpaavzc0nlji2ckz3crj7n30wqvc4dxf29cg9h2q")))

