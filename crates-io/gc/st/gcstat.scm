(define-module (crates-io gc st gcstat) #:use-module (crates-io))

(define-public crate-gcstat-0.0.1 (c (n "gcstat") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "rust-htslib") (r "^0.38.2") (d #t) (k 0)))) (h "0q3szlx6bd3gm2qqkq0rd5yhg00wab8qcv4krcy3vp04fm8dw49q") (y #t)))

