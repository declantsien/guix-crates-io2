(define-module (crates-io gc d- gcd-rs) #:use-module (crates-io))

(define-public crate-gcd-rs-0.1.0 (c (n "gcd-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "07d91ngl17h0nhrmpadlm2kmprrk15q02kaz1a4ricmkalhsdrpb")))

(define-public crate-gcd-rs-0.1.1 (c (n "gcd-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1bq881ay2lcb8mamrlv4ks0fdh37fp2q85xph1pvad62hyr0b45y")))

(define-public crate-gcd-rs-0.1.2 (c (n "gcd-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "08p6mk42y0gg2ydzsjb0nlfkp12l0zq4mbzcirpggravy5v80lxd")))

