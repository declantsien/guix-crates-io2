(define-module (crates-io gc d- gcd-bitwise) #:use-module (crates-io))

(define-public crate-gcd-bitwise-0.1.0 (c (n "gcd-bitwise") (v "0.1.0") (h "0nwq7kl5jl6smb0xnw640nbpcpjbqxavsnw33qi9gglwdrbv8h4w")))

(define-public crate-gcd-bitwise-0.1.1 (c (n "gcd-bitwise") (v "0.1.1") (h "14fhdl4lr9i8iy6hd62f2bzkd85h8rw88jhlcjir0g1klkjz2hna")))

(define-public crate-gcd-bitwise-0.1.2 (c (n "gcd-bitwise") (v "0.1.2") (h "13asqnp7mdrwvgdyzh5xskx8q5ni6n2vkkhq06iqbj1la8jab55v")))

(define-public crate-gcd-bitwise-0.1.3 (c (n "gcd-bitwise") (v "0.1.3") (h "1shq620qb1prf4a36hz86q16khs9cz04qx3vwm36jg17rlcgyd89")))

(define-public crate-gcd-bitwise-0.1.4 (c (n "gcd-bitwise") (v "0.1.4") (h "0y5aq2c3vldyg5gw842d2wsj41ysklbz8d8xmvp2kxa08prg20ak")))

(define-public crate-gcd-bitwise-0.1.5 (c (n "gcd-bitwise") (v "0.1.5") (h "1043vnam6aw6qn1qfkcjjvyvd9kn5ag26zns0plqia7jsr6j4hqy")))

(define-public crate-gcd-bitwise-0.1.6 (c (n "gcd-bitwise") (v "0.1.6") (h "1mgwvdjlfylam3vay9hxb8bypsvsmwl3f7lyxy12kdwa3mai16h3")))

(define-public crate-gcd-bitwise-0.1.8 (c (n "gcd-bitwise") (v "0.1.8") (h "1h1ykyll4bb8gnnlfv6i9991sa2zxbp8rsxn01akyfw6d128nygf")))

(define-public crate-gcd-bitwise-0.1.9 (c (n "gcd-bitwise") (v "0.1.9") (h "0q8pih6byr7wadzy60wn5c5pxzcngbw2nk6a87rs5kdlxdfqdlf9")))

(define-public crate-gcd-bitwise-0.2.0 (c (n "gcd-bitwise") (v "0.2.0") (h "0yx126chwxr446ynkw649rnkm459ikpvw8i4bgx14400413hy4b2")))

(define-public crate-gcd-bitwise-0.3.0 (c (n "gcd-bitwise") (v "0.3.0") (h "1kpfmmpzqjy108lm33x0clzxg75mbshvw844bs7b527cb0h6wffk")))

