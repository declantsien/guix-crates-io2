(define-module (crates-io gc nc gcnctrlusb) #:use-module (crates-io))

(define-public crate-gcnctrlusb-0.1.0 (c (n "gcnctrlusb") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "libusb") (r "^0.2") (d #t) (k 0)))) (h "1nphwkm8gf3bnm1kxxn8cyq0lpmla6yk469626vz663q8x9r6l5p")))

