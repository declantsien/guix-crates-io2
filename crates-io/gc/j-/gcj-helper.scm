(define-module (crates-io gc j- gcj-helper) #:use-module (crates-io))

(define-public crate-gcj-helper-0.1.0 (c (n "gcj-helper") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0xndzinn08bsjcfblj8rmrgf16xj1w2x4kwfi26ic9vm9m3dzmrx") (y #t)))

(define-public crate-gcj-helper-0.1.1 (c (n "gcj-helper") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0p100zszwxys5jflrga9vhk0nib26bvz07vnasv4n9xdywj7lqp9")))

(define-public crate-gcj-helper-0.2.0 (c (n "gcj-helper") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1xrbdinn0vzlnmnv41ylb643q3ja4si1gcqqzvxlbpl53vdmvf1v")))

(define-public crate-gcj-helper-0.3.0 (c (n "gcj-helper") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)))) (h "013gz00fs0lgd3ynr94rfnm050la3kyr9bjybdhw4frhgzwia8pp") (f (quote (("parallel" "rayon") ("default"))))))

(define-public crate-gcj-helper-0.4.0 (c (n "gcj-helper") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1r7d5ivr4l6314xc0n3bnszwkmjn5n2zqaxd55vd747sskbr7vay") (f (quote (("parallel" "rayon") ("default"))))))

(define-public crate-gcj-helper-0.5.0 (c (n "gcj-helper") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)))) (h "132mmbgv1zi2c4bj2x41rxxvpnyimkzfzd1r548c502x4w9128bs") (f (quote (("parallel" "rayon") ("default"))))))

