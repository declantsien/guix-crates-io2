(define-module (crates-io gc ra gcra) #:use-module (crates-io))

(define-public crate-gcra-0.1.0 (c (n "gcra") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)))) (h "1xi22f6dzbz3hzymzyrl6830c3wkg8xn1phdnvi29h71by4hi7dv")))

(define-public crate-gcra-0.2.0 (c (n "gcra") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08nsnjz4n2554imdlp333vn66a4mh8dlh2ldvkdzfl8qr6nmaxjs")))

(define-public crate-gcra-0.3.0 (c (n "gcra") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "1xag3jg7flnc7xdjrga0zrdhwdkkd1mfm6901l4pn298l2pv1jyh") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default"))))))

(define-public crate-gcra-0.3.1 (c (n "gcra") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "11058b7ay04q2bj1xjfkl6yr6lz394zkc39dvn6pyhq0rw9h2ama") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default"))))))

(define-public crate-gcra-0.3.2 (c (n "gcra") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0krf2msczwr15iafr3l69f59h0pkdbsn2lprwr40ialfbh81djv7") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.3.3 (c (n "gcra") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0i2sk9nh6863nfzx2gi2bffyx6w3b3jxwn1kjvh680xq677pibfb") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.3.4 (c (n "gcra") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0clnzmj4z3aj5ckznwahc2kifsvx93xlhcaylb6p14yl9nrsmjfs") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.3.5 (c (n "gcra") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "thingvellir") (r "^0.0.6-alpha1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "04q4k4h1hgv6lm5yhd29qw2cb6fqljhf8as84h3m3p0c4dy8z7rn") (f (quote (("rate-limiter" "thingvellir" "tokio") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.4.0 (c (n "gcra") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "154i0dkw6hhr7kvfyszrm3fwnz4511dhxpi697y3l4dc935wfiyi") (f (quote (("rate-limiter" "dashmap" "rustc-hash") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.4.1 (c (n "gcra") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0p2h5ig3rs3a1i03pdzfc45xchpxla916gsvpc3mychk59yj3gqf") (f (quote (("rate-limiter" "dashmap" "rustc-hash") ("default" "rate-limiter"))))))

(define-public crate-gcra-0.5.0 (c (n "gcra") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1mhfd4i1r9qpiwf4v5gf1lxyqihd3l01ksmlhzxqiyaiybqrw9y6") (f (quote (("rate-limiter" "dashmap" "rustc-hash") ("default" "rate-limiter"))))))

