(define-module (crates-io jb ra jbrain) #:use-module (crates-io))

(define-public crate-jBrain-0.1.0 (c (n "jBrain") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0als2v7whhk8mf754njkyqgvgd9q6girdr21z1kjydki1flf5maa")))

(define-public crate-jBrain-0.1.1 (c (n "jBrain") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "07afjagc65x1dciklkg4bx39dhba3ipqkjxqc6bazdw12i5dzg2s")))

(define-public crate-jBrain-0.1.2 (c (n "jBrain") (v "0.1.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "090lv4l52gp1gmilxbiw9hwpnfipn1pa0xzjwfirwbq5yggwdpil")))

