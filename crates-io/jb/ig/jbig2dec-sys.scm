(define-module (crates-io jb ig jbig2dec-sys) #:use-module (crates-io))

(define-public crate-jbig2dec-sys-0.1.0 (c (n "jbig2dec-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1ab640xdh2w2l982zrzi2yclc6jx88wrp1vp74aqxxn07dsr7y59") (l "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1.1 (c (n "jbig2dec-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "00264ip44i221r1ic2qbhmbmb8zjki0lxx4ay2ha8x2pcvypj00m") (l "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1.2 (c (n "jbig2dec-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "08ygwqj3m3kzzj6k9qyns6nlnk1xs91fc17a86xiywq8af371m3c") (l "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1.3 (c (n "jbig2dec-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "0m0h1m39y3vb21677sqbj3zv228v1gr4dd0kkgs110hjclgffhd4") (l "jbig2dec")))

(define-public crate-jbig2dec-sys-0.18.0 (c (n "jbig2dec-sys") (v "0.18.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1i6gxhf3n44z6354qii6wpvngpm6cbp2wkggpw9jcmjdih8vq4j9") (l "jbig2dec")))

(define-public crate-jbig2dec-sys-0.19.0 (c (n "jbig2dec-sys") (v "0.19.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "00lmvnrbwa7h32kwn2cs64pqwxikf4ybl0sihfxl6ziq18wq0jza") (l "jbig2dec")))

