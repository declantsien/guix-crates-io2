(define-module (crates-io jb ig jbig2dec) #:use-module (crates-io))

(define-public crate-jbig2dec-0.1.0 (c (n "jbig2dec") (v "0.1.0") (d (list (d (n "jbig2dec-sys") (r "^0.1.0") (d #t) (k 0)))) (h "13gr0mz9nr01q5dzcmwgqjhnszvvgjj0svz82b9vds5cyw2f1amp")))

(define-public crate-jbig2dec-0.1.1 (c (n "jbig2dec") (v "0.1.1") (d (list (d (n "jbig2dec-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1pki0d5s0scfa6k4cw8m88y2dz08r47jc4d8jwnl2yhrp35f51lh")))

(define-public crate-jbig2dec-0.1.2 (c (n "jbig2dec") (v "0.1.2") (d (list (d (n "image") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "jbig2dec-sys") (r "^0.1") (d #t) (k 0)))) (h "195fch4cs7939jxypx8asqbnxgxp41368gr3imw9yla5lmvg6h49") (f (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1.3 (c (n "jbig2dec") (v "0.1.3") (d (list (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (o #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "jbig2dec-sys") (r "^0.1") (d #t) (k 0)))) (h "0snrh1fjpchhn2xngjml5da2v6l570dp6hjagp3k1ljm6y8j752f") (f (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1.4 (c (n "jbig2dec") (v "0.1.4") (d (list (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (o #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "jbig2dec-sys") (r "^0.1") (d #t) (k 0)))) (h "1db06l7kh75xyp394bnnmsl0mvj1rsmdkwp7nndv48syfv7wc62z") (f (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1.5 (c (n "jbig2dec") (v "0.1.5") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (o #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "jbig2dec-sys") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.15.3") (o #t) (d #t) (k 0)))) (h "0z27zb2dvw41picsd7vdf4pg0ykk3zzdv4nsgfkvc1x4am4a2sm3") (f (quote (("with-image" "image" "png") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.2.0 (c (n "jbig2dec") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "jbig2dec-sys") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.15.3") (o #t) (d #t) (k 0)))) (h "1k4899pby4qg9h87qvk30zn4qxdmq69zrj28a4iswaazy6cj061b") (f (quote (("default" "png"))))))

(define-public crate-jbig2dec-0.2.1 (c (n "jbig2dec") (v "0.2.1") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "jbig2dec-sys") (r "^0.18") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "08y99bwynaxs3g549h8mnanij16fplf741kbn6h1iw3xdj2vjv8d") (f (quote (("default" "png")))) (y #t)))

(define-public crate-jbig2dec-0.3.0 (c (n "jbig2dec") (v "0.3.0") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "jbig2dec-sys") (r "^0.18") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "0qc298k69qlwsfa1nha8m8895l8vv44sx5jzl0689ibiazl4fcxw") (f (quote (("default" "png"))))))

(define-public crate-jbig2dec-0.3.1 (c (n "jbig2dec") (v "0.3.1") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "jbig2dec-sys") (r "^0.19") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "05bb99hp4v654lx7sw8djsknrggvdgrjalcg5ygxhjvjbxv8bifr") (f (quote (("default" "png"))))))

