(define-module (crates-io jb ig jbigkit-sys) #:use-module (crates-io))

(define-public crate-jbigkit-sys-1.0.0+2.1.0 (c (n "jbigkit-sys") (v "1.0.0+2.1.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0im0dxvw0b4csqx6knsqnx0f93aqgbxlr29dnsjg40ndd9jyby2m")))

