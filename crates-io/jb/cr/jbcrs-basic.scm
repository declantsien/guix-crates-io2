(define-module (crates-io jb cr jbcrs-basic) #:use-module (crates-io))

(define-public crate-jbcrs-basic-0.1.4 (c (n "jbcrs-basic") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "yade") (r "^0.1.2") (d #t) (k 0)))) (h "0zipkag3i32vp9wpmdy9hxwv780w309w5c7mdlxcnmf3ywl5mn6w")))

