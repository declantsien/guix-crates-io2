(define-module (crates-io jb cr jbcrs) #:use-module (crates-io))

(define-public crate-jbcrs-0.1.0 (c (n "jbcrs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "yade") (r "^0.1.2") (d #t) (k 0)))) (h "11fcivllz0h1vz5bcc93gvg6qg1rsb5k9ms978jgi9lxn77vq9f1")))

(define-public crate-jbcrs-0.1.1 (c (n "jbcrs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "yade") (r "^0.1.2") (d #t) (k 0)))) (h "0ln7z7dxd646n0r02dfj80gsgcwqznqpmh8wvr1d7glhkwmz7yql")))

(define-public crate-jbcrs-0.1.2 (c (n "jbcrs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "yade") (r "^0.1.2") (d #t) (k 0)))) (h "08zbmrvkkq3sw9fm0w0wgq7rnf17a5ac6z3dng502az88jgmxy4k")))

(define-public crate-jbcrs-0.1.3 (c (n "jbcrs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "yade") (r "^0.1.2") (d #t) (k 0)))) (h "0srkswk5n3y9lppb95wqc7y18zyzdz6h22s54cnwfwj1bfb045f4")))

