(define-module (crates-io go ng gong) #:use-module (crates-io))

(define-public crate-gong-0.1.0 (c (n "gong") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "mysql") (r "^0.19.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)) (d (n "uuid") (r "^0.1.17") (d #t) (k 0)))) (h "04vsp38mna89gkngdwyyzahvdksk06cclprjk3gxja7r3mix5wp5") (y #t)))

(define-public crate-gong-1.0.0 (c (n "gong") (v "1.0.0") (h "0lm5icjg517gq1m2bn8cjrg1yh7pqqw3jbvm461nhi0x064j0mbf") (y #t)))

(define-public crate-gong-1.0.1 (c (n "gong") (v "1.0.1") (h "0pxf3rsv0w4b23n2ab5zh61wfl1pvnyrfyfspzx4c5x6k304v7dy") (y #t)))

(define-public crate-gong-1.0.2 (c (n "gong") (v "1.0.2") (h "0a7z6rgyrg32zk1q8ncqkdslknfxd8yvm0y7mqcjvbq4pri7v1p6") (y #t)))

(define-public crate-gong-1.0.3 (c (n "gong") (v "1.0.3") (h "0v0zrik4ml0mw0hv5rfa2yahnzm6n0x4pwyy23pkxcd758spma1b") (y #t)))

(define-public crate-gong-1.1.0 (c (n "gong") (v "1.1.0") (h "0va83k13p4lfm8b7zflnbjx3k1i4hqhzvm98mcmmr717dmwlf94r") (y #t)))

(define-public crate-gong-1.1.1 (c (n "gong") (v "1.1.1") (h "1f834h6xpkm9kl390y8h8svwj2kl9bppr9jy5jxwfwklzz6rc91x") (y #t)))

(define-public crate-gong-1.0.4 (c (n "gong") (v "1.0.4") (h "1jhx1wkkkqliggv1hl9navi4x80h2qfdih8pg0w8m0flafjb4xk7")))

(define-public crate-gong-1.1.2 (c (n "gong") (v "1.1.2") (h "0kqpbk53c9ip7r0pak1bhdsf01ialmiy8gqk2wh86kr3hf23vf4d")))

(define-public crate-gong-1.2.0 (c (n "gong") (v "1.2.0") (h "0f0xaj1g4y878yavmn806zbhmd3c0csdxi1lb1fwkgf4iq8djw9z")))

(define-public crate-gong-1.2.1 (c (n "gong") (v "1.2.1") (h "11vk8qsq2zf3wr1fzw1ipr5bjl5lq97py15p1spq3hjd6hdmx632")))

(define-public crate-gong-1.3.0 (c (n "gong") (v "1.3.0") (h "0bb06z319mszj7n8573cn6170z2554k6h1vrsi3j9n2igq26v3gb")))

(define-public crate-gong-1.0.5 (c (n "gong") (v "1.0.5") (h "1gn0wy0i3z4scbzv1y3fj649m1f4ac46i1bg0fpfy86djzyfamx6")))

(define-public crate-gong-1.1.3 (c (n "gong") (v "1.1.3") (h "08xsfr8xivkp9w7pjywdnzb9sls3bc7gmbi5ac96298sf8bpzqn0")))

(define-public crate-gong-1.2.2 (c (n "gong") (v "1.2.2") (h "1ps00h08mcpgkmzcawysigyhprn0z4n4jwbpvmxydjypg9jmxh8d")))

(define-public crate-gong-1.3.1 (c (n "gong") (v "1.3.1") (h "1806jsl56hp0pw277c34dzmczs3r4f3jzar6z1qsjif36pbny66w")))

(define-public crate-gong-1.4.0 (c (n "gong") (v "1.4.0") (h "1biwrcyp3blhmanlddpcc2smy3nffs55la60nv92xmyds9hab7sp")))

(define-public crate-gong-1.4.1 (c (n "gong") (v "1.4.1") (h "0f3j4kjr0h1hf395j80dx11gwsx2idnq6q757448xx52cr6jaf7g")))

(define-public crate-gong-1.4.2 (c (n "gong") (v "1.4.2") (h "0bm53lnc9rlfrmrn958bdzvxi7lc0kd1i5spmnsb09d7bb5vsd3f")))

