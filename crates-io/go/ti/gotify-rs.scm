(define-module (crates-io go ti gotify-rs) #:use-module (crates-io))

(define-public crate-gotify-rs-0.1.0 (c (n "gotify-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "stream" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "1b4i9wlk0ckki7lav27k158ldvqv4q6akwwbj3nm1x03j7n5nvdr") (f (quote (("default" "async") ("async" "async-trait" "tokio"))))))

