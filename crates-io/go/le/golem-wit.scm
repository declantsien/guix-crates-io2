(define-module (crates-io go le golem-wit) #:use-module (crates-io))

(define-public crate-golem-wit-0.1.0 (c (n "golem-wit") (v "0.1.0") (h "0hni363dnvc8nq9swdciw79bnj52f02g5kjzwphrlhcjq5hags2c")))

(define-public crate-golem-wit-0.2.0 (c (n "golem-wit") (v "0.2.0") (h "00dkf3cqcpv0sl1k10vr2gvplqk9fra6194h54zh9jwkm8i3q9mk")))

(define-public crate-golem-wit-0.2.1 (c (n "golem-wit") (v "0.2.1") (h "1x0w5kard96y6wvgkcjpyav5yxvd6zc9hq80kxx881f5lw9v1ma5")))

(define-public crate-golem-wit-0.2.2 (c (n "golem-wit") (v "0.2.2") (h "1njgrcipnrgw2c6mdj0w6l79vd20flkw3sl4751circ2fgbmjl1g")))

(define-public crate-golem-wit-0.2.3 (c (n "golem-wit") (v "0.2.3") (h "13cr73g6npvbwmkv1yij6xfivh8dylvbc3pi132vwmw6ws1bjwk8")))

(define-public crate-golem-wit-0.2.4 (c (n "golem-wit") (v "0.2.4") (h "0g3lqjs1gxh56b49ij9i8nlbjwli6h0x3qiaryg866sdnl2j8956")))

(define-public crate-golem-wit-0.2.5 (c (n "golem-wit") (v "0.2.5") (h "03i3gnpq9rsqs7s95wcjmvl0dpv0fwlsnx2h9zyn90kbssahkym5")))

(define-public crate-golem-wit-0.2.6 (c (n "golem-wit") (v "0.2.6") (h "1sbr628smiz4cvd86a54vfpf4pppyj9nnvb3nqhpzil4p8k4fg0y")))

(define-public crate-golem-wit-0.2.7 (c (n "golem-wit") (v "0.2.7") (h "0ssdvxxy8nd62qg6my17vn329hlrdchq2k9rkc3208kvwkzqm6r9")))

(define-public crate-golem-wit-0.2.8 (c (n "golem-wit") (v "0.2.8") (h "12hliklqkj0dwsxzd37da8ndg3yjx5bxg8fxm50dfqyr719zfh8z")))

(define-public crate-golem-wit-0.2.9 (c (n "golem-wit") (v "0.2.9") (h "1jpmgyg9x8pfa10z5f6ny3dqyixcrrvvapwx13mcl6ncj8k02bfj")))

(define-public crate-golem-wit-0.2.10 (c (n "golem-wit") (v "0.2.10") (h "0jclwkpjqyvwl8688gda9z7gbgj93wb8b0hkjs9xc1n4mg8z7a89")))

