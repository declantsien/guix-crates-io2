(define-module (crates-io go le golem-rust-macro) #:use-module (crates-io))

(define-public crate-golem-rust-macro-0.2.0 (c (n "golem-rust-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "04daddyrarqc5c3zypi33drqrn6pq7h0qzvh6daxrk774dyggyz9")))

(define-public crate-golem-rust-macro-0.2.1 (c (n "golem-rust-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "116aakwb76pfz409c2zw814z6cknmg152l8l1lqbqgka54iw3qmq")))

(define-public crate-golem-rust-macro-0.2.2 (c (n "golem-rust-macro") (v "0.2.2") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "0dsdypv21f2zxkpryfdsbq0b35c7pkg9mwgfgy69b0930pv715dz")))

