(define-module (crates-io go le golem-rpc-macros) #:use-module (crates-io))

(define-public crate-golem-rpc-macros-0.1.0 (c (n "golem-rpc-macros") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01svlh7kfbhkxqsgf72nnh1iwzm1fpsji4y62jxxq8a7cwwsl2v1")))

(define-public crate-golem-rpc-macros-0.2.0 (c (n "golem-rpc-macros") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15s35ks1lkn3nnqw797zyjdblbcc3hqa8bl3n2iwlzly58pl73ch")))

