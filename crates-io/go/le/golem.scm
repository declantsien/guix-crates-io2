(define-module (crates-io go le golem) #:use-module (crates-io))

(define-public crate-golem-0.1.0-alpha0 (c (n "golem") (v "0.1.0-alpha0") (d (list (d (n "blinds") (r "^0.1.0-alpha5") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0m2g6gvxs6h4wfvf5b0xcaslydbvip9ahnsqql4i7vgghapl85db") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha1 (c (n "golem") (v "0.1.0-alpha1") (d (list (d (n "blinds") (r "^0.1.0-alpha6") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06m5n94wb0565mxc9f650sllalgj5axp89bhizcnqkmrzf9qkgr4") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha2 (c (n "golem") (v "0.1.0-alpha2") (d (list (d (n "blinds") (r "^0.1.0-alpha6") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p02m85z2nhnky2b85pai1b4v7b39sqa6n2z39z9m12bg8wrsl53") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha3 (c (n "golem") (v "0.1.0-alpha3") (d (list (d (n "blinds") (r "^0.1.0-alpha6") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hb03r7y80v6wqkbk15j33w5m4m2140lcn7j2gaqqz24z83wn7cj") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha4 (c (n "golem") (v "0.1.0-alpha4") (d (list (d (n "blinds") (r "^0.1.0-alpha6") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jrngxb2df53ivln4dz9wcb5b89r8siqkffnwh2v82z7ln7zycr4") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha5 (c (n "golem") (v "0.1.0-alpha5") (d (list (d (n "blinds") (r "^0.1.0-alpha6") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lbx9lb84x2kmmq198jpyjmdllg2n1z66bzjrlabrxgz1d4h9pjc") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0-alpha6 (c (n "golem") (v "0.1.0-alpha6") (d (list (d (n "blinds") (r "^0.1.0-alpha9") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qnwwk6clh189abjv7w8yyp577f3x07b658x8lqbbknvk2wlmrr1") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.0 (c (n "golem") (v "0.1.0") (d (list (d (n "blinds") (r "= 0.1.0-alpha10") (f (quote ("gl"))) (d #t) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0w09schw1wbyml16s08p6i8c7fm3imbx3s7vhkssc6mplac24w20") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.1 (c (n "golem") (v "0.1.1") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0m6l735nkj2l8c5y9wsn0f6djf10n4r82iwi56k14ll5xmzdhra7") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.2 (c (n "golem") (v "0.1.2") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0295zjy0ihx7c27g39m9x3qabs9hplbrsra8kpma4pz00h8vikzg") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb"))))))

(define-public crate-golem-0.1.3 (c (n "golem") (v "0.1.3") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09bb8jq4gx6gns9lx8r1n6f0i25dhjw3c2355qjs59mva342d8x9") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb") ("std"))))))

(define-public crate-golem-0.1.4 (c (n "golem") (v "0.1.4") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.7.0") (d #t) (k 2)))) (h "0n9wycfaki0skk9sm50k883i7zqwmk5j6xfdqzi9mad86mzax2xj") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb") ("std")))) (y #t)))

(define-public crate-golem-0.1.5 (c (n "golem") (v "0.1.5") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.7.0") (d #t) (k 2)))) (h "0nyl8zyi6xx7wpjpfy5vw2d7326adk0pb6dp40679d39xsrjmf7x") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb") ("std"))))))

(define-public crate-golem-0.1.6 (c (n "golem") (v "0.1.6") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.7.0") (d #t) (k 2)))) (h "1zkqpcn7xx1dxz87p7n97rbx0lpplicq1zrcwgkbaqhx9s916d7r") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb") ("std"))))))

(define-public crate-golem-0.1.7 (c (n "golem") (v "0.1.7") (d (list (d (n "blinds") (r "^0.1.0") (f (quote ("gl"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.7.0") (d #t) (k 2)))) (h "0lah5f906j1pdlrp594yhzrrzrbwbv7r5gfyq4gahpjhnii7m6n2") (f (quote (("web-sys" "glow/web-sys") ("stdweb" "glow/stdweb") ("std"))))))

