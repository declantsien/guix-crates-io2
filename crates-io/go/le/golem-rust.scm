(define-module (crates-io go le golem-rust) #:use-module (crates-io))

(define-public crate-golem-rust-0.1.0 (c (n "golem-rust") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "15awvafagmzj1ljllwg6qhxldfx2nqa2xfqjppzjgz73ggcfrhfg")))

(define-public crate-golem-rust-0.2.0 (c (n "golem-rust") (v "0.2.0") (d (list (d (n "golem-rust-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (f (quote ("realloc"))) (k 0)))) (h "1icarh5ipn9mh9wjf9fn66piymvhs07k9igdf9cziaxhf5ais2bl") (f (quote (("default" "macro" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid") ("macro" "dep:golem-rust-macro"))))))

(define-public crate-golem-rust-0.2.1 (c (n "golem-rust") (v "0.2.1") (d (list (d (n "golem-rust-macro") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (f (quote ("realloc"))) (k 0)))) (h "0aanhmid5hz76fgw17kkcdfqvf03q9a5ick7kmipk630jabjqahb") (f (quote (("default" "macro" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid") ("macro" "dep:golem-rust-macro"))))))

(define-public crate-golem-rust-0.2.2 (c (n "golem-rust") (v "0.2.2") (d (list (d (n "golem-rust-macro") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "=0.17.0") (f (quote ("realloc"))) (k 0)))) (h "1pnrbzjz0253dv9ijzi364dxrd2mma3iv4mvx974gs0f7h9bwf0s") (f (quote (("default" "macro" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid") ("macro" "dep:golem-rust-macro"))))))

