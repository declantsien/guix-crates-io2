(define-module (crates-io go le golem-scalajs-wit-bindgen) #:use-module (crates-io))

(define-public crate-golem-scalajs-wit-bindgen-0.0.2 (c (n "golem-scalajs-wit-bindgen") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.1") (d #t) (k 0)))) (h "03s4baxnnrhm0kkqr2305vw11wjkrnxihcq0fd3s683hydim5zdw")))

