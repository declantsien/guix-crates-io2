(define-module (crates-io go _t go_true) #:use-module (crates-io))

(define-public crate-go_true-0.1.0 (c (n "go_true") (v "0.1.0") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 2)) (d (n "jwt") (r "^0.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1mn8m1005wx4m7j1p9c1jvlp8xcs00qk6m8pv7xq566jmr2qfm0k")))

(define-public crate-go_true-0.1.1 (c (n "go_true") (v "0.1.1") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 2)) (d (n "jwt") (r "^0.16.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "10d5b0j6jamfds6vblvqc2ifv533l32wya4iz0ibpgxc13kim1vf")))

