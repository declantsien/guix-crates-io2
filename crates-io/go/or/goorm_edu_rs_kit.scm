(define-module (crates-io go or goorm_edu_rs_kit) #:use-module (crates-io))

(define-public crate-goorm_edu_rs_kit-0.1.0 (c (n "goorm_edu_rs_kit") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1qw8qgyrq6nwpvdwqnfrqzh9q22142wd2z0l69rpb3lnk8xvr8dn")))

(define-public crate-goorm_edu_rs_kit-0.1.1 (c (n "goorm_edu_rs_kit") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0kaz967fmglw36wbzn6rk7zy5h4ipw045sza57ill11b1j5m01s3")))

