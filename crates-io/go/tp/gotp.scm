(define-module (crates-io go tp gotp) #:use-module (crates-io))

(define-public crate-gotp-0.1.0 (c (n "gotp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.10") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0sadkl35gamarn5ai2cbwvr570q5089w79rb3h84hs0k7372n9ps")))

(define-public crate-gotp-0.1.1 (c (n "gotp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.10") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0i33ndqvf3v5db0ddh7rcfl51f1vjrn2ligxkyqd2k4dmh275swz")))

(define-public crate-gotp-0.1.2 (c (n "gotp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.10") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "19vmxxbngyplr2gfqav8v7bnb013kmj9is1chn5nrfplys0i7qg6")))

