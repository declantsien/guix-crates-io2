(define-module (crates-io go tp gotpm) #:use-module (crates-io))

(define-public crate-gotpm-0.1.0 (c (n "gotpm") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zj0yk3p8izhfq85lyrb1yq9d7acml1bwfnzdlz96jl7c44f8y9p")))

(define-public crate-gotpm-0.2.0 (c (n "gotpm") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.2") (d #t) (k 0)))) (h "1cckvhxnqzs5bvsrq38pba5w0lxwm3j7qsgrj9zsfd73l25pkfms")))

(define-public crate-gotpm-0.2.1 (c (n "gotpm") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.2") (d #t) (k 0)))) (h "0zv7zywzw517sx83svawr9w1gwk6yrjki2bvpsswdzw8p93mii0q")))

