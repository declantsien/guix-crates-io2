(define-module (crates-io go f- gof-rs) #:use-module (crates-io))

(define-public crate-gof-rs-0.1.0 (c (n "gof-rs") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1n0kmyx5q36xvi4bwsyw8jkgkridp883812rcqsbxc4zcpm8c4y3")))

(define-public crate-gof-rs-0.2.0 (c (n "gof-rs") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0i9dk1lq65h4xrx94x6r27j45gh4371pi8lzz4vkcrj3f7rhdhfi")))

(define-public crate-gof-rs-0.2.1 (c (n "gof-rs") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0rznhz9mamp5rf4v45r2rz8bfbxvabh5cbxfncs2jsgm3ll5pmm0")))

(define-public crate-gof-rs-0.2.2 (c (n "gof-rs") (v "0.2.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0hgsj7jhdm9klhv4shard01r3bz0qqcm1lbla2lx7y70l55zmfv9")))

(define-public crate-gof-rs-0.2.3 (c (n "gof-rs") (v "0.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1hwgpp1w6n9xshqxb8naq6rdgq0x24brrhxajsiaq8bhqx48k4dp")))

