(define-module (crates-io go st gostd_settings) #:use-module (crates-io))

(define-public crate-gostd_settings-0.1.0 (c (n "gostd_settings") (v "0.1.0") (d (list (d (n "gostd") (r "^0.3.7") (d #t) (k 0)))) (h "14l8w5kl52ibxrwryq7m2hbvpbvmwrn8bmcrj58gr7302awhm0ys")))

(define-public crate-gostd_settings-0.1.1 (c (n "gostd_settings") (v "0.1.1") (d (list (d (n "gostd") (r "^0.3.7") (d #t) (k 0)))) (h "1rrgz8nj6dj386g6mqafiq9qvjq56ys7fmf2aj47mlwwykqx99fy")))

(define-public crate-gostd_settings-0.1.3 (c (n "gostd_settings") (v "0.1.3") (d (list (d (n "gostd") (r "^0.3.7") (d #t) (k 0)))) (h "02ycmyc1h1skgr2i3rbp2jdqz5b1ayz8ivcpdy9dq4pjb232968p")))

(define-public crate-gostd_settings-0.1.4 (c (n "gostd_settings") (v "0.1.4") (d (list (d (n "gostd") (r "^0.3") (d #t) (k 0)))) (h "0w4xp01ia8lpkrr464j6z465iaqkg5p0gw3lbjxykxziz1i3dadc")))

