(define-module (crates-io go st gostd_derive) #:use-module (crates-io))

(define-public crate-gostd_derive-0.0.1 (c (n "gostd_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d0dqrlk30kbvjphmjpph5w99f9mj8f6gnkpmcy90r5ldcpl69i8")))

(define-public crate-gostd_derive-0.0.2 (c (n "gostd_derive") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kjwf510nc4skmk3kqrrsvxifjyvq50yl79z21n568hbczxri4xh")))

(define-public crate-gostd_derive-0.0.3 (c (n "gostd_derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02g264nfzb2rynvm20mjllq5nw41xlfw6isajhcg00p0pczp3lby")))

