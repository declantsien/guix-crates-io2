(define-module (crates-io go st gostd_builtin) #:use-module (crates-io))

(define-public crate-gostd_builtin-1.0.1 (c (n "gostd_builtin") (v "1.0.1") (h "1xp5fvazdqg8dhawy9nlfdxyi07l0yg4w10ay142vr51mvs47947")))

(define-public crate-gostd_builtin-1.0.2 (c (n "gostd_builtin") (v "1.0.2") (h "0cgw461962szr1ni4957dhf52n2h852qcp2sf0k8hydb3966545m")))

(define-public crate-gostd_builtin-1.0.3 (c (n "gostd_builtin") (v "1.0.3") (h "15iclviybj3985g8i0zn02yi4aj01hqark0ps41b9vdw5j3sc3pa")))

(define-public crate-gostd_builtin-1.0.4 (c (n "gostd_builtin") (v "1.0.4") (h "0xva4q45h9wv8zdirlvim91al07sddff95dbx2hz2nq2zq2y5lr2")))

