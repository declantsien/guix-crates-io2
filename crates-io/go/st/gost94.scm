(define-module (crates-io go st gost94) #:use-module (crates-io))

(define-public crate-gost94-0.1.0 (c (n "gost94") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0wg482zqvk8gpb77iqc2n9y01n40dmpq1zf99xwy9gzjkiqc9g1l")))

(define-public crate-gost94-0.1.1 (c (n "gost94") (v "0.1.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0g1g2sdbhsrlkr8czbias0nf93bjl8lq3lysnjl0j1ihdmgvwi16")))

(define-public crate-gost94-0.2.0 (c (n "gost94") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1im51lc44f62c5kmv2vdzqpc583pajr73drd6nz6xaar8c0qsclj")))

(define-public crate-gost94-0.2.1 (c (n "gost94") (v "0.2.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1xxglvapqcjkz37jn95vr436gy832rc195zkcrm4q0f60cp3bhbc")))

(define-public crate-gost94-0.3.0 (c (n "gost94") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1r9zpgfwafvfx3nqjjm1hhznyrbra1iag6vy65nrrc037jq9n8wz")))

(define-public crate-gost94-0.3.1 (c (n "gost94") (v "0.3.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0y7yk6aajxlbjzgwmaqjzh9yr3dq11yldqss8zrj6xdk5ksmh95v")))

(define-public crate-gost94-0.4.0 (c (n "gost94") (v "0.4.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0jkq4z75kh9jvnjdglqz2fr6nb5vc7ypa2xwp03ff15ri77yjy4w")))

(define-public crate-gost94-0.7.0 (c (n "gost94") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "16xqyf3x8vjlyldcybx5s2y35qnjbqdc8pbbp1q6gzwbmzjpk918")))

(define-public crate-gost94-0.9.0 (c (n "gost94") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1pbdwnrhfq9b7l3hcnf7viyhwgynlmg1izkk87aldl0bq6frb1yn") (f (quote (("std" "digest/std") ("default" "std")))) (y #t)))

(define-public crate-gost94-0.9.1 (c (n "gost94") (v "0.9.1") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "17121vsafrd3s0lajr1mw46b2gk8pi9avkv0mcn9mzfdsjjxdh3j") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-gost94-0.10.0 (c (n "gost94") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "12kdpljzqyrpmhjgd13q2835wni2j4nxh937lnx16r8xc6dyh2nr") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-gost94-0.10.1 (c (n "gost94") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "1z66g8gmswb0rajdwj86glfz1bi89ln8s6jsyxg4ywz4xr5yc7dm") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-gost94-0.10.2 (c (n "gost94") (v "0.10.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "08dmqib9jl90ikxkd05d6zrxkfwva0mixmaqslwsd46k0qy889l5") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-gost94-0.10.3 (c (n "gost94") (v "0.10.3") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "15zqh6d7fcn9az1qjd60pr7qyynbl725qvcxvvkz3xn4pfc8c0g1") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-gost94-0.10.4 (c (n "gost94") (v "0.10.4") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0v2jhmmqxzgw9f4qbp325vc67vxmb50xr5shsjklfppbm05hx5ig") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std"))))))

