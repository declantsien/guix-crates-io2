(define-module (crates-io go st gostd_time) #:use-module (crates-io))

(define-public crate-gostd_time-1.0.1 (c (n "gostd_time") (v "1.0.1") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "^1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1krxlzzkm16k065ki7iap1y2nyd2hnp4476n7clrh5y9rr23qmni")))

(define-public crate-gostd_time-1.0.2 (c (n "gostd_time") (v "1.0.2") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.2") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k26fd12vs32a5fhyyz3v0wq8sh3l1b78yblddanbhwqnwrk97gp")))

(define-public crate-gostd_time-1.0.3 (c (n "gostd_time") (v "1.0.3") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03cs734rqszc3xlphqiz0ldmdf5ba6v4rcm81p6wvh4f1lvpw80x")))

(define-public crate-gostd_time-1.0.4 (c (n "gostd_time") (v "1.0.4") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jdq9zs9zv4461500kjnaa2zs8zl85pqkzadr3mrsjiz7lbj0v6p")))

(define-public crate-gostd_time-1.0.5 (c (n "gostd_time") (v "1.0.5") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l26adlfsgn6gzk3g2cgc80h2pcmjhlymcwv86w6fzcr3k5lksyp")))

(define-public crate-gostd_time-1.0.6 (c (n "gostd_time") (v "1.0.6") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "035cd2mvwv0qnq1ns837dnmjzl8cahsnfl2mlw288zvq6awr2dx0")))

(define-public crate-gostd_time-1.0.7 (c (n "gostd_time") (v "1.0.7") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bpys5pfdpsp01if8iclfgs1jaayjas5ijwkqnfpv0hnz88bfi3d")))

(define-public crate-gostd_time-1.0.8 (c (n "gostd_time") (v "1.0.8") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "=1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gy1apkzdjchls9lkz7x1612g1y3m1xd0xijldvxjx1qhl1wcm67")))

(define-public crate-gostd_time-1.0.9 (c (n "gostd_time") (v "1.0.9") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_builtin") (r "^1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wvlpjlg7k32sg4xm1r5jaifq4x2yhivxrg0kgv665q7g85a4wlc")))

