(define-module (crates-io go st gostd) #:use-module (crates-io))

(define-public crate-gostd-0.0.1 (c (n "gostd") (v "0.0.1") (h "01dxl68013zprlsjpihwwji2agn94nly7xp13zc1i5a82bzsr0pl")))

(define-public crate-gostd-0.0.2 (c (n "gostd") (v "0.0.2") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b4vqpm6gsyx355ymj3mbrblfk0wa9balcpfzlvd1lmz7bald404")))

(define-public crate-gostd-0.0.3 (c (n "gostd") (v "0.0.3") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cbzljnr7f2v8n5pmvd314yfp130ps5mj3f10prq5lvsvmskb3h7")))

(define-public crate-gostd-0.0.4 (c (n "gostd") (v "0.0.4") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12m1pl3sybxbmi2kp64h05w7sgrnzss1y0zpb4wd7m9a2r7cx9pr")))

(define-public crate-gostd-0.0.5 (c (n "gostd") (v "0.0.5") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y7cy5lc3hvi8gsbkzjxxixl6awxysarqrhlndjxb7ql6xnj5r7k")))

(define-public crate-gostd-0.0.6 (c (n "gostd") (v "0.0.6") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k384h5si5xq0icjqdxcrv9xhvqnrbrpz60jpnxand8pkpi31hy8")))

(define-public crate-gostd-0.0.7 (c (n "gostd") (v "0.0.7") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hr3gvfy9wi0n2dp4lagkcfq5rq12qbymjpzswbx89n3hywjbn1f")))

(define-public crate-gostd-0.0.8 (c (n "gostd") (v "0.0.8") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_dervie") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "120bs7g601hs7p9fh7gxnagpiv1dljqhblg6fqv2msknjprlhb6v")))

(define-public crate-gostd-0.0.9 (c (n "gostd") (v "0.0.9") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n4ia14gigrhs02zpf7fbwily1wlpir09gkmb3lar53vmcxf5gyf")))

(define-public crate-gostd-0.1.0 (c (n "gostd") (v "0.1.0") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cwvfmgp1ky4klrwsgkf2rvs4n3d7vzqir2fyd706by0n9yx1jr5")))

(define-public crate-gostd-0.1.1 (c (n "gostd") (v "0.1.1") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p3w53m9y59f3namx9abxhqjkydm7mmj143qwpv9kg7w91rsz6zw")))

(define-public crate-gostd-0.1.2 (c (n "gostd") (v "0.1.2") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p5znrk64db26vi0r15cm6q8zmk6bmwgsiz697bf9s6jlz0ljf3q")))

(define-public crate-gostd-0.1.3 (c (n "gostd") (v "0.1.3") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02qimkd3wl9dwfwgkax1hi18i15z5c58iiflm9n7hihsq9w11mh7")))

(define-public crate-gostd-0.1.4 (c (n "gostd") (v "0.1.4") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17zxqvw3jl1kih5q50af5lzakapaqmpch204ph5sjgg81kr1v9dk")))

(define-public crate-gostd-0.1.5 (c (n "gostd") (v "0.1.5") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "055p04813q4h1c6dqjs0r8lgh2qi4rgba81r62ppdwwhvzxmvyfj")))

(define-public crate-gostd-0.1.6 (c (n "gostd") (v "0.1.6") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hy6lbxz1zgzs894wpmgy6pv7spam3la29bp1hfnkjblf4f0ck60")))

(define-public crate-gostd-0.1.7 (c (n "gostd") (v "0.1.7") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dfjd5akdbljkpiz0khnbxzhvd3dp19g1vgcg1k3vahybwikvwf4")))

(define-public crate-gostd-0.1.8 (c (n "gostd") (v "0.1.8") (d (list (d (n "cvt") (r "^0.1.1") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p2505r44ljrjjfsxmg6n3jlgl8gsjikibfjz9xfz78gvahn5w82")))

(define-public crate-gostd-0.1.9 (c (n "gostd") (v "0.1.9") (d (list (d (n "gostd_builtin") (r "=1.0.2") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.2") (d #t) (k 0)))) (h "0wanwyxzqfibyznb11km5q5zlaaglcar451194fkpvmh23x3bm88")))

(define-public crate-gostd-0.2.0 (c (n "gostd") (v "0.2.0") (d (list (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.3") (d #t) (k 0)))) (h "0sswy3q102y9wsqh9vdgajv82yi1711banj3jq39dnx49cz0pw2a")))

(define-public crate-gostd-0.2.1 (c (n "gostd") (v "0.2.1") (d (list (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.4") (d #t) (k 0)))) (h "1k0il67carn045afdi7xdnnl6f0brsndd0cpbn321g7ww754rynk")))

(define-public crate-gostd-0.2.2 (c (n "gostd") (v "0.2.2") (d (list (d (n "gostd_builtin") (r "=1.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.5") (d #t) (k 0)))) (h "07aaihcb0i8qpnm539wjkm0bv4mnx73bn1ny94xgnrdsx1ncb9pp")))

(define-public crate-gostd-0.2.3 (c (n "gostd") (v "0.2.3") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)))) (h "1ys1s87viraby4761zgp40y45gis2z9g449p6cpfigw56v1452vb")))

(define-public crate-gostd-0.2.4 (c (n "gostd") (v "0.2.4") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)))) (h "1b3834xp5ffm1wq34dgxr3k9n8q2iarzjg4n7p7rskdl7kqmbzj2")))

(define-public crate-gostd-0.2.5 (c (n "gostd") (v "0.2.5") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)))) (h "1lvadnbfcnrvh1nczg661fpzkj9xfwjjc91g29lahwqx9b3fbz7v")))

(define-public crate-gostd-0.2.6 (c (n "gostd") (v "0.2.6") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0nknf22hps1wbibgw9p44p6hf8nfs9zarm83yh9g9bwchca2kill")))

(define-public crate-gostd-0.2.7 (c (n "gostd") (v "0.2.7") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1nklz564yhsvvq4kpljckad9l1v78m090vs2zdj8y4y2qfdca1s2")))

(define-public crate-gostd-0.2.8 (c (n "gostd") (v "0.2.8") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0cagb36i37pkgq23api3n0fmk2zyy6fbbkb9gz3l38z9n8kvm4l0")))

(define-public crate-gostd-0.2.9 (c (n "gostd") (v "0.2.9") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0flk2aqz52bmcn8ls3x6sdfg6mwqrj652x31a0snhg7yda49hiib")))

(define-public crate-gostd-0.3.1 (c (n "gostd") (v "0.3.1") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1c4519xrdhr8g5nr0izmch4sk9m5bsf3bjh8c4v44cdwhlsy6szi")))

(define-public crate-gostd-0.3.2 (c (n "gostd") (v "0.3.2") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1l833m01326jwxavshdnn9snzfb05wmky8r737ypffl4xdg2n7p8")))

(define-public crate-gostd-0.3.3 (c (n "gostd") (v "0.3.3") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1h32gq5dbag43594k4ic19a18lp1fab5xshbnpn2c3ba9pgnwml7")))

(define-public crate-gostd-0.3.4 (c (n "gostd") (v "0.3.4") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1j2456zz0x243whv7k3hgq3ipks5na94d8njlzi2cp1lv767kdfh")))

(define-public crate-gostd-0.3.5 (c (n "gostd") (v "0.3.5") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1rgqxxv58wq5z37faxyjb1rksc4i5av7zb7gpc8sqcy4w1bnsmxk")))

(define-public crate-gostd-0.3.6 (c (n "gostd") (v "0.3.6") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1wiprfmkqmb12ngmyx59yh41jhx2kx5mppi064z3ka2yyk4j578n")))

(define-public crate-gostd-0.3.7 (c (n "gostd") (v "0.3.7") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "133yh1ch1s3h1cayl2fzl48qh2j9f9ksbd1i19vsxl0j0cbjdyl4")))

(define-public crate-gostd-0.3.8 (c (n "gostd") (v "0.3.8") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0652r3na73l2dkyz8gp6q7sispmzqsdq5jl7ipsmr5fi23d35l9h")))

(define-public crate-gostd-0.3.9 (c (n "gostd") (v "0.3.9") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1n8f2y0pzvw58087hw412385rybsskjkgm51gihi37gvk1bidm11")))

(define-public crate-gostd-0.3.10 (c (n "gostd") (v "0.3.10") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0d4iss5kc8gicnk13l9avcaw9ip9c17ql5p42bcdhmqxq0wpmb78")))

(define-public crate-gostd-0.3.11 (c (n "gostd") (v "0.3.11") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0v0gxbx7d2k9rlxa6m7vcfp1ivqgbyd2fzsapqdcgg0srvw67wzs")))

(define-public crate-gostd-0.3.12 (c (n "gostd") (v "0.3.12") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "1vwclscqimy4y45bn0xlzhvc6cvfsiib8wz3cid0a1bzpr3s6y01")))

(define-public crate-gostd-0.3.13 (c (n "gostd") (v "0.3.13") (d (list (d (n "gostd_builtin") (r "=1.0.4") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0x05i8cmh3q23b4kb6paxpb0i758zz105b2x08ghhnvza1yn4ais")))

(define-public crate-gostd-0.3.14 (c (n "gostd") (v "0.3.14") (d (list (d (n "gostd_builtin") (r "=1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (d #t) (k 0)))) (h "0nywps70cbgp5cz3faf7hfandqzqzfz9kn8zl5bs94614wxvxz69")))

(define-public crate-gostd-0.3.15 (c (n "gostd") (v "0.3.15") (d (list (d (n "gostd_builtin") (r "=1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "=1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (d #t) (k 0)))) (h "0bgx3vqpm07ijxdmz6lw3krni5756m1c04skn690f9nv2csv1mjd")))

(define-public crate-gostd-0.3.16 (c (n "gostd") (v "0.3.16") (d (list (d (n "gostd_builtin") (r "^1.0") (d #t) (k 0)) (d (n "gostd_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (d #t) (k 0)))) (h "0mg9xvcssh7sygcy4ng6mk5jdd24vasv7xrbcfjkvsxg4kr58vpw")))

