(define-module (crates-io go ku goku) #:use-module (crates-io))

(define-public crate-goku-0.1.0 (c (n "goku") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0nm20jdv10zacahrb6ydfki0aqi11fwm07h6mag67wnvnsrcah46")))

(define-public crate-goku-0.1.1 (c (n "goku") (v "0.1.1") (d (list (d (n "async-std") (r "^0.99.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0r1iph8blk7dl8hgw57g24vzwfmibkg3xshr3crdd93651ph88i1")))

