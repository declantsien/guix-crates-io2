(define-module (crates-io go bs gobs) #:use-module (crates-io))

(define-public crate-gobs-0.1.0 (c (n "gobs") (v "0.1.0") (d (list (d (n "dot_vox") (r "^4.1.0") (d #t) (k 2)) (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "glutin") (r "^0.24.0") (d #t) (k 2)) (d (n "vek") (r "^0.10.1") (d #t) (k 0)))) (h "02qipy3y8416rfk7v9rxxwm9ci83ya97z4p60y67vvknjrdqsxsk") (y #t)))

(define-public crate-gobs-0.1.1 (c (n "gobs") (v "0.1.1") (d (list (d (n "dot_vox") (r "^4.1.0") (d #t) (k 2)) (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "glutin") (r "^0.24.0") (d #t) (k 2)) (d (n "vek") (r "^0.10.1") (d #t) (k 0)))) (h "028vwia2wr2anx5v01z44b5cbmx8nw2jv9lqa8l2ldklzpv6nnkj")))

(define-public crate-gobs-0.2.0 (c (n "gobs") (v "0.2.0") (d (list (d (n "dot_vox") (r "^4.1.0") (d #t) (k 2)) (d (n "glium") (r "^0.29.0") (d #t) (k 2)) (d (n "glutin") (r "^0.26.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "vek") (r "^0.13.0") (d #t) (k 0)))) (h "1q9bx5aspcg9axdzhcm810v8p0bxl8rbl15vns69hyqvqrl1s3rb")))

