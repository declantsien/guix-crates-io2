(define-module (crates-io go su gosumemory_helper) #:use-module (crates-io))

(define-public crate-gosumemory_helper-0.1.0 (c (n "gosumemory_helper") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tungstenite") (r "^0.18.0") (d #t) (k 0)))) (h "164l2am8xrqzrmgslypb4mr3icabpv82snqavaljv0n039j3ysfz")))

