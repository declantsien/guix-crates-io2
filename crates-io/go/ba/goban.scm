(define-module (crates-io go ba goban) #:use-module (crates-io))

(define-public crate-goban-0.1.0 (c (n "goban") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1qz0wi44kn72hwrrv3shq5plrchbhby09vjsf9sv8rzs7h3gbqqa")))

(define-public crate-goban-0.1.1 (c (n "goban") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1vnvc52pi5qhgfw25ls0wnh4mhzvq6d72fmlcdk5gpby6nmh9ldw")))

(define-public crate-goban-0.1.2 (c (n "goban") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0f78pd0ai8ai45igyrrh211ydmvp5zikkca394x0xv27dg820vsl")))

(define-public crate-goban-0.1.3 (c (n "goban") (v "0.1.3") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0d5drmkr1539p3x4nhz4iyvzacj9gkilicc0idkgqjrynhv5axsn")))

(define-public crate-goban-0.1.5 (c (n "goban") (v "0.1.5") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0d11jws0dljxqrsid2awyqyrf1zp2cbmq106r83d8q305qg9kzq9")))

(define-public crate-goban-0.1.6 (c (n "goban") (v "0.1.6") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1k8z1fn90fxccp30aw4ln0lfc8bnhkz0nnw2ja11whgx9w969mi9")))

(define-public crate-goban-0.1.7 (c (n "goban") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0111jibbvr1qy9msggyhf7h5qz834fm6qh6z32lybw130z9208w0")))

(define-public crate-goban-0.1.8 (c (n "goban") (v "0.1.8") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0swl6lcqwlydpxq6xa7288xbvqcrkszpivbdhni7slvghkn38hrk")))

(define-public crate-goban-0.1.9 (c (n "goban") (v "0.1.9") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1rrsyddhhp137np1cqv3b657vk9l6nf83kai34c583m2l58z9c72")))

(define-public crate-goban-0.1.10 (c (n "goban") (v "0.1.10") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1ic997mdg4xmvch843k0af0801nd0lajy7kc0aaywxf9vnmx73sl")))

(define-public crate-goban-0.1.11 (c (n "goban") (v "0.1.11") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1jz7884gjf95xhx2aq9bkdb8p62pf2x8y3vp697mz8y7hy7v3psx")))

(define-public crate-goban-0.2.0 (c (n "goban") (v "0.2.0") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "01irb0n8s7pdwscag0y4phkdgb7sddawv2krm8s1vyhqz7s9101d")))

(define-public crate-goban-0.2.1 (c (n "goban") (v "0.2.1") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0f9wwjzmfy41f8faqnra6ifqi3vzsb1byj93ji51wdhjfwljd7hk")))

(define-public crate-goban-0.2.2 (c (n "goban") (v "0.2.2") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0dsa63hp7mdw8y2x097dzkd8csdw3ky9iz66shn4vagw1812invz")))

(define-public crate-goban-0.2.3 (c (n "goban") (v "0.2.3") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0vx981f1j8h5vvaha727521brspcdl48xycrnvp086i2rvs66ybf")))

(define-public crate-goban-0.2.5 (c (n "goban") (v "0.2.5") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0fsf5d1qhdma2ji4rqymw3w8jz2cz4hmkzdzmqljq9xqija2986g")))

(define-public crate-goban-0.2.8 (c (n "goban") (v "0.2.8") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1rbpgwr23dxlvmm7q0gq4x5gsingbnwmvlzzb3rv24kdwcbrb23y")))

(define-public crate-goban-0.3.0 (c (n "goban") (v "0.3.0") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "09v68kw8nbrr64hqr4xfj9c662c0kp9ymzcrqa4jnl1cdlg8iv0p")))

(define-public crate-goban-0.3.1 (c (n "goban") (v "0.3.1") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0zy0l109jddjgrg4rd71727q90n3z154vhaxcs343k3c66lg50fb")))

(define-public crate-goban-0.3.2 (c (n "goban") (v "0.3.2") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "173ka342kly77kg9zvnnib0n1l0k8jyd31ri560aww2glqm58qgg")))

(define-public crate-goban-0.3.3 (c (n "goban") (v "0.3.3") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1wawfskxv7s8zcv8aqk77hz9iq7z40c76kwjzcx2lglq5rh3hgd8")))

(define-public crate-goban-0.3.5 (c (n "goban") (v "0.3.5") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0bjj06vs9qmxrm7y4ccymy8vjk4yn8zwl2i30jf7sc92iqddl3ah")))

(define-public crate-goban-0.3.6 (c (n "goban") (v "0.3.6") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1aw8cb9yp9c704dxd7ywjr3p6q9n9q4xg6zqrgaxzypyblccz50h")))

(define-public crate-goban-0.3.9 (c (n "goban") (v "0.3.9") (d (list (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.1.1") (d #t) (k 0)))) (h "0pia8vjdghx51cm8bz6cacvv3m30x49vzq7qf9a1300i9j3vpxnf")))

(define-public crate-goban-0.4.0 (c (n "goban") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.1.1") (d #t) (k 0)))) (h "0qamcf1lzcvihfaqm7wlvgi942aw6xwycvagh2y8zpgh19l7xi97")))

(define-public crate-goban-0.4.2 (c (n "goban") (v "0.4.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "getset") (r "^0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.1.1") (d #t) (k 0)))) (h "1qqgy0vmnw9fznga5lfjkmg5cfihixdbjmcp8zgr5vy0ycyrdj5i")))

(define-public crate-goban-0.4.3 (c (n "goban") (v "0.4.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1ph20q92hn1ml9ji42l8d6g52j0zgr6n39lznajkz7khaad1cgmk")))

(define-public crate-goban-0.4.4 (c (n "goban") (v "0.4.4") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1ms4f8k3wamjb76l0lzgmfkisnyri3dnrp2ma3aq7yqf2ligdmpy")))

(define-public crate-goban-0.4.5 (c (n "goban") (v "0.4.5") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0zrzsqz5r70chiqfv4zb8wk08cm1c9l5dr872sairlwyc2rim7wc")))

(define-public crate-goban-0.4.7 (c (n "goban") (v "0.4.7") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0-pre.1") (d #t) (k 2)))) (h "0mp92k20y8j26gy5am7hh8ks4md5rh9k8sbncb994l0apkz1j9wx")))

(define-public crate-goban-0.6.0 (c (n "goban") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.2.0") (d #t) (k 0)))) (h "1lafsy5wkpd3bikcr27x53zkcljz24gvk7r5mrgvp57ii97q7irg")))

(define-public crate-goban-0.6.1 (c (n "goban") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.2.0") (d #t) (k 0)))) (h "0ggpyid3nzidsmnbfji1iprsannmbck850gsn94j1w01zvbiwhi0")))

(define-public crate-goban-0.6.2 (c (n "goban") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "1vvfk8rmx9a8makr4xvj7k79xrimx5hdqd4byy82mvqq263pabj1")))

(define-public crate-goban-7.0.0 (c (n "goban") (v "7.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)) (d (n "sloth") (r "^0.2.0") (d #t) (k 0)))) (h "18g1hdfv69ishcjjj5kas460cl9b0aa1017wyw49xwbabb1d8sbv") (y #t)))

(define-public crate-goban-0.8.0 (c (n "goban") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "19lbvscgg22av09zi83sx46ng1dp6m2mlvp34japsfsp8fqjqwh2") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.8.1 (c (n "goban") (v "0.8.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "1iy1n20xvz2iiri55vn7kw264lblkdjfyx4acdgzika3zg42pivq") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.8.2 (c (n "goban") (v "0.8.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "160m39827k73q4p7l4digj5cxdcjmh51k1m4bp4glb6aivb8y3p1") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.9.0 (c (n "goban") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "12g8pf3h73273d8n0dbhxks2lwppfbxw4vkyrd9dsil83l4y6n2r") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.10.0 (c (n "goban") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "0yyxhiix0jwk8sbfxascdxq20a5nbp5ljln5dsmyyavyjr9wbqzs") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.10.1 (c (n "goban") (v "0.10.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "0h6a3akfhqghywbsi940z4xcz13m3dzjpg9i1ridkc4rsakjzzy5") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.11.1 (c (n "goban") (v "0.11.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.3.0") (d #t) (k 0)))) (h "1pwdjlqh71gqyadmfyi9vadb0lmxclnvaxd23fcjm0vn4xz8gk0i") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.12.1 (c (n "goban") (v "0.12.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "1nv2xfwlrwvcfn2hz3s1c7cc0lldmrmi5ix4i8jy478ls2gkc0ym") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.12.2 (c (n "goban") (v "0.12.2") (d (list (d (n "ahash") (r "^0.3.8") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "05n99clljc3mp72bj31x5x69zr8vd3j6a3azsmjnw8lmpgzkbyck") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.12.3 (c (n "goban") (v "0.12.3") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0lacgyp6kvbii2hyh55lm92jcmg2h6k8jyl9dnd40s9cpn7fyqqa") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.13.0 (c (n "goban") (v "0.13.0") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0hysriq3gi4lbxfnlnw1vvz5g9warglbkziqdrqw6b1bl92naz6l") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.14.0 (c (n "goban") (v "0.14.0") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0axgfa7xpnmqbx8bxkkcjypz00aw1l5fmg4q9ppabvvwpnpcdg4w") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.15.0 (c (n "goban") (v "0.15.0") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "1qk858lif6iy98ji3lfb32xsjw9abbl3q0nqinsdf8mngypbjw63") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.15.1 (c (n "goban") (v "0.15.1") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "00bsag1ck8wbv13d8pz19y6aldi4i6da9gphxj5hh8lak0fyi0hk") (f (quote (("thread-safe") ("history"))))))

(define-public crate-goban-0.15.2 (c (n "goban") (v "0.15.2") (d (list (d (n "ahash") (r "^0.5.8") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0jzh9qblyc7d5fq72ya4j31rjmcczq92w5zdb2v0rq4g7aprg4fh") (f (quote (("thread-safe") ("history") ("deadstones" "rand" "oxymcts"))))))

(define-public crate-goban-0.16.0 (c (n "goban") (v "0.16.0") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0fd0bcpkgsxflimjmzfjdzww6bcsqmyqq3mn8ar9aq49y6hv3m4x") (f (quote (("thread-safe") ("history") ("deadstones" "rand" "oxymcts")))) (y #t)))

(define-public crate-goban-0.16.1 (c (n "goban") (v "0.16.1") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.4.0") (d #t) (k 0)))) (h "0rmrcgcd05v7a7gdqxqj8hjj5imbl57ghdjch9lkmq6amwm2flda") (f (quote (("thread-safe") ("history") ("deadstones" "rand" "oxymcts"))))))

(define-public crate-goban-0.17.0 (c (n "goban") (v "0.17.0") (d (list (d (n "ahash") (r "^0.7.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.6.0") (d #t) (k 0)))) (h "13kdpydywhjdm4pwsal6lsjpdas9q3snafcljk4h1fkvsk2nisfj") (f (quote (("history") ("deadstones" "rand" "oxymcts"))))))

(define-public crate-goban-0.18.0-beta (c (n "goban") (v "0.18.0-beta") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.6.0") (d #t) (k 0)))) (h "1lxjkwv7i3s887bk5agmyvhykcs3wn5m9hnn851g8rw8mfc8ss90") (f (quote (("history") ("deadstones" "rand" "oxymcts"))))))

(define-public crate-goban-0.18.0 (c (n "goban") (v "0.18.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sgf-parser") (r "^2.6") (d #t) (k 0)))) (h "0cx86bixgp0kv6cp0bf6lcfnip6h9v5qw2q18pg4iwsfi9fhs7fg") (f (quote (("history") ("deadstones" "rand" "oxymcts"))))))

(define-public crate-goban-0.18.1 (c (n "goban") (v "0.18.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "oxymcts") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sgf-parser") (r "^2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "05sa3gz7v4y6ikdnri79mph8qmmqqs9nbs2xnj82d6vrqkrisa0z") (f (quote (("history") ("deadstones" "rand" "oxymcts"))))))

