(define-module (crates-io go ba goban-screenhack) #:use-module (crates-io))

(define-public crate-goban-screenhack-0.1.0 (c (n "goban-screenhack") (v "0.1.0") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "gl") (r "~0.14") (d #t) (k 0)) (d (n "glutin") (r "~0.24") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "rand") (r "~0.7") (d #t) (k 0)) (d (n "sgf-parse") (r "~0.2") (d #t) (k 0)))) (h "14m4d91h4p9nfan81q40pl8vbfvycpkz9a2raq2mzia611gki5hn")))

(define-public crate-goban-screenhack-0.1.1 (c (n "goban-screenhack") (v "0.1.1") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "gl") (r "~0.14") (d #t) (k 0)) (d (n "glutin") (r "~0.24") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "rand") (r "~0.7") (d #t) (k 0)) (d (n "sgf-parse") (r "~0.3") (d #t) (k 0)))) (h "18pdbvljn7hp9n7vk7qkpi03mcgqgc43nllb2d6gg8n8lrnvjws6")))

(define-public crate-goban-screenhack-0.1.2 (c (n "goban-screenhack") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.26.0") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "sgf-parse") (r "^2.0.2") (d #t) (k 0)))) (h "18xah0ikacp0ps78lcvwrjmjwmvxsqf3gcfcikqmg7lk73pk2lz1")))

(define-public crate-goban-screenhack-0.1.3 (c (n "goban-screenhack") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.27.0") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sgf-parse") (r "^3.0.2") (d #t) (k 0)))) (h "06anijnv4kwf4iay6i8fhpcnspck4i968bkbfb622d52nv9jf69z")))

(define-public crate-goban-screenhack-0.1.4 (c (n "goban-screenhack") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.27.0") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sgf-parse") (r "^3.1.1") (d #t) (k 0)))) (h "0xw9lm9nqaf248pa6rysyvicvj02fvb7ifwi0765gxigarx5rxm9")))

