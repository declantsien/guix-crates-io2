(define-module (crates-io go l- gol-lib) #:use-module (crates-io))

(define-public crate-gol-lib-0.1.0 (c (n "gol-lib") (v "0.1.0") (h "0pf60rnakqnfn9y4zl3iq5fvc8wlcvdwipq8hn2fhsn11ikxpyja") (y #t)))

(define-public crate-gol-lib-0.1.1 (c (n "gol-lib") (v "0.1.1") (h "1kd26hw7iffydlcs78qz7p9avdhpzc4r2aglpbhag1zcvbmnhg31") (y #t)))

(define-public crate-gol-lib-0.1.2 (c (n "gol-lib") (v "0.1.2") (h "05a4mdmqsibzryr2va90cl65lgl9di5bs0ri6gwqmll81qw0v284") (y #t)))

(define-public crate-gol-lib-0.1.3 (c (n "gol-lib") (v "0.1.3") (h "18x2gszwp22cinjv7r8p8j1hiza9n3axalg1gix16bay3d6ijs5m")))

