(define-module (crates-io go l- gol-core) #:use-module (crates-io))

(define-public crate-gol-core-0.1.0 (c (n "gol-core") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0s5vgm3ilgnj9f9bgwflf7js0y2viqgy7mlcvgp0gyv53df1m5kv")))

(define-public crate-gol-core-0.2.0 (c (n "gol-core") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0lfv0jnx6d771nn4j3yy2qs5vh8apxv7nb982a0zrr66pq7isjlh")))

(define-public crate-gol-core-0.3.0 (c (n "gol-core") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1mshamj55fl61gln1finw69hdyn5vbx0p2wy1ckrzlgy1pz9aksg")))

(define-public crate-gol-core-0.4.0 (c (n "gol-core") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0fyby8yszmlw61xhnpd94fasbhhq99iy6lic7z2p1bxijn1zw01h")))

(define-public crate-gol-core-1.0.0 (c (n "gol-core") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1zl255jl39slzx7xih7g5xbk72gpryn2r6gy6xxkl7ky0426n1wd")))

