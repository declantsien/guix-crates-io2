(define-module (crates-io go to gotopub) #:use-module (crates-io))

(define-public crate-gotopub-0.0.1 (c (n "gotopub") (v "0.0.1") (h "0n9j08d8i8ahrlzc05k48v2cl1a4iin2wa02sid2xhci7zxs0p80") (y #t)))

(define-public crate-gotopub-0.0.2 (c (n "gotopub") (v "0.0.2") (h "0x9dcjcnm68nxnsxsjcvl8z3p11p4w7zd00cjlgzapg881hijkhh")))

