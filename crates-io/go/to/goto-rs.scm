(define-module (crates-io go to goto-rs) #:use-module (crates-io))

(define-public crate-goto-rs-0.1.0 (c (n "goto-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0bmkm759s61q3grcb9c8kfll8rxkz5q6aks2qp1j10swfzn411q2")))

(define-public crate-goto-rs-0.1.1 (c (n "goto-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "19qrvsjz4c5i288kclna7b7pp2zklk1bmk6q6z0mvfwjpi917kbj")))

(define-public crate-goto-rs-0.1.2 (c (n "goto-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1kk5ym1pvrc8786camwzrfvqp7z9lfqpaps5pfl2f6i79k61s8sl")))

(define-public crate-goto-rs-0.2.0 (c (n "goto-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "125d0573fihl2yb3wyrrzrljh2d72l4vbyfay2hcqfx8hr82pghj")))

(define-public crate-goto-rs-0.2.1 (c (n "goto-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "01vqfbavazdsq9xfkzxacn2b6hryhyqhzg5hl74g90j098y3m7cd")))

(define-public crate-goto-rs-0.2.2 (c (n "goto-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "03p407awf2vqln1n1pbh5nnrlr40i5ppfczinm1ail4lg5292xbf")))

(define-public crate-goto-rs-0.2.3 (c (n "goto-rs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1fsyp9s3lhxxvx90mvl1qy8is21g9qa2v4247azp2hn3x70n4jcn")))

(define-public crate-goto-rs-0.2.4 (c (n "goto-rs") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "150msk6gxkcils6743pikn7gx4cv3fc5zlnzxgw3qc2i0lyhh389")))

(define-public crate-goto-rs-0.3.0 (c (n "goto-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0mx9mxmdavgwp72wqf9xr2wcxg7sw3jngc1kkbr06jmjj6c96n5k")))

