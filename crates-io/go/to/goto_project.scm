(define-module (crates-io go to goto_project) #:use-module (crates-io))

(define-public crate-goto_project-0.1.0 (c (n "goto_project") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1vjcff9zx521pq437bmsb0zisb1nrww6z56imv86mw62afivzx07")))

(define-public crate-goto_project-0.1.1 (c (n "goto_project") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0lzgz5hvcjvp13nh6wk6qz39v3w58hf2p8zdk1kaqy7dzjn91ar5")))

(define-public crate-goto_project-0.1.2 (c (n "goto_project") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "17zdi7dxfml44dg374bbbq8ixsa2pnd8cmsjqydnykr49vc7f1fk")))

(define-public crate-goto_project-0.1.3 (c (n "goto_project") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "10s6lny3ry555q42c1qsb7nwg389n1m29qn8f2pbf4lx2ipfb13l")))

(define-public crate-goto_project-0.1.4 (c (n "goto_project") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "18ja67ziqfga1x31mscxpqk0z0y9bbbzvc52is6333283nc7jq1j")))

(define-public crate-goto_project-0.1.5 (c (n "goto_project") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0cvb7d2x9ivxwg55in8hhl758q9kikzg0qai2fwkawywq4028fqv")))

(define-public crate-goto_project-0.2.0 (c (n "goto_project") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0qzsg4ywrhgqv3lv6x4bdw6ss1pvpfg09gv5k7a461vw1gjm5fq5")))

(define-public crate-goto_project-0.2.1 (c (n "goto_project") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "16dknk6h207vxln601rzpbsr89ggv4m0xm0184x0zsz4xz8ycpr2")))

