(define-module (crates-io go to gotoo) #:use-module (crates-io))

(define-public crate-gotoo-0.1.0 (c (n "gotoo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0kf3yl17cf11m90zpdg9s73h6wyvmf7rwadvcz4m8lwa0rz0miai")))

(define-public crate-gotoo-0.1.1 (c (n "gotoo") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "182qwsaj01c35fkz7xrp7yr6bfgj54qkfd8fbpl8np9r3n27bb91")))

(define-public crate-gotoo-0.1.5 (c (n "gotoo") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "0i4407s61mxld3h483assyj4w71aqrsf1d2d99qhz2vjjnvdmn8b")))

(define-public crate-gotoo-0.1.8 (c (n "gotoo") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1mhb6h8qvisk2a697pmz0jxka1ajs7ni3rgy3vb7gpd4jdk5lh1r")))

