(define-module (crates-io go to goto) #:use-module (crates-io))

(define-public crate-goto-0.1.1 (c (n "goto") (v "0.1.1") (h "1dqz51085q2f7cfyqi3sqx9xqf9ssh3yi6zmvqvf1m9caqvr6jwx")))

(define-public crate-goto-0.1.2 (c (n "goto") (v "0.1.2") (h "0bhk11mkpvyhhvndsfla942ds0pylhwrpdnbhn5b4jgj3bfg98d4")))

