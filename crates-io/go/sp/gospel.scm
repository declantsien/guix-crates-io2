(define-module (crates-io go sp gospel) #:use-module (crates-io))

(define-public crate-gospel-0.1.0 (c (n "gospel") (v "0.1.0") (d (list (d (n "gospel-dump") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1abj4maf4l8m7ma5rjbhkybhmwy88sqxkfagzb4zvmj187n3bhfi") (f (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.1.1 (c (n "gospel") (v "0.1.1") (d (list (d (n "gospel-dump") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0ffvf7ssxz08gkx0mfqx3rabf2l2lyz9z04rjlbrj4jp2jdnz04m") (f (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.2.1 (c (n "gospel") (v "0.2.1") (d (list (d (n "gospel-dump") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0sg6dccd9vwj1pqd6ckashpwm234wsr3acfxiyqw943charaqzvv") (f (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.3.0 (c (n "gospel") (v "0.3.0") (d (list (d (n "gospel-dump") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0hm0ij9m5fq58jaibnyys4l97q3b7wlm9xydd3x7sqprp7xhw22c") (f (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.3.1 (c (n "gospel") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "19v5ibpvmp0q3blbiwxa0ij8y56a6s29z00d77lg3ngfykkcrvnc")))

