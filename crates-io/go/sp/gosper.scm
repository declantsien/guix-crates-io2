(define-module (crates-io go sp gosper) #:use-module (crates-io))

(define-public crate-gosper-0.1.0 (c (n "gosper") (v "0.1.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qvwh44k5npkivc02sf87asfcqx3lm143pxkhp5j0qmpcy8b8z86")))

