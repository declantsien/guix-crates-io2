(define-module (crates-io go sp gospin) #:use-module (crates-io))

(define-public crate-gospin-0.1.0 (c (n "gospin") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jcpxdgabr2wpk84xfr0ks1984jb89ri1m7j4m7i520xzgfzh3d2")))

(define-public crate-gospin-0.1.1 (c (n "gospin") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "16zzjd8z8jdx3wli9d1m0ld8awvw4l80hlcya32j3fnbjdvmfh97")))

(define-public crate-gospin-0.1.2 (c (n "gospin") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b5lya4fq1r54zz4yfg6vbac2hml90ps9zyvdinsbm1qgdyh3gk8")))

(define-public crate-gospin-0.1.6 (c (n "gospin") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (f (quote ("ansi-parsing"))) (k 0)))) (h "014w33pglmaln4pgkw52m771lddw6k8azp46acy97cnjrz0qravm")))

