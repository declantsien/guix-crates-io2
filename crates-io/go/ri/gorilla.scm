(define-module (crates-io go ri gorilla) #:use-module (crates-io))

(define-public crate-gorilla-0.1.0 (c (n "gorilla") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1s35fpqz9mm576d4p6js4kvidhxzlicid4k9naxh4lm3rlhnq1sk") (y #t)))

(define-public crate-gorilla-0.2.0 (c (n "gorilla") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "00yy5mg1qj9n6bjh5v185lfghg0mcsas9saapcb9w4hsg8yc9i74")))

(define-public crate-gorilla-0.2.1 (c (n "gorilla") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "10dwbbr1rkwxlhyjr45ngz4kr14vqjinh59zfybwpf2qw97qnvr4") (r "1.56")))

