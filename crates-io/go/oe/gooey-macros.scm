(define-module (crates-io go oe gooey-macros) #:use-module (crates-io))

(define-public crate-gooey-macros-0.1.0 (c (n "gooey-macros") (v "0.1.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.10.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote-use") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "00jzi2209j6w3cr5j6653ldm2s22khr2s38gn87xbbzdn6xq7s6n")))

(define-public crate-gooey-macros-0.2.0 (c (n "gooey-macros") (v "0.2.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.10.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote-use") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1rm2f05nnfmh3q24xfri3fg886lkwhv98fd5ynj1wh9s0mf8lpjl")))

