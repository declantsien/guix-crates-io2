(define-module (crates-io go ya goya) #:use-module (crates-io))

(define-public crate-goya-0.1.0 (c (n "goya") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0vxga4m4gj4hld276agjm0fhg1abp7a5sphz846brddglgfdwkn1")))

(define-public crate-goya-0.1.1 (c (n "goya") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "188pdj45njb18iw58ha1s3x2rpy3sngqd39b2vgsbpx6ljc03y0h")))

(define-public crate-goya-0.1.3 (c (n "goya") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1fgyv9ahm500vfxb5fsmmgdjnwrrpgpvjdrqmg2ri8svpbgfjp6n")))

(define-public crate-goya-0.1.4 (c (n "goya") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0hvqib92r1y1rgmp97fw0fcirw471ysa52z4g5f4i8a0hq501yk0")))

(define-public crate-goya-0.1.5 (c (n "goya") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1alkkriv94k7h9wmwyh75n59zkph3zkx53dqpxj4k42px8m6cy8y")))

(define-public crate-goya-0.1.7 (c (n "goya") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0mcznvl4xm0nrkz27wg7cnazc0jpvrv1v5sjwvamcdhc4azdviff")))

(define-public crate-goya-0.1.8 (c (n "goya") (v "0.1.8") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0pcxdj5jpjvrm6akdkyqy9dib8mbzv1z3s741jxcd579j9xz8dv4")))

(define-public crate-goya-0.1.9 (c (n "goya") (v "0.1.9") (d (list (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1036n8c92il1f3kwapkg23694prcnzpqqa4a9g2zj4xvqw07i92i")))

