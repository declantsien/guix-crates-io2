(define-module (crates-io go th gotham_middleware_headerauth) #:use-module (crates-io))

(define-public crate-gotham_middleware_headerauth-0.1.0 (c (n "gotham_middleware_headerauth") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "gotham") (r "^0.7") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.7") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "1nz2fjl2m3xlvkcnwfj9r9ygllchw3y83g9pp0vjbh2zdypcs3bd")))

