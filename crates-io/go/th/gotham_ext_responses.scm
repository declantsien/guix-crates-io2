(define-module (crates-io go th gotham_ext_responses) #:use-module (crates-io))

(define-public crate-gotham_ext_responses-0.0.0 (c (n "gotham_ext_responses") (v "0.0.0") (d (list (d (n "askama") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "gotham") (r "^0.4.0") (d #t) (k 0)))) (h "141gib7l80z6y5dgr03f581s16zlwjhlixxkabdabsrw2qjskjgz")))

