(define-module (crates-io go th gotham_derive) #:use-module (crates-io))

(define-public crate-gotham_derive-0.1.0 (c (n "gotham_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "1qa616avbmdn9fy5zl2acj5bpfd6r53ajkvqq877zd2ghd7nghsr")))

(define-public crate-gotham_derive-0.2.0 (c (n "gotham_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0bb9zaa47ily70w81qclyjzyacqaxhmcf79bjys3crgbyj70246k")))

(define-public crate-gotham_derive-0.2.1 (c (n "gotham_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1hirds3cfljy3ry8yqk3nfwl7ah8d65464dhlgg3y884y5ll1m7f")))

(define-public crate-gotham_derive-0.3.0 (c (n "gotham_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "117h6qin3sxlcqshxkka0m5mzzbk1n2b69f5fszq1v0f1fjr67y7")))

(define-public crate-gotham_derive-0.4.0 (c (n "gotham_derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "101l2s20m2br11xi4ybr4bjbqzi9k6qlip3ypgj7nb8b2amd41xj")))

(define-public crate-gotham_derive-0.5.0-rc.0 (c (n "gotham_derive") (v "0.5.0-rc.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b21i2gyffyi0jsrw3nl4hq04z2qp8hnn8vzx5nwkma537ycgvw4")))

(define-public crate-gotham_derive-0.5.0-rc.1 (c (n "gotham_derive") (v "0.5.0-rc.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "182jjsqv5x8h7vahp54s10s0a8l7avjd1bzix8il2clrh9jvf1vj")))

(define-public crate-gotham_derive-0.5.0 (c (n "gotham_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pzvgdc82qh0ksfagsxi43gq6ipmlqbgrb7s1bvikygb0v9wz09s")))

(define-public crate-gotham_derive-0.6.0 (c (n "gotham_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ix5yd5d0h59dizzdph1sqnqw8v3vylxgppi6msjlrbc6920x6km")))

(define-public crate-gotham_derive-0.7.0 (c (n "gotham_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rh1ycpzsp2xmddmwkijlj1404190fhz2gfmi8743d8zys095rv1")))

(define-public crate-gotham_derive-0.7.1 (c (n "gotham_derive") (v "0.7.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0n845a6njxmp8mrv1qaznbvanksplyillvnqyh5z8k2g6aqn14mk")))

