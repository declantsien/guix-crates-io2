(define-module (crates-io go th gotham_state) #:use-module (crates-io))

(define-public crate-gotham_state-0.1.0 (c (n "gotham_state") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "068541d4b2bmkkjr81sa7bzd6qfk9y9fvyymx8va56hsaf0cj05d")))

(define-public crate-gotham_state-1.0.0 (c (n "gotham_state") (v "1.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "002h0q1sc3ga1xz94yfmgxxkwbj5s53ahascd0j8ihj4djbrpaa6")))

(define-public crate-gotham_state-1.0.1 (c (n "gotham_state") (v "1.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "10s33p1ljvmdphwvsykcgfcmdid2xv2nhhwv2mrjzfpzkc7l4lnx")))

