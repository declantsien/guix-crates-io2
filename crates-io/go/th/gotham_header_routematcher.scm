(define-module (crates-io go th gotham_header_routematcher) #:use-module (crates-io))

(define-public crate-gotham_header_routematcher-0.1.0 (c (n "gotham_header_routematcher") (v "0.1.0") (d (list (d (n "gotham") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)))) (h "09iailg3kmd820baj6cl90kkpyifn84igd2as0l9qg4qg7dlwgfj")))

(define-public crate-gotham_header_routematcher-0.2.0 (c (n "gotham_header_routematcher") (v "0.2.0") (d (list (d (n "gotham") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "08f2v0bakv03ycc59nfydv72rblv5m1bj7q3hm1w5g7gdya16dzz")))

