(define-module (crates-io go th gotham-middleware-basicauth) #:use-module (crates-io))

(define-public crate-gotham-middleware-basicauth-0.1.0 (c (n "gotham-middleware-basicauth") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "gotham") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)))) (h "1rg9d9fcacank4hm682jj18bcwqv3l8yqrgmvl2r4mcs3mspzyj5")))

(define-public crate-gotham-middleware-basicauth-0.2.0 (c (n "gotham-middleware-basicauth") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "gotham") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.12.33") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)))) (h "1rlbvh9f5k2f8yn031a0yww5azgkwfr8kgqhbvg245kyqnpdi1a2")))

(define-public crate-gotham-middleware-basicauth-0.3.0 (c (n "gotham-middleware-basicauth") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "gotham") (r "^0.6") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)))) (h "0a9145r059cygwms5jqfn3dmrm5386c9zh0k5a9qz6813ihi593g")))

