(define-module (crates-io go th gothook) #:use-module (crates-io))

(define-public crate-gothook-0.1.0 (c (n "gothook") (v "0.1.0") (d (list (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "0ycldqmwr19hj315a1biir4yd0jpgnfv9rvb1g8b37jxw7518qn0")))

(define-public crate-gothook-0.1.1 (c (n "gothook") (v "0.1.1") (d (list (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "04kwp1j0pcpy3dfwyfsga6hgj47rfs972lldn39jv1vzal00kza4")))

