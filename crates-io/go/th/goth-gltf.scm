(define-module (crates-io go th goth-gltf) #:use-module (crates-io))

(define-public crate-goth-gltf-0.1.0 (c (n "goth-gltf") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "09hv746lxm70i9xac7amkalvnvbsiac8y0x6rravrs9q3pmv15d1") (f (quote (("primitive_reader" "bytemuck" "thiserror"))))))

(define-public crate-goth-gltf-0.1.1 (c (n "goth-gltf") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "1aix8vbjqgwz6c852jd2l7idiada7ricm7ic947j2qk9bp71ncks") (f (quote (("primitive_reader" "bytemuck" "thiserror"))))))

