(define-module (crates-io go th gotham_formdata_derive) #:use-module (crates-io))

(define-public crate-gotham_formdata_derive-0.0.1 (c (n "gotham_formdata_derive") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lkyrha330zgc4yd3wkj78cbly1xkd1dbnhqnvh52ypg76a4y7cq") (f (quote (("regex"))))))

(define-public crate-gotham_formdata_derive-0.0.2 (c (n "gotham_formdata_derive") (v "0.0.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14a6ms6am0rwkdmayrf6yb1q9pc4mrsxx61d6b4h77vbbwimshs7") (f (quote (("regex"))))))

(define-public crate-gotham_formdata_derive-0.0.3 (c (n "gotham_formdata_derive") (v "0.0.3") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1mi7z3kik8a9rabxnkvfrmq1aybscl4rwxxmb3m1w8plq94q98qp") (f (quote (("regex"))))))

(define-public crate-gotham_formdata_derive-0.0.4 (c (n "gotham_formdata_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1m66ddgj8caxghba2wrr9f84iw8z8czszw55r495wpapbgyyl0mv") (f (quote (("regex"))))))

(define-public crate-gotham_formdata_derive-0.0.5 (c (n "gotham_formdata_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "syn-path") (r "^1") (d #t) (k 0)))) (h "0yx7jlq1nwg7bxxbbxsdhvfgd2invd5qpcil80cwx8yfl28z3g5y")))

(define-public crate-gotham_formdata_derive-0.0.7 (c (n "gotham_formdata_derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "syn-path") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (k 2)))) (h "0b3si8gnw8cnyg0sdpgqbrcvidfcfv2a5b5lk1b3rpwf3c3lxw2m")))

