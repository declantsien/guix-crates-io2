(define-module (crates-io go th gotham-cors-middleware) #:use-module (crates-io))

(define-public crate-gotham-cors-middleware-0.1.0 (c (n "gotham-cors-middleware") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)) (d (n "unicase") (r "^2.1") (d #t) (k 0)))) (h "0gid51k94jf6h40gsnrlvv91dwq7l6xbw1x0hm8yaklf8s70wxj6")))

(define-public crate-gotham-cors-middleware-0.2.0 (c (n "gotham-cors-middleware") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)) (d (n "unicase") (r "^2.1") (d #t) (k 0)))) (h "0kyn2p69v0nc6d56vk8wx4qffzjdvl9q7bhkdq5pasg2dpb0nrr8") (y #t)))

(define-public crate-gotham-cors-middleware-0.2.1 (c (n "gotham-cors-middleware") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)) (d (n "unicase") (r "^2.1") (d #t) (k 0)))) (h "042d9xfhg1mj21naa8x9j04w9521nqywc92gwag6ipwf3wzs9jv4")))

