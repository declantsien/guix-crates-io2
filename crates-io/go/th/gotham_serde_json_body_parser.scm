(define-module (crates-io go th gotham_serde_json_body_parser) #:use-module (crates-io))

(define-public crate-gotham_serde_json_body_parser-0.1.0 (c (n "gotham_serde_json_body_parser") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gd261g85ss1xpxshh1frlfvrwzbig0l4v9v6wx8yg9jxg7sl5rj")))

(define-public crate-gotham_serde_json_body_parser-0.2.0 (c (n "gotham_serde_json_body_parser") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bbnjnavpwibrxpb1kfsij3zyg1942y6c0g34vgidymfnhqvz7yr")))

