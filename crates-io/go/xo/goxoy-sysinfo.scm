(define-module (crates-io go xo goxoy-sysinfo) #:use-module (crates-io))

(define-public crate-goxoy-sysinfo-0.1.0 (c (n "goxoy-sysinfo") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0p33h0iw9jz6azaz6adyihyi0lgjyljan1m1q3l383xl5h3hi8hy") (y #t)))

