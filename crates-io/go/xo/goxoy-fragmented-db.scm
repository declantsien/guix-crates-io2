(define-module (crates-io go xo goxoy-fragmented-db) #:use-module (crates-io))

(define-public crate-goxoy-fragmented-db-0.1.0 (c (n "goxoy-fragmented-db") (v "0.1.0") (d (list (d (n "goxoy-hash") (r "^0.0.1") (d #t) (k 0)) (d (n "goxoy-key-value") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0bmzdwdmzbk676kyvph9im23ywr99yjfkq73sginc7jy1m3jvzbs")))

(define-public crate-goxoy-fragmented-db-0.1.1 (c (n "goxoy-fragmented-db") (v "0.1.1") (d (list (d (n "goxoy-hash") (r "^0.0.1") (d #t) (k 0)) (d (n "goxoy-key-value") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1yalh9jgr2rwmlp2w9rswqz5l9k3bn2qfpfbz0bw9vl5hcs44yaw")))

