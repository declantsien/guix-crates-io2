(define-module (crates-io go xo goxoy-interpreter-broom) #:use-module (crates-io))

(define-public crate-goxoy-interpreter-broom-0.0.1 (c (n "goxoy-interpreter-broom") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1mv7f8y95hlp4dr6fx1c9ycw8fd7nhbi92fwwckb795z0knpnmsy") (y #t)))

