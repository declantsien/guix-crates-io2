(define-module (crates-io go xo goxoy-tar-archive) #:use-module (crates-io))

(define-public crate-goxoy-tar-archive-0.1.0 (c (n "goxoy-tar-archive") (v "0.1.0") (d (list (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0hv660n0w6v72v6h7ns6ylnnn6j9kw0cjnxnp0hh6qmqpzkkvd94")))

(define-public crate-goxoy-tar-archive-0.1.1 (c (n "goxoy-tar-archive") (v "0.1.1") (d (list (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1hx748bb1lvs4850w5azw455a8syvxx3bpjm3h427351ilqzxpk7")))

(define-public crate-goxoy-tar-archive-0.1.2 (c (n "goxoy-tar-archive") (v "0.1.2") (d (list (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0fpk76bfpn7gphpnq9v69k02i3xp7wa7s3mdh27hjnkz76p3x6sj")))

(define-public crate-goxoy-tar-archive-0.1.3 (c (n "goxoy-tar-archive") (v "0.1.3") (d (list (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0sxhdcjangqwxd25cadxndjqnlby2b4jm8a7k2imqjfvgzlmvrd7")))

