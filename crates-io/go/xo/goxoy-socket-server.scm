(define-module (crates-io go xo goxoy-socket-server) #:use-module (crates-io))

(define-public crate-goxoy-socket-server-0.1.0 (c (n "goxoy-socket-server") (v "0.1.0") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)) (d (n "goxoy-socket-client") (r "^0.1.6") (d #t) (k 0)))) (h "17hrv0210xpiqkgw08am1s7awldxcdjczf17bpmyab70zkd6vl4w") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-server-0.1.1 (c (n "goxoy-socket-server") (v "0.1.1") (d (list (d (n "goxoy-address-parser") (r "^0.0.7") (d #t) (k 0)) (d (n "goxoy-socket-client") (r "^0.1.7") (d #t) (k 0)))) (h "0jld8vj2ldyvizhw44c34vgr8dgpd2hm2vvijhny52pwcn2jd7wf") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-server-0.1.2 (c (n "goxoy-socket-server") (v "0.1.2") (d (list (d (n "goxoy-address-parser") (r "^0.0.9") (d #t) (k 0)) (d (n "goxoy-socket-client") (r "^0.1.9") (d #t) (k 0)))) (h "0kwnlpj4h17k6jrm9dgi5lzkgl7qsahwa59wc2jsfmgwkz7rrbdb") (r "1.66.1")))

