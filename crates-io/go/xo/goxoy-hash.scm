(define-module (crates-io go xo goxoy-hash) #:use-module (crates-io))

(define-public crate-goxoy-hash-0.0.1 (c (n "goxoy-hash") (v "0.0.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "02a36nscvyxhz4qqacczyyf9x1vnprxkg7gyql4ijdpi8rzwpma6") (r "1.66.1")))

