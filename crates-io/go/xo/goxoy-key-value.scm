(define-module (crates-io go xo goxoy-key-value) #:use-module (crates-io))

(define-public crate-goxoy-key-value-0.1.0 (c (n "goxoy-key-value") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "16c3dvshsqplhvvsp7rcvqfra9pi2gr7mnyc7xmxl1ndm2qng6wv") (y #t)))

(define-public crate-goxoy-key-value-0.1.1 (c (n "goxoy-key-value") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "03fxkv6wyh737jbsbg4y7r6axjqhky0nc3zpshqz0gd1229di0ih") (y #t)))

(define-public crate-goxoy-key-value-0.1.2 (c (n "goxoy-key-value") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0aiggs1kwjfibchcnslnxqls1l5md3dd9g8nvxp3k3kf80z74vhb")))

