(define-module (crates-io go xo goxoy-address-parser) #:use-module (crates-io))

(define-public crate-goxoy-address-parser-0.0.1 (c (n "goxoy-address-parser") (v "0.0.1") (h "0plw413q5ir1hmj67721wfpq5302fxhipgw0rxx2pdlgxq5wqn49") (y #t)))

(define-public crate-goxoy-address-parser-0.0.2 (c (n "goxoy-address-parser") (v "0.0.2") (h "0p6xvky6325j65wpw4sm3kbjhi2xjsx2jd3zdkd6l1i4sri7jqqc") (y #t)))

(define-public crate-goxoy-address-parser-0.0.3 (c (n "goxoy-address-parser") (v "0.0.3") (h "1dw8qp5jng4hdx6a99kgnhbbv9qx1yxllpg8x20k5zslrmfmf329") (y #t)))

(define-public crate-goxoy-address-parser-0.0.4 (c (n "goxoy-address-parser") (v "0.0.4") (h "1c3cjq30g1w85vj2hw6sl8j5i0s5wj4blns4gcvr83jfc6vm1cm4") (y #t)))

(define-public crate-goxoy-address-parser-0.0.5 (c (n "goxoy-address-parser") (v "0.0.5") (h "037fb1ls004dw3qn2yah3g2zshzz413gpvsxymiqb50y6j48mlwx") (y #t)))

(define-public crate-goxoy-address-parser-0.0.6 (c (n "goxoy-address-parser") (v "0.0.6") (h "0kmmyh8iysyid0d6pzjnr035vz4y27ymh6k043q6hc2v483x2251") (y #t)))

(define-public crate-goxoy-address-parser-0.0.7 (c (n "goxoy-address-parser") (v "0.0.7") (h "0ala4ndw8ixc1i5w7cgrikn70v1309gmkis5irzp4z8876zlic3x") (y #t)))

(define-public crate-goxoy-address-parser-0.0.8 (c (n "goxoy-address-parser") (v "0.0.8") (h "0lfyjfrv6nsw468w1lvgbncbj0jbv0ln9bmds7fs4b05ypqc2s9v") (y #t)))

(define-public crate-goxoy-address-parser-0.0.9 (c (n "goxoy-address-parser") (v "0.0.9") (h "1fhp3zsrkcyl908zbql1wjvzrmjwsymk4frw4148cldk6hcfm47k")))

(define-public crate-goxoy-address-parser-0.0.10 (c (n "goxoy-address-parser") (v "0.0.10") (h "0ajxxqzxgmq1w2klfpfr0yw85fcmkcpgznxh8fk48if3f964jc10")))

