(define-module (crates-io go xo goxoy-file-chunker) #:use-module (crates-io))

(define-public crate-goxoy-file-chunker-0.0.1 (c (n "goxoy-file-chunker") (v "0.0.1") (d (list (d (n "blake3") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0am30csn3wn5d7hq623dsvljisq1l2v0xc6kg8xwnaq743a75zkb")))

(define-public crate-goxoy-file-chunker-0.0.2 (c (n "goxoy-file-chunker") (v "0.0.2") (d (list (d (n "blake3") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "1zm08d7p0vsg15m06zgjr4ji8x4wz2znwljwgrx9rqflwf1m0j00")))

(define-public crate-goxoy-file-chunker-0.0.3 (c (n "goxoy-file-chunker") (v "0.0.3") (d (list (d (n "blake3") (r "^0.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1yp3y19y6kbzpwdg2yzj82c1j6y2xd1f1f9z2pynv01a4ciafchf")))

