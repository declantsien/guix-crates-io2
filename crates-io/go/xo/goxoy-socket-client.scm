(define-module (crates-io go xo goxoy-socket-client) #:use-module (crates-io))

(define-public crate-goxoy-socket-client-0.1.0 (c (n "goxoy-socket-client") (v "0.1.0") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "08yh2iqfj7q2ac9la6kn58naj62a8vi0da4fmh2qyfgkqp4m31kg") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.1 (c (n "goxoy-socket-client") (v "0.1.1") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "17qm7srmrgi3xzxa40xp17qqixnrvpv4pspi58ngp9l65x5j8mvl") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.2 (c (n "goxoy-socket-client") (v "0.1.2") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "0kg9s2bl2safc6nmf29qj54xzxli7jq6h3dkmva015w88c88snrv") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.3 (c (n "goxoy-socket-client") (v "0.1.3") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "1n23rma4yxnj4dp2l7xi86gnpyd0b226d1sc6n9xdhy2aiw6qi5g") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.4 (c (n "goxoy-socket-client") (v "0.1.4") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "0jd7ip50fwfd56hgfby4qm8hipklrzh1sgydfjdgadrw7dvs4031") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.5 (c (n "goxoy-socket-client") (v "0.1.5") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "1gy9p38ly88w9ymq2fp0b7d3r173fa36h5xa90hjy8hdc75garfw") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.6 (c (n "goxoy-socket-client") (v "0.1.6") (d (list (d (n "goxoy-address-parser") (r "^0.0.3") (d #t) (k 0)))) (h "0lx25hvms0bh7mqsnpz37vp96rf1gm8ay51mivx8iyvqi6hm992g") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.7 (c (n "goxoy-socket-client") (v "0.1.7") (d (list (d (n "goxoy-address-parser") (r "^0.0.5") (d #t) (k 0)))) (h "029jiq7kl2qafj3fcwd32ibrinwcyghzcvq03q1prs8cg8a7zs1m") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.8 (c (n "goxoy-socket-client") (v "0.1.8") (d (list (d (n "goxoy-address-parser") (r "^0.0.7") (d #t) (k 0)))) (h "1id14i2g8bi7z1dv5ac0fm311i67pkkv4zidvr47s9ir76l4nrn4") (y #t) (r "1.66.1")))

(define-public crate-goxoy-socket-client-0.1.9 (c (n "goxoy-socket-client") (v "0.1.9") (d (list (d (n "goxoy-address-parser") (r "^0.0.9") (d #t) (k 0)))) (h "1imky79g0ixz4v34lhy5z99rwdsr6l7xl3jki1y0zg0m2rawlf3b") (r "1.66.1")))

