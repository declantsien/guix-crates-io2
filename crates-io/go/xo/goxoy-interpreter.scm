(define-module (crates-io go xo goxoy-interpreter) #:use-module (crates-io))

(define-public crate-goxoy-interpreter-0.1.0 (c (n "goxoy-interpreter") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "032wbki8x77zxxbmf4ik6wkw6lll8g8vk5kacq7w3a897426msmc") (f (quote (("show_raw_bytecode") ("show_bytecode"))))))

