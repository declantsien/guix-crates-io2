(define-module (crates-io go xo goxoy_tcp_common) #:use-module (crates-io))

(define-public crate-goxoy_tcp_common-0.1.0 (c (n "goxoy_tcp_common") (v "0.1.0") (d (list (d (n "goxoy-address-parser") (r "^0.0.9") (d #t) (k 0)))) (h "1jmndrfdxqvinnnh6wx8r5v7yg2zwdshz44zkmavw0md1gwj13g9")))

(define-public crate-goxoy_tcp_common-0.1.1 (c (n "goxoy_tcp_common") (v "0.1.1") (d (list (d (n "goxoy-address-parser") (r "^0.0.9") (d #t) (k 0)))) (h "1j77xw300qyk0mv7n5wdirnazrdx5hrlgnhdy5sy6sabnk35q72c")))

(define-public crate-goxoy_tcp_common-0.1.2 (c (n "goxoy_tcp_common") (v "0.1.2") (d (list (d (n "goxoy-address-parser") (r "^0.0.9") (d #t) (k 0)))) (h "1pmzkd57fplmiqlhspz5xb3znha09qfqbk4xf28p1wkxqb5sq5b1")))

