(define-module (crates-io go a- goa-oauth2) #:use-module (crates-io))

(define-public crate-goa-oauth2-1.1.0 (c (n "goa-oauth2") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "zbus") (r "^2.0.0-beta.6") (d #t) (k 0)) (d (n "zvariant") (r "^2.8") (d #t) (k 0)))) (h "1ixmkgkgri18g1ffapdi0py7jjb393757a0s2j99b9jn055dbs49")))

(define-public crate-goa-oauth2-1.1.1 (c (n "goa-oauth2") (v "1.1.1") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "zbus") (r "^2.1") (d #t) (k 0)) (d (n "zvariant") (r "^2.8") (d #t) (k 0)))) (h "1h8cr6i5q6ync87bjzjn04x0gc7lx0zv9w5ch6jbhpc166xzy2r4")))

(define-public crate-goa-oauth2-1.1.2 (c (n "goa-oauth2") (v "1.1.2") (d (list (d (n "clap") (r "^4.4") (d #t) (k 0)) (d (n "zbus") (r "^3.15") (d #t) (k 0)) (d (n "zvariant") (r "^3.15") (d #t) (k 0)))) (h "1lnyadrc72w50kr30581wg2x7m6zd16w30ndpqmyg6hjqg41qiva")))

