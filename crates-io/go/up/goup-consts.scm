(define-module (crates-io go up goup-consts) #:use-module (crates-io))

(define-public crate-goup-consts-0.2.3 (c (n "goup-consts") (v "0.2.3") (h "1s630p2qqgck6r6jvs928jqzi7m9bl88y3s3zcccm3ypxmlzzrxv") (y #t) (r "1.70.0")))

(define-public crate-goup-consts-0.2.4 (c (n "goup-consts") (v "0.2.4") (h "1dm7azwmcgvd72i0vxfj4gq4qkjqabxz5pm76d80b26gph9rp83y") (r "1.70.0")))

(define-public crate-goup-consts-0.2.5 (c (n "goup-consts") (v "0.2.5") (h "1vnwayxgnx6a5hf589bcd11pzp7qh7ycpgs7ga0d3q9595zwbz9s") (r "1.70.0")))

(define-public crate-goup-consts-0.2.6 (c (n "goup-consts") (v "0.2.6") (d (list (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "0vy4fgf7ixgcml3wr05qaw4w0ff1dv4263303ffxbpb1q0zgr70m") (y #t) (r "1.70.0")))

(define-public crate-goup-consts-0.2.7 (c (n "goup-consts") (v "0.2.7") (d (list (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "1scxb5v9ncmbql115rlv9vbqw68hfl8s82sny61dj561mljj6daz") (r "1.70.0")))

(define-public crate-goup-consts-0.2.8 (c (n "goup-consts") (v "0.2.8") (d (list (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "09m77jblfpf0y1v83f8jclx6n4f28vj8vm8bl3z2ab2a4x3j5w9w") (r "1.70.0")))

(define-public crate-goup-consts-0.2.9 (c (n "goup-consts") (v "0.2.9") (d (list (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "1i8vv9rl17hkkhriqqpfyl59liiggvgjh3g53ql0nmgd4k0vfg5p") (r "1.70.0")))

(define-public crate-goup-consts-0.3.0 (c (n "goup-consts") (v "0.3.0") (d (list (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "1zij6argj5dmnl0qrbdibfvg377nlfl5pbnffz6zkzx2bv34d9n0") (r "1.70.0")))

