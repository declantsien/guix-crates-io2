(define-module (crates-io go d_ god_mode) #:use-module (crates-io))

(define-public crate-god_mode-0.1.0 (c (n "god_mode") (v "0.1.0") (h "0fc04jia30c046j3n3f11s06lc8s96jz21gi84qpjlcj534lbzw6") (y #t)))

(define-public crate-god_mode-0.1.1 (c (n "god_mode") (v "0.1.1") (h "01qjcrykfzc0zn2ba84wij5fd8hlycx293xck6vck52684bz4w75") (y #t)))

