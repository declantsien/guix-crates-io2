(define-module (crates-io go sh gosh-lbfgs) #:use-module (crates-io))

(define-public crate-gosh-lbfgs-0.1.0 (c (n "gosh-lbfgs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linesearch") (r "^0.1.0") (d #t) (k 0) (p "gosh-linesearch")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 2)))) (h "1iybkfkcyffpw4c4p6mqcbpviqycqy41h89p5gfihdvg3rh0fykl") (f (quote (("adhoc"))))))

