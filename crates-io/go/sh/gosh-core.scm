(define-module (crates-io go sh gosh-core) #:use-module (crates-io))

(define-public crate-gosh-core-0.0.1 (c (n "gosh-core") (v "0.0.1") (d (list (d (n "gchemol") (r "^0.0.38") (d #t) (k 0)) (d (n "gut") (r "^0.0.8") (d #t) (k 0) (p "gchemol-gut")) (d (n "text-parser") (r "^0.0.8") (d #t) (k 0) (p "gchemol-parser")) (d (n "vecfx") (r "^0.0.8") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1cxjcvrawzn6hqvacl81c2k3ziv3h5m3p68j72dyw759nsclrl6s") (y #t)))

(define-public crate-gosh-core-0.0.2 (c (n "gosh-core") (v "0.0.2") (d (list (d (n "gchemol") (r "^0.0.42") (d #t) (k 0)) (d (n "gut") (r "^0.1") (d #t) (k 0) (p "gchemol-gut")) (d (n "text-parser") (r "^0.3") (d #t) (k 0) (p "gchemol-parser")) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "0bd4xr4sgnf0lng4nprgr7wypp65zlqp0pwzmzfv49kz7b045vfi") (f (quote (("adhoc" "gchemol/adhoc" "gut/adhoc" "vecfx/adhoc" "text-parser/adhoc"))))))

(define-public crate-gosh-core-0.1.0 (c (n "gosh-core") (v "0.1.0") (d (list (d (n "gchemol") (r "^0.0.42") (d #t) (k 0)) (d (n "gut") (r "^0.2") (d #t) (k 0) (p "gchemol-gut")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "text-parser") (r "^0.3") (d #t) (k 0) (p "gchemol-parser")) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "0182igf9317m5rrs23sw3zrs9c5z8jk1k7kpfm8j97l8qvbq7yzv") (f (quote (("adhoc" "gchemol/adhoc" "gut/adhoc" "vecfx/adhoc" "text-parser/adhoc"))))))

(define-public crate-gosh-core-0.2.0 (c (n "gosh-core") (v "0.2.0") (d (list (d (n "gchemol") (r "^0.1.3") (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "text-parser") (r "^0.4") (d #t) (k 0) (p "gchemol-parser")) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1bw975mzsqpr2i5xqf48rc2b726fq9wdzd33wxl7943g9w7nzwvf") (f (quote (("adhoc" "gchemol/adhoc" "gut/adhoc" "vecfx/adhoc" "text-parser/adhoc"))))))

