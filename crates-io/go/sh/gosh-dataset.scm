(define-module (crates-io go sh gosh-dataset) #:use-module (crates-io))

(define-public crate-gosh-dataset-0.0.2 (c (n "gosh-dataset") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "parquet") (r "^28.0.0") (d #t) (k 0)) (d (n "parquet_derive") (r "^28.0.0") (d #t) (k 0)) (d (n "polars") (r "^0.25.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "02y5dv2q0ndb057wrcgh4maxbv3a829aq5d4rzw88zr0y58hj01m") (f (quote (("adhoc")))) (y #t)))

(define-public crate-gosh-dataset-0.1.0 (c (n "gosh-dataset") (v "0.1.0") (d (list (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "parquet") (r "^50") (d #t) (k 0)) (d (n "parquet_derive") (r "^50") (d #t) (k 0)))) (h "1fzci79jjd4n98cph46y7ixb1s74kzb434zhmb3xpa2pc320nbzc") (f (quote (("adhoc"))))))

(define-public crate-gosh-dataset-0.2.0 (c (n "gosh-dataset") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.17") (f (quote ("io_parquet_compression" "io_parquet"))) (d #t) (k 0)) (d (n "gchemol") (r "^0.1") (d #t) (k 2)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrow") (r "^0.10") (f (quote ("arrow2-0-17"))) (d #t) (k 0)))) (h "0hliipmaw628p5qyzvgkrdnvmmy01b0h2f39mzr0k4w8m0zys4qm") (f (quote (("adhoc")))) (y #t)))

(define-public crate-gosh-dataset-0.2.1 (c (n "gosh-dataset") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.17") (f (quote ("io_parquet_compression" "io_parquet"))) (d #t) (k 0)) (d (n "gchemol") (r "^0.1") (d #t) (k 2)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrow") (r "^0.10") (f (quote ("arrow2-0-17"))) (d #t) (k 0)))) (h "1sv7sl1yzmbj1ip2igxyhhmnp5awrlsi4k2rd4m5rzp3gnf7ydkd") (f (quote (("adhoc"))))))

