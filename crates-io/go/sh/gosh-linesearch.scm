(define-module (crates-io go sh gosh-linesearch) #:use-module (crates-io))

(define-public crate-gosh-linesearch-0.1.0 (c (n "gosh-linesearch") (v "0.1.0") (d (list (d (n "gut") (r "^0.3") (d #t) (k 0) (p "gchemol-gut")))) (h "0hncly7ng28ip1kaml5i3qa900zaksx2bphz8ymz0g9akckmg9hg") (f (quote (("adhoc"))))))

