(define-module (crates-io go sh gosh_fhirworks2020_docugen) #:use-module (crates-io))

(define-public crate-gosh_fhirworks2020_docugen-0.1.0 (c (n "gosh_fhirworks2020_docugen") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pom") (r "^3.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0f967z7vafa5xdvxjn84bd46740d71z9flnnxaklcbq61hhgg295")))

