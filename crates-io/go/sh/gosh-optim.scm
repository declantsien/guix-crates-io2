(define-module (crates-io go sh gosh-optim) #:use-module (crates-io))

(define-public crate-gosh-optim-0.1.0 (c (n "gosh-optim") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "fire") (r "^0.1") (d #t) (k 0) (p "gosh-fire")) (d (n "gosh-core") (r "^0.1.0") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gosh-database") (r "^0.1") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gosh-model") (r "^0.1") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "lbfgs") (r "^0.1") (d #t) (k 0) (p "gosh-lbfgs")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z1cj1a8g1dfpqnpgj616rjn6iv2avpq0501fz3my03lhxalna4p") (f (quote (("adhoc"))))))

