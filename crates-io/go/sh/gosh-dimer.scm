(define-module (crates-io go sh gosh-dimer) #:use-module (crates-io))

(define-public crate-gosh-dimer-0.2.0 (c (n "gosh-dimer") (v "0.2.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "gut") (r "^0.3.0") (d #t) (k 0) (p "gchemol-gut")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1jj1fby3xirqkx1rgrc2c3p29rh1qvvwcs9qfkjm4ml4c4bss897") (f (quote (("adhoc"))))))

