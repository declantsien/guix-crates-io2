(define-module (crates-io go sh gosh-fire) #:use-module (crates-io))

(define-public crate-gosh-fire-0.1.0 (c (n "gosh-fire") (v "0.1.0") (d (list (d (n "gut") (r "^0.3") (d #t) (k 0) (p "gchemol-gut")) (d (n "linesearch") (r "^0.1") (d #t) (k 0) (p "gosh-linesearch")) (d (n "pyo3") (r "^0.10.1") (d #t) (k 2)) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1ryi0xy5nrli1k3ww4dhf14ib1iqsq8js9zd85322l7h5k3j93p9") (f (quote (("adhoc"))))))

