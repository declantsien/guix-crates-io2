(define-module (crates-io go sh gosh-repl) #:use-module (crates-io))

(define-public crate-gosh-repl-0.1.0 (c (n "gosh-repl") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rustyline") (r "^11") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "1ny6xmgd51qa4cjjh8mwj2xk3pa49zknjgadbz5vvn0skwlmlnx5") (f (quote (("adhoc")))) (y #t)))

(define-public crate-gosh-repl-0.1.1 (c (n "gosh-repl") (v "0.1.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rustyline") (r "^11") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "1r5fdzi9a8zc73w631swhihiq6dqwpd6yq2lz3qdlw6cmmrjbp1k") (f (quote (("adhoc"))))))

(define-public crate-gosh-repl-0.1.2 (c (n "gosh-repl") (v "0.1.2") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rustyline") (r "^11") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "0p3whmnknsl7sf55nc06a481k9qv0lix1b5crff8mx0kq9fkivxs") (f (quote (("adhoc"))))))

(define-public crate-gosh-repl-0.1.3 (c (n "gosh-repl") (v "0.1.3") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rustyline") (r "^11") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "1qw72xzy4kimz0ycj49an9x4rwcrg1cpyl8xg2g7yb8n1sxqycjb") (f (quote (("adhoc"))))))

