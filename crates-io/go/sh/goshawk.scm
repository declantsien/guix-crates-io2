(define-module (crates-io go sh goshawk) #:use-module (crates-io))

(define-public crate-goshawk-0.1.0 (c (n "goshawk") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)))) (h "0dan12rbfw4akm0ix5brqj26njs3avp475va3gp0chlabmrzn9hx")))

(define-public crate-goshawk-0.1.1 (c (n "goshawk") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)))) (h "0rgpb2ihngsmcmnlr7bwrnk7spvg37cx0mrivalcl61ma7g1s050")))

