(define-module (crates-io go -t go-tool-dist-list) #:use-module (crates-io))

(define-public crate-go-tool-dist-list-0.1.0 (c (n "go-tool-dist-list") (v "0.1.0") (h "1p7dgw9lwm540nlkjynr5aqv267bh2wshipyy3lhmbd4406r67wf") (r "1.56.1")))

(define-public crate-go-tool-dist-list-0.1.1 (c (n "go-tool-dist-list") (v "0.1.1") (h "1r9wb37rqr0pfrzlz9gf9jcdinwb4i3c9z6kjd66b6nb061rpb9w") (r "1.56.1")))

(define-public crate-go-tool-dist-list-0.1.2 (c (n "go-tool-dist-list") (v "0.1.2") (h "1jy7g1cd4gdsg9c56i08q8wx4y2s7wigq53qr43gpigmxbl335qd") (r "1.56.1")))

