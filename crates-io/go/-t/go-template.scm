(define-module (crates-io go -t go-template) #:use-module (crates-io))

(define-public crate-go-template-0.0.1 (c (n "go-template") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03h6hvmkk6kxc91mwcl58ffpaj0g02f8qcfcw8fvmb61jch65607") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-go-template-0.0.2 (c (n "go-template") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sdr4rgfydbq2raa8kv77p4ydx79l1lp38cl4l4ixdivilgrigdr") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-go-template-0.0.3 (c (n "go-template") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18gc1pxj0l808gb01q670q2yc5kvkllq0qs50j6cvcwra67iiy8j") (f (quote (("gtmpl_dynamic_template"))))))

