(define-module (crates-io go -t go-types) #:use-module (crates-io))

(define-public crate-go-types-0.1.0 (c (n "go-types") (v "0.1.0") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "01z5wjlrvjyd50x8nr2gfml3hd83yxf6ja0fm2vz89xw0srsp9hc") (f (quote (("default") ("btree_map" "go-parser/btree_map"))))))

(define-public crate-go-types-0.1.1 (c (n "go-types") (v "0.1.1") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1iza6zpnrp7bk0r36qkbg7572d2n7p68f0gvxb3lcjq2qr2abxzm") (f (quote (("default") ("btree_map" "go-parser/btree_map"))))))

(define-public crate-go-types-0.1.4 (c (n "go-types") (v "0.1.4") (d (list (d (n "go-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1wd3iqpbjbw2fpg52wwigik25bl165442jj165pr5c51fk8nl7s6") (f (quote (("default") ("btree_map" "go-parser/btree_map"))))))

(define-public crate-go-types-0.1.5 (c (n "go-types") (v "0.1.5") (d (list (d (n "go-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0gd57v7g0j52q7jfbw2wkl061wa1fnijqpyyrjy5qc737czmqrc7") (f (quote (("default") ("btree_map" "go-parser/btree_map"))))))

