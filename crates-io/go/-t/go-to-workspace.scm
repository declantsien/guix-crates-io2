(define-module (crates-io go -t go-to-workspace) #:use-module (crates-io))

(define-public crate-go-to-workspace-0.1.0 (c (n "go-to-workspace") (v "0.1.0") (d (list (d (n "confy") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "1kf81bdnz0zj4fkihmzmppaapd0i5nax4sj3ldh7vqsn8rq61x0g")))

(define-public crate-go-to-workspace-0.1.1 (c (n "go-to-workspace") (v "0.1.1") (d (list (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1yf9gzkcazc7b3j4nza4j87la55cp44498si3p1a4y8m0syrd0mq")))

(define-public crate-go-to-workspace-0.1.2 (c (n "go-to-workspace") (v "0.1.2") (d (list (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0j9rssdkj1zsw3x2p6wpm674p0r4fh9q36ikaakhkq10qc8zgyna")))

