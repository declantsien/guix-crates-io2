(define-module (crates-io go gg goggled) #:use-module (crates-io))

(define-public crate-goggled-0.0.0 (c (n "goggled") (v "0.0.0") (h "1kj80hkypz25a6c7b9id1d0wc6hjgwc4avyni1fghp28v061dkmx")))

(define-public crate-goggled-0.1.0 (c (n "goggled") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt" "time"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "chrono" "env-filter" "fmt"))) (k 0)) (d (n "x11-dl") (r "^2.21.0") (k 0)) (d (n "zbus") (r "^3.14") (f (quote ("tokio"))) (k 0)))) (h "16kjb402jk4s6ls7nmd518rpfz9vsmgcp5ss9gvd53scrlkrjsgn")))

(define-public crate-goggled-0.1.1 (c (n "goggled") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 1)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grev") (r "^0.1.3") (k 1)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt" "time"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "chrono" "env-filter" "fmt"))) (k 0)) (d (n "x11-dl") (r "^2.21.0") (k 0)) (d (n "zbus") (r "^4.0") (f (quote ("tokio"))) (k 0)))) (h "1k6crb2wrkk6c1n46ks7n41sp91jyncl2zn4mg8xfnlik6whnpry")))

