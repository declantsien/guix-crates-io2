(define-module (crates-io go gg goggles) #:use-module (crates-io))

(define-public crate-goggles-0.1.0 (c (n "goggles") (v "0.1.0") (d (list (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)) (d (n "hibitset") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gwivii6f0yf56pp9bcq5i93xld900wss9n2wdrld6z07n7s32pm")))

(define-public crate-goggles-0.2.0 (c (n "goggles") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)) (d (n "hibitset") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kqrl37v20w30kir2aib3dis0c7qq8xy4l5yskayzwmlak9gly1q") (f (quote (("default" "rayon"))))))

