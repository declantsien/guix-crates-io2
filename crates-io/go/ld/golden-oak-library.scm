(define-module (crates-io go ld golden-oak-library) #:use-module (crates-io))

(define-public crate-golden-oak-library-0.1.0 (c (n "golden-oak-library") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "1p024i7mbwr5psdmzqxfx2gyihf08ir2s2qxknm3fagwg7k3hbwq") (y #t)))

