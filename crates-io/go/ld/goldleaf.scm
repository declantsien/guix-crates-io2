(define-module (crates-io go ld goldleaf) #:use-module (crates-io))

(define-public crate-goldleaf-0.1.0 (c (n "goldleaf") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "goldleaf-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)))) (h "14js02im2f2qzmp5nx3k8pif3qm9j82pfanvdbrhgxm12d9xv9xx")))

(define-public crate-goldleaf-0.1.1 (c (n "goldleaf") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "goldleaf-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)))) (h "04682m7mbgwfhc4jjh4kbmmq8k479b3scd8q6k2b7jak8gklac02")))

(define-public crate-goldleaf-0.1.2 (c (n "goldleaf") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "goldleaf-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)))) (h "0n26ngqlkpfhaqd3lq8l51jjj125rbcv9yx29y1q85bljjqm344l")))

(define-public crate-goldleaf-0.1.3 (c (n "goldleaf") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "goldleaf-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)))) (h "184yq762a0af0k6rr0bxmjyhbfgq49znfl5i154ykrfpav047nwb")))

(define-public crate-goldleaf-0.1.4 (c (n "goldleaf") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "goldleaf-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)))) (h "0d3wsjh4gakqk7x44qrxbxnzl9bsqpxm7cdikgnjrsb3qzz9fkf9")))

