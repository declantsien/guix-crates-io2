(define-module (crates-io go ld goldilocks-ntt) #:use-module (crates-io))

(define-public crate-goldilocks-ntt-0.1.0 (c (n "goldilocks-ntt") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)))) (h "0g5mg1sfbw7ll8jc4kjaqa7qcj7ib9iylqxmf258zgdn732v2yv0")))

