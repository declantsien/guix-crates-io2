(define-module (crates-io go ld golduck) #:use-module (crates-io))

(define-public crate-golduck-0.1.0 (c (n "golduck") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)))) (h "1j9kba8f6c86aml54a7gbz4byjb6gl55jl8xnjxjk750sq846a0c")))

(define-public crate-golduck-0.1.1 (c (n "golduck") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)))) (h "1hipg9i61m2yffkb9m88ngqzr7mrdqhqfv6bqcniafgfcqlk0vbi")))

(define-public crate-golduck-0.1.2 (c (n "golduck") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1v0350rasjz1xzaxcq7kvzv5myxaj6bwpixrpcrdppzqjk7l96b3")))

(define-public crate-golduck-0.1.3 (c (n "golduck") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "175pl6gdpiy9519qrvvxvpaz378gjiinpia3pfn66lai45s0lmaw")))

(define-public crate-golduck-1.0.0 (c (n "golduck") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1b8s9frmqzns7xyy95lc4apwfdj3ikcwv82xlrn93vp649nwi094")))

(define-public crate-golduck-1.0.1 (c (n "golduck") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1p24xkhqy5wcmrl6ps2pzqa467h13n3bcs7ifi939dgd8ah8hnyw")))

