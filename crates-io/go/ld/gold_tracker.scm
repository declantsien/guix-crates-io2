(define-module (crates-io go ld gold_tracker) #:use-module (crates-io))

(define-public crate-gold_tracker-0.1.0 (c (n "gold_tracker") (v "0.1.0") (h "19iqzbnv4hjrg8rhxzdgwcv5cw98wdcx5ic88ljaarrzhqq66w41")))

(define-public crate-gold_tracker-0.1.1 (c (n "gold_tracker") (v "0.1.1") (h "06fl9ahp7ya3ppamq0azwcxm5zgd6sbyd7qzwik2h6v4nijr2fdg")))

(define-public crate-gold_tracker-0.1.2 (c (n "gold_tracker") (v "0.1.2") (h "1551vyj7alnfw36pqzbq6isk1hd71n3lq581zm1cpvh82bvz1my8")))

