(define-module (crates-io go ld golden_apple) #:use-module (crates-io))

(define-public crate-golden_apple-0.1.0 (c (n "golden_apple") (v "0.1.0") (h "12y9rsx6hqxc42mlpyhwzb8fi9ah1ir8ssc6ldasv993pzdjl02z")))

(define-public crate-golden_apple-0.2.0 (c (n "golden_apple") (v "0.2.0") (h "1l9q9irzcq4fj85d367dcxcbns81labcyzih3z9imd5q2klnrbcl")))

(define-public crate-golden_apple-0.2.1 (c (n "golden_apple") (v "0.2.1") (h "1jl3pbjlvx0qk5yv3hsn0igdzampnk8j7qgjlppk3ljm665k49fj")))

(define-public crate-golden_apple-0.3.0 (c (n "golden_apple") (v "0.3.0") (h "0hhlyy9j6p67k83sndvswwcd4d8clc1i74w91kc8wnjr4m569zcg")))

(define-public crate-golden_apple-0.3.1 (c (n "golden_apple") (v "0.3.1") (h "1nlb5glzqhghr9l48jpqjs2wiqa71psx8qpzpc1jak1dfih8wh2g")))

(define-public crate-golden_apple-0.4.0 (c (n "golden_apple") (v "0.4.0") (h "1wsbb8a149c10shw1lx5jimfm6pd2hjbh43frddgcg2bvlfp6kjs")))

(define-public crate-golden_apple-0.5.0 (c (n "golden_apple") (v "0.5.0") (h "10i2gs2k4abr66rcdh5lgh69nk2khc0z7q3f21l07c8ndbqb9jwn")))

(define-public crate-golden_apple-0.6.0 (c (n "golden_apple") (v "0.6.0") (h "1p09cjcbg1ad64nzngsr94l2f6likwkfi1ci84l0n53yd5bdxh42")))

(define-public crate-golden_apple-0.7.0 (c (n "golden_apple") (v "0.7.0") (h "1vvg0dsf8j493vl018nn4nc83vb026awksa64rirb9j6vrql5msn")))

(define-public crate-golden_apple-0.8.0 (c (n "golden_apple") (v "0.8.0") (h "1dkplsbfc25a7sg62hj6dl9hjn193j6pfr8fyyxplk938hm0r4i2")))

(define-public crate-golden_apple-0.9.0 (c (n "golden_apple") (v "0.9.0") (h "04qayzp7rnfvn7683n1cvgkyipv6wwli624js8xdd7d6c71dzn0w")))

(define-public crate-golden_apple-0.10.0 (c (n "golden_apple") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "07909hgq2d5fac7jwbd441311g5awybrwf84mczvfyxxz14h1bvz")))

(define-public crate-golden_apple-0.11.0 (c (n "golden_apple") (v "0.11.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "13li86918vvq6q001mx2qbqk9m9zhlnar45d2lwhdn55rfvs6bhh")))

(define-public crate-golden_apple-0.12.0 (c (n "golden_apple") (v "0.12.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "0m5mys2ji5if6krlr8ygiyyqfc8ypvflv5dcx6mnkfy3bdphbnh4")))

(define-public crate-golden_apple-0.13.0 (c (n "golden_apple") (v "0.13.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "1rdbxx9c24y0n8fgrwbj7r12wjyvkxq00w8pzb6mpkyvkcrhs96g")))

(define-public crate-golden_apple-0.14.0 (c (n "golden_apple") (v "0.14.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "0b6wl2n3cas36zi3y2ngd9ijvk9rjwdf97qc8zrdi3gvzb8zqjrd")))

(define-public crate-golden_apple-0.14.1 (c (n "golden_apple") (v "0.14.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "04lllipxfbzppywyaxq0sp6i5gl7ngl5v3xm3iapz2nxz2cpylym")))

(define-public crate-golden_apple-0.15.0 (c (n "golden_apple") (v "0.15.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "1v5678725yc0r20ifcqn43557ldqpr22gvvjpfbalpjmb7jc69pz")))

(define-public crate-golden_apple-0.16.0 (c (n "golden_apple") (v "0.16.0") (d (list (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "09z1h7iaafklp7zdm8m91yagkp79r7ic0kpapf79hix6rp86i62c")))

(define-public crate-golden_apple-0.17.0 (c (n "golden_apple") (v "0.17.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 1)))) (h "1slwzynpmbnbmwrkyywlbzyqy7zkrkpiyynm201w4i2875gxfiwr")))

(define-public crate-golden_apple-0.17.1 (c (n "golden_apple") (v "0.17.1") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 1)))) (h "0m889qxwlj2fhlixgn50df5hww6yc4g4cd6v2408wgi09zkg7cvb")))

