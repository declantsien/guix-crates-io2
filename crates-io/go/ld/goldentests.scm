(define-module (crates-io go ld goldentests) #:use-module (crates-io))

(define-public crate-goldentests-0.2.0 (c (n "goldentests") (v "0.2.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0zq3rn475y0468lzxpz4girpb1rzs0cyc3qbbc5mpng79xvj7adj")))

(define-public crate-goldentests-0.2.1 (c (n "goldentests") (v "0.2.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "062dj5v4asp51qbgaf6mq1dxqsdf2rxkpljdaxz8hprgwsqhqf8f")))

(define-public crate-goldentests-0.2.2 (c (n "goldentests") (v "0.2.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0j0s2kmawipykabbajxyfczkdac9anhfy37h0px7ban1xyh1wjmi")))

(define-public crate-goldentests-0.3.0 (c (n "goldentests") (v "0.3.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1qxs13is2jlk3nr2d4xicx3ldn60nhr3rbxkaxaj8g4pcr5kmxvk")))

(define-public crate-goldentests-0.3.1 (c (n "goldentests") (v "0.3.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "19gkvm7n2gjshd8m79l48cd9xis8zklswf14lnjar3dir3idvclk")))

(define-public crate-goldentests-0.3.2 (c (n "goldentests") (v "0.3.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0xxva93kanbayj110768syp1mky794s1k0wk1qw5inxm82fll311")))

(define-public crate-goldentests-0.3.3 (c (n "goldentests") (v "0.3.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0ssb00j4xxa5njqgbgz6gh39ncvs8pk2msfsbm54ndppwr7whxj6")))

(define-public crate-goldentests-0.3.4 (c (n "goldentests") (v "0.3.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1s40l1ap040cpmknzwsw3nwj1hyrban9adl5ga66g875plnm1h74")))

(define-public crate-goldentests-0.3.5 (c (n "goldentests") (v "0.3.5") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "13kc2pg1jkq0892bfgnihdspnf2whfdr6n7cnz5gr2bqk7i5bh8r")))

(define-public crate-goldentests-0.3.6 (c (n "goldentests") (v "0.3.6") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "01ny4kz4y1lhhqzyiayr103fln2hafh08wc3zmjybixnaj70q4ky")))

(define-public crate-goldentests-0.3.7 (c (n "goldentests") (v "0.3.7") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0kd4hl2aa5930q48d7lygzadnkqfkvy4dn5bpv2k85gl9x91khil")))

(define-public crate-goldentests-0.3.8 (c (n "goldentests") (v "0.3.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "shlex") (r "^1.0.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "0d24i1g93mfmbx371sy8hqf2l90zrwqp0gzjcny0h8sc9mypyjzb")))

(define-public crate-goldentests-1.0.0 (c (n "goldentests") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)))) (h "1yqyplhfx36dh6qq9h0np26x473cy9as9zjwhkglnhij7chnkvpw") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-goldentests-1.1.0 (c (n "goldentests") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)))) (h "1gy1ri6bzjclf5l5y7v79vr32xkg44ivd0879gpczrlhisl8mzah") (f (quote (("progress-bar" "indicatif") ("parallel" "rayon") ("default" "parallel") ("binary" "parallel" "progress-bar" "clap"))))))

(define-public crate-goldentests-1.1.1 (c (n "goldentests") (v "1.1.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)))) (h "1ifb3n4vymj3b8vrs711nnbgn9zgd2a00h8xpqbzr3vmcxd7hxl4") (f (quote (("progress-bar" "indicatif") ("parallel" "rayon") ("default" "parallel") ("binary" "parallel" "progress-bar" "clap"))))))

