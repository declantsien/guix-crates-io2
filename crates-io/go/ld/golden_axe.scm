(define-module (crates-io go ld golden_axe) #:use-module (crates-io))

(define-public crate-golden_axe-0.1.0 (c (n "golden_axe") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_audio") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1m1n13sfmdl9df5nwzziymil61yg3305fhligvj9lgvpxk9lx2vx")))

