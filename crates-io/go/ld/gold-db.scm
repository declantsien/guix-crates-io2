(define-module (crates-io go ld gold-db) #:use-module (crates-io))

(define-public crate-gold-db-0.1.0 (c (n "gold-db") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xqrs4r7a1ikqw60w1fwhbzby0s40ix7p2fy7vsg4yd4pxi0pcm9")))

(define-public crate-gold-db-0.1.0-2 (c (n "gold-db") (v "0.1.0-2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03scvfm9yc7dhnd0f839hfn0cln0gn91xs8d7bi2akl6fs6qmr7c") (y #t)))

(define-public crate-gold-db-0.1.1 (c (n "gold-db") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14vpzh0j2gs93070s76ss4yiaj2yq0dbgc4j1qvnv5mwvf3jbxp5")))

(define-public crate-gold-db-0.1.2 (c (n "gold-db") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dpqgmhv6lcxy8q4cwrws2xf6vglk25nj132j9x5dwksvyiv4vjq")))

(define-public crate-gold-db-0.2.0 (c (n "gold-db") (v "0.2.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pz08wf3a8693xcqh04w7kadx6bpljs23qws3zyrgqcij9p7v8x4")))

