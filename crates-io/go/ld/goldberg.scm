(define-module (crates-io go ld goldberg) #:use-module (crates-io))

(define-public crate-goldberg-0.1.0 (c (n "goldberg") (v "0.1.0") (d (list (d (n "moisture") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "044prp9pr3s09l5kihxld83i9gzgk6mkms84akik86mszp8wpnfj")))

