(define-module (crates-io go ld goldleaf-derive) #:use-module (crates-io))

(define-public crate-goldleaf-derive-0.1.0 (c (n "goldleaf-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "13hhvfslcq9w63b32r8g8m88zyx8aq337x6fsik3yvvryg1ink8b")))

(define-public crate-goldleaf-derive-0.1.1 (c (n "goldleaf-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "169a12ckk96k25a6x2a6ancjswc8p3f5xl03ais60pyg7k4xs2d1")))

(define-public crate-goldleaf-derive-0.1.2 (c (n "goldleaf-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0k9kshns80spdbzvf9rsfm2kabg9bl9wb57k9z48ci6d1r07vx10")))

(define-public crate-goldleaf-derive-0.1.3 (c (n "goldleaf-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "00iqa77kl6hgfc6j5zvs280am8lz55irhzmczxfgskpdx24qpr2h")))

(define-public crate-goldleaf-derive-0.1.4 (c (n "goldleaf-derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0ar82x7g82n0mqbz74xd1wdzq8izrgwkg0fiylg8df1mdmfahf8h")))

