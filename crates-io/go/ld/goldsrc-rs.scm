(define-module (crates-io go ld goldsrc-rs) #:use-module (crates-io))

(define-public crate-goldsrc-rs-0.7.1 (c (n "goldsrc-rs") (v "0.7.1") (d (list (d (n "byteorder") (r "^1.4.3") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0xl1paqnynygbl17k4wk2n8nik4p5ssmbzm9ihkqwl9cj5fv56ri") (f (quote (("default" "byteorder")))) (y #t)))

(define-public crate-goldsrc-rs-0.7.2 (c (n "goldsrc-rs") (v "0.7.2") (d (list (d (n "byteorder") (r "^1.4.3") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0v2jms0lagddcng3rqcvfzrbdrin1534nzhmyj0vw9xaz14ffnmf") (f (quote (("default" "byteorder")))) (y #t)))

(define-public crate-goldsrc-rs-0.7.3 (c (n "goldsrc-rs") (v "0.7.3") (d (list (d (n "byteorder") (r "^1.4.3") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1c0vxx0pnp54sjg65v9q3kfxp3f21yk82wmjabqn84zh5l19bf4x") (f (quote (("default" "byteorder"))))))

(define-public crate-goldsrc-rs-0.8.0 (c (n "goldsrc-rs") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "smol_str") (r "^0.1.23") (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "16j27l2zysgz3pxvkqh2ldp7vjxlb0rvmffd1vrcsmwnjb8gy0ib")))

(define-public crate-goldsrc-rs-0.9.0 (c (n "goldsrc-rs") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0r3srdck7a1lsvgqdmzm47dsx9irnfhc0lbswh6iz28w24d835rs") (y #t)))

(define-public crate-goldsrc-rs-0.9.1 (c (n "goldsrc-rs") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (d #t) (k 2)) (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "10jdav3h99jlgdy4kw4mpnxsiajyl2bwzg40f8gcs4d45xzk8chk") (y #t)))

(define-public crate-goldsrc-rs-0.10.0 (c (n "goldsrc-rs") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "smallstr") (r "^0.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "17m0lyvqgx700rxbvcykmz7gccghn2kyz1smy16qsn6cbxn5ds0c")))

