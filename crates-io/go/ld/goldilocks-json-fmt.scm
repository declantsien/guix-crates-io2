(define-module (crates-io go ld goldilocks-json-fmt) #:use-module (crates-io))

(define-public crate-goldilocks-json-fmt-0.1.0 (c (n "goldilocks-json-fmt") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0h93km2k3sr25nq0xcgh9pycipzhwfv16g9njanc57423fmkp2z8")))

(define-public crate-goldilocks-json-fmt-0.2.0 (c (n "goldilocks-json-fmt") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0rnn3d7r1a9lpwap0gspqbhw1qb5zjwxpnbn4qinw7qa4amcx29p")))

