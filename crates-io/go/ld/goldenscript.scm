(define-module (crates-io go ld goldenscript) #:use-module (crates-io))

(define-public crate-goldenscript-0.1.0 (c (n "goldenscript") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.38") (d #t) (k 2)) (d (n "dateparser") (r "~0.2.1") (d #t) (k 2)) (d (n "goldenfile") (r "~1.7.1") (d #t) (k 0)) (d (n "nom") (r "~7.1.3") (d #t) (k 0)) (d (n "test_each_file") (r "~0.3.2") (d #t) (k 2)))) (h "0wljfvqxnl8qdw0885l6pfk7bddd6089nmsy9rpd92fiq88zjpv9")))

(define-public crate-goldenscript-0.2.0 (c (n "goldenscript") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "dateparser") (r "^0.2.1") (d #t) (k 2)) (d (n "goldenfile") (r "^1.5") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "test_each_file") (r "^0.3.2") (d #t) (k 2)))) (h "0cr5mmxi6aqczy463ras3ibvkwwb59rfwdb7hmg2jvk97la1f6r2")))

(define-public crate-goldenscript-0.3.0 (c (n "goldenscript") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "dateparser") (r "^0.2.1") (d #t) (k 2)) (d (n "goldenfile") (r "^1.5") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "test_each_file") (r "^0.3.2") (d #t) (k 2)))) (h "1fyfzvnqya3k1nq0m635vdxq5xnm7g51zqvdxrw6bpdn3y4fys81")))

(define-public crate-goldenscript-0.4.0 (c (n "goldenscript") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "dateparser") (r "^0.2.1") (d #t) (k 2)) (d (n "goldenfile") (r "^1.5") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "test_each_file") (r "^0.3.2") (d #t) (k 2)))) (h "1mk7snv73h3k19qnbn8cxqdhdj5irib2nhg92ycm0ia8ks384qx2")))

