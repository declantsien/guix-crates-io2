(define-module (crates-io go ld goldengrove) #:use-module (crates-io))

(define-public crate-goldengrove-0.1.0 (c (n "goldengrove") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "1xsclcbs0s643hfg33hmxm96mq6812hraz15iba1rm0h9jhg4xkw")))

