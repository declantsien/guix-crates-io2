(define-module (crates-io go ld goldboot-macros) #:use-module (crates-io))

(define-public crate-goldboot-macros-0.0.1 (c (n "goldboot-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "00kmg5ylxk96im9hv9by4f0mbvm8b6307nk0mkn4p4sdj9vhwvdn") (r "1.70")))

(define-public crate-goldboot-macros-0.0.2 (c (n "goldboot-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0l1lrl1b1m2sa76jnvr5pl8ywf4nikaz9j3pnkyph9y8wvih5x4r") (r "1.74")))

