(define-module (crates-io go ld goldenfile) #:use-module (crates-io))

(define-public crate-goldenfile-0.4.0 (c (n "goldenfile") (v "0.4.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0g3d48kv3k764sssrck42h0chx5gi1qc0kq2hlh1268ikniri8rp")))

(define-public crate-goldenfile-0.5.0 (c (n "goldenfile") (v "0.5.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "09aw8w4377kadhsjz7l808w8xmbksp4x0mlc9xg7hn03dj7zjzcl")))

(define-public crate-goldenfile-0.5.1 (c (n "goldenfile") (v "0.5.1") (d (list (d (n "difference") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1n3b0bj90s60xiq1krhspg1bjnaavhix4n1wl1i92wfa1wry9wnr")))

(define-public crate-goldenfile-0.6.0 (c (n "goldenfile") (v "0.6.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "156ps9j3qgann69v2ffs3n8vqnlfs599n68cccdqwvwvv8rbs7qm")))

(define-public crate-goldenfile-0.7.0 (c (n "goldenfile") (v "0.7.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "004lxg7lppbgkk6sjhv4w65yzb7ha7xqqkqswwmlr1x4im1s4anl")))

(define-public crate-goldenfile-0.7.1 (c (n "goldenfile") (v "0.7.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1n9mvdx0mqj99crkcnlxkk35g06s5f52s78v2xjs0dvxz6j4iqrk")))

(define-public crate-goldenfile-1.0.0 (c (n "goldenfile") (v "1.0.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "08ccngdspp6zx11vk8bza6kirklqlgllismvibi38b2fclvc2fh7")))

(define-public crate-goldenfile-1.0.1 (c (n "goldenfile") (v "1.0.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0mqrsb0z9zrzyfqp624j0cpj8ajjlnv95bzxbckzi5x71c8m51ds")))

(define-public crate-goldenfile-1.0.2 (c (n "goldenfile") (v "1.0.2") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "03vpahbji099appif2dlizqgpgnjmbpz7aw96vj1q34qw2rs9zzg")))

(define-public crate-goldenfile-1.0.3 (c (n "goldenfile") (v "1.0.3") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0z6jjh6jm01pgd8vf80rwq5k0haavq9bjfy2cr7v6a13qs133k6a")))

(define-public crate-goldenfile-1.1.0 (c (n "goldenfile") (v "1.1.0") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1wj5h7j6j3r9zzmg3r2pnqdampjgxy7dsdhdlywz01hcsyjfciiz")))

(define-public crate-goldenfile-1.2.0 (c (n "goldenfile") (v "1.2.0") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0v5a63brj67pymixs0v4z18585d58nglghcv1np57k4z508ancip")))

(define-public crate-goldenfile-1.3.0 (c (n "goldenfile") (v "1.3.0") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "082fgmyhp1vyc6hw2d87ciid7wax3gxmxpkphgm40cqr23m3p380")))

(define-public crate-goldenfile-1.4.0 (c (n "goldenfile") (v "1.4.0") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0zjlvwhc0wyzmf0dpiqxyd76drphqswh7j2mn1pgmyiyrz9an3ci")))

(define-public crate-goldenfile-1.4.1 (c (n "goldenfile") (v "1.4.1") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "13zl0gbw40la3swvly4v9cc56xk5znhaylavqfgg1vjkz985ljjq")))

(define-public crate-goldenfile-1.4.2 (c (n "goldenfile") (v "1.4.2") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1snk8bpd633182gk289gm0c8ggnqyjf55kk7d3sm1f7h12cis62h")))

(define-public crate-goldenfile-1.4.3 (c (n "goldenfile") (v "1.4.3") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "02wr3h959b0207sfzgdv9iagmfsnanwxc5khsdly4v525sf0xg83")))

(define-public crate-goldenfile-1.4.4 (c (n "goldenfile") (v "1.4.4") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0436sb24yalfgk3ps6hpaf5x5c4pdlwsv0rvs1i2cwx0a08k9jr3")))

(define-public crate-goldenfile-1.4.5 (c (n "goldenfile") (v "1.4.5") (d (list (d (n "similar-asserts") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0nrhf5h1rv727zmm9pjy4zh8fnxh2aiflgp0jywn0sbad9v6c7mz")))

(define-public crate-goldenfile-1.5.0 (c (n "goldenfile") (v "1.5.0") (d (list (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0p1i3hz2p5m245yhzpk0y07x9lfddshmrhf8qn6nlmlv786cq95k")))

(define-public crate-goldenfile-1.5.1 (c (n "goldenfile") (v "1.5.1") (d (list (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1h8gl1f2rrwb8qin4mj6l2kpxc8ahn5ap5x5ks4nyg9s9y2w1qs7")))

(define-public crate-goldenfile-1.5.2 (c (n "goldenfile") (v "1.5.2") (d (list (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "04qp53fspi325k3hf3hsi4fl8v591nl0ibbb1m2xa75azxljwd46")))

(define-public crate-goldenfile-1.6.0 (c (n "goldenfile") (v "1.6.0") (d (list (d (n "scopeguard") (r "^1.2") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0q759dlr9pvgp8kfsjxhkymyqpnpxr7zvnmf2f1bsn5kld9p99p4")))

(define-public crate-goldenfile-1.6.1 (c (n "goldenfile") (v "1.6.1") (d (list (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "yansi") (r "^1") (d #t) (k 0)))) (h "021dqhkpxlb4jd65dfv5cz5lyhijs5cqa9hk4n7dhj3waiywvx8a")))

(define-public crate-goldenfile-1.7.0 (c (n "goldenfile") (v "1.7.0") (d (list (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "yansi") (r "^1") (d #t) (k 0)))) (h "0hhl09c4cfn7c3fmcrb0bamvg83anjm7m8bn3kwiqk0wl4xwv9p3") (y #t)))

(define-public crate-goldenfile-1.7.1 (c (n "goldenfile") (v "1.7.1") (d (list (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "yansi") (r "^1") (d #t) (k 0)))) (h "11y5wfsnhaghhg2crynpqgw2i3h6kz7b95wwl4765v5scm1c9md0")))

