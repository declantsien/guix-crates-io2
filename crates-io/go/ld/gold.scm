(define-module (crates-io go ld gold) #:use-module (crates-io))

(define-public crate-gold-0.0.0 (c (n "gold") (v "0.0.0") (h "01d841ljmlj22fxjxhh3sfnch1mx50ds8wx9b5r9jqj65w5yq2wn")))

(define-public crate-gold-0.0.1 (c (n "gold") (v "0.0.1") (h "18ssf266133z71wks3zfwz2g14ppwdmjg4fivv1vsdq5lfayfva1")))

