(define-module (crates-io go ff goffin) #:use-module (crates-io))

(define-public crate-goffin-0.1.0 (c (n "goffin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ns2mb1b5akd4i28yg8hwzyrp9nhi8ny2337v2vd25n8g9k9m3aj")))

