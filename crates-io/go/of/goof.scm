(define-module (crates-io go of goof) #:use-module (crates-io))

(define-public crate-goof-0.1.0 (c (n "goof") (v "0.1.0") (h "09c96ks6sq26wbd5vqx1axvdp45p9jbjd25llvkyfv0n9myc76x6")))

(define-public crate-goof-0.2.0 (c (n "goof") (v "0.2.0") (h "1y4n4apk9z26c17rbyv6cgcs9y8lk0gk6sbjdcrbdbb4wa8grzj5")))

(define-public crate-goof-0.2.1 (c (n "goof") (v "0.2.1") (h "1jx64l5d47dz4y4aysbiywj8dbvmbafpd7jm1bp41s4f6fcib2q8")))

(define-public crate-goof-0.2.2 (c (n "goof") (v "0.2.2") (h "14ch42lhhvm2dds8rfb0psxihms9xwja56pblpwz9vkn1hhkzhly")))

(define-public crate-goof-0.2.3 (c (n "goof") (v "0.2.3") (h "1p5fyqqnjkvy80mg7qqsc4li2klj20wmfwdrdcnfnplmj98lkzlk")))

