(define-module (crates-io go sc goscript-types) #:use-module (crates-io))

(define-public crate-goscript-types-0.1.0 (c (n "goscript-types") (v "0.1.0") (d (list (d (n "goscript-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "slotmap") (r "^0.4") (d #t) (k 0)))) (h "01vxma0vxk8cabvn4z0m80m7dq2p49yl1r7mvrja0ac8i12khllr")))

