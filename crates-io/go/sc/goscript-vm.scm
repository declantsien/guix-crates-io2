(define-module (crates-io go sc goscript-vm) #:use-module (crates-io))

(define-public crate-goscript-vm-0.1.0 (c (n "goscript-vm") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-executor") (r "^1.4.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "goscript-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "slotmap") (r "^0.4") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 0)))) (h "0280hh96xz764wppcgrd0fxisjlhab28fid10j9pvl07gxx9hk99")))

