(define-module (crates-io go sc goscript-codegen) #:use-module (crates-io))

(define-public crate-goscript-codegen-0.1.0 (c (n "goscript-codegen") (v "0.1.0") (d (list (d (n "goscript-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "goscript-types") (r "^0.1.0") (d #t) (k 0)) (d (n "goscript-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "slotmap") (r "^0.4") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 0)))) (h "1awdm764nlqz9fs542nqgqf9paq0vd91xnv626y7hwaphv0vacgr")))

