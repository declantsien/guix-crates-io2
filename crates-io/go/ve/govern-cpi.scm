(define-module (crates-io go ve govern-cpi) #:use-module (crates-io))

(define-public crate-govern-cpi-0.1.0 (c (n "govern-cpi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0lnbvd3d9yqkm34ln361hynnnhllflrx34rc5q8h2d9983h3vg1b")))

(define-public crate-govern-cpi-0.1.1 (c (n "govern-cpi") (v "0.1.1") (d (list (d (n "anchor-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "14525qr5x5r0x1ds3m3594bicsfwl49gs48nsi0kb147spa7m916")))

(define-public crate-govern-cpi-0.1.2 (c (n "govern-cpi") (v "0.1.2") (d (list (d (n "anchor-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "04wccmw2cpq79irh24haq1n0lk3yvvlyb06ix58ri935z5lzm16z") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-govern-cpi-0.2.0 (c (n "govern-cpi") (v "0.2.0") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "1gacbaafi7bnrkwq34lqarj9n3dlc0z8zq8v4p7jrjw9z8w2rvv2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-govern-cpi-0.3.0 (c (n "govern-cpi") (v "0.3.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0if8fr9pfj7a3f8nxcff82j26m38h071hkhqmklx8dqj0zixgpc4") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

