(define-module (crates-io go ds godsays) #:use-module (crates-io))

(define-public crate-godsays-0.1.0 (c (n "godsays") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "05nc2z6507ghnxi8yqnpcc1albl6hmhb85ihjl1fhhypaip2j9yh") (f (quote (("server" "hyper" "tokio" "pretty_env_logger"))))))

(define-public crate-godsays-0.1.2 (c (n "godsays") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0np9g6496dc591gsafl8mdb4b6bjvvbni1x0f98xj1ks0if0ny0g") (f (quote (("server" "hyper" "tokio" "pretty_env_logger"))))))

(define-public crate-godsays-0.1.4 (c (n "godsays") (v "0.1.4") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^6.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0x1m2zigw7scjk7lpihbnra010gnlrfsgmbg2ppn0qvsfz5rmxrz") (f (quote (("server" "hyper" "tokio" "pretty_env_logger"))))))

