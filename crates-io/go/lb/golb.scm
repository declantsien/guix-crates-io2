(define-module (crates-io go lb golb) #:use-module (crates-io))

(define-public crate-golb-0.1.0 (c (n "golb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "upon") (r "^0.6.0") (d #t) (k 0)))) (h "0c88b2c9n2fad4m918nkys9jw9l12zccsv297g9qk18j821814i2")))

