(define-module (crates-io go b_ gob_archive) #:use-module (crates-io))

(define-public crate-gob_archive-1.0.0 (c (n "gob_archive") (v "1.0.0") (d (list (d (n "gob_rs") (r "^1.1.0") (d #t) (k 0)))) (h "07v1cvazbr5qjkdpwmdfl1iqk0wbx82blr9i2ap3xjy4smh7f5my")))

(define-public crate-gob_archive-1.0.1 (c (n "gob_archive") (v "1.0.1") (d (list (d (n "gob_rs") (r "^1.2.0") (d #t) (k 0)))) (h "0ycifgglp9lpqyfiiww9vkhkd6k5l8ab9ahsk9py80qg4ar5iy5b")))

(define-public crate-gob_archive-1.0.2 (c (n "gob_archive") (v "1.0.2") (d (list (d (n "gob_rs") (r "^1.2.1") (d #t) (k 0)))) (h "1jbqfpgbm5i4vqh2d5ahzng672bwbw2y0y97mg3z90x5zhklj4mq")))

