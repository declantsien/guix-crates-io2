(define-module (crates-io go b_ gob_rs) #:use-module (crates-io))

(define-public crate-gob_rs-1.0.0 (c (n "gob_rs") (v "1.0.0") (h "1w4pvnmi9ncn5pf1hw5xzzvksj93aj6vb921901b3pc9jsw7r5y6") (y #t)))

(define-public crate-gob_rs-1.1.0 (c (n "gob_rs") (v "1.1.0") (h "1xig2pvhj3lfdcgypkyafybkxgx2xj5rz88xcx58gx3isg1a6rsv")))

(define-public crate-gob_rs-1.2.0 (c (n "gob_rs") (v "1.2.0") (h "1mz9w3rx1dsaydih866fnvb1fx7lqqgg66bg05pn2205424k48lm")))

(define-public crate-gob_rs-1.2.1 (c (n "gob_rs") (v "1.2.1") (h "04989q6kfmvdfnk3nnwmdj577rwjiy2kraxbaazry9j94s2pa20y")))

(define-public crate-gob_rs-1.3.0 (c (n "gob_rs") (v "1.3.0") (h "1j8j1pj6fsxmay5vnqcddagvvpjsl277xl9h12qj05awnvsgb764")))

