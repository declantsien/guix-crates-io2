(define-module (crates-io go ro goro) #:use-module (crates-io))

(define-public crate-goro-0.1.0 (c (n "goro") (v "0.1.0") (h "1qx95vh86c52nbavdanf9w22h6v787z6sdzindwz2ygilyg2pmjn")))

(define-public crate-goro-0.1.1 (c (n "goro") (v "0.1.1") (h "1266rbbsh37n0qmldnsrx8qz4n6r4h3p7rwhgb60rnncwv3iq033")))

(define-public crate-goro-0.2.0 (c (n "goro") (v "0.2.0") (h "182pbvrknliqgpmxmv30f08sm3bsj382g601ippkcs8gbq6bqlvv")))

(define-public crate-goro-0.2.1 (c (n "goro") (v "0.2.1") (h "1jgr7wcj1wjc7aln2lrd6bxzz81afyxs4g0h6dv2czbra28gi47q")))

