(define-module (crates-io go og google-dataflow1_b4) #:use-module (crates-io))

(define-public crate-google-dataflow1_b4-0.1.3+20150401 (c (n "google-dataflow1_b4") (v "0.1.3+20150401") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0h12pg887asxcmc8xg5a0lr753gcbjw304pdcgcsxkg59a4lwjq6")))

(define-public crate-google-dataflow1_b4-0.1.4+20150401 (c (n "google-dataflow1_b4") (v "0.1.4+20150401") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "04997p7ak71kiqycxslwzza0nnnsaxjaixlj5h221rp9p0n0s5w4")))

(define-public crate-google-dataflow1_b4-0.1.5+20150401 (c (n "google-dataflow1_b4") (v "0.1.5+20150401") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "002b9rz8nbphbplir4hdaskqzyiyyk73rkc77g6kd6mq0v9i6b68")))

