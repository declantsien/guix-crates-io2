(define-module (crates-io go og google-cloud-service-flows) #:use-module (crates-io))

(define-public crate-google-cloud-service-flows-0.1.0 (c (n "google-cloud-service-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "09v3xb56mnjqg369g211ziijqly0zammb4vvzmj0n6jvpfrqk9hk")))

