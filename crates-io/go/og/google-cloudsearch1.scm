(define-module (crates-io go og google-cloudsearch1) #:use-module (crates-io))

(define-public crate-google-cloudsearch1-0.1.3+20150309 (c (n "google-cloudsearch1") (v "0.1.3+20150309") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0hjmyjglh3p249rsk3gx170jd7bssyy5h4a3299jcl0yqx1j3fxm")))

(define-public crate-google-cloudsearch1-0.1.4+20150309 (c (n "google-cloudsearch1") (v "0.1.4+20150309") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "016j4x43w29m7g8wfsgfwhw6rbc9300k80x7cwp6ksh1kbd9mjhm")))

(define-public crate-google-cloudsearch1-0.1.5+20150309 (c (n "google-cloudsearch1") (v "0.1.5+20150309") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "00blcyb41vdvjrc474594db83whjf76s9j338mx7ngab3ppss4by")))

(define-public crate-google-cloudsearch1-0.1.6+20150416 (c (n "google-cloudsearch1") (v "0.1.6+20150416") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1xgn5ld8wwqlb76qzk203ng7mj4qgr922959i4p5grkbiijb2rb5")))

