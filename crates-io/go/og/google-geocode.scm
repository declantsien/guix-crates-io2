(define-module (crates-io go og google-geocode) #:use-module (crates-io))

(define-public crate-google-geocode-0.1.0 (c (n "google-geocode") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11j17amfvqfambx7sszb0pzgl4dhr5svw3q6rw9zma5n2dxlcr47")))

(define-public crate-google-geocode-0.1.1 (c (n "google-geocode") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d8hrmv4f97r09lsvmfg92s0lfyqaxchbih8ykgdm6zfrwykpamm")))

(define-public crate-google-geocode-0.1.2 (c (n "google-geocode") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qbrs4nnirdb5xv6rc1748v7hcq8imqzg1nknp2z7c8zdhan9gl2")))

(define-public crate-google-geocode-0.1.3 (c (n "google-geocode") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vby4gk2jdmnylf4mhcg4fk1x550znynym89s4w79ia72nr5js8s")))

(define-public crate-google-geocode-0.1.4 (c (n "google-geocode") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zwi68d0wdyakpblxr279ihk4a7czvjjxq3zfx25d2vazh736vjw")))

(define-public crate-google-geocode-0.1.5 (c (n "google-geocode") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jwqardgyvz8wxvcr6jwhza07gdbm586i7dpfksflwk5zrwk5x6n")))

