(define-module (crates-io go og google-genomics1_beta2) #:use-module (crates-io))

(define-public crate-google-genomics1_beta2-0.1.0+20150303 (c (n "google-genomics1_beta2") (v "0.1.0+20150303") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0v6qlnmk8yck6sr9gnnrnpx22dlymhanvl7ryxkwjq6fd2nqjfb1")))

(define-public crate-google-genomics1_beta2-0.1.1+20150303 (c (n "google-genomics1_beta2") (v "0.1.1+20150303") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "152a8c7wjigwy67y22i27rq9wq5qd822c31zq7ii6rgs6jdrx4b0")))

(define-public crate-google-genomics1_beta2-0.1.2+20150317 (c (n "google-genomics1_beta2") (v "0.1.2+20150317") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1fn9zh7h0rsrx3xy2dv9jj9gwjkl4jvyqph4rxq5x5hncf3qla74")))

(define-public crate-google-genomics1_beta2-0.1.3+20150326 (c (n "google-genomics1_beta2") (v "0.1.3+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "05v3y6568n7wpvlqn8l6lq3v18a0g2124wda02d0bac2jj7qy71i")))

(define-public crate-google-genomics1_beta2-0.1.4+20150326 (c (n "google-genomics1_beta2") (v "0.1.4+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "04wiqlar4ragpagk62g9nkjm4gwr3f05a0pg5xkpgq0l952bmzwq")))

(define-public crate-google-genomics1_beta2-0.1.5+20150326 (c (n "google-genomics1_beta2") (v "0.1.5+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "18my2ps8vw3ip2vxgpiw251k38xq523r3hv99aq4q07gb1d0w82h")))

(define-public crate-google-genomics1_beta2-0.1.6+20150326 (c (n "google-genomics1_beta2") (v "0.1.6+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "07qsnlac6xypnsx4q1591r86qxibl2rfgmiyy3z60nb00fkc8qn2")))

(define-public crate-google-genomics1_beta2-0.1.7+20150326 (c (n "google-genomics1_beta2") (v "0.1.7+20150326") (d (list (d (n "hyper") (r ">= 0.4.0") (d #t) (k 0)) (d (n "json-tools") (r ">= 0.3.0") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0bj1ncd2xn8hdyrb3gkfj6y3vda95b3la1ll4kimv75hdwx2z123")))

(define-public crate-google-genomics1_beta2-0.1.8+20150326 (c (n "google-genomics1_beta2") (v "0.1.8+20150326") (d (list (d (n "hyper") (r ">= 0.5.2") (d #t) (k 0)) (d (n "mime") (r "^0.0.11") (d #t) (k 0)) (d (n "serde") (r ">= 0.4.1") (d #t) (k 0)) (d (n "serde_codegen") (r "*") (d #t) (k 1)) (d (n "syntex") (r "*") (d #t) (k 1)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0ay3nbl8y3q3vq38wmqn6cbwl4kcz4ssk7bxskd3vfrcdr7zslm8")))

