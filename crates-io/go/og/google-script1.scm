(define-module (crates-io go og google-script1) #:use-module (crates-io))

(define-public crate-google-script1-0.1.9+20150922 (c (n "google-script1") (v "0.1.9+20150922") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "mime") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r ">= 0.6.0") (d #t) (k 0)) (d (n "serde_codegen") (r "*") (d #t) (k 1)) (d (n "serde_json") (r ">= 0.6.0") (d #t) (k 0)) (d (n "syntex") (r "*") (d #t) (k 1)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0dpn8pwy15a18a3lnvxm2lq2gjskgml6xsx98xql7y8zzjp29cp8")))

