(define-module (crates-io go og google-cloud-grpc) #:use-module (crates-io))

(define-public crate-google-cloud-grpc-0.1.0 (c (n "google-cloud-grpc") (v "0.1.0") (d (list (d (n "google-cloud-auth") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (f (quote ("tls" "prost"))) (d #t) (k 0)))) (h "0ryrd9515iijbjid7z6sxm5bqmpkqxbriim94qp26zkc29x8dkn3")))

(define-public crate-google-cloud-grpc-0.2.0 (c (n "google-cloud-grpc") (v "0.2.0") (d (list (d (n "google-cloud-auth") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (f (quote ("tls" "prost"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (f (quote ("filter"))) (d #t) (k 0)))) (h "1zsvd5z2pk25wxkkncgam9cwlcz4fhy4k03d607hb09qnzjd1vgs")))

(define-public crate-google-cloud-grpc-0.2.1 (c (n "google-cloud-grpc") (v "0.2.1") (d (list (d (n "google-cloud-auth") (r "^0.1.2") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (f (quote ("tls" "prost"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (f (quote ("filter"))) (d #t) (k 0)))) (h "1rij873q3qyzx5mmn2bcchqlvq80fd2r1760fs7sr7dm7yjmx48j")))

