(define-module (crates-io go og google-clis-common) #:use-module (crates-io))

(define-public crate-google-clis-common-4.0.0 (c (n "google-clis-common") (v "4.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^7.0") (d #t) (k 0)))) (h "0k91m44wychlp4yxl08j73x98nr4nx2772m3nvi7ak1ssxv7am4y")))

(define-public crate-google-clis-common-5.0.0 (c (n "google-clis-common") (v "5.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^7.0") (d #t) (k 0)))) (h "0282p9ky1njpdwcgcigcmjw99agrzcq42fix9fi7isglpblsj0pn")))

(define-public crate-google-clis-common-6.0.0 (c (n "google-clis-common") (v "6.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^8.2") (d #t) (k 0)))) (h "1bjnc79s417i2z9fsh069q1j25ynxvhy9jsb7wrmpcdqg78sflq6")))

