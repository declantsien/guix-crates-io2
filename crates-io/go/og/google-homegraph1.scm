(define-module (crates-io go og google-homegraph1) #:use-module (crates-io))

(define-public crate-google-homegraph1-1.0.14+20200703 (c (n "google-homegraph1") (v "1.0.14+20200703") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "0lzm6l44nwzapbzvlk46xa4k41mhvwww0g1bc6w3jclbbfgch4hl") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

