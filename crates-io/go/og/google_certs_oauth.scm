(define-module (crates-io go og google_certs_oauth) #:use-module (crates-io))

(define-public crate-google_certs_oauth-0.1.0 (c (n "google_certs_oauth") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "01a6kz1nhxnprkz80pvk6z02mscdhp6nbavnrp9mvkmww7cn8zim") (y #t)))

(define-public crate-google_certs_oauth-0.1.1 (c (n "google_certs_oauth") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "02mflrwsm85mgqxgyj515mafzdqq5zs64nv22jabv8hraggp2n88") (y #t)))

(define-public crate-google_certs_oauth-0.1.3 (c (n "google_certs_oauth") (v "0.1.3") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0iyn4srn9wkh1zik10pf471ic21kcblja1d6h22qfdc16f76mqm0")))

(define-public crate-google_certs_oauth-0.1.4 (c (n "google_certs_oauth") (v "0.1.4") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "18hz4wfs70af5mwkh0lh9nv89rda44krfrmz3pl259r03kfpj7ax")))

