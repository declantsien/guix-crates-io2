(define-module (crates-io go og google-dns-rs) #:use-module (crates-io))

(define-public crate-google-dns-rs-0.1.0 (c (n "google-dns-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (f (quote ("json" "charset" "tls"))) (d #t) (k 0)))) (h "08da8mjvp9811rxhlqx464b75hqq0sh0c2j3gkhn5jxph9szblw6")))

(define-public crate-google-dns-rs-0.2.0 (c (n "google-dns-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (f (quote ("json" "charset" "tls"))) (d #t) (k 0)))) (h "1646psxkn1vwhnmam4423yy2k5zaqsh5g4c0085v6pjm1hjmvg8c")))

(define-public crate-google-dns-rs-0.3.0 (c (n "google-dns-rs") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (f (quote ("json" "charset" "tls"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5") (d #t) (k 2)))) (h "0m0q3yyak5ny5qwx6jkhq6f2dhswijbzwiprwigjpw8dd1hic8vm")))

