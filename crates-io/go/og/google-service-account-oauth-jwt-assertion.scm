(define-module (crates-io go og google-service-account-oauth-jwt-assertion) #:use-module (crates-io))

(define-public crate-google-service-account-oauth-jwt-assertion-0.1.0 (c (n "google-service-account-oauth-jwt-assertion") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "google-service-account-json-key") (r "^0.1") (o #t) (k 0)) (d (n "jsonwebtoken") (r "^8") (f (quote ("use_pem"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "0wpp57jkgga568qsh9w39rbg5b5pvl951zfs4qjgn7igwhs03lj1") (f (quote (("default"))))))

