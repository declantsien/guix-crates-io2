(define-module (crates-io go og googleplay-protobuf) #:use-module (crates-io))

(define-public crate-googleplay-protobuf-0.1.0 (c (n "googleplay-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.23") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.23") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1gwzbd54nqpmnr79hnp2h207dag41252yx0drnjfmw5628qn6j7j") (f (quote (("with-serde"))))))

(define-public crate-googleplay-protobuf-1.0.0 (c (n "googleplay-protobuf") (v "1.0.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)))) (h "18kc0q3bh25c7j15p733ifjkg93m5zng0f6lkvqph699a8ng2q3g")))

(define-public crate-googleplay-protobuf-1.0.1 (c (n "googleplay-protobuf") (v "1.0.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)))) (h "10a8gxj0k0cwyd6mj3mjvf61h10xykiy5w8i02wv7ps0bsxabhn3")))

(define-public crate-googleplay-protobuf-2.0.0 (c (n "googleplay-protobuf") (v "2.0.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 1)))) (h "1mjjjs903ix1jafwlw7gm55r76qk9zxlh4s7jqb0yidvbziq94px")))

