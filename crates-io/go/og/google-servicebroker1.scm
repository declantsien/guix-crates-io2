(define-module (crates-io go og google-servicebroker1) #:use-module (crates-io))

(define-public crate-google-servicebroker1-1.0.7+20180924 (c (n "google-servicebroker1") (v "1.0.7+20180924") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 0.5") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "0qsg1imwhrxvy13sc5bh0i3nlbk101yhf25j10z6w5pm5f2plbjw")))

(define-public crate-google-servicebroker1-1.0.8+20180924 (c (n "google-servicebroker1") (v "1.0.8+20180924") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 0.5") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "1745mwiwfax1sj9s5vabzdgh4nkiq6dqv22pznva549770kl5kk4")))

(define-public crate-google-servicebroker1-1.0.9+20190624 (c (n "google-servicebroker1") (v "1.0.9+20190624") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "03i1zyp3b20g39jsxy57sld9h6kpa5xi149mpiyw6qmh9c89ff1j")))

(define-public crate-google-servicebroker1-1.0.10+20190624 (c (n "google-servicebroker1") (v "1.0.10+20190624") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "0rs737j609sh8k4k5gbmji178ylbv1m9mcsr2lbc6690agl96b4a")))

(define-public crate-google-servicebroker1-1.0.12+20190624 (c (n "google-servicebroker1") (v "1.0.12+20190624") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "0ww39yrkmriwczzhd69c487g8hac0r1j6b2lw7g8xlz62ifwq4dx") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-servicebroker1-1.0.13+20190624 (c (n "google-servicebroker1") (v "1.0.13+20190624") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "0682lpn6650liz0lbaad132dyk023hja3g9ncvqjfycai5vz14h6") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-servicebroker1-1.0.14+20190624 (c (n "google-servicebroker1") (v "1.0.14+20190624") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "1ybpnrx90vc1f9vl1smh0r2nd284j5gynd15caj7phs2m51bcwgz") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-servicebroker1-2.0.0+20190624 (c (n "google-servicebroker1") (v "2.0.0+20190624") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^5.0") (d #t) (k 0)))) (h "1avrlphlpssln2bqva6isvcg7r28l0p9sqhkqa8l3lbsnw72m0ng")))

(define-public crate-google-servicebroker1-2.0.4+20190624 (c (n "google-servicebroker1") (v "2.0.4+20190624") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^5.0") (d #t) (k 0)))) (h "0pplmszmyk97ma1072xm2fv8rx62r0a7fczknzwv80a494yibcpr")))

(define-public crate-google-servicebroker1-2.0.8+20190624 (c (n "google-servicebroker1") (v "2.0.8+20190624") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^5.0") (d #t) (k 0)))) (h "035l7ar8g44f0zxmlhfqyqkbgipjjh26kh0665rb881gyqs7igji")))

(define-public crate-google-servicebroker1-3.0.0+20190624 (c (n "google-servicebroker1") (v "3.0.0+20190624") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^6.0") (d #t) (k 0)))) (h "1dp19r0zdvlgxiy0ri2859zd1lqfbhh9b4jj81b5yl15idzpx7db")))

(define-public crate-google-servicebroker1-3.1.0+20190624 (c (n "google-servicebroker1") (v "3.1.0+20190624") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^6.6") (d #t) (k 0)))) (h "1hqwc8h5sj4ibg1fw8bmf8jnkgahcwkrzfqpaaf21q14hjnf8il2")))

(define-public crate-google-servicebroker1-4.0.1+20190624 (c (n "google-servicebroker1") (v "4.0.1+20190624") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)) (d (n "yup-oauth2") (r "^7.0") (d #t) (k 0)))) (h "1aq6ymbkpz7g9mzxqc4jf3a9qi8zpqhadc9n8q98v0qs34kcxchr")))

(define-public crate-google-servicebroker1-5.0.2-beta-1+20190624 (c (n "google-servicebroker1") (v "5.0.2-beta-1+20190624") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "google-apis-common") (r "^5.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)))) (h "101izpx3ncjjayry0lfam67bma3nq7qj1y4911i9fdyxq7ybwvjw") (f (quote (("yup-oauth2" "google-apis-common/yup-oauth2") ("default" "yup-oauth2"))))))

(define-public crate-google-servicebroker1-5.0.2+20190624 (c (n "google-servicebroker1") (v "5.0.2+20190624") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "google-apis-common") (r "^5.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)))) (h "106m1fhal62xzm6pf0ha27378g8xbs515c0xxm6nzk3jbcna5zkx") (f (quote (("yup-oauth2" "google-apis-common/yup-oauth2") ("default" "yup-oauth2"))))))

(define-public crate-google-servicebroker1-5.0.3+20190624 (c (n "google-servicebroker1") (v "5.0.3+20190624") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "google-apis-common") (r "^6.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)))) (h "047yfp5av2jwjq8xnjqmgrqmhfdjqpvivwi7idqfdwcig09fx2zx") (f (quote (("yup-oauth2" "google-apis-common/yup-oauth2") ("default" "yup-oauth2"))))))

(define-public crate-google-servicebroker1-5.0.4+20190624 (c (n "google-servicebroker1") (v "5.0.4+20190624") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "google-apis-common") (r "^6.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "=1.7") (d #t) (k 0)))) (h "0m5rmzvrzhnqp44q77wciv5w3qyrdq9kyxiai4sz61hdsb7wb5p9") (f (quote (("yup-oauth2" "google-apis-common/yup-oauth2") ("default" "yup-oauth2"))))))

