(define-module (crates-io go og google-somethin) #:use-module (crates-io))

(define-public crate-google-somethin-0.1.0 (c (n "google-somethin") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "12vdxxa9c2dh2k0nxdckvci05v6br09klfh419p5ca3iz407xlp7")))

(define-public crate-google-somethin-0.1.1 (c (n "google-somethin") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "04i0xwg0dhpvfac5pqyvbwqazij7m94pn083fr6va0nvjgcfv7cr")))

(define-public crate-google-somethin-0.1.2 (c (n "google-somethin") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "13hkv9i6r7633lxqym568a5gv8a0scn4m1qiamlkqifqdgz6kxnf")))

(define-public crate-google-somethin-0.1.3 (c (n "google-somethin") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0ci4kq1fkr1q0c6bk4ccl0y39xa5payf9ilcdhpaksvzbw3xndxc")))

(define-public crate-google-somethin-0.1.4 (c (n "google-somethin") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0zjfvycr4jmsglwsafa87fypwzqb6zlaj22vzvyymb8hgdzsybl4")))

