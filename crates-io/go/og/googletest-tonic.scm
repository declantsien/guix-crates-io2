(define-module (crates-io go og googletest-tonic) #:use-module (crates-io))

(define-public crate-googletest-tonic-0.2.0 (c (n "googletest-tonic") (v "0.2.0") (d (list (d (n "googletest") (r "^0.3") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "1dys17v5g4inqvmb6hhabzpiqwmfvspjs4vlqr1kri4qvws0pclx")))

