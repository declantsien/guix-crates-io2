(define-module (crates-io go og google_translate_tts) #:use-module (crates-io))

(define-public crate-google_translate_tts-0.1.0 (c (n "google_translate_tts") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "0vw1s8xr0m93hnas5hj5fmhg3n9nja348mc0q3ciccmq54bcih91")))

(define-public crate-google_translate_tts-0.1.1 (c (n "google_translate_tts") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1gwl12s92bxfiizlqb1cgy0g6i9868dj4s416bhxla3fwrr9hdla")))

(define-public crate-google_translate_tts-0.1.2 (c (n "google_translate_tts") (v "0.1.2") (d (list (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1zhidzbawjl4a6bid2hag8rkdrf8wg50db8my4mqxx3np731y112")))

