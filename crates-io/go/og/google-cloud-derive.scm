(define-module (crates-io go og google-cloud-derive) #:use-module (crates-io))

(define-public crate-google-cloud-derive-0.0.0 (c (n "google-cloud-derive") (v "0.0.0") (h "10djqf2265jf7hrqhxvkkm0p84a1njniw1lkm6ginvq31ajb2g2c")))

(define-public crate-google-cloud-derive-0.1.0 (c (n "google-cloud-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "0l4q99y2lkplyyrr79000kbx54j9s0kvb6lgc2ml0jq1wmbx1qcz")))

(define-public crate-google-cloud-derive-0.2.0 (c (n "google-cloud-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "15kxw7r1kdc2w5866ybxz2yxprfjy6a52xybqvjjmss9mbibnpnq")))

(define-public crate-google-cloud-derive-0.2.1 (c (n "google-cloud-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "0lrlcpca0idb7nfqc42rry8m9ybs9ljvjj3hy0rdhyqz1z9g13ss")))

