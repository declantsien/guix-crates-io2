(define-module (crates-io go og googology) #:use-module (crates-io))

(define-public crate-googology-0.1.0 (c (n "googology") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0f8g0yw47v8139s9sn4c407jhpvw617248xgy2wz1w92la2vy3vm")))

(define-public crate-googology-0.2.0 (c (n "googology") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0dih8a5lf4pb11b1r69rwlqk5gq1zm3ahmgsbir2ln9sp9vzm355")))

(define-public crate-googology-0.2.1 (c (n "googology") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0r6af1jdsns4a245n1n0all0a4zy3crxx78frvfr05kqzqjr25lx")))

