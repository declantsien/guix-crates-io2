(define-module (crates-io go og google-docs-api-types) #:use-module (crates-io))

(define-public crate-google-docs-api-types-0.1.0 (c (n "google-docs-api-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0n7lv40dpx0jwpq4x14m2kikf07vypq213xayg0wrz07y937xa2c")))

(define-public crate-google-docs-api-types-0.2.0 (c (n "google-docs-api-types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cmcs4kni7f6jd8s66sq104kahr55drs415x2ky4klsaa0wgf8ms")))

(define-public crate-google-docs-api-types-0.2.1 (c (n "google-docs-api-types") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06cz4hn6gjq1lk749iras77cc0f0z8007ch7k3m7wgr0hk1k65l7")))

