(define-module (crates-io go og google-search-console-api) #:use-module (crates-io))

(define-public crate-google-search-console-api-0.1.0 (c (n "google-search-console-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^7.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "008cmsbvaq8f8y9y0kgg1h67pkz7xccyz9a5d5hhfmsqskj3hlqp")))

