(define-module (crates-io go og googleprojection) #:use-module (crates-io))

(define-public crate-googleprojection-1.0.0 (c (n "googleprojection") (v "1.0.0") (h "0h2hk6nssz34a9jpvf9x56flb8f6wqlxvxhgxnfg3fq9ycp1fqzm")))

(define-public crate-googleprojection-1.1.0 (c (n "googleprojection") (v "1.1.0") (h "0zzwrm2nl993wdpm30zvvf2mihipkx9jfv7yq8988m5imipnz9q3")))

(define-public crate-googleprojection-1.2.0 (c (n "googleprojection") (v "1.2.0") (h "08xjpmpx900ajq84kpq77v0cbvw9774z0gx9gc6wrk22pfd6mld4")))

