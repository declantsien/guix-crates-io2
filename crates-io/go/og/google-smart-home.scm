(define-module (crates-io go og google-smart-home) #:use-module (crates-io))

(define-public crate-google-smart-home-0.1.0 (c (n "google-smart-home") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1bgzzmxik7q6f3cqp6fgw8r5blz8f7hs03hxad26bxvghgxkqyxn")))

(define-public crate-google-smart-home-0.1.1 (c (n "google-smart-home") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0jqnsl6s0309b721ad41sjzlmwrs3ar1587xhpls9lznmsyipa33")))

(define-public crate-google-smart-home-0.1.2 (c (n "google-smart-home") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "19207pfxmlm0m67rbx5d65qfiqclrcaha1f2iqi47n6lc2pjdhg9")))

(define-public crate-google-smart-home-0.1.3 (c (n "google-smart-home") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1vrxxq20lgmprxyf1q3rz4qdx65vy71692zs7kfagjcwqjm9m7ag")))

