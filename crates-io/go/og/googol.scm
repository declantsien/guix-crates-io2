(define-module (crates-io go og googol) #:use-module (crates-io))

(define-public crate-googol-0.1.0 (c (n "googol") (v "0.1.0") (d (list (d (n "gcp_auth") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wa8nk4s0m7xxg7i5yb3azxlm64ynnnxpi1cf9c403mcy632rkf8")))

(define-public crate-googol-0.1.1 (c (n "googol") (v "0.1.1") (d (list (d (n "gcp_auth") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g458b58hmvxgxlmrhbv47ffsikdwrw59syv8q5i3mv8g5hqbzcl")))

(define-public crate-googol-0.1.2 (c (n "googol") (v "0.1.2") (d (list (d (n "gcp_auth") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y656fwjbsivxw051yky98g55h7jcmpm1aidpb58l0r7fsv3ra7x")))

(define-public crate-googol-0.1.3 (c (n "googol") (v "0.1.3") (d (list (d (n "gcp_auth") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "074zdk4hxhnmw8hl35fqq74y2f032kly8mmbayb8ciy51md36yr4")))

