(define-module (crates-io go og google-signin-client) #:use-module (crates-io))

(define-public crate-google-signin-client-0.1.1 (c (n "google-signin-client") (v "0.1.1") (d (list (d (n "async_cell") (r "^0.2.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "12q7dmv0z194vv0yfzagmlm712m1ax92d7z4alhl865210k9dhqk")))

(define-public crate-google-signin-client-0.1.2 (c (n "google-signin-client") (v "0.1.2") (d (list (d (n "async_cell") (r "^0.2.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "12ji1n78jpim4gdw19hhd1cxq6ragiznikg7g2wgd3p9q049ckcc")))

(define-public crate-google-signin-client-0.1.3 (c (n "google-signin-client") (v "0.1.3") (d (list (d (n "async_cell") (r "^0.2.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.67") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "1j51c2sy6inq9r4nm031l65iha69j3ralc5j7dw65f6az6q14sp4")))

(define-public crate-google-signin-client-0.2.0 (c (n "google-signin-client") (v "0.2.0") (d (list (d (n "async_cell") (r "^0.2.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "1sxgxqwvaw6m8j5xsay7zydf3rl7vhdfj97hkz2jrh45pp9v852q")))

