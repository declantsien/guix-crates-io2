(define-module (crates-io go og google-chat-types) #:use-module (crates-io))

(define-public crate-google-chat-types-0.1.0 (c (n "google-chat-types") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1r9aami7qcvbiv0jdmqk40d18nlnj7717ny1p5r9hqgqd80h43sz") (y #t)))

(define-public crate-google-chat-types-0.1.1 (c (n "google-chat-types") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0iiyrb8gprkrp4arwdqcz8yq635bcbasxsyvaddd5qp8c5r4xllg") (y #t)))

(define-public crate-google-chat-types-0.1.2 (c (n "google-chat-types") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1s8pqh5hv3ygayip1mi95lfszvs93lb2yq8vckz9fv4nk4jis5pw") (y #t)))

(define-public crate-google-chat-types-0.1.3 (c (n "google-chat-types") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0wivsl2swsp0p46gx5w279c8yyqdnwfh2xyl95l1m69n7qxbj9fr")))

(define-public crate-google-chat-types-0.1.4 (c (n "google-chat-types") (v "0.1.4") (d (list (d (n "derive_builder") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0k090l36wcqf13gc0ppaknibnlrr2a8rmj0ka181nmcrx0pzidya")))

(define-public crate-google-chat-types-0.1.5 (c (n "google-chat-types") (v "0.1.5") (d (list (d (n "derive_builder") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ph8202jav32gjsa3h0dvdgbjjs3cf15apw3zfx8rcbidqg9fmxi")))

