(define-module (crates-io go og googleapis) #:use-module (crates-io))

(define-public crate-googleapis-0.0.1 (c (n "googleapis") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-derive") (r "^0.4") (d #t) (k 0)) (d (n "prost-types") (r "^0.4") (d #t) (k 0)))) (h "0l81cixlc8ajd0nrnm7kvm06nqlb1svn2kxb5fzbbg36p0rh7p70")))

