(define-module (crates-io go og googlescrape) #:use-module (crates-io))

(define-public crate-googlescrape-0.1.0 (c (n "googlescrape") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.22") (d #t) (k 0)) (d (n "curl-sys") (r "^0.4.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 0)) (d (n "scraper") (r "^0.10.0") (d #t) (k 0)))) (h "0b0nwc387v7il50b8cj219d703w0dga661r2jy893plb5hmwp8r2")))

(define-public crate-googlescrape-0.3.1 (c (n "googlescrape") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0814npfbagy0fz7i581c6lmmg674g55p0idkzv1kcslrqd44f48w") (f (quote (("ffi" "serde" "rmp-serde"))))))

