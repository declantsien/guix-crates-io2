(define-module (crates-io go og google-dns1_beta1) #:use-module (crates-io))

(define-public crate-google-dns1_beta1-0.1.0+20150114 (c (n "google-dns1_beta1") (v "0.1.0+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "01rimsphj4m56cpayymw7dxnyk1iazcisp6rrxzi5f3scwbpbdq6")))

(define-public crate-google-dns1_beta1-0.1.1+20150114 (c (n "google-dns1_beta1") (v "0.1.1+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0zh0rljcxdn5aiyq96i2lmm991ncivw1hsi1jvy9s5gn525l21da")))

(define-public crate-google-dns1_beta1-0.1.2+20150114 (c (n "google-dns1_beta1") (v "0.1.2+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1g5wjpv3b1zjvy9l7syvnlfab6bi1c2a4i8fgfk6xa4f6d2719li")))

(define-public crate-google-dns1_beta1-0.1.3+20150114 (c (n "google-dns1_beta1") (v "0.1.3+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "07wn4wqilmc3c2nw6w233dxhljb93b2gsnqfmkhayhgr4d4fq4s5")))

(define-public crate-google-dns1_beta1-0.1.4+20150114 (c (n "google-dns1_beta1") (v "0.1.4+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0cp0hqznbyysrkxlwsgqw3vl4b403sgy2pjrxz5ag5l1ki4v7n0q")))

(define-public crate-google-dns1_beta1-0.1.5+20150114 (c (n "google-dns1_beta1") (v "0.1.5+20150114") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1zrz94nj2nlsbi6al7ij0z22nnryi29rbp9svrxymkpy2hj9idzk")))

