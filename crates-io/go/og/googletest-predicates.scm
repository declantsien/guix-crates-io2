(define-module (crates-io go og googletest-predicates) #:use-module (crates-io))

(define-public crate-googletest-predicates-0.1.0 (c (n "googletest-predicates") (v "0.1.0") (d (list (d (n "googletest") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 0)))) (h "1ppd1y8sa915s5jn9jcwl4ws0b7g1cmad3i8r7mp8ihv8nrlhl59")))

(define-public crate-googletest-predicates-0.1.1 (c (n "googletest-predicates") (v "0.1.1") (d (list (d (n "googletest") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 0)))) (h "12f8a1r9zbwdxlrxz7jib87430m869sh43jg4l3hdnw7p8czdl6w")))

(define-public crate-googletest-predicates-0.2.0 (c (n "googletest-predicates") (v "0.2.0") (d (list (d (n "googletest") (r "^0.3") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 0)))) (h "15whmw6cf50x48sg9znwyj79p2xl72xll37c9ngj61lq3xc9ha5g")))

(define-public crate-googletest-predicates-0.3.0 (c (n "googletest-predicates") (v "0.3.0") (d (list (d (n "googletest") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 0)))) (h "1nylw39ia8fxj5himk7hbhprzp1d3sfs76wgi0fj6fl1bqvzj388")))

(define-public crate-googletest-predicates-0.4.0 (c (n "googletest-predicates") (v "0.4.0") (d (list (d (n "googletest") (r "^0.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "predicates") (r "^3.0") (d #t) (k 0)))) (h "0fdwq4zm52b9676k4zaz549d28v8cisbcqa6zzidsfmz5pjzz0bs")))

