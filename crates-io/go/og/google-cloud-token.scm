(define-module (crates-io go og google-cloud-token) #:use-module (crates-io))

(define-public crate-google-cloud-token-0.1.0 (c (n "google-cloud-token") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)))) (h "10sqwyjshmmyrgvpcdp6znlc57520zqpw4ln2i9x62db871ab7qg")))

(define-public crate-google-cloud-token-0.1.1 (c (n "google-cloud-token") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)))) (h "0sjkrcxxc04b6bmm6vgka1bgdi0p7sfa0cycbc42zpp36kmn5k8g")))

(define-public crate-google-cloud-token-0.1.2 (c (n "google-cloud-token") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)))) (h "0pvjdnfnz6qrz7mq1xhm8k7gnxv94iamjn785j5147djm0mw2jcg")))

