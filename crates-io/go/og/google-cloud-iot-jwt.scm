(define-module (crates-io go og google-cloud-iot-jwt) #:use-module (crates-io))

(define-public crate-google-cloud-iot-jwt-0.1.0 (c (n "google-cloud-iot-jwt") (v "0.1.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.9") (f (quote ("ufmt-impl"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "p256") (r "^0.10.0") (f (quote ("ecdsa"))) (k 0)) (d (n "pem-rfc7468") (r "^0.3.1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0vz3gkn1flwbr0ynvxsscwpxfbli0palcv6bfq0435649hffl7kq") (y #t)))

(define-public crate-google-cloud-iot-jwt-0.1.1 (c (n "google-cloud-iot-jwt") (v "0.1.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.9") (f (quote ("ufmt-impl"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "p256") (r "^0.10.0") (f (quote ("ecdsa"))) (k 0)) (d (n "pem-rfc7468") (r "^0.3.1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0lpmlgsqpk9wk12vf1191ihbv81xq5z32kbm5mmhf9hfpwql9pvj")))

