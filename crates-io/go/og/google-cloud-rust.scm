(define-module (crates-io go og google-cloud-rust) #:use-module (crates-io))

(define-public crate-google-cloud-rust-0.10.0 (c (n "google-cloud-rust") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "google-cloud-rust-raw") (r "^0.10.0") (d #t) (k 0)) (d (n "grpcio") (r "^0.9.0") (d #t) (k 0)))) (h "13rbzpdz46760afbj2a27k4g9v5fggp9bid43pjlw9l67a99h9sp")))

(define-public crate-google-cloud-rust-0.13.0 (c (n "google-cloud-rust") (v "0.13.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "google-cloud-rust-raw") (r "^0.13.0") (d #t) (k 0)) (d (n "grpcio") (r "^0.10.0") (d #t) (k 0)))) (h "1s3ixr23vhvn7h6pakwkc2yfj5b6d4fix76kfpjjysgb3d44dm35")))

(define-public crate-google-cloud-rust-0.14.0 (c (n "google-cloud-rust") (v "0.14.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "google-cloud-rust-raw") (r "^0.14.0") (d #t) (k 0)) (d (n "grpcio") (r "^0.12.0") (f (quote ("_secure"))) (d #t) (k 0)))) (h "0rkzh24sx6s4vlxwfry3y06a972hd8ln1yrj8d4y7jjxhs9dyixl")))

(define-public crate-google-cloud-rust-0.15.0 (c (n "google-cloud-rust") (v "0.15.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "google-cloud-rust-raw") (r "^0.15.0") (f (quote ("bigtable" "pubsub" "spanner"))) (d #t) (k 0)) (d (n "grpcio") (r "=0.12.1") (f (quote ("_secure"))) (d #t) (k 0)))) (h "1ldl52hp1if4nfi6nsqahglddnwmx20gaxbdw8b4ini5x00jgxx9")))

