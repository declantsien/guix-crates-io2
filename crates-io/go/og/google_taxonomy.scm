(define-module (crates-io go og google_taxonomy) #:use-module (crates-io))

(define-public crate-google_taxonomy-0.1.0 (c (n "google_taxonomy") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "174lkasj93nns6jr6v36l9wggnng7c7nma5nsx3wkjyz65axgyw7") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-google_taxonomy-0.1.1 (c (n "google_taxonomy") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "0c27gjby5lw16sqqzky034il610jaai2y65j060iiam4ihnn29wp") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-google_taxonomy-0.2.0 (c (n "google_taxonomy") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1f3kajr17z30qa879vx5aa92i83qjhm38zqnxkwr70nr5v3g0wk9") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-google_taxonomy-0.2.1 (c (n "google_taxonomy") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "149wbkas9zlbzvin7c9p7fk452lkz9apv1a7pf8rr609ri7xja6l") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-google_taxonomy-0.2.2 (c (n "google_taxonomy") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1d9ywfch6r3bjgaf32b21i7rf8bda85h909pkglspmigp36ppdil") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-google_taxonomy-0.3.0 (c (n "google_taxonomy") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "0wnnynsa9dhffvvs0ac8jqn22r1wnr889zq7vhr42f2s2nw8w06h") (f (quote (("default"))))))

(define-public crate-google_taxonomy-0.3.1 (c (n "google_taxonomy") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "0df2m9napv1amy8r2y32vzishvvjixjgj0jhn022n312m4vxfr4l") (f (quote (("default"))))))

(define-public crate-google_taxonomy-0.3.2 (c (n "google_taxonomy") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1fc3qfmx6zi8zdsfp17lbq79327r3ngfv1ip429v2dkhx61h4sva") (f (quote (("default"))))))

