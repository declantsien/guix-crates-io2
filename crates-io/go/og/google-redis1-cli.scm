(define-module (crates-io go og google-redis1-cli) #:use-module (crates-io))

(define-public crate-google-redis1-cli-1.0.8+20180928 (c (n "google-redis1-cli") (v "1.0.8+20180928") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.8") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "1z3k468z7j4a49m381z3hpw5ydwk1h9zzzkw2hx43vd8a94jwg2z")))

(define-public crate-google-redis1-cli-1.0.9+20190628 (c (n "google-redis1-cli") (v "1.0.9+20190628") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.9") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "1aqq1jix37wg0f9bjbki6vjjp4l6h9iqjv4dd2qphlgp06x3n8pr")))

(define-public crate-google-redis1-cli-1.0.10+20190628 (c (n "google-redis1-cli") (v "1.0.10+20190628") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.10") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "1x2x7j20707r8kqbyc91zwgy2ncaq04y0ckqcp8z28698as4r5kk")))

(define-public crate-google-redis1-cli-1.0.12+20190628 (c (n "google-redis1-cli") (v "1.0.12+20190628") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.12") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "1mv0jmsdqcsn5fpj0wka263xvphd7asqfw5ic2nr42n42wr3l0mk") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-redis1-cli-1.0.13+20200402 (c (n "google-redis1-cli") (v "1.0.13+20200402") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "1y2mh07csr61kcinzmkgff150rxvghhfpa3q7b7ib7qwf6v4ijqg") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-redis1-cli-1.0.14+20200623 (c (n "google-redis1-cli") (v "1.0.14+20200623") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^1.0.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "yup-hyper-mock") (r "^2.0") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (k 0)))) (h "12r5qymk4w8cpfilsf64kxxg9lg5m14dd99iclvcqfcvqhs5hgnm") (f (quote (("rustls" "yup-oauth2/no-openssl") ("openssl" "yup-oauth2/default") ("default" "openssl"))))))

(define-public crate-google-redis1-cli-2.0.0+20210325 (c (n "google-redis1-cli") (v "2.0.0+20210325") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^2.0.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yup-oauth2") (r "^5.0") (d #t) (k 0)))) (h "11hdyj76ya76bpapryk02pa5mkyxqw1vpldpsrysyn5qvgsjjqsa")))

(define-public crate-google-redis1-cli-2.0.4+20210325 (c (n "google-redis1-cli") (v "2.0.4+20210325") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^2.0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yup-oauth2") (r "^5.0") (d #t) (k 0)))) (h "0hscyisikvpx5pm1dncbp1w67q900259szr97f35wqg4xyikdfn1")))

(define-public crate-google-redis1-cli-3.0.0+20220301 (c (n "google-redis1-cli") (v "3.0.0+20220301") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-redis1") (r "^3.0.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yup-oauth2") (r "^6.0") (d #t) (k 0)))) (h "1dhmg43hhkk87vdrxxdjcwvkc0dh93mgr3z0y8hgnhvg230nihab")))

(define-public crate-google-redis1-cli-5.0.4+20240226 (c (n "google-redis1-cli") (v "5.0.4+20240226") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "google-clis-common") (r "^6.0") (d #t) (k 0)) (d (n "google-redis1") (r "^5.0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)))) (h "0apb606qhppgyd0y3pbw6llki00x2yj0n7sk9nprcl8f49ggkagm")))

