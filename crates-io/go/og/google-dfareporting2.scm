(define-module (crates-io go og google-dfareporting2) #:use-module (crates-io))

(define-public crate-google-dfareporting2-0.1.0+20150223 (c (n "google-dfareporting2") (v "0.1.0+20150223") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "0pbh4smwxh856x9rl8q4ack4w7lr0vwzp5vqwnykqjgcmn18h42l")))

(define-public crate-google-dfareporting2-0.1.1+20150223 (c (n "google-dfareporting2") (v "0.1.1+20150223") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1x8r628wrqz96n0xia8z9yfym5vwgs8pl5y6ln3gx5ch96hfgw4x")))

(define-public crate-google-dfareporting2-0.1.2+20150223 (c (n "google-dfareporting2") (v "0.1.2+20150223") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "19s0vpiygf314h66n60kl9292nqv8asyfw388fml7kcfk8bn60qx")))

(define-public crate-google-dfareporting2-0.1.3+20150326 (c (n "google-dfareporting2") (v "0.1.3+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1syyfnmiiw2y1r9mrd2m6gp64x3yfa38fw3sbhh9lmzyjqx390vz")))

(define-public crate-google-dfareporting2-0.1.4+20150326 (c (n "google-dfareporting2") (v "0.1.4+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1klx790crk932gb3z319pi9mzaq9x9invmmp6l2m2z304hp2lgyp")))

(define-public crate-google-dfareporting2-0.1.5+20150326 (c (n "google-dfareporting2") (v "0.1.5+20150326") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "serde") (r ">= 0.3.0") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "yup-oauth2") (r "*") (d #t) (k 0)))) (h "1z97f6i54njpqiz9zr6b3b5rhm4b44wxadzk5zv2ljhpmpaapsqm")))

