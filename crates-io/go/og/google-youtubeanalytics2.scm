(define-module (crates-io go og google-youtubeanalytics2) #:use-module (crates-io))

(define-public crate-google-youtubeanalytics2-1.0.7+20181010 (c (n "google-youtubeanalytics2") (v "1.0.7+20181010") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 0.5") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "02syx9d4k5g132g2i0qm105k9005jwd4dk76nr5pd8bypv9i79zk")))

(define-public crate-google-youtubeanalytics2-1.0.8+20181010 (c (n "google-youtubeanalytics2") (v "1.0.8+20181010") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 2)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "= 0.5") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0") (d #t) (k 0)))) (h "0v7944gdpipvmqs3kv6impcplbgwrwyss31hggg73mamsa0fy3kh")))

