(define-module (crates-io go og google-androidpublisher-rest) #:use-module (crates-io))

(define-public crate-google-androidpublisher-rest-0.1.0 (c (n "google-androidpublisher-rest") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^0.6") (f (quote ("chrono"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde_repr") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "06fqg7nqnipd6qkgz15wgz1rbg3r538a7ccgbpykmmb5gpbg52z1")))

(define-public crate-google-androidpublisher-rest-0.1.1 (c (n "google-androidpublisher-rest") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^4") (f (quote ("chrono"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde_repr") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "1pdzzp1jdbgd78cpqr6l5ac5l0rph7a32836xrzg9aq32nd8grnn")))

