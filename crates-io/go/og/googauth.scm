(define-module (crates-io go og googauth) #:use-module (crates-io))

(define-public crate-googauth-0.5.0 (c (n "googauth") (v "0.5.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "openidconnect") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.1") (d #t) (k 0)))) (h "1qc5zvl8382r2a4xjlj6xpv8f4c1zzwfrw7sza5kr4adq2hcbfbj")))

(define-public crate-googauth-0.6.0 (c (n "googauth") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "googauth-lib") (r "^0.6.0") (d #t) (k 0)))) (h "14arqwhb7g6bi2s8ci87w1apxybfjidmkycwqdwylralc4fjikwr")))

(define-public crate-googauth-0.7.0 (c (n "googauth") (v "0.7.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "googauth-lib") (r "^0.7.0") (d #t) (k 0)))) (h "1cw89a26mgk8nx571c0vic4k70pin1yw9x21mr56h7m1ac3b0258")))

(define-public crate-googauth-0.8.0 (c (n "googauth") (v "0.8.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "googauth-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "064mgf39z9mb14snv03zlr4ga5dczdwkrknd3i6d7z1qjg2j6qw9")))

(define-public crate-googauth-0.8.1 (c (n "googauth") (v "0.8.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "googauth-lib") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00j3ndhyvf42zr084f9x4xxg2yvj0586swkrq59qqlmdfsnyf1hy")))

