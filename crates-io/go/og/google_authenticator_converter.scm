(define-module (crates-io go og google_authenticator_converter) #:use-module (crates-io))

(define-public crate-google_authenticator_converter-0.1.0 (c (n "google_authenticator_converter") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "05gy54rmsdq18gmws461047m1z9ksxwp1yq3fz7di1xxpq6bp615")))

(define-public crate-google_authenticator_converter-0.2.0 (c (n "google_authenticator_converter") (v "0.2.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0z8ssq3afcqm38mipwljghjg8riwqjxf8x3vvpx9m9lc41pqhipa")))

