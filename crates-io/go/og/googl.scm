(define-module (crates-io go og googl) #:use-module (crates-io))

(define-public crate-googl-0.0.1 (c (n "googl") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1nwklbga9k1mdk2lzci4ja8rkpmwlq45s95vswqy8z4dhjjl1z0c")))

(define-public crate-googl-0.0.2 (c (n "googl") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "16wvc8wgmd9cpl51bl0a50nfdyi4494y7891c29c8hv40br6awsk")))

(define-public crate-googl-0.0.3 (c (n "googl") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1885vqajnvcbk8zaghd8sm4b37mv5rlaq9m7gvrrxzcr2kr73zai")))

(define-public crate-googl-0.0.4 (c (n "googl") (v "0.0.4") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "144m784cpq402hqdrnldab4glsay9mrrfl3kv8hifhv2i2qx2zca")))

(define-public crate-googl-0.0.5 (c (n "googl") (v "0.0.5") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1svx0kvd3l8wk2wwcmkj5d5lkp4h0ywsmmf2ikh9ziyqwj1jqj7a")))

(define-public crate-googl-0.1.0 (c (n "googl") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "0lqv4pqxclxzpf1ja2c9r3k81kgs6nyn2pjc73q57bp2imksnjq0")))

