(define-module (crates-io go og goog) #:use-module (crates-io))

(define-public crate-goog-0.1.0 (c (n "goog") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08c8xwwbaj2kqcgmfskfah3ybi3ljfw7c98fgq5nnb25lj3pd1mm")))

(define-public crate-goog-0.1.1 (c (n "goog") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0i23w7dkxyxikawdsn4px4s3vzrvyxkgixwbrkwyi8nd0blbsgg2")))

(define-public crate-goog-0.1.2 (c (n "goog") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)) (d (n "os_info") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fkb3333fyinfpax6qpwrjcvqi2pj48a6ksr97s5j9in1m996bl2")))

(define-public crate-goog-0.2.0 (c (n "goog") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)) (d (n "os_info") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "180zh7bfb3j8v96rcp67x5iwmvpxwylbcm47z1wdbdwhgg10g00k")))

(define-public crate-goog-2.0.0 (c (n "goog") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)) (d (n "os_info") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1wvg11s2025m43v79yjadrfhh4p40s8l4ik65jak8hddawqrzs35")))

