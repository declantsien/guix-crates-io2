(define-module (crates-io go og google-service-account-json-key) #:use-module (crates-io))

(define-public crate-google-service-account-json-key-0.1.0 (c (n "google-service-account-json-key") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.3") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)))) (h "0pfk2jj57cy1y5fyxrhxdarzdb4dz38qzd5ay2qc9gml56hk13r8")))

