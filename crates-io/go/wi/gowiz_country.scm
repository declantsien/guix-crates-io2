(define-module (crates-io go wi gowiz_country) #:use-module (crates-io))

(define-public crate-gowiz_country-0.0.1 (c (n "gowiz_country") (v "0.0.1") (h "06gvlwqqrl0jn2jcn9mblik5p1mb4wj6q8qxcsb9sdncz88xlb8i")))

(define-public crate-gowiz_country-0.0.2 (c (n "gowiz_country") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1km0jygxkh2qm66d31wwykplc28iwv5rfm93a5lgx2q73p53dj55")))

(define-public crate-gowiz_country-0.0.3 (c (n "gowiz_country") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hhk356jnn81wd82b0bz8kqb8xncrxv1dc0dkwh6g1c26d8qipj4")))

(define-public crate-gowiz_country-0.0.4 (c (n "gowiz_country") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nbglf8xq3kslyyma36fkdg4wn6z4r3yj0bsxii3lv9c8qr3m87h")))

