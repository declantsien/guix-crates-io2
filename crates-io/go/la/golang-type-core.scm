(define-module (crates-io go la golang-type-core) #:use-module (crates-io))

(define-public crate-golang-type-core-0.1.0 (c (n "golang-type-core") (v "0.1.0") (d (list (d (n "golang-parser") (r "~0.1") (d #t) (k 0)) (d (n "golang-struct-tag") (r "~0.1") (d #t) (k 0)) (d (n "golang-type-name-core") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1b78jp83jbwlacyzv1qw89dvx13b3s29mlkbarsyhzfpybdr0af3") (f (quote (("enable-quote-to_tokens" "golang-type-name-core/enable-quote-to_tokens" "proc-macro2" "quote") ("default"))))))

