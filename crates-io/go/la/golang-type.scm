(define-module (crates-io go la golang-type) #:use-module (crates-io))

(define-public crate-golang-type-0.0.0 (c (n "golang-type") (v "0.0.0") (h "09mszw8f6zmfxrk94kfs57rgpvwcfvrvkqvjl53f1ynyph9jzq1y")))

(define-public crate-golang-type-0.1.0 (c (n "golang-type") (v "0.1.0") (d (list (d (n "golang-type-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "golang-type-macro") (r "=0.1.0") (d #t) (k 0)))) (h "0lad1dzzfxy4xkyv1adyhi9rhishkfd5dyqa8a9vycgf3gki4z54")))

