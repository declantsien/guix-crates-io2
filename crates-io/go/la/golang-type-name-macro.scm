(define-module (crates-io go la golang-type-name-macro) #:use-module (crates-io))

(define-public crate-golang-type-name-macro-0.1.0 (c (n "golang-type-name-macro") (v "0.1.0") (d (list (d (n "golang-type-name-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1gb0cphnhllk9kxzhpd7w3zgajfmmldxynbndnh7w5cr8hgqbrlm")))

