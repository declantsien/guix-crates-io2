(define-module (crates-io go la golang-type-macro) #:use-module (crates-io))

(define-public crate-golang-type-macro-0.1.0 (c (n "golang-type-macro") (v "0.1.0") (d (list (d (n "golang-type-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0wq2s5c8xvfw0vva37va3ysd67gkksnkim034z3xch5hc3bgfs89")))

