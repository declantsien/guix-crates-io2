(define-module (crates-io go la golang-parser) #:use-module (crates-io))

(define-public crate-golang-parser-0.0.0 (c (n "golang-parser") (v "0.0.0") (h "1a3n4mpz6qj72cn83dqccqb99v46yffjffrv5zrbjarcbkyqq5i6")))

(define-public crate-golang-parser-0.1.0 (c (n "golang-parser") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tree-sitter") (r "~0.17") (k 0)) (d (n "tree-sitter-go") (r "~0.16") (k 0)))) (h "0gbcy5paazn0yayg5d91x0skdlf28bf61aiqpg9cif2ypkpjh6h4")))

