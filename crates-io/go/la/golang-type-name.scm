(define-module (crates-io go la golang-type-name) #:use-module (crates-io))

(define-public crate-golang-type-name-0.0.0 (c (n "golang-type-name") (v "0.0.0") (h "04aia8nm867nwmwas8fmp7f2rl1iw9l69dp0c5irzr0l3xk0zzdj")))

(define-public crate-golang-type-name-0.1.0 (c (n "golang-type-name") (v "0.1.0") (d (list (d (n "golang-type-name-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "golang-type-name-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 2)))) (h "0g8x1a5yidf7k01m3hab0b2dwvzkv2ddqr7404q588wji9r7agmz")))

