(define-module (crates-io go la golana) #:use-module (crates-io))

(define-public crate-golana-0.1.0 (c (n "golana") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.0") (f (quote ("btree_map" "serde_borsh"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.10.41") (d #t) (k 0)))) (h "19hbf7npqyyz75pvbkpakppwlw1a8axgv5sl7mzskp2bywpm1pb7")))

(define-public crate-golana-0.1.1 (c (n "golana") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.4") (f (quote ("btree_map" "serde_borsh" "instruction_pos"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.9") (d #t) (k 0)))) (h "00748qxpdj2ac36w9i4ipp797729fpiia207hhpfg9s9c4z498ma")))

(define-public crate-golana-0.1.2 (c (n "golana") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.4") (f (quote ("btree_map" "serde_borsh" "instruction_pos"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.9") (d #t) (k 0)))) (h "14qb5m21lkda2jwsgs6wy3npnzia9ajdk49dazg6px17q6hyrx5p")))

(define-public crate-golana-0.1.3 (c (n "golana") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.5") (f (quote ("btree_map" "serde_borsh" "instruction_pos"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.9") (d #t) (k 0)))) (h "020kj1fb3vlwmriq2k0db0y1vjar0wy5sny5klqyp5aq100m9n61")))

