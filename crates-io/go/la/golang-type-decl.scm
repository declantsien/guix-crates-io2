(define-module (crates-io go la golang-type-decl) #:use-module (crates-io))

(define-public crate-golang-type-decl-0.0.0 (c (n "golang-type-decl") (v "0.0.0") (h "0757sjx05qj3nqxlakck3k08j1xp96gass2az35j723n4ywdwjq8") (y #t)))

(define-public crate-golang-type-decl-0.1.0 (c (n "golang-type-decl") (v "0.1.0") (d (list (d (n "golang-type-decl-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "golang-type-decl-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rqsg9rrb8l2krnhxcigqd96p7z2qbi1i76x60xdngyk4m0y2zfh") (y #t)))

(define-public crate-golang-type-decl-0.2.0 (c (n "golang-type-decl") (v "0.2.0") (d (list (d (n "golang-type-decl-core") (r "=0.2.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "golang-type-decl-macro") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-aux") (r "^2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19r9bszpsg9ix9k676armaslv3pdv1zpw4ya3qs99q1br1dxrh3c")))

(define-public crate-golang-type-decl-0.3.0 (c (n "golang-type-decl") (v "0.3.0") (d (list (d (n "golang-type-decl-core") (r "=0.3.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "golang-type-decl-macro") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-aux") (r "^2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1byg4f899fkwgk4274c1ayd979pninl5ly7sp3x1d9kgpafw99h9")))

