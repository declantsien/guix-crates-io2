(define-module (crates-io go la golangify) #:use-module (crates-io))

(define-public crate-golangify-0.1.0 (c (n "golangify") (v "0.1.0") (h "1z7kypj04j2bv2s9kbbld52xxrgqwcgwds2sd55ff49vblp8f1cd") (y #t)))

(define-public crate-golangify-0.1.1 (c (n "golangify") (v "0.1.1") (h "1q4fw681dv7x7cgqrg10mlb26k06qnx9hsxpgigk08g5vp0cfing") (y #t)))

(define-public crate-golangify-0.1.2 (c (n "golangify") (v "0.1.2") (h "1gmahcgnkcrzdb8f03vq8a90gcdhnmfcyzpsdd358q1dbsc059lx") (y #t)))

(define-public crate-golangify-0.1.3 (c (n "golangify") (v "0.1.3") (h "17dz6qzsyjfll796pb81qnanw92fhbd9qldvwclfw1cswza2mg4l") (y #t)))

(define-public crate-golangify-0.1.4 (c (n "golangify") (v "0.1.4") (h "0gxr5ccsskcxcq4mfibr08sldzqy3i6wli8cbfhljz1kvh0pvccw") (y #t)))

(define-public crate-golangify-0.1.4-24 (c (n "golangify") (v "0.1.4-24") (h "1x8rj5avkhddn6f69l1kr51nv769icpy36mqa486nfsr5sapvl4z") (y #t)))

(define-public crate-golangify-1.4.24 (c (n "golangify") (v "1.4.24") (h "1nakm5l7nnwlakxrh3zwa992b2k2f4m9r95xj2vbm5jwbvknkjri")))

