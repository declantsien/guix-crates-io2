(define-module (crates-io go la golang-struct-tag) #:use-module (crates-io))

(define-public crate-golang-struct-tag-0.0.0 (c (n "golang-struct-tag") (v "0.0.0") (h "06zfbml06svlk57q4619xy683d0095jfdcr9wv0xcpnhn8rjk151")))

(define-public crate-golang-struct-tag-0.1.0 (c (n "golang-struct-tag") (v "0.1.0") (d (list (d (n "golang-parser") (r "~0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (k 0)) (d (n "pest_derive") (r "^2.1") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1l6b1b8gdxravmkijrh58yfp3zvzy71a9697693ivq1ax0vqryax")))

