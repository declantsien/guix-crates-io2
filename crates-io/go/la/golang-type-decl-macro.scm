(define-module (crates-io go la golang-type-decl-macro) #:use-module (crates-io))

(define-public crate-golang-type-decl-macro-0.1.0 (c (n "golang-type-decl-macro") (v "0.1.0") (d (list (d (n "golang-type-decl-core") (r "=0.1.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "regex") (r "^1.4") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "08jzxby0gi3nslx01fx6wl7rk1baypl7i6xd1pb3llx7s5f46451") (y #t)))

(define-public crate-golang-type-decl-macro-0.2.0 (c (n "golang-type-decl-macro") (v "0.2.0") (d (list (d (n "golang-type-decl-core") (r "=0.2.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "regex") (r "^1.4") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "0gb8ms8k0djbwk0sz6c3anp6c6lfv7cd95zxjcn973xdyg2n8pw8")))

(define-public crate-golang-type-decl-macro-0.3.0 (c (n "golang-type-decl-macro") (v "0.3.0") (d (list (d (n "golang-type-decl-core") (r "=0.3.0") (f (quote ("enable-quote-to_tokens"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "regex") (r "^1.4") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "0jnxcvxfjxi26l7g9fnhkbyb47n4vw0i75scwbpgp2rwv5qiqxgy")))

