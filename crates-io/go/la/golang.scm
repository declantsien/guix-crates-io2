(define-module (crates-io go la golang) #:use-module (crates-io))

(define-public crate-golang-0.1.0 (c (n "golang") (v "0.1.0") (h "0in32ifs0fdhcj2m40vic9x3yhc9xf0hdqnllb5h2gj7af1v85sm")))

(define-public crate-golang-0.1.1 (c (n "golang") (v "0.1.1") (h "1bl7n9wqpccb5yg0pjilq16lpblrblr5cb582kmn35jqfvkgqbkg")))

