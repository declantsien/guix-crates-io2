(define-module (crates-io go la golang-type-name-core) #:use-module (crates-io))

(define-public crate-golang-type-name-core-0.1.0 (c (n "golang-type-name-core") (v "0.1.0") (d (list (d (n "golang-parser") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0wkkaccadgrix5cpnv2ibsmc1wys221p6f5d6wgaf8aapa8c26yp") (f (quote (("enable-quote-to_tokens" "proc-macro2" "quote") ("default"))))))

