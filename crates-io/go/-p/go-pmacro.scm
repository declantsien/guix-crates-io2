(define-module (crates-io go -p go-pmacro) #:use-module (crates-io))

(define-public crate-go-pmacro-0.1.0 (c (n "go-pmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02yfairn5c6l5kjy8gvszxrdhlgv75dfhfdpp9pkznf07knxbrpc")))

(define-public crate-go-pmacro-0.1.4 (c (n "go-pmacro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aj4cgiydway1nm550hni10caf24gpnrsazcl78inb016pmh13ry")))

(define-public crate-go-pmacro-0.1.5 (c (n "go-pmacro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "112y1514gnzybwv1v0v1ircpfnkgcgwyv2xa9jplyicvlbw6qfl6")))

