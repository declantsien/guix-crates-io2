(define-module (crates-io go -p go-parser) #:use-module (crates-io))

(define-public crate-go-parser-0.1.0 (c (n "go-parser") (v "0.1.0") (h "183ccxjsvy4m167cvgb0psa4xfk2b5j7fmijm9b3jchjdrbhzgsa") (f (quote (("default") ("btree_map"))))))

(define-public crate-go-parser-0.1.4 (c (n "go-parser") (v "0.1.4") (d (list (d (n "borsh") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "19in71c92i8qj2k7n10r5dr7qr1p153509y38mcg6wwvn4hvxgqc") (f (quote (("default") ("btree_map")))) (s 2) (e (quote (("serde_borsh" "dep:borsh"))))))

(define-public crate-go-parser-0.1.5 (c (n "go-parser") (v "0.1.5") (d (list (d (n "borsh") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1z4pp0kwhk6l5rxqa1xvqfghikwybi73rkgf38g2a3z8193hzzr7") (f (quote (("default") ("btree_map")))) (s 2) (e (quote (("serde_borsh" "dep:borsh"))))))

