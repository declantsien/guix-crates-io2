(define-module (crates-io go -p go-parse-duration) #:use-module (crates-io))

(define-public crate-go-parse-duration-0.1.0 (c (n "go-parse-duration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0d4rqml3ph6xbqzz21mn3p4jcsfalirc66l1b5fscsg5cdq6c6k4")))

(define-public crate-go-parse-duration-0.1.1 (c (n "go-parse-duration") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1g9c5vd9d38k7nql3vf4qakfggf8fvhy4qhfmyrfbxbi92aqi2sm")))

