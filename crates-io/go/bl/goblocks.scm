(define-module (crates-io go bl goblocks) #:use-module (crates-io))

(define-public crate-goblocks-0.1.0 (c (n "goblocks") (v "0.1.0") (d (list (d (n "mlua") (r "^0.9.1") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib-ffi") (r "^4.5.3") (d #t) (k 0)))) (h "047bks633pjh3axvb6chzm4jcbs4s5mhvc8bakf76766xhja0yyp")))

(define-public crate-goblocks-0.1.2 (c (n "goblocks") (v "0.1.2") (d (list (d (n "mlua") (r "^0.9.1") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib-ffi") (r "^4.5.3") (d #t) (k 0)))) (h "1dqrkyaqy8qzi9pr0c6mn72bq09q7nl4hkbsnpgzrna0laf4wi1a")))

(define-public crate-goblocks-0.1.3 (c (n "goblocks") (v "0.1.3") (d (list (d (n "mlua") (r "^0.9.1") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib-ffi") (r "^4.5.3") (d #t) (k 0)))) (h "09jfzbb9vq6naj7fjdh5y69mya45br96b2i6j39wy9yl67mglgwb")))

