(define-module (crates-io go do godotdoc) #:use-module (crates-io))

(define-public crate-godotdoc-0.1.0 (c (n "godotdoc") (v "0.1.0") (d (list (d (n "ansi_term") (r "~0.11") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "glob") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0kx0pf2gv0an0i5ck00lai31sycif0ycm3fkbn092n6b018w6qng")))

