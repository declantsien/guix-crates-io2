(define-module (crates-io go do godot_image_replacer) #:use-module (crates-io))

(define-public crate-godot_image_replacer-0.1.1 (c (n "godot_image_replacer") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "02c429sci2ym0g8rwgwcrj7n1nq28i5rfcwh3i0npdimizb1sr57")))

