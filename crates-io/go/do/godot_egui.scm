(define-module (crates-io go do godot_egui) #:use-module (crates-io))

(define-public crate-godot_egui-0.1.0 (c (n "godot_egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "1kxxl8nxqc10h8ih1c2b7had451pg3r37f5f65s8jzync46sc6sv")))

(define-public crate-godot_egui-0.1.1 (c (n "godot_egui") (v "0.1.1") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "0xicbldjmzlppdv2wyb8r72hdxywa15ml0408kbxamax1pw3bjn4")))

(define-public crate-godot_egui-0.1.2 (c (n "godot_egui") (v "0.1.2") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "01v3jysjsqhw7i1rhy907i6zlmx1bxhrlpwmj9mz2ggnpkiichmg")))

(define-public crate-godot_egui-0.1.3 (c (n "godot_egui") (v "0.1.3") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "19ki137shmgl4xkv9pn1jwk0li6r043piyl7ikdac9r0ri07prgv")))

(define-public crate-godot_egui-0.1.5 (c (n "godot_egui") (v "0.1.5") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "0664fc5hhl88943slmw4ifkx9m92asyf3drzc2vna6d8dbjwwngn")))

(define-public crate-godot_egui-0.1.6 (c (n "godot_egui") (v "0.1.6") (d (list (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "0mgc74pwlpn3qx03s4dg8kc4yc717psmi66rl25d0f777ia0y4qz")))

(define-public crate-godot_egui-0.1.8 (c (n "godot_egui") (v "0.1.8") (d (list (d (n "egui") (r "^0.14.2") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "1disslnlgw3m16v4mjh68ky4rfmb0q3371pxwv7m2rhfw3mzi1n4")))

