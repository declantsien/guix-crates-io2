(define-module (crates-io go do godot-rust-cli-upgrader) #:use-module (crates-io))

(define-public crate-godot-rust-cli-upgrader-0.1.0 (c (n "godot-rust-cli-upgrader") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1gfpp0r3g8xnfs7lxjra58md1qw2dpha32nnnmjvbl0drxw1ikp8")))

