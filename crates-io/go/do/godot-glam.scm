(define-module (crates-io go do godot-glam) #:use-module (crates-io))

(define-public crate-godot-glam-0.1.0 (c (n "godot-glam") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kfprvr448slvmz8wgkswzpmi5qz6mxhqh6gwyilig523h0n8i87")))

(define-public crate-godot-glam-0.1.1 (c (n "godot-glam") (v "0.1.1") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1435kb3yljlfqvhp5dsh16kbjn6h9x78ymb4d3xx194nqsslaf10")))

(define-public crate-godot-glam-0.1.2 (c (n "godot-glam") (v "0.1.2") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mqyvh0g5i32h754gkn7im8z8mw18a3wc4sd105cg3sy3c1jydxv")))

(define-public crate-godot-glam-0.2.0 (c (n "godot-glam") (v "0.2.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10aiip53yabvjfa78bqyf72qnxq4rsx4m9ivfs7qfqnj9sciax00")))

(define-public crate-godot-glam-0.2.1 (c (n "godot-glam") (v "0.2.1") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d0mghvg4wfhywa94ygssmcjhap8jaxcvnk2dcin3n1257b4grgm")))

(define-public crate-godot-glam-0.3.0 (c (n "godot-glam") (v "0.3.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kasq2h59j81p5nwqkbaighpxm6vfpwcia9zkxakk5y9vcs0n57j")))

(define-public crate-godot-glam-0.4.0 (c (n "godot-glam") (v "0.4.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ddh3933r0bnvk65x574w6585f7dax59pyaayjy5945wb8582lwc")))

