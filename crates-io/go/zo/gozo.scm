(define-module (crates-io go zo gozo) #:use-module (crates-io))

(define-public crate-gozo-0.1.0 (c (n "gozo") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "async-nats") (r "^0.34") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "0r3ws3hv1k9wn9yg356mr667xk338k6sjhz4fhykxna64wzrw4cq")))

