(define-module (crates-io go vu govuk-notify-cli) #:use-module (crates-io))

(define-public crate-govuk-notify-cli-0.1.0 (c (n "govuk-notify-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h8nnrdld8v6624x91jrwqfjfkbwcs9z4q4h7wdsqpk61qw316mz") (y #t)))

