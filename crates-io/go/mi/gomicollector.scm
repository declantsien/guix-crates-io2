(define-module (crates-io go mi gomicollector) #:use-module (crates-io))

(define-public crate-gomicollector-0.1.0 (c (n "gomicollector") (v "0.1.0") (h "1x6azrdw0al89zp8h7zk77sv4wmmqanpjqqsishi8745ywhzd97y")))

(define-public crate-gomicollector-0.1.1 (c (n "gomicollector") (v "0.1.1") (h "0xskrw3jzqs41ri5mbn93qyi5dnm9g4x8spvg44z53lrhyrgj55f")))

