(define-module (crates-io go at goat) #:use-module (crates-io))

(define-public crate-goat-0.5.0 (c (n "goat") (v "0.5.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 0)))) (h "1ppdsk36n3wj16i9zslhbccpv4nrdzjx4ln3nfhlljbyfm3wp2fi")))

(define-public crate-goat-0.6.0 (c (n "goat") (v "0.6.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 0)))) (h "1mz4yvqhki955v5b08h4zf702d8csr7y3ayva1alqgsy2bs2mmzc")))

(define-public crate-goat-0.6.1 (c (n "goat") (v "0.6.1") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 0)))) (h "0yiiy27as1x2mh37llh1zmw4jy33vkf6fy6mafrc5bchlkawimxw")))

(define-public crate-goat-0.6.2 (c (n "goat") (v "0.6.2") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 0)))) (h "0zg2x6nksvdwjbgyjad4vfds8cid03zbridvbx2rhlzrlzr356sl")))

(define-public crate-goat-0.7.0 (c (n "goat") (v "0.7.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 0)))) (h "1031nc7i2is792klj755c04wg60nllqdclxlmhh2q7k32mbikaj6")))

(define-public crate-goat-0.8.0 (c (n "goat") (v "0.8.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.3") (d #t) (k 0)))) (h "189hf7zx9h1iy3sc6d271n6ymk8lxr5si1d12gn3xb0r47k7q8gx")))

(define-public crate-goat-0.9.0 (c (n "goat") (v "0.9.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.6") (d #t) (k 0)))) (h "1psbwm3zyhjz7bwffmhjjsqxh20dss6xgzk55z1vn601lvxwgg9v")))

(define-public crate-goat-0.9.1 (c (n "goat") (v "0.9.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.6") (d #t) (k 0)))) (h "02l3dgqqq6w34bikzjxyvjav9sq8rgxn0q2112cvx3kxlrzaqj2k")))

(define-public crate-goat-0.10.0 (c (n "goat") (v "0.10.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.12") (d #t) (k 0)))) (h "0d3mnn89grpfa3rh9kyi60ldwx55bayh0jc1llf7f9h93av1a495")))

