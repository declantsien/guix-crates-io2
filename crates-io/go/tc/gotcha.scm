(define-module (crates-io go tc gotcha) #:use-module (crates-io))

(define-public crate-gotcha-0.1.0 (c (n "gotcha") (v "0.1.0") (h "1s7wbfq3bcqv5n6hkjmp03rm4al5x4p3v1s9a05h4hxbd191sx30")))

(define-public crate-gotcha-0.1.1 (c (n "gotcha") (v "0.1.1") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1gynzcrj4fpjspjif15a41gf896jcgs71maxkgk4ymyilr9ly43y")))

(define-public crate-gotcha-0.1.2 (c (n "gotcha") (v "0.1.2") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1rjlgcqlwfiw39cr7xz88xd8spwvjq1i2f19csak6hjqk9kihikr")))

(define-public crate-gotcha-0.1.3 (c (n "gotcha") (v "0.1.3") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "08fhzlnhhz2qspnzb4jn7cabs4ijgy6rcfpfv9f35gsdd5n5b517")))

(define-public crate-gotcha-0.1.5 (c (n "gotcha") (v "0.1.5") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0qgslq2848mywpgn9cilk5g3341dnz17zda2ilv8cnx6y28n9hiw")))

(define-public crate-gotcha-0.1.6 (c (n "gotcha") (v "0.1.6") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1imsmskqm8w4wadhssmsc3xwfps6aj09ikk4f2dvss88x23d9a64")))

(define-public crate-gotcha-0.1.8 (c (n "gotcha") (v "0.1.8") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ivm5bys980mglqmrly38gakxw5prb7szfavm5q3aq8aqa6jhb6y")))

(define-public crate-gotcha-0.1.9 (c (n "gotcha") (v "0.1.9") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pwl9apc8azm0g275068ja67n3gis0dq4rhp6hw3vdxl98c56jjn")))

(define-public crate-gotcha-0.1.10 (c (n "gotcha") (v "0.1.10") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1khvbilnimxnyarzwnfi91dawdb264dv6bl3xz0i6k7cg56jb38i")))

(define-public crate-gotcha-0.1.11 (c (n "gotcha") (v "0.1.11") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12rsps6czpy3yvxy29bz3a8hazhbhwpbgxip853m7yr91fbchxhz")))

(define-public crate-gotcha-0.1.12 (c (n "gotcha") (v "0.1.12") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "gotcha_core") (r "^0.1") (d #t) (k 0)) (d (n "gotcha_macro") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oas") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "yaac") (r "^0.1") (d #t) (k 0)))) (h "1vijfamg5ggw5i90vic6yl6rp2kzf38jqrq24338mdhc90r5ra7d")))

