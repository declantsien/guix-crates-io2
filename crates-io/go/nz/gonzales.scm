(define-module (crates-io go nz gonzales) #:use-module (crates-io))

(define-public crate-gonzales-0.0.1-beta (c (n "gonzales") (v "0.0.1-beta") (h "1bzg9zy7cdv3j8mqg98p8dxzii1rj32c8fw6shqfdvnbg5xnb907")))

(define-public crate-gonzales-0.0.2-beta (c (n "gonzales") (v "0.0.2-beta") (h "0b8izz3kp8zany3ibri94hmzwg2jacmyf0nhw2xjgkj3sjjxz792")))

(define-public crate-gonzales-0.0.3-beta (c (n "gonzales") (v "0.0.3-beta") (d (list (d (n "actix-router") (r "^0.2.7") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.4.3") (f (quote ("criterion" "protobuf" "flamegraph"))) (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "01w0prwnqqp527qqb593fd25y4cnhgczk1fz9k8yygp4l3h3m9j7")))

