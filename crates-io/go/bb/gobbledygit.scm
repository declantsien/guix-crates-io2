(define-module (crates-io go bb gobbledygit) #:use-module (crates-io))

(define-public crate-gobbledygit-0.1.0 (c (n "gobbledygit") (v "0.1.0") (d (list (d (n "git2") (r "^0.10.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1n5pdjlk0h3y93qk15wqmm7j4953db6lcy9gvbg7m5jna9a4yjfk")))

