(define-module (crates-io go bb gobble) #:use-module (crates-io))

(define-public crate-gobble-0.1.0 (c (n "gobble") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "1s17q2ixrljkcy94ipgdjwn7jkrgdm30zj9ldrjf48d7sd0vmmm4")))

(define-public crate-gobble-0.1.1 (c (n "gobble") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "184fl0ny53952i7va60rp2ppkg1jzyhc91xwmxxzg79mkppkh0ji")))

(define-public crate-gobble-0.1.2 (c (n "gobble") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0zyag4lvsgx51gka7w0m3d7fsinvcgsb0a7hf0d8ai2rp0740kh6")))

(define-public crate-gobble-0.1.3 (c (n "gobble") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "1skxb0qr4xvnhnrw5w1sf6546g5qpqpckbdrdrim86049q59ba42")))

(define-public crate-gobble-0.1.4 (c (n "gobble") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0g1c9iik793975n0f61zllpwb47bxsy7bxhpx5hsax6g0w712l4g")))

(define-public crate-gobble-0.1.5 (c (n "gobble") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0c86hv5qb0lf868051siy5dipmcqds8s6wfm3s2anm1cfrbl0d60")))

(define-public crate-gobble-0.1.6 (c (n "gobble") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "021jdxh2hrkxnnij70vsldfzk1jgcdvfpl3a15fc3rwv36k9s9vq")))

(define-public crate-gobble-0.2.0 (c (n "gobble") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0blym4mb04ck5p1k1fy6n9g8xmnz7i1r6pn4wmb3jhdpivfbq2z8")))

(define-public crate-gobble-0.2.1 (c (n "gobble") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0s5ff9sg5cl1gci50yfglncz3c3rl8bpng2xacpvyvqbzv6dq1r5")))

(define-public crate-gobble-0.3.0 (c (n "gobble") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1my2ipqv4l18kbl40jg8w6smr0q6jlqld117v022lisbv98y33m4")))

(define-public crate-gobble-0.4.0 (c (n "gobble") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "03v4p0rq08gg5mdbqg2f5bg137284595mm2swy0dl0avfx5wmkmr")))

(define-public crate-gobble-0.4.1 (c (n "gobble") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "14nnl52qzqa3rkxx0pq48d3faq4pz1kvflq1m0v0l2zk6477ir7c")))

(define-public crate-gobble-0.4.2 (c (n "gobble") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1lg0kw6bxpllg8wpsrch49yfvmgdx5hp4960p2q78ngkbfmgaafx")))

(define-public crate-gobble-0.4.3 (c (n "gobble") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "03gvvj0p2ffkiscxbcz24jx3f65p9k8qx6kqa7sfgynaic9f6y6x")))

(define-public crate-gobble-0.4.4 (c (n "gobble") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1l94aiqm79y34vfq5j2csmjny9k1xznw0hw3sap8bqinxrma8sfr")))

(define-public crate-gobble-0.5.0 (c (n "gobble") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "0hr814gam57yy9imx95gy9w7abxk0chczp0vkf35mhn2pr07w1dk")))

(define-public crate-gobble-0.5.1 (c (n "gobble") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1ql1a17h41lia6l9399zq0i79l8a2q6slbrsnqnixx8pdxipcxzy")))

(define-public crate-gobble-0.5.2 (c (n "gobble") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1dmkkvw1xdjgf7qfw5xlc2xa6mvpzgdxjss1y5a46jiv0ghksfh9")))

(define-public crate-gobble-0.5.3 (c (n "gobble") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "14m5hayma30d5rw7g46jxg5s7c1zs1lpbrk5gpf4kfm22dd8hx68")))

(define-public crate-gobble-0.6.0 (c (n "gobble") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "04kmzdckksslaw0lcy3p9sn977vjzidnbgwzybjkmx75nyvww0m8")))

(define-public crate-gobble-0.6.1 (c (n "gobble") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "1ri5xk5mibqjpxbc7v3wr5cpbqa66ihpgkki31nhrm721bbgjqvh")))

(define-public crate-gobble-0.6.2 (c (n "gobble") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "0acjig9ni5r0rv3hkql09kd2mgxwx7qi333gw7bhb02kv77g033z")))

(define-public crate-gobble-0.6.3 (c (n "gobble") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "058xv5b4hw40xa7zg0hbxmgyaydm46sk2idimyfnycja7xyhzl7r")))

