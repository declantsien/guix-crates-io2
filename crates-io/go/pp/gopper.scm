(define-module (crates-io go pp gopper) #:use-module (crates-io))

(define-public crate-gopper-0.1.0 (c (n "gopper") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "goblin") (r "^0.8") (d #t) (k 0)) (d (n "iced-x86") (r "^1.20.0") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.12") (d #t) (k 0)) (d (n "owo-colors") (r "^4") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "10yvll34dziyygfw8yjv5kbs2mnnyiw7j0sxpdza24ql3q0c2prw")))

