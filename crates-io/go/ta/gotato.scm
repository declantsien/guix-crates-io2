(define-module (crates-io go ta gotato) #:use-module (crates-io))

(define-public crate-gotato-0.1.0 (c (n "gotato") (v "0.1.0") (h "1ygl6mwsrs9gcbnhy09ph7mr0z6nrpw13kvli23q6bp1ydsws2ga")))

(define-public crate-gotato-0.1.1 (c (n "gotato") (v "0.1.1") (h "1jski4gggwnxnj4x1cw7gj17gnipanlv62zp0vlg9dzm3b6qvd5g")))

