(define-module (crates-io go rn gorn) #:use-module (crates-io))

(define-public crate-gorn-0.1.1 (c (n "gorn") (v "0.1.1") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0phcbzcizp3rimsy4ll5nsgp9fwcb7yi189pwvym23mrj1ly21ly")))

