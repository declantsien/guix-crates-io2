(define-module (crates-io go go gogo) #:use-module (crates-io))

(define-public crate-gogo-0.1.0 (c (n "gogo") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "02d69r7ymyl49d02cbqvypdbw7409lzmxvrrvy2ygq39g3g65w51")))

(define-public crate-gogo-0.2.0 (c (n "gogo") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1qd9y5l2qpdir342z2jg5q2jak67msaqmnwjf7l84g9n9wbv4jd3")))

(define-public crate-gogo-0.3.0 (c (n "gogo") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "15sbf602pyglizlc8x61iwblmq7499k2l4lpcgddb04gz66jv98i")))

(define-public crate-gogo-0.4.0 (c (n "gogo") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0niyrd2dqf88lvv7inkhlwr8ixmrmdsdvyagxkb33yf102qvqya9")))

(define-public crate-gogo-0.5.0 (c (n "gogo") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0ndj1l8gnxdg2krn47i9v63yg79xxiqzc16h6hwd00dikd5gzgm3")))

(define-public crate-gogo-0.5.1 (c (n "gogo") (v "0.5.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0ikj0vy5ppxisbyfzpq54l77q32vkq49jn7ynjb52w71bay9d2l0")))

(define-public crate-gogo-0.6.0 (c (n "gogo") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "10rs5g8w775nqp0l36jl6afacpmfa3lzlhih82cf919k0cg37k2z")))

(define-public crate-gogo-0.6.1 (c (n "gogo") (v "0.6.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0pch213jrgrlzrsr4dawnwgpbcmqrhsfqgkrqig4m9n4sqwk8mgq")))

(define-public crate-gogo-0.7.0 (c (n "gogo") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "10kpgp8j6klm7jr442sqy39bvy494jjahws26xnhqm8061x65ybc")))

(define-public crate-gogo-0.8.0 (c (n "gogo") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1lhis0wqrflsxrx1wp2sbpgh02kfxhbxxw0g73hg7xpcybqkivmy")))

(define-public crate-gogo-1.0.0 (c (n "gogo") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1263fgmrb266d84nn4xr8f8mfzywmamixk10xb0rwzws30kb0pqr")))

(define-public crate-gogo-1.0.1 (c (n "gogo") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xl4ll4xr2sjsvnr4jr9wfgj12nfallm1nkr8pi79i60gip9xf8w")))

(define-public crate-gogo-2.0.0 (c (n "gogo") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_trim") (r "^0") (d #t) (k 0)) (d (n "tabled") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "00lmjif8rrlfr2cacf8zic2i84fnqxc90xg6890s8nphg3kbzvib")))

(define-public crate-gogo-2.0.1 (c (n "gogo") (v "2.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_trim") (r "^0") (d #t) (k 0)) (d (n "tabled") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0f1rp13r3y72ix3g3rcalkx35rym89p7axfmmmzjmd4aa1chbyih")))

(define-public crate-gogo-2.0.2 (c (n "gogo") (v "2.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_trim") (r "^0") (d #t) (k 0)) (d (n "tabled") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ks4h4hhds1q2rlnacfn26b86h2sh7b2mrs6bkvja8zrv91d48xs") (y #t)))

(define-public crate-gogo-2.0.3 (c (n "gogo") (v "2.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_trim") (r "^0") (d #t) (k 0)) (d (n "tabled") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1919j5ifvbpy2dvbnbk2hymcg21bhq378zcdi39yqwlqm5db3z7p")))

(define-public crate-gogo-2.0.5 (c (n "gogo") (v "2.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_trim") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gd4bbxh2hmf52ssj47g37mxx946arvfa41myw4523g0q4d681ha")))

