(define-module (crates-io go -h go-heap-rs) #:use-module (crates-io))

(define-public crate-go-heap-rs-0.1.0 (c (n "go-heap-rs") (v "0.1.0") (h "1bcd8kr2fjgysszpcjcng7zarvh91zqpnn1xlin7kn3823iibdwv")))

(define-public crate-go-heap-rs-0.1.1 (c (n "go-heap-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0aqc3ml49733gk1hprins3r0ic0irgpwz6aafyvfy1f1jhjixaj3")))

