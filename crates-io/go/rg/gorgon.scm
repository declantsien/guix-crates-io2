(define-module (crates-io go rg gorgon) #:use-module (crates-io))

(define-public crate-gorgon-0.0.0 (c (n "gorgon") (v "0.0.0") (d (list (d (n "axum") (r "^0.2") (d #t) (k 0)) (d (n "cyberdeck") (r "^0.0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "04y5sjk4qfmrppxykkl8vjvwrnsd6hqzlar86q1yxa2g0ich1wi6")))

