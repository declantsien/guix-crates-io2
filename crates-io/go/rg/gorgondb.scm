(define-module (crates-io go rg gorgondb) #:use-module (crates-io))

(define-public crate-gorgondb-0.0.1 (c (n "gorgondb") (v "0.0.1") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)))) (h "1cz21r3ws5djap1zz88zyg0yj2k54fw8lb0fbvkigl6glv7slg9h")))

