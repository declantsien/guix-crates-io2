(define-module (crates-io go nk gonk-database) #:use-module (crates-io))

(define-public crate-gonk-database-0.1.2 (c (n "gonk-database") (v "0.1.2") (d (list (d (n "memmap2") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.1") (f (quote ("mp3" "isomp4" "alac" "aac"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "161wxm6nnf6lqx5gbz1n2ph1ak50jxa02bj3ggbp59ryvlas1691")))

