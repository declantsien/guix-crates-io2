(define-module (crates-io go nk gonk-player) #:use-module (crates-io))

(define-public crate-gonk-player-0.0.1 (c (n "gonk-player") (v "0.0.1") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.1") (d #t) (k 0)) (d (n "optick") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "10hm6gfcxmx9gvhk4yni065dnb0jvjagy6bipcfj949dswshrnyr")))

(define-public crate-gonk-player-0.0.2 (c (n "gonk-player") (v "0.0.2") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "1b0610b6hqdafj6fdanaykybvj030cqyjlj2xx3dr35ch8yr950y")))

(define-public crate-gonk-player-0.0.3 (c (n "gonk-player") (v "0.0.3") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "1znm4zvi07593jmg5sbvh7lb269pa0icgijp0rpcik8zfb3vwxjc")))

(define-public crate-gonk-player-0.0.4 (c (n "gonk-player") (v "0.0.4") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "0jk8na4a7niscgv2y6p43ggzznczkb91rjqgp5yb23491glvdkfd")))

(define-public crate-gonk-player-0.0.5 (c (n "gonk-player") (v "0.0.5") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "0z6s8pahyg20app6ryfm8ppgiqgq5ik4805s5xpp94dxrl9rxv2h")))

(define-public crate-gonk-player-0.0.6 (c (n "gonk-player") (v "0.0.6") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "1vl58ysl22glxvkr3wf5ibxkqabchc413fr5slkqd8mms8r4lixr")))

(define-public crate-gonk-player-0.0.7 (c (n "gonk-player") (v "0.0.7") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "1al5mrwcsl6iqszyn6lad2bwmv0kva0wai31wavi1idkq0k6ia86")))

(define-public crate-gonk-player-0.0.8 (c (n "gonk-player") (v "0.0.8") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "1lc1qbax2jg851mja5h7fynd2ppw2i3kwcn1p2w9glh57fbgl8iq")))

(define-public crate-gonk-player-0.0.9 (c (n "gonk-player") (v "0.0.9") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "gonk-core") (r "^0.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.0") (f (quote ("aac" "mp3" "isomp4"))) (d #t) (k 0)))) (h "0hrydf88cyyvppmxxbjjsag248p43z8niaxs5qljz5sf3bxhmsrx")))

(define-public crate-gonk-player-0.1.2 (c (n "gonk-player") (v "0.1.2") (d (list (d (n "alsa") (r "^0.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "gonk-database") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.10.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "symphonia") (r "^0.5.1") (f (quote ("mp3" "isomp4" "alac" "aac"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("audioclient" "combaseapi" "devpkey" "errhandlingapi" "handleapi" "ksmedia" "mmdeviceapi" "synchapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0c5w8wm8f1wzvp0d2fzddkbzncsix08qpvsrf25ssh24jyar1zfm")))

