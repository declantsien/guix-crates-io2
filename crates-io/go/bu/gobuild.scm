(define-module (crates-io go bu gobuild) #:use-module (crates-io))

(define-public crate-gobuild-0.1.0-alpha.1 (c (n "gobuild") (v "0.1.0-alpha.1") (h "1b0rqmhbf5mhs6kq7v5lsrhd4b2q3qaq2cwph4zj79nrslzq51w7")))

(define-public crate-gobuild-0.1.0-alpha.2 (c (n "gobuild") (v "0.1.0-alpha.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "17mcm6kih1m6g1n57hdxg5b9mg8k2h8nr50nh5gfngdzvnj5dqbi")))

