(define-module (crates-io go ph gophers) #:use-module (crates-io))

(define-public crate-gophers-0.0.1 (c (n "gophers") (v "0.0.1") (d (list (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "14p7pk63a5n4rrxqynjkq825ssliz754n2p8qchnqczx809dfp72")))

