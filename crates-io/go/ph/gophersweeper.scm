(define-module (crates-io go ph gophersweeper) #:use-module (crates-io))

(define-public crate-gophersweeper-1.0.0 (c (n "gophersweeper") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zvj83lgfg3q7l9z5pjrj71a41saw9pslinyhmx4svp00crxgic8") (y #t)))

(define-public crate-gophersweeper-1.0.1 (c (n "gophersweeper") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c3b5df5rijgngf61mf71jkrj64iac6bsljyyrm5jm3hlf0myf1p") (y #t)))

(define-public crate-gophersweeper-1.0.2 (c (n "gophersweeper") (v "1.0.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hqxmfy1zpn2100f3889c09isya49w6hpaawmx265vd5633zr8dm") (y #t)))

(define-public crate-gophersweeper-1.0.3 (c (n "gophersweeper") (v "1.0.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1727nga2fwscb5ly9ig14gpr2splwcgrp12zqgsawjdwb8l6f1fm") (y #t)))

(define-public crate-gophersweeper-1.0.4 (c (n "gophersweeper") (v "1.0.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17kr778fwmr63ydhmhgzv8rr3d75jnnk0wrlmwivc6rrim9a3nw9") (y #t)))

(define-public crate-gophersweeper-1.0.5 (c (n "gophersweeper") (v "1.0.5") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hc9q6nd436lw30xz36ay6qhmxkmfgv71js4plr05hz8ijsz0ac2") (y #t)))

(define-public crate-gophersweeper-1.0.6 (c (n "gophersweeper") (v "1.0.6") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q2rhri5cli45hdsd81qzcfbq385cj5asja5bldpsyv77psa3f43")))

