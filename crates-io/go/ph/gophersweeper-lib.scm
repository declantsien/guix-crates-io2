(define-module (crates-io go ph gophersweeper-lib) #:use-module (crates-io))

(define-public crate-gophersweeper-lib-0.1.0 (c (n "gophersweeper-lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mzy2dhliwv7184n1z91wgvv4jv3aa03mkqslkc46yq86jbc37wq") (y #t)))

(define-public crate-gophersweeper-lib-0.1.1 (c (n "gophersweeper-lib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w69309ymjpi7mhlp95l4ziccqfsanajdnjf719zbbqgchiz4prz") (y #t)))

