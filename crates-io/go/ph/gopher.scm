(define-module (crates-io go ph gopher) #:use-module (crates-io))

(define-public crate-gopher-0.0.1 (c (n "gopher") (v "0.0.1") (h "1bghjq13p0yzvv7k298in515ysfl5lk2xb3adljwb5xmspyz1jsp")))

(define-public crate-gopher-0.0.2 (c (n "gopher") (v "0.0.2") (h "1h5kv7p6367rjrczvz9x7yaq7mbdawymz0kl56zlcm6nrgh46ail")))

(define-public crate-gopher-0.0.3 (c (n "gopher") (v "0.0.3") (h "17kqrd45zhcq5hwxqirgi5s0k56szn21jqwyf2xax1yg646jsnqj")))

