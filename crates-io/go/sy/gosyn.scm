(define-module (crates-io go sy gosyn) #:use-module (crates-io))

(define-public crate-gosyn-0.1.0 (c (n "gosyn") (v "0.1.0") (d (list (d (n "pprof") (r "^0.6.2") (f (quote ("flamegraph" "protobuf"))) (d #t) (k 2)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "14rvrfd9h5x7pv33zz5rlk12vm5gnrd9ql1lh85h4p96z8bj481b")))

(define-public crate-gosyn-0.1.1 (c (n "gosyn") (v "0.1.1") (d (list (d (n "pprof") (r "^0.6.2") (f (quote ("flamegraph" "protobuf"))) (d #t) (k 2)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "0nbrl7cp7w0ism0j84zdf7xw7z0nvl38l9g0yb7djcp3v6f9jvkn")))

(define-public crate-gosyn-0.2.0 (c (n "gosyn") (v "0.2.0") (d (list (d (n "pprof") (r "^0.6.2") (f (quote ("flamegraph" "protobuf"))) (d #t) (k 2)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "1nfmmfkw1piv2qdcazxf2nv53yf4wikvnmjpcpmx4ir9n6nhns3s")))

(define-public crate-gosyn-0.2.1 (c (n "gosyn") (v "0.2.1") (d (list (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "137cj9040swdyvm9vzc4fjms366vs645hk3bvnarhjygjbglb1sn")))

(define-public crate-gosyn-0.2.2 (c (n "gosyn") (v "0.2.2") (d (list (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "1rqya401qhs5mrcq3hh3b9p2bqgnchf9bl8r3i49f7bgrg5w910m")))

(define-public crate-gosyn-0.2.3 (c (n "gosyn") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "11fch2yzw3ma17prfd71vrw67ajffva7nzig4y82xv42n8k4p2i4")))

(define-public crate-gosyn-0.2.4 (c (n "gosyn") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "0qpkc7bxh2m57d8m8s51sl4i7wl12q96f9w035rz6i6lsm33bfbh")))

(define-public crate-gosyn-0.2.5 (c (n "gosyn") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "1a3svwly8v1xmpz99g1jaypjmb6dbl3wzahb2y5vn6ks2s63n01c")))

(define-public crate-gosyn-0.2.6 (c (n "gosyn") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "1q7qicbig6xxhqn4riz65sxxxl7fzrf44pp0z43bg9bprzh2k7h9")))

(define-public crate-gosyn-0.2.7 (c (n "gosyn") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "0s3cjjdqfsai9jwvyq6sw4574k0dvkvcbwl6mb4aw8d6d07bqzcl")))

(define-public crate-gosyn-0.2.8 (c (n "gosyn") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.11.0") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "1wg3csy7b47112aycdqd4bpn2ai7j8c9hqbs8i86yjdicvskq5hl")))

