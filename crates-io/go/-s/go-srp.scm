(define-module (crates-io go -s go-srp) #:use-module (crates-io))

(define-public crate-go-srp-0.1.0 (c (n "go-srp") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1pzxs9pig2vmi83pf232h9mb8jwbwvz3bgl8kdlgdcya028k129m")))

(define-public crate-go-srp-0.1.1 (c (n "go-srp") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1j5draj7n258qmcy2g924f5dhas0rpaks30w5qgjij3q3flr7kcy")))

(define-public crate-go-srp-0.1.2 (c (n "go-srp") (v "0.1.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1phj47gdz0h6r4nvcfpxlzh3rhj6j43h5q4wcdikag723qxd5nqg")))

(define-public crate-go-srp-0.1.3 (c (n "go-srp") (v "0.1.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1x5r1ivf24bxi8rplgqmkwylmv10wmrikdapiw3cy0sg7h8936z9")))

(define-public crate-go-srp-0.1.4 (c (n "go-srp") (v "0.1.4") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0saqyg6lz8xlgyp0594drsbv2f2y6rz5kh065hgg6xrn7ppyg5fz")))

(define-public crate-go-srp-0.1.5 (c (n "go-srp") (v "0.1.5") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0lvc0yv98yp0qi2nk75jkpjjpai4jayr875hwnbazzkj33w8dxy0")))

(define-public crate-go-srp-0.1.6 (c (n "go-srp") (v "0.1.6") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0y91ik3a0ia21mdzpkk4f29a9c0ic27bh6iv90xzhzpagmmch31m")))

