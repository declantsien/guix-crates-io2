(define-module (crates-io go -s go-spawn) #:use-module (crates-io))

(define-public crate-go-spawn-0.1.0 (c (n "go-spawn") (v "0.1.0") (d (list (d (n "state") (r "^0.5.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jhcmffagdsa8824gdhrk8qz2mj89b6v74bjyrckjphc62rqxlrd")))

(define-public crate-go-spawn-0.1.1 (c (n "go-spawn") (v "0.1.1") (d (list (d (n "state") (r "^0.5.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0lpwgnm9s07vfbj260q8vy6hlgzfv3c5myrap1n71wahs7cjbji6")))

(define-public crate-go-spawn-0.1.2 (c (n "go-spawn") (v "0.1.2") (d (list (d (n "state") (r "^0.5.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13ak9h1mgfyh25md49a9mc2m040vk1sbxa3hkyrg520r7nhykf5p")))

