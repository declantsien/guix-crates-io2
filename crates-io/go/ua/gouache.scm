(define-module (crates-io go ua gouache) #:use-module (crates-io))

(define-public crate-gouache-0.1.0 (c (n "gouache") (v "0.1.0") (h "064rphn2ac294bzsjmmzhq9527llf1icmr4idwv5agfmplr7y2mb")))

(define-public crate-gouache-0.2.0 (c (n "gouache") (v "0.2.0") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.2.2") (d #t) (k 2)) (d (n "usvg") (r "^0.9.0") (d #t) (k 2)))) (h "1c4czrh9mnr2glapd39nk4924d8v6689j982qxliklsvkcmbdp1x") (y #t)))

