(define-module (crates-io go er goertzel_algorithm) #:use-module (crates-io))

(define-public crate-goertzel_algorithm-0.1.0 (c (n "goertzel_algorithm") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)))) (h "1bnfay6pxqq0hbkbvy9hnxgl98p14p5d5fgw5xcwjb939ixc669d") (r "1.60.0")))

