(define-module (crates-io go er goertzel) #:use-module (crates-io))

(define-public crate-goertzel-0.0.0 (c (n "goertzel") (v "0.0.0") (h "0gzs0gpjbyibk9m9ylzvmwagiir4dyg0f7709h0mniq33vrgczkh")))

(define-public crate-goertzel-0.0.1 (c (n "goertzel") (v "0.0.1") (h "0pbkk3gi4c0nh51j051rh8ajqb1hg21b8yf9xjvxj4nc1101vl12")))

(define-public crate-goertzel-0.2.0 (c (n "goertzel") (v "0.2.0") (h "07jgraqz68lsl12h8zx5chcg80spf5gf6q5pyyppkzsakmavbcyz")))

