(define-module (crates-io go er goertzel-nostd) #:use-module (crates-io))

(define-public crate-goertzel-nostd-0.1.0 (c (n "goertzel-nostd") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "00152mv584rdl8fi8iqswkg63xsch8hgkc0cqpnisfwwk03gi4ri")))

(define-public crate-goertzel-nostd-0.2.0 (c (n "goertzel-nostd") (v "0.2.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "0dcm7114pv91risab680qyv1g8ajmv7imxfc3c49wq0d91k25p6i")))

(define-public crate-goertzel-nostd-0.2.1 (c (n "goertzel-nostd") (v "0.2.1") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "0b5xf1bgykxd2hsrv90njxk288qjymis0vx4vhak4519b5xhngx6")))

(define-public crate-goertzel-nostd-0.3.0 (c (n "goertzel-nostd") (v "0.3.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "03qzprxl4lxxzg2dhxcdwlaj8znvkj7l97f98vxxhyqbvsk3b3bc")))

