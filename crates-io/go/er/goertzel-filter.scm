(define-module (crates-io go er goertzel-filter) #:use-module (crates-io))

(define-public crate-goertzel-filter-0.1.0 (c (n "goertzel-filter") (v "0.1.0") (d (list (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "10y3q1f1m5kk8jvy7lfsisrq1bfm4s123b7d8pq53c6af9bgwdsb")))

(define-public crate-goertzel-filter-0.1.1 (c (n "goertzel-filter") (v "0.1.1") (d (list (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0y0wz5vi97ypvjlhb08qmmw35pj95yirlkxbbw6pxhb5xhfkkjd1")))

(define-public crate-goertzel-filter-0.2.0 (c (n "goertzel-filter") (v "0.2.0") (d (list (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0wfnfrnkriqhh5x8hh0zrklpkhb2lfrzl7qshs4xw4i9wasxw8y4")))

