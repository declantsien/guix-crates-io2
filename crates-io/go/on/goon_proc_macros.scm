(define-module (crates-io go on goon_proc_macros) #:use-module (crates-io))

(define-public crate-goon_proc_macros-0.1.0 (c (n "goon_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j3440n5aqbig5q2xpiffaib9l1ngmj0h8ic5m8lsxj6llbz2q9b")))

