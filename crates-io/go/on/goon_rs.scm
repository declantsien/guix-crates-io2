(define-module (crates-io go on goon_rs) #:use-module (crates-io))

(define-public crate-goon_rs-0.1.0 (c (n "goon_rs") (v "0.1.0") (d (list (d (n "goon_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09lnzc6a153kwas39d5ijvb9inzh6qfr51p5qpdxy2ah51zl9nl0")))

