(define-module (crates-io go _c go_core) #:use-module (crates-io))

(define-public crate-go_core-0.1.0 (c (n "go_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "01hrj31b5v2vsgh47a98hxk96nwnc8j1167h92143332q0mmsz1r")))

