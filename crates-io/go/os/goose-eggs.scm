(define-module (crates-io go os goose-eggs) #:use-module (crates-io))

(define-public crate-goose-eggs-0.1.1 (c (n "goose-eggs") (v "0.1.1") (d (list (d (n "goose") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1zacvqcidvlrg723gipsvk2x9kvwnrinv1w0hsp3kbdlhcak839a")))

(define-public crate-goose-eggs-0.1.2 (c (n "goose-eggs") (v "0.1.2") (d (list (d (n "goose") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0492jzj38dn4ayr6p2cvwhn62ivla37chflnjf7bj3q3zmzkzs8g")))

(define-public crate-goose-eggs-0.1.3 (c (n "goose-eggs") (v "0.1.3") (d (list (d (n "goose") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0snzzqkzbqkhhjzjalinfkcly5zyf0aj5gz0ifmx2v9q51ck29j6")))

(define-public crate-goose-eggs-0.1.4 (c (n "goose-eggs") (v "0.1.4") (d (list (d (n "goose") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1bbnanq5lxl51cgzl68s5kpvdmgs771zravnf30j2yfpfjd9bp23")))

(define-public crate-goose-eggs-0.1.5 (c (n "goose-eggs") (v "0.1.5") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1mqhis5swja6k6f2xm5farvdhdh04mldfxj6kx1rr2rnmdq59cz4")))

(define-public crate-goose-eggs-0.1.6 (c (n "goose-eggs") (v "0.1.6") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "17sywndjqnzz22vclmx7yzm0wpflkis6bwfkdv6nmhwvr6k3ydah")))

(define-public crate-goose-eggs-0.1.7 (c (n "goose-eggs") (v "0.1.7") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1ahw5ch8cb16nmg6jhw3al3j76pf5kinlczivq236067k70q0iil")))

(define-public crate-goose-eggs-0.1.8 (c (n "goose-eggs") (v "0.1.8") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1089sg7zpbwhbm15lc7sabz580x8p0plvf83akz60166lclv95l7")))

(define-public crate-goose-eggs-0.1.9 (c (n "goose-eggs") (v "0.1.9") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1vszfvjk5p40fm50p22mfdw0kn94vda5nbmgj9vrg6qwga40d93f")))

(define-public crate-goose-eggs-0.1.10 (c (n "goose-eggs") (v "0.1.10") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "15rkfi7p1x83k3w8276rdr6rgpl8cf68qpc3avbpgsllkb1v3bkk")))

(define-public crate-goose-eggs-0.1.11 (c (n "goose-eggs") (v "0.1.11") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1nmg7s4dh216kxgl41s18jk0zmp7673q0qzviz78fa6zf5z30jb6")))

(define-public crate-goose-eggs-0.1.12 (c (n "goose-eggs") (v "0.1.12") (d (list (d (n "goose") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0bia1xn22xmml22gi05gngcb5gymxy09cf9v339ifz6n2rrw6spd")))

(define-public crate-goose-eggs-0.2.0 (c (n "goose-eggs") (v "0.2.0") (d (list (d (n "goose") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1dpis5mad0ys6jz5kz6lwm00wgic20say6fyijj78kqjv2gn1ny9")))

(define-public crate-goose-eggs-0.3.0 (c (n "goose-eggs") (v "0.3.0") (d (list (d (n "goose") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0l6xkcycdaacx85wl9c8g591zs19dfdbmafqn01q72jk0lzfmb6d")))

(define-public crate-goose-eggs-0.3.1 (c (n "goose-eggs") (v "0.3.1") (d (list (d (n "goose") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0115gcn7zlwl0z6854xwp9a7bhx4cfn38vj9k9amh5qai0mcvsh7")))

(define-public crate-goose-eggs-0.4.0 (c (n "goose-eggs") (v "0.4.0") (d (list (d (n "goose") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0w1fh5mqhjc16qc1mslhvc02r4d4dcrbracvkvwxlp4x0xlm0b84")))

(define-public crate-goose-eggs-0.4.1 (c (n "goose-eggs") (v "0.4.1") (d (list (d (n "goose") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1g1psm6izyvyrd5wvhryds7f1wkw66dpa6hh2jgg6pvz0ld2s6cq")))

(define-public crate-goose-eggs-0.5.0 (c (n "goose-eggs") (v "0.5.0") (d (list (d (n "goose") (r "^0.17") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0b6ndxj6lmkn7xfj4mj6636lri6rrywjmpc7qh5df7kipys422zs") (f (quote (("rustls-tls" "goose/rustls-tls" "reqwest/rustls-tls") ("default" "goose/default" "reqwest/default-tls"))))))

(define-public crate-goose-eggs-0.5.1 (c (n "goose-eggs") (v "0.5.1") (d (list (d (n "goose") (r "^0.17") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0yk3pbrakprb580w14ky1sapxym33350ac9qa42xrlm2l422slw3") (f (quote (("rustls-tls" "goose/rustls-tls" "reqwest/rustls-tls") ("default" "goose/default" "reqwest/default-tls"))))))

(define-public crate-goose-eggs-0.5.2 (c (n "goose-eggs") (v "0.5.2") (d (list (d (n "goose") (r "^0.17") (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1qyxjilm4s8wq9b29yk3xk4ndafl012kgrz6vjqqanbqv65a8akw") (f (quote (("rustls-tls" "goose/rustls-tls" "reqwest/rustls-tls") ("default" "goose/default" "reqwest/default-tls"))))))

