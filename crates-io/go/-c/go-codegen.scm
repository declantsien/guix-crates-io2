(define-module (crates-io go -c go-codegen) #:use-module (crates-io))

(define-public crate-go-codegen-0.1.0 (c (n "go-codegen") (v "0.1.0") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "go-types") (r "^0.1.0") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "0d9r3b2dflqbdqzim77m7k26mwfq868k21qq4mahrxiqqkw9fj0x") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

(define-public crate-go-codegen-0.1.1 (c (n "go-codegen") (v "0.1.1") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "go-types") (r "^0.1.0") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "0wljk9z5phhhd94w24g3x9xij2w78szy6l4ayrybx6285997f2ai") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

(define-public crate-go-codegen-0.1.2 (c (n "go-codegen") (v "0.1.2") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "go-types") (r "^0.1.1") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.1") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "02l8f78l2adljxwmvl91n1cci61ljj4bpsj7jdvda4x5ss7nmqc5") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

(define-public crate-go-codegen-0.1.3 (c (n "go-codegen") (v "0.1.3") (d (list (d (n "go-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "go-types") (r "^0.1.1") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.2") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "1gjh8nbrlgrd0j29ckrr1f1r197sqhkg1s20j8r88gapy830q7ng") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

(define-public crate-go-codegen-0.1.4 (c (n "go-codegen") (v "0.1.4") (d (list (d (n "go-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "go-types") (r "^0.1.4") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.4") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "108pl7by45gs5srm2jr1hq631sindbpzkdp15ckvv7djxzmp2mwg") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

(define-public crate-go-codegen-0.1.5 (c (n "go-codegen") (v "0.1.5") (d (list (d (n "go-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "go-types") (r "^0.1.5") (d #t) (k 0)) (d (n "go-vm") (r "^0.1.5") (d #t) (k 0)) (d (n "time-test") (r "^0.2.2") (d #t) (k 2)))) (h "0955370p8w5sklss9mwpa1nfna49f2in5lga8n35zwi6dklchy1q") (f (quote (("default") ("btree_map" "go-parser/btree_map") ("async" "go-vm/async"))))))

