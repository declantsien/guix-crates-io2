(define-module (crates-io go ud gouda) #:use-module (crates-io))

(define-public crate-gouda-0.1.0 (c (n "gouda") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "1n7ansqymigwbs9mgk51kisz9v0w3zmy2jkb6l41xm6i6db7vdbw")))

(define-public crate-gouda-0.2.0-alpha (c (n "gouda") (v "0.2.0-alpha") (d (list (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0mcacml5ilj3m6f4nl9fs7xxnyk9w52q4cphwkambcjn8n4apimx")))

