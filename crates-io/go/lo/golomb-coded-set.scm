(define-module (crates-io go lo golomb-coded-set) #:use-module (crates-io))

(define-public crate-golomb-coded-set-0.1.0 (c (n "golomb-coded-set") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.3") (d #t) (k 0)) (d (n "hex") (r "= 0.3.2") (d #t) (k 2)))) (h "15dh44cx0a8wkvjyg1ifzvsibk3snf78gzaf4h9bn02dcf14qk23")))

(define-public crate-golomb-coded-set-0.2.0 (c (n "golomb-coded-set") (v "0.2.0") (d (list (d (n "ckb-hash") (r "^0.103.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "=0.3.2") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0zlsxa4cpj42xzbmc4kax2jlgpa3ck1j43svg15xhmv2rp07cw2c")))

(define-public crate-golomb-coded-set-0.2.1 (c (n "golomb-coded-set") (v "0.2.1") (d (list (d (n "ckb-hash") (r "^0.103.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "=0.3.2") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "161g3rrzpvnvp24garbar8x3zy3qjm9qi0qakl7pynzvk5532bw1")))

