(define-module (crates-io go li golink) #:use-module (crates-io))

(define-public crate-golink-0.1.0 (c (n "golink") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1jrzr2k5alk2wmx9ir3vnmv226yalksv51461jrv8spkc8a0my50")))

(define-public crate-golink-0.2.0 (c (n "golink") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "15vzrdyln1is66yi1w2n8l33vbk50fdls8ayqap9fyh7a55wsz3c")))

(define-public crate-golink-0.5.0 (c (n "golink") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0shq031li8dhp6s8g1vbxd54k6ajvzymbhgxjg9hj0cxrxf7wr22")))

(define-public crate-golink-0.6.0 (c (n "golink") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "080qvvblzl98jhwxm5201r30l83gdavvzznkih9vyz401dmgs8nd")))

