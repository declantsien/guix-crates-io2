(define-module (crates-io go li goliard) #:use-module (crates-io))

(define-public crate-goliard-0.1.0 (c (n "goliard") (v "0.1.0") (d (list (d (n "braille") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "cursive") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "smeagol") (r "^0.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0s7zzmn58327xmd3jjwha361518qq6xwvwjdz6yjdpmsai4lvb23")))

