(define-module (crates-io go ry gory) #:use-module (crates-io))

(define-public crate-gory-0.0.11 (c (n "gory") (v "0.0.11") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1fkpqpv2287fgb4bmzkgwvhl7hvh8nhipvrqby749rdm9bl81vl8")))

(define-public crate-gory-0.1.1 (c (n "gory") (v "0.1.1") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1sa58zbwg5shkj35hcagyq3rihlrdjssxy9bdnbg2nlv8b1g506n")))

(define-public crate-gory-0.1.6 (c (n "gory") (v "0.1.6") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1gap3ghsmgy73hj850k1sz1mxzwacfpadb28442z17zwhsi1lagb")))

(define-public crate-gory-0.1.7 (c (n "gory") (v "0.1.7") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0wxri4328a23pbcinjrqqglzkn92j40hgvr7rmg6k571xlq32xgw")))

(define-public crate-gory-0.1.8 (c (n "gory") (v "0.1.8") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1vhbxj5n4mk2xn4jbz5x69dkwsjyfwag5039sls8babciq6vprzi")))

(define-public crate-gory-0.1.9 (c (n "gory") (v "0.1.9") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1w8480b2yxvciv903l8zsq0jrc9rsw01dq1rdqyzyn0dp9mlb88v")))

(define-public crate-gory-0.1.10 (c (n "gory") (v "0.1.10") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1zx3jyq2f6h2j2nm2pfg93f7dj9k454c18d1ivi4vwb6ifc2f70y")))

(define-public crate-gory-0.1.11 (c (n "gory") (v "0.1.11") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1j7vs1pal7x31z1pramhb66h0svswli0drhzalr0p70pcri31p6p")))

(define-public crate-gory-0.1.13 (c (n "gory") (v "0.1.13") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0blm41mqcpswzn34majlkd6i96agi4jybqyinwlnd9bsmzhkrvhd")))

(define-public crate-gory-0.1.14 (c (n "gory") (v "0.1.14") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1pg18vc0bvfma1wb69x9jjb6rjf51kiyqif6qckacr46lfg1rz8q")))

