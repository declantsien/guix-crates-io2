(define-module (crates-io go bj gobject-2-0-sys) #:use-module (crates-io))

(define-public crate-gobject-2-0-sys-0.46.0-dev (c (n "gobject-2-0-sys") (v "0.46.0-dev") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v52g14bv0b7561cagrh5z9rb2dk1dw870281lg45vhx00l3ls22") (y #t)))

(define-public crate-gobject-2-0-sys-0.46.0 (c (n "gobject-2-0-sys") (v "0.46.0") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1p398p5v5kmyncirh9lsv6db9p1dr1ky968aqrhp645myif61hr0")))

(define-public crate-gobject-2-0-sys-0.46.1 (c (n "gobject-2-0-sys") (v "0.46.1") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1904san8slpqm1h2025fqhcw240164v29gbnm72dnd0l7ds443sb")))

(define-public crate-gobject-2-0-sys-0.46.2 (c (n "gobject-2-0-sys") (v "0.46.2") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ldv4mqzslybz01jyp28d3nnsgx5mqhq8a43z93cxx61j9b3z41r")))

(define-public crate-gobject-2-0-sys-0.46.3 (c (n "gobject-2-0-sys") (v "0.46.3") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qd8dh9dk193cj7cr17gsjrldnw9wsih6kq6w1qz1xi24p1fjqvx")))

(define-public crate-gobject-2-0-sys-0.46.4 (c (n "gobject-2-0-sys") (v "0.46.4") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i5r52vz50m60khy0fmgm9p6zd4jr3zqr2bmf5a37ga8wq4yxnnn")))

