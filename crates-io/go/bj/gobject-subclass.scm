(define-module (crates-io go bj gobject-subclass) #:use-module (crates-io))

(define-public crate-gobject-subclass-0.1.0 (c (n "gobject-subclass") (v "0.1.0") (d (list (d (n "glib") (r "^0.5") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01j6rz0czysw8q8fxsm6k677zpm377a55pjh7zr3w7xdm3i0gra6")))

(define-public crate-gobject-subclass-0.2.0 (c (n "gobject-subclass") (v "0.2.0") (d (list (d (n "glib") (r "^0.6") (d #t) (k 0)) (d (n "glib-sys") (r "^0.7") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wqgjd9wj4r8dw7s7mz913943xf4x4j84lyzwb5v2bsdh377k5rx")))

(define-public crate-gobject-subclass-0.2.1 (c (n "gobject-subclass") (v "0.2.1") (d (list (d (n "glib") (r "^0.6") (d #t) (k 0)) (d (n "glib-sys") (r "^0.7") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dh1yaxrbfznhaffwzj2f3l4xbh579pzik7jq17r278pjyn6b8r3")))

