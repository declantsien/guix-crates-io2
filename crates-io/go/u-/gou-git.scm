(define-module (crates-io go u- gou-git) #:use-module (crates-io))

(define-public crate-gou-git-0.1.0 (c (n "gou-git") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b0myw3hylnml88jrihpb5mh0ygbpgs9kxg94i614gf6jiphq20i")))

(define-public crate-gou-git-0.2.0 (c (n "gou-git") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty-log") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1zb7p28n2g2qr84434c61ri7q3msyldvrsjkn3lmykbsj1yhicip")))

