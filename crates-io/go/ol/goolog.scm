(define-module (crates-io go ol goolog) #:use-module (crates-io))

(define-public crate-goolog-0.1.0 (c (n "goolog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "074rp51plzpyi8pi8bjjvqxqj7garwxkdsi83l7p5844c27wb8an")))

(define-public crate-goolog-0.2.0 (c (n "goolog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.25") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1wairxvhj2w1plcpg4q0imm1laz78p66dc5a771hylm7073889bw") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("timestamp" "dep:chrono")))) (r "1.60")))

(define-public crate-goolog-0.2.1 (c (n "goolog") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.25") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1jc9p6bs55mazkz50q7hq0560a21scxlisvq279sjgscwi0vz9bk") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("timestamp" "dep:chrono")))) (r "1.60")))

(define-public crate-goolog-0.3.0 (c (n "goolog") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.25") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0yfqxqc5gmrdaqai8hq2g99p2p04qhkv9y3460mv3aa29a9sxs58") (f (quote (("esp") ("default" "timestamp")))) (s 2) (e (quote (("timestamp" "dep:chrono")))) (r "1.60")))

(define-public crate-goolog-0.4.0 (c (n "goolog") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0cby27im6djpa4ak6q1ca6m5svrg17k9rzkak1814jxgwnxyqzfq") (f (quote (("esp") ("default" "timestamp")))) (s 2) (e (quote (("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.4.1 (c (n "goolog") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1a3mzjxx11yw1wdsjp3l9r13njypfnlssl783kppwn8m1acn4yyg") (f (quote (("esp") ("default" "timestamp")))) (s 2) (e (quote (("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.5.0 (c (n "goolog") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "0ridh44war30h2hqmmxxy91c52brpif4pss6srqbvi1x2ac18wq0") (f (quote (("esp") ("default" "timestamp")))) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.6.0 (c (n "goolog") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "1v39m9y3ip2d96lxk9rvk6rrdaxyw87grzwkcnz2knzgwc79rcq2") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.7.0 (c (n "goolog") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "0p6wmqn2v4mxmhmm2k9p4k7i87ps6qdrlj4w1yr4045pvd6kwrcb") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.8.0 (c (n "goolog") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "0wm1bj4m2iyf3z96gsx80w2brz1b5nz619qxm35k2n95fjd2l6q9") (f (quote (("default" "timestamp")))) (y #t) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.8.1 (c (n "goolog") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "1kh8qa617zldfj9fzpll3i1caazv2kqrfw0w3nv7wkb37m84rw50") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.9.1 (c (n "goolog") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "0d9krpzfzgws3r8hhk7c3smn5xwy547rfd8nv3fgnka1p9rd2174") (f (quote (("default" "timestamp")))) (s 2) (e (quote (("wasm" "dep:web-sys") ("timestamp" "dep:chrono"))))))

(define-public crate-goolog-0.10.0 (c (n "goolog") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.34") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1afqq9qafw29r41ygzxlccqhs6g88165xkfmql73visapaqans04") (f (quote (("default")))) (s 2) (e (quote (("std" "dep:chrono"))))))

