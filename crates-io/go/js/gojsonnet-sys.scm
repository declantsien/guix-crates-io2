(define-module (crates-io go js gojsonnet-sys) #:use-module (crates-io))

(define-public crate-gojsonnet-sys-0.17.0 (c (n "gojsonnet-sys") (v "0.17.0") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "048iypykphvx8i9lypd4ayjk9fbizm35l7hgb37prsinh79i2fbx")))

(define-public crate-gojsonnet-sys-0.17.0+p1 (c (n "gojsonnet-sys") (v "0.17.0+p1") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0nllf0xqvdl6dpk32fvag1m31d62xy9d7mnqj9wnm1xxm36r3xgb")))

(define-public crate-gojsonnet-sys-1.0.0-alpha.1+0.17.0-57e6137 (c (n "gojsonnet-sys") (v "1.0.0-alpha.1+0.17.0-57e6137") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1iaqap7ia13i27js1q9j66y64qha4kj0nm5h062g1f26hkb00dl8")))

(define-public crate-gojsonnet-sys-1.0.0-alpha.2+0.17.0-57e6137 (c (n "gojsonnet-sys") (v "1.0.0-alpha.2+0.17.0-57e6137") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0gci7fslz4kcjvdz15pwgq14nddrfsjsmp1dwi3y0lx74612cldc")))

(define-public crate-gojsonnet-sys-1.0.0-alpha.3+0.17.0-35acb29 (c (n "gojsonnet-sys") (v "1.0.0-alpha.3+0.17.0-35acb29") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mvkzvv13pm3wxfbd444mvyznd6aqyigvbyw74mw6qghy836w80r")))

