(define-module (crates-io go od goods-treasury-import-ffi) #:use-module (crates-io))

(define-public crate-goods-treasury-import-ffi-0.1.0 (c (n "goods-treasury-import-ffi") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "goods-treasury-import") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1c2dp329i8djxq9p76vi7w0w0zfdmcmqpl4nx5qvr0qmjqbdayq8")))

