(define-module (crates-io go od good_stv) #:use-module (crates-io))

(define-public crate-good_stv-0.1.0 (c (n "good_stv") (v "0.1.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "0nkcsk4al7k2hqvxb44r9qihqs6mr3c3dswym65b5gjvmp83jfh8")))

