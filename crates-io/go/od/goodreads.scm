(define-module (crates-io go od goodreads) #:use-module (crates-io))

(define-public crate-goodreads-0.1.0 (c (n "goodreads") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i84p6b7i40760qhrpqmh4rx2wb13ryd7w4mxq29vgasic5kmag8")))

(define-public crate-goodreads-0.1.1 (c (n "goodreads") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pxwfbbic0gkfkli00q95ilfin5cmxy9i8sv54dz82iih3ngcnkv")))

