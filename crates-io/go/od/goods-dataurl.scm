(define-module (crates-io go od goods-dataurl) #:use-module (crates-io))

(define-public crate-goods-dataurl-0.1.0 (c (n "goods-dataurl") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "goods") (r "^0.8.0") (d #t) (k 0)))) (h "0yzi0pyxj0sfy1np7868hdcis9b838kl5sxh8icwpl2l13a3w702")))

