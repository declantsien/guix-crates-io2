(define-module (crates-io go od goodnight) #:use-module (crates-io))

(define-public crate-goodnight-0.1.0 (c (n "goodnight") (v "0.1.0") (h "118f4dxdr8ljdk1dfiw56r40alv7r98zvhiscdd5a1jy4hhjlmjv")))

(define-public crate-goodnight-0.1.1 (c (n "goodnight") (v "0.1.1") (h "0lfr1g69y53f9vh646z2nzipcj1ffgn9hi5g26s9hgal0a2p17d5")))

(define-public crate-goodnight-0.1.2 (c (n "goodnight") (v "0.1.2") (h "061wb8f9cqr7p5yny13y05qad3p676y2kbs4gnpifmmdsjx7qx7g")))

