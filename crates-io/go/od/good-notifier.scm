(define-module (crates-io go od good-notifier) #:use-module (crates-io))

(define-public crate-good-notifier-0.1.0 (c (n "good-notifier") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1yz50p5waacpacwxwv375pw0gdg6lmxl2g819cpi1gwj3n9wnlpi")))

(define-public crate-good-notifier-0.1.101 (c (n "good-notifier") (v "0.1.101") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "16k8fb817mqr5gnjwikrag9yv2myz6zyzhdqhnzay5c6sz85km62") (y #t)))

(define-public crate-good-notifier-0.1.12 (c (n "good-notifier") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1c302g1z1hsz2f00q3ndmks05f928clvav0vzbxbcvzasc23xsqr")))

(define-public crate-good-notifier-0.1.102 (c (n "good-notifier") (v "0.1.102") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "03ydg8p61lbhqisgg8qnjyagimq4qy5396jjxrk2ckxw404l2i40")))

