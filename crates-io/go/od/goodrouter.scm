(define-module (crates-io go od goodrouter) #:use-module (crates-io))

(define-public crate-goodrouter-0.1.3 (c (n "goodrouter") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hapdikkxxb26crh1bwvzwca1biphkgjp98rrhi3w353qa1va04x") (y #t)))

(define-public crate-goodrouter-1.0.0 (c (n "goodrouter") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gjha9p41fzipcyx5llrxjw0p809j82i5fdjcm249zdgx4rwg9p0") (y #t)))

(define-public crate-goodrouter-1.0.1 (c (n "goodrouter") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yy7dskbj15yfr9dq5x6iawlfgdhgwybvrkdf1b6x83jcd3yfcgv") (y #t)))

(define-public crate-goodrouter-1.0.2 (c (n "goodrouter") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mbd6sz0kvl6a5qddc3sil9ka98yn1a3r3zqhxwig11797z0lpqh") (y #t)))

(define-public crate-goodrouter-1.0.3 (c (n "goodrouter") (v "1.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bhyvh8s4764k0v7715pxyqjysv5kpr8zczypg9d80xzpdl1z041") (y #t)))

(define-public crate-goodrouter-1.0.4 (c (n "goodrouter") (v "1.0.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14s9cg2frfj3k1i8jifjvghjfd7wn80rp45awg8cgirkwqxpixfn") (y #t)))

(define-public crate-goodrouter-1.0.5 (c (n "goodrouter") (v "1.0.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xdgaxlgnqqm5cd0nj2pfrw5a5k08avjx220p6hbmyx5m83nnd8c") (y #t)))

(define-public crate-goodrouter-1.0.8 (c (n "goodrouter") (v "1.0.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0p0kyla8yc59637hksa7xz65ycd6kx1s6csj3wivzk60wwamzrg4") (y #t)))

(define-public crate-goodrouter-1.0.10 (c (n "goodrouter") (v "1.0.10") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1hz4khrxz4xnia7nmm75vlxvgw5kn13yc12cy3fl6sil9mifld88") (y #t)))

(define-public crate-goodrouter-1.0.11 (c (n "goodrouter") (v "1.0.11") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0nvd7mwrhxs40pnd7y6rghyxd1bbn1r2h18cr4x6miqmyyw0csig")))

(define-public crate-goodrouter-1.0.12 (c (n "goodrouter") (v "1.0.12") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0dhkr6xc0mxrzwhpm5jhyvdh40p9jsdcs9kr4cycqb3b01ajnxj2")))

