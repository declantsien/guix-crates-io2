(define-module (crates-io go od goods-yaml) #:use-module (crates-io))

(define-public crate-goods-yaml-0.1.0 (c (n "goods-yaml") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "goods") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10p2zxij7jpbl3lwi9r4r1l5nr46nb81bpihpc3r3203763d56y0")))

