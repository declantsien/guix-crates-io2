(define-module (crates-io go od goods-treasury-import) #:use-module (crates-io))

(define-public crate-goods-treasury-import-0.1.0 (c (n "goods-treasury-import") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1v8dh9yx9pa1pd5jzb46c5qq56hq1a8g0m4jiy2vh4x8hssmiczv") (f (quote (("ffi") ("default" "ffi"))))))

(define-public crate-goods-treasury-import-0.2.0 (c (n "goods-treasury-import") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0lnam0287nbz1vpw8q86lldfnnyy2dgbkzzr4x6r6x32cs90kjns") (f (quote (("ffi") ("default" "ffi"))))))

