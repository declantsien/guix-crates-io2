(define-module (crates-io go od goodsql) #:use-module (crates-io))

(define-public crate-GoodSql-0.1.0 (c (n "GoodSql") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "1qzidzpdkqfz495ji1b0acaq345f2dwahhcv9j8xh3ls0i57nvgi") (y #t)))

(define-public crate-GoodSql-0.1.1 (c (n "GoodSql") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "0y9w7xikjzkfzpms6g3xsm5ivib9807np64yldh5wxfa83wqff7p") (y #t)))

