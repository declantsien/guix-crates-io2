(define-module (crates-io go od good-git) #:use-module (crates-io))

(define-public crate-good-git-0.1.0 (c (n "good-git") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ferris-says") (r "^0.3.1") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "121s3phf6xnn0zqbggyqkpz1q1yc8x2mhlyp5hgvgnbgr58mzmwl")))

(define-public crate-good-git-0.1.1 (c (n "good-git") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "0kd5iq5min65zr84b528qlaayn85nri6als4iw3bm3ypgzbf5ixy")))

(define-public crate-good-git-0.1.2 (c (n "good-git") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "128kz7i6xnhm3mzwbanwgc77wnllbpxvp00hjp52r8g3m75ji416")))

(define-public crate-good-git-0.1.3 (c (n "good-git") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "1kd8nqhb8ix8z7njh7f2lk70554vranv6a5wpdzy0xfdpj1qzp3d")))

