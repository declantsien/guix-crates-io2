(define-module (crates-io go od goodname) #:use-module (crates-io))

(define-public crate-goodname-0.2.1 (c (n "goodname") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "0qlgz19fbpdn0zlp1jh3qfgil66mq89g9z2qvkipwpzccj9d0w1l")))

(define-public crate-goodname-0.2.2 (c (n "goodname") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "09qpykhwn5k12m1xxnhcjq4q0bafzhi7vgidvl6qccks6qcsy5kk")))

