(define-module (crates-io go od goods-json) #:use-module (crates-io))

(define-public crate-goods-json-0.1.0 (c (n "goods-json") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "goods") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xr9bv6dah81mv3ns2lzxaq5hsv41js0kvm3x4gkgvq0wxrx7jr5")))

