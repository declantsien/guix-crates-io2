(define-module (crates-io go od goods-fs) #:use-module (crates-io))

(define-public crate-goods-fs-0.1.0 (c (n "goods-fs") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "goods") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1ivbqv6syr2bjq087pwzq5cfwp22y2pcxmpfc7xr63i0ar4bqqma")))

