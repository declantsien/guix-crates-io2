(define-module (crates-io go gl goglob-serde-tests) #:use-module (crates-io))

(define-public crate-goglob-serde-tests-0.2.0 (c (n "goglob-serde-tests") (v "0.2.0") (d (list (d (n "goglob") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.136") (d #t) (k 0)))) (h "0mjmr6bpzkkwzwr1axgkkzhxz1gz54k3gyv8ack4zcp4xli4kzqj")))

