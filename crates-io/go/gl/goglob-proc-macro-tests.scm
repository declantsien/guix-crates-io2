(define-module (crates-io go gl goglob-proc-macro-tests) #:use-module (crates-io))

(define-public crate-goglob-proc-macro-tests-0.2.0 (c (n "goglob-proc-macro-tests") (v "0.2.0") (d (list (d (n "goglob") (r "^0.2.0") (f (quote ("proc-macro"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "1hrd4mv6b9iqr86ak8x1zivxz06ln7vywmidklzrk4lzvyih70af")))

