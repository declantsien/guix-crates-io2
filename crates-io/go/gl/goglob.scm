(define-module (crates-io go gl goglob) #:use-module (crates-io))

(define-public crate-goglob-0.2.0 (c (n "goglob") (v "0.2.0") (d (list (d (n "goglob-common") (r "^0.2.0") (d #t) (k 0)) (d (n "goglob-proc-macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1hzf711l2pkqc4psd3d3h6l0yzw66g3pm5bfiwqis7i8pf5dbzy2") (f (quote (("serde" "goglob-common/serde")))) (s 2) (e (quote (("proc-macro" "dep:goglob-proc-macro"))))))

