(define-module (crates-io go gl goglob-proc-macro) #:use-module (crates-io))

(define-public crate-goglob-proc-macro-0.2.0 (c (n "goglob-proc-macro") (v "0.2.0") (d (list (d (n "goglob-common") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1i3gn2l0yiabaqx09y576midl7xzjr6f1rla4zfq3bv3gd43bi11")))

