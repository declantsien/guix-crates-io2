(define-module (crates-io go ss gossip-relay-picker) #:use-module (crates-io))

(define-public crate-gossip-relay-picker-0.1.0 (c (n "gossip-relay-picker") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "nostr-types") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "14r5xql60v5pygnlf7yhlajm7hjgwacl6z1hsgwb4i75nysm7jfg")))

