(define-module (crates-io go ss gossip) #:use-module (crates-io))

(define-public crate-gossip-0.0.1 (c (n "gossip") (v "0.0.1") (d (list (d (n "blake3") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0-alpha-2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "123qh5y6c8q9x452mr5fja3ggcb8kxn3yaldncmpd295m0xgl5fi")))

(define-public crate-gossip-0.0.2 (c (n "gossip") (v "0.0.2") (d (list (d (n "blake3") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0-alpha-2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1xq59ak90pasq83zna0h3lclx3a4m3kqa5vdi8z4xll609l49zw4")))

(define-public crate-gossip-0.0.3 (c (n "gossip") (v "0.0.3") (d (list (d (n "blake3") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1j4wfsxs5dzjlzcsasf2a96jkpwsikyj7zrcfvzrkvkisvf7pi3b")))

