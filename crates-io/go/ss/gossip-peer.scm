(define-module (crates-io go ss gossip-peer) #:use-module (crates-io))

(define-public crate-gossip-peer-0.0.1 (c (n "gossip-peer") (v "0.0.1") (h "1wnc6b1rm4y05ja69bw3p5961qc05s0i63arv68qjqgianfkv1jp")))

(define-public crate-gossip-peer-0.1.0 (c (n "gossip-peer") (v "0.1.0") (d (list (d (n "borrowed-byte-buffer") (r "^0.1.0") (d #t) (k 0)))) (h "1b5wpgdnsmpi6jhcsx5ich14wgxb4kh9va6zii2h9yxilfak5av0")))

