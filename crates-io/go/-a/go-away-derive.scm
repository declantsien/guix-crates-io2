(define-module (crates-io go -a go-away-derive) #:use-module (crates-io))

(define-public crate-go-away-derive-0.1.0 (c (n "go-away-derive") (v "0.1.0") (d (list (d (n "go-away-derive-internals") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08mwmsswgmjh6kam8b52zjr3l1b3mlw24bp4jlks60fflm2qjjji")))

(define-public crate-go-away-derive-0.1.1 (c (n "go-away-derive") (v "0.1.1") (d (list (d (n "go-away-derive-internals") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "131i00rl4dvmkq6h9xaj0q33rfrmcgnix5h67c3imcv7c09sz49v")))

(define-public crate-go-away-derive-0.2.0 (c (n "go-away-derive") (v "0.2.0") (d (list (d (n "go-away-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j0m0bp3bpcm0pi6ahg5psl32148877xn6jgpn9ak812yayqqzy2")))

(define-public crate-go-away-derive-0.3.0 (c (n "go-away-derive") (v "0.3.0") (d (list (d (n "go-away-derive-internals") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18nq4xsfc33r83mdir1iavpr3bzigy51cwgxhp1zvw14h6yzsz0q")))

(define-public crate-go-away-derive-0.4.0 (c (n "go-away-derive") (v "0.4.0") (d (list (d (n "go-away-derive-internals") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qx57247c6cc28cdl6prxvdfbb2cnaf2s354bci75l6fijnfyb11")))

(define-public crate-go-away-derive-0.5.0 (c (n "go-away-derive") (v "0.5.0") (d (list (d (n "go-away-derive-internals") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03x0x4f4hbk4mmja3d4ffp5sc3iqqss3wrf8ybrw3fgjgjz25wi9")))

