(define-module (crates-io go -a go-away-derive-internals) #:use-module (crates-io))

(define-public crate-go-away-derive-internals-0.1.0 (c (n "go-away-derive-internals") (v "0.1.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "1h0izx0pngr0mnah89giinvwc8yngnfx237zqpzqzb7ymq360prv")))

(define-public crate-go-away-derive-internals-0.1.1 (c (n "go-away-derive-internals") (v "0.1.1") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "1ma39k0dvsp8cyc4d6zaj1jffwrxqyn2g1vmwjws48xrszb26izm")))

(define-public crate-go-away-derive-internals-0.2.0 (c (n "go-away-derive-internals") (v "0.2.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "1nk8vyrav97d5sw5x3fbdxk86z2gi8189jkw43gp3j7xyakdivhl")))

(define-public crate-go-away-derive-internals-0.3.0 (c (n "go-away-derive-internals") (v "0.3.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "06nij7g0qdpwgsnspxmg001dh4br43k1nsazshsan8irkqs1l3zd")))

(define-public crate-go-away-derive-internals-0.4.0 (c (n "go-away-derive-internals") (v "0.4.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "049l4qn29dlgihmhsfarxmwn741sjapqmf7zzvb9gp9bhmcd7pcp")))

(define-public crate-go-away-derive-internals-0.5.0 (c (n "go-away-derive-internals") (v "0.5.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "=0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xshell") (r "^0.1.13") (d #t) (k 2)))) (h "0nbcd11l4vllyvdxhkqcyryiv6bgwkmigxc34r9fwl0dpsczvxjx")))

