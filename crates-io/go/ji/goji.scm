(define-module (crates-io go ji goji) #:use-module (crates-io))

(define-public crate-goji-0.1.0 (c (n "goji") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1sgnkzw6icn42w85fzmi518b85v17x7739fpql6mb1lq2h7nc9lr")))

(define-public crate-goji-0.1.1 (c (n "goji") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0xlgz2qc5ngk4zp0x209swcyrqjs58i6s6rbssm56ilbhhprfhzd")))

(define-public crate-goji-0.2.0 (c (n "goji") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "url") (r "~1.5") (d #t) (k 0)))) (h "09mhg62j4l9y352xr9yhc5w7yqz2cblj04hpnhri8yv7y5rgmasf")))

(define-public crate-goji-0.2.1 (c (n "goji") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1rwhsw79bmzcm3wic6p2ng1gdx5bd50drw61mrwq8aq9flmi2c7i")))

(define-public crate-goji-0.2.2 (c (n "goji") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "10v5y2avhqs41napa8q3in1wgmbi7in8lq8q1w5cwdjn7hv141rw")))

(define-public crate-goji-0.2.3 (c (n "goji") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1631zlyzzzw8283s7y0v25h0w9asnydgd7maf2j41imjjd41y2kn")))

(define-public crate-goji-0.2.4 (c (n "goji") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "06gr9pm5y8a91i14wrsnn018mjkbadjbsxapmv0qh77rzs5gzmrs")))

