(define-module (crates-io go al goal) #:use-module (crates-io))

(define-public crate-goal-1.0.0 (c (n "goal") (v "1.0.0") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "inner") (r "^0.1.1") (d #t) (k 0)) (d (n "open") (r "^1.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "08iymn1lwbbkq11abw827dh4wa8dd573zcp26cpjnl788s58gjcf") (y #t)))

(define-public crate-goal-1.0.1 (c (n "goal") (v "1.0.1") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "inner") (r "^0.1.1") (d #t) (k 0)) (d (n "open") (r "^1.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0d75mc38825s36yz3li13qw9gnfa3r31f17cc1bmbbwr4lrvlvqi")))

