(define-module (crates-io go du godunov) #:use-module (crates-io))

(define-public crate-godunov-0.1.0 (c (n "godunov") (v "0.1.0") (d (list (d (n "gauss-quad") (r "^0.1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "nalgebra-lapack") (r "^0.10") (d #t) (k 0)))) (h "1xrafjrqf5wb8scabym03k46z27id7myg092zn2mj2hifg6gcb9c")))

