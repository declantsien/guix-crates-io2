(define-module (crates-io go dw godwoken-polyjuice) #:use-module (crates-io))

(define-public crate-godwoken-polyjuice-0.0.1 (c (n "godwoken-polyjuice") (v "0.0.1") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "015lwa051wh4djxnziparmzija1a3rsgwd1p2cryz3lr44yrnydg")))

(define-public crate-godwoken-polyjuice-0.0.2 (c (n "godwoken-polyjuice") (v "0.0.2") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "18mgiqg15iyrd42h964vllv9gghmbr0x0gny7q8hbxfwc65plvz5")))

(define-public crate-godwoken-polyjuice-0.0.3 (c (n "godwoken-polyjuice") (v "0.0.3") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1qdpcgpj2vz9hcarzbxvjy7chcpbkw9s1hsflnc1yp60fflprz5p")))

(define-public crate-godwoken-polyjuice-0.0.4 (c (n "godwoken-polyjuice") (v "0.0.4") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "0prrr8nci2d00y50gy116c489kfq4iw2nzhwd2l9j349x9plg2qr")))

(define-public crate-godwoken-polyjuice-0.0.5 (c (n "godwoken-polyjuice") (v "0.0.5") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "0yccgaa8y7r99k0pqwmn33bml2kq37hk2cz46q1qgm9qwzyhzb7j")))

(define-public crate-godwoken-polyjuice-0.0.6 (c (n "godwoken-polyjuice") (v "0.0.6") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1xp65h5z0axb72zig4pgvn1i8zggwgqv42xlgv6jnvz12gzww5va")))

(define-public crate-godwoken-polyjuice-0.0.7 (c (n "godwoken-polyjuice") (v "0.0.7") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "038mhs28npxp7c8vr8npp5z017hg4k5jlsc0aj91h3n026p0v2a1")))

(define-public crate-godwoken-polyjuice-0.0.8 (c (n "godwoken-polyjuice") (v "0.0.8") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "0x51ax2ig259khg6y9g33rcn3w3akjw5rrj591aiqjsbb1fcv06i")))

(define-public crate-godwoken-polyjuice-0.1.0 (c (n "godwoken-polyjuice") (v "0.1.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "0v65dyvs8p53xnsmpw1l63dflppj105ypjxj4w5ik3kvpga3yxkh")))

(define-public crate-godwoken-polyjuice-0.2.0 (c (n "godwoken-polyjuice") (v "0.2.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "06x78scw685zsmh36mksn72rvawmgv8l6s8vfrbccqsi9g6afk5c")))

(define-public crate-godwoken-polyjuice-0.1.4 (c (n "godwoken-polyjuice") (v "0.1.4") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1a1mjyshd75my3zx9pzckzdfm6q0c5nlrhyj8ssklnhwqycy6j7q")))

(define-public crate-godwoken-polyjuice-0.4.0 (c (n "godwoken-polyjuice") (v "0.4.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "0ax4hlgfjiapm42iiw1mmbggnr7lwh9iawm3p09xc1mczg9gzjc5")))

(define-public crate-godwoken-polyjuice-0.6.0 (c (n "godwoken-polyjuice") (v "0.6.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 1)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 1)) (d (n "includedir") (r "^0.5.0") (d #t) (k 0)) (d (n "includedir_codegen") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "00ndpbqvrb7qq2pnkvfzrfafdvm0y005176h9vdn5kl13ch0rwqn")))

