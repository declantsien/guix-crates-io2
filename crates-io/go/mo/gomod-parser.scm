(define-module (crates-io go mo gomod-parser) #:use-module (crates-io))

(define-public crate-gomod-parser-0.0.1 (c (n "gomod-parser") (v "0.0.1") (d (list (d (n "winnow") (r "^0.5.34") (d #t) (k 0)))) (h "02n8hc4mkw3c8icgkyklbdf06fjzpwvypys9yd818s116rakmd87") (r "1.64")))

(define-public crate-gomod-parser-0.0.2 (c (n "gomod-parser") (v "0.0.2") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "winnow") (r "^0.5.34") (d #t) (k 0)))) (h "12008zsmx7hjakxz3yia0l84v1qynxharsb55sp6xqfzw60hmsdx") (r "1.64")))

(define-public crate-gomod-parser-0.0.3 (c (n "gomod-parser") (v "0.0.3") (d (list (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "0hhwzx4kd0j5sxxq5my77jl1990xf7998akd5bw6mca8jqd91lqv") (r "1.64")))

