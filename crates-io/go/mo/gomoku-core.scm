(define-module (crates-io go mo gomoku-core) #:use-module (crates-io))

(define-public crate-gomoku-core-0.1.0 (c (n "gomoku-core") (v "0.1.0") (h "10kv4nibab6b3c4a7lhi68wy59sfjhsfii0xivcgjz868h9p50na")))

(define-public crate-gomoku-core-0.1.1 (c (n "gomoku-core") (v "0.1.1") (h "1ramdchx9knfd0l98ixw0s226vjfhysg05fq8hk39g7fbgshra2q")))

