(define-module (crates-io go mo gomod-rs) #:use-module (crates-io))

(define-public crate-gomod-rs-0.1.0 (c (n "gomod-rs") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "0jnw70p53hhjfs3gal8d7jqbilzp37z9g3hfnba61wsirqigv0qd")))

(define-public crate-gomod-rs-0.1.1 (c (n "gomod-rs") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "1228ggxgwlx38w4vd0rcd0x6g59k3aryngrm6hafr87xbk1x025s")))

