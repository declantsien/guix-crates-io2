(define-module (crates-io go rr gorrosion) #:use-module (crates-io))

(define-public crate-gorrosion-0.1.0 (c (n "gorrosion") (v "0.1.0") (h "18ag6ix7dg11hhcq9gwmxkljgvbx484p2pa8ldgxpcpz2i6fzp2c")))

(define-public crate-gorrosion-0.1.1 (c (n "gorrosion") (v "0.1.1") (h "1p1781n0qjbx1dvy50fni97aqsd2hrl2ng5c30smp1qnflc1dqqg")))

(define-public crate-gorrosion-0.1.2 (c (n "gorrosion") (v "0.1.2") (h "0di7vz01nzhq835ch58qmsflr0qvclj6fxmb697n7n20ywpszvrm")))

