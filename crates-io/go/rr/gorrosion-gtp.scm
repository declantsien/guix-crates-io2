(define-module (crates-io go rr gorrosion-gtp) #:use-module (crates-io))

(define-public crate-gorrosion-gtp-0.1.0 (c (n "gorrosion-gtp") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "141a98yfszp82md5a2rr8zmnm6b1fsi3hhgz489dk9kd15wby2c4")))

(define-public crate-gorrosion-gtp-0.1.1 (c (n "gorrosion-gtp") (v "0.1.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "006f9l640mfd9vgzw4dba0qzv3ms4clkwgpfv75pkjwjw2dk1mbh")))

(define-public crate-gorrosion-gtp-0.1.2 (c (n "gorrosion-gtp") (v "0.1.2") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0k3gzpp2lq2mlwl52r8zll172l31x0k03q70ypbqyk6kfglz80r0")))

(define-public crate-gorrosion-gtp-0.2.0 (c (n "gorrosion-gtp") (v "0.2.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "19yqsfqvwvf2a6rv8wdmwixx0b2ngy9bj5mwr4xg16a7xy7jn1m6")))

(define-public crate-gorrosion-gtp-0.2.1 (c (n "gorrosion-gtp") (v "0.2.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "15vc24j66rmfnxhl3fj6d4iw5d8yqnd0bkbsk4bpf2a50rrhbrsx")))

(define-public crate-gorrosion-gtp-0.3.0 (c (n "gorrosion-gtp") (v "0.3.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "125n6xvhdfd2dblwigjc70xyhjsgwqycxn630ah20j34a9ha0wid")))

(define-public crate-gorrosion-gtp-0.4.0 (c (n "gorrosion-gtp") (v "0.4.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0sxq3s30460pj2ik8z8z63xznvn7x57nh7pga5rq2wfwd8y4plbq")))

(define-public crate-gorrosion-gtp-0.4.1 (c (n "gorrosion-gtp") (v "0.4.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "1crz8vh9p9vfjxhk5nvp1pgz1y0sihnhwd6mlcdr1f5shb6bw1jc")))

