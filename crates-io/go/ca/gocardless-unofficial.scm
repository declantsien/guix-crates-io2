(define-module (crates-io go ca gocardless-unofficial) #:use-module (crates-io))

(define-public crate-gocardless-unofficial-0.1.0 (c (n "gocardless-unofficial") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "02jkh94wlq0yqhixc2rflw568wjlizlpypzkcm9r28zjbwcak9bp") (r "1.63.0")))

(define-public crate-gocardless-unofficial-0.1.1 (c (n "gocardless-unofficial") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0v57c9hln80adqwf2gzm8mnqwrfnnd6gm5xkwprsfvdsf3cbm4k3") (r "1.63.0")))

(define-public crate-gocardless-unofficial-0.1.2 (c (n "gocardless-unofficial") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0g6k3v9bassclh9gbx1a9h5nrm35w7sd6gwjpvj2z7y0afr9pvf5") (r "1.63.0")))

