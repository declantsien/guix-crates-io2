(define-module (crates-io go pr gopro-controller) #:use-module (crates-io))

(define-public crate-gopro-controller-0.8.0 (c (n "gopro-controller") (v "0.8.0") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "13g85i4d6z5n6vz39dkcjlnb0r5wbw90p711mswha1bh8mnj51y7") (y #t)))

(define-public crate-gopro-controller-0.8.1 (c (n "gopro-controller") (v "0.8.1") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "1gsgrrdqkf3ab91s1qhhbdfq7l9q60gbbqy9mhrdw6ddz2lwgm1q") (y #t)))

(define-public crate-gopro-controller-0.9.0 (c (n "gopro-controller") (v "0.9.0") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "043n273zvg5vssfkldx9laa8jhrshib1ldrhm0vrck8y1m3ydzss") (y #t)))

(define-public crate-gopro-controller-0.10.0 (c (n "gopro-controller") (v "0.10.0") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "0al6s4y5jp55jz3lg8gx8an9gf4pk8fa1nh00dljdhx2a1kwa3bq") (f (quote (("wifi" "query") ("query") ("default" "query")))) (y #t)))

(define-public crate-gopro-controller-0.10.1 (c (n "gopro-controller") (v "0.10.1") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "1ckqnn5zd95kc8qw11xd4gd3rgdyjymgf5dc5vb2v2xyzv5s8i5y") (f (quote (("wifi" "query") ("query") ("default" "query")))) (y #t)))

(define-public crate-gopro-controller-0.10.2 (c (n "gopro-controller") (v "0.10.2") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "1lzvpq6w7bwvfinp4npzqmmj5kkm57i4li3ifvln3gfjylp6krgg") (f (quote (("wifi" "query") ("query") ("default" "query"))))))

(define-public crate-gopro-controller-0.10.5 (c (n "gopro-controller") (v "0.10.5") (d (list (d (n "btleplug") (r "^0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "13jbbv4z1jwbpxv1crr3skb4jhz2dvsaagb9zm2zyf4y27lxw4ir") (f (quote (("wifi" "query") ("settings") ("query") ("default" "settings")))) (y #t)))

