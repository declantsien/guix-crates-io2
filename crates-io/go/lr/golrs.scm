(define-module (crates-io go lr golrs) #:use-module (crates-io))

(define-public crate-golrs-0.1.0 (c (n "golrs") (v "0.1.0") (d (list (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1iarjdl2xxb0bq2a2cfsqpwqr23lxqxdsk1rbq3aq71cfllm1ncz")))

(define-public crate-golrs-0.2.0 (c (n "golrs") (v "0.2.0") (d (list (d (n "metrohash") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "08brinya6xm4kdd4dxhmcyfplmcl076qsiq3lhy4j0zhs09bchp7")))

