(define-module (crates-io iy es iyes_perf_ui) #:use-module (crates-io))

(define-public crate-iyes_perf_ui-0.1.0 (c (n "iyes_perf_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (f (quote ("clock"))) (o #t) (k 0)))) (h "0rs0ny8nsvvzz5raijp8683qsk0a45kg51ykwdy3gh3gq0wn6h1l") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-iyes_perf_ui-0.2.0 (c (n "iyes_perf_ui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (f (quote ("clock"))) (o #t) (k 0)))) (h "1pv70ssdbjr060nyfyhb7p2g8vv9dps3a96nf3dkyjl6xkc5ylzi") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-iyes_perf_ui-0.2.1 (c (n "iyes_perf_ui") (v "0.2.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (f (quote ("clock"))) (o #t) (k 0)))) (h "0y6jph7zsfy4lzv3z4yxqypj7z0qh2v2s47gbwiyp5psh6p6nwvl") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-iyes_perf_ui-0.2.2 (c (n "iyes_perf_ui") (v "0.2.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (f (quote ("clock"))) (o #t) (k 0)))) (h "04xf6s60msnnxm5b7vaa265vnmkbjvqg0iiqgfccmn1f4dc21a14") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-iyes_perf_ui-0.2.3 (c (n "iyes_perf_ui") (v "0.2.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (f (quote ("clock"))) (o #t) (k 0)))) (h "17hzjq5s4d1wnfy2fb60w0fav12cm70dspn4fk9gvbbdhsjmm7x4") (s 2) (e (quote (("chrono" "dep:chrono"))))))

