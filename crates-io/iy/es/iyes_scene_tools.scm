(define-module (crates-io iy es iyes_scene_tools) #:use-module (crates-io))

(define-public crate-iyes_scene_tools-0.1.0 (c (n "iyes_scene_tools") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "1l276yh9c4k26b57sqmqpi2qhihyid5dfj6mlzf948gbxj1ycd9k")))

(define-public crate-iyes_scene_tools-0.1.1 (c (n "iyes_scene_tools") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)) (d (n "ron") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1p1czlfp5fd6x4irzh5hfpp6ks73knmflj914vgrqw507qnw9mdh")))

(define-public crate-iyes_scene_tools-0.2.0 (c (n "iyes_scene_tools") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "18vm24jx1z7vbip9avb7nwgjwf25y2vsl5qz4d8bfdsi7wsyib0s")))

