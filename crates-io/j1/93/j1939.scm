(define-module (crates-io j1 #{93}# j1939) #:use-module (crates-io))

(define-public crate-j1939-0.1.13 (c (n "j1939") (v "0.1.13") (h "0ficx9qx3dmw90941503ilvvvhg50sdm7ppy7zp3xnl2whnimlww") (f (quote (("can"))))))

(define-public crate-j1939-0.1.14 (c (n "j1939") (v "0.1.14") (h "0cfx0zqpq3xj5059hzggxzqn24v6s28ssr179dsjqmdy5s3gwwc6") (f (quote (("can"))))))

(define-public crate-j1939-0.1.15 (c (n "j1939") (v "0.1.15") (h "0s872hqm0wl8is71rsk11whsl91d2j5npvwn1fzn5s37xsbdxwhv") (f (quote (("can"))))))

(define-public crate-j1939-0.1.16 (c (n "j1939") (v "0.1.16") (h "1381q65gkmn5ss1169hp6w2mvnl9q7ihwrd6mrr0b8sfkvbl55k2") (f (quote (("can"))))))

(define-public crate-j1939-0.1.17 (c (n "j1939") (v "0.1.17") (h "0rqhbnybxdrri04gswb080drjjy5avsqhwjx18lkfaacbbgvhfs8") (f (quote (("can"))))))

(define-public crate-j1939-0.1.18 (c (n "j1939") (v "0.1.18") (h "1qvlp3l3yg85m6vwr73xrhm0lhjiz65f446sr5xwlmiwi51qbydd") (f (quote (("can"))))))

(define-public crate-j1939-0.1.19 (c (n "j1939") (v "0.1.19") (h "0qsm2jqrr5z78k36wd28sjfbbjb3jrg7d7426yp8ywb7qh9kqz3f") (f (quote (("can"))))))

(define-public crate-j1939-0.1.20 (c (n "j1939") (v "0.1.20") (h "0l0mwip8d12j8znzrf37x660h58sa4d77s1jgkad5x4gmnl4fdh2") (f (quote (("can"))))))

(define-public crate-j1939-0.1.21 (c (n "j1939") (v "0.1.21") (h "1g1r26rq00gisg0cra5scvgy5h0lp2kka513zyi1s6m554yxmsp1") (f (quote (("can"))))))

(define-public crate-j1939-0.1.22 (c (n "j1939") (v "0.1.22") (h "0d7zazhps8v6sw8s9vnhm296s7lwjlv1rkqjm4silrdpg4gmipqn") (f (quote (("can"))))))

(define-public crate-j1939-0.1.23 (c (n "j1939") (v "0.1.23") (h "12vfx9m37xwk5g51klnc24mlg9bryi8cqa3yly9ham605f5hykmd") (f (quote (("can"))))))

(define-public crate-j1939-0.1.24 (c (n "j1939") (v "0.1.24") (h "0yhvym9wrcv7wyk5sk6mgzhz832xcjz84d3alsrk35y84ckb8c98") (f (quote (("can"))))))

(define-public crate-j1939-0.1.25 (c (n "j1939") (v "0.1.25") (h "0xxjbl4pr7xzp1npgl0mjmwxqx77sbhli0fz65fpgybdpz9r4ssz") (f (quote (("can"))))))

(define-public crate-j1939-0.1.26 (c (n "j1939") (v "0.1.26") (h "0c8jk9rg6r6lbhqm469jsjcx9jdkzz7r1rzxflz3c2ldf6i25ndv") (f (quote (("can"))))))

(define-public crate-j1939-0.1.27 (c (n "j1939") (v "0.1.27") (h "0ihhg33bqp3jzd6kddkq7p4ksqmnp5jz3y579lx648adcv8y0glf") (f (quote (("can"))))))

(define-public crate-j1939-0.1.28 (c (n "j1939") (v "0.1.28") (h "1q0ahscaa9ph5s4h9sgr54piyws26dvi8cmy3snxfrwj4i5cxw8q") (f (quote (("can"))))))

(define-public crate-j1939-0.1.29 (c (n "j1939") (v "0.1.29") (h "0q2phjrs932s3j9ljz6ab7a79614y78m8ddxgv7zy3j3zhf0fbh1")))

(define-public crate-j1939-0.1.30 (c (n "j1939") (v "0.1.30") (h "1964xrp2c7na37014law3n5c6r3i38phpvwrrqyc5p882dqrizin")))

(define-public crate-j1939-0.1.31 (c (n "j1939") (v "0.1.31") (h "1ab9zgbs72j807ry4ix1qkwf9k7r1cvw3cimg14g7p279jcp9bnb")))

