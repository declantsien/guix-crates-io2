(define-module (crates-io tw ma twmap-tools) #:use-module (crates-io))

(define-public crate-twmap-tools-0.1.0 (c (n "twmap-tools") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.3") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)))) (h "1f8vy8piz5nmjq1xry9rhvsjb17afg818ppwkksyg4rlbbpnm4w6")))

(define-public crate-twmap-tools-0.1.1 (c (n "twmap-tools") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.3") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)))) (h "0im6hhahvwig13pk93is56ps9af1yq91m9dahymwmgysk0cigb66")))

(define-public crate-twmap-tools-0.2.0 (c (n "twmap-tools") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)))) (h "1vqadbxgybcwk4vxnic8vwcyzqaihb0z3hvlhprmab5b2ql3wa2a")))

(define-public crate-twmap-tools-0.3.0 (c (n "twmap-tools") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "twmap") (r "^0.8.0") (d #t) (k 0)))) (h "0nlinw0b11vykllqf6h2k3659w1d0r27zf335ia96m6j2847wn4b")))

(define-public crate-twmap-tools-0.3.1 (c (n "twmap-tools") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "twmap") (r "^0.9.0") (d #t) (k 0)))) (h "0afcy6b4nzlld3a2wxqac9j7cb39vn5d8n1cq270ajhzcb7lvy02")))

(define-public crate-twmap-tools-0.3.2 (c (n "twmap-tools") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.27.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "twmap") (r "^0.12.0") (d #t) (k 0)) (d (n "vek") (r "^0.16.1") (f (quote ("az" "rgba" "std" "uv"))) (k 0)))) (h "0z90z00d0vlhz1jx9icgq497zbybvxjlznp7jhl4kdqh53cysvvm")))

