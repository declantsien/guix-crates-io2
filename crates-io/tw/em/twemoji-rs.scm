(define-module (crates-io tw em twemoji-rs) #:use-module (crates-io))

(define-public crate-twemoji-rs-0.1.0 (c (n "twemoji-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0fkqssvmwfw392114inh8vxgp1mvnjpmknf5914gyzhnx2jirby3")))

(define-public crate-twemoji-rs-0.1.1 (c (n "twemoji-rs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0isrb2j5wmg7pj7f2g2ysbq2ay0wy14sin4fg699p45gr8g4f0lf")))

(define-public crate-twemoji-rs-0.1.2 (c (n "twemoji-rs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1pd4fsw1mqcjmpq2szwajz5ya1k4pndd2r81gb76zncvfl73navk")))

