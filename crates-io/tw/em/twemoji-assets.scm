(define-module (crates-io tw em twemoji-assets) #:use-module (crates-io))

(define-public crate-twemoji-assets-0.1.0+14.0.2 (c (n "twemoji-assets") (v "0.1.0+14.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0xvyvn8228bk7d9sg9jyzyzyiwsi00ijm9hjq6rm3jfr6gyvb0hf") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.0.0+14.0.2 (c (n "twemoji-assets") (v "1.0.0+14.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1h3b8s9d6viz5vzs069rkvkxky10y3x4zqmlcgfw9wnh1hrbdwpc") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.1.0+14.1.2 (c (n "twemoji-assets") (v "1.1.0+14.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "09j15f03xw2jv3y5barf56gsv4rvvfhjw513iyr498pmfcmlh3aw") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.1.1+14.1.2 (c (n "twemoji-assets") (v "1.1.1+14.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1i8ndg7ss1k7rrd0gh9qhpn45k2b1g2z69w4qgj7hzx8j7biqvvd") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.2.0+15.0.2 (c (n "twemoji-assets") (v "1.2.0+15.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1n1knapsfin9m8sqbzhpbxpqlnbjk3d3hgc5zv0csi6a84mvfpjp") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.2.1+15.0.3 (c (n "twemoji-assets") (v "1.2.1+15.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1x9c5cwx42rf1il6734ji32ah8bc3c27dal5slbks3m6kabby4wg") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1.3.0+15.1.0 (c (n "twemoji-assets") (v "1.3.0+15.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "070nb3zlfcxa59kb3n2298ranl9yybflg47araz6sxlr6b12bl40") (f (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

