(define-module (crates-io tw ix twixter) #:use-module (crates-io))

(define-public crate-twixter-0.1.0 (c (n "twixter") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.8") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "dirs") (r "~2.0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.9.20") (d #t) (k 0)) (d (n "rust-ini") (r "~0.13.0") (d #t) (k 0)) (d (n "strfmt") (r "~0.1.6") (d #t) (k 0)))) (h "0kr95ncy5sk7820rcxsx5shfl0svs2m4wccshaa26g3ib7f6q3mr")))

(define-public crate-twixter-0.2.0 (c (n "twixter") (v "0.2.0") (d (list (d (n "chrono") (r "~0.4.8") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "dirs") (r "~2.0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.9.20") (d #t) (k 0)) (d (n "rust-ini") (r "~0.13.0") (d #t) (k 0)) (d (n "strfmt") (r "~0.1.6") (d #t) (k 0)))) (h "0lp8yakq1hw2vinkxzaijd70k7hhvpzsz17c1r4bqv9ybd481k2j")))

