(define-module (crates-io tw is twist-lz4) #:use-module (crates-io))

(define-public crate-twist-lz4-0.1.0 (c (n "twist-lz4") (v "0.1.0") (d (list (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.2.0") (d #t) (k 0)))) (h "0adw7v22m2daa08mgamcpfy1xj8wf4ccb9vg1dx5w4n29ir6kvhk")))

(define-public crate-twist-lz4-0.1.1 (c (n "twist-lz4") (v "0.1.1") (d (list (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.3.0") (d #t) (k 0)))) (h "0nqkr9vdr2fnb6z9dfg1dqirsixqm4xb2v2bk2zh0fd9pkwzwzgm")))

(define-public crate-twist-lz4-0.1.2 (c (n "twist-lz4") (v "0.1.2") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.4.0") (d #t) (k 0)))) (h "0mcfs367cib97l8mixgawm0i897bc358x0n4nbp9pb2pq9ism3wv")))

(define-public crate-twist-lz4-0.1.3 (c (n "twist-lz4") (v "0.1.3") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.5.0") (d #t) (k 0)))) (h "0a774jg7rffgf51kqky0yf1ds7jar2g3vrlvxwhf9jpmwis5lkf7")))

(define-public crate-twist-lz4-0.1.4 (c (n "twist-lz4") (v "0.1.4") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.5.1") (d #t) (k 0)))) (h "0n4wz1kr51n8xkd640bffvphwls9206in8k1bg37yij3af9sbci6")))

(define-public crate-twist-lz4-0.1.5 (c (n "twist-lz4") (v "0.1.5") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.5.1") (d #t) (k 0)))) (h "094l41hd6fi8ik41brax1vvbjfmmn49kwl2i0csd0xbfxyc485fx")))

(define-public crate-twist-lz4-0.1.6 (c (n "twist-lz4") (v "0.1.6") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.5.1") (d #t) (k 0)))) (h "0hly5pzv0q4vc8nsnfxr8brp68gjaklv92y00jig76by1vfqknpp")))

(define-public crate-twist-lz4-0.2.0 (c (n "twist-lz4") (v "0.2.0") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.6.0") (d #t) (k 0)))) (h "13yg86d75pzbshjg6ql08mfqcig1v6sajk4ixc290wzvdprg4cwg")))

(define-public crate-twist-lz4-0.3.0 (c (n "twist-lz4") (v "0.3.0") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.7.0") (d #t) (k 0)))) (h "12lsj3q1zp371jmhc27r7dybhqwmxw2kgl6a9dn13vp37nvy3l18")))

(define-public crate-twist-lz4-0.3.1 (c (n "twist-lz4") (v "0.3.1") (d (list (d (n "lz4-compress") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "twist") (r "^0.7.1") (d #t) (k 0)))) (h "0rbj5zafws2mzn6x64hqbgahhq92ccicm7rfq89k7gq0r8kz5f69")))

