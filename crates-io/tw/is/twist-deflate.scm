(define-module (crates-io tw is twist-deflate) #:use-module (crates-io))

(define-public crate-twist-deflate-0.1.0 (c (n "twist-deflate") (v "0.1.0") (d (list (d (n "libz-sys") (r "^1.0.13") (d #t) (k 0)) (d (n "slog") (r "^1.5.2") (d #t) (k 0)) (d (n "slog-term") (r "^1.5.0") (d #t) (k 0)) (d (n "twist") (r "^0.1.4") (d #t) (k 0)))) (h "10n1a43ypsn17rs3y1cvwmqyj01fn6yj87flyhd6syxnxil4914l")))

