(define-module (crates-io tw im twiml) #:use-module (crates-io))

(define-public crate-twiml-0.2.0 (c (n "twiml") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "00lslwmdj6vqdksfy2cjin2p420k8c39k6il5fbdbx5bkmxhyza7")))

(define-public crate-twiml-0.2.1 (c (n "twiml") (v "0.2.1") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1grwnsfwj5pnbkkx3d6n7kjjj5dwrr9dblyyd9jbab89nyka32jg")))

(define-public crate-twiml-0.3.0 (c (n "twiml") (v "0.3.0") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1dgx71wp3an6rivdajzzmp140ily53xha23pzx98fn39vha2r6hi")))

