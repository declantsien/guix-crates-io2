(define-module (crates-io tw en twenty-minutes) #:use-module (crates-io))

(define-public crate-twenty-minutes-0.0.1 (c (n "twenty-minutes") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "structopt") (r "^0.2.18") (k 0)))) (h "1zj2b4pgn5p60y94zqyp9vy5k33zx8hr397c213gkgrq2pid3xyk")))

(define-public crate-twenty-minutes-0.0.2 (c (n "twenty-minutes") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "structopt") (r "^0.2.18") (k 0)))) (h "1mx70fnr22xr1gzvhg1hxf90fxyziz5ax809gzh4f73rw2brfg4f")))

(define-public crate-twenty-minutes-0.0.3 (c (n "twenty-minutes") (v "0.0.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "14z5g2fcyk7dv14hzg2rw411jdj6bsv8qf5jmm9zg34ggnlwywa0")))

