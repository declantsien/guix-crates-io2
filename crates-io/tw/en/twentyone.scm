(define-module (crates-io tw en twentyone) #:use-module (crates-io))

(define-public crate-twentyone-0.1.0 (c (n "twentyone") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "160zjkbjpf2rm9h3v3pa7y57kwg0viksnk49myl0y37zhiddgl1q")))

(define-public crate-twentyone-0.2.0 (c (n "twentyone") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17qbzp0pb9a1lqsv9kcygqph6dychs0rgnyflsa4li3p6sd83485")))

