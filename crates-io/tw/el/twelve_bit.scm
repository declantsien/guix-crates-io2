(define-module (crates-io tw el twelve_bit) #:use-module (crates-io))

(define-public crate-twelve_bit-0.1.0 (c (n "twelve_bit") (v "0.1.0") (h "0mp6k99czlcb3jgsr4qg86pr9w42ppkibx8hl022jdzy31z47jjq")))

(define-public crate-twelve_bit-0.1.1 (c (n "twelve_bit") (v "0.1.1") (h "15vdangb3rzn4zc0q1dpfi2r6xdbdsysbjgd2lkrhfl2bfb1zs6p")))

