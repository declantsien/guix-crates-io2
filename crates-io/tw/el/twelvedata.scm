(define-module (crates-io tw el twelvedata) #:use-module (crates-io))

(define-public crate-twelvedata-0.1.0 (c (n "twelvedata") (v "0.1.0") (h "0w6s54n0ncl2q27cp4zh7big3jqb446420084j9v6linff75hvl9")))

(define-public crate-twelvedata-0.1.1 (c (n "twelvedata") (v "0.1.1") (h "0g2mvwkk40rm6bmj7i182islgapzmylkdikyf6hmnlpi7ijxzvvz")))

(define-public crate-twelvedata-0.2.0 (c (n "twelvedata") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dyqydkbaibsikj3nb62rbwq5d5hmb238whmy1kryx5csvs8rmgj")))

(define-public crate-twelvedata-0.2.1 (c (n "twelvedata") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6l95zmy9m01l4ri0l499xc1nfxgk48rh1scvygma8na095d2hp")))

(define-public crate-twelvedata-0.2.2 (c (n "twelvedata") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bsj1i6xxjkxkbpd4l1mwl6brlnmcdy5z3x5plysr4a8gx74yzm5")))

(define-public crate-twelvedata-0.2.3 (c (n "twelvedata") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "07nx05211lbdd888k903dlfv1igr8nkc6krgckbf9z6nw0yfa430")))

(define-public crate-twelvedata-0.2.4 (c (n "twelvedata") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19q7361j2bym6ybk89blxc3k0m9qhzd3nmnj3mbdmk9n9g93pfj0")))

(define-public crate-twelvedata-0.3.0 (c (n "twelvedata") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15q793bvvzrjhcx9gmp6vkyvlyjwjd11dpizzvxvvi0sx5c1kc0d")))

(define-public crate-twelvedata-0.3.1 (c (n "twelvedata") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z3vgr7s5a1hkdkv4dcg49h0sgvzrgb0jla4hybjl4l44g2hvdjp")))

(define-public crate-twelvedata-0.4.0 (c (n "twelvedata") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0md4d05ch2a5n2wlxbrxkqd5a42hqbf4sg6hlbzd8njig2nirsg3")))

(define-public crate-twelvedata-0.5.0 (c (n "twelvedata") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a0m2syp16vhlbagqmygxfwlzphlj4kj6cky27b9qg8982xvyw36")))

(define-public crate-twelvedata-0.6.0 (c (n "twelvedata") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dm5ywadjc6c8l5yrl98372ayf1ikdqrkg5frddvlvm9pldqqa7s")))

(define-public crate-twelvedata-0.6.1 (c (n "twelvedata") (v "0.6.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fr0dqqav854h4ll6icx4cfgcvrbw6bihi99dn1az6i72krrddhl")))

(define-public crate-twelvedata-0.6.2 (c (n "twelvedata") (v "0.6.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.28.2") (k 0)))) (h "0swn1gr3nh5gg3rvcrl8ian2vsliqsd470wlnk5pxkdly6his242") (y #t)))

(define-public crate-twelvedata-0.6.3 (c (n "twelvedata") (v "0.6.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "1lpw3lm7fsna1qkpn39brcd7yan6f71k0f2jifyxa1y0i9kkbz17")))

(define-public crate-twelvedata-0.6.4 (c (n "twelvedata") (v "0.6.4") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1p65rw33k77j6a30805gb2pf8jz07msfw4x8g8f6nw0wnvz6f6dm")))

(define-public crate-twelvedata-0.7.0 (c (n "twelvedata") (v "0.7.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cn6r9dl5lsy1rj4h4riqvcvsgpr6ligyfwsbhfaynn7a9ik29b9")))

(define-public crate-twelvedata-0.8.0 (c (n "twelvedata") (v "0.8.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (k 0)) (d (n "serde_json") (r "^1.0.114") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05zj909knlmcdccxha0ni9zgjcv3m9kjl6l11flyl7v1pb6dy795") (f (quote (("pro" "grow") ("grow" "basic") ("enterprise" "pro") ("default" "basic") ("basic"))))))

(define-public crate-twelvedata-0.8.1 (c (n "twelvedata") (v "0.8.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (k 0)) (d (n "serde_json") (r "^1.0.114") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j4dqqry6hqpahfwhizpd15l7pv4w3d4w5crzwyjzrg2bcikialp") (f (quote (("pro" "grow") ("grow" "basic") ("enterprise" "pro") ("default" "basic") ("basic"))))))

