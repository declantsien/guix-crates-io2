(define-module (crates-io tw eb tweb) #:use-module (crates-io))

(define-public crate-tweb-0.2.0 (c (n "tweb") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (d #t) (k 0)) (d (n "chunked_transfer") (r "^1.4.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1rg56byw79376xarcq50sf7gx9kd8idc0p2qdv3chpmbqkgwmp4p")))

