(define-module (crates-io tw ib twibint) #:use-module (crates-io))

(define-public crate-twibint-0.1.0 (c (n "twibint") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "typed_test_gen") (r "^0.1") (d #t) (k 2)))) (h "1296rbdlaqvvlnxkigxkiklxfy6m44rbxq9krw57igcg6a89qd2j")))

(define-public crate-twibint-0.2.0 (c (n "twibint") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "typed_test_gen") (r "^0.1") (d #t) (k 2)))) (h "1c65pyyz0znxynf7y7chx67ly1gyanbvsr9d6fz2dqhkggnzk2da")))

(define-public crate-twibint-0.2.1 (c (n "twibint") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "typed_test_gen") (r "^0.1") (d #t) (k 2)))) (h "1w2ywmxny8k2m9s0bsxjgss15krv4xglng1akgpbxazddpimy7n7")))

(define-public crate-twibint-0.2.2 (c (n "twibint") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "typed_test_gen") (r "^0.1") (d #t) (k 2)))) (h "1wf0cfjbjxl7rb1vb7i7kzxzrvz38kb6mhsifbdsf1x5bvvyapyv")))

