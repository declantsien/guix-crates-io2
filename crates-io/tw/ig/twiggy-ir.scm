(define-module (crates-io tw ig twiggy-ir) #:use-module (crates-io))

(define-public crate-twiggy-ir-0.1.0 (c (n "twiggy-ir") (v "0.1.0") (d (list (d (n "cpp_demangle") (r "^0.2.7") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.11") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "100ahg28mg0m6and3s6a07ix6rgrvb8pnh3lrd2ssxp1qf0yf3ah")))

(define-public crate-twiggy-ir-0.2.0 (c (n "twiggy-ir") (v "0.2.0") (d (list (d (n "cpp_demangle") (r "^0.2.7") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.12") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)))) (h "1d33hgqvx1n31yxpl77r94ylgpaqxz664rci65nww4chqxq8kqj8")))

(define-public crate-twiggy-ir-0.3.0 (c (n "twiggy-ir") (v "0.3.0") (d (list (d (n "cpp_demangle") (r "^0.2.12") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "1ix8mcbhy6mvjkk2n8j809gh0w0lzi0rppybbz9yhlpynjm3n4y0")))

(define-public crate-twiggy-ir-0.4.0 (c (n "twiggy-ir") (v "0.4.0") (d (list (d (n "cpp_demangle") (r "^0.2.12") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)))) (h "0fsmn258asxylv65rp1i445hjhy20myrfss706ix2ynchaz6yl1m")))

(define-public crate-twiggy-ir-0.5.0 (c (n "twiggy-ir") (v "0.5.0") (d (list (d (n "cpp_demangle") (r "^0.2.12") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)))) (h "0nikl6z5g1wcjsmgl0lihi0rhcwa5ym323npg9r5zbkq8xsyff93")))

(define-public crate-twiggy-ir-0.6.0 (c (n "twiggy-ir") (v "0.6.0") (d (list (d (n "cpp_demangle") (r "^0.2.12") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.93") (d #t) (k 0)))) (h "1n2wcyn3cjg6dv9859vjj7casly4kdrai0qqxsj020gd0zabizq8")))

(define-public crate-twiggy-ir-0.7.0 (c (n "twiggy-ir") (v "0.7.0") (d (list (d (n "cpp_demangle") (r "^0.3.3") (k 0)) (d (n "frozen") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xsx0m9wgqmgn44y8683m2pii3yjdprxb07h8dqxk8amgya8k9cf")))

