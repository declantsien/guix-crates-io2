(define-module (crates-io tw ig twig) #:use-module (crates-io))

(define-public crate-twig-0.0.1 (c (n "twig") (v "0.0.1") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "13kvvj951vx4n2ihi0lcy2rkf34nxmy94ww39f97hmfrgvf2zdrw")))

(define-public crate-twig-0.0.2 (c (n "twig") (v "0.0.2") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "17ygnzqv50rbyrs93nnf88ghn5cbhqbjizr8bkz558039mss9dvq")))

(define-public crate-twig-0.0.3 (c (n "twig") (v "0.0.3") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "1635n897g0s4msq4yl84gjl2j9rbk01gxfmfpgdgx42albjhbjb4")))

(define-public crate-twig-0.0.4 (c (n "twig") (v "0.0.4") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 2)) (d (n "little") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 2)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 2)) (d (n "term") (r "^0.2.12") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1vyw8pp82x76i3gwqiqvn3248vdlvsx102s9hy7zrj3pvy7il6zc")))

(define-public crate-twig-0.1.0 (c (n "twig") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "little") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 2)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)) (d (n "sha1") (r "^0.1.1") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "068hb2w9havzk195crv2y1zm0vkxl4p6s6xv8yzyk3i8igq8nigm")))

