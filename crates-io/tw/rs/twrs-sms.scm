(define-module (crates-io tw rs twrs-sms) #:use-module (crates-io))

(define-public crate-twrs-sms-0.1.0 (c (n "twrs-sms") (v "0.1.0") (d (list (d (n "mockito") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("rustls-tls" "socks" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.6") (d #t) (k 0)))) (h "11g35wkmjhm9jwrl7qc2jk2vb6wxpf2gxcrq0zqcwk6pwdbkbb4m")))

