(define-module (crates-io tw #{2s}# tw2s) #:use-module (crates-io))

(define-public crate-tw2s-1.0.0 (c (n "tw2s") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.5") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.1") (d #t) (k 0)))) (h "0pcgw26cpqqkk8cfxfq5ccafw3xzingps6gizvm0s707sd4ms7qy")))

(define-public crate-tw2s-1.0.1 (c (n "tw2s") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.5") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.1") (d #t) (k 0)))) (h "0z8si0cxh8qd7mk3lzhvax9qpmx5528nh3inphnflhgqc5v675gc")))

(define-public crate-tw2s-1.0.2 (c (n "tw2s") (v "1.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.7") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.3") (d #t) (k 0)))) (h "156mikgxwbw0538mnlsx9lpkjgfkk6jhilldypbdnk8zby8856fj")))

(define-public crate-tw2s-1.0.3 (c (n "tw2s") (v "1.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.7") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.3") (d #t) (k 0)))) (h "1njzppzr0bngbmbn0iz8r8a364gswmhva9n36sca7lkb57b7s3ns")))

(define-public crate-tw2s-1.0.4 (c (n "tw2s") (v "1.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1vsc84d4wgkn5d15pr08q1v6cw2qa4vqq28pdrqgzjlxxq9cnng4") (y #t)))

(define-public crate-tw2s-1.0.5 (c (n "tw2s") (v "1.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1b1yzabdilinv9b5fz3kk3hr2lb5wb6xj019830spywmay3cplkx")))

(define-public crate-tw2s-1.0.6 (c (n "tw2s") (v "1.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "11yd1nqsriszr0mcdp8z7d2nzwnpyrfd8vdqs0n828lpinjprgda")))

(define-public crate-tw2s-1.0.7 (c (n "tw2s") (v "1.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1cy851vaza3fl7fksqrw47c2bf66l918mzy1i3v9hh0pxw570yda")))

(define-public crate-tw2s-1.0.8 (c (n "tw2s") (v "1.0.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0a6fryla509dqwsh0y3kj34fjx4i014xyxmmh30rfvp5xf9ll5wh")))

(define-public crate-tw2s-1.0.9 (c (n "tw2s") (v "1.0.9") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0x5ps0j3qnvmqld4bcmykfh7s5jh2ivihsnakk027ya485jzc05r")))

(define-public crate-tw2s-1.0.10 (c (n "tw2s") (v "1.0.10") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "04s9fjpf8shz137nb333za2b4hirjnr9d947w2qi4n18sjwxzyph")))

(define-public crate-tw2s-1.0.11 (c (n "tw2s") (v "1.0.11") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "05z71fmyfx7ylm5mbvg261npxr2cqbs1lqhr12690ij627cdqxw0")))

(define-public crate-tw2s-1.0.12 (c (n "tw2s") (v "1.0.12") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0xwijlcxr7cbmvxjhy4qh5nxhs2g5j2pc8k45332fb5nr06mp56f")))

(define-public crate-tw2s-1.0.13 (c (n "tw2s") (v "1.0.13") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "08r4cqcipnsygphgxvd2r7sqcmvmdawxfpkw6738x71mpdl5wmdq")))

(define-public crate-tw2s-1.0.14 (c (n "tw2s") (v "1.0.14") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1g42r1j9wf6df1dx7l4ljicr8hp2m03vkwyifdg132g85z9v0543")))

(define-public crate-tw2s-1.0.15 (c (n "tw2s") (v "1.0.15") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1gc2z1jg3178akg1gvl3dripv3wa8wlk6z7pq5hrdcvi8vk5qgcd")))

(define-public crate-tw2s-1.0.16 (c (n "tw2s") (v "1.0.16") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0hkysy7d51kbcmwpgn31xz9592fkviv5icl3yhxm49hp9l5m86vz")))

(define-public crate-tw2s-1.0.17 (c (n "tw2s") (v "1.0.17") (d (list (d (n "clap") (r "^3.2.3") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "16jsg9dlmjmj4f0zs329immqagmfmka5d0vl1vn8zxmff6wq6f28")))

(define-public crate-tw2s-1.0.18 (c (n "tw2s") (v "1.0.18") (d (list (d (n "clap") (r "^3.2.3") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "04kaxpbn32855pd1g7b6izjxqyiwlgmf4cmzh75ij572yd8v23m3") (r "1.60")))

(define-public crate-tw2s-1.0.19 (c (n "tw2s") (v "1.0.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "1gz9k2m5qgdfpziqy2bfp92bsggkr04wh6g01f7h65sl8d0d3spm") (r "1.70")))

