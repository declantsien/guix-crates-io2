(define-module (crates-io tw il twilight-mention) #:use-module (crates-io))

(define-public crate-twilight-mention-0.0.0 (c (n "twilight-mention") (v "0.0.0") (h "138rlcs4kj9ggqwqhxibi338xzygrdwm803309pjp99va3pjw7jm")))

(define-public crate-twilight-mention-0.1.0 (c (n "twilight-mention") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.1") (k 0)))) (h "0j8bm1kdgyc6mhyg2dqysjgcqh0njv3c04hv12jj6bj1f5c8rhaq")))

(define-public crate-twilight-mention-0.1.1 (c (n "twilight-mention") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.1") (k 0)))) (h "0c8m0v3h62w4bc68vkcvi0j5pvf42nrh1mjq2lr0sgk8hy35jl9c")))

(define-public crate-twilight-mention-0.2.0-beta.0 (c (n "twilight-mention") (v "0.2.0-beta.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.2.0-beta") (k 0)))) (h "0v2vzhsdjnrcwfq4x0dphyiqw4j0wksqd54i36plyg1lip20das7")))

(define-public crate-twilight-mention-0.2.0 (c (n "twilight-mention") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.2") (k 0)))) (h "1ppp0imcsasyzqrl3w37fp3y54icdl8lk62n4ilprq830d7f916v")))

(define-public crate-twilight-mention-0.3.0 (c (n "twilight-mention") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.3") (k 0)))) (h "04n0ya6hj000x4v24ky9y4yxywl79rwq7nmw6hrcqgrr312ga42b")))

(define-public crate-twilight-mention-0.4.0 (c (n "twilight-mention") (v "0.4.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.4") (k 0)))) (h "0f4rdvv1lyx6dm0wx7icxnb4w6pr7br778fz6k7x4as74zrwlyrg")))

(define-public crate-twilight-mention-0.4.1 (c (n "twilight-mention") (v "0.4.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.4") (k 0)))) (h "03r97lkgxccs34xxaw55s5d4a923q55dc5aj3dnrvdkfqvlvk47v")))

(define-public crate-twilight-mention-0.4.2 (c (n "twilight-mention") (v "0.4.2") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.4") (k 0)))) (h "17r71wm41sk2j0bxy5nqgs6zywyqjxs9rv0c3mip63hlbxzbw2cx")))

(define-public crate-twilight-mention-0.5.0 (c (n "twilight-mention") (v "0.5.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.5.0") (k 0)))) (h "08hrxy9ihfwszqczxpqw8l5fbmlwwcag8d4n3y7y6nvc0ki4d6qk")))

(define-public crate-twilight-mention-0.5.1 (c (n "twilight-mention") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.5") (k 0)))) (h "1wxm5i41pqb16ymih1m8rfjvjk2hwq9s401z2kzql9npw00fv90q")))

(define-public crate-twilight-mention-0.6.0 (c (n "twilight-mention") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.6") (k 0)))) (h "1a7cqhmn9nkfqj1fbr3ix73dngcgsfy0ncgbhqin0sf9w0i6qrbn")))

(define-public crate-twilight-mention-0.7.0 (c (n "twilight-mention") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.7") (k 0)))) (h "1ddm330a017scl57w2jw47km2qypngb8g7vplrh2572slx4fhfs7")))

(define-public crate-twilight-mention-0.8.0 (c (n "twilight-mention") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.8") (k 0)))) (h "0mflncnkz3wnxhzj4a5slqd93dngglfn6ki807c6syy60ldf2l92")))

(define-public crate-twilight-mention-0.9.0 (c (n "twilight-mention") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.9") (k 0)))) (h "0ypypgcbxix826mfibkv5ialgi5fx6i8r7nqdbss076n5kpr7jlr") (r "1.57")))

(define-public crate-twilight-mention-0.10.0 (c (n "twilight-mention") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.10") (k 0)))) (h "11fnkznrhjybns065z3bv3w2rbrikfq7kynpjhc61kz5hd5x8jsg") (r "1.57")))

(define-public crate-twilight-mention-0.11.0 (c (n "twilight-mention") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.11.0") (k 0)))) (h "0vcc27ah7ngblsdr3vrydyr999qwvn0lv4r1bvq5db86glw6d2jk") (r "1.60")))

(define-public crate-twilight-mention-0.11.1 (c (n "twilight-mention") (v "0.11.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.11.1") (k 0)))) (h "0636kmzi8x0pfz7zcijd2k15sfilk2cs4wrg8vwpans7rb2rv3nk") (r "1.60")))

(define-public crate-twilight-mention-0.12.0 (c (n "twilight-mention") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.12.0") (k 0)))) (h "0i2b5wba2c59cmxzymd7l1skyxynz2l0qx2pccsnrj83qpgblvrk") (r "1.60")))

(define-public crate-twilight-mention-0.12.1 (c (n "twilight-mention") (v "0.12.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.12.2") (k 0)))) (h "10qgrjnz6dzr3k9f8nrqs4d7icv5yzp7kd2q9s80x9qcfhqvxxwz") (r "1.60")))

(define-public crate-twilight-mention-0.13.0 (c (n "twilight-mention") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.13.0") (k 0)))) (h "11nq8g2g2rha9h32w3gblc7rzxviiz94q87jgg4r1624ma51239b") (r "1.60")))

(define-public crate-twilight-mention-0.14.0 (c (n "twilight-mention") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.14.0") (k 0)))) (h "1jfxjq4scag9vn3l150r9qjj29m99xbhpk730019993zzi2a84by") (r "1.64")))

(define-public crate-twilight-mention-0.15.0-rc.1 (c (n "twilight-mention") (v "0.15.0-rc.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.15.0-rc.1") (k 0)))) (h "1lnwizfsjkpajhpz1xxxdq4zl86n7bap0qm9i4cf4gams5gbx9pf") (r "1.64")))

(define-public crate-twilight-mention-0.15.0 (c (n "twilight-mention") (v "0.15.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.15.0") (k 0)))) (h "0pg2jsah046gyh20sn127ykl67zhj9xvq5q8fxa8q7blnh09mqpf") (r "1.64")))

(define-public crate-twilight-mention-0.15.1 (c (n "twilight-mention") (v "0.15.1") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.15.1") (k 0)))) (h "0l60nfgln3cv4756nik188mwbiz4bmnnrqdhg80j2v7mcf8g3jva") (r "1.64")))

(define-public crate-twilight-mention-0.15.2 (c (n "twilight-mention") (v "0.15.2") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.15.3") (k 0)))) (h "1zqmbcbkb7m7nml4mz5b228w5nx485fs4mrjxiis1phz4485f65h") (r "1.67")))

(define-public crate-twilight-mention-0.15.3 (c (n "twilight-mention") (v "0.15.3") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.15.4") (k 0)))) (h "1l989xcdf43hcpia5qs8nwk977f26ra4yvifdiqx83m7cgvkccch") (r "1.67")))

(define-public crate-twilight-mention-0.16.0-rc.1 (c (n "twilight-mention") (v "0.16.0-rc.1") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.16.0-rc.1") (k 0)))) (h "0nsc3dbbc5ac2gcrk3rkdzr036nadds1x5nal2jd4ldgzcamvhrx") (r "1.67")))

