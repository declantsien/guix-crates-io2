(define-module (crates-io tw il twilio-data) #:use-module (crates-io))

(define-public crate-twilio-data-0.1.0 (c (n "twilio-data") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "06bpknmkzzypqzwbr72dlfj32x90ard0licbzm8ymnw6bhml8qqj") (y #t)))

(define-public crate-twilio-data-0.1.1 (c (n "twilio-data") (v "0.1.1") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "0a5fq64mlsznrpbr5jflwp9hc18a8yw38cafsq1vdpj5n8n6ld3v") (y #t)))

(define-public crate-twilio-data-0.1.2 (c (n "twilio-data") (v "0.1.2") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "0axbbfjjv6a150axnr1a4x9v6kdwh06hvmg5rzplmi5cg4agxjha") (y #t)))

(define-public crate-twilio-data-0.1.3 (c (n "twilio-data") (v "0.1.3") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "1ax2iak30k1vk6akiq868i6svxkkwympwwp62am7iqdsgqzi2yl5") (y #t)))

(define-public crate-twilio-data-0.1.4 (c (n "twilio-data") (v "0.1.4") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "1kdjc7qif2ci3xi4ab8qz59f142xxkgmr9npm5n22k6zan9f58dh") (y #t)))

(define-public crate-twilio-data-0.1.5 (c (n "twilio-data") (v "0.1.5") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "0sky5sq2lficyfnny08cl68lgayhxpnhryr76h17rs3s0x846mpk")))

(define-public crate-twilio-data-0.2.0 (c (n "twilio-data") (v "0.2.0") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "str-buf") (r "^3.0.1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 2)))) (h "01x3pqfam79c1rsnsk3dnv0vyz97n07nj5327f2iqpy27jfdrz5d")))

