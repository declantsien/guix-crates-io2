(define-module (crates-io tw il twilight-command-parser) #:use-module (crates-io))

(define-public crate-twilight-command-parser-0.0.0 (c (n "twilight-command-parser") (v "0.0.0") (h "0fgldbg5j5dji9kwzk9xib68qk3lgrv1iddl7d6f2a8b2svb1q2z")))

(define-public crate-twilight-command-parser-0.1.0 (c (n "twilight-command-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "1fjrr4w1m030dglipn1d3bpknf57lrhf6l03rpiqndr1s79f9zh2")))

(define-public crate-twilight-command-parser-0.1.1 (c (n "twilight-command-parser") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "0fbbcqwmjwnh42033axqfsq3bb7r7rx5ljhl5nc4gdskk0kzpz2k")))

(define-public crate-twilight-command-parser-0.1.2 (c (n "twilight-command-parser") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "0a0k1k1dv78924fzalwi3vgcwkq59haz1ygfclvm1gyjj2gs9yvw")))

(define-public crate-twilight-command-parser-0.2.0-beta.0 (c (n "twilight-command-parser") (v "0.2.0-beta.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "1r8zrv8y5rha3wzl06hjinf83qsri58hwg9phj5bjclb9d7nfd22")))

(define-public crate-twilight-command-parser-0.2.0 (c (n "twilight-command-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1b07gczgqc99gvvl8wrwbcnhbnrc8iyjr9772890b5wg6bz6qf94")))

(define-public crate-twilight-command-parser-0.2.1 (c (n "twilight-command-parser") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "18g3rgnn8p3nmvrp8g5f2acg7dqilnz9ad6rwy7sdalpfs9g4kg8")))

(define-public crate-twilight-command-parser-0.2.2 (c (n "twilight-command-parser") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "1d83gm54977aij2lms8r53pckbm1ldd5dsj5pn6wlyzvpsk4iqz8")))

(define-public crate-twilight-command-parser-0.1.3 (c (n "twilight-command-parser") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "1fpgff1jg4hp53c36jdfc08r091pkdarppcc8vjgd47a9my29dil")))

(define-public crate-twilight-command-parser-0.2.3 (c (n "twilight-command-parser") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "076dl9msng82wghiglspbwm4nhpizp534q1wqj5qnbxw1k9sdv9b")))

(define-public crate-twilight-command-parser-0.3.0 (c (n "twilight-command-parser") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "0bi1x2l396ywy3s5ph68fdfysm2w8jc36871wihqyad214647cpp")))

(define-public crate-twilight-command-parser-0.4.0 (c (n "twilight-command-parser") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "0fhdxckc41vas9bdw2m518qk2h7h96bibggqg9f78b6dfbpdgwcd")))

(define-public crate-twilight-command-parser-0.4.1 (c (n "twilight-command-parser") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "18b2qwnlk2scfv9xlk0kn49v1xkfffk3rz1dx6ak7f1gyfh6jppn")))

(define-public crate-twilight-command-parser-0.4.2 (c (n "twilight-command-parser") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "07fwjjvl5rk9d2p30g1xy3v7s1mcgb4rz823gs7zxsz1r5pmcxai")))

(define-public crate-twilight-command-parser-0.5.0 (c (n "twilight-command-parser") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "00rlq74dm1pgpn5nm765pjyp7mb0lk13sfvjjz28cak6lakw3skq")))

(define-public crate-twilight-command-parser-0.6.0 (c (n "twilight-command-parser") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "patricia_tree") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "03yl27wx69xw63k8ajdbvvgkhgva86q4va04mhnjn471bar3fbsx")))

(define-public crate-twilight-command-parser-0.7.0 (c (n "twilight-command-parser") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "patricia_tree") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "140p9kq3y6rfic5b2nw4ddwl0jljmcxlamca38qydzjcdqfcn7r5")))

(define-public crate-twilight-command-parser-0.8.0 (c (n "twilight-command-parser") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "patricia_tree") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "0p1hxxxr786yk13bkcpkv6ifjmcp38sx6jixa7hbr1diki4311ha")))

(define-public crate-twilight-command-parser-0.8.1 (c (n "twilight-command-parser") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "patricia_tree") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "175ak0w9n16nwg5wgkdj4hczvxcp03r38ldnszq00zfy84ai7j2p")))

(define-public crate-twilight-command-parser-0.9.0 (c (n "twilight-command-parser") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "patricia_tree") (r "^0.3") (k 2)) (d (n "static_assertions") (r "^1") (k 2)) (d (n "unicase") (r "^2") (k 0)))) (h "1afj6zgjw1a0195rd4lxn94lpqcjfbpg8q81dlw02745dzbivbqr") (r "1.57")))

