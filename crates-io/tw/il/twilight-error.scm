(define-module (crates-io tw il twilight-error) #:use-module (crates-io))

(define-public crate-twilight-error-0.11.0 (c (n "twilight-error") (v "0.11.0") (d (list (d (n "twilight-http") (r "^0.11") (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "09m3ipnlmwpkbiljqv4408rd8piip492j0vd6j3pdfsb5x5yhqjz") (y #t)))

(define-public crate-twilight-error-0.11.1 (c (n "twilight-error") (v "0.11.1") (d (list (d (n "twilight-http") (r "^0.11") (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "0dkldxwam5sjdbpq7gh7a2wjfb72i2hfdw6x6fag9p571ir0fjg6") (y #t)))

(define-public crate-twilight-error-0.11.2 (c (n "twilight-error") (v "0.11.2") (d (list (d (n "twilight-http") (r "^0.11") (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "1n2v49caz0p5x3c4niihfiwiw4hr6wnc0fp9k9v0aas52p4nxx7x") (y #t)))

(define-public crate-twilight-error-0.11.3 (c (n "twilight-error") (v "0.11.3") (d (list (d (n "twilight-http") (r "^0.11") (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "02pnbafwkfyp0wjsfz6ifav3f7nx0wxcidiphyyyr0wv8d1972dd") (y #t)))

(define-public crate-twilight-error-0.11.4 (c (n "twilight-error") (v "0.11.4") (d (list (d (n "twilight-http") (r "^0.11") (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "1jpm88dp2fgqzjal0idbj65llq4cpxl1mg62vf3ki7mv603dsjjc") (y #t)))

(define-public crate-twilight-error-0.12.0 (c (n "twilight-error") (v "0.12.0") (d (list (d (n "twilight-http") (r "^0.12") (d #t) (k 0)) (d (n "twilight-model") (r "^0.12") (d #t) (k 0)))) (h "0p11pfm3gr64960db565pl435lpbldga4wmc3k06k95pdylmdfxv") (y #t)))

