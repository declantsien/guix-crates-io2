(define-module (crates-io tw il twilight-convenience) #:use-module (crates-io))

(define-public crate-twilight-convenience-0.1.0 (c (n "twilight-convenience") (v "0.1.0") (d (list (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)))) (h "1ygj5z0shrjsls0wb9kizwzjv38wf7q1lclkvni3v8kf1z8hxgpl") (y #t)))

(define-public crate-twilight-convenience-0.1.1 (c (n "twilight-convenience") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "0yafhivpnks5vd11iwb4ffqhl98x5zj6v1bpywzyb876d4i2bh0p") (y #t)))

(define-public crate-twilight-convenience-0.1.2 (c (n "twilight-convenience") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "1zjrmx8xgjby2vfy2gamq19r0x7446h07yva6biih8l6kzjcx981") (y #t)))

(define-public crate-twilight-convenience-0.1.3 (c (n "twilight-convenience") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "1bzmbhzq9cnvfcwfcm7vwlx35pywba47w148x1bv971mk7jjf4kp") (y #t)))

(define-public crate-twilight-convenience-0.1.4 (c (n "twilight-convenience") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "048az1bkxldj01npniccph3z0klk6sjxk6if5i3wabwl08wjfs9s") (y #t)))

(define-public crate-twilight-convenience-0.1.5 (c (n "twilight-convenience") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "0r2dsmhwn5i8j5p1cxrykf67aiqmyaslw08g7dg62plp5fysn1vg") (y #t)))

(define-public crate-twilight-convenience-0.2.0 (c (n "twilight-convenience") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twilight-embed-builder") (r "^0.6") (d #t) (k 0)) (d (n "twilight-http") (r "^0.6") (d #t) (k 0)) (d (n "twilight-model") (r "^0.6") (d #t) (k 0)) (d (n "twilight-util") (r "^0.6") (f (quote ("snowflake"))) (d #t) (k 0)))) (h "1kpjmzxayprkpghiakqxn2rxvmx0ya842xnp2gbfj14y9snzwmc0") (y #t)))

