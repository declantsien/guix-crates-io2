(define-module (crates-io tw il twilio-rs) #:use-module (crates-io))

(define-public crate-twilio-rs-0.1.0 (c (n "twilio-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.28.1") (f (quote ("serde-with-str"))) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "166j5pl8j588igsdkd6rk2ghp717xzf50pmggn0my4iz7rwxccbr")))

(define-public crate-twilio-rs-0.1.1 (c (n "twilio-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.28.1") (f (quote ("serde-with-str"))) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0a83yp7jk2lnw93hij1waalvcxfhwqhzf6xz3fgz7d7ykwh9lcdk")))

