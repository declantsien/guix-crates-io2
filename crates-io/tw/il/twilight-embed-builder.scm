(define-module (crates-io tw il twilight-embed-builder) #:use-module (crates-io))

(define-public crate-twilight-embed-builder-0.0.0 (c (n "twilight-embed-builder") (v "0.0.0") (h "1bynxx792rfg5jwby9k8x9bryiycpdj8cz8c3pwvnxb74gaw486d")))

(define-public crate-twilight-embed-builder-0.1.0 (c (n "twilight-embed-builder") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.1") (k 0)))) (h "0m43q0gqgfv52bmf3kjr9qh4yl8961d52fr2asndcyaln4vhbm0r")))

(define-public crate-twilight-embed-builder-0.1.1 (c (n "twilight-embed-builder") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "<0.3, >=0.1, >=0.2.0-beta.0") (k 0)))) (h "0vrcd9fr1x5in826rn4ag2zfkh148j08ddrgg8gyg2874av2p8ra") (y #t)))

(define-public crate-twilight-embed-builder-0.2.0-beta.0 (c (n "twilight-embed-builder") (v "0.2.0-beta.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.2.0-beta") (k 0)))) (h "15mgrpp5iz0djn7grc4d36kvqm25jqwwzfvdsn489n9w3j3j5kk9")))

(define-public crate-twilight-embed-builder-0.2.0 (c (n "twilight-embed-builder") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.2") (k 0)))) (h "1hijgh36sbb80vl606qgxk1lw3wfm8ia4y56kc90zxj22igm1x2a")))

(define-public crate-twilight-embed-builder-0.3.0 (c (n "twilight-embed-builder") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.3") (k 0)))) (h "12id3i7pvcql9gsz883arq6b20s1yhqz9q4fzx0ns6h6fmv3rd9i")))

(define-public crate-twilight-embed-builder-0.4.0 (c (n "twilight-embed-builder") (v "0.4.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.4") (k 0)))) (h "1nbpl5hfrmw7yin7ihc188l3y5iambgwq8cwbixii86n744qfzak")))

(define-public crate-twilight-embed-builder-0.4.1 (c (n "twilight-embed-builder") (v "0.4.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.4") (k 0)))) (h "11cdyk4dpgb0zr5fkbi3i0w8fvyvrfr5h78ndvl01ivcficmld5c")))

(define-public crate-twilight-embed-builder-0.5.0 (c (n "twilight-embed-builder") (v "0.5.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.5.0") (k 0)))) (h "0s69qbmdxmpgbwyzm5vvys0kfsdv8j2nr089316hzxs9b9a8sbqx")))

(define-public crate-twilight-embed-builder-0.5.1 (c (n "twilight-embed-builder") (v "0.5.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.5") (k 0)))) (h "1ybg0931dr5wd827cbsdznd1zrj1dywz1jr5kadp60a62za5s8mh")))

(define-public crate-twilight-embed-builder-0.5.2 (c (n "twilight-embed-builder") (v "0.5.2") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.5") (k 0)))) (h "1p333ya2rcgpca4l2nlcgdfjwcay6513n6c8jpfa173xrg4ispn2")))

(define-public crate-twilight-embed-builder-0.6.0 (c (n "twilight-embed-builder") (v "0.6.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.6") (k 0)))) (h "14zcy9w5isyjv5lmw7dmc5nl4xkll34c7injim7p0nhpffvhacd1")))

(define-public crate-twilight-embed-builder-0.7.0 (c (n "twilight-embed-builder") (v "0.7.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.7") (k 0)))) (h "059c8m6zds2jk396c42r1b0mm3gv43kda6fax1wpbgn02y3sd5v2")))

(define-public crate-twilight-embed-builder-0.7.1 (c (n "twilight-embed-builder") (v "0.7.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.7") (k 0)))) (h "017pv10sp0lh72p78m7z2j0b37zzhm5ysn2j9g2d2n82f8v746ar")))

(define-public crate-twilight-embed-builder-0.8.0 (c (n "twilight-embed-builder") (v "0.8.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.8") (k 0)))) (h "0px9amjp1p813ni9671v24wcjjqk7ax9xaal63g2d29lz9m2fcmr")))

(define-public crate-twilight-embed-builder-0.9.0 (c (n "twilight-embed-builder") (v "0.9.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.9") (k 0)))) (h "0q47ajp273bz7qaypa540jc7ry9bix1q70ayxjqn0w5n0fid14m2") (r "1.57")))

(define-public crate-twilight-embed-builder-0.10.0 (c (n "twilight-embed-builder") (v "0.10.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.10") (k 0)))) (h "1pcphybb7g4zs0irgmarmsi0gibjbp6zggpwp21br74l64mzb9as") (r "1.57")))

(define-public crate-twilight-embed-builder-0.10.1 (c (n "twilight-embed-builder") (v "0.10.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.10.1") (k 0)))) (h "12kshv9k395zk51rx6v62mjn0bcrnvhpl00gwyz7f5ky5sxd543j") (r "1.57")))

(define-public crate-twilight-embed-builder-0.11.0 (c (n "twilight-embed-builder") (v "0.11.0") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.11.0") (k 0)))) (h "0afivccwm0mrrkipbxj9fdcy34mgff4b1393k1ra112m1y4gjaqb") (r "1.60")))

(define-public crate-twilight-embed-builder-0.11.1 (c (n "twilight-embed-builder") (v "0.11.1") (d (list (d (n "static_assertions") (r "^1") (k 2)) (d (n "twilight-model") (r "^0.11.1") (k 0)))) (h "1hf1jk921dwg9nw14m8qz0xw80fhipkh2154ss5c5s4q08wvd60k") (r "1.60")))

