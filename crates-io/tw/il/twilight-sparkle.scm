(define-module (crates-io tw il twilight-sparkle) #:use-module (crates-io))

(define-public crate-twilight-sparkle-0.1.0 (c (n "twilight-sparkle") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0fqsbwzwgcpgxd64mxv2mpkl901sc23i0bm1gh3n05fq7sisw3xf")))

(define-public crate-twilight-sparkle-0.1.3 (c (n "twilight-sparkle") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1wmh4gxmwpzdyqym1fzbnprz9cs9zcckca4hspdmdwcn3hz5gc03")))

(define-public crate-twilight-sparkle-0.2.0 (c (n "twilight-sparkle") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1f91cppm8ap2v41291hb5p5wn310gwmnqis9qid24syc576024yc")))

(define-public crate-twilight-sparkle-0.2.1 (c (n "twilight-sparkle") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0dap5q0p5qdnhk7j2s1vvs2pigbjwvmqvv7jdcw2rsxg4xicqqci")))

(define-public crate-twilight-sparkle-0.2.2 (c (n "twilight-sparkle") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1k52hx12ls3sb3x6h6agffjlidndi549cpccaryf991f7377r4l7")))

(define-public crate-twilight-sparkle-0.2.3 (c (n "twilight-sparkle") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1c9ygan36qnfxik5ihsn8lnsj3fwfmkacybgi6lkizf8j7nm33v9")))

(define-public crate-twilight-sparkle-0.2.4 (c (n "twilight-sparkle") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "httparse") (r "^1.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "178fdwba0v93x2l3pfwidk69w4il0w0agr0bykaq45182hhh1pfz")))

(define-public crate-twilight-sparkle-0.2.5 (c (n "twilight-sparkle") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0ag9q4npn677cdhlkyg81vdzw83rr9livs4ax24j4zb20vb2qpvw")))

(define-public crate-twilight-sparkle-0.3.0 (c (n "twilight-sparkle") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0vm4bvnhag8lir2wv9s3bv0xlj3hphga8wxjyzkf9zjhmdbp4nyn")))

(define-public crate-twilight-sparkle-0.4.0 (c (n "twilight-sparkle") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1h4pgza0nnxq9c7chzhfyip0g5x8a5bi2yxar41hkvwr0lr3rdrj")))

(define-public crate-twilight-sparkle-0.5.1 (c (n "twilight-sparkle") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jk96ys6182qlmcy1cnyg3xwpyxyddfqnlh7rnx5wdrqv13wmzfl")))

(define-public crate-twilight-sparkle-0.6.0 (c (n "twilight-sparkle") (v "0.6.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "httparse") (r "^1.5.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1sz6y9xkkgdx5yb1xmvyzb5bi7pzv9gxkw8fa6j85kcsgidwdnrr")))

(define-public crate-twilight-sparkle-0.7.0 (c (n "twilight-sparkle") (v "0.7.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "httparse") (r "^1.5.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18p38dp7kx5cls446q08k70j7riify2by0xcmfyl1yma6sybb0ps")))

(define-public crate-twilight-sparkle-0.8.0 (c (n "twilight-sparkle") (v "0.8.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1yn8l6mwv8in55a6vcicpzvhpbnq7w7q78lxyka4msynjx7hs9lj")))

(define-public crate-twilight-sparkle-0.8.1 (c (n "twilight-sparkle") (v "0.8.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1c64q94ipcz2qnc4b88k0f50y3pl1f8wmsscashls3k7mzqi53vx")))

(define-public crate-twilight-sparkle-0.9.0 (c (n "twilight-sparkle") (v "0.9.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0npcnb4161bywjvhz3cq9f2frxq22csc76yx74g41gbxafb4shmg")))

