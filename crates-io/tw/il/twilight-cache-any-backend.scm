(define-module (crates-io tw il twilight-cache-any-backend) #:use-module (crates-io))

(define-public crate-twilight-cache-any-backend-0.13.0 (c (n "twilight-cache-any-backend") (v "0.13.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "twilight-model") (r "^0.13") (d #t) (k 0)) (d (n "twilight-util") (r "^0.13") (f (quote ("permission-calculator"))) (d #t) (k 0)))) (h "0725ml5j5cq68831xs9awbysxn91wwfqf6g5v9z790qkwyxj1pm6") (y #t)))

