(define-module (crates-io tw il twilight-bucket) #:use-module (crates-io))

(define-public crate-twilight-bucket-0.1.0 (c (n "twilight-bucket") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "1vbl3a4qx8q6m3gsgghc45nmvdkm1xg45xqzvvar3h2l68w9ly1f") (y #t)))

(define-public crate-twilight-bucket-0.1.1 (c (n "twilight-bucket") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros" "time"))) (k 2)))) (h "1v3pgcqps1c2jqq2m7k2bpkbqyzqgrac71ir2541c2jkacxzmxhw")))

(define-public crate-twilight-bucket-0.1.2 (c (n "twilight-bucket") (v "0.1.2") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros" "time"))) (k 2)))) (h "1wsv79s0di53771iplx106hfz69jkza2x15g58fv6pnpn8g9v6lb")))

(define-public crate-twilight-bucket-0.1.3 (c (n "twilight-bucket") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros" "time"))) (k 2)))) (h "06w40fc48a41d95c8xl6dbms15frrx5qh811z5ycj4sjm0kp1scl")))

(define-public crate-twilight-bucket-0.2.1 (c (n "twilight-bucket") (v "0.2.1") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros" "time"))) (k 2)))) (h "1q9hgs0rx7ysn4ddx4bg2f4yrjgrscfqgb7hk2531pqn47vccj1r")))

(define-public crate-twilight-bucket-0.2.2 (c (n "twilight-bucket") (v "0.2.2") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros" "time"))) (k 2)))) (h "0qx2892daxfg17255frcw77wmc3vrsl78gbnv588i9ny7df92jl9")))

