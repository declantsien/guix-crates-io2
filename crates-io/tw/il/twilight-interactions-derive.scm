(define-module (crates-io tw il twilight-interactions-derive) #:use-module (crates-io))

(define-public crate-twilight-interactions-derive-0.7.0 (c (n "twilight-interactions-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15r81z758zmghp6lz730s17q35xnx1d7pv6h1phdydzn755cfsqq")))

(define-public crate-twilight-interactions-derive-0.7.1 (c (n "twilight-interactions-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v5xyqjgwhkrp45as6as25ljiigvfk6spp40kci2ad0vqzgnf56d")))

(define-public crate-twilight-interactions-derive-0.7.2 (c (n "twilight-interactions-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "024x92xzzl507yfv7xq5kdcza86aav00jl2093rd6n6siji1wj7m")))

(define-public crate-twilight-interactions-derive-0.8.0 (c (n "twilight-interactions-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15w9srj4c3lsgxphsvq7pps35b9ygbxk9v02ksbs7rkl2as6casq")))

(define-public crate-twilight-interactions-derive-0.8.1 (c (n "twilight-interactions-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fqm2b7z1w9vspxm6adrdk1qn9i609c1fx5calqmipxvx4c5i4pk")))

(define-public crate-twilight-interactions-derive-0.9.0 (c (n "twilight-interactions-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kq8sq8h1h6hz761pfjskm1s14sbwmckkbi9aj9siad20i2wx86q") (r "1.57")))

(define-public crate-twilight-interactions-derive-0.9.1 (c (n "twilight-interactions-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1l635y25md5yzy4x90j7p7k9jbbg1iyadhzqy2y4a36vjqkvn8dn") (r "1.57")))

(define-public crate-twilight-interactions-derive-0.10.0 (c (n "twilight-interactions-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kfw9d6xi6zxam0qnyxarwbkpqny5d7kmk92rr36p49aqys4gzqg") (r "1.57")))

(define-public crate-twilight-interactions-derive-0.10.1 (c (n "twilight-interactions-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d1czxmnmmzsbi2f8gx24ldqkkkxyx0mmg53dzpxxpsp0szp2sdm") (r "1.57")))

(define-public crate-twilight-interactions-derive-0.11.0 (c (n "twilight-interactions-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14j7c456s73rsv9cmqljk4xz8qpfk8dy94qx3jp1v84bivhmxr1p") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.12.0 (c (n "twilight-interactions-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kd5fbr8qhyy7qx2j4avdkvfsnf7cchqz6v823ys0fp32jvalyg6") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.13.0 (c (n "twilight-interactions-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16q0sa78m1qallfxkjk90z2z9cj2dzv08gpmz2xmxv5316sh1ng2") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.14.0 (c (n "twilight-interactions-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vh3injcjsnwa2hf58nfhcr1ndbnmvkg7gyyzdf3l3cqgwxiwphs") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.14.1 (c (n "twilight-interactions-derive") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "021lbfz3lnzdfkc452sl58va2sb0v5kwmi2gspn9361sdz3f8drb") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.14.2 (c (n "twilight-interactions-derive") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1gx6r917dkrgwi7iniygx3740dyfh2lfybyvbj9cfxaia0c7w6r5") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.14.3 (c (n "twilight-interactions-derive") (v "0.14.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1727m3s2m123yk8ys2g3sqdv0scncrgrkax5c2wsfx5xrg2n4xkp") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.14.4 (c (n "twilight-interactions-derive") (v "0.14.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "138pqhr1h4834zy3p0spg6gh9zajpwyhf0ngb8bfc1z3rn2wak8p") (r "1.60")))

(define-public crate-twilight-interactions-derive-0.15.0 (c (n "twilight-interactions-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kk22y7sp6zf3djiw4jr8vz3x0pwqmdfy1zkpywnhlghypxzf229") (r "1.64")))

(define-public crate-twilight-interactions-derive-0.15.1 (c (n "twilight-interactions-derive") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mba58w4f7rb6j48k09a2ya60qbxdpxxfjqxpg6jcxil37srp607") (r "1.64")))

(define-public crate-twilight-interactions-derive-0.15.2 (c (n "twilight-interactions-derive") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "12fllwh6z4b40qihkddjvh0ggxgzyjsaq93nyhpnf3p6kvrpwa9s") (r "1.67")))

(define-public crate-twilight-interactions-derive-0.16.0-rc.1 (c (n "twilight-interactions-derive") (v "0.16.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0w44v0bb97jh0qpf1bdh7b1nvkjjrnpyrkk52wlqdl6jr73pmh76") (r "1.67")))

