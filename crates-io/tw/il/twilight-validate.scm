(define-module (crates-io tw il twilight-validate) #:use-module (crates-io))

(define-public crate-twilight-validate-0.9.0 (c (n "twilight-validate") (v "0.9.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.9") (k 0)))) (h "1lz3wq8slffndzrrjd7jcy1xz0i7wffxmln7p5a5hpzpnfm0r4q2") (r "1.57")))

(define-public crate-twilight-validate-0.9.1 (c (n "twilight-validate") (v "0.9.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.9.1") (k 0)))) (h "1rdyka637fv9bxjzj8ib8h6w7rn84zsxy71624dgpd40nvqw1wlf") (r "1.57")))

(define-public crate-twilight-validate-0.9.2 (c (n "twilight-validate") (v "0.9.2") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.9.2") (k 0)))) (h "1443h7ym8v68wvzm75kmb4v5j7q25hbcqjipx2mhizyfqkzgk6dl") (r "1.57")))

(define-public crate-twilight-validate-0.10.0 (c (n "twilight-validate") (v "0.10.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.10") (k 0)))) (h "1b1ay1ixp6fn2yrdh3a0qznw13fbsbghcz79cpqfpqx30llgky40") (r "1.57")))

(define-public crate-twilight-validate-0.10.1 (c (n "twilight-validate") (v "0.10.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.10.1") (k 0)))) (h "0vvvdjwx1qmmjzsh6asjbmxivky16amvmr39zyjgzvlhgn8w7qq8") (r "1.57")))

(define-public crate-twilight-validate-0.10.2 (c (n "twilight-validate") (v "0.10.2") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.10.2") (k 0)))) (h "0qyiwvdbygsv6dncm8wcs7rkly7cvivza6ddzf7q9iqlb0hy2mvl") (r "1.57")))

(define-public crate-twilight-validate-0.10.3 (c (n "twilight-validate") (v "0.10.3") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.10.0") (k 0)))) (h "06iv18z7fvad2y6wy6fdxrvvw4ymznkshbwf7iaq5xc95iq0c439") (r "1.57")))

(define-public crate-twilight-validate-0.11.0 (c (n "twilight-validate") (v "0.11.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.11.0") (k 0)))) (h "0hgapha96v1jf8pql0fv27dh4x8agxm1rbfrpm50c8akgqnf53jd") (r "1.60")))

(define-public crate-twilight-validate-0.11.1 (c (n "twilight-validate") (v "0.11.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.11.1") (k 0)))) (h "1nqhr21fvqx6jczrlgf2yp3g024fv3al1vq507kcpbmjwiszdml2") (r "1.60")))

(define-public crate-twilight-validate-0.12.0 (c (n "twilight-validate") (v "0.12.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.12.0") (k 0)))) (h "128750qi5n7244w8ax4h03wniq06krs979c9q6bgq2hi6md3fzya") (r "1.60")))

(define-public crate-twilight-validate-0.12.1 (c (n "twilight-validate") (v "0.12.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.12.2") (k 0)))) (h "0qai12zy61f5kf9hgjl0a1m6s99jp7p9nfjnf5cyp2qdbc3z00p0") (r "1.60")))

(define-public crate-twilight-validate-0.13.0 (c (n "twilight-validate") (v "0.13.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.13.0") (k 0)))) (h "1yfb27d8igx5g0rn5zy7zgx58hmvd9h29w58vf0qxqhyqhxq5ab5") (r "1.60")))

(define-public crate-twilight-validate-0.13.1 (c (n "twilight-validate") (v "0.13.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.13.2") (k 0)))) (h "09lq5lv3s3imxlb1qncl8cx8xmi3mp05bzjmzxw4rbhjsc1fa5sc") (r "1.60")))

(define-public crate-twilight-validate-0.13.2 (c (n "twilight-validate") (v "0.13.2") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.13.6") (k 0)))) (h "0nwch7w2bw8p1a646yanfj2kki3wx30g1bh14k7v0m91bprkri4f") (r "1.60")))

(define-public crate-twilight-validate-0.14.0 (c (n "twilight-validate") (v "0.14.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.14.0") (k 0)))) (h "1kliqqss942qa6qgxpp7fd25yw4m8zlrlkvjd251n9kk9z6z15y8") (r "1.64")))

(define-public crate-twilight-validate-0.14.1 (c (n "twilight-validate") (v "0.14.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.14.1") (k 0)))) (h "1il4pccsd42mk648nq7yb707wc0ihjimdyz48j8dd9p6b6y9l8pc") (r "1.64")))

(define-public crate-twilight-validate-0.15.0-rc.1 (c (n "twilight-validate") (v "0.15.0-rc.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.15.0-rc.1") (k 0)))) (h "0bvy2sqbicra42yghi2z3l9qv1qj6m51b72nb65p58haxp44sx4c") (r "1.64")))

(define-public crate-twilight-validate-0.14.2 (c (n "twilight-validate") (v "0.14.2") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.14.3") (k 0)))) (h "1lpwmygr1xwj0lx568sfc3m0fdzasxv5i97vznk9q1fbh6fqiryj") (r "1.64")))

(define-public crate-twilight-validate-0.15.0 (c (n "twilight-validate") (v "0.15.0") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.15.0") (k 0)))) (h "1n3xagxdds498s32r7xrz41fyixw1abgfrv0xyril7pzgxgbvwhz") (r "1.64")))

(define-public crate-twilight-validate-0.15.1 (c (n "twilight-validate") (v "0.15.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.15.1") (k 0)))) (h "094gx8w3vhagxm034fyjyznrcy2b34maxn00aqsg0hsk0pxdcgwp") (r "1.64")))

(define-public crate-twilight-validate-0.15.2 (c (n "twilight-validate") (v "0.15.2") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.15.3") (k 0)))) (h "0n3crxymsr2azy46nz8hbd2ncqd9qkd3pp2zj3pwfpd6dy36ndwk") (r "1.67")))

(define-public crate-twilight-validate-0.15.3 (c (n "twilight-validate") (v "0.15.3") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.15.4") (k 0)))) (h "192xh7ds25dk4rss5vbdn5x13a6kafdqj0ba16bkm3axb6nxb59m") (r "1.67")))

(define-public crate-twilight-validate-0.16.0-rc.1 (c (n "twilight-validate") (v "0.16.0-rc.1") (d (list (d (n "static_assertions") (r "^1.1.0") (k 2)) (d (n "twilight-model") (r "^0.16.0-rc.1") (k 0)))) (h "0jcb8zfmrg1mz1i3bmqyhkpmr8q8y7p23s4081ks289ld0mvyw8d") (r "1.67")))

