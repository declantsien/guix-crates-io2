(define-module (crates-io tw il twilight-interactions) #:use-module (crates-io))

(define-public crate-twilight-interactions-0.7.0 (c (n "twilight-interactions") (v "0.7.0") (d (list (d (n "twilight-http") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "twilight-interactions-derive") (r "=0.7.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.7") (d #t) (k 0)))) (h "1d439cr77xg0f7y4mydz66jz79bnn2nk0wbx701h6i8gjq07vdc3") (f (quote (("http" "twilight-http") ("derive" "twilight-interactions-derive") ("default" "derive"))))))

(define-public crate-twilight-interactions-0.7.1 (c (n "twilight-interactions") (v "0.7.1") (d (list (d (n "twilight-http") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "twilight-interactions-derive") (r "=0.7.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.7") (d #t) (k 0)))) (h "1x0ma1wcn6ijgvhww6iambf2admyyi6hqsj8q4mpajrrgzzjckh7") (f (quote (("http" "twilight-http") ("derive" "twilight-interactions-derive") ("default" "derive"))))))

(define-public crate-twilight-interactions-0.7.2 (c (n "twilight-interactions") (v "0.7.2") (d (list (d (n "twilight-http") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "twilight-interactions-derive") (r "=0.7.2") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.7.2") (d #t) (k 0)))) (h "0d63g3wsay0rxa3yrv429a3k58q70f904sb78n9r4xlj5xr860qv") (f (quote (("http" "twilight-http") ("derive" "twilight-interactions-derive") ("default" "derive"))))))

(define-public crate-twilight-interactions-0.8.0 (c (n "twilight-interactions") (v "0.8.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.8.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.8") (d #t) (k 0)))) (h "1a0nzpr9izqsc3j5svgndb3adfd8yl6qnlq041sg3jckzzhbdxjm") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive"))))))

(define-public crate-twilight-interactions-0.8.1 (c (n "twilight-interactions") (v "0.8.1") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.8.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.8") (d #t) (k 0)))) (h "0ihcbsbwasfw24wxx8mb9igd6lkvb5rfv46pd0plr2m63c2vlbpb") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive"))))))

(define-public crate-twilight-interactions-0.9.0 (c (n "twilight-interactions") (v "0.9.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.9.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.9") (d #t) (k 0)))) (h "1v5qihvc1chmzwrdqfw36zal5rm05jlijdpy6ycc9g86mhbnc2la") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.57")))

(define-public crate-twilight-interactions-0.9.1 (c (n "twilight-interactions") (v "0.9.1") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.9.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.9") (d #t) (k 0)))) (h "0kp711l3f1invx54vd95vkkhq2qnxpvl2rib8mpdj6g7v9mcm0nv") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.57")))

(define-public crate-twilight-interactions-0.10.0 (c (n "twilight-interactions") (v "0.10.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.10.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.10") (d #t) (k 0)))) (h "0h2lwxpdprh1amqbdq2rv2y6m58i53l84gzngp31fdjgj64fg1fy") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.57")))

(define-public crate-twilight-interactions-0.10.1 (c (n "twilight-interactions") (v "0.10.1") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.10.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.10") (d #t) (k 0)))) (h "1v0y1zibfs62iz6w94w9rmlgj5hsl71qrpl80bxx0zz6hllwq8xb") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.57")))

(define-public crate-twilight-interactions-0.11.0 (c (n "twilight-interactions") (v "0.11.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "twilight-interactions-derive") (r "=0.11.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.11") (d #t) (k 0)))) (h "1nadaqc4sdjaa52ynxjmfz9mpzkdqgz2dlrnpz0kdl300i31vnhf") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.12.0 (c (n "twilight-interactions") (v "0.12.0") (d (list (d (n "twilight-interactions-derive") (r "=0.12.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.12") (d #t) (k 0)))) (h "1xfhbm1h4aq7sybp50r586lcjz69vdlcscr4292hfzjw1rkm1xhq") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.13.0 (c (n "twilight-interactions") (v "0.13.0") (d (list (d (n "twilight-interactions-derive") (r "=0.13.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.13") (d #t) (k 0)))) (h "099whsv5ldl508cxs1mjdyxp7p89sr4c1y1f0bj198rvwz90bjaz") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.14.0 (c (n "twilight-interactions") (v "0.14.0") (d (list (d (n "twilight-interactions-derive") (r "=0.14.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.14") (d #t) (k 0)))) (h "1z7n66wcb2sxr6qscnbsy4jwrpmsam5i1v0ly78n3sdi9axaf4ak") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.14.1 (c (n "twilight-interactions") (v "0.14.1") (d (list (d (n "twilight-interactions-derive") (r "=0.14.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.14") (d #t) (k 0)))) (h "0kfvi52rchxkygm13fwgy9jl0cy69aavydnd6n9ncf731jw135y3") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.14.2 (c (n "twilight-interactions") (v "0.14.2") (d (list (d (n "twilight-interactions-derive") (r "=0.14.2") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.14") (d #t) (k 0)))) (h "0kg6a73aiga5844z4axpp0b3ayf5ba61f830g8lxpn7s1367my2k") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.14.3 (c (n "twilight-interactions") (v "0.14.3") (d (list (d (n "twilight-interactions-derive") (r "=0.14.3") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.14.1") (d #t) (k 0)))) (h "1gwq479kdywmrn1kp0mc1xmjhcv87lac1fzjx4hlvmld9m3508jn") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.14.4 (c (n "twilight-interactions") (v "0.14.4") (d (list (d (n "twilight-interactions-derive") (r "=0.14.4") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.14.1") (d #t) (k 0)))) (h "0vyldxs6qn4dkxyzz33k66vxcvr9ipl6535j15nh5wxb82mr0v8y") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.60")))

(define-public crate-twilight-interactions-0.15.0 (c (n "twilight-interactions") (v "0.15.0") (d (list (d (n "twilight-interactions-derive") (r "=0.15.0") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.15.0") (d #t) (k 0)))) (h "1maahik8x1ijfkg3mggb23bbic9cw0rfdanxjjcmdqkbajx6q7cg") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.64")))

(define-public crate-twilight-interactions-0.15.1 (c (n "twilight-interactions") (v "0.15.1") (d (list (d (n "twilight-interactions-derive") (r "=0.15.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.15.0") (d #t) (k 0)))) (h "1cl0jr7bnhjlnm873xv0s9d695nl2h1h4yf6k0vdkwvhds7dh5my") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.64")))

(define-public crate-twilight-interactions-0.15.2 (c (n "twilight-interactions") (v "0.15.2") (d (list (d (n "twilight-interactions-derive") (r "=0.15.2") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "^0.15.0") (d #t) (k 0)))) (h "15ap53dnxnaz2h4ajyxpxmhc92ld72xhm7fy5a146q38fhhncizi") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.67")))

(define-public crate-twilight-interactions-0.16.0-rc.1 (c (n "twilight-interactions") (v "0.16.0-rc.1") (d (list (d (n "twilight-interactions-derive") (r "=0.16.0-rc.1") (o #t) (d #t) (k 0)) (d (n "twilight-model") (r "=0.16.0-rc.1") (d #t) (k 0)))) (h "08mpdjpw8k0smvqzzfiv6ync6yl0q4pszxl75czfdvqf0mc1fryw") (f (quote (("derive" "twilight-interactions-derive") ("default" "derive")))) (r "1.67")))

