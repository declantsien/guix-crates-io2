(define-module (crates-io tw yn twyngled) #:use-module (crates-io))

(define-public crate-twyngled-0.0.0 (c (n "twyngled") (v "0.0.0") (d (list (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "twyg") (r "^0.1.10") (d #t) (k 0)))) (h "0sk78q84r1gqimvgl179qw2yapzvawrsl72g4b1w623z8ybgraxn")))

(define-public crate-twyngled-0.0.1 (c (n "twyngled") (v "0.0.1") (d (list (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "twyg") (r "^0.1.10") (d #t) (k 0)))) (h "0sjjcrjsmm4widvy36fkhnmnb4hnj3xpgc78kcph5r9kaxanbwwv")))

