(define-module (crates-io tw yn twyne) #:use-module (crates-io))

(define-public crate-twyne-0.0.0 (c (n "twyne") (v "0.0.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cp6jab6y40rqwcdlzccfdckwxj2kcsdbvcsjlnbkg70n3xdjs5i")))

(define-public crate-twyne-0.1.0 (c (n "twyne") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lrlrg8mmavyf8gx5a9lapywlk1wlkr2kc9klv8p58n5gnick8vs")))

(define-public crate-twyne-0.2.0-rc1 (c (n "twyne") (v "0.2.0-rc1") (d (list (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h5338cqz0mwlf1agr5xq06qbmzw01rgk4gp78cj6bmf1x68q1bq")))

(define-public crate-twyne-0.2.0 (c (n "twyne") (v "0.2.0") (d (list (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19lmacf9gh34fysrm47j6k0xm52m0d9prln5xm3pc4gx4iszr8ri")))

