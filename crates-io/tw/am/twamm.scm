(define-module (crates-io tw am twamm) #:use-module (crates-io))

(define-public crate-twamm-0.1.0 (c (n "twamm") (v "0.1.0") (d (list (d (n "ahash") (r "=0.7.6") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pyth-sdk-solana") (r "^0.7.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.13") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "0sm65ryzgq6567phyjb5dljxwgg4dll9v1jskrjyj0m1xggrkkp7") (f (quote (("test") ("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

