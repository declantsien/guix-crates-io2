(define-module (crates-io tw am twamp) #:use-module (crates-io))

(define-public crate-twamp-0.1.0 (c (n "twamp") (v "0.1.0") (d (list (d (n "bebytes") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net" "os-ext"))) (d #t) (k 0)) (d (n "network_commons") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validator") (r "^0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l42wag2hl1wjdikivsbxy1pxm2yjyj5zgnsl04rd5xx6l6bffm6")))

