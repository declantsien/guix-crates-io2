(define-module (crates-io tw ea tweakster) #:use-module (crates-io))

(define-public crate-tweakster-0.0.1 (c (n "tweakster") (v "0.0.1") (d (list (d (n "merge") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nld43jrdhls8xvpkbwcp29mwar65fqacnqy3hlcckxbkk0kz5ll")))

(define-public crate-tweakster-0.0.2 (c (n "tweakster") (v "0.0.2") (d (list (d (n "merge") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06snjl2j75vck0wh1qhhk5m4qw5gh2s0wkij50mcpmjnsj43dcq5")))

(define-public crate-tweakster-0.0.3 (c (n "tweakster") (v "0.0.3") (d (list (d (n "goblin") (r "^0.2.1") (d #t) (k 0)) (d (n "merge") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^1.5") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01zc9z0lq7xfb38fpb8p1jb6rh0gxxjfp78hww2hs7vfvsannzpm")))

