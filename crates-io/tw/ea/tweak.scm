(define-module (crates-io tw ea tweak) #:use-module (crates-io))

(define-public crate-tweak-0.1.0 (c (n "tweak") (v "0.1.0") (h "1wjw3cc1ynnmghjd8pzi82iw175rif7724hvdvsx3yah9qfyfg17")))

(define-public crate-tweak-0.1.1 (c (n "tweak") (v "0.1.1") (h "0n726csld7xwli68lfsj4acfzfxrsxp81qk0sh0pqgw0qcbg1bdb")))

