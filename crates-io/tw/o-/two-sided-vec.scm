(define-module (crates-io tw o- two-sided-vec) #:use-module (crates-io))

(define-public crate-two-sided-vec-0.1.0 (c (n "two-sided-vec") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "04x0v09c3ih0zv8g3vd63jf6p7jh9w95nz91zv7p9v0ii4j4ra4w")))

(define-public crate-two-sided-vec-0.1.1 (c (n "two-sided-vec") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1k2diihgdid5gmyswg8wdfhfmfr1ifqji51hbql3i495cqi08n6c")))

(define-public crate-two-sided-vec-0.1.2 (c (n "two-sided-vec") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1kfivah9kim1rv21pjj7y45cgr3b9ixgvlx06mshkkx8frncfn2s")))

(define-public crate-two-sided-vec-0.1.3 (c (n "two-sided-vec") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1v40a9a05j6yhm14p871cm9w7y27swlfpn0x2i7qqb4gz912yawg")))

(define-public crate-two-sided-vec-0.1.4 (c (n "two-sided-vec") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0zij05iz8xbrgazlr1m2cvllv5wyqvq5higrb18bn7gh392gj183")))

(define-public crate-two-sided-vec-0.1.5 (c (n "two-sided-vec") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "148vwbnr6n0c1dvhm5jsjpj7hy5r985wsr84wy8n00p5zszrq9xg")))

(define-public crate-two-sided-vec-0.1.6 (c (n "two-sided-vec") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "01zp3qbgi1vb6fp4yzyp5sac4ns4jkysq0hb50l0wb1lxdpb3fj7")))

(define-public crate-two-sided-vec-0.1.7 (c (n "two-sided-vec") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0k7yik8hwlr54rl36vz7155k1byawfpkl7sbykkpacw12s67cqx8")))

(define-public crate-two-sided-vec-0.1.8 (c (n "two-sided-vec") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1szpl3iyg7jixvmm9fp89xwak4jf40qi3kzf1glmh8bnww5axk33")))

(define-public crate-two-sided-vec-0.1.9 (c (n "two-sided-vec") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0bna80w5x7kr1wpl747iwmwxqa98pc7r81wqxja6cwawzbb43zkd")))

(define-public crate-two-sided-vec-0.1.10 (c (n "two-sided-vec") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1z5ld7l972ypzfaij9ci8s6imfqzybi9dg0i3jyxjg3mpnfxiy6c")))

(define-public crate-two-sided-vec-0.1.11 (c (n "two-sided-vec") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1637myv6l568g320vj90vz0w1xw3j0900qrq6c29if4yhf9brwmq")))

