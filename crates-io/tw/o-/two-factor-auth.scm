(define-module (crates-io tw o- two-factor-auth) #:use-module (crates-io))

(define-public crate-two-factor-auth-0.1.0 (c (n "two-factor-auth") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 0)))) (h "1kxh2fx0b5y6r5lw77ldgng1hym7nb9i36l0v5qvjf94d84cfpjv")))

(define-public crate-two-factor-auth-0.1.1 (c (n "two-factor-auth") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 0)))) (h "1d1875qcpdcljh51kkp7ldx19msmxglp4y430s8k94zb4f6ixrd6")))

