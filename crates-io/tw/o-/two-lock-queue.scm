(define-module (crates-io tw o- two-lock-queue) #:use-module (crates-io))

(define-public crate-two-lock-queue-0.1.0 (c (n "two-lock-queue") (v "0.1.0") (h "1qwh532yg6fv9bnw78ialijh7hblm8kjg2cz6k9y4xvx07sszljq")))

(define-public crate-two-lock-queue-0.1.1 (c (n "two-lock-queue") (v "0.1.1") (h "1a9gr0fi0wn2vccx8lw4x877x2iqlx3f9yqhzjni9f6wx3agmaxx")))

