(define-module (crates-io tw o- two-rusty-forks-macro) #:use-module (crates-io))

(define-public crate-two-rusty-forks-macro-0.1.0 (c (n "two-rusty-forks-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "02hxxhbkcpl5dd1hh13zl3d9fbjsnmz3vpzwlzxnl3jg3ymdgss9") (y #t)))

(define-public crate-two-rusty-forks-macro-0.1.1 (c (n "two-rusty-forks-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "13qf2406zh4rbmwyq34mwjh5zdqfbl392l248dwzdb00q5f48caq")))

(define-public crate-two-rusty-forks-macro-0.4.0 (c (n "two-rusty-forks-macro") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1qr9gzxppf4gnmqwdy3nwyvmwcmdyzws8f0yqa34y5bqfhs1yl01")))

