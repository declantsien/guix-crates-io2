(define-module (crates-io tw it twitter-api-v2) #:use-module (crates-io))

(define-public crate-twitter-api-v2-0.0.0 (c (n "twitter-api-v2") (v "0.0.0") (h "0s85l0xi3v2sn0qy2a3pxcawkd555vc4ryqllbac7xbbfjq3a32z")))

(define-public crate-twitter-api-v2-0.0.1 (c (n "twitter-api-v2") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-aux") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)))) (h "0a0f7n2p67zdyz4m0cw9ys0qq4xb0sxg1gqn048r1fb6w59132dg")))

