(define-module (crates-io tw it twitter-text) #:use-module (crates-io))

(define-public crate-twitter-text-0.1.0 (c (n "twitter-text") (v "0.1.0") (d (list (d (n "idna") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "twitter_text_config") (r "^0.1.0") (d #t) (k 0)) (d (n "twitter_text_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "0766mgds4gl0a0rk0hk903vhjfgmj2zhmb5rnf26cbfifv1cz98d")))

(define-public crate-twitter-text-0.2.0 (c (n "twitter-text") (v "0.2.0") (d (list (d (n "idna") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "twitter_text_config") (r "^0.2.0") (d #t) (k 0)) (d (n "twitter_text_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "0wd8sd52bfc8zwz2d70cbgpxgdd7wh0qqs7kh6sjy45q8b208da0")))

