(define-module (crates-io tw it twitter-stream-message) #:use-module (crates-io))

(define-public crate-twitter-stream-message-0.1.0 (c (n "twitter-stream-message") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04plfpkx76wpzhxybf9d3m03pizp18y798n309cdp3xkgnd1lkkm")))

(define-public crate-twitter-stream-message-0.2.0 (c (n "twitter-stream-message") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0d6f3m9isx73lp3pm86hh6hziabwrxc0z8v05h8x2vx945zyp34w")))

(define-public crate-twitter-stream-message-0.2.1 (c (n "twitter-stream-message") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02gjf31xb42vxy9vfhbk53iqy9plc0ap7j02dnnqxsks7qnjyy7i")))

(define-public crate-twitter-stream-message-0.3.0 (c (n "twitter-stream-message") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nb371ygdzz0r5f2k75wxrh5yh19qhs6gn5vk6fvq690lniclni5")))

