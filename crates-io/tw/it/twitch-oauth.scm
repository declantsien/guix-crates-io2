(define-module (crates-io tw it twitch-oauth) #:use-module (crates-io))

(define-public crate-twitch-oauth-0.1.0 (c (n "twitch-oauth") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xpbwkfbzfiyh0s8fiw8z5pbpbsdw6svsxr30valsir3fgw2jk88")))

