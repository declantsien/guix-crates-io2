(define-module (crates-io tw it twitch_ircv3_parse) #:use-module (crates-io))

(define-public crate-twitch_ircv3_parse-0.0.1 (c (n "twitch_ircv3_parse") (v "0.0.1") (d (list (d (n "ircv3_parse") (r "^0.0.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "twitch_helix_api") (r "^0.0.1") (d #t) (k 0)))) (h "0gqilb5xanljapaylyf8bxcjs8bi1m7dgyh4v77na4k4iv31v2py")))

(define-public crate-twitch_ircv3_parse-0.0.2 (c (n "twitch_ircv3_parse") (v "0.0.2") (d (list (d (n "ircv3_parse") (r "^0.0.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "twitch_highway") (r "^0.0.2") (d #t) (k 0)))) (h "08win6kfz94hybbmxc1sdp9yvrbmxhkzcs754fawi1nv8kp8bdzw")))

(define-public crate-twitch_ircv3_parse-0.0.3 (c (n "twitch_ircv3_parse") (v "0.0.3") (d (list (d (n "ircv3_parse") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "twitch_highway") (r "^0.0.2") (d #t) (k 0)))) (h "1b16flhays89hpgir7gnm64vbjib3963s6xjmn3icmhswlz731lk")))

