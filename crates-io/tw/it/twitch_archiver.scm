(define-module (crates-io tw it twitch_archiver) #:use-module (crates-io))

(define-public crate-twitch_archiver-0.3.0 (c (n "twitch_archiver") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0grmaa3k51y3vfygyj14z9q0ikj4rim1y995nyfz5zcxnhaf9xfn") (y #t)))

(define-public crate-twitch_archiver-0.3.1 (c (n "twitch_archiver") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lxpgprm388gk7xx2gsz4dn5dpaj3is5a2bv9h404gs9lcyhm1nf") (y #t)))

(define-public crate-twitch_archiver-0.3.2 (c (n "twitch_archiver") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d4aamhwk4f5m85fi1f0ydig1rdwflz34fvnnrzgi4hl9vvxxvgd")))

(define-public crate-twitch_archiver-0.3.3 (c (n "twitch_archiver") (v "0.3.3") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rgl41qcjhhh86n0ma4yy4w8ynyq9xd1dhy4xxqcz480yhc8vci9")))

