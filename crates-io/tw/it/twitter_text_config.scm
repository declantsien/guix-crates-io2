(define-module (crates-io tw it twitter_text_config) #:use-module (crates-io))

(define-public crate-twitter_text_config-0.1.0 (c (n "twitter_text_config") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n8k0cy0cdmd2qxq4crhwk21bwfs0skl56vhvqn8ccpwqh5jfscz")))

(define-public crate-twitter_text_config-0.2.0 (c (n "twitter_text_config") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xzc0mfjf48rwaw6pch1b3id67pna10dnzrrb9q1l898h8vqyzig")))

