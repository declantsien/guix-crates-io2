(define-module (crates-io tw it twitter_text_parser) #:use-module (crates-io))

(define-public crate-twitter_text_parser-0.1.0 (c (n "twitter_text_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "08m7ljf027dybpnqin562y93l6f6fhrkvj8hd2s9h4d2l92lqdnb")))

(define-public crate-twitter_text_parser-0.2.0 (c (n "twitter_text_parser") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0s13w7brilic24gwvfg7b893cz4v31cr7kbcx9f7civqz0295ma6")))

