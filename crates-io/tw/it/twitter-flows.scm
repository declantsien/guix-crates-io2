(define-module (crates-io tw it twitter-flows) #:use-module (crates-io))

(define-public crate-twitter-flows-0.1.0 (c (n "twitter-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0kl7f8b0wyq9gimvmqyv82scjyl8miljw2w3lgdxahxzycgflnmf")))

(define-public crate-twitter-flows-0.2.0 (c (n "twitter-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "079xv314hl3qc7k7wirvh4iirkcqg53054cdafrhk4bmv5w5s8xj")))

(define-public crate-twitter-flows-0.2.1 (c (n "twitter-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "09lhim3rmi987ax0nsc5ji7d5m9mdzq76kbhjf7vh17xkpv8fhkk")))

