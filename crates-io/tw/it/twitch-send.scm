(define-module (crates-io tw it twitch-send) #:use-module (crates-io))

(define-public crate-twitch-send-1.0.0 (c (n "twitch-send") (v "1.0.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "1hg6fl48rj87b6y82zc60arcdy6a8wsdmbvpvjp8ccq3jgblx27g")))

(define-public crate-twitch-send-1.0.1 (c (n "twitch-send") (v "1.0.1") (d (list (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "09gqjv89l2lcr808q757nll00yk2afvpwkhcl996ym2wvhxz3020")))

(define-public crate-twitch-send-1.0.2 (c (n "twitch-send") (v "1.0.2") (d (list (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "0h5cgcsgfvfj5x2mkkzqvgc3wx9fafldxz7n499pcspibqps6whh")))

