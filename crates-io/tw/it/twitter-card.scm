(define-module (crates-io tw it twitter-card) #:use-module (crates-io))

(define-public crate-twitter-card-0.1.0 (c (n "twitter-card") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1yld3498a2hnym82ms7j786gzjk2mhl31zxdnxi2qp7fi2np21mh")))

(define-public crate-twitter-card-1.0.0 (c (n "twitter-card") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1qq43ixgz42rqdh6bad8y67hn3padn18niq705jbz03wyckcw6ms")))

(define-public crate-twitter-card-1.0.1 (c (n "twitter-card") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "011k3dqmys2m780j6hbj96nh4s9c5x48k6p6m9bc7qwdmdqwvn9c")))

