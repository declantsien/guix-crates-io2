(define-module (crates-io tw it twitch_app_api) #:use-module (crates-io))

(define-public crate-twitch_app_api-0.1.0 (c (n "twitch_app_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "sync" "blocking" "macros"))) (d #t) (k 0)))) (h "046bi0nsggvc5qsyfk4sagy26zhplhryyg31x989wh6b4gkvr5n7")))

(define-public crate-twitch_app_api-0.1.1 (c (n "twitch_app_api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1kydp246fni86k72canr5hf0mf8k35hp3ps5b87hsgmh08hwhany")))

(define-public crate-twitch_app_api-0.1.2 (c (n "twitch_app_api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1hb5j1swq58kr6x0bnfzmwb2y02vvrz21kv0646xxycbfaffmlii")))

(define-public crate-twitch_app_api-0.1.3 (c (n "twitch_app_api") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1ranx543w6hq57ky5hwljldmv0gw84m5g3ypddcp3w7rs39208ap")))

