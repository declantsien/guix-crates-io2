(define-module (crates-io tw it twitter_text_conformance) #:use-module (crates-io))

(define-public crate-twitter_text_conformance-0.2.0 (c (n "twitter_text_conformance") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "twitter-text") (r "^0.2.0") (d #t) (k 0)) (d (n "twitter_text_config") (r "^0.2.0") (d #t) (k 0)) (d (n "twitter_text_parser") (r "^0.2.0") (d #t) (k 0)))) (h "1hn8zpg01s7zzjc2wmmdgxmpdv1yy4zwnghz3h66i2499ijwj2ba")))

