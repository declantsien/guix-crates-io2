(define-module (crates-io tw it twitter-archive) #:use-module (crates-io))

(define-public crate-twitter-archive-0.0.1 (c (n "twitter-archive") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4.3.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 2)))) (h "1gk4zcyssdzlw3hd3kp8cdky4f2zjhdh7wry01ffsr0njwkwn3bs")))

