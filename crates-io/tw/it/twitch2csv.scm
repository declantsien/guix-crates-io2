(define-module (crates-io tw it twitch2csv) #:use-module (crates-io))

(define-public crate-twitch2csv-0.1.0 (c (n "twitch2csv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "time" "sync" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^3.0.1") (d #t) (k 0)))) (h "0vjydn1723pjy8c96nxvzilyqwckryww1wx6rmhqg5ypc65b759f")))

(define-public crate-twitch2csv-0.1.1 (c (n "twitch2csv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "time" "sync" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^3.0.1") (d #t) (k 0)))) (h "0gcf0pi5ql5qwd8il41q6m5nhwfpz1qybbv81mwdj81lhvcm7k2a")))

