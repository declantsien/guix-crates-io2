(define-module (crates-io tw it twitter-bot) #:use-module (crates-io))

(define-public crate-twitter-bot-0.1.0 (c (n "twitter-bot") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "egg-mode") (r "^0.15") (f (quote ("rustls"))) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "rss") (r "^1.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1vcjpp2s1v16fpdsldbjg4syg96xyig3f968b1a7rsixl4l5kxbg")))

(define-public crate-twitter-bot-0.2.0 (c (n "twitter-bot") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "egg-mode") (r "^0.15") (f (quote ("rustls"))) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "rss") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1kn048dkr0q4dkzhby5b96mjddzcbiy9sm1hp81lbi8d9a6zcgc4")))

(define-public crate-twitter-bot-0.3.0 (c (n "twitter-bot") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "egg-mode") (r "^0.15") (f (quote ("rustls"))) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "rss") (r "^1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1iv7zzkkhfrfm3adx3hxfkm1yywc0jnqjcmgjh7igfsk6s6jp3r1") (y #t)))

(define-public crate-twitter-bot-0.3.1 (c (n "twitter-bot") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "egg-mode") (r "^0.15") (f (quote ("rustls"))) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "rss") (r "^1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)))) (h "1b5gxszdn1cbxji406l03y354q4lkc3bb0lh8jfwdwss76mwpbxv")))

