(define-module (crates-io tw it twitch-rs) #:use-module (crates-io))

(define-public crate-twitch-rs-0.1.0 (c (n "twitch-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10racnn6bc9mkw5ksh96sppws99631c6c8100zkcbzagwrpdpx23")))

(define-public crate-twitch-rs-0.2.0 (c (n "twitch-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bx360l6qrp0v6kdp6h49y5v8hgs8qs6rywp3r4m3v8v16wldidm")))

