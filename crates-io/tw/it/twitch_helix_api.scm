(define-module (crates-io tw it twitch_helix_api) #:use-module (crates-io))

(define-public crate-twitch_helix_api-0.0.1 (c (n "twitch_helix_api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0sfja4hyc7ahy1kyq1m2nqavyncp9g7qxilg447zknw3j57wpn0g") (y #t)))

(define-public crate-twitch_helix_api-9.9.9 (c (n "twitch_helix_api") (v "9.9.9") (h "01cn2n9f5jjjryl23qrypmpqx29waic9h8bnv5ixwn25ga52c7f3") (y #t)))

