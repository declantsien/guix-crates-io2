(define-module (crates-io tw it twitch_chat_wrapper) #:use-module (crates-io))

(define-public crate-twitch_chat_wrapper-0.1.1 (c (n "twitch_chat_wrapper") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "smol") (r "^0.3.3") (d #t) (k 0)) (d (n "twitchchat") (r "^0.14.4") (f (quote ("smol" "async"))) (d #t) (k 0)))) (h "0jf0gc1kpg37xgfb8m9vkgs9rkkp8jl9y6d3f3z23fxdicabqnqx")))

(define-public crate-twitch_chat_wrapper-0.2.0 (c (n "twitch_chat_wrapper") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "smol") (r "^0.3.3") (d #t) (k 0)) (d (n "twitchchat") (r "^0.14.4") (f (quote ("smol" "async"))) (d #t) (k 0)))) (h "0kh0h8nwk3s9pd8nnjmndi25p8jyipm5cdpwjrclw73p7jnnaicg")))

