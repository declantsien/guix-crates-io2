(define-module (crates-io tw it twitch_asknothingx2) #:use-module (crates-io))

(define-public crate-twitch_asknothingx2-0.0.1 (c (n "twitch_asknothingx2") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (f (quote ("native-tls" "rustls"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0dxcyv0xam3ikn16izf70xg9fjs2i9jhliyphf9nd2v8j156gdja")))

