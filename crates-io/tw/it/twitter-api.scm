(define-module (crates-io tw it twitter-api) #:use-module (crates-io))

(define-public crate-twitter-api-0.0.1 (c (n "twitter-api") (v "0.0.1") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)))) (h "0mrxsad4slsazv6r7vf5j87r4m08bwk0slc70jwpqnpdbl4q4c01")))

(define-public crate-twitter-api-0.0.2 (c (n "twitter-api") (v "0.0.2") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)))) (h "0yx6jfhxf1s83iccnhf2rn52kccipijjaykvs1dxjmxw3rbv2nvp")))

(define-public crate-twitter-api-0.0.3 (c (n "twitter-api") (v "0.0.3") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)))) (h "1f08044rl69yqvpv4pzylgfi94zxpkqmf92hwv634njpl4yi02zv")))

(define-public crate-twitter-api-0.0.4 (c (n "twitter-api") (v "0.0.4") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0incg8pr2xiw9bdrw4wv8920w6vlsfr33kxr28bqsifdm2s71650")))

(define-public crate-twitter-api-0.0.6 (c (n "twitter-api") (v "0.0.6") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "06y3xsrm57d706g69ic5p4kakxw64c53vy226fvyr28rcxhgjliy")))

(define-public crate-twitter-api-0.0.7 (c (n "twitter-api") (v "0.0.7") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1853xziiclyv0zj50jzz3pnd4733d8pz5sdyk3mb4pjnqbih9k6v")))

(define-public crate-twitter-api-0.0.8 (c (n "twitter-api") (v "0.0.8") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "133cbfd75zvaxcqrpjybyp1vl9s259sf8m50cwszwxsfy3lyk7lm")))

(define-public crate-twitter-api-0.0.9 (c (n "twitter-api") (v "0.0.9") (d (list (d (n "oauth-client") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1bywib3mzvfk6rzb32vshnbb08c02fyp38p09v2p0y4qqh39hb36")))

(define-public crate-twitter-api-0.1.0 (c (n "twitter-api") (v "0.1.0") (d (list (d (n "oauth-client") (r "^0.0.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1am09bqrrviwha9y56g987syxv0qpflqbxcxd7fxplvd277v5bw5")))

(define-public crate-twitter-api-0.2.0 (c (n "twitter-api") (v "0.2.0") (d (list (d (n "oauth-client") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "05d1jnli7hcrifna8ks0qrg0gyw2d6g8zrqizyrhhja7dmspgy8k")))

(define-public crate-twitter-api-0.2.1 (c (n "twitter-api") (v "0.2.1") (d (list (d (n "oauth-client") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m40zm8x3b1saagpsmzzcgzln81nxbzrhgmfnhqkj8by1plj4711")))

(define-public crate-twitter-api-0.3.0 (c (n "twitter-api") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "oauth-client") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d0kncyl7ipqm4k13v1pmabpwzibh54f9byljw4df8mp5kazlrim")))

(define-public crate-twitter-api-0.4.0 (c (n "twitter-api") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "oauth-client") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qjpzlc55fz03aj81rf743r3z2ap2sh2z2kpwhlch2pqwdnp0ykk")))

(define-public crate-twitter-api-0.5.0 (c (n "twitter-api") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 2)) (d (n "oauth-client") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0gdkd6c66rh5jawwlzylja87i36gwb4i2gkg5d01yc4gn8bdh68x")))

(define-public crate-twitter-api-0.6.0 (c (n "twitter-api") (v "0.6.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 2)) (d (n "oauth-client") (r "^0.8.0") (k 0)) (d (n "oauth-client") (r "^0.8.0") (f (quote ("client-reqwest"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1n0cnqg1lgz7d9j4c0lk1zjb6y84531d51bms2yw8fgxjzp19ss3") (r "1.57.0")))

(define-public crate-twitter-api-0.6.1 (c (n "twitter-api") (v "0.6.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 2)) (d (n "oauth-client") (r "^0.8.0") (k 0)) (d (n "oauth-client") (r "^0.8.0") (f (quote ("client-reqwest"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rz4c19bkh4mb0v2w6by5x4bp6gz2rbxz0fjqjhsfxdmzvwnr894") (r "1.57.0")))

