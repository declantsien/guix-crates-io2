(define-module (crates-io tw it twitch-scraper) #:use-module (crates-io))

(define-public crate-twitch-scraper-0.1.0 (c (n "twitch-scraper") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "1rasxm14phvnrv3nx3v7933fhqg2rix9r4q007cihzc4zim8sd92")))

(define-public crate-twitch-scraper-0.1.2 (c (n "twitch-scraper") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "164gnvq318iz6ihmq0miia1nb69p2dl2s9d5kghbfy5jd534dg85")))

