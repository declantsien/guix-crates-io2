(define-module (crates-io tw it twitter-scraper) #:use-module (crates-io))

(define-public crate-twitter-scraper-0.1.0 (c (n "twitter-scraper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zpsh4ypy78i6blb2hax8rjxrsi9hcp1i1vzr8pc0q7bcsfvswnh")))

(define-public crate-twitter-scraper-0.2.0 (c (n "twitter-scraper") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dw36hl722fwib5ljcbs0xa9vczbzbfcgkb40idqh9sddn4gfpri")))

(define-public crate-twitter-scraper-0.3.0 (c (n "twitter-scraper") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xz3p3lwfg0yxd7xi7ivj5dm3bf3skbynb7d849dd5kw4pr0cvrm")))

