(define-module (crates-io tw it twitch-helix) #:use-module (crates-io))

(define-public crate-twitch-helix-0.1.0 (c (n "twitch-helix") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1x4jp1y56qgflr65prdgyy6y5xpx6isj3lvbb9kf1q9llmwwn31h")))

