(define-module (crates-io tw of twofloat) #:use-module (crates-io))

(define-public crate-twofloat-0.1.0 (c (n "twofloat") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1m87qy7hs63rciz61s31rn1bkr5yazjidsiky5pqs68rqj3rzvyb")))

(define-public crate-twofloat-0.1.1 (c (n "twofloat") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0z2f40jjbv2hm6n79l18bmgf3ff45rpcxxq6dnk3rmv2v0g0k6g6") (y #t)))

(define-public crate-twofloat-0.1.2 (c (n "twofloat") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gdb0p27bqs14f1khrfpfkr0nlv3fpbw4y9lpw6b4n381mgvwj79") (y #t)))

(define-public crate-twofloat-0.1.3 (c (n "twofloat") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "070dnbslpd8060rzc90b3vd5bcx3dipang6i8daprv8rq4w164mq")))

(define-public crate-twofloat-0.1.4 (c (n "twofloat") (v "0.1.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1b9r732czp28yrygdpg52z04rvh7vcq7j5ibjpi1aacmw141rzb2")))

(define-public crate-twofloat-0.2.0 (c (n "twofloat") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1g6f4mr2pasy8q95frp7v2pwv5rv9rr3dz4w7zhz568vqq0hapxk")))

(define-public crate-twofloat-0.2.1 (c (n "twofloat") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1bqcz9hfmc5vgdy3kjl1kl55vfjwj5fm1aflsp5fprnblwy1z7rx") (y #t)))

(define-public crate-twofloat-0.2.2 (c (n "twofloat") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1rv23h5vj5mlzyk3qy3cv8xc8qlfsx3rx62p9d4fljym756p2kb8")))

(define-public crate-twofloat-0.3.0 (c (n "twofloat") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05fz0g27dad22qngifzignvn7jlgaz36mxp1rnb4r47jycc783kl") (f (quote (("serde_support" "serde") ("math_funcs") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.3.1 (c (n "twofloat") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "17rrdz878c2g45ballkxnxm6sjkhgaqn10l57q87g8lvp9ihixkn") (f (quote (("serde_support" "serde") ("math_funcs") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.4.0 (c (n "twofloat") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05mwhmn24asjibcprg7qg681py1bnnjrdjmb7zmqrgdyxai0kilx") (f (quote (("serde_support" "serde") ("math_funcs") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.4.1 (c (n "twofloat") (v "0.4.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0ls2r363s38iisrqwkpkngd8aga0666dwz19i9k7is18gbcbklz1") (f (quote (("serde_support" "serde") ("math_funcs") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.5.0 (c (n "twofloat") (v "0.5.0") (d (list (d (n "hexf") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "04vcx8vq3d7yzjamgsl4pipn07swh1w2bwn4yjcp96np2994gbpm") (f (quote (("math_funcs" "num-traits/std") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.6.0 (c (n "twofloat") (v "0.6.0") (d (list (d (n "hexf") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "14zghnp5xq6cn720mh617wf8p9d1bdxaxjnjldzprgbvligkvnhf") (f (quote (("math_funcs" "num-traits/std") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.6.1 (c (n "twofloat") (v "0.6.1") (d (list (d (n "hexf") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (t "cfg(all(windows, target_env = \"gnu\"))") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03gp4w5w5anygpc3z3pi245w193g29897yifalx3sc9s0snyzlvb") (f (quote (("math_funcs" "num-traits/std") ("default" "math_funcs"))))))

(define-public crate-twofloat-0.7.0 (c (n "twofloat") (v "0.7.0") (d (list (d (n "hexf") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (t "cfg(all(windows, target_env = \"gnu\"))") (k 0)))) (h "1iccj9yxwfir9cip5h9z986lknbgjnrmbyxhh9wygy5vnk6q23pm") (f (quote (("std" "num-traits/std") ("math_funcs" "std") ("default" "std" "math_funcs"))))))

