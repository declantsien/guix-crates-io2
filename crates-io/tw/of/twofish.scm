(define-module (crates-io tw of twofish) #:use-module (crates-io))

(define-public crate-twofish-0.0.0 (c (n "twofish") (v "0.0.0") (h "0qklnj4swc4q0yav3l5xivwvrr3zjpqr4ni8pj2xg0gssixclblv")))

(define-public crate-twofish-0.1.0 (c (n "twofish") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0bp2q9k8w7h7z2pl01bywgp2qq4z9n7rs4bxkldyrl5h0mzk5vqy")))

(define-public crate-twofish-0.2.0 (c (n "twofish") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1q9l4h2zqr119x8fwn596vdkavn3cynaqpdpvgich9z7hcg2cbbi")))

(define-public crate-twofish-0.3.0 (c (n "twofish") (v "0.3.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1xxadb4crx1y2jb7shsh2r95sf44p25jn78d6hxcgv2c33vwgiss")))

(define-public crate-twofish-0.4.0 (c (n "twofish") (v "0.4.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "04hga0m7rjg8r5v6qw2rmz8lvqrfwvlsi87g11p8yf6pasr0v8z7")))

(define-public crate-twofish-0.5.0 (c (n "twofish") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1hhv8h6gb7xr2g9d7b3qbpnl7y34qvafl21hpjhwkv135ycgaa00")))

(define-public crate-twofish-0.6.0 (c (n "twofish") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "15hjr7d6s2n9vipxrghcaxx1kx3383j7falxzrrd49a8g1z6p3vj")))

(define-public crate-twofish-0.7.0 (c (n "twofish") (v "0.7.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0qcl10ixad4qw35jyk0ksmirqrahdgr37i3nnx1k7dfzlg3bk026") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-twofish-0.7.1 (c (n "twofish") (v "0.7.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "04w0ii2c0c9ws08aw6c7illh9zql22il9lbwjk1mgir30aiq73m7") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

