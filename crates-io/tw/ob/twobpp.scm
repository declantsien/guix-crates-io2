(define-module (crates-io tw ob twobpp) #:use-module (crates-io))

(define-public crate-twobpp-0.1.0 (c (n "twobpp") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "14dh4jn85pxzylhxi9ds8i833llmv4jah4pijsrd248fw7hds3g3")))

(define-public crate-twobpp-0.1.1 (c (n "twobpp") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "09gj3i5qq1ag8k3pgqg5zjbn97wzwmgi0g9dm9zx9qhd9vd7jdl1")))

