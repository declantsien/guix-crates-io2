(define-module (crates-io tw ob twobit) #:use-module (crates-io))

(define-public crate-twobit-0.1.0 (c (n "twobit") (v "0.1.0") (h "00fw91rsfczcnsh1l7zphjds2bgzln7fbg3s22b6b1maziwsmcwb")))

(define-public crate-twobit-0.1.1 (c (n "twobit") (v "0.1.1") (h "0fip08vxy14jfm9f15ffvhqcb65hhbw5ls65bblpf7f5mb6iji6x")))

(define-public crate-twobit-0.2.0 (c (n "twobit") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (t "cfg(bench)") (k 2)))) (h "1p9w4mpnckx74rm3avx4p34kk8nvdk05ndbxnaa4ncr75xj4fmf7")))

(define-public crate-twobit-0.2.1 (c (n "twobit") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (t "cfg(bench)") (k 2)))) (h "01lh7mkanxrd7bkd3qn552r359j96k06x0p9w6n3qb3sd8xpkmfc")))

