(define-module (crates-io tw as twasmi-validation) #:use-module (crates-io))

(define-public crate-twasmi-validation-0.3.0 (c (n "twasmi-validation") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "tetsy-wasm") (r "^0.41.0") (k 0)))) (h "1vzpvi96jp6lkkipgzjzlsh6ac67hfwqldisdwlxc5dygfva11vb") (f (quote (("std" "tetsy-wasm/std") ("default" "std") ("core"))))))

