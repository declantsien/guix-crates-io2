(define-module (crates-io tw as twasm-utils-cli) #:use-module (crates-io))

(define-public crate-twasm-utils-cli-0.6.0 (c (n "twasm-utils-cli") (v "0.6.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-wasm") (r "^0.31.1") (d #t) (k 0)) (d (n "twasm-utils") (r "^0.6.1") (d #t) (k 0)))) (h "0x57sh0w0xvgqzkymnspfjy759gxlrcca76f3mn49gxfkbnz3ljg")))

