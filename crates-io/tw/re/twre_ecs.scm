(define-module (crates-io tw re twre_ecs) #:use-module (crates-io))

(define-public crate-twre_ecs-0.0.1 (c (n "twre_ecs") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "156i6cbgn2qdna50l9srwlvwn4vvk8b5kx07srsgxsldgpj734a5")))

(define-public crate-twre_ecs-0.0.2 (c (n "twre_ecs") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0vfz6974kg1f9s9ixi0yc5z355kz3m4ld0sbn6i1iky8jjb6b0mp")))

