(define-module (crates-io tw oz twozero48) #:use-module (crates-io))

(define-public crate-twozero48-0.1.0 (c (n "twozero48") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "128x7mzqsmvq5dki7lwsf6awl476va144kldfvjxxvljza0xpk9d")))

(define-public crate-twozero48-0.1.1 (c (n "twozero48") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0w6qbdxnn32gzkw2n9yxyj3g4imi9289yz0dkjp2fi37ajdzcqpv") (y #t)))

(define-public crate-twozero48-0.1.2 (c (n "twozero48") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0j6aji3cckp8wliggc55jii0djmawqi9pk3rz8hz2iiwcaa3dr01")))

(define-public crate-twozero48-0.1.3 (c (n "twozero48") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1dylirim12wh2bly1fjx87x9c39bf47kfrfk6ynbcrcjdwm43253")))

