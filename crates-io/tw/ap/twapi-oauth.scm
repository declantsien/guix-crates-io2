(define-module (crates-io tw ap twapi-oauth) #:use-module (crates-io))

(define-public crate-twapi-oauth-0.1.0 (c (n "twapi-oauth") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0k6908jgmf7wpb3jfwiz4bsrzvdrapvcrkmz8daivv0nrzjnsdx2")))

(define-public crate-twapi-oauth-0.1.1 (c (n "twapi-oauth") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0gn6vyh909rnqd692989bd0rqf5lm7vfzaw97h31m72q1f71n8kk")))

(define-public crate-twapi-oauth-0.1.2 (c (n "twapi-oauth") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1z43jjl4x0s1yk11y3iyci6zp0v98nycgg1bx1q1xmy4g5zzq5mn")))

(define-public crate-twapi-oauth-0.1.3 (c (n "twapi-oauth") (v "0.1.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0ris5zhj25dgmrzvb3xldjlga43z3npjwrksa11c2wakjn2jwdjx")))

(define-public crate-twapi-oauth-0.1.4 (c (n "twapi-oauth") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0gnyihpf9vpsqhngnr9hpphzhfs41a8869dhg1mql4268kj7qqpq")))

