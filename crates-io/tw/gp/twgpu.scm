(define-module (crates-io tw gp twgpu) #:use-module (crates-io))

(define-public crate-twgpu-0.1.0 (c (n "twgpu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.12.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "121zy43cx7431s4xz80nlwhmhqk4axgg7c6n17kn111pyc1cd732")))

(define-public crate-twgpu-0.2.0 (c (n "twgpu") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "1ihw4axifbldzj0ad911yc344bnv6ryjlkhkv8p744gi3ncjhk9i")))

