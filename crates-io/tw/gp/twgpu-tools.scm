(define-module (crates-io tw gp twgpu-tools) #:use-module (crates-io))

(define-public crate-twgpu-tools-0.2.0 (c (n "twgpu-tools") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "twgpu") (r "^0.2.0") (d #t) (k 0)) (d (n "twmap") (r "^0.7.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "018wrlp0rn5x3jlik29frdah9gm7hl8riy2w7p7mf6zzj4gmv5hz")))

