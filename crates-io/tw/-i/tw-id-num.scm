(define-module (crates-io tw #{-i}# tw-id-num) #:use-module (crates-io))

(define-public crate-tw-id-num-0.1.0 (c (n "tw-id-num") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0nynkgwqa4fc5k5yxz0mpqn21hdf8cjpck59038gfpr4mq36qg2h") (f (quote (("generate" "rand"))))))

(define-public crate-tw-id-num-0.1.1 (c (n "tw-id-num") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0kyqnhiaw68c61m4a47fwj0i8s0gygr5q9ni2l3xqfllxrxxiw60") (f (quote (("generate" "rand"))))))

(define-public crate-tw-id-num-0.1.2 (c (n "tw-id-num") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0nd7lb77d0xj7y7jj7zi3mb6vkiwfkpsgps7bw8xdm1x1q4cja6d") (f (quote (("generate" "rand")))) (r "1.56")))

