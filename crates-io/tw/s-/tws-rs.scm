(define-module (crates-io tw s- tws-rs) #:use-module (crates-io))

(define-public crate-tws-rs-0.1.0 (c (n "tws-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "time-tz") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 0)))) (h "1ky2na6p5r1kp8vl2jjg10fcmqggmdlry6nv616bibj4g4961zih")))

