(define-module (crates-io tw _p tw_pack_lib) #:use-module (crates-io))

(define-public crate-tw_pack_lib-0.1.13 (c (n "tw_pack_lib") (v "0.1.13") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "1ms6dm19b4aprbdlnia820x9kwscpcx370jvai02nknsf3qs94vb")))

(define-public crate-tw_pack_lib-0.1.14 (c (n "tw_pack_lib") (v "0.1.14") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "0bdxvkdyiwpbp4rjfhbmdc7jds6yqr5j6hdflhbpqghsjx6ckz5j")))

(define-public crate-tw_pack_lib-0.1.15 (c (n "tw_pack_lib") (v "0.1.15") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "1as8l05kb0035zp4dg7m9vb6alz4rlvg9yjx3d66k41n4hq56359")))

(define-public crate-tw_pack_lib-0.1.16 (c (n "tw_pack_lib") (v "0.1.16") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "0cx361vzk52ikmg0z3j4dd5c8wzqigbal2qbjs7qhfdd9mz86bd5")))

(define-public crate-tw_pack_lib-0.1.17 (c (n "tw_pack_lib") (v "0.1.17") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "14qfxv9x51wds3k01ph6hqnzwirmx0mklsqrd5l72cqmv8mbhlgp")))

(define-public crate-tw_pack_lib-0.1.18 (c (n "tw_pack_lib") (v "0.1.18") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "124j9ij2wg9nm3h9z5snsyfc4zflrswgpbm0n8yckjfghfvrn2aq")))

(define-public crate-tw_pack_lib-0.1.19 (c (n "tw_pack_lib") (v "0.1.19") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "0jwmhsscqb0rk4yslrgdqy253vldsg63rbqzvlfdqam4wvxny071")))

(define-public crate-tw_pack_lib-0.1.20 (c (n "tw_pack_lib") (v "0.1.20") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.3") (d #t) (k 0)))) (h "1a0ndff6qpcc1zr0swq9wr4fl8v1bg8s55myp7wgrp2ayz8zlhz5")))

(define-public crate-tw_pack_lib-0.1.21 (c (n "tw_pack_lib") (v "0.1.21") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.3") (d #t) (k 0)))) (h "170ddbg35g3dqw8sra3yiycsxlwz71whwa3r43r7868n36ya0wr2")))

