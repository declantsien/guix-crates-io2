(define-module (crates-io tw in twink) #:use-module (crates-io))

(define-public crate-twink-0.1.0 (c (n "twink") (v "0.1.0") (h "1y4m4d9fap4jdnvgfgmqgl14vzjrhbm7gssd0d09gmpsrc4a84cg")))

(define-public crate-twink-0.1.1 (c (n "twink") (v "0.1.1") (h "1j5y3ind53zfg48lxbzkk9yjq5gmx3gkzng7la2i9qz4vkzi0svk")))

(define-public crate-twink-1.0.0 (c (n "twink") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "twink_macros") (r "^1.0.0") (d #t) (k 0)))) (h "0nqwlcpvg0zb8d1xc6cvz2s19cw0s72n8ggm0jcaqvfp514sq5hc") (f (quote (("24-hour" "timestamps") ("12-hour" "timestamps")))) (s 2) (e (quote (("timestamps" "dep:chrono"))))))

(define-public crate-twink-1.1.0 (c (n "twink") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("kv_unstable"))) (o #t) (d #t) (k 0)) (d (n "twink_macros") (r "^1.1.0") (d #t) (k 0)))) (h "1qrnh5yxjni265vp7cj4qk7vsxi08mbximqnxqyq73a8p8vxvk2i") (f (quote (("24-hour" "timestamps") ("12-hour" "timestamps")))) (s 2) (e (quote (("timestamps" "dep:chrono") ("log" "dep:log"))))))

(define-public crate-twink-1.1.1 (c (n "twink") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("kv_unstable"))) (o #t) (d #t) (k 0)) (d (n "twink_macros") (r "^1.1.0") (d #t) (k 0)))) (h "041hphblgm2bv890jb5gpn6plimbckypgpmaiwfrw08sd631cb4c") (f (quote (("24-hour" "timestamps") ("12-hour" "timestamps")))) (s 2) (e (quote (("timestamps" "dep:chrono") ("log" "dep:log"))))))

(define-public crate-twink-1.1.2 (c (n "twink") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("kv_unstable"))) (o #t) (d #t) (k 0)) (d (n "twink_macros") (r "^1.1.0") (d #t) (k 0)))) (h "1fdab73vc917i6d12krsxvyb9c86cigmlml9ryz1xzp4cls82x2a") (f (quote (("24-hour" "timestamps") ("12-hour" "timestamps")))) (s 2) (e (quote (("timestamps" "dep:chrono") ("log" "dep:log"))))))

(define-public crate-twink-1.1.3 (c (n "twink") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("kv_unstable"))) (o #t) (d #t) (k 0)) (d (n "twink_macros") (r "^1.1.0") (d #t) (k 0)))) (h "10d1vhg52m09y8zvrbi38g73gbwaw2wfn4vm5y7prjp4qijw3d0y") (f (quote (("24-hour" "timestamps") ("12-hour" "timestamps")))) (s 2) (e (quote (("timestamps" "dep:chrono") ("log" "dep:log"))))))

