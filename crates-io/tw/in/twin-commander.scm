(define-module (crates-io tw in twin-commander) #:use-module (crates-io))

(define-public crate-twin-commander-0.1.0 (c (n "twin-commander") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("clock"))) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (f (quote ("no_alloc"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("termion"))) (k 0)) (d (n "tui-input") (r "^0.8.0") (f (quote ("termion"))) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kh87pk6nj9fkh12snwz1c3mlppvcipjw5qf3g2z923mcf1w8gx3")))

