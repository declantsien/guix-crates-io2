(define-module (crates-io tw in twinsies) #:use-module (crates-io))

(define-public crate-twinsies-1.0.0 (c (n "twinsies") (v "1.0.0") (h "0y0nn3jp4lr8ff9snkms41nmqsnkigp1j97078z2rbig118v6a7q")))

(define-public crate-twinsies-1.1.0 (c (n "twinsies") (v "1.1.0") (h "13dg5q3fvbxqls1dwj18b23jiksw96ikfmvvi7g0b14vficfig1j")))

(define-public crate-twinsies-1.2.0 (c (n "twinsies") (v "1.2.0") (h "142rlf41gzl9kda524n481nf29b5krfy7zkdvpyzvbvqbngizkp2") (f (quote (("safest-memory-ordering") ("default" "safest-memory-ordering"))))))

(define-public crate-twinsies-1.2.1 (c (n "twinsies") (v "1.2.1") (h "0yb69anmm08z9bx7y9vlpb2ima5szpafk31py0zisp7k1zclklp1") (f (quote (("safest-memory-ordering") ("default" "safest-memory-ordering"))))))

