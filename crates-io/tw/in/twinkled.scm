(define-module (crates-io tw in twinkled) #:use-module (crates-io))

(define-public crate-twinkled-0.1.0 (c (n "twinkled") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jh0pnc5ihs2bx6w5nf7hh118ar7claicb0p0k41s1yi5ba2akl3")))

(define-public crate-twinkled-0.1.1 (c (n "twinkled") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0l4ajda0g9dlkpld73lhwwy6vvr62ipydry2mfmcsdzqhr9qkyyv")))

(define-public crate-twinkled-0.1.2 (c (n "twinkled") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "06jykq5dvaxq27ygwx7bacgl23gz4hgam3d39vn61mbfffvv1rph")))

