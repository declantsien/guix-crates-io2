(define-module (crates-io tw in twink_macros) #:use-module (crates-io))

(define-public crate-twink_macros-0.1.0 (c (n "twink_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "090qlanzx8ma8ilq382vdzaasg7l0a4s0l551crzbqr1afsq6c1j") (y #t)))

(define-public crate-twink_macros-1.0.0 (c (n "twink_macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0ljn6blj27pjbs1np4qvs92hvglrn6c45nrx8rd18njj4ms73sa7")))

(define-public crate-twink_macros-1.1.0 (c (n "twink_macros") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0d5hfjgzwqagbv61039w48hlljn8j7k73kz4638kvw7ak5j3z0bf")))

