(define-module (crates-io tw in twine) #:use-module (crates-io))

(define-public crate-twine-0.0.1 (c (n "twine") (v "0.0.1") (h "1vxzvagqzh73j1hxmawwsxxyhh54kgfs99ylkngavm71zyaj5d58") (y #t)))

(define-public crate-twine-0.0.2 (c (n "twine") (v "0.0.2") (h "07rxg1yaqb6j39qshx1s24xpi0vsjvb5v023l00xjj05gh222qx1") (y #t)))

(define-public crate-twine-0.1.0 (c (n "twine") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1awlvpsa4jak0p10lbc77yh6i31qb920971bqa1dk7b8855k7bfn")))

(define-public crate-twine-0.1.1 (c (n "twine") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fym5bp035rnxw847xpqbsy03vmrx86bwy2cnqqy15lkds70ky56")))

(define-public crate-twine-0.1.2 (c (n "twine") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cbcni3si1q7q1izzrwfyy9kk61rsal1jk080s4zm829nzgmmnq2")))

(define-public crate-twine-0.2.0 (c (n "twine") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09dy3bih0hdng1mgypjkjm7jxmx2rfd4i2vzncjsqr9mmawbfclh")))

(define-public crate-twine-0.3.0 (c (n "twine") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08cknsllv3pa3my28vgm8phqgxl4qmahm33khl4l020cjdwfq4pp") (y #t)))

(define-public crate-twine-0.3.1 (c (n "twine") (v "0.3.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ahr5pn0ybfap98myvfsg519vjvw7vh09zxkwbx2ii24lgazvrzi")))

(define-public crate-twine-0.3.2 (c (n "twine") (v "0.3.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ggkxdqkgx1sj33vma66chd3jr8m2nx4pimdjzph256772blgkjb") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.3.3 (c (n "twine") (v "0.3.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0d7giq204pjfnhvqkigly45jnyhkn5fhsrivv58ibj36h9dgjnva") (f (quote (("serde"))))))

(define-public crate-twine-0.3.4 (c (n "twine") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "10r5h77isw5q8gdrc3nrpfdr6d74971q7g6mn3y4cnjqmhsij1l4") (f (quote (("serde"))))))

(define-public crate-twine-0.3.5 (c (n "twine") (v "0.3.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0a0z4nqflqxi1cvisg3k97ayzpxp4md0v932m0i15nj7ipj826h2") (f (quote (("serde"))))))

(define-public crate-twine-0.3.6 (c (n "twine") (v "0.3.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0w1x6qyxw3173qzys0674v80fm6yxj34iyc63z6kxf0kpjllp1nd") (f (quote (("serde"))))))

(define-public crate-twine-0.3.7 (c (n "twine") (v "0.3.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1jd0vl2dbdsrp0f5zw30fmcy92x7yyw8rm1xy8nkpsvyi66a5bks") (f (quote (("serde"))))))

(define-public crate-twine-0.3.8 (c (n "twine") (v "0.3.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02cx91v9mqpgdc6zyn1pncw45q95x4achzf1iwdq7his21fv86gq") (f (quote (("serde"))))))

(define-public crate-twine-0.3.9 (c (n "twine") (v "0.3.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lvzzzi0dcrgfjx4ibi51z20y58r2kyap7rinfcb5ws2qsygy7y4") (f (quote (("serde"))))))

(define-public crate-twine-0.3.10 (c (n "twine") (v "0.3.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1mmipavy31b56vi143hprbqvl3r7m7j90pl4jyp30n1jdwmzbp8f") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.3.11 (c (n "twine") (v "0.3.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wr34f8kid8hgffy8ixvhnf846wcr4f6g4j6c1sc3phnap37p72h") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.3.12 (c (n "twine") (v "0.3.12") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13zgjc3526bszpqbgqdywkd5bjcxarf0nb7zainfbbplwfg89rqr") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.3.13 (c (n "twine") (v "0.3.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1la190dwfls2i3yphbqi2a0m8flx9i7xjlk5wl359kq0pff6r1yp") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.4.0 (c (n "twine") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h4113axhmnn06w8lf06pb9frs70s5v2pbc03s6wk3639895h9i0") (f (quote (("serde"))))))

(define-public crate-twine-0.5.0 (c (n "twine") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1brhdcsxcb6js5ldqxxiggfm41vl6vlqx8yrd8xpk0y936bpj9z2") (f (quote (("serde"))))))

(define-public crate-twine-0.5.1 (c (n "twine") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06ivgx6mk0bs0bmyl6w9x85bapgc3ym0wx72swz0rbs50icajxs6") (f (quote (("serde")))) (y #t)))

(define-public crate-twine-0.5.2 (c (n "twine") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "097py7fh74ilmizf0y2jg9i2qv3hr9h41h1bm2nl9sfv0l13pba4") (f (quote (("serde"))))))

(define-public crate-twine-0.5.3 (c (n "twine") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1n1ivn0kgxsz87ipvdd3ysl7wjzajpnwviwma2ziycfd4j2kp8rx") (f (quote (("serde"))))))

(define-public crate-twine-0.6.0 (c (n "twine") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07k3vr747g8609gi3j90r23hmic74by5xy7kwmw70za45yasdlss") (f (quote (("serde"))))))

(define-public crate-twine-0.7.0 (c (n "twine") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indenter") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0aqd8dadkw9wcj7yg3d2xzf6wx8r58a3y68ha6c9bbk07wappd12") (f (quote (("serde"))))))

