(define-module (crates-io tw in twin) #:use-module (crates-io))

(define-public crate-twin-0.1.0 (c (n "twin") (v "0.1.0") (d (list (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "08r4vsbpmr7c7y4382qmgsqgwr35ll63igcdb0nyn8qzbc7gim2q")))

(define-public crate-twin-0.1.4 (c (n "twin") (v "0.1.4") (d (list (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "0clx1fdd7d12j0sx76lrs76wvkydcmdmzw6n431s6jhm6qvj1bhf")))

