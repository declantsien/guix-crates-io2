(define-module (crates-io tw in twinkle) #:use-module (crates-io))

(define-public crate-twinkle-0.1.0 (c (n "twinkle") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bqbg19m49hkhjgyc5fmlirh88fjkv5ra1qfrm1g2c2a52jgl3b2")))

(define-public crate-twinkle-0.1.1 (c (n "twinkle") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kw7y28s7pkpbmbzyvrxxw3z748dbwx4dkanxsbj85qgask3vqpa")))

(define-public crate-twinkle-0.1.2 (c (n "twinkle") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kbgznv7zqh667336z8lj2ranqsx1mw2lrni881yp1bqvd0pw93g")))

(define-public crate-twinkle-0.1.3 (c (n "twinkle") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wh56188pydj3w19fm4ybmz34b2hihfald0z92dr2v8b41qrzrzg")))

