(define-module (crates-io tw in twinkle_client) #:use-module (crates-io))

(define-public crate-twinkle_client-0.1.0 (c (n "twinkle_client") (v "0.1.0") (d (list (d (n "pin-project") (r "^1.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0f0kja7ag99vw8df4pkiraa7h0qi0knb2mzq9avbzxfsqc5ap208")))

(define-public crate-twinkle_client-0.2.0 (c (n "twinkle_client") (v "0.2.0") (d (list (d (n "pin-project") (r "^1.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "15600212bschnpx1zvfahim0pvis299ddx6l2ly8vdx72csghhcg")))

(define-public crate-twinkle_client-0.2.1 (c (n "twinkle_client") (v "0.2.1") (d (list (d (n "pin-project") (r "^1.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnj3ng8wqb5q4b4abbphvina7mgj4mmvfcwh5yph8w1b0dk41gd")))

