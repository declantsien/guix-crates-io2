(define-module (crates-io tw -s tw-storage-extra) #:use-module (crates-io))

(define-public crate-tw-storage-extra-0.1.2 (c (n "tw-storage-extra") (v "0.1.2") (d (list (d (n "cosmwasm-std") (r "^0.16.7") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.7") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1b7jh0si174947k6ra983ad6hicg1fyjxdjhgn8z5bffdpjp4am0") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

