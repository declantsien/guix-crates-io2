(define-module (crates-io tw -s tw-storage-macros) #:use-module (crates-io))

(define-public crate-tw-storage-macros-0.1.2 (c (n "tw-storage-macros") (v "0.1.2") (d (list (d (n "cosmwasm-std") (r "^0.16.7") (d #t) (k 2)) (d (n "cosmwasm-storage") (r "^0.16.7") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0.93") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyqrx1vsvjmzmcrfaq836a7cjk528h4fw4h5klrsa6xm3gi9wlk") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

