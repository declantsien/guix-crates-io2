(define-module (crates-io tw an twang) #:use-module (crates-io))

(define-public crate-twang-0.1.0 (c (n "twang") (v "0.1.0") (d (list (d (n "adi") (r "^0.12") (f (quote ("speaker"))) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1ksichpclcm3jc42gc3gs8fl1vwrp7fax2xplgjx55nhsxwizrvr")))

(define-public crate-twang-0.2.0 (c (n "twang") (v "0.2.0") (d (list (d (n "adi") (r "^0.12") (f (quote ("speaker"))) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0gzcxmh1rc1ylyjl81012rcfp814z14iysqidax1y8akswvy9c9s")))

(define-public crate-twang-0.3.0 (c (n "twang") (v "0.3.0") (d (list (d (n "fon") (r "^0.2") (d #t) (k 0)))) (h "01mrmj79g1izzxdf7nwzxq8d8511r11fksl9mslb1xwhvj418sqd")))

(define-public crate-twang-0.4.0 (c (n "twang") (v "0.4.0") (d (list (d (n "fon") (r "^0.2") (d #t) (k 0)))) (h "0q3z0nrk0r1jjnf3mbdxbcawa2fy5h3xcwph9r3xz8r3h7a8ar48")))

(define-public crate-twang-0.5.0 (c (n "twang") (v "0.5.0") (d (list (d (n "fon") (r "^0.3") (d #t) (k 0)))) (h "05zl5ara7hs06fllcp36cq44mcl94wzyk8z1wqjbc76a9vqh5x5k")))

(define-public crate-twang-0.6.0 (c (n "twang") (v "0.6.0") (d (list (d (n "fon") (r "^0.4") (d #t) (k 0)))) (h "1pvyhav42cqj2invmp5qk5z53jw5bz1r8z8vc8h8r11249rmf8yy")))

(define-public crate-twang-0.7.0 (c (n "twang") (v "0.7.0") (d (list (d (n "fon") (r "^0.5") (d #t) (k 0)))) (h "1fgic4bvv904ahp8ib5r8y0l3k5dy13m8v2x1x38vhbzfr43js2z")))

(define-public crate-twang-0.8.0 (c (n "twang") (v "0.8.0") (d (list (d (n "fon") (r "^0.6") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "0fl0g22xfv31nnw2m0h7psvbvn54wfgc4axhssrg43bh382qjaj8") (r "1.56.1")))

(define-public crate-twang-0.9.0 (c (n "twang") (v "0.9.0") (d (list (d (n "fon") (r "^0.6") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "splotch") (r "^0.0.1") (d #t) (k 2)))) (h "03bzhc4clbmsgwzfhlrwrpd01biihc5b9mjbz0lvh5s43b08rr6i") (r "1.60.0")))

