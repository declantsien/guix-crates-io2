(define-module (crates-io tw ic twice) #:use-module (crates-io))

(define-public crate-twice-0.2.0 (c (n "twice") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15qb8vlfchqyqwd92zplhzma2kx9pnndblw2ysdrcwch6zf4f9i0")))

(define-public crate-twice-0.3.0 (c (n "twice") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05nxf9i9lxrkyggiib302z5na4v2i1pny07xg12xxqp9d8yymrqv")))

