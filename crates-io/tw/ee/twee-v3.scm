(define-module (crates-io tw ee twee-v3) #:use-module (crates-io))

(define-public crate-twee-v3-0.1.0 (c (n "twee-v3") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0x7haizd7hl8d4xkv59bv499bgb5aabbq3fvg1rz6kg6xs11q9qf")))

(define-public crate-twee-v3-0.1.1 (c (n "twee-v3") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03i034xlxarm3xjbiz3aa5wm6bvigq5nna7bya7frkfc9iz4qjy4")))

(define-public crate-twee-v3-0.1.2 (c (n "twee-v3") (v "0.1.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d9d8w32hwwvir505dnfjjz6asy12x5mvhapyc56z15fpm3ak70l")))

(define-public crate-twee-v3-0.2.0 (c (n "twee-v3") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hff9bc21giqyi9zvz8zxw12g6b4akc7l757wrkw7i5q1cslxyfb")))

(define-public crate-twee-v3-0.2.1 (c (n "twee-v3") (v "0.2.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10y43kl8h2yxhq8ws4vr46r3wqfs1qa93ykdz1ak2q70f29hcl1j")))

