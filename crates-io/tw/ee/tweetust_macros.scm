(define-module (crates-io tw ee tweetust_macros) #:use-module (crates-io))

(define-public crate-tweetust_macros-0.0.1 (c (n "tweetust_macros") (v "0.0.1") (h "1r8rclsgwwcm5g2666avamnk5258q9cjrghhpn76lkm2r6yb87q1") (y #t)))

(define-public crate-tweetust_macros-0.0.2 (c (n "tweetust_macros") (v "0.0.2") (h "1am25b9l37xnz0cazl6wif8c12hs202d73ird5bzkfz794al8pjc") (y #t)))

