(define-module (crates-io tw ee tweetnacly) #:use-module (crates-io))

(define-public crate-tweetnacly-0.1.3 (c (n "tweetnacly") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)))) (h "0yzwq2p2g2bwfl6llbawwa4k765v8213m9zg0kflmfa0d9j6jd0k")))

(define-public crate-tweetnacly-0.2.0 (c (n "tweetnacly") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "poly1305") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1x36f98cg98in07xbkwigcnqb8jkdkq9in8vdrslrzr46rbqqm12") (f (quote (("fast" "poly1305") ("default" "fast"))))))

