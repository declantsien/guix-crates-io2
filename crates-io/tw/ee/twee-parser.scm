(define-module (crates-io tw ee twee-parser) #:use-module (crates-io))

(define-public crate-twee-parser-0.1.0 (c (n "twee-parser") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "16am5q20cjr7pp5zifdpqyycr6nas4ai472iiyb31qavvi1msza2") (y #t) (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.1 (c (n "twee-parser") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "0xqv6p7p276f61b0gx5m7i3753vgin7xliyv6c103lvs03syxvcn") (y #t) (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.2 (c (n "twee-parser") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "06kcfhwm28y6a1fv977hkpxy5v7fhazslrkwff9843nqp83qnlp9") (y #t) (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.3 (c (n "twee-parser") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "09nn3gzbwzw35k69lawpp9vd0nc6x3aha5jdpc6gr5nxkphcsbqi") (y #t) (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.4 (c (n "twee-parser") (v "0.1.4") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "15dbm05qs171sacg5j3cfzvmqgnrw3jn0506aifv8xz6rbmwy2bg") (y #t) (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.5 (c (n "twee-parser") (v "0.1.5") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "13z525dfgiiw0w37y19f4awr3d7aacm7kdm3dhg7jbza4fpwy421") (s 2) (e (quote (("html" "dep:xmltree"))))))

(define-public crate-twee-parser-0.1.6 (c (n "twee-parser") (v "0.1.6") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1zl2f37limwd4b3gsbfbr3rdb1drljlc2z3x3k5h1ymzjkdbxr18") (s 2) (e (quote (("html" "dep:xmltree"))))))

