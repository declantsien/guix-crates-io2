(define-module (crates-io tw ee tweetnacl) #:use-module (crates-io))

(define-public crate-tweetnacl-0.1.0 (c (n "tweetnacl") (v "0.1.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 2)) (d (n "tweetnacl-sys") (r "0.*") (d #t) (k 0)))) (h "0gd47icvgzi611d6fd0ssbxwbmlxc8cai36fz1vh2ja1n2s6kr58")))

(define-public crate-tweetnacl-0.1.2 (c (n "tweetnacl") (v "0.1.2") (d (list (d (n "rand") (r "0.*") (d #t) (k 2)) (d (n "tweetnacl-sys") (r "0.*") (d #t) (k 0)))) (h "04llqvzfhcqkpfalsd8mxbkzxz5yllrnhqcmvlp8w9c3iw93j8c5")))

(define-public crate-tweetnacl-0.2.0 (c (n "tweetnacl") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tweetnacl-sys") (r "0.*") (d #t) (k 0)))) (h "0y0k1r1y1ci3v3alvdxb84gzffrzyj4ngfp6mz446lii8yxr02rx")))

(define-public crate-tweetnacl-0.2.1 (c (n "tweetnacl") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tweetnacl-sys") (r "0.*") (d #t) (k 0)))) (h "04b230ka8ip8g7cq91nkx1w47p9rsbbcdpga7zmgfjsj63w1f2l0")))

(define-public crate-tweetnacl-0.4.0 (c (n "tweetnacl") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tweetnacl-sys") (r "0.*") (d #t) (k 0)))) (h "03iqa9jsnrw4mmapj6k6z4icjnagh5wgynq69ja5w7b1b1661vsi")))

