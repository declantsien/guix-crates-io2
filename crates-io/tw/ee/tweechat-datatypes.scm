(define-module (crates-io tw ee tweechat-datatypes) #:use-module (crates-io))

(define-public crate-tweechat-datatypes-0.0.1 (c (n "tweechat-datatypes") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vdvsj7jl4x5sr5rdcshwim89mr6m8rbyaqv04dsai409mg2cgkg")))

(define-public crate-tweechat-datatypes-0.0.2 (c (n "tweechat-datatypes") (v "0.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n42xq39rx0zbhgmd1sg6m8cx6na6wlgr7d4hyli28xa3yq1mh86")))

(define-public crate-tweechat-datatypes-0.0.3 (c (n "tweechat-datatypes") (v "0.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z68nik0hcmvi2gkx08ffvjhyh38gz0iz5f9q9gdp8izz6zism3j")))

(define-public crate-tweechat-datatypes-0.0.4 (c (n "tweechat-datatypes") (v "0.0.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pxpiaim4x71853ngga4xdiz7x0zld0wyq4gxbjc6dxqsvw1l0pj")))

(define-public crate-tweechat-datatypes-0.0.5 (c (n "tweechat-datatypes") (v "0.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "110cf6yiz4fglzfi112apmgbjnc8qdypv4vnp28g0r7cbjiab4rm")))

(define-public crate-tweechat-datatypes-0.0.6 (c (n "tweechat-datatypes") (v "0.0.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wq1rml64pfknjx282lblwhqdlcjxq1lk7bqv67z9h3mcmg1aw8s")))

(define-public crate-tweechat-datatypes-0.0.7 (c (n "tweechat-datatypes") (v "0.0.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bdhb5bx1b5jz5pn0a10clz13swci8q18l347i7az7p9niwi00j4")))

