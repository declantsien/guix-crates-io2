(define-module (crates-io tw ee tweetnacl-sys) #:use-module (crates-io))

(define-public crate-tweetnacl-sys-0.1.0 (c (n "tweetnacl-sys") (v "0.1.0") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0phnlablhl9098zv4wf1gl9s9wx55iwf7hl8xacrnjsf7fcfviqn")))

(define-public crate-tweetnacl-sys-0.1.1 (c (n "tweetnacl-sys") (v "0.1.1") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0qrlpbqacgbpmbl97pkjrabh1hhfqpk9mdci07laywvdmiixv42c")))

(define-public crate-tweetnacl-sys-0.1.2 (c (n "tweetnacl-sys") (v "0.1.2") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0z38a2ipz69nfj95qsv2r81phv6439d4dq1q9zhwp0s45v6zfsqz")))

(define-public crate-tweetnacl-sys-0.1.3 (c (n "tweetnacl-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v86m5yprz23bphvlgbf20v8f36n4klk991z256a1mk9800gxc6f")))

(define-public crate-tweetnacl-sys-0.1.4 (c (n "tweetnacl-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kf2sjgs3zgd7vyr4ibfzkcdhb6505b5kmxc299jsd5axxf80c1q")))

(define-public crate-tweetnacl-sys-0.1.5 (c (n "tweetnacl-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0qbbaarc6z2sd27bpbrai56fxfq8w7vi264xw0p8r85013k63wym")))

