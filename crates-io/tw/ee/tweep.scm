(define-module (crates-io tw ee tweep) #:use-module (crates-io))

(define-public crate-tweep-0.1.0 (c (n "tweep") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1drzwr9sdq7g6hslcwksz2ij6wyh1yhyx2xg9rh15gjlmcywd214")))

(define-public crate-tweep-0.2.0 (c (n "tweep") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "110q4bmmr21jximgm2rn6hxbym9gbp0zj81lcghgydw9dvg4ymcm")))

(define-public crate-tweep-0.2.1 (c (n "tweep") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vv2lr5ggbk7p7rz36429lrh6n9czwv1wzh6iyarxa5zymq3y6gv")))

(define-public crate-tweep-0.2.2 (c (n "tweep") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "178829scb9kg0hq8f07al2x5i8f88pgr3laa030wdmhcvhfnca4y")))

(define-public crate-tweep-0.2.3 (c (n "tweep") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h8zyrjmmawgksq9rwrssmcxfjl3g53ps7vi7bd82330jnn4p21y")))

(define-public crate-tweep-0.2.4 (c (n "tweep") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "131l9bg8zhbhjbwbzpnmbcm62gf1zicmvcy34i79gk225cgm3vbk") (f (quote (("warning-names"))))))

(define-public crate-tweep-0.2.5 (c (n "tweep") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1nwfprw4wbjrwlfymp4x34q1y0dhxnq6jqc20ahggxwy4gfhr027") (f (quote (("warning-names"))))))

(define-public crate-tweep-0.3.0 (c (n "tweep") (v "0.3.0") (d (list (d (n "bimap") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "16w649pxyq6gb3w2z5i47y5w7qbgb8jbyw3w4lfr0asfn41x7xjs") (f (quote (("issue-names") ("full-context" "bimap"))))))

