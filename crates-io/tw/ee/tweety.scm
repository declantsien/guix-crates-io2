(define-module (crates-io tw ee tweety) #:use-module (crates-io))

(define-public crate-tweety-0.1.0 (c (n "tweety") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dxlm834zik62mx54l7jf14dvlbzrdxfpasc78jkw67nv25vgy7x")))

(define-public crate-tweety-0.1.1 (c (n "tweety") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cza43s01nhrdm7dpna43fl4f2xyyaan2w82zdrclgpgwic8xixn")))

(define-public crate-tweety-0.1.2 (c (n "tweety") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xi21hpdjdgw1nwphf5jgrqncd7vb78l1rczcf8q40v54vb17fgv")))

(define-public crate-tweety-0.1.3 (c (n "tweety") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d0l4r6vfvvgy60iq33cngs7gsqccvcx9x9n4vhb1ra2kxd55l4p")))

(define-public crate-tweety-0.1.4 (c (n "tweety") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xs3yr6yylh88cx64ra0cfb9bm7yvhw2w653lqfclz18qxhan3zi")))

(define-public crate-tweety-0.1.5 (c (n "tweety") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b6chx47n8kk6yr2mrv22004b6h7ssgxg7dcfvknsl4qpcjibzyl")))

