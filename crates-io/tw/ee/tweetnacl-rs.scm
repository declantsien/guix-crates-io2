(define-module (crates-io tw ee tweetnacl-rs) #:use-module (crates-io))

(define-public crate-tweetnacl-rs-0.1.0 (c (n "tweetnacl-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sodalite") (r "^0.3") (d #t) (k 0)))) (h "1y3za5bj5ih39ccajmbc7s60s4p988azfgakdd1jbvrypcvsn1bh")))

(define-public crate-tweetnacl-rs-0.1.1 (c (n "tweetnacl-rs") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sodalite") (r "^0.3") (d #t) (k 0)))) (h "1lsp0gns92hnm3pskfh90xvl74asyj2j2fi8szij74nfanm8j7gv")))

(define-public crate-tweetnacl-rs-0.1.2 (c (n "tweetnacl-rs") (v "0.1.2") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sodalite") (r "^0.3") (d #t) (k 0)))) (h "0kwl7h8j8bhlbd3kd0mq5vvzz36zhjrglq1zwr3d1msykkvr18rp")))

(define-public crate-tweetnacl-rs-0.1.3 (c (n "tweetnacl-rs") (v "0.1.3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sodalite") (r "^0.3") (d #t) (k 0)))) (h "0zq4kwzrfiw24iy7sng7g3s4h9ma2fzq92n786vqh1shq4kqxx4a")))

