(define-module (crates-io tw ee tweecrypto) #:use-module (crates-io))

(define-public crate-tweecrypto-0.0.0 (c (n "tweecrypto") (v "0.0.0") (h "1xcrl3h16rg1phdbi4wcrlxk8dfp2w7j4qkf5mkn9ndgyp5cmdk4")))

(define-public crate-tweecrypto-0.0.1 (c (n "tweecrypto") (v "0.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "tink-aead") (r "^0.2") (d #t) (k 0)) (d (n "tink-core") (r "^0.2") (d #t) (k 0)))) (h "1yyicznqbpl3v5hk04gilprkch9jjrvg5wabmx119dhsl0krqf2l")))

(define-public crate-tweecrypto-0.0.2 (c (n "tweecrypto") (v "0.0.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tink-aead") (r "^0.2") (d #t) (k 0)) (d (n "tink-core") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0ych2q1mpbbi7yxyl10ijyx4286h7pk3jl4vh294whqd56r1lnhp")))

