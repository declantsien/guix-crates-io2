(define-module (crates-io tw ee tweetr) #:use-module (crates-io))

(define-public crate-tweetr-0.2.0 (c (n "tweetr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "clap") (r "^2.11") (d #t) (k 0)) (d (n "egg-mode") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0gg6bbjarrsd7wjb2kkwpjbbdhy22ly8hz55ilijssxk5x2zbjvm")))

(define-public crate-tweetr-0.2.1 (c (n "tweetr") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "clap") (r "^2.13") (d #t) (k 0)) (d (n "egg-mode") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "02zmal99wxp58hgq32kflyw2z618x86l2jagdz3n80gmyvdmpfpp")))

