(define-module (crates-io tw ee tweet) #:use-module (crates-io))

(define-public crate-tweet-0.1.0 (c (n "tweet") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k7hvja1hv0faipzina93lxighicd38zgwlpix0lgx2y5alsq6v5")))

(define-public crate-tweet-0.2.0 (c (n "tweet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nn7wi9yjfspl7h9nsafs1n8yhgiwsikhcy2gaxr9pvhlx6p0p2c")))

(define-public crate-tweet-0.3.0 (c (n "tweet") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rjw546fgg758kakhvmd5mvb96m3pv155wn5vwlcmvb8x8wxd8jg")))

