(define-module (crates-io tw ee tweetnacl-rs-near) #:use-module (crates-io))

(define-public crate-tweetnacl-rs-near-0.1.3 (c (n "tweetnacl-rs-near") (v "0.1.3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sodalite") (r "^0.3") (d #t) (k 0)))) (h "05fzlnvmzl7pxjxlj9mpfk5vrwcpgcg1iswvp0426s9v9gzxkds6")))

