(define-module (crates-io tw ee tweetter) #:use-module (crates-io))

(define-public crate-tweetter-0.1.0 (c (n "tweetter") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "1215vhq9qnyzdm3vvwz83a87xzv66gk3lgbd6fccpsv721qz33dl")))

(define-public crate-tweetter-1.0.0 (c (n "tweetter") (v "1.0.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "1ymj7v01wxdkpawbrfvn0a1a641mlrm8pbdpl8rwalj1d3arizqr")))

(define-public crate-tweetter-1.0.1 (c (n "tweetter") (v "1.0.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "184c2jjcnmlcs4pnf6sz09a50bc5ksqavfivb3av1gx11xki1c9l")))

(define-public crate-tweetter-1.0.2 (c (n "tweetter") (v "1.0.2") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "1y8q3f1pfdghmgy92lmzq21s4brf46196aydg4qalrryw6am8b7v")))

(define-public crate-tweetter-2.0.0 (c (n "tweetter") (v "2.0.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "0zw2izqlaxdbn2015jx317hzz12hrv89yqh86may9pcc354xjfjl")))

(define-public crate-tweetter-2.3.0 (c (n "tweetter") (v "2.3.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "1fybnwc01dhhn1c9ygvbi3pll5cbgs3gb88jg1kcf4sxav4rixl0")))

(define-public crate-tweetter-2.3.1 (c (n "tweetter") (v "2.3.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "0rb40njjph6qzrhjz5r5qqkbjfxqal3qryjfv6b00h9a6k9qrczq")))

