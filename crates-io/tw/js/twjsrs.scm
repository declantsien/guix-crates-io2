(define-module (crates-io tw js twjsrs) #:use-module (crates-io))

(define-public crate-twjsrs-0.1.0 (c (n "twjsrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "08fwc9qfd9mgv0hnpabmvim8ja7n5h62c0jyv7kxhrncjw7sc00g") (f (quote (("wrap" "parse") ("default" "parse" "wrap")))) (s 2) (e (quote (("parse" "dep:chrono" "dep:thiserror"))))))

