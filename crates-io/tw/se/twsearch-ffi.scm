(define-module (crates-io tw se twsearch-ffi) #:use-module (crates-io))

(define-public crate-twsearch-ffi-0.6.1 (c (n "twsearch-ffi") (v "0.6.1") (d (list (d (n "cubing") (r "^0.7.3") (d #t) (k 2)) (d (n "libloading") (r "^0.8.1") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)) (d (n "twsearch") (r "^0.6.1") (d #t) (k 0)))) (h "0fsfxjpmsjmc66ra7h1hrxav5w2x15jc27j2bprjrpvh9bkc8ah4") (f (quote (("default"))))))

(define-public crate-twsearch-ffi-0.6.2 (c (n "twsearch-ffi") (v "0.6.2") (d (list (d (n "cubing") (r "^0.7.3") (d #t) (k 2)) (d (n "libloading") (r "^0.8.1") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)) (d (n "twsearch") (r "^0.6.1") (d #t) (k 0)))) (h "0a0i68zqz6yw4yz8c3spldjfs8blqdccdls0lxsjwqp7bcxp8ygy") (f (quote (("default"))))))

(define-public crate-twsearch-ffi-0.6.3 (c (n "twsearch-ffi") (v "0.6.3") (d (list (d (n "cubing") (r "^0.7.3") (d #t) (k 2)) (d (n "libloading") (r "^0.8.1") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)) (d (n "twsearch") (r "^0.6.3") (d #t) (k 0)))) (h "0cg3q04vs9ski0i284ciz462hign70ml3vzxspgds30bqarg044y") (f (quote (("default"))))))

(define-public crate-twsearch-ffi-0.6.4 (c (n "twsearch-ffi") (v "0.6.4") (d (list (d (n "cubing") (r "^0.7.3") (d #t) (k 2)) (d (n "libloading") (r "^0.8.1") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)) (d (n "twsearch") (r "^0.6.4") (d #t) (k 0)))) (h "0ycgaq1mxrcjikzw459a8m0spgfcfk5wviclqs3l2km4dszb175l") (f (quote (("default"))))))

(define-public crate-twsearch-ffi-0.6.5 (c (n "twsearch-ffi") (v "0.6.5") (d (list (d (n "cubing") (r "^0.7.3") (d #t) (k 2)) (d (n "libloading") (r "^0.8.1") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)) (d (n "twsearch") (r "^0.6.5") (d #t) (k 0)))) (h "0pfam872hg07dv83xy42q9vr4bcihnbzl86axl25yms5ynb4cwqq") (f (quote (("default"))))))

