(define-module (crates-io tw a_ twa_pack_lib) #:use-module (crates-io))

(define-public crate-twa_pack_lib-0.1.0 (c (n "twa_pack_lib") (v "0.1.0") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "15cjpvgnm624da89ydrpqp10gp49h2s8rak5g7d0nw14cqjrvcqc") (y #t)))

(define-public crate-twa_pack_lib-0.1.1 (c (n "twa_pack_lib") (v "0.1.1") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "1v1df1fn1pf8yqp9widliv4vnw2p6hpv0ncjaxgvplcif64kykhh") (y #t)))

(define-public crate-twa_pack_lib-0.1.2 (c (n "twa_pack_lib") (v "0.1.2") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "09vi59z1n5skl9v537vvb0s3j2qc4fkax8d3f081m4y80msj7jsl") (y #t)))

(define-public crate-twa_pack_lib-0.1.3 (c (n "twa_pack_lib") (v "0.1.3") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "1nql7fqay87zmyy3bgkn0mhsbccwp0zl8a2qzgsvlhypcw5ab8z8") (y #t)))

(define-public crate-twa_pack_lib-0.1.4 (c (n "twa_pack_lib") (v "0.1.4") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "0ww8l0hbdgmm9f8w7fclcag699vn3hrhjxalh9qlhh9k8yz4ikc9") (y #t)))

(define-public crate-twa_pack_lib-0.1.5 (c (n "twa_pack_lib") (v "0.1.5") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "1fiv55psskyrbvm83wlr53ygry2l3prypwka3vlbq93as7ih1bmr") (y #t)))

(define-public crate-twa_pack_lib-0.1.6 (c (n "twa_pack_lib") (v "0.1.6") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "1g8abc2i14wakv3kw2rq70qrl2fwzq9snmkzdvn3g7pbhz1z0fx1") (y #t)))

(define-public crate-twa_pack_lib-0.1.7 (c (n "twa_pack_lib") (v "0.1.7") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "1vqzjx7fdqqb6a0y7vk9x2ix3mw9fss11pr8ipljvbmrf3s2n8ly") (y #t)))

(define-public crate-twa_pack_lib-0.1.8 (c (n "twa_pack_lib") (v "0.1.8") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)))) (h "090fr2q48dhqys3pd7m4d9mz687ji64cg5cwlfsqb4fm1628rpvd") (y #t)))

(define-public crate-twa_pack_lib-0.1.9 (c (n "twa_pack_lib") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1rrhzybfw4313rpwrdgz2z8rq4l3j94f4h6fqsby82d11lp8swdn") (y #t)))

(define-public crate-twa_pack_lib-0.1.10 (c (n "twa_pack_lib") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0qawg7bcx4yj7z6f6vmhbg6fzmhsyha8c5fxlq58hi33b8a6sldx") (y #t)))

(define-public crate-twa_pack_lib-0.1.11 (c (n "twa_pack_lib") (v "0.1.11") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1dvl11v3sm7jgdf66mcamck8508dshl6vklyxcpn1pd3g7j28v64") (y #t)))

(define-public crate-twa_pack_lib-0.1.12 (c (n "twa_pack_lib") (v "0.1.12") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.1") (d #t) (k 0)))) (h "0v9kjy9559ma8jmvb5bk428v092fryrfqw202whc2kmbcpa65f97") (y #t)))

(define-public crate-twa_pack_lib-0.1.13 (c (n "twa_pack_lib") (v "0.1.13") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "cached_file_view") (r "^0.1.2") (d #t) (k 0)))) (h "1d8ysc05h24qmwml70myfiadrggfn27nnigiij69mj0dkf7f42gb") (y #t)))

