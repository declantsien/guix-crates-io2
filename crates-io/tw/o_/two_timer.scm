(define-module (crates-io tw o_ two_timer) #:use-module (crates-io))

(define-public crate-two_timer-0.1.0 (c (n "two_timer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0al8n96gz4h2hn1j0nf9yhlws7z09lk3qbz8iz3gx1wfhwmnzm9h")))

(define-public crate-two_timer-1.0.0 (c (n "two_timer") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d4zn4hn3l3lmmxk4663s0qwafqgb0n1i7i2ax3i78739dwsmay1")))

(define-public crate-two_timer-1.0.1 (c (n "two_timer") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jww08f17d3w0c46lnyngib7jwkmm9rjh1zx3bf27zpmwf60c6xz")))

(define-public crate-two_timer-1.0.2 (c (n "two_timer") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00j9chir42wc5qczjrbvsi0qkqlb6drgr7bvmkpvh6f1372qy6pz")))

(define-public crate-two_timer-1.0.3 (c (n "two_timer") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04r87cjsb8sf9qbikv7wxs9iqm2mb7bv0nszvxis96p27w061iag")))

(define-public crate-two_timer-1.0.4 (c (n "two_timer") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ljmm5zak3cxbkl9aiy60g3sdavd8vgj0rh661mphsq42rmyqbly")))

(define-public crate-two_timer-1.0.5 (c (n "two_timer") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gbfck6046bbn6qcz8x934f7ba3gvq9fx849gg8kgfc8q52kzix9")))

(define-public crate-two_timer-1.0.6 (c (n "two_timer") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xagagpfim9s5mj4gvxdjfrxzdsjwshgsf9px9f0vss3c60wfap4")))

(define-public crate-two_timer-1.0.7 (c (n "two_timer") (v "1.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rx5rwj6pvdkidcxrs27v96gh7sj7skq1f76lr76zk6b9y39ymn2")))

(define-public crate-two_timer-1.0.8 (c (n "two_timer") (v "1.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17263hp96m1ib6bj3p55rzfz8xd230yvscrxq7qbfvg4k0l1f9r9")))

(define-public crate-two_timer-1.2.0 (c (n "two_timer") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "pidgin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05xy7kig0d7zc0b8ljdykka9mc3dhjalnwsa73p8a5vyj48zwn8n")))

(define-public crate-two_timer-1.2.1 (c (n "two_timer") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dlcply1md42cjkm0b469mbffdhz1flh46r9l3jphqq4h01c4cbw")))

(define-public crate-two_timer-1.3.0 (c (n "two_timer") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "053v2d2rxsg2s4xl3s5xzv275l5krmrk5wkx4rn1vqnxi93plgzn") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-1.3.1 (c (n "two_timer") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y1xl53mykfvwqavjgbzklz4mw089yh9paf033m7kyc0dhmcq02c") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-1.3.2 (c (n "two_timer") (v "1.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17yd5l0ydfdyv6iivbsd0yj658nsfvk6d22nznz3ajymdxc3m0wp") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-1.3.3 (c (n "two_timer") (v "1.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0c5d160g4x7qpvyrkm40nllfjxgg66klbprbiiyzacx7x64f4977") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-1.3.4 (c (n "two_timer") (v "1.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05ga45zxacx083sj87cqvbpshh5mn6z2cf393zq5l7lnkb4dgis9") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.0.0 (c (n "two_timer") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rh73hr0g3v9599jd4y3l9j3f157bk22z2c8bhagp5jmh88pwka7") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.1.0 (c (n "two_timer") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02p5d2sr9zdzw50jir98cri8dsh02pjppds0rz734qr226y7zwg7") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.0 (c (n "two_timer") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0n1iiz21ffi104jvfgwq6w73mr4bpjfmyb7pplbb0rqqg04xk6b9") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.1 (c (n "two_timer") (v "2.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18s9143y4ppwdf2ix2vkj5dhd2m8kjvsfshjwy4g1jb91phixvc7") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.2 (c (n "two_timer") (v "2.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v22p9v7zwm4dawd84zchavbci5n1fpqwda6shlwg3xlggw4n4pi") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.3 (c (n "two_timer") (v "2.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yk60v8ybkf3w2hv3xqyinyb2jz6gar2zmaryvk8aq3nf5h2ai8v") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.4 (c (n "two_timer") (v "2.2.4") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock"))) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pan7kf4jzy64sma4dc20rc2q679pzfsq01cn4n5dmkqs7y9mng4") (f (quote (("small_grammar"))))))

(define-public crate-two_timer-2.2.5 (c (n "two_timer") (v "2.2.5") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock"))) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pidgin") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vh4aj8r45dsf16pg8rgqbaxb28yd1m7j5q6b1pch31xymq25qkm") (f (quote (("small_grammar"))))))

