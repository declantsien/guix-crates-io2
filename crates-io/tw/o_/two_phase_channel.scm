(define-module (crates-io tw o_ two_phase_channel) #:use-module (crates-io))

(define-public crate-two_phase_channel-0.1.0 (c (n "two_phase_channel") (v "0.1.0") (h "1iig633yrxjgxc400jbmg1jd76a73nh2ln4nvwpypvyni5bvhpwn")))

(define-public crate-two_phase_channel-0.2.0 (c (n "two_phase_channel") (v "0.2.0") (h "18cf5n7vpvlznkijyrpj2sgvv3aryfapg3bn8vhj701i34469xmf")))

(define-public crate-two_phase_channel-0.2.1 (c (n "two_phase_channel") (v "0.2.1") (h "00g6189hafxhxm4xf4s4fbksxqc2svan54z00fgqx0bnyrg15zkv") (r "1.56.1")))

(define-public crate-two_phase_channel-0.2.2 (c (n "two_phase_channel") (v "0.2.2") (h "1qx8mknrraidh03b1xifh8by37r9li2asbfwz2mz71agh4fsnw5i") (r "1.56.1")))

