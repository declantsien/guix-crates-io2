(define-module (crates-io tw o_ two_three) #:use-module (crates-io))

(define-public crate-two_three-0.0.1 (c (n "two_three") (v "0.0.1") (h "124lysp8qi57sq43zmhmlbbih64fdrrm26hnbaszh2xyrccwcp3f")))

(define-public crate-two_three-0.0.2 (c (n "two_three") (v "0.0.2") (h "0k2yy3vlyfizkw675yhmzfm7xz7hmmh1ch4lxl8n1ygcigv0199j")))

(define-public crate-two_three-0.0.3 (c (n "two_three") (v "0.0.3") (h "0dkyf0qxpksa2a2cx3y5y0sy64kmh2x10mc54ffpm8qxa0ly7iqg")))

(define-public crate-two_three-0.0.4 (c (n "two_three") (v "0.0.4") (h "09sxaz925a4gxjfv4aw2nldjvfcz0azbf8isn50s7vv0hw1a0zi2")))

(define-public crate-two_three-0.0.5 (c (n "two_three") (v "0.0.5") (h "0509sa14dg53qjp6agzhgrv9s5v3bmbsm1kjpy44sqzsh9wf0m8s")))

(define-public crate-two_three-0.0.6 (c (n "two_three") (v "0.0.6") (h "1rj1mkh6cdnp4zhi72wf2sv5gndghm45irysrx8h3pf0h3hda2vw")))

