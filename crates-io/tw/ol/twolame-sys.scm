(define-module (crates-io tw ol twolame-sys) #:use-module (crates-io))

(define-public crate-twolame-sys-0.1.0 (c (n "twolame-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1yhsdnxq35hz51i5g4m76f8fhmwjlhz5c9a3yvqjv9cpqc969crf") (y #t) (l "twolame")))

(define-public crate-twolame-sys-0.1.1 (c (n "twolame-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (k 1)))) (h "0wbw0vasy04zq01n43xjxqd5lqq8j24lspl80gzhxb2ymd7scij6") (l "twolame")))

