(define-module (crates-io tw ta twtar) #:use-module (crates-io))

(define-public crate-twtar-0.1.0 (c (n "twtar") (v "0.1.0") (d (list (d (n "egzreader") (r "^2.0.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^5.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "roaes") (r "^0.1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (f (quote ("backtraces"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.37") (d #t) (k 0)))) (h "1h6q6lgp6bjrjqgpnr0b7faj213c1avcnz7baya9r6gs3fng494j")))

(define-public crate-twtar-0.1.1 (c (n "twtar") (v "0.1.1") (d (list (d (n "egzreader") (r "^2.0.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^5.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "roaes") (r "^0.1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (f (quote ("backtraces"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.37") (d #t) (k 0)))) (h "1lmclil73waj57mxl5kdld0zzcv309phc0q2jx58malka2igaw98")))

