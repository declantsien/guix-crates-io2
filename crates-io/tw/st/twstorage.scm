(define-module (crates-io tw st twstorage) #:use-module (crates-io))

(define-public crate-twstorage-0.1.0 (c (n "twstorage") (v "0.1.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0ga43azdgffc4nvyx25z4ijda4asfgaarjifdn4js9vzw9as6nx1")))

(define-public crate-twstorage-0.1.1 (c (n "twstorage") (v "0.1.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1fspkyyy5frrmbbk5dykx2bplrc52caz0azrb2mpcd9r6jwxhczi")))

