(define-module (crates-io tw ox twox-hash) #:use-module (crates-io))

(define-public crate-twox-hash-0.1.0 (c (n "twox-hash") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dbckxs2f7pxv5wqgyri169f7mzpi6hxzgh95mwndjccyp59s9h5")))

(define-public crate-twox-hash-0.1.1 (c (n "twox-hash") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1l2if8df4899ah3g07s3gr4dgvskiswkzyvgn8bm35p3k3hfkmwy")))

(define-public crate-twox-hash-1.0.0 (c (n "twox-hash") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "04qn72ir0hw690wjz12dnnl830yq87fgzdqgrzd7ingsilwcqzcn") (f (quote (("unstable"))))))

(define-public crate-twox-hash-1.0.1 (c (n "twox-hash") (v "1.0.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "057gbd7af90xwbinbyiiljjwwxqsq76jj4h5a70hj8lnfnlvaw32") (f (quote (("unstable"))))))

(define-public crate-twox-hash-1.1.0 (c (n "twox-hash") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dbldl2wxjaglqcclq5vywax3l78cfhj8xn2righqabsdqh54ls7")))

(define-public crate-twox-hash-1.1.1 (c (n "twox-hash") (v "1.1.1") (d (list (d (n "rand") (r ">= 0.3.10, < 0.6") (d #t) (k 0)))) (h "0l47f7jy7kfsnxcsm61cb45cw16vf1jzb304swpdf3hib9bbx1ag")))

(define-public crate-twox-hash-1.1.2 (c (n "twox-hash") (v "1.1.2") (d (list (d (n "rand") (r ">= 0.3.10, < 0.7") (d #t) (k 0)))) (h "1bvlqhk6f4gd8w26fxa768gig2mh9jngld0yyaxjn4l0jj8d8p2m")))

(define-public crate-twox-hash-1.2.0 (c (n "twox-hash") (v "1.2.0") (d (list (d (n "rand") (r ">= 0.3.10, < 0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k6698ijj4khadvcc1bjpjzbckja83yj1cp0h9h2692ly6liv1q9") (f (quote (("serialize" "serde"))))))

(define-public crate-twox-hash-1.3.0 (c (n "twox-hash") (v "1.3.0") (d (list (d (n "rand") (r ">= 0.3.10, < 0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16pzzbq4j1z99j9nfc1xrf7qf15h4lizl2dpsvf8n091s75cwyvc") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.4.0 (c (n "twox-hash") (v "1.4.0") (d (list (d (n "digest") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r ">= 0.3.10, < 0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nf86vl5qpm3gczi431sb2m9i02q4fb9a9l7p9y21axrcxxb4hgl") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std")))) (y #t)))

(define-public crate-twox-hash-1.4.1 (c (n "twox-hash") (v "1.4.1") (d (list (d (n "digest") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r ">= 0.3.10, < 0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rm9vqjri2y3vca17iakgfjg809lfdwsdijzzgm6827dm5k9jq2i") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std")))) (y #t)))

(define-public crate-twox-hash-1.4.2 (c (n "twox-hash") (v "1.4.2") (d (list (d (n "digest") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r ">= 0.3.10, < 0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00f16clj1rrgpv5pn2yq351j7lj73xqdvv8cjghliz1gan0490z7") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.5.0 (c (n "twox-hash") (v "1.5.0") (d (list (d (n "digest") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r ">= 0.3.10, < 0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mmgs93dahlsqqwy447znljcad6y787gk7lvzxwffp4jaxsmpz9v") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.6.0 (c (n "twox-hash") (v "1.6.0") (d (list (d (n "cfg-if") (r "^0.1") (k 0)) (d (n "digest") (r "^0.8") (o #t) (k 0) (p "digest")) (d (n "digest_0_9") (r "^0.9") (o #t) (k 0) (p "digest")) (d (n "rand") (r ">=0.3.10, <0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (k 0)))) (h "0ndb4pil758kn0av83jjgq8kkfkwc5lhi5ii7fk5yw96h1wapy04") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.6.1 (c (n "twox-hash") (v "1.6.1") (d (list (d (n "cfg-if") (r ">=0.1, <2") (k 0)) (d (n "digest") (r "^0.8") (o #t) (k 0) (p "digest")) (d (n "digest_0_9") (r "^0.9") (o #t) (k 0) (p "digest")) (d (n "rand") (r ">=0.3.10, <0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (k 0)))) (h "13np3gap2p2q9k205qaiq9rmjnlv5v8i18n6ramvvqp29m39nm8z") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.6.2 (c (n "twox-hash") (v "1.6.2") (d (list (d (n "cfg-if") (r ">=0.1, <2") (k 0)) (d (n "digest") (r "^0.8") (o #t) (k 0) (p "digest")) (d (n "digest_0_10") (r "^0.10") (o #t) (k 0) (p "digest")) (d (n "digest_0_9") (r "^0.9") (o #t) (k 0) (p "digest")) (d (n "rand") (r ">=0.3.10, <0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (k 0)))) (h "1866mb05iimdb2vmbn2wc5fifcajmn6dkm5qah1r9zi495p3xrsf") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

(define-public crate-twox-hash-1.6.3 (c (n "twox-hash") (v "1.6.3") (d (list (d (n "cfg-if") (r ">=0.1, <2") (k 0)) (d (n "digest") (r "^0.8") (o #t) (k 0) (p "digest")) (d (n "digest_0_10") (r "^0.10") (o #t) (k 0) (p "digest")) (d (n "digest_0_9") (r "^0.9") (o #t) (k 0) (p "digest")) (d (n "rand") (r ">=0.3.10, <0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (k 0)))) (h "0xgn72j36a270l5ls1jk88n7bmq2dhlfkbhdh5554hbagjsydzlp") (f (quote (("std" "rand") ("serialize" "serde") ("default" "std"))))))

