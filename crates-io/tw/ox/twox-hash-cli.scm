(define-module (crates-io tw ox twox-hash-cli) #:use-module (crates-io))

(define-public crate-twox-hash-cli-1.0.0 (c (n "twox-hash-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1.2") (d #t) (k 0)))) (h "0xkqxi8zfl3bjnna0ykj3a971d5hg8sprgz5xa7bp1cajx8dvp50")))

