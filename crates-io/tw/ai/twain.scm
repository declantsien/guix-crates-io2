(define-module (crates-io tw ai twain) #:use-module (crates-io))

(define-public crate-twain-0.1.0 (c (n "twain") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f40cnmcx061d0aamdh1mlmkwxhcgxbv4l8d5rlxjnsrkwd12afl")))

