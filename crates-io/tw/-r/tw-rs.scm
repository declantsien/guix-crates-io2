(define-module (crates-io tw -r tw-rs) #:use-module (crates-io))

(define-public crate-tw-rs-0.1.0 (c (n "tw-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1rqzry20a1qip52cz5zjyl2ya6ldwqcrvmhwlmd0kgs2iycg9qaf")))

(define-public crate-tw-rs-0.1.12 (c (n "tw-rs") (v "0.1.12") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "17nh9swarjry2i6vf92da966p1d49nizrb735i3xyf865n86n7xi")))

(define-public crate-tw-rs-0.1.13 (c (n "tw-rs") (v "0.1.13") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "17x71j6v9vv84qdrclqrdhzzyf25g37cwlyczdzpzq4plpgspxyz")))

(define-public crate-tw-rs-0.1.14 (c (n "tw-rs") (v "0.1.14") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0w102cv5wkrvb03akz45ga2wcbjblysf592j10xay3mjnxzbiidv")))

(define-public crate-tw-rs-0.1.15 (c (n "tw-rs") (v "0.1.15") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1ajw4pr1qz3khhrq65a8mwarm36r8ja9pmkp8z06hcnayvxfj73f")))

(define-public crate-tw-rs-0.1.16 (c (n "tw-rs") (v "0.1.16") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0vkdawnn27dcm0n4zw5y5684hidwb2ih6a207rl9xn3vcikfagck")))

(define-public crate-tw-rs-0.1.17 (c (n "tw-rs") (v "0.1.17") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "15riqz9dv3gl878flaldfhz5dfvarqdfwgbfrq7759h2qqp4vd44")))

(define-public crate-tw-rs-0.1.18 (c (n "tw-rs") (v "0.1.18") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1llhw7gyv1wvrvzc7dqvb9mrgkha904bjsnnfi3hlp1df8g97b8i")))

(define-public crate-tw-rs-0.1.19 (c (n "tw-rs") (v "0.1.19") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "00j8mc3nhkmbk5jiiz7m7w1m9kljbncm3dv5gnxzzkicbyd3dkf7")))

(define-public crate-tw-rs-0.1.20 (c (n "tw-rs") (v "0.1.20") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1hg3gqd1fzqw838w2xq83m5wvbiq6jqnm6av7ivv872l9b3g369k")))

(define-public crate-tw-rs-0.1.21 (c (n "tw-rs") (v "0.1.21") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0g616pbkl9c5wbw8pq6h3k6qi91pm682dpy4qwida2gdh2avrgk3")))

(define-public crate-tw-rs-0.1.22 (c (n "tw-rs") (v "0.1.22") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1yringxls3hcgpjz5r66qa0xhp4lrvabdrncwjalygzk63n3mwfc")))

(define-public crate-tw-rs-0.1.23 (c (n "tw-rs") (v "0.1.23") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0r22w7vhgskjxax1nrm1llkqmnaf3vjwn6zmghdlg9aswfqaighl")))

(define-public crate-tw-rs-0.1.24 (c (n "tw-rs") (v "0.1.24") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "01zhp9i3bjqxzj52s2jckzs751fiagl4yb5q5y8fillapgk2zpfa")))

(define-public crate-tw-rs-0.1.25 (c (n "tw-rs") (v "0.1.25") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0md55s6zk2wf91nnx8n9kkwk4slp5z0bcj6w3jiawn9lyrkpban7")))

(define-public crate-tw-rs-0.1.26 (c (n "tw-rs") (v "0.1.26") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0mb9zrlfzqzzlg33c9ws1hxj3fk8pl207adngb94i7xk6qbhm10d")))

(define-public crate-tw-rs-0.1.27 (c (n "tw-rs") (v "0.1.27") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0548pqwrx8p53kqfvydxvz0d3p725wi0agz0zlspqlajn83pzqx0")))

(define-public crate-tw-rs-0.1.28 (c (n "tw-rs") (v "0.1.28") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0dkm63ka7yw0xssqx4wflv6s14m6w0dqlc0vkjc6p622vca40as5")))

(define-public crate-tw-rs-0.1.29 (c (n "tw-rs") (v "0.1.29") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "1dr5yrrlj7fkv2s3si3cwdv0naxgrhixgnm933p47ryalx7fr8sn")))

(define-public crate-tw-rs-0.1.30 (c (n "tw-rs") (v "0.1.30") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.23") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0b4zdvi2433i71r9akg07kzyrdlj8hglgy2ns50nklak14hgnrm8")))

(define-public crate-tw-rs-0.1.31 (c (n "tw-rs") (v "0.1.31") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.23") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "09b15704dzhknk0mj72nzf4zj3cbnbflv9jcm89gxv092abhp4hr")))

(define-public crate-tw-rs-0.1.32 (c (n "tw-rs") (v "0.1.32") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.23") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0vhj9pzzc98crw02dd47jqnk1ywxx48xny8h86zlb7x9zykkxqi3")))

(define-public crate-tw-rs-0.1.33 (c (n "tw-rs") (v "0.1.33") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.23") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "0ivz4wn0ymr5q32bj6ihjyz3q3pk4jv7zlrqy6rjv60h02a8wmf1")))

(define-public crate-tw-rs-0.1.34 (c (n "tw-rs") (v "0.1.34") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.24") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "oauth-client-fix") (r "^0.1") (d #t) (k 0)))) (h "05ldnipjrwlnaxiby80xpz01w32kavwlq3jam44z3gqcwb3iskdz")))

