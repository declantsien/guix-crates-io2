(define-module (crates-io tw _u tw_unpack) #:use-module (crates-io))

(define-public crate-tw_unpack-0.1.10 (c (n "tw_unpack") (v "0.1.10") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "tw_pack_lib") (r "^0.1.15") (d #t) (k 0)))) (h "08qzb1jcprkis5fsvzfgdky0xjbrn0g9m1b3mnrks9d0agrsafy9")))

(define-public crate-tw_unpack-0.1.11 (c (n "tw_unpack") (v "0.1.11") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "tw_pack_lib") (r "^0.1.21") (d #t) (k 0)))) (h "0l0bbpclznigcgbdql1w57a77cqky28dxxbdhrmgzxynicz8iml8")))

