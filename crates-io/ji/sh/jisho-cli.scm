(define-module (crates-io ji sh jisho-cli) #:use-module (crates-io))

(define-public crate-jisho-cli-0.1.0 (c (n "jisho-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1h491p5f11cic565jbcbyizlllwl9nbn9pash2akml41yzyspsga")))

(define-public crate-jisho-cli-0.1.1 (c (n "jisho-cli") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0lab4cp7b2szbyvndn1vwv5rb3i0kbgsvjyjcrvx984b2qy94bf4")))

(define-public crate-jisho-cli-0.1.2 (c (n "jisho-cli") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "16ga0wybhi18h00z5z9xrx70ri0h80wsiq1dcaliw2ryz3hr4i1y")))

(define-public crate-jisho-cli-0.1.3 (c (n "jisho-cli") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0brn0bnif1vwrfh3vgy09wkp846110g5khwh2glj3r0s69gfpa3k")))

