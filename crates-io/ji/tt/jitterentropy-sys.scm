(define-module (crates-io ji tt jitterentropy-sys) #:use-module (crates-io))

(define-public crate-jitterentropy-sys-0.1.0 (c (n "jitterentropy-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "14ngh5g5c55w70wckqs56sbs0qzkx5mb5qsxfzlnrbj88lgybhwl")))

