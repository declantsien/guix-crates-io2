(define-module (crates-io ji tt jittr) #:use-module (crates-io))

(define-public crate-jittr-0.1.0 (c (n "jittr") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1wbfidv166fwxwrq2zpicsk5r0ap4hlxm40l1ns37jck2ap4l109") (f (quote (("default" "log"))))))

(define-public crate-jittr-0.1.1 (c (n "jittr") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0g1a9p22ixzk8wnpz17xzkg6s16s25xyg1zz1g0zfnkc93wsdrjz") (f (quote (("default" "log"))))))

(define-public crate-jittr-0.2.0 (c (n "jittr") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0gbj0h17kvskk0x67x8gqshi84ik948pgak9ib27rl2ffp355mas") (f (quote (("default" "log"))))))

