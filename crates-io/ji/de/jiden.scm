(define-module (crates-io ji de jiden) #:use-module (crates-io))

(define-public crate-jiden-0.1.0 (c (n "jiden") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1qz2i3gqb3xryhmn5bavfa1i150a1mizgy4s4n3qa5dbifd2i6lq")))

(define-public crate-jiden-0.1.1 (c (n "jiden") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1jpvv7fvh61vpqlhdb3bk0j1w5wfl19xpvp7207whfin3ppix3ya")))

