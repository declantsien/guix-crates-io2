(define-module (crates-io ji eb jiebars) #:use-module (crates-io))

(define-public crate-jiebars-0.1.0 (c (n "jiebars") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cganbhc7xanm4rslgbcay63vxs96bwqdnlkyy61l2v83nbabi1d")))

