(define-module (crates-io ji eb jieba) #:use-module (crates-io))

(define-public crate-jieba-0.1.0 (c (n "jieba") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)))) (h "012qx0r6wgwfv07v5q1wcsxsinnzl1xdxbfl723hx8zdkrwsjfj2")))

(define-public crate-jieba-0.1.1 (c (n "jieba") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1xrvxrlzlx96qw1cb4390ah55awjqkicaqish60izs41byqlidrg")))

(define-public crate-jieba-0.1.2 (c (n "jieba") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)))) (h "05sg401pc2cyn82qzxw20z2p0q3k4xznps56s27hh27ma1m242vq")))

(define-public crate-jieba-0.1.3 (c (n "jieba") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1dkhafw2afl02nj763bq97p9vh25kkpbvksz31rj5hgjgccgbmgj")))

