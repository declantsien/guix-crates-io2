(define-module (crates-io ji ts jitsi-xmpp-parsers) #:use-module (crates-io))

(define-public crate-jitsi-xmpp-parsers-0.1.0 (c (n "jitsi-xmpp-parsers") (v "0.1.0") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.14") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "016av3yiwl4gpdqwxpzmdsl6ski04iqxvky22mpnp7w0an4g3wkc")))

(define-public crate-jitsi-xmpp-parsers-0.1.1 (c (n "jitsi-xmpp-parsers") (v "0.1.1") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.15") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "1mg7wwsm1grsspyhnyxhvhy3z5cwskjmnhccrsp77ljsyrbqck6s")))

(define-public crate-jitsi-xmpp-parsers-0.1.2 (c (n "jitsi-xmpp-parsers") (v "0.1.2") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.15") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "0lzppkz6njn07rq9lkx7v0izgib29cbxh0cqfra4q709iz6j7hya")))

(define-public crate-jitsi-xmpp-parsers-0.1.3 (c (n "jitsi-xmpp-parsers") (v "0.1.3") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.15") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "0y4ngrbkls7wz91zkijhllg9si0pw9cqpwyn1kvbzpx2snrs0ixq")))

(define-public crate-jitsi-xmpp-parsers-0.1.4 (c (n "jitsi-xmpp-parsers") (v "0.1.4") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.15") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "0h5rnhgp3drlnb06z8jf5nf2spnc89k1xmjmd7fzaqz2wixn8qz4")))

(define-public crate-jitsi-xmpp-parsers-0.2.0 (c (n "jitsi-xmpp-parsers") (v "0.2.0") (d (list (d (n "jid") (r "^0.9") (f (quote ("minidom"))) (k 0)) (d (n "minidom") (r "^0.15") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "0sich0h5705dcy60ijs6v0hvlxhisx0rsrrvr7jg35cv02csapbg")))

