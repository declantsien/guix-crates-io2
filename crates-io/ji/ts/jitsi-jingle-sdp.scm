(define-module (crates-io ji ts jitsi-jingle-sdp) #:use-module (crates-io))

(define-public crate-jitsi-jingle-sdp-0.1.0 (c (n "jitsi-jingle-sdp") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (f (quote ("use_std"))) (k 0)) (d (n "jitsi-xmpp-parsers") (r "^0.1") (k 0)) (d (n "minidom") (r "^0.15") (k 2)) (d (n "pretty_assertions") (r "^1") (f (quote ("std"))) (k 2)) (d (n "sdp") (r "^0.5") (k 0)) (d (n "thiserror") (r "^1") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "114bcjwgxhp6rmr2ync5rgqvhvvpbvqr0xa9rhqg97ck7rl4yl8b")))

(define-public crate-jitsi-jingle-sdp-0.2.0 (c (n "jitsi-jingle-sdp") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (f (quote ("use_std"))) (k 0)) (d (n "jitsi-xmpp-parsers") (r "^0.1") (k 0)) (d (n "minidom") (r "^0.15") (k 2)) (d (n "pretty_assertions") (r "^1") (f (quote ("std"))) (k 2)) (d (n "sdp") (r "^0.5") (k 0)) (d (n "thiserror") (r "^1") (k 0)) (d (n "xmpp-parsers") (r "^0.19") (f (quote ("disable-validation"))) (k 0)))) (h "1bgaj3fcigwam36irl0kdx0gpa58aw8vmg9bi286l6zjhlzfd2n3")))

