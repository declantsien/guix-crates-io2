(define-module (crates-io ji ra jirachi) #:use-module (crates-io))

(define-public crate-jirachi-0.1.0 (c (n "jirachi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0d11idv2zkxyv7qb7cihs17ad8zmhralqbxlwydx3xixhmrhy89g")))

(define-public crate-jirachi-0.1.1 (c (n "jirachi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "17w9qsblsizp0fsm5qdbqwlxgj1p55nax3x0wf4ggvjjz43cyk8d")))

(define-public crate-jirachi-0.1.2 (c (n "jirachi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "041bp2zbwrs1ylk44g7brbwafpjw9xy780760wishc80ibd8x9ax") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.3 (c (n "jirachi") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gi5v7vwdpn9id4cdmlvk2i40fiw67yvhwinkw8bax94mwpmisws") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.4 (c (n "jirachi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xiwr4hwmrhj1v1khskyy0bh6k2kj4yl9kbdrqqr6spfl3rqdr12") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.5 (c (n "jirachi") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0si1br8sp4bq022fvx011b79il0lxpyxw4xbvqpk8jkmndfa4d39") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.6 (c (n "jirachi") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hr2ncxa2pznhqan3ysy8gqkxri16q245p64fmxbsbp2zfbmphd9") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.7 (c (n "jirachi") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hap4f7796vkxh405bjq125fnd6z5zr2401r795z18q6ipy3m0gi") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.8 (c (n "jirachi") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1q6yzq7gk0rsljsdmchb17kin4br8xf73zkmv9gx7hak1cap8dyd") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

(define-public crate-jirachi-0.1.9 (c (n "jirachi") (v "0.1.9") (d (list (d (n "anyhow") (r ">=1.0.31, <2.0.0") (d #t) (k 0)) (d (n "diesel") (r ">=1.4.4, <2.0.0") (f (quote ("postgres" "r2d2"))) (o #t) (d #t) (k 0)) (d (n "dotenv") (r ">=0.15.0, <0.16.0") (o #t) (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)))) (h "1mwvlsmizkbskipinchwqiwvc4gadwgyh43pvq893mf1yxayxj7f") (f (quote (("collision-resistant" "diesel" "dotenv"))))))

