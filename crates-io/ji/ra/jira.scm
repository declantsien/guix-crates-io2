(define-module (crates-io ji ra jira) #:use-module (crates-io))

(define-public crate-jira-0.0.0 (c (n "jira") (v "0.0.0") (h "1c7ssdly8afvzjbklwz2b9vgilg8mxpnnjvfzfv4nm7sah7fkd08")))

(define-public crate-jira-0.0.1 (c (n "jira") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "172fz25wkb1a4bca5zibmdqnhah66mfdgwpd8m837bhvkqrkssw5")))

