(define-module (crates-io ji ra jirard) #:use-module (crates-io))

(define-public crate-jirard-0.1.0 (c (n "jirard") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1q6scsqd5n1ymw1qgz2wwjbf98f4jyab98vl9jlssl3rfm3cw5fy") (y #t)))

(define-public crate-jirard-0.1.1 (c (n "jirard") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1azavf6x6rbbsr2pycxv7ydvcs4qzg470yc1n5cqsavcyiya07yr") (y #t)))

(define-public crate-jirard-0.1.2 (c (n "jirard") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0m463gmw4p7qbnxiwkjwfnkj4azkn8aa6apvlhjinbplba8d00vb")))

