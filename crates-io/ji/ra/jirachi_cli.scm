(define-module (crates-io ji ra jirachi_cli) #:use-module (crates-io))

(define-public crate-jirachi_cli-0.1.0 (c (n "jirachi_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0yg2jfkn8mxaygf1r301rkzmgqw4wjrbzb25v31rbpwsb5crs0kw")))

(define-public crate-jirachi_cli-0.1.1 (c (n "jirachi_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "1jdlpn9fjv24wzvgql39qyj97ws8yd5kf53vbsr2h3azmwdasxx8")))

(define-public crate-jirachi_cli-0.1.2 (c (n "jirachi_cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "164s2gg0ks0a6hg8rn76vs2xr0ljvmcad70jvhc3ich9lvkd07a1")))

