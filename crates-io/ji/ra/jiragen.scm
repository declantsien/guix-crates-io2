(define-module (crates-io ji ra jiragen) #:use-module (crates-io))

(define-public crate-jiragen-0.9.0 (c (n "jiragen") (v "0.9.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rr18rbzndcrx41l406kqcls2wzf7abyk3aq94xf425hig9cqp05")))

(define-public crate-jiragen-0.9.1 (c (n "jiragen") (v "0.9.1") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04dhrpsiqkzw4rgwhyyzkacwn5bif086i2ggjhmfgppkix0gijbf")))

