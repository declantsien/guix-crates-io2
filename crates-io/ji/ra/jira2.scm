(define-module (crates-io ji ra jira2) #:use-module (crates-io))

(define-public crate-jira2-0.0.1 (c (n "jira2") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0hs6d2lns2xa92291442fmspwc2cw5qxv60dankn969v106v98xh")))

(define-public crate-jira2-0.0.2 (c (n "jira2") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11dal64z6ld1jg728mfvh8g43c65707svb3wnvxxmgy70dx91nh4")))

(define-public crate-jira2-0.0.3 (c (n "jira2") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0n020l5b4iy7an8idzjc4ldrzhvhyfwmwlf9z69x28rlnmw1liid")))

