(define-module (crates-io ji ra jirav2) #:use-module (crates-io))

(define-public crate-jirav2-0.0.1 (c (n "jirav2") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0pfg61ganrxx88r5b0dz32aib4fhki4zwgf6c8pvxagh821z7nvr")))

