(define-module (crates-io ji ra jiragen-cli) #:use-module (crates-io))

(define-public crate-jiragen-cli-0.9.1 (c (n "jiragen-cli") (v "0.9.1") (d (list (d (n "clap") (r "^2.32") (f (quote ("color"))) (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "jiragen") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ihcgyx8jr3by69c4lq4lysfyp8phdhzijnxy0kq4h1fg7v6xgzz")))

