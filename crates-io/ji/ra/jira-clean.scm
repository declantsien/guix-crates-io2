(define-module (crates-io ji ra jira-clean) #:use-module (crates-io))

(define-public crate-jira-clean-0.1.0 (c (n "jira-clean") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1jmd12zxkh8glmxmlfqdm2zjg2k2isgqccwnxn0bp73pj87f6j16")))

(define-public crate-jira-clean-0.1.1 (c (n "jira-clean") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1b2j8dsiyr5hip8aidjvskhhlcypjbfs223wp1vqwj6jmrdwvycf")))

(define-public crate-jira-clean-0.1.2 (c (n "jira-clean") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1r1m8b5bxvm2zffw6cw8gyfrvzbl2calxblf1d9g64m0b6j15ahw")))

