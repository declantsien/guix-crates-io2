(define-module (crates-io ji ao jiao-web) #:use-module (crates-io))

(define-public crate-jiao-web-0.2.1 (c (n "jiao-web") (v "0.2.1") (d (list (d (n "jiao") (r "^0.2.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("CanvasRenderingContext2d" "Document" "HtmlCanvasElement" "HtmlElement" "Path2d" "SvgElement" "Window"))) (d #t) (k 0)))) (h "1ma14rijdfpvq81sxgdfdmq2fbjwv0ksg3s7wwdj3ikhvy6vmbh2")))

(define-public crate-jiao-web-0.3.5 (c (n "jiao-web") (v "0.3.5") (d (list (d (n "jiao") (r "^0.3.5") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("CanvasRenderingContext2d" "Document" "HtmlCanvasElement" "HtmlElement" "Path2d" "SvgElement" "Window"))) (d #t) (k 0)))) (h "09bxq2mjljlv69qn9qakxwpfvn7s008nxn47c538c8k3an38kvnq")))

