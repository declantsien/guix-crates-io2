(define-module (crates-io ji ao jiao-qt) #:use-module (crates-io))

(define-public crate-jiao-qt-0.2.1 (c (n "jiao-qt") (v "0.2.1") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "jiao") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 0)))) (h "1xda54wrvmfqxbsx2fiqaf6iv0sj4bdzraavcbsbc0jjhkc2vaw1")))

(define-public crate-jiao-qt-0.3.5 (c (n "jiao-qt") (v "0.3.5") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "jiao") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 0)))) (h "00im88fag43aryn6gjflk4szl5fxprj2marzl0q8gsg691cb1kxm")))

