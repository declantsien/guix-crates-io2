(define-module (crates-io ji ao jiao-shapes) #:use-module (crates-io))

(define-public crate-jiao-shapes-0.2.1 (c (n "jiao-shapes") (v "0.2.1") (d (list (d (n "jiao") (r "^0.2.1") (d #t) (k 0)) (d (n "jiao-cairo") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "jiao-qt") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "jiao-skia") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "jiao-web") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0gyyf8avxdk3a991zvcva38in0qgh1kp36h0903mbfdbbxx74qfk") (f (quote (("web" "jiao-web") ("skia" "jiao-skia") ("qt" "jiao-qt") ("default" "web") ("cairo" "jiao-cairo"))))))

(define-public crate-jiao-shapes-0.3.5 (c (n "jiao-shapes") (v "0.3.5") (d (list (d (n "cairo-rs") (r "^0.17.0") (f (quote ("glib" "png" "svg" "pdf"))) (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "jiao") (r "^0.3.5") (d #t) (k 0)) (d (n "jiao-cairo") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "jiao-qt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "jiao-skia") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "jiao-web") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("HtmlElement"))) (o #t) (d #t) (k 0)))) (h "0yf7y4d29d8yi8xq1k1rqf9ab2kgymnwpakxavkwgikfnlydv31h") (f (quote (("web" "jiao-web" "web-sys") ("skia" "jiao-skia") ("qt" "cpp_core" "jiao-qt" "qt_gui") ("default") ("cairo" "cairo-rs" "jiao-cairo"))))))

