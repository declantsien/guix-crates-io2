(define-module (crates-io ji ao jiao-skia) #:use-module (crates-io))

(define-public crate-jiao-skia-0.2.1 (c (n "jiao-skia") (v "0.2.1") (d (list (d (n "jiao") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skia-safe") (r "^0.58.0") (d #t) (k 0)))) (h "08i65m7i337cirqm5pb3sbxwy92ayppjzb00gd8ira4m7039ichi")))

(define-public crate-jiao-skia-0.3.5 (c (n "jiao-skia") (v "0.3.5") (d (list (d (n "jiao") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skia-safe") (r "^0.58.0") (d #t) (k 0)))) (h "01bawrbgrlypagp11kqa4di70bs2340cifa730rpp2yhv4nwdvki")))

