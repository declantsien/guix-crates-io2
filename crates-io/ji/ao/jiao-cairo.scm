(define-module (crates-io ji ao jiao-cairo) #:use-module (crates-io))

(define-public crate-jiao-cairo-0.2.1 (c (n "jiao-cairo") (v "0.2.1") (d (list (d (n "cairo-rs") (r "^0.16.7") (f (quote ("glib" "png" "svg" "pdf"))) (d #t) (k 0)) (d (n "jiao") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "042zmv1mn38fzsc9ba0vgwnpwajms3zn22d416rjafdldi7xdmxz")))

(define-public crate-jiao-cairo-0.3.5 (c (n "jiao-cairo") (v "0.3.5") (d (list (d (n "cairo-rs") (r "^0.17.0") (f (quote ("glib" "png" "svg" "pdf"))) (d #t) (k 0)) (d (n "jiao") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0xjbdpvivy919avhhyvidrgcz84b3dmgadqilkbh5kvk9v9jj9ig")))

