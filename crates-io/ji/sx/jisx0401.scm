(define-module (crates-io ji sx jisx0401) #:use-module (crates-io))

(define-public crate-jisx0401-0.1.0-alpha.2 (c (n "jisx0401") (v "0.1.0-alpha.2") (h "0cgn9mslcnn66rrlz712sf3f3f145hnshr43kkhii8rbdmw5xk3h")))

(define-public crate-jisx0401-0.1.0-alpha.3 (c (n "jisx0401") (v "0.1.0-alpha.3") (h "0ldryrwr2j37k7bqkdbgi3mb2x8g1ikskfsajh1fqw36v5gy7aih")))

(define-public crate-jisx0401-0.1.0-beta.1 (c (n "jisx0401") (v "0.1.0-beta.1") (h "12n365am1grb4glryhn17qhaiwsmr5bkg62wbdh8m0xhfplfnyxd")))

(define-public crate-jisx0401-0.1.0-beta.2 (c (n "jisx0401") (v "0.1.0-beta.2") (h "10113m8yrxmmv85rrri1m93qrwbm38qfr3lfsmlrw95j2aww6zhy")))

