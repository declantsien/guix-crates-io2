(define-module (crates-io ji tr jitregistry) #:use-module (crates-io))

(define-public crate-jitRegistry-0.0.1 (c (n "jitRegistry") (v "0.0.1") (d (list (d (n "oci-image-spec") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "03kdqin169gfwnbd0yxjk1ax0m0dj4mn14w4l9zivr6bwbr20zdy")))

