(define-module (crates-io ji pp jippigy) #:use-module (crates-io))

(define-public crate-jippigy-0.4.0 (c (n "jippigy") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (f (quote ("crossbeam-deque"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "img-parts") (r "^0.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "turbojpeg") (r "^1.0") (f (quote ("image"))) (d #t) (k 0)))) (h "1jlh7ri9z82a8nl4s7qvg4kvbjm0kp10b2zfsd8v72p89xywzmhi")))

(define-public crate-jippigy-1.0.0 (c (n "jippigy") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.8.4") (f (quote ("crossbeam-deque"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "img-parts") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "turbojpeg") (r "^1.0") (f (quote ("image"))) (d #t) (k 0)))) (h "0plbhsmn3wdbmgfpq6p83zc0kisrqf3jk3qz8q3n1hggplg0nciz")))

(define-public crate-jippigy-1.0.1 (c (n "jippigy") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "image-compare") (r "^0.3.1") (d #t) (k 2)) (d (n "img-parts") (r "^0.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "turbojpeg") (r "^1.0") (f (quote ("image"))) (d #t) (k 0)))) (h "1p18xmz1kj5wl0mgkf9h8zz8vp8nj7pcrbd15c2v47p0sx1aidrw")))

