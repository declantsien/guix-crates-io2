(define-module (crates-io ji qi jiqi-cli) #:use-module (crates-io))

(define-public crate-jiqi-cli-0.1.0 (c (n "jiqi-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (f (quote ("default"))) (d #t) (k 0)) (d (n "jiqi-degit") (r "^0.1.0") (d #t) (k 0)))) (h "18xiy7fhvxvmsj18vqzi4ciydrnacajqq8glwiwdyxlmbyn53xk7")))

(define-public crate-jiqi-cli-0.1.1 (c (n "jiqi-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (f (quote ("default"))) (d #t) (k 0)) (d (n "jiqi-degit") (r "^0.1.0") (d #t) (k 0)))) (h "0g5x6jfw9xmg2zxd6vv59jigql8ia05hpqbfs21dd2b4pczp3r5j")))

(define-public crate-jiqi-cli-0.1.2 (c (n "jiqi-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (f (quote ("default"))) (d #t) (k 0)) (d (n "jiqi-degit") (r "^0.1.1") (d #t) (k 0)))) (h "0rnw13m7fm49g65kc3bxs6mh2y0w23ri0dlcfqqmjpirkkmqx63b")))

