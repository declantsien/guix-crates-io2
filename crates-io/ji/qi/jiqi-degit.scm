(define-module (crates-io ji qi jiqi-degit) #:use-module (crates-io))

(define-public crate-jiqi-degit-0.1.0 (c (n "jiqi-degit") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "177gzkiazdww1njf8rp5vdi8acidw6kq6w2kiqfrv554zkpn1nvn") (y #t)))

(define-public crate-jiqi-degit-0.1.1 (c (n "jiqi-degit") (v "0.1.1") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "loading") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11sd6x96srfdgla0c190v5lhf6y61fn6cvml7zhsv5gynqd1x5sp") (y #t)))

