(define-module (crates-io ji nt jintemplify_plugin_macro) #:use-module (crates-io))

(define-public crate-jintemplify_plugin_macro-0.1.0 (c (n "jintemplify_plugin_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1x1l3j3d0ha344fwz963pazqb9x29g1sb61wrmwscm5z4by4xds2")))

