(define-module (crates-io ji mu jimu-framework) #:use-module (crates-io))

(define-public crate-jimu-framework-0.1.0 (c (n "jimu-framework") (v "0.1.0") (h "1xbadfcdcm5lrwrj01aca8civ5avs4xjy0kmbnsw4mdd8ci227lf")))

(define-public crate-jimu-framework-0.1.1 (c (n "jimu-framework") (v "0.1.1") (h "0adx4lj12na4v0f6v1k7rxwr0pq90wcs1dlf2znl3ysvy3ngbm8w")))

