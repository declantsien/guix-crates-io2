(define-module (crates-io ji nx jinx) #:use-module (crates-io))

(define-public crate-jinx-0.0.0 (c (n "jinx") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)))) (h "0bdi2zfbqsiplgz30cwmmkg08dsaknwlxc58pbvxk2wig85bqnkb") (y #t)))

