(define-module (crates-io ji gg jiggle) #:use-module (crates-io))

(define-public crate-jiggle-0.1.0 (c (n "jiggle") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1wj2f60iqnhsdhnl7vxlv1ak8zqkkq345fvd2sixxpmaiksgl1py")))

