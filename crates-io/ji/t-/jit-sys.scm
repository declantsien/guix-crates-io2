(define-module (crates-io ji t- jit-sys) #:use-module (crates-io))

(define-public crate-jit-sys-0.1.0 (c (n "jit-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1dbj4ib295h4885xkb5ygr5p841zcmhnyl60zsrknxz0zvvqkaab") (y #t)))

(define-public crate-jit-sys-0.1.1 (c (n "jit-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0q5j1d50mgahdbwkjsb61nh8c12dm06a3rrbfhf8lav4lvvxprg2") (f (quote (("docs-rs")))) (y #t)))

(define-public crate-jit-sys-0.1.2 (c (n "jit-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "05zlgw7nyxayj1nz8m5icsv05chjc6qc4wsb1y71iyw53r81fis3") (f (quote (("docs-rs")))) (y #t)))

(define-public crate-jit-sys-0.1.3 (c (n "jit-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "07gygq0yjldama4la9rp28yq9c9bjknygw4izn2r1b464qcqslgr") (f (quote (("docs-rs"))))))

