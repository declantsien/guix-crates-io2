(define-module (crates-io ji t- jit-spirv-impl) #:use-module (crates-io))

(define-public crate-jit-spirv-impl-0.1.0 (c (n "jit-spirv-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0jf07x8bmgsmb1fnricywqql7sjlr87x3ivpmvcc6jl426474wl9") (f (quote (("shaderc") ("naga") ("default" "shaderc"))))))

(define-public crate-jit-spirv-impl-0.1.1 (c (n "jit-spirv-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0368cbp69g6r2jycxwizx5zp9ag1yyxrfbxzg1zahncyqbhwgr71") (f (quote (("shaderc") ("naga") ("default" "shaderc" "naga"))))))

