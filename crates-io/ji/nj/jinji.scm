(define-module (crates-io ji nj jinji) #:use-module (crates-io))

(define-public crate-jinji-0.1.0 (c (n "jinji") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1bswqx15mmfyabjdbr05gv0fl1yvcxppcv6g8n6kppcjpfzkqcks")))

