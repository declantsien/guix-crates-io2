(define-module (crates-io ji ma jimage-sys) #:use-module (crates-io))

(define-public crate-jimage-sys-0.1.0 (c (n "jimage-sys") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "minidl") (r "^0.1") (d #t) (k 0)))) (h "10jrpd8b47awg58rm5581g353m2lqzwgm3nrhwq6r5vcp6bdfryk") (f (quote (("nightly"))))))

