(define-module (crates-io ji ma jimaku) #:use-module (crates-io))

(define-public crate-jimaku-0.1.0 (c (n "jimaku") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "querystring") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1r6m2yxcmd0kx3b14y1rsvq1f6lrgqggadzk16y2s5hjdv5grgpa")))

(define-public crate-jimaku-0.1.1 (c (n "jimaku") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "querystring") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "175iq5z8va9h0yn16rs0krklwm7pj3x9nf6j3aqhrwfw6ch6bvyw")))

(define-public crate-jimaku-0.2.0 (c (n "jimaku") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "querystring") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1bxrqrm4r7pizszmb542rb7754gq6x7l831awsvzsakl9lmnvp2s")))

