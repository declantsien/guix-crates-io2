(define-module (crates-io ji ma jimage) #:use-module (crates-io))

(define-public crate-jimage-0.1.0 (c (n "jimage") (v "0.1.0") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)))) (h "14rndfpaqk5b6pck88fafzhi7bf0jwcj68xdl156iw7wmvpxw40p") (f (quote (("nightly"))))))

(define-public crate-jimage-0.2.0 (c (n "jimage") (v "0.2.0") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "16wl3sgvpz3csf7mvllqgq1nvlmpxhmmhj99c7g3d4zf36pd3fk9") (f (quote (("nightly")))) (y #t)))

(define-public crate-jimage-0.2.1 (c (n "jimage") (v "0.2.1") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0qbhgc543vcl6flfqsw96k2x1pcpnx3w4m1w6y059my5n0sr6dsb") (f (quote (("nightly"))))))

(define-public crate-jimage-0.2.2 (c (n "jimage") (v "0.2.2") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1gw68igppz41hyf93szlx1w0nisqywhir36dsygrzib7zcainkmd") (f (quote (("nightly"))))))

(define-public crate-jimage-0.2.3 (c (n "jimage") (v "0.2.3") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1jzly7kgiif5a57hnfp25kvvs63xncrpzbwld62wqg0x5z9xdhiz") (f (quote (("nightly"))))))

(define-public crate-jimage-0.2.4 (c (n "jimage") (v "0.2.4") (d (list (d (n "jimage-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "19gndpwj5ayfyx6kfqv0xasbr5lngjzvqdnn0gd2l71lscr1hjmc") (f (quote (("nightly"))))))

