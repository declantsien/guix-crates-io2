(define-module (crates-io ji ms jims-rust-cli-test) #:use-module (crates-io))

(define-public crate-jims-rust-cli-test-0.1.2 (c (n "jims-rust-cli-test") (v "0.1.2") (h "0n0xbz2mbfsm726la58r9ql7vlhyis0abk4qax3wv0mzb8pwwrfc")))

(define-public crate-jims-rust-cli-test-0.1.3 (c (n "jims-rust-cli-test") (v "0.1.3") (h "11kxpp1mzz06synvky5zi85rvn3dfxnd9c5sy2dgywi9qa8p1z75")))

