(define-module (crates-io ji ta jitash-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-jitash-bitcoincore-rpc-0.17.0 (c (n "jitash-bitcoincore-rpc") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jitash-jsonrpc") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "026s7c63v5waifkpc3m15b1swv9saip8wnyxiwd6sy2k2j7jdhdg")))

