(define-module (crates-io ji ta jitash-jsonrpc) #:use-module (crates-io))

(define-public crate-jitash-jsonrpc-0.15.0 (c (n "jitash-jsonrpc") (v "0.15.0") (d (list (d (n "base64") (r "^0.21.2") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("json-using-serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "01k2cplnbgkssf7p5a8p2crq6jjkksqnprn011j0ycnvd2vh6f74") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("minreq_http" "base64" "minreq") ("default" "simple_http" "simple_tcp"))))))

