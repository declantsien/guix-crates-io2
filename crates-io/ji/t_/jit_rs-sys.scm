(define-module (crates-io ji t_ jit_rs-sys) #:use-module (crates-io))

(define-public crate-jit_rs-sys-0.1.1 (c (n "jit_rs-sys") (v "0.1.1") (d (list (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)))) (h "0rm2jzrlb75x7iqp2myhfj2kih9jj0xrl6gl9bg530qdlbd24rly")))

(define-public crate-jit_rs-sys-0.1.2 (c (n "jit_rs-sys") (v "0.1.2") (d (list (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)))) (h "0dzdimaj5m2d4vllvd5qqwxj8r302d4c8cdpc2snrvnn8g31qyg1")))

(define-public crate-jit_rs-sys-0.1.3 (c (n "jit_rs-sys") (v "0.1.3") (d (list (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)))) (h "0kikahbv0kzrcpb3yffqrxcjckzy3p273cr6p3z06d7fmnibdnbr")))

