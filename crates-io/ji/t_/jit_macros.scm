(define-module (crates-io ji t_ jit_macros) #:use-module (crates-io))

(define-public crate-jit_macros-0.0.1 (c (n "jit_macros") (v "0.0.1") (d (list (d (n "jit") (r "*") (d #t) (k 0)))) (h "0xgmwnq2n1cd2qm53rkmnrhfh69a48hzs0fjdfnp756bgvcxzcqj")))

(define-public crate-jit_macros-0.0.2 (c (n "jit_macros") (v "0.0.2") (h "1gnxg0gkclcka0hibdi90f6nq3iqjk48y6n3f4bjj94lj8pzrrga")))

(define-public crate-jit_macros-0.0.3 (c (n "jit_macros") (v "0.0.3") (h "0qwlgdgjdsd85ikz3ssdcj54yhgnpwcd94kgcc5mvsimvdvayiwn")))

(define-public crate-jit_macros-0.0.4 (c (n "jit_macros") (v "0.0.4") (h "19nharidwwi6v912fv78vgw805lw644wn874rj8jdqphnh7p3xis")))

(define-public crate-jit_macros-0.0.5 (c (n "jit_macros") (v "0.0.5") (h "02vrl8yhlh7isflc1f7g9v3s750fcss8nrm94m76d2w57gdmml1q")))

(define-public crate-jit_macros-0.0.6 (c (n "jit_macros") (v "0.0.6") (d (list (d (n "matches") (r "*") (d #t) (k 0)))) (h "1sgb439rfgwyg6w9a2yz6hngc5lpnsw4mfv1gf5m6886pyyfgcri")))

(define-public crate-jit_macros-0.0.7 (c (n "jit_macros") (v "0.0.7") (d (list (d (n "matches") (r "*") (d #t) (k 0)))) (h "1fj90z56dfrh2p27wp1q3mxlk80nb4ddcfn0fcm87prs92l2grkq")))

(define-public crate-jit_macros-0.0.8 (c (n "jit_macros") (v "0.0.8") (d (list (d (n "matches") (r "*") (d #t) (k 0)))) (h "0xmwfbqh6k4zx4g1n5vc9hmm7k8dqhs3wq6vdqa073dwavnsc385")))

(define-public crate-jit_macros-0.0.9 (c (n "jit_macros") (v "0.0.9") (d (list (d (n "matches") (r "*") (d #t) (k 0)))) (h "1rh85mriflh564gn9dn2vi2mq5m0j56i3n5k4kk6my6mql6nmd85")))

