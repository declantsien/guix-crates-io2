(define-module (crates-io ji t_ jit_rs) #:use-module (crates-io))

(define-public crate-jit_rs-0.1.0 (c (n "jit_rs") (v "0.1.0") (h "1dr0wl756k9zy2n8d1q4q9pinchy76cmq80mwydqfddc0vkb9ick")))

(define-public crate-jit_rs-0.1.1 (c (n "jit_rs") (v "0.1.1") (d (list (d (n "jit_rs-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0krrkcyffp7lym07xayks85kg85116kbk18yfrvik9p4hsizff6r")))

(define-public crate-jit_rs-0.1.2 (c (n "jit_rs") (v "0.1.2") (d (list (d (n "jit_rs-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1z3x9yb7g9jzxc9m39460fi1yljg4gs029a9njs68cvvi6clsfxf")))

(define-public crate-jit_rs-0.1.3 (c (n "jit_rs") (v "0.1.3") (d (list (d (n "jit_rs-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0bm9l89cyhxjkvx34ba337md0v0kx5lgihwj4abf2jpza9zhykpg")))

