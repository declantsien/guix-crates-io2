(define-module (crates-io ji to jito-relayer-rpc) #:use-module (crates-io))

(define-public crate-jito-relayer-rpc-0.1.0 (c (n "jito-relayer-rpc") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-client") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)))) (h "0d83ydn2b0wah22f29jqylkaln9z144i7jm9l49da5ndcawlv5b6")))

