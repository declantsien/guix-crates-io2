(define-module (crates-io ji to jito-tip-distribution) #:use-module (crates-io))

(define-public crate-jito-tip-distribution-0.1.1-alpha.6 (c (n "jito-tip-distribution") (v "0.1.1-alpha.6") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "jito-programs-vote-state") (r "^0.1.1-alpha.4") (d #t) (k 0)) (d (n "solana-program") (r "~1.10.29") (d #t) (k 0)))) (h "01s2232g09vxqyj219dp8gag4b1ya3l419b8y57kphkldzd84cns") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jito-tip-distribution-0.1.1 (c (n "jito-tip-distribution") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "jito-programs-vote-state") (r "^0.1.1-alpha.4") (d #t) (k 0)) (d (n "solana-program") (r "~1.10.29") (d #t) (k 0)))) (h "1cyl6gfaqi8xy79if4w0njjn4yai28f88dc8di5100snv856cw48") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jito-tip-distribution-0.1.2 (c (n "jito-tip-distribution") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "jito-programs-vote-state") (r "^0.1.3") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "0hf2v783y44274bzmsff2krg5l6wwyh00d2wwxdcd6y9sppnaf0m") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

