(define-module (crates-io ji to jito-relayer-protos) #:use-module (crates-io))

(define-public crate-jito-relayer-protos-0.1.0 (c (n "jito-relayer-protos") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "solana-perf") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1cwbh5qpgc3w1bh2zx7l7d4lqkzncqh239c6m24r12d349lxmjfk")))

