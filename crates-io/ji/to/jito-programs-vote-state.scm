(define-module (crates-io ji to jito-programs-vote-state) #:use-module (crates-io))

(define-public crate-jito-programs-vote-state-0.1.1-alpha.4 (c (n "jito-programs-vote-state") (v "0.1.1-alpha.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "solana-program") (r "^1.10") (d #t) (k 0)))) (h "09l7db93zdsl2b7frpi3h06ldvs7vil3frammjrl1kbl3frfphhs")))

(define-public crate-jito-programs-vote-state-0.1.1 (c (n "jito-programs-vote-state") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "solana-program") (r "^1.10") (d #t) (k 0)))) (h "10cbps4kflkd6pynjxq69nvc848xwgkv2qvll4x4hspbbsmm0nac")))

(define-public crate-jito-programs-vote-state-0.1.2 (c (n "jito-programs-vote-state") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.13") (d #t) (k 0)))) (h "01qp1y8s330m46k1wcxr90jsvgwj95va81n0wklzjk54n10l2645")))

(define-public crate-jito-programs-vote-state-0.1.3 (c (n "jito-programs-vote-state") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "0va425528wkpplln9rddnf0l5lwj297lx3dhxyavrp6sr9avr1s8")))

