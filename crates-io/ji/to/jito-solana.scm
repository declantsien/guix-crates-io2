(define-module (crates-io ji to jito-solana) #:use-module (crates-io))

(define-public crate-jito-solana-0.1.0 (c (n "jito-solana") (v "0.1.0") (h "0dwz9k2c59g8m28bzcj81j3k8m2kqkhp5frqf1zr14p65fvj5jjy")))

(define-public crate-jito-solana-0.1.1 (c (n "jito-solana") (v "0.1.1") (h "1q5xs8l1li6s1lxj13m1kc9wlkykysfi4mikam6f1ywc5k1zfcif")))

