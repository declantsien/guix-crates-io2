(define-module (crates-io ji to jito-rpc) #:use-module (crates-io))

(define-public crate-jito-rpc-0.1.0 (c (n "jito-rpc") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-client") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-metrics") (r "=1.14.13") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.13") (d #t) (k 0)))) (h "05l86vz134wh5w2z5vs4l4ldqsf7i236ylc4kma0qp4768lyggqy")))

