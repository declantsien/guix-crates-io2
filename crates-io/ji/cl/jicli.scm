(define-module (crates-io ji cl jicli) #:use-module (crates-io))

(define-public crate-jicli-0.1.0 (c (n "jicli") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18i4hrb0ga5z40h7gzb1ypwfvz30bn1w8rbh3z4gdsf509hp5dnb")))

(define-public crate-jicli-0.1.1 (c (n "jicli") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09j3jgpdw8aglaxq93akjyay7nhi9piaah3w42i75sfy76g5ksc1")))

(define-public crate-jicli-0.1.2 (c (n "jicli") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1481p51gkpxqmgicvk0rgj0m4cvgk690m2jh3827izgq87gbzv37")))

(define-public crate-jicli-0.1.3 (c (n "jicli") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pp1vq22fdalwgkcy37fxdcmkhs93gkb54iawh99xvpm68bm3m36")))

(define-public crate-jicli-0.1.4 (c (n "jicli") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8fkcyskcilpz37a81jm1wswvr7vlfd3bz8rpb8adb6yhjjwk27")))

(define-public crate-jicli-0.1.5 (c (n "jicli") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ibdlswwd3vdzq7ry8m820vyld584307q3rnqwaahnbsfb1gd2a2")))

