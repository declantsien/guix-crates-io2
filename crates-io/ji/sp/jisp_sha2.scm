(define-module (crates-io ji sp jisp_sha2) #:use-module (crates-io))

(define-public crate-jisp_sha2-0.1.0 (c (n "jisp_sha2") (v "0.1.0") (d (list (d (n "crypto-bigint") (r "^0.5.5") (d #t) (k 0)))) (h "1dldvxlvyibx9021rfwdad64xak0br82ig9c8zrqn7ak01shvj04")))

(define-public crate-jisp_sha2-0.2.0 (c (n "jisp_sha2") (v "0.2.0") (d (list (d (n "crypto-bigint") (r "^0.5.5") (d #t) (k 0)))) (h "1afrvpic35lmfvvlgxxsyp554x6m9mr149axyragamzvaybiny2p")))

(define-public crate-jisp_sha2-0.2.1 (c (n "jisp_sha2") (v "0.2.1") (d (list (d (n "crypto-bigint") (r "^0.5.5") (d #t) (k 0)))) (h "15f3sgz3ms1ibhrqxwxj6rqmw9rny7b642zpfsl5cikabp4351w8")))

(define-public crate-jisp_sha2-0.2.2 (c (n "jisp_sha2") (v "0.2.2") (d (list (d (n "crypto-bigint") (r "^0.5.5") (d #t) (k 0)))) (h "1sangxplqfmf8k8shqmyljl8dqrvdmkl7nycxcw7hbk31xpl7gxg")))

(define-public crate-jisp_sha2-0.2.3 (c (n "jisp_sha2") (v "0.2.3") (d (list (d (n "crypto-bigint") (r "^0.5.5") (d #t) (k 0)))) (h "193qd4kqbh6djnzxk8vr524z2i2vk4pik3xsf6xg17w2mr1v8jhz")))

