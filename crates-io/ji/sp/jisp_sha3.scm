(define-module (crates-io ji sp jisp_sha3) #:use-module (crates-io))

(define-public crate-jisp_sha3-0.1.0 (c (n "jisp_sha3") (v "0.1.0") (h "1c86czmj31ngasp04a837gadjjsk50nh4ddsbrksn76j6j4vbdlb")))

(define-public crate-jisp_sha3-0.1.1 (c (n "jisp_sha3") (v "0.1.1") (h "1y80xnvjr2qgw8xbfismlcdwk5p06rzrm3dlrg4fqyia8kp92627")))

