(define-module (crates-io ji so jisort) #:use-module (crates-io))

(define-public crate-jisort-0.0.4 (c (n "jisort") (v "0.0.4") (d (list (d (n "argh") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "095jy9qr74khy87phhi8yp922f25rj3zma833rf4q590cy56vrpr") (f (quote (("default" "argh"))))))

