(define-module (crates-io ji np jinpyok_input) #:use-module (crates-io))

(define-public crate-jinpyok_input-0.1.0 (c (n "jinpyok_input") (v "0.1.0") (h "1m5qdpg4dh6jbav8nbd5nnxvh9p539i9q52jf0b6nzddc7f8blwg") (y #t)))

(define-public crate-jinpyok_input-0.1.1 (c (n "jinpyok_input") (v "0.1.1") (h "0c01rb0app6pm3bz3bvzdx6a4qcar57y3ha6xx1pig9yxwxnz04k") (y #t)))

(define-public crate-jinpyok_input-0.1.2 (c (n "jinpyok_input") (v "0.1.2") (h "0g9zrsppks892ms1wjxi1qn4wjgcj0djvd59iiawcazam13dciva") (y #t)))

(define-public crate-jinpyok_input-0.1.3 (c (n "jinpyok_input") (v "0.1.3") (h "1j10p62pfs3d5c8g7qalsj6f4vc2rq5hbpwhaizmm5d10107zsbi")))

