(define-module (crates-io ji gu jiguang-certification) #:use-module (crates-io))

(define-public crate-jiguang-certification-0.1.0 (c (n "jiguang-certification") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rsa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0chj2yihjh8w5qpnldzngz7idb44kcpbqkhkgijclp6m3imk6nry")))

(define-public crate-jiguang-certification-0.1.1 (c (n "jiguang-certification") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rsa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05w108c8jbnmzs9fh4c658ir88v85bq9z12jgkxddqd95wcpg77r")))

