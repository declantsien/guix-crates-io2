(define-module (crates-io ji _c ji_cloud_shared) #:use-module (crates-io))

(define-public crate-ji_cloud_shared-0.1.0 (c (n "ji_cloud_shared") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "17csz0y3wmkjhw2fhjd70fx49wzp7kc3wd5bi9kwzl5p4ywh8mzx") (f (quote (("frontend") ("backend"))))))

