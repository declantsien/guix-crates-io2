(define-module (crates-io ji mm jimmy) #:use-module (crates-io))

(define-public crate-jimmy-0.9.0 (c (n "jimmy") (v "0.9.0") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "1vpzd6gg9ficgn69wsn1zqy6a7myzla3wpdzy3rzw6a105ayp0r7")))

(define-public crate-jimmy-0.10.0 (c (n "jimmy") (v "0.10.0") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "0n9q1bl4yzppr634ixf3bf35z96hhqwn9cc7a8waklzja3yyjkyc")))

