(define-module (crates-io ji d- jid-gst-meet) #:use-module (crates-io))

(define-public crate-jid-gst-meet-0.9.2 (c (n "jid-gst-meet") (v "0.9.2") (d (list (d (n "minidom-gst-meet") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dp98l09rj3df5s92qlp03b555zrsgmpinj0m8rids8ha541c31q")))

(define-public crate-jid-gst-meet-0.9.3 (c (n "jid-gst-meet") (v "0.9.3") (d (list (d (n "minidom") (r "^0.13") (o #t) (d #t) (k 0) (p "minidom-gst-meet")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wssyry9dxms4qxz2kmrazycww49mg4wzgbhw1nmwlwda5dr7q0r")))

