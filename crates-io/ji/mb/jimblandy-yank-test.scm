(define-module (crates-io ji mb jimblandy-yank-test) #:use-module (crates-io))

(define-public crate-jimblandy-yank-test-0.1.0 (c (n "jimblandy-yank-test") (v "0.1.0") (h "1mgmsmnla8wl513mi26xwzq0w05crqbhk5rpcwhhqjmsx69q94dx")))

(define-public crate-jimblandy-yank-test-0.1.1 (c (n "jimblandy-yank-test") (v "0.1.1") (h "1gl55xcp0sgjwyd3yv1mdj25rlk92mn626m44bxklzqh2pbyzf9w") (y #t)))

(define-public crate-jimblandy-yank-test-0.1.2 (c (n "jimblandy-yank-test") (v "0.1.2") (h "0kcx71cm8s1kcf4i14fdwp9l4iwy1mi6bhwcf48d666mn5664w7q") (y #t)))

