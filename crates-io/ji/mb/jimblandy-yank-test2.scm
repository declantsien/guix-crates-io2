(define-module (crates-io ji mb jimblandy-yank-test2) #:use-module (crates-io))

(define-public crate-jimblandy-yank-test2-0.1.0 (c (n "jimblandy-yank-test2") (v "0.1.0") (d (list (d (n "jimblandy-yank-test") (r "^0.1.1") (d #t) (k 0)))) (h "1a7bmj8nn1r6jrh643wr73bl5d3dhwk9wn9r86fdlvi6lclvb2p9")))

(define-public crate-jimblandy-yank-test2-0.1.2 (c (n "jimblandy-yank-test2") (v "0.1.2") (d (list (d (n "jimblandy-yank-test") (r "^0.1.2") (d #t) (k 0)))) (h "05zcgij2yncxg0mgnlblh035rq4lx89sswimkd3i2j8qxi6ay9dh")))

