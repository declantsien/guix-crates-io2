(define-module (crates-io ji an jiang_mini_grep) #:use-module (crates-io))

(define-public crate-jiang_mini_grep-0.1.0 (c (n "jiang_mini_grep") (v "0.1.0") (h "0fyra6aiv3ay5w0jsa2c4i0hgkkqzj5x8rif1pc5d2wmxf9lcakh")))

(define-public crate-jiang_mini_grep-0.1.1 (c (n "jiang_mini_grep") (v "0.1.1") (h "0ji65s4rv5bcp3k2il9c68c577idrmyrh7c3bzc19ck10wvw1h79")))

