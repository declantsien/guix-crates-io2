(define-module (crates-io ji a_ jia_pipe_viewer) #:use-module (crates-io))

(define-public crate-jia_pipe_viewer-0.1.2 (c (n "jia_pipe_viewer") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "1rdsc78lc2d3h8aks96z1pxkrnbljhgh8nd5sr2gzn5zjk832sxf")))

(define-public crate-jia_pipe_viewer-0.1.3 (c (n "jia_pipe_viewer") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "0xlasdzda03fllp8c6gwk5735k505vp1iarnm9g2yx5j8nppjxsf")))

(define-public crate-jia_pipe_viewer-0.1.4 (c (n "jia_pipe_viewer") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "0mzxdf76nsmixc0z3bjz9b4c83hqqk9j5sa2hvz9k17q70dp1l14")))

