(define-module (crates-io kk cl kkcloud-framework) #:use-module (crates-io))

(define-public crate-kkcloud-framework-0.0.3 (c (n "kkcloud-framework") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "ruc") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "113qf1zb2a43r265n7v791lgqbyh6fy0xhiz0mf7vffnrqyf0cp1")))

(define-public crate-kkcloud-framework-0.0.4 (c (n "kkcloud-framework") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "ruc") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lzcxyz4j7bq66g1rrcx68cira580xq92d48qrmkp9rbfivsq9w5")))

