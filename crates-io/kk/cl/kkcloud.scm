(define-module (crates-io kk cl kkcloud) #:use-module (crates-io))

(define-public crate-kkcloud-0.0.3 (c (n "kkcloud") (v "0.0.3") (d (list (d (n "kkcloud-client") (r "^0") (d #t) (k 0)) (d (n "kkcloud-server") (r "^0") (d #t) (k 0)))) (h "0w7ayf1mywf022nfhr52ryc2lhmffyyql0nx6b1fyivifq7hzkfr")))

(define-public crate-kkcloud-0.0.4 (c (n "kkcloud") (v "0.0.4") (d (list (d (n "kkcloud-client") (r "^0") (d #t) (k 0)) (d (n "kkcloud-server") (r "^0") (d #t) (k 0)))) (h "1p68fxl6qs1rxfixg6d4kpjia0damc4l3gm9x0h5l6ywpbkdf8yk")))

