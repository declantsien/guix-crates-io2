(define-module (crates-io kk bt kkbt) #:use-module (crates-io))

(define-public crate-kkbt-0.1.0 (c (n "kkbt") (v "0.1.0") (h "13rzw9qfigfpnxrrb5prifvxi460v1ayg97ayna4bs43cqxw0zps") (y #t)))

(define-public crate-kkbt-0.1.1 (c (n "kkbt") (v "0.1.1") (h "0lq5148ix8d6m0w8d0l7yby9fnq6c7wq473qsn5mjrs6p8wx4xq2")))

(define-public crate-kkbt-0.1.2 (c (n "kkbt") (v "0.1.2") (h "1ndx52ndv1ac6lcgidzvgf3p8qm0fy4a1ikm24nzqwdc418fy4f8")))

(define-public crate-kkbt-0.1.3 (c (n "kkbt") (v "0.1.3") (h "0xlpk2s14336ya3k5ahj8wkfwh65jcp2966g44a6zic7svkqha23")))

