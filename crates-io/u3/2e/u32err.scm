(define-module (crates-io u3 #{2e}# u32err) #:use-module (crates-io))

(define-public crate-u32err-0.1.0 (c (n "u32err") (v "0.1.0") (h "1jrx1dazldjxsm7xb2dh7fgnbiqrk264mb6l2hhyxd6r1sz6j5ww")))

(define-public crate-u32err-0.1.1 (c (n "u32err") (v "0.1.1") (h "06ss31i6056yhplw3qcnq6rxz5c4w5p6i6srb3wv3qg52sk8rv96")))

