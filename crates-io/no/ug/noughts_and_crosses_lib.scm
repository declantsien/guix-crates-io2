(define-module (crates-io no ug noughts_and_crosses_lib) #:use-module (crates-io))

(define-public crate-noughts_and_crosses_lib-0.0.1 (c (n "noughts_and_crosses_lib") (v "0.0.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0lhsapl3srx1ybzj2x56qgx64b0vafpj10c878l423vczc5sx2sq")))

(define-public crate-noughts_and_crosses_lib-0.0.2 (c (n "noughts_and_crosses_lib") (v "0.0.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1ikqvinha4l18rkn6w43y6d05h6p760r4r4n4dyhgpy8j0npsn3d")))

(define-public crate-noughts_and_crosses_lib-0.0.3 (c (n "noughts_and_crosses_lib") (v "0.0.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1c7ah74va0shdgmcz9j6s1clah582i1f6mlaf4lwgr2xkbpxi5cl")))

