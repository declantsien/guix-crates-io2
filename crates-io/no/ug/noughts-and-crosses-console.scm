(define-module (crates-io no ug noughts-and-crosses-console) #:use-module (crates-io))

(define-public crate-noughts-and-crosses-console-0.1.0 (c (n "noughts-and-crosses-console") (v "0.1.0") (d (list (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 0)))) (h "0dpip3s82ryvlh0i03d8f94klqs1cs7s5nm1y6h1k2p8bva33gz6")))

(define-public crate-noughts-and-crosses-console-0.1.1 (c (n "noughts-and-crosses-console") (v "0.1.1") (d (list (d (n "dont_disappear") (r "^1") (d #t) (k 0)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 0)))) (h "10abi9ricmg7bjkg34ddpaiqhg476270n9j1kyg3dwfqr8icf1hx")))

