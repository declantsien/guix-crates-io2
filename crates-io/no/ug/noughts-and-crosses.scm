(define-module (crates-io no ug noughts-and-crosses) #:use-module (crates-io))

(define-public crate-noughts-and-crosses-0.0.1 (c (n "noughts-and-crosses") (v "0.0.1") (d (list (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)))) (h "05cg4s0qwwhspzwl1vbg2l1fxi14q8x4bh9vh5056h8ikwkckyb8")))

(define-public crate-noughts-and-crosses-0.0.2 (c (n "noughts-and-crosses") (v "0.0.2") (d (list (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)))) (h "0b23636cc41wcdafw8x9ia6rim9gmwyqs7wdqa7hickaba9669if")))

(define-public crate-noughts-and-crosses-0.0.3 (c (n "noughts-and-crosses") (v "0.0.3") (d (list (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)))) (h "0rwxiq8vb7xc3dprhnagygdk88is1jvsskm9p8m25qhxcqkk2b5r")))

(define-public crate-noughts-and-crosses-0.0.4 (c (n "noughts-and-crosses") (v "0.0.4") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)))) (h "0hn0y0ip665v0nr9552hxyir6gflawifcgh690yv4il14q3nkvbp")))

(define-public crate-noughts-and-crosses-0.0.5 (c (n "noughts-and-crosses") (v "0.0.5") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)) (d (n "shadow-clone") (r "^0.1") (d #t) (k 0)))) (h "171jb4bva6r2snz6zxhrfq386h0g2kwkb40k1j65d08lc0jppxsd")))

(define-public crate-noughts-and-crosses-0.0.6 (c (n "noughts-and-crosses") (v "0.0.6") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)) (d (n "shadow-clone") (r "^0.1") (d #t) (k 0)))) (h "1sjjx8vnj5j203278w8b6085623mp1a7b3wbx5ad6j568wwqcmz7")))

(define-public crate-noughts-and-crosses-0.0.7 (c (n "noughts-and-crosses") (v "0.0.7") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)) (d (n "shadow-clone") (r "^0.1") (d #t) (k 0)))) (h "02fcj8gh473c9nmp1sg9b5yapw88rj0fms1zy38yjgmh1qljzily")))

(define-public crate-noughts-and-crosses-0.0.8 (c (n "noughts-and-crosses") (v "0.0.8") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)) (d (n "shadow-clone") (r "^0.1") (d #t) (k 0)))) (h "148r6c864m7q2vaciha0mg8vgp8spzr1y6rxmw4igqxrnn07mpx1")))

(define-public crate-noughts-and-crosses-0.0.9 (c (n "noughts-and-crosses") (v "0.0.9") (d (list (d (n "gdk-pixbuf") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "pango") (r "^0.6") (d #t) (k 0)) (d (n "shadow-clone") (r "^1") (d #t) (k 0)))) (h "0hmpziyd82hr60403963kxwbyh8fjs7lsgpm41qawkgh69c3islm")))

