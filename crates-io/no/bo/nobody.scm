(define-module (crates-io no bo nobody) #:use-module (crates-io))

(define-public crate-nobody-0.1.0 (c (n "nobody") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "minimo") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0bw5qs3rdc9dx0r39cbrssycix6hi20a6zxmgsxy8ypnkyh9gj1d")))

