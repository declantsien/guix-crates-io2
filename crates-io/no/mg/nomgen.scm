(define-module (crates-io no mg nomgen) #:use-module (crates-io))

(define-public crate-nomgen-0.1.0 (c (n "nomgen") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1azbwb75lq6p5g55a3nlv6hsbabx3xg8rapiv648w8vaxn4xmjz1")))

(define-public crate-nomgen-0.1.1 (c (n "nomgen") (v "0.1.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0qv0x0hxmy3r7whflp8gz6q8nkfsvyx6jgpnqjymz3z0h3sxq1mg")))

