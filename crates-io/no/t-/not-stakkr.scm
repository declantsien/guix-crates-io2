(define-module (crates-io no t- not-stakkr) #:use-module (crates-io))

(define-public crate-not-stakkr-0.1.0 (c (n "not-stakkr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "clap") (r "^2.11") (d #t) (k 0)) (d (n "egg-mode") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0kkkvfrd2lpfwf3pm9f6qbbcg4f4ni7h47ncj1yrdy5j30srb05d")))

(define-public crate-not-stakkr-1.0.0 (c (n "not-stakkr") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "clap") (r "^2.11") (d #t) (k 0)) (d (n "egg-mode") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0ri4p79pppzcym9fcb0hq5x15nzhxlcflvi5z8f4fbdlq5ygc9y9")))

