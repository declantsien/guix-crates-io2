(define-module (crates-io no t- not-notes) #:use-module (crates-io))

(define-public crate-not-notes-0.1.0 (c (n "not-notes") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "066p492qllxfx5gk6h4pjn31xla8cjd0hf63b8ld49qi4pzkh26h")))

