(define-module (crates-io no t- not-so-fast) #:use-module (crates-io))

(define-public crate-not-so-fast-0.1.0 (c (n "not-so-fast") (v "0.1.0") (d (list (d (n "not-so-fast-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "025cnqw4q5d2r7xshcygmsysdsqb82mrxa2k7j6lzfyfabaf6lsg") (f (quote (("derive" "not-so-fast-derive") ("default"))))))

(define-public crate-not-so-fast-0.2.0 (c (n "not-so-fast") (v "0.2.0") (d (list (d (n "not-so-fast-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0c9hq56miv7hs47z2b67l1kpkscnbm6a4794wpn44wbr9j5zcj88") (f (quote (("derive" "not-so-fast-derive") ("default"))))))

