(define-module (crates-io no t- not-so-fast-derive) #:use-module (crates-io))

(define-public crate-not-so-fast-derive-0.1.0 (c (n "not-so-fast-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0xk6qpmaxg5jpf3fvkdhzvrljjp2bk60vwnl3qrpsm6v7xsv6c45")))

