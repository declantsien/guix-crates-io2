(define-module (crates-io no op noop_proc_macro) #:use-module (crates-io))

(define-public crate-noop_proc_macro-0.1.0 (c (n "noop_proc_macro") (v "0.1.0") (h "0wfv666i0am2ph8nyydv6q1bs81q4dq7vnb6zpphlm1jvdp3wiqs")))

(define-public crate-noop_proc_macro-0.2.0 (c (n "noop_proc_macro") (v "0.2.0") (h "0si51sfsbh7h5ghsrdhg41x5ikhsf6pqn4wpzs849p5q5qxfzbdr")))

(define-public crate-noop_proc_macro-0.2.1 (c (n "noop_proc_macro") (v "0.2.1") (h "0in1l0rjxzs4fylb6zad484z1c58jxyzchhc12k0cjrvm0y6zwsz")))

(define-public crate-noop_proc_macro-0.3.0 (c (n "noop_proc_macro") (v "0.3.0") (h "1j2v1c6ric4w9v12h34jghzmngcwmn0hll1ywly4h6lcm4rbnxh6")))

