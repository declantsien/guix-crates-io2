(define-module (crates-io no ct noctilucent) #:use-module (crates-io))

(define-public crate-noctilucent-0.1.0 (c (n "noctilucent") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "numberkit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "voca_rs") (r "^1.14.0") (d #t) (k 0)))) (h "1f3ziqdbkzyd8ar634lrbnnzc6d9hfvcrks1y68498jzwgj3h9ji") (y #t)))

(define-public crate-noctilucent-0.2.0 (c (n "noctilucent") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "numberkit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "voca_rs") (r "^1.14.0") (d #t) (k 0)))) (h "0cnx5121y019pray2a99qp7rwnqd94xbc395jf47i42nxmyf6g3x") (y #t)))

