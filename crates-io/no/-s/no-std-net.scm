(define-module (crates-io no -s no-std-net) #:use-module (crates-io))

(define-public crate-no-std-net-0.1.0 (c (n "no-std-net") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1ms9g2s82v5lk79s7d7cri7qy0ax0xbv1hy3zhy52c890lvkb1vd")))

(define-public crate-no-std-net-0.2.0 (c (n "no-std-net") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "0xlf5b88s5qjn6q20hx66wc74x8xgxbqndakpsyrvqbi00rkqhc2") (f (quote (("i128" "byteorder/i128") ("default" "i128"))))))

(define-public crate-no-std-net-0.2.1 (c (n "no-std-net") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "0piirsnlc27asky7y0am9r25j14c5d35pag2140188jqji2vbnbj") (f (quote (("i128" "byteorder/i128") ("default" "i128"))))))

(define-public crate-no-std-net-0.2.2 (c (n "no-std-net") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "05gdd50bjs4p75qfb16zrf52r5kc2343qbj0dwfnmzn5m03kx8q5") (f (quote (("i128" "byteorder/i128") ("default"))))))

(define-public crate-no-std-net-0.2.3 (c (n "no-std-net") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1mcjc1n716v2mwnr6y1rb880nsrnxmcai5r8xq3y7a20ac5qnsq0") (f (quote (("i128" "byteorder/i128") ("default"))))))

(define-public crate-no-std-net-0.3.0 (c (n "no-std-net") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1snnvbbkaxrr46jgncn01v59vd6hk7prjw3fwvx86rqci7wjzgr0") (f (quote (("i128" "byteorder/i128") ("default"))))))

(define-public crate-no-std-net-0.4.0 (c (n "no-std-net") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0dcz790qry82f5bdn0444gblsddnzwxrrg40f6zfjkmfg1s14y11") (f (quote (("i128") ("default"))))))

(define-public crate-no-std-net-0.5.0 (c (n "no-std-net") (v "0.5.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0lkilh0wc7big3m5lsn9wqiz2xvj21kgmpbc15z92j93n51wxkhv") (f (quote (("i128") ("default"))))))

(define-public crate-no-std-net-0.6.0 (c (n "no-std-net") (v "0.6.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ravflgyh0q2142gjdz9iav5yqci3ga7gbnk4mmfcnqkrq54lya3") (f (quote (("unstable_ip") ("std") ("i128"))))))

