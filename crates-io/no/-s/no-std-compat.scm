(define-module (crates-io no -s no-std-compat) #:use-module (crates-io))

(define-public crate-no-std-compat-0.1.0 (c (n "no-std-compat") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 2)))) (h "034k1ggc9zlrq57ldiasmmw43nmbn9dpanjmlyn5zhq4998l9gkn") (f (quote (("std") ("compat_macros") ("compat_hash") ("alloc"))))))

(define-public crate-no-std-compat-0.1.1 (c (n "no-std-compat") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 2)))) (h "1z52kc99myp380a87ajpw213fiy6lkya1f1q19y2k7zq23kz6nvb") (f (quote (("std") ("compat_macros") ("compat_hash" "hashbrown") ("alloc"))))))

(define-public crate-no-std-compat-0.2.0 (c (n "no-std-compat") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 2)))) (h "1d3n0a9hjjjp4dw974icn5flswcjnzn91n20j92n4kghlw4h49yz") (f (quote (("unstable") ("std") ("compat_macros") ("compat_hash" "hashbrown") ("alloc"))))))

(define-public crate-no-std-compat-0.3.0 (c (n "no-std-compat") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "18mb1fzl5dzys4hvs89kb2xa16lmp635qfcrbj78rzq78ng2mvam") (f (quote (("unstable") ("std") ("compat_sync" "spin") ("compat_macros") ("compat_hash" "hashbrown") ("alloc"))))))

(define-public crate-no-std-compat-0.4.0 (c (n "no-std-compat") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "06shnmx40asyyy4ip5cpcpqlczh7ng8w0gxlgaqq5g1xw06d8j24") (f (quote (("unstable") ("std") ("compat_sync" "spin") ("compat_macros") ("compat_hash" "hashbrown") ("alloc"))))))

(define-public crate-no-std-compat-0.4.1 (c (n "no-std-compat") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 2)) (d (n "spin") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "132vrf710zsdp40yp1z3kgc2ss8pi0z4gmihsz3y7hl4dpd56f5r") (f (quote (("unstable") ("std") ("compat_sync" "spin") ("compat_macros") ("compat_hash" "hashbrown") ("alloc"))))))

