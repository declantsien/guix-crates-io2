(define-module (crates-io no -s no-std-thiserror-impl) #:use-module (crates-io))

(define-public crate-no-std-thiserror-impl-1.0.56 (c (n "no-std-thiserror-impl") (v "1.0.56") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0m7mapqcr4x2rjhfwm58y0w1wfjijv0hm1kvaiqvsxjq1qizgcqd") (f (quote (("std")))) (r "1.56")))

