(define-module (crates-io no -s no-sql) #:use-module (crates-io))

(define-public crate-no-sql-0.0.0 (c (n "no-sql") (v "0.0.0") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("tokio-comp"))) (d #t) (k 0)))) (h "1c6jycvb72md5mdv6jvw8s2bccf2kp01gv1v7n2hhc9rnpchmnsr")))

