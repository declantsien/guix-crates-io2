(define-module (crates-io no -s no-std-async) #:use-module (crates-io))

(define-public crate-no-std-async-1.0.0 (c (n "no-std-async") (v "1.0.0") (d (list (d (n "pin-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0mfwnwxcm89zrhvjb69iy07fbj2n4ppz276wr4icnyqcrzfsbw8z") (y #t)))

(define-public crate-no-std-async-1.1.0 (c (n "no-std-async") (v "1.1.0") (d (list (d (n "pin-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1n4dwjila0kdxzgv3nwdn90y9cjfdpr1k843ykvgql4skmm7rf47") (y #t)))

(define-public crate-no-std-async-1.1.1 (c (n "no-std-async") (v "1.1.1") (d (list (d (n "pin-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0x7pm31rmvq9w6yd7r4b9hw9y28nrasvwvpdk8qi5p102jywx0mr")))

(define-public crate-no-std-async-1.1.2 (c (n "no-std-async") (v "1.1.2") (d (list (d (n "pin-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "13310153y6q63v14h4avmjvk3kaiamc1abrk38bkn60cxz01c8s8")))

