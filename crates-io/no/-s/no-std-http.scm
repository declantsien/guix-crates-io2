(define-module (crates-io no -s no-std-http) #:use-module (crates-io))

(define-public crate-no-std-http-0.1.0 (c (n "no-std-http") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "derive_builder") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "smallvec") (r "^1.3") (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (k 0)))) (h "1jqcyl3ld4kxwhbd7nbhb45kqmfc6855f51d0rz8zkkgiajrp0di")))

(define-public crate-no-std-http-0.1.1 (c (n "no-std-http") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "derive_builder") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "smallvec") (r "^1.3") (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (k 0)))) (h "1nwxhdlc4rid8qz43ap03lj3pa6djxhf0bdq389zvp69c0vqz3hw")))

