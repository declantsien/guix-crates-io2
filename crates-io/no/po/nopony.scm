(define-module (crates-io no po nopony) #:use-module (crates-io))

(define-public crate-nopony-0.0.0 (c (n "nopony") (v "0.0.0") (h "16zrc02xgi2h39chixampm3lmjx5izjb5avj6vvc8kk6074yh5f8") (y #t)))

(define-public crate-nopony-0.0.1 (c (n "nopony") (v "0.0.1") (h "1lmawsn6dc0h6ilhvzsnk4wdirycrpscsmd8g3fgmdvd43arv68p") (y #t)))

