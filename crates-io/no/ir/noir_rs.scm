(define-module (crates-io no ir noir_rs) #:use-module (crates-io))

(define-public crate-noir_rs-0.1.0 (c (n "noir_rs") (v "0.1.0") (h "1if4qya0zkr9fdivzk5am79g71yqgpxh5xqrz0gr4f47yh1lk1hj")))

(define-public crate-noir_rs-0.1.1 (c (n "noir_rs") (v "0.1.1") (h "1b6bfx3bivgx8h5ssbvf910d4il5ikxg9k2fbs7xcaqvymqfpd0k")))

