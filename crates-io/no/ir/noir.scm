(define-module (crates-io no ir noir) #:use-module (crates-io))

(define-public crate-noir-0.1.0 (c (n "noir") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.77") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.2") (d #t) (k 0)) (d (n "difference") (r "^0.4") (d #t) (k 0)) (d (n "httparse") (r "^1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0h4wv1zk9q5s88far7ykp28ajhzp242cb2bhr1qmw433f9c2mdxd") (f (quote (("lint" "clippy"))))))

(define-public crate-noir-0.2.0 (c (n "noir") (v "0.2.0") (d (list (d (n "colored") (r "^1.2") (d #t) (k 0)) (d (n "difference") (r "^0.4") (d #t) (k 0)) (d (n "httparse") (r "^1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0k3dfsh9rgcr2za5n9lf4bjldb31gvkbqrvh0msqj5x83b8n4l39")))

