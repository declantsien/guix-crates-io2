(define-module (crates-io no ir noir_grumpkin) #:use-module (crates-io))

(define-public crate-noir_grumpkin-0.1.0 (c (n "noir_grumpkin") (v "0.1.0") (d (list (d (n "ark-bn254") (r "^0.4.0") (f (quote ("scalar_field" "curve"))) (k 0)) (d (n "ark-ec") (r "^0.4.0") (k 0)) (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)))) (h "0l3kw3ffr05hgnnsm38lyj4ana2q705ph18bfgfc04sbn6j4jzaf") (f (quote (("std" "ark-std/std" "ark-ff/std" "ark-ec/std" "ark-bn254/std") ("default")))) (r "1.66")))

