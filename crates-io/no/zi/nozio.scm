(define-module (crates-io no zi nozio) #:use-module (crates-io))

(define-public crate-nozio-0.1.0 (c (n "nozio") (v "0.1.0") (h "08b5z2h1vhfwlc2km9llvvdqvhpicgqalb6b2i1wjy49rh183sz1") (y #t)))

(define-public crate-nozio-0.1.1 (c (n "nozio") (v "0.1.1") (h "1kr5ax2ckxxn5rbhd2kar8ha64hnrhqzzqq4j3jadxswvk34k22a") (y #t)))

