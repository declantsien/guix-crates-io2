(define-module (crates-io no ub noubeacon) #:use-module (crates-io))

(define-public crate-noubeacon-0.1.0 (c (n "noubeacon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "cpal") (r "^0.11.0") (d #t) (k 0)))) (h "06fank1wmpbvcc0vizq5ax6lvm25fhacdxmy2swnhnd1i3cn3cl2")))

(define-public crate-noubeacon-0.1.1 (c (n "noubeacon") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "cpal") (r "^0.11.0") (d #t) (k 0)))) (h "1acfnkq8m60naca16yilq3m9xarq7da8qs1fx5hqq45pm0rmrqj5")))

