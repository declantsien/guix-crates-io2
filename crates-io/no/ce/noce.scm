(define-module (crates-io no ce noce) #:use-module (crates-io))

(define-public crate-noce-0.1.0 (c (n "noce") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tbot") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "process" "io-util"))) (d #t) (k 0)))) (h "1h7vjp979smylzk82dg43qzklnlhqchkdb6qb4k17mrp89lswsg9")))

(define-public crate-noce-0.1.1 (c (n "noce") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tbot") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "process" "io-util"))) (d #t) (k 0)))) (h "184akp75vzwpg4ibqvydxpxczmfvjzkszr7m9ivmxaf96gw7mmzb")))

(define-public crate-noce-0.1.2 (c (n "noce") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tbot") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "process" "io-util"))) (d #t) (k 0)))) (h "1sblld0zhjca75p2f9444m0r91nd7xsjg8ahr5kr553jsi7hxzjp")))

(define-public crate-noce-0.1.3 (c (n "noce") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tbot") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "process" "io-util"))) (d #t) (k 0)))) (h "10cv7jjr6kxa64ipzzdv098xbn2wjmr6kdhmxyani8gzjq3hlsn6")))

