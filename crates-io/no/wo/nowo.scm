(define-module (crates-io no wo nowo) #:use-module (crates-io))

(define-public crate-nowo-0.0.1 (c (n "nowo") (v "0.0.1") (h "0d9xicfsymn7cmfw6ngmjfbyyb4nvfghvj8wggwv9n680sy9f7hp")))

(define-public crate-nowo-0.0.2 (c (n "nowo") (v "0.0.2") (h "1kxpxbi0fnvb63y56iv95vrdg6j25i75mifmfyajj2wy8x604bp5")))

(define-public crate-nowo-0.0.3 (c (n "nowo") (v "0.0.3") (h "0pxkxxvmaf7fbazajxax8wk9nw4pdzwzp0k15s0f0an7mcd89zl5")))

