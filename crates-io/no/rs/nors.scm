(define-module (crates-io no rs nors) #:use-module (crates-io))

(define-public crate-nors-0.1.0 (c (n "nors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y4cy912ky1gw1v0ylns8ybmv38637jhx5llgsn3mx9y32dx7sff")))

