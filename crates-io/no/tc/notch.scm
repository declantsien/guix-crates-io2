(define-module (crates-io no tc notch) #:use-module (crates-io))

(define-public crate-notch-0.1.0 (c (n "notch") (v "0.1.0") (h "1a2s7i5n9h56vfiqw7l646dib2x1wpi55qdr3qdfj55k1yprwpcm") (y #t)))

(define-public crate-notch-0.1.1 (c (n "notch") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "055ldhswlgs28sinmqwfpw5niy3iq9lidgz16v93ydgwrbqaj3zk")))

(define-public crate-notch-0.1.2 (c (n "notch") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1j6xr42a6yxdli4nsb3rr3rmnh5dgzjsy53l1xwvswq1895vsn0z")))

(define-public crate-notch-0.1.3 (c (n "notch") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "06cj219qyhkh7rj279ml9wh8zw30yhbjbn1kqx3mswdp1cf3zmj6")))

(define-public crate-notch-0.1.4 (c (n "notch") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1bcb7w52qfmpnsk2axnyi8hrqx358zziwacq5c3fv3xrgibj3776")))

(define-public crate-notch-0.1.5 (c (n "notch") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "11zayvx4mchzicxa96hki236n879jqpwkrv33fhr5nn38mys1r0z")))

(define-public crate-notch-0.1.6 (c (n "notch") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1p36h2bny5r9cfibrwdi0dwkxkxdsk0him7qqwhclz4rqm4pwfmz")))

(define-public crate-notch-0.1.7 (c (n "notch") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0glhmmlcws6nybl40ba3x46pb40symw37zz2s6hgw1ff0ccmh2vs")))

(define-public crate-notch-0.1.8 (c (n "notch") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "08lx6g6wacp9al2vabsbfdnkspixrb75lgw3gybbklhk9whqrrpj")))

(define-public crate-notch-0.1.9 (c (n "notch") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1q66q5l5634x2d0k32rdp4019i9xqdbn12vpqa457h3gfms5dpj5")))

(define-public crate-notch-0.1.10 (c (n "notch") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1489n9w1i7lbca0yx9qfsakxpyfx8rjmk8m76nn47f25r3l2vfiz")))

