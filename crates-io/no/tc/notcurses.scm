(define-module (crates-io no tc notcurses) #:use-module (crates-io))

(define-public crate-notcurses-0.9.2 (c (n "notcurses") (v "0.9.2") (h "11sgjqdd4g5q3r980c37rxpghzy1nvn1078g3cdxy0kbxp3aalna")))

(define-public crate-notcurses-0.9.9 (c (n "notcurses") (v "0.9.9") (h "005l98h2i45q1kxa83xpdmfgxc25cszk4fdzam2vj9z503hga9xf")))

(define-public crate-notcurses-1.0.2 (c (n "notcurses") (v "1.0.2") (h "1jj3nqdbgqqilf1qi3llqli2j232bsm9jl7kz2228vfjqvrfm881")))

(define-public crate-notcurses-1.1.0 (c (n "notcurses") (v "1.1.0") (h "1vhcsx59fqzslgzxbmr16qwnfizlcrlcpddsg2zki44im7q5jivg")))

(define-public crate-notcurses-1.1.4 (c (n "notcurses") (v "1.1.4") (h "0z8hbrpf0bgggvld9v04nhbs4cp0xvlbrfaza8z7khsn8yi4qabf")))

(define-public crate-notcurses-1.1.5 (c (n "notcurses") (v "1.1.5") (h "145kgj31h17asz308w1fbp994cw439zvmk85w33hjwykg92jv1zq")))

(define-public crate-notcurses-1.1.7 (c (n "notcurses") (v "1.1.7") (h "0bwqyj0sb48k49q2m3526vwnk8zyc7xsd8cnyja4dml5rj5cb6rz")))

(define-public crate-notcurses-1.1.8 (c (n "notcurses") (v "1.1.8") (h "16vp1ybpdrp35i5sdy3r4s6ccy9y0sbhhskmf3b7lv3jabnndcna")))

(define-public crate-notcurses-1.2.0 (c (n "notcurses") (v "1.2.0") (h "0qpj6kvy7pvrb2v4fzlxmhvahnjjp8c7qcrbqswl9bbsh0n0h8jr")))

(define-public crate-notcurses-1.2.2 (c (n "notcurses") (v "1.2.2") (h "12x51g126w053h4gjfx2c384sx39270ldkv4srzdxqpylrcg924c")))

(define-public crate-notcurses-1.2.3 (c (n "notcurses") (v "1.2.3") (d (list (d (n "libnotcurses-sys") (r "^1.2.3") (d #t) (k 0)))) (h "0jg5my1p8vlwnnv4rcm36598w5ncns5jgin5dzppkij9f6n0yqa9")))

(define-public crate-notcurses-1.2.4 (c (n "notcurses") (v "1.2.4") (d (list (d (n "libnotcurses-sys") (r "^1.2.4") (d #t) (k 0)))) (h "12rm7p6adjm4476gyzzjnrl29ibc2k7xw3gxk72wh39yakhwi0df")))

(define-public crate-notcurses-1.2.5 (c (n "notcurses") (v "1.2.5") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.2.5") (d #t) (k 0)))) (h "1vxh4h5x62k3cx84akrqvirwn8nrkxa8arvn7j1432mhs256s9hh")))

(define-public crate-notcurses-1.3.2 (c (n "notcurses") (v "1.3.2") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.3.2") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "0r2rlr0x52cyb52m99gk7fg718gli2fmkx3sglxdlsrcrsf456hj")))

(define-public crate-notcurses-1.3.3 (c (n "notcurses") (v "1.3.3") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.3.3") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "1pfv6ai3hr625drm1vcadsn7m2m8pgljwqcxb8b5jiz9z7rf35n3")))

(define-public crate-notcurses-1.3.4 (c (n "notcurses") (v "1.3.4") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.3.4") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "0c8dm6xc9303dja6jjp5c339qksa3yswi17k9kjnv5kbf8svyp72")))

(define-public crate-notcurses-1.4.0 (c (n "notcurses") (v "1.4.0") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "0p4mybwg2zaajlvbmj4b2ckpy2657jwjqmkzyz6xqv531aq34yv1")))

(define-public crate-notcurses-1.4.1 (c (n "notcurses") (v "1.4.1") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "0ndhq1n0qafg6s0wlwrgyjgh9v1lcb3p82v4psd4hdvw2nc2pn60")))

(define-public crate-notcurses-1.4.2 (c (n "notcurses") (v "1.4.2") (d (list (d (n "all_asserts") (r ">= 1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.4.2") (d #t) (k 0)) (d (n "serial_test") (r ">= 0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">= 0.4.0") (d #t) (k 2)))) (h "1maj4krn8jczyy1r2ayaz61w3vd8k3bhlhbr755a0c2zf3wcbr6l")))

(define-public crate-notcurses-1.4.4 (c (n "notcurses") (v "1.4.4") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.4.4") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "0mb9mbhkf1r18q6lpvxippfviy4bv65875qbs5xbjlhx5sf1hqgs")))

(define-public crate-notcurses-1.5.1 (c (n "notcurses") (v "1.5.1") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.5.1") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "1sf86kavgxi5zj3gsppf1ii62422m5kbjii8rbmrf4inwsx1fb0y")))

(define-public crate-notcurses-1.5.2 (c (n "notcurses") (v "1.5.2") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.5.2") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "1nahvgnkk5cnr60pvp8n6cnqcmbbyh5mx8rqmf4h2dhlgkajzh69")))

(define-public crate-notcurses-1.5.3 (c (n "notcurses") (v "1.5.3") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.5.3") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "04gq06a5afg96w8yny9ah48bk3v22iranj5rym0bpbpdsidnmzrj")))

(define-public crate-notcurses-1.6.0 (c (n "notcurses") (v "1.6.0") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.0") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "1m5s4wi17pr8ili1lj2h07xx47wv9iqzxiqkw3zb9ldr1y6kgrrl")))

(define-public crate-notcurses-1.6.2 (c (n "notcurses") (v "1.6.2") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.2") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "18zv3bdzznxz34540nrdi76fm4mzcayba3xrasrx440h4ggs45kw")))

(define-public crate-notcurses-1.6.6 (c (n "notcurses") (v "1.6.6") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.6") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "09sq4v36yipffmhk0999p2l4v20gc7afr5wcg0x9p3by2633j99l")))

(define-public crate-notcurses-1.6.7 (c (n "notcurses") (v "1.6.7") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.7") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "1j05r4y21i13hxsk52yp9xp1m6672n6vas58kaidd9xmpz8a9gbm")))

(define-public crate-notcurses-1.6.8 (c (n "notcurses") (v "1.6.8") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.8") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "0ra9nwlj7xswk2cirw822c52qjh47lzrqklqqk9hpjwb1xr9a4vv")))

(define-public crate-notcurses-1.6.9 (c (n "notcurses") (v "1.6.9") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.9") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "014ppkcm1pqjvm5fps6xjcr1lgr5vdpwlplmdb84qlf9zzw54gmk")))

(define-public crate-notcurses-1.6.10 (c (n "notcurses") (v "1.6.10") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.10") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "0b6rk4q2araw2km88sds24nfr4y1kqd1waam8c5dp3821hf4zmb4")))

(define-public crate-notcurses-1.6.11 (c (n "notcurses") (v "1.6.11") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libc-stdhandle") (r ">=0.1.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.11") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "09wjbm35lr97h8ik023v2sk5szqdsjg8754vyp9g85kf8h57212g")))

(define-public crate-notcurses-2.0.0-alpha.1 (c (n "notcurses") (v "2.0.0-alpha.1") (d (list (d (n "enumflags2") (r "^0.7.0-preview1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^1.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "strum") (r "^0.19") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0hzfxa0p0bl05hn38ij1jjxkfnjvby9kw9wpv4ygbl5ncsns7agh") (y #t)))

(define-public crate-notcurses-2.0.0 (c (n "notcurses") (v "2.0.0") (d (list (d (n "libnotcurses-sys") (r "^2") (d #t) (k 0)))) (h "1m2qgl0yjljc3f40ci0dxpa4bkb8vnhzamafsgcjpnkmr454zycj")))

(define-public crate-notcurses-3.0.0 (c (n "notcurses") (v "3.0.0") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13lnd7b029aslxqydwn1ac9g5h2db2xp2lvrhzfnds9g17dj23kh") (y #t) (r "1.58.1")))

(define-public crate-notcurses-3.0.1-alpha0 (c (n "notcurses") (v "3.0.1-alpha0") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jrb31j03jj5qi77pyjxn4vvg7zqpgpizx1j1q58pgy7g48zk1xd") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (y #t) (r "1.58.1")))

(define-public crate-notcurses-3.0.1 (c (n "notcurses") (v "3.0.1") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "114hqfswmpq9hgbzw72i3608n3xcqlfd3dkhpmysl51cy0hxsxm9") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.58.1")))

(define-public crate-notcurses-3.0.2 (c (n "notcurses") (v "3.0.2") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vvy845ydgh3fgdadhyrllgp117k2rsgyfs7h8agjzwwsvg2k8d2") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.58.1")))

(define-public crate-notcurses-3.0.3 (c (n "notcurses") (v "3.0.3") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qxppldx213z82g6xdy9ylm4sg5vyx049y6gvkgbqb2wy824ig0q") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.58.1")))

(define-public crate-notcurses-3.1.0 (c (n "notcurses") (v "3.1.0") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0nm53lm5qdza4a56rpch0qrk15qrgg528z3cy4a6svmd5zzhxb6k") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.64.0")))

(define-public crate-notcurses-3.2.1 (c (n "notcurses") (v "3.2.1") (d (list (d (n "az") (r "^1.2.1") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "0z6mn7v5kxp1bl13rm2xr6dhp4a5hhcqpmvay1xx6dyb6c40isz8") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings") ("default" "vendored")))) (r "1.64.0")))

(define-public crate-notcurses-3.2.2 (c (n "notcurses") (v "3.2.2") (d (list (d (n "az") (r "^1.2.1") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.7.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "02rh3mjvri0d349icy6zm722p87wq4cz43072v09wbpnh1q1yg0k") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.64.0")))

(define-public crate-notcurses-3.2.3 (c (n "notcurses") (v "3.2.3") (d (list (d (n "az") (r "^1.2.1") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.7.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "1xhbs4vzwsbnnm2v0ibf2cldciqv2ajn6bg8kp87fd3yz7b8jqgy") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings")))) (r "1.64.0")))

(define-public crate-notcurses-3.3.0 (c (n "notcurses") (v "3.3.0") (d (list (d (n "az") (r "^1.2.1") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.9.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "1fh7mkh8rsyyqww81a74vn6waskbyyar02mb93q7gnvl7j2fs632") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings") ("default")))) (r "1.64.0")))

(define-public crate-notcurses-3.4.0 (c (n "notcurses") (v "3.4.0") (d (list (d (n "cuadra") (r "^0.3.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.9.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "04xk70aj0i1ahlvfslvkp38idby4gpcjg3j9hhybpydh7bk6qwkl") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings") ("default")))) (r "1.64.0")))

(define-public crate-notcurses-3.4.1 (c (n "notcurses") (v "3.4.1") (d (list (d (n "cuadra") (r "^0.3.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.9.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "1zm38c6s36hy8sbd2j5hdixvhxncv5p31vy6a6xnnp6ap3kw9hch") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings") ("default")))) (r "1.64.0")))

(define-public crate-notcurses-3.5.0 (c (n "notcurses") (v "3.5.0") (d (list (d (n "cuadra") (r "^0.3.0") (d #t) (k 0)) (d (n "libnotcurses-sys") (r "^3.10.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb") (r "^0.8.35") (k 0)))) (h "013yyf28x9x01p43qf5fc8vnzp5k5gpz04l5p2d7zrvwmnlpakmz") (f (quote (("vendored" "libnotcurses-sys/use_vendored_bindings") ("nightly_docs" "vendored") ("default")))) (r "1.65.0")))

