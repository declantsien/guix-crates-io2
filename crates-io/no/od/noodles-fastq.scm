(define-module (crates-io no od noodles-fastq) #:use-module (crates-io))

(define-public crate-noodles-fastq-0.1.0 (c (n "noodles-fastq") (v "0.1.0") (h "0igkxa89l4pc4adskjrq94yi73xy534zjpv3da10f3hjccdjmbs8")))

(define-public crate-noodles-fastq-0.1.1 (c (n "noodles-fastq") (v "0.1.1") (h "1gxy73r7ijxadq8pmzqclq8jv7m37isccl4gj4ww7cffmrmyr4d3")))

(define-public crate-noodles-fastq-0.2.0 (c (n "noodles-fastq") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0salc0b9rk65v657vph5p6rdh60h5x2718vjisp78b02ahdvks0n") (f (quote (("async" "futures" "tokio"))))))

(define-public crate-noodles-fastq-0.3.0 (c (n "noodles-fastq") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07128sns31h8v25b99xkfa208cv6wqa82vj49p501a9mb94lbbmx") (f (quote (("async" "futures" "tokio"))))))

(define-public crate-noodles-fastq-0.4.0 (c (n "noodles-fastq") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ssi5zlkjfvhk068n7lv552k213w0cv0cfba2bryg0nwnlmwplwj") (f (quote (("async" "futures" "tokio"))))))

(define-public crate-noodles-fastq-0.5.0 (c (n "noodles-fastq") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1w62aqzf5m5ix2hc696cblk1chmrdii6w4nzxzki826j0s7cwj3y") (f (quote (("async" "futures" "tokio")))) (r "1.56.0")))

(define-public crate-noodles-fastq-0.5.1 (c (n "noodles-fastq") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xmcmf6c5xvni169f6wi5qa3s29q0vaqbwb0z0j4y8mccddqk5g6") (f (quote (("async" "futures" "tokio")))) (r "1.56.0")))

(define-public crate-noodles-fastq-0.6.0 (c (n "noodles-fastq") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1yzxqdk1ha5m3jn164h9kc1x440lpshj8mphkpm1823wmdjx1ir7") (f (quote (("async" "futures" "tokio")))) (r "1.64.0")))

(define-public crate-noodles-fastq-0.7.0 (c (n "noodles-fastq") (v "0.7.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cw88gcw29xbqyccjpilfmjd8vidz1ka4pdgzi49qnzw96l832qk") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.64.0")))

(define-public crate-noodles-fastq-0.7.1 (c (n "noodles-fastq") (v "0.7.1") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1s82ivfndm0rgpbzdiavl61yz194iv1akpgnv8xjiknz09271kqa") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.64.0")))

(define-public crate-noodles-fastq-0.8.0 (c (n "noodles-fastq") (v "0.8.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0h96mlg72fb4wcak920g7zvpb33j0nm4fsk7hsdf78fisa338xp1") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.64.0")))

(define-public crate-noodles-fastq-0.9.0 (c (n "noodles-fastq") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vhxm6i66g0811ivvb290kp876jkhja1lmg492zm927ppj74nqvn") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.64.0")))

(define-public crate-noodles-fastq-0.10.0 (c (n "noodles-fastq") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "io-std" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17xbb5fqq3bvzw0wqb9w57aa4zfjf97q1rha9r2j77rivh2r0m83") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.70.0")))

