(define-module (crates-io no od noodles-bed) #:use-module (crates-io))

(define-public crate-noodles-bed-0.1.0 (c (n "noodles-bed") (v "0.1.0") (h "1f6h8f9p6dhyla9hp67km382zxwaivyblf9yhgizkmk09xjmf5pc") (r "1.56.0")))

(define-public crate-noodles-bed-0.2.0 (c (n "noodles-bed") (v "0.2.0") (d (list (d (n "noodles-core") (r "^0.6.0") (d #t) (k 0)))) (h "0d1w9mmfhr30qz7g0n0n3zznj37cijcrr16p2vr8g2k9mfa161jj") (r "1.56.0")))

(define-public crate-noodles-bed-0.3.0 (c (n "noodles-bed") (v "0.3.0") (d (list (d (n "noodles-core") (r "^0.7.0") (d #t) (k 0)))) (h "1hd99f1s0pwrmgzg3cxra036adp3f1j150bfasqwn4pc9h6bc0kf") (r "1.56.0")))

(define-public crate-noodles-bed-0.4.0 (c (n "noodles-bed") (v "0.4.0") (d (list (d (n "noodles-core") (r "^0.8.0") (d #t) (k 0)))) (h "1db22y1p1x1sx8swxdms1pba26j39phc8bad6vchcj0rx3zvfs3l") (r "1.57.0")))

(define-public crate-noodles-bed-0.5.0 (c (n "noodles-bed") (v "0.5.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)))) (h "0v0yqvkn3lqhjkp1hjc214nibmcgas6ymhl6rp5fbdm46py8hcqs") (r "1.57.0")))

(define-public crate-noodles-bed-0.6.0 (c (n "noodles-bed") (v "0.6.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)))) (h "131mz7zdljrx6zhml8kaq9ilzyycv21w35lirsql77gw13yczh1h") (r "1.57.0")))

(define-public crate-noodles-bed-0.7.0 (c (n "noodles-bed") (v "0.7.0") (d (list (d (n "noodles-core") (r "^0.10.0") (d #t) (k 0)))) (h "1ppj3625lsjvq85xy41n7gfxmpwr9390bifz9pf5fvx9xb9x66sw") (r "1.64.0")))

(define-public crate-noodles-bed-0.8.0 (c (n "noodles-bed") (v "0.8.0") (d (list (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)))) (h "1h28gxci0n5hp64l8lwzpxqsna7d0xmy93cgm0s4cdmm6gwnaksf") (r "1.64.0")))

(define-public crate-noodles-bed-0.9.0 (c (n "noodles-bed") (v "0.9.0") (d (list (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)))) (h "13q0zx20njjr3j2df7kyp9dlqk473rgk70acynwj5aksyzsf53mk") (r "1.64.0")))

(define-public crate-noodles-bed-0.10.0 (c (n "noodles-bed") (v "0.10.0") (d (list (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)))) (h "0ji5mw804gprr163h92fijhbkk70hg4mn7d9glq29iclgc5iifny") (r "1.64.0")))

(define-public crate-noodles-bed-0.11.0 (c (n "noodles-bed") (v "0.11.0") (d (list (d (n "noodles-core") (r "^0.13.0") (d #t) (k 0)))) (h "0raz8pawkrq2d60x58mal1k41j0wlkgacin0cyf9r94nx47jb6zg") (r "1.70.0")))

(define-public crate-noodles-bed-0.12.0 (c (n "noodles-bed") (v "0.12.0") (d (list (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)))) (h "09xd8jzyw0k48niv0y7yq3hr0mi0w4p1l7bpb1nglgyxwcagy5n1") (r "1.70.0")))

(define-public crate-noodles-bed-0.13.0 (c (n "noodles-bed") (v "0.13.0") (d (list (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)))) (h "0z0qkcjnwr0w3svypxrlqp6g5baaanlgpfnr86i4rs7wwlpbpkap") (r "1.70.0")))

