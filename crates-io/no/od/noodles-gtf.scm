(define-module (crates-io no od noodles-gtf) #:use-module (crates-io))

(define-public crate-noodles-gtf-0.1.0 (c (n "noodles-gtf") (v "0.1.0") (h "117an28mnm7sz37vvcksnhvfrby6z389ymif7xqa5pqm8jcc0qjc")))

(define-public crate-noodles-gtf-0.2.0 (c (n "noodles-gtf") (v "0.2.0") (h "0ibddnl1bl6vxybpsf3cffds44yvj8032as8ja8v35z802w7wnfk") (r "1.56.0")))

(define-public crate-noodles-gtf-0.3.0 (c (n "noodles-gtf") (v "0.3.0") (d (list (d (n "noodles-core") (r "^0.6.0") (d #t) (k 0)))) (h "0mcnrqfpyxgiqa62gcid4yjgzqg4ypf8s78yk424axsj5nh15x6f") (r "1.56.0")))

(define-public crate-noodles-gtf-0.3.1 (c (n "noodles-gtf") (v "0.3.1") (d (list (d (n "noodles-core") (r "^0.7.0") (d #t) (k 0)))) (h "1p7llv4qwvlf02ps3ngjfmb03la3328nwmg643v7kps7k6fzaa6h") (r "1.56.0")))

(define-public crate-noodles-gtf-0.4.0 (c (n "noodles-gtf") (v "0.4.0") (d (list (d (n "noodles-core") (r "^0.8.0") (d #t) (k 0)))) (h "06pndqvdwssnh8j99xax5pzpxg91yprvsc3xpwpc3wqli1q19v9v") (r "1.57.0")))

(define-public crate-noodles-gtf-0.5.0 (c (n "noodles-gtf") (v "0.5.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)))) (h "08f94f74zm9s6b1jrswa1hjhk3a0h5mnhrkbbv1nvx1mvv7ib4d1") (r "1.57.0")))

(define-public crate-noodles-gtf-0.6.0 (c (n "noodles-gtf") (v "0.6.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)))) (h "0nkpqd2z4nf9sl8cmjgmd7qbyi77a5f8x2a855k9zmyvqp7c0jwy") (r "1.57.0")))

(define-public crate-noodles-gtf-0.7.0 (c (n "noodles-gtf") (v "0.7.0") (d (list (d (n "noodles-core") (r "^0.10.0") (d #t) (k 0)))) (h "1j6a8xw8vcwwcblp10w332ca0vwh4d856wrmnprzc0ydylcjqh5y") (r "1.64.0")))

(define-public crate-noodles-gtf-0.8.0 (c (n "noodles-gtf") (v "0.8.0") (d (list (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)))) (h "12wmpgkqlj4przy6f5gsmhc0qjr6z4f3zfaxsrg0vv9rdyfyzq61") (r "1.64.0")))

(define-public crate-noodles-gtf-0.9.0 (c (n "noodles-gtf") (v "0.9.0") (d (list (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)))) (h "17cj9fdvsc9b5jjkmg55b1rnm2ik4f4v3bb6ph37lvj1acbnyssr") (r "1.64.0")))

(define-public crate-noodles-gtf-0.10.0 (c (n "noodles-gtf") (v "0.10.0") (d (list (d (n "noodles-bgzf") (r "^0.21.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.18.0") (d #t) (k 0)))) (h "1lhnkhbbrrym3djiyhlxsrq3hlqdpj141b3vhzpjnm0x8kl7marb") (r "1.64.0")))

(define-public crate-noodles-gtf-0.11.0 (c (n "noodles-gtf") (v "0.11.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.19.0") (d #t) (k 0)))) (h "05khvaj3sa8q9dpnqhj9y5wfa2bi4d1650ba3iiqdcnnm5iiqy96") (r "1.64.0")))

(define-public crate-noodles-gtf-0.12.0 (c (n "noodles-gtf") (v "0.12.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.20.0") (d #t) (k 0)))) (h "1r897hqavpd32fgl2aa0pdgx666a388sy0n9hiy3667bi25vdzzw") (r "1.64.0")))

(define-public crate-noodles-gtf-0.13.0 (c (n "noodles-gtf") (v "0.13.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.21.0") (d #t) (k 0)))) (h "1javqnmnmf6pdbxzzhmm6zsw3bh867knac276ax63kpggcc0inpn") (r "1.64.0")))

(define-public crate-noodles-gtf-0.14.0 (c (n "noodles-gtf") (v "0.14.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.22.0") (d #t) (k 0)))) (h "0rbl300j0zv250pmgxj1asd2kbpcv8l9cyh8vji4s91b72xnfqq9") (r "1.64.0")))

(define-public crate-noodles-gtf-0.15.0 (c (n "noodles-gtf") (v "0.15.0") (d (list (d (n "noodles-bgzf") (r "^0.23.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.23.0") (d #t) (k 0)))) (h "118sx4q4n7ip0irq3l8fb7ki2g6nr85k1ix4v3i1l3crqkac0mjq") (r "1.64.0")))

(define-public crate-noodles-gtf-0.16.0 (c (n "noodles-gtf") (v "0.16.0") (d (list (d (n "noodles-bgzf") (r "^0.24.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.24.0") (d #t) (k 0)))) (h "0nnx0vlpr6y5s893i45pmnb0g3qqby50qg4jkwi59wr8c2mbzdiw") (r "1.64.0")))

(define-public crate-noodles-gtf-0.17.0 (c (n "noodles-gtf") (v "0.17.0") (d (list (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.25.0") (d #t) (k 0)))) (h "1pcyyg21z06lkzziifi2gz7pfg5i21igsgrws6f2yiz5zlyrqxlz") (r "1.64.0")))

(define-public crate-noodles-gtf-0.18.0 (c (n "noodles-gtf") (v "0.18.0") (d (list (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.25.1") (d #t) (k 0)))) (h "0h3d0i88rlc45d8k501zgb60986dpa2xcg6y28q6wyxn2i4z2c8l") (r "1.64.0")))

(define-public crate-noodles-gtf-0.19.0 (c (n "noodles-gtf") (v "0.19.0") (d (list (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.26.0") (d #t) (k 0)))) (h "1ak7dwm9cdhka80llkpn6ykxg3j0ni95vs1664hlgi40586rw27w") (r "1.64.0")))

(define-public crate-noodles-gtf-0.20.0 (c (n "noodles-gtf") (v "0.20.0") (d (list (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.27.0") (d #t) (k 0)))) (h "1fzkdljkmj5khwksi2849m75cbalz0ijg2q69i8va7ansjrgq7xw") (r "1.64.0")))

(define-public crate-noodles-gtf-0.21.0 (c (n "noodles-gtf") (v "0.21.0") (d (list (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.28.0") (d #t) (k 0)))) (h "1jnciamvsshp4kdsx7plnnal4hal42bb60zacsddnd556ck8mvi6") (r "1.64.0")))

(define-public crate-noodles-gtf-0.22.0 (c (n "noodles-gtf") (v "0.22.0") (d (list (d (n "noodles-bgzf") (r "^0.26.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.13.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.29.0") (d #t) (k 0)))) (h "0h7ap580bc0m20scbb0na6rx5kcnsmw08vlg7g79hlhcrm721h7j") (r "1.70.0")))

(define-public crate-noodles-gtf-0.23.0 (c (n "noodles-gtf") (v "0.23.0") (d (list (d (n "noodles-bgzf") (r "^0.26.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.30.0") (d #t) (k 0)))) (h "0bvv8l11g83ki4hkllf2byx26sv6cgfvwv7w8hhvxwmv2mmz0w5b") (r "1.70.0")))

(define-public crate-noodles-gtf-0.24.0 (c (n "noodles-gtf") (v "0.24.0") (d (list (d (n "noodles-bgzf") (r "^0.27.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.31.0") (d #t) (k 0)))) (h "0v8pxm1q21ny6zkrxx151dcrf3ncjvgxck337z3ip238x6p67mb3") (r "1.70.0")))

(define-public crate-noodles-gtf-0.25.0 (c (n "noodles-gtf") (v "0.25.0") (d (list (d (n "noodles-bgzf") (r "^0.28.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.32.0") (d #t) (k 0)))) (h "1kx4sm2yp6bwzp6fqvqal2sy0ypk9n7mws210g080kjp5sygmm4r") (r "1.70.0")))

(define-public crate-noodles-gtf-0.26.0 (c (n "noodles-gtf") (v "0.26.0") (d (list (d (n "noodles-bgzf") (r "^0.29.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.33.0") (d #t) (k 0)))) (h "1mkwmc6dmg9lc7wvwx1i0nlpvf3fjgzdcbnkr8brapbz8hg86j61") (r "1.70.0")))

(define-public crate-noodles-gtf-0.27.0 (c (n "noodles-gtf") (v "0.27.0") (d (list (d (n "noodles-bgzf") (r "^0.29.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.34.0") (d #t) (k 0)))) (h "1g888hb2gw0hzcfig5kr68pywrn3dh1g8dsc8bipkqia1d48hgm2") (r "1.70.0")))

(define-public crate-noodles-gtf-0.28.0 (c (n "noodles-gtf") (v "0.28.0") (d (list (d (n "noodles-bgzf") (r "^0.30.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.35.0") (d #t) (k 0)))) (h "1jbi331nl0j6vqd5x24xh8l984viz25a6vz3d20q0hg7948lwshj") (r "1.70.0")))

