(define-module (crates-io no od noodle) #:use-module (crates-io))

(define-public crate-noodle-0.1.0 (c (n "noodle") (v "0.1.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19355zpmzazxsqk3q4cqbqf6yj7skkpk4r6ycqfs51qwwmsnlbz9")))

(define-public crate-noodle-0.2.0 (c (n "noodle") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0y9sfhpzh20rzi9vdhqg48nbfm4rz88bxs7qbd2df59zrz4374gc")))

