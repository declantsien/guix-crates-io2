(define-module (crates-io no od noodles-gff) #:use-module (crates-io))

(define-public crate-noodles-gff-0.1.0 (c (n "noodles-gff") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1czb5xllpz54vcs25vglb0x0yjjfp7k0898vr65ycvgk6xbb6yx5")))

(define-public crate-noodles-gff-0.1.1 (c (n "noodles-gff") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "17gqkzz27m83z1q560id0xlwfxg4lh6gvjxf7ba8lh4rirb1ilia")))

(define-public crate-noodles-gff-0.2.0 (c (n "noodles-gff") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0diz7155b9bj8f3s3gr20y4998wb2ipqppxmdx53mila6q79qpky")))

(define-public crate-noodles-gff-0.3.0 (c (n "noodles-gff") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0rzilm2bgdyn8w3wlzrmar8fmy7byisjxwbsrdbhd4k12kb8f0kg")))

(define-public crate-noodles-gff-0.4.0 (c (n "noodles-gff") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1r3nizfld4f5ayk7pm46lqs2b10wv7cmcr9l4cz6f275h7j0ksjb")))

(define-public crate-noodles-gff-0.5.0 (c (n "noodles-gff") (v "0.5.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "01h2nvqbkvig0zasbyigcin3vygkm05ix4bc1kdrizlf8i098gcq") (r "1.56.0")))

(define-public crate-noodles-gff-0.6.0 (c (n "noodles-gff") (v "0.6.0") (d (list (d (n "noodles-core") (r "^0.6.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1hghidq74vfzw3l021f2h0gibgzyyr01qsshk05in7khl504vbqr") (r "1.56.0")))

(define-public crate-noodles-gff-0.6.1 (c (n "noodles-gff") (v "0.6.1") (d (list (d (n "noodles-core") (r "^0.7.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0lnvmp9gawk3nrx1p1zfgdxv369ii8vdw7f62b241vaml7847c81") (r "1.56.0")))

(define-public crate-noodles-gff-0.7.0 (c (n "noodles-gff") (v "0.7.0") (d (list (d (n "noodles-core") (r "^0.8.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1i2drl41y2appsq9cjvhzvrgrr6kcjlz1vvgwlhndiscmq8vvac2") (r "1.57.0")))

(define-public crate-noodles-gff-0.8.0 (c (n "noodles-gff") (v "0.8.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0s2qw6mnpv7ma48lv2b0iycwl8xzplmbzm7h7825cgf498vrfpz1") (r "1.57.0")))

(define-public crate-noodles-gff-0.9.0 (c (n "noodles-gff") (v "0.9.0") (d (list (d (n "noodles-core") (r "^0.9.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1iw8fqy7z27002i3hgdi8wljpx5xhwhbbb6vv3avzjnwvdghp6gq") (r "1.57.0")))

(define-public crate-noodles-gff-0.10.0 (c (n "noodles-gff") (v "0.10.0") (d (list (d (n "noodles-core") (r "^0.10.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "093dsqm130y8f7pcfdds6pkz0jmh7m7bs25l15qscxmcl1p3n28k") (r "1.64.0")))

(define-public crate-noodles-gff-0.11.0 (c (n "noodles-gff") (v "0.11.0") (d (list (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0az1xxrg4193i8gkmb6zwl86491r7kb8787zp941y0xyhq8zpgnn") (r "1.64.0")))

(define-public crate-noodles-gff-0.12.0 (c (n "noodles-gff") (v "0.12.0") (d (list (d (n "noodles-bgzf") (r "^0.21.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.18.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1hfd8dvpgxngzpd79bwm1fflip99pm0ssiyaf83zywqz3wspgf74") (r "1.64.0")))

(define-public crate-noodles-gff-0.13.0 (c (n "noodles-gff") (v "0.13.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.11.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.19.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1lmdl5vsp6kbhkh56dm8cf5pmp29a1qm9vgg4panwnajv8mlcdxg") (r "1.64.0")))

(define-public crate-noodles-gff-0.14.0 (c (n "noodles-gff") (v "0.14.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.20.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0wnf14k876x6aa517wpngb52ml1vn22ln2iswhccj68wv7sj6xi6") (r "1.64.0")))

(define-public crate-noodles-gff-0.15.0 (c (n "noodles-gff") (v "0.15.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.21.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1inwhbbsggglfqynkm7ga2npa2ms7393k250wfm29mvg864xjhxr") (r "1.64.0")))

(define-public crate-noodles-gff-0.16.0 (c (n "noodles-gff") (v "0.16.0") (d (list (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.22.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0kxg6ja3k5abfzsy6lshwqxzs4sqas8j5h78dmqyb1mr6miqhb8i") (r "1.64.0")))

(define-public crate-noodles-gff-0.17.0 (c (n "noodles-gff") (v "0.17.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.22.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0x1va5xd2rhg2vk2531a5bh5jid1yfchxarf49xplc9rpwqn3k99") (r "1.64.0")))

(define-public crate-noodles-gff-0.18.0 (c (n "noodles-gff") (v "0.18.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.22.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.22.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "10bc7x7qi5zsp7va7hnn9gxcyp46phjk4h2qq81jyqlxgcc6kyvn") (r "1.64.0")))

(define-public crate-noodles-gff-0.19.0 (c (n "noodles-gff") (v "0.19.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.23.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.23.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1nxnalw2zcpq10cmnk8zrs0njbp6crwc5izh8vjmiwq3xmr5w57c") (r "1.64.0")))

(define-public crate-noodles-gff-0.19.1 (c (n "noodles-gff") (v "0.19.1") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.23.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.23.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0jnf50w913hbax9kwd1cjkwkc36a0722d5g3ympjd25ik128spw0") (r "1.64.0")))

(define-public crate-noodles-gff-0.20.0 (c (n "noodles-gff") (v "0.20.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.24.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.24.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0ghb9mvdybpjs4qppnhlh0qwyizdw4i0fl6r80rkikpg92wfds88") (r "1.64.0")))

(define-public crate-noodles-gff-0.21.0 (c (n "noodles-gff") (v "0.21.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.25.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1c2iggiifjnjxrdfpdjx5kpvk7cavw9072l7yqiwhw16sqzf2sp2") (r "1.64.0")))

(define-public crate-noodles-gff-0.22.0 (c (n "noodles-gff") (v "0.22.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.25.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1hi91ramv2vyfxb2nr97na99w9yl5md8ig7v959m2g3nvmll91nx") (r "1.64.0")))

(define-public crate-noodles-gff-0.23.0 (c (n "noodles-gff") (v "0.23.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.26.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1rlc2ih01f9h78lzzgx959h24b2cask85whphqcmigzh8lnhaw1a") (r "1.64.0")))

(define-public crate-noodles-gff-0.24.0 (c (n "noodles-gff") (v "0.24.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.27.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1vx4s41ql4cg9cjb946vzv7yp08d3ra3dg1ifls7bggnh2v6s7si") (r "1.64.0")))

(define-public crate-noodles-gff-0.25.0 (c (n "noodles-gff") (v "0.25.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.25.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.12.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.28.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "14nh7xlhp1h4dn5n867x7qlfq2llraz7aw4w3ianvkkc63cjpipy") (r "1.64.0")))

(define-public crate-noodles-gff-0.26.0 (c (n "noodles-gff") (v "0.26.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.26.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.13.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.29.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0x291dvgw7l5lsbqxkvns8mvy856kp1wmm70i8ylv14nm98hay81") (r "1.70.0")))

(define-public crate-noodles-gff-0.27.0 (c (n "noodles-gff") (v "0.27.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.26.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.30.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "04rpdnivcgvwgbd8n8v6zv8zw2xx5jy4r8lfdmyzac1nzs3yry0l") (r "1.70.0")))

(define-public crate-noodles-gff-0.28.0 (c (n "noodles-gff") (v "0.28.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.27.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.31.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "06f7ivnig8c18880v3w1cnzkv7ifx53yp13nb8h9s43gwnq24yvs") (r "1.70.0")))

(define-public crate-noodles-gff-0.29.0 (c (n "noodles-gff") (v "0.29.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.28.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.32.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "03p1qjn88b5vlbbchijf9mc3zvjpg3gz642v8axqm25z2x2n65lj") (r "1.70.0")))

(define-public crate-noodles-gff-0.30.0 (c (n "noodles-gff") (v "0.30.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.29.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.14.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.33.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1ixg784kcwqb67vrwvq6kjavswsjb9y0m445q2pkb10lj8qvcmwz") (r "1.70.0")))

(define-public crate-noodles-gff-0.31.0 (c (n "noodles-gff") (v "0.31.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.29.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.34.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1jlzc5hcfjsf7is1agj8rvc4lv3yivj0kxwc8p00pinbcymyfpk5") (r "1.70.0")))

(define-public crate-noodles-gff-0.32.0 (c (n "noodles-gff") (v "0.32.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.30.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.35.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0f9d188cr2lrc7v2dbk4wlvg4ssmjd9ax0b59l2734yvhrnh9924") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.70.0")))

(define-public crate-noodles-gff-0.33.0 (c (n "noodles-gff") (v "0.33.0") (d (list (d (n "futures") (r "^0.3.15") (f (quote ("std"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.30.0") (d #t) (k 0)) (d (n "noodles-core") (r "^0.15.0") (d #t) (k 0)) (d (n "noodles-csi") (r "^0.35.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1l3h061ncd9flmjxcwb9cqaf4f7x6h75kj7l19r2kw63ypx6x2sp") (s 2) (e (quote (("async" "dep:futures" "dep:tokio")))) (r "1.70.0")))

