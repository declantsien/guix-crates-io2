(define-module (crates-io no od noodles-core) #:use-module (crates-io))

(define-public crate-noodles-core-0.1.0 (c (n "noodles-core") (v "0.1.0") (d (list (d (n "noodles-sam") (r "^0.1.0") (d #t) (k 0)))) (h "0g6v3ckyn1zfzzmm93y5ppyqc0hzj1r287a27m5bw9aaknxyhaj9")))

(define-public crate-noodles-core-0.1.1 (c (n "noodles-core") (v "0.1.1") (d (list (d (n "noodles-sam") (r "^0.1.1") (d #t) (k 0)))) (h "0gj2hffkq9kr0na50b0lnb0zswmcqci2dq7zkxsb6dvl0zifgbhs")))

(define-public crate-noodles-core-0.1.2 (c (n "noodles-core") (v "0.1.2") (d (list (d (n "noodles-sam") (r "^0.2.0") (d #t) (k 0)))) (h "1s7xzfi1shxs92zzw5y1m8sz316yyjknmirjwdv9iwq2bdlz15n0")))

(define-public crate-noodles-core-0.2.0 (c (n "noodles-core") (v "0.2.0") (d (list (d (n "noodles-sam") (r "^0.3.0") (d #t) (k 0)))) (h "0mcwxvsb00b389l0n2rvn5ps3q8pnfajgh5gcdim10sdri228cdc")))

(define-public crate-noodles-core-0.2.1 (c (n "noodles-core") (v "0.2.1") (d (list (d (n "noodles-sam") (r "^0.4.0") (d #t) (k 0)))) (h "1n0a84p8nxjk27gx80hwf0n08n5jpz7qfi51xlblgnh39hj7m0pz")))

(define-public crate-noodles-core-0.2.2 (c (n "noodles-core") (v "0.2.2") (d (list (d (n "noodles-sam") (r "^0.5.0") (d #t) (k 0)))) (h "0f0q2vmff6x337anf3ml3i8yziykrbpxjr9mzxlsakj482mdi7s0")))

(define-public crate-noodles-core-0.2.3 (c (n "noodles-core") (v "0.2.3") (d (list (d (n "noodles-sam") (r "^0.6.0") (d #t) (k 0)))) (h "0jmq6hq7s59jhmf3d0j3mq76cybpl9a71hc3khq42wxldy5l6fi8")))

(define-public crate-noodles-core-0.3.0 (c (n "noodles-core") (v "0.3.0") (d (list (d (n "noodles-sam") (r "^0.7.0") (d #t) (k 0)))) (h "0s1gvrr7a0i65mq7yjiqxk71ly0nrbjyrjyd3jqjanb21bplrkjm")))

(define-public crate-noodles-core-0.3.1 (c (n "noodles-core") (v "0.3.1") (d (list (d (n "noodles-sam") (r "^0.8.0") (d #t) (k 0)))) (h "04awsx279ngnaj60w42d848maiiqaqwim7c0psg09163a915zzv6")))

(define-public crate-noodles-core-0.3.2 (c (n "noodles-core") (v "0.3.2") (d (list (d (n "noodles-sam") (r "^0.9.0") (d #t) (k 0)))) (h "0vqhfymim2d278xlhpf25xly9x6jbngarm2r711qcikw06v8ajci")))

(define-public crate-noodles-core-0.3.3 (c (n "noodles-core") (v "0.3.3") (d (list (d (n "noodles-sam") (r "^0.10.0") (d #t) (k 0)))) (h "0p77hg8d5wxa5mv9h8p8jwcix1q42fmkbpk2arp31hf7mi4z92pn")))

(define-public crate-noodles-core-0.3.4 (c (n "noodles-core") (v "0.3.4") (d (list (d (n "noodles-sam") (r "^0.11.0") (d #t) (k 0)))) (h "1d42g1k3vfzdrar52lf3pryg1d61vyyprz371hjgdr3jdrlgsnn3")))

(define-public crate-noodles-core-0.4.0 (c (n "noodles-core") (v "0.4.0") (d (list (d (n "noodles-sam") (r "^0.12.0") (d #t) (k 0)))) (h "08l7w8am67iafydady9mw1fjav2gwdzdjf5a9dilnjvhl3kw07dl") (r "1.56.0")))

(define-public crate-noodles-core-0.5.0 (c (n "noodles-core") (v "0.5.0") (d (list (d (n "noodles-sam") (r "^0.13.0") (d #t) (k 0)))) (h "09ww9m99r5d3da0823clgh3r1wh0wwixrnlxdrmmxfvk3h2d2nri") (r "1.56.0")))

(define-public crate-noodles-core-0.6.0 (c (n "noodles-core") (v "0.6.0") (h "1hq8i80pj60dvb762szb2b9w8jaw58djc6l21bhhmhglamyb8wgk") (r "1.56.0")))

(define-public crate-noodles-core-0.7.0 (c (n "noodles-core") (v "0.7.0") (h "1rrqjd6d6z88g3y0c9khfaps860420alnnkz2ggm8kf0bs4cqbah") (r "1.56.0")))

(define-public crate-noodles-core-0.8.0 (c (n "noodles-core") (v "0.8.0") (h "1j58jqnx4zplymfgnz2f5hnzvy5a3n8c31kkqvqfgmi0zscbjn5s") (r "1.57.0")))

(define-public crate-noodles-core-0.9.0 (c (n "noodles-core") (v "0.9.0") (h "1r1gywapdgvqk5ah07lnfikls58nn5m7dmbh3m05klg1fqxg4dd5") (r "1.57.0")))

(define-public crate-noodles-core-0.10.0 (c (n "noodles-core") (v "0.10.0") (h "02b9iwxnnz83mnblmdp4s6d68yr3azpclg2fx5v4sbvrph6jzx9j") (r "1.64.0")))

(define-public crate-noodles-core-0.11.0 (c (n "noodles-core") (v "0.11.0") (h "1zn22mi79xs78qqd4aq0n9fawajxaw4j2l77jwbyg4ikw44spybj") (r "1.64.0")))

(define-public crate-noodles-core-0.12.0 (c (n "noodles-core") (v "0.12.0") (h "0fl47dq19zhdvd3db5hrdxhgrc1rgxjqglzdpb5clfp35wcy7ywl") (r "1.64.0")))

(define-public crate-noodles-core-0.13.0 (c (n "noodles-core") (v "0.13.0") (h "06zyxsnmg47d9sl4d8i78py8jghmdsivhih4cy8y2jdl4wcs14r9") (r "1.70.0")))

(define-public crate-noodles-core-0.14.0 (c (n "noodles-core") (v "0.14.0") (h "10n20qxyzn7x5gcp523f66impsqv6cr2mcf98iaf1r1dcnzc6dkk") (r "1.70.0")))

(define-public crate-noodles-core-0.15.0 (c (n "noodles-core") (v "0.15.0") (d (list (d (n "bstr") (r "^1.9.0") (f (quote ("std"))) (k 0)))) (h "00326wij796in2l1qmjdiwycqpkc793b9ymhyaz5l86i42qcda65") (r "1.70.0")))

