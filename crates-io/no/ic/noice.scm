(define-module (crates-io no ic noice) #:use-module (crates-io))

(define-public crate-noice-0.7.0 (c (n "noice") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "1xb08fzpgyx55335lha7f5nhi33l2bwn2zr790sn14rr6hanc8l0") (f (quote (("default" "image"))))))

(define-public crate-noice-0.7.1 (c (n "noice") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0y4sr3p9zzrymxv57qzvmygww4xj9r84npjsnavzp9870mkl5lpl") (f (quote (("default" "image"))))))

