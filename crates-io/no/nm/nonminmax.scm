(define-module (crates-io no nm nonminmax) #:use-module (crates-io))

(define-public crate-nonminmax-0.1.0 (c (n "nonminmax") (v "0.1.0") (h "00ja39q5ph5nbab4ih16qh5vdk451f0b9f19s8b8v7gbwjj3fxlb")))

(define-public crate-nonminmax-0.1.1 (c (n "nonminmax") (v "0.1.1") (h "1hb4i8rjq9kfhr4q68rx9fmg87sjkr7g5kamrpwwdwl516iin6nl")))

