(define-module (crates-io no nm nonmax) #:use-module (crates-io))

(define-public crate-nonmax-0.2.0 (c (n "nonmax") (v "0.2.0") (h "1pm7267yscaq330gyqr6ihybairvkays47rzg5dsgy07mbap622z")))

(define-public crate-nonmax-0.3.0 (c (n "nonmax") (v "0.3.0") (h "0riy83grrbws9w48yh5q58kya8la2qfpxq76ibnwbymnbcnm1iv6")))

(define-public crate-nonmax-0.4.0 (c (n "nonmax") (v "0.4.0") (h "1fy90q6vk7s0jzc5h1kgm15v34a4g8vz0jx7ld3dqqgn8n4zd05x")))

(define-public crate-nonmax-0.5.0 (c (n "nonmax") (v "0.5.0") (h "1knhqjszba18a0pn3x50ld12v8fk7z40jy7x8q08sfcyqch6syi8") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5.1 (c (n "nonmax") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x0pls3s01jnqi4yd753i9p3wwx2yb3l42cgmncjswy2jia78mb4") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5.2 (c (n "nonmax") (v "0.5.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cisin0ms9rnkilwdi3qsmdq2mj0v0y6zdpl2j7a24xxk0bgghyg") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-nonmax-0.5.3 (c (n "nonmax") (v "0.5.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0s9nxcbj6b5vjbz5wmm3cnxl59f3niksqq6n1j7m4dg1jda6yxcr") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5.4 (c (n "nonmax") (v "0.5.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08p4lxazaw4ybgc0wzlli0mxrdph5k47agfi99ldizpzlhz5jjkf") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5.5 (c (n "nonmax") (v "0.5.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0lfvyfz4falgmc9g1cbfi2wkys9wka2nfmdyga87zikf636ml2k1") (f (quote (("std") ("default" "std"))))))

