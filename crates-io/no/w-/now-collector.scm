(define-module (crates-io no w- now-collector) #:use-module (crates-io))

(define-public crate-now-collector-0.1.0 (c (n "now-collector") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("net"))) (d #t) (k 0)))) (h "0l4i7c6mxkw95v0lsxiqsb0sfgzcia3109b4vmhbranjda9pna33")))

(define-public crate-now-collector-0.2.0 (c (n "now-collector") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "http1" "tcp"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("net" "macros" "rt-multi-thread" "fs" "sync"))) (d #t) (k 0)))) (h "08klz6f1j4ljk97k71dybqixlwj7v09sijpb9snm108kdk9kakr6")))

