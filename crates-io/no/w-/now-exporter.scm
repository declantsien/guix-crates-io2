(define-module (crates-io no w- now-exporter) #:use-module (crates-io))

(define-public crate-now-exporter-0.1.0 (c (n "now-exporter") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("net"))) (d #t) (k 0)))) (h "1qn3492ishh2kr4zzhlnvalm9lnxs6azmd3dgffykqql6kk2g82f")))

