(define-module (crates-io no mb nombytes) #:use-module (crates-io))

(define-public crate-nombytes-0.1.0 (c (n "nombytes") (v "0.1.0") (d (list (d (n "bytes") (r ">=0.5.3, <2.0.0") (k 0)) (d (n "miette") (r ">=3.0.0, <6.0.0") (o #t) (k 0)) (d (n "nom") (r ">=6.0.0, <8.0.0") (k 0)))) (h "1k7ys9wrc4fx819dyncki3xgf5z3hbd55haj2hp41id5dybbj770") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("miette" "dep:miette" "std")))) (r "1.60")))

(define-public crate-nombytes-0.1.1 (c (n "nombytes") (v "0.1.1") (d (list (d (n "bytes") (r ">=0.5.3, <2.0.0") (k 0)) (d (n "miette") (r ">=3.0.0, <6.0.0") (o #t) (k 0)) (d (n "nom") (r ">=6.0.0, <8.0.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0wbl5wdh4ss34770pn1z28axzxk59dlysfpfsccpql5z2a7y1mqw") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "bytes/serde") ("miette" "dep:miette" "std")))) (r "1.60")))

