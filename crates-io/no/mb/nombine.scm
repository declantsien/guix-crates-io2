(define-module (crates-io no mb nombine) #:use-module (crates-io))

(define-public crate-nombine-0.1.0 (c (n "nombine") (v "0.1.0") (d (list (d (n "combine") (r "^3") (k 0)) (d (n "little-skeptic") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "little-skeptic") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "nom") (r "^4") (k 0)))) (h "1im2zam27qsbhlcpc3glxfvpzl76b0y9j38nkkpf7kkr8pmfpk86") (f (quote (("verbose-errors" "nom/verbose-errors") ("std" "nom/std" "combine/std") ("default" "std"))))))

