(define-module (crates-io no nz nonzero_lit) #:use-module (crates-io))

(define-public crate-nonzero_lit-0.1.0 (c (n "nonzero_lit") (v "0.1.0") (h "09hqr8ywlbzq1j9gc69cxk9w5q0xv7w5qybjd53jkvfr6q5ww69r")))

(define-public crate-nonzero_lit-0.1.1 (c (n "nonzero_lit") (v "0.1.1") (h "1l93snp090diwzf2qrir6x8q7a33spipp96fhgxrvvxshn454wf9")))

(define-public crate-nonzero_lit-0.1.2 (c (n "nonzero_lit") (v "0.1.2") (h "1yh8nggqlpb3glaykxrryla1qg2jdj6jc63j0qywxf29b0vf1a9h")))

