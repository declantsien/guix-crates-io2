(define-module (crates-io no nz nonzero_signed) #:use-module (crates-io))

(define-public crate-nonzero_signed-1.0.0 (c (n "nonzero_signed") (v "1.0.0") (h "1vvi7b9v2kw8k3lb7awkq1dc4in9ijwimzacn9s7x1nfychlydif") (y #t)))

(define-public crate-nonzero_signed-1.0.1 (c (n "nonzero_signed") (v "1.0.1") (h "0kmrcl2zjnww2z7x83yz8dzn9w9xnvrbvk48sq695a4v33xzvp89") (y #t)))

(define-public crate-nonzero_signed-1.0.2 (c (n "nonzero_signed") (v "1.0.2") (h "1phq7swdqwkkjqq711asqk7ypdwffdcqvk7f82fbz2m3kd2kr0fr") (y #t)))

(define-public crate-nonzero_signed-1.0.3 (c (n "nonzero_signed") (v "1.0.3") (h "1n1pdqzfzj8a8yh1s16sjrk5jjj5c3xvh4albwzhsfrkh823ly02")))

(define-public crate-nonzero_signed-1.0.4 (c (n "nonzero_signed") (v "1.0.4") (d (list (d (n "rustversion") (r "^1.0.17") (d #t) (k 0)))) (h "1h54bw67rdcxg6niagbxkj7kr715sbpaf1mphhbcx3a1j6r7apqi")))

