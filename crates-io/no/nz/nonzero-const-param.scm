(define-module (crates-io no nz nonzero-const-param) #:use-module (crates-io))

(define-public crate-nonzero-const-param-0.0.1-alpha.1 (c (n "nonzero-const-param") (v "0.0.1-alpha.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k9p0bn1hhhb87bfl72hlc4d3d12j4mrcyfrjg1qjjwg92k82yzc")))

