(define-module (crates-io no nz nonzero_ext) #:use-module (crates-io))

(define-public crate-nonzero_ext-0.0.1 (c (n "nonzero_ext") (v "0.0.1") (h "0g8dxv3xag5nz497yxjj976yzdpqws6nd419vw74qb059fjdi1bz")))

(define-public crate-nonzero_ext-0.0.2 (c (n "nonzero_ext") (v "0.0.2") (h "0qzj4khacch9gw1x1r74njkxl7gk857vfxnkqf0q7lf9309jmz53")))

(define-public crate-nonzero_ext-0.1.0 (c (n "nonzero_ext") (v "0.1.0") (h "164vshfpfs946mw57ywmypk9b0vv6ncz6hl1xggsh63840nfv36z")))

(define-public crate-nonzero_ext-0.1.1 (c (n "nonzero_ext") (v "0.1.1") (h "1n20q215p46chfy9jqmlika05vijzdb923wma0wdkj901lclnkrj")))

(define-public crate-nonzero_ext-0.1.2 (c (n "nonzero_ext") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3.14") (f (quote ("tmp"))) (o #t) (d #t) (k 0)))) (h "1x73wr2ndnxq5szrviwjcfl15gzsq2xhhjc1hmda9a85c7l6c349") (f (quote (("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default") ("beta" "compiletest_rs/stable"))))))

(define-public crate-nonzero_ext-0.1.3 (c (n "nonzero_ext") (v "0.1.3") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("tmp"))) (o #t) (d #t) (k 0)))) (h "01yq7094c2f3i5asan9ya9m0vpapka8lc9d23smpj2gz2nqsiz37") (f (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable")))) (y #t)))

(define-public crate-nonzero_ext-0.1.4 (c (n "nonzero_ext") (v "0.1.4") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp"))) (o #t) (d #t) (k 0)))) (h "14i1lzh561y4k9i2gsmh3jpjdnnnd2fbcfgvwzs9nb8zbrzpfxh5") (f (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable")))) (y #t)))

(define-public crate-nonzero_ext-0.1.5 (c (n "nonzero_ext") (v "0.1.5") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp"))) (o #t) (d #t) (k 0)))) (h "1sd85jx8r8xk67r1jx08vi0clk6qsjp14r50wgk7n81bjdil26yv") (f (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable"))))))

(define-public crate-nonzero_ext-0.2.0 (c (n "nonzero_ext") (v "0.2.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0i7lhlzfv62756q2zwsqiqr3wdh37yy0rbv0x9ivmfpak43jk8a4") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonzero_ext-0.3.0 (c (n "nonzero_ext") (v "0.3.0") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "08fghyinb07xwhbj7vwvlhg45g5cvhvld2min25njidir12rdgrq") (f (quote (("std") ("default" "std"))))))

