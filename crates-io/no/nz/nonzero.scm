(define-module (crates-io no nz nonzero) #:use-module (crates-io))

(define-public crate-nonzero-0.1.0 (c (n "nonzero") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0lh0l77a7k2xaqwlgvaxdzc3w1cnyamxvpmgq1znkb01y0ywi0k9")))

(define-public crate-nonzero-0.2.0 (c (n "nonzero") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0zhj60rz5yycrklv5h6ji80vhzmivcbqxjfaaivky2lkcv6rm6qd")))

