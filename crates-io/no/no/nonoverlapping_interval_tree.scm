(define-module (crates-io no no nonoverlapping_interval_tree) #:use-module (crates-io))

(define-public crate-nonoverlapping_interval_tree-0.1.0 (c (n "nonoverlapping_interval_tree") (v "0.1.0") (h "0zabgklzihag9ax3gqbqm91vdrv678ax0l9vri2bfb8vc60jqpbb") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonoverlapping_interval_tree-0.1.1 (c (n "nonoverlapping_interval_tree") (v "0.1.1") (h "1b48ff4dfsjgj7i5ry2a09n3ia6zhrvc1vzx5c2fhcs260qmgd06") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonoverlapping_interval_tree-0.1.2 (c (n "nonoverlapping_interval_tree") (v "0.1.2") (h "1dvx8r6v5gia076bzwara7i4z5baq189zbx633m9sy13qslgp2fj") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonoverlapping_interval_tree-0.1.3 (c (n "nonoverlapping_interval_tree") (v "0.1.3") (h "1jwgngs1ybh5yifz77s4r5iqqphw3qpb4m93l4x94azxhlg7dhvk") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonoverlapping_interval_tree-0.1.4 (c (n "nonoverlapping_interval_tree") (v "0.1.4") (h "00nhvy3azg6gqzdr9cvnf5w4ax94wlj98s80ygj889admb5zajx4") (f (quote (("std") ("default" "std"))))))

(define-public crate-nonoverlapping_interval_tree-0.1.5 (c (n "nonoverlapping_interval_tree") (v "0.1.5") (h "0c7kvkmy74mb24s87bp1967yqn1iazgw8ny7sqdhm8zji4gqhzy5") (f (quote (("std") ("default" "std"))))))

