(define-module (crates-io no no nonogram) #:use-module (crates-io))

(define-public crate-nonogram-0.1.0 (c (n "nonogram") (v "0.1.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1l6nm6l041x7md1fps0z48ih9q426zy1xm8dgy0s2v96rlz9ldz1")))

(define-public crate-nonogram-0.2.0 (c (n "nonogram") (v "0.2.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086qb2874p2vkd7saz0h023g4ijh5k9k1yh55l0xjgyhlmd84x07")))

(define-public crate-nonogram-0.2.1 (c (n "nonogram") (v "0.2.1") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ny8g1cnpqvdi7w46g61pwr773znzlqjz15dd54z10an3w3pfbiq")))

(define-public crate-nonogram-0.2.2 (c (n "nonogram") (v "0.2.2") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nzwdy0dc3qwfxbqqy76vhg5l1ky2rn0j3nlzxa4d5c0gf0v8ylv")))

(define-public crate-nonogram-0.3.0 (c (n "nonogram") (v "0.3.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17qcwfshdz3ym8hks0p6jyfkkq533a93jabzdsm0csbipkz6b8zy")))

