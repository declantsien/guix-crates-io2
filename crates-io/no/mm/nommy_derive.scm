(define-module (crates-io no mm nommy_derive) #:use-module (crates-io))

(define-public crate-nommy_derive-0.1.0 (c (n "nommy_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zgs64wc1zh00csrm1mg7d5mlqq8a9cwbzcfjqjln6fvjx3iqbk2")))

(define-public crate-nommy_derive-0.1.1 (c (n "nommy_derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08gpi09x8141q40jw4bg4y3hwpcwxcry1h8gbv6y8401168xwrwn")))

(define-public crate-nommy_derive-0.2.0 (c (n "nommy_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ml3bfr36myzyaczn42k8vqxmz3ry3p5129hb94456kmhjncwj1i")))

(define-public crate-nommy_derive-0.2.1 (c (n "nommy_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n60lxlwnckvqdi6v0xnpw0w7crl6nh84b9j56ssjgmq56pr1f3i")))

(define-public crate-nommy_derive-0.3.0 (c (n "nommy_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bnyixsp57lqyvaydgrqyk2v46kmq5clc3smfkwm2q5cb213jaha")))

(define-public crate-nommy_derive-0.4.0 (c (n "nommy_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0929mbfn38vzdjybsd6mfgfnlcdrpikaklp4rbmvz5i7zldzyvyj")))

(define-public crate-nommy_derive-0.4.1 (c (n "nommy_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sf09m5s6asbn4g75xv0kjji5077ivzbdgg1dp6ncdfvlzy9jh6v")))

