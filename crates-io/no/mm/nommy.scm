(define-module (crates-io no mm nommy) #:use-module (crates-io))

(define-public crate-nommy-0.1.0 (c (n "nommy") (v "0.1.0") (d (list (d (n "nommy_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h72crmyp4ks0q33z6x9dyx18k15rjml6vhjm5557djhkp0f1xha")))

(define-public crate-nommy-0.1.1 (c (n "nommy") (v "0.1.1") (d (list (d (n "nommy_derive") (r "=0.1.0") (d #t) (k 0)))) (h "01i8a15lqp5ajiilhyddqfjwfx0sag9p05xvq4xnqllzq7j2sk32")))

(define-public crate-nommy-0.1.2 (c (n "nommy") (v "0.1.2") (d (list (d (n "nommy_derive") (r "=0.1.1") (d #t) (k 0)))) (h "1s0nb5g2if0i03p3qj62k54xfi0a27m6r72acmbzrihjbgh2ndm5")))

(define-public crate-nommy-0.1.3 (c (n "nommy") (v "0.1.3") (d (list (d (n "nommy_derive") (r "=0.1.1") (d #t) (k 0)))) (h "1z8zpnf95gi8j6c2z9i264vdvazvd6h6wgnc6x43w4z38criqf6y")))

(define-public crate-nommy-0.1.4 (c (n "nommy") (v "0.1.4") (d (list (d (n "nommy_derive") (r "=0.1.1") (d #t) (k 0)))) (h "0nqx14s1zqwxvp6dp0k6mp5yg03p8n38p30fvgyz09m72b76hvlm")))

(define-public crate-nommy-0.2.0 (c (n "nommy") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.2.0") (d #t) (k 0)))) (h "1wm1z3a7kzcy0hghj72j2wfzvaiqmwzlvwqdqyrgn0z343cjc1z4")))

(define-public crate-nommy-0.2.1 (c (n "nommy") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.2.1") (d #t) (k 0)))) (h "1knii89d69z7ifmlcxidpm4iw0g8rqhrj1hsrbyqkjd88icm48sz")))

(define-public crate-nommy-0.3.0 (c (n "nommy") (v "0.3.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.3.0") (d #t) (k 0)))) (h "109nk55d1rgj3crz2z3k3xgbl90lwg95g5lccg3k5m994yvkssf4")))

(define-public crate-nommy-0.3.1 (c (n "nommy") (v "0.3.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.3.0") (d #t) (k 0)))) (h "13a7c5by6wll1fn6vq8iwhzcq8sff3mn4dcv241h8idxa6sy8bbx")))

(define-public crate-nommy-0.3.2 (c (n "nommy") (v "0.3.2") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.3.0") (d #t) (k 0)))) (h "1fbhkwvynv13likxfbfx9x47vz73kw8mmdqbqsw177jkxxppj3yq")))

(define-public crate-nommy-0.3.3 (c (n "nommy") (v "0.3.3") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.3.0") (d #t) (k 0)))) (h "1h73mmnk6z73yvx0a9fx6cc3k7l47vcxzqyd5v2991x1gi41ddgc")))

(define-public crate-nommy-0.3.4 (c (n "nommy") (v "0.3.4") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.3.0") (d #t) (k 0)))) (h "17irc48bfa51g51khn7i5fimnc7409lpap7raq9i5fvl3ym3pp2w")))

(define-public crate-nommy-0.4.0 (c (n "nommy") (v "0.4.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.4.0") (d #t) (k 0)))) (h "0n00yip2khvlrbh6ycdl1xbdahrb2p89anhgs97w376a7pakm745")))

(define-public crate-nommy-0.4.1 (c (n "nommy") (v "0.4.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "nommy_derive") (r "=0.4.1") (d #t) (k 0)))) (h "0ab4chsx108d1kr7xcjb1sssgx76fi38sl7600dfwvly5wigbidn")))

