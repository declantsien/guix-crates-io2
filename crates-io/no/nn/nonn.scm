(define-module (crates-io no nn nonn) #:use-module (crates-io))

(define-public crate-nonn-0.1.0 (c (n "nonn") (v "0.1.0") (d (list (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1x4hmvpfhfi6x7yrl1i7n4j24ql8cbwr6rvx19sxilhq1zz553ys") (y #t) (r "1.57.0")))

(define-public crate-nonn-0.1.1 (c (n "nonn") (v "0.1.1") (d (list (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "15129qvq6qvs9rczk328dp0rasai5gjgsdyqy9a334nc842m8m9g") (y #t) (r "1.57.0")))

(define-public crate-nonn-0.1.2 (c (n "nonn") (v "0.1.2") (d (list (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0bl97cdj00z6q2p72wsl1s3l16w00n4m6kfikf0km4zpffc95kfw") (y #t) (r "1.57.0")))

(define-public crate-nonn-0.1.3 (c (n "nonn") (v "0.1.3") (d (list (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "14cmp1lb8vm251hn3ddilwzlvv9ax4cvfjhg2ydq0336vhz4h9bf") (y #t) (r "1.57.0")))

(define-public crate-nonn-0.1.4 (c (n "nonn") (v "0.1.4") (d (list (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "043fk675y87vhlc97yw06k4v4j5cnd7kr8jdyyr75zac2mi4a0p8") (r "1.57.0")))

