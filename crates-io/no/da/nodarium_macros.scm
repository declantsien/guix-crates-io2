(define-module (crates-io no da nodarium_macros) #:use-module (crates-io))

(define-public crate-nodarium_macros-0.1.0 (c (n "nodarium_macros") (v "0.1.0") (d (list (d (n "nodarium_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxfdlza417sja4j8gigf6rczwv2689wvc82p1vpw1kdxjg4f5sk")))

