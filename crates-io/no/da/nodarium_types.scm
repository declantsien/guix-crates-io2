(define-module (crates-io no da nodarium_types) #:use-module (crates-io))

(define-public crate-nodarium_types-0.1.0 (c (n "nodarium_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1i70wrxg2pjjr8mnl4dsr58ap7lxmagg1z1caxbzxcv0vic8mkdl")))

