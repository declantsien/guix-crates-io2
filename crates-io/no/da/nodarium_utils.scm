(define-module (crates-io no da nodarium_utils) #:use-module (crates-io))

(define-public crate-nodarium_utils-0.1.0 (c (n "nodarium_utils") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "noise") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("console"))) (d #t) (k 0)))) (h "08i35cqbag0jl8zijqpzbdphrh13qa1d2b2fynlfyqmb2qs6s0jg") (f (quote (("default" "console_error_panic_hook"))))))

