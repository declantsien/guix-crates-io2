(define-module (crates-io no sa nosana-node-helper) #:use-module (crates-io))

(define-public crate-nosana-node-helper-0.1.0 (c (n "nosana-node-helper") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "0fjsmmfmlviy6gvlq3pmiszhaqnnr1n1arp8dx02xxhw76jrp73r")))

(define-public crate-nosana-node-helper-0.1.1 (c (n "nosana-node-helper") (v "0.1.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "16xz4rzs8s5n0rg3ph6yc1a5fn4wjvvg4ypk7yhyj87ivr5br1km")))

(define-public crate-nosana-node-helper-1.0.0 (c (n "nosana-node-helper") (v "1.0.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1psdzmqp191k5k0jh5m0v66a8sjzm94m184dk926cm9d33lfdm6n")))

