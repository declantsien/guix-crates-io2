(define-module (crates-io no -a no-adjacent) #:use-module (crates-io))

(define-public crate-no-adjacent-0.1.0 (c (n "no-adjacent") (v "0.1.0") (h "1f6zwgb50ywvrs2q0aay0n9v01jvbjfprajvqc11h9krkpbyy8hp")))

(define-public crate-no-adjacent-0.1.1 (c (n "no-adjacent") (v "0.1.1") (h "0wmghq1i3kizldvws87kc5q3bdjy9fsd2l114dfcwrhrxbb6lhgy")))

