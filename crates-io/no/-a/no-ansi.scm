(define-module (crates-io no -a no-ansi) #:use-module (crates-io))

(define-public crate-no-ansi-0.1.0 (c (n "no-ansi") (v "0.1.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "0i9ci3c0k1caj4m0k324c8id10ldhzwmpmsq1nznivsrfkv6jzq7")))

