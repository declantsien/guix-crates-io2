(define-module (crates-io no nb nonblock-logger) #:use-module (crates-io))

(define-public crate-nonblock-logger-0.1.0 (c (n "nonblock-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sdp9zgzjayrxvp980q5l9ay9hba4vp2mj5n0dg5f2cq4fp5i2ak") (f (quote (("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.1 (c (n "nonblock-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "197dkj1gkif74bmx48y5qb09sxwpllajwb1q4yv4kmbfk3w4zs8s") (f (quote (("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.2 (c (n "nonblock-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sc1mwv8hcjq8g4pp359rgzp9pka8y8wci8yfd2pim4cv4hzwlhn") (f (quote (("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.3 (c (n "nonblock-logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0896ns9gaz0hc48jd64j8p0nl4xs94dzg097p77m9nl4fy6m9zm4") (f (quote (("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.4 (c (n "nonblock-logger") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zaimc158z7p40070yndd7zylzm0yldffab76vnky386v42f45pl") (f (quote (("dbg") ("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.5 (c (n "nonblock-logger") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lra4i10z7s6pic77jkngmrxp8h4qnih9ra6nm5vi75apwhjk16j") (f (quote (("dbg") ("color" "colored"))))))

(define-public crate-nonblock-logger-0.1.6 (c (n "nonblock-logger") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1z1d26rnp4p0b50ql9j1gb5jjbi0rg93c3gcwlcs3qks1cdmmij4") (f (quote (("dbg") ("color" "colored"))))))

(define-public crate-nonblock-logger-0.2.0 (c (n "nonblock-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "15ziwv91j7kj1g0kly9vqxvna8h54qf8djdvksvm63b1cqpa6prz") (f (quote (("default" "color") ("dbg") ("color" "yansi")))) (y #t)))

(define-public crate-nonblock-logger-0.2.1 (c (n "nonblock-logger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0rb8hk4z6dyp00r9k83nbinj7z579f5z2q95316f4r2w5v8p16yh") (f (quote (("dbg") ("color" "yansi"))))))

(define-public crate-nonblock-logger-0.2.2 (c (n "nonblock-logger") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0y60dwls6z19sygg7i4k36z3k7lj5bxl2igaw9x1lsl233yp8xxr") (f (quote (("dbg") ("color" "yansi"))))))

