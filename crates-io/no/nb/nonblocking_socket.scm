(define-module (crates-io no nb nonblocking_socket) #:use-module (crates-io))

(define-public crate-nonblocking_socket-0.0.1 (c (n "nonblocking_socket") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "18phxbcrqi3c0kk2337sqs86c9hc210a39hhcxhihka5mgm6h89p")))

(define-public crate-nonblocking_socket-0.0.2 (c (n "nonblocking_socket") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0jh8r1yhfm0np0c8f49rk4p8pyb31hsvz3yq3313g83d06l52d9y")))

