(define-module (crates-io no nb nonblocking) #:use-module (crates-io))

(define-public crate-nonblocking-0.1.0 (c (n "nonblocking") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1hizbywha7mikfakm2fhb1lv5nslnnq6whj029kdlfbhvrv49h9w") (y #t)))

(define-public crate-nonblocking-0.1.1 (c (n "nonblocking") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "02l1ahnd8nwi88b8311bli4gxnvz2gcy98k8s5lnz5jw9m4ai7sy") (y #t)))

(define-public crate-nonblocking-0.1.2 (c (n "nonblocking") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0nwr8gym8zdhhz300lff190nxsr3w004vkjigxgsbc4xv72cgaa0") (y #t)))

(define-public crate-nonblocking-0.1.3 (c (n "nonblocking") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("async-await" "thread-pool"))) (k 0)))) (h "19nnan6anzrdf0rxqgsp07q3mfyv00ydgffhg5lcyanhrys3c55f") (y #t)))

(define-public crate-nonblocking-0.1.4 (c (n "nonblocking") (v "0.1.4") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("async-await" "thread-pool"))) (k 0)))) (h "027kpy4n0adc59cg5bq2bdq4bvpx127v188w8fhxcqysrvgqhmd3")))

