(define-module (crates-io no nb nonblocking-channel) #:use-module (crates-io))

(define-public crate-nonblocking-channel-0.1.0 (c (n "nonblocking-channel") (v "0.1.0") (d (list (d (n "ringbuf") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "02zgq83hcrkgnfgi2pd76m2xj6d1w5piw9ssl15dlbba1vvhjxdl")))

