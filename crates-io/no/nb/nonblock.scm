(define-module (crates-io no nb nonblock) #:use-module (crates-io))

(define-public crate-nonblock-0.1.0 (c (n "nonblock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)))) (h "0wr141pkv15n7pj9b5i82gp11d9fqmbipdv6ps6cxi4g6p442qbr")))

(define-public crate-nonblock-0.2.0 (c (n "nonblock") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 2)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1m53m23vcvw990h4a6rycwaiisw7mvqmdayn0pc2nbjz5vra9isi")))

