(define-module (crates-io no es noest) #:use-module (crates-io))

(define-public crate-noest-0.1.0 (c (n "noest") (v "0.1.0") (d (list (d (n "av-metrics-decoders") (r "^0.3.1") (f (quote ("vapoursynth"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)))) (h "0yjffq3aa76x043n4si5ygspm9ic3q9m6xda2l424waswd81wcff")))

(define-public crate-noest-0.1.1 (c (n "noest") (v "0.1.1") (d (list (d (n "av-metrics-decoders") (r "^0.3.1") (f (quote ("vapoursynth"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)))) (h "1z6mxkfkzfyscif044s4i0vhpp7rs1fxsx6088da1r2llfl5zig7")))

