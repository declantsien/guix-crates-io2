(define-module (crates-io no ot noot) #:use-module (crates-io))

(define-public crate-noot-0.1.0 (c (n "noot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "16va2pm9zy6scwcs5qnwyxnjpam7q5b7kbb0ki3p4c9nj1npg5wv")))

