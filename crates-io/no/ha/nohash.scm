(define-module (crates-io no ha nohash) #:use-module (crates-io))

(define-public crate-nohash-0.0.0 (c (n "nohash") (v "0.0.0") (h "023ml856f18wzwly08mwi80vpwvill8szd158x9414cw4dvqbqx4")))

(define-public crate-nohash-0.2.0 (c (n "nohash") (v "0.2.0") (h "1ji8ylndxnq02pjwxfbac0yfs7xmcibpfd9c8j1xzb7pcvxqky50") (f (quote (("std") ("default" "std"))))))

