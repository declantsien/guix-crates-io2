(define-module (crates-io no ha nohash-hasher) #:use-module (crates-io))

(define-public crate-nohash-hasher-0.1.0 (c (n "nohash-hasher") (v "0.1.0") (h "01vls6vzik4z59hnnnsj9l3430y3fcm7k4lywydfr31b8dr3qn97")))

(define-public crate-nohash-hasher-0.1.1 (c (n "nohash-hasher") (v "0.1.1") (h "1f0vfk8rcvc5v4ycblyg6gbc0sm9x2qxjlzbnv61klljrvy8l4qd")))

(define-public crate-nohash-hasher-0.1.2 (c (n "nohash-hasher") (v "0.1.2") (h "13hfpr4xl0xwnxs11vr53nvxbz5rdpm38w3gdyj3p6kzr5p7lraf")))

(define-public crate-nohash-hasher-0.1.3 (c (n "nohash-hasher") (v "0.1.3") (h "13skkr5aimyx5za44fzcqpzb5x31rv20p60agvqynnb1qbqjn6kj") (f (quote (("std") ("default" "std"))))))

(define-public crate-nohash-hasher-0.2.0 (c (n "nohash-hasher") (v "0.2.0") (h "0lf4p6k01w4wm7zn4grnihzj8s7zd5qczjmzng7wviwxawih5x9b") (f (quote (("std") ("default" "std"))))))

