(define-module (crates-io no m8 nom8) #:use-module (crates-io))

(define-public crate-nom8-0.1.0 (c (n "nom8") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (k 0)) (d (n "minimal-lexical") (r "^0.2.0") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0b16yz3z25ybzizlsj8n0ixkvzpf0y5l73a79v9jcdbw57q0inbm") (f (quote (("unstable-doc" "alloc" "std") ("std" "alloc" "memchr/std" "minimal-lexical/std") ("default" "std") ("alloc")))) (r "1.51.0")))

(define-public crate-nom8-0.2.0 (c (n "nom8") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1y6jzabxyrl05vxnh63r66ac2fh0symg5fnynxm4ii3zkif580df") (f (quote (("unstable-doc" "alloc" "std") ("std" "alloc" "memchr/std") ("default" "std") ("alloc")))) (r "1.51.0")))

