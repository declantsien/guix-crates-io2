(define-module (crates-io no sh noship) #:use-module (crates-io))

(define-public crate-noship-0.0.1 (c (n "noship") (v "0.0.1") (h "1hgmjx0m8303jkjdvahg36m3fg4fva1zdi3d64w7l0nmy7yc4krk") (y #t)))

(define-public crate-noship-0.1.0 (c (n "noship") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0ygwiwzgjjrs6a2b8d0p48rvhrql7hn66b35jx9lxai2b5y858q4") (y #t)))

(define-public crate-noship-0.1.1 (c (n "noship") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "05cpc9l5imc3jhg7cffclpqscvdr5cvny5my4kvvjkmn2606hj0z") (y #t)))

