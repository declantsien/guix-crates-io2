(define-module (crates-io no tu notus-mempool) #:use-module (crates-io))

(define-public crate-notus-mempool-0.1.1 (c (n "notus-mempool") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0shjm95l12x2y1kqazrkf5whkcyymlm7phvc9jv79wc6ybxjxhvz")))

