(define-module (crates-io no tu notugly) #:use-module (crates-io))

(define-public crate-notugly-0.2.0 (c (n "notugly") (v "0.2.0") (h "025qnvxbj8p94l3jc0f3ndkh9gj9yf808kprgafzqf2gwhdcx228")))

(define-public crate-notugly-0.2.1 (c (n "notugly") (v "0.2.1") (h "1b1blpc3vfv1nm2q8rydkkyj6pnwsdpm1nkrp4hrzws9qfbywiyh")))

(define-public crate-notugly-0.2.2 (c (n "notugly") (v "0.2.2") (h "1gw2lvm7bgsb96b56fjxv05xbgz9fh64i53xx2f2icf3px0q01bk")))

