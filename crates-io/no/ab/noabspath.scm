(define-module (crates-io no ab noabspath) #:use-module (crates-io))

(define-public crate-noabspath-0.1.0 (c (n "noabspath") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "18fpvx7fdcjs64j4706ywga1krhjvwpixahg3383vwzwwns6kxp3")))

(define-public crate-noabspath-0.1.1 (c (n "noabspath") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "1clyvyqdrwqhs4crfgr08cazvkqp8p9pvigjnjaahd64dzcv5x8b")))

(define-public crate-noabspath-0.1.2 (c (n "noabspath") (v "0.1.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "1r85f8k7n5s6pkcanbzcv8nvq2qv5m353qv2797zg84zk67i8gam")))

(define-public crate-noabspath-0.1.3 (c (n "noabspath") (v "0.1.3") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "1c3bi2w2268pmf8yb832vw5vxmwp0z6n4y3qvjp7yfabvix9q3xx")))

(define-public crate-noabspath-0.1.4 (c (n "noabspath") (v "0.1.4") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "0vwqcm5w14gff6rdzw0dyabxc1j9r0aa9dpqr79hmhy90f685509")))

(define-public crate-noabspath-0.1.5 (c (n "noabspath") (v "0.1.5") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)) (d (n "wildmatch") (r "~1.0.11") (d #t) (k 0)))) (h "0bqcfqa7j63hlf917mhwy5q8zxpmkpsn1dqpy71dibsgz8ndx9ha")))

(define-public crate-noabspath-0.1.6 (c (n "noabspath") (v "0.1.6") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.3.1") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)) (d (n "wildmatch") (r "~1.0.11") (d #t) (k 0)))) (h "1k7brbs3afa9xamk5fadrxvbqdq4svd8gm07wxci0kvx9m209l1m")))

(define-public crate-noabspath-0.1.8 (c (n "noabspath") (v "0.1.8") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "~2.0.0") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.5.2") (d #t) (k 0)) (d (n "regex") (r "~1.5.5") (d #t) (k 0)) (d (n "wildmatch") (r "~2.1.0") (d #t) (k 0)))) (h "05rn1kkrcxymbgg96q5gpl1n0rzzndd21riwj7k4m42bzagfl3kj")))

(define-public crate-noabspath-1.0.0 (c (n "noabspath") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "~2.0.0") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.5.2") (d #t) (k 0)) (d (n "regex") (r "~1.5.5") (d #t) (k 0)) (d (n "wildmatch") (r "~2.1.0") (d #t) (k 0)))) (h "0rsvwgfmhwhvvfc46zhkyz07nsbs2ksalwhpgwsvf41q940lpzdk")))

(define-public crate-noabspath-1.0.1 (c (n "noabspath") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "~2.0.0") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1.5.2") (d #t) (k 0)) (d (n "regex") (r "~1.5.5") (d #t) (k 0)) (d (n "wildmatch") (r "~2.1.0") (d #t) (k 0)))) (h "1dhpv8jnn0prwwicinnab8w6c3ppx2q7jah23d0malzkjkm2k6vc")))

(define-public crate-noabspath-1.0.2 (c (n "noabspath") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "~2") (d #t) (k 0)) (d (n "glob") (r "~0.3.0") (d #t) (k 0)) (d (n "rayon") (r "~1") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "wildmatch") (r "~2") (d #t) (k 0)))) (h "0n75pwm07sk4y776jzi30bjpdyvsnnl7i0rcivxcnr4gplcm0sba")))

