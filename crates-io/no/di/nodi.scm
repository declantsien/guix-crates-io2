(define-module (crates-io no di nodi) #:use-module (crates-io))

(define-public crate-nodi-0.1.0 (c (n "nodi") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0f9lrjj5ml4bqh0xwligf1n5z3plpyj4xici2vjdyx2vzh0jlqsq")))

(define-public crate-nodi-0.1.1 (c (n "nodi") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0sg4bkh0qchrirjr6arn62w1w4mnjd7i99msr3wvyvrz2337kafs")))

(define-public crate-nodi-0.1.2 (c (n "nodi") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1fk0n4i909jxcyhkv15jvdi552zqv6qqwsklzfq7fjr8vzqvlkf6")))

(define-public crate-nodi-0.1.3 (c (n "nodi") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1hly3j2blg8x8s5ml33ii39r15pha9g8wr4imfbf6rcybi0lfb92")))

(define-public crate-nodi-0.1.4 (c (n "nodi") (v "0.1.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "06nb8pqh7g9zvqjd63bhi4ibxk37lc316a96a5ilxkhdxzmykzx2")))

(define-public crate-nodi-0.1.5 (c (n "nodi") (v "0.1.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0k2k0kw7mdnkxailqd0h1xmp0dspwykz7l23pfzflahzqfhf74xq")))

(define-public crate-nodi-0.2.0 (c (n "nodi") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1j0l828pjrvc0w99n3fswj513z4hrz3i0cqk3jngvrjriy3389id")))

(define-public crate-nodi-0.2.1 (c (n "nodi") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1z36gznnlzh3ikm394ajwhfnii56gfph7m0gjn26n83p2p0r76d5")))

(define-public crate-nodi-0.2.2 (c (n "nodi") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0aib0gpv1zb9pjmxqhq00yyq1wxyf7d38gjncj6w0r0vi0xyr4dm")))

(define-public crate-nodi-0.3.0 (c (n "nodi") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "02l7vk02ym8vfjbbgrc8ph9rk5ny1r2bh7p04yyd90cy9s58wqkk")))

(define-public crate-nodi-0.4.0 (c (n "nodi") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "177983vpp0g2idrhb781nqj69ym0l412xr4jz76jj7xi2kfqgxr7") (y #t)))

(define-public crate-nodi-0.4.1 (c (n "nodi") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "08a98gky4bcc3ysasf4kx0qszbv5zkh552svpvwjs0qmxnbxmxr4") (y #t)))

(define-public crate-nodi-0.5.0 (c (n "nodi") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "18hg6a0igvr4k1wpkhd6ijahxrw7lr4w2qyy4ad91g3wzzzsw13s") (y #t)))

(define-public crate-nodi-0.5.1 (c (n "nodi") (v "0.5.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0w2kbanw1wxz70wxjy5w0m8x2bp29g7nr8zlg8d8bzknkacnqn1b") (y #t)))

(define-public crate-nodi-0.5.2 (c (n "nodi") (v "0.5.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0wqj9j56paj9126qz2k87gbrrd8c7gpn2w81a1mvg5w82pripklz") (y #t)))

(define-public crate-nodi-0.5.3 (c (n "nodi") (v "0.5.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "12fq0snplg7s1lj0adlyrcwlsy3nrby2as3bnmmlg9yck3jmilz5") (y #t)))

(define-public crate-nodi-0.6.0 (c (n "nodi") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0j0hryy70js7cdbc81h41lfpkiyr132prgpv8c8iqlb77kbnq92n") (y #t)))

(define-public crate-nodi-0.6.1 (c (n "nodi") (v "0.6.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0p552kjifd9vj8myw1n72f8bqi6aprf2l4yhxg1w0qn7sxfz8ayk") (y #t)))

(define-public crate-nodi-0.6.2 (c (n "nodi") (v "0.6.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "07k41zlg2z2c4rx71vafd37llhcmwzsbbi5n8cni469bgd6zs606") (y #t)))

(define-public crate-nodi-0.6.3 (c (n "nodi") (v "0.6.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1jcsr5981mljsmmbslk00x0xvqdg5g1nnbjqw2pzkj8xi2db71z5") (y #t)))

(define-public crate-nodi-0.7.0 (c (n "nodi") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0n79nql19brhib071di70i2r7md6x712lnwbi7igdgwzanmh5sbg") (y #t)))

(define-public crate-nodi-0.7.1 (c (n "nodi") (v "0.7.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0i1zsm64rg2qcvcnl48w21fz82d4jqzs0hvh95k8srlzfnjaq8ln") (y #t)))

(define-public crate-nodi-0.8.0 (c (n "nodi") (v "0.8.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "141m3wnm1qk8k9s4w2i70igkls1fz1sg4ssiigsgyb1qmrcgxmw8") (y #t)))

(define-public crate-nodi-0.8.1 (c (n "nodi") (v "0.8.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0587liq4vndqnawfw86kwwbcw88gvvvczaw1hz0j39f30c045dg5") (y #t)))

(define-public crate-nodi-0.9.0 (c (n "nodi") (v "0.9.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0iwd53sgsixhlrfmrh6i6d3lqc3zbdvgkk7k2pj5kmyckmq3w63n") (y #t)))

(define-public crate-nodi-0.9.1 (c (n "nodi") (v "0.9.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0q1db6vpk90zsgcizh1r6rqn2yayqi8hh03s5bxy6qaafm5mh7v9") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.9.2 (c (n "nodi") (v "0.9.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0ly4fmm61arflh908s4qd4p4yjkp4q9iccsv47kizqmk51bjymk2") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.9.3 (c (n "nodi") (v "0.9.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0mxb5ql8317w9zb8cn8780v9bd988xvz6x9cvx1qq39kg5878am3") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.9.4 (c (n "nodi") (v "0.9.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0lggvyy1vvpxhgpik7z2zsq7bfin83a7nbpbb2xcyvnimsz6a2d1") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.10.0 (c (n "nodi") (v "0.10.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0xfv5g2h62ih0lfw7h6wqkdvy7z9bl1cjqqyqag44aycsxyh91r5") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.10.2 (c (n "nodi") (v "0.10.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0r0sbr5lmgxkzdimfsd1bzbnp4ggr4c8zd2xhwsizsqa6k054fh6") (f (quote (("verbose-log"))))))

(define-public crate-nodi-0.11.0 (c (n "nodi") (v "0.11.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0120va8pnlijxq8fia9i86478jinw0xh30p4n0vy16crh823swif") (f (quote (("verbose-log") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.12.0 (c (n "nodi") (v "0.12.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1z5xpn9l8xpb1mzk0fs2f9qfd6jbbzpa0mnj19hn598dk14j9il3") (f (quote (("winrt" "midir/winrt") ("verbose-log") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.13.0 (c (n "nodi") (v "0.13.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "17kzyb1lhp7fr765hz44wjkb1hxkx1y76538w9ascgbhsa6gbn42") (f (quote (("winrt" "midir/winrt") ("verbose-log") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.13.1 (c (n "nodi") (v "0.13.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1b5fz0cd7ai6vak2jlagczdark8q62ssq9il3a37blc5pi5j95d6") (f (quote (("winrt" "midir/winrt") ("verbose-log") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.13.2 (c (n "nodi") (v "0.13.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1lm0nb0l79h7h6pqx617qazx9kqa9ih293n19awls6280l12y6rn") (f (quote (("winrt" "midir/winrt") ("verbose-log") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.13.4 (c (n "nodi") (v "0.13.4") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1z3km6dbj0265imccis5d1n2rkpg23599b5dgwwwq1gxi27dr5xm") (f (quote (("winrt" "midir/winrt") ("verbose-log") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.14.0 (c (n "nodi") (v "0.14.0") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0fwaaq16k11g85d1yb0kiw6yqa4g9lq98w2wl1mdbqq0d6bq3p9b") (f (quote (("winrt" "midir/winrt") ("verbose-tracing") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.15.1 (c (n "nodi") (v "0.15.1") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0v0bidsdss0n2y2d1hm4lypjnw1nmb6if5zvgvi75bdkx3y1r203") (f (quote (("winrt" "midir/winrt") ("verbose-tracing") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.15.2 (c (n "nodi") (v "0.15.2") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0pn80k9a5ikhdc87c2lbmz7vr41ir8k181i2mz133slgywv652hk") (f (quote (("winrt" "midir/winrt") ("verbose-tracing") ("jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.16.0 (c (n "nodi") (v "0.16.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "093b8jnbpfk5i2cr4g5qplbv0d0lj4bq5b05vsgh4bsl52mwsdcx") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.18.1 (c (n "nodi") (v "0.18.1") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "023mj49m59iwfq4vgwj4xih4jwqbqjsvlm5ygkv0wfxma5d7bbcz") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.19.0 (c (n "nodi") (v "0.19.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1jjg04bs4ya33kx3b6015shvsbqnhk36ksb360ax1x7lbsiy6z1x") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.19.1 (c (n "nodi") (v "0.19.1") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "1yxxpaf4fv32s0fy6szxyqawm9g08nx73l7jlq1r3q02am5ziwm9") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-0.19.2 (c (n "nodi") (v "0.19.2") (d (list (d (n "clap") (r "=3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0wqndmazh3g8vik7kq0yndjkgzkcm1b0jhpag23dzx7zijwlzk8m") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

(define-public crate-nodi-1.0.0 (c (n "nodi") (v "1.0.0") (d (list (d (n "clap") (r "=3.1.18") (d #t) (k 2)) (d (n "midir") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 2)) (d (n "midly") (r "^0.5.3") (d #t) (k 0)))) (h "0avmckhv17zn1m8a5in8xmc77ib36ar3283141c4z7ndjrqy00v9") (f (quote (("midir-winrt" "midir/winrt") ("midir-jack" "midir/jack") ("hybrid-sleep") ("default" "hybrid-sleep"))))))

