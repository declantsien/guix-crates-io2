(define-module (crates-io no di nodium) #:use-module (crates-io))

(define-public crate-nodium-0.1.0 (c (n "nodium") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_node_graph") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lxkwr95y4x6j4kj61j06dqqrb04845gdl55vvg0fpz5zkl3s6xd") (f (quote (("persistence" "serde" "egui_node_graph/persistence" "eframe/persistence") ("default"))))))

(define-public crate-nodium-0.1.1 (c (n "nodium") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_node_graph") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1b5wim1k5372lwd0lhw5rdxf03j58qhbqfkd9snq8v8fwpj71jmh") (f (quote (("persistence" "serde" "egui_node_graph/persistence" "eframe/persistence") ("default"))))))

(define-public crate-nodium-0.1.2 (c (n "nodium") (v "0.1.2") (d (list (d (n "eframe") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nodium-app") (r "^0.1.0") (d #t) (k 0)) (d (n "nodium-events") (r "^0.1.1") (d #t) (k 0)) (d (n "nodium-plugins") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dshc1b3h45bi07a7zwmkpd5mrs9y4xygv8ycp42ipbi69fg3014")))

