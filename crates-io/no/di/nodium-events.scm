(define-module (crates-io no di nodium-events) #:use-module (crates-io))

(define-public crate-nodium-events-0.1.0 (c (n "nodium-events") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "sync"))) (d #t) (k 0)))) (h "1hgqm3skzin8qx769rj5ns2vbxn5q1y4lssg9jz5df4mic83clhh")))

(define-public crate-nodium-events-0.1.1 (c (n "nodium-events") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "sync"))) (d #t) (k 0)))) (h "1f1l92fxvfbm22a8k3gmqkqk8pbpfpyi1sxz8m3n29bb8qq26vis")))

(define-public crate-nodium-events-0.1.2 (c (n "nodium-events") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "sync"))) (d #t) (k 0)))) (h "03il4wmll81pri2zm0xfiiws4yaci65x3njvrdjhlimaw6prih0m")))

