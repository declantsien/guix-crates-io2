(define-module (crates-io no di nodium-plugins) #:use-module (crates-io))

(define-public crate-nodium-plugins-0.1.0 (c (n "nodium-plugins") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nodium-events") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15sxykkjszxac6j0bnjgpwzmw95xf95a6w9scwbd3plqvy2nrfhb")))

(define-public crate-nodium-plugins-0.1.1 (c (n "nodium-plugins") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nodium-events") (r "^0.1.2") (d #t) (k 0)) (d (n "nodium-pdk") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0saam1fv1ap4z6xqa8ksbr2zq92db3difwvlrq26s0sc6ckxds9n")))

