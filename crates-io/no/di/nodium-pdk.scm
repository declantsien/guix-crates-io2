(define-module (crates-io no di nodium-pdk) #:use-module (crates-io))

(define-public crate-nodium-pdk-0.1.0 (c (n "nodium-pdk") (v "0.1.0") (h "06ni04ap3clqx76yzjvd0379i6ay46i13kajy3qsxpw80f6s7x17")))

(define-public crate-nodium-pdk-0.1.1 (c (n "nodium-pdk") (v "0.1.1") (d (list (d (n "nodium-events") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0ysbzqgwrpyqcwd0x20bh7h3gr7zm49ip32gv2yv6z62hsgn4lx1")))

