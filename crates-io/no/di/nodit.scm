(define-module (crates-io no di nodit) #:use-module (crates-io))

(define-public crate-nodit-0.7.0 (c (n "nodit") (v "0.7.0") (d (list (d (n "btree_monstrousity") (r "^0.0.4") (f (quote ("btree_drain_filter" "btree_cursors"))) (k 0)) (d (n "either") (r "^1.9.0") (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)))) (h "00vivfff9qyn8mi844r5aflz73ma3cfw9s6xd5chkh9rn5x88m2w")))

(define-public crate-nodit-0.7.1 (c (n "nodit") (v "0.7.1") (d (list (d (n "btree_monstrousity") (r "^0.0.4") (f (quote ("btree_drain_filter" "btree_cursors"))) (k 0)) (d (n "either") (r "^1.9.0") (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)))) (h "1hzv1rcz1iqa34n0h64dj8crss7lfqsp2rsydkknpy4zr6vdrkgh")))

(define-public crate-nodit-0.8.0 (c (n "nodit") (v "0.8.0") (d (list (d (n "btree_monstrousity") (r "^0.0.4") (f (quote ("btree_drain_filter" "btree_cursors"))) (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "smallvec") (r "^1.13.1") (k 0)))) (h "1lxpvlmls486cg5l821m6d124xhh8f5if2yg9243f2gpzm8dyiaa")))

(define-public crate-nodit-0.9.0 (c (n "nodit") (v "0.9.0") (d (list (d (n "btree_monstrousity") (r "^0.0.4") (f (quote ("btree_drain_filter" "btree_cursors"))) (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (k 0)) (d (n "smallvec") (r "^1.13.1") (k 0)))) (h "0rnsf6hsi5fmnk31crmjaacq67i1p7ipxrivsnn6s27jriqrx4yb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nodit-0.9.1 (c (n "nodit") (v "0.9.1") (d (list (d (n "btree_monstrousity") (r "^0.0.4") (f (quote ("btree_drain_filter" "btree_cursors"))) (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (k 0)) (d (n "smallvec") (r "^1.13.1") (k 0)))) (h "07bzknmia6iszgj69i301kpwf5vlrfv05plvw5imx29lxyx8wqm5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

