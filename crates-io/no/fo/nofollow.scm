(define-module (crates-io no fo nofollow) #:use-module (crates-io))

(define-public crate-nofollow-0.1.0 (c (n "nofollow") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.86") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0z9wl2sz7f3f2bb84q5qi24sqq1w8cd26q7vl3y8jbjcqjs16yix") (y #t)))

(define-public crate-nofollow-0.1.1 (c (n "nofollow") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.86") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1nqlhk9n69v6ch0195fg51xh2ysgkj98w6w4f61a0x7wjzm3klzk") (y #t)))

(define-public crate-nofollow-0.1.2 (c (n "nofollow") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.93") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0idhb5nnfk2j9kijd2ffpdwzl9fw4nxx132aphnd032xhjmp3fdn") (y #t)))

(define-public crate-nofollow-0.1.3 (c (n "nofollow") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.94") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0fa6k88a2irz5v80a8cfw38lcv7d57zdi6vni1jrdiw08vc8gny6") (y #t)))

(define-public crate-nofollow-0.1.4 (c (n "nofollow") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.96") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0j5gjrv9clahybfxv469vmcfkgbhdz53j8hpplc6x89jgxzgbnsd") (y #t)))

