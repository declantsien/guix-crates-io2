(define-module (crates-io no rg norgopolis-protos) #:use-module (crates-io))

(define-public crate-norgopolis-protos-0.1.0 (c (n "norgopolis-protos") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1d28ikc0rx3pb7cchmqhjy9zw7wfrjf74fvaz742wky8a22dz8ln")))

(define-public crate-norgopolis-protos-0.2.0 (c (n "norgopolis-protos") (v "0.2.0") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "11hgamxvghw9g2jj3v2dxhagyz213xpjvhndqzlglmiripl423jd")))

(define-public crate-norgopolis-protos-0.3.0 (c (n "norgopolis-protos") (v "0.3.0") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0sws87ycqmk5vi83dif8k0q973yy075bj7xg9g9glahixmrjxas7")))

