(define-module (crates-io no ur nourl) #:use-module (crates-io))

(define-public crate-nourl-0.1.0 (c (n "nourl") (v "0.1.0") (h "0h6x43ms37l7xwm441fvbi7in30xdjf025l56598p913l0ld0igs")))

(define-public crate-nourl-0.1.1 (c (n "nourl") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0sajfbvka39v3id8f1shic3hgb0qzn9vvlw13n81dzk5vbgjxhdk")))

