(define-module (crates-io no pr noproto-derive) #:use-module (crates-io))

(define-public crate-noproto-derive-0.0.0 (c (n "noproto-derive") (v "0.0.0") (h "03blqraib56zh8zghqcgyj8srm2ivy60asrzrdzbkfnxgb6xq96l")))

(define-public crate-noproto-derive-0.1.0 (c (n "noproto-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19c60js7szmpyh4rb1jyh4hd4z51djc3qv9ikwyx95c3n5bz6j18")))

