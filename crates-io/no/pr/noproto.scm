(define-module (crates-io no pr noproto) #:use-module (crates-io))

(define-public crate-noproto-0.0.0 (c (n "noproto") (v "0.0.0") (h "14g9h0ma3rlcbsgiklsm8hwcpqxws9s2ldrkrhmhqb9pikr6hx91")))

(define-public crate-noproto-0.1.0 (c (n "noproto") (v "0.1.0") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "noproto-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0c4llfs1an2kv4b5znjfb0pg32bp821fsd7jahj7czyggfs8rbip") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:noproto-derive"))))))

