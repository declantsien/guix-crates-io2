(define-module (crates-io no ki nokiahealth) #:use-module (crates-io))

(define-public crate-nokiahealth-0.1.0 (c (n "nokiahealth") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "influent") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "0dxmfwi1bg9ix1az8k32w1kqrl4b2s570yppx69zy216wrn0nvps")))

