(define-module (crates-io no cl noclip) #:use-module (crates-io))

(define-public crate-noclip-0.1.0 (c (n "noclip") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1593i9h8innljshnfxc6g83i9pq6vnxh9c0sbvv23nx8arb3v014")))

