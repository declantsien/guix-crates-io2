(define-module (crates-io no t_ not_empty) #:use-module (crates-io))

(define-public crate-not_empty-0.1.0 (c (n "not_empty") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1l891il124m7iq4g5q1xa72xivqc6krvm6hbyvw3hdkayill0jx9") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1.1 (c (n "not_empty") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "046j159lza3dj7qpq9pqq6r029fcha5iyy9swzw6vms7a75waszm") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1.2 (c (n "not_empty") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0ch0a28rs1rlfmsdqc60ig24i6g11v6kb4g9ivnwg9zz2bp8gy3g") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1.3 (c (n "not_empty") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "02r8rkf7mdjc8m3q3h6qv2kah1afgz8sk9z5z1g1wahggz9r49b0") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

