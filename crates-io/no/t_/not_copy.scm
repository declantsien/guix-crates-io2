(define-module (crates-io no t_ not_copy) #:use-module (crates-io))

(define-public crate-not_copy-1.0.0 (c (n "not_copy") (v "1.0.0") (h "0mkifslyllri2a660fvdjqm5jhl3l1amq9pcwgks740lwl2njv4g")))

(define-public crate-not_copy-1.0.1 (c (n "not_copy") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12sr4drw6lcbvqmnfdvy8k5wgkmibzm8n081rgyv8mgmi0j8gk0c")))

