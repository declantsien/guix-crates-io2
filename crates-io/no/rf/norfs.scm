(define-module (crates-io no rf norfs) #:use-module (crates-io))

(define-public crate-norfs-0.0.0 (c (n "norfs") (v "0.0.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1") (d #t) (k 2)))) (h "1qjr93hdh7s3p6sfp49k6zvi5j3dipxrif8iq5p9v8jjbmsgrbmz") (f (quote (("internal-flash" "esp32s3") ("esp32s3" "internal-flash")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

