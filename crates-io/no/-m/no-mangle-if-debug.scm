(define-module (crates-io no -m no-mangle-if-debug) #:use-module (crates-io))

(define-public crate-no-mangle-if-debug-0.1.0 (c (n "no-mangle-if-debug") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z07lisvv1lijqkf86l8x4rv7x3d3fp275skh15j9pdv70lknv2c")))

