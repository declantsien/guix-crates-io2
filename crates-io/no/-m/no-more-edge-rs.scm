(define-module (crates-io no -m no-more-edge-rs) #:use-module (crates-io))

(define-public crate-no-more-edge-rs-0.1.0 (c (n "no-more-edge-rs") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (k 0)))) (h "0ywbg1qx2jd2iw184sn6xinm7kxajmjdrv9h6zvndbcxs8yh515v")))

