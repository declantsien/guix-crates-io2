(define-module (crates-io no gg noggin-parser) #:use-module (crates-io))

(define-public crate-noggin-parser-0.1.0 (c (n "noggin-parser") (v "0.1.0") (d (list (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "091jfkyjk01ksvng30a5rpjkdwhf4xi331r53qr3nz4hxmivj9hg")))

