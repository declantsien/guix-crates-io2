(define-module (crates-io no gg noggin-derive) #:use-module (crates-io))

(define-public crate-noggin-derive-0.1.0 (c (n "noggin-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1xwaqx5hys74dpbzkm7k0kz1lzllm6yfwc21bq5c175srannrjqw")))

