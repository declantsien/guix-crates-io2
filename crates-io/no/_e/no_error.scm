(define-module (crates-io no _e no_error) #:use-module (crates-io))

(define-public crate-no_error-0.0.0 (c (n "no_error") (v "0.0.0") (d (list (d (n "no_error_macro") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "18xbv81ay956hd6h7mwznj9wh3ybznkxmydsrg5rynhmkzk4g2xb")))

(define-public crate-no_error-0.0.1 (c (n "no_error") (v "0.0.1") (d (list (d (n "no_error_macro") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0f015c4vvsv99hywzcbmzm30hpbi0ghqhiw0197kyi9n5as5cfjm")))

(define-public crate-no_error-0.0.2 (c (n "no_error") (v "0.0.2") (d (list (d (n "no_error_macro") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1dj5558rdzcc5wnin7qdrm63d9xk27cdp3xjw4amivmd4gypf01n")))

