(define-module (crates-io no _e no_error_macro) #:use-module (crates-io))

(define-public crate-no_error_macro-0.0.0 (c (n "no_error_macro") (v "0.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0y7204208savnfpy341zh8r95k37cy1rrw9dv0vn20vxn5gx5ksh")))

(define-public crate-no_error_macro-0.0.1 (c (n "no_error_macro") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0hlfmbblrch0122j4r73phfzfyrg10z88jy3gcbsfzh1wa1vqswl")))

