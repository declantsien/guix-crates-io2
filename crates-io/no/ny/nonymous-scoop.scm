(define-module (crates-io no ny nonymous-scoop) #:use-module (crates-io))

(define-public crate-nonymous-scoop-0.0.0 (c (n "nonymous-scoop") (v "0.0.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nonymous") (r "^0.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0spsvv24xky56p37pnfz60icl515yz9m8fc8qqx9qshaq38lw6cj") (f (quote (("strict"))))))

(define-public crate-nonymous-scoop-0.0.1 (c (n "nonymous-scoop") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nonymous") (r "^0.0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 0)))) (h "1scp2w8dg4jwc7a9n1nczybhgzfw9a0zy2aa31jydbhdprcisp7f") (f (quote (("strict"))))))

(define-public crate-nonymous-scoop-0.0.2 (c (n "nonymous-scoop") (v "0.0.2") (d (list (d (n "bore") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.2") (f (quote ("macros" "time" "io-util" "udp"))) (d #t) (k 0)))) (h "0ic8bzybkaayhilaba9hwa0yk1l8fj73776l4qvsmm63i72z04sp")))

