(define-module (crates-io no ri noria-mir) #:use-module (crates-io))

(define-public crate-noria-mir-0.4.0 (c (n "noria-mir") (v "0.4.0") (d (list (d (n "common") (r "^0.4.0") (d #t) (k 0) (p "noria-common")) (d (n "dataflow") (r "^0.4.0") (d #t) (k 0) (p "noria-dataflow")) (d (n "nom-sql") (r "^0.0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "slog") (r "^2.4.0") (d #t) (k 0)))) (h "1h5ni7kplc65wgz8xym9nk9j7sdy8xn2sll645g2zvpraniz4qfq")))

(define-public crate-noria-mir-0.6.0 (c (n "noria-mir") (v "0.6.0") (d (list (d (n "common") (r "^0.6.0") (d #t) (k 0) (p "noria-common")) (d (n "dataflow") (r "^0.6.0") (d #t) (k 0) (p "noria-dataflow")) (d (n "nom-sql") (r "^0.0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "slog") (r "^2.4.0") (d #t) (k 0)))) (h "1iclyp6crss5kpi96ap8ww687gi26p3k39l4yxkcxg2kkw3jz3md")))

