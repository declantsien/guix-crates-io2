(define-module (crates-io no ri noria-common) #:use-module (crates-io))

(define-public crate-noria-common-0.4.0 (c (n "noria-common") (v "0.4.0") (d (list (d (n "arccstr") (r "^1.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "noria") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "1kz6f5g65img45anxvs8cprsiibwhir4pxnis5izc0gkv44dwf8p")))

(define-public crate-noria-common-0.6.0 (c (n "noria-common") (v "0.6.0") (d (list (d (n "arccstr") (r "^1.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "noria") (r "^0.6.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "slog") (r "^2.4.0") (d #t) (k 0)))) (h "0mv1677nkjcsz1c46ql4shw0vi9h7clank77a2d83mklyhhfpcym")))

