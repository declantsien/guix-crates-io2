(define-module (crates-io no ri nori) #:use-module (crates-io))

(define-public crate-nori-0.1.0 (c (n "nori") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "osrmreader") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a29gsy79c473lgv8m0a1j2w6bclyg7qqcic85lvnjbqwd66f35y")))

