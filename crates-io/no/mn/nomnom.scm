(define-module (crates-io no mn nomnom) #:use-module (crates-io))

(define-public crate-nomnom-0.0.1 (c (n "nomnom") (v "0.0.1") (h "1yj54ykbmp9vkk8rvizbsgjz9v5jpdyf32bdidh8ili8jxg0w134") (y #t)))

(define-public crate-nomnom-0.0.2 (c (n "nomnom") (v "0.0.2") (h "0qql9l8qcq5lwxh4x2k9rkvn8ay8d5a9qxh27pbq5nljzbmhwy51") (y #t)))

(define-public crate-nomnom-0.0.3 (c (n "nomnom") (v "0.0.3") (h "0rdrw9xlca6kfc7z3li33zrm8w8m7ch9v6m78mnlvr5lqffb3dwz") (y #t)))

(define-public crate-nomnom-0.0.4 (c (n "nomnom") (v "0.0.4") (h "0ds3c13wfqc7qbwg3601g1jal5kx6ymlkx81dsl2wry5i81lsfrr") (y #t)))

