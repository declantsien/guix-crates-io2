(define-module (crates-io no rp norpc-macros) #:use-module (crates-io))

(define-public crate-norpc-macros-0.4.0 (c (n "norpc-macros") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hrhzpszd6wc25zbb5r181f79qbl4aivjimjxwzgdfamdwpgsnij")))

(define-public crate-norpc-macros-0.4.1 (c (n "norpc-macros") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12w61ad4masdrqk8vh7vxb8drav29hw6ksh2ripy4bv2ayziz3j0")))

(define-public crate-norpc-macros-0.5.0 (c (n "norpc-macros") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dy971i5dan5fd2isld5swkrscl7nhb1qqwc382kd07g41j4n1fb")))

(define-public crate-norpc-macros-0.5.1 (c (n "norpc-macros") (v "0.5.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zjshr2p7v096lilxr3p6pphsp80xfypwdhpqba32cz76g2gp979")))

(define-public crate-norpc-macros-0.5.2 (c (n "norpc-macros") (v "0.5.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h58mwbbp41ddfypxrw68bbg5490kc31ffz81zxazb2kg0bppv1i")))

(define-public crate-norpc-macros-0.6.0 (c (n "norpc-macros") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01sdc923hdavisqryv8bb06xsrixrxm5yhj956rivwg7rq5c8srb")))

(define-public crate-norpc-macros-0.6.1 (c (n "norpc-macros") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04j6qhcyz0vmm481kkyiz4h6xpxs5rkdhbv3hdvbd2m437wnfy6r")))

(define-public crate-norpc-macros-0.7.0 (c (n "norpc-macros") (v "0.7.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3qsv1pdhcnaci7r7majsg47yprb1jflyw4jpzkabmxgd343z0p")))

(define-public crate-norpc-macros-0.8.0 (c (n "norpc-macros") (v "0.8.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r24gs1p5da1nfqxw8cvws059xmq0xa07bz191d9wsgk6dybfp13")))

(define-public crate-norpc-macros-0.8.1 (c (n "norpc-macros") (v "0.8.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10fsckly63h1p7hl2ppyq5d6nl053wvcfafclad9kxky2553l2iv")))

(define-public crate-norpc-macros-0.9.0 (c (n "norpc-macros") (v "0.9.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1chvr9vav0lzixfbz1diwbm0mi58q6zskpj7lqqd2n05vjbkllsr")))

(define-public crate-norpc-macros-0.9.1 (c (n "norpc-macros") (v "0.9.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03mj0kk77yf9g4prar7c61wscbvyjy4kbpqhh41znkr77a94y9jv")))

