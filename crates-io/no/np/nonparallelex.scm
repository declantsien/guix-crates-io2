(define-module (crates-io no np nonparallelex) #:use-module (crates-io))

(define-public crate-nonparallelex-0.2.0 (c (n "nonparallelex") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 2)))) (h "0hkqg8g2m83im93jhqf65725g6ha58ccn9hrxg3x019nxxxii4vg")))

