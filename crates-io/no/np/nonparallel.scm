(define-module (crates-io no np nonparallel) #:use-module (crates-io))

(define-public crate-nonparallel-0.1.0 (c (n "nonparallel") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmpk06gv9afdcc5700qrirh593xxdpymj8v488p6q6hjjfkc5hj")))

(define-public crate-nonparallel-0.1.1 (c (n "nonparallel") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9n0m582h5a7br6i0p80bvw7c3vcqvypjvc2mrb9sk9jy6hn7zr")))

