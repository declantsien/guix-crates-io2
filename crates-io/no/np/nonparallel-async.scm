(define-module (crates-io no np nonparallel-async) #:use-module (crates-io))

(define-public crate-nonparallel-async-0.1.0 (c (n "nonparallel-async") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "parking_lot" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z1i3cf22r1yyjwkq5wbxn7v181aba4b3lb6ngzm67daigbgxy0x")))

