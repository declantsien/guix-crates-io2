(define-module (crates-io no ne nonempty-collections) #:use-module (crates-io))

(define-public crate-nonempty-collections-0.1.0 (c (n "nonempty-collections") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13kz4y4dxqgnljj630hh9r7w5bn96gd3l692rkhhzc4gmi0b0jlr")))

(define-public crate-nonempty-collections-0.1.1 (c (n "nonempty-collections") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1y9p8mmflqbqqmy937q0m53imd79h2mg20gbx5h8fwakckkjsil6")))

(define-public crate-nonempty-collections-0.1.2 (c (n "nonempty-collections") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "093a4585x0kk6vg27vc0vk6gglf99ls1ggm86pw3ryy9bx0s5l0p")))

(define-public crate-nonempty-collections-0.1.3 (c (n "nonempty-collections") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "150x64mwn4dpy4mcjay52jz0gpvb83r0fzbv3wrqhmg5178w431x")))

(define-public crate-nonempty-collections-0.1.4 (c (n "nonempty-collections") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "074psg71pi4ndsmvqz7cww9fmfsdkbckjpad50bgnkqsfsbiiw82")))

(define-public crate-nonempty-collections-0.1.5 (c (n "nonempty-collections") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0q6zdp9qyvpbixw2bi15h0nysp80c05fgwkhdq8h47m6xhp40fz4")))

(define-public crate-nonempty-collections-0.2.0 (c (n "nonempty-collections") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0fp0v64zlfag71m4q4pgxvn16np58gxbzvissfqic00ql02pam25") (y #t)))

(define-public crate-nonempty-collections-0.2.1 (c (n "nonempty-collections") (v "0.2.1") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1n622rrc1i2nsdlb0dp666v597kkf4q8fls7wpbn50dp0z3hsdq3") (y #t)))

(define-public crate-nonempty-collections-0.2.2 (c (n "nonempty-collections") (v "0.2.2") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d9waxjj05rlbsys91yng608117xs0ssmdb3z8yc380pzg0hrm7i") (y #t)))

(define-public crate-nonempty-collections-0.2.3 (c (n "nonempty-collections") (v "0.2.3") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lj3wq2r9qqh0km8dpzsqhv4gbx9j6q7x6ryg71wpy03w7ihlxcr")))

(define-public crate-nonempty-collections-0.2.4 (c (n "nonempty-collections") (v "0.2.4") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "05h9bgs5j09l9cb2r0pwz8q88q4kwqqv9r4l3vwmj4kwkzyz4yxx")))

(define-public crate-nonempty-collections-0.2.5 (c (n "nonempty-collections") (v "0.2.5") (d (list (d (n "indexmap") (r "^2.2.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kbgzvsiv3c2j9bgv059ky9dxchz8vpm6rz5iilfyl4immbk516f")))

