(define-module (crates-io no ne none-array) #:use-module (crates-io))

(define-public crate-none-array-1.0.0 (c (n "none-array") (v "1.0.0") (h "1mc5sxab68y31c3vdq70wllsqwnwcijm2h6jk1vamvh7xljqqv3r")))

(define-public crate-none-array-1.0.1 (c (n "none-array") (v "1.0.1") (h "0wnv64b70iw76rbbjlf27d7l5954cw87i08q54iayki2lzyf9nnq")))

