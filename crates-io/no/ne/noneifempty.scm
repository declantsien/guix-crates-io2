(define-module (crates-io no ne noneifempty) #:use-module (crates-io))

(define-public crate-noneifempty-0.1.0 (c (n "noneifempty") (v "0.1.0") (h "19bvy91zq881jw05gra24p8kkylp406z881dffzqg2m09visf77m")))

(define-public crate-noneifempty-0.1.1 (c (n "noneifempty") (v "0.1.1") (h "1sind2sjyqz1j6s9jsair9dx66iggmfgxdidla1ny0jb3h6v4xd4")))

(define-public crate-noneifempty-0.1.2 (c (n "noneifempty") (v "0.1.2") (h "07y0imd8aizpm88kycjr3a15hr2jvwncqr6ds500k89ga93lkmxa")))

(define-public crate-noneifempty-0.1.3 (c (n "noneifempty") (v "0.1.3") (h "05j99ik5w92qlvn5mjcx7qqwwq2j0b80znr7qac6kvrp8266idcp")))

