(define-module (crates-io no n_ non_blocking_mutex) #:use-module (crates-io))

(define-public crate-non_blocking_mutex-1.0.0 (c (n "non_blocking_mutex") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.9") (d #t) (k 0)))) (h "0vav93z6hs4z5wlnqqhy392gl3p6n5y4z4gv150mslirc02x4w7v")))

(define-public crate-non_blocking_mutex-1.0.1 (c (n "non_blocking_mutex") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.9") (d #t) (k 0)))) (h "110qrdcc4353xk7r4l3fr7cylbrksqjzx1hky43xsdck7p35w6dk")))

(define-public crate-non_blocking_mutex-1.0.2 (c (n "non_blocking_mutex") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.9") (d #t) (k 0)))) (h "0y2znhwk7gdskw16kmv3r7dbgmz6vb8jcgrsr0a1yvaa6nzb15xv")))

(define-public crate-non_blocking_mutex-1.0.3 (c (n "non_blocking_mutex") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.9") (d #t) (k 0)))) (h "0h5b23zmbhfg1d47l90v4xpzbvdpkbci6iyr2fqblvip00khjgpn")))

(define-public crate-non_blocking_mutex-1.0.4 (c (n "non_blocking_mutex") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.10") (d #t) (k 0)))) (h "0082f3gl71w1sg38xqslslah6sd1rnqz87qidrb752mbw2r1xb7k")))

(define-public crate-non_blocking_mutex-1.0.5 (c (n "non_blocking_mutex") (v "1.0.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.17") (d #t) (k 0)))) (h "0xlj70aslb7wysilc0y8j00ky7g12rsfzic58pik0v51gz4cccps")))

(define-public crate-non_blocking_mutex-1.0.6 (c (n "non_blocking_mutex") (v "1.0.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.18") (d #t) (k 0)))) (h "02s093qlqw9fhdh4bixgqfi2x3n3yy873l4fr2wqk709px32lqwy")))

(define-public crate-non_blocking_mutex-1.0.7 (c (n "non_blocking_mutex") (v "1.0.7") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.19") (d #t) (k 0)))) (h "15j6mf8pnqs2is83lhnlcjjfzrqkfplvnmdsa5jf5p66ijvw516p")))

(define-public crate-non_blocking_mutex-2.0.0 (c (n "non_blocking_mutex") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.22") (d #t) (k 0)))) (h "0acxy0hrv631in4zb5is9vkhlgp2pvw1xazbc71hn5svdy1975z6")))

(define-public crate-non_blocking_mutex-2.0.1 (c (n "non_blocking_mutex") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.23") (d #t) (k 0)))) (h "0bblfaws2dxdp4ahfvhfvdp3yzv9v5zzynx5gjn7z6hcskd6lbcz")))

(define-public crate-non_blocking_mutex-2.0.2 (c (n "non_blocking_mutex") (v "2.0.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.24") (d #t) (k 0)))) (h "0lq2lgk82jz0bc1nswcz5m7na6h76lf7q1radlq3srvcmi3nyhnh")))

(define-public crate-non_blocking_mutex-2.0.3 (c (n "non_blocking_mutex") (v "2.0.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.25") (d #t) (k 0)))) (h "00kpxyv3ymyn11s6l3ppqjmldraxgmablvwk4yc8q2jms8kpls7h")))

(define-public crate-non_blocking_mutex-2.0.4 (c (n "non_blocking_mutex") (v "2.0.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.26") (d #t) (k 0)))) (h "0sgpsvp6gl2g03b4xm05v0p94bz8np2pf6skikqbs12q69q8wp0i")))

(define-public crate-non_blocking_mutex-2.0.5 (c (n "non_blocking_mutex") (v "2.0.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^1.0.26") (d #t) (k 0)))) (h "008g9dllf6gqwvkkwzmav0xhp80xnakrrrib8sbgspqcrbmc0zh8")))

(define-public crate-non_blocking_mutex-3.0.0 (c (n "non_blocking_mutex") (v "3.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^2") (d #t) (k 0)))) (h "1d1sl6y6wjwc8k47hl927kq6ph43zxnzjhrkbs19acivvlypmxjl")))

(define-public crate-non_blocking_mutex-3.1.0 (c (n "non_blocking_mutex") (v "3.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^2") (d #t) (k 0)))) (h "19fz2s3fcc6qdnx44jr6hyizq02h8gdpwl9v15bzs3mg9pq6m9sx")))

(define-public crate-non_blocking_mutex-3.1.1 (c (n "non_blocking_mutex") (v "3.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^2") (d #t) (k 0)))) (h "03gldf2w62gzj66818w3q55l1q7vckkynkijagp9lh0ckj0xs77y")))

(define-public crate-non_blocking_mutex-3.1.2 (c (n "non_blocking_mutex") (v "3.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^2") (d #t) (k 0)))) (h "11zvcs8pnv99j2ybh6g0zzvk8rycvxjp3cv5503hhp1f43q7kgnd")))

(define-public crate-non_blocking_mutex-3.1.3 (c (n "non_blocking_mutex") (v "3.1.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)) (d (n "sharded_queue") (r "^2") (d #t) (k 0)))) (h "0j3l8b1z2sq2y7nbvd30d2b52lrvhfqfcvzpv3hgccx14flhq28j")))

