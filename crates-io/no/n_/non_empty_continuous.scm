(define-module (crates-io no n_ non_empty_continuous) #:use-module (crates-io))

(define-public crate-non_empty_continuous-0.1.0 (c (n "non_empty_continuous") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.13.2") (o #t) (d #t) (k 0)) (d (n "static_assert_generic") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0slvqj19l32r4m3b09s885pzpz9d4hk2rsxc55ngq6csigwh2ifb") (s 2) (e (quote (("static_assert_generic" "dep:static_assert_generic") ("smallvec" "dep:smallvec"))))))

