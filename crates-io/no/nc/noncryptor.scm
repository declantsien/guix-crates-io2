(define-module (crates-io no nc noncryptor) #:use-module (crates-io))

(define-public crate-noncryptor-0.1.0 (c (n "noncryptor") (v "0.1.0") (h "1zqcgnj1rss1ca44sa0byl0a898d8m8rwmr0nisk85i0k5da5klk")))

(define-public crate-noncryptor-0.1.1 (c (n "noncryptor") (v "0.1.1") (h "0z47jzpsbx0w67wzvywravbz0v6nqnxz51z46a02w9ysscb8v2c9")))

(define-public crate-noncryptor-0.1.2 (c (n "noncryptor") (v "0.1.2") (h "0h8n5lfm3vidxvkpbb42hdb9m6na1yzdfhv1267cs0j0gxywdjwz")))

