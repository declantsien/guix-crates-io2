(define-module (crates-io no nc nonce-extension) #:use-module (crates-io))

(define-public crate-nonce-extension-0.1.1 (c (n "nonce-extension") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 2)))) (h "1h0snwiv5zjps2kjpkxvvb48xxzsmwkmsib2v3idknz635xqc0gd")))

(define-public crate-nonce-extension-0.1.2 (c (n "nonce-extension") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 2)))) (h "1jmr6q7iscrzasgg3yqwj82hza728q0jqmxa58dgqhbpd8nwfql5")))

(define-public crate-nonce-extension-0.2.0 (c (n "nonce-extension") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 2)))) (h "1siay90hs5bpwc9sl7bg26c5fxrxmsrsz43b6knf21ln0ldh2gfd")))

