(define-module (crates-io no _p no_proto) #:use-module (crates-io))

(define-public crate-no_proto-0.0.0-beta.1 (c (n "no_proto") (v "0.0.0-beta.1") (d (list (d (n "json") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1aw46170rznhgxalqgdinwd14h15lqxyc7f7wd1yjizllfrvcpkq")))

(define-public crate-no_proto-0.0.0-beta.2 (c (n "no_proto") (v "0.0.0-beta.2") (d (list (d (n "json") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1anqsfb5c0rc6cilkpzcrppp3clynxr8kdpjx40z2699pif8c8g0")))

(define-public crate-no_proto-0.0.0-beta.3 (c (n "no_proto") (v "0.0.0-beta.3") (h "1n6h13iq5mzic78cvrlfj1ldzvw2dblpn9hr2f59vijgaqcznigs")))

(define-public crate-no_proto-0.0.0-beta.4 (c (n "no_proto") (v "0.0.0-beta.4") (h "0pg8mm6073m7nlkl8r8zbv2knav83hhdjdbviwkn4zswn4bw92n6")))

(define-public crate-no_proto-0.0.0-beta.5 (c (n "no_proto") (v "0.0.0-beta.5") (h "1zql2vr6kv4x47xb9qpf6rcx2siknc3gzx48zh4hli7q2yj3llgd")))

(define-public crate-no_proto-0.0.1 (c (n "no_proto") (v "0.0.1") (h "0dlr6knhnf1mxqjdywrn69ljsvmjn8vnnn92l984pkn5f4ibnkaf")))

(define-public crate-no_proto-0.0.2 (c (n "no_proto") (v "0.0.2") (h "0s1xlpqgsmxhny74jcncidgsd9975dqzgxzikp36kc6lbrb7k2h5")))

(define-public crate-no_proto-0.0.3 (c (n "no_proto") (v "0.0.3") (h "17ngpr11c8yr5snn80z2k7h2yzfdycji146mbl251y7ikgl69ipj")))

(define-public crate-no_proto-0.0.4 (c (n "no_proto") (v "0.0.4") (h "02j3drqk8642plyrxvkrsmmbzna8h67hs8d6anlansarj17v97m2")))

(define-public crate-no_proto-0.0.5 (c (n "no_proto") (v "0.0.5") (h "108wc7pxqxa1fa4m6d586php9knlnqhcq7xschgh6ca7nkvnyp54")))

(define-public crate-no_proto-0.1.0 (c (n "no_proto") (v "0.1.0") (h "1sq0x4qd674syqigzwi2dg4b6x54x3brpbgs809hmcmfhqkaj1p9")))

(define-public crate-no_proto-0.1.1 (c (n "no_proto") (v "0.1.1") (h "1vvyrr5pcx1gwp25xw4qfddjgsj8643p0gr1zzxhdc62c5xsz3ip")))

(define-public crate-no_proto-0.1.2 (c (n "no_proto") (v "0.1.2") (h "024x227bq0q92584la2qdjgvraw75hp733gb0w7y5zlqzsrb3phn")))

(define-public crate-no_proto-0.2.0 (c (n "no_proto") (v "0.2.0") (h "03cxl1jqggmjfgzx5fsz14cvgszfhhygsrc6hanpcgkx1imzhjjg")))

(define-public crate-no_proto-0.2.1 (c (n "no_proto") (v "0.2.1") (h "1fgsarw8hp671wldyrjjwdi0qqshcgbljk4wz2v81dn1g6xd3cpm")))

(define-public crate-no_proto-0.2.2 (c (n "no_proto") (v "0.2.2") (h "09wa1rc4fnmlvz7dds717aqxps144n1wfchnqdikslnr9wxjrrkq")))

(define-public crate-no_proto-0.3.0 (c (n "no_proto") (v "0.3.0") (h "11i57a1wcy09721m4bla1gni1fi9d315l97cg6x3i3hpixz7fwx8")))

(define-public crate-no_proto-0.4.0 (c (n "no_proto") (v "0.4.0") (h "1vhnmfb12vy97pqbcybc21lrb9kci58kqr7fbgzw8ipsjds6pn26")))

(define-public crate-no_proto-0.4.1 (c (n "no_proto") (v "0.4.1") (h "1rdyhkk1p756yycm8j025pqzd7hislrwijpmf9n60h22pag3vvhf")))

(define-public crate-no_proto-0.4.2 (c (n "no_proto") (v "0.4.2") (h "1haf6251jbdks9mgbz86kg36npgy9nlnpvjy3ypahp57zph288z0")))

(define-public crate-no_proto-0.5.0 (c (n "no_proto") (v "0.5.0") (h "1r4i286a6w06a1d6vjdlw9cs1jb6w1l0z7j8yd9r15rwnyzgmwmp")))

(define-public crate-no_proto-0.5.1 (c (n "no_proto") (v "0.5.1") (h "0lavwa4zlp2zx3da9ai7plwn4llg1x62kcvrcm9qsarpdbc42kfm")))

(define-public crate-no_proto-0.6.0 (c (n "no_proto") (v "0.6.0") (h "19hsfw7bpspn0lxlhkyygbwskmxksfl0s7aawg96wlrxrpwqp62h")))

(define-public crate-no_proto-0.6.1 (c (n "no_proto") (v "0.6.1") (h "1mpf9dxbbmqyxj8r69n0jvwk6cwkwc4kmds112ma51i491q098vh")))

(define-public crate-no_proto-0.6.2 (c (n "no_proto") (v "0.6.2") (h "12vr8fc3hjphp6v53373fzr4lf1gxgqph4xrwnr4iz05x4sky70b") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.6.3 (c (n "no_proto") (v "0.6.3") (h "003ryik189c2nvbva4i92mhgmnh2az6fc0r325h6hgm2w1hhwvib") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7.0 (c (n "no_proto") (v "0.7.0") (h "0s2bq6n99mzx2gmbz1x6ix9h93zj01mp3nd0lciqlhrwwzfh4m8n") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7.1 (c (n "no_proto") (v "0.7.1") (h "1qlcy22l5k7ibl3lq43i1yb9i1rd4p44knqg4jx6r596l384q952") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7.2 (c (n "no_proto") (v "0.7.2") (h "0h11kh3qw7wz2hmkdl3kfylkcs9sgy74zncijm8w9xv7gi3pfd3w") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7.3 (c (n "no_proto") (v "0.7.3") (h "1lklk3wzid187kipvvgsfdaabxfng5g8zmg8c7r1gmksf2bzqqvd") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7.4 (c (n "no_proto") (v "0.7.4") (h "0rkcz37bkgjkf482qy38w2f7dba8n8qy9yh99nws3v3cn87pcqbn") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.8.0 (c (n "no_proto") (v "0.8.0") (h "1iyrpxzbf7wdzy1v6xa37fxvdj2rv6ca1xnxp4a7cfjmqj2r2gsh") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.0 (c (n "no_proto") (v "0.9.0") (h "0cl8yx2r0wpn47n47hk79mz52f0f5ia7c1ja7k5r3iwa13ls8m69") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.1 (c (n "no_proto") (v "0.9.1") (h "1h4zz7dicr5hxfxqcvlcd4vz35hr1zg2igxhw69a633wmc76izpb") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.2 (c (n "no_proto") (v "0.9.2") (h "1w8da8868gdmzr1cl6frxx444dxbzsyyp6cvrayjkvilvawjghwy") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.3 (c (n "no_proto") (v "0.9.3") (h "0bvrqllqnvf39qvdk7wvah7cnrk55j1s0r24740idfrxax3wd4m6") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.4 (c (n "no_proto") (v "0.9.4") (h "19s72yp1pwys9739d38apg83iivghcpipr2mlb9zg1wsyiv62cdz") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.5 (c (n "no_proto") (v "0.9.5") (h "1f43838rv51zf52bq2gw140g2y5lz73xqyjb97yij317siwmq43i") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.51 (c (n "no_proto") (v "0.9.51") (h "1wy1v53vawpqi7icqlb0n8g73g37ndgkhci3bh0h4ccqjwnsb67z") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.6 (c (n "no_proto") (v "0.9.6") (h "1g8mw1a164266f38aji33d8npgrhm77m6p99yvcvln2ywmphqq3b") (f (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9.60 (c (n "no_proto") (v "0.9.60") (h "06nifnhpnnfcdnj0kx68jis25w9lmp8fy2z5qqqa2zbwrdfcm4kd") (f (quote (("np_rpc") ("default" "np_rpc"))))))

