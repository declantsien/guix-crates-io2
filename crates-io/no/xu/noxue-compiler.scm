(define-module (crates-io no xu noxue-compiler) #:use-module (crates-io))

(define-public crate-noxue-compiler-0.1.0 (c (n "noxue-compiler") (v "0.1.0") (h "1gjmj7m4vs3j9v39fk0ik1c2vmv3f46w8kr9wcgzhavfdlfxjfpf")))

(define-public crate-noxue-compiler-0.1.1 (c (n "noxue-compiler") (v "0.1.1") (h "18pm6xh5c0z0v0sgpnmyidqhdavd1v70k120lrncsi03kqs4abm8")))

(define-public crate-noxue-compiler-0.1.2 (c (n "noxue-compiler") (v "0.1.2") (h "0pa4l13r4cia34aikhhh3f2ya0vqq5cnpb9g79kkikisqnizd20a")))

(define-public crate-noxue-compiler-1.0.0 (c (n "noxue-compiler") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xqz8jkdfbfxkz7ih0jzqclrz9mc1k1lh73d7d7mm6y17pyxc0bv")))

(define-public crate-noxue-compiler-1.0.1 (c (n "noxue-compiler") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j712z42fbava2b2ls8i04fndw1qiwxr7isbias88gj0v6vjkpch")))

(define-public crate-noxue-compiler-1.0.2 (c (n "noxue-compiler") (v "1.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "171bb45wk2hb6h9vq18b9ygnbm4svym9qh3ajfs1504xz67vvi3n")))

(define-public crate-noxue-compiler-1.0.3 (c (n "noxue-compiler") (v "1.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "045x2pzzz5myzwqah3phgcy8vbwfc62bsp851wc1dqcbz2mbddfp")))

(define-public crate-noxue-compiler-1.0.4 (c (n "noxue-compiler") (v "1.0.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mh12s3q8qc8ykjaq33xixs2gvcpk9d0621x2q2nzhn0r1k3s69k")))

