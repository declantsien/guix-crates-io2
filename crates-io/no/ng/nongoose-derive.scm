(define-module (crates-io no ng nongoose-derive) #:use-module (crates-io))

(define-public crate-nongoose-derive-0.1.0-beta.1 (c (n "nongoose-derive") (v "0.1.0-beta.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h0m89ghnzv3max28mmks0k7mbryif9c796aiyhig4nfna060cyp")))

