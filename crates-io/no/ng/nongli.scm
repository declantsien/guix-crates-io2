(define-module (crates-io no ng nongli) #:use-module (crates-io))

(define-public crate-nongli-0.1.0 (c (n "nongli") (v "0.1.0") (d (list (d (n "anstyle") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "unstable-doc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "109628kp09rczvhspqnbm9avqcjl316r4y6hv042vwyv43m8cvcj")))

(define-public crate-nongli-0.1.1 (c (n "nongli") (v "0.1.1") (d (list (d (n "anstyle") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "unstable-doc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n6wdl50sh14br6yi38p9d64qydb6a43jyf573bvrg14djjiq0ki")))

