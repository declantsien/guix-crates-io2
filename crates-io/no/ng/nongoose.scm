(define-module (crates-io no ng nongoose) #:use-module (crates-io))

(define-public crate-nongoose-0.1.0-beta.1 (c (n "nongoose") (v "0.1.0-beta.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mongodb") (r "^2.1.0") (f (quote ("bson-chrono-0_4" "bson-uuid-0_8" "sync"))) (k 0)) (d (n "nongoose-derive") (r "^0.1.0-beta.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "1wvd7j4a8yymv91bc522i1z7zsgrwpa0m13nhzw2gf79s6fjyqjf") (f (quote (("tokio-runtime" "async-trait" "tokio") ("sync") ("derive" "nongoose-derive") ("default" "derive" "tokio-runtime"))))))

