(define-module (crates-io no w_ now_playing) #:use-module (crates-io))

(define-public crate-now_playing-0.1.0 (c (n "now_playing") (v "0.1.0") (d (list (d (n "mpris") (r "^2.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.11.0") (d #t) (k 0)))) (h "1v5dhjzvf1d6ri2cxqlkr7fyndga2s2v40f6clsjagv3l67mcraa")))

