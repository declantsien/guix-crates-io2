(define-module (crates-io no w_ now_lambda) #:use-module (crates-io))

(define-public crate-now_lambda-0.1.0 (c (n "now_lambda") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "18vv4g97h6js0y28hs4rna55lglz2nkvlzlyh9ay3yrnnv8kndn7")))

(define-public crate-now_lambda-0.1.1 (c (n "now_lambda") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0vy4cs85lv1bxyazsb306mx1cfz26f9yc3wfrd544h3yp2nn0vdg")))

(define-public crate-now_lambda-0.1.2 (c (n "now_lambda") (v "0.1.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1a9hgbh0064m7x8sd8rfchp80cn2ydsrb0zll0nn4rggxdf68a7v")))

(define-public crate-now_lambda-0.1.3 (c (n "now_lambda") (v "0.1.3") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1irhkg7v02p74640fzjlfsflsbnmh83kaxg2fc6qcyccxr8jpszq")))

