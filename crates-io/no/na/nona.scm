(define-module (crates-io no na nona) #:use-module (crates-io))

(define-public crate-nona-0.1.0 (c (n "nona") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamped") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1p2ksanklmff9bpc6ay9xc7ya6jr4w75p61nrsvfgc8fw7na124j")))

(define-public crate-nona-0.1.1 (c (n "nona") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamped") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "04wq6sqp6nrykww36qyps1lfd5npl6ri2mz4n060ik7xkkdz865w")))

(define-public crate-nona-0.1.2 (c (n "nona") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamped") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0akhmcyiigz8nbayi2lq5naa07v93mp15r5nlr6w6wzm0wqllksw")))

