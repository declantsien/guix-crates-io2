(define-module (crates-io no na nonaquad) #:use-module (crates-io))

(define-public crate-nonaquad-0.1.0 (c (n "nonaquad") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.37") (d #t) (k 0)) (d (n "nona") (r "^0.1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1yr9gjhakrcifs2rmx6ckvmy6zy6n8ixslckh82z39w0q88zx34a")))

(define-public crate-nonaquad-0.1.1 (c (n "nonaquad") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.37") (d #t) (k 0)) (d (n "nona") (r "^0.1.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0j6nw7pqdcl3k9ypqr5awbhqw9j7vgwsdxnypajwswmqzdc5wz4m")))

(define-public crate-nonaquad-0.1.2 (c (n "nonaquad") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.37") (d #t) (k 0)) (d (n "nona") (r "^0.1.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0fa5f0bj5m10lfrplq0mpcv42w00p460sw2brghsmfxad089bnkp")))

