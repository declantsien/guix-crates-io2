(define-module (crates-io no -p no-panic) #:use-module (crates-io))

(define-public crate-no-panic-0.1.0 (c (n "no-panic") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0k7q3zp2yp7p01ipxk3pxbs57hmslhs4gzcsp4nd49pm7xvsj6r8")))

(define-public crate-no-panic-0.1.1 (c (n "no-panic") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1fzhgjdbmd119pzyhbp9kcdvyaf9jlrqhkz8zpbgwl9hxn3rlfin")))

(define-public crate-no-panic-0.1.2 (c (n "no-panic") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "01qjfzf36w0xkmxqkvwn13vvfn25v0shqm7y8ckdqv8fj11cgs3y")))

(define-public crate-no-panic-0.1.3 (c (n "no-panic") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0ixnkmfddpylz2gprd0pm47xa4rf6w679ykj34abzbkqilq933ha")))

(define-public crate-no-panic-0.1.4 (c (n "no-panic") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1mc3nxkzjma45fznxybk1hnpfphv1jlw71fzh9whf3z72k7hwdil")))

(define-public crate-no-panic-0.1.5 (c (n "no-panic") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0ii76s1fcvg6k70ih7938qi5lvq3s64dxsldfj7fhk9snavw9xsr")))

(define-public crate-no-panic-0.1.6 (c (n "no-panic") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "05414dpavcxg40wjm7iiw95w6j7zlcgv63kii7zvm7dqhbxzivg8")))

(define-public crate-no-panic-0.1.7 (c (n "no-panic") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0q82y9izadr1yajn6xdc4d2dr7miv58m3qw25g3hhg9bvb15bcl2")))

(define-public crate-no-panic-0.1.8 (c (n "no-panic") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0fhaa0c0v5v1pcikjijqdnqnx77r2bnlg56pb5cv2wvj41b6qkx4")))

(define-public crate-no-panic-0.1.9 (c (n "no-panic") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0wvjqwgzm39jmaykq71vkscgna59dailvzcnxmzfin0m4aqhv6zx")))

(define-public crate-no-panic-0.1.10 (c (n "no-panic") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "089gmyxg7kviimqn5nmghm5kngnmi77a0c6fbv0j67jxx7pjhq3r")))

(define-public crate-no-panic-0.1.11 (c (n "no-panic") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0r3r67w0blgfh9l75jjd2isldy73x5siclsf33wd4p83cgwrdpwq")))

(define-public crate-no-panic-0.1.12 (c (n "no-panic") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0xan5v9ac1aklinc8aw16raq36pb4idjrl502np8gy32gfs6s751")))

(define-public crate-no-panic-0.1.13 (c (n "no-panic") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0azppm5n19b2jzj6hj8yqw3xzs3h3q98ysfwy6lgiackh5sj3nml")))

(define-public crate-no-panic-0.1.14 (c (n "no-panic") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1m89i8zykqbiiigrish27imkfjxl5v8dqvnl2d181r0z0101xvdd")))

(define-public crate-no-panic-0.1.15 (c (n "no-panic") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0dkzdwix0jsbarjbbx8mhvch7rawcw17jxd7742rg03s987lx21v")))

(define-public crate-no-panic-0.1.16 (c (n "no-panic") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1z6zq9zaa6n6c6h9mqjpmiqwq5wmyk44q7qpzkq3h8fgdm5hvw8j") (r "1.31")))

(define-public crate-no-panic-0.1.17 (c (n "no-panic") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "0h9pdaxi3zyi15pp3hj9zhx4nf9hf8ldh526sxvvy43xhsy0g218") (r "1.31")))

(define-public crate-no-panic-0.1.18 (c (n "no-panic") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1a5piyx69n6f3pxddd5yfvbjjcq1r4ax429ic71dvsfkpg9dbmnr") (r "1.31")))

(define-public crate-no-panic-0.1.19 (c (n "no-panic") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1j9m3y6ahn3z970hwxx9j46k5rpayrvnxcdsn7ihwsff6z5zfgwz") (r "1.31")))

(define-public crate-no-panic-0.1.20 (c (n "no-panic") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1iwa3lchvvwpn5k9agx1i9yr4911da1rwk3yfgrc8qnacfdrhv42") (r "1.31")))

(define-public crate-no-panic-0.1.21 (c (n "no-panic") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1k69qwv7z57zj20q0vxf16w6hrnb3zf9zlyprk75ayk8kaivhh77") (r "1.31")))

(define-public crate-no-panic-0.1.22 (c (n "no-panic") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fqflk07wmicr52sqrg4yx63z81ishr1aw7xpvb91nbby6ghrx3c") (r "1.56")))

(define-public crate-no-panic-0.1.23 (c (n "no-panic") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mn1bf29plm45wdq2445ajj2g5yx5nygrrk9k8637a6j387g844s") (r "1.56")))

(define-public crate-no-panic-0.1.24 (c (n "no-panic") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.69") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mbjz8b4zxbwg4rd61y8vhd83r3jfmwk0lq86bfwga89rdsvvsql") (r "1.56")))

(define-public crate-no-panic-0.1.25 (c (n "no-panic") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1whrf5azzhxz6dckzfpkzmaxyp2vhgq0kzcnzzkl7qfsi83cfqc1") (r "1.56")))

(define-public crate-no-panic-0.1.26 (c (n "no-panic") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0sp4d9815f8vdz31qm10wpnswi0x89hlbzw78nxy0p2g88kd39ki") (r "1.56")))

(define-public crate-no-panic-0.1.27 (c (n "no-panic") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "18lhgzrqfm2x4s0yicd8wimcng4h8p49xbvvyz05w57p38g0i65z") (r "1.56")))

(define-public crate-no-panic-0.1.28 (c (n "no-panic") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "108zx85k9rijv4zhl74crx1lvyvidr4kzcxhm11xr17558d86mpw") (r "1.56")))

(define-public crate-no-panic-0.1.29 (c (n "no-panic") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lpgg5yf0n6b5ryvksdi9a9jijv16nh61bbj6sxrcapcxlp544f7") (r "1.56")))

(define-public crate-no-panic-0.1.30 (c (n "no-panic") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0g8bfvbz36w1qzgn3y4pqgzdn34hxxm7f19aniw625i0kbcvfh45") (r "1.56")))

