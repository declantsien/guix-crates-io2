(define-module (crates-io no -p no-proxy) #:use-module (crates-io))

(define-public crate-no-proxy-0.1.0 (c (n "no-proxy") (v "0.1.0") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)))) (h "0p30qc865klppgla72zfabavdqn5zlcm9w0c4wgn8rjxj6wvbpx6")))

(define-public crate-no-proxy-0.2.0 (c (n "no-proxy") (v "0.2.0") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14hb68k3n3x07bf0k5pwdawggdlisw2aafbc8pb26rsr88lcs886") (f (quote (("serialize" "serde") ("default" "serialize"))))))

(define-public crate-no-proxy-0.2.1 (c (n "no-proxy") (v "0.2.1") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09jl5x115phm72b4c4hlnqickip2bllca38c8isjklh2431a84jz") (f (quote (("serialize" "serde") ("default" "serialize"))))))

(define-public crate-no-proxy-0.2.2 (c (n "no-proxy") (v "0.2.2") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1md0ib8sq3c3clmrgcnlmrpq7rn4r4r0iv4isydlpy7mc1sb8ry0") (f (quote (("serialize" "serde") ("default" "serialize"))))))

(define-public crate-no-proxy-0.3.0 (c (n "no-proxy") (v "0.3.0") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14yshsf1x1m71ymx149lzz31bw2h85fhzrc419wmp61m18xjsd6n") (f (quote (("serialize" "serde") ("default" "serialize"))))))

(define-public crate-no-proxy-0.3.1 (c (n "no-proxy") (v "0.3.1") (d (list (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0l8nzz7xkgk5a6vibiaccl3ibif5dgg71pi2gz0xqfc1xihklk6v") (f (quote (("serialize" "serde") ("default" "serialize"))))))

(define-public crate-no-proxy-0.3.2 (c (n "no-proxy") (v "0.3.2") (d (list (d (n "async-graphql") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "009x023fm7fdfq5yx1v5jdzj7g81j1jczm1fyb3a5iwjvdmq4jxj") (f (quote (("serialize" "serde") ("graphql" "async-graphql") ("default" "serialize"))))))

(define-public crate-no-proxy-0.3.3 (c (n "no-proxy") (v "0.3.3") (d (list (d (n "async-graphql") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rwy2066isa1gsdddl8w7qc682il2hpd32hh9s978dafqxn9biaw") (f (quote (("serialize" "serde") ("graphql" "async-graphql") ("default" "serialize"))))))

(define-public crate-no-proxy-0.3.4 (c (n "no-proxy") (v "0.3.4") (d (list (d (n "async-graphql") (r "^6.0") (o #t) (d #t) (k 0)) (d (n "cidr-utils") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xwcymig0bb00ykmyjjz0dwii8v2znx087j3jakqwry3km3yfh8v") (f (quote (("serialize" "serde") ("graphql" "async-graphql") ("default" "serialize"))))))

