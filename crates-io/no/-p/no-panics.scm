(define-module (crates-io no -p no-panics) #:use-module (crates-io))

(define-public crate-no-panics-0.0.1 (c (n "no-panics") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.77") (f (quote ("diff"))) (d #t) (k 2)))) (h "18fcpmszi56lcqmpbmfx6m7dfb7dnfijxwcnxncxz5b26vrrdsix") (r "1.39")))

