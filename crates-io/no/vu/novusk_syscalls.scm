(define-module (crates-io no vu novusk_syscalls) #:use-module (crates-io))

(define-public crate-novusk_syscalls-0.1.0 (c (n "novusk_syscalls") (v "0.1.0") (d (list (d (n "register") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "uefi") (r "^0.8.1") (d #t) (k 0)))) (h "1ysxj67xm481hws3ydm1a0ca47jhlmppznvch617ms66n1nbdy4k") (f (quote (("novusk_uefi" "register") ("baremetal_aarch64" "register"))))))

(define-public crate-novusk_syscalls-0.2.0 (c (n "novusk_syscalls") (v "0.2.0") (h "0w8ikwnsbw59yb04msi7rp1kw84x4yppz7rk66q7whapmpnhrnzq")))

(define-public crate-novusk_syscalls-0.2.1 (c (n "novusk_syscalls") (v "0.2.1") (h "0pgh15l4a5acnw1n6pf91hg6y3ml7nymi7qnl51s76n4pkdjfk1a")))

