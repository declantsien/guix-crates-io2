(define-module (crates-io no bl noble-node-authorization) #:use-module (crates-io))

(define-public crate-noble-node-authorization-2.0.0 (c (n "noble-node-authorization") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "0w25gbms3kk4cpfgh9nlan3lqk3mpj3p7jy44x08xzp50pdiqgnw") (f (quote (("std" "serde" "codec/std" "fabric-support/std" "fabric-system/std" "tet-core/std" "tet-io/std" "tp-runtime/std" "tetcore-std/std") ("default" "std"))))))

