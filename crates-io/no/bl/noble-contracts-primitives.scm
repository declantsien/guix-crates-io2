(define-module (crates-io no bl noble-contracts-primitives) #:use-module (crates-io))

(define-public crate-noble-contracts-primitives-2.0.0 (c (n "noble-contracts-primitives") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "056fjz0wwhflk9d1c35l0b4d0hbmjah459m4h3b035b1q5bh0hjd") (f (quote (("std" "codec/std" "tp-runtime/std" "tetcore-std/std") ("default" "std"))))))

