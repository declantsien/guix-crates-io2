(define-module (crates-io no bl noble-staking-reward-curve) #:use-module (crates-io))

(define-public crate-noble-staking-reward-curve-2.0.0 (c (n "noble-staking-reward-curve") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 2)))) (h "0m2qaml7lkmp5pinsrhjdk3lb8m4hhnf07d20z5gcvnwa4mazhmq")))

