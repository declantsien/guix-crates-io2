(define-module (crates-io no bl noble-sudo) #:use-module (crates-io))

(define-public crate-noble-sudo-2.0.0 (c (n "noble-sudo") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 2)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1g5q5mrpdrsjywh9zkj3qi5rs8a7gaxqziyddvmxs1gc45m5xhba") (f (quote (("std" "serde" "codec/std" "tetcore-std/std" "tet-io/std" "tp-runtime/std" "fabric-support/std" "fabric-system/std") ("default" "std"))))))

