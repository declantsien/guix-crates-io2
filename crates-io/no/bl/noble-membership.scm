(define-module (crates-io no bl noble-membership) #:use-module (crates-io))

(define-public crate-noble-membership-2.0.0 (c (n "noble-membership") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 2)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "09zdq3jdbngm3vmfah733sxqh3akjx03p6mgk1qqcgw3d15bwrmd") (f (quote (("std" "serde" "codec/std" "tp-runtime/std" "tetcore-std/std" "tet-io/std" "fabric-support/std" "fabric-system/std") ("default" "std"))))))

