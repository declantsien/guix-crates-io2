(define-module (crates-io no bl noble-template) #:use-module (crates-io))

(define-public crate-noble-template-2.0.0 (c (n "noble-template") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 2)) (d (n "tet-core") (r "^2.0.0") (k 2)) (d (n "tet-io") (r "^2.0.0") (k 2)) (d (n "tp-runtime") (r "^2.0.0") (k 2)))) (h "1yamzar5f6jz299i6xnicl19r8kw9mihvgf2gkacxshamayvbw34") (f (quote (("std" "codec/std" "fabric-support/std" "fabric-system/std") ("default" "std"))))))

