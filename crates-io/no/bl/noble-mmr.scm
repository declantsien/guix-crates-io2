(define-module (crates-io no bl noble-mmr) #:use-module (crates-io))

(define-public crate-noble-mmr-2.0.0 (c (n "noble-mmr") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "fabric-benchmarking") (r "^2.0.0") (o #t) (k 0)) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "mmr-lib") (r "^0.3.1") (k 0) (p "ckb-merkle-mountain-range")) (d (n "noble-mmr-primitives") (r "^2.0.0") (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "0450s25r2ydpmi45pd7fmb4kkv24rp2xfv15mracf2f9h8860lk6") (f (quote (("std" "codec/std" "fabric-benchmarking/std" "fabric-support/std" "fabric-system/std" "mmr-lib/std" "noble-mmr-primitives/std" "serde" "tet-core/std" "tet-io/std" "tp-runtime/std" "tetcore-std/std") ("runtime-benchmarks" "fabric-benchmarking") ("default" "std"))))))

