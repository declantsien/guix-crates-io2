(define-module (crates-io no bl noble-example-parallel) #:use-module (crates-io))

(define-public crate-noble-example-parallel-2.0.1 (c (n "noble-example-parallel") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)) (d (n "tp-tasks") (r "^2.0.1") (k 0)))) (h "1qaz54aanl9n2qmsrmyf989cblk82zs7pvbsp747a3zqa6r1bygf") (f (quote (("std" "codec/std" "fabric-support/std" "fabric-system/std" "tet-core/std" "tet-io/std" "tp-runtime/std" "tetcore-std/std" "tp-tasks/std") ("default" "std"))))))

