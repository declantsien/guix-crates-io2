(define-module (crates-io no bl noble-transaction-payment-rpc-runtime-api) #:use-module (crates-io))

(define-public crate-noble-transaction-payment-rpc-runtime-api-2.0.0 (c (n "noble-transaction-payment-rpc-runtime-api") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "noble-transaction-payment") (r "^2.0.0") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "02z5irzw8g1yakf5bg1p20vfwkxza4rxm0190z522p2znrip5mii") (f (quote (("std" "tp-api/std" "codec/std" "tp-runtime/std" "noble-transaction-payment/std") ("default" "std"))))))

