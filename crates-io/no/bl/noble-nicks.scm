(define-module (crates-io no bl noble-nicks) #:use-module (crates-io))

(define-public crate-noble-nicks-2.0.1 (c (n "noble-nicks") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "noble-balances") (r "^2.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 2)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1lgvgyp5gw973i9gyskkzad4ys7w7acb7ryij7aw85gbwlr9l88v") (f (quote (("std" "serde" "codec/std" "tetcore-std/std" "tet-io/std" "tp-runtime/std" "fabric-support/std" "fabric-system/std") ("default" "std"))))))

