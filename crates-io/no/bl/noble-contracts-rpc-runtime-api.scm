(define-module (crates-io no bl noble-contracts-rpc-runtime-api) #:use-module (crates-io))

(define-public crate-noble-contracts-rpc-runtime-api-0.8.0 (c (n "noble-contracts-rpc-runtime-api") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "noble-contracts-primitives") (r "^2.0.0") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "01kcbxvh61sbszr27swg5i7z21khz6n5hvph2c9j74sclz2smjlg") (f (quote (("std" "tp-api/std" "codec/std" "tetcore-std/std" "tp-runtime/std" "noble-contracts-primitives/std") ("default" "std"))))))

