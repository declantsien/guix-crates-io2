(define-module (crates-io no bs nobs-vulkanism-headless) #:use-module (crates-io))

(define-public crate-nobs-vulkanism-headless-0.1.0 (c (n "nobs-vulkanism-headless") (v "0.1.0") (d (list (d (n "nobs-vk") (r "^0.2.0") (d #t) (k 0)) (d (n "nobs-vkmem") (r "^0.2.0") (d #t) (k 0)) (d (n "nobs-vkpipes") (r "^0.1.0") (d #t) (k 0)))) (h "1s5dzz3n4z6gw389aq3caa7fv9iwsqgwd0cpdg78in9798h0c56k")))

