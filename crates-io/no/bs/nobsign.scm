(define-module (crates-io no bs nobsign) #:use-module (crates-io))

(define-public crate-nobsign-0.1.0 (c (n "nobsign") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0fc7qdy6p3lq1axxz7cdl91d4jq7yvc4r7mkc8wnmnzq2s9hf79p")))

(define-public crate-nobsign-0.2.0 (c (n "nobsign") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "ring") (r "^0.14") (d #t) (k 0)))) (h "14mb845kyzfacfbkdb7i86vhyfl4r1slmhw7zjvfnfsqfai9k4m3")))

