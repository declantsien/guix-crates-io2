(define-module (crates-io no bs nobs-vk) #:use-module (crates-io))

(define-public crate-nobs-vk-0.1.0 (c (n "nobs-vk") (v "0.1.0") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "04b7kqwlpqbhy3iwk74xqd73pc0ssrvyvfvhagw30zswh0c080aj")))

(define-public crate-nobs-vk-0.1.1 (c (n "nobs-vk") (v "0.1.1") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "0szzvlrzx7v0a3c8nbijh08rl510nhg8n6yykl831ix7jz2i0w72")))

(define-public crate-nobs-vk-0.1.2 (c (n "nobs-vk") (v "0.1.2") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1s1h1m7rz53gzry53xsikpdcmv5jas4nz3vpizsw22hdbm2c2v7k")))

(define-public crate-nobs-vk-0.1.3 (c (n "nobs-vk") (v "0.1.3") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1w3f2688rkkn91calphnwyz3cg6kad8sikfd3g1f2a7b66aca8lg")))

(define-public crate-nobs-vk-0.1.4 (c (n "nobs-vk") (v "0.1.4") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "12a6fac3k566g4wg46v3q49z7x7svl986z8x0f6jhm4sch9iahf0")))

(define-public crate-nobs-vk-0.1.5 (c (n "nobs-vk") (v "0.1.5") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "0d1a0wlzydqshv76c69xjjy1fl9f6xplzqddk00rmyrmxikqwzg8")))

(define-public crate-nobs-vk-0.1.6 (c (n "nobs-vk") (v "0.1.6") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1r3c860my73ddjzj6x56x7dxsh7zmddb4k8l7x848pvq4jbzq5ld")))

(define-public crate-nobs-vk-0.1.7 (c (n "nobs-vk") (v "0.1.7") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1x0qg8v6aci415x25pgq0ipn20n13fpjyma351nr8irpfg3lqf3x")))

(define-public crate-nobs-vk-0.1.8 (c (n "nobs-vk") (v "0.1.8") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1si6lprd6gx4mladi6hg07hg95i6jj2idm36p2waz8p4p3bpgar0")))

(define-public crate-nobs-vk-0.1.9 (c (n "nobs-vk") (v "0.1.9") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "10qmkf9zkzwmldhrwx9xyazsabil632r6rczqd9i7d0viz10c51s")))

(define-public crate-nobs-vk-0.1.10 (c (n "nobs-vk") (v "0.1.10") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "1z50n0ply6s44sdlis04cqirxy6dl72k8iwfn95ykk702bs4ih5q")))

(define-public crate-nobs-vk-0.2.0 (c (n "nobs-vk") (v "0.2.0") (d (list (d (n "shared_library") (r "^0.1.7") (d #t) (k 0)))) (h "0gkrsjc2ahbkilb9fz62a1009kqa4ndqba9v657ckygvmb8qd52j")))

