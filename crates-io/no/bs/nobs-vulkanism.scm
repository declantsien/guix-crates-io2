(define-module (crates-io no bs nobs-vulkanism) #:use-module (crates-io))

(define-public crate-nobs-vulkanism-0.1.0 (c (n "nobs-vulkanism") (v "0.1.0") (d (list (d (n "nobs-vulkanism-headless") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.18.1") (d #t) (k 0)))) (h "1riy22ivylr54dbd18nykykzdz9f77126bfhzyklkb96lax1ndvg")))

