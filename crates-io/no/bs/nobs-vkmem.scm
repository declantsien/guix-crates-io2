(define-module (crates-io no bs nobs-vkmem) #:use-module (crates-io))

(define-public crate-nobs-vkmem-0.1.0 (c (n "nobs-vkmem") (v "0.1.0") (d (list (d (n "nobs-vk") (r "^0.1.8") (d #t) (k 0)))) (h "0670m086bh9asdj274awmfs26wchrp0cv4c4s62bwrvvy5jxb72g")))

(define-public crate-nobs-vkmem-0.2.0 (c (n "nobs-vkmem") (v "0.2.0") (d (list (d (n "nobs-vk") (r "^0.2.0") (d #t) (k 0)))) (h "0plfwa403749w8rjm7ni9dkpw9aa1dvq1ynnzikhlx6xdcz3wyvl")))

