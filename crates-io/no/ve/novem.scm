(define-module (crates-io no ve novem) #:use-module (crates-io))

(define-public crate-novem-0.1.0 (c (n "novem") (v "0.1.0") (d (list (d (n "configparser") (r "^3.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0ba48mlg573zdda04z78bvs53cv37q32w7zi3j4y6cgpvd9maf8b")))

