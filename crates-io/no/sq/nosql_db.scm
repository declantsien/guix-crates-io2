(define-module (crates-io no sq nosql_db) #:use-module (crates-io))

(define-public crate-nosql_db-0.0.1 (c (n "nosql_db") (v "0.0.1") (h "1076lpzkxwahypah221mp3ikm133i0i3249dnn5y72pvfc533wgz")))

(define-public crate-nosql_db-0.0.2 (c (n "nosql_db") (v "0.0.2") (h "171rclmnbi4sadsa56n04kxirbicfpdf5iyagr13s6qlzaci9jh7")))

