(define-module (crates-io no la nolan) #:use-module (crates-io))

(define-public crate-nolan-0.1.0 (c (n "nolan") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yqqbhd4mnfpjv62031yv2xcy37q3ha91vb2pds9fig98vsrplsw")))

(define-public crate-nolan-0.1.1 (c (n "nolan") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ihp7xkw9fr530mdwl3960ll5xgshydjcjmxkbf7mchxrnqwbswj")))

