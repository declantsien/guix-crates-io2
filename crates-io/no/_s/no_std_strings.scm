(define-module (crates-io no _s no_std_strings) #:use-module (crates-io))

(define-public crate-no_std_strings-0.1.0 (c (n "no_std_strings") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0p5gz2yhyz8d04pm2g1yq9kv25zwnp6zrv0rrv78drd08cx6m0h8") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1.1 (c (n "no_std_strings") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0fvk56yr1kfqmidbam4g54na7ni09kjbjnamy24s2hjcj64m8qq0") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1.2 (c (n "no_std_strings") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1pfigwz8jp54w1174m0mgmbc981rv1vb3ap557x7piwrfdpibmd3") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1.3 (c (n "no_std_strings") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0vm7k1969fzamcxk0cikp555h1rcxfdf6cwsfjy2423q3dycgc55") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1.31 (c (n "no_std_strings") (v "0.1.31") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1ywqlsk9v74044kizzcf84sf7djm7n5slvxhwm9y00k9jjmi57kl") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

