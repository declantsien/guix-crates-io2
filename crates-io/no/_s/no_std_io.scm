(define-module (crates-io no _s no_std_io) #:use-module (crates-io))

(define-public crate-no_std_io-0.5.0 (c (n "no_std_io") (v "0.5.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "056mixy7cshxn7w1cyl7f4shpqifs3i5lq9q3wcdpamrvzlp13ad") (f (quote (("std" "alloc") ("nightly") ("default") ("alloc")))) (y #t)))

(define-public crate-no_std_io-0.5.1 (c (n "no_std_io") (v "0.5.1") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "1x98cx9pa9sbi5xn36n4kplhmxjp4n6xmkzvizx9nxkhhys66kka") (f (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.5.2 (c (n "no_std_io") (v "0.5.2") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "1jxb2zms61jq4rr1wryh5xsqrk4kg9yg46r803ss7sjbwsvi7n85") (f (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.5.3 (c (n "no_std_io") (v "0.5.3") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "0haky3jjwmmzjw6m4lnp76bnz1nhjqgdz1awvz7c3w0103i9jbvn") (f (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.6.0 (c (n "no_std_io") (v "0.6.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "02422zrmvxbgx5snr4mzc507dccmc6s9pwkjs57iph7jlq3g79bz") (f (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

