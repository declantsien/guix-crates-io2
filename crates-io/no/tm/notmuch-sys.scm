(define-module (crates-io no tm notmuch-sys) #:use-module (crates-io))

(define-public crate-notmuch-sys-4.3.0 (c (n "notmuch-sys") (v "4.3.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1l9a1vyy3kcd2szh32581yn0w38gfif992qzk625j6l6qyrh635i")))

(define-public crate-notmuch-sys-4.3.1 (c (n "notmuch-sys") (v "4.3.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1h15ini0gad6zz2pfs0n669y12vxvz74pb4ycpj23mh1rqjr9540")))

(define-public crate-notmuch-sys-4.3.2 (c (n "notmuch-sys") (v "4.3.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08kwvhw9f8rksjjfx3v090vy9jlxmydzh9p4k3773gqh6xpvv325")))

(define-public crate-notmuch-sys-4.3.3 (c (n "notmuch-sys") (v "4.3.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1z99ja9513cf1aszsbjq902h7i8j0n1rj24x50pshgzzl3nf1nf5")))

(define-public crate-notmuch-sys-4.3.4 (c (n "notmuch-sys") (v "4.3.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "161cdghr02k5iky8lkm1szwz5p0gg2yaad9add0aziaswyn4n6a4")))

(define-public crate-notmuch-sys-4.3.5 (c (n "notmuch-sys") (v "4.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0qmkd11xz7dpik10kzphw4mvqbv4ky7ndkj7y48k3dblgi5kb41s")))

(define-public crate-notmuch-sys-4.4.0 (c (n "notmuch-sys") (v "4.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "14s8dsc030n8fjm56py3rvk583j4dadbq5dlyh63bd0af6sy0jzr")))

(define-public crate-notmuch-sys-4.4.1 (c (n "notmuch-sys") (v "4.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gdc0hhnxbf2mfzxndr644k70xv3jgkz1ywznq2gan4xllqpm7kr")))

(define-public crate-notmuch-sys-4.4.2 (c (n "notmuch-sys") (v "4.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0qr43liaxky5gfpav6aksgwp8z40g5y6hxb3ykszynar7pz6fbzf")))

