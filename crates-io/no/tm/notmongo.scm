(define-module (crates-io no tm notmongo) #:use-module (crates-io))

(define-public crate-notmongo-0.1.5 (c (n "notmongo") (v "0.1.5") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0f74a01783px5kp8g5r3kzi065mz3vin15qyh5vcn963f6nj9fi6")))

(define-public crate-notmongo-0.1.6 (c (n "notmongo") (v "0.1.6") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1am4cwlix3qv4vrjrbdy5zcf7fhjhr9bxbzc13j6i2xhmwrwf41h")))

(define-public crate-notmongo-0.1.7 (c (n "notmongo") (v "0.1.7") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0ynf2mgdkajf9y3160jrfc0g6lwyjgdbnlp59mj6iwvh5an7lr5d")))

