(define-module (crates-io no tm notmuch-more) #:use-module (crates-io))

(define-public crate-notmuch-more-0.0.0 (c (n "notmuch-more") (v "0.0.0") (d (list (d (n "ammonia") (r "^3.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "email") (r "^0.0.21") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lettre") (r "=0.10.0-alpha.1") (f (quote ("builder" "rustls-tls" "smtp-transport"))) (k 0)) (d (n "mailparse") (r "^0.13.5") (d #t) (k 0)) (d (n "notmuch") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "19rscfn1c7qjymyq3j0wbnalcb775qlq77wzx1jmzd9z6c41gcay")))

