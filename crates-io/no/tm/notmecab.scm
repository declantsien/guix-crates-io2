(define-module (crates-io no tm notmecab) #:use-module (crates-io))

(define-public crate-notmecab-0.1.0 (c (n "notmecab") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "11n8cvs39njfmngqhsnxx9lsj4xdwlpy08g7yzv5pskxkl27apc8")))

(define-public crate-notmecab-0.1.1 (c (n "notmecab") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "1kd5kq4h6ixad9z9gp6jjz9hbsnghrlfgd9183bbppg8cnf9yxfv")))

(define-public crate-notmecab-0.2.0 (c (n "notmecab") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "091sy9vw8626jqif8vd90x95j4a78jf297pbxpqsxc8d57x2wfgm")))

(define-public crate-notmecab-0.3.0 (c (n "notmecab") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "0na09x9lzfkz4bv3bkf4b01jksdrc9qlfv3ab6g8khyb4gjfwlxc")))

(define-public crate-notmecab-0.3.1 (c (n "notmecab") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "1j6z92jkmqpbaysybyq0dshhrpsjrn1z43ggvvz4l75388dy08kf")))

(define-public crate-notmecab-0.3.2 (c (n "notmecab") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "1cgi9nkaksbxj3wl0fhfr6a3iphjqcwn7fi1bd8b96qs3q3wapnj")))

(define-public crate-notmecab-0.4.0 (c (n "notmecab") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "1k6wg5pqmlz7lr0440hddnphmzwzrd9m77fa4irpvqz4wbhih63b")))

(define-public crate-notmecab-0.5.0 (c (n "notmecab") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "14k82cmdlakw4a9s03wbfvbqiihhhiz6vda3b5n73zdcbg4865rf") (f (quote (("default" "hashbrown"))))))

(define-public crate-notmecab-0.5.1 (c (n "notmecab") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0fmn6v972bw3k86y1mz35b6pijbmxam57ixfd2daq1wfg0k9jxsf") (f (quote (("default" "hashbrown"))))))

