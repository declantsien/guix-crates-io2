(define-module (crates-io no tm notmuch) #:use-module (crates-io))

(define-public crate-notmuch-0.0.1 (c (n "notmuch") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q3gn63mpswd0qbzi7q6dsk2cwgyj2dvajr0ip1k796ssifiqnfz")))

(define-public crate-notmuch-0.0.2 (c (n "notmuch") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mjv856ncrb0ccr8l016jxwydz3mxc7sdn5j91jif8nwdbg80mzj")))

(define-public crate-notmuch-0.0.3 (c (n "notmuch") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "104dpa8ywz46lspfzp29gg0ngsvqsx9hw5q18h2yshjk4hdg9xc8")))

(define-public crate-notmuch-0.0.4 (c (n "notmuch") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p7vcygp213bxy9dl5525y1c389sn0snyrzdfbya54jpk3zjqajp")))

(define-public crate-notmuch-0.0.5 (c (n "notmuch") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1grd79ibnqkvqwql0l41lrjm2zq3hyrbkzmsw9jflsirzd2vs6hd")))

(define-public crate-notmuch-0.0.6 (c (n "notmuch") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ijaycbw68fj609d6fjy3ms3hx30g1qf14cidmk23pcvrxv8xbz8")))

(define-public crate-notmuch-0.0.7 (c (n "notmuch") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "034hc5jllc469xn29lazl46441y5jambglwjiqgfiad4fsc8qdf0")))

(define-public crate-notmuch-0.0.8 (c (n "notmuch") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pw2p8lh18lq2qa0mxnliwcs7lymzc994qxh8980vag5ifpfhis4")))

(define-public crate-notmuch-0.0.9 (c (n "notmuch") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w4jr6k98z7ymzkp7ivz7dh33wha1k5vavzizwmqsz1qja34j64q")))

(define-public crate-notmuch-0.0.10 (c (n "notmuch") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ciqf7h0s27k86map1w22xs8wsq02vlhq0g29140binck6iz6qm4")))

(define-public crate-notmuch-0.0.11 (c (n "notmuch") (v "0.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h4wsswhml15w88vrc20pqkhlapjbf0rkphwdizjhwbnwlfa4wsy")))

(define-public crate-notmuch-0.0.12 (c (n "notmuch") (v "0.0.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x25x8aqxv4wlkyyy7gy8d68bvm7zn2rlsl0ry52sms7c7b2rbv6")))

(define-public crate-notmuch-0.1.0 (c (n "notmuch") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06nz0v0w0swdzbv4g2v83rdyw9x4p073d874hq5mgl5llxx4xc3a")))

(define-public crate-notmuch-0.1.1 (c (n "notmuch") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sy1zxfgd90gbam98pmnc1rxn7fy0g112vviwxk12qkiyahryx9i") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.2.0 (c (n "notmuch") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r0jl5q9822rq19fhsbfg5p09grva6bl8x40npibqq92gj4snc0q") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.2.1 (c (n "notmuch") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1finzvwr6cxq2z23v6k47mk7yn161ggm0dzsiz3f79m93np7n3wb") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.3.0 (c (n "notmuch") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "1a2vp3cl58fb54rj7h52jzc7avzilzjzxz50bkli900q5njnzpxb") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.3.1 (c (n "notmuch") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "17m753idvpmjhrcwb54mvi872xnj6q3zwgqrimkzxg2j7xknga1x") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4.0 (c (n "notmuch") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "0vzz3ii4kfvsf9a5fhsgvg47m2nxzpvjgx18x7lnxb1fvx1vkw70") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4.1 (c (n "notmuch") (v "0.4.1") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "0mjrqlazj4pp9gip3mrnpx1wqczl99fhxyq6i47l6xvkdkypy3wz") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4.2 (c (n "notmuch") (v "0.4.2") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "0hiffw2k0fxdhghvrqm464vbq9hi6ggmf63f84j5gzd920gglcy0") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.5.0 (c (n "notmuch") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.193") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)))) (h "1s92w3j77ij8z5fw1vs3ypd119ksi3f38gs24r8m0s49nv0j65l3") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.6.0 (c (n "notmuch") (v "0.6.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 2)) (d (n "lettre") (r "^0.9.2") (d #t) (k 2)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maildir") (r "^0.3.2") (d #t) (k 2)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19q93iyvx4liksm09mhq9ibm8zj7i3dizc1s40f916z0kbpn9k5w") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7.0 (c (n "notmuch") (v "0.7.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "from_variants") (r "^0.6.0") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 2)) (d (n "lettre") (r "^0.9.2") (d #t) (k 2)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maildir") (r "^0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ahd11y36jisfjssr2056857dywzyd79rsyx815x42nkzlm9np8d") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7.1 (c (n "notmuch") (v "0.7.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "from_variants") (r "^0.6.0") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 2)) (d (n "lettre") (r "^0.9.2") (d #t) (k 2)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maildir") (r "^0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1y8c76by7mp9rmaa99whc6vhkfdgxxb4yja27ng55f7mkbyl22fa") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7.2 (c (n "notmuch") (v "0.7.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "from_variants") (r "^0.6.0") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 2)) (d (n "lettre") (r "^0.9.2") (d #t) (k 2)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maildir") (r "^0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11dzsbbs3nd8nf4wbkk4cgsp1xx7dpgmvmb1s954ddxmxmyb4yld") (f (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.8.0 (c (n "notmuch") (v "0.8.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 2)) (d (n "from_variants") (r "^0.6.0") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 2)) (d (n "lettre") (r "^0.9.2") (d #t) (k 2)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maildir") (r "^0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0i6xc7lv10m2sq6vlpjr5wxmlxihvd0v4f5if75r2kwz8ji12pg2") (f (quote (("v0_32" "v0_26") ("v0_26" "v0_21") ("v0_21") ("default" "v0_32"))))))

