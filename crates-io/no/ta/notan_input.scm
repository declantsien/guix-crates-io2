(define-module (crates-io no ta notan_input) #:use-module (crates-io))

(define-public crate-notan_input-0.4.0 (c (n "notan_input") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notan_core") (r "^0.4.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.4.0") (d #t) (k 0)))) (h "1q4ivhjyrrzh5d6zlzmrxz83n60z5bavpx29bgkq41dwdrj469wk")))

(define-public crate-notan_input-0.4.1 (c (n "notan_input") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notan_core") (r "^0.4.1") (d #t) (k 0)) (d (n "notan_math") (r "^0.4.1") (d #t) (k 0)))) (h "0cl5xxaqcnma4qdzknm3wyvjfhfyw7fqp63aa603cqsw4ngpicp5")))

(define-public crate-notan_input-0.4.2 (c (n "notan_input") (v "0.4.2") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notan_core") (r "^0.4.2") (d #t) (k 0)) (d (n "notan_math") (r "^0.4.2") (d #t) (k 0)))) (h "1nqcchxwnz7ccgfxz0h3yhicj9lq8clqnpcjk1rzhzcaqcxynqrl")))

(define-public crate-notan_input-0.5.0 (c (n "notan_input") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notan_core") (r "^0.5.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.5.0") (d #t) (k 0)))) (h "0598rbz0ssl5ys72g1pg0f5lh3dqh23sfx81n13rz741a0r4ky4a")))

(define-public crate-notan_input-0.5.1 (c (n "notan_input") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notan_core") (r "^0.5.1") (d #t) (k 0)) (d (n "notan_math") (r "^0.5.1") (d #t) (k 0)))) (h "1cwx47hgh8pf370hl15nc0al5165z813nysvd41ni53yc6lr2rf6")))

(define-public crate-notan_input-0.6.0 (c (n "notan_input") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.12.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.6.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.6.0") (d #t) (k 0)))) (h "14r5pvaxxbsyymqfpjg0vgkgrf22xb3xrdgn9rj8zkim8zc7j1yq")))

(define-public crate-notan_input-0.7.0 (c (n "notan_input") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.7.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.7.0") (d #t) (k 0)))) (h "1ayc6ddn2mw52924130vj658gqyslg2zg0c53vy1zv13w57cfx94")))

(define-public crate-notan_input-0.7.1 (c (n "notan_input") (v "0.7.1") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.7.1") (d #t) (k 0)) (d (n "notan_math") (r "^0.7.1") (d #t) (k 0)))) (h "1x9r8fszlx3jphcl1xrs7f88wng8cadivcdl5063glgwigz5jch7")))

(define-public crate-notan_input-0.8.0 (c (n "notan_input") (v "0.8.0") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.8.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.8.0") (d #t) (k 0)))) (h "1ynmbc4gbjnd31bxjjmmglwf4fi81764mlbnvhn2a7alrcfn239b")))

(define-public crate-notan_input-0.9.0 (c (n "notan_input") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.0") (d #t) (k 0)))) (h "1grdb3gsyavh22zr30vw4mzf77hh87m5g3qfil25iqmqp4y8dq0z")))

(define-public crate-notan_input-0.9.1 (c (n "notan_input") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.1") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.1") (d #t) (k 0)))) (h "0zj99f5vg3bqdqh3j6ic6hcy471r9l9v6mbqhrbdvf1q2iqif8xm")))

(define-public crate-notan_input-0.9.2 (c (n "notan_input") (v "0.9.2") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.2") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.2") (d #t) (k 0)))) (h "1s5s3dzncvwfvhggbkixmn4vxqicm4gbwarz1gqw39l9znydmqpf")))

(define-public crate-notan_input-0.9.3 (c (n "notan_input") (v "0.9.3") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.3") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.3") (d #t) (k 0)))) (h "1kwgdhqr1p0smpba3rr4a5kxgrf90va1bh86gvvr88r3rr00w977")))

(define-public crate-notan_input-0.9.4 (c (n "notan_input") (v "0.9.4") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.4") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1l01kivkz6jd6s68qfym09y791yf72lmgwmwg5qlh0qji9zs08yy") (s 2) (e (quote (("serde" "dep:serde" "notan_core/serde" "notan_math/serde" "hashbrown/serde"))))))

(define-public crate-notan_input-0.9.5 (c (n "notan_input") (v "0.9.5") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notan_core") (r "^0.9.5") (d #t) (k 0)) (d (n "notan_math") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "085khcrj06ssq5d43qx8hp6jnvb8z0ivbfzx757f450d66x85hbq") (s 2) (e (quote (("serde" "dep:serde" "notan_core/serde" "notan_math/serde" "hashbrown/serde"))))))

(define-public crate-notan_input-0.10.0 (c (n "notan_input") (v "0.10.0") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notan_core") (r "^0.10.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zlpmmd9930flgmsc57bagf6h094anr1xyskc6hcc230k48l017b") (s 2) (e (quote (("serde" "dep:serde" "notan_core/serde" "notan_math/serde" "hashbrown/serde"))))))

(define-public crate-notan_input-0.11.0 (c (n "notan_input") (v "0.11.0") (d (list (d (n "hashbrown") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notan_core") (r "^0.11.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mh9sdr8xg6x748vhvhmqnpqqarbd2nw4777qm38wcqyp7ahr24g") (s 2) (e (quote (("serde" "dep:serde" "notan_core/serde" "notan_math/serde" "hashbrown/serde"))))))

(define-public crate-notan_input-0.12.0 (c (n "notan_input") (v "0.12.0") (d (list (d (n "hashbrown") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notan_core") (r "^0.12.0") (d #t) (k 0)) (d (n "notan_math") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0jnb4340h5jwa573mi52nj1n2y2jnsjwqn5l007zmq5ycdf1a5lv") (s 2) (e (quote (("serde" "dep:serde" "notan_core/serde" "notan_math/serde" "hashbrown/serde"))))))

