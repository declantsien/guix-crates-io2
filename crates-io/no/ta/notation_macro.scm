(define-module (crates-io no ta notation_macro) #:use-module (crates-io))

(define-public crate-notation_macro-0.3.2 (c (n "notation_macro") (v "0.3.2") (d (list (d (n "notation_dsl") (r "^0.3.2") (d #t) (k 0)))) (h "1f9hqlmrwa6w5azh8ira9absz8cmgz3rijc8dwd2fj6fvgshxp4a")))

(define-public crate-notation_macro-0.3.3 (c (n "notation_macro") (v "0.3.3") (d (list (d (n "notation_dsl") (r "^0.3.3") (d #t) (k 0)))) (h "1g4wlsrnk4ag9985p6nsb9529id4a0qhjqz362l9wllkwlf3b4hk")))

(define-public crate-notation_macro-0.3.4 (c (n "notation_macro") (v "0.3.4") (d (list (d (n "notation_dsl") (r "^0.3.4") (d #t) (k 0)))) (h "0gmg3szvdpxd5bp1sf9dflfh0xsirbcl9s03br4vy0lcpl5n1prp")))

(define-public crate-notation_macro-0.4.0 (c (n "notation_macro") (v "0.4.0") (d (list (d (n "notation_dsl") (r "^0.4.0") (d #t) (k 0)))) (h "132jdkfxypwj3qc50wps95h006z0wzpipyw2qjkwb6vlgi670k2c")))

(define-public crate-notation_macro-0.4.2 (c (n "notation_macro") (v "0.4.2") (d (list (d (n "notation_dsl") (r "^0.4.2") (d #t) (k 0)))) (h "11pm7jsakfsaw3iw2y6i0nszb35g674ahz6nkanh9kdjpziv1wil")))

(define-public crate-notation_macro-0.4.3 (c (n "notation_macro") (v "0.4.3") (d (list (d (n "notation_dsl") (r "^0.4.3") (d #t) (k 0)))) (h "1cqim3csj5rwa4npl4mhvq7vl9k4qii92cjxlcbvfh1vd3pqci2q")))

(define-public crate-notation_macro-0.4.4 (c (n "notation_macro") (v "0.4.4") (d (list (d (n "notation_dsl") (r "^0.4.4") (d #t) (k 0)))) (h "12rl3jjy8zagpf1li3g6k4c40r9kbnz6ab6br47mcczpssim9ki5")))

(define-public crate-notation_macro-0.5.0 (c (n "notation_macro") (v "0.5.0") (d (list (d (n "notation_dsl") (r "^0.5.0") (d #t) (k 0)))) (h "02vqr5h6wvkgk2m03d72iisfg2zn87xqjck3lwx5mfrfyp3kj74c")))

(define-public crate-notation_macro-0.6.0 (c (n "notation_macro") (v "0.6.0") (d (list (d (n "notation_dsl") (r "^0.6.0") (d #t) (k 0)))) (h "1i1gzcr4hc5jza2cyqyrkx85jpffb8m3gr7rfc7pwj2ksay92dcj")))

