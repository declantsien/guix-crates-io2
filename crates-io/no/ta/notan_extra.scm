(define-module (crates-io no ta notan_extra) #:use-module (crates-io))

(define-public crate-notan_extra-0.6.0 (c (n "notan_extra") (v "0.6.0") (d (list (d (n "notan_app") (r "^0.6.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1i8a8aby1x53hdg2v6z9h1674mlxlc5b1nk8sck1x3l48qxa3cg7")))

(define-public crate-notan_extra-0.7.0 (c (n "notan_extra") (v "0.7.0") (d (list (d (n "notan_app") (r "^0.7.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "04w9y0r29zp9377s79kqr8ci1jgq651bnl5xqg83q52n0dc3n8vd")))

(define-public crate-notan_extra-0.7.1 (c (n "notan_extra") (v "0.7.1") (d (list (d (n "notan_app") (r "^0.7.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0aslmlr8bzy5a3s3flblz67vhqif89f40xvl3rb5r0k0lp5mm2sc")))

(define-public crate-notan_extra-0.8.0 (c (n "notan_extra") (v "0.8.0") (d (list (d (n "notan_app") (r "^0.8.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0biabk07zcxaxc5gq5zgqg603l8d31nm3xc0p65xbzhia7q5m9y2")))

(define-public crate-notan_extra-0.9.0 (c (n "notan_extra") (v "0.9.0") (d (list (d (n "notan_app") (r "^0.9.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0prhyrqg9hqfqqahi567a25wmwcvdqq56npiaf8pv06in53rd9g2")))

(define-public crate-notan_extra-0.9.1 (c (n "notan_extra") (v "0.9.1") (d (list (d (n "notan_app") (r "^0.9.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "14jx8wjqvz5bp4a767x97x1gbajppcrh8f0m1v226sbd3brbl6mb")))

(define-public crate-notan_extra-0.9.2 (c (n "notan_extra") (v "0.9.2") (d (list (d (n "notan_app") (r "^0.9.2") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0fkwm99rk87asd61581g99fz7rhzgsl95hhr2ppqyxz0vbgwfx4g")))

(define-public crate-notan_extra-0.9.3 (c (n "notan_extra") (v "0.9.3") (d (list (d (n "notan_app") (r "^0.9.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0d8binz8mn8aqgjna6d46xvd2fdifvgk8q9jpgqb0rcb9ahvjpjf")))

(define-public crate-notan_extra-0.9.4 (c (n "notan_extra") (v "0.9.4") (d (list (d (n "notan_app") (r "^0.9.4") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0ghdxcmi6nkrk3xihsw29vkyspnbdfzzqd03p3bbjha9nkkliz43")))

(define-public crate-notan_extra-0.9.5 (c (n "notan_extra") (v "0.9.5") (d (list (d (n "notan_app") (r "^0.9.5") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1zg0hlj7hpp6wxpms9499g9r15iwykgpb7gmxq5dnw4zfdk5ii48")))

(define-public crate-notan_extra-0.10.0 (c (n "notan_extra") (v "0.10.0") (d (list (d (n "notan_app") (r "^0.10.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0bpsikiyw1vkhm1fz19y2fk9wkxkmmzjxcrfgwb92izk7axrs4li")))

(define-public crate-notan_extra-0.11.0 (c (n "notan_extra") (v "0.11.0") (d (list (d (n "notan_app") (r "^0.11.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0i20s0027jkf7vfz0azym2jsypb4kkvdz4bpi6a84qrnvw7ff91g")))

(define-public crate-notan_extra-0.12.0 (c (n "notan_extra") (v "0.12.0") (d (list (d (n "notan_app") (r "^0.12.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1b67bppcfb9aicpxvk77q2lwbr1bkvpqxh9cpy84g74r1d7ndn8k")))

