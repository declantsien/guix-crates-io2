(define-module (crates-io no ta notan_math) #:use-module (crates-io))

(define-public crate-notan_math-0.1.0 (c (n "notan_math") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.7.2") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "1cgmc5sj18n9hlvay739b8wxl55pv456s1xnm923i02sc6mkbs8c")))

(define-public crate-notan_math-0.2.0 (c (n "notan_math") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "00fjrh3vfl24kyl1bvngx0axpfgp5h9022nzn9w1s85nj9zv9h1m")))

(define-public crate-notan_math-0.2.1 (c (n "notan_math") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "1y251iyvy32r1ri4r057cjxd1b3fscz5jljv19xkr4cs47vml6nc")))

(define-public crate-notan_math-0.3.0 (c (n "notan_math") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0xi1nm6bhd23kq5c0m3wq8rwv37b936g36dhfihlh72kd5lw2qw1")))

(define-public crate-notan_math-0.4.0 (c (n "notan_math") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0zi5f4whqch0fb2m5fxkvrs7cdnd2sj815h17iilfnjq8lz23852")))

(define-public crate-notan_math-0.4.1 (c (n "notan_math") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0q1mz0qjyq9fdpg0yvqnvvx9bmbw59c38wys8k3dg9x0gzfgjxna")))

(define-public crate-notan_math-0.4.2 (c (n "notan_math") (v "0.4.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.20") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "050bqvxm2r5aa986fb3ccpvlpw7cgzwsrdrg3k5mgq5kdndy8gaa")))

(define-public crate-notan_math-0.5.0 (c (n "notan_math") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.21") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0mgimlxn6jxpgkv5qf96r4dyx35yp43v42axiw0699f1mgvya8r1")))

(define-public crate-notan_math-0.5.1 (c (n "notan_math") (v "0.5.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.21") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0ql03qcdpz4rw54fzaxilz3mgrkjs2nn9dmhajhyv260mggi7r50")))

(define-public crate-notan_math-0.6.0 (c (n "notan_math") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "glam") (r "^0.21.2") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0kpy3faph99gmb02sj73jgkrzdk0pg2l45qm4d66nfd265y4fndm")))

(define-public crate-notan_math-0.7.0 (c (n "notan_math") (v "0.7.0") (d (list (d (n "glam") (r "^0.21.3") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "0346v3s5knlaxddhi9xfqkn8m2xy2y9snigb1iyww7mj7lx1dqhf")))

(define-public crate-notan_math-0.7.1 (c (n "notan_math") (v "0.7.1") (d (list (d (n "glam") (r "^0.21.3") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "1c48hzqfr0bbhay9871aswaiwzasbam1wrs788mbbkkwmrjalbzl")))

(define-public crate-notan_math-0.8.0 (c (n "notan_math") (v "0.8.0") (d (list (d (n "glam") (r "^0.21.3") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "1l81az2ypihnv8lw477aaiqds84mqdxyrb49fvmdyh54v498p0kl")))

(define-public crate-notan_math-0.9.0 (c (n "notan_math") (v "0.9.0") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "1jc9g9ca47acki26kzhg0wcj9qnxw3x7y3r0m28hrflzqagj3v93")))

(define-public crate-notan_math-0.9.1 (c (n "notan_math") (v "0.9.1") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "0563fc0crhhg2406q2yzqrmfcfd5brf5zrbm0pak35p64l4mc4kk")))

(define-public crate-notan_math-0.9.2 (c (n "notan_math") (v "0.9.2") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "0280np6cnl5sdc4z59zj4zank3qxqvcwvihb67vqx74q01z23401")))

(define-public crate-notan_math-0.9.3 (c (n "notan_math") (v "0.9.3") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)))) (h "1f0al3km5g8qlkmjn1dvrnnd0biwz5ba8aqxnvlrvydz6cd54272")))

(define-public crate-notan_math-0.9.4 (c (n "notan_math") (v "0.9.4") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mlqdl8kq03f2ici83wbijhv52f77sp8330cg6bp7qgdi5zx8qak") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

(define-public crate-notan_math-0.9.5 (c (n "notan_math") (v "0.9.5") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("scalar-math" "bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "07df597i6zfaqkj15qzbrbcbz12d3m6mzppz1pi5lqqgyahxcs27") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

(define-public crate-notan_math-0.10.0 (c (n "notan_math") (v "0.10.0") (d (list (d (n "glam") (r "^0.24.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1vsim5r1rkqqcid0mizmmqnr3rc79i1cz73dvs341bnin78vq21f") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

(define-public crate-notan_math-0.11.0 (c (n "notan_math") (v "0.11.0") (d (list (d (n "glam") (r "^0.24.2") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0dhawl6rqnxlna3yizm5c1yxsfyi8qyvajbbbllj5vzamqwi3bl7") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

(define-public crate-notan_math-0.12.0 (c (n "notan_math") (v "0.12.0") (d (list (d (n "glam") (r "^0.24.2") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1xv8h8mm5v45018zqr8fajn4v542bsqhksx40k71wbwc5s2c5j8z") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

