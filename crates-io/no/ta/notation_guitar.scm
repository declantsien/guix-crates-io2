(define-module (crates-io no ta notation_guitar) #:use-module (crates-io))

(define-public crate-notation_guitar-0.1.0 (c (n "notation_guitar") (v "0.1.0") (d (list (d (n "notation_core") (r "^0.1.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nqb5jzf6hl1byvbc14caxp3fbcx1g8vc2b38dsp80bdyykb2iha")))

(define-public crate-notation_guitar-0.2.0 (c (n "notation_guitar") (v "0.2.0") (d (list (d (n "notation_core") (r "^0.2.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pny9k3y3ib2apdw30qz8ldc4i52vgxsjhyds5v14w6qfvxf5i55")))

(define-public crate-notation_guitar-0.3.0 (c (n "notation_guitar") (v "0.3.0") (d (list (d (n "notation_core") (r "^0.3.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zf0havphnql1fqa6ka78ggl9mm6rdvx88b9ykqjsnj12blhhswj")))

(define-public crate-notation_guitar-0.3.1 (c (n "notation_guitar") (v "0.3.1") (d (list (d (n "notation_core") (r "^0.3.1") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwxj20817j834na25lmziw6c3mwha6337wfkmyb0f1kr99kx1yg")))

(define-public crate-notation_guitar-0.3.2 (c (n "notation_guitar") (v "0.3.2") (d (list (d (n "notation_core") (r "^0.3.2") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "003hz18mmbjpw6hm029dfq2g3km357gh8hj2793z5snivc8j7jvx")))

(define-public crate-notation_guitar-0.3.4 (c (n "notation_guitar") (v "0.3.4") (d (list (d (n "notation_core") (r "^0.3.4") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "01kxbx4n2ds5rh9wy02ccc0gd97h772w6wshhnmqs2ca9xm0jkrf")))

(define-public crate-notation_guitar-0.4.0 (c (n "notation_guitar") (v "0.4.0") (d (list (d (n "notation_core") (r "^0.4.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v7ilv8nci85xq48czc3lyqgxj2ixzcdz0knrmmgpwrardbcxkpn")))

(define-public crate-notation_guitar-0.4.3 (c (n "notation_guitar") (v "0.4.3") (d (list (d (n "notation_core") (r "^0.4.3") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "16lfbj6hw0hngivixk6gxxs8asjvqngjvb0r86wsisc3l56lzqrm")))

(define-public crate-notation_guitar-0.4.4 (c (n "notation_guitar") (v "0.4.4") (d (list (d (n "notation_core") (r "^0.4.3") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aw8fd96q4cxwyww5m76mbsr2ax1vsp6gw7mqanz8r44yswzivyd")))

(define-public crate-notation_guitar-0.5.0 (c (n "notation_guitar") (v "0.5.0") (d (list (d (n "notation_core") (r "^0.5.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i4snmrgqlxldyx9dbcwhl88b46lp42w57k59ipx427jpxrpp8b0")))

(define-public crate-notation_guitar-0.6.0 (c (n "notation_guitar") (v "0.6.0") (d (list (d (n "notation_core") (r "^0.6.0") (d #t) (k 0)) (d (n "notation_fretted") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z05x68769bpkmr8cy1xv7svb3sln92amqawij5624l5ww2xd495")))

