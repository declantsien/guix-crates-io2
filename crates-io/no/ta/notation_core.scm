(define-module (crates-io no ta notation_core) #:use-module (crates-io))

(define-public crate-notation_core-0.1.0 (c (n "notation_core") (v "0.1.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "05a9n5qw7m03dj0mgyqpvgj072qhd24l8fvxbxrfvr4j3mv90q8c")))

(define-public crate-notation_core-0.2.0 (c (n "notation_core") (v "0.2.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l280dlgc7xswm520xwapbsi4p23n0r49nq0ac6zgmpmkij2pl8d")))

(define-public crate-notation_core-0.3.0 (c (n "notation_core") (v "0.3.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bd3fqab06cqk2lvlwqr3ly7h9pl6kxmd6ms94sj7v5hvbq7lrxv")))

(define-public crate-notation_core-0.3.1 (c (n "notation_core") (v "0.3.1") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j6ppaam2nqydg7a1gbjzx9hhny7b104jf96clq9d4rylb4a0lfy")))

(define-public crate-notation_core-0.3.2 (c (n "notation_core") (v "0.3.2") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1snhxfnyh8ca3shz8gzzghamic52qnlh3w6k3ncz7rlw0n98hbl9")))

(define-public crate-notation_core-0.3.4 (c (n "notation_core") (v "0.3.4") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zsn7axq5dcr4x3ykshl62lv1lr5s0zhsl7lgcxrxjf9jh5xdic5")))

(define-public crate-notation_core-0.4.0 (c (n "notation_core") (v "0.4.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dphwlra0l5d4qdja3k9cnncbcdhj1gp6srgpxzwfhj8vw6vkgi2")))

(define-public crate-notation_core-0.4.3 (c (n "notation_core") (v "0.4.3") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h3dhjghvyhnaxl62ga8n36vyj61sr5n5zqymfaya5gl2zmzr24j")))

(define-public crate-notation_core-0.5.0 (c (n "notation_core") (v "0.5.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k3s3g6q8chmvy21ywxcn4hl3vln3ww9yj7ip3ywy4r6n7i75bxr")))

(define-public crate-notation_core-0.6.0 (c (n "notation_core") (v "0.6.0") (d (list (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zw263j0mbyagpphkm9glqwmp6hzwkrri5113qr3l3ny46w8l1x1")))

