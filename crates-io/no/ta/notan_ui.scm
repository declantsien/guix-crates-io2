(define-module (crates-io no ta notan_ui) #:use-module (crates-io))

(define-public crate-notan_ui-1.0.0 (c (n "notan_ui") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "math_thingies") (r "^0.1.0") (d #t) (k 0)) (d (n "notan") (r "^0.7.1") (f (quote ("text"))) (d #t) (k 0)))) (h "11dy9j4i6ylb22ch8azw16q88lxx3rl751h1m6810n6mn5cv91wm")))

(define-public crate-notan_ui-1.0.1 (c (n "notan_ui") (v "1.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.9") (d #t) (k 0)) (d (n "math_thingies") (r "^0.1.0") (d #t) (k 0)) (d (n "notan") (r "^0.7.1") (f (quote ("text"))) (d #t) (k 0)))) (h "18vh2j4rzxk5gs8g8c03gb97r0yzyfph0nwnry6crddwlwm6vkn5")))

