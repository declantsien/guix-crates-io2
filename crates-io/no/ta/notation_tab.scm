(define-module (crates-io no ta notation_tab) #:use-module (crates-io))

(define-public crate-notation_tab-0.3.0 (c (n "notation_tab") (v "0.3.0") (d (list (d (n "notation_dsl") (r "^0.3.0") (d #t) (k 0)) (d (n "notation_proto") (r "^0.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0v3mmwpc658lsql1hvlb42g5bhg62wyz6ywd6w1316wxmrhan0mc")))

(define-public crate-notation_tab-0.3.1 (c (n "notation_tab") (v "0.3.1") (d (list (d (n "notation_dsl") (r "^0.3.1") (d #t) (k 0)) (d (n "notation_proto") (r "^0.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0z2qxzx6j9ak6mpw8sphi9jvc82xc26p164hg0vhrdgbw11hb846")))

(define-public crate-notation_tab-0.3.2 (c (n "notation_tab") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.3.2") (d #t) (k 0)) (d (n "notation_macro") (r "^0.3.2") (d #t) (k 0)) (d (n "notation_proto") (r "^0.3.2") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "09i7h42xyvxrr8lzql928pfmq8kmd7xmcgia47y8jhcpx9zblpxb")))

(define-public crate-notation_tab-0.3.3 (c (n "notation_tab") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.3.3") (d #t) (k 0)) (d (n "notation_macro") (r "^0.3.3") (d #t) (k 0)) (d (n "notation_proto") (r "^0.3.2") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "18d70061icx6mx6i9mw3k6c55srhrb8v57g9m0bf2dqi4jq58xrj")))

(define-public crate-notation_tab-0.3.4 (c (n "notation_tab") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.3.4") (d #t) (k 0)) (d (n "notation_macro") (r "^0.3.4") (d #t) (k 0)) (d (n "notation_proto") (r "^0.3.4") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0p8bs0hp983zllyl213g1hipddm6nd8mzjjnxwiq4zvl3jjil07c")))

(define-public crate-notation_tab-0.4.0 (c (n "notation_tab") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.4.0") (d #t) (k 0)) (d (n "notation_macro") (r "^0.4.0") (d #t) (k 0)) (d (n "notation_proto") (r "^0.4.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1wlvsp2hiw09p9sa29pk3889gslk7f6azvh0wzdnna8jiwv9xg83")))

(define-public crate-notation_tab-0.4.2 (c (n "notation_tab") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.4.2") (d #t) (k 0)) (d (n "notation_macro") (r "^0.4.2") (d #t) (k 0)) (d (n "notation_proto") (r "^0.4.2") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "00w9k5iwz9n830z4daldyqbrxpjvfh5vd2lddy905wcd1c0rlafr")))

(define-public crate-notation_tab-0.4.3 (c (n "notation_tab") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.4.3") (d #t) (k 0)) (d (n "notation_macro") (r "^0.4.3") (d #t) (k 0)) (d (n "notation_proto") (r "^0.4.3") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "02zpajp1fwli82vl5bsnz2yxka40wwkhin60kjqkf339d9h0zxi2")))

(define-public crate-notation_tab-0.4.4 (c (n "notation_tab") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.4.4") (d #t) (k 0)) (d (n "notation_macro") (r "^0.4.4") (d #t) (k 0)) (d (n "notation_proto") (r "^0.4.4") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "11q394a9n569dfkv5k8aijdlwas9xzwfjyig1ahxrvw5ivijjwqv")))

(define-public crate-notation_tab-0.5.0 (c (n "notation_tab") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.5.0") (d #t) (k 0)) (d (n "notation_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "notation_proto") (r "^0.5.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1vqbd1m4nxmvgxj4izh4qrnl0lpn9dpm674w9j17vxx3x8lxf4rb")))

(define-public crate-notation_tab-0.6.0 (c (n "notation_tab") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notation_dsl") (r "^0.6.0") (d #t) (k 0)) (d (n "notation_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "notation_proto") (r "^0.6.0") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0s60cxnn58bxkka4cvvr8ynrs43vy01g88fbarik3skcip616r17")))

