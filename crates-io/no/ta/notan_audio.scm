(define-module (crates-io no ta notan_audio) #:use-module (crates-io))

(define-public crate-notan_audio-0.2.0 (c (n "notan_audio") (v "0.2.0") (h "1si67xfh5d2hjls3air48jqs5y9y2y8a6qqabwz7laz1sjpchp9p")))

(define-public crate-notan_audio-0.2.1 (c (n "notan_audio") (v "0.2.1") (h "1zqkbwqwpik8n78f60fd74wm0x8hsylz5yf301dicm883yjnhc53")))

(define-public crate-notan_audio-0.3.0 (c (n "notan_audio") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1gs5axv3kkx49gsv8q2bz64m9n9b2mfpg8s5qwa1kq48k9camrca")))

(define-public crate-notan_audio-0.4.0 (c (n "notan_audio") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0i299xvzc9iz1wgagqnpb4p18v1gb3ni4bmvp7qbf9nqhnnwqabi")))

(define-public crate-notan_audio-0.4.1 (c (n "notan_audio") (v "0.4.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1c5b4n4a8xi3nvd81ssgy8h95axadkfglgw2qwkxnmnr1xpxrs0l")))

(define-public crate-notan_audio-0.4.2 (c (n "notan_audio") (v "0.4.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1sxaw2wc5xwdr91xarvqb78jk0qyj6s17a7wpvyx4ibdn0vx14z0")))

(define-public crate-notan_audio-0.5.0 (c (n "notan_audio") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0zvmvq14wf6pp289lnx30q7gx010yf86ma8882k0fh7zdhyhqj5h")))

(define-public crate-notan_audio-0.5.1 (c (n "notan_audio") (v "0.5.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0c27ymxzmwva85996c4kfqs6v1bg523fnd348in9mswbcwnm69nv")))

(define-public crate-notan_audio-0.6.0 (c (n "notan_audio") (v "0.6.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0jjr3ifpvsgakjf0j90zvn1dmy5hp42zk7z29bhr1q3n0d1x6gfr")))

(define-public crate-notan_audio-0.7.0 (c (n "notan_audio") (v "0.7.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "12kasxh3klzlvg1njlxjhbq62qgql22awbwn0ziidwn3w0j8h9xl")))

(define-public crate-notan_audio-0.7.1 (c (n "notan_audio") (v "0.7.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1b8cph2dafqcra98wyfp5bzqxxxq52hl2kd4hrlxcla5anh7hmim")))

(define-public crate-notan_audio-0.8.0 (c (n "notan_audio") (v "0.8.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "001zx8l0m27i05lpfyf42vkk7fbngpz9pqnsi8i95c4dcrbxjgj2")))

(define-public crate-notan_audio-0.9.0 (c (n "notan_audio") (v "0.9.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0afhl5a10nnn5hbjnsqir7cknsyg5i8pb2k95fvl1xkm0l9py0np")))

(define-public crate-notan_audio-0.9.1 (c (n "notan_audio") (v "0.9.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0sp6gvj5h770pscsng3ll56dww0hbbsgyzq1pil142zvpml3kqp9")))

(define-public crate-notan_audio-0.9.2 (c (n "notan_audio") (v "0.9.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "01ys4hj45hh3xhjcmbz51fk1qw19fjmyaw1am352zdwz5wxbqx2j")))

(define-public crate-notan_audio-0.9.3 (c (n "notan_audio") (v "0.9.3") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "011wsp3xdy88mlnmk2h12f9j02h6gafng97sgsihsilppga2s6rs")))

(define-public crate-notan_audio-0.9.4 (c (n "notan_audio") (v "0.9.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1pn4szq6s7qdpb273ig7crcmp8vk49gr4gd03k7bs4f8wrvhbmz6")))

(define-public crate-notan_audio-0.9.5 (c (n "notan_audio") (v "0.9.5") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0x2z20d1dbwk03ydpmmnz2b4jak9qpyqsb4i37hq208fa12bppsf")))

(define-public crate-notan_audio-0.10.0 (c (n "notan_audio") (v "0.10.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0s12lyv4g1mg25ic1d1bcpap33lpfs7dngfxd6yhnjhn45780rbg")))

(define-public crate-notan_audio-0.11.0 (c (n "notan_audio") (v "0.11.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "188xc1ldkfgj7m6w5mg83z2v9dg24rsbh3ripbvw491akhj5866z")))

(define-public crate-notan_audio-0.12.0 (c (n "notan_audio") (v "0.12.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0vlnipjgwiphc745xjwmx9a640f7kki0v26bk6rz9xqdijh0274q")))

