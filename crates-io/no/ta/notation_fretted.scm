(define-module (crates-io no ta notation_fretted) #:use-module (crates-io))

(define-public crate-notation_fretted-0.1.0 (c (n "notation_fretted") (v "0.1.0") (d (list (d (n "notation_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "0l3gq9izvzf2jj8dqygl9fn4qlv1rw66hfdafh4dnfzpsfcwnp7n")))

(define-public crate-notation_fretted-0.2.0 (c (n "notation_fretted") (v "0.2.0") (d (list (d (n "notation_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "13s6rld6pb83x3d1d86q8qifcaikxrgxp1lc2bh5ay5pbib90aa5")))

(define-public crate-notation_fretted-0.3.0 (c (n "notation_fretted") (v "0.3.0") (d (list (d (n "notation_core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "01vs81pas0wk841l11mkariqcr8b96616k2wgqnr7ksxj93i473c")))

(define-public crate-notation_fretted-0.3.1 (c (n "notation_fretted") (v "0.3.1") (d (list (d (n "notation_core") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "06b08bpwvhwrd9c5w8h29cspd6w6q639vycg9mkaj7xwnkck941q")))

(define-public crate-notation_fretted-0.3.2 (c (n "notation_fretted") (v "0.3.2") (d (list (d (n "notation_core") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "18nrbpbvvxa7a9gbl911y5ihgrh8ng0amak7z9052lckfkkkzc8k")))

(define-public crate-notation_fretted-0.3.4 (c (n "notation_fretted") (v "0.3.4") (d (list (d (n "notation_core") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "0v2giian87bcg2jpp91i826y2lfp706li4n2iiahr21vxg0q9ip5")))

(define-public crate-notation_fretted-0.4.0 (c (n "notation_fretted") (v "0.4.0") (d (list (d (n "notation_core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "02hskkj7aw0qdyhsw2vwcbfpzr8cy6wsrqm8psn9wblbc918bpzp")))

(define-public crate-notation_fretted-0.4.3 (c (n "notation_fretted") (v "0.4.3") (d (list (d (n "notation_core") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "1d3dham9r2xscdcw103pplyw4pql7bx8hhla4jcwq091742x5a7s")))

(define-public crate-notation_fretted-0.4.4 (c (n "notation_fretted") (v "0.4.4") (d (list (d (n "notation_core") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "18i16vxxryb4244c91fap4m9vhbmgalvaw4brdwxl1wq4iyyy1l1")))

(define-public crate-notation_fretted-0.5.0 (c (n "notation_fretted") (v "0.5.0") (d (list (d (n "notation_core") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 0)))) (h "0a5l1zl0wcgfcdnqk3m4qrdq1lpf94sc1ilp28afidyqy3s5z23p")))

(define-public crate-notation_fretted-0.6.0 (c (n "notation_fretted") (v "0.6.0") (d (list (d (n "notation_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (d #t) (k 0)))) (h "1ysrksyjm1c1kb3kchyjp0xp855nldh9ff2sv6vpkcdpziq49zal")))

