(define-module (crates-io no ta notation_audio) #:use-module (crates-io))

(define-public crate-notation_audio-0.3.4 (c (n "notation_audio") (v "0.3.4") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy_kira_audio") (r "^0.7.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)))) (h "1j5f78rf8n0g5pw55m1ggqycpa37g1h51jgvwzs84qwwz2lndgdx")))

(define-public crate-notation_audio-0.4.0 (c (n "notation_audio") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy_kira_audio") (r "^0.8.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)))) (h "1rmbblpsfccmch3zzsdbhh7kp2a0j43fp36np6caaysmdi0a1297")))

(define-public crate-notation_audio-0.6.0 (c (n "notation_audio") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "ringbuffer") (r "^0.15.0") (d #t) (k 0)))) (h "0d23ar2zkrkqj6srlswnvaqfp2npc9k7q98z76kav1xk6jhwyzxy") (f (quote (("default" "bevy/bevy_audio" "bevy/bevy_asset"))))))

