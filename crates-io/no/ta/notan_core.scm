(define-module (crates-io no ta notan_core) #:use-module (crates-io))

(define-public crate-notan_core-0.4.0 (c (n "notan_core") (v "0.4.0") (d (list (d (n "web-sys") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1hsckcnz6qq5mg77wnbd9nd5cddi2fbsblw05iwpngrn6mgvmskx") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.4.1 (c (n "notan_core") (v "0.4.1") (d (list (d (n "web-sys") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0niyb9klm3by3q4y2jhrvp64xdkij4kmcgxi0gh7vyhjg3vlbxry") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.4.2 (c (n "notan_core") (v "0.4.2") (d (list (d (n "web-sys") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "07ijb5vvdsk726g5m3j1yxd22ady1fjlkmzi7ayx8y1355grr6zn") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.5.0 (c (n "notan_core") (v "0.5.0") (d (list (d (n "web-sys") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0q5vp660923d20rlc3n9l7rynx51n8dyj8wn0043mk20b0bpkgcp") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.5.1 (c (n "notan_core") (v "0.5.1") (d (list (d (n "web-sys") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0ls79jdnys9cs0q2dcknn927x7i290f04khxa2n09gzn9g0yl189") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.6.0 (c (n "notan_core") (v "0.6.0") (d (list (d (n "web-sys") (r "^0.3.58") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "060iimhzrnkln657w0izgppiv1z2fki742yrsq83lrw99325xcr1") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.7.0 (c (n "notan_core") (v "0.7.0") (d (list (d (n "web-sys") (r "^0.3.59") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1nhcfprcbn7mngrnyd4g3kfwmabdsfb3lj6qy9i57d2l7jrfrqhd") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.7.1 (c (n "notan_core") (v "0.7.1") (d (list (d (n "web-sys") (r "^0.3.59") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0mkvb5bhyjw0r41bkp3aqm3y8nhbilbjfiyb30ad5729d66c4fi3") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.8.0 (c (n "notan_core") (v "0.8.0") (d (list (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0y9w740mlxsm38c7xjhshrh7h19p3zgjw8mz8i0dikw9vbm50yaz") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.0 (c (n "notan_core") (v "0.9.0") (d (list (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1vargv90al11f327hi5j8pbkrq07iiri5l0hqfrg4ddblgjbr5ib") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.1 (c (n "notan_core") (v "0.9.1") (d (list (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1k0nh7xnz4zp161q2sgbhdryp8flhwlrzhmmvijjnm3mizi7p928") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.2 (c (n "notan_core") (v "0.9.2") (d (list (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "04v7zp9xfniif8ryz2r8mz7yl9s5zk4kws1zfkhda8xlwknz8zqb") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.3 (c (n "notan_core") (v "0.9.3") (d (list (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "00nc1wrpjyfgys71nnn2q5ymhsj09baiqp7sm9ff58zc5l3iv5w8") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.4 (c (n "notan_core") (v "0.9.4") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1rhlm4mjr7amzia84ap2n8fbj73s89c9rdqp7688wdrvp3ppsjsb") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.9.5 (c (n "notan_core") (v "0.9.5") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "049f9ykgmb3n4m7v8k51226f8rbk5013n6q35a013wrhgx5676qd") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.10.0 (c (n "notan_core") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "01rwxy8mkafndrwfn88h4jhpwikfy08x6rng30l48h6jxxynv2jz") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.11.0 (c (n "notan_core") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1h6s45cr5pqbgf95aik4dlklpp4x10nnp1a7rykq6861ja0j6w8z") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

(define-public crate-notan_core-0.12.0 (c (n "notan_core") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1mzhq7d7k0crx3dar788m2dh7cja4shizlgqz659iiidp8j892r3") (f (quote (("links") ("drop_files" "web-sys" "web-sys/File") ("clipboard"))))))

