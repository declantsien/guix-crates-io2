(define-module (crates-io no ta notary-client) #:use-module (crates-io))

(define-public crate-notary-client-0.1.0 (c (n "notary-client") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 1)))) (h "1yi8aar34sy0n5pr1sv35947k6izz5b3nji0w75rfibll5683vr0") (y #t)))

