(define-module (crates-io no te notedown-rs) #:use-module (crates-io))

(define-public crate-notedown-rs-0.0.1 (c (n "notedown-rs") (v "0.0.1") (d (list (d (n "colored") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "159w56aqwbc32g0hjnzbz2dg9zpmmp2b6i7fwy1z3gr0f2jhrp84") (f (quote (("default" "colored"))))))

(define-public crate-notedown-rs-0.0.2 (c (n "notedown-rs") (v "0.0.2") (d (list (d (n "colored") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "04gjrmvlhah245s0gwzvh4hdr883aq8j9a0jfh0ni9z8ph4yic7v") (f (quote (("default" "colored"))))))

