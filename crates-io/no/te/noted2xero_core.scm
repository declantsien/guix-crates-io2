(define-module (crates-io no te noted2xero_core) #:use-module (crates-io))

(define-public crate-noted2xero_core-1.11.2 (c (n "noted2xero_core") (v "1.11.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "19aln8s9sw299q37l24m1xxab63sbfa68ya53qhmc5c9hhm9gy2c")))

(define-public crate-noted2xero_core-1.11.4 (c (n "noted2xero_core") (v "1.11.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lwh9irz5kssmipjy7n43rqyv65szg7a77w6pqbxss64c4b05j18")))

(define-public crate-noted2xero_core-1.11.5 (c (n "noted2xero_core") (v "1.11.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1mpvlnd3m1zb0zaaq0zmk2kivazlzwv6770rbnjd0q53fbxbhck8")))

(define-public crate-noted2xero_core-1.11.6 (c (n "noted2xero_core") (v "1.11.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0s7qmiqjb5fja0ysyim557pj9rz9nywvlx5fdllmxzkffq7jnlq4")))

(define-public crate-noted2xero_core-1.11.7 (c (n "noted2xero_core") (v "1.11.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1f66cywqklbhi9a6siaan9r3i6wrkfwmpjb8wimr1wb3ycjz37f8")))

(define-public crate-noted2xero_core-1.11.8 (c (n "noted2xero_core") (v "1.11.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0anjcrbclp8q5wgij4czwp6c6qvlajw2zhp71a366svhzjszr979")))

(define-public crate-noted2xero_core-1.11.10 (c (n "noted2xero_core") (v "1.11.10") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1f5l2qxwgjkv247lkzddvzkj13mz6661w1hxpg90jpbyk5wcjh52")))

