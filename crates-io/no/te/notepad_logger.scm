(define-module (crates-io no te notepad_logger) #:use-module (crates-io))

(define-public crate-notepad_logger-0.1.0 (c (n "notepad_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (k 0)))) (h "079n2xhfzavp4pafbjvfmgb0m9k5fannsvh3kai27rnxii967r6k")))

(define-public crate-notepad_logger-0.1.1 (c (n "notepad_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (k 0)))) (h "02f78ja1w4m8lkfgnivd7p5kksxh88cx9r8xyk1rpry5ys2zjl6d")))

