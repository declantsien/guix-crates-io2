(define-module (crates-io no te notem) #:use-module (crates-io))

(define-public crate-notem-0.1.0 (c (n "notem") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "0l42865k4wx6sb9qjillw6klyn1j6vprp0ypjkbmb15zb2cdxidv")))

(define-public crate-notem-0.2.0 (c (n "notem") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "1i802yx2851bk86iwdgjpjdzxciy8cps9lf4mvbsh5n86nizcbfn")))

(define-public crate-notem-0.3.0 (c (n "notem") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "1d60q5hdwhhwdvr2wicv8knpyx5l7nr3kagnchlcgzzc5s235lfz")))

