(define-module (crates-io no te notes-cli) #:use-module (crates-io))

(define-public crate-notes-cli-0.1.0 (c (n "notes-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "confy") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (d #t) (k 0)))) (h "0kwiwfzikwq2z9scgrd4dw51r1l9b2zksj1i0kzvi93yrhmvxlan")))

(define-public crate-notes-cli-0.1.1 (c (n "notes-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "confy") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08pgrjk4vx3jnpimwfphv3il8n5m59h28n7cs6fgkg8gv4xryd3p")))

(define-public crate-notes-cli-0.1.2 (c (n "notes-cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "confy") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17k9znns9bzwhgvhk3xrw6515f7y0b2yk9yi7q3fbapckx5sa4fx")))

