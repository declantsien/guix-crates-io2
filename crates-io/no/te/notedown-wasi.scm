(define-module (crates-io no te notedown-wasi) #:use-module (crates-io))

(define-public crate-notedown-wasi-0.0.0 (c (n "notedown-wasi") (v "0.0.0") (d (list (d (n "rctree") (r "^0.6.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.23.0") (d #t) (k 2)) (d (n "wit-bindgen-rust") (r "^0.23.0") (d #t) (k 2)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 2)))) (h "1d88mgm5hr6w9qjppv28bg3mbzdhrkakkj2yf2kgb1ik55hx381c") (f (quote (("default"))))))

(define-public crate-notedown-wasi-0.0.1 (c (n "notedown-wasi") (v "0.0.1") (d (list (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)))) (h "09yfc28j4w309iwkqp7va55nc5zxhpibb0g59a4pir3labvpf0yk") (f (quote (("default"))))))

(define-public crate-notedown-wasi-0.0.2 (c (n "notedown-wasi") (v "0.0.2") (d (list (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)))) (h "05yygl8byxaq81j8kn58jld6m8hxv15nrxmbzmih8bns67slb9cp") (f (quote (("default"))))))

(define-public crate-notedown-wasi-0.0.3 (c (n "notedown-wasi") (v "0.0.3") (d (list (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)))) (h "1aqn4kz04hdz945628if0759bqqir3ndcxliss97r93k03qrbld9") (f (quote (("default"))))))

(define-public crate-notedown-wasi-0.0.4 (c (n "notedown-wasi") (v "0.0.4") (d (list (d (n "serde_json") (r "^1.0.115") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)))) (h "017h82bqpsqv81z8bykwbwj29cm2ln09i0k4y9nlki5lk9kxh0wl") (f (quote (("default"))))))

(define-public crate-notedown-wasi-0.0.5 (c (n "notedown-wasi") (v "0.0.5") (d (list (d (n "serde_json") (r "^1.0.115") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.23.0") (d #t) (k 0)))) (h "1vjd14zfkipjbbhpbbfac70k3v0iga642l6vaimc8pz3aig7mzbn") (f (quote (("default"))))))

