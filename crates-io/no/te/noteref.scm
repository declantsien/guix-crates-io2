(define-module (crates-io no te noteref) #:use-module (crates-io))

(define-public crate-noteref-0.1.0 (c (n "noteref") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04jhlxhylks567kzxak36x07621ab4czcs9vr16jh9hb0p8y2j0m")))

(define-public crate-noteref-0.2.0 (c (n "noteref") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vs1020dbanr74dgfdl9z33hfgah1n3igxkgbc759q2z111byq2s")))

(define-public crate-noteref-0.3.0 (c (n "noteref") (v "0.3.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g3bxgczl097m9hd159l9xjrzx2qpcxmsn0kshc1slw627npb4wz")))

(define-public crate-noteref-0.4.0 (c (n "noteref") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nsj3kvkbffnl1cp7y8qaw9f2vr2xz8bs9bi49f19zw2fjqb1z7s")))

(define-public crate-noteref-0.5.0 (c (n "noteref") (v "0.5.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xwmbxf9g2cj5w1g5x9rkrjm8rrzn0fpj291rv1ng7jmz76y7asl")))

(define-public crate-noteref-0.6.0 (c (n "noteref") (v "0.6.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p6yjbqx37vmbid3wikkvpcyg1hy3gaawmzm302c8iz2wvdxl5g8")))

(define-public crate-noteref-0.7.0 (c (n "noteref") (v "0.7.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v2kv63cnljnhwjn7wph9jb9vkmbaggl5gqz9pllggx30xizkm8l")))

(define-public crate-noteref-0.8.0 (c (n "noteref") (v "0.8.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rzbm4xiq5bf5xyh8y1p5zqhc2rnajqzfwba02xqpn13j77sayhy")))

(define-public crate-noteref-0.8.1 (c (n "noteref") (v "0.8.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15d7xq4hfzcw8s585j23q3jq54s5j1w968wk0km2lbmgil019npm")))

(define-public crate-noteref-0.9.0 (c (n "noteref") (v "0.9.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jzzn5srbl53wvqfq0rgc2sbm2hpg8bd904mfmv6znrn3i9x1hny")))

(define-public crate-noteref-0.9.1 (c (n "noteref") (v "0.9.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "194811z80l9xdrmga6mqwdm22x2lrzkfywxhvs5rqyq8j7xsjahz")))

