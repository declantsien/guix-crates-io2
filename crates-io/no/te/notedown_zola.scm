(define-module (crates-io no te notedown_zola) #:use-module (crates-io))

(define-public crate-notedown_zola-1.0.0 (c (n "notedown_zola") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0sdd2h56xysfmk02d3fbayss77k03i19nmngkraqs3jpvar2ihrp")))

