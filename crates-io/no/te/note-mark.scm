(define-module (crates-io no te note-mark) #:use-module (crates-io))

(define-public crate-note-mark-0.0.1 (c (n "note-mark") (v "0.0.1") (d (list (d (n "peekmore") (r "^1.2.0") (d #t) (k 0)))) (h "1yf06bxpmqhgcb00gghird8z5n40ay7jhqwwyx9fqdd9swpzd6gw")))

(define-public crate-note-mark-0.0.2 (c (n "note-mark") (v "0.0.2") (d (list (d (n "peekmore") (r "^1.2.0") (d #t) (k 0)))) (h "1fr3d7lvwgbnra3gzgz2r947npb4i88mk846cpzpwg8mfagl1pfj")))

