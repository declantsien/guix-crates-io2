(define-module (crates-io no te notedown_parser) #:use-module (crates-io))

(define-public crate-notedown_parser-0.1.0 (c (n "notedown_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1ymycwc26gq0wilwc45479azzcs5fcd5brwygix2zx69s8y4fh1w")))

(define-public crate-notedown_parser-0.2.0 (c (n "notedown_parser") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "14bhl3zpklbnrpp3lfp7cd8s6053fii87qi88h4amd75qkiy4y2p")))

(define-public crate-notedown_parser-0.3.0 (c (n "notedown_parser") (v "0.3.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0w25ppbccjwjvb72j618c6b2vrskcf6d2bv49m48j7j020mc53v8")))

(define-public crate-notedown_parser-0.4.0 (c (n "notedown_parser") (v "0.4.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1wdfc5qraa4302g0zsxnqc48v5raal9jim62xf8qlscsylv34sar")))

(define-public crate-notedown_parser-0.4.2 (c (n "notedown_parser") (v "0.4.2") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0jj8zqzdx48jsj79bb1fw84qvp7gbr2ppp7y539di1xcrvfwzakr")))

(define-public crate-notedown_parser-0.4.3 (c (n "notedown_parser") (v "0.4.3") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1y7vc27csv9abkzivp3l25mrqmhcnjab0h0jlkair9wij1snan0b")))

(define-public crate-notedown_parser-0.5.0 (c (n "notedown_parser") (v "0.5.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "03wdj9hmny2v07d7pwyzsg016j7xhr6syan15n5s4yix55l1yqzb")))

(define-public crate-notedown_parser-0.6.0 (c (n "notedown_parser") (v "0.6.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)))) (h "1r9iwlbijl6yz3xhfz41s971lccpa82xcszs3cmj6510p5yphdxi")))

(define-public crate-notedown_parser-0.6.1 (c (n "notedown_parser") (v "0.6.1") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)))) (h "168v2qlhzllxk0ysmk3q76a0y3nxcy29sf9rqydfxra5vk17gza0")))

(define-public crate-notedown_parser-0.6.2 (c (n "notedown_parser") (v "0.6.2") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)))) (h "1pnhrr0f9pa0bsgmkxj0v72s8rvfak2qavj5yadmy4aciagv4jrq")))

(define-public crate-notedown_parser-0.7.0 (c (n "notedown_parser") (v "0.7.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)))) (h "099ygawigs4nhpigwldjc5s9fpdfxj9d0l6pzfdx76qycfgm6hs5")))

(define-public crate-notedown_parser-0.7.1 (c (n "notedown_parser") (v "0.7.1") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1shlymqrwg4yqw7cw0w473yy3g8x6vhzfp1rpwg4hkc5jialw10i")))

(define-public crate-notedown_parser-0.7.2 (c (n "notedown_parser") (v "0.7.2") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1hqccxx7cb118xf6kci7hajdzc2bgq25zh4infwj4ybzhf7avzfp")))

(define-public crate-notedown_parser-0.7.3 (c (n "notedown_parser") (v "0.7.3") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0gy5848fwdk977hrxgw9y58b7y0wc31hf40g3iq3dq2shidg4s3k")))

(define-public crate-notedown_parser-0.7.4 (c (n "notedown_parser") (v "0.7.4") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1z36a63a4lzn0wck70nlywkfivxl795l7jky4ijbz370bz9zlhdr")))

(define-public crate-notedown_parser-0.8.0 (c (n "notedown_parser") (v "0.8.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11nc3r3sr1hi384xyhicmkq4y2dr0bfr3pfsnx080knyl3hkh6yy")))

(define-public crate-notedown_parser-0.8.1 (c (n "notedown_parser") (v "0.8.1") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.10") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0kjin85xp0d4nb7xxmss1vbwxrkbjjll4qianwywhrxbp1r6vg86")))

(define-public crate-notedown_parser-0.8.2 (c (n "notedown_parser") (v "0.8.2") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.10") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1x9kyxfwpvbb89yf1ggqfgxanxmk318qdbjn0752mwhzs045i9av")))

(define-public crate-notedown_parser-0.9.0 (c (n "notedown_parser") (v "0.9.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0g21kdkx4n3zwggidx0z9n10dgfhh8d36a7qqjczx7ddaxm2dh6v")))

(define-public crate-notedown_parser-0.11.0 (c (n "notedown_parser") (v "0.11.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "01x174772xbgqj53fpc8a8zf3c16fd9gf2d1rfvcvxg36x65lxni")))

(define-public crate-notedown_parser-0.12.0 (c (n "notedown_parser") (v "0.12.0") (d (list (d (n "notedown-pest") (r "^1.0") (d #t) (k 0)) (d (n "notedown_ast") (r "^0.12") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1k7hjc7d2214d9z3g1jy5bsmcg52076phvavmnh7dvzjw27xi5vl")))

