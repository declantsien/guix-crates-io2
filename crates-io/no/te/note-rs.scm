(define-module (crates-io no te note-rs) #:use-module (crates-io))

(define-public crate-note-rs-0.1.0 (c (n "note-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 0)))) (h "1b9mci7h6n8z7lz2pc720kjs92z4cgf2lybyr5rk3h650milpw5j") (y #t)))

(define-public crate-note-rs-0.1.1 (c (n "note-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)))) (h "1kjp0d0ag0kcx1pmn7w7g7wimkhk0j1vg6l5f5rdzd16fz7n1b9z") (y #t)))

