(define-module (crates-io no te notedown_fmt) #:use-module (crates-io))

(define-public crate-notedown_fmt-0.2.0 (c (n "notedown_fmt") (v "0.2.0") (d (list (d (n "notedown_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "120q3l8n6fa55fsc4299k2fzk2bfsiif9ar0c6b5r2fa6930zp6h")))

(define-public crate-notedown_fmt-0.3.0 (c (n "notedown_fmt") (v "0.3.0") (d (list (d (n "notedown_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "pangu") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1xciyh2im20shn1jdxzmwdy9llsk6bj8sab8cgkv4x95ac6vk6gn")))

(define-public crate-notedown_fmt-0.3.1 (c (n "notedown_fmt") (v "0.3.1") (d (list (d (n "notedown_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "pangu") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "19wxlvgxsvri1iq5j6iijca3kf2sagxf3d9mxycki6g493i6x35h")))

(define-public crate-notedown_fmt-0.3.2 (c (n "notedown_fmt") (v "0.3.2") (d (list (d (n "notedown_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "pangu") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "text-utils") (r "^0.2.1") (d #t) (k 0)))) (h "0p1j4ghwahx59d7kx029c9zyifvdhb6sxz7q2syfldj6mxwp2bln")))

