(define-module (crates-io no te noter) #:use-module (crates-io))

(define-public crate-noter-0.1.1 (c (n "noter") (v "0.1.1") (h "1qnd33kz6im8ld7x9vp6fapq0p51zq9xvq69j8aayg87scy7wjc2")))

(define-public crate-noter-0.2.0 (c (n "noter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "04cjpdgm12vnb14im1dxw3hg9vfxm6b4sdx0wppfgm40g8p16z3p")))

(define-public crate-noter-0.3.0 (c (n "noter") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q2d6gxk9dpg781f4cpzaq8hbmn4g9idcz8hg5sbi4ib77nd9kk9")))

