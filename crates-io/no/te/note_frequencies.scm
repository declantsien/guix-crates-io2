(define-module (crates-io no te note_frequencies) #:use-module (crates-io))

(define-public crate-note_frequencies-0.1.0 (c (n "note_frequencies") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "0zxngar2cpmjjyvmq35qdg5qmyz88d9r1y5qd95crf3db0ava8bs")))

(define-public crate-note_frequencies-0.1.1 (c (n "note_frequencies") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "19za7jc059004qj3nh0fj2ihlqrgnhk6frhq30yk2jvrch26nhc9")))

