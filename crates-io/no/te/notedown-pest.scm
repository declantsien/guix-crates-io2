(define-module (crates-io no te notedown-pest) #:use-module (crates-io))

(define-public crate-notedown-pest-1.0.0 (c (n "notedown-pest") (v "1.0.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1yi6d2hlpa4qs0q3qzgxm3r21qpfwydz63bxbmv05r86fsgcmpa7")))

