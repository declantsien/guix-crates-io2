(define-module (crates-io no te noted) #:use-module (crates-io))

(define-public crate-noted-0.1.0 (c (n "noted") (v "0.1.0") (d (list (d (n "crt0stack") (r "^0") (d #t) (k 2)) (d (n "goblin") (r "^0") (d #t) (k 2)))) (h "05jmpvnra3y3bgwx75m6v23sgy25qckn0alwvlyvlwbibf5mpg51")))

(define-public crate-noted-1.0.0 (c (n "noted") (v "1.0.0") (d (list (d (n "crt0stack") (r "^0") (d #t) (k 2)) (d (n "goblin") (r "^0") (d #t) (k 2)))) (h "15yka5yjvszhy0ds9g87c8hq83gnhmdrmd8y8qdjpwgmikl3vvf9")))

