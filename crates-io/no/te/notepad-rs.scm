(define-module (crates-io no te notepad-rs) #:use-module (crates-io))

(define-public crate-notepad-rs-0.1.0 (c (n "notepad-rs") (v "0.1.0") (h "077hzq24x7q0g876aw4ihcpna00jhq9fljv1jlg1pvyn12hxzfyz") (y #t)))

(define-public crate-notepad-rs-0.0.0 (c (n "notepad-rs") (v "0.0.0") (d (list (d (n "iced") (r "^0.3.0") (d #t) (k 0)))) (h "1ljf81i37958z7x741qyy8b84snma2540vn96nkwfpisvrh9fa84")))

