(define-module (crates-io no tz notzero) #:use-module (crates-io))

(define-public crate-notzero-1.0.0 (c (n "notzero") (v "1.0.0") (d (list (d (n "typewit") (r "^1.8") (k 0)))) (h "0a2bbvm8qr8iraliyd5lddsg6mr83w2xgrqz5gp2mwc0ffkk4syg")))

(define-public crate-notzero-1.0.1 (c (n "notzero") (v "1.0.1") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "typewit") (r "^1.8") (k 0)))) (h "13casw3a8dasblkr2z1cfav9a16j680jsvwj6bdrfr26sca18iki")))

