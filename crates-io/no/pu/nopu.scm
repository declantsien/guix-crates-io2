(define-module (crates-io no pu nopu) #:use-module (crates-io))

(define-public crate-nopu-0.1.0 (c (n "nopu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("fs"))) (d #t) (k 0)))) (h "06zqj7d4684wfiqaqvxi6rb73ny16vgn5l8psmfv6fcp2g7krzib")))

