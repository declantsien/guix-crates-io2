(define-module (crates-io no to notox) #:use-module (crates-io))

(define-public crate-notox-0.1.0 (c (n "notox") (v "0.1.0") (h "1xb2bx2qfmkspf6mg6qikf5mdvwwhyazgaip0wfk77bdnv8wiblw")))

(define-public crate-notox-0.2.0 (c (n "notox") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rr62sjym282zz7zvqxrh9z386111x8hv7hccv3ndbg5pcw1sggk") (f (quote (("default" "serde")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.0 (c (n "notox") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00sz20skzjf846v1cibajbz3mhlpnzl24m3q7n9ws2zp63m5fxc3") (f (quote (("default" "serde")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.1 (c (n "notox") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0khwn2k0y82yrd3sybjv13k5bg2ay54kns49ivxq93c3acgi1pwl") (f (quote (("default" "serde")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.2 (c (n "notox") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pi6dizrf2k838g0ad3gz3x7iycrn9gxi85lvzw5lr1d67mm2ncx") (f (quote (("default" "serde")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.3 (c (n "notox") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kaykk68svmn10sjyfz8myw30wfzpd12bqc6h29gj6cxaajh3mii") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.4 (c (n "notox") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x24aaigv8bh8znkd53bfnisgz6ja6ill6zrp1vbijiz24plqy2i") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.5 (c (n "notox") (v "1.0.5") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "071dg8fyrajmczia1yqs3yyfiqzqs1yil04nvk6sp5j2sllpqmn7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-notox-1.0.6 (c (n "notox") (v "1.0.6") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fsdqkkdqdwxv819lrpj57m05x17sq975q6vvmw0qx4niw6c0cc9") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

