(define-module (crates-io no to notoize) #:use-module (crates-io))

(define-public crate-notoize-1.0.0 (c (n "notoize") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01pyllwrg8zzp8dqh5j78b295ibq6rkqlicisqscjbbbhx455yyd")))

(define-public crate-notoize-1.1.0 (c (n "notoize") (v "1.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "12sxh260wx35lzpz6ivwa6mk80nr1n5sg75w1s5ib7xmsnijfcn1")))

(define-public crate-notoize-1.2.0 (c (n "notoize") (v "1.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1abrrhrw97h3ysw7jyxv6bjg8jsy6k1psg9b8p7631jlwjhfn884")))

(define-public crate-notoize-1.3.0 (c (n "notoize") (v "1.3.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^1.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0am4wpig8zjvszvnsvm5bg5cvmiqld080plal1pqk1mcgr1yaliv")))

(define-public crate-notoize-1.4.0 (c (n "notoize") (v "1.4.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^2.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1sff73kjck1a3cmfvzmhmxwpk0ylpnfwaqxncr6zvx6yvyb2fcjy")))

(define-public crate-notoize-1.5.0 (c (n "notoize") (v "1.5.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "gh-file-curler") (r "^2.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0mq92w737biafh16xsg1c1vjxmym15r7ncx0vbli0vvd4nmj2fr6")))

(define-public crate-notoize-2.0.0 (c (n "notoize") (v "2.0.0") (d (list (d (n "gh-file-curler") (r "^2.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "05x8202nxvqnwsf6n2dqjvk41kj1fqrh9g46vinjk9b1lvc7878z")))

(define-public crate-notoize-2.1.0 (c (n "notoize") (v "2.1.0") (d (list (d (n "gh-file-curler") (r "^2.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0n9bkx4chj5zdj7ngmlraxpx5m8l5riqdrrwjbx8w7vwap0w86cw")))

(define-public crate-notoize-2.2.0 (c (n "notoize") (v "2.2.0") (d (list (d (n "gh-file-curler") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0qpdald06irnh9j4d5pap8iykr9157c8fbkdm5x5wd3p08vcl77n")))

(define-public crate-notoize-2.2.1 (c (n "notoize") (v "2.2.1") (d (list (d (n "gh-file-curler") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0cxqs2zc2jjalfrpr7hax3ijr7kl87ar32f70h9l6znjk7ww3jb5")))

(define-public crate-notoize-2.2.2 (c (n "notoize") (v "2.2.2") (d (list (d (n "gh-file-curler") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1mdfmiy4jzvp2hj3581hjw2bkmk1rly4mavm5wg178nc89dvrkps")))

(define-public crate-notoize-2.2.3 (c (n "notoize") (v "2.2.3") (d (list (d (n "gh-file-curler") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0qyyq57fmrrl836j4xwa7l7wx25fqrngx9qslafgd4ry6vpsk6rh")))

(define-public crate-notoize-2.2.4 (c (n "notoize") (v "2.2.4") (d (list (d (n "gh-file-curler") (r "^2.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0wjdk5zcfa4vc2rf2q502h58xy1b97mhfiy2wczl06gbipiwfm28")))

(define-public crate-notoize-2.3.0 (c (n "notoize") (v "2.3.0") (d (list (d (n "gh-file-curler") (r "^2.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "18qma8zcr1z65j66241n46a32fiygzqfkbssq8cifravbcwlj2vn")))

(define-public crate-notoize-2.4.0 (c (n "notoize") (v "2.4.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1gl2f29bwjjp378zc9ml0dqdaiwgwn2fwcdzjr5vqw1z1h7v35l2")))

(define-public crate-notoize-2.5.0 (c (n "notoize") (v "2.5.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "14qmdmk3m4yx96yg9h6wvkicp5kn4fb9nf9fpjn12chvl340pjs8")))

(define-public crate-notoize-2.5.1 (c (n "notoize") (v "2.5.1") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0mpp2ysk8d31jkxjv3yyp3s802s46bdqi64alc8di78pnnci4aad")))

(define-public crate-notoize-2.6.0 (c (n "notoize") (v "2.6.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1f2x93hq6nxyqagvf9ps91y59n14x17k2qgp5h5ab4l3kfznn72q")))

(define-public crate-notoize-2.6.1 (c (n "notoize") (v "2.6.1") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1ik4g1gaw8rrwk1mihvwmzf10wpvcj9haixlglh2pzgh0yaxkhif")))

(define-public crate-notoize-2.7.0 (c (n "notoize") (v "2.7.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1k9zggxcjsi3jb9v4044fnbv27lq07s42s5nqvbpwvd52wsqg5v2")))

(define-public crate-notoize-2.8.0 (c (n "notoize") (v "2.8.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0kwnmd64apdvf330ppj0sh8078v785nh209s9v2x6wp0dbs90h08")))

(define-public crate-notoize-2.9.0 (c (n "notoize") (v "2.9.0") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "18gbbfvdg3vmbzb0mbfm1fjz0sf6zw9qi6dla1w4pmni3ysj4agq")))

(define-public crate-notoize-2.9.1 (c (n "notoize") (v "2.9.1") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01ab172wj9mv122cs1w40mj61wh5gsj207l7fi0kixcd5c0f6xas")))

(define-public crate-notoize-2.9.2 (c (n "notoize") (v "2.9.2") (d (list (d (n "gh-file-curler") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0ipl28agvlk9qfp0czc9w7wmph1aagvl5bg3qpxzxnxrwxblpsix")))

