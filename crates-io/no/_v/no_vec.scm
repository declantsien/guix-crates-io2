(define-module (crates-io no _v no_vec) #:use-module (crates-io))

(define-public crate-no_vec-0.1.0 (c (n "no_vec") (v "0.1.0") (h "1gqjw28f06p66kivysci2rlkr5jfiijvpbznnvinwk2w4iwnqj0n")))

(define-public crate-no_vec-0.3.0 (c (n "no_vec") (v "0.3.0") (h "1h52l6mnmx4c6z8bw6j9p6373z40962cqsq7dys779vdxcd036hs")))

