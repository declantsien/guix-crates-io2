(define-module (crates-io no _v no_vpn) #:use-module (crates-io))

(define-public crate-no_vpn-0.1.0 (c (n "no_vpn") (v "0.1.0") (d (list (d (n "base16") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1lr2i44s1x0drviwflb131vk1qbsi66fj8pcsm1rl7704mr482ig")))

(define-public crate-no_vpn-0.1.1 (c (n "no_vpn") (v "0.1.1") (d (list (d (n "base16") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "09kcaimjd99yf1kfhzbhmwfz4rjwa8sdxn607rznh5i6kbnmmjz4")))

