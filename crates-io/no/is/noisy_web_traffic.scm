(define-module (crates-io no is noisy_web_traffic) #:use-module (crates-io))

(define-public crate-noisy_web_traffic-0.2.0 (c (n "noisy_web_traffic") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1flhzdff68sr2h5d6v1klhr16xl9zi1iwdm17h2xvxdzdsy405ax")))

