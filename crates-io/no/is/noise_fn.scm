(define-module (crates-io no is noise_fn) #:use-module (crates-io))

(define-public crate-noise_fn-0.1.0 (c (n "noise_fn") (v "0.1.0") (d (list (d (n "higher_order_functions") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sized_matrix") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0a9xh0fsg59aihvn5jdym9wd320x0yhvh3p5ahfzrf2ya1wax45y")))

(define-public crate-noise_fn-0.1.1 (c (n "noise_fn") (v "0.1.1") (d (list (d (n "higher_order_functions") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sized_matrix") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ygaf8cv50cs9az9a437804p3azj0c0j32wzc1w4kvm20lj6qf5p")))

(define-public crate-noise_fn-0.1.2 (c (n "noise_fn") (v "0.1.2") (d (list (d (n "higher_order_functions") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sized_matrix") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "wyhash") (r "^0.4") (d #t) (k 0)))) (h "1hkkqhh9jp3mdkcxzd4vzm1cmckj47fcjjw9bi96ysg2mznniw04")))

(define-public crate-noise_fn-0.2.0 (c (n "noise_fn") (v "0.2.0") (d (list (d (n "higher_order_functions") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sized_matrix") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)) (d (n "wyhash") (r "^0.5.0") (d #t) (k 0)))) (h "1b2pg59nipgx89j2w3j6f3csjlyvkhdi9a911mq18xs979brx4k9")))

