(define-module (crates-io no is noise-protocol) #:use-module (crates-io))

(define-public crate-noise-protocol-0.0.1 (c (n "noise-protocol") (v "0.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0g881i690ck0243n94k6hyj3rzx2vdm972880jgm2nhm935nqw0k")))

(define-public crate-noise-protocol-0.0.2 (c (n "noise-protocol") (v "0.0.2") (h "138aq10fbnvk1a5rpvq2zzaaap8sc1fz3ck2bx2ln6s7djkzaky7")))

(define-public crate-noise-protocol-0.0.3 (c (n "noise-protocol") (v "0.0.3") (h "09qci581yld2dbp3qzam8cqk4h3wa65l5xpydb307nn2mar3n3kf")))

(define-public crate-noise-protocol-0.0.5 (c (n "noise-protocol") (v "0.0.5") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "17920w6qscpg8lyzasmznfjbqy29ja18bxy9qlbzyf9zc244azrj") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.1.0 (c (n "noise-protocol") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)))) (h "0vqr9rhlibndlpadv0gmish616pkswc25v5y4aldk9hx307j4dkg") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.1.1 (c (n "noise-protocol") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5") (k 0)))) (h "13lixynhws4b5nas2ybwim6lx9smjfg806vrz6zj34dxf03z9mwp") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.1.2 (c (n "noise-protocol") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5") (k 0)))) (h "0i63d6xffnafpwnf4x1m4bcfvvq8hkz6rxm7w76wmbjzjgpfkskj") (f (quote (("use_std") ("default" "use_std")))) (y #t)))

(define-public crate-noise-protocol-0.1.3 (c (n "noise-protocol") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5") (k 0)))) (h "097zls47ky249mmmr8xlvq62zv91awcbgl9w3ii8g15i7bc9zk2y") (f (quote (("use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.1.4 (c (n "noise-protocol") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)))) (h "0ax5l7g3f9z9f05065n7jldyx4px1lmywgvkgr6vnlgydp9p9d2g") (f (quote (("use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.2.0-rc1 (c (n "noise-protocol") (v "0.2.0-rc1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)))) (h "1ngv6an1s4mwcdvz4g4ih4ym15j7zzdbwrxv5lsyki4f9iiybk7f") (f (quote (("use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-noise-protocol-0.2.0 (c (n "noise-protocol") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)))) (h "1lxi33n4yr564lmni1i26nzi2mkdyycpvvxacfizafd8i6bd6wr4") (f (quote (("use_std") ("use_alloc") ("default" "use_std"))))))

