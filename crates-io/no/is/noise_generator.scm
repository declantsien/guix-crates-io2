(define-module (crates-io no is noise_generator) #:use-module (crates-io))

(define-public crate-noise_generator-0.1.0 (c (n "noise_generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1ya588ydak7snxzz0irsyll5kj6m5xs7fc5vs85xcf9glr97pdc0")))

(define-public crate-noise_generator-0.1.1 (c (n "noise_generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1whg1s75dyxak0mdi1nnmfvgjjld8dj9h5mz1mg99rqnf5hpymks")))

