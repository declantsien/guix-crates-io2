(define-module (crates-io no is noise) #:use-module (crates-io))

(define-public crate-noise-0.0.1 (c (n "noise") (v "0.0.1") (h "0xa9c8r4gw9aqpz1nc52jajzfary1b9vfx22bnkw8p9w5bxcb2bp")))

(define-public crate-noise-0.0.3 (c (n "noise") (v "0.0.3") (h "18ii7kz2igx5npwljdbqvirkjd14g7mvxpivyzls5rg0vdqa7b7m") (y #t)))

(define-public crate-noise-0.0.2 (c (n "noise") (v "0.0.2") (h "164c2r7hqa4a5sasbzf1jsxgkjmnb81qvd138in57cw3ssa9pyd2")))

(define-public crate-noise-0.0.4 (c (n "noise") (v "0.0.4") (h "17j9c8a6zlir6gj4lkqz789lj4mv848i99z13rzbghrcxmdind7r")))

(define-public crate-noise-0.1.0 (c (n "noise") (v "0.1.0") (d (list (d (n "image") (r "*") (d #t) (k 2)))) (h "0siilw2inb8fdgf95lm7vv8r94qri3iy60jjanicjjgl8zs1836w")))

(define-public crate-noise-0.1.1 (c (n "noise") (v "0.1.1") (d (list (d (n "image") (r "*") (d #t) (k 2)))) (h "0qbm618ix0n2s88g4lvs8v1m9cpzihrv030pmlzrvnn73g0cbis0")))

(define-public crate-noise-0.1.2 (c (n "noise") (v "0.1.2") (d (list (d (n "image") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "0vqzv8bb96m8hnnfph9yajv63fk9i3pavjdrbflfi7lyvd15qh3l")))

(define-public crate-noise-0.1.3 (c (n "noise") (v "0.1.3") (d (list (d (n "image") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "042gdswz53djc2wlpcc0055ax4z1vlihvkhj768vyblm4jv89dia")))

(define-public crate-noise-0.1.4 (c (n "noise") (v "0.1.4") (d (list (d (n "image") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "17v2fxp1m0s69bz9f32746qxjmpm842xsd99rx307kys8j424d23")))

(define-public crate-noise-0.1.5 (c (n "noise") (v "0.1.5") (d (list (d (n "image") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1a7z9lpdx9v569w5148f1bal5ilqgw2hqqcffc5hcsshpxvg2jim")))

(define-public crate-noise-0.2.0 (c (n "noise") (v "0.2.0") (d (list (d (n "image") (r "^0.7") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0qvgzz5wy7k7ccgahb8sh5jj190kjai75fgszskkaz573dbcrvib")))

(define-public crate-noise-0.3.0 (c (n "noise") (v "0.3.0") (d (list (d (n "image") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "112dhfbgyjd3cqcba9bba7bi9aqnvlph9imyndbxsgj1pfgqw2dq")))

(define-public crate-noise-0.4.0 (c (n "noise") (v "0.4.0") (d (list (d (n "image") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1a3dr4gxp12yj7wdja95pw1khyadz46kgprm3rhnzzb556c2iq8c")))

(define-public crate-noise-0.4.1 (c (n "noise") (v "0.4.1") (d (list (d (n "image") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cq03ndf66pya869jj4gpw3f7ss18d7pxp65d0lxvcg71b8jn5bp")))

(define-public crate-noise-0.5.0 (c (n "noise") (v "0.5.0") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "12jiy252j0sd117kkhm5b7i02bnpppwxfn7qk4mg819mx6wks055")))

(define-public crate-noise-0.5.1 (c (n "noise") (v "0.5.1") (d (list (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0h811gqadyig8ccr52z9rahbvfdgc1drzbbykf8ra7x3z3a38fls")))

(define-public crate-noise-0.6.0 (c (n "noise") (v "0.6.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)) (d (n "image") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0w7p5cgqwiyyjdz4n7pdwlix03pgb20ah7m02fv9g8fq9mvjax9k") (f (quote (("default" "image"))))))

(define-public crate-noise-0.7.0 (c (n "noise") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0hsbw9gpsz8w9msvyvddygagd9wj93hqpg5pxz388laxfkb1s1c2") (f (quote (("default" "image"))))))

(define-public crate-noise-0.8.0 (c (n "noise") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "01wkih2xqvqnhvabck6g6yx25z31rp3aparf8bxsy5p3iv6ya8gi") (f (quote (("std") ("images" "image" "std") ("default"))))))

(define-public crate-noise-0.8.1 (c (n "noise") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0gdia6q1sqv33ms5s0iwngqkmhl9sr7rhlshqcrvxif2shx3xk0k") (f (quote (("std") ("images" "image" "std") ("default"))))))

(define-public crate-noise-0.8.2 (c (n "noise") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0h0f30lc14clbdsrwz8s9yndxzx4g5q2ra0cq6332yb8f7hnka4v") (f (quote (("std") ("images" "image" "std") ("default"))))))

(define-public crate-noise-0.9.0 (c (n "noise") (v "0.9.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 0)))) (h "1k7hrwb958lcmkvwk94cmm7vh3h6ps0a6y2xcvy55qgj6f1mr93d") (f (quote (("std") ("images" "image" "std") ("default"))))))

