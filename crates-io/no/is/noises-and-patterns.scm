(define-module (crates-io no is noises-and-patterns) #:use-module (crates-io))

(define-public crate-noises-and-patterns-0.1.0 (c (n "noises-and-patterns") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)))) (h "0nhnkaxd2axh8x179gw3g0p4gl32zbajxdw0fs8j6rlqn919iz5s")))

(define-public crate-noises-and-patterns-0.1.1 (c (n "noises-and-patterns") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)))) (h "0w2bakcplw4khvsszb060m7c0a4dgji3yiicymdll82qlwmgipjj")))

(define-public crate-noises-and-patterns-0.1.2 (c (n "noises-and-patterns") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)))) (h "0pvj83240yly27lmlwgd9gxhwf6qrbm9clh4iqli74zbpbmjzmbv")))

(define-public crate-noises-and-patterns-0.1.3 (c (n "noises-and-patterns") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)))) (h "1jlkck3n56f8jb90zlci6a532jk8zzn01yvjz30h5j7b94dfz2a4")))

