(define-module (crates-io no is noise-ring) #:use-module (crates-io))

(define-public crate-noise-ring-0.0.1 (c (n "noise-ring") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.7.1") (d #t) (k 0)))) (h "1y2fzac5j598dn0pr5i7cjgn8wfmygz070py4acdhaf1kzvp0mws")))

(define-public crate-noise-ring-0.0.2 (c (n "noise-ring") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.2") (d #t) (k 0)) (d (n "ring") (r "^0.7.1") (d #t) (k 0)))) (h "1fy57l72z9gja2w0h3yrbkz1vlks6cz51yywzzn8jj31z09n6yx3")))

(define-public crate-noise-ring-0.0.3 (c (n "noise-ring") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.3") (d #t) (k 0)) (d (n "ring") (r "^0.7.1") (d #t) (k 0)))) (h "1b0zxhb2xf0m5m5yx7phdd76w95a7pppv19ry9k7ly2mjyb4m5kw")))

(define-public crate-noise-ring-0.0.5 (c (n "noise-ring") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.5") (d #t) (k 0)) (d (n "ring") (r "^0.13.2") (d #t) (k 0)))) (h "1i6qvhl4hiasv069gjs8l2asak0ida7aqan6w1g855njw7jc7236")))

(define-public crate-noise-ring-0.1.0 (c (n "noise-ring") (v "0.1.0") (d (list (d (n "noise-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)))) (h "1cb3y3xn017n0vvsmdck6d0gb27h4h6pyxqpj8bcisf4m2g2wb82")))

(define-public crate-noise-ring-0.2.0-rc.1 (c (n "noise-ring") (v "0.2.0-rc.1") (d (list (d (n "noise-protocol") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "05bmq3kwcsqz0hc93lzkxrks6ikx1k63k13sxvmgh3wh1qnqwwwb")))

(define-public crate-noise-ring-0.2.0 (c (n "noise-ring") (v "0.2.0") (d (list (d (n "noise-protocol") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "ring") (r "^0.17") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "02rpydzgjf489s627apdbakxcxfrspmkz2bvnacg9hp9szsjbrcx")))

