(define-module (crates-io no is noise_search_deps_librocksdb-sys) #:use-module (crates-io))

(define-public crate-noise_search_deps_librocksdb-sys-0.1.0 (c (n "noise_search_deps_librocksdb-sys") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.2") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1xizkn484cfr0v8bmf8aa0bd0iw1d1pfc3jpgy4gwgl1pa820mk9") (f (quote (("static") ("default" "static"))))))

