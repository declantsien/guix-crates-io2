(define-module (crates-io no is noise-functions-config) #:use-module (crates-io))

(define-public crate-noise-functions-config-0.0.0 (c (n "noise-functions-config") (v "0.0.0") (d (list (d (n "noise-functions") (r "^0.0.0") (k 0)))) (h "1zp9vv19sxrgj8ip7r7dydmjmbj0kzag7g1wgc94d87hjq9p50dj") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

(define-public crate-noise-functions-config-0.1.0 (c (n "noise-functions-config") (v "0.1.0") (d (list (d (n "noise-functions") (r "^0.1.0") (k 0)))) (h "0ihm4qhxfz3iqbhbggrvdhm4y67jigkhqqsvxvz2bx5hbfzk67vx") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

(define-public crate-noise-functions-config-0.2.0 (c (n "noise-functions-config") (v "0.2.0") (d (list (d (n "noise-functions") (r "^0.1.2") (k 0)))) (h "1vflvrp981h260aswf2lg39g1xgymbi88s0jjrpygd67qx3cycv3") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

(define-public crate-noise-functions-config-0.3.0 (c (n "noise-functions-config") (v "0.3.0") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "noise-functions") (r "^0.1.2") (k 0)))) (h "1zbrs1i97sfpdfb60mi15kfi4l6b2i7a6l188jhvcyyw9gwbg0yf") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std")))) (y #t)))

(define-public crate-noise-functions-config-0.3.1 (c (n "noise-functions-config") (v "0.3.1") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "noise-functions") (r "^0.1.3") (k 0)))) (h "1a39rhxfb6aka5v41nm0s0nhvrpdrhngsikb5chh2c98wlz53ak0") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

(define-public crate-noise-functions-config-0.4.0 (c (n "noise-functions-config") (v "0.4.0") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "noise-functions") (r "^0.2.0") (k 0)))) (h "1wl0xqg3sk14cxglmd4w7s6z6mwvp619r8hqzwsqin071z31b9kd") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

(define-public crate-noise-functions-config-0.4.1 (c (n "noise-functions-config") (v "0.4.1") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "noise-functions") (r "^0.2.1") (k 0)))) (h "1wvmvhsdrfaaml3mlvhqv20i83x4cd65yhhbbiiv3zyczwh3y6w9") (f (quote (("std" "noise-functions/std") ("nightly-simd" "noise-functions/nightly-simd") ("libm" "noise-functions/libm") ("default" "std"))))))

