(define-module (crates-io no is noiserand) #:use-module (crates-io))

(define-public crate-noiserand-1.0.0 (c (n "noiserand") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "isahc") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)))) (h "085bc1y95pdra3p274d1q2dmzvpancr5ssd5dyy6d9hn5j1nkl12")))

