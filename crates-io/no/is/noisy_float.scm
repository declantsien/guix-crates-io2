(define-module (crates-io no is noisy_float) #:use-module (crates-io))

(define-public crate-noisy_float-0.1.0 (c (n "noisy_float") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "13kmq3mdadkrkhzljx03pb0qh7gzb5fl7ss0jycjdlpmk26n8wwy")))

(define-public crate-noisy_float-0.1.1 (c (n "noisy_float") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "1ihndgmf3ryx171hfsfxc1qcswl8799ndjq37iayck200ikimf2h")))

(define-public crate-noisy_float-0.1.2 (c (n "noisy_float") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)))) (h "0f6l37bvmmik3ac0adi0w9g72g53s28jddxplxg4h8jcsfycr2wv")))

(define-public crate-noisy_float-0.1.3 (c (n "noisy_float") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qdkh4dqp634ik2k3ghldsr89f0yxpdcswg0ynxxf576v2qaisam") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.4 (c (n "noisy_float") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.1.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13dbfkfqm59cgn4jxpz2bj1lsr175ijsvdlzyyy54yc3skvxzivi") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.5 (c (n "noisy_float") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wb1nawbhnkwk8aghlpbf0g0dfsx8igsmpwlar04a9y1kwcsyx4a") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.6 (c (n "noisy_float") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v02dl33pqg0r7cm0v9hwk35bv8cq5jx5w3zzh8j3qj3szd8816r") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.7 (c (n "noisy_float") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iz4qhh94qwzcvvvzhsibrk0kgkai86lfn6j8c11gbjaa4kmyj93") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.8 (c (n "noisy_float") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1h1hi30ngn8kzihs7snwyqp1prwizh0kwy5pbyvlkwyl96ljac29") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.9 (c (n "noisy_float") (v "0.1.9") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a102q6cw2fzf0vwd48n37crrj7lz7jmkxp8m8b5a9z8r4ch0wj0") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.10 (c (n "noisy_float") (v "0.1.10") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0l6z9fm2xgq47zaqjyqkdiy9lnsy7rpnvn4g18pw7ybag13aqq95") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.11 (c (n "noisy_float") (v "0.1.11") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d1jrqvcfj3jjfyqwma17q9qba75mia3i0wgrm3lyq1c8p35w6a8") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.12 (c (n "noisy_float") (v "0.1.12") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bnkcjfbcg451jkdaapbln3cydciw2mpsk0x9cxw43d78yyy3cxi") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.13 (c (n "noisy_float") (v "0.1.13") (d (list (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17c0k0v6ik1dsxw08bz6618m4dw9rd487zag10ccv8cjwg6ick51") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.14 (c (n "noisy_float") (v "0.1.14") (d (list (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bv8xnrhizih9mbrpcifyiikz1vbc75dnycsi2cl2xkm21gvkj4q") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.1.15 (c (n "noisy_float") (v "0.1.15") (d (list (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n12j2pma99867b3wz0garyf34kzy2ppv62sh16cw39c0vw8ksrx") (f (quote (("serde-1" "serde"))))))

(define-public crate-noisy_float-0.2.0 (c (n "noisy_float") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bqkl82pfp98i98s3van73hkvqcx5p55dm1wagg57gy0xgkfd3wp")))

