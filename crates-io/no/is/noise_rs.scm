(define-module (crates-io no is noise_rs) #:use-module (crates-io))

(define-public crate-noise_rs-0.1.0 (c (n "noise_rs") (v "0.1.0") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.3") (d #t) (k 0)))) (h "13qnwi1ddyvslnxanshr5v3lhrv82ijrss9pgrh6isqi7naxc761")))

(define-public crate-noise_rs-0.1.1 (c (n "noise_rs") (v "0.1.1") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.3") (d #t) (k 0)))) (h "0fwx27z01j61l8hcarsmr4n4nkja3gsh4frc85dkp145xfw054zb")))

(define-public crate-noise_rs-0.1.2 (c (n "noise_rs") (v "0.1.2") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.3") (d #t) (k 0)))) (h "0qbv6mzs1q04z764xga1q693d57mkqipilgpznzy57l4m5ap2nww")))

(define-public crate-noise_rs-0.1.4 (c (n "noise_rs") (v "0.1.4") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)))) (h "0wyvsmcavn3vgsk3kjaxx9557f46zimpb12hyl9hjnw89v5dwcfa")))

(define-public crate-noise_rs-0.1.5 (c (n "noise_rs") (v "0.1.5") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)))) (h "19l4r41gyfb3y855fnndiyf7y2lipnhhrxwbshg76mw917hkr43f")))

(define-public crate-noise_rs-0.1.6 (c (n "noise_rs") (v "0.1.6") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)))) (h "1zknawswm3dv00fh835qads6ikj4p2qczla7kmm2q2j6k7b8grpq")))

(define-public crate-noise_rs-0.1.7 (c (n "noise_rs") (v "0.1.7") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1v416z9ldn8vxpvv1vmb2jl2sdq7as10n14cvi8c9165cs72j2xs")))

(define-public crate-noise_rs-0.1.8 (c (n "noise_rs") (v "0.1.8") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0s8fcsz5klwvhhcszxn2qp9nyv8hf8d2026qpkds34am3la8hqd3")))

(define-public crate-noise_rs-0.1.9 (c (n "noise_rs") (v "0.1.9") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0vclznpj12plp6gwvxfff2mm93gwm6myxvpvdbqbx0ih9a4d87iw")))

(define-public crate-noise_rs-0.1.10 (c (n "noise_rs") (v "0.1.10") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0nnmpc1zljpfd97c7z0cc6fmhpry0mygam4p6d5rs8l0ifcyci9g")))

(define-public crate-noise_rs-0.1.11 (c (n "noise_rs") (v "0.1.11") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "13qnnbrz0srj2sw3cw39446jxcd8ll2hl7m9ksrzlln700aczls8")))

(define-public crate-noise_rs-0.1.12 (c (n "noise_rs") (v "0.1.12") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0zszvwcwqgpi73dmgkdh8xfqjsbzndhz1jy4ps3lrf4wfq81yq3w")))

(define-public crate-noise_rs-0.1.13 (c (n "noise_rs") (v "0.1.13") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1xmxyzyl03mh9ignarjvyi53kal3r3hl0afs9gmq0v91srr2q5sy")))

(define-public crate-noise_rs-0.1.14 (c (n "noise_rs") (v "0.1.14") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0v4g597s0wh1b7l9n59y1avdmcdwrlxnfzdgb46hfc2pz3x3ff6r")))

(define-public crate-noise_rs-0.1.15 (c (n "noise_rs") (v "0.1.15") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1shj77v9myiifka5dh7ym9awc3b7aqax72abhbr4j4bkm4l2aqfq") (f (quote (("default" "const_fn") ("const_fn" "java_random/const_fn"))))))

(define-public crate-noise_rs-0.1.16 (c (n "noise_rs") (v "0.1.16") (d (list (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1h93mnr71z58lwyhws51vxyq8065dvp4649caff8lpbz8c8inb1s") (f (quote (("default" "const_fn") ("const_fn" "java_random/const_fn"))))))

