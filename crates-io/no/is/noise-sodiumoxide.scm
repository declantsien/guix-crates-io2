(define-module (crates-io no is noise-sodiumoxide) #:use-module (crates-io))

(define-public crate-noise-sodiumoxide-0.0.1 (c (n "noise-sodiumoxide") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.14") (k 0)))) (h "15akqcnflanc5hwk7ip7yi790nj1dlmf3hv5c5d580mks2i6i6k4")))

(define-public crate-noise-sodiumoxide-0.0.2 (c (n "noise-sodiumoxide") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.14") (k 0)))) (h "0sb3nsa9l3mflinkhkb0vh21hgq2w6k9gw4cf50yj52ffzggvh4l")))

(define-public crate-noise-sodiumoxide-0.0.3 (c (n "noise-sodiumoxide") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.14") (k 0)))) (h "0xr5zmir96kchaz0wh7qvjs2b4kjgxvk9fzaji3ja4i054jim69k")))

(define-public crate-noise-sodiumoxide-0.0.5 (c (n "noise-sodiumoxide") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.16") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.5") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "0ynzzk610dqg2vs2hf0ixfn5vbr7x18lqbsfg46nqq3va1z85fj9")))

(define-public crate-noise-sodiumoxide-0.0.6 (c (n "noise-sodiumoxide") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.1") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.0.5") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.1") (d #t) (k 0)))) (h "0i43ijiaz1y37hjrnq2s098s9qjvqxgqinq6k1c02jby74l4wp5r")))

(define-public crate-noise-sodiumoxide-0.1.0 (c (n "noise-sodiumoxide") (v "0.1.0") (d (list (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0fjz01gxnwk6h2wgn4dpr00c09d5fis9bi2wq3f2m0cbcz39vrl5")))

(define-public crate-noise-sodiumoxide-0.1.1 (c (n "noise-sodiumoxide") (v "0.1.1") (d (list (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "noise-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1ica15mmhd7vvcpx20fwjxqvs3g4556njicha4nhippha1d1n326")))

