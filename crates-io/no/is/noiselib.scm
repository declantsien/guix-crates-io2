(define-module (crates-io no is noiselib) #:use-module (crates-io))

(define-public crate-noiselib-0.1.0 (c (n "noiselib") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "0b2vrb3rp6spkyfypxijsynvx4320fj9i2ini9xh5v0ph8g472x7")))

(define-public crate-noiselib-0.2.0 (c (n "noiselib") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "157pyr3dv5kxwxml0iy91mhcagjfyp13k8jjknmlk37vgcw4ig1a")))

(define-public crate-noiselib-0.2.1 (c (n "noiselib") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "0qpgjwxnchkqhmf15q6rvlyacka1j2pnqaws31a2ghdak0jg718d")))

(define-public crate-noiselib-0.2.3 (c (n "noiselib") (v "0.2.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "0j71gc6xg419h1x7j1by1kr59d7ykcyqs67cyvidybnflympsclh")))

(define-public crate-noiselib-0.2.4 (c (n "noiselib") (v "0.2.4") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "1h54wnikrawajdx5mwrdd66f1mdbvif6hsqgsyfn2kw9h5zpqhbg")))

