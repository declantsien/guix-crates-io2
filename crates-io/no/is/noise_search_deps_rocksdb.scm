(define-module (crates-io no is noise_search_deps_rocksdb) #:use-module (crates-io))

(define-public crate-noise_search_deps_rocksdb-0.1.0 (c (n "noise_search_deps_rocksdb") (v "0.1.0") (d (list (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "noise_search_deps_librocksdb-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1p7d8rl12q3dpyw9cm26rxjvg6whg2sds524xnf0b204jiw3z6j7") (f (quote (("valgrind") ("default"))))))

(define-public crate-noise_search_deps_rocksdb-0.1.1 (c (n "noise_search_deps_rocksdb") (v "0.1.1") (d (list (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "noise_search_deps_librocksdb-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0pmpl52ixznnfy1x6dqdc92hr9iqn2hs4ff5naajf5xyikqzzvjl") (f (quote (("valgrind") ("default"))))))

