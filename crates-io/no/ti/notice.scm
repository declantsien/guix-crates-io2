(define-module (crates-io no ti notice) #:use-module (crates-io))

(define-public crate-notice-0.1.0 (c (n "notice") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "notice-eventfd") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "notice-pipe") (r "^0.1.0") (d #t) (t "and(cfg(unix), not(target_os = \"linux\"))") (k 0)) (d (n "notice-test") (r "^0.1.0") (d #t) (k 2)))) (h "08z2d22siv78ps46p4z1jjj6illdza7s5738clr96ags8v0p9mk4")))

