(define-module (crates-io no ti notify-me) #:use-module (crates-io))

(define-public crate-notify-me-0.1.0 (c (n "notify-me") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)))) (h "0zp5s7ip9dglcfvg32ij58ilp8lqw8bwykpw58q9yxdxyi0pr9rh")))

(define-public crate-notify-me-0.2.0 (c (n "notify-me") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lettre") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)))) (h "0qf7889wd0d7iglc8pygs3i6m4whwlhhmy58q18jzvv1vylwls8r")))

