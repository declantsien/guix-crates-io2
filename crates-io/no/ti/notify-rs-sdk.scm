(define-module (crates-io no ti notify-rs-sdk) #:use-module (crates-io))

(define-public crate-notify-rs-sdk-0.0.1 (c (n "notify-rs-sdk") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "08fpvrpr185vb3bpgdw1zw45rii3f8ny992f8gckn3mzva7vrjkm") (y #t)))

