(define-module (crates-io no ti notice-pipe) #:use-module (crates-io))

(define-public crate-notice-pipe-0.1.0 (c (n "notice-pipe") (v "0.1.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "notice-core") (r "^0.1.0") (d #t) (k 0)) (d (n "notice-test") (r "^0.1.0") (d #t) (k 2)))) (h "0ykj7jgjnsfk9cspwja9wa6zz32i49gnkf1sw7y14lwbggr7sn3a")))

