(define-module (crates-io no ti notion-flows-macros) #:use-module (crates-io))

(define-public crate-notion-flows-macros-0.1.0 (c (n "notion-flows-macros") (v "0.1.0") (d (list (d (n "notion-wasi") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "035ifc7dcc4i9pw8i5xgzpflf36hfk1znn4kqyr8yxylb3jlfy56")))

