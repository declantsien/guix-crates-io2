(define-module (crates-io no ti notipriv) #:use-module (crates-io))

(define-public crate-notipriv-1.0.0 (c (n "notipriv") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "human-panic") (r "^1") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shadow-rs") (r "^0.23") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.23") (d #t) (k 1)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0935kd3g6i54cf067qnr787vxdmss6axldq4ri6nbd0385mndfgf")))

