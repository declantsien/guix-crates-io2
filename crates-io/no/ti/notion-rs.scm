(define-module (crates-io no ti notion-rs) #:use-module (crates-io))

(define-public crate-notion-rs-0.1.0 (c (n "notion-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.5.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1552h6rm2f7bb75phlr1d0fqfacd1p52kk98lqyad3lbc4h58rk3") (f (quote (("default" "client") ("client" "reqwest"))))))

