(define-module (crates-io no ti notifier) #:use-module (crates-io))

(define-public crate-notifier-0.1.0 (c (n "notifier") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "palaver") (r "^0.1") (d #t) (k 0)) (d (n "tcp_typed") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jcqksmf09mwn98jp1wlj6lh01rl5zji6scbmd5mkaq2ym330zah")))

(define-public crate-notifier-0.1.1 (c (n "notifier") (v "0.1.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "tcp_typed") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "100p0lba8n5cnmdbzvvnim4vgfcgx4gmphna1fdadmdf8ymdvsk8")))

(define-public crate-notifier-0.1.2 (c (n "notifier") (v "0.1.2") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "tcp_typed") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1xqj1lw7vska0ll83js7v2z28g81l53mf1dnsg1y8lzczpglqxsn")))

(define-public crate-notifier-0.1.3 (c (n "notifier") (v "0.1.3") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "tcp_typed") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "01iswqdr5g5zpzxjln0l8a76g8sq5q329l1j9y4l7427kwmnlk6y")))

