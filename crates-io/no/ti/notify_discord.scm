(define-module (crates-io no ti notify_discord) #:use-module (crates-io))

(define-public crate-notify_discord-0.1.0 (c (n "notify_discord") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serenity") (r "^0.11.5") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0j4k0hsyq7kp065w5j7yk5j67ib1g40wc33l1x80vzwsmkq18rb9") (y #t)))

