(define-module (crates-io no ti notify-stream) #:use-module (crates-io))

(define-public crate-notify-stream-0.1.0 (c (n "notify-stream") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "=5.0.0-pre.6") (d #t) (k 0)))) (h "02kf229mv2c0ycf714vwd54glrjyg0hmf1d1bbdsl0drv4mnc51a")))

(define-public crate-notify-stream-0.1.1 (c (n "notify-stream") (v "0.1.1") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "=5.0.0-pre.6") (d #t) (k 0)))) (h "1b4caj0w936z5jfmdx8msyaaiid8qdlld7g50nwbjklvwba34nh6")))

