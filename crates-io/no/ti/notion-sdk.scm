(define-module (crates-io no ti notion-sdk) #:use-module (crates-io))

(define-public crate-notion-sdk-0.0.0 (c (n "notion-sdk") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde" "clock"))) (k 0)) (d (n "emojis") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0xnx2930ig2lfardkn4rrhn6rns0fj5dp3glrwhl9a9yyz55lxzb")))

