(define-module (crates-io no ti notify_send) #:use-module (crates-io))

(define-public crate-notify_send-0.0.1 (c (n "notify_send") (v "0.0.1") (d (list (d (n "dbus") (r "^0.0.9") (d #t) (k 0)))) (h "1d94cc8x1f0b37ck5x1fbid5530xw421z0q87qa01i5sky8lqzck") (y #t)))

(define-public crate-notify_send-0.0.2 (c (n "notify_send") (v "0.0.2") (d (list (d (n "dbus") (r "^0.0.9") (d #t) (k 0)))) (h "03fpniwff1ycg3w1acb1d7x4a41gxk9g31kw50jawh40xrrqdns0") (y #t)))

(define-public crate-notify_send-0.0.3 (c (n "notify_send") (v "0.0.3") (d (list (d (n "dbus") (r "^0.0.9") (d #t) (k 0)))) (h "00d952xaazs352i0s6g4v371kd6h3swmmblmbmsy4crkxss0gsjm") (y #t)))

(define-public crate-notify_send-0.0.4 (c (n "notify_send") (v "0.0.4") (d (list (d (n "dbus") (r "^0.0.9") (d #t) (k 0)))) (h "168x5na8gnmq72ksn0wahanvgdj1v5jqadcpb0qx20428as9qlxa") (y #t)))

(define-public crate-notify_send-0.0.5 (c (n "notify_send") (v "0.0.5") (d (list (d (n "dbus") (r "^0.0.9") (d #t) (k 0)))) (h "1frapc8kkc49mzqibxdyvjnhrwgpxy60s63y0wkp3hamh99h1ybc") (y #t)))

