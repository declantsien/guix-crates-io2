(define-module (crates-io no ti notion-flows) #:use-module (crates-io))

(define-public crate-notion-flows-0.1.0 (c (n "notion-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.11.1") (d #t) (k 0)) (d (n "notion-wasi") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "13pb8fqhj50xn07hsjd2knzj9ld1bppihcyh5c464j0n8qyv7ygk")))

(define-public crate-notion-flows-0.1.1 (c (n "notion-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.11.1") (d #t) (k 0)) (d (n "notion-wasi") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "06sz5z8s7wq2lz3z1d7m96k8iihnxd9k29bha2dyvrrib6ig0845")))

(define-public crate-notion-flows-0.2.0 (c (n "notion-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "notion-flows-macros") (r "^0.1") (d #t) (k 0)) (d (n "notion-wasi") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "0kdzjva4gfj3rmy66v6cny1gzid5g1j35qnvd8mh6lnzfdm22hpy")))

