(define-module (crates-io no ti notifica) #:use-module (crates-io))

(define-public crate-notifica-1.0.0 (c (n "notifica") (v "1.0.0") (d (list (d (n "mac-notification-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "125l1hwxn2wr7gk9hyqfdcickqvhp032spy2h700ddm818kx5iid")))

(define-public crate-notifica-1.0.1 (c (n "notifica") (v "1.0.1") (d (list (d (n "mac-notification-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "177wngdgsanfgz54y7s65rpv63030kal8afd8lnivjbln87fykw1")))

(define-public crate-notifica-1.0.2 (c (n "notifica") (v "1.0.2") (d (list (d (n "mac-notification-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.5.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05386nnlvz9i9fsvk0vlbhhy3piz196knjw3cwafq4xas3ihpgz4")))

(define-public crate-notifica-2.0.0 (c (n "notifica") (v "2.0.0") (d (list (d (n "mac-notification-sys") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.6.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00xpvf704l6w7fmrkyrlaq4f1djy9lsx99xsw1yykq8g2ag5xhlb")))

(define-public crate-notifica-3.0.0 (c (n "notifica") (v "3.0.0") (d (list (d (n "mac-notification-sys") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.6.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q1ph0y77pcwrqfpn7rrablynh8w16d83hz3ri71nmghpgzyhpb4")))

(define-public crate-notifica-3.0.1 (c (n "notifica") (v "3.0.1") (d (list (d (n "mac-notification-sys") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.6.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ds6yy3f2644w9zcp9j7bkg8d3djaw7swh2n010kl6xc7xm4g11w")))

(define-public crate-notifica-3.0.2 (c (n "notifica") (v "3.0.2") (d (list (d (n "mac-notification-sys") (r "^0.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "notify-rust") (r "^3.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winrt") (r "^0.6.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xz31jdq60dja82q4al8aldn53valqjzbbb6kfbq6lvmz76izs54")))

