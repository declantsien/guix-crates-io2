(define-module (crates-io no ti notifme) #:use-module (crates-io))

(define-public crate-notifme-0.0.1 (c (n "notifme") (v "0.0.1") (h "04fx3jlrca12h2fzci06hys0lq6n0vb2cjkk496bjy92618z3jm0")))

(define-public crate-notifme-0.0.2 (c (n "notifme") (v "0.0.2") (h "042ynd01xx94i0lzvqxacyhjrv78nmqk2jpx7461pmg796s8yi8a")))

