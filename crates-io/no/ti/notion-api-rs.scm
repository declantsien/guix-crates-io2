(define-module (crates-io no ti notion-api-rs) #:use-module (crates-io))

(define-public crate-notion-api-rs-0.1.0 (c (n "notion-api-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mgpvbvdcfwsxy882v73fvdjg35svnby6897bvgn8k84rii2mc7s")))

