(define-module (crates-io no ti notice-eventfd) #:use-module (crates-io))

(define-public crate-notice-eventfd-0.1.0 (c (n "notice-eventfd") (v "0.1.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "notice-core") (r "^0.1.0") (d #t) (k 0)) (d (n "notice-test") (r "^0.1.0") (d #t) (k 2)))) (h "1nf1mwc11yr5r82lrq33zypkk1ffx9mrpiyzav3iwsafyspspi8y")))

