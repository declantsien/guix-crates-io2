(define-module (crates-io no ti notifier-rs) #:use-module (crates-io))

(define-public crate-notifier-rs-0.1.0 (c (n "notifier-rs") (v "0.1.0") (d (list (d (n "slack-hook") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "15il3vg3l1rvr19aihpipxql8dxdidxm0p6gi5h4ja027am4x791")))

(define-public crate-notifier-rs-0.1.1 (c (n "notifier-rs") (v "0.1.1") (d (list (d (n "slack-hook") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "0iqjkc97xj4wjjkcy9rszyb2wslz1sd1py8xcz2fq1zv4zc20xvc")))

(define-public crate-notifier-rs-0.1.2 (c (n "notifier-rs") (v "0.1.2") (d (list (d (n "slack-hook") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "03zmq0nq7h7p1vjq8qx9z5pyh555b7ghjsrqdlgr3wn34qq3x6rj")))

(define-public crate-notifier-rs-0.1.3 (c (n "notifier-rs") (v "0.1.3") (d (list (d (n "slack-hook") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1k172q5ayzj37pmrw69vc6s4yc7rikhdw42h4fb6b07pxfqaq8fs")))

