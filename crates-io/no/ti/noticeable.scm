(define-module (crates-io no ti noticeable) #:use-module (crates-io))

(define-public crate-noticeable-0.1.0 (c (n "noticeable") (v "0.1.0") (h "0wsxsa0d7p56y5jm0988wk35v0glii0ajhhsgx4ia8qg3xpaf744")))

(define-public crate-noticeable-0.2.0 (c (n "noticeable") (v "0.2.0") (h "1z7mw0ylpq4m5s5j74124f5nshjf7vjmizr1day4zvdgbsa0h7p3")))

