(define-module (crates-io no m- nom-bufreader-rp) #:use-module (crates-io))

(define-public crate-nom-bufreader-rp-0.2.0 (c (n "nom-bufreader-rp") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("compat"))) (d #t) (k 2)))) (h "06nwxjbhhqakziwgqy4r2012bjv767mhniq9a111p1x9d2d2big8") (f (quote (("default" "async") ("async" "futures" "async-trait" "pin-project-lite"))))))

