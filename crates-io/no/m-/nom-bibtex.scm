(define-module (crates-io no m- nom-bibtex) #:use-module (crates-io))

(define-public crate-nom-bibtex-0.1.0 (c (n "nom-bibtex") (v "0.1.0") (d (list (d (n "nom") (r "3.0.*") (d #t) (k 0)) (d (n "quick-error") (r "1.2.*") (d #t) (k 0)))) (h "1nf3sd60n4whfnds8qiklxsxf6mkhpm7mh80p42wqmfhndjpa661") (f (quote (("nightly") ("default"))))))

(define-public crate-nom-bibtex-0.1.1 (c (n "nom-bibtex") (v "0.1.1") (d (list (d (n "nom") (r "3.0.*") (d #t) (k 0)) (d (n "quick-error") (r "1.2.*") (d #t) (k 0)))) (h "1v6cckpyqcf3j4hzpdizsxh68x48v301ycvcz78rwjyjnj04i9bi") (f (quote (("nightly") ("default"))))))

(define-public crate-nom-bibtex-0.2.0 (c (n "nom-bibtex") (v "0.2.0") (d (list (d (n "nom") (r "3.0.*") (d #t) (k 0)) (d (n "quick-error") (r "1.2.*") (d #t) (k 0)))) (h "0p1g9ci5qlwvali5bdaq9z14z63wzrp3dgacns1ib0wa9lxdq39j")))

(define-public crate-nom-bibtex-0.3.0 (c (n "nom-bibtex") (v "0.3.0") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.5.2") (d #t) (k 0)) (d (n "nom_locate") (r "^2.0.0") (d #t) (k 0)) (d (n "quick-error") (r "1.2.*") (d #t) (k 0)))) (h "168j3igwx7xzcp02mvrz31yia3197ara9rkynqx8p73vdizjbny9") (f (quote (("trace" "nom-tracable/trace") ("default"))))))

(define-public crate-nom-bibtex-0.4.0 (c (n "nom-bibtex") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.8") (d #t) (k 0)) (d (n "nom_locate") (r "^4.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0ab3h6947zrccx707g2ga0ww8azglkd1gg8fjk5qsvs5akdb7rzh") (f (quote (("trace" "nom-tracable/trace") ("default"))))))

(define-public crate-nom-bibtex-0.5.0 (c (n "nom-bibtex") (v "0.5.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0dyy0ifxpl2y0wg18pddm6hanmxvxq2vmpdsbk9inw0nbvv9nqvp") (f (quote (("trace" "nom-tracable/trace") ("default"))))))

