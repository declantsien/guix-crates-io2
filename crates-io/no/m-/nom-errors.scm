(define-module (crates-io no m- nom-errors) #:use-module (crates-io))

(define-public crate-nom-errors-0.0.1 (c (n "nom-errors") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1n7ljiwwhkp78hg67lzlvw4w964xqwl7ivz1ybjpqa2iqnn6bcmf")))

(define-public crate-nom-errors-0.0.2 (c (n "nom-errors") (v "0.0.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0zgyb6lkwc8ygwkckx3c7dh1li0dkk71dsifhfci5k97k6vkk4fc")))

(define-public crate-nom-errors-0.0.3 (c (n "nom-errors") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "09b15a4wjld0v404l09nv07kvbj7sp6q94rqky8lw06vxi4wn6bd")))

(define-public crate-nom-errors-0.0.4 (c (n "nom-errors") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0qhnp3qa7pvzi59pz11ym52179ygarr4h7b7w32yfdyhh4w2jily")))

(define-public crate-nom-errors-0.0.5 (c (n "nom-errors") (v "0.0.5") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0gxlgy1yis4kb5acab3brngccd2hyjkp5yamnax9gz2bvgxvmc3x")))

(define-public crate-nom-errors-0.0.6 (c (n "nom-errors") (v "0.0.6") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "00i5lasbidfrwij4flwsyfx4wiy8j4vz95q5b31s9lrms4vr60n1")))

(define-public crate-nom-errors-0.0.7 (c (n "nom-errors") (v "0.0.7") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "16ibw1cha80r4nm3p95sv2ywl6xhds57nrbkz1sinr6ssr9m561x")))

(define-public crate-nom-errors-0.0.8 (c (n "nom-errors") (v "0.0.8") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0bkgr4rgh456rrllidk17nkhx8ii4qzan8q60k7dnly5dc5s7vh8")))

(define-public crate-nom-errors-0.0.9 (c (n "nom-errors") (v "0.0.9") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "17isaci67s0wf2rx6vn920yvf9nlmkbz751phdjfxhz4i9n50agl")))

(define-public crate-nom-errors-0.0.10 (c (n "nom-errors") (v "0.0.10") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0y0mv692p03wb4ddkms70k3psd4k4y8ingcbgxz8q9hhmfxd6b9y")))

(define-public crate-nom-errors-0.0.11 (c (n "nom-errors") (v "0.0.11") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1xm6sq90i3h38cw4fhvrj08iyq555v2w48q4rmkpajsm5v3asxc1")))

(define-public crate-nom-errors-0.0.12 (c (n "nom-errors") (v "0.0.12") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "07405hsanxyz4qslivk05jfcf844lcj1bzq3dvg9fxlsxypnncdw")))

(define-public crate-nom-errors-0.0.13 (c (n "nom-errors") (v "0.0.13") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1my4hq270yyxbjqpcszvmvpv9ifswbkixjawpza4qh7lhc448n8v")))

