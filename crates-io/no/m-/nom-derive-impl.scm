(define-module (crates-io no m- nom-derive-impl) #:use-module (crates-io))

(define-public crate-nom-derive-impl-0.8.0 (c (n "nom-derive-impl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0is55541x2537wz5khw4lqb36l3cxj2wxp1ya93mfgqmy32g29dd")))

(define-public crate-nom-derive-impl-0.9.0 (c (n "nom-derive-impl") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1chyph3rk4vd36sslwq7y8gbccdzqys370w7whskvmv9m0l1zib4")))

(define-public crate-nom-derive-impl-0.9.1 (c (n "nom-derive-impl") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "07840l6p0x78yiy3vx4blcfgc623cj2gcwwyabd9vw0c4m12qpjx")))

(define-public crate-nom-derive-impl-0.10.0 (c (n "nom-derive-impl") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0p9ph7sbg2iddrwkcga1782rq9ly8fm6382cv025qa3ws5kw02kp")))

(define-public crate-nom-derive-impl-0.10.1 (c (n "nom-derive-impl") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0qnsvvg74wdpzhbkxrndr7x6vb1kvhrd60hfwz1kw3abm29rl2yd")))

