(define-module (crates-io no m- nom-bitvec) #:use-module (crates-io))

(define-public crate-nom-bitvec-0.1.0 (c (n "nom-bitvec") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "nom") (r "^7.0.0-alpha1") (f (quote ("lexical"))) (k 0)))) (h "1c544l62sb2aa0wxk136wmjfwnc5shiz859058lqbvxl2gm6d64n") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-nom-bitvec-0.2.0 (c (n "nom-bitvec") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (k 0)))) (h "1kcmyf6k1bsda43qgbryg4dz08ak3ba7p782d2jpgp6q4fwk77yk") (f (quote (("default" "alloc") ("alloc"))))))

