(define-module (crates-io no m- nom-derive) #:use-module (crates-io))

(define-public crate-nom-derive-0.1.0 (c (n "nom-derive") (v "0.1.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0bpncqsl2nkggrba2nwsskp1j0iqncrf4xrva9fbrn1gaaph7mg0")))

(define-public crate-nom-derive-0.2.0 (c (n "nom-derive") (v "0.2.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1yl3yzwsx5a5pqwafsggp52g27hxm9fjxpgap890vliq1jkj1rwj")))

(define-public crate-nom-derive-0.3.0 (c (n "nom-derive") (v "0.3.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0scyb3b8lc8zgyjaihkx0pc97kzdsqnidih7lv3fapi8zz0d1i7f")))

(define-public crate-nom-derive-0.4.0 (c (n "nom-derive") (v "0.4.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0iyswd8cv16hc2f43vh3wq7y3jcvikliy0xrsf25chw00f4imh6q")))

(define-public crate-nom-derive-0.5.0 (c (n "nom-derive") (v "0.5.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "112b2cshmqib2b6g3dp40qpmzwbkyl235hnyii518b07y3qffqid")))

(define-public crate-nom-derive-0.6.0-beta1 (c (n "nom-derive") (v "0.6.0-beta1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "061slkjrj44pcidsipjwx878l1a2yh4c2jrpwnirpkc6q9mhkwdd")))

(define-public crate-nom-derive-0.6.0-beta2 (c (n "nom-derive") (v "0.6.0-beta2") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0xnxxw6p0j0mr6a1l2078gl5rd16cn5amba8866677vjslx4ryng")))

(define-public crate-nom-derive-0.6.0 (c (n "nom-derive") (v "0.6.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1v325n2c0nhdr2rvz8i02ygh9c9mqk089gvbbq6mn0mh0qv49vjr")))

(define-public crate-nom-derive-0.6.1 (c (n "nom-derive") (v "0.6.1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0x6qhz3amz80r04pmbin0ywbgaw6mni99h52zwvm8ppd4as5jnmr")))

(define-public crate-nom-derive-0.6.2 (c (n "nom-derive") (v "0.6.2") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0slmzgk9xblhdm4f7nzz0vp1pzqk050874qwlxrbsszyb9qaqz5r")))

(define-public crate-nom-derive-0.6.3 (c (n "nom-derive") (v "0.6.3") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "180fp46pw0sggj9dzhqqjh5pr3s995hc0jdw4jqxzqdxp4sp0iy8")))

(define-public crate-nom-derive-0.7.0 (c (n "nom-derive") (v "0.7.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0hn6acrm8p9wxnfzfyar1piz6g3bqb9vialnl9xd27iaclpq5506")))

(define-public crate-nom-derive-0.7.1 (c (n "nom-derive") (v "0.7.1") (d (list (d (n "nom") (r "^6.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "030xhcs96nv7cnw9pcbrbpfij0249411ay68s8hm9j7blkpydcwy")))

(define-public crate-nom-derive-0.6.4 (c (n "nom-derive") (v "0.6.4") (d (list (d (n "nom") (r "^5.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "00wnb8fn3nhjnfg2giwpyzz5a6rx914ybv9xx4j55ymgnjjsxz8d")))

(define-public crate-nom-derive-0.7.2 (c (n "nom-derive") (v "0.7.2") (d (list (d (n "nom") (r "^6.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.58") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1xwbbkfhr8zrlcknlj75l50rp2b267qm9sd8vphx6gwzyg24n2zz")))

(define-public crate-nom-derive-0.8.0 (c (n "nom-derive") (v "0.8.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive-impl") (r "=0.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fq61dk9s4awns17v6z7dvr1l1w1j84b43gc040cg8xk5f33rv0n")))

(define-public crate-nom-derive-0.9.0 (c (n "nom-derive") (v "0.9.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive-impl") (r "=0.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1j7ddr7313pc69mzqgpvsb00nwrbizjjg5zi9xhx4zh47vi2rv1d")))

(define-public crate-nom-derive-0.9.1 (c (n "nom-derive") (v "0.9.1") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive-impl") (r "=0.9.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "104izdzny7yirrz0kaf9h443f4asqzlzy3fd8ljc3l3yqvhwhvzi")))

(define-public crate-nom-derive-0.10.0 (c (n "nom-derive") (v "0.10.0") (d (list (d (n "nom") (r "^7.0") (f (quote ("std"))) (k 0)) (d (n "nom-derive-impl") (r "=0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1swmm83nicyavqvy4358b9nf05djb3ivi5sgxn5qz4ql8hnh8zh0")))

(define-public crate-nom-derive-0.10.1 (c (n "nom-derive") (v "0.10.1") (d (list (d (n "nom") (r "^7.0") (f (quote ("std"))) (k 0)) (d (n "nom-derive-impl") (r "=0.10.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19vrbhkn47zw5sks17ril7aryzx0ixg62n0ddrxbil48igb47y8z")))

