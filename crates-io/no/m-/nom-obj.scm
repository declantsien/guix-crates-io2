(define-module (crates-io no m- nom-obj) #:use-module (crates-io))

(define-public crate-nom-obj-0.1.1 (c (n "nom-obj") (v "0.1.1") (d (list (d (n "nom") (r "^2.2") (d #t) (k 0)))) (h "0b7ynd39iqn5b9mynxg85ib5r426ny9wzsiqjrq1835ldy5lvisy")))

(define-public crate-nom-obj-0.2.0 (c (n "nom-obj") (v "0.2.0") (d (list (d (n "nom") (r "^2") (d #t) (k 0)))) (h "06w4lbrbv0aissgbb3cjgn69344z27hvq0x4gfhdghasnal078n0")))

