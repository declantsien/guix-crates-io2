(define-module (crates-io no m- nom-syslog) #:use-module (crates-io))

(define-public crate-nom-syslog-0.1.0 (c (n "nom-syslog") (v "0.1.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08mm4r1lijvq73dz4bpz599llmzm30izfmsxvw2dvfh7llrygb31")))

(define-public crate-nom-syslog-0.1.1 (c (n "nom-syslog") (v "0.1.1") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "124a8bm8aksn6bkfqkjd9amjypmg6wmn7gzmf07z8gjf0i34vhmy")))

(define-public crate-nom-syslog-0.1.2 (c (n "nom-syslog") (v "0.1.2") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "186663ymsc4xl9splgnnsxwc9w0lg230flvai3vbhgyh73m0zcs7")))

(define-public crate-nom-syslog-0.1.3 (c (n "nom-syslog") (v "0.1.3") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1awdw131q9hwqy788950wcgzcicg6svp42aiv60k1934cwp5vzky")))

