(define-module (crates-io no m- nom-midi) #:use-module (crates-io))

(define-public crate-nom-midi-0.1.0 (c (n "nom-midi") (v "0.1.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "1m1j1akdphnrc239gg2bb9n5313y732pirjk3kx4a88y8q6n1770") (f (quote (("default") ("continuation_sysex"))))))

(define-public crate-nom-midi-0.2.0 (c (n "nom-midi") (v "0.2.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "0m339454lkshzfmma47cdawciirqdrb7fd07nlf52lc8g84jr8l7") (f (quote (("default") ("continuation_sysex"))))))

(define-public crate-nom-midi-0.4.0 (c (n "nom-midi") (v "0.4.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "02mipc91g8bjq2jwxacnjxnc0ig22sknfbxa3by7cnr9qn6ll2q5") (f (quote (("default") ("continuation_sysex"))))))

(define-public crate-nom-midi-0.5.0 (c (n "nom-midi") (v "0.5.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "17bxz56vm2galak2jqrij5aki50dq1yg4698rkkh5gqb111vvnyk") (f (quote (("default") ("continuation_sysex"))))))

(define-public crate-nom-midi-0.5.1 (c (n "nom-midi") (v "0.5.1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "06ql2sn2hrz3wdzr3r3215304vmjdwpzl5z9i2awmcrrn16hlsbj") (f (quote (("default") ("continuation_sysex"))))))

