(define-module (crates-io no m- nom-pdb) #:use-module (crates-io))

(define-public crate-nom-pdb-0.0.1 (c (n "nom-pdb") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yw430bk65c999gk4ab7b0hcikzzynykbi602mwc4qbc40xmaxzw")))

(define-public crate-nom-pdb-0.0.2 (c (n "nom-pdb") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wdr6m2w9a5s1ah5n283ydzn86h2l5ah566v7j73za6hskny21xp")))

(define-public crate-nom-pdb-0.0.6 (c (n "nom-pdb") (v "0.0.6") (d (list (d (n "atoi") (r "^0.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vxqm4r117s1kskhsdz2sjlq7cv553w9x1b7l36ciww3gv1j863p")))

(define-public crate-nom-pdb-0.0.8 (c (n "nom-pdb") (v "0.0.8") (d (list (d (n "atoi") (r "^0.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ywpa3mi0nyf65gy9pfhvn2na7l9a6bdvhlhrs0qc85widljvl07")))

(define-public crate-nom-pdb-0.0.9 (c (n "nom-pdb") (v "0.0.9") (d (list (d (n "atoi") (r "^0.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12gqjc1fbcvkgry65i1p4nbjkw0qgqvwqx3mh4p8f3bkss3csxfr")))

