(define-module (crates-io no m- nom-reprap-response) #:use-module (crates-io))

(define-public crate-nom-reprap-response-0.1.0 (c (n "nom-reprap-response") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "insta") (r "^0.16.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "0nz5na5chqp2jdbnyd9phmy5vqxb5qvmsajkk58ff7dq251kgb73")))

