(define-module (crates-io no m- nom-psl) #:use-module (crates-io))

(define-public crate-nom-psl-0.1.0 (c (n "nom-psl") (v "0.1.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "05f8md66gh34ab127nnb5a69dkn9qm8bm549lg0cirnh3ryjvwak")))

(define-public crate-nom-psl-1.0.0 (c (n "nom-psl") (v "1.0.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0w6lw6b4y3x0f2m2v3k4w5jr1x5k0rhkmz0zgjz9hl283sgwrv23")))

(define-public crate-nom-psl-1.1.0 (c (n "nom-psl") (v "1.1.0") (d (list (d (n "cache_2q") (r "^0.10") (d #t) (k 0)) (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1inxs9p6l67cx5wdh5lx154c0aa90d7byiby1dx8rr7pyb974qzb")))

(define-public crate-nom-psl-1.2.0 (c (n "nom-psl") (v "1.2.0") (d (list (d (n "cache_2q") (r "^0.10") (d #t) (k 0)) (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1acpfpwy81wpap1a9k7k1xf2l9hxf2xbw8km2hlysjkifhfbbs1m")))

