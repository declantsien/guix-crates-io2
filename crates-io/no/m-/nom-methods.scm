(define-module (crates-io no m- nom-methods) #:use-module (crates-io))

(define-public crate-nom-methods-0.1.0 (c (n "nom-methods") (v "0.1.0") (d (list (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0hsn333qcih2lffhdn8v43aj9345gqj71m7lqmbhmiviipd07hs6")))

(define-public crate-nom-methods-0.2.0 (c (n "nom-methods") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1xlc83b7zfxfvil1zz08dkdjmklfc26a4irsfdn477p84vk65116")))

