(define-module (crates-io no m- nom-recursive) #:use-module (crates-io))

(define-public crate-nom-recursive-0.1.1 (c (n "nom-recursive") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-recursive-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^1.0.0") (d #t) (k 0)))) (h "0ag9s6crzckfzdfiqd78ph3m49cs98li5bwf0znca7dsp8ykgf8z") (f (quote (("tracer256") ("tracer128") ("default"))))))

(define-public crate-nom-recursive-0.2.0 (c (n "nom-recursive") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-recursive-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^2") (d #t) (k 0)) (d (n "nom_locate1") (r "^1") (d #t) (k 0) (p "nom_locate")))) (h "1ch5na84hg55p754zjvd2rbcr88f2xxhxcd4lr9qm8vf7zshqbmi") (f (quote (("tracer256") ("tracer128") ("default"))))))

(define-public crate-nom-recursive-0.3.0 (c (n "nom-recursive") (v "0.3.0") (d (list (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 2)) (d (n "nom-recursive-macros") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "nom_locate") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "nom_locate1") (r ">=1.0.0, <2.0.0") (d #t) (k 0) (p "nom_locate")))) (h "1k5sf0ihb6r7wa9hm7hvjnblkg0scfzakkvdb445n1prsikjkpp0") (f (quote (("tracer256") ("tracer128") ("default"))))))

(define-public crate-nom-recursive-0.4.0 (c (n "nom-recursive") (v "0.4.0") (d (list (d (n "nom") (r "^7") (d #t) (k 2)) (d (n "nom-recursive-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "nom_locate1") (r "^1") (d #t) (k 0) (p "nom_locate")))) (h "01z820j36fkpiqp49z0fc83x38qgra01h7waqiv5gpf91vzqw6cm") (f (quote (("tracer256") ("tracer128") ("default"))))))

(define-public crate-nom-recursive-0.5.0 (c (n "nom-recursive") (v "0.5.0") (d (list (d (n "nom") (r "^7") (d #t) (k 2)) (d (n "nom-recursive-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "0qzck4170bbsv4nlnlcb68l57a7x40pvc47s16789bk0ljb72q33") (f (quote (("tracer256") ("tracer128") ("default"))))))

(define-public crate-nom-recursive-0.5.1 (c (n "nom-recursive") (v "0.5.1") (d (list (d (n "nom") (r "^7") (d #t) (k 2)) (d (n "nom-recursive-macros") (r "^0.5.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "0ypbkb5cjvxvmsjjk3zy0057qdk07mzcrbykkpqsbycpqszydp9q") (f (quote (("tracer256") ("tracer128") ("default"))))))

