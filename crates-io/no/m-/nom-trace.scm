(define-module (crates-io no m- nom-trace) #:use-module (crates-io))

(define-public crate-nom-trace-0.1.0 (c (n "nom-trace") (v "0.1.0") (d (list (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1hbgf44r7lj7rlccc39p130230cxgfiqxyi17w90vsw55nzwcshb")))

(define-public crate-nom-trace-0.2.0 (c (n "nom-trace") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0xnj13zqgdnjd1cyxp4w67z45asl81n9h1ixk8bprilwcsdp1zr0")))

(define-public crate-nom-trace-0.2.1 (c (n "nom-trace") (v "0.2.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1whjdhw4l6fhcggjmwnrf3paxcs3hw7553sb9afdczkn0qkq2w2n")))

