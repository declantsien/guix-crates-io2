(define-module (crates-io no m- nom-tracable-macros) #:use-module (crates-io))

(define-public crate-nom-tracable-macros-0.1.1 (c (n "nom-tracable-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0hl3ahd5cdbqx6df111728bl26qkayhf9rzbsa152f6kla65d4l8")))

(define-public crate-nom-tracable-macros-0.2.0 (c (n "nom-tracable-macros") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "157bbihc5p624j1dfwpipqg11vni0ik1g9gn05r5rph2hiajvdnh")))

(define-public crate-nom-tracable-macros-0.2.1 (c (n "nom-tracable-macros") (v "0.2.1") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1d7qv5fgvd5ahi9aym3n2dknyycjkmvvs4pprgdnqrvjb0axi97d")))

(define-public crate-nom-tracable-macros-0.3.0 (c (n "nom-tracable-macros") (v "0.3.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0v2b0zmjsxwag0sjarkl5hyann2s5rxk7jv3kqcyhrh1ma3j2g8p")))

(define-public crate-nom-tracable-macros-0.3.1 (c (n "nom-tracable-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1ckc0ia54616rbf0wkh7s273c7wkk3v37fvv12k6p4i9p3xg2y9b")))

(define-public crate-nom-tracable-macros-0.4.0 (c (n "nom-tracable-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ra77nsr1plr1mpakwkcfzgfjk79k7zx6gbb82ynizm9fw4gf9gx")))

(define-public crate-nom-tracable-macros-0.4.1 (c (n "nom-tracable-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1yyzr1qpjzd6787l1f3kv8drlgbn5nqnb7jcdv9ky1r8ybwya5lc")))

(define-public crate-nom-tracable-macros-0.5.0 (c (n "nom-tracable-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1b2j9fjdm81406iaf8b3az08xw26xqgy8s1bjr1dlkzk500bkhh6")))

(define-public crate-nom-tracable-macros-0.5.1 (c (n "nom-tracable-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "157hfqmxm0di6w7lv5xywj4lcl34frp8sd309dd7xd8riqrcci5v")))

(define-public crate-nom-tracable-macros-0.5.2 (c (n "nom-tracable-macros") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "15rigi52kfzhgrr6axb6pdp4l46zdxz34bq4i7d62k3dyh7n7bb5")))

(define-public crate-nom-tracable-macros-0.6.0 (c (n "nom-tracable-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "11wgw8f1l81zr2n2xxd56kxym1bldrl53fzlba35ghnfa1f7c9ln")))

(define-public crate-nom-tracable-macros-0.6.1 (c (n "nom-tracable-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0bcmm14lsr5wnl8p60900a5fm3dx212a4wmgi0vx01qm7bpp9jcb")))

(define-public crate-nom-tracable-macros-0.6.2 (c (n "nom-tracable-macros") (v "0.6.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1yqkbha6637dw2g9jsdlbz16sm4xc8mpgy1ihcwlzp473habmd58")))

(define-public crate-nom-tracable-macros-0.7.0 (c (n "nom-tracable-macros") (v "0.7.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1rkrndigj2afhinf9pmg17b3b29nwyy9z4l10dxj23dhadazq5l4")))

(define-public crate-nom-tracable-macros-0.8.0 (c (n "nom-tracable-macros") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "04v91wzd89yihl78ns1h7kpv1lzj2macwigbw20yxq6d5vmffsls")))

(define-public crate-nom-tracable-macros-0.9.0 (c (n "nom-tracable-macros") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13f40ibm3pzs7xfgjli4610f6chnbnkf7sxx0fq6im63l0g6ib57")))

(define-public crate-nom-tracable-macros-0.9.1 (c (n "nom-tracable-macros") (v "0.9.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0jnzbfg6nyghi5g5irkihjjy36pirbv87q1w7ccswk952r9qzin9")))

