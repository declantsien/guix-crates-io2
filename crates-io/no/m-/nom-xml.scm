(define-module (crates-io no m- nom-xml) #:use-module (crates-io))

(define-public crate-nom-xml-0.1.0-pre-alpha (c (n "nom-xml") (v "0.1.0-pre-alpha") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "03hi0j4crzmkqg55dqzmrfng0i9wpxr2rflh4j6z2vlb3fx9xsym") (y #t)))

(define-public crate-nom-xml-0.1.1-pre-alpha (c (n "nom-xml") (v "0.1.1-pre-alpha") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0zcyf2qfq6gnqbjvrsa265gm5whc8hcpzpiaqv670lq1rx4m1jxk") (y #t)))

(define-public crate-nom-xml-0.1.2-pre-alpha (c (n "nom-xml") (v "0.1.2-pre-alpha") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "02i1790jvybmjqibdgcn7jrglabhp65s02v42ww12pamhcqkygv3")))

