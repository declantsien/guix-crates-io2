(define-module (crates-io no m- nom-config-in) #:use-module (crates-io))

(define-public crate-nom-config-in-0.1.0 (c (n "nom-config-in") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1a30p2bcg7jd6swgv5z678infqz12b07n9c64biqj83ihgivfna2") (r "1.56")))

