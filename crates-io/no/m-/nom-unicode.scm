(define-module (crates-io no m- nom-unicode) #:use-module (crates-io))

(define-public crate-nom-unicode-0.1.0 (c (n "nom-unicode") (v "0.1.0") (d (list (d (n "nom") (r "^5.0") (k 0)))) (h "1mq3dy5ssn9cnpp1cscc35mbmd864s1196c4c7jj3p2a5d26wiwh") (f (quote (("std" "nom/std") ("regexp_macros" "nom/regexp_macros") ("regexp" "nom/regexp") ("lexical" "nom/lexical") ("default" "std" "lexical") ("alloc" "nom/alloc"))))))

(define-public crate-nom-unicode-0.2.0 (c (n "nom-unicode") (v "0.2.0") (d (list (d (n "nom") (r "^6.0") (k 0)))) (h "0hcwkwg0b98xkarq97rxwphk5an8cp8wyjvb0m3slvi9fr9lzdl8") (f (quote (("std" "nom/std") ("regexp" "nom/regexp") ("lexical" "nom/lexical") ("default" "std" "lexical") ("alloc" "nom/alloc"))))))

(define-public crate-nom-unicode-0.3.0 (c (n "nom-unicode") (v "0.3.0") (d (list (d (n "nom") (r "^7.0") (k 0)))) (h "1zhzfxhc42hpyszdyimijf080fpikng34rf1ab3l15xbf56kmh55") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

