(define-module (crates-io no m- nom-lua) #:use-module (crates-io))

(define-public crate-nom-lua-0.0.2 (c (n "nom-lua") (v "0.0.2") (d (list (d (n "dot") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "hexf-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^2.2") (d #t) (k 0)))) (h "151mkhnn46i7cyidvcn4qm3ac6ji42xk8ckkw0ihgn1vp61q7rrm") (f (quote (("graphviz" "dot"))))))

