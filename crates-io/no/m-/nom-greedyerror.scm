(define-module (crates-io no m- nom-greedyerror) #:use-module (crates-io))

(define-public crate-nom-greedyerror-0.1.0 (c (n "nom-greedyerror") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "nom_locate") (r "^1") (d #t) (k 0)))) (h "00ab7xabf8yskwdf92yazkdns397ymps76bnpsw5h1cnl292a036")))

(define-public crate-nom-greedyerror-0.1.1 (c (n "nom-greedyerror") (v "0.1.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "nom_locate") (r "^1") (d #t) (k 0)))) (h "02vd9f40qq1492nb1bsd7z88cclmb9gipdg0675n03xyz7p8ww52")))

(define-public crate-nom-greedyerror-0.1.2 (c (n "nom-greedyerror") (v "0.1.2") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "nom_locate") (r "^1") (d #t) (k 0)))) (h "05rmkmql6mw4imcaqkccqzbjna89xk6ibq3vlbpa83xswpbjb9vc")))

(define-public crate-nom-greedyerror-0.2.0 (c (n "nom-greedyerror") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "nom_locate") (r "^2") (d #t) (k 0)) (d (n "nom_locate1") (r "^1") (d #t) (k 0) (p "nom_locate")))) (h "105i09v64402nkhs40vl3h1jbzqakblp371gwfg93spklzyqhl80")))

(define-public crate-nom-greedyerror-0.3.0 (c (n "nom-greedyerror") (v "0.3.0") (d (list (d (n "nom5") (r ">=5.0.0, <6.0.0") (d #t) (k 0) (p "nom")) (d (n "nom6") (r ">=6.0.0, <7.0.0") (d #t) (k 0) (p "nom")) (d (n "nom_locate1") (r ">=1.0.0, <2.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate2") (r ">=2.0.0, <3.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate3") (r ">=3.0.0, <4.0.0") (d #t) (k 0) (p "nom_locate")))) (h "0ddbq2jkmhbsk89wc2jad6fs6gdy6qj809v3i3vssyf8zjdips2g")))

(define-public crate-nom-greedyerror-0.3.1 (c (n "nom-greedyerror") (v "0.3.1") (d (list (d (n "nom5") (r ">=5.0.0, <6.0.0") (d #t) (k 0) (p "nom")) (d (n "nom6") (r ">=6.0.0, <7.0.0") (d #t) (k 0) (p "nom")) (d (n "nom_locate1") (r ">=1.0.0, <2.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate2") (r ">=2.0.0, <3.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate3") (r ">=3.0.0, <4.0.0") (d #t) (k 0) (p "nom_locate")))) (h "0lkwk4m7y6s239g54lp4y5sm91zk61p3n2i0wcsl4p5nq0j50ghk")))

(define-public crate-nom-greedyerror-0.4.0 (c (n "nom-greedyerror") (v "0.4.0") (d (list (d (n "nom5") (r "^5") (d #t) (k 0) (p "nom")) (d (n "nom7") (r "^7") (d #t) (k 0) (p "nom")) (d (n "nom_locate1") (r "^1") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate2") (r "^2") (d #t) (k 0) (p "nom_locate")) (d (n "nom_locate4") (r "^4") (d #t) (k 0) (p "nom_locate")))) (h "1isav60bfkbfc2vcj2pi6mfd8i5h6d8mqs4kl0rq2jxc8v5ivl7w")))

(define-public crate-nom-greedyerror-0.5.0 (c (n "nom-greedyerror") (v "0.5.0") (d (list (d (n "nom7") (r "^7") (d #t) (k 0) (p "nom")) (d (n "nom_locate4") (r "^4") (d #t) (k 0) (p "nom_locate")))) (h "05mmfk5iq8iqfa9j6n71z7q6aicfbh6zyx29dv6j0nshgl05kwvk")))

