(define-module (crates-io no m- nom-test-helpers) #:use-module (crates-io))

(define-public crate-nom-test-helpers-1.0.0 (c (n "nom-test-helpers") (v "1.0.0") (d (list (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "1p5nl38a0g995ci7k0b2sjxvzywbd94hm0s3cqcrq2jygjnjsizf")))

(define-public crate-nom-test-helpers-1.0.1 (c (n "nom-test-helpers") (v "1.0.1") (d (list (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "06ib5jnsk2gr1bm6ayxih8a33h4rgngi3p0ifdbz70ik7a1k9b9y")))

(define-public crate-nom-test-helpers-2.0.0 (c (n "nom-test-helpers") (v "2.0.0") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1q1djrgy93cv2vvh7c96cpzivy7qpz7g62z2b5s56fpqj086s5hg") (f (quote (("verbose-errors"))))))

(define-public crate-nom-test-helpers-3.0.0 (c (n "nom-test-helpers") (v "3.0.0") (d (list (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "02b3ws6rzf5x5wl65vi1sbndnkyi1w5wki2g7paa2h7p5ma5idb1") (f (quote (("verbose-errors"))))))

(define-public crate-nom-test-helpers-6.0.0 (c (n "nom-test-helpers") (v "6.0.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0brwnxixkqbg2i8dimaz4w7wvy46prn4m4367h904ipwkcp0wz5f")))

(define-public crate-nom-test-helpers-6.1.0 (c (n "nom-test-helpers") (v "6.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0shapi8iddaiiydy102wgxa6nyssdrswx8v033rqv8myndgvk571")))

(define-public crate-nom-test-helpers-6.1.1 (c (n "nom-test-helpers") (v "6.1.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0x9f06ri3hvy3y6rjblhpisi2klbmxqgw52mrn18zpchfac2znxm")))

(define-public crate-nom-test-helpers-6.1.2 (c (n "nom-test-helpers") (v "6.1.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1mawln26iy3ndwpj22c3giqgc3q1093532fgmzf56089xp09nqpx")))

(define-public crate-nom-test-helpers-6.1.3 (c (n "nom-test-helpers") (v "6.1.3") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1iahpb88fcwmzwbsykh314isvzhv3nx2mwjv515gx7v7kvszqali")))

