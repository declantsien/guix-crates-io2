(define-module (crates-io no m- nom-gtf) #:use-module (crates-io))

(define-public crate-nom-gtf-0.1.1 (c (n "nom-gtf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "02617abl7nsjqbqs2ap2xsylj0ric29s4j3d18lv1cg82c1z75ca") (y #t)))

(define-public crate-nom-gtf-0.1.2 (c (n "nom-gtf") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1g3wwagf2lnc1b8hxjzgjc3shad0dcwl3wf02kxx4n8iwnzsv5c5") (y #t)))

(define-public crate-nom-gtf-0.1.3 (c (n "nom-gtf") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0knykw39apb4m7asdl5mqr79528kqvan1x7d600yai445kkqmb2p") (y #t)))

