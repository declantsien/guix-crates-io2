(define-module (crates-io no m- nom-gcode) #:use-module (crates-io))

(define-public crate-nom-gcode-0.1.0 (c (n "nom-gcode") (v "0.1.0") (d (list (d (n "insta") (r "^0.16.1") (d #t) (k 2)) (d (n "nom") (r "^6.0.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0dgxhxvmxd7g5b07v07flpda891ry8rbgcjwpfqkwrj8n5p91yjx")))

(define-public crate-nom-gcode-0.1.1 (c (n "nom-gcode") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0dmx779ij3rfglzdn80hgggx0ffbihb44ahl0jffhvdyp2sax68m")))

