(define-module (crates-io no m- nom-span) #:use-module (crates-io))

(define-public crate-nom-span-0.1.0 (c (n "nom-span") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1ngmvf1f2w7q6nv6p0z480b8gp46ssawpnlvk20rm2124pnd3vbc")))

(define-public crate-nom-span-0.1.1 (c (n "nom-span") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1a6s6lbc4v8sd9vgzqs970shclv2cmnbpg2i48vdzmw36dmshnq7")))

