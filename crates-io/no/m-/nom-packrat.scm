(define-module (crates-io no m- nom-packrat) #:use-module (crates-io))

(define-public crate-nom-packrat-0.1.0 (c (n "nom-packrat") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1x58bx33bl54m3mxc0b3npd68b431ad7dz87ri7vamlbq7w41xzn")))

(define-public crate-nom-packrat-0.1.1 (c (n "nom-packrat") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1pl9qn91fw4cplqsz4bicxqi16n9x7hwfs1nv4qfb1azgpm1lp2f")))

(define-public crate-nom-packrat-0.1.7 (c (n "nom-packrat") (v "0.1.7") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "12dmb8n85bd1zn2q5rpzjj82cpkja0js5gkbk8hac107is4lns58")))

(define-public crate-nom-packrat-0.1.8 (c (n "nom-packrat") (v "0.1.8") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1290420niprviap336ajjfkxv3p6ap0frgc4lnvv5d3dd5gwsfa6")))

(define-public crate-nom-packrat-0.1.9 (c (n "nom-packrat") (v "0.1.9") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1zhy0fqngkn8rsvyskj2cw3whgfr28dvzjrm9hz1qha1aczsgci8")))

(define-public crate-nom-packrat-0.1.10 (c (n "nom-packrat") (v "0.1.10") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0vki69r1mmj0yakpcj8q8dkmkzs0wskbwn3g70y994zadhc1xavc")))

(define-public crate-nom-packrat-0.1.12 (c (n "nom-packrat") (v "0.1.12") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "nom-packrat-macros") (r "^0.1.13") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 0)))) (h "10iyf5qqafrdspp82f36xrrb56zwqlv6h8yibxiak0c9f5751ssh")))

(define-public crate-nom-packrat-0.1.16 (c (n "nom-packrat") (v "0.1.16") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.16") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0wk7dhair4av7pv91laymdl08jzpz892gaa29ylc2dcvv5yipl2z")))

(define-public crate-nom-packrat-0.1.17 (c (n "nom-packrat") (v "0.1.17") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.16") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1lp4mc5rg6zyn8minll9vlxb9jkfxyfz03f911lab5mq9s3kadw9")))

(define-public crate-nom-packrat-0.1.18 (c (n "nom-packrat") (v "0.1.18") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.1.16") (d #t) (k 0)) (d (n "nom_locate") (r "^0.4.0") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1j74k31q1x52z3sglq3y7z7yj9crw4rmzjbh75pjba9vw68qbdj7")))

(define-public crate-nom-packrat-0.2.0 (c (n "nom-packrat") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^1.0.0") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0kn4fxwh2dq0ijflsgbrck11m2azgz9h7zrh5wgibsjfc2c4cbcj")))

(define-public crate-nom-packrat-0.3.0 (c (n "nom-packrat") (v "0.3.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "nom_locate") (r "^1.0.0") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0iaq04aqgh3jwr8bcznzm5bmahb4sk2yg10k0sw2ascpqc001giy")))

(define-public crate-nom-packrat-0.4.0 (c (n "nom-packrat") (v "0.4.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "nom_locate") (r "^1.0.0") (d #t) (k 0)) (d (n "nom_locate2") (r "^2.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0q42xgmfbzvq7mx89avm4gp1pixgf16i99gx8vb7kglqbr965v63")))

(define-public crate-nom-packrat-0.5.0 (c (n "nom-packrat") (v "0.5.0") (d (list (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 2)) (d (n "nom-packrat-macros") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "nom_locate1") (r ">=1.0.0, <2.0.0") (d #t) (k 0) (p "nom_locate")) (d (n "stats_alloc") (r ">=0.1.0, <0.2.0") (d #t) (k 2)))) (h "0ys7rw3zqqqjyxdd404gnxmhqw0xpgl99zdxaffkqg78xaksbif5")))

(define-public crate-nom-packrat-0.6.0 (c (n "nom-packrat") (v "0.6.0") (d (list (d (n "nom") (r "^7") (d #t) (k 2)) (d (n "nom-packrat-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "nom_locate1") (r "^1") (d #t) (k 0) (p "nom_locate")) (d (n "stats_alloc") (r "^0.1") (d #t) (k 2)))) (h "01jpl5plf98s7iip7wc6bbnkjxgpkffdy9pnnb652829ba864x0s")))

(define-public crate-nom-packrat-0.7.0 (c (n "nom-packrat") (v "0.7.0") (d (list (d (n "nom-packrat-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1") (d #t) (k 2)))) (h "1h820rb0qzsii5n6bcpqw2rgadzzlavhw39ksrnqla8xi6h169pc")))

