(define-module (crates-io no m- nom-operator) #:use-module (crates-io))

(define-public crate-nom-operator-0.0.1 (c (n "nom-operator") (v "0.0.1") (d (list (d (n "nom") (r "^2.2") (d #t) (k 0)))) (h "1ibs4llvn50zq9cjqs6kjaccrjggg53aa84zj93ss9n43gvwwzqp")))

(define-public crate-nom-operator-0.0.2 (c (n "nom-operator") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "0d923zbvx3i0h2if55vm08fd6gp3fqg9cb7b7xcg7ai8h9ncaqv3") (f (quote (("default"))))))

