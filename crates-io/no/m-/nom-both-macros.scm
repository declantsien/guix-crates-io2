(define-module (crates-io no m- nom-both-macros) #:use-module (crates-io))

(define-public crate-nom-both-macros-0.1.1 (c (n "nom-both-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "19klq9s097ggv4rxb7ypc9lz5ic8h8c2f3iqsavcdx19rncgyj1x")))

