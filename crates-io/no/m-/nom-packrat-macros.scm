(define-module (crates-io no m- nom-packrat-macros) #:use-module (crates-io))

(define-public crate-nom-packrat-macros-0.1.0 (c (n "nom-packrat-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1q57904g204nsfsbfsfjfypr2i93jphwnkvn8c4pab9bnbpjhx2j")))

(define-public crate-nom-packrat-macros-0.1.1 (c (n "nom-packrat-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1p1yryx44rk7q6vzxaxnl34c6vhb80m4k6c01ffcjgfrr8lsd77h")))

(define-public crate-nom-packrat-macros-0.1.2 (c (n "nom-packrat-macros") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0wmzig1a3dkjbq77b73z0vgcw2bwxgv6bqjbn6nsdcikbvd9sy3f")))

(define-public crate-nom-packrat-macros-0.1.3 (c (n "nom-packrat-macros") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "18z7qig27cnxxgndxkykylfr0kvvsyii1iy0izi2xxkc4zhy17g5")))

(define-public crate-nom-packrat-macros-0.1.4 (c (n "nom-packrat-macros") (v "0.1.4") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "05jxxisqp838rz8c68vlw20wbd1vsp297swxg2810aa76zif4na7")))

(define-public crate-nom-packrat-macros-0.1.5 (c (n "nom-packrat-macros") (v "0.1.5") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0c66swi6psjvl5l9mjjjszqj8cr1a41cdqax4zjx3ba1psyy74n7")))

(define-public crate-nom-packrat-macros-0.1.6 (c (n "nom-packrat-macros") (v "0.1.6") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1zf6ybx36kzzhnkrl4q5gijl4afaa3czmjr26klj28375jy03cdl")))

(define-public crate-nom-packrat-macros-0.1.7 (c (n "nom-packrat-macros") (v "0.1.7") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0wz8nkms5834mq6hg1yqg62jvi6ziqzi2cl2jvgkrs5zqf0bikj2")))

(define-public crate-nom-packrat-macros-0.1.9 (c (n "nom-packrat-macros") (v "0.1.9") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1318z5mpl0bn5lc09sfcv2kgm1wdbafn9gy9mq6rqpmpms5gi5vp")))

(define-public crate-nom-packrat-macros-0.1.11 (c (n "nom-packrat-macros") (v "0.1.11") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1926fc5sj4snk2zm31av8sjgl4h5r18r8i4bxgnmhv0mzin6shd6")))

(define-public crate-nom-packrat-macros-0.1.12 (c (n "nom-packrat-macros") (v "0.1.12") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "014k47v076i5wzrli5xpcpdm8wr4fci135wk2ihxngb7wzdcnmlv")))

(define-public crate-nom-packrat-macros-0.1.13 (c (n "nom-packrat-macros") (v "0.1.13") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1i8fza6xx0a2rrxl2glwa92wz4kx1nyqprz8qygiayzqxclsqxhh")))

(define-public crate-nom-packrat-macros-0.1.15 (c (n "nom-packrat-macros") (v "0.1.15") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "098qkip1ilmsdnl88jy5pbsl0j50jg8rv4ff0gjdlfrzr5gwz0hl")))

(define-public crate-nom-packrat-macros-0.1.16 (c (n "nom-packrat-macros") (v "0.1.16") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1ipxfi00y947mgnlqm3ls1ishzk8zh4hn574nd7908qmf3988km5")))

(define-public crate-nom-packrat-macros-0.1.17 (c (n "nom-packrat-macros") (v "0.1.17") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0w0yq78zsv47ycnw93m923nhmn4dvgqhmz7slc0zfgvmvimi497y")))

(define-public crate-nom-packrat-macros-0.1.18 (c (n "nom-packrat-macros") (v "0.1.18") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "07m7rbkcjf1a04a5l87msp9n2g6wd8wl5870ggnarrlh56mbib3c")))

(define-public crate-nom-packrat-macros-0.2.0 (c (n "nom-packrat-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13y63152drrf3pmalkal678fazyvmjdwl4dyn4r41rdsg8qjghj1")))

(define-public crate-nom-packrat-macros-0.3.0 (c (n "nom-packrat-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1jf6qv65ig94z6jf9i3al1yjgspyz16swjqim639f0fmqdw5jf4g")))

(define-public crate-nom-packrat-macros-0.4.0 (c (n "nom-packrat-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1hj0h5wazrsbrvyc8xf6ppvs8a8amw6yk1flinrim1hrpm55v12a")))

(define-public crate-nom-packrat-macros-0.5.0 (c (n "nom-packrat-macros") (v "0.5.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "029x7ja3wy8kxq695y9ddbkav5r7vr976yyd324s050xfysdzk3z")))

(define-public crate-nom-packrat-macros-0.6.0 (c (n "nom-packrat-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "18x7hgykvwjp1gm0588y9sf8n9rsfa55vz8fhnpw0ixg8dhhgm2g")))

(define-public crate-nom-packrat-macros-0.7.0 (c (n "nom-packrat-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "14m32m6jzrchxgac172dkqmxf57gd24i1bawcwhafsfcgy0v93bk")))

