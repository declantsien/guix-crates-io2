(define-module (crates-io no m- nom-recursive-macros) #:use-module (crates-io))

(define-public crate-nom-recursive-macros-0.1.1 (c (n "nom-recursive-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "095vqwmwpd329d9d8zi3lzaz1smfl7sqrfx51fy2kxnrrh58yic8")))

(define-public crate-nom-recursive-macros-0.2.0 (c (n "nom-recursive-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1gix4hm7xsqgcdd0vlcy397121qsggm8rv5w531zzd0zn9msqs9d")))

(define-public crate-nom-recursive-macros-0.3.0 (c (n "nom-recursive-macros") (v "0.3.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13f7xbaaqgdnc9sgm2aj77ksyzpj0l1j1qgrk1qsyfs2nz34yx07")))

(define-public crate-nom-recursive-macros-0.4.0 (c (n "nom-recursive-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0s80j51bxdnqjnbvqlcwbcv28cf8fh8lxy1dpw4nv6mqfv6a7fck")))

(define-public crate-nom-recursive-macros-0.5.0 (c (n "nom-recursive-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "120qhpcvh0bblam1qpxg5qwhrlkzf3plq9llkjq1y866zp91qqn8")))

(define-public crate-nom-recursive-macros-0.5.1 (c (n "nom-recursive-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1kpk9qd9lp65sz5zgknmcqk97nn2iyg220ycg479cnmhikbq8sg1")))

