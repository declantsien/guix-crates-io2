(define-module (crates-io no m- nom-async) #:use-module (crates-io))

(define-public crate-nom-async-0.0.0 (c (n "nom-async") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0ppjahlhi42a91k3vy5xw07gh314jxrfbjd4h805a4vdkpj619z7")))

(define-public crate-nom-async-0.1.0 (c (n "nom-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 2)))) (h "18q80f2vhklk7wq333qllnv7129r1hbl43pmxh8pfva4vwmmlzj6")))

