(define-module (crates-io no m- nom-rfc8288) #:use-module (crates-io))

(define-public crate-nom-rfc8288-0.1.0 (c (n "nom-rfc8288") (v "0.1.0") (h "1wvr5a1h34c0fppkimn8vyxjhcahqj7lrjygnp29saak5prwkh6a")))

(define-public crate-nom-rfc8288-0.2.0 (c (n "nom-rfc8288") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1gakxmmmhi8cxkrracsa8mzy286g506bqvqss425ajivvahw5dmx")))

(define-public crate-nom-rfc8288-0.2.1 (c (n "nom-rfc8288") (v "0.2.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0d0r123xlll56hzxaz83wbvcnzsb89j6cb0qw170vyxijhyyk001")))

(define-public crate-nom-rfc8288-0.2.2 (c (n "nom-rfc8288") (v "0.2.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1cs4pwc9aa13q59rw86w9snpsd1igxa6wcb4xk22yi4ln9ah5lbb")))

(define-public crate-nom-rfc8288-0.2.3 (c (n "nom-rfc8288") (v "0.2.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1gsd5xbfrl4kbrgkb424niwk2s5gr3gwi0ihvbcw8zsnyqb42l9x")))

