(define-module (crates-io no m- nom-grapheme-clusters) #:use-module (crates-io))

(define-public crate-nom-grapheme-clusters-0.1.0 (c (n "nom-grapheme-clusters") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1mb414zqd08g5bkslnb2ibgwx904ckyb0s2q5gmawbaffyij0vhr")))

(define-public crate-nom-grapheme-clusters-0.2.0 (c (n "nom-grapheme-clusters") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "145jc1nrl0xbn3cbnh3hyslcmrzdfbqwn4hp2bgviilg9ym426mr")))

(define-public crate-nom-grapheme-clusters-0.3.0 (c (n "nom-grapheme-clusters") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0n5igbm3mi3r8209amplbgxlbqm6hwmgwcmfqk82wqh552pwqv6v")))

(define-public crate-nom-grapheme-clusters-0.3.1 (c (n "nom-grapheme-clusters") (v "0.3.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0jdw3q98qji1hdib632f6xkq6908hr1ra595qpwq1sazf3vbbnyp")))

(define-public crate-nom-grapheme-clusters-0.4.0 (c (n "nom-grapheme-clusters") (v "0.4.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1sxhh9av2ha3s39bdmp9zmbbb2spki4dj7ylgbpnmcr3diqgdnp8")))

(define-public crate-nom-grapheme-clusters-0.4.1 (c (n "nom-grapheme-clusters") (v "0.4.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0qcbniarj8waca2qxj1fpmwyp78nm4hiz804jj60pd3j8w4xl4lh")))

(define-public crate-nom-grapheme-clusters-0.5.0 (c (n "nom-grapheme-clusters") (v "0.5.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0xffrdgrz0mh528wx3nrl0ls4qgq0czwyhgmxbszrh42nabgm56x")))

(define-public crate-nom-grapheme-clusters-0.5.1 (c (n "nom-grapheme-clusters") (v "0.5.1") (d (list (d (n "nom") (r "^7.1") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0g29vf8fh5wv5wcvvn0qcl37j1lnmdi4i3llsnnj44ajds91pwcg") (f (quote (("parse" "nom") ("default" "parse"))))))

(define-public crate-nom-grapheme-clusters-0.5.2 (c (n "nom-grapheme-clusters") (v "0.5.2") (d (list (d (n "nom") (r "^7.1") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "1fv1clggyjgb0xs8j68h223gfmcnjhzsch66lf1k33zx9q60qjnm") (f (quote (("parse" "nom") ("default" "parse"))))))

