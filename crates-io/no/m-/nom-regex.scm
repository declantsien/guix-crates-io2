(define-module (crates-io no m- nom-regex) #:use-module (crates-io))

(define-public crate-nom-regex-0.1.0 (c (n "nom-regex") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0-alpha1") (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0j1d1q6jjazfqkyf1hfl4bjrvwxgzrjr4g3gqlbxcwrskd05vkwa") (f (quote (("default" "alloc") ("alloc" "nom/alloc"))))))

(define-public crate-nom-regex-0.2.0 (c (n "nom-regex") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "060wr7yxvm0377rckph8ksn1m1jy8yi55v842sv704sc3irwgrbj") (f (quote (("default" "alloc") ("alloc" "nom/alloc"))))))

