(define-module (crates-io no m- nom-leb128) #:use-module (crates-io))

(define-public crate-nom-leb128-0.1.0 (c (n "nom-leb128") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1hn5h8pbbqwrgdqb51llijx2sl9s4i5d1fp0xf4qlwj6ax3ijl5z")))

(define-public crate-nom-leb128-0.2.0 (c (n "nom-leb128") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "arrayvec") (r "^0.7.0") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "0qlf08qj915kwsrqp4wd8wpqv0zga6hdppjhml9gzkwy79n3p9sj") (f (quote (("std" "nom/std"))))))

