(define-module (crates-io no m- nom-sql) #:use-module (crates-io))

(define-public crate-nom-sql-0.0.1 (c (n "nom-sql") (v "0.0.1") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0d1r568k1v30rhfxds31nj0j9r400h1s4isr5z3wb2a9yvwvq5dq")))

(define-public crate-nom-sql-0.0.2 (c (n "nom-sql") (v "0.0.2") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "03rw368q2z7fgikpxyr32qh3di3w8mmbgmdzdvagvc4lk4xlr7l7")))

(define-public crate-nom-sql-0.0.3 (c (n "nom-sql") (v "0.0.3") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bwgy1yvzk4v2hqw3hs5ywy91mcdkmc4487f2bcgwcpg2da8lzpv")))

(define-public crate-nom-sql-0.0.4 (c (n "nom-sql") (v "0.0.4") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1mbdz5w0hh082xcrrzighy0clyzmpsb3yvv1vbgn0bk2mvzpk389")))

(define-public crate-nom-sql-0.0.5 (c (n "nom-sql") (v "0.0.5") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ankk9lyks7ib5bgg0lv802cwrk6j45a64cl249z3k20fz7anr5q")))

(define-public crate-nom-sql-0.0.6 (c (n "nom-sql") (v "0.0.6") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0c68wwa69afxchklf7k2ih8yg1wdp6h5rrgz7ssq8h5lrf10nq92")))

(define-public crate-nom-sql-0.0.7 (c (n "nom-sql") (v "0.0.7") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "057d7gl2jc0qww4j8qz197gasapy0sj7d6qc6c0blwrd8p7ghzkf")))

(define-public crate-nom-sql-0.0.8 (c (n "nom-sql") (v "0.0.8") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0r15vsm5f75vghflxpg3bw86szb0ai3vwavrgahm45rhjs7ncx35")))

(define-public crate-nom-sql-0.0.9 (c (n "nom-sql") (v "0.0.9") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zafzkwp4fivp6dk5xs124dybp501dx35ssb4yvkcfkg61xa0syg")))

(define-public crate-nom-sql-0.0.10 (c (n "nom-sql") (v "0.0.10") (d (list (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0arm49ss36zk7cjavkj11smrfy06z8spq8v916gmxg5kbivixcyd")))

(define-public crate-nom-sql-0.0.11 (c (n "nom-sql") (v "0.0.11") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0c4p0wl99imbmsxgj2mij5vrwyjyfabdd1f3zlidm1b22v7nn9n5")))

