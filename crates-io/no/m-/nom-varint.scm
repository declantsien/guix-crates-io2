(define-module (crates-io no m- nom-varint) #:use-module (crates-io))

(define-public crate-nom-varint-0.0.2 (c (n "nom-varint") (v "0.0.2") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "07wzgl8jmygb9960q92wl22wp1xs5k2jihfiq78j4yni61y9rh1w") (y #t)))

(define-public crate-nom-varint-0.1.0 (c (n "nom-varint") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "01g5r2b2c4w92zhk5kjc73zp5rcaaiqdy1vixxrs3z98p1sm8dk8")))

(define-public crate-nom-varint-0.2.0 (c (n "nom-varint") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0ll4jj41r9xcb92lbr0v6qb9qwmvvlpjfjcaylid6i1y91llza2x")))

(define-public crate-nom-varint-0.2.1 (c (n "nom-varint") (v "0.2.1") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "10l901b8wp2mqzkr6xkjf5prfcgysa119n7sl9ghbnw5fnq97i26")))

(define-public crate-nom-varint-0.3.0 (c (n "nom-varint") (v "0.3.0") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "0m6zh47n9wgnygfh671ycrrqh6s429wkvj2syzcl6d90dhjv4gj4")))

(define-public crate-nom-varint-0.3.1 (c (n "nom-varint") (v "0.3.1") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "11kwvsh4rx75sgjck065sqdr3kbjd009vd6bbszi4rchgzmyigl0")))

(define-public crate-nom-varint-0.4.0 (c (n "nom-varint") (v "0.4.0") (d (list (d (n "nom") (r "^6.1.2") (k 0)))) (h "18c0xzvdlj3hqvdf7silig00dimiv467dksmgrlhm9il6jd0rqf7")))

(define-public crate-nom-varint-0.5.0 (c (n "nom-varint") (v "0.5.0") (d (list (d (n "nom") (r "^7.1") (k 0)))) (h "1qj0kyhlllcqs8jb4bsxi2v04s3rmn58cwlhipi452lals5bax7p")))

