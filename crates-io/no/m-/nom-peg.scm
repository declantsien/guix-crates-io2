(define-module (crates-io no m- nom-peg) #:use-module (crates-io))

(define-public crate-nom-peg-0.1.0 (c (n "nom-peg") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16afd0mlk9lp17lpb7p2c9mwj1vfg0g7kba05ychl4kw18h72wni")))

(define-public crate-nom-peg-0.1.1 (c (n "nom-peg") (v "0.1.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18z63fssnd6krdmn32714hgvzvjplkmwndaixs94z34yhsifygss")))

