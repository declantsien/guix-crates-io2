(define-module (crates-io no m- nom-reader) #:use-module (crates-io))

(define-public crate-nom-reader-0.1.0 (c (n "nom-reader") (v "0.1.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0mzajrqnxj92d1zr3nak5xcdgpy523zqfjd5lbgbvm63r34k0ivc")))

(define-public crate-nom-reader-0.2.0 (c (n "nom-reader") (v "0.2.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1ili6lab8l82awi86yhbkh2kdzvwkzpwsf04wh2fkaj1qckw8rl1")))

