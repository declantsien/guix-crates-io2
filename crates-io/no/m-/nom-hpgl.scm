(define-module (crates-io no m- nom-hpgl) #:use-module (crates-io))

(define-public crate-nom-hpgl-0.1.0 (c (n "nom-hpgl") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "12kp2x6m7y4qxcil88cwvgnjnbh8p3nd65r0dy9jazmc0c1m7zrm")))

(define-public crate-nom-hpgl-0.1.1 (c (n "nom-hpgl") (v "0.1.1") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1zh165xk6bzrb83c34xhl6d33asmb78q3xcpqz4irfcnrpavaqkc")))

(define-public crate-nom-hpgl-0.1.2 (c (n "nom-hpgl") (v "0.1.2") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1z3bk6vl567armzw5d4pqvdc0chh68djrnwa9cq4w9m6s282zajy")))

(define-public crate-nom-hpgl-0.1.3 (c (n "nom-hpgl") (v "0.1.3") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "svg") (r "^0.5.6") (d #t) (k 2)))) (h "0nnwrkvwj4cp9l9nbyckims44pp1p4qvlfk4shxp8g26s5r4zdck")))

(define-public crate-nom-hpgl-0.1.4 (c (n "nom-hpgl") (v "0.1.4") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "svg") (r "^0.5.6") (d #t) (k 2)))) (h "10ibx3lk2y94lvv804ka1104hhhina773pqi068fyicc7cwi7h8b")))

(define-public crate-nom-hpgl-0.2.0 (c (n "nom-hpgl") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "svg") (r "^0.5.6") (d #t) (k 2)))) (h "1z5b1dl93spvrz0f7x90hm4k5mk59cv3d83xgdyxh74cdyp4iqb8")))

