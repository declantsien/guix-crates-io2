(define-module (crates-io no fr noframe) #:use-module (crates-io))

(define-public crate-noframe-0.0.1 (c (n "noframe") (v "0.0.1") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "13brkph0gmpgnygd8wwqj6gqii24k4ri5j92a8shif1am50fhhlb")))

(define-public crate-noframe-0.0.2 (c (n "noframe") (v "0.0.2") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "1pvqchgg1f5x8l9yphd1j1znj2ywcnwffllnqlcmv6x4wl491vfj")))

(define-public crate-noframe-0.0.3 (c (n "noframe") (v "0.0.3") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "1km73fdb1nmb5rk28dss8yyikbbgi5fxgw0myvfyx79fwdj1z6ih")))

(define-public crate-noframe-0.0.4 (c (n "noframe") (v "0.0.4") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "07dfj5fq6xvs4in1nir02vvdnhj35lx47gbj0600fl60jmndwiwa")))

(define-public crate-noframe-0.0.5 (c (n "noframe") (v "0.0.5") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "12rhgy36limv0hz8rqlbgwjllc2kmkn8a9dqzmyf2v7aaipzks7b")))

(define-public crate-noframe-0.0.6 (c (n "noframe") (v "0.0.6") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "06m2c32ra1hswp6l9k98r5ww2mkx54m6jf7r25f59h6s6q7wvc8m")))

(define-public crate-noframe-0.0.7 (c (n "noframe") (v "0.0.7") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "1y3kfhrpxsdkpqicwldildhwf9yfnmxh0h77vf4pzhpbfx8kg9h0")))

(define-public crate-noframe-0.0.8 (c (n "noframe") (v "0.0.8") (d (list (d (n "ggez") (r "^0.4.4") (d #t) (k 0)))) (h "0wx0lxqga0kcjn98l0wgvh2bw3f3vhzap495xpy340383y0wnq43")))

