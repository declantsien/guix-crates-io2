(define-module (crates-io no bj nobject-rs) #:use-module (crates-io))

(define-public crate-nobject-rs-1.0.0 (c (n "nobject-rs") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "084f1mfi43ahk48pl35ph6wysdasnw2bca3a46psl79mb7an7315")))

(define-public crate-nobject-rs-1.0.1 (c (n "nobject-rs") (v "1.0.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07zixksmnpi0rpqf2qy0bx9n3384lzbnh63x93d56bgn596pra4j")))

(define-public crate-nobject-rs-1.1.0 (c (n "nobject-rs") (v "1.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "022176s5qh05mr4z5yfiwpjlyg31cdaw016ky527q92j60z77h4b")))

(define-public crate-nobject-rs-1.1.1 (c (n "nobject-rs") (v "1.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "123q7ac66gmlsgrg44x3faqzc6n9xmdyw2ic157956bm2di0ny82")))

(define-public crate-nobject-rs-1.1.2 (c (n "nobject-rs") (v "1.1.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vsz5i6n8fk2z1lhg0q2qa7l6kg0a7fw5b5iq3fabkv6sj2mkir8")))

(define-public crate-nobject-rs-1.2.0 (c (n "nobject-rs") (v "1.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j2wq5niaviqn0xgiw4hfbwbdwyy1x2ywajjr9ddbrs3ihaannfb")))

(define-public crate-nobject-rs-1.3.0 (c (n "nobject-rs") (v "1.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cy6zlmipml5lm8dnxwy0nx3f17dnzs8zh773vp4bkmvfk42przw")))

(define-public crate-nobject-rs-1.3.1 (c (n "nobject-rs") (v "1.3.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18bbyqp6hysijzwb001bwp8g8ffvwxwl48mjdyf2gjjbsgrypvq3")))

(define-public crate-nobject-rs-1.3.2 (c (n "nobject-rs") (v "1.3.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10242wyv097pk8hc883j2wgig0h4qgz2lr0sw3769hz8ynq0vm4w")))

(define-public crate-nobject-rs-2.0.0 (c (n "nobject-rs") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09lw6fmd1j5w9x3ig23jqdhjhn0m4z9d9scnvlydn0psfghjccq1")))

