(define-module (crates-io no zz nozzle-bindings) #:use-module (crates-io))

(define-public crate-nozzle-bindings-1.0.0+1.90.632-b035 (c (n "nozzle-bindings") (v "1.0.0+1.90.632-b035") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2.0") (d #t) (k 0)))) (h "03s08li63slqpn7qv11bnjq8xd313lnakvlzhpsz5y9xdda4k9nw")))

