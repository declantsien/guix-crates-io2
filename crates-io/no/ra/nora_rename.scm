(define-module (crates-io no ra nora_rename) #:use-module (crates-io))

(define-public crate-nora_rename-0.1.3 (c (n "nora_rename") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1s16gaiq91axg3kywkgj17yx4lxhwcqffnbs9hb3pvgbc8xyf26p")))

(define-public crate-nora_rename-1.0.0 (c (n "nora_rename") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1brjhind5rn4ggx2dvdd1azwwgx6pkshkdqqsaj56prmdd1c3123")))

