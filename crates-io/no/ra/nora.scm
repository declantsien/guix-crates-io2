(define-module (crates-io no ra nora) #:use-module (crates-io))

(define-public crate-nora-0.1.0 (c (n "nora") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "xcb") (r "^0.9.0") (d #t) (k 0)))) (h "0x79b7jxfv7a85sl33dz6xb0i210h0kj69500hmhrdxf138xaigi")))

(define-public crate-nora-0.2.0 (c (n "nora") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (d #t) (k 0)))) (h "0b01r004d4qgjkz1ckfnjwgvm9kkqxxh78vf6518z6y560vsps32")))

