(define-module (crates-io no ra nora_endian) #:use-module (crates-io))

(define-public crate-nora_endian-0.1.0 (c (n "nora_endian") (v "0.1.0") (h "0fj2hv6n7jmbgl9x854pf818jn90wnpj9jpn5s48xyl4panrb2a2")))

(define-public crate-nora_endian-0.1.1 (c (n "nora_endian") (v "0.1.1") (h "16wwnwhkr4y7nlfp0618815lgx31089q22p122ig69dwhm2mk6yd")))

(define-public crate-nora_endian-0.1.2 (c (n "nora_endian") (v "0.1.2") (h "1h0jnn8h4lhflsb7kjn7z5zfidll5cavpwix4pzlgx3fam6vc398")))

