(define-module (crates-io no kh nokhwa-bindings-linux) #:use-module (crates-io))

(define-public crate-nokhwa-bindings-linux-0.1.0-rc.4 (c (n "nokhwa-bindings-linux") (v "0.1.0-rc.4") (d (list (d (n "nokhwa-core") (r "^0.1.0-rc.4") (d #t) (k 0)) (d (n "v4l") (r "^0.13") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "v4l2-sys-mit") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "17di1y8gk8lnpfsy456sdd4gkx6via9s70g5q2pqfg6ic859vjdp")))

(define-public crate-nokhwa-bindings-linux-0.1.0-rc.5 (c (n "nokhwa-bindings-linux") (v "0.1.0-rc.5") (d (list (d (n "nokhwa-core") (r "^0.1.0-rc.5") (d #t) (k 0)) (d (n "v4l") (r "^0.13") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "v4l2-sys-mit") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0i987k55dxw2igdqbzzl3nan2pks5b71c7pwgg0j9ibxss0pnm65")))

(define-public crate-nokhwa-bindings-linux-0.1.0-rc.6 (c (n "nokhwa-bindings-linux") (v "0.1.0-rc.6") (d (list (d (n "nokhwa-core") (r "^0.1.0-rc.6") (d #t) (k 0)) (d (n "v4l") (r "^0.13") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "v4l2-sys-mit") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1132kg884l7nrhywj3jzw595p7ngdqpq2fhhhdibfwwpd9rj6d78")))

(define-public crate-nokhwa-bindings-linux-0.1.0 (c (n "nokhwa-bindings-linux") (v "0.1.0") (d (list (d (n "nokhwa-core") (r "^0.1.0") (d #t) (k 0)) (d (n "v4l") (r "^0.13") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "v4l2-sys-mit") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1jk0qinmsbyyflnqn5gvfmpdmlkryq4k8hhicf5ic4lfysdb13a4")))

