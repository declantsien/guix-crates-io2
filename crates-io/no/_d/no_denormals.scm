(define-module (crates-io no _d no_denormals) #:use-module (crates-io))

(define-public crate-no_denormals-0.1.0 (c (n "no_denormals") (v "0.1.0") (h "15k3bk2yrjnn8fknplrmqj8a1c3hix91k2x860fkp190cqkrsrc3")))

(define-public crate-no_denormals-0.1.1 (c (n "no_denormals") (v "0.1.1") (h "14zwyc3frdmqsd0wldvli7vk9ic5947s4xly47yn4dlr1b93ma14")))

(define-public crate-no_denormals-0.1.2 (c (n "no_denormals") (v "0.1.2") (h "1y8bdw739lg2h9xn60sdpg15460zh77xbp9nfwfyafrsw3f4x3gw")))

