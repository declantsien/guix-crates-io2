(define-module (crates-io no _d no_debug) #:use-module (crates-io))

(define-public crate-no_debug-0.1.0 (c (n "no_debug") (v "0.1.0") (h "07xi7c1g3pw6pa482jlgdwc8dlrc4wzxi636mspjj67p7fqnqg04") (y #t)))

(define-public crate-no_debug-1.0.0 (c (n "no_debug") (v "1.0.0") (h "016brn86h619pb1ywxycp1lxrvihk40giwkdjsjbyyh9248axd4s") (y #t)))

(define-public crate-no_debug-1.0.1 (c (n "no_debug") (v "1.0.1") (h "024y996zyjqnfqcjgpf3nj9s229b06mnghpjks2l5dx0s70jmixm") (y #t)))

(define-public crate-no_debug-1.0.2 (c (n "no_debug") (v "1.0.2") (h "1757jismm72qjhp9fbfksp3xnw78s366kyb0dw9ybv50ikdj3d8h") (y #t)))

(define-public crate-no_debug-2.0.0 (c (n "no_debug") (v "2.0.0") (h "0hiqgk7vjzifx6vv09dbac3m6jdmikbzzyd7nnnj2hav8fz4qpfb")))

(define-public crate-no_debug-2.0.1 (c (n "no_debug") (v "2.0.1") (h "01fn6ws9y8rzvlq849qkla80jq3q5cbsa8fisvkx63arz7p40lyc")))

(define-public crate-no_debug-3.0.0 (c (n "no_debug") (v "3.0.0") (h "01lnbj5h3w1r9vzzx94r3frqsm53qp3nrz27lnxw2552ynkxmcym")))

(define-public crate-no_debug-3.1.0 (c (n "no_debug") (v "3.1.0") (h "192j780ihhaq8d10nd95hz8pvp6gw1952hyr3py4848fhl6ac8rz")))

