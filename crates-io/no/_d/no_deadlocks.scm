(define-module (crates-io no _d no_deadlocks) #:use-module (crates-io))

(define-public crate-no_deadlocks-1.0.0 (c (n "no_deadlocks") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1gpwsqyhn1d352fmw9n53m3gr0bamy2xkmcx5xlla8qgqhazipax") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.1.0 (c (n "no_deadlocks") (v "1.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^0.5") (o #t) (d #t) (k 0)))) (h "026vcyvyalksq9aahns30dkxq486b4rml1bjzpqhx5lbwkkszy6d") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.1.1 (c (n "no_deadlocks") (v "1.1.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17a2bzyxal6axi69w2fanqyrwjk4rzrldj0942g5ppl4ms7rkmxm") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.1.2 (c (n "no_deadlocks") (v "1.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1l10zfxcy538x78drkqbgddy5nd4ngxsl7lj2ancbzwsjza51vly") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.2.0 (c (n "no_deadlocks") (v "1.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jjwibxcgcxiclf523z6dbiqnbjdrv6idlx8wx11lx37la6l7ssj") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.3.0 (c (n "no_deadlocks") (v "1.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10y5lb6i8099x4djxihkf4p410ja0d5bm248p92q561k1flarswl") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.3.1 (c (n "no_deadlocks") (v "1.3.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "198lmjw0gprzjdvd3dmmypcws20qnsdjkmgyjrjij5ykjixlij0f") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1.3.2 (c (n "no_deadlocks") (v "1.3.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "vector-map") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pnfsb09h3cdgnrx06ndl5dbhl5p9xgbzbsc49gnj3fg9w7azq70") (f (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

