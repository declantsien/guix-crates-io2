(define-module (crates-io no x_ nox_lib) #:use-module (crates-io))

(define-public crate-nox_lib-0.1.0 (c (n "nox_lib") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ikaris25f1f8dxnrap574ccbc7657a3rjzd8q0cy9sgr66mlnxw")))

(define-public crate-nox_lib-0.1.1 (c (n "nox_lib") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kiknf4dgb2qkk8i742akfkqks3ygbx4zk9v4bcnq1w6bbqma85k")))

