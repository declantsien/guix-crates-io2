(define-module (crates-io no ns nonsense_package_to_test_crates_io_behaviour) #:use-module (crates-io))

(define-public crate-nonsense_package_to_test_crates_io_behaviour-0.1.0 (c (n "nonsense_package_to_test_crates_io_behaviour") (v "0.1.0") (h "1dkipb1m88zw36qcgm0anpvl4p3q7wr9d2l1kmx4l8ckc9wkxsyz") (y #t)))

(define-public crate-nonsense_package_to_test_crates_io_behaviour-0.1.1 (c (n "nonsense_package_to_test_crates_io_behaviour") (v "0.1.1") (h "08lji5l9q6vp0r2l3dyqpfkhiblqknvwpl00d956fsn7x02f9306") (y #t)))

