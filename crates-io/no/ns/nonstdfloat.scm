(define-module (crates-io no ns nonstdfloat) #:use-module (crates-io))

(define-public crate-nonstdfloat-0.1.0 (c (n "nonstdfloat") (v "0.1.0") (d (list (d (n "bitvec") (r "~1") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "~1") (d #t) (k 2)) (d (n "simple-soft-float") (r "~0.1") (o #t) (d #t) (k 0)))) (h "1jnxpp8ic8br85h5prqyxp3z3981w538vc3ndk0w825ddlncr3jw") (f (quote (("f64") ("f128" "simple-soft-float") ("extendable" "simple-soft-float") ("default")))) (y #t)))

(define-public crate-nonstdfloat-0.1.1 (c (n "nonstdfloat") (v "0.1.1") (d (list (d (n "bitvec") (r "~1") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "~1") (d #t) (k 2)) (d (n "simple-soft-float") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "strafe-testing") (r "~0.1") (d #t) (k 2)))) (h "0zdcj38bgaj0k8c243yv124xqarhx8vnfw6w4xdq87yf9l2bpmvk") (f (quote (("f64") ("f128" "simple-soft-float") ("extendable" "simple-soft-float") ("default"))))))

