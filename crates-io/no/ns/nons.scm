(define-module (crates-io no ns nons) #:use-module (crates-io))

(define-public crate-nons-0.1.0 (c (n "nons") (v "0.1.0") (d (list (d (n "bio") (r "0.14.*") (d #t) (k 0)) (d (n "rust-htslib") (r "0.12.*") (d #t) (k 0)))) (h "09h14n00a1v7ms63il1iwswfbhmjbnwyjw5x8kvg86i4ns7bvda9")))

