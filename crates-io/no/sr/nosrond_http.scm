(define-module (crates-io no sr nosrond_http) #:use-module (crates-io))

(define-public crate-nosrond_http-0.1.0 (c (n "nosrond_http") (v "0.1.0") (d (list (d (n "mockito") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("rt" "macros"))) (k 2)))) (h "1mmnc8pk1f1f30bcif4nna0mwf14ys4cjjzc89q04ahp5nbvghwn") (f (quote (("blocking" "reqwest/blocking"))))))

