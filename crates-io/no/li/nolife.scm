(define-module (crates-io no li nolife) #:use-module (crates-io))

(define-public crate-nolife-0.1.0 (c (n "nolife") (v "0.1.0") (h "09fss3y8f3jgb60hs6gnapxjyw99759rmdqj5h2yngqqg79dhzrn")))

(define-public crate-nolife-0.2.0 (c (n "nolife") (v "0.2.0") (h "0dfpla74qmf0fnd7c2a5lm5kcqj0mn81p276alxswy4yx9g7fz31")))

(define-public crate-nolife-0.3.0 (c (n "nolife") (v "0.3.0") (h "0mnxm439c7j0cb2dswglib3v87l6zxd6ypck0zq6gqm9siiwjjpx")))

(define-public crate-nolife-0.3.1 (c (n "nolife") (v "0.3.1") (h "1p2h7vz5qqwnqim9rgrn06fzzivamhnkzy4j4rx2x9g8hzqallmw")))

(define-public crate-nolife-0.3.2 (c (n "nolife") (v "0.3.2") (h "102mrnh81nml86zbx1mr4si8m7grgjxycgcvvs125j0z0542xf8y")))

(define-public crate-nolife-0.3.3 (c (n "nolife") (v "0.3.3") (h "07rr7qhiqalm5sgmk907fdig0m10qsbm7hqhjsim6ljhhrn53sj5")))

(define-public crate-nolife-0.4.0 (c (n "nolife") (v "0.4.0") (h "0wav02d661pzckbzs30zdxawd8znd100rgqs32nzq9kdg8msjv2l")))

