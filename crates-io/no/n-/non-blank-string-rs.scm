(define-module (crates-io no n- non-blank-string-rs) #:use-module (crates-io))

(define-public crate-non-blank-string-rs-1.0.1 (c (n "non-blank-string-rs") (v "1.0.1") (d (list (d (n "fake") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0hcfnccm9ihjgr55qa9mgplnmw6a0scshxpph32r4svm5ab8h4ad") (f (quote (("utils") ("default"))))))

(define-public crate-non-blank-string-rs-1.0.2 (c (n "non-blank-string-rs") (v "1.0.2") (d (list (d (n "fake") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0qanibnnpm5nh6f33sqgyzw8kzn6lin12bixbw2yqx4jxz94sfzc") (f (quote (("utils") ("default"))))))

(define-public crate-non-blank-string-rs-1.0.3 (c (n "non-blank-string-rs") (v "1.0.3") (d (list (d (n "fake") (r "^2.6.1") (o #t) (d #t) (k 0)) (d (n "fake") (r "^2.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "16rbsy27d84lhkjcz5bq4xrixnin0d6myyriq3m1y8la7327bhkf") (f (quote (("default")))) (s 2) (e (quote (("utils" "dep:fake"))))))

