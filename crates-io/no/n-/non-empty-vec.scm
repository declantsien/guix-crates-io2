(define-module (crates-io no n- non-empty-vec) #:use-module (crates-io))

(define-public crate-non-empty-vec-0.1.0 (c (n "non-empty-vec") (v "0.1.0") (h "11b0s9zxwqimrxglm80lkk1911qdqm3jzj5l6pzmdg1jwkws7w8v")))

(define-public crate-non-empty-vec-0.1.1 (c (n "non-empty-vec") (v "0.1.1") (h "1avlz1dri5c7zkswzi4szsd7s11b2svp9hydwnnyxlq1q4q2irw6")))

(define-public crate-non-empty-vec-0.1.2 (c (n "non-empty-vec") (v "0.1.2") (h "1qzzh3vzfdhwv1491m7x80pz60qkwazcvd2xx5jfcym4hna1vgzr")))

(define-public crate-non-empty-vec-0.2.0 (c (n "non-empty-vec") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10m8v63ym4glir5ly61jskk6mz6yai4xk0w2szclh03w1yscj4g4")))

(define-public crate-non-empty-vec-0.2.1 (c (n "non-empty-vec") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xnjcz7q3w3d9ib9jdmf4sixhyirh36hgwq11grb37dg6zvsbryd")))

(define-public crate-non-empty-vec-0.2.2 (c (n "non-empty-vec") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b6946xpdbdki4h7b0nm35kplz9csmxf3h8i73yqcmdvar884csx")))

(define-public crate-non-empty-vec-0.2.3 (c (n "non-empty-vec") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12zc4vmgvv9lkn4c1rmvd2s8ifsn4vmig3xcxvf3riflm2dadsyf")))

