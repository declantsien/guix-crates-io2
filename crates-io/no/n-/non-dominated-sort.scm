(define-module (crates-io no n- non-dominated-sort) #:use-module (crates-io))

(define-public crate-non-dominated-sort-0.1.0 (c (n "non-dominated-sort") (v "0.1.0") (h "0w89wj44nvazl0kfpr8bgqyhvq4nxa20l98d376mk1vj3ljg8ai3")))

(define-public crate-non-dominated-sort-0.2.0 (c (n "non-dominated-sort") (v "0.2.0") (h "0bsqn400hsfn095in3hgxzli03f49flngi3kh433rkxrfya3q1x0")))

(define-public crate-non-dominated-sort-0.3.0 (c (n "non-dominated-sort") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dominance-ord") (r "^0.1") (d #t) (k 0)))) (h "0sc18iyakpyydbv0cpmpx606a8xz332h8y9v6zmxvl9c2ffsj87h")))

(define-public crate-non-dominated-sort-0.3.1 (c (n "non-dominated-sort") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dominance-ord") (r "^0.1") (d #t) (k 0)))) (h "1cn28lz1hk6w8qkqpzbgyszi1pcl7vpg1i3d25ciwv9q0lxfjkdw")))

