(define-module (crates-io no n- non-empty-collections) #:use-module (crates-io))

(define-public crate-non-empty-collections-0.1.0 (c (n "non-empty-collections") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "1cp0d2wwqna34s9iqvbhakvs97b1w4c0fw6g721hbicqmdyzkcvj")))

(define-public crate-non-empty-collections-0.1.1 (c (n "non-empty-collections") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "10pdy9yzrdxjcxhhvd7sc45v8ihaap59znkrs8z6fqfwc4x5nl3v") (y #t)))

(define-public crate-non-empty-collections-0.1.2 (c (n "non-empty-collections") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "15j0mjpr9q5lrj7ybgjkaxfhzb0gxs48w5vk6cxhkvmlb1lz403k")))

(define-public crate-non-empty-collections-0.1.3 (c (n "non-empty-collections") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0c1ddqrf8f20z1k14lkh5844x8k9gzbnbi1f0v7l95wicd8177g6") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1.5 (c (n "non-empty-collections") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0070xy6v17vi5pb4ll4bjwqrlbihzcvsdivrj956b8da7im3dkdf") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1.6 (c (n "non-empty-collections") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1k7hich5gv32qva176s0r97wcv9q7rbbmfqp40gps4dv3n6wvhhh") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1.7 (c (n "non-empty-collections") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w4scrri6ly6vhjscxqss69i1i9yfv8ah6qnpxxf7qxjx2a3qs4m") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1.8 (c (n "non-empty-collections") (v "0.1.8") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gar1x6js3jb6d7hm3fdx67abdfs8j9k9434h6dli8fhmyy690y9") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1.9 (c (n "non-empty-collections") (v "0.1.9") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "11b4rr4xy5k2mh5wqk3cal9dg6qdnhf1bbjmvjsiznvlrhybahjw") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

