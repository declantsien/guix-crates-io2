(define-module (crates-io no n- non-empty-string) #:use-module (crates-io))

(define-public crate-non-empty-string-0.1.0 (c (n "non-empty-string") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "07cjifrasmgyq2610vr18zy29qyd46dvlkgwl5hcnils12fj4m82")))

(define-public crate-non-empty-string-0.2.0 (c (n "non-empty-string") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ws48img7lr1mb5db1wfghs7fywg1ij8y1yhhn5ghs9pw6gji5rd") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2.1 (c (n "non-empty-string") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rp1ff43023y647q1cxc04zia1dacv28zw6mj6m1fnff5iwmvckr") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2.2 (c (n "non-empty-string") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "delegate") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1gdak4awvm26li80ks9y926qizw0w10w9nnm90lpfflp8dzdacan") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2.3 (c (n "non-empty-string") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "delegate") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0yparanfxa87yxlhhnm4mvx584w21l2wwrrzyv6k5jqqnhg8yhxv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2.4 (c (n "non-empty-string") (v "0.2.4") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "delegate") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0cxvjibhv3sjdamacj56b10mcl0iralkv18ra98awig3c100zksm") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

