(define-module (crates-io no n- non-exhaustive) #:use-module (crates-io))

(define-public crate-non-exhaustive-0.1.0 (c (n "non-exhaustive") (v "0.1.0") (h "02axw2gk96gmzwlaqwwihfd6lb85bay8zar72l78pc3lpd306j3f")))

(define-public crate-non-exhaustive-0.1.1 (c (n "non-exhaustive") (v "0.1.1") (h "1zw13252rn85nzg1xkv4pfn6nzyhnc93a7r73yq6z6ckp49lxvns")))

