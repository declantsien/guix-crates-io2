(define-module (crates-io no _a no_alloc) #:use-module (crates-io))

(define-public crate-no_alloc-0.1.0 (c (n "no_alloc") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (f (quote ("x86-sync-pool"))) (d #t) (k 2)))) (h "1ph3j3b2k7j2f5lg6i139rrhx22704fr07x4aq0kkcl8gqy1dchc") (f (quote (("pool" "heapless") ("default") ("const_generics") ("coerce_unsized"))))))

