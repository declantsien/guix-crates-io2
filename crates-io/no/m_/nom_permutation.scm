(define-module (crates-io no m_ nom_permutation) #:use-module (crates-io))

(define-public crate-nom_permutation-0.1.0 (c (n "nom_permutation") (v "0.1.0") (d (list (d (n "nom") (r "^7") (k 0)))) (h "1mbfdsrz606a00shb0ii7fjlflr64qaiav0ds71fc8jaspvd3jgk") (f (quote (("permutation_opt") ("permutation") ("default" "permutation" "permutation_opt")))) (r "1.56")))

