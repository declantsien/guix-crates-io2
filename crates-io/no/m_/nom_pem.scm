(define-module (crates-io no m_ nom_pem) #:use-module (crates-io))

(define-public crate-nom_pem-1.0.0 (c (n "nom_pem") (v "1.0.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "08z5w3gi0xq1inn6w6jm801qrair28xjak29qq4l6m7h9pwgkwqn")))

(define-public crate-nom_pem-1.0.1 (c (n "nom_pem") (v "1.0.1") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0s44r61rw21fnrfk0n7rgvg78z5xxfc46ay2jawlba548qcwczdl")))

(define-public crate-nom_pem-1.0.2 (c (n "nom_pem") (v "1.0.2") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "08x45w54xigjrh9hpqj0b9iqb544c8spv83imjzx4zp4ar1vca8i")))

(define-public crate-nom_pem-1.0.3 (c (n "nom_pem") (v "1.0.3") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0ldh7dln28s67fxhdl5605n647dmqa3wzxk7af6g8snnmwarc4q1")))

(define-public crate-nom_pem-1.0.4 (c (n "nom_pem") (v "1.0.4") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "00gccc2qwm3jyg2r79l3m6qhmhr6appszpsizmknxkh34ajjhvys")))

(define-public crate-nom_pem-4.0.0 (c (n "nom_pem") (v "4.0.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0bdxy2qvibdxmwd1d0dpvbv46d7igivi9wiv5cx3dpaam0r54d26") (f (quote (("std") ("default" "std"))))))

