(define-module (crates-io no m_ nom_input_aux) #:use-module (crates-io))

(define-public crate-nom_input_aux-0.1.0 (c (n "nom_input_aux") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1hpsfls8i4p22rr8z0ncz2akay171vz2mn9aqlhqhcgvm8ixz6l6")))

(define-public crate-nom_input_aux-0.1.1 (c (n "nom_input_aux") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0js58imkki0la7mlxn0aj7033lk903512yr6d2khcibcfmiw95x7")))

