(define-module (crates-io no m_ nom_stl) #:use-module (crates-io))

(define-public crate-nom_stl-0.1.0 (c (n "nom_stl") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "17lmmwp98psp5wkp3bca396qrxi77cbmmwraajq9zdsnq8ylji5m")))

(define-public crate-nom_stl-0.2.0 (c (n "nom_stl") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0lpl96zahjqkhsl564rgm6wpq51vi6dn7fv53dycnlvin7vz947n")))

(define-public crate-nom_stl-0.2.1 (c (n "nom_stl") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1vq9m3fn7wybn0kl4g9jrqvpw0lih2rddaj6ha54mzpyawbh1b4b")))

(define-public crate-nom_stl-0.2.2 (c (n "nom_stl") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "11sbzpyii315nak0m5rs9mxal2hm2zhbydand97n93xll6x5rlny")))

