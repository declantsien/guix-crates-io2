(define-module (crates-io no m_ nom_html_parser) #:use-module (crates-io))

(define-public crate-nom_html_parser-0.1.0 (c (n "nom_html_parser") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "1kvx08rqggqygc5z5qhigwmvc4cgkzb6djli16m5vpgmdyvvj3k8")))

(define-public crate-nom_html_parser-0.1.1 (c (n "nom_html_parser") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "070qy0ndpply2dh9rbnl59fxnlbs5h5xw3bmlim0779rqvb6qnf9")))

