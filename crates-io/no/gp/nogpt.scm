(define-module (crates-io no gp nogpt) #:use-module (crates-io))

(define-public crate-nogpt-0.1.0-pre1 (c (n "nogpt") (v "0.1.0-pre1") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "block_device") (r "^0.1") (d #t) (k 0)) (d (n "crc") (r "^1.8") (k 0)) (d (n "err-derive") (r "^0.3") (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 2)))) (h "1ynwpg2pz4qz1lgvg2bwfv80m0vczwgmcybarcqqnzf8pfk4irjq") (f (quote (("std" "alloc" "err-derive/std") ("default" "bitflags") ("alloc"))))))

