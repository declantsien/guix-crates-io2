(define-module (crates-io no ex noexcept-impl) #:use-module (crates-io))

(define-public crate-noexcept-impl-0.0.1 (c (n "noexcept-impl") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3ys7lijpzk0syjkfa6xqh7v1gp2kfhm6yaddvmyy3j9z171x5f")))

(define-public crate-noexcept-impl-0.0.2 (c (n "noexcept-impl") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "151s2fdp9vfxd3ci5mafgz20ph8n0w75yc9zkxbipzmbh8abp94l")))

