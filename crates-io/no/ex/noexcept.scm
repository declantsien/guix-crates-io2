(define-module (crates-io no ex noexcept) #:use-module (crates-io))

(define-public crate-noexcept-0.0.1 (c (n "noexcept") (v "0.0.1") (d (list (d (n "noexcept-impl") (r "^0.0.1") (d #t) (k 0)))) (h "1155kp6766ljv77rs0mp8kijamjw19m19443milnd1z7yjg9kfsa")))

(define-public crate-noexcept-0.0.2 (c (n "noexcept") (v "0.0.2") (d (list (d (n "noexcept-impl") (r "^0.0.2") (d #t) (k 0)))) (h "005vxv8f4iczg47fyz5prwfi5xmmv6p9l5gmh6wy4h9jkjb6c26w")))

