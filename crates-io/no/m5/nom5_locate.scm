(define-module (crates-io no m5 nom5_locate) #:use-module (crates-io))

(define-public crate-nom5_locate-0.1.0 (c (n "nom5_locate") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.3") (d #t) (k 0)) (d (n "memchr") (r ">= 1.0.1, < 3.0.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)))) (h "0z8vnyy4711wknjhxb421wi6an5r377r69xzzf0ba658msv5pzsw") (f (quote (("std" "nom/std" "alloc") ("simd-accel" "bytecount/simd-accel") ("default" "std") ("avx-accel" "bytecount/avx-accel") ("alloc" "nom/alloc"))))))

(define-public crate-nom5_locate-0.1.1 (c (n "nom5_locate") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.3") (d #t) (k 0)) (d (n "memchr") (r ">= 1.0.1, < 3.0.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)))) (h "0g92csk8c2wb8bvvzwddlfjjy882wl3l54sb6h4xja4bgx314hrx") (f (quote (("std" "nom/std" "alloc") ("simd-accel" "bytecount/simd-accel") ("default" "std") ("avx-accel" "bytecount/avx-accel") ("alloc" "nom/alloc"))))))

