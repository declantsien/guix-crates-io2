(define-module (crates-io no it noiton) #:use-module (crates-io))

(define-public crate-noiton-0.1.0 (c (n "noiton") (v "0.1.0") (h "0nay85hgvqghq9v4cfhibmn81wswk2a90z9in85yimfiyrsg8zwc") (y #t)))

(define-public crate-noiton-0.1.1 (c (n "noiton") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fybww55ajrmwpmr2svz34731m1xv3p89bryq1fjxjl0qhl9hqvm") (y #t)))

(define-public crate-noiton-0.1.2 (c (n "noiton") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4nnfs1wvq3a186l8670hykcdy0gm5sqwk7gjx1y6z14p3wxziv") (y #t)))

(define-public crate-noiton-0.1.3 (c (n "noiton") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01zhyc9801hkvp0rgfawb0wisba12nwyrpis0n6h48im7fm70qpr")))

