(define-module (crates-io no mi nominatim) #:use-module (crates-io))

(define-public crate-nominatim-0.1.0 (c (n "nominatim") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "092nc03pgva08247csryy06wq571q3k54l3j7n6p7033zb98jflb")))

(define-public crate-nominatim-0.2.0 (c (n "nominatim") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "115b8rj633wwpzqriic016nsi4nfcw2296032lmb663gi23y6n58")))

(define-public crate-nominatim-0.2.1 (c (n "nominatim") (v "0.2.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pajrq5sxynfaiqwyxp0c96wxwhll1wl3phxh31s1ln797cpvxhx")))

(define-public crate-nominatim-0.3.0 (c (n "nominatim") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0zdzhs4w48sw1d2g41dfwzsx0zifavdqbg2n3qmvva6gbqx0k6rz")))

(define-public crate-nominatim-0.3.1 (c (n "nominatim") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0xpxwy9j4ilmmkh09b4fb5wdlsx17l26asdnivzmx9r2jrfqd23z")))

(define-public crate-nominatim-0.3.2 (c (n "nominatim") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1jicv92zi9xyv5qb0cdyjxnxqh6d31mpi4c12s73irm8mb701rd7")))

(define-public crate-nominatim-0.3.3 (c (n "nominatim") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "09b6ifc815kkap1d9is1gcvzcfch1v3ijyh4lvwv0q6mzax2l89f")))

(define-public crate-nominatim-0.3.4 (c (n "nominatim") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "19kknc9hbdh5ypz5g29lz5q8nfqjd7cyc0mvp1llwm5zgr9frk9v") (f (quote (("rustls" "reqwest/__rustls") ("default" "reqwest/default-tls"))))))

(define-public crate-nominatim-0.3.5 (c (n "nominatim") (v "0.3.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "02zjps7f5mmiajj70v4ckzp5jrixbzkx881s0f63wcq4aki8lnpi") (f (quote (("rustls" "reqwest/rustls-tls") ("default" "reqwest/default-tls"))))))

