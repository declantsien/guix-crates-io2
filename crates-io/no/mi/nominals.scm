(define-module (crates-io no mi nominals) #:use-module (crates-io))

(define-public crate-nominals-0.1.0 (c (n "nominals") (v "0.1.0") (d (list (d (n "chinese-number") (r "^0.7.7") (f (quote ("number-to-chinese"))) (o #t) (k 0)))) (h "11233ij6j67nln71k482zjbkfp3ykh8r8aqrjsim5c72az1g1bfc") (f (quote (("default" "chinese") ("alloc")))) (s 2) (e (quote (("chinese" "dep:chinese-number" "alloc")))) (r "1.65.0")))

(define-public crate-nominals-0.2.0 (c (n "nominals") (v "0.2.0") (d (list (d (n "chinese-number") (r "^0.7.7") (f (quote ("number-to-chinese"))) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 2)))) (h "1gm3mxpdp5412jywsg5nq7idgivpymy21cyk1vfyg4ajvrlr1cmx") (f (quote (("default" "alloc") ("alloc")))) (y #t) (r "1.65.0")))

(define-public crate-nominals-0.2.1 (c (n "nominals") (v "0.2.1") (d (list (d (n "chinese-number") (r "^0.7.7") (f (quote ("number-to-chinese"))) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 2)))) (h "0ks1abil5dlkywn0c3779xkbdgr5ynjg2b59zxf0f01xm5mfa6lc") (f (quote (("default" "alloc") ("alloc")))) (r "1.65.0")))

(define-public crate-nominals-0.2.2 (c (n "nominals") (v "0.2.2") (d (list (d (n "chinese-number") (r "^0.7.7") (f (quote ("number-to-chinese"))) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 2)))) (h "1f2m6mdv8wsjizn2jhn4250zgxl5yjwf119161i739bva4mgzzrm") (f (quote (("default" "alloc") ("alloc")))) (r "1.65.0")))

(define-public crate-nominals-0.3.0 (c (n "nominals") (v "0.3.0") (d (list (d (n "chinese-number") (r "^0.7.7") (f (quote ("number-to-chinese"))) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 2)))) (h "151rq9hpjf1idmxf75kxh7jp55fbr6ig9xzck5723wm7l186wjxx") (f (quote (("default" "alloc") ("alloc")))) (r "1.65.0")))

