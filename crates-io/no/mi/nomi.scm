(define-module (crates-io no mi nomi) #:use-module (crates-io))

(define-public crate-nomi-0.0.0 (c (n "nomi") (v "0.0.0") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "0cw211qvj4f0nivyz0ah2q736ym5qy11sa1kbsaji73snmpy23by")))

(define-public crate-nomi-0.0.1 (c (n "nomi") (v "0.0.1") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "1263aq090hdpbpjgw400iyqzkdlimjnadk4w90mql21bj0rihqbx")))

(define-public crate-nomi-0.0.2 (c (n "nomi") (v "0.0.2") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "03wgfflism5sqb87yf91wkvwlr3izykzqxsiqnngmsghcpkl15n7")))

