(define-module (crates-io no rt north-service) #:use-module (crates-io))

(define-public crate-north-service-0.1.0 (c (n "north-service") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async-zeroconf") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "north-common") (r "^0.0.1") (d #t) (k 0)) (d (n "north-consul") (r "^0") (o #t) (d #t) (k 0)))) (h "065rmiibw2w07b806cnaspfi1bi5w2b6647m3csxh2fbsdcvbmn0") (f (quote (("registry-local" "async-zeroconf") ("registry-consul" "north-consul") ("default"))))))

