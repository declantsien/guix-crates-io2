(define-module (crates-io no rt north-derives) #:use-module (crates-io))

(define-public crate-north-derives-0.1.1 (c (n "north-derives") (v "0.1.1") (d (list (d (n "north-common") (r "^0.0.3") (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("sse" "compression" "cookie" "embed" "opentelemetry" "tokio-metrics" "tower-compat" "websocket" "acme" "redis-session" "prometheus" "rustls"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0nwrcg9yvm1z9sbm7hp0w8nmxw5rb93zz83hgzajhq8vp71yfwpp")))

(define-public crate-north-derives-0.1.2 (c (n "north-derives") (v "0.1.2") (d (list (d (n "north-common") (r "^0.0.3") (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("sse" "compression" "cookie" "embed" "opentelemetry" "tokio-metrics" "tower-compat" "websocket" "acme" "redis-session" "prometheus" "rustls"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "14wb9jv469rghl2ngz4958dmhapcd9wxqifj3fzqlq6pvvabmhdd")))

