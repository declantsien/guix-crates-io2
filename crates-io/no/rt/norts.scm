(define-module (crates-io no rt norts) #:use-module (crates-io))

(define-public crate-norts-0.1.0 (c (n "norts") (v "0.1.0") (h "1kl2almpjig3n84h9qrba3b5zzhgd26xhknxxxaca0drz67mwji4") (y #t)))

(define-public crate-norts-1.0.0 (c (n "norts") (v "1.0.0") (h "1670kppaqh6rl39ahnbz6b0jvcvl88fgdg4xw4kljj824izvwrvi")))

(define-public crate-norts-1.1.0 (c (n "norts") (v "1.1.0") (h "1mm9xh5jnnb9kx2l3l8qxxwanfj4yhqw7gkaa08h75p7qgfqa6vp")))

