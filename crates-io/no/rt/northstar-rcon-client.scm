(define-module (crates-io no rt northstar-rcon-client) #:use-module (crates-io))

(define-public crate-northstar-rcon-client-0.1.0 (c (n "northstar-rcon-client") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "064hdr0y3w3rvyf9ff0jh0c04q9r5lyyjv87l3azs6jifxpzcgjx")))

(define-public crate-northstar-rcon-client-0.1.1 (c (n "northstar-rcon-client") (v "0.1.1") (d (list (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0k0rbc51hk08v5n4fy0pb5fvrj66i3xrw1i40j1gv8zhkkf3bwr6")))

(define-public crate-northstar-rcon-client-0.2.0 (c (n "northstar-rcon-client") (v "0.2.0") (d (list (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nvlvi01dy45gbxf1gxym4y4dpnn1vq6lc57fs0rz825qj2rb42k")))

(define-public crate-northstar-rcon-client-0.2.1 (c (n "northstar-rcon-client") (v "0.2.1") (d (list (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0b8qi1npw9phinndx764ihfndzi11hdnyfh4y997qpia2i1y2vz5")))

(define-public crate-northstar-rcon-client-0.1.2 (c (n "northstar-rcon-client") (v "0.1.2") (d (list (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1lwl9zbap708zpdam9l263dswppriss2wb6jp5l7n5cqgr7nd54w")))

