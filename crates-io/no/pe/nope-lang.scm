(define-module (crates-io no pe nope-lang) #:use-module (crates-io))

(define-public crate-nope-lang-0.1.0 (c (n "nope-lang") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)))) (h "08scrgb0ssm1vg160985agc871zs53bagl17rgmv4zinrzin5k3y")))

(define-public crate-nope-lang-0.1.1 (c (n "nope-lang") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)))) (h "09vfpg805yi6mhbgghgiaqqaj7cszcbd4b2shk4ypdpifw62kvh3")))

(define-public crate-nope-lang-0.1.2 (c (n "nope-lang") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)))) (h "0yyvjzr1dh2z72cjr6ghkaziig341vc6id83jygflc7cwvhwpgrv")))

(define-public crate-nope-lang-0.1.3 (c (n "nope-lang") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)))) (h "0a6gk6d234gpc9flpwb8fzdj3a0mbjcwsbvy5xbx81hk7rgw73qb")))

(define-public crate-nope-lang-0.1.4 (c (n "nope-lang") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)))) (h "008a93sk6ii4bcajgidhn2axxqy1cl11sc5f535ps1dn49m4nwj7")))

