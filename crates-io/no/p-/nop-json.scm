(define-module (crates-io no p- nop-json) #:use-module (crates-io))

(define-public crate-nop-json-0.0.2 (c (n "nop-json") (v "0.0.2") (d (list (d (n "nop-json-derive") (r "^0.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0jmqaj7vgj0bm74ynh6alzjxzxx326bdkl22cvrz9srxkxvrdnl3")))

(define-public crate-nop-json-0.0.3 (c (n "nop-json") (v "0.0.3") (d (list (d (n "nop-json-derive") (r "^0.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0m8s1fv9wr01yz4mpif6sf1ilqsbzzfxmgq4pqprrsj21qnkb9l4")))

(define-public crate-nop-json-0.0.4 (c (n "nop-json") (v "0.0.4") (d (list (d (n "nop-json-derive") (r "^0.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0mg05kl8mq8j3n666985hwc6sc4srl6zyfdq8dpygf9rs2ii2qns")))

(define-public crate-nop-json-1.0.0 (c (n "nop-json") (v "1.0.0") (d (list (d (n "nop-json-derive") (r "^1.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "1gpbh1ygmqaaglx3f8j9lgg911vpnsph9676nd6l3ly1l1qivpzr")))

(define-public crate-nop-json-1.0.1 (c (n "nop-json") (v "1.0.1") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0h385fss6q7z2wccf12k7wzxi8lkgd7b96dmpcpr12l6jbzc646a")))

(define-public crate-nop-json-1.0.2 (c (n "nop-json") (v "1.0.2") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0h2vc6kp552i97zwdppd934h6a9piww30zzsmkfmlf2sxq123sap")))

(define-public crate-nop-json-1.0.3 (c (n "nop-json") (v "1.0.3") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "0b086jkwvq7xpivnpr4k98l4nphvyr9n0cr0ah1dy927msd06sr0")))

(define-public crate-nop-json-1.0.4 (c (n "nop-json") (v "1.0.4") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "16775bi9waxfn7yanb063h41qlsr0s07jdh8057s5lrpsz9fsjyw")))

(define-public crate-nop-json-1.0.5 (c (n "nop-json") (v "1.0.5") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "1y6j045gsrivgwn6569db91sqihw9c4nv76n4s3k6c8jb7zc7lsz")))

(define-public crate-nop-json-1.0.6 (c (n "nop-json") (v "1.0.6") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "1xdwk2gsvvm01mnnmjri2asw3mpzxahz5983qan6lcx6w26nhgg2")))

(define-public crate-nop-json-1.0.7 (c (n "nop-json") (v "1.0.7") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "08vihiwhfjcifkh4wj9snhkyhdhm6x3bk49iyqmwwqy85vlfkzka")))

(define-public crate-nop-json-1.0.8 (c (n "nop-json") (v "1.0.8") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "03ahcb92pig34kf097a0vdjdnajm69p5pnh4922g74z2jcwjql40")))

(define-public crate-nop-json-1.0.9 (c (n "nop-json") (v "1.0.9") (d (list (d (n "nop-json-derive") (r "^1.1") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "15jw8gl2wfvil02nz9nxmyk5cmrzwaqcq6jidcq4cddnanq6m75w")))

(define-public crate-nop-json-2.0.0 (c (n "nop-json") (v "2.0.0") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "02sghykrvh4fdv21g9r2rqh23hl1f1nc2pa96p8pp2fv9plzahas")))

(define-public crate-nop-json-2.0.1 (c (n "nop-json") (v "2.0.1") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "1pwyvrvqfamz0c5rfq684kxwkxdwxwmn2v3l9dq0h3jhmxrdpwvg")))

(define-public crate-nop-json-2.0.2 (c (n "nop-json") (v "2.0.2") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "17c3swvq7ns0gshw6ypcskhm2ayb52afjn4s6r5w7pvq3vlm17wn")))

(define-public crate-nop-json-2.0.3 (c (n "nop-json") (v "2.0.3") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "1yy5846jyxncx9y8jbq4y05if02jl8izc049280fjl86wnrgx6x4")))

(define-public crate-nop-json-2.0.4 (c (n "nop-json") (v "2.0.4") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "0m668af2zll17vkxhkpk9rbdvgn3i97cl2ih6j1n5sbyf43hfix8")))

(define-public crate-nop-json-2.0.5 (c (n "nop-json") (v "2.0.5") (d (list (d (n "nop-json-derive") (r "^2.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)) (d (n "read_iter") (r "^0.1") (d #t) (k 2)))) (h "1rqqg6vg64wchfvzkzfnirz3dnvb1laa8n2aawflnbrwhs7pfd70")))

