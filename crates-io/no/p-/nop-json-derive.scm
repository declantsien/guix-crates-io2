(define-module (crates-io no p- nop-json-derive) #:use-module (crates-io))

(define-public crate-nop-json-derive-0.0.1 (c (n "nop-json-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "178xcf8y86p7z7w9np5pd9d1i1dhgwrw2z6v9ws1p6i1yyn39wis")))

(define-public crate-nop-json-derive-1.0.0 (c (n "nop-json-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a48rfg1vh4hvj1f0kam3igb3hlvq57cikqzzy5kb85mpp25awrx")))

(define-public crate-nop-json-derive-1.1.0 (c (n "nop-json-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fxcmz00jh53sra4f0nq923kyzpxz3cdawwmp066ap3d33n3a8vr")))

(define-public crate-nop-json-derive-1.1.1 (c (n "nop-json-derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19f9c9wmngwl3dmvrvl9911136gwv24wv5g4gfnhvv6a3l98r9ir")))

(define-public crate-nop-json-derive-1.1.2 (c (n "nop-json-derive") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n1xx8qx7mpanzjddv3y5h4gnpccggn8i01m0gzlsjq51n99dwjx")))

(define-public crate-nop-json-derive-1.1.3 (c (n "nop-json-derive") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bdaic2pnw3i7b54q9ykiygqhiahb7sjhyw0l6j0hxk62vinpm35")))

(define-public crate-nop-json-derive-1.1.4 (c (n "nop-json-derive") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "115czinczx5a1lzr37a474ff1zjhlp7q190zfnqy728q1l775843")))

(define-public crate-nop-json-derive-2.0.0 (c (n "nop-json-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0542rxncpdwmc6zaxj399yyq881caxbzmka1mzy718ca89dqka3n")))

(define-public crate-nop-json-derive-2.0.1 (c (n "nop-json-derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19rkc27qz7w0jik5i6kmbrs9iwp13w8n6hp7mfk44s7srb7isv7l")))

(define-public crate-nop-json-derive-2.0.2 (c (n "nop-json-derive") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b4fzrcc4qm38nwskzzc25a1p5dp6yy0xbd4aay6wjffi5jp758s")))

(define-public crate-nop-json-derive-2.0.3 (c (n "nop-json-derive") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0igi3p7fxa8qyw61haw1pl0mq1hlflnqw5h6ci3s2q4zv8q4i6az")))

(define-public crate-nop-json-derive-2.0.4 (c (n "nop-json-derive") (v "2.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v12ixz3aq89bdk6jjaiszn5wjshn6ymlwf1r5fm34c98k70q3ys")))

(define-public crate-nop-json-derive-2.0.5 (c (n "nop-json-derive") (v "2.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yz0qr27rc17zxkqs8003r6cakn2qi5ln0x6rwyz5kpgjss0i4lx")))

