(define-module (crates-io no dr nodrop) #:use-module (crates-io))

(define-public crate-nodrop-0.1.0 (c (n "nodrop") (v "0.1.0") (h "1r5pxz6489amfiyf8crym7drkhw26pysz62kg1ln27spl0l3j3l8")))

(define-public crate-nodrop-0.1.1 (c (n "nodrop") (v "0.1.1") (h "1d9s9m55yr621bxb8dkzpazy7qkrdxn7rwh248i8g17gspgmji9i")))

(define-public crate-nodrop-0.1.2 (c (n "nodrop") (v "0.1.2") (h "0a1a1h2ci6ksxswfcf6ldbiyg0gqzbnnzzizq9c25glx89yz3zqw") (f (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1.3 (c (n "nodrop") (v "0.1.3") (d (list (d (n "odds") (r "^0.1") (d #t) (k 0)))) (h "14abqcfv91xhm95fn40093ia48s7hm4b3dkbb59x6l9zsljsrpl5") (f (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1.4 (c (n "nodrop") (v "0.1.4") (d (list (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "1k4v2qc5klpiv8vf2avrllg85jqrqglyd2f1l5xks7ipnlpyrdbc") (f (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1.5 (c (n "nodrop") (v "0.1.5") (d (list (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "0xm1h4si6awlpg18xx4ais4vj9438532q11c2q5h6h1rbbqz1fg0") (f (quote (("use_needs_drop") ("no_drop_flag"))))))

(define-public crate-nodrop-0.1.6 (c (n "nodrop") (v "0.1.6") (d (list (d (n "odds") (r "^0.2.12") (k 0)))) (h "13yx41qq386vh0nvg1z1zsifgm11cm6l9gsw4yzzgvmxrvdj56jd") (f (quote (("use_needs_drop") ("std" "odds/std") ("no_drop_flag") ("default" "std"))))))

(define-public crate-nodrop-0.1.7 (c (n "nodrop") (v "0.1.7") (d (list (d (n "odds") (r "^0.2.12") (k 0)))) (h "0zbfxgy5hza1rz6yy56j0lynnyb4fmjjjkvbzkvn296i0khpz2dc") (f (quote (("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1.8 (c (n "nodrop") (v "0.1.8") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1ibwvmddp0rrrqgy9cjw0dbspwfs1i6bxd6rsc5ym3f9yk9svfqd") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1.9 (c (n "nodrop") (v "0.1.9") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1chpa6liwidsy1i14vi4asd1c1s0ny8k0vnc61j5kfmy176p9kaj") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1.10 (c (n "nodrop") (v "0.1.10") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1vk9zh754lbklcq9j0q3vdz65jqp9frj1r4npvm5pfqjq75n43y2") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1.11 (c (n "nodrop") (v "0.1.11") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "0xxz5az4gpf878aap6ddqxsn4wgs3g7qd6d4h645aipkazpl8jxa") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1.12 (c (n "nodrop") (v "0.1.12") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "18p0d9xniqbwrhf6v83lw399c978sa5yvwk2aad0c23ilpf2h8ls") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

(define-public crate-nodrop-0.1.13 (c (n "nodrop") (v "0.1.13") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0if9ifn6rvar5jirx4b3qh4sl5kjkmcifycvzhxa9j3crkfng5ig") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

(define-public crate-nodrop-0.1.14 (c (n "nodrop") (v "0.1.14") (d (list (d (n "nodrop-union") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1fz1v9r8ijacf0hlq0pdv5l9mz8vgqg1snmhvpjmi9aci1b4mvvj") (f (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

