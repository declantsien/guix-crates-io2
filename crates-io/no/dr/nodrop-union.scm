(define-module (crates-io no dr nodrop-union) #:use-module (crates-io))

(define-public crate-nodrop-union-0.1.8 (c (n "nodrop-union") (v "0.1.8") (h "1n2rh3pac8fkdawzlfn91lcx5dkjkhq07lqs4cv889ha6mb1hk8n")))

(define-public crate-nodrop-union-0.1.9 (c (n "nodrop-union") (v "0.1.9") (h "07zhhmk0yygarmzv4zxm9vrkm35c53k82q6v6y1gd9dn9rq625z3")))

(define-public crate-nodrop-union-0.1.10 (c (n "nodrop-union") (v "0.1.10") (h "0jsnkdn9l8jlmb9h4wssi76sxnyxwnyi00p6y1p2gdq7c1gdw2b7")))

(define-public crate-nodrop-union-0.1.11 (c (n "nodrop-union") (v "0.1.11") (h "1h59pph19rxanyqcaid8pg73s7wmzdx3zhjv5snlim5qx606zxkc")))

