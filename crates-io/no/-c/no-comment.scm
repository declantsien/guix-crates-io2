(define-module (crates-io no -c no-comment) #:use-module (crates-io))

(define-public crate-no-comment-0.0.1 (c (n "no-comment") (v "0.0.1") (h "1wva0524y2r7svwfp99cpwpxa6li3sg79am4dyyvlkp6yjlangsb")))

(define-public crate-no-comment-0.0.2 (c (n "no-comment") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)))) (h "1xav5vnmzr86hrc7ns2f651adi1qaxns7n8hx7n5m79cbgzpy2ca")))

(define-public crate-no-comment-0.0.3 (c (n "no-comment") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)))) (h "1kn2bb2whn7n12jq6rw9rjwrcrfqaii5gn1rv3z0abjqkrw3mwhg")))

