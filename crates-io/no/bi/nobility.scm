(define-module (crates-io no bi nobility) #:use-module (crates-io))

(define-public crate-nobility-0.1.0 (c (n "nobility") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1d3miikb9vzsabs06hf400nqxik40shag4abpjdl95vlgd3m692g")))

(define-public crate-nobility-0.2.0 (c (n "nobility") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "1507cs0n6dbydjnnl0d0fyfyz05ld8q9dzsh261yxafma4ajmilh") (f (quote (("default" "uuid"))))))

