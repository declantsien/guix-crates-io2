(define-module (crates-io no ck nock) #:use-module (crates-io))

(define-public crate-nock-0.0.0 (c (n "nock") (v "0.0.0") (h "0faycwb04jmjc81p1syhjdmx9jli8zyhcgckqzi2v514cga5mg8f")))

(define-public crate-nock-0.1.0 (c (n "nock") (v "0.1.0") (h "00s415nkwq12fvk1m6d7ganq9dwi2kvzm31c09akjgf60z608rqy")))

(define-public crate-nock-0.2.0 (c (n "nock") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1bfk7jpp41xjhl551l1cb0zcz26hlmjffkaxcqx02z811p2ri43q")))

(define-public crate-nock-0.2.1 (c (n "nock") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0p9sccagq9xlfqkawz46aqc1yy4dfkm8klxxdmmm1sqyq0nz2q1s")))

(define-public crate-nock-0.3.0 (c (n "nock") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "15zq3i8ihxgvf943ha892hgn5m3ah43x4hqq3lv218qg1j5skw4h")))

(define-public crate-nock-0.4.0 (c (n "nock") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "17w4681lq5krxip4f0j5shy13m4ckp7sj4187v4y9s72ni2infcj")))

