(define-module (crates-io no bc nobcd) #:use-module (crates-io))

(define-public crate-nobcd-0.1.0 (c (n "nobcd") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "097sb0h34d1gys3vla0j6a4170i5j994yl3rcxxnyc9y014xik40")))

(define-public crate-nobcd-0.1.1 (c (n "nobcd") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "10b0rlj533ryiiqgyny4bi03gxxnf8r1b4g8ncvy4afw97fvd9xj")))

(define-public crate-nobcd-0.2.0 (c (n "nobcd") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1wsbz4blhyxx8gnbrzrxwaqadavkmkisvh991ddjdl45smd63l9f")))

