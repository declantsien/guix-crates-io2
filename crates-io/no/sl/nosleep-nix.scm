(define-module (crates-io no sl nosleep-nix) #:use-module (crates-io))

(define-public crate-nosleep-nix-0.2.0-rc.1 (c (n "nosleep-nix") (v "0.2.0-rc.1") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1sqp1spqbf8i0kl8qm56514y3mzmhfw96w90cpfyg7iqgc90smqd") (r "1.57")))

(define-public crate-nosleep-nix-0.2.0-rc.2 (c (n "nosleep-nix") (v "0.2.0-rc.2") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1n990z33cik180qadnvjsxnv5zlixpnqmcx5wzpryqs8di2y56pa") (r "1.57")))

(define-public crate-nosleep-nix-0.2.0-rc.3 (c (n "nosleep-nix") (v "0.2.0-rc.3") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "14r6vjw6b6q2p5cp3dfjp4596gwfr06mq4slhg7mi129ka479l1m") (r "1.57")))

(define-public crate-nosleep-nix-0.2.0 (c (n "nosleep-nix") (v "0.2.0") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "nosleep-types") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "0x0ckzpwyiqka38dp762kvvcwnayanwlmclz11f36ipdmgcljd84") (r "1.57")))

(define-public crate-nosleep-nix-0.2.1 (c (n "nosleep-nix") (v "0.2.1") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "nosleep-types") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1gar7snrvppmrx8v3c14hdmwpliw2hf41wnndmsvciia9i6dkd4w") (r "1.57")))

