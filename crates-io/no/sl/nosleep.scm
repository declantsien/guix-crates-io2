(define-module (crates-io no sl nosleep) #:use-module (crates-io))

(define-public crate-nosleep-0.1.0 (c (n "nosleep") (v "0.1.0") (h "0pr24c0f818w56xgc6bz04hc62g0ns2zlxkyiamqmq5xhlcx7drr") (r "1.56")))

(define-public crate-nosleep-0.2.0-rc.1 (c (n "nosleep") (v "0.2.0-rc.1") (d (list (d (n "nosleep-mac-sys") (r "^0.2.0-rc.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nosleep-nix") (r "^0.2.0-rc.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "nosleep-windows") (r "^0.2.0-rc.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1n3pmxsi3gv9h13vqm67i336jvvpwa37spkv179hs72p5k9ra3d4") (r "1.57")))

(define-public crate-nosleep-0.2.0-rc.2 (c (n "nosleep") (v "0.2.0-rc.2") (d (list (d (n "nosleep-mac-sys") (r "^0.2.0-rc.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nosleep-nix") (r "^0.2.0-rc.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.2") (d #t) (k 0)) (d (n "nosleep-windows") (r "^0.2.0-rc.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1h3jka9k2lrzdxnyrr3dnc0xaijk8qpwmzkilvv7b5k1x4nxby2y") (f (quote (("serde" "nosleep-types/serde")))) (r "1.57")))

(define-public crate-nosleep-0.2.0-rc.3 (c (n "nosleep") (v "0.2.0-rc.3") (d (list (d (n "nosleep-mac-sys") (r "^0.2.0-rc.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nosleep-nix") (r "^0.2.0-rc.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nosleep-types") (r "^0.2.0-rc.3") (d #t) (k 0)) (d (n "nosleep-windows") (r "^0.2.0-rc.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0kjln1p3jqrcs54msd4i174va6arj8z8xsxd335lnpv9fs27m2q4") (f (quote (("serde" "nosleep-types/serde")))) (r "1.57")))

(define-public crate-nosleep-0.2.0 (c (n "nosleep") (v "0.2.0") (d (list (d (n "nosleep-mac-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nosleep-nix") (r "^0.2.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nosleep-types") (r "^0.2.0") (d #t) (k 0)) (d (n "nosleep-windows") (r "^0.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "17qv2w5jnkl2dgjss6kx22vc3bb3a7b0a6p5n57k4p74si1wgk1r") (f (quote (("serde" "nosleep-types/serde")))) (r "1.57")))

(define-public crate-nosleep-0.2.1 (c (n "nosleep") (v "0.2.1") (d (list (d (n "nosleep-mac-sys") (r "^0.2.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "nosleep-nix") (r "^0.2.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nosleep-types") (r "^0.2.1") (d #t) (k 0)) (d (n "nosleep-windows") (r "^0.2.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1fpbldsygp693pwng9fl8aj3zmhyx6csf46apc5dr301imasvv8c") (f (quote (("serde" "nosleep-types/serde")))) (r "1.57")))

