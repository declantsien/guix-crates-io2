(define-module (crates-io no sl nosleep-types) #:use-module (crates-io))

(define-public crate-nosleep-types-0.2.0-rc.1 (c (n "nosleep-types") (v "0.2.0-rc.1") (d (list (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1kr7ka3qfcshhvn84d0yfxl510gznnjnv69h6zbrcrmffx825rlz") (r "1.57")))

(define-public crate-nosleep-types-0.2.0-rc.2 (c (n "nosleep-types") (v "0.2.0-rc.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "04xm5958qabj6hv3907plb8zx393nqp70lyg1gc84lnccr5s8sr6") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.57")))

(define-public crate-nosleep-types-0.2.0-rc.3 (c (n "nosleep-types") (v "0.2.0-rc.3") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1k03fi69lphz3jxmhhxyx9574118x7zh8xfpkhanxr85z3ld741l") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.57")))

(define-public crate-nosleep-types-0.2.0 (c (n "nosleep-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "118vzg2kymbk27mkc19g2xci30gd0qbiixml3xp7svniz61xg66i") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.57")))

(define-public crate-nosleep-types-0.2.1 (c (n "nosleep-types") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "0jb4k014934gjcz9j38kcyw7nblhhanfaafacwxc7c879s84sbfx") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.57")))

