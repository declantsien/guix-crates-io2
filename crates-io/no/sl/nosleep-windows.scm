(define-module (crates-io no sl nosleep-windows) #:use-module (crates-io))

(define-public crate-nosleep-windows-0.2.0-rc.1 (c (n "nosleep-windows") (v "0.2.0-rc.1") (d (list (d (n "nosleep-types") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Power" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)))) (h "0fkzrshk1m1v6cmpypfjmhrqnydyxg4f7nijx8pvg055jbsp9qax") (r "1.57")))

(define-public crate-nosleep-windows-0.2.0-rc.2 (c (n "nosleep-windows") (v "0.2.0-rc.2") (d (list (d (n "nosleep-types") (r "^0.2.0-rc.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Power" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)))) (h "167dvnh98zmlfzvvaig1jynryzq6qdbjx5h9sckbdx1c5mrbjg49") (r "1.57")))

(define-public crate-nosleep-windows-0.2.0-rc.3 (c (n "nosleep-windows") (v "0.2.0-rc.3") (d (list (d (n "nosleep-types") (r "^0.2.0-rc.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Power" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)))) (h "0hpjgiaa4x4jij1ijwz1x99s2r0ahm50jm7vkaac5qagkbh35qar") (r "1.57")))

(define-public crate-nosleep-windows-0.2.0 (c (n "nosleep-windows") (v "0.2.0") (d (list (d (n "nosleep-types") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Power" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)))) (h "12wbnrg49pc5bm69sicfngmixryvmlfwzrjpwx4ihlwj46nld67x") (r "1.57")))

(define-public crate-nosleep-windows-0.2.1 (c (n "nosleep-windows") (v "0.2.1") (d (list (d (n "nosleep-types") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Power" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)))) (h "07ld9kd9a4x38c67r8c2zvc9mn6pcd5g3imfpw4g92skmycvsw15") (r "1.57")))

