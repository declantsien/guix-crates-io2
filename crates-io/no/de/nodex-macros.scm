(define-module (crates-io no de nodex-macros) #:use-module (crates-io))

(define-public crate-nodex-macros-0.1.0 (c (n "nodex-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nrqh7xwnn2g227k6zs6adjczv9b6ki8gv2i6d26c7ji40i4qxw4")))

(define-public crate-nodex-macros-0.1.2 (c (n "nodex-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kvhi41hnacalkbv6absrllaspw67v43i6vlkw8794nxi1qmphgn")))

