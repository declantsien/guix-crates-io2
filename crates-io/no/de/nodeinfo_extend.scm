(define-module (crates-io no de nodeinfo_extend) #:use-module (crates-io))

(define-public crate-nodeinfo_extend-0.0.1 (c (n "nodeinfo_extend") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0vw59fqmdqnmkbfh7yg2j6vkg7wr1b5zqdch9jk2s5iy9kxr075x") (f (quote (("mastodon") ("default" "mastodon"))))))

