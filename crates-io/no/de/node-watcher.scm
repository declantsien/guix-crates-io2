(define-module (crates-io no de node-watcher) #:use-module (crates-io))

(define-public crate-node-watcher-1.0.0 (c (n "node-watcher") (v "1.0.0") (h "0jciick33xvkv7kl3jxfkhgkvbzny1yd3x55jnvlng490kkd51sa")))

(define-public crate-node-watcher-1.0.1 (c (n "node-watcher") (v "1.0.1") (h "1d2fwm1lhxqniq2xd61nmc36pj52dkkd8v013ab7npjm3zrwvr7z")))

(define-public crate-node-watcher-1.0.2 (c (n "node-watcher") (v "1.0.2") (h "1vzavv0wj0c0slxyyzrj9qjmm3si98lnabqx2bzcg1a9hs3bhbn7")))

(define-public crate-node-watcher-2.0.0 (c (n "node-watcher") (v "2.0.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "testcontainers") (r "^0.12.0") (d #t) (k 2)))) (h "1bsb2a33gfjmhxp09rh17ajxdmgcda0j3q9729xfpph006pir7j2")))

