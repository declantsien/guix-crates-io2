(define-module (crates-io no de node_editor) #:use-module (crates-io))

(define-public crate-node_editor-0.0.1 (c (n "node_editor") (v "0.0.1") (d (list (d (n "aflak_cake") (r "^0.0.1") (d #t) (k 0)) (d (n "aflak_imgui") (r "^0.18.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08nd5nfd1c85l0qjnvckcbvyl148vp3b0w1c62xr39gyqwjsnh7k")))

(define-public crate-node_editor-0.0.3 (c (n "node_editor") (v "0.0.3") (d (list (d (n "aflak_cake") (r "^0.0.3") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jxz99imnshqzifdsmf2c53fxrrklcb3s5j1ywm97ysb7w0yr0xb")))

