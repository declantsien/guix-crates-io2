(define-module (crates-io no de node-api-rs) #:use-module (crates-io))

(define-public crate-node-api-rs-0.1.0-alpha.0 (c (n "node-api-rs") (v "0.1.0-alpha.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "0d4n2w9ml13kbm5cc13nxdkhkpwbywrhb1hwzsz7lla38n1dif73") (y #t)))

(define-public crate-node-api-rs-0.1.0-alpha.1 (c (n "node-api-rs") (v "0.1.0-alpha.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "0lp6zfb7jiqbzakgaayy4hx5s3lygzyzqn4h4q6b88r4564kkmc3") (y #t)))

