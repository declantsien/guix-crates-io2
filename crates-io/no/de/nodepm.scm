(define-module (crates-io no de nodepm) #:use-module (crates-io))

(define-public crate-nodepm-0.1.0 (c (n "nodepm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "02q6wgpywxamnj5pj11zzr2cra45p886kw6z5wpfiqmc1ims6l29")))

(define-public crate-nodepm-0.2.0 (c (n "nodepm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0b1nfnxvg0fjg9njlyc9hknjd4p57kdqaryq3mjbvpxvnj2vbpcr")))

