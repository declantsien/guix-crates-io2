(define-module (crates-io no de node_tree) #:use-module (crates-io))

(define-public crate-node_tree-0.1.0 (c (n "node_tree") (v "0.1.0") (d (list (d (n "node_tree_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0xr9pjw33k68cll5a2wmi1id3rznljymx9xx5g0fkbm9y43rnml7") (r "1.78")))

(define-public crate-node_tree-0.1.1 (c (n "node_tree") (v "0.1.1") (d (list (d (n "node_tree_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ay5kh1l8gfsw3g025wsvp4m6airzanqwila0fm4y9jxxscxrd9l") (r "1.78")))

(define-public crate-node_tree-0.1.2 (c (n "node_tree") (v "0.1.2") (d (list (d (n "node_tree_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0kjsspzp8av41m9ij1bznnipl5siwyhw387bbrj0csih59f67458") (r "1.78")))

(define-public crate-node_tree-0.2.0 (c (n "node_tree") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "node_tree_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1xk7x0gmmlbf1q5v7z09ndwzlqcwn3gllgkmqnqads1xqjjgxcxs") (r "1.78")))

(define-public crate-node_tree-0.2.1 (c (n "node_tree") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "node_tree_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0g7fflxyskw5nbykvsxv7j9x19dlqxmccskd3ix5y36k1cjqzv3r") (r "1.78")))

