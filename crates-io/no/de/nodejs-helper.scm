(define-module (crates-io no de nodejs-helper) #:use-module (crates-io))

(define-public crate-nodejs-helper-0.0.1 (c (n "nodejs-helper") (v "0.0.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1j497220dy77309s89d3rln1pksyh5x8hanp08yzcnxxpd5na67n")))

(define-public crate-nodejs-helper-0.0.2 (c (n "nodejs-helper") (v "0.0.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0gs3w5m6wmjmg0i4rpf8ld2fq4d1gjizqqc5ij2yab1j770isvzd")))

(define-public crate-nodejs-helper-0.0.3 (c (n "nodejs-helper") (v "0.0.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0myb8f3gl20sxz3mhby6ml7pb6whh6mbssk9fdkmxflvp2klg96j")))

