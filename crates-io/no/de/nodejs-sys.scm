(define-module (crates-io no de nodejs-sys) #:use-module (crates-io))

(define-public crate-nodejs-sys-0.1.0 (c (n "nodejs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1blgbyx3nvg06cncaa3nxpp9y1hr68nyzxp8f05ihdz3pbk4hilf") (f (quote (("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.1.1 (c (n "nodejs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "0jzdxg96dpmg17zkrmfvlpq4dbcshk6w5ghavzw92bdr12g2bvzs") (f (quote (("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.2.0 (c (n "nodejs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "0pswyvlc946skham1cgl86zzf4hvxljcmpgp0pzgi9gl7xmg7yv4") (f (quote (("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.3.0 (c (n "nodejs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "1c6n44qai40a6wmdxg9lsgy9n7gr6sma8zhsd5krfai4imzpz3yj") (f (quote (("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.4.0 (c (n "nodejs-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0qv10mrf7ynfw917v5822lhpbpcixi8qlziwfmi688fgbn8qi8i9") (f (quote (("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.4.1 (c (n "nodejs-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0s5jfz4z9ahziwj7i05vy0mn5z7ngbpspkayyhlkhrb5pgzmvvv2") (f (quote (("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.5.0 (c (n "nodejs-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1xcxjv5hliqk0128wqfv7xv0kdf240pfqk7bi9ysmah3ghr38pzj") (f (quote (("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.6.0 (c (n "nodejs-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1bm1j26rrabiyv326dzp0gjmhd226wxkc3v1j0iy3hslnh5ha3yh") (f (quote (("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.7.0 (c (n "nodejs-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0nw5xfwv86323mx3h3mmll0xsa2w94kd4fvf05axsm68l12wwyv0") (f (quote (("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.8.0 (c (n "nodejs-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0jfcsrz7myh3423jag4gai6syyr6bb0wfimxi4xfn0kycx6bb6dl") (f (quote (("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.9.0 (c (n "nodejs-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "19k74k7svcsp7d2nrzh75fmrq9a56wly06x8cc9485qf83da7mpa") (f (quote (("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.10.0 (c (n "nodejs-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0r59zqf3yy0v88bh26nn0723ls51784l4kr2js8l9y5kl581j2in") (f (quote (("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.11.0 (c (n "nodejs-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "08nlwfv4dig0adwz8yj1z79ffa79jlvk2nlbbdvsjl49idkayy03") (f (quote (("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.12.0 (c (n "nodejs-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "009kh860gkbdf3m11cy7fd425b1mqp9wxmczsjq4p5bgvmprnzy8") (f (quote (("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.13.0 (c (n "nodejs-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "0vfyspw2d27xw3wyj8kh18lzyywprw7jkzc4pyyvg57pa197b0wv") (f (quote (("napi_v8") ("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.14.0 (c (n "nodejs-sys") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0y4xbwhl8r15dbb8ss2dcvjp70vif395y72wr1pggc27i5q7nxxl") (f (quote (("napi_v8") ("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

(define-public crate-nodejs-sys-0.15.0 (c (n "nodejs-sys") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "00wrvrh1y6dz7nniw1x6758kfixs4rm1siv5mf95xpbjggfwqlnj") (f (quote (("napi_v8") ("napi_v7") ("napi_v6") ("napi_v5") ("experimental") ("default"))))))

