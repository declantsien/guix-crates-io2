(define-module (crates-io no de nodegraph) #:use-module (crates-io))

(define-public crate-nodegraph-0.1.0 (c (n "nodegraph") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0phg8a0yr0c9lqdk72ds52s8smfkwnwyz398lqzv0lbygisvbzpp")))

