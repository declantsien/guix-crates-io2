(define-module (crates-io no de node2object) #:use-module (crates-io))

(define-public crate-node2object-0.1.0 (c (n "node2object") (v "0.1.0") (d (list (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "treexml") (r "^0.3") (d #t) (k 0)))) (h "0nszqrf9nyk968ags6kj0ay2rh9gkhqpz4n1i3ykz0rx7jx736zp")))

(define-public crate-node2object-0.1.1 (c (n "node2object") (v "0.1.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "treexml") (r "^0.3") (d #t) (k 0)))) (h "0s084q56ns0blksl741vqzfqci5k4g63hdaswwwdw2ir4jca4pvd")))

(define-public crate-node2object-0.1.2 (c (n "node2object") (v "0.1.2") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "treexml") (r "^0.6") (d #t) (k 0)))) (h "0a71q8472v40gmdrid7rw4x3wp82v1l4dr9hzwzfnj8hbabmpqrv")))

