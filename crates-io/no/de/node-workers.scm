(define-module (crates-io no de node-workers) #:use-module (crates-io))

(define-public crate-node-workers-0.5.0 (c (n "node-workers") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1iqaswm1yrnzx38iaj2r1xvnbkm677v6znnnjnqm3pxif87gfmzd")))

(define-public crate-node-workers-0.5.1 (c (n "node-workers") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1dhzl7gv3nv5nq7hg6s615a7a7qa9bh4hp742fm86hcdj0kl489p")))

(define-public crate-node-workers-0.5.2 (c (n "node-workers") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0sslj8r6qxlj2nz4hlw5lhsg86dx08axrqlyp09rkx94la0aqfxk")))

(define-public crate-node-workers-0.6.0 (c (n "node-workers") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0s02hrr6nzzcyqnr5v5sh812wj69085cnc1xiyrasmna630nvd7x")))

(define-public crate-node-workers-0.7.0 (c (n "node-workers") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0i9wbb98x85vqbaqc132i2jbkc5alidx9nvmrjm2ngl99ywg9msj")))

(define-public crate-node-workers-0.8.0 (c (n "node-workers") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "benchman") (r "^0.2.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "087xjvly4pgwqg6jv8r6bi2f2i6brpc35qysl24hjlbqj1pc9jfc")))

