(define-module (crates-io no de nodejs_path) #:use-module (crates-io))

(define-public crate-nodejs_path-0.0.1 (c (n "nodejs_path") (v "0.0.1") (h "0dz2qbkavlzhrhlx42f0jcc389wpvhm68ipgjrqa2lddywn19zvg")))

(define-public crate-nodejs_path-0.0.2 (c (n "nodejs_path") (v "0.0.2") (h "04dmw4mbla01xz6jcs2jhk4fg4izdgsi6xrpdgdglq8bys7zws59")))

(define-public crate-nodejs_path-0.0.3 (c (n "nodejs_path") (v "0.0.3") (h "0fp05pwhm470zrzzda373k7m4wbkjmf6qydqj5k2gn05dq40046h")))

(define-public crate-nodejs_path-0.0.4 (c (n "nodejs_path") (v "0.0.4") (h "042hp1nma3i9r5gr2aqxg9jjhb1n3wpfpgsjim8bv1zqssfy310f")))

(define-public crate-nodejs_path-0.0.5 (c (n "nodejs_path") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1444ypyr3m9l8grfsx7x8f1b8kbyjidgylvqih2ww0m50l9x5g59")))

(define-public crate-nodejs_path-0.0.6 (c (n "nodejs_path") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0v3f9hcjymm8g23x2z8p7aa0szxdmv8jv1sk6l1yfqpw19wlajnk")))

(define-public crate-nodejs_path-0.0.7 (c (n "nodejs_path") (v "0.0.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0n7b3dj1z13avc58kadf1gpimnjpk91s1dn5ap563xjzcy1baymq")))

(define-public crate-nodejs_path-0.0.8 (c (n "nodejs_path") (v "0.0.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1n099ps0q1llp171jlq74s9mjd41png37d47v43vn0rbr8jcxddq")))

(define-public crate-nodejs_path-0.0.9 (c (n "nodejs_path") (v "0.0.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "19ydvr1b829ivdw21b6k6h9yp7bklqiicg7z5hhzkw2x7wx0mdlx") (y #t)))

