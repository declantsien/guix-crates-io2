(define-module (crates-io no de nodeless-rs) #:use-module (crates-io))

(define-public crate-nodeless-rs-0.1.0 (c (n "nodeless-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1q3k7vbl9718y4a7wn9n7k6kxanfhv2xim72vrwy2zrhrd7nwvpj")))

