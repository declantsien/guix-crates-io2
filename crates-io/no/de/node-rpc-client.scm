(define-module (crates-io no de node-rpc-client) #:use-module (crates-io))

(define-public crate-node-rpc-client-0.0.0 (c (n "node-rpc-client") (v "0.0.0") (h "043f00cz7k4npbnsyw9v86w910dzjpmsf9y2jr8zxdr4vp0pjds5") (y #t)))

(define-public crate-node-rpc-client-2.0.0 (c (n "node-rpc-client") (v "2.0.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "~0.12.35") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "node-primitives") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-rpc") (r "^2.0.0") (d #t) (k 0)) (d (n "tetcore-tracing") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core-client") (r "^15.1.0") (f (quote ("http"))) (k 0)))) (h "1pj8jgc9fh05ib9dxc8am076f8wfw9ab0c827q5w7n69xr0asndg")))

