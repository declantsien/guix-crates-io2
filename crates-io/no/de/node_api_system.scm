(define-module (crates-io no de node_api_system) #:use-module (crates-io))

(define-public crate-node_api_system-0.1.0 (c (n "node_api_system") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "0p3722ci7v0fdqbmhx3pspj8l6gfxmdb2n60knslb3d20ywwacmh") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.2.0 (c (n "node_api_system") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "1cr9rhsayby14hv3pvn8ybs566irryna474dm8y6c3223n1xmd83") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.3.0 (c (n "node_api_system") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "1xvznn5kr22s8qfakrbfgiiias4vg7yj0mb224k40rdjxwspcgwj") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.3.1 (c (n "node_api_system") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "03m86l44whb9p1iqpcb5c5i7qa6kgq5vlb0xgmbzvasdn0riv5bm") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.4.0 (c (n "node_api_system") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "1677rmda48jx8fawdcxiprj3jkqknvs3nb6dvacxzic9d830hiwn") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.4.1 (c (n "node_api_system") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 1)))) (h "0jy8v9z4sig7l9436mwzljqlric2hs1ynahi2mgrsi7k504n2xx7") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

(define-public crate-node_api_system-0.5.0 (c (n "node_api_system") (v "0.5.0") (h "0gcxqxq0a2y2m767hzb1z17026856axha1w71g7c65ibmjiwy7lz") (f (quote (("v7" "v1" "v2" "v3" "v4" "v5" "v6") ("v6" "v1" "v2" "v3" "v4" "v5") ("v5" "v1" "v2" "v3" "v4") ("v4" "v1" "v2" "v3") ("v3" "v1" "v2") ("v2" "v1") ("v1") ("default" "v7"))))))

