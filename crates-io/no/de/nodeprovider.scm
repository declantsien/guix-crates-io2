(define-module (crates-io no de nodeprovider) #:use-module (crates-io))

(define-public crate-nodeprovider-0.1.0 (c (n "nodeprovider") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "flowrlib") (r "= 0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Directory" "FileReader" "FileReaderSync" "console" "Headers" "Request" "RequestInit" "RequestMode" "Response" "Window"))) (d #t) (k 0)))) (h "1gxyadk07xz7vnhapp4g0b2pwkm2pv8w5kbij4n3n4r1sz45h2mh")))

