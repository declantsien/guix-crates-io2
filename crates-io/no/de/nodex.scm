(define-module (crates-io no de nodex) #:use-module (crates-io))

(define-public crate-nodex-0.1.0-alpha.0 (c (n "nodex") (v "0.1.0-alpha.0") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0x4ibsl0f5biryh27qr68wzyss29zsa6dy00x1q5ni3aq5fp9v8c")))

(define-public crate-nodex-0.1.0-alpha.1 (c (n "nodex") (v "0.1.0-alpha.1") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.8") (d #t) (k 0)))) (h "1zryn0xa0llr69lj65r9a3h8n6z6402xshdilwnqmzv5rggjxwvv")))

(define-public crate-nodex-0.1.0-alpha.9 (c (n "nodex") (v "0.1.0-alpha.9") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.9") (d #t) (k 0)))) (h "11vs1vf0qbhirbfkwrbdcyq4c14ns0lxvb1qkj0fbns1qs51qb4g") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.10 (c (n "nodex") (v "0.1.0-alpha.10") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.10") (d #t) (k 0)))) (h "0h85sa2333gwg7qbgm4d064sf19h7zw5m12nf04a1axiccly3msv") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.11 (c (n "nodex") (v "0.1.0-alpha.11") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.11") (d #t) (k 0)))) (h "1vk2ninxi9s0npa4k9jscbhccg53q6vh8gwwji0wlvjdapp42y93") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.12 (c (n "nodex") (v "0.1.0-alpha.12") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.12") (d #t) (k 0)))) (h "1c3brpifcqd6mpbrkyflv1z92x9hbx9l3r3hi2d2gjvv9smrygi9") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.13 (c (n "nodex") (v "0.1.0-alpha.13") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.13") (d #t) (k 0)))) (h "06vzifn8wxny8h2l5fh35p1609b026kswggcw26rj1pqqk6fqib0") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.14 (c (n "nodex") (v "0.1.0-alpha.14") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.14") (d #t) (k 0)))) (h "1ibhfj6j756fa7jg12bd5kwa02yw00ycmwwwlgqsxmvs6v9ndp19") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-alpha.15 (c (n "nodex") (v "0.1.0-alpha.15") (d (list (d (n "nodex-api") (r "^0.1.0-alpha.15") (d #t) (k 0)))) (h "1fhnf284yx3622j8dckrgz5lsj5g8107xjfi13h658m1vswafr4c") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-beta.1 (c (n "nodex") (v "0.1.0-beta.1") (d (list (d (n "nodex-api") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "16zfj3mn1a2628qqs7bal8hcddirgx2vfacmi9lwkvpn0dx7aih2") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-beta.2 (c (n "nodex") (v "0.1.0-beta.2") (d (list (d (n "nodex-api") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "16syky27nzikcm97vqaxbmjx4m45kblycz8kzp5snkaqp4rfw235") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-beta.3 (c (n "nodex") (v "0.1.0-beta.3") (d (list (d (n "nodex-api") (r "^0.1.0-beta.3") (d #t) (k 0)))) (h "0yvj30dx24k7f1b6x1x363pfiwhha3hkjvhggm1j5lip4xj4aqlf") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0-beta.4 (c (n "nodex") (v "0.1.0-beta.4") (d (list (d (n "nodex-api") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "1c9ik8q4ns79z91fbr8p5mg0f6f9ksn166gfr8420vr27m4p31q9") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.0 (c (n "nodex") (v "0.1.0") (d (list (d (n "nodex-api") (r "^0.1.0") (d #t) (k 0)))) (h "1jzyws9hy32vc24xfyxicia7f9hlw9zazxvz8ys080b00l3zcbcf") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.1 (c (n "nodex") (v "0.1.1") (d (list (d (n "nodex-api") (r "^0.1.1") (d #t) (k 0)))) (h "0b2p3x94s15bd9b70d2c5ad2w9k6k9bvzf806w85kfc5g121rq3g") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.2 (c (n "nodex") (v "0.1.2") (d (list (d (n "nodex-api") (r "^0.1.2") (d #t) (k 0)))) (h "0swxazs4sg7z6bhhagm53wqvl9z54c86xiyc9jh305ywk0bad82j") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("no-linkage" "nodex-api/no-linkage") ("default" "v1"))))))

(define-public crate-nodex-0.1.3 (c (n "nodex") (v "0.1.3") (d (list (d (n "nodex-api") (r "^0.1.3") (d #t) (k 0)))) (h "1jlsia7zix5ppv0zl8yhqyk3nyx0k6zmaykd7srfa925hxh6pja8") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("no-linkage" "nodex-api/no-linkage") ("default" "v1"))))))

(define-public crate-nodex-0.1.4 (c (n "nodex") (v "0.1.4") (d (list (d (n "nodex-api") (r "^0.1.4") (d #t) (k 0)))) (h "0yv83x4wk2girrk70kr7ymwx41kisfgx01g5vjilh1d5hspmm8f5") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("no-linkage" "nodex-api/no-linkage") ("default" "v1"))))))

(define-public crate-nodex-0.1.5 (c (n "nodex") (v "0.1.5") (d (list (d (n "nodex-api") (r "^0.1.5") (d #t) (k 0)))) (h "0afjp72sh18hzixcy5g03ncbzbs2j5kz86g9cx87i2l2wz1f24rp") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.1.6 (c (n "nodex") (v "0.1.6") (d (list (d (n "nodex-api") (r "^0.1.6") (d #t) (k 0)))) (h "0gahpwbbihh75hravcxbwbqs8f9qc014ipaq1civs86nrz3yqqk5") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.2.0 (c (n "nodex") (v "0.2.0") (d (list (d (n "nodex-api") (r "^0.2.0") (d #t) (k 0)))) (h "1lp4km366ramfbl04azvs1f9pmpa5hb09p5csf80x31hz32h0qsf") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1")))) (y #t)))

(define-public crate-nodex-0.2.1 (c (n "nodex") (v "0.2.1") (d (list (d (n "nodex-api") (r "^0.2.1") (d #t) (k 0)))) (h "0cd6q6y0yc9g9whg3gypzpyn8nafs0ncw2sf3i9yqb0p7bh0pj6s") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1")))) (y #t)))

(define-public crate-nodex-0.2.2 (c (n "nodex") (v "0.2.2") (d (list (d (n "nodex-api") (r "^0.2.2") (d #t) (k 0)))) (h "1872jjdcdp8m712yd373iay50092gypq6bqskbkrifmi4az5drjx") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.2.3 (c (n "nodex") (v "0.2.3") (d (list (d (n "nodex-api") (r "=0.2.3") (d #t) (k 0)))) (h "1mhmh6b78gsgl2l2f3xcz050xyjlrr9j10w6fbkmr9fjcmrxx6g0") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

(define-public crate-nodex-0.2.4 (c (n "nodex") (v "0.2.4") (d (list (d (n "nodex-api") (r "=0.2.4") (d #t) (k 0)))) (h "1g9qmbj7mrk82zcv3xj6d6q41fzyjyxc5az7c9r2nlywcjarpfwl") (f (quote (("v8" "nodex-api/v8") ("v7" "nodex-api/v7") ("v6" "nodex-api/v6") ("v5" "nodex-api/v5") ("v4" "nodex-api/v4") ("v3" "nodex-api/v3") ("v2" "nodex-api/v2") ("v1" "nodex-api/v1") ("default" "v1"))))))

