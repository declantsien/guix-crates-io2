(define-module (crates-io no de node-emoji) #:use-module (crates-io))

(define-public crate-node-emoji-1.0.7 (c (n "node-emoji") (v "1.0.7") (d (list (d (n "phf") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (f (quote ("std"))) (k 0)))) (h "1ajxkd15sm7ybabfz4gbwrx38f2lrz5jfzrp8c7ijallps8d3dni") (f (quote (("emojidb"))))))

