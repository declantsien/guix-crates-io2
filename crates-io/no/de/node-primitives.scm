(define-module (crates-io no de node-primitives) #:use-module (crates-io))

(define-public crate-node-primitives-0.0.0 (c (n "node-primitives") (v "0.0.0") (h "1xzd1j4iyf7xmn9rdhzsdgiv8clnn5cw9nlpgkzk2q1i1493i29m") (y #t)))

(define-public crate-node-primitives-2.0.0 (c (n "node-primitives") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serializer") (r "^2.0.2") (d #t) (k 2)) (d (n "tet-application-crypto") (r "^2.0.2") (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1zj1dagk8p0mpyh6j4rlvf4ly97h4q78fc8qz1al844a7c05i0hd") (f (quote (("std" "codec/std" "fabric-system/std" "tet-application-crypto/std" "tet-core/std" "tp-runtime/std") ("default" "std"))))))

