(define-module (crates-io no de node-types) #:use-module (crates-io))

(define-public crate-node-types-0.0.1 (c (n "node-types") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vmrjsh5gn0h2gb6da5khar5krcy5hp42ks6q3awb8khcabhnhd4")))

(define-public crate-node-types-0.1.0 (c (n "node-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18k542l6k87iamrf66826bid033an6py7x6hb3ikpg0ivr53vjl9")))

