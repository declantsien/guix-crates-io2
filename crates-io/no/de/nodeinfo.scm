(define-module (crates-io no de nodeinfo) #:use-module (crates-io))

(define-public crate-nodeinfo-0.0.1 (c (n "nodeinfo") (v "0.0.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "0vm44sv1llfhp6dkxrjcnxqw0y3yk7q4b8hs26bp0vbqpgcyf4jk") (y #t)))

(define-public crate-nodeinfo-0.0.2 (c (n "nodeinfo") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1n51yr4h1cjnz97nhyjb0xc9h2qxbhppsrnba4i0i1s7b36blkw1")))

