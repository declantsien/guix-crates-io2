(define-module (crates-io no de node_backend) #:use-module (crates-io))

(define-public crate-node_backend-0.1.0 (c (n "node_backend") (v "0.1.0") (h "16jvqjrvmga1sidxgm81wk38y64ldyaqm6nsy6khyk1c4yhimpq1")))

(define-public crate-node_backend-0.1.1 (c (n "node_backend") (v "0.1.1") (h "1y6myiphg3mksnznkvd826jlg03md192msykd6fgw2yrjl51pngl")))

(define-public crate-node_backend-0.1.2 (c (n "node_backend") (v "0.1.2") (h "17r51iscfapfw0s4ahk3a3b178hylhafrr1pfjhcziv9zg97p6d1")))

