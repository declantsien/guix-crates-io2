(define-module (crates-io no de nodex-plugin-helloworld) #:use-module (crates-io))

(define-public crate-nodex-plugin-helloworld-0.1.0 (c (n "nodex-plugin-helloworld") (v "0.1.0") (d (list (d (n "nodex") (r "^0.1") (d #t) (k 0)))) (h "0yh5s9a0svjk39p52v54ci75lkai370xkna1kjf07bcdlkmzffq1")))

(define-public crate-nodex-plugin-helloworld-0.2.0 (c (n "nodex-plugin-helloworld") (v "0.2.0") (d (list (d (n "nodex") (r "^0.2") (d #t) (k 0)))) (h "036sk43ygp1rhjqwvzqp56cf7d4id20azj2d7fjpjwqvscjij7gb")))

