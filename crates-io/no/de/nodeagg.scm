(define-module (crates-io no de nodeagg) #:use-module (crates-io))

(define-public crate-nodeagg-0.1.0 (c (n "nodeagg") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "combine") (r "^4.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0898w2gahyfjvz3msh8vhpf7g7d8bbj4h214x7982k4qqr4f718w") (f (quote (("cmdline" "clap"))))))

(define-public crate-nodeagg-0.2.0 (c (n "nodeagg") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.30") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "combine") (r "^4.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "01brvi8yah1jpp19kgpd8l6fy2n5yrrga0ffla3k0srwsgnr6hi3") (f (quote (("cmdline" "clap"))))))

