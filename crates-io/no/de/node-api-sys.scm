(define-module (crates-io no de node-api-sys) #:use-module (crates-io))

(define-public crate-node-api-sys-0.1.0 (c (n "node-api-sys") (v "0.1.0") (h "0wj6wc9n3jvz89c8sv7637pfhf3bk7jx3kh60r5bvfmpg1yb1lcn") (y #t)))

(define-public crate-node-api-sys-0.1.1 (c (n "node-api-sys") (v "0.1.1") (h "1s518njy89xmmi5r8369wyd7h6m5j7bcjjymdysc6w3sc5pr1hps") (y #t)))

