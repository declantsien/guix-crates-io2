(define-module (crates-io no de node_api_macro) #:use-module (crates-io))

(define-public crate-node_api_macro-0.1.0 (c (n "node_api_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsr3x579xf7z4z62zv7ma2zgbnsjmw8xjy44ncn2r176hq8gvjv")))

(define-public crate-node_api_macro-0.2.0 (c (n "node_api_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11svj6wi70vzgr8j04080flzgcl8m8g9wlbnr94nn4iffbdnhsyx")))

(define-public crate-node_api_macro-0.3.0 (c (n "node_api_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "144rdmv1xysjmbgz8lrpyq8xca7ndmm75nq9if6xi4l4irqx0d6n")))

(define-public crate-node_api_macro-0.3.1 (c (n "node_api_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "006bsrn9xj4zf5x4jx28zfmf7jzg211qb7a65kbp5p8i4w2vc8nh")))

(define-public crate-node_api_macro-0.4.1 (c (n "node_api_macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12wb9vabr7ddrxziklc28xvff6pmjssmnzvj3ygwd48d60v3f0yg")))

(define-public crate-node_api_macro-0.5.0 (c (n "node_api_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x8jg0gdxvdhflx1vhg2srj98i458b5vq1qba4lqznwdqs36wmfv")))

