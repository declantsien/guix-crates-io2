(define-module (crates-io no de node2text) #:use-module (crates-io))

(define-public crate-node2text-0.1.0 (c (n "node2text") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1ws3mg0nifk599bcm2i6jzd4p6v26f65vbv3qk4jjqp75105nmji")))

(define-public crate-node2text-0.2.0 (c (n "node2text") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1bp9rsh3y3ay6hgybpva6pqj44j0hgw1j4wpqirqx9z3gp7a4zc9")))

