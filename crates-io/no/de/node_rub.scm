(define-module (crates-io no de node_rub) #:use-module (crates-io))

(define-public crate-node_rub-0.0.1 (c (n "node_rub") (v "0.0.1") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0cw2fqqhrsp7fz3ndmy5kx4idfpqyp43aywd658qyvx1j14f2bsv")))

(define-public crate-node_rub-0.0.2 (c (n "node_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1mnzzh5x636if8hkkz10qlmlmi87wxrg3436h84888b7xqn9x4m2")))

(define-public crate-node_rub-0.0.3 (c (n "node_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "10x1vcmdqh0qjr62fyl0y3aagrkrbd8rwic7c3lp56fs1xap007p")))

