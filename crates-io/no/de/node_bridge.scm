(define-module (crates-io no de node_bridge) #:use-module (crates-io))

(define-public crate-node_bridge-0.1.0 (c (n "node_bridge") (v "0.1.0") (h "1jlwpvbjwbxn5scsziqbj5fhw447fngs62d9wvaqhjg8r4rkn4l1")))

(define-public crate-node_bridge-0.1.1 (c (n "node_bridge") (v "0.1.1") (h "1higr814ap3mbj6fsihgrg8nrji9gh24d57fh910lwpi09cynm6j")))

(define-public crate-node_bridge-0.1.2 (c (n "node_bridge") (v "0.1.2") (h "0pv97844hwc764j43s138z15c9r5ldfnp69x4i5ksmgqmhg350v7")))

(define-public crate-node_bridge-1.0.0 (c (n "node_bridge") (v "1.0.0") (d (list (d (n "async_fn_traits") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "macros"))) (d #t) (k 0)))) (h "0pdjha0qrhhxy811c6w655lrxb2nz0s5db8irywn533w36spyfhn")))

(define-public crate-node_bridge-1.0.1 (c (n "node_bridge") (v "1.0.1") (d (list (d (n "async_fn_traits") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "macros"))) (d #t) (k 0)))) (h "1nskcl9qkpbna4mli8yl8zakc2xvfj69y6a0h4dqwm2s12mbakig")))

(define-public crate-node_bridge-1.0.2 (c (n "node_bridge") (v "1.0.2") (d (list (d (n "async_fn_traits") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "macros"))) (d #t) (k 0)))) (h "0qslcvp2m1yhi5lhk5p2pyrq44ggm891sf33yyqfs1w1gh0dhxdc")))

