(define-module (crates-io no de node_crunch) #:use-module (crates-io))

(define-public crate-node_crunch-0.1.0 (c (n "node_crunch") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1spvkkww93f49rh12497r4nhg01i4skvwfi01ybg0xr36aw3l6dz")))

