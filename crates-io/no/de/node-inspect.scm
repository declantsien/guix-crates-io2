(define-module (crates-io no de node-inspect) #:use-module (crates-io))

(define-public crate-node-inspect-0.0.0 (c (n "node-inspect") (v "0.0.0") (h "1a3p1gssrzs00mxj2hdp9qr1in7lqvzs28nvpyqsb49fdcgnz3nr") (y #t)))

(define-public crate-node-inspect-0.8.0 (c (n "node-inspect") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "tc-cli") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-service") (r "^0.8.0") (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "1lf9vpzdmajj9vnxs08xxnillzp1ygvz8j8s1rjg6gnmprw1smhi")))

