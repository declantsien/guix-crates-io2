(define-module (crates-io no de node-rs) #:use-module (crates-io))

(define-public crate-node-rs-0.1.0 (c (n "node-rs") (v "0.1.0") (d (list (d (n "stdweb") (r "^0.4.8") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.4.0") (d #t) (k 0)))) (h "12iyxh6wynawywlhiir28m6qnjrx624spdch9faqzbmkmj0h3f6y")))

(define-public crate-node-rs-0.1.1 (c (n "node-rs") (v "0.1.1") (d (list (d (n "stdweb") (r "^0.4.8") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0a735svx33p8fkr4crijkk88f1vxfbmz0j91i0qa2gb4ml3kdb6q") (f (quote (("docs-rs" "stdweb/docs-rs"))))))

(define-public crate-node-rs-0.1.2 (c (n "node-rs") (v "0.1.2") (d (list (d (n "stdweb") (r "^0.4.8") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.4.0") (d #t) (k 0)))) (h "06p8zlgli5ajmmrjrl34x1vf5dfm5rphbz0fqm3h8dnbb5m6n280") (f (quote (("docs-rs" "stdweb/docs-rs"))))))

(define-public crate-node-rs-0.1.3 (c (n "node-rs") (v "0.1.3") (d (list (d (n "stdweb") (r "^0.4.8") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.4.0") (d #t) (k 0)))) (h "033j7xqnmh800ipppwp2slf8khx387ypf9v5b504904a2c3wzkf5") (f (quote (("docs-rs" "stdweb/docs-rs"))))))

(define-public crate-node-rs-0.1.4 (c (n "node-rs") (v "0.1.4") (d (list (d (n "stdweb") (r "^0.4.9") (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)))) (h "05x8xplkjfsllfrny2ii0a1fhlbyxkzpsyblh4sc3dfkpbdhvxmr") (f (quote (("docs-rs" "stdweb/docs-rs"))))))

