(define-module (crates-io no de nodekill) #:use-module (crates-io))

(define-public crate-nodekill-1.0.0 (c (n "nodekill") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)))) (h "100q68ym0zizpn6xbgzxzd8l65x81mqr4xd66xmklxd1kkgb59r3")))

