(define-module (crates-io no de noders) #:use-module (crates-io))

(define-public crate-noders-0.0.0 (c (n "noders") (v "0.0.0") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "mio") (r "^0.6.15") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)))) (h "07xb2499cg2kzflqb7h15mp1010f1fwyw1iwinqfb6plvxvip8nn")))

(define-public crate-noders-0.0.1 (c (n "noders") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "mio") (r "^0.6.15") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)))) (h "128c52bdjqfy7gqbd2zdkdxy6k61sr021kdzi4xkw15fih366g5a")))

(define-public crate-noders-0.0.2 (c (n "noders") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "mio") (r "^0.6.15") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)))) (h "1kcww7hm3ikndv4d9issz5h5cxz4ixskl11xxri5nr6k8z61zvqv")))

