(define-module (crates-io no de node_api) #:use-module (crates-io))

(define-public crate-node_api-0.1.0 (c (n "node_api") (v "0.1.0") (d (list (d (n "node_api_macro") (r "^0.1") (d #t) (k 0)) (d (n "node_api_system") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0m4wgg2kdgjwwf348kw1qy5sd4cn3f72fxyfyvdbngcfp2yna5wb") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-node_api-0.2.0 (c (n "node_api") (v "0.2.0") (d (list (d (n "node_api_macro") (r "^0.2") (d #t) (k 0)) (d (n "node_api_system") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "08aaawnbzpx7adyfzz668dad9b2izl8zl2s63ssirn60rc8ssaxx") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-node_api-0.3.0 (c (n "node_api") (v "0.3.0") (d (list (d (n "node_api_macro") (r "^0.3") (d #t) (k 0)) (d (n "node_api_system") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ic7ipkhgsy9185yhpm7kfn1qy078gf5rnprp2fp4pd6n1m0hd0y") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-node_api-0.3.1 (c (n "node_api") (v "0.3.1") (d (list (d (n "node_api_macro") (r "^0.3") (d #t) (k 0)) (d (n "node_api_system") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0m1jqk56lp2vbxkvnz1927h5nd23l7p3p912zrvlwbpwwi476jdc") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-node_api-0.4.1 (c (n "node_api") (v "0.4.1") (d (list (d (n "node_api_macro") (r "^0.4") (d #t) (k 0)) (d (n "node_api_system") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1phslxp2h2r49yzjrplbj12grzqc8nsnikawzjycb5yhpz6znj86")))

(define-public crate-node_api-0.5.0 (c (n "node_api") (v "0.5.0") (d (list (d (n "node_api_macro") (r "^0.5") (d #t) (k 0)) (d (n "node_api_system") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0arz2jg1l4zhhiybw2q588zf6kivi4vigcasivpxryda8qk10m9k")))

