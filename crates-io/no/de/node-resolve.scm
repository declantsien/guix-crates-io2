(define-module (crates-io no de node-resolve) #:use-module (crates-io))

(define-public crate-node-resolve-1.0.0 (c (n "node-resolve") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "0gbfq81d2bjzjdj8hlywa2xa935lmd2qg91ldydiq14v602bf3w3")))

(define-public crate-node-resolve-1.0.1 (c (n "node-resolve") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "0yrv239x19mhidzwwczwzaqxrv0vzqq6f6czak0v9yva8x59xm0n")))

(define-public crate-node-resolve-1.1.0 (c (n "node-resolve") (v "1.1.0") (d (list (d (n "node-builtins") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "14n146plh4qhmxi3wmba3hw3il0sr7mcc104ld87l0gy3ivr9lxa")))

(define-public crate-node-resolve-2.0.0 (c (n "node-resolve") (v "2.0.0") (d (list (d (n "node-builtins") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "1v2grnljwh06vgn1jpc21f9rm9yisbqh6qqf8wwb432jkc1gs623")))

(define-public crate-node-resolve-2.1.1 (c (n "node-resolve") (v "2.1.1") (d (list (d (n "node-builtins") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "0iikq2b8y4sh052dkwq466k35c332vl0qshgyvravrmymszzjach")))

(define-public crate-node-resolve-2.2.0 (c (n "node-resolve") (v "2.2.0") (d (list (d (n "node-builtins") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "1693iqydnnnxqjyrzddk48r6kg53vcvsvnkqdy39fc5p4hxhxagn")))

