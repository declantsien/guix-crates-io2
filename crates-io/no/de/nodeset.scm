(define-module (crates-io no de nodeset) #:use-module (crates-io))

(define-public crate-nodeset-0.0.1 (c (n "nodeset") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1jk86rl8p4pb8idzjdq4j9gbp39jykv4bsp7clbwqapziyyxpcrv")))

(define-public crate-nodeset-0.0.2 (c (n "nodeset") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1cw21gir7q66hv86wfcvmby7h8ws8fn4qas09fpv3824wq2p6kaw")))

(define-public crate-nodeset-0.1.0 (c (n "nodeset") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0is8akx57039d0r716ciavf0f4fb8268ramywrw8chhymvz8vnwl")))

(define-public crate-nodeset-0.1.1 (c (n "nodeset") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0m5g3biw3k9bv2nsj9rl6y3y1c2ha8fqxmvk7gpz1mnhy6fj695w")))

(define-public crate-nodeset-0.1.2 (c (n "nodeset") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "07rvky08xkyf4b32y2l3ilwyg7pppz1cg85b0rnaq52r0hr4q6c3")))

(define-public crate-nodeset-0.2.0 (c (n "nodeset") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "13n9g7csbwdbrqsy0cassa9p0qlphl0jfg3sjwjg5riqdpryv9y0")))

(define-public crate-nodeset-0.3.1 (c (n "nodeset") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0rnnlzif1144ggi6jyvyvzlqi29bkir9y73b658r16r4ah5pqmvf")))

(define-public crate-nodeset-0.3.2 (c (n "nodeset") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0zv4fcp0z098i1kxn8ig8j3zmvr4yzvgz5vy9gdkc02156v8sx6l")))

(define-public crate-nodeset-0.3.3 (c (n "nodeset") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "04g1dcran4lwjbvw156awdlcfd792zvvp567d5vbbykihic7pzfy")))

(define-public crate-nodeset-0.3.4 (c (n "nodeset") (v "0.3.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0j7vc0j82brpd3dw08mdvwmi8hrxzl33mrl57wrnq1l2z4pnq1c7")))

(define-public crate-nodeset-0.4.0 (c (n "nodeset") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "17zx7d3bxir36y3551091mgacp55zalvql80kghygmk61iscfgwp")))

(define-public crate-nodeset-0.4.1 (c (n "nodeset") (v "0.4.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1j2k9qbs4399apqz01n4gmdaxnw9y5c2m843k9a2axcgnqa7rwq1")))

