(define-module (crates-io no de node-browser-testing) #:use-module (crates-io))

(define-public crate-node-browser-testing-0.0.0 (c (n "node-browser-testing") (v "0.0.0") (h "0xlfwqlwf2sydci2v21zbwkb21kzrzrmhj9spn60h9ilzxv1ykwa") (y #t)))

(define-public crate-node-browser-testing-2.0.0 (c (n "node-browser-testing") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "node-cli") (r "^2.0.0") (f (quote ("browser"))) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tc-rpc-api") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-libp2p") (r "^0.34.2") (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 0)))) (h "1s9kzylfga5wfn5d4lhc6g68xclmfiqf50wakphyfjhiz0kj6l0v")))

