(define-module (crates-io no de node-to-arc-centric-dbg) #:use-module (crates-io))

(define-public crate-node-to-arc-centric-dbg-1.0.0 (c (n "node-to-arc-centric-dbg") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genome-graph") (r "^5.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "04n7ljkyr9d537s0v4wb5y4cmnw2j0s9yk00x7nhz0dz6x2phszw")))

(define-public crate-node-to-arc-centric-dbg-2.0.0 (c (n "node-to-arc-centric-dbg") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genome-graph") (r "^5.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "11vp91jai6p0bm1r2q7mnij8jwh3v5vbaqwcq8dj34smqp489bsk") (r "1.65.0")))

(define-public crate-node-to-arc-centric-dbg-2.0.1 (c (n "node-to-arc-centric-dbg") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genome-graph") (r "^6.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "0175895bv9r2050ggsz6r28pgd1bpvzwly9m6w2hz6ka1jk0vd77") (r "1.65.0")))

(define-public crate-node-to-arc-centric-dbg-3.0.0 (c (n "node-to-arc-centric-dbg") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genome-graph") (r "^8.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "self-meter") (r "^0.6.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "08a5j3lk1xj4lwsld9fj8q41d5k3n4ss0smac40pgznzgmwyf61l") (r "1.67.0")))

(define-public crate-node-to-arc-centric-dbg-3.0.1 (c (n "node-to-arc-centric-dbg") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genome-graph") (r "^8.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "self-meter") (r "^0.6.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "0r4vkxikz7vqp67r99kji29m9h489374cgi97pjns8w0msbrj7mb") (r "1.70.0")))

