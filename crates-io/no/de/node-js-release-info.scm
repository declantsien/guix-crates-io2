(define-module (crates-io no de node-js-release-info) #:use-module (crates-io))

(define-public crate-node-js-release-info-0.1.0 (c (n "node-js-release-info") (v "0.1.0") (d (list (d (n "mockito") (r "1.*") (d #t) (k 2)) (d (n "reqwest") (r "0.11.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("macros" "net" "time"))) (k 0)))) (h "1kirgkykwvh3bwhb5l326znxwm10bijsnr9g6f95c1dvfr10glqk")))

(define-public crate-node-js-release-info-0.1.1 (c (n "node-js-release-info") (v "0.1.1") (d (list (d (n "mockito") (r "1.*") (d #t) (k 2)) (d (n "reqwest") (r "0.11.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("macros" "net" "time"))) (k 0)))) (h "02gld8qzblxfj1jzaasldvmn6gvs9z7qgcfanik5bdxa8wgb392v")))

(define-public crate-node-js-release-info-1.0.0 (c (n "node-js-release-info") (v "1.0.0") (d (list (d (n "mockito") (r "1.*") (d #t) (k 2)) (d (n "reqwest") (r "0.11.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)) (d (n "tokio") (r "1.*") (f (quote ("macros" "net" "time"))) (k 0)))) (h "1331jxsxzb5hyw8fj2ay59b1qlixh13h7wd86xn55qfw58yddr3q") (s 2) (e (quote (("json" "dep:serde"))))))

(define-public crate-node-js-release-info-1.1.0 (c (n "node-js-release-info") (v "1.1.0") (d (list (d (n "mockito") (r "1.*") (d #t) (k 2)) (d (n "reqwest") (r "0.11.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)) (d (n "tokio") (r "1.*") (f (quote ("macros" "net" "time"))) (k 0)))) (h "0fxk5qs2qbh0gfj840mnfwwr4ymsdn3zysmvpdygshl26jf2a0x8") (s 2) (e (quote (("json" "dep:serde"))))))

(define-public crate-node-js-release-info-1.1.1 (c (n "node-js-release-info") (v "1.1.1") (d (list (d (n "mockito") (r "1.*") (d #t) (k 2)) (d (n "reqwest") (r "0.11.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 2)) (d (n "tokio") (r "1.*") (f (quote ("macros" "net" "time"))) (k 0)))) (h "1dgmcidgqqzd8gcq0zb7dgpwlh2wq83350c3gs35xg9dypz7agbz") (s 2) (e (quote (("json" "dep:serde"))))))

