(define-module (crates-io no de nodejs-launcher) #:use-module (crates-io))

(define-public crate-nodejs-launcher-0.1.0 (c (n "nodejs-launcher") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0vk68nylyd7dggizwdlfd76cijyfqcmjjpmib4viwdr81yvbln69")))

(define-public crate-nodejs-launcher-0.1.1 (c (n "nodejs-launcher") (v "0.1.1") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "13bxliqwgr9rjac9v1hfjdh747kzff596a4s2zzqlj7c2yx7v31i")))

(define-public crate-nodejs-launcher-0.1.2 (c (n "nodejs-launcher") (v "0.1.2") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "17z6jdi2wh5pqk5j66dbswbb3zj0hks4d4r2i4mzgybmigs872q7")))

(define-public crate-nodejs-launcher-0.1.3 (c (n "nodejs-launcher") (v "0.1.3") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "17sf4kkgsf6kz992p8qpnvglbrv6kzcj9873s09r7k20p921s2md")))

