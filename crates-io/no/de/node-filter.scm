(define-module (crates-io no de node-filter) #:use-module (crates-io))

(define-public crate-node-filter-0.0.0 (c (n "node-filter") (v "0.0.0") (h "0a4930vl3xd15g43w35zfjqyyb0v6g4zyl35ijkxgff15z692108")))

(define-public crate-node-filter-1.12.0 (c (n "node-filter") (v "1.12.0") (d (list (d (n "client-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-kvdb-memorydb") (r "^0.3.2") (d #t) (k 2)) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)) (d (n "vapabi-contract") (r "^9.0.0") (d #t) (k 0)) (d (n "vapabi-derive") (r "^9.0.1") (d #t) (k 0)) (d (n "vapcore") (r "^1.12.0") (d #t) (k 0)) (d (n "vapcore-io") (r "^1.12.0") (d #t) (k 2)) (d (n "vapcore-network") (r "^1.12.0") (d #t) (k 0)) (d (n "vapcore-network-devp2p") (r "^1.12.0") (d #t) (k 0)) (d (n "vapcore-spec") (r "^0.1.0") (d #t) (k 2)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "16vq8cm4rf62q435bsmgh04byyny37lpzkhhg9hfqs4j2gpvyzfa")))

