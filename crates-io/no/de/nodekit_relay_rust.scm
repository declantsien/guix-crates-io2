(define-module (crates-io no de nodekit_relay_rust) #:use-module (crates-io))

(define-public crate-nodekit_relay_rust-0.1.0 (c (n "nodekit_relay_rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "bs58") (r "^0.5.1") (d #t) (k 0)) (d (n "nodekit_seq_sdk") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "04hk9inwrsw3l6ias0yj3y8b4r5f8x7wx3kq5xz5b6nik2khz93c")))

