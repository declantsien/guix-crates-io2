(define-module (crates-io no de node_resolver) #:use-module (crates-io))

(define-public crate-node_resolver-0.1.0 (c (n "node_resolver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0npaf0lxq4y3c326aqbpxigg68wlq0xvxmi636sk6kkg9xhd2pp3")))

(define-public crate-node_resolver-0.1.1 (c (n "node_resolver") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "path-clean") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bp52kr1gvgp64jlv3wha3340bcapbvbmx1i4wklrhaw9z5jy0gi")))

