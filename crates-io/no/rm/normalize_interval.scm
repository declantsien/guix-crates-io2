(define-module (crates-io no rm normalize_interval) #:use-module (crates-io))

(define-public crate-normalize_interval-0.13.0 (c (n "normalize_interval") (v "0.13.0") (d (list (d (n "few") (r "^0.1") (d #t) (k 0)))) (h "15g14xqj0bx685d8pg07qh54nxc77b9fgsa9y94fifhx3irzdazz") (f (quote (("default"))))))

(define-public crate-normalize_interval-0.13.1 (c (n "normalize_interval") (v "0.13.1") (d (list (d (n "few") (r "^0.1") (d #t) (k 0)))) (h "0w3xpgnvgrd5r1579647w06ns2kp9c73knl6zg2y3x30y8haagyb") (f (quote (("default"))))))

(define-public crate-normalize_interval-0.14.0 (c (n "normalize_interval") (v "0.14.0") (d (list (d (n "few") (r "^0.1") (d #t) (k 0)))) (h "052azfzs3w54a2vhpvld636hhjx1nyrc4dw2cid53843zb5kiv0c") (f (quote (("default"))))))

