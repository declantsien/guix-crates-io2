(define-module (crates-io no rm norm-email) #:use-module (crates-io))

(define-public crate-norm-email-0.1.0 (c (n "norm-email") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0xgwfrsxcnp9d02khlfyhr3xqdnij2jfmn2qsgal6c6cq34010y0")))

