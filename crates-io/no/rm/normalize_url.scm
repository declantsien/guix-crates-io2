(define-module (crates-io no rm normalize_url) #:use-module (crates-io))

(define-public crate-normalize_url-0.1.0 (c (n "normalize_url") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "03gb37yg89d439f9l89wnk6q3qbxx1ns7yq057c35zz4yl8njg5w")))

(define-public crate-normalize_url-0.1.1 (c (n "normalize_url") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "05yq8j6hcp709fqi5q60p55pmdw9bh2q6flzsnyakaszkmlvqdh7")))

(define-public crate-normalize_url-0.2.0 (c (n "normalize_url") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "111772p3a89c0l00sr2935hlxy9zhj1kxk1pwg8bvk7560x29whi")))

