(define-module (crates-io no rm normal_map) #:use-module (crates-io))

(define-public crate-normal_map-0.1.0 (c (n "normal_map") (v "0.1.0") (h "1w40ki9la3g2nv3i9fcijgx4yg7xdq02n8aqxi1rn6sw0h3x2kpw")))

(define-public crate-normal_map-0.1.1 (c (n "normal_map") (v "0.1.1") (h "1nppdmim0xa8fr6nz428w6kcjhh8h6zqmijd4ky8xl5dhkssn082")))

(define-public crate-normal_map-0.1.2 (c (n "normal_map") (v "0.1.2") (h "11ay5y8nvakxavg83zdn4wwh3y4nqnyl2xdiac284ix1i4fdfrr3")))

(define-public crate-normal_map-0.2.0 (c (n "normal_map") (v "0.2.0") (h "141d11pnd9lm068fcbbsxq5wz1q0d6zj1k7nlgrbqyal65xy7h68")))

(define-public crate-normal_map-0.2.1 (c (n "normal_map") (v "0.2.1") (h "1b1kjhlj85zczipg8irxc8a0cwgcnb6xzjfnd8q34mrh92gg2q1v")))

