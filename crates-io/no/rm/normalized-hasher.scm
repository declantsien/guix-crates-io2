(define-module (crates-io no rm normalized-hasher) #:use-module (crates-io))

(define-public crate-normalized-hasher-0.1.0 (c (n "normalized-hasher") (v "0.1.0") (d (list (d (n "base16ct") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0zvv0rv32w94203f7ik7bwhx6mqisx7xamh0f9b8j6vc9h8ay64n")))

(define-public crate-normalized-hasher-0.2.0 (c (n "normalized-hasher") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "normalized-hash") (r "^0.1.0") (d #t) (k 0)))) (h "1i3g6k9xdydh2vjg57cj009hj9p323p9bj0d9kxajm8zlz05ng0s")))

