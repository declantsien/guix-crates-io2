(define-module (crates-io no rm normalized-hash) #:use-module (crates-io))

(define-public crate-normalized-hash-0.1.0 (c (n "normalized-hash") (v "0.1.0") (d (list (d (n "base16ct") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0j5yac2fysrkllkc13ddiql3s47dsfqzv4l5qzyryg8zdfvkp78d")))

