(define-module (crates-io no rm normalize_country) #:use-module (crates-io))

(define-public crate-normalize_country-0.1.0 (c (n "normalize_country") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0jzyll2rk0xm7x2fm7s1wcnf4np6s0ccjgn44j0bfldcm1598473")))

(define-public crate-normalize_country-0.1.1 (c (n "normalize_country") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1iq53721148h0wrbwrwrvsqd0297gid5h5d0mfad2v5mj5v441ww")))

(define-public crate-normalize_country-0.1.2 (c (n "normalize_country") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0wagk7c4r4m9cmhc8k5f2x94k2lvlc0sc01f5cr9599lg80vjyny")))

