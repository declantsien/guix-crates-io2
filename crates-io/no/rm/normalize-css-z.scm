(define-module (crates-io no rm normalize-css-z) #:use-module (crates-io))

(define-public crate-normalize-css-z-0.1.0 (c (n "normalize-css-z") (v "0.1.0") (h "016r0wpjr20866pglf24gi791rz2bqw62kmnh9n2f5y6g89n4xrn") (y #t)))

(define-public crate-normalize-css-z-0.1.1 (c (n "normalize-css-z") (v "0.1.1") (h "1zp6lcayhsaff016qpjzsz94qz5538cjf6dppbs41da60n5jqism") (y #t)))

(define-public crate-normalize-css-z-0.1.2 (c (n "normalize-css-z") (v "0.1.2") (h "07v6c48fb6sxs9l2harinixww6f7xgngibfy5pks8h27s3py8fg8") (y #t)))

(define-public crate-normalize-css-z-0.1.3 (c (n "normalize-css-z") (v "0.1.3") (h "0iwmphhwdk5wvd6x6r8v6gxhhp52swm078yyscmban2xjjva3p7j") (y #t)))

(define-public crate-normalize-css-z-0.2.0 (c (n "normalize-css-z") (v "0.2.0") (h "01wzzsxl1815ms0nxhppygqcaffz9hh9k4bhhyjrkhgz69h300zw")))

(define-public crate-normalize-css-z-0.2.1 (c (n "normalize-css-z") (v "0.2.1") (h "10kfl9dq9687makcm8ah4c41ap0lfb3h0qjdsry48x206js7lzbv")))

(define-public crate-normalize-css-z-0.3.0 (c (n "normalize-css-z") (v "0.3.0") (h "04nh7nlhdvn0wgz0c8gzg5x4dcmx8447704i7bgld0klkb3jbxj9")))

(define-public crate-normalize-css-z-0.3.1 (c (n "normalize-css-z") (v "0.3.1") (h "0wsygmv3hfz5p8c4aqaqwq72iq02dr8yslfkknpxs3l3ij1c2yd1")))

(define-public crate-normalize-css-z-0.3.2 (c (n "normalize-css-z") (v "0.3.2") (h "179gx2fh4q6p0fkjy2q639n62r6sa591sqyapmpgw7w926bqk6c6")))

(define-public crate-normalize-css-z-0.4.0 (c (n "normalize-css-z") (v "0.4.0") (h "16w9iqynj8h5yxlhy46rcii4cmy9jrxzxmc81bnnilnx206k84a2")))

(define-public crate-normalize-css-z-0.4.1 (c (n "normalize-css-z") (v "0.4.1") (h "03bjnjal7w38fpj433jizls5mi9zz4mdwxnxjnjcf6nhymj4h54i")))

(define-public crate-normalize-css-z-0.4.2 (c (n "normalize-css-z") (v "0.4.2") (h "1s0mllwbc4433jx6cw5xf0dphf5imgfi7cd3nfrvpbr54qz5jhgn")))

(define-public crate-normalize-css-z-0.4.3 (c (n "normalize-css-z") (v "0.4.3") (h "0j43gn2mfd6ap1y54av6g483h79mrrvvd9j8w9qs4d8wh1awml54")))

(define-public crate-normalize-css-z-0.5.0 (c (n "normalize-css-z") (v "0.5.0") (h "083cm7jn6kxlrzcb563q4m10wjrsgi19jcq6x7h1fimms3r5kjif")))

(define-public crate-normalize-css-z-0.5.1 (c (n "normalize-css-z") (v "0.5.1") (h "013whmxnwkcliw4hjmf2dj5g8bk9b85rcgnc9wm35rhybx3i46mf")))

(define-public crate-normalize-css-z-0.6.0 (c (n "normalize-css-z") (v "0.6.0") (h "0k5y4dzmg939krbjdsxmxnd47wgv49gsjccd8vz1mligk7dfq6lf") (f (quote (("custom"))))))

(define-public crate-normalize-css-z-0.6.1 (c (n "normalize-css-z") (v "0.6.1") (h "0195k7pjvgvzs6i3jp6d2lj9lwqr222300qccjzfqkjcxmqdfldw") (f (quote (("custom"))))))

(define-public crate-normalize-css-z-0.7.0 (c (n "normalize-css-z") (v "0.7.0") (h "1ckyc7ld7ipc7bmas0jhfad85sakfy19vmcnabl9ncp6krzplyil") (f (quote (("custom"))))))

