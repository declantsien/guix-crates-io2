(define-module (crates-io no rm normalize-line-endings) #:use-module (crates-io))

(define-public crate-normalize-line-endings-0.1.0 (c (n "normalize-line-endings") (v "0.1.0") (h "1iyl0nf31lymj2cgl7hy383q4791fsvfs2iqhkmw5gwpzrrrqwgy")))

(define-public crate-normalize-line-endings-0.2.0 (c (n "normalize-line-endings") (v "0.2.0") (h "1sb25kxmi1xs63n1bdkwf6hbhigb85hs8h3g5diq2a7g8vlj1myk")))

(define-public crate-normalize-line-endings-0.2.1 (c (n "normalize-line-endings") (v "0.2.1") (h "15rzn43h1prkmkf3kp11bxplyiwi1g6r088m6in00wndmy17nqj1")))

(define-public crate-normalize-line-endings-0.2.2 (c (n "normalize-line-endings") (v "0.2.2") (h "1a1knz9j1w5a1pl2q6whmjphm3z6p64r5njnam7syp5rx8wil2if")))

(define-public crate-normalize-line-endings-0.3.0 (c (n "normalize-line-endings") (v "0.3.0") (h "1gp52dfn2glz26a352zra8h04351icf0fkqzw1shkwrgh1vpz031")))

