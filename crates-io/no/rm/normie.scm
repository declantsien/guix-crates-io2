(define-module (crates-io no rm normie) #:use-module (crates-io))

(define-public crate-normie-1.0.0 (c (n "normie") (v "1.0.0") (h "04sbiv5hr3735rmml0kykrdkg1yl014qqsxj3qwzw55zw5gasbqc")))

(define-public crate-normie-1.0.1 (c (n "normie") (v "1.0.1") (h "10j419syp8nbh23524j5qs1vgnhrk2pq4rkdndk0d233wsgalkn8")))

(define-public crate-normie-1.0.2 (c (n "normie") (v "1.0.2") (h "0j9a2gcw1nixi944ryxmagdwmjjfi1jwvyva8kzf3qbchnxjma56")))

