(define-module (crates-io no rm normalize-path) #:use-module (crates-io))

(define-public crate-normalize-path-0.1.0 (c (n "normalize-path") (v "0.1.0") (h "0xsf2l58r6d96xql68wcz9gzrkh9s330lrv09s3wkww7ldziphg5") (r "1.61.0")))

(define-public crate-normalize-path-0.2.0 (c (n "normalize-path") (v "0.2.0") (h "1wnrx1rrm0hfd67wgddz1990lbl2qrq3nbjpa1rm3jz3n8cy68ng") (r "1.61.0")))

(define-public crate-normalize-path-0.2.1 (c (n "normalize-path") (v "0.2.1") (h "0bc919zp34b9312wqd92jbi3xa9gxljxh8nfw7v6sk7znb98shzm") (r "1.61.0")))

