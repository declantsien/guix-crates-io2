(define-module (crates-io no rm normal-form) #:use-module (crates-io))

(define-public crate-normal-form-0.1.0 (c (n "normal-form") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "grdf") (r "^0.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rdf-types") (r "^0.15.3") (d #t) (k 2)))) (h "0dlzw9j4g7zg72jzrmyl6rfy6m6brjcf5dk42y6zkg9kcc3h72xa")))

(define-public crate-normal-form-0.1.1 (c (n "normal-form") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "grdf") (r "^0.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rdf-types") (r "^0.15.3") (d #t) (k 2)))) (h "0mjg7854xh1r9f3jb4v1vjbb9pyv13vx9xsqg14izg7slh96xmc6")))

