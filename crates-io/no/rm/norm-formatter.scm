(define-module (crates-io no rm norm-formatter) #:use-module (crates-io))

(define-public crate-norm-formatter-1.0.2 (c (n "norm-formatter") (v "1.0.2") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "10g6wp7jpyjfja7rhclk365mpacy5r0syzx6fmlvzjp6jpyl8590")))

(define-public crate-norm-formatter-1.0.4 (c (n "norm-formatter") (v "1.0.4") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "11xlqq2kc3zambipz2idbyrmahgp7sh0m47g1ia3iq7ya1330w4g")))

