(define-module (crates-io no rm norms) #:use-module (crates-io))

(define-public crate-norms-0.1.0 (c (n "norms") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0rcspg3vmh2bngb4cbinwyhcnkqf8ki1y95w418cw4jf6jda6ydj") (f (quote (("fzf-v2" "__any-metric") ("fzf-v1" "__any-metric") ("__tests") ("__into-score") ("__benches") ("__any-metric"))))))

(define-public crate-norms-0.1.1 (c (n "norms") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0ldpcxnzcrww6q8igdcda8zr9ap4m5i7sbq2hxwa11cqi5knjk2z") (f (quote (("fzf-v2" "__any-metric") ("fzf-v1" "__any-metric") ("__tests" "fzf-v1" "fzf-v2") ("__into-score") ("__benches") ("__any-metric"))))))

