(define-module (crates-io no rm norman) #:use-module (crates-io))

(define-public crate-norman-0.0.4 (c (n "norman") (v "0.0.4") (d (list (d (n "ndarray") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "1in4szvl7jnavnj5q0afg5fl4kfz4v3vn325wdxlv5gkrpzlwb0d") (f (quote (("default" "num-complex" "ndarray") ("array"))))))

