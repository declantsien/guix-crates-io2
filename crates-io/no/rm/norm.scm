(define-module (crates-io no rm norm) #:use-module (crates-io))

(define-public crate-norm-0.1.1 (c (n "norm") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "021523m3sq0j9djqv1gdfgs0vh91ry3sydrlvw5dqi4w6yijamzd") (f (quote (("fzf-v2" "__any-metric") ("fzf-v1" "__any-metric") ("__tests" "fzf-v1" "fzf-v2") ("__into-score") ("__benches") ("__any-metric"))))))

