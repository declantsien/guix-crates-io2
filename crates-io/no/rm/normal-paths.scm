(define-module (crates-io no rm normal-paths) #:use-module (crates-io))

(define-public crate-normal-paths-0.1.0 (c (n "normal-paths") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "04r8cjpi2nbsfr5dg2vrirm6s3akf81jirfkk5cmrb3ms5shs8fw")))

