(define-module (crates-io no mo nomos) #:use-module (crates-io))

(define-public crate-nomos-0.1.0 (c (n "nomos") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "nomos-runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.5.7") (f (quote ("singlepass"))) (d #t) (k 0)))) (h "0l75zg9lrq1k4h10a5vd407j8vckx430kxl648djwc88f3grqffp")))

