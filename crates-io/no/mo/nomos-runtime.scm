(define-module (crates-io no mo nomos-runtime) #:use-module (crates-io))

(define-public crate-nomos-runtime-0.1.0 (c (n "nomos-runtime") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03bvb41q0i2f84dabnchi8q1z22ik5z80jmjsll5nvpml9ck2wib")))

