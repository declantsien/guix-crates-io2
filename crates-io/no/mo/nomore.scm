(define-module (crates-io no mo nomore) #:use-module (crates-io))

(define-public crate-nomore-0.1.0 (c (n "nomore") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1qr24rys5fjg9gcl1f0i1yrid0vl117wrrfjil8kbkcabmh78raw") (y #t)))

(define-public crate-nomore-0.1.2 (c (n "nomore") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1szx153bdd1sfhiw6jwg6dnxx7kvg9f4jbyp84d9kwqr3bgj97dv") (y #t)))

(define-public crate-nomore-0.1.3 (c (n "nomore") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "18f08lkgakww134bx9pc5rf3smjfr6gk13wi4ljgm09dm5wqhl09")))

(define-public crate-nomore-0.1.4 (c (n "nomore") (v "0.1.4") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "0c92gdm4rrazld5j5vvxrqix2cmid5f3p5aq9l92k520k2900bdl")))

(define-public crate-nomore-0.2.0 (c (n "nomore") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "0ymdq431v5mqw96aybgi83i5kr5qqj3q9fyl54mhwms31jm28qbg") (y #t)))

(define-public crate-nomore-0.2.1 (c (n "nomore") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1z6k9dgfmwziln11d7c7wyc1sr3pbx2d8cnag61rryxgxzm5cy1q")))

