(define-module (crates-io no -b no-bitches) #:use-module (crates-io))

(define-public crate-no-bitches-0.1.0 (c (n "no-bitches") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.2.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0zq2rcsm4s8cm34b3sxfynh4xib1yva04sa1bjjkdfyv1l2ca1rz") (f (quote (("no-reexport") ("default"))))))

