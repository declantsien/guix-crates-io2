(define-module (crates-io no rx norx-permutation) #:use-module (crates-io))

(define-public crate-norx-permutation-0.1.0 (c (n "norx-permutation") (v "0.1.0") (d (list (d (n "coresimd") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "1nz7j0gky9vzjb1m7879jz8zhnal2jrs8ijhg72nv2rdb2l85ww3") (f (quote (("simd" "coresimd") ("default" "W64" "L4") ("W8") ("W64") ("W32") ("W16") ("L6") ("L4"))))))

(define-public crate-norx-permutation-0.1.1 (c (n "norx-permutation") (v "0.1.1") (d (list (d (n "coresimd") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "04f3dld7p3l2ggjs1yx084zykdz0m5mb1kgcz0aip17y2yf46vi2") (f (quote (("simd" "coresimd") ("default" "W64" "L4") ("W8") ("W64") ("W32") ("W16") ("L6") ("L4"))))))

(define-public crate-norx-permutation-0.1.2 (c (n "norx-permutation") (v "0.1.2") (h "1bq4mknp09y2gbkad9g0cn4gs56lx5swx0hha7srxszlz1r1y4qh") (f (quote (("simd") ("default" "W64" "L4") ("W8") ("W64") ("W32") ("W16") ("L6") ("L4"))))))

(define-public crate-norx-permutation-0.1.3 (c (n "norx-permutation") (v "0.1.3") (d (list (d (n "packed_simd") (r "^0.1") (f (quote ("into_bits"))) (o #t) (d #t) (k 0)))) (h "1p8gvk0xh3bkiim3ndl8y4i5nwcvjpfiqbq98h2q5r1hbj3pypzk") (f (quote (("simd" "packed_simd") ("default" "W64" "L4") ("W8") ("W64") ("W32") ("W16") ("L6") ("L4"))))))

(define-public crate-norx-permutation-0.1.4 (c (n "norx-permutation") (v "0.1.4") (d (list (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (o #t) (d #t) (k 0)))) (h "09n9y3ls4i23wyv4j108mmj9hlqaylkllxci36gvxjcvf712105s") (f (quote (("simd" "packed_simd") ("default" "W64" "L4") ("W8") ("W64") ("W32") ("W16") ("L6") ("L4"))))))

