(define-module (crates-io no ma nomad) #:use-module (crates-io))

(define-public crate-nomad-0.1.0 (c (n "nomad") (v "0.1.0") (h "0fifnv8zd4ddk6clkikws7mqry5yr643rg0nppqlxhklas1pfnlf")))

(define-public crate-nomad-0.2.0 (c (n "nomad") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.2") (d #t) (k 0)))) (h "1hzp604qdfh0ig2wywz8qplmljyg1n1jykjhnq17jxv72sd13pw3")))

