(define-module (crates-io no ma nomad-client) #:use-module (crates-io))

(define-public crate-nomad-client-0.0.1 (c (n "nomad-client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "15ypa4bparz9qh9pywzmld1f1zj6b22nqvxyp9yyikg2rd2gh459")))

(define-public crate-nomad-client-0.0.2 (c (n "nomad-client") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "048a8zpv1n3d9basm6xaa8z9jjw09f6h3g9ggm4cpijsaha5ya4x")))

(define-public crate-nomad-client-0.0.3 (c (n "nomad-client") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1wp24wk46f1l58cyc8dfjkv6bvr7pm4lzhjnlg3dfdvwy933z79r")))

(define-public crate-nomad-client-0.0.4 (c (n "nomad-client") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0l92hszgr15gn3g5wj64078si2bkibnxzgv94r11kxh7pgc69na0")))

(define-public crate-nomad-client-0.0.5 (c (n "nomad-client") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "148a9lnylc08686hhqilpjhmsgafx3r9qw4jpql6v9gs5422znyr")))

(define-public crate-nomad-client-0.0.6 (c (n "nomad-client") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1rshwpzv77mqf857ijahxrya91qipfm2wqx5wsaac5p4jq740v6y")))

(define-public crate-nomad-client-0.0.7 (c (n "nomad-client") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0wfywmdypbjm7prw235xclqh9ib3fhx8ik9zfzdb7gr0q6j4qmhx")))

(define-public crate-nomad-client-0.0.8 (c (n "nomad-client") (v "0.0.8") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "16sc5x3ync895q6rffjv2mk5lqdjhpvcv39zy2limkjfrbjqbmfr")))

(define-public crate-nomad-client-0.0.9 (c (n "nomad-client") (v "0.0.9") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1avb44blmyn04y9vs9hs7ca2qp0sv7i43yv92078n22s1wlvfj0m")))

(define-public crate-nomad-client-0.0.10 (c (n "nomad-client") (v "0.0.10") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0saw70yy0inhgzpa5ic8ydlachqvxx51bx3jr8c5vmr9yhjn5ckr")))

