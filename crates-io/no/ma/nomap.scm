(define-module (crates-io no ma nomap) #:use-module (crates-io))

(define-public crate-nomap-0.1.0 (c (n "nomap") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "nom-fields") (r "^0.1.0") (d #t) (k 0)))) (h "0rf29lmr6fyfbq6r89z6h5yxbl23h4gz9bjlp1j5pxwgzfvqfam9")))

(define-public crate-nomap-0.1.1 (c (n "nomap") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "nom-fields") (r "^0.1.0") (d #t) (k 0)))) (h "1n72mq3wwsl6y37j2hcy7wk3zhvvs1h5n17088p21p7p2j90fyxk") (f (quote (("display") ("default"))))))

(define-public crate-nomap-0.2.0 (c (n "nomap") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "nom-fields") (r "^0.1.1") (d #t) (k 0)))) (h "1wy8hz9wzyxaspyh203vz7y8493ayj14v7fbpkcfhmjw54hayxgd") (f (quote (("display") ("default"))))))

(define-public crate-nomap-0.2.1 (c (n "nomap") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "nom-fields") (r "^0.1.1") (d #t) (k 0)))) (h "18pdi2xsp278p7mf9m2d1fng91knzynw4p2j66j1gigv5rksbr92") (f (quote (("display") ("default"))))))

