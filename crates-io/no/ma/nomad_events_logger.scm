(define-module (crates-io no ma nomad_events_logger) #:use-module (crates-io))

(define-public crate-nomad_events_logger-0.1.3 (c (n "nomad_events_logger") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nomad-client-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d7g85v5srfmyrn90irxkbg54xwy1m4a9m0s1arzcpk56hdr6dxr")))

