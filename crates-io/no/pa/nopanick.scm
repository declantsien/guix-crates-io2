(define-module (crates-io no pa nopanick) #:use-module (crates-io))

(define-public crate-nopanick-0.1.0 (c (n "nopanick") (v "0.1.0") (h "12kajca95dw33d0v841b0mhw0kahg32hg3al2z1di4w9hw9szzq2") (y #t)))

(define-public crate-nopanick-0.1.1 (c (n "nopanick") (v "0.1.1") (h "0gbz73srnr4zymi3vgkbdp95vaf8b46jkg1mp8d4x1in07hbnq24") (y #t)))

(define-public crate-nopanick-0.1.2 (c (n "nopanick") (v "0.1.2") (h "0ff1gqq9cxyz3yr29azm24nvd83hcr55zlizqrjf5v3fg5l1rxxa") (y #t)))

(define-public crate-nopanick-0.1.3 (c (n "nopanick") (v "0.1.3") (h "1igfz1p6iwba3nsmmrivv4q5jzrzb6aacqs4fsmpdnb12rwq8zyj") (y #t)))

(define-public crate-nopanick-0.1.4 (c (n "nopanick") (v "0.1.4") (h "1cq9ykx5b4yq6qm1v6kyr2nl3cdjjvv4mbk4a6ywmm3iaf0y2814")))

(define-public crate-nopanick-0.1.5 (c (n "nopanick") (v "0.1.5") (h "1hsmazjvsmdfyv1203h7idzin19xcg2cn9bl2kp50k5s9pmwdmw6") (y #t)))

(define-public crate-nopanick-0.1.6 (c (n "nopanick") (v "0.1.6") (h "1s0ar1b0bcv9zs81sfz2fbsn6ia4nd6k3s0xs5jzrsc1swg7b792")))

(define-public crate-nopanick-0.1.7 (c (n "nopanick") (v "0.1.7") (h "17nn9vrqhm266dxycc5fwz9qa7d0ri1hkhbcvb15y2m3jk26c0sm")))

(define-public crate-nopanick-0.2.0 (c (n "nopanick") (v "0.2.0") (h "1av617lg20398dxnisj1f56iw2p240v1ky4wjk11ybzajvjvpw0x")))

