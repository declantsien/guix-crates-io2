(define-module (crates-io no _m no_mutex) #:use-module (crates-io))

(define-public crate-no_mutex-0.0.0 (c (n "no_mutex") (v "0.0.0") (h "0z1haq06l3amvjlx5bjmwfskyn4scy3pg12m1vz55nvald6a17fh")))

(define-public crate-no_mutex-0.0.1 (c (n "no_mutex") (v "0.0.1") (h "19n5v9k7d1ngsxw7k4fa3yw3v2c1fvg5m7c88z6fam4mpj27nsvk")))

