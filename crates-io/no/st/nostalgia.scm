(define-module (crates-io no st nostalgia) #:use-module (crates-io))

(define-public crate-nostalgia-0.0.1 (c (n "nostalgia") (v "0.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "fake") (r "^2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "nostalgia-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "01jgicnc4fpl5xvsgbkimpgm9kkwa5ya6bj63krz4bdffqpxqj05")))

