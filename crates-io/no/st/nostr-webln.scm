(define-module (crates-io no st nostr-webln) #:use-module (crates-io))

(define-public crate-nostr-webln-0.0.0 (c (n "nostr-webln") (v "0.0.0") (h "03d4hwdcvfky8h7cfgyzyx9fv5zviqj9yki62r70xywh8v61nzmn")))

(define-public crate-nostr-webln-0.28.0 (c (n "nostr-webln") (v "0.28.0") (d (list (d (n "nostr-zapper") (r "^0.28") (k 0)) (d (n "webln") (r "^0.1") (d #t) (k 0)))) (h "1ccw1i5ddfy645pmwh67xrg18jkc8f79j5rdfkvcv8q4172db32d") (r "1.64.0")))

(define-public crate-nostr-webln-0.29.0 (c (n "nostr-webln") (v "0.29.0") (d (list (d (n "nostr-zapper") (r "^0.29") (k 0)) (d (n "webln") (r "^0.1") (d #t) (k 0)))) (h "18nvg37vpz8dkzbl2sh0yi7ayx1hignnn4n7c33gfzx9kp0zfqi1") (r "1.64.0")))

(define-public crate-nostr-webln-0.30.0 (c (n "nostr-webln") (v "0.30.0") (d (list (d (n "nostr-zapper") (r "^0.30") (k 0)) (d (n "webln") (r "^0.2") (d #t) (k 0)))) (h "1r8fvx70qf8c3263hxmacmzwqa5zaf9iimcvfrdr4s588562330v") (r "1.64.0")))

(define-public crate-nostr-webln-0.31.0 (c (n "nostr-webln") (v "0.31.0") (d (list (d (n "nostr-zapper") (r "^0.31") (k 0)) (d (n "webln") (r "^0.2") (d #t) (k 0)))) (h "1r5smp1h598li43hp9fpzgmn2kkyk3spq2xmz466libqgx06yrh6") (r "1.64.0")))

