(define-module (crates-io no st nostalgia-derive) #:use-module (crates-io))

(define-public crate-nostalgia-derive-0.0.1 (c (n "nostalgia-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.32") (d #t) (k 2)))) (h "0q8wzd3yg95l4k7mrh70xjb4mv9sh65ssmach4r651ad0fqb792h")))

