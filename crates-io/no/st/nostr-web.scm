(define-module (crates-io no st nostr-web) #:use-module (crates-io))

(define-public crate-nostr-web-0.1.0 (c (n "nostr-web") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "axum-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "nostr") (r "^0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1hbpd8xm6852wpl3ydhixb5fi9wbqdai23zbrmbk20xrc91swpi1") (f (quote (("default" "axum") ("actix" "actix-web" "futures")))) (s 2) (e (quote (("axum" "dep:axum" "axum-core" "async-trait"))))))

