(define-module (crates-io no st nostd_structs) #:use-module (crates-io))

(define-public crate-nostd_structs-0.1.0 (c (n "nostd_structs") (v "0.1.0") (h "0dah557pl54kq7mf9ny2nnf69vqm2knkmh0h8sqq3l34z2bsxynb")))

(define-public crate-nostd_structs-0.1.1 (c (n "nostd_structs") (v "0.1.1") (h "1f266qgr26jw42b0cc6xq6nrwqxi0175byqf6x3giwwbbdz2cwyi")))

(define-public crate-nostd_structs-0.2.0 (c (n "nostd_structs") (v "0.2.0") (h "0mfv4a24373x6ld2g341yn14kn3nqlmmzgrldi36cz7s48dfrzxq")))

(define-public crate-nostd_structs-0.1.2 (c (n "nostd_structs") (v "0.1.2") (h "0xs398hp22iz592pmiix4xfzcrjf42mcnghvhcq3kzg9ddphvj8d")))

(define-public crate-nostd_structs-0.1.3 (c (n "nostd_structs") (v "0.1.3") (h "18a5pqlm07ihz0d7ghpdf3bbwd8jvm41igdbsm6jg5fgc4gyil7k")))

(define-public crate-nostd_structs-0.1.4 (c (n "nostd_structs") (v "0.1.4") (h "1qsav4k4g2pazgk7l069clc0j2p9c4985hrfm6wd0sfr2ykdhpsx")))

(define-public crate-nostd_structs-0.1.5 (c (n "nostd_structs") (v "0.1.5") (h "10a8g9r874f9rq5kzriscl7n0j9w4xss9wl26g7dzfl2xbnmbkx0")))

(define-public crate-nostd_structs-0.1.6 (c (n "nostd_structs") (v "0.1.6") (h "0bzprhrs1wc0sb2ysrbq89w935s5rpq02y31g81q1i6hz3bqhxm5")))

