(define-module (crates-io no st nostcat) #:use-module (crates-io))

(define-public crate-nostcat-0.2.0 (c (n "nostcat") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1f4wdnwm4626izpayd1az1jvsi8imzn6c93bf9viprl7mavqjdq9")))

(define-public crate-nostcat-0.3.0 (c (n "nostcat") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0lzyjs3qxhawcbg4a7hhfc7vnjg76v43q23c3k6129rchbjw975g")))

(define-public crate-nostcat-0.3.1 (c (n "nostcat") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "04n2lj0scyn73g5q04kana0xysxwvk5jb0dvs45nlk4nbpr30jn9")))

(define-public crate-nostcat-0.3.2 (c (n "nostcat") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1k0l7ylbaslgzmx88dwbg4nv1cilgc7zv9fpxviad3p4ab1a67r7")))

(define-public crate-nostcat-0.3.3 (c (n "nostcat") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0lss98dadhi2nbl618ha5wmpgh2js5ddgmrxcdkrm8fybgl0w7mq")))

