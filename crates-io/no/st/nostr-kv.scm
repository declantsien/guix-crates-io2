(define-module (crates-io no st nostr-kv) #:use-module (crates-io))

(define-public crate-nostr-kv-0.1.0 (c (n "nostr-kv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "lmdb-master-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ybnv977gyh6hk0cvhavv2laff5wvs5vicjbjk2bqcfs3x76vjs0")))

(define-public crate-nostr-kv-0.2.0 (c (n "nostr-kv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "lmdb-master-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0k61m6cidbrcvnbbc07k5hfs49m6mflf9wbgzvcbq04jijldvzzv")))

(define-public crate-nostr-kv-0.3.0 (c (n "nostr-kv") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "lmdb-master-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0hbf4czq2i0jwrf1qx3iaqz86738bpb09zpyi2xw8855ddgl42q9")))

(define-public crate-nostr-kv-0.3.1 (c (n "nostr-kv") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "lmdb-master-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0fdy8jwdidrgjbbisal5q529hhhzax3hkgp6li312k8bj36ng4nz")))

