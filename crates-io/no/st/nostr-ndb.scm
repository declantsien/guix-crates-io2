(define-module (crates-io no st nostr-ndb) #:use-module (crates-io))

(define-public crate-nostr-ndb-0.0.0 (c (n "nostr-ndb") (v "0.0.0") (h "0dxycj6fibpa9v33j25fpmjffrk5ka5ybjk230578f5bsy7li7s0")))

(define-public crate-nostr-ndb-0.30.0 (c (n "nostr-ndb") (v "0.30.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "nostr") (r "^0.30") (f (quote ("std"))) (k 0)) (d (n "nostr-database") (r "^0.30") (k 0)) (d (n "nostrdb") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std" "attributes"))) (k 0)))) (h "0440h3zx5nfspi35kmh0pyf1xq26wxmv3iwirq13kgqaaddkq37l") (r "1.70.0")))

(define-public crate-nostr-ndb-0.31.0 (c (n "nostr-ndb") (v "0.31.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "nostr") (r "^0.31") (f (quote ("std"))) (k 0)) (d (n "nostr-database") (r "^0.31") (k 0)) (d (n "nostrdb") (r "^0.3.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std" "attributes"))) (k 0)))) (h "1jm1hhav379yag5g22s456ma46z08bpynmh6j1jl3r3s3rkyzcfh") (r "1.70.0")))

