(define-module (crates-io no st nostrum-macros) #:use-module (crates-io))

(define-public crate-nostrum-macros-0.1.0 (c (n "nostrum-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wnzn228jz59xdswlh5f5s9w9ycxi4b5njg59gf9y5l6nxzvlwn4")))

