(define-module (crates-io no st nostr-rs-plugin) #:use-module (crates-io))

(define-public crate-nostr-rs-plugin-0.1.0 (c (n "nostr-rs-plugin") (v "0.1.0") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "08ldgxg2wwg84kjdy9b64rx32hyskk2k7vjgi0vc01sw71acnm8j")))

(define-public crate-nostr-rs-plugin-0.1.1 (c (n "nostr-rs-plugin") (v "0.1.1") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "106bqhx9jxc1nghd10k15kjv2f35ni3awsp9x88rd28smrmkmf26")))

(define-public crate-nostr-rs-plugin-0.1.2 (c (n "nostr-rs-plugin") (v "0.1.2") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "1f5aq4165s668frh8pz0p40wgklm8h1y3xrxky62689hqwkcpdz0")))

(define-public crate-nostr-rs-plugin-0.1.3 (c (n "nostr-rs-plugin") (v "0.1.3") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "0jpppih5ppy1cr538xx9s69hqfpvgfkjxdc8gdishhszrznz9q1y")))

(define-public crate-nostr-rs-plugin-0.1.4 (c (n "nostr-rs-plugin") (v "0.1.4") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "07dajz29xqfzwiij0l3cr38dxvr4a930qa616ymygma2h9vvy4il")))

(define-public crate-nostr-rs-plugin-0.1.5 (c (n "nostr-rs-plugin") (v "0.1.5") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "0syqf3bxpq5hvhv1py98avc5vm8qjvvmgfp0b1kcn271yd3gnn75")))

(define-public crate-nostr-rs-plugin-0.1.6 (c (n "nostr-rs-plugin") (v "0.1.6") (d (list (d (n "nostr-rs-proto") (r "^0.1.0") (d #t) (k 0)))) (h "1kqlp3dpck3q0v2v93mqvffjlfad4c6rb6zm49ac4a97mi9i7pin")))

