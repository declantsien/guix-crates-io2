(define-module (crates-io no st nostd-cursor) #:use-module (crates-io))

(define-public crate-nostd-cursor-0.1.0 (c (n "nostd-cursor") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (k 0)))) (h "1hh3a0jdgs5h9fp78l7qxn3z25hzg1hj53kdmp8cck95nhv57jhb")))

(define-public crate-nostd-cursor-0.1.1 (c (n "nostd-cursor") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (k 0)))) (h "1yjiz2551mvr32iwrg4wmvwr16d6jmv98pbjzyl7h2f3b9r9xs82")))

(define-public crate-nostd-cursor-0.1.2 (c (n "nostd-cursor") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (o #t) (k 0)))) (h "1yq2zp7mjpqd7xwp4kkbkb23484rbmhjzxylmydi4dvrp8lhvwph") (f (quote (("default" "byteorder")))) (s 2) (e (quote (("byteorder" "dep:byteorder"))))))

