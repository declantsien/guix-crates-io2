(define-module (crates-io no st nostr-nostd) #:use-module (crates-io))

(define-public crate-nostr-nostd-0.0.1 (c (n "nostr-nostd") (v "0.0.1") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("serde" "rand" "recovery" "lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "1n7dsx4m4njhrmlh05n43ldlzbxr0ajmj3pcggqwh9cai90xdzn2")))

(define-public crate-nostr-nostd-0.0.2 (c (n "nostr-nostd") (v "0.0.2") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("serde" "rand" "recovery" "lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "08zzg1c407g526zdwwkr5sz61d483mz2dr01myg30b9siwpk4f46")))

(define-public crate-nostr-nostd-0.0.3 (c (n "nostr-nostd") (v "0.0.3") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("serde" "rand" "recovery" "lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "1zlcj0isaynlfrkdnj8khxjr4sp4vbfs66d85amhms493m4c31nh")))

(define-public crate-nostr-nostd-0.1.0 (c (n "nostr-nostd") (v "0.1.0") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("serde" "rand" "recovery" "lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "1j5y1p83mlziiq4iq3qyhzs0lbbwbrikngwzqbkkx0a66grzw1x0")))

(define-public crate-nostr-nostd-0.2.0 (c (n "nostr-nostd") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "00gjy97i2n37g0q5ijvh7cyskhjgqf3wkhzhw3llqxdbgskrdl6f")))

(define-public crate-nostr-nostd-0.2.1 (c (n "nostr-nostd") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (k 0)) (d (n "heapless") (r "^0.7.14") (k 0)) (d (n "secp256k1") (r "^0.27.0") (f (quote ("lowmemory"))) (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)))) (h "1gqwga209j87dmghgsn59rriifhww7fbb5mn1vcaz4l73fj2ymi8")))

