(define-module (crates-io no st nostbeep) #:use-module (crates-io))

(define-public crate-nostbeep-0.1.0 (c (n "nostbeep") (v "0.1.0") (h "154zny840bd8hf8p1j7cjggr8pdwv5a9i0wayaia5vqpplf1adb9")))

(define-public crate-nostbeep-0.1.1 (c (n "nostbeep") (v "0.1.1") (h "0p2b25b740aj9nl0h8q85c3rpnqj320g88ja8rwf65zk2ffll1kd")))

