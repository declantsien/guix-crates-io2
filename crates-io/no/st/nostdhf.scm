(define-module (crates-io no st nostdhf) #:use-module (crates-io))

(define-public crate-nostdhf-0.1.0 (c (n "nostdhf") (v "0.1.0") (h "0rlcpfwzhq5gdmnkx41lhjnsvfg5b1wm8zc0yxv894qsfm2d2cmk")))

(define-public crate-nostdhf-0.1.1 (c (n "nostdhf") (v "0.1.1") (h "08cs8javddpg9w3m2hhl0wpz9cq6s7mk3vh8rdlyha8k4f4ziw5m")))

