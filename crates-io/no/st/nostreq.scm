(define-module (crates-io no st nostreq) #:use-module (crates-io))

(define-public crate-nostreq-0.1.0 (c (n "nostreq") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1nlmhdnhlr95cargalvhdk4ihgaa79dmn58id19ag92b6b0mk7f2")))

(define-public crate-nostreq-0.1.1 (c (n "nostreq") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0n0nvrga033xg32423yrgcyzbnhd7kb6bbqzvqiibbphsbdc05dp")))

(define-public crate-nostreq-0.1.2 (c (n "nostreq") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0f48v7lbfi16ksbyhb2qww28a013wa5bzzy8clg8ckyjl6cjla8l")))

