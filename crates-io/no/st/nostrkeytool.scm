(define-module (crates-io no st nostrkeytool) #:use-module (crates-io))

(define-public crate-nostrkeytool-0.1.0 (c (n "nostrkeytool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d6rycnp4pxwn28sy0armw7mabgjmxy5rl2vh492qpqhhss0kj2c")))

(define-public crate-nostrkeytool-0.2.0 (c (n "nostrkeytool") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8zslcgqk340x8ffnk4pjf8b71x5bpg386v3nrxzq1zr6g1s07y")))

