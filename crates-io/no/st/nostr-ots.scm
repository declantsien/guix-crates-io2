(define-module (crates-io no st nostr-ots) #:use-module (crates-io))

(define-public crate-nostr-ots-0.1.0 (c (n "nostr-ots") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "opentimestamps") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1qs9mj90y0m9znvlg2xrvkr5bnsbbznrcbkwwa3216365qzpqjmx")))

(define-public crate-nostr-ots-0.1.1 (c (n "nostr-ots") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "opentimestamps") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "069f3mj38hmskdxk9fs709xwykyvjmb7nnr9dvjyfszb1xgbxjpa")))

(define-public crate-nostr-ots-0.2.0 (c (n "nostr-ots") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "opentimestamps") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1qf04hvh7bhmbmk5x5vlw2lg3fk6myd98sq610rg49bg3iwr9w4f")))

(define-public crate-nostr-ots-0.2.1 (c (n "nostr-ots") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.12.0") (d #t) (k 0)) (d (n "opentimestamps") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "19vmgny25blzzll5nv5b8qa58v1gnniyfn8b89i26864l93qd75k")))

