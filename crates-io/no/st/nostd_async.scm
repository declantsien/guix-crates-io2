(define-module (crates-io no st nostd_async) #:use-module (crates-io))

(define-public crate-nostd_async-0.3.1 (c (n "nostd_async") (v "0.3.1") (d (list (d (n "futures-micro") (r "^0.4") (d #t) (k 2)))) (h "13lfsic1p7b4qfwlfbrlg90d09v9r8ysyiygrhb9n8yl1dj14qqf")))

(define-public crate-nostd_async-0.6.0 (c (n "nostd_async") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "futures-micro") (r "^0.5") (d #t) (k 2)))) (h "1sslyr5k8qliyk6qmrf3d5vgkvgs9xihj3d62wp675mhi67c6aq4") (f (quote (("cortex_m" "cortex-m"))))))

(define-public crate-nostd_async-0.6.1 (c (n "nostd_async") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "futures-micro") (r "^0.5") (d #t) (k 2)))) (h "071r35c1rv4gkmb86r9ykk4zyiz23nlr65nl45nwzwiigw8fcigh") (f (quote (("wfe" "cortex-m") ("cortex_m" "cortex-m"))))))

