(define-module (crates-io no cu nocurses_rust) #:use-module (crates-io))

(define-public crate-nocurses_rust-0.1.0 (c (n "nocurses_rust") (v "0.1.0") (h "17rjp3lyc87qnknrim5mq15jhrfxdm8lli5i0sjqb1ll20hkagpm")))

(define-public crate-nocurses_rust-0.2.0 (c (n "nocurses_rust") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26") (f (quote ("filedescriptor"))) (d #t) (k 0)))) (h "10vj55n3hkl096wc29p4r7im4h516winyazy4l88da69vmdlcin0")))

(define-public crate-nocurses_rust-0.2.1 (c (n "nocurses_rust") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26") (f (quote ("filedescriptor"))) (d #t) (k 0)))) (h "1v4qxzjh0cfsbdly30ydib52f6yydvikxm18hnqpdh22s3sqk2xp")))

