(define-module (crates-io no zo nozomi) #:use-module (crates-io))

(define-public crate-nozomi-1.0.0 (c (n "nozomi") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l0z34agcbx2n8ayybh39jz3bvphc0ka1z03m66a7z6swn5wxqbg") (y #t)))

(define-public crate-nozomi-1.0.1 (c (n "nozomi") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jsyfgkvin09ciwg2pz1wqpav29nvyc1gnj9mk89yc2z432spcs8") (y #t)))

(define-public crate-nozomi-1.0.2 (c (n "nozomi") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "091srrkwjsnzafzzpnmgphhf1b61hmd54dy1100qx9djcv04hrr6")))

(define-public crate-nozomi-2.0.0 (c (n "nozomi") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0py0w5cskllw0bjxfw7j856c4kv85q9izjwxnxfmjhmjxkhpagki")))

(define-public crate-nozomi-2.0.1 (c (n "nozomi") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18kpdxdqji5xyzvvikkk0jwxflin5bj2ss640chr9c2wiac383xf")))

