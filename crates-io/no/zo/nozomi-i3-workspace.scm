(define-module (crates-io no zo nozomi-i3-workspace) #:use-module (crates-io))

(define-public crate-nozomi-i3-workspace-0.1.0 (c (n "nozomi-i3-workspace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 0)))) (h "1idf24j50cmk2c6ly9w64rkawh22hh9shvpgpxywrdqb4bf10279")))

