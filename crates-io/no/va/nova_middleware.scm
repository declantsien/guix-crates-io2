(define-module (crates-io no va nova_middleware) #:use-module (crates-io))

(define-public crate-nova_middleware-0.0.1 (c (n "nova_middleware") (v "0.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nova_core") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g31sgariggdgnsdkra38c7dlmllwvkncgxijzdvznvghjvb33v3")))

