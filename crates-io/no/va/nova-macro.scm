(define-module (crates-io no va nova-macro) #:use-module (crates-io))

(define-public crate-nova-macro-0.3.0 (c (n "nova-macro") (v "0.3.0") (d (list (d (n "nova-impl") (r "=0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0afg9kp3rdqb4zb905vc5dwgx4qsb1k80l8rci8mwqc7q1h3lpzs") (f (quote (("sqlx" "nova-impl/sqlx") ("serde" "nova-impl/serde"))))))

(define-public crate-nova-macro-0.3.1 (c (n "nova-macro") (v "0.3.1") (d (list (d (n "nova-impl") (r "=0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q92g83xn23i0m0am623z0g0g0vmn9ymc0c715ka75fy00f3w9r5")))

(define-public crate-nova-macro-0.3.2 (c (n "nova-macro") (v "0.3.2") (d (list (d (n "nova-impl") (r "=0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03pdjgdi4bdwyabpav2bx5l3099qihjrw1c81hipg2sdkwjdkyd7")))

(define-public crate-nova-macro-0.4.0 (c (n "nova-macro") (v "0.4.0") (d (list (d (n "nova-impl") (r "=0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qs0q7x4bjkdgvhjblzx12ybwjrmrshmpycc1cdp77hd78flpwgy")))

(define-public crate-nova-macro-0.5.0 (c (n "nova-macro") (v "0.5.0") (d (list (d (n "nova-impl") (r "=0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jyzalmwsn6mwzskzi873md2jg92hb3w2zsnisf68l6gbqyfbnpj")))

(define-public crate-nova-macro-0.5.1 (c (n "nova-macro") (v "0.5.1") (d (list (d (n "nova-impl") (r "=0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00h691yisjmcbqmwqk5rj5319hp7wbq30d86k9vb2sc50lv9w1ql")))

(define-public crate-nova-macro-0.5.2 (c (n "nova-macro") (v "0.5.2") (d (list (d (n "nova-impl") (r "=0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nvfijfa3yf2rwbw9d2dmsbnraz7lxhdjwx79cwxf3h8bxd05wic")))

(define-public crate-nova-macro-0.5.3 (c (n "nova-macro") (v "0.5.3") (d (list (d (n "nova-impl") (r "=0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0saakgm2gjrf8l8ck5w9iynxw2gladc65f8gc3xapksv9m5pqz98")))

(define-public crate-nova-macro-0.5.4 (c (n "nova-macro") (v "0.5.4") (d (list (d (n "nova-impl") (r "=0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x6hiawrm96rfrs2h6i0pkdfx6kq34pdxn33w71x1ci4hc0ckc85")))

