(define-module (crates-io no va nova-math) #:use-module (crates-io))

(define-public crate-nova-math-0.4.0 (c (n "nova-math") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14pqa463myhbg5klnj3fq87gps7qffiz5y9v8w626di14cb4swdj")))

(define-public crate-nova-math-0.4.1 (c (n "nova-math") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0sb4yn64vf4c0kb57fw0gpm8z5pxvpxj68r2szxb0flb0c46z3hr")))

(define-public crate-nova-math-0.4.2 (c (n "nova-math") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hchzrj4c9r2rpmnws5g8i5il5vzgag8yg2rdyb5895ygdxci8z1")))

(define-public crate-nova-math-0.4.3 (c (n "nova-math") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mkkzbdp5n0jxsv32pld1sn6pwranv2x7c6ah6bgpzhplgmrzgva")))

(define-public crate-nova-math-0.5.0 (c (n "nova-math") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hf3fmmb2c1rxfl1wzp6h1nfa9l03azigqzfgmc24b8g62k357z2")))

