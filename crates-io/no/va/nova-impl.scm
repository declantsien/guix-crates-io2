(define-module (crates-io no va nova-impl) #:use-module (crates-io))

(define-public crate-nova-impl-0.3.0 (c (n "nova-impl") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z102dq717qfb0sgjz5986fqhicf8mjj3564rkpms2crf9gyh0hv") (f (quote (("sqlx") ("serde"))))))

(define-public crate-nova-impl-0.3.1 (c (n "nova-impl") (v "0.3.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wwm24d4bicbb4ndqid23rmhqn8j3r5is577fnshp1kb7y7kfd6h")))

(define-public crate-nova-impl-0.3.2 (c (n "nova-impl") (v "0.3.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z8z4p1f6pjkrp59s0ylclv9fbc42pffcp96kglrmaw2564q1wis")))

(define-public crate-nova-impl-0.4.0 (c (n "nova-impl") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "154kp1wpj0c6dwlq7cckrl319y40q3xz0mbmnbq4bzmcdd3bvxwp")))

(define-public crate-nova-impl-0.5.0 (c (n "nova-impl") (v "0.5.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0asy9fiiq6mg57hva0z9y26szyx6a8k7rc16rh0win20vm6m5fnf")))

(define-public crate-nova-impl-0.5.1 (c (n "nova-impl") (v "0.5.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cp647pvy1m4ra0ds1b6519l9xapacjdccrfl43gxqwhwqcs9yqb")))

(define-public crate-nova-impl-0.5.2 (c (n "nova-impl") (v "0.5.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "173gdz4kpmb5y2i4567cxckf1yykbl1406zavgcd1zgmn533cam1")))

(define-public crate-nova-impl-0.5.3 (c (n "nova-impl") (v "0.5.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nw1al1bj71vca32zn8w025qmjwgc0j5l7q6afzwscp6c61wpa8d")))

(define-public crate-nova-impl-0.5.4 (c (n "nova-impl") (v "0.5.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ymazcyqw6gdf9k2j1y644raql9lspj32cafvjkrsixha1j9y80k")))

