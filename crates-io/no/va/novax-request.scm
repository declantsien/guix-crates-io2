(define-module (crates-io no va novax-request) #:use-module (crates-io))

(define-public crate-novax-request-0.0.1 (c (n "novax-request") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0c4mwfkdwnxzgz4fjaix6gr03d4g9jm29pb1nn0lcw38764z98rm")))

(define-public crate-novax-request-0.0.3 (c (n "novax-request") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1ws4sm8r7nhk18s820yppfzh1r27c67fi98p8z81niq1gngl9n71")))

(define-public crate-novax-request-0.0.4 (c (n "novax-request") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1fk5r7abgh8phhsm9vxziv69cin6qrp7ycx3f7gxmf241cmizj0g")))

(define-public crate-novax-request-0.0.5 (c (n "novax-request") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "17szf4xpcdks7l2vp03jhlydixzzjkm5cavn42m90pjs99xpnyla")))

(define-public crate-novax-request-0.0.6 (c (n "novax-request") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1czhxqmrjjn3lhyil32wiv8lwqaf462q9nrmab5786zf6fj8yljm")))

(define-public crate-novax-request-0.0.7 (c (n "novax-request") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "082w5brbmabqb19k4vws97zyl3q5fd5r2hchcraijvyxma2k0575")))

(define-public crate-novax-request-0.0.8 (c (n "novax-request") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0007wh1jnp4p58nv4ck5djsxblpmhjlm9i6k88g6m6kb47q9f797")))

(define-public crate-novax-request-0.0.9 (c (n "novax-request") (v "0.0.9") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1k2xk3c05la5bff2pfnkfb6pvmsfs8xkk1b5a36n3qj1k2hg002q")))

(define-public crate-novax-request-0.0.10 (c (n "novax-request") (v "0.0.10") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0j7hfsq3a1adwdzvdirv1chj5f1fqvp2010sy7340ki7dyifpr30")))

(define-public crate-novax-request-0.0.11 (c (n "novax-request") (v "0.0.11") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1yvmhmyh687bsp8lplwkb312d5a7sh8p8g0ppf7zjnvskvdvsi0s")))

(define-public crate-novax-request-0.0.12 (c (n "novax-request") (v "0.0.12") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "101fskqr109i9w43yy8dfjlyha5jghi2gm866gxhac4c5a0bk6nx")))

(define-public crate-novax-request-0.0.13 (c (n "novax-request") (v "0.0.13") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1qrrrf668ah8cf4mrmzvnqrp7c68487kbmgh60bsymqy3s9n0a40")))

(define-public crate-novax-request-0.0.14 (c (n "novax-request") (v "0.0.14") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0ycafhp024fkk6wqa00cdmpm1618nn33ng2v72jld957crpkxrm9")))

(define-public crate-novax-request-0.0.15 (c (n "novax-request") (v "0.0.15") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "18lp4i9zqza9lv4rq9q8alcwp1s4wcrmrym34kh1r2q1gpgc751y")))

(define-public crate-novax-request-0.0.16 (c (n "novax-request") (v "0.0.16") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0raj3fhnrrc6cldvbii20dnwa12f54gcp5qss0cmgszgl79f9qv6")))

(define-public crate-novax-request-0.0.17 (c (n "novax-request") (v "0.0.17") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0h4ri2rpm2yar33km71yxrh0mfvfa7q17qm2f07l8lz33zqnm2qp")))

(define-public crate-novax-request-0.0.18 (c (n "novax-request") (v "0.0.18") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "158w8kwvkcr40rk6j93acvmwv5zz5gh1idrcvi5n68vhcnh3wi3h")))

(define-public crate-novax-request-0.0.19 (c (n "novax-request") (v "0.0.19") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1r4swv8sdjkc832i801p124rb7p5xg58zgnawwlzjmfydf887n71")))

(define-public crate-novax-request-0.0.20 (c (n "novax-request") (v "0.0.20") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0ifizmh7k1r8qid57ghwnzg7q32kxrv11m4mqa0j6agk2vyw6xib")))

(define-public crate-novax-request-0.0.21 (c (n "novax-request") (v "0.0.21") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0nxs78mzxpnnnh7xamf65sdpxw59nvhmbmllr46vzcr9rpn92aik")))

(define-public crate-novax-request-0.0.22 (c (n "novax-request") (v "0.0.22") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1li7kn33g4fp6fb3kj9i1n8gj066swx45bi09j2bp48q6gpq052n")))

(define-public crate-novax-request-0.0.23 (c (n "novax-request") (v "0.0.23") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "16gb8l6glw1p79fpahw2as8zf84h7q8rjcykbgn054mnz2ni2x7c")))

(define-public crate-novax-request-0.0.24 (c (n "novax-request") (v "0.0.24") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "13646r1klws6xlq8g4rh89mv2hwp06z2mmz0rf0bn0w0dam10kia")))

(define-public crate-novax-request-0.0.25 (c (n "novax-request") (v "0.0.25") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1d6bzc8jh8m0kh7scy7andwj4nsbnhax8cnkzy8h3sk6im0izimz")))

(define-public crate-novax-request-0.1.0 (c (n "novax-request") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0hd9mchzki9saibicbny9kdz6wiqbrlfaqsnz0n15dfdiyljw7wy")))

(define-public crate-novax-request-0.1.1 (c (n "novax-request") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0rhh65049frs180qqzf0z6nr05n6pp3b1zj3ijxy9mi4acyl6spg")))

(define-public crate-novax-request-0.1.2 (c (n "novax-request") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1g96zwfszn99pc0h47jk3jnqv465c5f4i3sp6d4f447imxc655cv")))

(define-public crate-novax-request-0.1.3 (c (n "novax-request") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0c2dan1bl1jrwjpz40fqj5mihp11da65wdlpx324d2snr5mw122d")))

(define-public crate-novax-request-0.1.4 (c (n "novax-request") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "17jk92sd3i2j5fxizjj7ncnshmm7fc4r6fflaalzihj091hl9a1q")))

(define-public crate-novax-request-0.1.5 (c (n "novax-request") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "16y8sbkpzk4j814f70g99lhn9fy9jm1ndz5yj6ppwabwwsmd49q2")))

(define-public crate-novax-request-0.1.6 (c (n "novax-request") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "00di1vy7k2q9rspp7adhjsdar41mlxvdnmxvyhz68phhfc4216pj")))

