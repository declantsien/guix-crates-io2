(define-module (crates-io no va nova_serde) #:use-module (crates-io))

(define-public crate-nova_serde-0.0.1 (c (n "nova_serde") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "028av90rrrzjgj8wc0q4vivzq6zl29k4anv1zyx4mf0ssqdy4w7r")))

