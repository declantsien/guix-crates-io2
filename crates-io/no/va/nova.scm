(define-module (crates-io no va nova) #:use-module (crates-io))

(define-public crate-nova-0.1.0 (c (n "nova") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0n4capq2ym6wiiwpqqpxamp22hsy84znl5nkdrd5nq6n1w7xrg3w")))

(define-public crate-nova-0.2.0 (c (n "nova") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1kcwcl36ajnfsbn350z5xayswgxr0v5fjrlksnhbzmgqknyic621") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-nova-0.2.1 (c (n "nova") (v "0.2.1") (d (list (d (n "heapless") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0kccw0zq7p8srfl5bgksa8cdz8z161pyl1vfxp7acj2bbb2ib6hh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-nova-0.2.2-pre.0 (c (n "nova") (v "0.2.2-pre.0") (d (list (d (n "heapless") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "02h2qk4lqwqzq4iqgqhywf1j8qwp9q988zl7p2x90slpqfw3pqzf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-nova-0.2.3 (c (n "nova") (v "0.2.3") (d (list (d (n "heapless") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1ck5bzjj97jka4awliz4hrjf9rin70k9akcmaywpkpzj1h7cnk33") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-nova-0.3.0 (c (n "nova") (v "0.3.0") (d (list (d (n "nova-macro") (r "=0.3.0") (d #t) (k 0)))) (h "0gqp64ay9df6a8140wgdzr9bsb31a50hxy7i2fxdrywz8vwyz3s3") (f (quote (("sqlx" "nova-macro/sqlx") ("serde" "nova-macro/serde"))))))

(define-public crate-nova-0.3.1 (c (n "nova") (v "0.3.1") (d (list (d (n "nova-macro") (r "=0.3.1") (d #t) (k 0)))) (h "15fv6b2b1gdzl2675f8bn6psl0v9hksza8dihlr45ib8ircsp2cj")))

(define-public crate-nova-0.3.2 (c (n "nova") (v "0.3.2") (d (list (d (n "nova-macro") (r "=0.3.2") (d #t) (k 0)))) (h "09zrcqhspkq8cwnampz1nhnn6qdcjklg82mbsgj14aq99xz95lcb")))

(define-public crate-nova-0.4.0 (c (n "nova") (v "0.4.0") (d (list (d (n "nova-macro") (r "=0.4.0") (d #t) (k 0)))) (h "1y9h563f19zp7fmfpcpxxs9wbld5iklfjxfkc8dcf4rnnl6ifd62")))

(define-public crate-nova-0.5.0 (c (n "nova") (v "0.5.0") (d (list (d (n "nova-macro") (r "=0.5.0") (d #t) (k 0)))) (h "16c6mrs0dqgbdpkm1118m0g74jhqz8bmlbsny6c8jkykvfch1pjj") (y #t)))

(define-public crate-nova-0.5.1 (c (n "nova") (v "0.5.1") (d (list (d (n "nova-macro") (r "=0.5.1") (d #t) (k 0)))) (h "14bis9a4r2r397f2ks742py7lbk7jhjzkrybg7rnimx7lms15q4d")))

(define-public crate-nova-0.5.2 (c (n "nova") (v "0.5.2") (d (list (d (n "nova-macro") (r "=0.5.2") (d #t) (k 0)))) (h "0y2z9c8qib81l0vpy9xgakn7kv19s15n03072jz98h7pfspkq6bq")))

(define-public crate-nova-0.5.3 (c (n "nova") (v "0.5.3") (d (list (d (n "nova-macro") (r "=0.5.3") (d #t) (k 0)))) (h "1y5rpbk6801x7rlggj8aqy7198max35a355hdx1g7pc905hb52g4")))

(define-public crate-nova-0.5.4 (c (n "nova") (v "0.5.4") (d (list (d (n "nova-macro") (r "=0.5.4") (d #t) (k 0)))) (h "1qrv1am9k2i2l5i7l3ck29g2g8ca8m80ak4rz8pp0wr88d1xb39c")))

