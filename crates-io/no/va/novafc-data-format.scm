(define-module (crates-io no va novafc-data-format) #:use-module (crates-io))

(define-public crate-novafc-data-format-0.1.0 (c (n "novafc-data-format") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rqvxa5405bnsal5ihvqana79f84bv3r6rdq4z39pvsprcmw2fvb")))

