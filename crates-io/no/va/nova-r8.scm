(define-module (crates-io no va nova-r8) #:use-module (crates-io))

(define-public crate-nova-r8-0.1.0 (c (n "nova-r8") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r3cpzwxyk89fg68d1iwwg217y3p4zjkva20raiclclk9bcbv91y") (y #t) (l "nova-r8")))

(define-public crate-nova-r8-0.1.1 (c (n "nova-r8") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11k0cv9698rq1li7j44ir6mzgrygx1adzcq7n85shn9ws8wms772") (l "nova-r8")))

(define-public crate-nova-r8-0.2.0 (c (n "nova-r8") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12wwa2bjjlf5027l9xzi98iz0pkk9mm5ylnby3yim00zaw8qfvrg") (l "nova-r8")))

