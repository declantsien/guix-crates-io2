(define-module (crates-io no va novapolis) #:use-module (crates-io))

(define-public crate-novapolis-0.1.0 (c (n "novapolis") (v "0.1.0") (h "10ar475gv6nldng4qwaajyqm7dl9pgqfm330ydcl03jfxm1g782d") (y #t)))

(define-public crate-novapolis-0.1.1 (c (n "novapolis") (v "0.1.1") (h "0g12xnk5p17pka9vip8l3di3kscpbc085r5d0ckyygja6kgvd7y8") (y #t)))

(define-public crate-novapolis-0.1.2 (c (n "novapolis") (v "0.1.2") (h "027qdq8dqb28mva0hq2gbv2crsss6j7999z423mnr9aj9n1zf128") (y #t)))

