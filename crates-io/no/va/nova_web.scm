(define-module (crates-io no va nova_web) #:use-module (crates-io))

(define-public crate-nova_web-0.0.1 (c (n "nova_web") (v "0.0.1") (d (list (d (n "nova_core") (r "^0.0.1") (d #t) (k 0)) (d (n "nova_middleware") (r "^0.0.1") (d #t) (k 0)) (d (n "nova_router") (r "^0.0.1") (d #t) (k 0)) (d (n "nova_serde") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("full"))) (d #t) (k 0)))) (h "1av1qvx1r7p2r2x6yq216fvnqnxaz0li7cybgj48jf1c98zs3k0j") (s 2) (e (quote (("serde" "dep:nova_serde")))) (r "1.63")))

