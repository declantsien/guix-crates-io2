(define-module (crates-io no va novation_remote_25sl) #:use-module (crates-io))

(define-public crate-novation_remote_25sl-0.1.0 (c (n "novation_remote_25sl") (v "0.1.0") (d (list (d (n "midir") (r "^0.3") (d #t) (k 2)) (d (n "pitch_calc") (r "^0.11") (d #t) (k 0)))) (h "0fzv8213mylrwcnxf945xsidqf2wnh7ixr73g0gkhcf4d9rxxnlv")))

(define-public crate-novation_remote_25sl-0.1.1 (c (n "novation_remote_25sl") (v "0.1.1") (d (list (d (n "midir") (r "^0.3") (d #t) (k 2)) (d (n "pitch_calc") (r "^0.11") (d #t) (k 0)))) (h "184dy1lc4cvx31ml9ljagzmnyf5v4h3v71780bqjkvb1jhh9v8dv")))

