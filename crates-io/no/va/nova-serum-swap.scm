(define-module (crates-io no va nova-serum-swap) #:use-module (crates-io))

(define-public crate-nova-serum-swap-0.1.1 (c (n "nova-serum-swap") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (f (quote ("dex" "devnet"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0d1nx0zr8xhdf44fxl5ac8i1fls13b30q4sfw7i3g0rnqwlp8ssm") (f (quote (("no-idl") ("no-entrypoint") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-nova-serum-swap-0.4.1 (c (n "nova-serum-swap") (v "0.4.1") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.8.5") (d #t) (k 0)))) (h "0j0dnpn21c2a1573mxs6ra67ckk79ny16r9gvpi2n7km71gsygb3") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-nova-serum-swap-0.4.2 (c (n "nova-serum-swap") (v "0.4.2") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0qq2iim3vz1qzd8xb0q66sm2rijzph9ikx5v2xzs1lg2qnglva6x") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

