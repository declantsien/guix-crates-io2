(define-module (crates-io no va nova_core) #:use-module (crates-io))

(define-public crate-nova_core-0.0.1 (c (n "nova_core") (v "0.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nova_serde") (r "^0.0.1") (d #t) (k 0)))) (h "12f5qpz8ca7k8m4cigqprgazs710xiq9wj34p44ryh7dqzksr3va")))

