(define-module (crates-io am e2 ame2020) #:use-module (crates-io))

(define-public crate-ame2020-0.1.0 (c (n "ame2020") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1sjh7k3i5qhfcmxcm781j6gxmlkg647sb1cr2d8d9g5gz646p0b3") (s 2) (e (quote (("serde" "dep:serde" "arrayvec/serde"))))))

(define-public crate-ame2020-0.1.1 (c (n "ame2020") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "10x0rycd0wqsb0wwabq522d7cmppymmmjjwl3mjblbzzard989js") (s 2) (e (quote (("serde" "dep:serde" "arrayvec/serde"))))))

