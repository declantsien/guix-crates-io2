(define-module (crates-io am at amath) #:use-module (crates-io))

(define-public crate-amath-0.1.0 (c (n "amath") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.15.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1wv8xgcqqvjisyz6yga1d9hj9wdd4dd04i28v5sfvxbykm99l35x") (y #t)))

