(define-module (crates-io am ig amigo) #:use-module (crates-io))

(define-public crate-amigo-0.1.0 (c (n "amigo") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "11k51sy8lzgb3i530q627zzbymxzi9j9fhcppsm3rxfpy9cspjsk")))

(define-public crate-amigo-0.2.0 (c (n "amigo") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1z2i5cssicpkbjrgy04bhlh04yqg866ykyxbv4csg3rqg7mxkp8x")))

(define-public crate-amigo-0.2.1 (c (n "amigo") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ys88q8a0drspvwwimk5asb6bsc2xdb425rbkyczk7ba5cyyn7l6")))

(define-public crate-amigo-0.2.2 (c (n "amigo") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16m56l2a92zb10sn4fpd6067vls3kih9a2disprw3l9cxdlkqd2m")))

(define-public crate-amigo-0.3.0 (c (n "amigo") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1yv4fl7mrsj8a929a438692k225vgqz0vjrnqaj48acw0v4xgm9a")))

(define-public crate-amigo-0.3.1 (c (n "amigo") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0b025pxb08san2xj2jc65rki7vslilm6jxx71bagp5n05rx5nyn3")))

