(define-module (crates-io am ic amicola) #:use-module (crates-io))

(define-public crate-amicola-0.1.0 (c (n "amicola") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.3.1") (d #t) (k 2)))) (h "1d8iw0p4ll0g3sf507dx7isk9nxkb4k8383dz2qdp47mc087fir7") (f (quote (("release_max_level_off"))))))

