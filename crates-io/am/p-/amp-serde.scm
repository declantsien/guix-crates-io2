(define-module (crates-io am p- amp-serde) #:use-module (crates-io))

(define-public crate-amp-serde-0.1.0 (c (n "amp-serde") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cfsawqhqf5n8hzin0grxipqnafvp27ljar9v5g1qbbq9ay5025j")))

