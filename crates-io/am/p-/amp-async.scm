(define-module (crates-io am p- amp-async) #:use-module (crates-io))

(define-public crate-amp-async-0.1.0 (c (n "amp-async") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 0)))) (h "1v6q45xj9z6yn5v9ygcvp989rcpbz8gwm4lcap30g32g6p2vv750")))

(define-public crate-amp-async-0.1.2 (c (n "amp-async") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "io-util" "signal" "sync" "stream" "rt-util" "io-std" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "1l37mr3x63dzli3041nhdlq1jgasdbhaagwfj3ra63w63ymk2g8p")))

(define-public crate-amp-async-0.1.3 (c (n "amp-async") (v "0.1.3") (d (list (d (n "amp-serde") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "io-util" "signal" "sync" "stream" "rt-util" "io-std" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "16ym9fzanf4k5v13zw0sy5w7v4bsczjpqpj5wx0jvfjkvnknw37n")))

