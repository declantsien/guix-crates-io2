(define-module (crates-io am in amino) #:use-module (crates-io))

(define-public crate-amino-0.1.1 (c (n "amino") (v "0.1.1") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)))) (h "1rxkh01fq2kdh9iwf4nj5mqbj7155wsczhfk9sa0i0aacxl7y2cm")))

(define-public crate-amino-0.1.2 (c (n "amino") (v "0.1.2") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1llrm70vnx0vnscf686nvm364x1hpsn03zwvgdpjc43n3c46a4vw")))

(define-public crate-amino-0.1.3 (c (n "amino") (v "0.1.3") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1s5ahk45w8cs2gjiklyh999ajjlnq6z5ya4zazjjf0jkayn6p1ay")))

(define-public crate-amino-0.1.4 (c (n "amino") (v "0.1.4") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1hbc4k045pzq0m76isad1yjga71294raz4lblwm8cd07m3js3ryb")))

(define-public crate-amino-0.1.5 (c (n "amino") (v "0.1.5") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "16xiiz5h3w2lb9bby79lqc1vg9k76829wmhbhca2y2bhilaj37hr")))

(define-public crate-amino-0.1.6 (c (n "amino") (v "0.1.6") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1a3h4h29bdsmff9i3l5y494c7c0v5k00xj2jm8sl0n80d8gmqlcx")))

(define-public crate-amino-0.1.7 (c (n "amino") (v "0.1.7") (d (list (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0v4ycb2cndf9yqqlzxwdncaky1fam2q6bl6hi49wk6ahg4bd3mjz")))

