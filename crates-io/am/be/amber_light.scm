(define-module (crates-io am be amber_light) #:use-module (crates-io))

(define-public crate-amber_light-0.1.0 (c (n "amber_light") (v "0.1.0") (d (list (d (n "canvy") (r "^0.2.0") (d #t) (k 0)) (d (n "canvy") (r "^0.2.5") (d #t) (k 2)) (d (n "colorgrad") (r "^0.6.2") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0yis3igjq62cvrylj0w39icaygx1rnnylvnhzbldbinkmnfwbs9q")))

