(define-module (crates-io am be amberdata_api) #:use-module (crates-io))

(define-public crate-amberdata_api-1.0.0 (c (n "amberdata_api") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "18wcc6rlxhmip9khgwy27ha9pl4m761s04658nv9afxabxss8gcp") (y #t)))

