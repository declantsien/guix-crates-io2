(define-module (crates-io am ed ameda) #:use-module (crates-io))

(define-public crate-ameda-0.1.0 (c (n "ameda") (v "0.1.0") (h "12am4a5al0wp7gydc35232wr252n263zy89i4xahcdrwklisycwf")))

(define-public crate-ameda-0.1.1 (c (n "ameda") (v "0.1.1") (h "0zmybaa2gw3mq2h7kyxhbdfwwvaq80pkjpkqvswikilv0fr2l7h0")))

