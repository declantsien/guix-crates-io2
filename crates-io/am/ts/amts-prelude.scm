(define-module (crates-io am ts amts-prelude) #:use-module (crates-io))

(define-public crate-amts-prelude-0.1.0 (c (n "amts-prelude") (v "0.1.0") (d (list (d (n "bson") (r "^1.1") (d #t) (k 0)) (d (n "nats") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02za8awyqvb4qd0vqg3d49l7bvc5xalkbasr5vqdancnlr9kzyb2")))

(define-public crate-amts-prelude-0.1.1 (c (n "amts-prelude") (v "0.1.1") (d (list (d (n "bson") (r "^1.1") (d #t) (k 0)) (d (n "nats") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "083cw8zqr175437285igmhi78kbgyh31bi551nsizx7iwkq66v51")))

(define-public crate-amts-prelude-0.1.2 (c (n "amts-prelude") (v "0.1.2") (d (list (d (n "bson") (r "^1.1") (d #t) (k 0)) (d (n "nats") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12s58rpi8xsh490gp52gcbfqlw0yhw6l2av01a6a1rj48m41hik2")))

(define-public crate-amts-prelude-0.2.2 (c (n "amts-prelude") (v "0.2.2") (d (list (d (n "nats") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vxikniirv29vn7v6kyiwk5cww2zh0yvcnys4fzz5fydwl3hd6hj")))

(define-public crate-amts-prelude-0.2.3 (c (n "amts-prelude") (v "0.2.3") (d (list (d (n "nats") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "195pq289kpbm9h8fhz0427pmh3mgk52ci9pyp32lhb0zq6zlb4wm")))

(define-public crate-amts-prelude-0.2.4 (c (n "amts-prelude") (v "0.2.4") (d (list (d (n "nats") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0w17ng0vn4gnd2kgqh8650jipyidwy153bhbpia9c0iwl8xk6blb")))

