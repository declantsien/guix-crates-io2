(define-module (crates-io am #{23}# am2320) #:use-module (crates-io))

(define-public crate-am2320-0.1.0 (c (n "am2320") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11") (f (quote ("hal"))) (d #t) (k 2)))) (h "0yrdf5y35h4byggvjg4fcmh0nr0hhss1ss16jckjkj8lifc9q6s9")))

(define-public crate-am2320-0.1.1 (c (n "am2320") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0m06p4yyi8ra0im290idqhy1grhrzvcrv3v6pwrjs185jkxzq5qx")))

(define-public crate-am2320-0.2.0 (c (n "am2320") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1izr9agnkdy6r3c92sdvcp1x7il733cxd7lcqzzymmsp7ww763ww")))

