(define-module (crates-io am et amethyst_ecs) #:use-module (crates-io))

(define-public crate-amethyst_ecs-0.1.0 (c (n "amethyst_ecs") (v "0.1.0") (h "1if8nmr6d84g3xa2l26wawhfs24r9022gq4jx0zz7qwmnarzyx6h")))

(define-public crate-amethyst_ecs-0.1.1 (c (n "amethyst_ecs") (v "0.1.1") (h "045avbyhq5a9bi6567knwywadzm5myzh5cy1x1viwnyqzsa0sg2c")))

