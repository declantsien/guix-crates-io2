(define-module (crates-io am et amethyst_cli) #:use-module (crates-io))

(define-public crate-amethyst_cli-0.1.0 (c (n "amethyst_cli") (v "0.1.0") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (d #t) (k 0)))) (h "00nyvdhj2p4b1kgaxq0aj70nbr99wkqvdl0qabnxqclham39a39q")))

(define-public crate-amethyst_cli-0.1.1 (c (n "amethyst_cli") (v "0.1.1") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (k 0)))) (h "1yvnpapbq0lclsl9x2ddvhfdfgmya1844k5f54qb0y8llqwv5xvw")))

(define-public crate-amethyst_cli-0.1.2 (c (n "amethyst_cli") (v "0.1.2") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (k 0)))) (h "16ay35g06wq8ndhk9wz6d69nprm0rvigbab7r90gya7ln0q1whcc")))

(define-public crate-amethyst_cli-0.1.3 (c (n "amethyst_cli") (v "0.1.3") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (k 0)))) (h "1yvzy80x22w8xnp3qm1dazip4ymk6a131aish742ah0lhm7h5iak")))

(define-public crate-amethyst_cli-0.1.4 (c (n "amethyst_cli") (v "0.1.4") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (k 0)))) (h "067ka439bbza12j8lrfmanyb3am0dgjik1sfbk3ij71l1j6srzfr")))

(define-public crate-amethyst_cli-0.1.5 (c (n "amethyst_cli") (v "0.1.5") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "zip") (r "^0.1.15") (k 0)))) (h "0vr1c30jw99bfx75gw4g8dzdaqayyd2lgy8aapi58af49ikrf3ls")))

