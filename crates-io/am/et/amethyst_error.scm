(define-module (crates-io am et amethyst_error) #:use-module (crates-io))

(define-public crate-amethyst_error-0.1.0 (c (n "amethyst_error") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "1x5fhbfdnblfk59nqbkx7g4wpb110aazj134wdzwd9k23kws1bgk")))

(define-public crate-amethyst_error-0.2.0 (c (n "amethyst_error") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "0mwz43lnd758v6pch75w95kq092gl5fyjvr3s9bm1fggxiai8s52")))

(define-public crate-amethyst_error-0.3.0 (c (n "amethyst_error") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "0injcbs8aiqavvzd3xrcnq3zyklaq8nsc06zynya6rg0h5nzgv7y")))

(define-public crate-amethyst_error-0.4.0 (c (n "amethyst_error") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "0cznr5i3fc8sv1wh4qbffcdsndsw6pi096ih27n03b8vbvyziv75")))

(define-public crate-amethyst_error-0.5.0 (c (n "amethyst_error") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)))) (h "1b2xxl76nwn3bz1v6c4ljxklv5c35xsygf3p85fmycpmy3w540q7")))

(define-public crate-amethyst_error-0.5.1 (c (n "amethyst_error") (v "0.5.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)))) (h "1hb838yr87cyw8hydisg53cnbyl4in9l0bxcqc27644hykk2968x")))

(define-public crate-amethyst_error-0.15.2 (c (n "amethyst_error") (v "0.15.2") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)))) (h "0b3w24znwavk7f6my7i5bz4ffibjqskh85lk6a1ryjrzir9qn0b1") (y #t)))

(define-public crate-amethyst_error-0.15.3 (c (n "amethyst_error") (v "0.15.3") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)))) (h "1f5fdy5yq86r9zg2nixbya9ay1393n0818rqd7rykih109j07isv")))

