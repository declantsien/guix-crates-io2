(define-module (crates-io am et amethyst-inspector-derive) #:use-module (crates-io))

(define-public crate-amethyst-inspector-derive-0.1.0 (c (n "amethyst-inspector-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10knhw3w56qjhnhhsv9kv6j1kdig7qm6hqfmf2pd93dlhyna82qf")))

(define-public crate-amethyst-inspector-derive-0.2.0 (c (n "amethyst-inspector-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "120lsq6nkbrs1sbvgbrjmrvs9l8akip4x7xnv1i1lzswvcy2kl1p")))

