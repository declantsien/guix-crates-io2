(define-module (crates-io am et amethyst_engine) #:use-module (crates-io))

(define-public crate-amethyst_engine-0.2.0 (c (n "amethyst_engine") (v "0.2.0") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1lp9bmf8c7b432pwafq59wx6c091a8a786hwk84mhv4m0r6yapd1") (y #t)))

(define-public crate-amethyst_engine-0.2.1 (c (n "amethyst_engine") (v "0.2.1") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1qi1mqkksnhdpriaa4k19qi4gz32h08j2ll8i6m1vrby7rfil29w")))

(define-public crate-amethyst_engine-0.3.0 (c (n "amethyst_engine") (v "0.3.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07y53lm7ll4mjv152ja9bdzn3ijr4xl9js03cl0gd0d69w13wadv")))

(define-public crate-amethyst_engine-0.3.1 (c (n "amethyst_engine") (v "0.3.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1r9cpr2bvnmkz4bhbfc4aj69fri2b82l6zv0f79gdnvbf5kn7rq3")))

