(define-module (crates-io am et amethyst_physics) #:use-module (crates-io))

(define-public crate-amethyst_physics-0.0.1 (c (n "amethyst_physics") (v "0.0.1") (d (list (d (n "amethyst_core") (r "^0.8.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "06zq7y2dn0vcmj29j5srb7xycv0ag0w1a5v187b3ibjkg9ba7fqw")))

(define-public crate-amethyst_physics-0.1.0 (c (n "amethyst_physics") (v "0.1.0") (d (list (d (n "amethyst_core") (r "^0.8.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0ik860nx10gj05f86c3day83by0i0afkgbgmjzlfgn01x42cbqz0")))

(define-public crate-amethyst_physics-0.1.1 (c (n "amethyst_physics") (v "0.1.1") (d (list (d (n "amethyst_core") (r "^0.8.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0zdpsqbqnv1md3dh6nxa5nghmvsr2zvscrz994hyqm1s1f4cjagf")))

(define-public crate-amethyst_physics-0.2.0 (c (n "amethyst_physics") (v "0.2.0") (d (list (d (n "amethyst_core") (r "^0.10.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1nmrjhqxi7mwn6v8pgq04s27p034ydaqy3bakkavbfc3q25crc4k")))

