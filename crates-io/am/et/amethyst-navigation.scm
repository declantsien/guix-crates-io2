(define-module (crates-io am et amethyst-navigation) #:use-module (crates-io))

(define-public crate-amethyst-navigation-0.1.0 (c (n "amethyst-navigation") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.12") (f (quote ("vulkan"))) (d #t) (k 2)) (d (n "amethyst_core") (r "^0.7") (d #t) (k 0)) (d (n "oxygengine-navigation") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.19.2") (d #t) (k 2)))) (h "14azv41l81ppba2vg6wa53pv298dawjk09x652rsmjjjdjhqsafk") (f (quote (("parallel" "oxygengine-navigation/parallel"))))))

