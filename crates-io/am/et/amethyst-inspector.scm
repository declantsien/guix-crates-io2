(define-module (crates-io am et amethyst-inspector) #:use-module (crates-io))

(define-public crate-amethyst-inspector-0.1.0 (c (n "amethyst-inspector") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.13") (d #t) (k 0)) (d (n "amethyst-imgui") (r "^0.5") (d #t) (k 0)) (d (n "amethyst-inspector-derive") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "specs-derive") (r "^0.4") (d #t) (k 0)))) (h "0csq15wbcaf8v9hgv7m3695p9z3pixn081q6zsbyyk6j664c3il4") (f (quote (("saveload" "amethyst/saveload") ("default" "amethyst/vulkan"))))))

(define-public crate-amethyst-inspector-0.2.0 (c (n "amethyst-inspector") (v "0.2.0") (d (list (d (n "amethyst") (r "^0.13") (d #t) (k 0)) (d (n "amethyst-imgui") (r "^0.5") (d #t) (k 0)) (d (n "amethyst-inspector-derive") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0rx48b0m60hhp3kgwayn1s877y9xbzzrm2270m3q3fyzdd6833p4")))

(define-public crate-amethyst-inspector-0.3.0 (c (n "amethyst-inspector") (v "0.3.0") (d (list (d (n "amethyst") (r "^0.13") (d #t) (k 0)) (d (n "amethyst-imgui") (r "^0.6") (d #t) (k 0)) (d (n "amethyst-inspector-derive") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0pbp23i5sk9ggx8zxacaw2m73jsicizmbwa9ingk42jmr5w8ivrw")))

