(define-module (crates-io am et amethyst-console) #:use-module (crates-io))

(define-public crate-amethyst-console-0.1.0 (c (n "amethyst-console") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "amethyst-imgui") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "cvar") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0baa576p21pqbg6kwlzyf7yqkm6qj4ar99mj1kd9mg3y71gj9pa9") (f (quote (("amethyst-system" "amethyst" "amethyst-imgui"))))))

