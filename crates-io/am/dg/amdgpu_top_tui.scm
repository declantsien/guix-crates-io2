(define-module (crates-io am dg amdgpu_top_tui) #:use-module (crates-io))

(define-public crate-amdgpu_top_tui-0.2.0 (c (n "amdgpu_top_tui") (v "0.2.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.2.0") (d #t) (k 0)))) (h "0g8qshxn7xwcqx28gx73p2fbwjbcix3xs0f4hxa1j8fs7d9wjgbl")))

(define-public crate-amdgpu_top_tui-0.2.1 (c (n "amdgpu_top_tui") (v "0.2.1") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.2.1") (d #t) (k 0)))) (h "172cj9qxlwzrrmqs2hi43xljnd946l8jgz4bajw1g49iqqdyf97b")))

(define-public crate-amdgpu_top_tui-0.2.2 (c (n "amdgpu_top_tui") (v "0.2.2") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.2.2") (d #t) (k 0)))) (h "02wkbckd5mi8hf25livadkh01j6pih3rkhgkp5gd2gjkm2jsvfci")))

(define-public crate-amdgpu_top_tui-0.2.3 (c (n "amdgpu_top_tui") (v "0.2.3") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.2.3") (d #t) (k 0)))) (h "1kfs8mnfshqlbjv4qh0ha5lrv7im2bqqq9x17xn1cdcbfwb4wx9c")))

(define-public crate-amdgpu_top_tui-0.3.0 (c (n "amdgpu_top_tui") (v "0.3.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.3.0") (d #t) (k 0)))) (h "0wwjjz39lhw95mh04g3p5cyg1k1gc5phz917ba4rriii0fyljxvd")))

(define-public crate-amdgpu_top_tui-0.3.1 (c (n "amdgpu_top_tui") (v "0.3.1") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.3.1") (d #t) (k 0)))) (h "11dwwbgcllgqf86hwmpscmxnbn8whyphxkidzjns8fmj8byw616m")))

(define-public crate-amdgpu_top_tui-0.4.0 (c (n "amdgpu_top_tui") (v "0.4.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.4.0") (d #t) (k 0)))) (h "1wd1f14nnxbjyxcvdxw801b07xsbdvggmgrik7v7innpjfinc093")))

(define-public crate-amdgpu_top_tui-0.5.0 (c (n "amdgpu_top_tui") (v "0.5.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.5.0") (d #t) (k 0)))) (h "1k59bix8rzr8h37sdypbjipfb2w3wc0nlqmf2f0m1cbqywmir193")))

(define-public crate-amdgpu_top_tui-0.6.0 (c (n "amdgpu_top_tui") (v "0.6.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.6.0") (d #t) (k 0)))) (h "1wjrrcjrbkic998da9fvnz5r55g9r1ji4iahbkad1zgjl9633xlj")))

(define-public crate-amdgpu_top_tui-0.6.1 (c (n "amdgpu_top_tui") (v "0.6.1") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.6.1") (d #t) (k 0)))) (h "1jnl72vjfl6v8md0hix1fnjxiq0jkjipjqhgr8hh63qm4gzz7c53")))

(define-public crate-amdgpu_top_tui-0.7.0 (c (n "amdgpu_top_tui") (v "0.7.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.7.0") (d #t) (k 0)))) (h "1cpd5pz4l1mv86p21lj4lc1m7wip6ww7w47qsqqdiaxr2da8rb25")))

(define-public crate-amdgpu_top_tui-0.8.0 (c (n "amdgpu_top_tui") (v "0.8.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.8.0") (d #t) (k 0)))) (h "0xrskzpivzr745rliag0jnznc992i7xlwl38wn3dv3g898glfi3c")))

(define-public crate-amdgpu_top_tui-0.8.2 (c (n "amdgpu_top_tui") (v "0.8.2") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.8.2") (d #t) (k 0)))) (h "0gk2wrikz35c4gh23wka9mlb931zjzwxmf52nwxgjlxjzgvn6cyz")))

(define-public crate-amdgpu_top_tui-0.8.3 (c (n "amdgpu_top_tui") (v "0.8.3") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.8.3") (d #t) (k 0)))) (h "1ljpqz1r97h3y8kbnplfkk9lxs5197pk3na1s0xpf1rhybmy0fhv")))

(define-public crate-amdgpu_top_tui-0.8.5 (c (n "amdgpu_top_tui") (v "0.8.5") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "libamdgpu_top") (r "^0.8.5") (d #t) (k 0)))) (h "1jh11d2b93qg2s4kbrc1b0hmf5pj3xd0f08inbz38wj2hknpndc3")))

