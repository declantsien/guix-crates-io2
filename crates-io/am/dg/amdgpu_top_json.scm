(define-module (crates-io am dg amdgpu_top_json) #:use-module (crates-io))

(define-public crate-amdgpu_top_json-0.2.0 (c (n "amdgpu_top_json") (v "0.2.0") (d (list (d (n "libamdgpu_top") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1sq7637drgp04rgdjcilcpx2hhrvf3jmlb7n64rdpmcij6sg1hm1")))

(define-public crate-amdgpu_top_json-0.2.1 (c (n "amdgpu_top_json") (v "0.2.1") (d (list (d (n "libamdgpu_top") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0rab5yyay9nqrxmfb51i57lfvq40sjs0llqk6zzbcdiaib3aaw7r")))

(define-public crate-amdgpu_top_json-0.2.2 (c (n "amdgpu_top_json") (v "0.2.2") (d (list (d (n "libamdgpu_top") (r "^0.2.2") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "190jp5wfj688hgw61l0g00ggwv22pf0azb8ba0j080wxvpgvnm53")))

(define-public crate-amdgpu_top_json-0.2.3 (c (n "amdgpu_top_json") (v "0.2.3") (d (list (d (n "libamdgpu_top") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0fpvm90x3hpf6fwrxr5aq9dy5hzzgryyfyv2vhlnv1p5kpvcbg38")))

(define-public crate-amdgpu_top_json-0.3.0 (c (n "amdgpu_top_json") (v "0.3.0") (d (list (d (n "libamdgpu_top") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1705bv09rw8ahfbp22jr1a3cdp0iryq8mfz9p3bfrr9ii25j15r3")))

(define-public crate-amdgpu_top_json-0.3.1 (c (n "amdgpu_top_json") (v "0.3.1") (d (list (d (n "libamdgpu_top") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0cg72c9hk4pg3qdjb4rdqzdg2ca8d0p71dvyw61vykdn0n199d5i")))

(define-public crate-amdgpu_top_json-0.4.0 (c (n "amdgpu_top_json") (v "0.4.0") (d (list (d (n "libamdgpu_top") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "058cwwi6mmacmmla2hjy5w205p744gm0jvg0vfpbcnycwfvykkvq")))

(define-public crate-amdgpu_top_json-0.5.0 (c (n "amdgpu_top_json") (v "0.5.0") (d (list (d (n "libamdgpu_top") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1lnghiz4r1klzm2am2m3231vlasymzwr56lvdpizbn9lfpbpl6rp")))

(define-public crate-amdgpu_top_json-0.6.0 (c (n "amdgpu_top_json") (v "0.6.0") (d (list (d (n "libamdgpu_top") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "03ww80c5cf436544aw3i7dhldz4l0kylm349fp5jpj3x8f7dfhfr")))

(define-public crate-amdgpu_top_json-0.6.1 (c (n "amdgpu_top_json") (v "0.6.1") (d (list (d (n "libamdgpu_top") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1032mqg2ap63zk8cnxh43qnam4594acg5x9l5v58iwpyky5f0fq9")))

(define-public crate-amdgpu_top_json-0.7.0 (c (n "amdgpu_top_json") (v "0.7.0") (d (list (d (n "libamdgpu_top") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "19djlyvnxn11l4k9x0mizcz58b7im6psrvxx95zixxbpirlzh0cf")))

(define-public crate-amdgpu_top_json-0.8.0 (c (n "amdgpu_top_json") (v "0.8.0") (d (list (d (n "libamdgpu_top") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1m6231lb8q11vcfmgpcn5hcz7894bjqd686xxahlwy861awgczcp")))

(define-public crate-amdgpu_top_json-0.8.2 (c (n "amdgpu_top_json") (v "0.8.2") (d (list (d (n "libamdgpu_top") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "164b8bhdn5g69qa68fk57zf46v5aii7yi9zv7ilmsr3a9x4izyjx")))

(define-public crate-amdgpu_top_json-0.8.3 (c (n "amdgpu_top_json") (v "0.8.3") (d (list (d (n "libamdgpu_top") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0mhhl4cvi7mpjihsf961dsqzxf8zx1nkz4iv97kwnn164bb7s31q")))

(define-public crate-amdgpu_top_json-0.8.5 (c (n "amdgpu_top_json") (v "0.8.5") (d (list (d (n "libamdgpu_top") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0264hwp19h224ph7kwg2nljmyl2bblrvxm7c0bpf4zd9lpqkmkrq")))

