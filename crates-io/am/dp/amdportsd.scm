(define-module (crates-io am dp amdportsd) #:use-module (crates-io))

(define-public crate-amdportsd-0.1.0 (c (n "amdportsd") (v "0.1.0") (d (list (d (n "amdgpu") (r "^1") (f (quote ("gui-helper"))) (d #t) (k 0)) (d (n "eyra") (r "^0.16.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ron") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "09snqwqwvv2dv9l7psfidfpwrvrizyd9s10y8cbszljmr6ycsg0k")))

