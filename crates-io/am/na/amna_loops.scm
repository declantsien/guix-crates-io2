(define-module (crates-io am na amna_loops) #:use-module (crates-io))

(define-public crate-amna_loops-0.1.0 (c (n "amna_loops") (v "0.1.0") (h "11z575rq63iy19h92r3zjq83nm4sn66vfzmbidzs0x66inx15kb9")))

(define-public crate-amna_loops-0.1.1 (c (n "amna_loops") (v "0.1.1") (h "1crr5rzj97qm1c5k8gn84lchs342ramzm7613kv57vppzdmdmlm0")))

