(define-module (crates-io am ap amap) #:use-module (crates-io))

(define-public crate-amap-0.1.0 (c (n "amap") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1v92hz07cyb34fdwc8h6pm641wlfl6xhbyksyawk9wf579pahl77")))

(define-public crate-amap-0.1.1 (c (n "amap") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "09h0g3pwg7a2y9a21024xry4q58863hp7qf6f7xacaih7a071g4i")))

(define-public crate-amap-0.1.2 (c (n "amap") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0v43n5p9fy63n5xlcdwhn3dpf7yqspa1n1f9x6bzs1zkrzn7mzih")))

