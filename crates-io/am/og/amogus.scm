(define-module (crates-io am og amogus) #:use-module (crates-io))

(define-public crate-amogus-0.1.0 (c (n "amogus") (v "0.1.0") (h "0k8m8qs63wky9zpyf8s1afvxawhh41vdb58dnad53w31vy1adb81")))

(define-public crate-amogus-0.2.0 (c (n "amogus") (v "0.2.0") (h "1d3d3rgpimspq9bgbrmhfzyvv1hjcl3jasw4h7h8y6x9f84kdp1v")))

