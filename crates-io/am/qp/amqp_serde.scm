(define-module (crates-io am qp amqp_serde) #:use-module (crates-io))

(define-public crate-amqp_serde-0.1.0 (c (n "amqp_serde") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lk2hcs6jycdh93yx978m8nrma7vq8wdw044z7gfv0lv7g4kcqjs")))

(define-public crate-amqp_serde-0.1.1 (c (n "amqp_serde") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2ixdyc7dxnpmvwpy1awwcwdydplghp1mmw5j3iph6apblg905q")))

(define-public crate-amqp_serde-0.2.0 (c (n "amqp_serde") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0frqzxsqnq74rq3628rn446ia69cf4jnw3zkjd3z2rx9srkqrynj") (r "1.56")))

(define-public crate-amqp_serde-0.2.1 (c (n "amqp_serde") (v "0.2.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pg8d2bb337cprjwbv5fkg5fcwb054ya4vkzd47nkz6cyy1n117a") (r "1.56")))

(define-public crate-amqp_serde-0.3.0 (c (n "amqp_serde") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0044mydhfb0k8pyj0qpiln8kgzh7mhg1dgczg10p7pn5f58d9dv3") (r "1.56")))

(define-public crate-amqp_serde-0.4.0 (c (n "amqp_serde") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10g35x87c2awx8qrj60lrlgih6r4lc4rcr0pzba2r6rx0nmryr8r") (r "1.56")))

(define-public crate-amqp_serde-0.4.1 (c (n "amqp_serde") (v "0.4.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes_ng") (r "^0.1.1") (d #t) (k 0)))) (h "0k8110c0jzcswqyf5crvpb6vhzq8papj2kisvcfdd2m09h282xbq") (r "1.56")))

