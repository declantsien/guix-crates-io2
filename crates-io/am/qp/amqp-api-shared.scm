(define-module (crates-io am qp amqp-api-shared) #:use-module (crates-io))

(define-public crate-amqp-api-shared-0.1.0 (c (n "amqp-api-shared") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vl9n7h3pv0n9cn8xj7wm45n021h87lgspxl4apbk33r1s9szpjs")))

(define-public crate-amqp-api-shared-0.1.1 (c (n "amqp-api-shared") (v "0.1.1") (d (list (d (n "lapin") (r "^2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y1sqhbpvbm6jifhclj1m1p9jjrq0z8vpv85jzz55gyzprplcbw9")))

