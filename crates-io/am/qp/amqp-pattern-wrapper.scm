(define-module (crates-io am qp amqp-pattern-wrapper) #:use-module (crates-io))

(define-public crate-amqp-pattern-wrapper-0.0.1 (c (n "amqp-pattern-wrapper") (v "0.0.1") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "0xapyzqjjmdi5l1kw3s8iaz6p1q1jy021858khj9l9rn1kqhbrhj")))

(define-public crate-amqp-pattern-wrapper-0.0.2 (c (n "amqp-pattern-wrapper") (v "0.0.2") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "1bb3p80dqmay7pjrq9wk2g5arpasf0hsw15vv1qv6kyi5avjr69n")))

(define-public crate-amqp-pattern-wrapper-0.0.3 (c (n "amqp-pattern-wrapper") (v "0.0.3") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "1jv746sl1jiyhp7x8x1mn6iw6mfhzrhbwcqmi9j503rs0j126h9p")))

(define-public crate-amqp-pattern-wrapper-0.0.4 (c (n "amqp-pattern-wrapper") (v "0.0.4") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "06ianqli4h70340v1gw9q2fn99hskd2zr68chk471ql31vq8nviz")))

(define-public crate-amqp-pattern-wrapper-0.0.5 (c (n "amqp-pattern-wrapper") (v "0.0.5") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "1qqnlgf2a68slq351267w2h51nlljw1m1cmqnz8fqijgkppc8zgz")))

(define-public crate-amqp-pattern-wrapper-0.0.6 (c (n "amqp-pattern-wrapper") (v "0.0.6") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "1s0lyil8zhw0gsfrski6af894wnby7gng2x4bkqhw4qzbzqn8150")))

(define-public crate-amqp-pattern-wrapper-0.0.7 (c (n "amqp-pattern-wrapper") (v "0.0.7") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "0kl2dd24brir89xl2g6hh1mfw5yb98nv0ylgnfl7yig9j1jrl80v")))

(define-public crate-amqp-pattern-wrapper-0.0.8 (c (n "amqp-pattern-wrapper") (v "0.0.8") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "1a6pw4850b1dihjb47zgkl21bqh8ax8jvyl05kv5ic9rvkx4r0gv")))

(define-public crate-amqp-pattern-wrapper-0.0.9 (c (n "amqp-pattern-wrapper") (v "0.0.9") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)))) (h "0awrb7wxhawl4fkxgmyklwzlk9fzkh2h6xrh1nas1kbrlrbsrnmw")))

