(define-module (crates-io am qp amqpr-codec) #:use-module (crates-io))

(define-public crate-amqpr-codec-0.1.0 (c (n "amqpr-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0cpp31p8bmcpbz0qgf8zp27mxlh1x4nwfksffx0v586y7sa7iyz5")))

(define-public crate-amqpr-codec-0.1.1 (c (n "amqpr-codec") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1vpjwz8l4wjkfsd097ynnfsw1445qnz93vp7vfb31nl32vwr61l2")))

(define-public crate-amqpr-codec-0.2.0 (c (n "amqpr-codec") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1yps9qj1jr1h13qjx5v8ldrcwjb667lhrmhb3af2iscfwhyynhvn")))

(define-public crate-amqpr-codec-0.2.1 (c (n "amqpr-codec") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0gq9q0k0r6cwp2540msrs6n5h2hki26c2vg3qdi3jsp3f6cciphc")))

(define-public crate-amqpr-codec-0.2.2 (c (n "amqpr-codec") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "04gm3p3k9v62ii1in3mcm67cyx343jx1pyzs1wmxqvds97npqhfj")))

(define-public crate-amqpr-codec-0.2.3 (c (n "amqpr-codec") (v "0.2.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "141ravmazh0g8gh04v7l6224dx8d9xhmgg662pczvl6ik42wsp6f")))

(define-public crate-amqpr-codec-0.2.4 (c (n "amqpr-codec") (v "0.2.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1m4pwa8jql0j95gnma9ivnhi42gq8qyzfgg0gwhpgvgl74ingj8j")))

(define-public crate-amqpr-codec-0.2.5 (c (n "amqpr-codec") (v "0.2.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0ml4pha0dimcv1nzizz7xc7rd6hlmn6mhdl41vg4z8wnhgby0hrl") (y #t)))

(define-public crate-amqpr-codec-0.3.0 (c (n "amqpr-codec") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "118l6dkgwpsyl5p4w3nl4sbkhk3drsgjjdkdf1sz7jry549kk2r8")))

(define-public crate-amqpr-codec-0.3.1 (c (n "amqpr-codec") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0nymzx9xhmmk63gnjrxha59mwrhkz5d226gkrj1997hrk1x5gh5q")))

(define-public crate-amqpr-codec-0.3.2 (c (n "amqpr-codec") (v "0.3.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "11badmfhnlfm4nvwbp7q7ak33w21rcgqw4jl6l3icc4fkkc6v0iy")))

