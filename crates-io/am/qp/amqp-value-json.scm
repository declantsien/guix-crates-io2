(define-module (crates-io am qp amqp-value-json) #:use-module (crates-io))

(define-public crate-amqp-value-json-0.1.0 (c (n "amqp-value-json") (v "0.1.0") (d (list (d (n "amq-protocol-types") (r "^7.0.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "054sx2hcagjyd9qdnihxwdixzyy4qy2rcgw7xz7w4x4v1l8zrh1x")))

