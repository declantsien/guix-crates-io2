(define-module (crates-io am qp amqp-api-client) #:use-module (crates-io))

(define-public crate-amqp-api-client-0.1.0 (c (n "amqp-api-client") (v "0.1.0") (d (list (d (n "amqp-api-shared") (r "^0.1") (d #t) (k 0)) (d (n "cooplan-config-reader") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "0w8l17vkk3knirv1nh6v0glj1s2lr6gzsvixaq1ww3zpzmzfa1n4")))

