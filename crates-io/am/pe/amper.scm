(define-module (crates-io am pe amper) #:use-module (crates-io))

(define-public crate-amper-0.1.0 (c (n "amper") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "amper_mac") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "stdm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0zd19l5gvxlixrlahsb16p9qaz4402zc5knn6ny73c8vdb5pp0w4")))

(define-public crate-amper-1.0.0 (c (n "amper") (v "1.0.0") (d (list (d (n "actix-files") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "amper_mac") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "stdm") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0sclsxhkr6crr6hadvdvaphcg317x5724gwsakmd1zz4schhpk8p")))

