(define-module (crates-io am pe ampere-diskimage) #:use-module (crates-io))

(define-public crate-ampere-diskimage-0.1.0-rc0 (c (n "ampere-diskimage") (v "0.1.0-rc0") (d (list (d (n "fatfs") (r "^0.3.6") (d #t) (k 0)) (d (n "gpt") (r "^3.0.0") (d #t) (k 0)))) (h "1qms6vydyqks5c9nr3w2j3x2m732qri7bck6d0m6h9zbv8h8fki0")))

(define-public crate-ampere-diskimage-0.1.0-rc1 (c (n "ampere-diskimage") (v "0.1.0-rc1") (d (list (d (n "fatfs") (r "^0.3.6") (d #t) (k 0)) (d (n "gpt") (r "^3.0.0") (d #t) (k 0)))) (h "0563c8hnq67p7m98935fckapm02ccmcknihrypydp5g90p9vbha5")))

