(define-module (crates-io am ai amail) #:use-module (crates-io))

(define-public crate-amail-0.0.0 (c (n "amail") (v "0.0.0") (d (list (d (n "ammonia") (r "^3.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notmuch-more") (r "^0.0.0") (d #t) (k 0)) (d (n "tauri") (r "^1.0.0-beta.4") (f (quote ("api-all"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.0.0-beta.2") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "whoami") (r "^1.1.2") (d #t) (k 0)))) (h "01lazl771xa6h281xfyxxkrvxc1bzzr94jj636k6597zsmpdin2p") (f (quote (("default" "custom-protocol") ("custom-protocol" "tauri/custom-protocol"))))))

