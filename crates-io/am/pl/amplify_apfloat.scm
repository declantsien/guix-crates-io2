(define-module (crates-io am pl amplify_apfloat) #:use-module (crates-io))

(define-public crate-amplify_apfloat-0.1.0 (c (n "amplify_apfloat") (v "0.1.0") (d (list (d (n "amplify_num") (r "^0.4.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1bcr5c4hsh5p0sgfhd8vxb4ig603l5vva66m1blx82aih8ljxc81") (f (quote (("std") ("default" "std"))))))

(define-public crate-amplify_apfloat-0.1.1 (c (n "amplify_apfloat") (v "0.1.1") (d (list (d (n "amplify_num") (r "^0.4.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "18d735vgnzh1z61vlz4bqgf2q68wanjy2lxz6y8jnf1ragpv1scp") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc"))))))

(define-public crate-amplify_apfloat-0.1.2 (c (n "amplify_apfloat") (v "0.1.2") (d (list (d (n "amplify_num") (r "^0.4.0") (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "118v2wghv6sp0pic2x95m5j65yhkwd50hg56x494h672bjk734cz") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc"))))))

(define-public crate-amplify_apfloat-0.1.3 (c (n "amplify_apfloat") (v "0.1.3") (d (list (d (n "amplify_num") (r "^0.4.0") (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1w0bw31brqisawchqgkck6sbi6pwfxp518snahzw9lzcdlh42rjy") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc"))))))

(define-public crate-amplify_apfloat-0.1.4 (c (n "amplify_apfloat") (v "0.1.4") (d (list (d (n "amplify_num") (r "^0.5.0") (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "117wvd2j8lqa9yr86w0xj1z54vjwkrq24ayjp5s4x8wkd6gijn59") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc")))) (y #t) (r "1.60.0")))

(define-public crate-amplify_apfloat-0.2.0 (c (n "amplify_apfloat") (v "0.2.0") (d (list (d (n "amplify_num") (r "^0.5.0") (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "01fvjrx0p21rdv09c0py31x95sw6p22xml7yzdfyyg9amngz5dzm") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc")))) (r "1.60.0")))

(define-public crate-amplify_apfloat-0.3.0 (c (n "amplify_apfloat") (v "0.3.0") (d (list (d (n "amplify_num") (r "^0.5.2") (k 0)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1vk1ghmi2rn8d4xy5nk1dl0m33bjsac38qvsjnk5y1lrvrg3zqkj") (f (quote (("std" "amplify_num/std") ("default" "std") ("alloc" "amplify_num/alloc")))) (r "1.60.0")))

