(define-module (crates-io am pl amplify_derive) #:use-module (crates-io))

(define-public crate-amplify_derive-0.1.0 (c (n "amplify_derive") (v "0.1.0") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "142cm4yl8zqq10l9yarfcdrmsqrr5b8vpdil8fqqvzg723fj645b")))

(define-public crate-amplify_derive-0.1.1 (c (n "amplify_derive") (v "0.1.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1ajz0gwdlk0if7444cns9nwh6jklqc230nq7g0lkqf3qfrdf5vmj")))

(define-public crate-amplify_derive-0.1.2 (c (n "amplify_derive") (v "0.1.2") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1w19496bnjz6chzg3m5pskjzk6ykm1f9zcr1vc46c0hfax599jav")))

(define-public crate-amplify_derive-0.1.3 (c (n "amplify_derive") (v "0.1.3") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "08204kc1d2gph13lh0wc139cjqc94cxa0rh6vqczqjqda64ccgc8")))

(define-public crate-amplify_derive-1.0.0-beta.1 (c (n "amplify_derive") (v "1.0.0-beta.1") (d (list (d (n "amplify") (r "=1.0.0-beta.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "12zyxdvg4rl5f9l7xp45krls54136pnn5frjb7i6cq50frg4fnww")))

(define-public crate-amplify_derive-1.0.0 (c (n "amplify_derive") (v "1.0.0") (d (list (d (n "amplify") (r "~1.0.0") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1x39jg69ap2harbgpd2i18fj8d9n1r2280k8y84220xxc7hca045")))

(define-public crate-amplify_derive-1.1.0 (c (n "amplify_derive") (v "1.1.0") (d (list (d (n "amplify") (r "~1.0.0") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1gz0m3amf263d7xcpcr7jkasj6g48apqjj7wlarzvkm92gx6j565") (y #t)))

(define-public crate-amplify_derive-1.1.1 (c (n "amplify_derive") (v "1.1.1") (d (list (d (n "amplify") (r "~1.1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1in6ik4nh93m5ppnrvi6d82qwawbhsbss8vf1q625adkxwwmms66")))

(define-public crate-amplify_derive-1.2.0 (c (n "amplify_derive") (v "1.2.0") (d (list (d (n "amplify") (r "~1.2.0") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "02xcw0ccmdbc3zxjzxac7gagb4vj37k16s24q9akvsy1dijq1w2p")))

(define-public crate-amplify_derive-1.2.1 (c (n "amplify_derive") (v "1.2.1") (d (list (d (n "amplify") (r "~1.2.0") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "14hyc7amabycpi6lc7k4r3whvj58vs2z284h1z5x0gxszl9ybvyp")))

(define-public crate-amplify_derive-2.0.0-rc.1 (c (n "amplify_derive") (v "2.0.0-rc.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1iy6r11vypfzfcgr52f02v78wncjs9zrxl5a83gh29xq1g623vyn")))

(define-public crate-amplify_derive-2.0.0-rc.2 (c (n "amplify_derive") (v "2.0.0-rc.2") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0l1yxzjfsv9ssqprdi1zw6b55sfn89wr9mqc0n4rkrlp5xyrc6y1")))

(define-public crate-amplify_derive-2.0.0-rc.3 (c (n "amplify_derive") (v "2.0.0-rc.3") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1w73hwrqw72zwj6gbd4z1xmg7vgsh8lib0is4s8cl2s0vsspjgbm")))

(define-public crate-amplify_derive-2.0.0 (c (n "amplify_derive") (v "2.0.0") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0ih4hgi6xmjz6523gqwyywl6wxzpv39jrkl99vvr9vrzb5whxash")))

(define-public crate-amplify_derive-2.0.1 (c (n "amplify_derive") (v "2.0.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "05wfw6iimfsb8msszyl19kim256w78hakpmvd99hr1rms8z7rha7")))

(define-public crate-amplify_derive-2.0.2 (c (n "amplify_derive") (v "2.0.2") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0wbl8lmgg2xhkvmdc0xm5p475z2n0rlxgd2fz9j5x90sj4mg8kr5")))

(define-public crate-amplify_derive-2.0.3 (c (n "amplify_derive") (v "2.0.3") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "18f2nckin63wfd9knpd0jlfickjrbcp32l4b49np8nvhihkw1qs4")))

(define-public crate-amplify_derive-2.0.4 (c (n "amplify_derive") (v "2.0.4") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1zn73ga659b4n9ggchn6ijxz9jjsajbcla5mvhjggwd1kqac1rc4")))

(define-public crate-amplify_derive-2.0.5 (c (n "amplify_derive") (v "2.0.5") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1xk930arzsi9dr068m834aznl4m9dir1jz810jkygzg94nxd22d6")))

(define-public crate-amplify_derive-2.0.6 (c (n "amplify_derive") (v "2.0.6") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0rfg1s2bmn6i6ri04zjjn31yj1xfl3m2fjsbqj8dg6z9i06zzahv")))

(define-public crate-amplify_derive-2.0.7 (c (n "amplify_derive") (v "2.0.7") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "13mbyb7svd2rbygg561mvkw0if0wa118rb9zjpv6j93dljm6l492")))

(define-public crate-amplify_derive-2.1.0 (c (n "amplify_derive") (v "2.1.0") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1f5847ybqhigm755yb2yi1j4vv84lv2h2x9w7fc510vs2s4ilddj")))

(define-public crate-amplify_derive-2.1.1 (c (n "amplify_derive") (v "2.1.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0zssq0gfv1aayw9rfl3yy8szajh0lva56f7r9prkl3cij44kl0wa")))

(define-public crate-amplify_derive-2.1.2 (c (n "amplify_derive") (v "2.1.2") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "043cisaq0j79m6ihdcavbnq8hhgniffnclsjdip747sy2zrca178")))

(define-public crate-amplify_derive-2.2.0 (c (n "amplify_derive") (v "2.2.0") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0c5qg2bvykay7jwkh7pm9czihm2sld88a6znki9bazn2188jkcq4")))

(define-public crate-amplify_derive-2.2.1 (c (n "amplify_derive") (v "2.2.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1i1iq9niiylmz8m9n3anhrjkwmix2qgqs9wnamd5rg6lvs51qkmb")))

(define-public crate-amplify_derive-2.2.2 (c (n "amplify_derive") (v "2.2.2") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0dm6slgbf0bxrqdrq7hzp9vvbpjzpf85qg7b8jn5hbihqdk4p6k0")))

(define-public crate-amplify_derive-2.3.0 (c (n "amplify_derive") (v "2.3.0") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1l5gna34bapfs0vd65y5y16py0ss67b08bvq638fn790wjlrnb4r")))

(define-public crate-amplify_derive-2.3.1 (c (n "amplify_derive") (v "2.3.1") (d (list (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "03ab05jfg2zc3lpfx00yrhdxy6pjwg7fbizjzajbzs7wrm9nd81v")))

(define-public crate-amplify_derive-2.4.0 (c (n "amplify_derive") (v "2.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)))) (h "0vmk1pylrclxkr8gkmcrp0i78782j83qfwsn2ayj492rqxmmr348")))

(define-public crate-amplify_derive-2.4.1 (c (n "amplify_derive") (v "2.4.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)))) (h "0j8wknlngi1qfmqszkqgif4vmwm9aj2n2yf75s5a55adqwc9w5rq")))

(define-public crate-amplify_derive-2.4.2 (c (n "amplify_derive") (v "2.4.2") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)))) (h "0p8g4iknf5j1vf8hddqvshfk3gill890iiqv1n9yfz1pindrwh9w")))

(define-public crate-amplify_derive-2.4.3 (c (n "amplify_derive") (v "2.4.3") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1mzpjbqpggbjwxwlbq0gadf0nsb9fs6nqy7kb7avx7pf1j0ggdjj")))

(define-public crate-amplify_derive-2.4.4 (c (n "amplify_derive") (v "2.4.4") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1wxginbgfys914vfx5vnppwnr9alkambviyi548sz0zp2gym1k69")))

(define-public crate-amplify_derive-2.5.0 (c (n "amplify_derive") (v "2.5.0") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "0av0qyd5hf7wg0zghddf3zddaj97rab8if2gwd9rrxcdm0s3xd8m")))

(define-public crate-amplify_derive-2.5.1 (c (n "amplify_derive") (v "2.5.1") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "0xlvdrn5mi3zz98rkzvh05s247yrcxcv0zn5b0dg19jycz0fcjpj")))

(define-public crate-amplify_derive-2.5.2 (c (n "amplify_derive") (v "2.5.2") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "05vlrx12jkvfygb5aw5xgj4qv1rm86c3mk35mkis5sgdg17dz7lg")))

(define-public crate-amplify_derive-2.5.3 (c (n "amplify_derive") (v "2.5.3") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1fynkrbj8059fzhbpdiqmkwi8qhh9h3ibln2wwa2wkifrx226mr6")))

(define-public crate-amplify_derive-2.6.0 (c (n "amplify_derive") (v "2.6.0") (d (list (d (n "amplify_syn") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04rsfpd7jvck39hqmfk64ylspw63gf5bijgxl5xr83j9zxhzcdp2")))

(define-public crate-amplify_derive-2.7.0 (c (n "amplify_derive") (v "2.7.0") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kd386pn5h2yqrk5rxkadlgrxrsmh2007q74n2lmljgrj72m238m")))

(define-public crate-amplify_derive-2.7.1 (c (n "amplify_derive") (v "2.7.1") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0r8szpg6m1qdlmjhv900hlilmk72xzng7hi2hg3ync997q60w39m")))

(define-public crate-amplify_derive-2.7.2 (c (n "amplify_derive") (v "2.7.2") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08vbq6sljk9ikqvcssr9w0gx7rrrjypnz0hsl9npav5bq01mxhkh")))

(define-public crate-amplify_derive-2.8.0 (c (n "amplify_derive") (v "2.8.0") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17f701g37as1v9zvllp239x3gcxfyfpd0kwk74fchn7zdkss47hg")))

(define-public crate-amplify_derive-2.8.1 (c (n "amplify_derive") (v "2.8.1") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bl4n0fs6i4gi3ag2w29bc5sjzxd0hxx5jf3rz1n2cijzq238pca")))

(define-public crate-amplify_derive-2.8.2 (c (n "amplify_derive") (v "2.8.2") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0g97qmm6h1k9sxrzqgd9ai46i7wz3aba361lrz69da2m0mhc8c48")))

(define-public crate-amplify_derive-2.8.3 (c (n "amplify_derive") (v "2.8.3") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02rm5zcij5fnciws9asvg42ivf9pw3rx15q214ff6m8sjkqygs6k")))

(define-public crate-amplify_derive-2.9.0 (c (n "amplify_derive") (v "2.9.0") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wpp5ykm35dpab622mwpm7m4qg7jj1w9f3ygr5a34cyq7mlhhc20")))

(define-public crate-amplify_derive-2.10.0 (c (n "amplify_derive") (v "2.10.0") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15172fsmpzx5c9a1rg8ypc0gsgic6rskrdqnzwq1rhcbxqwbc8rc")))

(define-public crate-amplify_derive-2.11.0 (c (n "amplify_derive") (v "2.11.0") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07jqhfibx952gwd5iy6qmh4l75ydjfbmw16kalsfi4yqhpyx7h8f")))

(define-public crate-amplify_derive-2.11.1 (c (n "amplify_derive") (v "2.11.1") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dzp6f2iqc0w43s7qi1yjybc1hprnjfr84mml7q6qdkqwhccdklr")))

(define-public crate-amplify_derive-2.11.2 (c (n "amplify_derive") (v "2.11.2") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0p1ijh10pqxqdhqrgf7829q6ls6a9gv942d54nqjzcqgbhqxdndw")))

(define-public crate-amplify_derive-2.11.3 (c (n "amplify_derive") (v "2.11.3") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1b0372wb82nlyx0j2lnbhmp6ailh21q40d3wi93a89szwxqf4g8w")))

(define-public crate-amplify_derive-4.0.0-alpha.1 (c (n "amplify_derive") (v "4.0.0-alpha.1") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0fcsadakk1kbcfcj52fm42mi9j848xh8y432zl79wz0b308gawb4") (r "1.59.0")))

(define-public crate-amplify_derive-4.0.0-alpha.2 (c (n "amplify_derive") (v "4.0.0-alpha.2") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "14cfv9nf7mn2prfsib1h1sbqnnv43dans14yak0xxyair00qa99a") (r "1.59.0")))

(define-public crate-amplify_derive-4.0.0-alpha.3 (c (n "amplify_derive") (v "4.0.0-alpha.3") (d (list (d (n "amplify_syn") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0p20pj4x5slaqlck52hwmyfwmmpnz3h263ds6h1ls1yfzfmm6gv3") (r "1.60.0")))

(define-public crate-amplify_derive-4.0.0-alpha.4 (c (n "amplify_derive") (v "4.0.0-alpha.4") (d (list (d (n "amplify") (r "^4.0.0-beta.7") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1m7q4a6i109hvj55pfrdpha3mav42qx2j8xj2wf5jfmnlzk267rk") (r "1.60.0")))

(define-public crate-amplify_derive-4.0.0-alpha.5 (c (n "amplify_derive") (v "4.0.0-alpha.5") (d (list (d (n "amplify") (r "^4.0.0-beta.7") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1jk2hw86gmrl8c4j4j7zcn0h8b1gblrn901gwxm7a5ryflr18m48") (r "1.60.0")))

(define-public crate-amplify_derive-4.0.0-alpha.6 (c (n "amplify_derive") (v "4.0.0-alpha.6") (d (list (d (n "amplify") (r "^4.0.0-beta.16") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0crkdicmy606rd58nwwcikvgc0hkrbr5mv31g6fi89a7jrg87i01") (r "1.60.0")))

(define-public crate-amplify_derive-3.0.0 (c (n "amplify_derive") (v "3.0.0") (d (list (d (n "amplify") (r "^4.0.0-beta.22") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "19j2snja5f4j9j87ash9j3i8gwsk7bc2hlaisalcy44ykavi43sq") (r "1.60.0")))

(define-public crate-amplify_derive-3.0.1 (c (n "amplify_derive") (v "3.0.1") (d (list (d (n "amplify") (r "^4.0.0") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1lgrd2yp1gawimh0k1jjcwwn9jm97xwsd6zjap9z5cbfivrg0zf8") (r "1.60.0")))

(define-public crate-amplify_derive-4.0.0 (c (n "amplify_derive") (v "4.0.0") (d (list (d (n "amplify") (r "^4.0.0") (d #t) (k 2)) (d (n "amplify_syn") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1hfr4kg0d47qdc6qidsw6vzad2nc9k1kwjbdm1kq70sdz7xcp7bm") (r "1.60.0")))

