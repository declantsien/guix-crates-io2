(define-module (crates-io am pl amplify_syn) #:use-module (crates-io))

(define-public crate-amplify_syn-1.0.0 (c (n "amplify_syn") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qz0z5bppn1mppjsd2cfrpbpd70njzn9b9bh3vbkqh6fawc75nap") (y #t)))

(define-public crate-amplify_syn-1.0.1 (c (n "amplify_syn") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06kgy23h9wqib67wa63pmlnsrmjnya8r1b5fcjpx7rcy079lrqf4")))

(define-public crate-amplify_syn-1.1.0 (c (n "amplify_syn") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "166lhvvl30mxwhlwkv6ks7aparcb44az44dp2nhxlm6mh480bg59")))

(define-public crate-amplify_syn-1.1.1 (c (n "amplify_syn") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w8nx73aw24czllknssmhm83g1nsw605j3z7v5lh7557zzdbd24d")))

(define-public crate-amplify_syn-1.1.2 (c (n "amplify_syn") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0y9dn4p7svkv1rdhkkdndgam7mpsngwysnzj4yl1aqq88min4h7k")))

(define-public crate-amplify_syn-1.1.3 (c (n "amplify_syn") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vvch0wamrhmlqzakrkr5bziakb3ljp45hmwikx47l6d5lvd7fnz")))

(define-public crate-amplify_syn-1.1.4 (c (n "amplify_syn") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0shxr5nacwkrgm00in6c1myqa2ipc4rp2sncvqg53aasl8vnyadz")))

(define-public crate-amplify_syn-1.1.5 (c (n "amplify_syn") (v "1.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dshz3n52wbmv4z984dfsnbs3nmhjv40zj3kgf19iiscjr7ppwza")))

(define-public crate-amplify_syn-1.1.6 (c (n "amplify_syn") (v "1.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b11fbl74k1ahijjsmva5f0mpcp53zaw4wm05y2c6yyc8ladn96s")))

(define-public crate-amplify_syn-2.0.0-beta.1 (c (n "amplify_syn") (v "2.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ffzrrknq4yr5i23a30i68igdzjfcfzdyzpmmwcsg35plj54ifjy") (r "1.60.0")))

(define-public crate-amplify_syn-2.0.0-beta.2 (c (n "amplify_syn") (v "2.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0x0jldml8s93yp86dn78wgwgd1blnwhwaxd9csaqmwgqjv5vag9f") (r "1.60.0")))

(define-public crate-amplify_syn-2.0.0-beta.3 (c (n "amplify_syn") (v "2.0.0-beta.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13dvfr2b2r7vah4rijypglcr19xvxlmnl0i2110rldx9jnws0jzs") (r "1.60.0")))

(define-public crate-amplify_syn-2.0.0 (c (n "amplify_syn") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "009b4k529abzhs0la2ng5ib0dfqkpa8xpp5z9alxa1m4zms8vc19") (r "1.60.0")))

(define-public crate-amplify_syn-2.0.1 (c (n "amplify_syn") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0annky4dq5h5wg58pkbm0d3j07jndbgl9b2vic4q639w8y6zndkp") (r "1.60.0")))

