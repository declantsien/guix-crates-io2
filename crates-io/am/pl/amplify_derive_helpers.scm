(define-module (crates-io am pl amplify_derive_helpers) #:use-module (crates-io))

(define-public crate-amplify_derive_helpers-0.0.1 (c (n "amplify_derive_helpers") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1mrrr1bpasf8z1l2dzr63pv2vkgivdzaamisf78vsjcx4ddf3aw7")))

(define-public crate-amplify_derive_helpers-0.0.2 (c (n "amplify_derive_helpers") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1m7ry8s31i70m0cprnw3kc1cirmfsgmh8di60919izs6amiwb5vh")))

(define-public crate-amplify_derive_helpers-0.0.3 (c (n "amplify_derive_helpers") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0cvzb64hc2q2qd4y56jr3n5vvpnp11yls94cf9nf1pnmlv3dkrjr")))

(define-public crate-amplify_derive_helpers-0.0.4 (c (n "amplify_derive_helpers") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "08pwqy8anvyfxssyrxnbqci8hk1sglf1v4ingw5iligrannb4xlh")))

