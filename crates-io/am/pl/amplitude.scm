(define-module (crates-io am pl amplitude) #:use-module (crates-io))

(define-public crate-amplitude-0.1.0 (c (n "amplitude") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros"))) (d #t) (k 2)))) (h "03g3x58kwq16y4g54n347zrrz19j5q6an9l2ninp4w10vxzx40jf")))

