(define-module (crates-io am ja amjad_os_kernel_user_link) #:use-module (crates-io))

(define-public crate-amjad_os_kernel_user_link-0.1.0 (c (n "amjad_os_kernel_user_link") (v "0.1.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1x98gch1m9wm6cljb1bg84jn4kabkxgqdmngaj9qvnff5cvli9vf") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins" "alloc"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.1 (c (n "amjad_os_kernel_user_link") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0f0jf89qh9786gcd8cds5b4rg20sd1fzkmkgaqawkwhd4a1n01qj") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.2 (c (n "amjad_os_kernel_user_link") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0qw01xzb2yrdinzzg31p764bs2rx9642rq90si3wzd58fmanhgrz") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.3 (c (n "amjad_os_kernel_user_link") (v "0.1.3") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "04n80sh5bn3659hdbqdg0s6hfrhvbkk64d2m32il0x79xk4jsim3") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.4 (c (n "amjad_os_kernel_user_link") (v "0.1.4") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1lbp7k3g6mj59ax3yakv1009bjfnh6mlj5hxqg3i0c0aijyscl9w") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.5 (c (n "amjad_os_kernel_user_link") (v "0.1.5") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "040b5r2bk4qn3kv3yvcw4x2pzmx5vhrvq3cccsr2523qkswn1hd0") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.1.6 (c (n "amjad_os_kernel_user_link") (v "0.1.6") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1yriykcsdm3b9rgxg0g50vql6hswb7lls1ih19q43hn1lzakvgfz") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.2.0 (c (n "amjad_os_kernel_user_link") (v "0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0ivj2fz90znmk8dks98dkkkhl8isn5qg2ra8lyzl6q6gm4g2klg8") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-amjad_os_kernel_user_link-0.2.1 (c (n "amjad_os_kernel_user_link") (v "0.2.1") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "17k3y2zsxrpm6kvambfhhfa6dis197vsya93n5h6ipx6sfprlm0p") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

