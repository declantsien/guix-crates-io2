(define-module (crates-io am g8 amg88) #:use-module (crates-io))

(define-public crate-amg88-0.3.0 (c (n "amg88") (v "0.3.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "=0.4.0-alpha.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "13qq96nb1hd7cinmwy4cphq000nb9iaq99qpz794b8wn054rn4qy") (f (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

(define-public crate-amg88-0.4.0 (c (n "amg88") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1j66al2c5dm88bf42b89pvnl2iil0hvrzljl4swf59jql4fwhky1") (f (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

(define-public crate-amg88-0.4.1 (c (n "amg88") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1pdgshfbilyl3ccf04cqyvmag87wpcygxiphghna11lyhh2abzhn") (f (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

