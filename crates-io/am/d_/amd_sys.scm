(define-module (crates-io am d_ amd_sys) #:use-module (crates-io))

(define-public crate-amd_sys-0.1.0 (c (n "amd_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0zdv8amh9nbk3s3aijazvl659ax3sh7bii495fdfag7kfbwamw83") (f (quote (("dynamic")))) (l "amd")))

