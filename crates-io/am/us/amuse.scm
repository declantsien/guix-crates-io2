(define-module (crates-io am us amuse) #:use-module (crates-io))

(define-public crate-amuse-0.0.1-dev (c (n "amuse") (v "0.0.1-dev") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "midir") (r "^0.6") (d #t) (k 0)) (d (n "pitch_calc") (r "^0.12") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (d #t) (k 0)))) (h "098j0vx59jh4wlw3mc7191b0ni68xig4hqxrvpzizw3ndx34h9bh")))

