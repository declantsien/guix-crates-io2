(define-module (crates-io am id amid) #:use-module (crates-io))

(define-public crate-amid-0.1.2 (c (n "amid") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "14rkr253ir2iv3fy3r0shkkwzsrw15vl9n85kh3fzz5y7vvp5xhk")))

(define-public crate-amid-0.1.3 (c (n "amid") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0gl17r521qb60ghrcpx5w0k2xcpqh98giw9dgdsiw1wghdywc0zw")))

(define-public crate-amid-0.1.5 (c (n "amid") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "085amvav8i45jlfvb13p9vqpgwhv9rrsp2lhz27qhl91pdyh1rm7")))

