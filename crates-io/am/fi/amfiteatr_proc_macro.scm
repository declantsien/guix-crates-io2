(define-module (crates-io am fi amfiteatr_proc_macro) #:use-module (crates-io))

(define-public crate-amfiteatr_proc_macro-0.3.0 (c (n "amfiteatr_proc_macro") (v "0.3.0") (d (list (d (n "enum-map-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1xjjbv4nmgzm07q59cg58d7xccnjzncqyg6i4si27iy7j03hmmqf")))

(define-public crate-amfiteatr_proc_macro-0.4.0 (c (n "amfiteatr_proc_macro") (v "0.4.0") (d (list (d (n "enum-map-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0qarxqzla3dsyp0d2ffakp9qsfaphc5ny5x4jbpgy6h8yd81fkr5")))

(define-public crate-amfiteatr_proc_macro-0.4.1 (c (n "amfiteatr_proc_macro") (v "0.4.1") (d (list (d (n "enum-map-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0zlhf4ch8qbfzvjlcywa3s88cgvj31d7yf0qgmlzan21zmd4aphv")))

(define-public crate-amfiteatr_proc_macro-0.5.0 (c (n "amfiteatr_proc_macro") (v "0.5.0") (d (list (d (n "enum-map-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "01m92jr5l1pixgv123jfzz2imwabib7lanfc79qm19c4kkqh01qm")))

