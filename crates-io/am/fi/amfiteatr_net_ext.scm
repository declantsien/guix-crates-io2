(define-module (crates-io am fi amfiteatr_net_ext) #:use-module (crates-io))

(define-public crate-amfiteatr_net_ext-0.1.0 (c (n "amfiteatr_net_ext") (v "0.1.0") (d (list (d (n "amfiteatr_core") (r "^0.1.0") (f (quote ("speedy"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "speedy") (r "^0.8.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "021lqw2l7l8m35a6gpcrqdbwxwsnddavm0ghwl569ly0k59sgaqx")))

(define-public crate-amfiteatr_net_ext-0.2.0 (c (n "amfiteatr_net_ext") (v "0.2.0") (d (list (d (n "amfiteatr_core") (r "^0.2.0") (f (quote ("speedy"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "speedy") (r "^0.8.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1202q69fv4j8zyzk5syizyf8q7x5rzrimkqswrkvppbvzi8p19lq")))

(define-public crate-amfiteatr_net_ext-0.2.1 (c (n "amfiteatr_net_ext") (v "0.2.1") (d (list (d (n "amfiteatr_core") (r "^0.2.1") (f (quote ("speedy"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "speedy") (r "^0.8.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0gad47k6phfpcqdygcr0dy6v8dcaxxfw047gq0fcc01gkjzl62yk")))

(define-public crate-amfiteatr_net_ext-0.3.0 (c (n "amfiteatr_net_ext") (v "0.3.0") (d (list (d (n "amfiteatr_core") (r "^0.3.0") (f (quote ("speedy" "demo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "19pvrp70idbh4r6b7nrr2can205avdljp1g5b1v2158wfji5ssws")))

(define-public crate-amfiteatr_net_ext-0.4.0 (c (n "amfiteatr_net_ext") (v "0.4.0") (d (list (d (n "amfiteatr_core") (r "^0.4.0") (f (quote ("speedy" "demo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "1bsq0xazx2ixrq02v1hdiixkja8z9a00c750xzncwc80iw8m59dq")))

(define-public crate-amfiteatr_net_ext-0.4.1 (c (n "amfiteatr_net_ext") (v "0.4.1") (d (list (d (n "amfiteatr_core") (r "^0.4.1") (f (quote ("speedy" "demo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "02w8lc6dacqsyccvlfs4b69354m226w60by6rvr706cs0q8i9yaj")))

(define-public crate-amfiteatr_net_ext-0.5.0 (c (n "amfiteatr_net_ext") (v "0.5.0") (d (list (d (n "amfiteatr_core") (r "^0.5.0") (f (quote ("speedy" "demo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "0i5s0smmfbig0shhfvs2f98yz8zwb7cd9kcmdj6hnd3g4rvd0sgl")))

