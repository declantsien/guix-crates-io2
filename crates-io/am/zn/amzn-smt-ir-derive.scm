(define-module (crates-io am zn amzn-smt-ir-derive) #:use-module (crates-io))

(define-public crate-amzn-smt-ir-derive-0.1.0 (c (n "amzn-smt-ir-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0knj3j865rj0jjqnc66m90f9wmlww2apr5cfkgi4x0s8aawx4mvs") (y #t)))

