(define-module (crates-io am ou amount_conversion) #:use-module (crates-io))

(define-public crate-amount_conversion-0.1.0 (c (n "amount_conversion") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0f4rj6cc2zd413n9w2pcps5azkf32wnm32v25v74ylqdj8pz499y") (r "1.65")))

(define-public crate-amount_conversion-0.1.1 (c (n "amount_conversion") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "04fkqjqln0hz0mxp7mspiqbq3irajvq1401z1wvl4dilbjwh25dx") (r "1.65")))

