(define-module (crates-io am l_ aml_parser) #:use-module (crates-io))

(define-public crate-aml_parser-0.2.0 (c (n "aml_parser") (v "0.2.0") (h "0xrzwg0nz9zcww9fyvjlc7cs3r04jz35x9ksxi0f8kicb6xgsb5h") (y #t)))

(define-public crate-aml_parser-0.3.0 (c (n "aml_parser") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x2r6mlil87zfklbjmh06wy445fsg8m617hsa7al3hx0zj2g8m3q") (f (quote (("debug_parser_verbose") ("debug_parser")))) (y #t)))

