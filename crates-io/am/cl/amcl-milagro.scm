(define-module (crates-io am cl amcl-milagro) #:use-module (crates-io))

(define-public crate-amcl-milagro-3.2.6 (c (n "amcl-milagro") (v "3.2.6") (h "0w77f4zxi7w6iq4jazgia71cqzv1si7riqk2zq0wwxhq3pi1nzws") (f (quote (("secp256k1") ("rsa4096") ("rsa3072") ("rsa2048") ("nums512w") ("nums512e") ("nums384w") ("nums384e") ("nums256w") ("nums256e") ("nist521") ("nist384") ("nist256") ("hifive") ("goldilocks") ("fp512bn") ("fp256bn") ("ed25519") ("default") ("c41417") ("c25519") ("brainpool") ("bn254CX") ("bn254") ("bls48") ("bls461") ("bls383") ("bls381") ("bls24") ("ansii"))))))

