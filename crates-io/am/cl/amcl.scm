(define-module (crates-io am cl amcl) #:use-module (crates-io))

(define-public crate-amcl-0.1.0 (c (n "amcl") (v "0.1.0") (h "1i0b27cnkmw6ayv5r3qi8mcwa9a58j8zb99yd8dal8aasqc8bf1z")))

(define-public crate-amcl-0.1.1 (c (n "amcl") (v "0.1.1") (h "09pm7zfpvwl2y8jz17lzw0azqw5mma36f5c1mcxk2d0vvbcnqr1g")))

(define-public crate-amcl-0.1.2 (c (n "amcl") (v "0.1.2") (h "0fcbaix5wgm7wl7p4r19l2kr9x3whnn6rb9hs4546hg6l3x5qmca") (f (quote (("default" "BLS383") ("GOLDILOCKS") ("Ed25519") ("BN254") ("BLS455") ("BLS383"))))))

(define-public crate-amcl-0.1.3 (c (n "amcl") (v "0.1.3") (h "140avmnhv6xbkgjxibr38hkh5nvcb1vr2l84rwvkbwlv6wwyvlcq") (f (quote (("default" "BLS383") ("GOLDILOCKS") ("Ed25519") ("BN254") ("BLS455") ("BLS383"))))))

(define-public crate-amcl-0.2.0 (c (n "amcl") (v "0.2.0") (h "038g7zij8vcjnh5a4l7k0qqdfhz6s6lp44kzbfswx7cbvhfwlp7f") (f (quote (("secp256k1") ("rsa4096") ("rsa3072") ("rsa2048") ("nums512w") ("nums512e") ("nums384w") ("nums384e") ("nums256w") ("nums256e") ("nist521") ("nist384") ("nist256") ("hifive") ("goldilocks") ("fp512BN") ("fp256Bn") ("ed25519") ("default" "bn254") ("c41417") ("c25519") ("brainpool") ("bn254cx") ("bn254") ("bls48") ("bls461") ("bls383") ("bls381") ("bls24") ("ansii"))))))

