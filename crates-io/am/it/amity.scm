(define-module (crates-io am it amity) #:use-module (crates-io))

(define-public crate-amity-0.0.0 (c (n "amity") (v "0.0.0") (h "06sman94jr6b1i1d9m51gb8vp5sn9q277l34k0l399ixqvllf6lp")))

(define-public crate-amity-0.1.0 (c (n "amity") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "1fkfn8vxl5j8yk805yws4f603qrhq65xkllvbhcvrmxnn3zh4l77") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

