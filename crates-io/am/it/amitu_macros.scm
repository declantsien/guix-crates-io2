(define-module (crates-io am it amitu_macros) #:use-module (crates-io))

(define-public crate-amitu_macros-0.1.0 (c (n "amitu_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1wcx893qhkgpdlhkj6gpykilz9pzia3bzfbcm57mrp06rl5mryss")))

