(define-module (crates-io am ad amadeus-parquet-derive) #:use-module (crates-io))

(define-public crate-amadeus-parquet-derive-0.1.1 (c (n "amadeus-parquet-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1126mbflzzsnp5c691w7954n7165j0ys7js06r95kkwqnyhfxd2n")))

