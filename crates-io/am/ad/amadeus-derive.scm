(define-module (crates-io am ad amadeus-derive) #:use-module (crates-io))

(define-public crate-amadeus-derive-0.1.1 (c (n "amadeus-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "00sn047hbnr88sdn5yhila5f6j1f403fb7y8cnbhr6afa32ybmg9") (f (quote (("parquet"))))))

(define-public crate-amadeus-derive-0.1.2 (c (n "amadeus-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0zxkdw6igzw309r9kwavrxbfw448i9q13kpcngn6kg9v8yaaad8v") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.1.3 (c (n "amadeus-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11fkd69mp3sgyinfip9g81zj4dr1c5ppb182j1b90lb3m6c39a56") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.1.4 (c (n "amadeus-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0p46sxqjcybjifxjpzlbpijpdb0paws1zpzpf2jrkkj51dcd4zxp") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.1.5 (c (n "amadeus-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ihmig2s6xndhcfbqqcx8c7jib9g9aakxrxw4w0nyljxxvjv8dyh") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.1.6 (c (n "amadeus-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1hzry0jcy661y2yavdjysdkashx7ffklwfb727vpz06rl957s6ql") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.1.7 (c (n "amadeus-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0lng00xfrf1iy1n3pb739415sbxppn2snh3qqj2izb6axfb8prxy") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.0 (c (n "amadeus-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0ngkccr5x7kxzzxx7hkdhv8mx0rsbgyxq5vkwms6l9sslwb34sxz") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.1 (c (n "amadeus-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1pns3dfb9cqrk11br9si0js3hp0bj939z2rklma8vdx9b0155r5y") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.2 (c (n "amadeus-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1dcj2c19ik05007y68n6lz19sg8758yvxkdh3jzgvdrgm7bvscd6") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.3 (c (n "amadeus-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1mfcw9y9107iyxbglg92yz8p05cw73bw9s9x62k0ha522abp6v4v") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.4 (c (n "amadeus-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "03z6nbns16jjdf88qzvgkmpka4pdq9kdry1m37f5n5mrw4pabxd3") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.2.5 (c (n "amadeus-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0imqnk3ras9ag8ss8p4va4wszyzmwy63bqhid8gv58bdyr4aqgm9") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.0 (c (n "amadeus-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ii3bpi742fx5x6hx7qf91v31rfwk0w7kdivkjcm3qxzz2nr1d9z") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.1 (c (n "amadeus-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "08ggczkswy2flap055hhx3jqzly6js1f14h24vm8w81jwbci80k1") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.2 (c (n "amadeus-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0qqzssw8ddj5bxgwpx04kn86l7ccd6zm2g9jy3jxh1ygicac5kyv") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.3 (c (n "amadeus-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "04wk6i3xgiv4g2r1bw9bdz1yix5s6m5rk3cnaif11bmglcy1mbnq") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.4 (c (n "amadeus-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "15wbcwppxzq4m719iyi012p0ngz4d903smy5m3jddbvhd4dw9i6f") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.5 (c (n "amadeus-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0ywss48l18gb2nlzb7n0xylr5j2wvb0fmi854363x8alyrfvw5br") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.6 (c (n "amadeus-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0zl3jhhzlzqaz6i1mny9q6sj0vvbiy0mm4rh6q8x8pb2y3aq7050") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.3.7 (c (n "amadeus-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "133g1xcb0xx2n8k3hdwv9br8mdpg7crmk5n5zk2438kiq5n6h34a") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.4.0 (c (n "amadeus-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "00w982xpdyvy12izsc9zvgblagz4xb2qxxar9yswggza93fk8hqx") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.4.1 (c (n "amadeus-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1a1sach44pvqmljvlh5j2x4s3n33h22ypd3hvvfi0329y215z8bh") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.4.2 (c (n "amadeus-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0wcdcnijnkxf01gp7xwkvs0ynp5yaiicxcg6y042sgjlq7j5dvg9") (f (quote (("serde") ("postgres") ("parquet"))))))

(define-public crate-amadeus-derive-0.4.3 (c (n "amadeus-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0klg0h5piiji1lbg8m9vbj50mx7ib6bql7y7ydx9pykbcsvndbh3") (f (quote (("serde") ("postgres") ("parquet"))))))

