(define-module (crates-io am q- amq-protocol-uri) #:use-module (crates-io))

(define-public crate-amq-protocol-uri-2.0.0-rc9 (c (n "amq-protocol-uri") (v "2.0.0-rc9") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0m1bp8q8z1z6w5j48xhnl008b5qihk0kklr4bqk61i3s9manddgb")))

(define-public crate-amq-protocol-uri-2.0.0-rc10 (c (n "amq-protocol-uri") (v "2.0.0-rc10") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0ah2sfy4zmzidcq386ksypak00mdd5r14yiz5cqkqwc1n14r0vpy")))

(define-public crate-amq-protocol-uri-2.0.0-rc11 (c (n "amq-protocol-uri") (v "2.0.0-rc11") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0c2q05ibzns32f872lcxx42jkdmbxq80nlkxza1glx88iy3sz611")))

(define-public crate-amq-protocol-uri-2.0.0-rc12 (c (n "amq-protocol-uri") (v "2.0.0-rc12") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1q0y8h0qy1b3ar0cs8jlkbgzh7jwcays1xjq5gajvfgi5lpp32v6")))

(define-public crate-amq-protocol-uri-2.0.0-rc13 (c (n "amq-protocol-uri") (v "2.0.0-rc13") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1l5jznpjwl12r1v1dpz441crn26vf4ccmqx82wncd98vya58skd4")))

(define-public crate-amq-protocol-uri-2.0.0-rc14 (c (n "amq-protocol-uri") (v "2.0.0-rc14") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "07ixviahlqzl35mnl8qam968888jkbav1bsjai7qrz9bkx7rvqlg")))

(define-public crate-amq-protocol-uri-2.0.0-rc15 (c (n "amq-protocol-uri") (v "2.0.0-rc15") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "021x1320kqfa2yck3x407qf49vqvzqmcvg5yil4qqqlglag2cadv")))

(define-public crate-amq-protocol-uri-2.0.0-rc16 (c (n "amq-protocol-uri") (v "2.0.0-rc16") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0vfbx3c9i5k01h9zdm2mp6jsa0p9cds2jlxjj74z4mmxpjk9kwfc")))

(define-public crate-amq-protocol-uri-2.0.0-rc17 (c (n "amq-protocol-uri") (v "2.0.0-rc17") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "06qyyyav3g24ixa2dd4zk5ph99j7vqsjv7lqfk76rc613y3ldr2b")))

(define-public crate-amq-protocol-uri-2.0.0-rc18 (c (n "amq-protocol-uri") (v "2.0.0-rc18") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1wn3yfm3hl0631fdj3zsmwdzpdihrpqdgapy6x2cm0flkhd1xfb3")))

(define-public crate-amq-protocol-uri-2.0.0 (c (n "amq-protocol-uri") (v "2.0.0") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1fswvvsw3b8l7y0r8zjb657clmih9ngjvrmhj7f0m0b3wz9fb6fi")))

(define-public crate-amq-protocol-uri-2.0.2 (c (n "amq-protocol-uri") (v "2.0.2") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0hhchvvy852s41ll4ijx4rkv7acq6g3dqqs3gn8d1aqpwkw7l36q")))

(define-public crate-amq-protocol-uri-2.1.0 (c (n "amq-protocol-uri") (v "2.1.0") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1j858xpxgfznfxls70pqn6qb6khx09zr0qh4yifk09dkrgfcyy11")))

(define-public crate-amq-protocol-uri-2.2.0 (c (n "amq-protocol-uri") (v "2.2.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0alap3bv9xdw8zs404h8d0dmriw509zjwl6c362039jlbc4ifjqn")))

(define-public crate-amq-protocol-uri-3.0.0-beta.1 (c (n "amq-protocol-uri") (v "3.0.0-beta.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "06kd7src4ybdkdzrf9922m29dqjjwx5jxhxm9zm4ncj69kmmw3c3")))

(define-public crate-amq-protocol-uri-3.0.0-beta.2 (c (n "amq-protocol-uri") (v "3.0.0-beta.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0pax39zpxbg31p8rb0mqssz4vd7z98bzckkw96w0s5yxp26zi2rn")))

(define-public crate-amq-protocol-uri-2.3.0 (c (n "amq-protocol-uri") (v "2.3.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0by5qcsnaflszq2wkpss7mjkjw5v3w9d8vvhaq2nif0vb4qyldap")))

(define-public crate-amq-protocol-uri-3.0.0 (c (n "amq-protocol-uri") (v "3.0.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "03925vzxvymrbvpg27h94wfh22ppmwdxr3z6085l2rfb5235wkyl")))

(define-public crate-amq-protocol-uri-3.1.0 (c (n "amq-protocol-uri") (v "3.1.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1x0hkqb0a2gbwq6rd8k168jiz6s28cnyb0rh05xbls3rphq8prjj")))

(define-public crate-amq-protocol-uri-3.1.1 (c (n "amq-protocol-uri") (v "3.1.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "07692gz5r76d6apqxd096k7g4nw6bpszln7zmvdpgx14k021xjq9")))

(define-public crate-amq-protocol-uri-4.0.0 (c (n "amq-protocol-uri") (v "4.0.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0xd11ngc4i6a1agva5j3dbx2iyxs9yixz1y79rxr7rrg2vrr74h9")))

(define-public crate-amq-protocol-uri-4.1.0 (c (n "amq-protocol-uri") (v "4.1.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "13sf4k498qm51g18yfq7plddrlwnkbgm4b9hkzppzhjh7733c256")))

(define-public crate-amq-protocol-uri-4.2.0 (c (n "amq-protocol-uri") (v "4.2.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0xhm35gkgqg059y4bw63m0mzdwf3xydxrpjal2i20x2kxww9lp4y")))

(define-public crate-amq-protocol-uri-5.0.0-beta.1 (c (n "amq-protocol-uri") (v "5.0.0-beta.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1s5lmk6q95qj7i2l33cmnaqm7dcyhj4l7s5z775h5kzfhv3nkrb1")))

(define-public crate-amq-protocol-uri-5.0.0-beta.2 (c (n "amq-protocol-uri") (v "5.0.0-beta.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "18r43w43c8cx912z3hvmmps369b2284p7nab66ipd10n2wcvlknc")))

(define-public crate-amq-protocol-uri-5.0.0-beta.3 (c (n "amq-protocol-uri") (v "5.0.0-beta.3") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1k26vbh6l41lfisn4kx4y88chyspbm48hi6pjfyasdlfsv9bgpxm")))

(define-public crate-amq-protocol-uri-4.2.1 (c (n "amq-protocol-uri") (v "4.2.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0w1qal9nppw34r7c4pmkcrq75h5sv79g33xhxzk3q7fs3h5adcci")))

(define-public crate-amq-protocol-uri-4.2.2 (c (n "amq-protocol-uri") (v "4.2.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1w3534vm992zzpdzi9mgs8avp54wvpcl1m1gvzv2dq0b39ldxx3n")))

(define-public crate-amq-protocol-uri-5.0.0-beta.4 (c (n "amq-protocol-uri") (v "5.0.0-beta.4") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0sc3jcz37lzd775s418cg7wlvbc1nnkfwg1h0xd4g3syz85vmwdm")))

(define-public crate-amq-protocol-uri-5.0.0 (c (n "amq-protocol-uri") (v "5.0.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0my03aa7pqyjbbi8w4mpw24m8zp40scyd6yxipbsrflzdzxf4c1w")))

(define-public crate-amq-protocol-uri-5.0.1 (c (n "amq-protocol-uri") (v "5.0.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1ks19bjlm20kc4r60ksv7ay6c48k16hjj9pr83p4rl4zgq8g8755")))

(define-public crate-amq-protocol-uri-5.1.0 (c (n "amq-protocol-uri") (v "5.1.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "16li3rgx71nfj57hnrylk3r1ggrnihb7szd0ifpwmnkpgx3whyxy")))

(define-public crate-amq-protocol-uri-6.0.0-alpha1 (c (n "amq-protocol-uri") (v "6.0.0-alpha1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "179r8zbz1ynfsb5h6x5f7y7m3vaqnssr3vy1bpfn9vgyjihyk042")))

(define-public crate-amq-protocol-uri-6.0.0-alpha2 (c (n "amq-protocol-uri") (v "6.0.0-alpha2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1wbcpa93raj8vwfmxp07r1yv49q8x4bj0lyvvfjkzsp5jiansdzi")))

(define-public crate-amq-protocol-uri-6.0.0-alpha3 (c (n "amq-protocol-uri") (v "6.0.0-alpha3") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1ag8x6sdp8shy3b7p2n6vv2y0rv19kp0nzlsr5020d99mkmqh2aq")))

(define-public crate-amq-protocol-uri-6.0.0-alpha4 (c (n "amq-protocol-uri") (v "6.0.0-alpha4") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1n3zz7lhizginjvlwb7c0k079siblicdlsypwl512ifg45f98bbl")))

(define-public crate-amq-protocol-uri-6.0.0-alpha5 (c (n "amq-protocol-uri") (v "6.0.0-alpha5") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0zldl494hrzaniyp0k8wjqc9bd1hjxkbr4ngj0scci1rvdwvbc1q")))

(define-public crate-amq-protocol-uri-6.0.0-alpha6 (c (n "amq-protocol-uri") (v "6.0.0-alpha6") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1fq1i33c1k3d4aqcxgvzx26k26yans0qw2rcmdihh7g7lmlhzjl7")))

(define-public crate-amq-protocol-uri-6.0.0-beta1 (c (n "amq-protocol-uri") (v "6.0.0-beta1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0wcwjvpib08357vas6ib9gshbi3c6cnmlvvh6s312k4mq6n9f2lb")))

(define-public crate-amq-protocol-uri-6.0.0-beta2 (c (n "amq-protocol-uri") (v "6.0.0-beta2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "18mdw8abvj25yvxiahvfic3wchpd816w0axi0gdbif59dfkqfrd5")))

(define-public crate-amq-protocol-uri-6.0.0-beta3 (c (n "amq-protocol-uri") (v "6.0.0-beta3") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1yms6ir2lxwdzpy0717m54sg6fd8llgybfjmnmq2z6d4mlqq8040")))

(define-public crate-amq-protocol-uri-6.0.0-beta4 (c (n "amq-protocol-uri") (v "6.0.0-beta4") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0z8zc4ylg8pkbm23vb600wvrdf1vswl9z680l4wyi5y8hz01lw9r")))

(define-public crate-amq-protocol-uri-6.0.0-beta5 (c (n "amq-protocol-uri") (v "6.0.0-beta5") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0qry8dbjbnc7mxrisllkrxywlvi236vp8kl8s59r98kyfffpdlj8")))

(define-public crate-amq-protocol-uri-6.0.0-beta6 (c (n "amq-protocol-uri") (v "6.0.0-beta6") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1jiilgykdakb7cjgqpf5sv52kx63ndmivb957nq8q7j2hva1ad9c")))

(define-public crate-amq-protocol-uri-6.0.0-rc1 (c (n "amq-protocol-uri") (v "6.0.0-rc1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "16pqkp9vgv4ppqd13kp8w5b1r76d4l6j3sx2d2zbjljac6nb1b98")))

(define-public crate-amq-protocol-uri-6.0.0-rc2 (c (n "amq-protocol-uri") (v "6.0.0-rc2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "003ysnk1s9gs5071h870z5irzqyf13x60g9g48mmzg20ak5pd453")))

(define-public crate-amq-protocol-uri-6.0.0-rc3 (c (n "amq-protocol-uri") (v "6.0.0-rc3") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1r57v1i9wy5wr22bc6kfyz22lbhifarlbss415gq40lcwidvjlcm")))

(define-public crate-amq-protocol-uri-6.0.0-rc4 (c (n "amq-protocol-uri") (v "6.0.0-rc4") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "05dagkfmvhyknrwcv8hdw1shkl0wvv1pwllx54kdivc3hdl9b3x1")))

(define-public crate-amq-protocol-uri-6.0.0-rc5 (c (n "amq-protocol-uri") (v "6.0.0-rc5") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "13i4xbili0k5rk3fk4nkdpkbnrvrngnm1pd4jcg2ji6ahy287n5d")))

(define-public crate-amq-protocol-uri-6.0.0-rc6 (c (n "amq-protocol-uri") (v "6.0.0-rc6") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "15d5hs1fmg6pdwmawgrjhk4398s71gr2npxp5dw747q80ns11xf9")))

(define-public crate-amq-protocol-uri-6.0.0-rc7 (c (n "amq-protocol-uri") (v "6.0.0-rc7") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "05fj5an4ixfgl7kh4vf55xaa2v6grj6bpxcf4wqx4klff11172bv")))

(define-public crate-amq-protocol-uri-6.0.0-rc8 (c (n "amq-protocol-uri") (v "6.0.0-rc8") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "18d9bv7ka3gdgbscl26binwxsij0g629hhkmsmiwsh9rm3xz6vmv")))

(define-public crate-amq-protocol-uri-6.0.0-rc9 (c (n "amq-protocol-uri") (v "6.0.0-rc9") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0xn54qdw7iil323gjk84s7lyilc3rn5vd520ic4f6i0yay50q54q")))

(define-public crate-amq-protocol-uri-6.0.0-rc10 (c (n "amq-protocol-uri") (v "6.0.0-rc10") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1l2v3z70k6wg32zrakq4040vpqddqjzplchww944izh9q3r45wjl")))

(define-public crate-amq-protocol-uri-6.0.0-rc11 (c (n "amq-protocol-uri") (v "6.0.0-rc11") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0s1nl4rqmxmg8v52pqax7pv2zwrc1ayf5gppz32c1ny12cc4ykyc")))

(define-public crate-amq-protocol-uri-6.0.0-rc12 (c (n "amq-protocol-uri") (v "6.0.0-rc12") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "14y5vlcmrqrf1qzz7h8klw3khm8xcypjgkswi62drvv8il4a611x")))

(define-public crate-amq-protocol-uri-6.0.0 (c (n "amq-protocol-uri") (v "6.0.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0zlj9sb7k039saqvxr0s6rzg0vws0jm9z15wbbpwjjc3yfikfci8")))

(define-public crate-amq-protocol-uri-6.0.1 (c (n "amq-protocol-uri") (v "6.0.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1fik7xv824myl36xcrhgvclisa37dvh4cm0rzfa9fvyhgsw8dhav")))

(define-public crate-amq-protocol-uri-6.0.2 (c (n "amq-protocol-uri") (v "6.0.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0zh4w029kcgn5sjxry8rjjmqwym474cb9p66infpxvwgdy00697q")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.1 (c (n "amq-protocol-uri") (v "7.0.0-alpha.1") (d (list (d (n "percent-encoding") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "url") (r ">=2.0.0, <3.0.0") (d #t) (k 0)))) (h "0minjkqhs5jwhqfwn4gkqxjsp95r80j4vdy42zmggnl3khl2vizk")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.2 (c (n "amq-protocol-uri") (v "7.0.0-alpha.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "08nz95crf5i2x1jlpg40k9qsknjj5z57lxagz2c0w4agmnwmfl14")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.3 (c (n "amq-protocol-uri") (v "7.0.0-alpha.3") (d (list (d (n "amq-protocol-types") (r "=7.0.0-alpha.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "116l2n2ii1qk2s8zdn4hafk6mggqn2h0grx58va3ghfvqqhbsd22")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.4 (c (n "amq-protocol-uri") (v "7.0.0-alpha.4") (d (list (d (n "amq-protocol-types") (r "=7.0.0-alpha.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0r740f8xy5pd8143hyk3ahrcsc8g4dshkcji9mm2dp8rdcs58s6c")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.5 (c (n "amq-protocol-uri") (v "7.0.0-alpha.5") (d (list (d (n "amq-protocol-types") (r "=7.0.0-alpha.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1ff1lzsjdbmn580fq5lcwl31vrpd0zamz2z8alfrsh1b1fddlrx1")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.6 (c (n "amq-protocol-uri") (v "7.0.0-alpha.6") (d (list (d (n "amq-protocol-types") (r "=7.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0chz7pznppxsjz000ckfx825xwxyscawrq5c71y398csq9wwrayh")))

(define-public crate-amq-protocol-uri-6.0.3 (c (n "amq-protocol-uri") (v "6.0.3") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0idx883dc6ddasn7h9ivimcr6vp90majn5plrjxwwrls3z73x14z")))

(define-public crate-amq-protocol-uri-7.0.0-alpha.7 (c (n "amq-protocol-uri") (v "7.0.0-alpha.7") (d (list (d (n "amq-protocol-types") (r "=7.0.0-alpha.7") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0ka03vx8r86xlnfs7j7yvcjbhy7lfjyp2nri265zv04sm0ky9j5r")))

(define-public crate-amq-protocol-uri-6.1.0 (c (n "amq-protocol-uri") (v "6.1.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "15nsj45rjmhq5i9r132r68swlphcwnp5w7491126fps9m6pmk05h")))

(define-public crate-amq-protocol-uri-7.0.0-beta.1 (c (n "amq-protocol-uri") (v "7.0.0-beta.1") (d (list (d (n "amq-protocol-types") (r "=7.0.0-beta.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "07ajxknyr9i0zwh0s6lknvrmji96z48562hfz7w3zhljrb7wwimv")))

(define-public crate-amq-protocol-uri-7.0.0 (c (n "amq-protocol-uri") (v "7.0.0") (d (list (d (n "amq-protocol-types") (r "=7.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0hgbqlk4sm3lamz446fnnl6n6brhgg91gnp4lazli3125br9nqx4") (r "1.56.0")))

(define-public crate-amq-protocol-uri-7.0.1 (c (n "amq-protocol-uri") (v "7.0.1") (d (list (d (n "amq-protocol-types") (r "=7.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1y555j5qlcy3qpn7civlv3nlwmlijb6rd25f1i8fplj8pw47362n") (r "1.56.0")))

(define-public crate-amq-protocol-uri-7.1.0 (c (n "amq-protocol-uri") (v "7.1.0") (d (list (d (n "amq-protocol-types") (r "=7.1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0pkavd73y2f7pz5pz64vqx8hzwbhk504597j60rv93qsfpk4jdns") (r "1.63.0")))

(define-public crate-amq-protocol-uri-7.1.1 (c (n "amq-protocol-uri") (v "7.1.1") (d (list (d (n "amq-protocol-types") (r "=7.1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "155ix7ihmb989rqzd0wwgbsim35dgdb4r962ak2fl315a39g2vss") (r "1.63.0")))

(define-public crate-amq-protocol-uri-7.1.2 (c (n "amq-protocol-uri") (v "7.1.2") (d (list (d (n "amq-protocol-types") (r "=7.1.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1r3kr9zfg3alikagpayghsmwdz9i3dpmfh776di0cxh58iyvs6vm") (r "1.63.0")))

(define-public crate-amq-protocol-uri-7.2.0 (c (n "amq-protocol-uri") (v "7.2.0") (d (list (d (n "amq-protocol-types") (r "=7.2.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1w0drqi4arj87k5anab80ayx3w3i1gjs6a1z81qkxasxayvq2y7i") (r "1.63.0")))

