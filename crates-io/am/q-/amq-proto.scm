(define-module (crates-io am q- amq-proto) #:use-module (crates-io))

(define-public crate-amq-proto-0.1.0 (c (n "amq-proto") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1lslvj0d1k4qfzx8h5kl3ajia5i2zz13qs0jdh0cfx0znwwrdmv6")))

