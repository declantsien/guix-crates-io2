(define-module (crates-io am q- amq-protocol-types) #:use-module (crates-io))

(define-public crate-amq-protocol-types-0.1.0 (c (n "amq-protocol-types") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)))) (h "03kal4sa4r0g7fpf5j30nl0zhq3pbjaq4k56l1imbpsagfmr0hvv")))

(define-public crate-amq-protocol-types-0.1.1 (c (n "amq-protocol-types") (v "0.1.1") (d (list (d (n "serde") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)))) (h "1bmb7lim361nmcrgcsp3i825mh5n099xq15hml7c2x9z79zlpkdr")))

(define-public crate-amq-protocol-types-0.2.0 (c (n "amq-protocol-types") (v "0.2.0") (d (list (d (n "serde") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.10") (d #t) (k 0)))) (h "0gqb7qsgd6jinlh54yf8dyk3jj5mgysadqm8qng78xs9hq4296dq")))

(define-public crate-amq-protocol-types-0.2.1 (c (n "amq-protocol-types") (v "0.2.1") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "0bncqrcm5sajz2kjbrhy820nzfrp51wvyjb5hy5ykq46p3i43700")))

(define-public crate-amq-protocol-types-0.2.2 (c (n "amq-protocol-types") (v "0.2.2") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "1rh7k0gijqs6zsxsvzyrmxikalzkybil8badfclj96dk684pqrvc")))

(define-public crate-amq-protocol-types-0.3.0 (c (n "amq-protocol-types") (v "0.3.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "035faq3hbp4zx4m6y174a1qdba78ksgsss5m134nyx6xb913yva4")))

(define-public crate-amq-protocol-types-0.3.1 (c (n "amq-protocol-types") (v "0.3.1") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "1hh2j2168k8n6fg698vh5lzpksnjkzibv5ihzka6bkbsmv19dsaz")))

(define-public crate-amq-protocol-types-0.4.0 (c (n "amq-protocol-types") (v "0.4.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "1fk4pyg33p2jnilzh8s2prvpa2m272xgyp4rp94lq0a6v7n00rq0")))

(define-public crate-amq-protocol-types-0.5.0 (c (n "amq-protocol-types") (v "0.5.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "045xi9g12r3nq581zqx97nj98bhbp1i7c19656s8lq57qkqwb9iv")))

(define-public crate-amq-protocol-types-0.6.0 (c (n "amq-protocol-types") (v "0.6.0") (d (list (d (n "cookie-factory") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0xahpb1ipylvy5nlx14bj7pzg3d1zi860qqsrwki6ak3xk8qd2g0")))

(define-public crate-amq-protocol-types-0.7.0 (c (n "amq-protocol-types") (v "0.7.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0yja1zvn0yp1m1h19zf92qpg4l00ah0ph66sca7rgh5dp49vydlq")))

(define-public crate-amq-protocol-types-0.8.0 (c (n "amq-protocol-types") (v "0.8.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0ykq3ablyrjlm7pqsl7khfkjwg81czc0d1v017v1v82318jl7i6w")))

(define-public crate-amq-protocol-types-0.9.0 (c (n "amq-protocol-types") (v "0.9.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "001wxg83rfyk3w6ssmlbd5hy300adaa0siqjzgmb3dsm9hb194pa")))

(define-public crate-amq-protocol-types-0.10.0 (c (n "amq-protocol-types") (v "0.10.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "13gzs6ic97aqlgh44iy57j5lxcr6fws8pr5gqwxlpgcjlz2b19hk")))

(define-public crate-amq-protocol-types-0.11.0 (c (n "amq-protocol-types") (v "0.11.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0qa2vcly92c3d7n0n7nihdhkgwgc85wqjvcijbhrlm67l5jb0y9a")))

(define-public crate-amq-protocol-types-0.12.0 (c (n "amq-protocol-types") (v "0.12.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0xc444av96frvqddvrzc1x8yxnlnszxdr3xq1gbanqh6fv85aw4n")))

(define-public crate-amq-protocol-types-0.13.0 (c (n "amq-protocol-types") (v "0.13.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0ila5sy60abm9gwqwv23sfhmihl4271iqi0zqgm49fdzvrm7m5dv")))

(define-public crate-amq-protocol-types-0.14.0 (c (n "amq-protocol-types") (v "0.14.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "1sgjsg4dxlgh88jyvbm1n32ni5z0rcjzkpz82bfh3x7klyi03kcg")))

(define-public crate-amq-protocol-types-0.15.0 (c (n "amq-protocol-types") (v "0.15.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0vb4zg68c2r9595z9ag7dcc1vqsklgj23af6zg9v7x1k3kk4rnf1")))

(define-public crate-amq-protocol-types-0.16.0 (c (n "amq-protocol-types") (v "0.16.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "028vri8n08b2nj0zlhxdn4d50b9isf3nfj083j6xadaljs6ig404")))

(define-public crate-amq-protocol-types-0.17.0 (c (n "amq-protocol-types") (v "0.17.0") (d (list (d (n "cookie-factory") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0bl1v1biiynh55dxc9g8w9rfy8h414h9lrn6fmqg64gmrnsqwy34")))

(define-public crate-amq-protocol-types-0.18.0 (c (n "amq-protocol-types") (v "0.18.0") (d (list (d (n "cookie-factory") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i7lys2fkw6sda3bx4sbpr4rdnl5rqb290gk7h6yyff9570ia885")))

(define-public crate-amq-protocol-types-0.19.0 (c (n "amq-protocol-types") (v "0.19.0") (d (list (d (n "cookie-factory") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0014w5cqgx5vhqsmvm3x1s3s5sarg9jmjy8ng2g7jjlb39y1r5nw")))

(define-public crate-amq-protocol-types-0.20.0 (c (n "amq-protocol-types") (v "0.20.0") (d (list (d (n "cookie-factory") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aa667mjqvx5dmnzvvz9ygvp2pfwdhrwzc941yjhdxi73nmszdd4")))

(define-public crate-amq-protocol-types-1.0.0 (c (n "amq-protocol-types") (v "1.0.0") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c0cqjzlx0xck3wrh90li906a0v8qkw16gsacrj1akvdz5vzi4s7")))

(define-public crate-amq-protocol-types-1.1.0 (c (n "amq-protocol-types") (v "1.1.0") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xjpbl4581i9pcjzqq1393y1jnb6sv9r9jlrl442kiall7r8v7bh")))

(define-public crate-amq-protocol-types-1.1.1 (c (n "amq-protocol-types") (v "1.1.1") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0abhiq3208430fr9cq6l3snf1icyh1dkxlij3k9l7z30ywr4wqbq")))

(define-public crate-amq-protocol-types-1.2.0 (c (n "amq-protocol-types") (v "1.2.0") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1slz0208kskmcifsfxmbmvp3rkcjwz01fn9mrpm4b6wydhfdamv8")))

(define-public crate-amq-protocol-types-2.0.0-beta1 (c (n "amq-protocol-types") (v "2.0.0-beta1") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jhycvv2vxy4xhh9njgi5j9ik20wg1gs0r05z9l5hlqc31rnvhbx")))

(define-public crate-amq-protocol-types-2.0.0-beta2 (c (n "amq-protocol-types") (v "2.0.0-beta2") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xx60fay9wxald72f1f33i9699am4plw0fpg4w4cmpqq6n0kym8g")))

(define-public crate-amq-protocol-types-2.0.0-beta3 (c (n "amq-protocol-types") (v "2.0.0-beta3") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0drpykynr0njyk267pg9m5ac6r5zqmqssm1rl0h597yhlnwafs92")))

(define-public crate-amq-protocol-types-2.0.0-beta4 (c (n "amq-protocol-types") (v "2.0.0-beta4") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fk6d52l62kn61p5mzavi5mr22q6gdkq8vd9pcpp1kv598n92ga7")))

(define-public crate-amq-protocol-types-2.0.0-beta5 (c (n "amq-protocol-types") (v "2.0.0-beta5") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a85qy8kcg31619zr9f6x5rq69hj8aql4ymmfq4c8bzpx3p5g6cs")))

(define-public crate-amq-protocol-types-2.0.0-beta6 (c (n "amq-protocol-types") (v "2.0.0-beta6") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13dkb0mcfkkyi24bccvyjvv2jdx31m7y4c33kf0vvrj45ydd8hnh")))

(define-public crate-amq-protocol-types-2.0.0-rc1 (c (n "amq-protocol-types") (v "2.0.0-rc1") (d (list (d (n "cookie-factory") (r "^0.2.4") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wakk91b4r58jhlyxlb8kcb2dfwlmy2ywyj173qwk4bfsr6rga2y")))

(define-public crate-amq-protocol-types-2.0.0-rc2 (c (n "amq-protocol-types") (v "2.0.0-rc2") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta1") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fax4m26w5l39z3n2qf8l9r2awp2wmrmzb8q3xvxhgmhbxspafmy")))

(define-public crate-amq-protocol-types-2.0.0-rc3 (c (n "amq-protocol-types") (v "2.0.0-rc3") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta1") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ma3ywjinp7f6mlfw8mvjnn78q57g3w97a4x9yszc84dwdci19nf")))

(define-public crate-amq-protocol-types-2.0.0-rc4 (c (n "amq-protocol-types") (v "2.0.0-rc4") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta1") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i5gpssmvdqgqyz78jv14cap887bqw8vqf34wv0mbqvzljrfa8ln")))

(define-public crate-amq-protocol-types-2.0.0-rc5 (c (n "amq-protocol-types") (v "2.0.0-rc5") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta1") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10gzirnml0zpjdrr4m4h7982ny55g87m6cq10629hsc2vkmrbw75")))

(define-public crate-amq-protocol-types-2.0.0-rc6 (c (n "amq-protocol-types") (v "2.0.0-rc6") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta1") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d2mki6sj46zkd3x6nznwx8djhqvcjg2vjbxpiv6ym81syi8i17n")))

(define-public crate-amq-protocol-types-2.0.0-rc7 (c (n "amq-protocol-types") (v "2.0.0-rc7") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m2h19c1yn5lnkyc3bigam712nzmkgfvh1brd9jybqzaq2i7z6xs")))

(define-public crate-amq-protocol-types-2.0.0-rc8 (c (n "amq-protocol-types") (v "2.0.0-rc8") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xxlr9i88vyv9h1qilg9n84mwiiav843ijbl00q29ffr8li1v679")))

(define-public crate-amq-protocol-types-2.0.0-rc9 (c (n "amq-protocol-types") (v "2.0.0-rc9") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m1psgb4qj5iidklm7wcvw72dg88r9cy3j0317ajgvanwmdjpnym")))

(define-public crate-amq-protocol-types-2.0.0-rc10 (c (n "amq-protocol-types") (v "2.0.0-rc10") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rkjjjcrxbf3s9kfb1a8xh6mcs5v4281zrkxhzqfnzqxymrnhk7a")))

(define-public crate-amq-protocol-types-2.0.0-rc11 (c (n "amq-protocol-types") (v "2.0.0-rc11") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rqi7dbm31qm0ndpx24zgpsx1b592c3chqrzzn761hlcvrjay4lr")))

(define-public crate-amq-protocol-types-2.0.0-rc12 (c (n "amq-protocol-types") (v "2.0.0-rc12") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lmg1563vcsk6m32dlvw47siwm2yknc99arbj06ggh3jn09q60k5")))

(define-public crate-amq-protocol-types-2.0.0-rc13 (c (n "amq-protocol-types") (v "2.0.0-rc13") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a06w9pmgcnxd5d2g1fzpdgz1sjgmqjiqvpb7wiz17dl0k3l02ab")))

(define-public crate-amq-protocol-types-2.0.0-rc14 (c (n "amq-protocol-types") (v "2.0.0-rc14") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1raxmrjh9midcxnpshjypvbic3h0jgyhl9v73dj9j9iz70hfx2lv") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.0-rc15 (c (n "amq-protocol-types") (v "2.0.0-rc15") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w9jymjfh7i1zj4laq4ghgbxlwjlsdrhyf59bqklazljj1hgskyq") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.0-rc16 (c (n "amq-protocol-types") (v "2.0.0-rc16") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jssnnna79a7ai14zrwan1vg8cggqwjabyyxwyar2mn3yy8z1463") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.0-rc17 (c (n "amq-protocol-types") (v "2.0.0-rc17") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n2bypkvhm8c5c85cdjir6q1rwqzrdxp37rldys8wmgglqa1m0f2") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.0-rc18 (c (n "amq-protocol-types") (v "2.0.0-rc18") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p40p0rmf1s7jf0k7k6iwdynl5qn6m62ann40lqx2ij9xyghmb9i") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.0 (c (n "amq-protocol-types") (v "2.0.0") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b8cypg5j6mk5dcyimz5hzvxds5qs5nqy8sigr3r1yi04llqq2vd") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.0.2 (c (n "amq-protocol-types") (v "2.0.2") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "= 5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03bajcpan2ycbjsvrvfnkc63nd3msq5pdz788k70yh6jldv7y24r") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.1.0 (c (n "amq-protocol-types") (v "2.1.0") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xw6xhvmgd3ac89r02f5yzwgs9bv6905f36ay0chvhmaw1a3m1gf") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.1.1 (c (n "amq-protocol-types") (v "2.1.1") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0knkhzjnkjxa0ypmznffr5cqc0qayf5kkbbbvr8j3j5v9j6hywxy") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.1.2 (c (n "amq-protocol-types") (v "2.1.2") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a9ikb46v2lf22vl9krc4grnb1315bwpl109am3g19v0d4ccgs3l") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.2.0 (c (n "amq-protocol-types") (v "2.2.0") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18fb3nm6qawasanz1ls8ii96ld8gchzqjgc77l7ij4mwg7nrgci5") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-3.0.0-beta.1 (c (n "amq-protocol-types") (v "3.0.0-beta.1") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta5") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vvb3v987f1i97syx9zmlkdh420yfyfqnv7fc1hziy1djhjslgdy") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-3.0.0-beta.2 (c (n "amq-protocol-types") (v "3.0.0-beta.2") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta5") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nh88fwif7wa5nxp5mmafs3h178hnd3gvnxai0ma1gab2fsbyr2m") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-2.3.0 (c (n "amq-protocol-types") (v "2.3.0") (d (list (d (n "cookie-factory") (r "= 0.3.0-beta2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z6h38dlhlxki9a9z8mvck34f0m0ywpcdd9pg1n5idhdqfmgpz3x") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-3.0.0 (c (n "amq-protocol-types") (v "3.0.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1184xz4f5lknn7bnmq9ibrsah63zdr4lxkbrpqml157hgf1fai6x") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-3.1.0 (c (n "amq-protocol-types") (v "3.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03sxyx347a0kxfy2dy4s5m11j0lyml7g23n82zrcgalik2x8jd51") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-3.1.1 (c (n "amq-protocol-types") (v "3.1.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mk1vnhbd52cl48kl0yww2yizi0h0j661si2aa1ihxx65rr75i2f") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-4.0.0 (c (n "amq-protocol-types") (v "4.0.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hrb4l9a6qxknk29gm44nydazqm2kgnnm6kc79pgiw0h2r8l50f5") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-4.1.0 (c (n "amq-protocol-types") (v "4.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yfyzl472inb8g020pjmwmknd1bgyzm3zzlh73h0hcz5nifcilyg") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-4.2.0 (c (n "amq-protocol-types") (v "4.2.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pr9bf1c2j00rxzlgzvwzlng880mnq4ccqnhmclg1g79hhs2k08d") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.0-beta.1 (c (n "amq-protocol-types") (v "5.0.0-beta.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s1ny8iqymkgfx243zhkg73pkjasynv4a5kkd8is7d8idxbj2d22") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.0-beta.2 (c (n "amq-protocol-types") (v "5.0.0-beta.2") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0digkx4l9cd5bzrvw9z9gz04zjcaj2v5pxrdnwkd1cf6jv293gvx") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.0-beta.3 (c (n "amq-protocol-types") (v "5.0.0-beta.3") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0svg86zgnsn2hqzgmgxc9i4qnhx8r2j46cia04cblirhp7wifin9") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-4.2.1 (c (n "amq-protocol-types") (v "4.2.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06gznnybzhpwfrhra04pkxpl9b5f24rywjlnaicxrww3vy6jmanb") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-4.2.2 (c (n "amq-protocol-types") (v "4.2.2") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05r3yzx5pwffc5yzsnllizzlg7r2acf29ckzvpayqw0vvxpq6i8a") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.0-beta.4 (c (n "amq-protocol-types") (v "5.0.0-beta.4") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "133vgg2pnw3ay68fzpy5rd4r2j9pm6ncxq9k4zh1z3kgrghvif1j") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.0 (c (n "amq-protocol-types") (v "5.0.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i1z1sx6hibvy3qs9sv7ngk29y2vny06cjamb1j21j29s89ikc7c") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.0.1 (c (n "amq-protocol-types") (v "5.0.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cp1g165nnkl43mgidnhpi8ggi3948ip3cj5gj8b7a3ki083434m") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-5.1.0 (c (n "amq-protocol-types") (v "5.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12w8p9f35128rglnkswll913adg4ip3cg33cihs8fkd9958z36z0") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha1 (c (n "amq-protocol-types") (v "6.0.0-alpha1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nd8swcjcnvhmkif8z7a9gnc90xzz9ld7balbcdwis50mgw7y99l") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha2 (c (n "amq-protocol-types") (v "6.0.0-alpha2") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f5ycj9gciamcyvbdi3nr195xj9sd6qbc45n6hcdgg9j53j9625h") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha3 (c (n "amq-protocol-types") (v "6.0.0-alpha3") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "144391iaizk5d4lpddj0in9j5dpkhqf7jjcnyynaxjmd0wvzylvf") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha4 (c (n "amq-protocol-types") (v "6.0.0-alpha4") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qdhq8rwakji20li64y4gnnyzwd275qs4djwwc86vgwkm87a4fma") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha5 (c (n "amq-protocol-types") (v "6.0.0-alpha5") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ap5wjjbw0cgsqpwilvdd67cj2fb1b45gs40i6399q3s0hd4jjpj") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-alpha6 (c (n "amq-protocol-types") (v "6.0.0-alpha6") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fw5qrs9nj8qas7645wpagszf36y9q4nq0ffg5q5gnhpqpxqzvvj") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta1 (c (n "amq-protocol-types") (v "6.0.0-beta1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gri0xkj416c3r0mcd5rbk6b8hsg9nsbhw4by3x21ancpgrii4v4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta2 (c (n "amq-protocol-types") (v "6.0.0-beta2") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v4lvs3w8glmz6gf4rglifi5svi79g64dzqx9vbfs9ywc5kf6vly") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta3 (c (n "amq-protocol-types") (v "6.0.0-beta3") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14qink8f2j974dd0hlvkzz7i38fyc3vzknbj1g4js4m8lnqgm91n") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta4 (c (n "amq-protocol-types") (v "6.0.0-beta4") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z10pmviwsay64b923hzqn59yv02jjhikqrbnachkfmz16lwi0xg") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta5 (c (n "amq-protocol-types") (v "6.0.0-beta5") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02n3xbmrlbz0b3m07rbs266f1zsnjzm9cwh81dxddlawq45bq096") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-beta6 (c (n "amq-protocol-types") (v "6.0.0-beta6") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lnlcr0jwyjk8c96ibf74qb0d3cibwy61iibchhba3s5a0cnxncy") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc1 (c (n "amq-protocol-types") (v "6.0.0-rc1") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sr53qxnkrpjabl0xzzjd7rzlspcl7vx6pw76almps16izh5v59d") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc2 (c (n "amq-protocol-types") (v "6.0.0-rc2") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f6c3ihjzbsl57hgjd5fyvmyzkd46vp8lgbq5sbckby7xdnjc7qi") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc3 (c (n "amq-protocol-types") (v "6.0.0-rc3") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pqk3xqaggwd400l0nyjbc7vqn9dvnah7bmb688vz6h6lqmyh9a4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc4 (c (n "amq-protocol-types") (v "6.0.0-rc4") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i60azpb6adcqj6a0946ys5jck8nv2ixwmwfar57y7siw7a78kpv") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc5 (c (n "amq-protocol-types") (v "6.0.0-rc5") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dbfajsfcj7rmdvs108269031b15zf97gfr5jm4yw1xnz0zincd4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc6 (c (n "amq-protocol-types") (v "6.0.0-rc6") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h745hc93klb9rg89cx1m4ipd3djhwiz4vqfb5j8dm4nqb13xds0") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc7 (c (n "amq-protocol-types") (v "6.0.0-rc7") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15dsxydv32g3j9sf0mfpqi8sa85ma3m5vwv2yi1hq80nd61w1qh0") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc8 (c (n "amq-protocol-types") (v "6.0.0-rc8") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ppgjxb5gjhb5aaaxadvxi2k28cdfcag12jjmrx6vivcc2ivx06") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc9 (c (n "amq-protocol-types") (v "6.0.0-rc9") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "= 6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s64hgvp737pkv5j03zy63yfbxb8rlr3p8byxagds78wj5bm0s43") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc10 (c (n "amq-protocol-types") (v "6.0.0-rc10") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0566l12jfs7c51x5rq7dcn990ppff4z6yvikr84l8n8lg4n23lz4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc11 (c (n "amq-protocol-types") (v "6.0.0-rc11") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d0f6f658kxa03i0l0fh0nipnbbs12dazlr3lwnsrd870vz6w23p") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0-rc12 (c (n "amq-protocol-types") (v "6.0.0-rc12") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07sc2k2g5nvrp90q95sq4mb794ykcjs9krc2sbc0izvs5dfj9fkr") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.0 (c (n "amq-protocol-types") (v "6.0.0") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0629kn7714mpk8y477lbn82h57ap638hj49qm8lxvs3jy9k5jpf4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.1 (c (n "amq-protocol-types") (v "6.0.1") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fsayl67vs34hnsy10l88fh4rzb0k3m77d2hbfrp57m8la194kkv") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.2 (c (n "amq-protocol-types") (v "6.0.2") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00bw0pywmjxhipilyh644hpinyqakfx3kmh2ay5z9zz5pdcy3svk") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.1 (c (n "amq-protocol-types") (v "7.0.0-alpha.1") (d (list (d (n "cookie-factory") (r ">=0.3.0, <0.4.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r ">=6.0.0, <7.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "03n1z8ihq1nvdvm7vr5x45f5n6c4jdzy4nr7gw2zl7d569bgwag2") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.2 (c (n "amq-protocol-types") (v "7.0.0-alpha.2") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jh6ini4hfmjsds5lnvm9fm7zjwzyyq9805m6688arlc6a40c0ih") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.3 (c (n "amq-protocol-types") (v "7.0.0-alpha.3") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "017ddx3b112isac9ip2bs6s8jmq6fi3l7is2ra65p3vw6n77810p") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.4 (c (n "amq-protocol-types") (v "7.0.0-alpha.4") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19wl33d0azbg6pnx607292ppf7sfr1kn4p43sv3xllbsycypwk1j") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.5 (c (n "amq-protocol-types") (v "7.0.0-alpha.5") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h5hnwvhcgs27g65jvm4w3yq6r9pdrr20f0kd2r1r9sxs0im649y") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.6 (c (n "amq-protocol-types") (v "7.0.0-alpha.6") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "178w4ix5vnrlajyx4nqgjp26zyx8bx9jh23sc4rp6grj4dwmcd74") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.0.3 (c (n "amq-protocol-types") (v "6.0.3") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r53m0flc83k3lc2ixsfps5bywgl7w8hskvfjhddzsp0ppjcv66i") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-alpha.7 (c (n "amq-protocol-types") (v "7.0.0-alpha.7") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0igg6z6y8gwb152pffxql0mi6l0dm9sldf2gl8lhk8hjzjzaxdds") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-6.1.0 (c (n "amq-protocol-types") (v "6.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pmhdyxnkq2v6z8f7sfp0sakgb2v71g28853x1gm6dr1jdkbg302") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0-beta.1 (c (n "amq-protocol-types") (v "7.0.0-beta.1") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ryjmgbnxrhg578hhpj011pxd4caxhr100f1x9mgrm9app02kmy4") (f (quote (("verbose-errors"))))))

(define-public crate-amq-protocol-types-7.0.0 (c (n "amq-protocol-types") (v "7.0.0") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bm9bjrnlvfrhlxx888vapcqd5yr3jpj9sq6pypc8m3ch01c7pfw") (f (quote (("verbose-errors")))) (r "1.56.0")))

(define-public crate-amq-protocol-types-7.0.1 (c (n "amq-protocol-types") (v "7.0.1") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05417f6inrcws42758z2p5p2d0mk9h3i6061z2snssiv13ly0ig2") (f (quote (("verbose-errors")))) (r "1.56.0")))

(define-public crate-amq-protocol-types-7.1.0 (c (n "amq-protocol-types") (v "7.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11d4h170nk42yy98a34l2ph0443jdp8cr7x4rb9ik29z4pya54bq") (f (quote (("verbose-errors")))) (r "1.63.0")))

(define-public crate-amq-protocol-types-7.1.1 (c (n "amq-protocol-types") (v "7.1.1") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wwqdjazmz2yipwh2vsdvbsa7lc48jcan9nxyb78075jhcq4cvn6") (f (quote (("verbose-errors")))) (r "1.63.0")))

(define-public crate-amq-protocol-types-7.1.2 (c (n "amq-protocol-types") (v "7.1.2") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zyssc3yyz42cs2dj3fhd5h2n97659m85val9q5n1v9wi8yg2vqm") (f (quote (("verbose-errors")))) (r "1.63.0")))

(define-public crate-amq-protocol-types-7.2.0 (c (n "amq-protocol-types") (v "7.2.0") (d (list (d (n "cookie-factory") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v5zr4ka4mf2wi6v4n46ipj8s0n8ri1bn3gfi6ybkn6faiqd9k8a") (f (quote (("verbose-errors")))) (r "1.63.0")))

