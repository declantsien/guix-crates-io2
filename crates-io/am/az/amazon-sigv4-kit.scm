(define-module (crates-io am az amazon-sigv4-kit) #:use-module (crates-io))

(define-public crate-amazon-sigv4-kit-0.1.0 (c (n "amazon-sigv4-kit") (v "0.1.0") (d (list (d (n "aws-sigv4") (r "^0.2") (f (quote ("sign-http"))) (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "1pps4kqw1jywgilk8l9swzprp0h2kh4dg81qnsj6rp4sg73y6rnc")))

