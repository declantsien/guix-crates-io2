(define-module (crates-io am az amazon-rekognition) #:use-module (crates-io))

(define-public crate-amazon-rekognition-0.1.0 (c (n "amazon-rekognition") (v "0.1.0") (d (list (d (n "amazon-sigv4-kit") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1v0ndbx4r1hrp14p985hynbw89c0lxjc7d3c2pip7d0jx9yvs90h")))

(define-public crate-amazon-rekognition-0.1.1 (c (n "amazon-rekognition") (v "0.1.1") (d (list (d (n "amazon-sigv4-kit") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "06r1kzg5pnr5r4n7qqdaki6szab0gn80fmb189325xvk9d9ray0v")))

