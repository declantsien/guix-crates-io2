(define-module (crates-io am az amazon-captcha-rs) #:use-module (crates-io))

(define-public crate-amazon-captcha-rs-0.1.0 (c (n "amazon-captcha-rs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0afcdlvg50i11gm9zbfxvdz1v03n77qkm5njkwys2545sxl1kwz1")))

(define-public crate-amazon-captcha-rs-0.2.0 (c (n "amazon-captcha-rs") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0k0yc0lhgqfkf64c7d9ads4a6iphmwivphzsawapsxm5gm64dx6v")))

(define-public crate-amazon-captcha-rs-0.2.1 (c (n "amazon-captcha-rs") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1jvlcwl1g4xpx7ddwjaikyszxbzhrhwkrvhlhr98zzhgvizifbj6")))

(define-public crate-amazon-captcha-rs-0.2.2 (c (n "amazon-captcha-rs") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z9x0xgm8ckcnflpl4cymq00xmm785x61ll6k0rjaf1q6jp63isb")))

