(define-module (crates-io am az amazon-cloudfront-client-routing-lib) #:use-module (crates-io))

(define-public crate-amazon-cloudfront-client-routing-lib-1.0.0 (c (n "amazon-cloudfront-client-routing-lib") (v "1.0.0") (d (list (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1kngp7bsll99k8hmpadwz3dvf1yczm9xyiy6b5vkqq68ih9s40pf") (r "1.63")))

