(define-module (crates-io am az amazon) #:use-module (crates-io))

(define-public crate-amazon-0.0.0 (c (n "amazon") (v "0.0.0") (h "0b9b5fg1mr9gxgra4hzj5h9pjaznb4d707hhzfl68l24qy7b024k") (y #t)))

(define-public crate-amazon-0.0.1 (c (n "amazon") (v "0.0.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06nw14g1hpacsjgi7cszn9dgfz72607wd4xxd4nf66s28mkix2qb") (y #t)))

