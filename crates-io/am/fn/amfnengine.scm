(define-module (crates-io am fn amfnengine) #:use-module (crates-io))

(define-public crate-amfnengine-0.1.0 (c (n "amfnengine") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono_locale") (r "~0.1") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "13g43i25500b3s3pq71nw09ysfxmxxpx4ihqv0wxgs3rgkdrfp9i")))

(define-public crate-amfnengine-0.1.1 (c (n "amfnengine") (v "0.1.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono_locale") (r "~0.1") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "02l1krm2cq9cnj8xn6hq8gr4pq5f14hx80gax7a38hr4ih5jv709")))

(define-public crate-amfnengine-0.2.0 (c (n "amfnengine") (v "0.2.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono_locale") (r "~0.1") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1gicl3faazlm7vjqgxarg0qsy0f236q6vd68kbcixlqgjb9z7abh")))

(define-public crate-amfnengine-0.3.0 (c (n "amfnengine") (v "0.3.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1dxnzwdx763gx8bcw7542fskyjfd5zp51p169if8h80d4kgy9j00")))

(define-public crate-amfnengine-0.3.1 (c (n "amfnengine") (v "0.3.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1fxrkz8v2zb7m2k75a4hw2rzq8bx39z47zr7cpn779nm312k9k95")))

(define-public crate-amfnengine-0.3.2 (c (n "amfnengine") (v "0.3.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0zg987005lghbcsai844zrqdzs0hk9qm0sm9pyahk35fqvaqnkbc")))

(define-public crate-amfnengine-0.3.3 (c (n "amfnengine") (v "0.3.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0far14p83l517yqbyb5r6269a1s8vz2klb0vvinb6gh1rq8dnnag")))

(define-public crate-amfnengine-0.3.4 (c (n "amfnengine") (v "0.3.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.5.5") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1arpwanq1y6l9a9mc9nzbwqvz871jqv0pf4ljfddcvs9hgqpf78v")))

(define-public crate-amfnengine-0.3.5 (c (n "amfnengine") (v "0.3.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "compare") (r "~0.1") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libmath") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~1.5.5") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0lqvknb791rsix480l455p5zicb2irg9lvzl1mscgpf7kc0llfds")))

