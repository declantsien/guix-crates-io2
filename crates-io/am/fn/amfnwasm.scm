(define-module (crates-io am fn amfnwasm) #:use-module (crates-io))

(define-public crate-amfnwasm-0.2.0 (c (n "amfnwasm") (v "0.2.0") (d (list (d (n "amfnengine") (r "~0.2") (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "1l177a6ckw0wr2l3ybim26pyv7i18jw0xk2c0qkq2whn3w8ri8k7")))

(define-public crate-amfnwasm-0.3.0 (c (n "amfnwasm") (v "0.3.0") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "1waqrh4abb7wi8lby8dplaba38h6lrsi5cvsh95sdprb1qvgqjw4")))

(define-public crate-amfnwasm-0.3.1 (c (n "amfnwasm") (v "0.3.1") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "069nzg226zjmg619bldwy7q3v0inxfsfj5fp0wi8ag2dyffrm642")))

(define-public crate-amfnwasm-0.3.2 (c (n "amfnwasm") (v "0.3.2") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "0k7gcq8675zyx1bl89rafsdnv75ymkmsgb3rqajbfzyx0apgiy0z")))

(define-public crate-amfnwasm-0.3.3 (c (n "amfnwasm") (v "0.3.3") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "07h2na2zgadgp52n44skyxwrm0q4hfqysmbw0ghk0saj8f55q2h8")))

(define-public crate-amfnwasm-0.3.4 (c (n "amfnwasm") (v "0.3.4") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "1yj81xrg08dwzgq6knga0zanf2yvqmyys4y2wwqw3ln0gdd45qcp")))

(define-public crate-amfnwasm-0.3.5 (c (n "amfnwasm") (v "0.3.5") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "0n1cmw2srvgcwvh8zdp44rvvwn9vfzxfjm30pckw2qxqkww9axp2")))

(define-public crate-amfnwasm-0.3.6 (c (n "amfnwasm") (v "0.3.6") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "0m1a0kyfyscfiwxvgw4jfh6m8ym2s3z9ksm433w4lr99cny0lrff")))

(define-public crate-amfnwasm-0.3.7 (c (n "amfnwasm") (v "0.3.7") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "1yar6kv31l7x071nbqhmd5dq8kv3xhd1sfcv5lsdi1iziwhsj8g1")))

(define-public crate-amfnwasm-0.3.8 (c (n "amfnwasm") (v "0.3.8") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "~0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "~0.2") (d #t) (k 0)))) (h "1gzd6mpk8ghirsx95n5m5nsvk3f68cn7jxal15allc1m5asjli59")))

