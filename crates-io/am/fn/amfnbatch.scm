(define-module (crates-io am fn amfnbatch) #:use-module (crates-io))

(define-public crate-amfnbatch-0.1.0 (c (n "amfnbatch") (v "0.1.0") (d (list (d (n "amfnengine") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1ddc5mdzgy5sg5dvrb4fisa99aq6gfqnmqy71idcwv1y50im62jk")))

(define-public crate-amfnbatch-0.1.1 (c (n "amfnbatch") (v "0.1.1") (d (list (d (n "amfnengine") (r "~0.1") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1vk9j46zvk5478l7r5ppzghvcirzbjx1qbq95xyjmlpqbh6sqdcj")))

(define-public crate-amfnbatch-0.2.0 (c (n "amfnbatch") (v "0.2.0") (d (list (d (n "amfnengine") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1yw1rfnmv3drgh04cfghdn2xx7b2nhmbdc0k1h5j822w5z9ziyhj")))

(define-public crate-amfnbatch-0.3.0 (c (n "amfnbatch") (v "0.3.0") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "1q1h12mz53cpgz1j7q9yicp10ysn96pv0qp900v8jnf3c6235lqq")))

(define-public crate-amfnbatch-0.3.1 (c (n "amfnbatch") (v "0.3.1") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0nfq969pbmx2lx59cl5ahbm3h1ndi21zyyq71vc9jbx58sny080a")))

(define-public crate-amfnbatch-0.3.2 (c (n "amfnbatch") (v "0.3.2") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0mplx3nd96jg93iikrn14cb07y7pp1agvcxsi4bv3ihlfhyq4jj6")))

(define-public crate-amfnbatch-0.3.3 (c (n "amfnbatch") (v "0.3.3") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "0nmpmr5qvnc00rcg8iikkj9la118mzghghwc6ibv0jfvy7bq77hq")))

(define-public crate-amfnbatch-0.3.4 (c (n "amfnbatch") (v "0.3.4") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "16aw5jzms0q84w3jy5vl87v6mvk5kc7gx1q0h8f8rq0z0qra5jdv")))

(define-public crate-amfnbatch-0.3.5 (c (n "amfnbatch") (v "0.3.5") (d (list (d (n "amfnengine") (r "~0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "~2.0") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "rust_decimal") (r "~1.10") (d #t) (k 0)))) (h "12lcyq3liajcm39krzc3cjlbf9w2bq3ai1m6nwlid34dby60i9i2")))

