(define-module (crates-io am on amongos) #:use-module (crates-io))

(define-public crate-amongos-0.11.0 (c (n "amongos") (v "0.11.0") (d (list (d (n "bootloader") (r "^0.9.23") (f (quote ("map_physical_memory"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9.0") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.5.0") (d #t) (k 0)) (d (n "pic8259") (r "^0.10.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile") (r "^0.2.6") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "01d69aay8hwsk9s7cdmz5pyvywwwcjh131ljxic470j4imdvmjp2")))

