(define-module (crates-io am on amongrust) #:use-module (crates-io))

(define-public crate-amongrust-0.1.0 (c (n "amongrust") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1sjkav9hj9v7ilvw2lxyk7s97m6f94zz2qzvvzr42d5hda8a6lwg")))

(define-public crate-amongrust-0.1.1 (c (n "amongrust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jdhph4x9d0sks33avm98nzyv4d426sij9pnmngjk8g4d8j8s1ps")))

(define-public crate-amongrust-0.1.2 (c (n "amongrust") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xv8b909xgcmhx4xr5zrf6nw9i1y894f7chzcxqdk4vm0ckzi97m")))

(define-public crate-amongrust-0.1.4 (c (n "amongrust") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0z1r0r0w17vhkfsn2g5my06jabryma4jqzpnijdj2gd50f5f6czr")))

