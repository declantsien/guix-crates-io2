(define-module (crates-io am on amongify) #:use-module (crates-io))

(define-public crate-amongify-0.1.0 (c (n "amongify") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "0ljrvrs83vpzapl5b4r4b2hpvvsk65vlj702ai7f0hwx13b92vkk")))

