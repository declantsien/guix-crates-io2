(define-module (crates-io am on among_us) #:use-module (crates-io))

(define-public crate-among_us-0.1.0 (c (n "among_us") (v "0.1.0") (h "0wrlh78aahvdvds9sly68bxlhxslgd25dshr1ig425f2b9pn3jxh")))

(define-public crate-among_us-0.2.0 (c (n "among_us") (v "0.2.0") (h "0zrvwyy7i0va15py2vf7bh09fb732nixlz2hcyafwfgnvpldyxn6")))

