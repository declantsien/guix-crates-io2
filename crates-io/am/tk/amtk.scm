(define-module (crates-io am tk amtk) #:use-module (crates-io))

(define-public crate-amtk-0.1.0 (c (n "amtk") (v "0.1.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "block-modes") (r "^0.8") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.8") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10141xa8xi00s5qbqp9nrxw2dkk9y9wmhj9rxlrds51plna0mdwf")))

