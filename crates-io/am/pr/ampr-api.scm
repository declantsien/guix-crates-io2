(define-module (crates-io am pr ampr-api) #:use-module (crates-io))

(define-public crate-ampr-api-0.1.0 (c (n "ampr-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mcw527kdba6pzmfkzcmfvhcmh8ryhs1q82rmxx3lh1yn37f904g")))

