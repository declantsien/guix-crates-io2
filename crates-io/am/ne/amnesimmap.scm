(define-module (crates-io am ne amnesimmap) #:use-module (crates-io))

(define-public crate-amnesimmap-0.1.0 (c (n "amnesimmap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)))) (h "1aq6byswnlr4frpb8xmk6nbwk24z3f0hxzkaxgmm3c05sr5j0kc0")))

