(define-module (crates-io am d6 amd64_timer) #:use-module (crates-io))

(define-public crate-amd64_timer-1.0.0 (c (n "amd64_timer") (v "1.0.0") (h "14s9fp56vv3ywndk3gs11fc444n81s6nbqfxiz01kbcdjbx6b67p")))

(define-public crate-amd64_timer-1.2.0 (c (n "amd64_timer") (v "1.2.0") (h "12gy335w7xy8lq5gdr6km6cxy4p64g55cggv7f5dbsmyqi8zjmbd")))

(define-public crate-amd64_timer-1.2.1 (c (n "amd64_timer") (v "1.2.1") (h "1b3hjxjjdhzjfqgkhd26g9nshwvsjbx63s9cq03yxlh8cws3dc4f")))

(define-public crate-amd64_timer-1.3.0 (c (n "amd64_timer") (v "1.3.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0lcbk9a0kjl5fmpbcs8f2l89n1x6fj7q3k581pv2vv2q8pclwpnf") (f (quote (("std") ("default") ("OLD_ASM"))))))

