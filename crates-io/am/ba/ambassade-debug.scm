(define-module (crates-io am ba ambassade-debug) #:use-module (crates-io))

(define-public crate-ambassade-debug-0.1.0 (c (n "ambassade-debug") (v "0.1.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 1)))) (h "16sxccyzlb2pz5vjw2p70nbgi9mify3vdk6c5smlaf9pnxnwdsb0")))

(define-public crate-ambassade-debug-0.2.0 (c (n "ambassade-debug") (v "0.2.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 1)))) (h "181pwwvl1db645gcjzicwz64hl1b05f4g9vaw0x968asnki9fjqy")))

