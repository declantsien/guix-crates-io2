(define-module (crates-io am ba amba_pl011) #:use-module (crates-io))

(define-public crate-amba_pl011-0.1.0 (c (n "amba_pl011") (v "0.1.0") (d (list (d (n "console_impl") (r "^0.1") (k 0)) (d (n "semx_processor") (r "^0.1") (k 0)) (d (n "tock-registers") (r "^0.9") (d #t) (k 0)))) (h "1h8fyn73pb2m9ybdq1wb4cfn6y4whhgbi4nr4ivsizmwq3r7vgm7") (r "1.78")))

