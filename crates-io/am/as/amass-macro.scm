(define-module (crates-io am as amass-macro) #:use-module (crates-io))

(define-public crate-amass-macro-0.1.0 (c (n "amass-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("visit" "visit-mut"))) (d #t) (k 0)) (d (n "telety") (r "^0.2") (d #t) (k 0)))) (h "103i65zggr74fyxr4ggda28dn3wwn4hqd5g1sly05zpx3x7d0z12")))

