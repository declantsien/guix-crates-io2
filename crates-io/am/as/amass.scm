(define-module (crates-io am as amass) #:use-module (crates-io))

(define-public crate-amass-0.1.0 (c (n "amass") (v "0.1.0") (d (list (d (n "amass-macro") (r "^0.1") (d #t) (k 0)) (d (n "telety") (r "^0.2") (d #t) (k 0)))) (h "1fclk656lhv43sf8vxm4xg9mfdigsvs1bd0gpikyqfrp8qpwqq0b")))

