(define-module (crates-io am an amandine) #:use-module (crates-io))

(define-public crate-amandine-0.1.0 (c (n "amandine") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1w1nqxvf9rkm3h4q45xxkypnf220zf0n9v71qip6kbm6lswbfh7c")))

(define-public crate-amandine-0.1.1 (c (n "amandine") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1mqvg92j42h4nqjqr82ckx3y3rrd6l8pdrq5sqf2b9ny6bl17aax")))

(define-public crate-amandine-0.1.2 (c (n "amandine") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "137q3nrjq375hzp3kfcgf9s6ggr05a2d2g1qhnv0c1f1cghb10z9")))

