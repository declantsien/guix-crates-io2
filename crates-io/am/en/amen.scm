(define-module (crates-io am en amen) #:use-module (crates-io))

(define-public crate-amen-0.0.1 (c (n "amen") (v "0.0.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "0z7zdngzhhi1q9m1xhk3wynd6lxzc7gidxq3ssy5vc4gjqqwn63n")))

(define-public crate-amen-0.0.2 (c (n "amen") (v "0.0.2") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "05da61j7n1c6sj1jxn8mdjfabvdn69x4frmdbsfyg65ybdhwk6z0")))

(define-public crate-amen-0.0.3 (c (n "amen") (v "0.0.3") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "05xhfg6cnczzz1h19kxy2n57l9qdjkx43szcwfdvr51h4q41c9vp")))

