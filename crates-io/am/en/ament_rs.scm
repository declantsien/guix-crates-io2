(define-module (crates-io am en ament_rs) #:use-module (crates-io))

(define-public crate-ament_rs-0.2.0 (c (n "ament_rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17iip1rz4553pq7hyyc4fi99js4iay8z7vvmwaaa8yb0m8r0p690")))

(define-public crate-ament_rs-0.2.1 (c (n "ament_rs") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0jbfrwfq1sxn16ymkyzmlxazh6k86iv0rqb38pa205kvdfadl0dr")))

