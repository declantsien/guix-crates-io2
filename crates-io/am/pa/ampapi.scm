(define-module (crates-io am pa ampapi) #:use-module (crates-io))

(define-public crate-ampapi-0.1.0 (c (n "ampapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "0jxrnfdkx2rl0q10ca94nssb4ba857lnrkv3226a4l7z4y9h1dqq")))

(define-public crate-ampapi-0.1.1 (c (n "ampapi") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "1dxdxbcglmr23q5allaadgf26b3i4vzl1733spb6xw7mwpxxhsnl")))

(define-public crate-ampapi-0.1.2 (c (n "ampapi") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "006mjz4g8r5v5jpzx7lc54r517jds6b95vf3pa0qkk7lzdp7s2df")))

(define-public crate-ampapi-0.1.3 (c (n "ampapi") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "08wwgi5bmkanw668dc0ksjh16f8wxhamsa9k6hai7v0w6av0lghb")))

(define-public crate-ampapi-0.1.4 (c (n "ampapi") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "0zj640kqp129rpxbk12i3cz2kryg1xiivy6ay6xdjm4icj05rk65")))

(define-public crate-ampapi-0.1.5 (c (n "ampapi") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "1q5rnz4hjvqjbw2xifc4advajx8ivnws051fr2nq2xpvm0i1yq29")))

(define-public crate-ampapi-0.1.6 (c (n "ampapi") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "0g2rqy7rcajxrw79hbvz4g05m3fdh4k94qwdhnwylnjs498s7p08")))

