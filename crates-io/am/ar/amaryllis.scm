(define-module (crates-io am ar amaryllis) #:use-module (crates-io))

(define-public crate-amaryllis-0.1.0 (c (n "amaryllis") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "colorgrad") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("webp-encoder"))) (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1bxxd2qrn09j05gk2nrjpfgynj664kdhgiwgcacmrnwms1wfwfq8")))

