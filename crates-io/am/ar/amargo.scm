(define-module (crates-io am ar amargo) #:use-module (crates-io))

(define-public crate-amargo-0.1.0 (c (n "amargo") (v "0.1.0") (h "1s2zyjivqgmljpccr279ichhfhh8rm87zmm0rbcvc1sif0m7j20x") (y #t)))

(define-public crate-amargo-0.2.0 (c (n "amargo") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "05xplnsijc2nn71b24111g9pp2dbnx3ajfj6pq7crimvdbqrnf9l") (y #t)))

(define-public crate-amargo-0.2.1 (c (n "amargo") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1ypzml3ryzj3bzcjmxzin3mfhxcr3gzawbd9a1parzxjk6b9bhgf") (y #t)))

(define-public crate-amargo-0.2.2 (c (n "amargo") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0i58jnxv3744r6x0k41nf9fkiyprz6bir7zi5jpfqcm6g9l9zwna")))

