(define-module (crates-io am ka amka) #:use-module (crates-io))

(define-public crate-amka-0.1.0 (c (n "amka") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)))) (h "1mm1vhdn47w1layhrqbxl7hzp48daw96hvfkr1b4g7a47s1dp0vn")))

(define-public crate-amka-0.1.1 (c (n "amka") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)))) (h "0mairbkpgr1q9y8pnfjr5x312ziqwyl8dl2632aipgswax28g0xv")))

(define-public crate-amka-1.0.0 (c (n "amka") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)))) (h "10zxw4gy9hq26m5g36vrwxp25abjjz23rxdj2z6rfjawc92ncd4g")))

(define-public crate-amka-1.0.1 (c (n "amka") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)))) (h "04v4lxw41q6d1s6wiyx2kd7bg0apxda083vyfjgwh7nmvmxh1lp6")))

(define-public crate-amka-1.1.0 (c (n "amka") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)))) (h "1m3dm0fg23f266i9989278pds4wrybrq9j2nnisahl4k7vk10k0r")))

