(define-module (crates-io am d- amd-ext-d3d) #:use-module (crates-io))

(define-public crate-amd-ext-d3d-0.1.0 (c (n "amd-ext-d3d") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "1i5wz3zmifvrb7qk3rc0l95m2nrcrchwhxsfx1zma10v11wrdvwb")))

(define-public crate-amd-ext-d3d-0.1.1 (c (n "amd-ext-d3d") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "0afn8slmbd3g2sjvlsjnb50mj54lx2768f41zcycmiyq3ifa0wm0")))

(define-public crate-amd-ext-d3d-0.2.0 (c (n "amd-ext-d3d") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.51") (d #t) (k 0)))) (h "11g57dgfh5vwfz2m9qj7m6hm15lglk5jirh6syavnqppk7v5fs4k")))

(define-public crate-amd-ext-d3d-0.2.1 (c (n "amd-ext-d3d") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r ">=0.51, <=0.52") (f (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)) (d (n "windows-core") (r ">=0.51, <=0.52") (d #t) (k 0)))) (h "0fcpaqrlvb2zf71hwdavvqrj6whk5scsvxj95v1gi49h782ab2ad")))

