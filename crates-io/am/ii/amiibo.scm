(define-module (crates-io am ii amiibo) #:use-module (crates-io))

(define-public crate-amiibo-0.1.0 (c (n "amiibo") (v "0.1.0") (d (list (d (n "aes-ctr") (r "^0.6.0") (d #t) (k 0)) (d (n "aes-soft") (r "^0.6") (d #t) (k 0)) (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "ctr") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "0zxn17wgqjr48xk06wxfh74wfrcnaw64l08c7k680fqsgh8p805d")))

(define-public crate-amiibo-0.2.0 (c (n "amiibo") (v "0.2.0") (d (list (d (n "aes-ctr") (r "^0.6.0") (d #t) (k 0)) (d (n "aes-soft") (r "^0.6") (d #t) (k 0)) (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "ctr") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "0440gm645s5q8dbs9zr16l3mmy8bjdxw5x24fid0b5h41df6k0d7")))

