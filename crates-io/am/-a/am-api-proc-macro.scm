(define-module (crates-io am -a am-api-proc-macro) #:use-module (crates-io))

(define-public crate-am-api-proc-macro-1.0.0 (c (n "am-api-proc-macro") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19b0zcb1qsh6zfff35xinrfdzbi9biv7szas0hv61mqhpm6gasxq")))

