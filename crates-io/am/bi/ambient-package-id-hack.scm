(define-module (crates-io am bi ambient-package-id-hack) #:use-module (crates-io))

(define-public crate-ambient-package-id-hack-0.1.0 (c (n "ambient-package-id-hack") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "047p3ykjmdwfb16yg8pmbvnv6c0y3pjgw1an1aj9943jmjmx8i1d") (y #t)))

(define-public crate-ambient-package-id-hack-0.1.1 (c (n "ambient-package-id-hack") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0427a3lcgz7yvgwjcj5ch37vpq9h818alhvpbyw2qiz5172qs7dw") (y #t)))

(define-public crate-ambient-package-id-hack-0.1.2 (c (n "ambient-package-id-hack") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1v3n6d7scfzam82dvqfardxs8yxqns33lxhrki8sfh7451dh3sgj") (y #t)))

(define-public crate-ambient-package-id-hack-0.1.3 (c (n "ambient-package-id-hack") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "024zfdl1qzlr6jakcj4v6z1v057lnkgkc5il64clin4x9rd96x3x")))

