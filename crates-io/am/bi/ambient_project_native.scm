(define-module (crates-io am bi ambient_project_native) #:use-module (crates-io))

(define-public crate-ambient_project_native-0.2.0-rc6 (c (n "ambient_project_native") (v "0.2.0-rc6") (d (list (d (n "ambient_ecs") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mqmgvinspa6d33frkdijji0v2m1d72zqlxwd2g4jdwxn2y68kq2") (r "1.67.0")))

(define-public crate-ambient_project_native-0.2.0-rc7 (c (n "ambient_project_native") (v "0.2.0-rc7") (d (list (d (n "ambient_ecs") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1psfbfgg0h54wskg4zd4cxcvyp4w0jkf6bwsq1zjw3rpw9n4df1d") (r "1.67.0")))

(define-public crate-ambient_project_native-0.2.0-rc8 (c (n "ambient_project_native") (v "0.2.0-rc8") (d (list (d (n "ambient_ecs") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1djc7fp64sxrzfhq400yh074ncmkrjx6yk9pih108rihhhvcn9jp") (r "1.67.0")))

(define-public crate-ambient_project_native-0.2.0-rc9 (c (n "ambient_project_native") (v "0.2.0-rc9") (d (list (d (n "ambient_ecs") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12sflfn90prkrwnqqa0w0wsbq89sgg0623cpfj51s2kf212dyj98") (r "1.67.0")))

(define-public crate-ambient_project_native-0.2.0 (c (n "ambient_project_native") (v "0.2.0") (d (list (d (n "ambient_ecs") (r "^0.2.0") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vfa7psxv4qklpv12s7fxj53wxmqmkr6h4dimrabi2bm17y78w3n") (r "1.67.0")))

(define-public crate-ambient_project_native-0.2.1 (c (n "ambient_project_native") (v "0.2.1") (d (list (d (n "ambient_ecs") (r "^0.2.1") (d #t) (k 0)) (d (n "ambient_project") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kx5bdhpq5yqmwc404ccfdnh9xxb3dk5q4sp0g00ahaclsh7pqjx") (r "1.67.0")))

