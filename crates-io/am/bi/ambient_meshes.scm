(define-module (crates-io am bi ambient_meshes) #:use-module (crates-io))

(define-public crate-ambient_meshes-0.2.0-rc5 (c (n "ambient_meshes") (v "0.2.0-rc5") (d (list (d (n "ambient_gpu") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "11yg2pr8lixqlas0zfikh797l5nl4863jsvnikxwzg30kwzbp97c") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.0-rc6 (c (n "ambient_meshes") (v "0.2.0-rc6") (d (list (d (n "ambient_gpu") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "15d637mir1bj4p9w1d37qqn3qaxvdvi5py556mlj87p9s5dww31q") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.0-rc7 (c (n "ambient_meshes") (v "0.2.0-rc7") (d (list (d (n "ambient_gpu") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "15ki9d4vvl4lc87i1xdvrqzbsbp6rhcznnpnjrpdagnmnrz6j6gr") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.0-rc8 (c (n "ambient_meshes") (v "0.2.0-rc8") (d (list (d (n "ambient_gpu") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "19jxm25ycgp9ppq74hvwwk3053qynm0dxvqrah3301bmx7avs6xa") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.0-rc9 (c (n "ambient_meshes") (v "0.2.0-rc9") (d (list (d (n "ambient_gpu") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1rivz10fd1h6rjxkb5m326967bjck8jadb0sshw7xiwrhs6qmzl2") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.0 (c (n "ambient_meshes") (v "0.2.0") (d (list (d (n "ambient_gpu") (r "^0.2.0") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "06nlf88l9m103vvsjq04d7nl3zd7c36asx4sk6rbhlk9la4xqmz7") (r "1.67.0")))

(define-public crate-ambient_meshes-0.2.1 (c (n "ambient_meshes") (v "0.2.1") (d (list (d (n "ambient_gpu") (r "^0.2.1") (d #t) (k 0)) (d (n "ambient_std") (r "^0.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0p21xp28hy214v3gcmkpc88xrqkr2fk4lblakm186h52picd10hp") (r "1.67.0")))

