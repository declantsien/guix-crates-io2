(define-module (crates-io am bi ambigviz) #:use-module (crates-io))

(define-public crate-ambigviz-0.1.0 (c (n "ambigviz") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (f (quote ("kaleido"))) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.46.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0nq784y2vmxdw64jsr46r54lgkggixl3pb553d5wj9anl8ljb9n3")))

