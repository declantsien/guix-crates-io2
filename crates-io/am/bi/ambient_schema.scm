(define-module (crates-io am bi ambient_schema) #:use-module (crates-io))

(define-public crate-ambient_schema-0.2.0-rc3 (c (n "ambient_schema") (v "0.2.0-rc3") (h "07fls1qks2il8pa504a72n6l71nyl83hffnmhlvnqmdq9ihlvfgj") (r "1.67")))

(define-public crate-ambient_schema-0.2.0-rc5 (c (n "ambient_schema") (v "0.2.0-rc5") (h "0bg8m1dbkvqfmj01xfykv3wgww1g1nl1yxkbwxgxxca6ka3ai33k") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.0-rc6 (c (n "ambient_schema") (v "0.2.0-rc6") (h "14j4i6lzwkd4ccaqc9gm6kg13ypffj207ypaqjq3di77539zz26j") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.0-rc7 (c (n "ambient_schema") (v "0.2.0-rc7") (h "0j4mq400rr73lbc34252fkfj5njjjak1plqx50j1mbq2s8vkjhfc") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.0-rc8 (c (n "ambient_schema") (v "0.2.0-rc8") (h "0sbwv16f2fp7b3py84ip5w7sv8qb1f6dbz1bzi2nrl9yl2ag53h7") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.0-rc9 (c (n "ambient_schema") (v "0.2.0-rc9") (h "1bagn2ww64flija8i8i4acnqhynm1bfb7c23y5p177nbhcgdgn70") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.0 (c (n "ambient_schema") (v "0.2.0") (h "0ysr8fwjs3j2nnjacnrrdychd98jwjm4g2kfmfmpqs6bj85g3vdl") (r "1.67.0")))

(define-public crate-ambient_schema-0.2.1 (c (n "ambient_schema") (v "0.2.1") (h "08cqfjcwg885lgi0kc14gly0h96a0q8lsm6zb11ni7p6313yarhv") (r "1.67.0")))

(define-public crate-ambient_schema-0.3.0-rc.2 (c (n "ambient_schema") (v "0.3.0-rc.2") (h "1545s2z4gz8rrma9zs425nyg528aw40z61y1d1ywgacgpk3pmwwc") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.0-rc.3a (c (n "ambient_schema") (v "0.3.0-rc.3a") (h "0ya54zvncdmv72g7ji1gf2p5pyh9yxnnrcqcf8jjhjysmnwc7idz") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.0-rc.4 (c (n "ambient_schema") (v "0.3.0-rc.4") (h "1cn2ilclsy8v4djx4gwfr00bibdm0qnp303w6xskvk3jbdrqxwiv") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.0-rc.4a (c (n "ambient_schema") (v "0.3.0-rc.4a") (h "1ipqkgrc19avr8ndd4k7dd0zmsf69riwpmdn5pjrq01p5sfhzshl") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.0 (c (n "ambient_schema") (v "0.3.0") (h "00gzpkscnmnmviagh1mj9zwsbv5jjfkhd3a2ph16y2gyc39962a6") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.1-rc.1 (c (n "ambient_schema") (v "0.3.1-rc.1") (h "0ww8m0m79cgwr1xrf8sv6100pb5sz0ckkdj6lwb5zx1wadvvk92j") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.1-rc.2 (c (n "ambient_schema") (v "0.3.1-rc.2") (h "1p1899v5l8hwqbrmpcg10yvmrz2c6gh53zrgpi8730ml98yxnh89") (r "1.70.0")))

(define-public crate-ambient_schema-0.3.1 (c (n "ambient_schema") (v "0.3.1") (h "0pkaj36lqamgy3hl2mddjcd2bpy2hhvzc4i9r0qy67lrq4cw8cdc") (r "1.70.0")))

