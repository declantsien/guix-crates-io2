(define-module (crates-io am bi ambiq-apollo3-pac2) #:use-module (crates-io))

(define-public crate-ambiq-apollo3-pac2-0.2.0 (c (n "ambiq-apollo3-pac2") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1isrriwd78xgkil5dk80lz5vjid8xml0pdaayq6cz8dl4sl223ja") (f (quote (("rt" "cortex-m-rt/device"))))))

