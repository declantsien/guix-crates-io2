(define-module (crates-io am bi ambientcg) #:use-module (crates-io))

(define-public crate-ambientcg-0.1.0 (c (n "ambientcg") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "1xj480ig6lzspv446bwjkfmkvvm8p492f95qyq9gfmcapqbbgfvv")))

(define-public crate-ambientcg-0.1.1 (c (n "ambientcg") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "1laih87gz29q6hc7jzdfs0xyylcwymaq4q6z5lsrfw5ghk9h0cmb")))

(define-public crate-ambientcg-0.2.0 (c (n "ambientcg") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "0j22xgr4w272hwc36244gxy465h19vaj78clhrrd24bf6q72rb4y")))

(define-public crate-ambientcg-0.2.1 (c (n "ambientcg") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "11wvzi6q9bwjdgbmhry2jqk8h8kbr8nxwqcfypiad3lwd9mjvzj6")))

(define-public crate-ambientcg-0.2.2 (c (n "ambientcg") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "1mpcncr8l6rhfrb4jyfsqf4v3wj3fdlsn47bis6mpv0vil5iwkbp")))

