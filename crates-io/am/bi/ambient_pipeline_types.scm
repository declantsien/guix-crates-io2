(define-module (crates-io am bi ambient_pipeline_types) #:use-module (crates-io))

(define-public crate-ambient_pipeline_types-0.3.1-rc.1 (c (n "ambient_pipeline_types") (v "0.3.1-rc.1") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wgpu-types") (r "^0.16") (f (quote ("serde" "serde" "trace" "replay"))) (d #t) (k 0)))) (h "0nvygzsk03f5gcivrfphbd6di48xj6w5mhr0frk38a89w19xlqbd")))

(define-public crate-ambient_pipeline_types-0.3.1-rc.2 (c (n "ambient_pipeline_types") (v "0.3.1-rc.2") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wgpu-types") (r "^0.16") (f (quote ("serde" "serde" "trace" "replay"))) (d #t) (k 0)))) (h "1grw8bj0yj109f5yxcxgf0fkjxrrxgb5daxasz4075zr1rgkwnbm")))

(define-public crate-ambient_pipeline_types-0.3.1 (c (n "ambient_pipeline_types") (v "0.3.1") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wgpu-types") (r "^0.16") (f (quote ("serde" "serde" "trace" "replay"))) (d #t) (k 0)))) (h "1w6izhngcv2hy01f5jr2lsg0qipc284zza0jb6cbgz9d0jy8rzrw")))

