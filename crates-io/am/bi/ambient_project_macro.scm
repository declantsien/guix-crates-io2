(define-module (crates-io am bi ambient_project_macro) #:use-module (crates-io))

(define-public crate-ambient_project_macro-0.2.0-rc5 (c (n "ambient_project_macro") (v "0.2.0-rc5") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06ddbm7338l6cdibivfvdqy528gm0611nrr4i6244blkwdxwmq98") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.0-rc6 (c (n "ambient_project_macro") (v "0.2.0-rc6") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0-rc6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zdwr59ay41cjpffi8qrdlcv36xvzq02x8nlbnqszpfz5i649rg3") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.0-rc7 (c (n "ambient_project_macro") (v "0.2.0-rc7") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s9p4g6qkbwk676v5q0wm2w9468azk1cspccr8x4m6raxz08lxbf") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.0-rc8 (c (n "ambient_project_macro") (v "0.2.0-rc8") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15pykbbysg0hk42m6si1yvhck17xn1bpm9swz6h0zc83ffhzy6ji") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.0-rc9 (c (n "ambient_project_macro") (v "0.2.0-rc9") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ilp83ap0i5lsri11xsir31n6j9cfyhpv4n1m0k4nqz92g3icmf6") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.0 (c (n "ambient_project_macro") (v "0.2.0") (d (list (d (n "ambient_project_macro_common") (r "^0.2.0") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "011xra1zjhh6005a53rxnmvji5xy8dj4fnybihp2rv6w00kv4pbp") (r "1.67.0")))

(define-public crate-ambient_project_macro-0.2.1 (c (n "ambient_project_macro") (v "0.2.1") (d (list (d (n "ambient_project_macro_common") (r "^0.2.1") (d #t) (k 0)) (d (n "ambient_schema") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x1awz1az63ph84vpg61mrskp9q3yx2lmdcsf53g21ldb3ll4f46") (r "1.67.0")))

