(define-module (crates-io am bi ambient_project_rt) #:use-module (crates-io))

(define-public crate-ambient_project_rt-0.2.0-rc2 (c (n "ambient_project_rt") (v "0.2.0-rc2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x0mg41y329f6mvwswfb8mmdn51gzz52m1sgg03rr0zy0q4k74yn") (r "1.67")))

(define-public crate-ambient_project_rt-0.2.0-rc3 (c (n "ambient_project_rt") (v "0.2.0-rc3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jp8ky8jgiyx4mgvwflmf9z0jvynyh8aa17n3a1fax436i40d28l") (r "1.67")))

(define-public crate-ambient_project_rt-0.2.0-rc5 (c (n "ambient_project_rt") (v "0.2.0-rc5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10q9g35lzk8m594nxd5hgqnwb8jzbyvl26xhnjka7zc87paf0014") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.0-rc6 (c (n "ambient_project_rt") (v "0.2.0-rc6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00a7hhqdxx0h8zyzr77by0zwnwb2nr3wfbrlb1cga5g69a6nmkzv") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.0-rc7 (c (n "ambient_project_rt") (v "0.2.0-rc7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vx0n8vxl0rqjlwq6izp3m8jxs9fh6l55x48vk7px8w6mzcwmb02") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.0-rc8 (c (n "ambient_project_rt") (v "0.2.0-rc8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a75pls8ls8i3hhw3nvhf80lbl7n8cnk4c9k2gvxfc8n6mffb5yx") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.0-rc9 (c (n "ambient_project_rt") (v "0.2.0-rc9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00j0h2ip4lgr5mxn0z35dnjm98s9p8rlxrahaplv0h8mx08zv0lb") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.0 (c (n "ambient_project_rt") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04jbndx5nql7qhs72hazpawpqzsfd4n7yk4qs40a0qqayyl9cya4") (r "1.67.0")))

(define-public crate-ambient_project_rt-0.2.1 (c (n "ambient_project_rt") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a0c3f8ik5l3hs96jql0cazcyc15k6gxaliji2pbzdg6n55j8br3") (r "1.67.0")))

