(define-module (crates-io am bi ambient_git_rev) #:use-module (crates-io))

(define-public crate-ambient_git_rev-0.3.1-rc.1 (c (n "ambient_git_rev") (v "0.3.1-rc.1") (d (list (d (n "once_cell") (r "^1.18.0") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "127casbpld5jbrnyl52vn2njshpjs267y98nqg6k0jdw7m28fm3q") (r "1.70.0")))

(define-public crate-ambient_git_rev-0.3.1-rc.2 (c (n "ambient_git_rev") (v "0.3.1-rc.2") (d (list (d (n "once_cell") (r "^1.18.0") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "1ikc4ymbllm3rhh8grbk75ffbwmjq0fg9j2y2qdfnwhb7rc8hmk3") (r "1.70.0")))

(define-public crate-ambient_git_rev-0.3.1 (c (n "ambient_git_rev") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.18.0") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "1ppwwjr95x0khypgis43nv9ls3gb86z9q2j177prnxjf8l81dqsh") (r "1.70.0")))

