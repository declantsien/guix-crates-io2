(define-module (crates-io am bi ambiq-apollo2-pac) #:use-module (crates-io))

(define-public crate-ambiq-apollo2-pac-0.1.0 (c (n "ambiq-apollo2-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0f0214jksgxi0spprvp26jwv76wsqah2j1kmmgmnwlpx4wxp32l6") (f (quote (("rt" "cortex-m-rt/device"))))))

