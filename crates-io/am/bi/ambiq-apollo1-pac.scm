(define-module (crates-io am bi ambiq-apollo1-pac) #:use-module (crates-io))

(define-public crate-ambiq-apollo1-pac-0.1.0 (c (n "ambiq-apollo1-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0pzhwjs82nd0g8s037p31jan78knjxn78kz4rzc0v4c7alhswnxy") (f (quote (("rt" "cortex-m-rt/device"))))))

