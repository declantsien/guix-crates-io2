(define-module (crates-io am bi ambient_git_rev_init) #:use-module (crates-io))

(define-public crate-ambient_git_rev_init-0.3.1-rc.1 (c (n "ambient_git_rev_init") (v "0.3.1-rc.1") (d (list (d (n "ambient_git_rev") (r "^0.3.1-rc.1") (d #t) (k 0)))) (h "0376vd5f5qn3aavqzqvsww8hk9qkjxbhl58f57jlhzmlvp0wl3j9") (r "1.70.0")))

(define-public crate-ambient_git_rev_init-0.3.1-rc.2 (c (n "ambient_git_rev_init") (v "0.3.1-rc.2") (d (list (d (n "ambient_git_rev") (r "^0.3.1-rc.2") (d #t) (k 0)))) (h "07iwls7c79a6zq64lrz0sqaqxi5hhviliiqksspjxbdcxkignkp6") (r "1.70.0")))

(define-public crate-ambient_git_rev_init-0.3.1 (c (n "ambient_git_rev_init") (v "0.3.1") (d (list (d (n "ambient_git_rev") (r "^0.3.1") (d #t) (k 0)))) (h "0p3vcki83x7pncwywjrz43cpw1h2pxs8xfi00vglv990mj09jh8a") (r "1.70.0")))

