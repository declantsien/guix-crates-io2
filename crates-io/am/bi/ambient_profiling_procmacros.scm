(define-module (crates-io am bi ambient_profiling_procmacros) #:use-module (crates-io))

(define-public crate-ambient_profiling_procmacros-1.0.9 (c (n "ambient_profiling_procmacros") (v "1.0.9") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "046kx9qnmrxspr44fqzaqny296zkq68qa5aqknsb3rhlxfxgr1kh") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

