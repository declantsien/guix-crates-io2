(define-module (crates-io am bi ambient-weather-api) #:use-module (crates-io))

(define-public crate-ambient-weather-api-0.1.0 (c (n "ambient-weather-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "07fmsxnyvp80gi7572yyfl2yppnlxc3i75yfs9f59pqvwd8qws1c")))

(define-public crate-ambient-weather-api-0.1.1 (c (n "ambient-weather-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0xyixxcnks9j9kdypv797kl7ss2216j0krb9jyfkq2qlybvrgq3n")))

(define-public crate-ambient-weather-api-0.2.0 (c (n "ambient-weather-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y59s6jvpaxsd5h0cd5rn3zhfvzmjmnkqykfz4cb54aj0v81wfn0")))

(define-public crate-ambient-weather-api-0.3.0 (c (n "ambient-weather-api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g99qpagn4pr8zvrb7npydbg199yvi54j41lykv1673hw67c8rjs")))

(define-public crate-ambient-weather-api-0.3.1 (c (n "ambient-weather-api") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1li27f4wxq32pkqdr56gzvjz5bivbhdsjdy7fjrpz52kj0rhswar")))

