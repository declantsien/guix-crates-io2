(define-module (crates-io am bi ambisonic) #:use-module (crates-io))

(define-public crate-ambisonic-0.1.0 (c (n "ambisonic") (v "0.1.0") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "1bnizikk7yb9h37mzx74252i7r9qxcjl1g5b4bk5r64g4g8ms4zz")))

(define-public crate-ambisonic-0.1.1 (c (n "ambisonic") (v "0.1.1") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "0q3y482k5w8bans6cn187dbkjqp4s573i311j52640w2g12w90m4")))

(define-public crate-ambisonic-0.1.2 (c (n "ambisonic") (v "0.1.2") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "0imqpgzp2pa01p8bfyyjv8mxar411a9nj8cnimsr8fpwv1pgsdp3")))

(define-public crate-ambisonic-0.2.0 (c (n "ambisonic") (v "0.2.0") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "0jbnr3wff8s585sy3vi3h7dg132mvmw3hyphyl6lkxvwa5q9m3w8")))

(define-public crate-ambisonic-0.2.1 (c (n "ambisonic") (v "0.2.1") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "0qskccnpqr4svwz2pjnscy9jw90b727flwmb1rkcrv4zjv4xhwn7")))

(define-public crate-ambisonic-0.3.0 (c (n "ambisonic") (v "0.3.0") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "13p033bh5v6qc16wg6r2r9gxaiyhmncv5r6x6jzx647fhclq6ajy")))

(define-public crate-ambisonic-0.3.1 (c (n "ambisonic") (v "0.3.1") (d (list (d (n "cpal") (r "<0.12, >=0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)) (d (n "rodio") (r "<0.12, >=0.9") (d #t) (k 0)))) (h "1fj0cipilr2lh2pm1jixabd76ap7z24vpqwfrz623hsilv6j469y")))

(define-public crate-ambisonic-0.4.0 (c (n "ambisonic") (v "0.4.0") (d (list (d (n "cpal") (r ">=0.12, <=0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r ">=0.12, <=0.13") (d #t) (k 0)))) (h "10iq4fqaanijabdwrzd94agzm09ix9fzhnn3asbidbnx5h8wylqd")))

(define-public crate-ambisonic-0.4.1 (c (n "ambisonic") (v "0.4.1") (d (list (d (n "cpal") (r ">=0.12, <=0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r ">=0.12, <=0.13") (d #t) (k 0)))) (h "0cxqbc0hiblia18kz9p8c4szxpzwgfb3shxnxd6zg6p2jqvihajx")))

