(define-module (crates-io am bi ambient_time) #:use-module (crates-io))

(define-public crate-ambient_time-0.2.0-rc1 (c (n "ambient_time") (v "0.2.0-rc1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00bq7myg5hnkygs8c2dlbrhinvzqzzl8zimyy6ckd4d94lp8v988") (r "1.67")))

(define-public crate-ambient_time-0.2.0-rc2 (c (n "ambient_time") (v "0.2.0-rc2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g21sgrgqimid2pxh5qx8pkk5h760sq2qpm8pwnds1g26n9cnyr6") (r "1.67")))

(define-public crate-ambient_time-0.2.0-rc3 (c (n "ambient_time") (v "0.2.0-rc3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zcln5pr07l85a8xcnkz61p7h8rhsvxvhhfk5qcxpdfzhil1dn39") (r "1.67")))

(define-public crate-ambient_time-0.2.0-rc4 (c (n "ambient_time") (v "0.2.0-rc4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k3gm3040cns2fdq5931bqp8araql7fkabwxnzdqkd4q9xc099v9") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0-rc5 (c (n "ambient_time") (v "0.2.0-rc5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q0v56xby1602n3941g9xmlypfmvjc4lskds4v0kbsfzwaxlswh9") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0-rc6 (c (n "ambient_time") (v "0.2.0-rc6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kdi3i7mavvxfjdks7w4k00mk0a4ffrwldzp7d3sm4f12byfimdv") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0-rc7 (c (n "ambient_time") (v "0.2.0-rc7") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i9856acpx6kz4qd7wfj1z5nxn6c1zbnphkvb5d8grxmq1hrqr8h") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0-rc8 (c (n "ambient_time") (v "0.2.0-rc8") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "125f0qm6ssq30yhkznkdszl0hpkz109hnr7lfiqgsz76mbpz3l35") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0-rc9 (c (n "ambient_time") (v "0.2.0-rc9") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xhv41q6f1mfbmwncfwb7514zx53gmkdls67f9qxfqf40g4n6inl") (r "1.67.0")))

(define-public crate-ambient_time-0.2.0 (c (n "ambient_time") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06bxjmql2wys29h7brh0nn3jj54w6rxqm98vr8hds80c9bf4zfl1") (r "1.67.0")))

(define-public crate-ambient_time-0.2.1 (c (n "ambient_time") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a6nwg13sjmb3r3gf8q2417ha768514axgmcvvb98gazsq341ly9") (r "1.67.0")))

(define-public crate-ambient_time-0.3.0-rc.2 (c (n "ambient_time") (v "0.3.0-rc.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xl40xxzidlnvnmx0w90adly32clyccalrnc2y7l4jb45m5srp2g") (r "1.70.0")))

(define-public crate-ambient_time-0.3.0-rc.3 (c (n "ambient_time") (v "0.3.0-rc.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hv31vvmv7714niy7hqr9hsxmrm4rqa3i65sc2wkh0dcadirfdd7") (r "1.70.0")))

(define-public crate-ambient_time-0.3.0-rc.3a (c (n "ambient_time") (v "0.3.0-rc.3a") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x8b0vv3ygpxm6nmfi1knqsxi1r6rkpgzxkcwpxhz69w7bhgg1wf") (r "1.70.0")))

(define-public crate-ambient_time-0.3.0-rc.4 (c (n "ambient_time") (v "0.3.0-rc.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cpqc4hdi1wz8fzplyk713p2sa4l6kczyjsgsdl14szb9dl8ylqy") (r "1.70.0")))

(define-public crate-ambient_time-0.3.0-rc.4a (c (n "ambient_time") (v "0.3.0-rc.4a") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xwbfk4yl8amhc3sdzamc9rx29wnarhs26hzkxvqc7s62vchh17a") (r "1.70.0")))

(define-public crate-ambient_time-0.3.0 (c (n "ambient_time") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04gmcz7gwna2dclfrhnzypqkzx9r1mvsi7imqgakar88jxgzqqgn") (r "1.70.0")))

(define-public crate-ambient_time-0.3.1-rc.1 (c (n "ambient_time") (v "0.3.1-rc.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pncqqhmf5d95rlxycm0z2xxzvsvimn834yppfac5sj5zwrdgm12") (r "1.70.0")))

(define-public crate-ambient_time-0.3.1-rc.2 (c (n "ambient_time") (v "0.3.1-rc.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nvkld56wd42r20bc2hz4jvz620zcxc3015x483r74611w1h05gh") (r "1.70.0")))

(define-public crate-ambient_time-0.3.1 (c (n "ambient_time") (v "0.3.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pjmkvmh9ih637a5gkqzxf1capj68iyrj9vmllwsddilc9zqcmd8") (r "1.70.0")))

