(define-module (crates-io am bi ambient-weather-progenitor) #:use-module (crates-io))

(define-public crate-ambient-weather-progenitor-0.1.0 (c (n "ambient-weather-progenitor") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "progenitor-client") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00qsckzq01kvzg104h77vhfnkb0nf84c22yx4gaf6h8hvwy9ifpy")))

