(define-module (crates-io am bi ambient_dirs) #:use-module (crates-io))

(define-public crate-ambient_dirs-0.3.0-rc.3 (c (n "ambient_dirs") (v "0.3.0-rc.3") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "10xbmps4ddhkmbjwz3azpw0hb2gi98bhcrigpg0xhd5zsi6flrj7") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.0-rc.3a (c (n "ambient_dirs") (v "0.3.0-rc.3a") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0s46ycqp2scxb2qjnzcl6wm2ywxbljvmslf2hlqxybi121pmsxsb") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.0-rc.4 (c (n "ambient_dirs") (v "0.3.0-rc.4") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "01iflc5zyd94z3lyhyyikrp88bhw27j50jj8jjkqri8sa2v8cwxf") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.0-rc.4a (c (n "ambient_dirs") (v "0.3.0-rc.4a") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0x6hkhm0y52cgqq51brdrpidyy69yxvw3wxj95yqiwrkg31vn0n5") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.0 (c (n "ambient_dirs") (v "0.3.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0cc2r3rlh54qdpc636s8mfqny2wx8ivjcswijqb8rsnzbfqlyn4d") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.1-rc.1 (c (n "ambient_dirs") (v "0.3.1-rc.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "00zih4yx3saxijdip33zh2wczwqv4p64w976qaqcrhyk88gihz6r") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.1-rc.2 (c (n "ambient_dirs") (v "0.3.1-rc.2") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0x9633gwdbhlkjf0idldf75hypfxinyvp7sslnavh0xngaqsjhz2") (r "1.70.0")))

(define-public crate-ambient_dirs-0.3.1 (c (n "ambient_dirs") (v "0.3.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "1502mh6bdbdp3cryckf7nxnms64v4x728xghqmzlba11zkdi99qj") (r "1.70.0")))

