(define-module (crates-io am bi ambient_rustc) #:use-module (crates-io))

(define-public crate-ambient_rustc-0.2.0-rc5 (c (n "ambient_rustc") (v "0.2.0-rc5") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kffl5zsg8hfsrxiqy9y3jkvwknxywkqgn45rvpj5plmfi6kj1gg") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.0-rc6 (c (n "ambient_rustc") (v "0.2.0-rc6") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06pbz6snqmkcwa3wspim1dbjg19h18p1i4c1y6wpbywpy40gyz0h") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.0-rc7 (c (n "ambient_rustc") (v "0.2.0-rc7") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15910c42hksp26j0p4ksq9cg22w7z49b5ndigv3bdsgjhcynrkmr") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.0-rc8 (c (n "ambient_rustc") (v "0.2.0-rc8") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b6z2y9dxxnxk39disglzmdzp0pyknniffvbxjz9j8rgfs7ahy7a") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.0-rc9 (c (n "ambient_rustc") (v "0.2.0-rc9") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p605md8pl2raslgc9217hnr1kzi4v07z0v1pysvyzrmbg2zphph") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.0 (c (n "ambient_rustc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "024lm0c8bsh3c8n3045y0s7ln9drcspvw918lb456h40a1f33hnn") (r "1.67.0")))

(define-public crate-ambient_rustc-0.2.1 (c (n "ambient_rustc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "185nisaff510camwmz472yycxmlxqdfi5qrxjalfdkk9wzzkf37f") (r "1.67.0")))

(define-public crate-ambient_rustc-0.3.1-rc.1 (c (n "ambient_rustc") (v "0.3.1-rc.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01k323pypqpvypybxwdq28xpahvaiiqiwqa4dncxg7almw01hzq8") (r "1.70.0")))

(define-public crate-ambient_rustc-0.3.1-rc.2 (c (n "ambient_rustc") (v "0.3.1-rc.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fnr81xdz1salqzgp3nyb9kzh0mpr0zpbg2sjmixv3i3b8il3rrc") (r "1.70.0")))

(define-public crate-ambient_rustc-0.3.1 (c (n "ambient_rustc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15zlyrds4s2dhm3qnhx690s116ajp0wxipc8sskf6fn8nqz1q6w6") (r "1.70.0")))

