(define-module (crates-io am bi ambience) #:use-module (crates-io))

(define-public crate-ambience-0.1.0 (c (n "ambience") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05nnn9jvcj097xwpf0b04iyb63n5j9rq23fvk2qmlmykr2pb3fxd")))

(define-public crate-ambience-0.2.0 (c (n "ambience") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13pih3ka39s88f2hh19lc3fpdakddikvmrm1m0yhdxrfzmcirbjh")))

(define-public crate-ambience-0.2.1 (c (n "ambience") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c7hx060vsq1f98w5krl70znd1ybd1kh8fapl5mi7mvpc37qfmbc")))

