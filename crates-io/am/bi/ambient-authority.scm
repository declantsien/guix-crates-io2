(define-module (crates-io am bi ambient-authority) #:use-module (crates-io))

(define-public crate-ambient-authority-0.0.0 (c (n "ambient-authority") (v "0.0.0") (h "052dlmm7k3mdwb0zhc2zf8n538kidvzvqj22daa0yz0cydr2fqg0")))

(define-public crate-ambient-authority-0.0.1 (c (n "ambient-authority") (v "0.0.1") (h "0j9hxiqc03xizfclcfjznm9dali2n83fd3fqqg2ph2w4nknxd2pc")))

(define-public crate-ambient-authority-0.0.2 (c (n "ambient-authority") (v "0.0.2") (h "0fxsfyhy64jx7zrkb85h1vhr5nfqncja3pwpikid471d8w6yxm79")))

