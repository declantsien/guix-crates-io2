(define-module (crates-io am bi ambient_cb) #:use-module (crates-io))

(define-public crate-ambient_cb-0.2.0-rc1 (c (n "ambient_cb") (v "0.2.0-rc1") (h "0d5bp9bk10iqx67kvrd4hxspf6i9z5pfc5abyic16nlj1dhf2gq6") (r "1.67")))

(define-public crate-ambient_cb-0.2.0-rc2 (c (n "ambient_cb") (v "0.2.0-rc2") (h "1kzi0fa2iq8za4j758fz4jx9mk3wjj3662k79j9xir8jd4f31m54") (r "1.67")))

(define-public crate-ambient_cb-0.2.0-rc3 (c (n "ambient_cb") (v "0.2.0-rc3") (h "1zmqn60z6w6vhq6w95k9gqz62yc4x5c62943amv1msygscrja0vs") (r "1.67")))

(define-public crate-ambient_cb-0.2.0-rc5 (c (n "ambient_cb") (v "0.2.0-rc5") (h "0aykj3ahjcn2vbh1ys94rz7d9jdvmpn7xn0sijx9z41wi6d1dg9r") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.0-rc6 (c (n "ambient_cb") (v "0.2.0-rc6") (h "14n2fa1m2ck2qnpn9hng14ydj5hgz20svjc42gawh1gafmpqnl5z") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.0-rc7 (c (n "ambient_cb") (v "0.2.0-rc7") (h "053n9pmb03pdx3gk642r0wlacg1mmf80kjm3rq3r2mlzx36w7ggr") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.0-rc8 (c (n "ambient_cb") (v "0.2.0-rc8") (h "1gl6bcm6s33g6am9w3yyz837nq8vh39b6np0gspmswi41pcraqbf") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.0-rc9 (c (n "ambient_cb") (v "0.2.0-rc9") (h "0aa7cvyh10vd6gwbkqb1msw72fi31d4kv9yqk3f7cqj9mpcsd5p2") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.0 (c (n "ambient_cb") (v "0.2.0") (h "1vadmq6ajkmfd30k18ngb10i7ccylp4323w94n1nfchwplpvqhkg") (r "1.67.0")))

(define-public crate-ambient_cb-0.2.1 (c (n "ambient_cb") (v "0.2.1") (h "0470ap285vm0igzxh3j64rqzx4hk4s8bvxc9gxi1fn30a3nrsr0k") (r "1.67.0")))

(define-public crate-ambient_cb-0.3.0-rc.2 (c (n "ambient_cb") (v "0.3.0-rc.2") (h "0k2cv9g6sp23rcvw8wl1rbs4grc59wbipp010lvy5ggsvhpczldw") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.0-rc.3 (c (n "ambient_cb") (v "0.3.0-rc.3") (h "0xj02h4i93x3vzra4da17jlzqh8876amzf2aq8ngdn5622qi0rkz") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.0-rc.3a (c (n "ambient_cb") (v "0.3.0-rc.3a") (h "1j69ap6njiv0wrhy4bsdjlw0ypbc7igcw37zdmy1mm3ad9d778ad") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.0-rc.4 (c (n "ambient_cb") (v "0.3.0-rc.4") (h "0cb5si1h7mj1dz3xh3rrmm6bifwby84dhnzbi86zmj05ai955czg") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.0-rc.4a (c (n "ambient_cb") (v "0.3.0-rc.4a") (h "0nxcbnxb5gs0v9p99f1f1vwivf68xlw63x9ynlp1hrsj08py8fyh") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.0 (c (n "ambient_cb") (v "0.3.0") (h "18xq6q34bwn37v3lca4kn0ac58bxibcjk0yibnllgy8xp1qb5gm1") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.1-rc.1 (c (n "ambient_cb") (v "0.3.1-rc.1") (h "0lfshaqyld0c0fqv59h7zqsab6cap35v2hcdvkxv7divv5j2qqcs") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.1-rc.2 (c (n "ambient_cb") (v "0.3.1-rc.2") (h "1axm4whqvmapn28mv19spgm0c66x8ddbfdif8asfvrpa8xbsyx4f") (r "1.70.0")))

(define-public crate-ambient_cb-0.3.1 (c (n "ambient_cb") (v "0.3.1") (h "0yrykk7bm4czyr22502qb63zbkjhy6kchn1rib86lfdxxmvwpy59") (r "1.70.0")))

