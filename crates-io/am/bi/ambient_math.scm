(define-module (crates-io am bi ambient_math) #:use-module (crates-io))

(define-public crate-ambient_math-0.2.0-rc1 (c (n "ambient_math") (v "0.2.0-rc1") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0y58yha8cim863ca5d86k5qpg559xmqmajir7nxhx4cdrdgh15w6") (r "1.67")))

(define-public crate-ambient_math-0.2.0-rc2 (c (n "ambient_math") (v "0.2.0-rc2") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "04k3nwdrkcsmq5j0fp7bnqyhhr1m0gnyqrc9g36zcvyz1rc8p4y2") (r "1.67")))

(define-public crate-ambient_math-0.2.0-rc3 (c (n "ambient_math") (v "0.2.0-rc3") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1wa5zpb583qcfw770r2ihclf7s87cb63xkqq32m0xrs9g8cginnj") (r "1.67")))

(define-public crate-ambient_math-0.2.0-rc5 (c (n "ambient_math") (v "0.2.0-rc5") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1jb0xjm26xld9pwvnnzzkjj04g1mirssflyzi6qfdlqmymnqy54c") (r "1.67.0")))

(define-public crate-ambient_math-0.2.0-rc6 (c (n "ambient_math") (v "0.2.0-rc6") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1jkd55r1jq37qwdfha5d7scx9lj8jzibyi9kmlfibh25dnxmmbxy") (r "1.67.0")))

(define-public crate-ambient_math-0.2.0-rc7 (c (n "ambient_math") (v "0.2.0-rc7") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1h4bw850a16wbppha53lvn7cqf3iibic5spb5j6qgf22bfp6yhas") (r "1.67.0")))

(define-public crate-ambient_math-0.2.0-rc8 (c (n "ambient_math") (v "0.2.0-rc8") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "01b0c6ddjgpcrdvz9xybpmjwnvp29gbfbx4wk2vf5bywzkfljngi") (r "1.67.0")))

(define-public crate-ambient_math-0.2.0-rc9 (c (n "ambient_math") (v "0.2.0-rc9") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0m1pcws9vz2dlnxmbjl69xpgkqafpa1321c50bqp1cx1da4hsqnz") (r "1.67.0")))

(define-public crate-ambient_math-0.2.0 (c (n "ambient_math") (v "0.2.0") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0k82hcs6wnq71mg6b8y19rln63q3nsznjghwjxaci3y4k2jnxy57") (r "1.67.0")))

(define-public crate-ambient_math-0.2.1 (c (n "ambient_math") (v "0.2.1") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1c5iz70zf25zn60mjs6d4cgpqzkj50gj8qj1cm7sm76vckpng2vk") (r "1.67.0")))

(define-public crate-ambient_math-0.3.0-rc.2 (c (n "ambient_math") (v "0.3.0-rc.2") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1d4kgq6l4gng9ff8wqnf93pll55d292bqjz8x7ripqimlgm75jcj") (r "1.70.0")))

(define-public crate-ambient_math-0.3.0-rc.3 (c (n "ambient_math") (v "0.3.0-rc.3") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0vrnpvabk9as6q26qayh8bjsixvdxfa6d6p4sy92ryvk2l3vd06d") (r "1.70.0")))

(define-public crate-ambient_math-0.3.0-rc.3a (c (n "ambient_math") (v "0.3.0-rc.3a") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "159wz67iwiy7xds47rhy9bh7xhs3px37zqyck22byavc27ay4bgq") (r "1.70.0")))

(define-public crate-ambient_math-0.3.0-rc.4 (c (n "ambient_math") (v "0.3.0-rc.4") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0cf9fs92b0ri49x32w7nc13aqfm02ms6k2c5glxlipsrr7ix45ik") (r "1.70.0")))

(define-public crate-ambient_math-0.3.0-rc.4a (c (n "ambient_math") (v "0.3.0-rc.4a") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0rn5r8wfddz9l5499r4jqd0nri9qw834gd6qx0swrssxqj19q5m1") (r "1.70.0")))

(define-public crate-ambient_math-0.3.0 (c (n "ambient_math") (v "0.3.0") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1l42p1fsh5kjm8nhwvkhpd11h6qz88brz6dhcf52nlfvsmz5n41p") (r "1.70.0")))

(define-public crate-ambient_math-0.3.1-rc.1 (c (n "ambient_math") (v "0.3.1-rc.1") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "02966mrpp66rmh6jdwlfs1rqr5sw5hi7g3bnj9ry2ssxac9l59qb") (r "1.70.0")))

(define-public crate-ambient_math-0.3.1-rc.2 (c (n "ambient_math") (v "0.3.1-rc.2") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1r3w4r2jxvn1bihx4if10x213635y8643ihxq1074l0x5i2h7yvm") (r "1.70.0")))

(define-public crate-ambient_math-0.3.1 (c (n "ambient_math") (v "0.3.1") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck" "serde" "rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1f4b9c7a44g4b3r189ysjlkb5nxnvfv4n3znryn16hsly0n8x37h") (r "1.70.0")))

