(define-module (crates-io am bi ambiq-apollo3p-pac) #:use-module (crates-io))

(define-public crate-ambiq-apollo3p-pac-0.1.0 (c (n "ambiq-apollo3p-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1p3b47n79rlj45dwhmvxj5s69xgri2hzdlryrfq4hc203d09z868") (f (quote (("rt" "cortex-m-rt/device"))))))

