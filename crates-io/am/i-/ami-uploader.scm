(define-module (crates-io am i- ami-uploader) #:use-module (crates-io))

(define-public crate-ami-uploader-0.1.0 (c (n "ami-uploader") (v "0.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.47.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (d #t) (k 0)))) (h "03bc9sdl0fbjpsipsivsbx1wnkigxhw3dn7igg2d15mkl02y8flw")))

