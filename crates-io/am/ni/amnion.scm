(define-module (crates-io am ni amnion) #:use-module (crates-io))

(define-public crate-amnion-0.1.0 (c (n "amnion") (v "0.1.0") (d (list (d (n "amnion-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "amnion-frontend") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_aw") (r "^0.2.0") (f (quote ("tabs" "modal" "card"))) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "megalodon") (r "^0.2.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16c8aljiz7cnr229jkrby1214s51h8wv9rp8bwr47m1kxmcn1mnn") (r "1.59")))

