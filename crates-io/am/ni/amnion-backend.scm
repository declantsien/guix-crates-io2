(define-module (crates-io am ni amnion-backend) #:use-module (crates-io))

(define-public crate-amnion-backend-0.1.0 (c (n "amnion-backend") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "megalodon") (r "^0.2.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10zz0wm82jmxaamxyx2g5q0r1lkq2h8n3v46pdy6h6nnzymgrqzs")))

