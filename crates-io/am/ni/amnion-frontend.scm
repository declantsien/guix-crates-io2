(define-module (crates-io am ni amnion-frontend) #:use-module (crates-io))

(define-public crate-amnion-frontend-0.1.0 (c (n "amnion-frontend") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_aw") (r "^0.2.0") (f (quote ("tabs" "modal" "card"))) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "1byh8v3q1fymcwdj2w253av7jnvhqn84im2v1j217jqwi1cf9jr2")))

