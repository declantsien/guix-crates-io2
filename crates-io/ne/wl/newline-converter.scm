(define-module (crates-io ne wl newline-converter) #:use-module (crates-io))

(define-public crate-newline-converter-0.1.0 (c (n "newline-converter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0l9ab6r2q3d4n7blja5lniw827jh4g1034kqp0vyjmqs9mby76pz") (y #t)))

(define-public crate-newline-converter-0.2.0 (c (n "newline-converter") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0r85ci77fj02rxiwrdwrda6z2nz01bvsz9iwkcjc9fzf34miry6n") (y #t)))

(define-public crate-newline-converter-0.2.1 (c (n "newline-converter") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fancy-regex") (r "^0.10") (d #t) (k 2)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.15") (d #t) (k 2)))) (h "0chb0xws2mqpy0p67zmdik8sqxjyqid1jq23hsla0wzhpk94fpgh") (y #t)))

(define-public crate-newline-converter-0.2.2 (c (n "newline-converter") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fancy-regex") (r "^0.10") (d #t) (k 2)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.15") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "03y000bbxnwzb7aipxyw7gm68b1bd8dv7illz03l4qw7bjfx0w8z")))

(define-public crate-newline-converter-0.3.0 (c (n "newline-converter") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fancy-regex") (r "^0.10") (d #t) (k 2)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.15") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0zyw2hyjl89rj1zmp9n8fq69pbfp9zl1cbal73agxjxixjbv1dj7")))

