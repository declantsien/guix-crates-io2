(define-module (crates-io ne cr necrywagen) #:use-module (crates-io))

(define-public crate-necrywagen-0.1.0 (c (n "necrywagen") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1sqx35rw050v9awcldy432247bxlnzyn7468v3j1c34m7kvl47ji")))

(define-public crate-necrywagen-0.1.1 (c (n "necrywagen") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0vps1kzv270f90n53xhn1369bj52rpk6k3phflqkj93kawnam5zz")))

(define-public crate-necrywagen-0.2.0 (c (n "necrywagen") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "06gpbap4j65p843h2814n9b813zd39kp743rlrkgg2z2sgqgfkwp")))

