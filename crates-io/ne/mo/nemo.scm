(define-module (crates-io ne mo nemo) #:use-module (crates-io))

(define-public crate-nemo-0.0.1 (c (n "nemo") (v "0.0.1") (h "1ipnf8ndkhqisgig40kh84blk4hpj5amlx04pgwih9h3f7i8j5ys")))

(define-public crate-nemo-0.1.0 (c (n "nemo") (v "0.1.0") (h "1lfik0mnsbvjcl1lwz2r3v3kwzshbsl1cpbanby1mkclxq39m1w5")))

(define-public crate-nemo-0.1.1 (c (n "nemo") (v "0.1.1") (h "011pqlbf0xp12p05z3n5fj711zpgbp52iy8hl1wfsxmxc8w70qs3")))

(define-public crate-nemo-0.2.0 (c (n "nemo") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "09b6fa271r66jdy653l2b1dcam9z7g420fwdzq722z9lp24qc21f")))

(define-public crate-nemo-0.2.1 (c (n "nemo") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "16bnfl9x4ilka56rbmx7pvs646far0b7q6n0yps0c8qah86vsbf8")))

