(define-module (crates-io ne mo nemo157) #:use-module (crates-io))

(define-public crate-nemo157-0.1.0 (c (n "nemo157") (v "0.1.0") (h "1agkpy8m324q9vmxkszkj6k259jgxjnfvcrm611sdjzdbvqfp9ks")))

(define-public crate-nemo157-0.2.0 (c (n "nemo157") (v "0.2.0") (h "1f3z5im264zvq1l4cvm0d3bk2asw3swvqqh95ddspr3z2l44szjz")))

(define-public crate-nemo157-0.2.1 (c (n "nemo157") (v "0.2.1") (h "0p899idnzxk1f40ypjq7ibqpaqyfrax5z805wspm7c5ymqnh4rii") (y #t)))

(define-public crate-nemo157-0.3.0 (c (n "nemo157") (v "0.3.0") (h "0q66yd45pjh1196r1hf0ycdrw756xk72agxwqkdkifcw5ndm0qs8")))

(define-public crate-nemo157-0.3.1-pre (c (n "nemo157") (v "0.3.1-pre") (h "0zkscv95cbc3jspvp6r0y5hg2gwh75qx9h9ays99s5bgdidbq2pl")))

(define-public crate-nemo157-0.3.1-pre.1 (c (n "nemo157") (v "0.3.1-pre.1") (h "1j175vmyvp882ffw44rk1rwfrqbg97wdd2cdq2wdpklgcw36bw78")))

(define-public crate-nemo157-0.3.1-pre.3 (c (n "nemo157") (v "0.3.1-pre.3") (h "1zzdg4k5fwfiabki72z7zb4q1lz8v21hph23by5w3pafyil5mzza")))

