(define-module (crates-io ne mo nemo-extension) #:use-module (crates-io))

(define-public crate-nemo-extension-0.6.1 (c (n "nemo-extension") (v "0.6.1") (d (list (d (n "gio-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nemo-extension-sys") (r "^0.5.0") (d #t) (k 0)))) (h "083y1q937pgg9qzai9wkdxc97xqh56d33nnqkpy8srcp978scwpa")))

