(define-module (crates-io ne mo nemo-extension-sys) #:use-module (crates-io))

(define-public crate-nemo-extension-sys-0.5.0 (c (n "nemo-extension-sys") (v "0.5.0") (d (list (d (n "gio-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (f (quote ("v3_18"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j5afw2fv315r1sfxl65175kkx3xmp4qvh0wq6h20w1fbqxq4zba")))

