(define-module (crates-io ne ct necto) #:use-module (crates-io))

(define-public crate-necto-0.1.0 (c (n "necto") (v "0.1.0") (h "050ijnw7h9khhnyd324ma5lm5gvm2yrp5wnrp5pjs8ri2ydrfbvk")))

(define-public crate-necto-0.1.1 (c (n "necto") (v "0.1.1") (h "1a4vzx0v5c183aiysrf9cbr5qsanvgcjdcnv6vcwj10sr4dn47nx") (y #t)))

(define-public crate-necto-0.1.11 (c (n "necto") (v "0.1.11") (h "1rhjg6byghhqijb6886s4yizyxhx9c2k9n5kp61a4pa6a9dqj7gh")))

