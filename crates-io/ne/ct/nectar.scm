(define-module (crates-io ne ct nectar) #:use-module (crates-io))

(define-public crate-nectar-0.1.0 (c (n "nectar") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "07a9w7kwlji2idnxkcgmwipi7ar8d4aplf9a4qq6xgy7na9ff5j7")))

(define-public crate-nectar-0.2.0 (c (n "nectar") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1ydk4rirq5pknv33rcv0xp9gk8dv6jcryx5ppjn2bbr3bm4y6b9m")))

(define-public crate-nectar-0.3.0 (c (n "nectar") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1aad0dsx3c74r9fibyspsxcxmjqr7hqxq99927qcinyspixmxm8p")))

(define-public crate-nectar-0.4.0 (c (n "nectar") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1rhy47y5zdjivwlhly1fkjd3bqhi072c27byhdxmqrrkjhwi4iwy") (f (quote (("unicode"))))))

