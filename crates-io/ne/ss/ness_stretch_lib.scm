(define-module (crates-io ne ss ness_stretch_lib) #:use-module (crates-io))

(define-public crate-ness_stretch_lib-0.1.0 (c (n "ness_stretch_lib") (v "0.1.0") (d (list (d (n "bwavfile") (r "^2.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "realfft") (r "^3.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1") (d #t) (k 0)))) (h "0w9jcaspb9g1z8l2jfsawi7hnbsvqkkr51fs0y3vn1cy0di7fx5n")))

(define-public crate-ness_stretch_lib-0.1.2 (c (n "ness_stretch_lib") (v "0.1.2") (d (list (d (n "bwavfile") (r "^2.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "realfft") (r "^3.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1") (d #t) (k 0)))) (h "114xav1favlbmqaqgkg4p681fzpdxzc10a9dqprb940s46fmsnx9")))

