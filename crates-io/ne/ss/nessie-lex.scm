(define-module (crates-io ne ss nessie-lex) #:use-module (crates-io))

(define-public crate-nessie-lex-0.1.0 (c (n "nessie-lex") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nessie-lex-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "18bsi8gy719385wcly2mjf01mfpa0z9cdvcl6b6l01jshjynfygb")))

(define-public crate-nessie-lex-0.1.1 (c (n "nessie-lex") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nessie-lex-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0sgpg396hhn2dbq57bbdvzmdm58y8k9qrnl8iq598b06ghbx1d94")))

(define-public crate-nessie-lex-0.1.2 (c (n "nessie-lex") (v "0.1.2") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nessie-lex-proc-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1s5208c6b6virs8x1lls0qc48p8gxkim84828d4qlwpl403av8jm")))

(define-public crate-nessie-lex-0.1.3 (c (n "nessie-lex") (v "0.1.3") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nessie-lex-proc-macros") (r "^0.1.1") (d #t) (k 0)))) (h "01imx9yhdzqi33pz91iv6jbd2q1k9ha0pnjwdhw2kqsmz63h8bma")))

(define-public crate-nessie-lex-0.1.4 (c (n "nessie-lex") (v "0.1.4") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nessie-lex-proc-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0hwqd3bgmm80hpih4bzv1qnghw848s02nczhwj46l33nl07qhbh7")))

