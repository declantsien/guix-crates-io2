(define-module (crates-io ne ss ness_stretch) #:use-module (crates-io))

(define-public crate-ness_stretch-0.4.0 (c (n "ness_stretch") (v "0.4.0") (d (list (d (n "bwavfile") (r "^0.9.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "realfft") (r "^2.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "0y740188vna9fn5f2zr855c0yx6ivp17asqd6c186i4ksp81xq0s") (y #t)))

(define-public crate-ness_stretch-0.4.1 (c (n "ness_stretch") (v "0.4.1") (d (list (d (n "bwavfile") (r "^0.9.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "realfft") (r "^2.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "0kjhwiyzfp3sgvw7fcs2bkgz37b8rvy1saldyffiw34kjicdy3iz")))

(define-public crate-ness_stretch-0.4.3 (c (n "ness_stretch") (v "0.4.3") (d (list (d (n "bwavfile") (r "^2.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "klask") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "realfft") (r "^3.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1") (d #t) (k 0)))) (h "0idp6fr3ww97vzi85pa7zidiwr2zp5f7mb4wa6s510qli0d2bl1v")))

(define-public crate-ness_stretch-0.5.0 (c (n "ness_stretch") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ness_stretch_lib") (r "^0.1.2") (d #t) (k 0)))) (h "1xbxgda5q5pb6110hhawy0w39qwnn8g4hsl9yzs4amribarx0gwv")))

(define-public crate-ness_stretch-0.5.1 (c (n "ness_stretch") (v "0.5.1") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ness_stretch_lib") (r "^0.1.2") (d #t) (k 0)))) (h "1c3yhfgqzwahaasjgk8swjsxns9nkch8ym8pb9wh6kb8q6qafmhw")))

