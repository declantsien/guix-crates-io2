(define-module (crates-io ne ss nessie-lex-proc-macros) #:use-module (crates-io))

(define-public crate-nessie-lex-proc-macros-0.1.0 (c (n "nessie-lex-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0n9ggf58v810qlmlna927vknbx8pk8zjnd2xmrhb3l1fcd9d22pc")))

(define-public crate-nessie-lex-proc-macros-0.1.1 (c (n "nessie-lex-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0sv8zh9m4wv8ahh9954l8iqlfsb4yls2vnyv067f3qban3wyhyi7")))

