(define-module (crates-io ne ss nessus_xml_parser) #:use-module (crates-io))

(define-public crate-nessus_xml_parser-0.1.0 (c (n "nessus_xml_parser") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15pzb4ykamar441xzck2q1l9jmham0nfvlwmq4iyk0fgngxn685q")))

