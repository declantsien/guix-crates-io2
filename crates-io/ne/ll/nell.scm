(define-module (crates-io ne ll nell) #:use-module (crates-io))

(define-public crate-nell-0.0.0 (c (n "nell") (v "0.0.0") (h "0ahd0rscaarsnygcjilz1wgb2pxddgcmx1byhl5ic7aqbpg0vzsl")))

(define-public crate-nell-0.0.1 (c (n "nell") (v "0.0.1") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)))) (h "1g0q3hg20nr941y7drx4kilarh9mmma6qngx0y21diclqi8l6piz")))

(define-public crate-nell-0.0.2 (c (n "nell") (v "0.0.2") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "eui48") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)))) (h "0b61k35wkwasylaclj361faj8ww16pzk4ny99v923g4rw15wqbga")))

(define-public crate-nell-0.0.3 (c (n "nell") (v "0.0.3") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "eui48") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)))) (h "14476gbr5xhwl35vs38ryf2bi74iiarsssyrdkry64s4n1d8y5fr")))

(define-public crate-nell-0.1.0 (c (n "nell") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.18") (d #t) (k 2)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0z468fcwbw4iivkd2slsyw4b7zj2x138xx58is0y1yd84nmcmz9n") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-nell-0.2.0 (c (n "nell") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 2)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "078259lp714lxkqpp8snydw60s1l7nr4c7amv2frygjqs43l6h0f") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-nell-0.3.0 (c (n "nell") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)))) (h "1yzwfdsq9142f9vlywiabpdp08idwbza0jgp0vs8n606yq6ajjn0")))

