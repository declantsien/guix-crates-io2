(define-module (crates-io ne wr newrelic) #:use-module (crates-io))

(define-public crate-newrelic-0.0.1 (c (n "newrelic") (v "0.0.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "14wv3f3rxglw8nzbg6jnqb3v9sfdyw5id24y5sdp5rgpcic66bcx")))

(define-public crate-newrelic-0.0.2 (c (n "newrelic") (v "0.0.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "012z76cjcwrdv663l6np5dkwp987wh153q73b1f7xgiq83s6d99z")))

(define-public crate-newrelic-0.1.0 (c (n "newrelic") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "> 0.4.0") (d #t) (k 0)) (d (n "newrelic-sys") (r "^0.1") (d #t) (k 0)))) (h "1577kvyvb5rc6adll7sv9qyp58k5y1bmp89xp51mv22mr8acznng")))

(define-public crate-newrelic-0.2.0 (c (n "newrelic") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "> 0.4.0") (d #t) (k 0)) (d (n "newrelic-sys") (r "^0.1") (d #t) (k 0)))) (h "1ix0q24whp91dcybm3g681d2vakb4qkpghbhsd1azhw8g0qxygz5")))

(define-public crate-newrelic-0.2.2 (c (n "newrelic") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "> 0.4.0") (d #t) (k 0)) (d (n "newrelic-sys") (r "^0.2") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1a738cihcb3ylsl5nimd2h8a8ra42rcpscgzi0nkrlyc2arixv7p") (f (quote (("distributed_tracing" "libc") ("default") ("async" "pin-project"))))))

