(define-module (crates-io ne wr newron) #:use-module (crates-io))

(define-public crate-newron-0.1.0 (c (n "newron") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "1cx8ffqif25p2si06kdh0za0cnzmhmvgih8nhmqav7hsff7idbas")))

(define-public crate-newron-0.1.1 (c (n "newron") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "16dfmc755hv5w1411zzyx28nwqll4z6j5q3j2fhwxgicgz6da139")))

(define-public crate-newron-0.1.2 (c (n "newron") (v "0.1.2") (h "0a9sgbxq81gqcbzcc1fznvrgn3fcjqvv7rgjr0nkp6hq10xacvr7")))

(define-public crate-newron-0.2.0 (c (n "newron") (v "0.2.0") (h "0b7y03kpij1154r7mvx3ajvjchkpqz4n06rmd78s5qpvs8nvwn8c")))

(define-public crate-newron-0.3.0 (c (n "newron") (v "0.3.0") (h "1r1lfkpwj0yina1z0ajq4zlbx7xbx4x1809dlqm8d9i69fllf8rw")))

(define-public crate-newron-0.4.0 (c (n "newron") (v "0.4.0") (h "16qvyicz5ybb56rxw2apmjd0r48yzwnl8q73w2b5r6jig0wrxnj6")))

(define-public crate-newron-0.5.0 (c (n "newron") (v "0.5.0") (h "0n010fgsldbcg6rlwpckixa9j8cdg5q199ggzmca74zxq8xifnwl")))

(define-public crate-newron-0.5.1 (c (n "newron") (v "0.5.1") (h "0bxjzs351yzd3amg9hjhf12m75lihb724l9n3dpqc48fdzw2laqa")))

