(define-module (crates-io ne wr newrelic-sys) #:use-module (crates-io))

(define-public crate-newrelic-sys-0.1.0 (c (n "newrelic-sys") (v "0.1.0") (h "0lkvmilyp5hy9mc7p17gs81bxr2b74x2g8cnqp1qhzy439dbivf5") (l "newrelic")))

(define-public crate-newrelic-sys-0.2.0 (c (n "newrelic-sys") (v "0.2.0") (h "0fpr99xcjdqpgz81qmmhp4y7vq10j3wflvlmzcmaqczlnlq916qa") (l "newrelic")))

