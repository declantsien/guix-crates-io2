(define-module (crates-io ne wr newrelic-sdk) #:use-module (crates-io))

(define-public crate-newrelic-sdk-0.0.3 (c (n "newrelic-sdk") (v "0.0.3") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "12qy3q4y7dxnlp9n9g1xpwixdfhgk0nxh6c1zsisdm53qa3sk5v7")))

