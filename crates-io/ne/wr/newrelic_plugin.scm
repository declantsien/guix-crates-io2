(define-module (crates-io ne wr newrelic_plugin) #:use-module (crates-io))

(define-public crate-newrelic_plugin-0.1.0 (c (n "newrelic_plugin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "curl") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05vp806g77m4rsm9x6b3mqg4gnnwpzdd7dfzqky4ssl8c9p4hcw0")))

