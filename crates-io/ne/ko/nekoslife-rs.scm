(define-module (crates-io ne ko nekoslife-rs) #:use-module (crates-io))

(define-public crate-nekoslife-rs-0.1.1 (c (n "nekoslife-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0p9gym1p9awwacxbzx2vhx2a3qqdczfw4f2cyag7kz306qbzqgyr")))

(define-public crate-nekoslife-rs-0.1.2 (c (n "nekoslife-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1kkzl8s6dzhscx2m4w3934nmm51b7isz2avykvzaazv5r0k3yhjk")))

