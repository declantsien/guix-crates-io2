(define-module (crates-io ne ko neko-image) #:use-module (crates-io))

(define-public crate-neko-image-0.0.2 (c (n "neko-image") (v "0.0.2") (d (list (d (n "arboard") (r "^3.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bzs4hh3z2wzsrxznkcy4n51dnry0pp8flij26m8c9wdgbph4935")))

(define-public crate-neko-image-0.0.3 (c (n "neko-image") (v "0.0.3") (d (list (d (n "arboard") (r "^3.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0lyzinq4mxq8mc2hw3ff8v3q37mpn4339i220g5ayiqcn40k7lam")))

