(define-module (crates-io ne ko nekocli) #:use-module (crates-io))

(define-public crate-nekocli-0.1.0 (c (n "nekocli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04swxmvrcabgkkvsbg3b451gsvh64snbwljaf8pv7pd4xh6awld7") (y #t)))

