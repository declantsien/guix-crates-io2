(define-module (crates-io ne s- nes-yew) #:use-module (crates-io))

(define-public crate-nes-yew-0.0.1 (c (n "nes-yew") (v "0.0.1") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0jc8fjv130hxk8v2q3ncdaxjrswnyn953b0jyyh6iz81z9chmf47")))

(define-public crate-nes-yew-0.0.2 (c (n "nes-yew") (v "0.0.2") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "11dm4wkia3jvigyxk0z8lazh3m6xj4dsd65mj6ngbpg0xnfbx3n0")))

