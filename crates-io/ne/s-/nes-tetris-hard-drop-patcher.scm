(define-module (crates-io ne s- nes-tetris-hard-drop-patcher) #:use-module (crates-io))

(define-public crate-nes-tetris-hard-drop-patcher-0.1.0 (c (n "nes-tetris-hard-drop-patcher") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "ines") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mos6502_assembler") (r "^0.2") (d #t) (k 0)) (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "160i3x69kilgpmj8ndrdsiwjshb5a0lvwcf5z1mwpcci7fpbk4wd")))

(define-public crate-nes-tetris-hard-drop-patcher-0.1.1 (c (n "nes-tetris-hard-drop-patcher") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "ines") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mos6502_assembler") (r "^0.2") (d #t) (k 0)) (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "016sql0absyz1vfb6pgqzrkbz3ili02bv3ibcvfy4dp8wczyw44s")))

(define-public crate-nes-tetris-hard-drop-patcher-0.1.2 (c (n "nes-tetris-hard-drop-patcher") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "ines") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "meap") (r "^0.4") (d #t) (k 0)) (d (n "mos6502_assembler") (r "^0.2") (d #t) (k 0)) (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "10d35s5sn7fcs7xsn49bv2gwb1lvnwpzc40is46md736ci317kbh")))

