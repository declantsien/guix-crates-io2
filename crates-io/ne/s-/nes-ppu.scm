(define-module (crates-io ne s- nes-ppu) #:use-module (crates-io))

(define-public crate-nes-ppu-0.1.0 (c (n "nes-ppu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jps5wvlj7ghbfa2iz4iqkvr31f0xcx4q9h6qkljmjshzg0xkq4q")))

(define-public crate-nes-ppu-0.1.1 (c (n "nes-ppu") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z5126pyjchfnpbz7ag416v4j2l5f96kinfpi0y61b0pn490d52y")))

(define-public crate-nes-ppu-0.2.0 (c (n "nes-ppu") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xpkjpdmxnn4fkp0p8fxflccy6ndlkl4364syhqsagr3mv34w0k")))

