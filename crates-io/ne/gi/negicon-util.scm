(define-module (crates-io ne gi negicon-util) #:use-module (crates-io))

(define-public crate-negicon-util-0.1.0 (c (n "negicon-util") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "negicon-protocol") (r "^0.2.7") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "0p39gn387a41ks9vbi840wf14y40x5367drhrlg9wydnnbzgdgac")))

(define-public crate-negicon-util-0.1.1 (c (n "negicon-util") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "negicon-protocol") (r "^0.2.7") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "10jq60y95pi2b2116fif66s2ci0qmbk5y5lia5yahcmczhrys9l7")))

(define-public crate-negicon-util-0.1.2 (c (n "negicon-util") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "negicon-protocol") (r "^0.3.2") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "0s69wwh5kg88x4zhaaqbp8csyf1py46l8cvl5cclp48c8l1jav97")))

(define-public crate-negicon-util-0.1.3 (c (n "negicon-util") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "negicon-protocol") (r "^0.3.2") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "0qxwijna3hpzb5lsl88sdkxni97vrl75hw8xx7732rmm95l6lwv9")))

