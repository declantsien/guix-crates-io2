(define-module (crates-io ne gi negicon-protocol) #:use-module (crates-io))

(define-public crate-negicon-protocol-0.1.0 (c (n "negicon-protocol") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "1r0wndj4kdal48cxa9ynrh5w6rgd01r2s2y74pfidgsgqlk0x2qi")))

(define-public crate-negicon-protocol-0.1.1 (c (n "negicon-protocol") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "02arf829ni4y6c0ifpj368x5hqrakj4w2hbfvx69jdnrc4yv1j38")))

(define-public crate-negicon-protocol-0.1.2 (c (n "negicon-protocol") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "1nr4xiajfwsy2n48y2iwfcqvj5ai2lccakcv191xhl1pxdhbya7i")))

(define-public crate-negicon-protocol-0.1.3 (c (n "negicon-protocol") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "0yy16yigh8bnnsnkbmm3svkgmb69iaydpfd51b3ir650s0825zq6")))

(define-public crate-negicon-protocol-0.1.4 (c (n "negicon-protocol") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "1m2df43jf5yi2kbys2zcbwk0nfljp0bh3a920xwwfgxn9nz579xp")))

(define-public crate-negicon-protocol-0.2.0 (c (n "negicon-protocol") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "022a5ha0r3kfcnf9krxwliq3szh06413sql6k0ami3wvccy33adw")))

(define-public crate-negicon-protocol-0.2.1 (c (n "negicon-protocol") (v "0.2.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "19pc5v9zd9kjngd49lvjvza2wyx1171285bngf40m4kzyqkipmxc")))

(define-public crate-negicon-protocol-0.2.2 (c (n "negicon-protocol") (v "0.2.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "11cszyz2ihw2hz7kqf5ysjvjfjzwda9a3d52hnrawy77c7x43p8h")))

(define-public crate-negicon-protocol-0.2.3 (c (n "negicon-protocol") (v "0.2.3") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "14r3677pqd29rkfwwarnrnar92r9vv0q2ackrixhmbqq58jjmnnk")))

(define-public crate-negicon-protocol-0.2.4 (c (n "negicon-protocol") (v "0.2.4") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "15bpqfy3j34r5z3nzbcbky94mb2zaqlv2pqb1a89ksppzqmj6v5b")))

(define-public crate-negicon-protocol-0.2.5 (c (n "negicon-protocol") (v "0.2.5") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "122nyn2rcz8wwc1j34bfi9fkwl5lk32vl587200mjhnqgv42y563")))

(define-public crate-negicon-protocol-0.2.6 (c (n "negicon-protocol") (v "0.2.6") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "15a3nr71app6x7ngwaa948a4glzbnrdq4v0fgii76dwx8ja34y66")))

(define-public crate-negicon-protocol-0.2.7 (c (n "negicon-protocol") (v "0.2.7") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "1l1j4ih0i5s8pga7v7m7i557pbqbx03slxriygciah1r456jyrvh")))

(define-public crate-negicon-protocol-0.2.8 (c (n "negicon-protocol") (v "0.2.8") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "0rpfdvabnyvys63g0711c5414aq91la4dl8zgc24q5q7z3l2jza7")))

(define-public crate-negicon-protocol-0.3.0 (c (n "negicon-protocol") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "0gips22615mmx179pv5ailllhdwwk37z09516b4ijyx5ss5mim0p")))

(define-public crate-negicon-protocol-0.3.1 (c (n "negicon-protocol") (v "0.3.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "1df31195mgx3pfbyhpw0prmvisdnhrcqyh13d6nrr8gx5avdmy0g")))

(define-public crate-negicon-protocol-0.3.2 (c (n "negicon-protocol") (v "0.3.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "0f0wx97g9rp2xj7wx3589gqk8a1rxiv0chb1lc9rv0nxicdznyl7")))

(define-public crate-negicon-protocol-1.0.0 (c (n "negicon-protocol") (v "1.0.0") (d (list (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (k 0)))) (h "0w6r2ysr6avzy9byywhxip8b2hhzyj90abnmrgqlqbjdkgvvlfdc")))

