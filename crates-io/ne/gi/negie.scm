(define-module (crates-io ne gi negie) #:use-module (crates-io))

(define-public crate-negie-0.1.0 (c (n "negie") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0iiilyxcq3qf34w4a5rr9ahwlin911dm5slcjpp05kl0sqfm2pk1")))

(define-public crate-negie-0.1.1 (c (n "negie") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1g0v909bpf88918rc5vnqy5q9wyrykaliih4gqrqjgbi15qln1xw")))

(define-public crate-negie-0.1.2 (c (n "negie") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1jlbfnavpi396y7zlfc00h9pg0fisxlm6j1rgvy4nx6djbrlsxj1")))

(define-public crate-negie-0.1.3 (c (n "negie") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0a04l564jldfhbqm7xqc6r2l3rcx3a25bfs5lyf0iphpk7wci1j0")))

