(define-module (crates-io ne at neat-date-time) #:use-module (crates-io))

(define-public crate-neat-date-time-0.1.0 (c (n "neat-date-time") (v "0.1.0") (h "1bipi2al5rakfr1zh70r7xxsa3ja1pdghlsy1ipavkl9jmi05a0z") (y #t)))

(define-public crate-neat-date-time-0.1.1 (c (n "neat-date-time") (v "0.1.1") (h "1nai8q4xqx57v086yn2r27rgiihv9fslcpc6cpy6hb73d52i48hq") (y #t)))

(define-public crate-neat-date-time-0.1.2 (c (n "neat-date-time") (v "0.1.2") (h "0660kmg44wnj0r22c9xv26mvbh2i69iwzc3iyf7lb3swfy71cjpm") (y #t)))

(define-public crate-neat-date-time-0.1.3 (c (n "neat-date-time") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1r2wymqjwkcf0qbpcn9albqvfbn6fxbx11f1yxw1faxax88i9ccs") (y #t)))

(define-public crate-neat-date-time-0.1.4 (c (n "neat-date-time") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0g5lqw9mh4cd6m8c2yr0bwmvka411nqqvi5k69vihng3yqjl44gb") (y #t)))

(define-public crate-neat-date-time-0.1.5 (c (n "neat-date-time") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0dp2g4kr8p81h9s0cgk10jvlacrwkdy3ymvy49k84iky2rgy8skg") (y #t)))

(define-public crate-neat-date-time-0.1.6 (c (n "neat-date-time") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0wjr5k5qchr17gg33sfz9ihgn0gbgkr32v0j0fl60pgvhsav1vd2") (y #t)))

(define-public crate-neat-date-time-0.1.7 (c (n "neat-date-time") (v "0.1.7") (h "17rdfafskk5ra58qpm442q6i2j556qx05hzvlaqfflgmjxvg4bj8") (y #t)))

(define-public crate-neat-date-time-0.1.8 (c (n "neat-date-time") (v "0.1.8") (h "0l6fb6cw424cv2rxvl42rffwyw3hb5hjy1k9v7dbzbpfwvw9h3d3")))

(define-public crate-neat-date-time-0.1.9 (c (n "neat-date-time") (v "0.1.9") (h "03gvzgrpksx01jswr1briwdfgz7m3wf16i5kbi11xdg6dxqvnai1")))

(define-public crate-neat-date-time-0.2.0 (c (n "neat-date-time") (v "0.2.0") (h "086x17b74rac23q2mbll6cajx6kir3axg3anqyh0pm3qqm6r6148")))

