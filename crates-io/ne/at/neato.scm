(define-module (crates-io ne at neato) #:use-module (crates-io))

(define-public crate-neato-0.1.0 (c (n "neato") (v "0.1.0") (h "1dk8x0iahmbwswnbhw5hzdpad9qskgrdasd0v4f9xhsh07y93whh")))

(define-public crate-neato-0.1.1 (c (n "neato") (v "0.1.1") (h "18x3zlhzi2msxhc3zp5g9xbjksq64hmwh42p6fl39gvpbrihd7aw")))

