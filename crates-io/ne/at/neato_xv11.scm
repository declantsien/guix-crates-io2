(define-module (crates-io ne at neato_xv11) #:use-module (crates-io))

(define-public crate-neato_xv11-0.1.0 (c (n "neato_xv11") (v "0.1.0") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "1dggi420mp8nl6wfbhz6brq7x7rmxn11bl03mwhv3y49wniicwfq")))

(define-public crate-neato_xv11-0.1.1 (c (n "neato_xv11") (v "0.1.1") (d (list (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0ppb94qii4x09lsv5bii6dyclr5n0hzidfgrm5ap22azwdvvdbym")))

