(define-module (crates-io ne at neat-rs) #:use-module (crates-io))

(define-public crate-neat-rs-0.1.0 (c (n "neat-rs") (v "0.1.0") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "16v10a3m0mg2m5xldxd4awalm5xv8l54x06z9xav1j98r4cjqnwc")))

(define-public crate-neat-rs-0.1.1 (c (n "neat-rs") (v "0.1.1") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "076v59yac5rkw240pcwjk6g40nn151w97b7wrp2fxqvwcl4gx57k")))

(define-public crate-neat-rs-0.1.2 (c (n "neat-rs") (v "0.1.2") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "18qvbwjk1qbin22v46kxa8zvvhrj3d56k378612hx903asfw7g3j")))

(define-public crate-neat-rs-0.1.3 (c (n "neat-rs") (v "0.1.3") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "05w1802v9zicwhjqb540kj5n83hzgsjh3l9dsl14p4dvz2ww5rrp")))

(define-public crate-neat-rs-0.1.4 (c (n "neat-rs") (v "0.1.4") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "06702q4ym9b1pj3yd1hjhklvpsc4x630p8mivvanr46sj7h6xvr3")))

(define-public crate-neat-rs-0.1.5 (c (n "neat-rs") (v "0.1.5") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "1mvs8gfk65if64qbllx6ngxx3l5p9lipy66h2rpsa82nkz5b32kv")))

(define-public crate-neat-rs-0.1.6 (c (n "neat-rs") (v "0.1.6") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "1pmg32mbyhhbzd2dggwrs9qvd1ylr1wb9ysidkibmr0w9m1k0sjz")))

(define-public crate-neat-rs-0.1.7 (c (n "neat-rs") (v "0.1.7") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.1") (d #t) (k 0)))) (h "00fvfm6hscgcdkhiiclbk7jvlbw5l5v3qabddlfqdanvk3ijh900")))

(define-public crate-neat-rs-0.1.8 (c (n "neat-rs") (v "0.1.8") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.2") (d #t) (k 0)))) (h "1knk069khwpjyw8jdxg2fmi05mglvd9mrd9sh4jmnh2snv5a06j2")))

(define-public crate-neat-rs-0.1.81 (c (n "neat-rs") (v "0.1.81") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.21") (d #t) (k 0)))) (h "0n43zvnvjzxqgy8n03mpdpykm8kp095w7522krwsihiwfzw85pm6")))

(define-public crate-neat-rs-0.1.82 (c (n "neat-rs") (v "0.1.82") (d (list (d (n "gym") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slow_nn") (r "^0.1.22") (d #t) (k 0)))) (h "089lclcb3zhfs27pg1xf2dvm4v943hb7i9d2wk4swdvl5q2x5884")))

