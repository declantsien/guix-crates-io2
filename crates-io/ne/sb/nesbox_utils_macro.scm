(define-module (crates-io ne sb nesbox_utils_macro) #:use-module (crates-io))

(define-public crate-nesbox_utils_macro-0.0.1 (c (n "nesbox_utils_macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sl9xq72im3h3jsvnv0b2jm8agi2l7apf4pa74bsxx6dx2kmahjv")))

(define-public crate-nesbox_utils_macro-0.0.2 (c (n "nesbox_utils_macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07s77crhphgcx93fdf0lij4251bnc4xckkhqz1pbpa1ncgbgwj4m")))

(define-public crate-nesbox_utils_macro-0.0.3 (c (n "nesbox_utils_macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qqkfin89hd906fprghb3dmh1w3sc13j7ymswp1v5nskmda2h1jc")))

