(define-module (crates-io ne sb nesbox_utils) #:use-module (crates-io))

(define-public crate-nesbox_utils-0.0.1 (c (n "nesbox_utils") (v "0.0.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nesbox_utils_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "qoi") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "19lmh46f0prygr5p9yfl1dwigv04z3sf3v4jbwf0nhbm9n7zyp1p")))

(define-public crate-nesbox_utils-0.0.2 (c (n "nesbox_utils") (v "0.0.2") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nesbox_utils_macro") (r "^0.0.2") (d #t) (k 0)) (d (n "qoi") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "0f929278zgzan8cabwir7xj7hjvnj73hqrrq533imlaykm60h8x0")))

(define-public crate-nesbox_utils-0.0.3 (c (n "nesbox_utils") (v "0.0.3") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nesbox_utils_macro") (r "^0.0.3") (d #t) (k 0)) (d (n "qoi") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "10jac4rdxkmmv6fkdb9vng8n7ak4kaqijslfazhgz9z0g440v7c8")))

