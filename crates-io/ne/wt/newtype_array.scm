(define-module (crates-io ne wt newtype_array) #:use-module (crates-io))

(define-public crate-newtype_array-0.1.0 (c (n "newtype_array") (v "0.1.0") (h "0jn83h97n6ahsldbjs4140c56fg2jdshwgdaf08zj39law3xc656")))

(define-public crate-newtype_array-0.1.1 (c (n "newtype_array") (v "0.1.1") (h "052m6rw3c1cbdimvb4dma2fig4z2lihqlx3yac1h8a9fhjzd6r9f")))

(define-public crate-newtype_array-0.1.2 (c (n "newtype_array") (v "0.1.2") (h "1zg3ngzmc82gwnwfvbxk537dwl4jdkb5k8dckvrg3w8a184z6gnh")))

(define-public crate-newtype_array-0.1.3 (c (n "newtype_array") (v "0.1.3") (h "1qa67a5gc39asxpdpjizm1kv98r0433vadlv88c7c7yh4bqcdllw")))

(define-public crate-newtype_array-0.1.4 (c (n "newtype_array") (v "0.1.4") (h "13hwfwa2qdr8x9n3rm0haan5mv8a7y6j0md9bxmkiy37bwxlfh7s")))

(define-public crate-newtype_array-0.1.5 (c (n "newtype_array") (v "0.1.5") (h "1s83w6c3a92a9lwvrzlhlvryric73ni0q7g8w6m977jyv4xyv7wv")))

(define-public crate-newtype_array-0.1.6 (c (n "newtype_array") (v "0.1.6") (h "101qjkymks7dyvaw0qz2mv612y3rad4a226xfm542zbq1nhlkqw3")))

