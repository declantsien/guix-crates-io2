(define-module (crates-io ne wt newt_proc_macros) #:use-module (crates-io))

(define-public crate-newt_proc_macros-0.0.2 (c (n "newt_proc_macros") (v "0.0.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00fwmq983bk1zpqjrq0fmx8d1l3vf8xg8v9sp36l943m7iq7xzjf")))

(define-public crate-newt_proc_macros-0.0.3 (c (n "newt_proc_macros") (v "0.0.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1pjcrjfgxcspdkwbljbailb53mibln0rhczw7ydnn8r6zy72pq28")))

(define-public crate-newt_proc_macros-0.0.4 (c (n "newt_proc_macros") (v "0.0.4") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0mc6j2q92f0xa963hw0h3z78lbjjlqlv7i6531042lznj7z7c37h")))

(define-public crate-newt_proc_macros-0.0.5 (c (n "newt_proc_macros") (v "0.0.5") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0friak228qmkdn3yv47idicwvrszgph99zqyawh8hzjp0gcrfrpl")))

(define-public crate-newt_proc_macros-0.0.6 (c (n "newt_proc_macros") (v "0.0.6") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0l3i8dsyjcryfkcixj35frbh1lb94kspvbhz3kh5nllvprjxhsna")))

(define-public crate-newt_proc_macros-0.0.7 (c (n "newt_proc_macros") (v "0.0.7") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1x4gzps0r02awngx138ckbi9f9aqpai0nqcwj4y18g7js7498bm3")))

(define-public crate-newt_proc_macros-0.0.8 (c (n "newt_proc_macros") (v "0.0.8") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "105rcn5drni5q80i1m8p36b19xvncnvxfkf75y2c6yl8jgi9zymz")))

(define-public crate-newt_proc_macros-0.0.9 (c (n "newt_proc_macros") (v "0.0.9") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "14l43rbckfwnxdjx4xjpiw1kxk4yq013kppfrnhd7w66anqf0vqv")))

(define-public crate-newt_proc_macros-0.0.10 (c (n "newt_proc_macros") (v "0.0.10") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "19yx3jz4261vr0plwnykbvhp2rsc0v8gyd234l2dlwk6q4h8gv61")))

