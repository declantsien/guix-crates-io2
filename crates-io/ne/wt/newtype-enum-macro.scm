(define-module (crates-io ne wt newtype-enum-macro) #:use-module (crates-io))

(define-public crate-newtype-enum-macro-0.1.0 (c (n "newtype-enum-macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1y8p02kxpc4bzv6yhabq9z3n110rdcay01lrpjxbmhyscndfqz0x")))

