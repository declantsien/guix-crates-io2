(define-module (crates-io ne wt newtype-ops) #:use-module (crates-io))

(define-public crate-newtype-ops-0.1.2 (c (n "newtype-ops") (v "0.1.2") (h "03c3qv8xbznrdhm0p8yv7y8yalmxhcbd8axsccrjx22rr4az2ar0")))

(define-public crate-newtype-ops-0.1.3 (c (n "newtype-ops") (v "0.1.3") (h "1bfslzgfw8qg2bvm4cn7dj6imyck2fl9kqq1wgh9zy4sx6fbyw5l")))

(define-public crate-newtype-ops-0.1.4 (c (n "newtype-ops") (v "0.1.4") (h "0y3alcxsi57aj5lxpvajg25cs5csrqk9l1kv1sv9gvv9dks4fq6k") (f (quote (("debug-trace-macros"))))))

