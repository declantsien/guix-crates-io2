(define-module (crates-io ne wt newt_component_derive) #:use-module (crates-io))

(define-public crate-newt_component_derive-0.0.0 (c (n "newt_component_derive") (v "0.0.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1vqip64dkza2nqk42asgcflm3z2hkgnphd0gpam99ngi5zpwxzcs") (y #t)))

(define-public crate-newt_component_derive-0.0.1 (c (n "newt_component_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1665hplcc0simwn8kp23bcvv9hizgcn9qwwdaq20j5q4h07zfs2h") (y #t)))

