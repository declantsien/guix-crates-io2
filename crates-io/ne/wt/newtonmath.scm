(define-module (crates-io ne wt newtonmath) #:use-module (crates-io))

(define-public crate-newtonmath-0.1.0 (c (n "newtonmath") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "098n0aq9h5sn17z45hmfzvdsyq6r0l0k3irn9c0a9i4600qh5184")))

(define-public crate-newtonmath-0.2.0 (c (n "newtonmath") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0rj2n2yxkix5npqk77i197yfvvlyn0q76q4jjxqsx4ffwiq22d78")))

(define-public crate-newtonmath-0.2.1 (c (n "newtonmath") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "051cryg9h2m11hfaiw2c93i8rjjnvbyn1glysc69zdz2mzkz34fj")))

(define-public crate-newtonmath-0.2.2 (c (n "newtonmath") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "10w1wdpfbqr7fxicmm094jw025mxbjdbb9ziiwz268kncsf72x1z")))

(define-public crate-newtonmath-0.3.0 (c (n "newtonmath") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "04wv97yzii7q8dns4djxgwhq97nnk309qblbydh9mfb9haavisq7")))

