(define-module (crates-io ne wt newtype_derive) #:use-module (crates-io))

(define-public crate-newtype_derive-0.1.1 (c (n "newtype_derive") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "0fxlabws0fyxnmq4dyqq3y08xbs1qsp6jm5ppfd0wd5ln3nhycqr")))

(define-public crate-newtype_derive-0.1.2 (c (n "newtype_derive") (v "0.1.2") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "05k0pbbidymnmin21by9lvkyygk9d1pppqii47c4d2jai0dx5rca")))

(define-public crate-newtype_derive-0.1.3 (c (n "newtype_derive") (v "0.1.3") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "18chb8xbpdyp02vr9cd949l6wfs38n7hmrynzaa4nfm6qyzd5z27")))

(define-public crate-newtype_derive-0.1.4 (c (n "newtype_derive") (v "0.1.4") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "0b0fwganp5isjadvvs69rs69xmkz8xc2hqr1irfqmxaw7q19nrsb") (f (quote (("std-unstable"))))))

(define-public crate-newtype_derive-0.1.6 (c (n "newtype_derive") (v "0.1.6") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1v3170xscs65gjx5vl1zjnqp86wngbzw3n2q74ibfnqqkx6x535c") (f (quote (("std-unstable") ("std") ("default" "std"))))))

