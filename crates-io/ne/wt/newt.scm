(define-module (crates-io ne wt newt) #:use-module (crates-io))

(define-public crate-newt-0.1.0 (c (n "newt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "08bjmwcaf998im010h93s3zxrs13ypc6l2zfryfmm6rkqydixnpf")))

(define-public crate-newt-0.1.1 (c (n "newt") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0yggn3k58gam4bxq0xxybxqjwm3lxpp9kgbw24p4fh04gdwgsydy") (l "newt")))

(define-public crate-newt-0.2.0 (c (n "newt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "147hk7hwfl25d0w7qlwqm0w6p3gy8wgqczzcix1fffg2l1gkc05c") (l "newt")))

(define-public crate-newt-0.2.1 (c (n "newt") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0jh4vdnziy1c45wwjsjc5q62nsxbbnx820kgy57adg7013ink34h")))

(define-public crate-newt-0.2.2 (c (n "newt") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0wwjh5r4d0h88k6g3a6kglp4ychxvxpb5f9zg3vw09dgjfznb5c0") (f (quote (("static" "newt-sys/static"))))))

(define-public crate-newt-0.3.0 (c (n "newt") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "newt_component_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0j6i7n3v2lng5lipa58b70r7mziq5kxv9383ha7vwvwp8c8j8kjf") (f (quote (("static" "newt-sys/static"))))))

(define-public crate-newt-0.4.0 (c (n "newt") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "newt_component_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0kznq144d8qk3ssbglyr3n0x22j34ix4h830m7b6gnq51x1s22ns") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.0 (c (n "newt") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1") (d #t) (k 0)) (d (n "newt_proc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1d90g11zh1cj74smcqzl40jbnrmg2wdhbdmbgmddyshwiy9812qb") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.1 (c (n "newt") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1") (d #t) (k 0)) (d (n "newt_proc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0350pfnmshz8cgrwykzhaniiji96qn55w61ab2d6g1jqkpa78yvd") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.2 (c (n "newt") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1") (d #t) (k 0)) (d (n "newt_proc_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0x921hak91i2xskp0krvpinbvxky9ydc1pjaz900miblqczkbnlz") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.3 (c (n "newt") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "^0.1") (d #t) (k 0)) (d (n "newt_proc_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1b8r3cq6lf348ai2fvpvngvz80md1djqzrqkx7pa2ddzhar1cikj") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.4 (c (n "newt") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "= 0.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0zsg762m1w214gbjrdy8bl5iqcma74drn1zbfv8s3wggxrm9bci4") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.5.5 (c (n "newt") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "= 0.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "12sxv1ax4w9n48h72dnj18jip7xaq51avg0d50r1q66gvwphq871") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.0 (c (n "newt") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "= 0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "04ya57w96g5nkvd9prrcmbf4bd3rd23dshf018yj0df03jfr4p5a") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.1 (c (n "newt") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "= 0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1ns1h76y6i7zk9y40wjfzss2a5y3mjgn2l8pmy163nrs4pmcqvdg") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.2 (c (n "newt") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "= 0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "02v2fl9gp90dj4y2snvp6il6hwiqjwm1dxaphxmlxgj5amg8amq4") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.3 (c (n "newt") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "=0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1fg1whs0r8m6fsqaw2i2cbd1zqxz918w50d2q92q8lcrqcpfs7vp") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.4 (c (n "newt") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "=0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "08wlhlcnvzk0mg2f7kjww07n3fk7in57pvqw4b1k8brpd2kcmvsj") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.5 (c (n "newt") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.6") (d #t) (k 0)) (d (n "newt_proc_macros") (r "=0.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0d4yhkb4dn0lj14l26ipdcdhk8x4wp9cvm6ipwnx50x35ax9zfvj") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.7 (c (n "newt") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.8") (d #t) (k 0)) (d (n "newt_proc_macros") (r "=0.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1sr7z33a4aj93vsywyq11pb1bf3i4ip7x4jd38blsmzfj6ggswv5") (f (quote (("static" "newt-sys/static") ("asm"))))))

(define-public crate-newt-0.6.8 (c (n "newt") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "newt-sys") (r "~0.1.9") (d #t) (k 0)) (d (n "newt_proc_macros") (r "=0.0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "13ig51zb6cm9bgz050q4qsb6k7pc6sivsv4b1ad85wnjq1hhqrf6") (f (quote (("static" "newt-sys/static") ("asm"))))))

