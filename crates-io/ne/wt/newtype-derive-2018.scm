(define-module (crates-io ne wt newtype-derive-2018) #:use-module (crates-io))

(define-public crate-newtype-derive-2018-0.0.1 (c (n "newtype-derive-2018") (v "0.0.1") (d (list (d (n "generics") (r "^0.2.8") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 2)))) (h "0i1wjqdp60vihdikmr5x1yad6rdkvwg125h708vsl1lal14mg4p3")))

(define-public crate-newtype-derive-2018-0.0.2 (c (n "newtype-derive-2018") (v "0.0.2") (d (list (d (n "generics") (r "^0.3.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 2)))) (h "0sz352vkav7pprzb36i6g65n26fgzsdm3gjrka2z0h5qh0r7n0ym")))

(define-public crate-newtype-derive-2018-0.0.3 (c (n "newtype-derive-2018") (v "0.0.3") (d (list (d (n "generics") (r "^0.3.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 2)))) (h "13mbahsbhvkgjdw6r7amhnndhvq9qp359gcj0g02s7j404am7i98")))

(define-public crate-newtype-derive-2018-0.0.4 (c (n "newtype-derive-2018") (v "0.0.4") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)))) (h "1lk06ys556msyyq96jzw4jdx9my7fp9wmv8wv0gxi14d5swjkavs") (r "1.60")))

(define-public crate-newtype-derive-2018-0.0.5 (c (n "newtype-derive-2018") (v "0.0.5") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)))) (h "1fzyppwla9l425aj067k86khzfn8qcgnv4wim88z8h5rj7g27pkw") (r "1.60")))

(define-public crate-newtype-derive-2018-0.0.6 (c (n "newtype-derive-2018") (v "0.0.6") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)))) (h "0pw94sqkyqn25dk952a6r7vwx4bl031n96zhfrkfaj0qg002pw0a") (r "1.60")))

(define-public crate-newtype-derive-2018-0.0.7 (c (n "newtype-derive-2018") (v "0.0.7") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)))) (h "1blhf4w77701s5p6ag02d08h1fpxy3lj5a6imzbq80x8ngqbxqxa") (r "1.60")))

(define-public crate-newtype-derive-2018-0.1.0 (c (n "newtype-derive-2018") (v "0.1.0") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^2.0.1") (d #t) (k 2)))) (h "1hja7hxbzicpvyrk1qm4nmkqz7r85vfh75g4fx8280i8isyvjcfy") (r "1.60")))

(define-public crate-newtype-derive-2018-0.1.1 (c (n "newtype-derive-2018") (v "0.1.1") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "1knrlivbp10g5swksdjj99b2plqbxp60vjzpv4dvbkcrzwzgc9zy") (r "1.60")))

(define-public crate-newtype-derive-2018-0.2.0 (c (n "newtype-derive-2018") (v "0.2.0") (d (list (d (n "generics") (r "^0.4.1") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "1ninjjsn2vr0gnk6fiysqsa2fhvjp18icgcarvm4wqpjchkq3hkk") (r "1.71")))

(define-public crate-newtype-derive-2018-0.2.1 (c (n "newtype-derive-2018") (v "0.2.1") (d (list (d (n "generics") (r "^0.5.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "10wf1x5ffm8lrs46jspv5pmabb32541v334i1y3y2xdms8nicbkl") (r "1.71")))

(define-public crate-newtype-derive-2018-0.2.2 (c (n "newtype-derive-2018") (v "0.2.2") (d (list (d (n "generics") (r "^0.5.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "1v2nhmn5w14ar1kv8nql92b9dbf0vlvz6bkxxjihm3a61sxx8g6v") (r "1.71")))

(define-public crate-newtype-derive-2018-0.2.3 (c (n "newtype-derive-2018") (v "0.2.3") (d (list (d (n "generics") (r "^0.5.0") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "0qx15d3f54rqdpwxm7jnqakzixma9ilcr23lxjlk1fg2x9m8b8rd") (r "1.71")))

