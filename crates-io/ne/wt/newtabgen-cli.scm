(define-module (crates-io ne wt newtabgen-cli) #:use-module (crates-io))

(define-public crate-newtabgen-cli-0.2.1 (c (n "newtabgen-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "newtabgen") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "1dr736csgdvh1v78hpihzs39vxq1xsy1xn46q6adshnc9asaqvck")))

(define-public crate-newtabgen-cli-0.2.2 (c (n "newtabgen-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "newtabgen") (r "^0.4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "06ajbxbxdbxihfjbhala5x00nn6f24ll0hr5f34bvsc2a0n662hc")))

(define-public crate-newtabgen-cli-0.2.5 (c (n "newtabgen-cli") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "newtabgen") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0daavdf9ycgnxjc94s0b0h9a5mq9cp5gpk43f15j5j1snd4785r2")))

(define-public crate-newtabgen-cli-0.2.6 (c (n "newtabgen-cli") (v "0.2.6") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "newtabgen") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0r506p467aprdwgy4lm2z0bw4s2n210kz9jykvwfi21cqw6pmfyz")))

(define-public crate-newtabgen-cli-0.2.7 (c (n "newtabgen-cli") (v "0.2.7") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "newtabgen") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0h36lhh71hanl6a4xb54zl1v2f09m400b2fk10kw44kkjm5kpxbh")))

