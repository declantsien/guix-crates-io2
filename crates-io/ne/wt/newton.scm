(define-module (crates-io ne wt newton) #:use-module (crates-io))

(define-public crate-newton-0.1.0 (c (n "newton") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "10msqf2swijc1h7a443smzjqh53ybj91gxx1hsn1zj3zqdliabra")))

(define-public crate-newton-0.1.1 (c (n "newton") (v "0.1.1") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "0pwf6kfxpn3wfdr03l52rq1gb1ig78hfs9lwsi5xlvxki4pkyv91")))

(define-public crate-newton-0.1.2 (c (n "newton") (v "0.1.2") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "02qghr96lanwlw2g72gwphif1ghi1wgq83vr16skyl6piq0x9q0l")))

(define-public crate-newton-0.2.0 (c (n "newton") (v "0.2.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "1biz2hwbbrfh3kh9ifc6zq53n99lzdkrzw6p8b3ig1slk5c2c2bd")))

(define-public crate-newton-0.3.0 (c (n "newton") (v "0.3.0") (d (list (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)))) (h "0k1kglzccc4llxbabxsci6crxaw7ws4g997yjvf3ngpnfrdf96fb")))

(define-public crate-newton-0.3.1 (c (n "newton") (v "0.3.1") (d (list (d (n "serde") (r "^0.9.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wfiqdcb9wf1h9vpnz8z5fkg19kn794kgrpsas9kaq4hjmmipghk")))

(define-public crate-newton-0.3.2 (c (n "newton") (v "0.3.2") (d (list (d (n "serde") (r "^0.9.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qm1z5hrxn869qhshhl0i2ykq4ylgd4m88zg9kmn625l043hq9vy")))

(define-public crate-newton-0.4.0 (c (n "newton") (v "0.4.0") (d (list (d (n "serde") (r "^0.9.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hjkgfdam8k7fm32jxypnsn003jscfs7dlgpw60n0fhh28x6nmmw")))

(define-public crate-newton-0.5.0 (c (n "newton") (v "0.5.0") (d (list (d (n "serde") (r "^0.9.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cph0ycpf6mryxaxibh853x2c09bxgn6fb71mlrklsmh2gcapgmn")))

(define-public crate-newton-0.5.1 (c (n "newton") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pn13cayjfal4vbmqgfn48ayjfaycg7yaycjhci36gjjvb2zcr8m")))

