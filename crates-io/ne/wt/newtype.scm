(define-module (crates-io ne wt newtype) #:use-module (crates-io))

(define-public crate-newtype-0.1.0 (c (n "newtype") (v "0.1.0") (d (list (d (n "derive-newtype") (r "^0.1.0") (d #t) (k 0)))) (h "0xgjzbkjk233crhsks74qsj4fi8kb52msssf8lnckch3mi3m1nvj")))

(define-public crate-newtype-0.1.1 (c (n "newtype") (v "0.1.1") (d (list (d (n "derive-newtype") (r "^0.1.1") (d #t) (k 0)))) (h "0f0q57iri3iy3pqgy72knyzhvqn4wnixcwpf1ijnn23c3m98kir9")))

(define-public crate-newtype-0.1.2 (c (n "newtype") (v "0.1.2") (d (list (d (n "derive-newtype") (r "^0.1.1") (d #t) (k 0)))) (h "15da09gri0ljkkbflx78x2pjwak2hsh0vaz466qnhk9ckbzfm6gs")))

(define-public crate-newtype-0.2.0 (c (n "newtype") (v "0.2.0") (d (list (d (n "derive-newtype") (r "^0.1.1") (d #t) (k 0)))) (h "0hid6jra50cbp1nad7lphvw1v2x5chchzhr3l3wv4fcnkq84r5ib") (y #t)))

(define-public crate-newtype-0.2.1 (c (n "newtype") (v "0.2.1") (d (list (d (n "derive-newtype") (r "^0.2.0") (d #t) (k 0)))) (h "1hr4jlwna05anflhba2k74amvyg1ilf36zh4pvghfzncvjmzl6k3")))

