(define-module (crates-io ne xm nexmemory) #:use-module (crates-io))

(define-public crate-NEXMemory-0.1.0 (c (n "NEXMemory") (v "0.1.0") (h "1axzr705rj8dakac6ib955zsyglbn11dx1v5d7a52cpkp923i18g")))

(define-public crate-NEXMemory-0.1.1 (c (n "NEXMemory") (v "0.1.1") (h "1hk0pjyiwqbf2sgv9m0il00r1hblngl5l0i18ilg9s21dpygvwnf")))

(define-public crate-NEXMemory-0.1.2 (c (n "NEXMemory") (v "0.1.2") (h "0inih45yxzd5h5zcvjgygj1ysmd9883r5i0vaxh9ldnxz1ys3liy")))

