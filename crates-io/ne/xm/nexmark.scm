(define-module (crates-io ne xm nexmark) #:use-module (crates-io))

(define-public crate-nexmark-0.1.0 (c (n "nexmark") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0rzb8vnkb53v077zyplzvbhvrw0kgagmccap79cir1bs0vj9kpm3") (f (quote (("bin" "clap" "serde" "serde_json"))))))

(define-public crate-nexmark-0.2.0 (c (n "nexmark") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "14igwl9hiw6va867mpg7id6sm7m9k4bwaf320jkm8fcnfl8l1z25") (f (quote (("bin" "clap" "serde" "serde_json"))))))

