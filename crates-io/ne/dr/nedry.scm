(define-module (crates-io ne dr nedry) #:use-module (crates-io))

(define-public crate-nedry-0.1.0 (c (n "nedry") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lang-c") (r "^0.15.1") (d #t) (k 0)))) (h "1pha9a97dxq2mqmpva60l8bl7llg1rx66w6m62azc1qriyn1g32r")))

