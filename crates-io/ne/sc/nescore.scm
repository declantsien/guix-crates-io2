(define-module (crates-io ne sc nescore) #:use-module (crates-io))

(define-public crate-nescore-0.1.0 (c (n "nescore") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19n0b677snr5q11znfy5waa6vak7f3ywma8xf9bd8snmx12wzgs5") (f (quote (("events") ("default"))))))

(define-public crate-nescore-0.2.0 (c (n "nescore") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0rl4s7ylmj3np6nq08snlk1148jpir5n2xkyk4n1abh2ny0m0d1c") (f (quote (("events") ("default") ("bench-nescore"))))))

