(define-module (crates-io ne sc nescookie) #:use-module (crates-io))

(define-public crate-nescookie-0.1.0 (c (n "nescookie") (v "0.1.0") (d (list (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2.27") (d #t) (k 0)))) (h "0dm00pzmq9wwbk2nkk2jxbr59gy89dmbcbpp6l9mji14j35ar12q")))

(define-public crate-nescookie-0.2.0 (c (n "nescookie") (v "0.2.0") (d (list (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2.27") (d #t) (k 0)))) (h "0avr3qwxv1p59j1snpydf7kk17kxv0l5xfggbks377134xnrsk23")))

(define-public crate-nescookie-0.3.0 (c (n "nescookie") (v "0.3.0") (d (list (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.2.27") (d #t) (k 0)))) (h "0b9w3gfxv0kjrk1dylvc9sjjnm89nwgf3n88i7fsyfhkyqp2kzaf")))

