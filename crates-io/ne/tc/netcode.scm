(define-module (crates-io ne tc netcode) #:use-module (crates-io))

(define-public crate-netcode-0.1.0 (c (n "netcode") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "1mic1q52a03b0mgqp7zdg5blr01k5r5hc9blqig6dxfrb6wv20wf")))

(define-public crate-netcode-0.1.1 (c (n "netcode") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "146fd1javx0wkn5r15bc4pw394v3ljx90hv8k3mq23hxx80vhg2a")))

(define-public crate-netcode-0.1.2 (c (n "netcode") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "09a2gykzvzzyy3k87l4i0vf2vrjc292jnwl08rnms0ah8gksvynf")))

(define-public crate-netcode-0.1.3 (c (n "netcode") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "0zgr8h6wlgybzin9d276wh9hwrcp5zjjn9k9h0kn75gi7irzzc8s")))

(define-public crate-netcode-0.1.4 (c (n "netcode") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "0qalkpqk1jsbbvxrhfc0qfl8p27iibwpjdan535zzrfrsbhavwli")))

(define-public crate-netcode-0.1.5 (c (n "netcode") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "1778mhybaahbjph3m5r8ncs8sxwfvvvcyhj9y7538zrnyp88qy36")))

(define-public crate-netcode-0.2.1 (c (n "netcode") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)))) (h "040p392mp57fhrim8qhv8rjidpy68fsvq56gjd2lldb8wbr304d6")))

(define-public crate-netcode-0.2.2 (c (n "netcode") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libsodium-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0p8xq6fks40kv7b03ihp6wzl6j70ryhk49l4bjkkiin0cdj63sb2")))

(define-public crate-netcode-0.3.0 (c (n "netcode") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.6") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0b2qk0b302s8ylqjj52b06p27srz3ry0agqi06dl2mkngrx9jjkw")))

(define-public crate-netcode-0.3.1 (c (n "netcode") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.6") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0dd4x1hpbjai9gwsbjr4b0s8yfhp3m4rl787zcc2zdfwl7knjzl1")))

