(define-module (crates-io ne tc netctl2iwd) #:use-module (crates-io))

(define-public crate-netctl2iwd-0.1.0 (c (n "netctl2iwd") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)))) (h "0g56bbjxq49dy9cjwv6gx3lpcxfxxf281l3pbaizafg3b6qbq541") (y #t)))

(define-public crate-netctl2iwd-0.1.1 (c (n "netctl2iwd") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)))) (h "177b68798yg8nwb4s93sj8mkcsxvzghlsm2b3wbjdd1f9c3xlz0p")))

(define-public crate-netctl2iwd-0.1.2 (c (n "netctl2iwd") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)))) (h "0a7zdjw859bvz8hk2h4snry3vsyas49413x6xhd40af0z3gd4xvp")))

