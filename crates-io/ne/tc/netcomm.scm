(define-module (crates-io ne tc netcomm) #:use-module (crates-io))

(define-public crate-netcomm-0.1.0 (c (n "netcomm") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "info_utils") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ivj8wcpbxrappicyk28wz6bbxpgwi5q4jw572m5y9phl6hsphab")))

