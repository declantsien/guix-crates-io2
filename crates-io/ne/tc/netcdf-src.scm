(define-module (crates-io ne tc netcdf-src) #:use-module (crates-io))

(define-public crate-netcdf-src-0.1.0 (c (n "netcdf-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.7.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)))) (h "161a6h5schnn9fif6bdr5ig4d2l2dsricbpbcnldnmjyd8cjcmjv") (f (quote (("dap")))) (y #t) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.1.1 (c (n "netcdf-src") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.7.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)))) (h "0s0bx3pf2d31l6c92vazgzf88ax6m8kgj41nh5d72z8a2vv8rh00") (f (quote (("dap")))) (y #t) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.1.2 (c (n "netcdf-src") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.7.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)))) (h "1drm8y0is4694zn4m3q8xbvp2ra2c3wz6rxjwfkw1d7bbikc8cjm") (f (quote (("dap")))) (y #t) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.1.3 (c (n "netcdf-src") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.7.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)))) (h "0cfknwcj2hcdhzzv899pfzchy67hfr66k2n98xm7ma5dm5xprkzd") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.2.0 (c (n "netcdf-src") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)))) (h "1dqcmcaici9d65ni1hjk1n1d15x5f0qkjhaklfq41g0740zim5am") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.0 (c (n "netcdf-src") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)))) (h "1hwyyyp7nm5qm41aw3cs3bfww1irgbcw76dlfjnsvc6mflffrzcy") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.1 (c (n "netcdf-src") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)))) (h "1nfhbfvkzb87cpry0km46sb6rgymmxh073qqbffgz7d14h4q3sfc") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.2 (c (n "netcdf-src") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)))) (h "0m7ww49avx0ivqipi40yi4ifwpzzhfibw9zdfws1ic2s07v6shvp") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.3 (c (n "netcdf-src") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)))) (h "1zrvyvq217bdqjcqahirjjywk9jwqigkykhlcp513qk4wcvzahva") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.4 (c (n "netcdf-src") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)))) (h "0qjbyn2dj5m2rdrr61m06fcdjr0q475n0imiv42bjb3c4wdkckwy") (f (quote (("dap")))) (l "netcdfsrc")))

(define-public crate-netcdf-src-0.3.5 (c (n "netcdf-src") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "0l7klxakd410m7jznbqmr2i7rkzkx3608xz31nmx3pygfkvl76m5") (l "netcdfsrc") (s 2) (e (quote (("dap" "dep:link-cplusplus"))))))

(define-public crate-netcdf-src-0.3.6 (c (n "netcdf-src") (v "0.3.6") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "08xgvkfip9f1lfijahcvkscjlfywpymqa6a3ln37acqkdhfz10hk") (l "netcdfsrc") (s 2) (e (quote (("dap" "dep:link-cplusplus"))))))

(define-public crate-netcdf-src-0.3.7 (c (n "netcdf-src") (v "0.3.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "hdf5-sys") (r "^0.8.0") (f (quote ("hl" "deprecated" "zlib"))) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "15c7f193xvnmzv8fmxi6r75pk1l3qhfbac0spksxdnrw7b587bk8") (l "netcdfsrc") (s 2) (e (quote (("dap" "dep:link-cplusplus"))))))

