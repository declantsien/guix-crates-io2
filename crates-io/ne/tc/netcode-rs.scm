(define-module (crates-io ne tc netcode-rs) #:use-module (crates-io))

(define-public crate-netcode-rs-1.3.0 (c (n "netcode-rs") (v "1.3.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1fzmsawyia0ai10npxbk2yxzaj874dd2zrrkk6a3pfpilscki7y3")))

