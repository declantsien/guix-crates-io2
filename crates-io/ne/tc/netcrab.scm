(define-module (crates-io ne tc netcrab) #:use-module (crates-io))

(define-public crate-netcrab-1.0.0 (c (n "netcrab") (v "1.0.0") (d (list (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.13") (d #t) (k 0)))) (h "13qga98dhhmsg687g3kylwww3qnnb1xknqxbgq4r1063z506nnx2")))

