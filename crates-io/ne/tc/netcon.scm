(define-module (crates-io ne tc netcon) #:use-module (crates-io))

(define-public crate-netcon-0.1.0 (c (n "netcon") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1h37zpzx58hj60k0dvbnmjm8lav2mfrj85b7v9jlmv5yxm0px46p") (f (quote (("threadpool" "log"))))))

(define-public crate-netcon-0.1.1 (c (n "netcon") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0lqm4j213r6g1ysalbgd9ymgixkdvpf1fb2m11072wzrv5zh28ax") (f (quote (("threadpool" "log"))))))

(define-public crate-netcon-0.1.2 (c (n "netcon") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1dr3sbhpabj22mqrzagnksdjasfj8k9i5zab8asxv02hsmiygdci") (f (quote (("threadpool" "log"))))))

