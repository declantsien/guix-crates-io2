(define-module (crates-io ne tc netcatpf) #:use-module (crates-io))

(define-public crate-netcatpf-0.1.0 (c (n "netcatpf") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "libepf") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "net" "io-std"))) (d #t) (k 0)))) (h "0mixzbgbi3qdgyck5sdzyz3pin9qardl9yjywr4iiak2irq9x9db")))

