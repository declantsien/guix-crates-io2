(define-module (crates-io ne tc netcdf3) #:use-module (crates-io))

(define-public crate-netcdf3-0.1.0 (c (n "netcdf3") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1ya2xgn3pw4kk12larwn4b6j9adl3hfi7h7zl0liprjw4wv1f76g") (y #t)))

(define-public crate-netcdf3-0.2.0 (c (n "netcdf3") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "0na998g2dcjv3b21yh9fvsmjdc2710935056xqvlkmjbkr0g83lw")))

(define-public crate-netcdf3-0.3.0 (c (n "netcdf3") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1sdgrl3z3y67x1c4q59rlys6bgrdsl717c63kpih1g2nlc6zpra4")))

(define-public crate-netcdf3-0.3.1 (c (n "netcdf3") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "15rwmnz1585i7c8n0i27w2h95fgpiiq6jql95fjvwzwh86and30m")))

(define-public crate-netcdf3-0.4.0 (c (n "netcdf3") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "19l38sdwgw5054aa73lvqcnpavqhzlwzgkhzk5wvhb11rvcygl1q")))

(define-public crate-netcdf3-0.5.0 (c (n "netcdf3") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "091qa2cazsbcmw6zm7plj4m6h7cfw3l1w3a00zcss09vnb71c0yc")))

(define-public crate-netcdf3-0.5.1 (c (n "netcdf3") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0p3rmn9l6cjn8i9fx2gqdinh7msrcz6xahinb9v812n935svc6f6")))

(define-public crate-netcdf3-0.5.2 (c (n "netcdf3") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1as0iabfyw9zwjbgfm4nzrv28lf1g2lsp4clrlimgzafrghrga1k")))

