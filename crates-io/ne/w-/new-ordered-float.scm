(define-module (crates-io ne w- new-ordered-float) #:use-module (crates-io))

(define-public crate-new-ordered-float-1.0.0 (c (n "new-ordered-float") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "1nmjz0njwcnb5hs38ai52fmd6kdy5z1a024qfqg8r854wxl11cgr") (y #t)))

(define-public crate-new-ordered-float-1.0.1 (c (n "new-ordered-float") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "0sj76vzbld536z1lvxgz7gp6s4xw76rrrxzsvhmnsfmmdyxfpjwc") (y #t)))

(define-public crate-new-ordered-float-1.0.2 (c (n "new-ordered-float") (v "1.0.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "070ax7zpnmb7528gv02h4n90s88b9d4crpw46dgnnc4lqbyz4vn0") (y #t)))

