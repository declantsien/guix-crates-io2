(define-module (crates-io ne w- new-rawr) #:use-module (crates-io))

(define-public crate-new-rawr-0.0.1 (c (n "new-rawr") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07alhi6ha0z3181qw3cg1iaphqdl19ph0g53zg7paza711kvv7af") (y #t)))

