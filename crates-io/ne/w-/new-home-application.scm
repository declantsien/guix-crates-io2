(define-module (crates-io ne w- new-home-application) #:use-module (crates-io))

(define-public crate-new-home-application-0.1.0 (c (n "new-home-application") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mockall") (r "^0.6.0") (d #t) (k 2)) (d (n "polymap") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "08mk5y8nmzzcg9y2gz6f364bn4wg0rd6sg85y6xj4jnwv67l1qwl")))

(define-public crate-new-home-application-0.1.1 (c (n "new-home-application") (v "0.1.1") (d (list (d (n "mockall") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "14cp8nl8n6di26ah11i0q2v61s1mylral6xjwspnj95cv56pva25")))

(define-public crate-new-home-application-0.1.2 (c (n "new-home-application") (v "0.1.2") (d (list (d (n "mockall") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0vlh2ns4yzp23xvprigq3wpd7p79758b0iy2h4vb85dm43c9k6xz")))

(define-public crate-new-home-application-0.1.3 (c (n "new-home-application") (v "0.1.3") (d (list (d (n "mockall") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0nm82nv6afzd431znsqmrlkvjm9av276nvvmfm78j6bpwdas67s9")))

(define-public crate-new-home-application-1.0.0 (c (n "new-home-application") (v "1.0.0") (d (list (d (n "actix") (r "^0.10.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0djybqmipzg0q1ij9m99pw7j7ivhgrkilpr753w3fgw0k5k7iw9f")))

(define-public crate-new-home-application-1.0.1 (c (n "new-home-application") (v "1.0.1") (d (list (d (n "actix") (r "^0.10.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0al23sfnfyph3lksc6hshxivpjsp0sfh2dppanph3ssllcf04hiw")))

(define-public crate-new-home-application-1.0.2 (c (n "new-home-application") (v "1.0.2") (d (list (d (n "actix") (r "^0.10.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "144ax57rgxqgqh6dw7x51pxfafkhd1s6z0g6d79zr78q043b0k7y")))

(define-public crate-new-home-application-1.1.0 (c (n "new-home-application") (v "1.1.0") (d (list (d (n "actix") (r "^0.10.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0blyc9gxhl5l099w81zb08yh37gjji7fgnng03xxpvi2d2592vlq")))

