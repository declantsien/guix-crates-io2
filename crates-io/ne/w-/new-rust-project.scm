(define-module (crates-io ne w- new-rust-project) #:use-module (crates-io))

(define-public crate-new-rust-project-0.1.0 (c (n "new-rust-project") (v "0.1.0") (h "0lg5v7qjgr7jx7sdjsc4fha78jzjkpyadlfdhkjgpqhj5jc1zqr8")))

(define-public crate-new-rust-project-0.1.1 (c (n "new-rust-project") (v "0.1.1") (h "12a97v0p8q28iq49rr6y03krrfd801idwpjqn3dvlbmr55sgjb02")))

(define-public crate-new-rust-project-0.1.2 (c (n "new-rust-project") (v "0.1.2") (d (list (d (n "cargo-sync-readme") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0yxhbjlx6544pmi75gxrd1n8z4ykqcgbzpfv5lkhyv0aavi2xsil")))

(define-public crate-new-rust-project-0.1.3 (c (n "new-rust-project") (v "0.1.3") (d (list (d (n "cargo-sync-readme") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0jxj37wx9i15db91vf4varg6z0qyz0577csq4a548nizyklhn8v7")))

(define-public crate-new-rust-project-0.1.4 (c (n "new-rust-project") (v "0.1.4") (d (list (d (n "cargo-sync-readme") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1pmzpmbry97gmjbdb8939v20slgdphrx25y8wl28a5fb4v051vkz")))

