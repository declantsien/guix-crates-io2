(define-module (crates-io ne w- new-derive) #:use-module (crates-io))

(define-public crate-new-derive-0.1.0 (c (n "new-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1g9brmdh4xrx9dwfbvrlhqjr37jszycxlr59sb37izryqwkfmdpv")))

(define-public crate-new-derive-0.1.1 (c (n "new-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1lrhzjaiqfq1a4qyl86l2a0hmqmjjajclgvc8avz2qj29g2ghndf")))

(define-public crate-new-derive-0.1.2 (c (n "new-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0vi9why3bg00j36mrzlrijidqizpw0cgsqpiw0q5zs9j6pvm5lq6")))

(define-public crate-new-derive-0.1.3 (c (n "new-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0zc6jzi4xs54f0smj969bs315z69zjbyikz05sf4s3xmfmi16c0n")))

(define-public crate-new-derive-0.1.4 (c (n "new-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "09lnj825r4piaacnn1w7j1s5wbk1nl3pla773xd0pjg73nbwcfrv")))

(define-public crate-new-derive-0.2.0 (c (n "new-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0b4jvqjyadnkjw5hf42vn4gw8wkzzv0x7b21sm37bamf4nrzfq5s")))

(define-public crate-new-derive-0.2.1 (c (n "new-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "16gsb03sv1x9f6jsgfnbqgwssiypgbzgb3zrsxq9zz25riqg6wsn")))

