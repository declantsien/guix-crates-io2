(define-module (crates-io ne w- new-home-ws281x) #:use-module (crates-io))

(define-public crate-new-home-ws281x-1.0.0 (c (n "new-home-ws281x") (v "1.0.0") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rs_ws281x") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1arb50kl1qxn4d4i4c5amf82b921pdvbw347ffhn3i4wknhs9m28")))

(define-public crate-new-home-ws281x-1.0.1 (c (n "new-home-ws281x") (v "1.0.1") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rs_ws281x") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1rmr6kmx0rchx9gvh4mdmwnzqi9h8chingvnfqvzvffvarg5m9ff")))

(define-public crate-new-home-ws281x-1.0.2 (c (n "new-home-ws281x") (v "1.0.2") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rs_ws281x") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0my8c8h9z74cg3h8paqzhcfibhi8pzf8wv5ban806vl5fwrjs8bb")))

(define-public crate-new-home-ws281x-1.0.3 (c (n "new-home-ws281x") (v "1.0.3") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rs_ws281x") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1208lvyzybvzihg35scq68g76mjwf6s650yvgj8sjl0f0l784pcm")))

(define-public crate-new-home-ws281x-1.0.4 (c (n "new-home-ws281x") (v "1.0.4") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rs_ws281x") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0z1sn79n42a38n65sfn3bqcshfc4yrynrl974kb9hm3i5j2qcccx")))

