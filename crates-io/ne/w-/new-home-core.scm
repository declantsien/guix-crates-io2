(define-module (crates-io ne w- new-home-core) #:use-module (crates-io))

(define-public crate-new-home-core-0.1.0 (c (n "new-home-core") (v "0.1.0") (d (list (d (n "rouille") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10cag637yam78f7vprv27kxagnw483qbxxzrsn4wsmqbjv1iym4r")))

(define-public crate-new-home-core-0.1.1 (c (n "new-home-core") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "rouille") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1m6rggvkxs65slsb28gd424832g645c85gpbx03zy58nlp7pyib2")))

(define-public crate-new-home-core-0.1.2 (c (n "new-home-core") (v "0.1.2") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "rouille") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1fhizn1946fjbv230adr3yby5j2hnihih4j617k2a3vbnv622zw2")))

(define-public crate-new-home-core-0.1.3 (c (n "new-home-core") (v "0.1.3") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "rouille") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1my7b1wv7qnhlym3k8ap8kq326lgipavxah3kfrxy97ys9mhyhl8")))

