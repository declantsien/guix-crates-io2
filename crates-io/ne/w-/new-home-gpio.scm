(define-module (crates-io ne w- new-home-gpio) #:use-module (crates-io))

(define-public crate-new-home-gpio-1.0.0 (c (n "new-home-gpio") (v "1.0.0") (d (list (d (n "new-home-application") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "05am2ji01bz22dq2mhk2aiz34d8pkqmqi2z5wln8ahrgc5vs4aba")))

(define-public crate-new-home-gpio-1.0.1 (c (n "new-home-gpio") (v "1.0.1") (d (list (d (n "new-home-application") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1v5w2kck7cwp9w1bz8yk2fi261dmbjinpajc7n38gvzb5ibi6krh")))

