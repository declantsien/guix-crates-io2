(define-module (crates-io ne tp netplan-types) #:use-module (crates-io))

(define-public crate-netplan-types-0.1.0 (c (n "netplan-types") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yfwwhzz4vkd3a742qvywpcd7v0nmw87h48ng0k2dws8l0fk2b7p")))

(define-public crate-netplan-types-0.2.0 (c (n "netplan-types") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05pi0fibr96r4vrx7sn4ln7vbarvib8vz8914b3pjx2yvvyv2bgz")))

(define-public crate-netplan-types-0.2.1 (c (n "netplan-types") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pxh4jic65hkfcbwp03lknzmyzfn2y1kjp6a94cns482c524l19y") (f (quote (("repr-c") ("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.3.0 (c (n "netplan-types") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b6lssd4nnx018446dnpy0k7g8xi415nwnxq12agh89xw1xv2jl2") (f (quote (("repr-c") ("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.3.1 (c (n "netplan-types") (v "0.3.1") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rgn4xwxjc91mx6sgk27022nvj2rsydbigc3iw9rh7v78chyvz7i") (f (quote (("repr-c") ("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.3.2 (c (n "netplan-types") (v "0.3.2") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06w59dkg3v2bpwhm2cy9457c0pyk5r9ps4fps0q482k4yydq88ry") (f (quote (("repr-c") ("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.3.3 (c (n "netplan-types") (v "0.3.3") (d (list (d (n "derive_builder") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0988i531ldn3dgls09gkx22mqdbzmcrci4k1kyyr9rhybfqj3f1w") (f (quote (("repr-c") ("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.3.4 (c (n "netplan-types") (v "0.3.4") (d (list (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0nzjw9lsrbkk62qyzhqz8aygfsxc0ixhj5wc9syb3kfsxjimwv47") (f (quote (("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.4.0 (c (n "netplan-types") (v "0.4.0") (d (list (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0f55lakrn32s6d84h1ngh82ji5ixc6lvsx9q0rgw8hl5h5xr2yzm") (f (quote (("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.4.1 (c (n "netplan-types") (v "0.4.1") (d (list (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "12i7yrq4bf9d0dy1fq448bfjylx4blmh3qc0ms0s82l6v5d2gkgf") (f (quote (("default" "serde") ("builder" "derive_builder"))))))

(define-public crate-netplan-types-0.5.0 (c (n "netplan-types") (v "0.5.0") (d (list (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "1fgr3ml76grycay83f7g62gr4xgqj684rxn7br6nawdishgal5bw") (f (quote (("default" "serde") ("builder" "derive_builder"))))))

