(define-module (crates-io ne tp netproto) #:use-module (crates-io))

(define-public crate-netproto-0.1.0 (c (n "netproto") (v "0.1.0") (h "1w4j5fq30qydf5g9m0yibm7livld59ga9zbf6m8wppck4xx8z0cm")))

(define-public crate-netproto-0.1.1 (c (n "netproto") (v "0.1.1") (h "049f70myrc6ghxbysjblpm802ihl05cv1i96lvj1qk3ycr5gfgnw")))

