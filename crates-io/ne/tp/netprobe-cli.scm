(define-module (crates-io ne tp netprobe-cli) #:use-module (crates-io))

(define-public crate-netprobe-cli-0.0.1 (c (n "netprobe-cli") (v "0.0.1") (h "1w6r3nsqcmigi978m7103dxf0s6bv3mkgbg6r9fq5gfnwl337v4f") (y #t)))

(define-public crate-netprobe-cli-0.5.0 (c (n "netprobe-cli") (v "0.5.0") (d (list (d (n "async-io") (r "^1.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "domainscan") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)) (d (n "netscan") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "privilege") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tracert") (r "^0.5") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0g3hz8df2prijidvbrfhl6z37igf2pxrz59wl9lxpgpy9d0qpjp7") (y #t)))

(define-public crate-netprobe-cli-0.6.0 (c (n "netprobe-cli") (v "0.6.0") (h "09yqqwg7rc0ngq9ql89kwqwa1khfn7rgh3hi5qs7dbv34fncfy0b") (y #t)))

