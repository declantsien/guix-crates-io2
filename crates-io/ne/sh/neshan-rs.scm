(define-module (crates-io ne sh neshan-rs) #:use-module (crates-io))

(define-public crate-neshan-rs-0.1.0 (c (n "neshan-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0rankn2cw8bh07mii6hmr2vqr804r1qv4hxn7kk1bs042p92ylb5")))

(define-public crate-neshan-rs-0.2.0 (c (n "neshan-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "15i13mwd4iy0x7dabi13zf2aj2p98i16dy9vzsgswfnbx68w3hdx")))

