(define-module (crates-io ne t_ net_promoter_score) #:use-module (crates-io))

(define-public crate-net_promoter_score-0.1.0 (c (n "net_promoter_score") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "00y7m6kzzbqijz171yzljb7j4vzglafj5dhh25n450zh78r84gic")))

(define-public crate-net_promoter_score-0.1.1 (c (n "net_promoter_score") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "1w9wd64gr17ghjm7d0ynj21cr3xylq0pma7id7gljwh1zchkimb5")))

(define-public crate-net_promoter_score-0.2.0 (c (n "net_promoter_score") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "1vsv92m7q7jssiwyad8q9v1khggzc1qs278gddi8v3hp5lzxrf8g")))

(define-public crate-net_promoter_score-0.2.1 (c (n "net_promoter_score") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "12fqnf2f2w4dklalrh9h7w11n0xifggdvkf7vwilv5h7gq233nhv")))

