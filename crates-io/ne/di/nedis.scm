(define-module (crates-io ne di nedis) #:use-module (crates-io))

(define-public crate-nedis-0.1.0 (c (n "nedis") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "135bzc0n5gwji7rnx4wzwj75lbjdr3793zasq2zjr3mg9l1dw1p6")))

