(define-module (crates-io ne tn netns-utils) #:use-module (crates-io))

(define-public crate-netns-utils-0.1.0 (c (n "netns-utils") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("user" "hostname" "mount" "signal" "sched" "fs"))) (d #t) (k 0)))) (h "1i74s25dhwlpn06zs2jban9rhw3ha6bfhb69bm86d3acq4b1ns2l")))

