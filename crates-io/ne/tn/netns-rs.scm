(define-module (crates-io ne tn netns-rs) #:use-module (crates-io))

(define-public crate-netns-rs-0.1.0 (c (n "netns-rs") (v "0.1.0") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vi7slzq4qvraip7m3japcfb0j7alr916fnsl38qrlfpy6a1cm13")))

