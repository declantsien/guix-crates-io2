(define-module (crates-io ne tn netns_tcp_bridge) #:use-module (crates-io))

(define-public crate-netns_tcp_bridge-0.1.0 (c (n "netns_tcp_bridge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "either") (r "^1.7.0") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (f (quote ("async"))) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("sched" "socket" "uio" "fs" "net"))) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("net" "rt" "io-util"))) (d #t) (k 0)))) (h "0is32l2hw4dx07w942k7kf7ris7nq36cwrhzaacn5sr9r26jsrr5")))

