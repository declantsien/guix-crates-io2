(define-module (crates-io ne tn netnyan) #:use-module (crates-io))

(define-public crate-netnyan-0.1.0 (c (n "netnyan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full" "signal"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1cpzv8d92whlmzp2s8m6pp12qs3zjx34wdd0rkvhs8yacmrc3n7b")))

