(define-module (crates-io ne tn netneighbours) #:use-module (crates-io))

(define-public crate-netneighbours-0.1.0 (c (n "netneighbours") (v "0.1.0") (d (list (d (n "macaddr") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("netioapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1wp2chif7irdahqjyc75js0z4cqlw64dp7lb8frybdhb4kvgvs05")))

(define-public crate-netneighbours-0.1.1 (c (n "netneighbours") (v "0.1.1") (d (list (d (n "macaddr") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("netioapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1r68g8fsh7g5q216h1ifwrs2mswf13snmm356cxgmsm348dq1hzc")))

(define-public crate-netneighbours-0.1.2 (c (n "netneighbours") (v "0.1.2") (d (list (d (n "macaddr") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("netioapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "13rg62v908jjnr9r7a1ckd1j472h7cvsa0zfpyvh7l1xamzk9il5")))

