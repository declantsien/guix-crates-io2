(define-module (crates-io ne tn netns-exec) #:use-module (crates-io))

(define-public crate-netns-exec-0.1.0 (c (n "netns-exec") (v "0.1.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "08h63zkxiqvrm4npigs6246rnnvif2xnad6b8ff9qxkvl4hys06w")))

(define-public crate-netns-exec-0.1.1 (c (n "netns-exec") (v "0.1.1") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "0727nmvln6y5sfds6gr1g1agfyq4r4jgy5ini61kcamxv3zp5dwd")))

(define-public crate-netns-exec-0.1.2 (c (n "netns-exec") (v "0.1.2") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1dli0pq371v0i5cwnm8gacx3py2ra827bjc93l5mykpkyjs064km")))

(define-public crate-netns-exec-0.2.0 (c (n "netns-exec") (v "0.2.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "08qmkf6q54qzdl0pk5jjj5y3fk92s3bn6wgq3i7v0w48s81w7bv9")))

(define-public crate-netns-exec-0.2.1 (c (n "netns-exec") (v "0.2.1") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "14yb3yxla41a9bkw92pnhdxn23pg8p4r79fzvjanp46ccnzm421s")))

(define-public crate-netns-exec-0.2.2 (c (n "netns-exec") (v "0.2.2") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1rb8zw00jlg7z9zy5ngg1la0b7nvmwi5czny5im1z9rqbz1apdr8")))

