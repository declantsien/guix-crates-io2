(define-module (crates-io ne tl netloc-discord) #:use-module (crates-io))

(define-public crate-netloc-discord-0.1.0 (c (n "netloc-discord") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "netloc-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "webhook") (r "^1") (d #t) (k 0)))) (h "001fv7m00mk4qa41fjxv7aybmilb3xzk5qqiblidqjw4b6z6gj19")))

(define-public crate-netloc-discord-0.2.0 (c (n "netloc-discord") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "netloc-core") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "webhook") (r "^1") (d #t) (k 0)))) (h "0zpwfjcxvqwqzyf1vfg0c8fpy9b82qac5zg3p94fdcf1ipl2d8sl")))

(define-public crate-netloc-discord-0.2.1 (c (n "netloc-discord") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "netloc-core") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1ljyv7rc8ksfsknyf812rnrmzv07wdd3y3gdln1qyjx0jg8b7v0i")))

(define-public crate-netloc-discord-0.2.2 (c (n "netloc-discord") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "netloc-core") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1crj8ys3i3i0i3qbaqbgszv8igkg70cqqgxfay2svdi40b1lvhza")))

