(define-module (crates-io ne tl netlify_headers) #:use-module (crates-io))

(define-public crate-netlify_headers-0.1.0 (c (n "netlify_headers") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q90gl3gdgplm11455cmblhrm4rq6rd3x885x3sf8r9x8y9pxz18")))

(define-public crate-netlify_headers-0.1.1 (c (n "netlify_headers") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xsq1w5qj7jj2isi1fyn63z44shhrdyfqpvai502z5g4fmn65hc2")))

