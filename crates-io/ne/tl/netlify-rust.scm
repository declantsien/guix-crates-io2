(define-module (crates-io ne tl netlify-rust) #:use-module (crates-io))

(define-public crate-netlify-rust-0.1.0 (c (n "netlify-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "03wimzz1ygrhs2vpzfij5x0n3iypqrp683aia9qc6jdjm8dbkf5f")))

(define-public crate-netlify-rust-0.1.1 (c (n "netlify-rust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0q9bwff37gp8wk4g2b7v7y0lib4p0h5ai3z5h6fmffy6izkgka2r")))

