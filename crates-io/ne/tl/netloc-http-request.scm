(define-module (crates-io ne tl netloc-http-request) #:use-module (crates-io))

(define-public crate-netloc-http-request-0.1.0 (c (n "netloc-http-request") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "netloc-core") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0hlmlbr527x2rn1livrmkqk4r3zmga5v9gdk758aknmwkhyhdlr7")))

