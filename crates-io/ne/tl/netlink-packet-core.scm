(define-module (crates-io ne tl netlink-packet-core) #:use-module (crates-io))

(define-public crate-netlink-packet-core-0.1.0 (c (n "netlink-packet-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.1") (d #t) (k 0)))) (h "0myr7qp8f9r82rzqcry4rrj73l1rpb6y6vc8b3xvy06vl2dfknlw")))

(define-public crate-netlink-packet-core-0.2.0 (c (n "netlink-packet-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.2") (d #t) (k 0)))) (h "04w11qf4ijs36fzc4dfk9m3gml39bw3yfc220px3h943whksx82z")))

(define-public crate-netlink-packet-core-0.2.1 (c (n "netlink-packet-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.3") (d #t) (k 0)))) (h "0jf7fpxgn3vbk8rsgn6x9zmc2c9x05qxsbg6v8pmqgwyz7wafpi5")))

(define-public crate-netlink-packet-core-0.2.2 (c (n "netlink-packet-core") (v "0.2.2") (d (list (d (n "anyhow") (r ">=1.0.31, <2.0.0") (d #t) (k 0)) (d (n "byteorder") (r ">=1.3.2, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.66, <0.3.0") (d #t) (k 0)) (d (n "netlink-packet-utils") (r ">=0.3.0") (d #t) (k 0)))) (h "0kjnpsil1pvvxk23f3sc39b10lmi7n73hxcgi9wcx6j7xarhi72m")))

(define-public crate-netlink-packet-core-0.2.4 (c (n "netlink-packet-core") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-utils") (r ">=0.3, <0.5") (d #t) (k 0)))) (h "1jrwj3z501dwyh7pl7wm9gayrc6ny67vbdmwpmsz3gb2a2fjfj5c")))

(define-public crate-netlink-packet-core-0.3.0 (c (n "netlink-packet-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.8") (d #t) (k 2)) (d (n "netlink-packet-utils") (r ">=0.3, <0.5") (d #t) (k 0)))) (h "0cpfyambnc1lgdblksjx0jfj4zf7qkdn7x6841mzk8vm9ivwp2q7")))

(define-public crate-netlink-packet-core-0.4.0 (c (n "netlink-packet-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.8") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)))) (h "08lk8spj4kjxk04wznl35jbrkvr6jd1d2frvsk2shvxig7a569sj")))

(define-public crate-netlink-packet-core-0.4.1 (c (n "netlink-packet-core") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.10") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)))) (h "09z7phz33h5s5950ndhc1klr6yfanc3av1w5l6wbrnpmjn714jc3")))

(define-public crate-netlink-packet-core-0.4.2 (c (n "netlink-packet-core") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.10") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)))) (h "15qj97azqrkrkmp2w5cfwxhd16b6hmb8rs33csca4wafpnsqlnrl")))

(define-public crate-netlink-packet-core-0.5.0 (c (n "netlink-packet-core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.13.0") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5.2") (d #t) (k 0)))) (h "0njkzy91g5039y3icl8gdsd4q39ds47zyh2wc68lpnpz9ssz0p3y")))

(define-public crate-netlink-packet-core-0.6.0 (c (n "netlink-packet-core") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.13.0") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5.2") (d #t) (k 0)))) (h "1d1dv58cjybrcs7fbr9i6bfpwdz2av25mgdl0gavyzrb5bp5w956")))

(define-public crate-netlink-packet-core-0.7.0 (c (n "netlink-packet-core") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "netlink-packet-route") (r "^0.13.0") (d #t) (k 2)) (d (n "netlink-packet-utils") (r "^0.5.2") (d #t) (k 0)))) (h "197dh9c5570135kv5q770n2ih5prhsql58cd71xxcya4f2plywkj")))

