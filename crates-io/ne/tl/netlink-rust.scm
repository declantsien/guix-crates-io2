(define-module (crates-io ne tl netlink-rust) #:use-module (crates-io))

(define-public crate-netlink-rust-0.1.0 (c (n "netlink-rust") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)))) (h "0w8l2fbwwx4ic0i6cml5imkarkzhl010jqxgmz10an3xgzl763wy")))

