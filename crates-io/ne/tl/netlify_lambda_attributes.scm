(define-module (crates-io ne tl netlify_lambda_attributes) #:use-module (crates-io))

(define-public crate-netlify_lambda_attributes-0.1.0 (c (n "netlify_lambda_attributes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0p03k631hr147y3fv72rpknwsiy88hlbf62lals1qvpmmw835v8i")))

(define-public crate-netlify_lambda_attributes-0.1.1 (c (n "netlify_lambda_attributes") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0c6zpkjlfcqhbmh6zi1w24ndsinf2sb1746s1hdqfrjyixpc13qv")))

(define-public crate-netlify_lambda_attributes-0.2.0 (c (n "netlify_lambda_attributes") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1qzmak24v5r44f6vji0q6f72qy407l477yj6srrhcky3k2znh924")))

