(define-module (crates-io ne tl netlib-src) #:use-module (crates-io))

(define-public crate-netlib-src-0.5.0 (c (n "netlib-src") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mq1qp99imyhim1blxv0p41zlbgycs2n25aknwg015jvv2igjh1h") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.5.1 (c (n "netlib-src") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1rdj36aw8sl71hvpzvl37lc49qn2k3ys3blsnfn1yqa6yx90gvbn") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.6.0 (c (n "netlib-src") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16v6albyjw76lrh6hr56r3aiw0jh6rl0sllv402hbj4zjc0jrpq2") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.7.0 (c (n "netlib-src") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0phj27a4mprckrmnhyzy8pkc8vngphqp6nzcgjxvvh5576cgvh13") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.7.1 (c (n "netlib-src") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0hjzsbvp0bjpyxvvcn4mw4yh4da8knqg3sxf2ys97wfp6iwh7xkv") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.7.2 (c (n "netlib-src") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0vfc68qd1r0nr5k9lm5pbxka7yrzhczd2cgs2lzdfwlnq81qkb1m") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-src-0.7.3 (c (n "netlib-src") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "103jsk0fb3jg351c3psivgjxr222qw6bnzwwf3g9x2jlphx56lbz") (f (quote (("tmg") ("system") ("static") ("lapacke") ("default" "cblas" "lapacke" "tmg") ("cblas"))))))

(define-public crate-netlib-src-0.7.4 (c (n "netlib-src") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "112hwfw1zzdj10h3j213xxqjrq38iygb3nb3ijay65ycmrg819s4") (f (quote (("tmg") ("system") ("static") ("lapacke") ("default" "cblas" "lapacke" "tmg") ("cblas"))))))

(define-public crate-netlib-src-0.8.0 (c (n "netlib-src") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "04l2ggdaq0bjc64prsw2f8ddxn84m1rmpnkjb9nr0ijdpcv1zx1r") (f (quote (("tmg") ("system") ("static") ("lapacke") ("default" "cblas" "lapacke" "tmg") ("cblas")))) (l "blas")))

