(define-module (crates-io ne tl netlink-packet-generic) #:use-module (crates-io))

(define-public crate-netlink-packet-generic-0.1.0 (c (n "netlink-packet-generic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.2") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.4") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.7") (d #t) (k 2)))) (h "1bwah13jng3slg7i3j6l313fbhv0lkjm82lwnbcjfj6h1cvqr66r")))

(define-public crate-netlink-packet-generic-0.3.0 (c (n "netlink-packet-generic") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.4") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8") (d #t) (k 2)))) (h "0fx2awcghl5jwmzx9rsfhl0a3q0d5z9k3vc5psn8ipgkzsxzyy46")))

(define-public crate-netlink-packet-generic-0.2.0 (c (n "netlink-packet-generic") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.4") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8") (d #t) (k 2)))) (h "099zyfv7i5jac4yn2zqz1ppyl38c281fdsc4gf9jgjwqb3najqdv")))

(define-public crate-netlink-packet-generic-0.3.1 (c (n "netlink-packet-generic") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.4.2") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5.1") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8.3") (d #t) (k 2)))) (h "006b23qbfkcgx8x1il5xk3gx2170rfwpak4cwk6h70rfgbnxb9cl")))

(define-public crate-netlink-packet-generic-0.3.2 (c (n "netlink-packet-generic") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.5.0") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5.2") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8.3") (d #t) (k 2)))) (h "0zrnm4lyj7m3gkqaa27673qlr9zjwr710yb50zscbqjfb6rjyavc")))

(define-public crate-netlink-packet-generic-0.3.3 (c (n "netlink-packet-generic") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.7.0") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5.2") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8.3") (d #t) (k 2)))) (h "12rhb95ayx63zb55mz8z5vc5lghk92s8bxmpiimlrj1isf5fpmqw")))

