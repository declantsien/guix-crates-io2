(define-module (crates-io ne tl netlink-ip) #:use-module (crates-io))

(define-public crate-netlink-ip-0.0.1 (c (n "netlink-ip") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "eui48") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.0.1") (f (quote ("rtnl_support" "tokio_support"))) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)))) (h "146v2mx91hz0s436q6gfgxl27cz8cwb66vh5fibx3ng56670ddra") (y #t)))

