(define-module (crates-io ne tl netlist) #:use-module (crates-io))

(define-public crate-netlist-0.1.5 (c (n "netlist") (v "0.1.5") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "198qsxwzcxmvynb4b4z8hi6m884qqiqhfrzhl51mybygxvd0bnim")))

(define-public crate-netlist-0.1.6 (c (n "netlist") (v "0.1.6") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0788m4rk6r44nz8rag63qrfcrn60kgwb57n5zcjlvvs48b3a88gp")))

(define-public crate-netlist-0.1.7 (c (n "netlist") (v "0.1.7") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08zk8i0chnvdmy8f19qbzagvrzpy54sz2j78sirnzw3a9n3zxgca")))

(define-public crate-netlist-0.1.8 (c (n "netlist") (v "0.1.8") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i3hwx0s5h3jlikmc84ab9cr1bs2x0p7wcb8fgpzz96v0rz0ai7l")))

(define-public crate-netlist-0.1.9 (c (n "netlist") (v "0.1.9") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14imdr6fpz3wy98cx1jhmq3bb6xnvnilxi7dpp7lzgrwqhrsbbz5") (y #t)))

(define-public crate-netlist-0.1.10 (c (n "netlist") (v "0.1.10") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pvxs4wnz00jl989ccrh7mcqxnjrglgpwaybyzphvqq8nc9d0acr")))

(define-public crate-netlist-0.1.11 (c (n "netlist") (v "0.1.11") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f0wh2cw0vxhq0ab55f5b35k41zvvfplqmsiy9alsy0pbns8srhk")))

(define-public crate-netlist-0.1.12 (c (n "netlist") (v "0.1.12") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1llc1817cijlxsla1xazxwc85gmy3xxw894vigf6wqpvybffq14r")))

(define-public crate-netlist-0.1.13 (c (n "netlist") (v "0.1.13") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zp26hkczgb6jb5hzj8xi0h9qgdipxzq773lhm4wr8wf4fvrs249")))

(define-public crate-netlist-0.1.14 (c (n "netlist") (v "0.1.14") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nc828n8figdcpi2na8jjiqdv4vvx0560xrp9ncgm45iw8srzpq4")))

(define-public crate-netlist-0.1.15 (c (n "netlist") (v "0.1.15") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0si27j8z36qyzymxljn3wc3gn9vnbrygs26gznms6yi303m9cnhr")))

