(define-module (crates-io ne tl netlink-packet-utils) #:use-module (crates-io))

(define-public crate-netlink-packet-utils-0.1.0 (c (n "netlink-packet-utils") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1zysdwwvph4x06l4y4kgvfarlsb5kh292li54nlhkk777wzxw9na")))

(define-public crate-netlink-packet-utils-0.1.1 (c (n "netlink-packet-utils") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1p2h44kmks8y7j71zlb2s8jldj3sd2frmsnajb1xgai060npk1b7")))

(define-public crate-netlink-packet-utils-0.2.0 (c (n "netlink-packet-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rkamdmqbdy1m7643rsz1a72m82vb8bfcs2gvg9ri4b8lvx2iric")))

(define-public crate-netlink-packet-utils-0.3.0 (c (n "netlink-packet-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09jna1fcxq35rd0jrp89sdqk97wrva2wbdh4db6ggj948i03jq62")))

(define-public crate-netlink-packet-utils-0.4.0 (c (n "netlink-packet-utils") (v "0.4.0") (d (list (d (n "anyhow") (r ">=1.0.31, <2.0.0") (d #t) (k 0)) (d (n "byteorder") (r ">=1.3.2, <2.0.0") (d #t) (k 0)) (d (n "paste") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0kg5fdb1ppzw8996h2qcxm1rvf9qhidz43azx00cffhfklaznakc")))

(define-public crate-netlink-packet-utils-0.4.1 (c (n "netlink-packet-utils") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1137gb12679aiqz00cjvmmmdj6420ya6sn9r4d5rcvmnb3vvdksz")))

(define-public crate-netlink-packet-utils-0.5.0 (c (n "netlink-packet-utils") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07a4y6a3p4ccdq9ygl0lg8ag29ysbgqkfzwwfc3an37bxib8l00a")))

(define-public crate-netlink-packet-utils-0.5.1 (c (n "netlink-packet-utils") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pl4hhyf0wwj2v2p6nkimfh6lw3qgaphh5aav5xqnjamvkq9rbr5")))

(define-public crate-netlink-packet-utils-0.5.2 (c (n "netlink-packet-utils") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d3xfh9zg0x2hbmh8iws0dvhj69prpx54khfvmfakm8sqw48mphf")))

