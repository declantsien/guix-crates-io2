(define-module (crates-io ne tl netlink-socket) #:use-module (crates-io))

(define-public crate-netlink-socket-0.0.1 (c (n "netlink-socket") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.22") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.15") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "15whb4brirdgwsm2yvyfkjq1h653ag5xy77hq5i5pnpvb4j3mzkz") (f (quote (("tokio_support" "mio_support" "tokio-reactor" "futures") ("mio_support" "mio") ("default")))) (y #t)))

(define-public crate-netlink-socket-0.0.2 (c (n "netlink-socket") (v "0.0.2") (d (list (d (n "futures") (r "^0.1.22") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.15") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1l3v40qr83pv5bjwd17q3ihsam94pj3bc6nj166yjdd1hy8rrk0a") (f (quote (("tokio_support" "mio_support" "tokio-reactor" "futures") ("mio_support" "mio") ("default")))) (y #t)))

