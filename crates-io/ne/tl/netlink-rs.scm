(define-module (crates-io ne tl netlink-rs) #:use-module (crates-io))

(define-public crate-netlink-rs-0.0.1 (c (n "netlink-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q30xzr59qac3gd6kh3w4dpqxzai3d1vx5a5d7sq4mpvz6fgl97z") (f (quote (("default"))))))

(define-public crate-netlink-rs-0.0.2 (c (n "netlink-rs") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cri6k9nxc3bah10ljxmygnkbxi77jvri01fga5gpfh7fyx1idmm") (f (quote (("default"))))))

(define-public crate-netlink-rs-0.0.3 (c (n "netlink-rs") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0spkxwgha1gqj1wkvh0g7hj1mwkyvjmfylpc7fp0av1kw0hfwwam") (f (quote (("default"))))))

