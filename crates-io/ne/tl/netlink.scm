(define-module (crates-io ne tl netlink) #:use-module (crates-io))

(define-public crate-netlink-0.1.0 (c (n "netlink") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0im2fm91kzzf3fz16csb74886isp69dnb51rqd9zqpxv6vwm79q3")))

(define-public crate-netlink-0.1.1 (c (n "netlink") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "1v46iisjc7r912xd32ivp5acmlarjdgbvfa18cg8rsrbmgd0ih79")))

