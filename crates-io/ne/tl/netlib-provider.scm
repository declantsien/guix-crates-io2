(define-module (crates-io ne tl netlib-provider) #:use-module (crates-io))

(define-public crate-netlib-provider-0.0.8 (c (n "netlib-provider") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1mppi0iv2d9rkfxayk7n3h8q8w7ck7s2yl7sramxc3s8d8bfx3x4") (f (quote (("system-netlib") ("blas-only"))))))

(define-public crate-netlib-provider-0.1.0 (c (n "netlib-provider") (v "0.1.0") (h "1naxr86s7akhvfl2cshyf4sy80k5qxay2857smplk8dc6064znmd") (f (quote (("system-netlib") ("exclude-lapack") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.1.1 (c (n "netlib-provider") (v "0.1.1") (h "0d5i8x1d1zck5zrlfk36xk6hmxmkd4pjhq0387y9643mqdg09bdv") (f (quote (("system-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.1.2 (c (n "netlib-provider") (v "0.1.2") (h "1d56aw3gac756halz7n59jcmcjaf85jkzwx9gyjaip9msj86farc") (f (quote (("system-netlib") ("static-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.1.3 (c (n "netlib-provider") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0n3p5n501r6zkiyclkhbf1dxxbb9hdf1im5q3w4qnsnzsf199gip") (f (quote (("system-netlib") ("static-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.1.4 (c (n "netlib-provider") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l1sizkvj2bavd747m9k1chacglk898r1jg9fjhf4kc3ix426891") (f (quote (("system-netlib") ("static-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.1.5 (c (n "netlib-provider") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0wpy88l3052a9ihf650sgiis3dnca5jb4cka6wq8i06y9xizirzm") (f (quote (("system-netlib") ("static-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.2.0 (c (n "netlib-provider") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1dv9m3g8haxk4x6i7vsqn0lqra82iy141jvkpcawfbnax17gy5gc") (f (quote (("system-netlib") ("static-netlib") ("exclude-lapacke") ("exclude-cblas"))))))

(define-public crate-netlib-provider-0.3.0 (c (n "netlib-provider") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1lr2pfy3dlf0a8yh87lq3dkh7k44mhf1nphhdlbf08skm9x89gcw") (f (quote (("system-netlib") ("static-netlib") ("include-lapacke") ("include-cblas") ("default" "include-cblas" "include-lapacke"))))))

(define-public crate-netlib-provider-0.4.0 (c (n "netlib-provider") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0iw4z0s1lhcv9wmf2sm91q47gv9hdd80vlk415szbb7mkn5jlrcn") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

(define-public crate-netlib-provider-0.4.1 (c (n "netlib-provider") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12jhnmgw3hqh476v7c621s1syjy8chx74xanlds5h0bfjm5qkns1") (f (quote (("system") ("static") ("lapacke") ("default" "cblas" "lapacke") ("cblas"))))))

