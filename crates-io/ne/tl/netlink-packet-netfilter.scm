(define-module (crates-io ne tl netlink-packet-netfilter) #:use-module (crates-io))

(define-public crate-netlink-packet-netfilter-0.1.0 (c (n "netlink-packet-netfilter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.4") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8") (d #t) (k 2)))) (h "19gx60azxfhdhjvdwf5pm5cd7k5wd2cgmviflw42jicqmzmydhqj")))

(define-public crate-netlink-packet-netfilter-0.2.0 (c (n "netlink-packet-netfilter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.7.0") (d #t) (k 0)) (d (n "netlink-packet-utils") (r "^0.5.1") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8.3") (d #t) (k 2)))) (h "1mx7ziaampwha0vc062p81fszkyhmiqzj2nbpn5b27vzmpysw253")))

