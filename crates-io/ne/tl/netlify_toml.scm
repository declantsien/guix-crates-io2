(define-module (crates-io ne tl netlify_toml) #:use-module (crates-io))

(define-public crate-netlify_toml-0.1.0 (c (n "netlify_toml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0rgp7r6722r6xcn82gv96rz3x31rhpkz09b9v4nb45fk4q1zngkd")))

(define-public crate-netlify_toml-0.2.0 (c (n "netlify_toml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "0v7g6mz74iz3plsn1kr8ffnbnjbg86f822fgkmz63xg4jw78i0y5")))

(define-public crate-netlify_toml-0.2.1 (c (n "netlify_toml") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "1v1czqcdf3h1radwld3d6m923xll57id5124nvxraw0jr4znk2a3")))

(define-public crate-netlify_toml-0.2.2 (c (n "netlify_toml") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "0z10si3xsw4hj1ki8ks9l6liwhdwdj90riiixgczc209cz2lwr4k")))

(define-public crate-netlify_toml-0.2.3 (c (n "netlify_toml") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "0brz4d7l6q4ndz86m6glhma24q4yj6280bbcwav95z53ns8h84wk")))

(define-public crate-netlify_toml-0.2.4 (c (n "netlify_toml") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "00651qh08knhpcr23pk2mnpqxbxqa5826wh80a3rxak4lzp681sh")))

(define-public crate-netlify_toml-0.2.5 (c (n "netlify_toml") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "16h95lkm8j3f9jz7kxx5kdy6wqzyrd2s4a114md304px43a8q4ch")))

(define-public crate-netlify_toml-0.2.6 (c (n "netlify_toml") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "1ypmy6n08ias9ajz6f4nx04zyj2kz9wp9sdsyk6ifvwm4w63gqq9")))

(define-public crate-netlify_toml-0.2.7 (c (n "netlify_toml") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "1d49i94pkmdizwf92509r5jywmh4z5zb87i4wjz1rf44ws7643d7")))

(define-public crate-netlify_toml-0.2.8 (c (n "netlify_toml") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.9") (d #t) (k 0)))) (h "19jh3wr9fv2vakb2qgxp2c8hv1kd0jaqg0cm47pbbwblgscna4gh")))

(define-public crate-netlify_toml-0.3.0 (c (n "netlify_toml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1c5qzb6dzaln8bzs033sci6244jwijjzdmrmdb79m05ppkwwf3bd")))

(define-public crate-netlify_toml-0.3.1 (c (n "netlify_toml") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bfaa4ialn47chh5qj914gi801x6i5im10qx7wa0c06f6jjqam7a")))

(define-public crate-netlify_toml-0.3.2 (c (n "netlify_toml") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0rk4d85dpjzsa8a9m31f92a6dkp4w7s0ipbchfia5m2gkaj51lpx")))

(define-public crate-netlify_toml-0.3.3 (c (n "netlify_toml") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wqxyfai6lbcp0nf0b0lky7wcnzg9w55mzi46zlhv3xza2hpi6fl")))

(define-public crate-netlify_toml-1.0.0-alpha.1 (c (n "netlify_toml") (v "1.0.0-alpha.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "168kbcn8xsl7nf5dyahzmjcz6b8z5ilhf0lwl4k9r0kagrzz762q")))

(define-public crate-netlify_toml-0.5.0 (c (n "netlify_toml") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "09p8hy3qgh2zidwpzvxxcha6mxw0izq2d8drr58z9af7gfgjiqkh")))

(define-public crate-netlify_toml-0.6.0 (c (n "netlify_toml") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jxpc1zf4a11vxm9wkvb1z7gzmv16gmnl9870npv9mfh2xwc7dcf")))

(define-public crate-netlify_toml-1.0.0 (c (n "netlify_toml") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ljbxfrs7xd4jmd3xrrbnm7lyb52wy0fy6g5f16lmx7yyxf3bxm8")))

