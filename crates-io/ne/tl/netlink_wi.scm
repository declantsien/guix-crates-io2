(define-module (crates-io ne tl netlink_wi) #:use-module (crates-io))

(define-public crate-netlink_wi-0.1.0 (c (n "netlink_wi") (v "0.1.0") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "1sly03mqvhrrzqyf7acg78dqz6b88kvby7xf79n1by025250pgs5")))

(define-public crate-netlink_wi-0.1.1 (c (n "netlink_wi") (v "0.1.1") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "0sadp4wm114bhdqscm12hj7ph86n1zm0ii3wkkggwxgdhb5k9mw2")))

(define-public crate-netlink_wi-0.1.2 (c (n "netlink_wi") (v "0.1.2") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "0x28ji2xmr8fv6lngi89d7la2sdxk9hz439dr24qzjs7qwc2mkcr")))

(define-public crate-netlink_wi-0.2.0 (c (n "netlink_wi") (v "0.2.0") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "0brdfnzy0whyfprf0cc9j48nk8f0p2200891rd503ncffx8cc3mv")))

(define-public crate-netlink_wi-0.2.1 (c (n "netlink_wi") (v "0.2.1") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "0ixcs0bpjjgy6x902hh68k69c6dla18j6yz5h7dczx7p8nj2x5z5")))

(define-public crate-netlink_wi-0.3.0 (c (n "netlink_wi") (v "0.3.0") (d (list (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "1i1nyjbvwm4a9a57w4vc24p8c2m9hn4pnaggbc5nbym70x8rr2rc")))

(define-public crate-netlink_wi-0.4.0 (c (n "netlink_wi") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.6.3") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "1zwzls826pc6i389wyha8hyspmwhj0xhzrrfs2x5wgfya577a7dg")))

(define-public crate-netlink_wi-0.4.1 (c (n "netlink_wi") (v "0.4.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.6.3") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "0fqywxq7dbyb8vcs3xwalp6vb6kkpwawpxrih1wx9aawfijkqwnk")))

(define-public crate-netlink_wi-0.5.0 (c (n "netlink_wi") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.6.3") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "0z67scanbsckaikxg6hbw7322xpb4xl4kd45669r553yhz7qwz00")))

(define-public crate-netlink_wi-0.6.0 (c (n "netlink_wi") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.6.3") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)))) (h "0m3f52cm3p90x7hjwlamqk3fdiwm1xh5aia2p0xw7my0lpammnhh")))

(define-public crate-netlink_wi-0.7.0-rc1 (c (n "netlink_wi") (v "0.7.0-rc1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.7.0-rc2") (f (quote ("sync" "async"))) (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.31.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1mbx46sn0amvhmilxpqzpbvzbjxzfra6x0azcaq2yynxzyds1kpm")))

(define-public crate-netlink_wi-0.7.0-rc2 (c (n "netlink_wi") (v "0.7.0-rc2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.7.0-rc2") (f (quote ("sync" "async"))) (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.31.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1kaxwbm902lf15v5a1a71yyi3fxwlszisg88va6gl9fs7997ndkw")))

(define-public crate-netlink_wi-0.7.0-rc3 (c (n "netlink_wi") (v "0.7.0-rc3") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.7.0-rc2") (f (quote ("sync" "async"))) (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qry89a4s0anpr0a2qa05zxr4h724ml0adlkld5yzmv8h2mv2x41")))

(define-public crate-netlink_wi-0.7.0-rc4 (c (n "netlink_wi") (v "0.7.0-rc4") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "neli") (r "^0.7.0-rc2") (f (quote ("sync" "async"))) (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0g86hxzzyxv0khvnvbz5a6ddkpcihghl36ab0g71g4cl4i9d55z1")))

