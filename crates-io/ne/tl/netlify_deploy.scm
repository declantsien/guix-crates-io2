(define-module (crates-io ne tl netlify_deploy) #:use-module (crates-io))

(define-public crate-netlify_deploy-0.1.0 (c (n "netlify_deploy") (v "0.1.0") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "15qmdijpbjz4i99h8mdifjjp2q6y0j8p2rimdjjabigjddyrgbkh")))

