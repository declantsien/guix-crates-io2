(define-module (crates-io ne ig neighbor) #:use-module (crates-io))

(define-public crate-neighbor-0.1.0 (c (n "neighbor") (v "0.1.0") (d (list (d (n "actix-web") (r "^3") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "sitter") (r "^0") (d #t) (k 0)))) (h "1ak5wd6l1ak5byg78yb4111nz0xsnynjcwf3ypx2xrb0b96lh03a")))

(define-public crate-neighbor-0.1.4 (c (n "neighbor") (v "0.1.4") (d (list (d (n "actix-web") (r "^3") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sitter") (r "^0") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.0") (f (quote ("postgres" "uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1py6mjn3mpiafb9sm9lnbwh7631m1wyzqilbs096x13vbh9mi7xi")))

