(define-module (crates-io ne ig neighborly) #:use-module (crates-io))

(define-public crate-neighborly-0.0.0 (c (n "neighborly") (v "0.0.0") (h "0dkjzdzcz8khjfnj4vg73ida4kd5i3jqsfm8ciqw9a0f0jv13jvb")))

(define-public crate-neighborly-0.0.1 (c (n "neighborly") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 0)) (d (n "trait-variant") (r "^0.1.2") (d #t) (k 0)))) (h "0gc4jkhzkjynnihp9l4495hqg7vm50ri7q8kkwc0302bc5srld7m")))

