(define-module (crates-io ne ig neighborhood-diversity) #:use-module (crates-io))

(define-public crate-neighborhood-diversity-0.5.0 (c (n "neighborhood-diversity") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "05nr1ivd7vk917ay3lvsp6pw947106px4q01h3yq4kq254lpdl1l")))

(define-public crate-neighborhood-diversity-0.5.1 (c (n "neighborhood-diversity") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pnazichrv6bypafhpcyc3kc823gagfkx1qirbj899hkilyz2dr7")))

(define-public crate-neighborhood-diversity-0.5.2 (c (n "neighborhood-diversity") (v "0.5.2") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0cmrbjja4avyxwn416p0pdrklmjxrwkhz734gqynja13fng74ba7")))

(define-public crate-neighborhood-diversity-0.5.3 (c (n "neighborhood-diversity") (v "0.5.3") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07sq03m4kjnqjp35k3mdckpa8dfc41vfj33fdaq3bqnizgghnvkx")))

(define-public crate-neighborhood-diversity-0.5.5 (c (n "neighborhood-diversity") (v "0.5.5") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bsv8v67iqpgmb7zw3s52xivcajk5xdlmwqmp3yzyx1s59sfd3y4")))

