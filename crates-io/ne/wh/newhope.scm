(define-module (crates-io ne wh newhope) #:use-module (crates-io))

(define-public crate-newhope-0.1.0 (c (n "newhope") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "079dlvlkh29d6vghjyvyf172kk8k76vri3vk98bpxld782apdwf7") (f (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1.1 (c (n "newhope") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "1wpbh41l1l6c7wk6k670vp8id038qid03lkw4nlcm77f6wvaxvzz") (f (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1.2 (c (n "newhope") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "1alybw6lykc2bn3405nbd3syl62h7nmvndnxzvbb405v7hlxcm7j") (f (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1.3 (c (n "newhope") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "0jfrs88nr49lcyddhjbpqz5zrvfnkdbi11sbw70q10zj735kk449") (f (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1.4 (c (n "newhope") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "13jxmrhlii2ancdxkfx1fzms5bdpwymji9cs5wfkxsa2k57ylhha") (f (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1.5 (c (n "newhope") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "12nrgn1gzwc7a5ifa44j0k5fdsjnplxanddi6i8hv5cy5xfyiyr6") (f (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1.6 (c (n "newhope") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "04b7p4lsl2mr2qiqxvgkhbb3qqgzgddc44f71jcdy3jxdpyq24nw") (f (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1.7 (c (n "newhope") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)))) (h "0la1s4ysvma9fsw0sqgplfajz4az8h296y57wq0f7xbmd6l79nyi") (f (quote (("tor")))) (y #t)))

(define-public crate-newhope-0.2.0 (c (n "newhope") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)))) (h "12r4p558cq47lpz2v93zwq7z0f84wwl3k3jpvgxv0xr0hvgsxspx") (f (quote (("tor"))))))

(define-public crate-newhope-0.3.0 (c (n "newhope") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "09llyc6wlx1j362abz7qkcg7fkgylh8i2vij3j3ky5ym4ih6q8j3") (f (quote (("tor"))))))

