(define-module (crates-io ne st nested) #:use-module (crates-io))

(define-public crate-nested-0.1.0 (c (n "nested") (v "0.1.0") (h "1ixhn22ji0jg52y1rw1fc7zcik29vfq2ij6h5lnv8nyx3l8dvxls")))

(define-public crate-nested-0.1.1 (c (n "nested") (v "0.1.1") (h "17lwhdw0z8c4g00yfdasxh4zc5dq1ccylmbb0n1zw1wgcc7l4aya")))

