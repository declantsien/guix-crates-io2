(define-module (crates-io ne st nested_router) #:use-module (crates-io))

(define-public crate-nested_router-0.2.0 (c (n "nested_router") (v "0.2.0") (d (list (d (n "route-recognizer") (r "^0.3.1") (d #t) (k 0)))) (h "0vp5vva46w48wkbhsym4d2zzgv7ds551sfff6q2gdy9ad83cambc")))

(define-public crate-nested_router-0.2.1 (c (n "nested_router") (v "0.2.1") (d (list (d (n "route-recognizer") (r "^0.3.1") (d #t) (k 0)))) (h "0pc7y8bvq2mmwv2f799jd8b29kbiv6xc46yiy1jhnnpd8f2ig7cs")))

