(define-module (crates-io ne st nest_analyzer) #:use-module (crates-io))

(define-public crate-nest_analyzer-0.0.1 (c (n "nest_analyzer") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_common") (r "=0.7.0") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "=0.26.0") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "=0.31.0") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.11.0") (d #t) (k 0)))) (h "146msjfdsk07awq4yw81z22avs4fa8p3s1wm9qldj94004qw154a")))

