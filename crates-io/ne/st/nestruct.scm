(define-module (crates-io ne st nestruct) #:use-module (crates-io))

(define-public crate-nestruct-0.1.0 (c (n "nestruct") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1vc1bbfi5xwbmj31r8xalj3s635qzpyv8f0v5z7s2vxjac124viz")))

