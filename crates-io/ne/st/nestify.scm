(define-module (crates-io ne st nestify) #:use-module (crates-io))

(define-public crate-nestify-0.1.0 (c (n "nestify") (v "0.1.0") (h "0gc2iinlkygiagcv2rgyn40vbrphwfkc7x6m256jgwln2p7byrf5") (y #t)))

(define-public crate-nestify-0.1.1 (c (n "nestify") (v "0.1.1") (h "14xwcqyprfnwa3mfkwqlyaim4c7kf14crzd0r13fw7171msb1bwg")))

(define-public crate-nestify-0.2.0 (c (n "nestify") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "19jczrkgwjkp47q1qnjrhky07wc6fa4c4za0r1d12w6x2ia2wcv6")))

(define-public crate-nestify-0.2.1 (c (n "nestify") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0yff8bahr1lk82ih1l0dynx74l7cjh4jl6wi44lqf9dwnasyxdbl")))

(define-public crate-nestify-0.3.1 (c (n "nestify") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15fgvpngg1i7sy6i3mmgwli2b62h5z1c523bxv8f8298xh85qbaz")))

(define-public crate-nestify-0.3.2 (c (n "nestify") (v "0.3.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h6dnf6jv0fhqv0km9mnhjkx5qp75545p66gs8d7iqa3cpi0z3qr")))

(define-public crate-nestify-0.3.3 (c (n "nestify") (v "0.3.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05zacyhzh97h4wj72jwfpxjgilk3nx81h6rv1yjfim12f6gj9ms1")))

