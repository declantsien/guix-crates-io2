(define-module (crates-io ne st nestle_core) #:use-module (crates-io))

(define-public crate-nestle_core-0.1.0 (c (n "nestle_core") (v "0.1.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "05nbqn2mrvryykbvw6plj7n839ag8zfxlwhrnhiriz6lpw7kcm45")))

