(define-module (crates-io ne st nested_containment_list) #:use-module (crates-io))

(define-public crate-nested_containment_list-0.1.0 (c (n "nested_containment_list") (v "0.1.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1ikva54r02gm4mhlr4l102v1kqji55a9hrx18ap6ji630rcw45zj")))

(define-public crate-nested_containment_list-0.1.1 (c (n "nested_containment_list") (v "0.1.1") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1zsx5m1kx718mqp9pvr12m4i81xd759nfph3gn68m7xgmva24pwc")))

(define-public crate-nested_containment_list-0.2.0 (c (n "nested_containment_list") (v "0.2.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "more_ranges") (r "^0.1.0") (d #t) (k 2)))) (h "0nfazmvpm60d6n6b77vwaxlpa9rxzaqizfgjskml5vpi7gjr2f8a")))

(define-public crate-nested_containment_list-0.2.1 (c (n "nested_containment_list") (v "0.2.1") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "more_ranges") (r "^0.1.0") (d #t) (k 2)))) (h "067hy2gm5i45czkvch1qk1pb2cj4kkdwpwi3l83ai25v0v5dda8w")))

(define-public crate-nested_containment_list-0.3.0 (c (n "nested_containment_list") (v "0.3.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "doc_item") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "more_ranges") (r "^0.1.0") (d #t) (k 2)))) (h "1iklr9dnqlcyjdm7cfz1wqz3p1z2xsivxkj98d2q2lvlc3ajg40j") (f (quote (("doc" "doc_item"))))))

(define-public crate-nested_containment_list-0.3.1 (c (n "nested_containment_list") (v "0.3.1") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "doc_item") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "more_ranges") (r "^0.1.0") (d #t) (k 2)))) (h "1pyxl2dgaib3aml7amd6szf1s0s4jqsbsbagv6jj17pnvcx5wr9h")))

