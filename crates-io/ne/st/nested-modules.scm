(define-module (crates-io ne st nested-modules) #:use-module (crates-io))

(define-public crate-nested-modules-0.1.0 (c (n "nested-modules") (v "0.1.0") (h "0lpgrn4bbrqzhmd45wzy0zp92i35w2cpqlrb5afrsp6rm709hqkp")))

(define-public crate-nested-modules-0.2.0 (c (n "nested-modules") (v "0.2.0") (h "0izzdwp4xp3hlm42wvyh7ly00cw0nqr5nn8xafw218r001s7pmqz")))

