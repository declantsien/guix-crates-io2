(define-module (crates-io ne st nestadia-wasm) #:use-module (crates-io))

(define-public crate-nestadia-wasm-0.1.0 (c (n "nestadia-wasm") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "nestadia") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.50") (f (quote ("HtmlCanvasElement" "CanvasRenderingContext2d" "ImageData" "File" "FileList"))) (d #t) (k 0)) (d (n "yew") (r "^0.18.0") (d #t) (k 0)))) (h "1birgf1011xa0x6vdls48y1wxfvnhy4932rv1ylikxbx13a6rj2i")))

