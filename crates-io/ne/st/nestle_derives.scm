(define-module (crates-io ne st nestle_derives) #:use-module (crates-io))

(define-public crate-nestle_derives-0.1.0 (c (n "nestle_derives") (v "0.1.0") (d (list (d (n "nestle_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pvygimygd3ilj0xl9zmi4cg7mi4wjnsgzk34nyb699k77d9f9dx")))

