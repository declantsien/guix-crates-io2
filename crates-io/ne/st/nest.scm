(define-module (crates-io ne st nest) #:use-module (crates-io))

(define-public crate-nest-0.1.0 (c (n "nest") (v "0.1.0") (d (list (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "16mm4qi5yj53njdisrhbwqfqvk5nhikxw7zzqii17x09a95dmhqj")))

(define-public crate-nest-0.2.0 (c (n "nest") (v "0.2.0") (d (list (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0vp2gyj8rsa911chv5g9lbjss84ygrr9agrd5xgy6j47xhbm8fjj")))

(define-public crate-nest-0.3.0 (c (n "nest") (v "0.3.0") (d (list (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0c1zwf601z8r2xmsp7il6nmp021axn9nmry63pw0mg6w07hp5pwg")))

(define-public crate-nest-0.3.1 (c (n "nest") (v "0.3.1") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0iabva0frc3xiirs9b1035hbb0a9fn77d6nyrlf8gwyirprps870")))

(define-public crate-nest-0.3.2 (c (n "nest") (v "0.3.2") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1y5x2jhqmvkijxz6jnf4shpy4kwxxz1l7sadwi3vjkj86qaphacy")))

(define-public crate-nest-0.3.3 (c (n "nest") (v "0.3.3") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0l155ds882rlqx38bp8m1m1q663ma5bpmfkcy8xn6y43x4amkqbm")))

(define-public crate-nest-0.3.4 (c (n "nest") (v "0.3.4") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "12x768xba9pjpib7iqib4c3ak0kx94kkii744q39qjc70vc0hkg1")))

(define-public crate-nest-0.3.5 (c (n "nest") (v "0.3.5") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkdirp") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "00v66vy2a7sj5clf4h9mhlnpchdb90za6z25ry56a87pg2lml9ln")))

(define-public crate-nest-0.3.6 (c (n "nest") (v "0.3.6") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkdirp") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1saj8fymn0b9rhgz3n7gkzc68jc1519rp2j5f7p2cd03drlwga8b")))

(define-public crate-nest-0.4.0 (c (n "nest") (v "0.4.0") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkdirp") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "12sic335j9lf43v20cj1aqxmqplf7jv9bwwpyij0jf66nvypffkd")))

(define-public crate-nest-1.0.0 (c (n "nest") (v "1.0.0") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkdirp") (r "^0.1") (d #t) (k 0)) (d (n "objekt") (r "^0.1") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.9") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "snafu") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "12kqv802gz47aq4k9rykv9n79v0gsaflhysbx2gawcifis8j5cbc")))

