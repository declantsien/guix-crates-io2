(define-module (crates-io ne st nestjs-nats-connector) #:use-module (crates-io))

(define-public crate-nestjs-nats-connector-0.1.0 (c (n "nestjs-nats-connector") (v "0.1.0") (d (list (d (n "async-nats") (r "^0.10.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j6s1sv2zdg6bidbmx4ywmb31yyr8y1gdy184kxqrl3ilqil83lc")))

(define-public crate-nestjs-nats-connector-0.2.0 (c (n "nestjs-nats-connector") (v "0.2.0") (d (list (d (n "async-nats") (r "^0.10.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "11vsh1slla0s056q81dng7kkxa3xnjml4k1gr3avz2q5rpy7dy0v")))

(define-public crate-nestjs-nats-connector-0.3.0 (c (n "nestjs-nats-connector") (v "0.3.0") (d (list (d (n "async-nats") (r "^0.10.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x7ainyxm39q399138yrczqwkqrfd2nnl4g1wa7hj7qrg2nyn5ka")))

