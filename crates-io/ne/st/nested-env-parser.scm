(define-module (crates-io ne st nested-env-parser) #:use-module (crates-io))

(define-public crate-nested-env-parser-1.0.0 (c (n "nested-env-parser") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)))) (h "0cnaqwhm1v1k10a0dp4d59jn539k5hv4lycw57z8hyvvdmp1a2v4")))

(define-public crate-nested-env-parser-1.1.0 (c (n "nested-env-parser") (v "1.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)))) (h "1dna5xcgial1f1c8b5x05sfng6pv2lykfcla6hrxnpxnq5hzyabx")))

