(define-module (crates-io ne st nestadia) #:use-module (crates-io))

(define-public crate-nestadia-0.1.0 (c (n "nestadia") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1rfc8064aami31vfhpxv6vlk65a8vsr93d696yir8wlv7ag0lmq0") (f (quote (("default") ("debugger"))))))

