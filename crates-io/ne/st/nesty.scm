(define-module (crates-io ne st nesty) #:use-module (crates-io))

(define-public crate-nesty-0.1.0 (c (n "nesty") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "prettydiff") (r "^0.5.0") (o #t) (k 0)))) (h "03hb8jhrd46kz9inf4n5qk4ky4yd0md8k60xck34z3pwy1qqj0yp") (f (quote (("diff_assert" "prettydiff" "colored"))))))

(define-public crate-nesty-0.2.0 (c (n "nesty") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "prettydiff") (r "^0.5.0") (o #t) (k 0)))) (h "1x8fx7wkqix5snysmyyj5h001yw4drh1nk03y221kvk3fzs01iys") (f (quote (("diff_assert" "prettydiff" "colored"))))))

