(define-module (crates-io ne st nested_qs) #:use-module (crates-io))

(define-public crate-nested_qs-0.1.0 (c (n "nested_qs") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "0vazn3jjd4mi4n9r6fwvyhdi5d7rgmmz87mn0wdfbhvwkhaz3b4g")))

(define-public crate-nested_qs-0.1.1 (c (n "nested_qs") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "1lhksb9w800c8qqzg1x6ci057csl43wycfq8jmzs3q243lzngc6c")))

(define-public crate-nested_qs-0.1.2 (c (n "nested_qs") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "1qcr6v6yqw9h9y0da4zwqyp0mgdnih2n72qpfimbq3r86w7xd6fv")))

