(define-module (crates-io ne st nested_intervals) #:use-module (crates-io))

(define-public crate-nested_intervals-0.1.0 (c (n "nested_intervals") (v "0.1.0") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1vqbp7ys9avmclpncr6vrbqll7wwvvcx708v24x9c6dkn7p97by4")))

(define-public crate-nested_intervals-0.1.1 (c (n "nested_intervals") (v "0.1.1") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1ihfzdqag4p09nwswys6pwj8fhz4bd3mgm9si67pmy1zlni22pbx")))

(define-public crate-nested_intervals-0.1.2 (c (n "nested_intervals") (v "0.1.2") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0f985f654c86f3zhylj3pvsv83xc1akw4zgpygmlpzhghw42mq6c")))

(define-public crate-nested_intervals-0.1.3 (c (n "nested_intervals") (v "0.1.3") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1cy0pcnw98fipg1hf76jjas2biz59sbkf7hmi2y60bw6s48fgawc")))

(define-public crate-nested_intervals-0.1.4 (c (n "nested_intervals") (v "0.1.4") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1smakj6391a3f5wx6qnmjkxr5dmp3p4wjk4hr0bql6w7siv5qw6r")))

(define-public crate-nested_intervals-0.1.5 (c (n "nested_intervals") (v "0.1.5") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "10cx5madzfcfg7pxgrlm3j9x8gyv1c9skflri5xvl3cf6nrayjri")))

(define-public crate-nested_intervals-0.1.6 (c (n "nested_intervals") (v "0.1.6") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1ynf8ahljvjmqk4i12gjq6ly3b8n6105aqmaabjl29bqrkisnn1v")))

(define-public crate-nested_intervals-0.1.7 (c (n "nested_intervals") (v "0.1.7") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0m9937p0xr9kgri3x4qlrjy1d70b64b6r41cq26zhh6g9j9w748k")))

(define-public crate-nested_intervals-0.1.8 (c (n "nested_intervals") (v "0.1.8") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1as5s12ks6bbpgjbd5vs5c9pzc9nsdsaclj3vdpw7vbjn7kw7csg")))

(define-public crate-nested_intervals-0.1.9 (c (n "nested_intervals") (v "0.1.9") (d (list (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1wsf4baby8icq7rxr5r6v6bndi6hv82ya6mgd38k7z7z1i2ygbfg")))

(define-public crate-nested_intervals-0.2.0 (c (n "nested_intervals") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0rcqbricb27bwzvb9zhfgs23psa6w8r7w41ga53qr4jdjzs4ccpf")))

(define-public crate-nested_intervals-0.2.1 (c (n "nested_intervals") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1cc1y98bp420i7pbp7ply21j0rkchgmm0q23j05hvd8hl4s0g6nm")))

(define-public crate-nested_intervals-0.2.2 (c (n "nested_intervals") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1pym2hk1a4vvwsp0p590cdmbzlihm6kfvwmkdjysbgi7wlx2dr2j")))

(define-public crate-nested_intervals-0.3.0 (c (n "nested_intervals") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "194iww8m1h564nxdhka1snrd7h4mgkld9imyzi6qywwv5pfbibyv")))

(define-public crate-nested_intervals-0.4.0 (c (n "nested_intervals") (v "0.4.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "1x4hljlw9crnzqaqcdg2m1ych854llviqayfv0a49k9x011n29lw")))

(define-public crate-nested_intervals-0.5.0 (c (n "nested_intervals") (v "0.5.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "00mnzb3pcz0ni8acbfw9w0hhv35xkx5hfdw06xkcf5yc3pm3308q")))

(define-public crate-nested_intervals-0.5.1 (c (n "nested_intervals") (v "0.5.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "0jwlrrc2l813b6568ly27lsbpmkga0whhk0skachjl4w4zv1vyxx")))

(define-public crate-nested_intervals-0.6.0 (c (n "nested_intervals") (v "0.6.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "089609838aac5plb0460br8bxj265sq0ri749pq86x3wv65h9m40")))

