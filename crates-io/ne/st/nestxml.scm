(define-module (crates-io ne st nestxml) #:use-module (crates-io))

(define-public crate-nestxml-0.1.0 (c (n "nestxml") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0110y7i4rjnwsvh7jl1zkwwipgqr931m79km81906wkbk9wkbgas")))

(define-public crate-nestxml-0.2.0 (c (n "nestxml") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1289psn8y5ampiz14gigrcg2vpi1pihx9sri3xphjc5vzsizmxy1")))

(define-public crate-nestxml-0.2.1 (c (n "nestxml") (v "0.2.1") (d (list (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1x98mx3ygclkhcf98gpz6fh65saw5vqsniaql276rrfi00lbqmcm")))

(define-public crate-nestxml-0.2.2 (c (n "nestxml") (v "0.2.2") (d (list (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1fxf7xcgvkfra6vdfzbilbazwfm9p3g4zlfwsz9b1kf70z6ddlgq")))

