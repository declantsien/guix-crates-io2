(define-module (crates-io ne st nestadia-wgpu) #:use-module (crates-io))

(define-public crate-nestadia-wgpu-0.1.0 (c (n "nestadia-wgpu") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5.5") (d #t) (k 0)) (d (n "nestadia") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "03cxlrhjzl2l6i0n78957pqy0i20rjgslaz78zk42ynzpy9rv1cq")))

