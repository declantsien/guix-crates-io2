(define-module (crates-io ne st nestools) #:use-module (crates-io))

(define-public crate-nestools-0.1.0 (c (n "nestools") (v "0.1.0") (d (list (d (n "lodepng") (r "^2.1.2") (d #t) (k 0)))) (h "075rx2y3gsgwq0la71a5lbaij0g945lpmh6pggs71wx4l9vj3ay9")))

(define-public crate-nestools-1.0.0 (c (n "nestools") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kqby7ym431l8i6i3fqvkcs2ci614shgsz4056vv5k8ip2rcndb0")))

(define-public crate-nestools-1.0.1 (c (n "nestools") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zl9f5q3sgi7ccaiwlvqlfn53k1hcb69pi37s56is9w3s7aca33m")))

(define-public crate-nestools-1.1.0 (c (n "nestools") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "006xfnxzmbkzn7acsky9kfiqmrwxr3iyp7zssbsvla26kisv2p31")))

(define-public crate-nestools-1.1.1 (c (n "nestools") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yabg2qn6b822d461s5nqn75svxmilsskvrc2aydwglgvmc6asg1")))

(define-public crate-nestools-1.1.2 (c (n "nestools") (v "1.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1av3mfnilp1aaznf054gfbbpkgw7a22w2m9sv3r1kqnszdaw0ji5")))

(define-public crate-nestools-1.2.0 (c (n "nestools") (v "1.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xi0r9k0kl5x795s1hd7ciz2jiy5y9yw9c48ycp874v45il6k243")))

(define-public crate-nestools-1.2.1 (c (n "nestools") (v "1.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w16k3wmlqjvzy5z06k6jwji9wj9n5kd1agxlhv4w65f48zxsm0a")))

(define-public crate-nestools-1.2.2 (c (n "nestools") (v "1.2.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06xd0mk1a0c6pnyvf3idic0k2cjsjapp43llmd9a7i37fzp4iqlg")))

(define-public crate-nestools-1.3.1 (c (n "nestools") (v "1.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1m67iya62knhdwybxncvjyxgjz0q170zznn7xias12py1l95ay37")))

