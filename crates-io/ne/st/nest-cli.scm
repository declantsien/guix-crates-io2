(define-module (crates-io ne st nest-cli) #:use-module (crates-io))

(define-public crate-nest-cli-0.1.0 (c (n "nest-cli") (v "0.1.0") (d (list (d (n "clap-log-flag") (r "^0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nest") (r "^0.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0b6wrg50vly1kf3nn9hbjwwwn5i1hflqljq1nq4xd55ck670dcqm")))

(define-public crate-nest-cli-0.1.1 (c (n "nest-cli") (v "0.1.1") (d (list (d (n "clap-log-flag") (r "^0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nest") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1hmkaqnjjpfl9qgc9nwc7schbqmynl3dxa9cndfsclcc7qhlcpvk")))

(define-public crate-nest-cli-0.2.0 (c (n "nest-cli") (v "0.2.0") (d (list (d (n "clap-log-flag") (r "^0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nest") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0nng1sm4db18nbf3c7qfqgzy12nm1caczk16jw3362g3zqb4wyjj")))

(define-public crate-nest-cli-0.3.0 (c (n "nest-cli") (v "0.3.0") (d (list (d (n "clap-log-flag") (r "^0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nest") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1q9533k0x2vb22scj5w20f51g5p5nj1kn7rkz10c56kz2da61ivj")))

