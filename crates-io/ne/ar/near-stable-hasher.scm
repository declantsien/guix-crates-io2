(define-module (crates-io ne ar near-stable-hasher) #:use-module (crates-io))

(define-public crate-near-stable-hasher-0.1.0 (c (n "near-stable-hasher") (v "0.1.0") (h "1f2s2qk0jjp1djqj6286nm3svhja4vw0rh7d6hnb8n4nvdb02wy1")))

(define-public crate-near-stable-hasher-0.16.0 (c (n "near-stable-hasher") (v "0.16.0") (h "074mhh1bdkl67z5fp229p0kv7p2hgwdc3csqzy32r56s1l6v13bp") (r "1.68.0")))

(define-public crate-near-stable-hasher-0.16.1 (c (n "near-stable-hasher") (v "0.16.1") (h "1x7dnsb30ldfp1by1ia76whgk5jr6xw3h92xisamphvm1iv6ikfz") (r "1.68.0")))

(define-public crate-near-stable-hasher-0.17.0 (c (n "near-stable-hasher") (v "0.17.0") (h "0vz3b00zxizn22wnwaiiq72klrb21jjvag0h3vxnws7hr87kr6kn") (r "1.69.0")))

(define-public crate-near-stable-hasher-0.19.0-pre.2 (c (n "near-stable-hasher") (v "0.19.0-pre.2") (h "0khx53qkiyykghbyxws0j2mkcb8myygfl268cs6kldrps84cq3zl")))

(define-public crate-near-stable-hasher-0.19.0 (c (n "near-stable-hasher") (v "0.19.0") (h "0zrci0qiglwsqk8b0g93n5acmid43zl5k6ldyvm7awxbzs1magns")))

(define-public crate-near-stable-hasher-0.20.0 (c (n "near-stable-hasher") (v "0.20.0") (h "1fczhhsi0njm76ghfvyc4vsgy5yhzg1zbprwg24ldd48rv095sc6")))

(define-public crate-near-stable-hasher-0.20.1 (c (n "near-stable-hasher") (v "0.20.1") (h "0wkz15sjff64mzvkir01p1r0qmp306xb82nvn04iwgg6vfl5z51m")))

(define-public crate-near-stable-hasher-0.21.0 (c (n "near-stable-hasher") (v "0.21.0") (h "09yvgb02q6kjw7pa2bm4mlcr4fjpmb7i5mimpdyvgwkhjixsgs98")))

(define-public crate-near-stable-hasher-0.21.1 (c (n "near-stable-hasher") (v "0.21.1") (h "0qc16k0nvjnx2sykkqma9v9yz47wjhhgfwssndmsqdjxih3v03m3")))

(define-public crate-near-stable-hasher-0.21.2 (c (n "near-stable-hasher") (v "0.21.2") (h "1w99isjgr94nsmrfvkll82xw466h64fl3mh9g31h1cs214sz1imv")))

(define-public crate-near-stable-hasher-0.22.0 (c (n "near-stable-hasher") (v "0.22.0") (h "0vqaw3bvsar4rkx5z866fyvvzby5cw5fzsllkjrnrdw8hnan1fan")))

