(define-module (crates-io ne ar near-bindgen-core) #:use-module (crates-io))

(define-public crate-near-bindgen-core-0.1.0 (c (n "near-bindgen-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1s6r336hgiczc3gn3s115fz8rjf65f1giaw474qlspmainfn5zkv")))

(define-public crate-near-bindgen-core-0.2.0 (c (n "near-bindgen-core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1c3qdzr02arl65bsa5bn3phxk2hqznppfn6l4wl2j2i3gvr92siq")))

(define-public crate-near-bindgen-core-0.2.1 (c (n "near-bindgen-core") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04q70ihq4q18ci1i876vvq40lhzcxb09m4z5kabdxwnl1pwx27i0")))

(define-public crate-near-bindgen-core-0.2.2 (c (n "near-bindgen-core") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1k8wrj87i404qbdx9jgjyvmk59q9k6b7r8hw8a7421ra3cdxw7xm")))

(define-public crate-near-bindgen-core-0.2.3 (c (n "near-bindgen-core") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1lnlvf1b1mbs6868d806flx70ii854fymnxwhykmn14zxi4d15nz")))

(define-public crate-near-bindgen-core-0.2.4 (c (n "near-bindgen-core") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "16lnd2al18c0a75kzsr35r9yqnv6m0092qxb6f1gaq0hxbiclnq5")))

(define-public crate-near-bindgen-core-0.3.1 (c (n "near-bindgen-core") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1bmz2w1nwxqnzcv2c4dhaxrfbv6dx1mrp2pbx9h2fwlpalwh6rwk")))

(define-public crate-near-bindgen-core-0.3.2 (c (n "near-bindgen-core") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0i4cfj6qq9fppcbll42l41kar3j305f4d5xb85is10zlz6xsqhm5")))

(define-public crate-near-bindgen-core-0.3.3 (c (n "near-bindgen-core") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1fyz9piz3747bd11sa660ga0gvvqxcmlw18m6ak570hjd3x0mkyj")))

(define-public crate-near-bindgen-core-0.3.4 (c (n "near-bindgen-core") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1l9wk0gz3wnlnpcc7ypl9nx56w7804kxmqsc642pdm4hx6y0vzdi")))

(define-public crate-near-bindgen-core-0.3.5 (c (n "near-bindgen-core") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ax52vnbwb44gg6gccfa7rvk0c1b579r0dl2mzs5x8qg29jixr20")))

(define-public crate-near-bindgen-core-0.3.6 (c (n "near-bindgen-core") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jll068919lpfyv3acal88c4xcq1w6bcq4il43hxvxi6cjq5888r")))

(define-public crate-near-bindgen-core-0.3.7 (c (n "near-bindgen-core") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18yhnhifn4z2qi5whyawr0hnx8ps9520xgr8x86j925fggya2n4w")))

(define-public crate-near-bindgen-core-0.3.8 (c (n "near-bindgen-core") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0b6pl74mgyyvwymy5yhj5qhn6rjhpba0a9wdmbay57fc28k455dq")))

(define-public crate-near-bindgen-core-0.3.9 (c (n "near-bindgen-core") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1bw2n97l2ff4pq6s9xj2iwxxq62rnrm5brkqx9icacvf1fzlpk59")))

(define-public crate-near-bindgen-core-0.3.10 (c (n "near-bindgen-core") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0sld7ivzqp38hpzgqj5wi9gvbn5sblqvflxc1nplpsg88w668kcl")))

(define-public crate-near-bindgen-core-0.3.11 (c (n "near-bindgen-core") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "05bqpwb680mf0ccmc74j9sq9iplr4166q7smbg0acm5mqggfy8f1")))

(define-public crate-near-bindgen-core-0.4.0 (c (n "near-bindgen-core") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0i1nfbc8k0rrskzczc7fwsdrg9503mwmsq5byzj42winfqi4wpx9")))

(define-public crate-near-bindgen-core-0.4.1 (c (n "near-bindgen-core") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0j3n6i94fhdhkfp6k17aa20yl22glvhn8mjl8wsjz4ylcfzb43k7")))

(define-public crate-near-bindgen-core-0.4.2 (c (n "near-bindgen-core") (v "0.4.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1ij3l0pfi9gfw8nby8qknmvzlplv5gj53h6d4l8xs4b8s1rgmyiw")))

(define-public crate-near-bindgen-core-0.5.0 (c (n "near-bindgen-core") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "00spyqncmrdp6ljdzpl0l358nhqpg4wfs5rlr50zvj1gqss11y10")))

(define-public crate-near-bindgen-core-0.6.0 (c (n "near-bindgen-core") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1d2gw3jyxprgcrp8f8nfk3hcllipcdmy6xp3ry7247i6wjy50c61")))

