(define-module (crates-io ne ar near-metrics) #:use-module (crates-io))

(define-public crate-near-metrics-0.1.0 (c (n "near-metrics") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)))) (h "13jjc8w4b5val9qgq2hsgddh1r2pifkjkjsvr375zc8djsydrffp")))

(define-public crate-near-metrics-0.5.0 (c (n "near-metrics") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)))) (h "00h5knziasg7pjxxkhjfvln5lgzi0nnq7whm82w0npalcwh3fcz8")))

(define-public crate-near-metrics-0.10.0 (c (n "near-metrics") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)))) (h "0n9928bs37wfn7r9wc8qwfzm0m8nr7ccsbwgvgzx199m27gwr0yp")))

(define-public crate-near-metrics-0.11.0 (c (n "near-metrics") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)))) (h "0bfa4f74kr59iyqmz06yig371czdj2gdvmxl74j4srb9zpnx5dcf") (r "1.56.0")))

(define-public crate-near-metrics-0.12.0 (c (n "near-metrics") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)))) (h "1nwaxbay3lbacvphkghlsia8cb0q6dqw4ssp7c3p6k5jar5hgx4d") (r "1.56.0")))

(define-public crate-near-metrics-0.13.0 (c (n "near-metrics") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)))) (h "1k9h5z8xg87bk1ncw7ak88qcb429ivc6mgd1mbrr36jzk2p8dcx9") (r "1.60.0")))

(define-public crate-near-metrics-0.14.0 (c (n "near-metrics") (v "0.14.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prometheus") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)))) (h "18nimjbyjvhhlsvmfdv8wyayp33r1mc6yn4mhkba6zgd11bnlbqq") (r "1.60.0")))

