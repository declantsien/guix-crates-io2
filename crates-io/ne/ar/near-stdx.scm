(define-module (crates-io ne ar near-stdx) #:use-module (crates-io))

(define-public crate-near-stdx-0.15.0 (c (n "near-stdx") (v "0.15.0") (h "1mwwj5i45cjvdb111ixpimw3abam3b5bmk2a3dzsblv8lgj32apn") (r "1.65.0")))

(define-public crate-near-stdx-0.16.0 (c (n "near-stdx") (v "0.16.0") (h "03k53l071lmf17718dcsgy0k2p8zh3r03pxgpyp7l20cm7cxw1bc") (r "1.67.1")))

(define-public crate-near-stdx-0.16.1 (c (n "near-stdx") (v "0.16.1") (h "1zwv4aycyjgrwzn83jkprd2ngzx184958qjbrg14k6jb4yz7j4mw") (r "1.68.0")))

(define-public crate-near-stdx-0.17.0 (c (n "near-stdx") (v "0.17.0") (h "1g137ay6kh5x4qj3c0dv8zbsvwjc4k6yhyavaxfzx5jyp8piah35") (r "1.69.0")))

(define-public crate-near-stdx-0.17.1 (c (n "near-stdx") (v "0.17.1") (h "0s3q1f7sm59gxa064lkrg5gb45vhi3q8bm78i0fk5knranai9yzm") (y #t) (r "1.71.0")))

(define-public crate-near-stdx-0.18.0 (c (n "near-stdx") (v "0.18.0") (h "0g7q3v13ipsqkrswhcgbvdpf65555z99lyl2cplazvpkar7axrlv") (r "1.71.0")))

(define-public crate-near-stdx-0.19.0-pre.1 (c (n "near-stdx") (v "0.19.0-pre.1") (h "0ffzy2j12kyhwnrg6nshn7cllihb1yxf2kl1sdmb0lmxi1lb6nqs")))

(define-public crate-near-stdx-0.19.0-pre.2 (c (n "near-stdx") (v "0.19.0-pre.2") (h "1xlrmphs5xyvcwwvjxasyyhawws60p89gv9134al6m4hj964759q")))

(define-public crate-near-stdx-0.19.0 (c (n "near-stdx") (v "0.19.0") (h "0cl9yr2j72lnx2hkz6msz38kf7r30ixv601fdgnv95rmplqx7qla")))

(define-public crate-near-stdx-0.20.0 (c (n "near-stdx") (v "0.20.0") (h "09i274qjgxvkr9rzrmxhvdcyhgrsap8a5am6myfizq66f6j39k7n")))

(define-public crate-near-stdx-0.20.1 (c (n "near-stdx") (v "0.20.1") (h "1f23rckjn0kd78va2pa6dyrp3cz43vds5aqjvzzgckrv1radapw5")))

(define-public crate-near-stdx-0.21.0 (c (n "near-stdx") (v "0.21.0") (h "0gfzzj61cci31w9p4i5cvyybya8a9758q256mlvzx3vfi3k7hrf5")))

(define-public crate-near-stdx-0.21.1 (c (n "near-stdx") (v "0.21.1") (h "1fi8fh7xxai0xj3y1gkf76zizakw9madhzkc1dmyjl7gcyczv7pa")))

(define-public crate-near-stdx-0.21.2 (c (n "near-stdx") (v "0.21.2") (h "0hvwwxb23agb1n3cr0nz6zng1vx8n2dc9j76pbxd046127rrg9kk")))

(define-public crate-near-stdx-0.22.0 (c (n "near-stdx") (v "0.22.0") (h "1fcs6856szqph2j80dbf0fw5m7sxhca36vg80ms9p3fj4bdf04g1")))

