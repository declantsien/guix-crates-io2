(define-module (crates-io ne ar near_schemafy_lib) #:use-module (crates-io))

(define-public crate-near_schemafy_lib-0.7.0 (c (n "near_schemafy_lib") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemafy_core") (r "^0.7.0") (d #t) (k 0) (p "near_schemafy_core")) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uriparse") (r "^0.6") (d #t) (k 0)))) (h "1pf3v1v80l8zykmiq4lmlqk0hf5lkakvfrz5ljhxl6pmbpjwm763")))

