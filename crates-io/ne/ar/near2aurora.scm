(define-module (crates-io ne ar near2aurora) #:use-module (crates-io))

(define-public crate-near2aurora-0.1.0 (c (n "near2aurora") (v "0.1.0") (d (list (d (n "ethabi") (r "^17.2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "1xzblxiyld440cdb3bf91s46895cisd4x6wx3mvbjxgg6b16drai")))

