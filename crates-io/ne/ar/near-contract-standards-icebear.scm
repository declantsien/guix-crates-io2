(define-module (crates-io ne ar near-contract-standards-icebear) #:use-module (crates-io))

(define-public crate-near-contract-standards-icebear-0.0.1 (c (n "near-contract-standards-icebear") (v "0.0.1") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v5h6shwsfl0zhwk0hnfgah4y32ynmi091br7s7zp0vm7iha7dxz") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

