(define-module (crates-io ne ar near-abi-client) #:use-module (crates-io))

(define-public crate-near-abi-client-0.1.0 (c (n "near-abi-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "near-abi-client-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "near-abi-client-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0b577jsq4329r1723gi577q9fd00c2jc45ynrzknd1zfd6hyspq8") (r "1.56.0")))

(define-public crate-near-abi-client-0.1.1 (c (n "near-abi-client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "near-abi-client-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "near-abi-client-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "14kzkp43kgjir1i94gm76m5lg9944ixdxqfw98lrhr4d5qmw16l7") (r "1.56.0")))

