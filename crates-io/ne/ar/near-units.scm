(define-module (crates-io ne ar near-units) #:use-module (crates-io))

(define-public crate-near-units-0.1.0 (c (n "near-units") (v "0.1.0") (d (list (d (n "near-units-core") (r "^0.1.0") (d #t) (k 0)) (d (n "near-units-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1xjpl0r7fidn3gv3ysqhhxdgqgari8f3j16qfjhpxk0hy58arbh9")))

(define-public crate-near-units-0.2.0 (c (n "near-units") (v "0.2.0") (d (list (d (n "near-units-core") (r "^0.2.0") (d #t) (k 0)) (d (n "near-units-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1fc83507xgbpdp5laaxi9xrz2plhhw4aslgfxs4qafax55zvg8lp")))

