(define-module (crates-io ne ar near-token) #:use-module (crates-io))

(define-public crate-near-token-0.0.1 (c (n "near-token") (v "0.0.1") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1zh83ngn3wahgiqc18cw2426xa4k6i9lasqg3f653c2dp71244jc") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.2")))

(define-public crate-near-token-0.1.0 (c (n "near-token") (v "0.1.0") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ncja6sqv77qi0ssc96j6icn16hh0nj6jbsv0ys6j7k86q104z04") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.2")))

(define-public crate-near-token-0.2.0 (c (n "near-token") (v "0.2.0") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yw32sfpi7cpfa2yx7y44h4fg08bha7gzgpx7ss757s0lbwg6s3v") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.2")))

