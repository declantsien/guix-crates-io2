(define-module (crates-io ne ar near-env) #:use-module (crates-io))

(define-public crate-near-env-0.1.0 (c (n "near-env") (v "0.1.0") (d (list (d (n "near-sdk") (r "^2.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0pby26qvckhz1h2cpivmz0s32wz1h3wj59az9hrgg85izkxbr2ag")))

(define-public crate-near-env-0.1.1 (c (n "near-env") (v "0.1.1") (d (list (d (n "near-sdk") (r "^2.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0p14y84y095y36z610rpx0134jj10driq3n3y3c2rzffwlan5svi")))

(define-public crate-near-env-0.1.2 (c (n "near-env") (v "0.1.2") (d (list (d (n "near-sdk") (r "^2.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0vkpnp4sblyxvqwq6znx8fzln4126zb3spcfc2i4hwiw62fdq6wx")))

(define-public crate-near-env-0.1.3 (c (n "near-env") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "1gc32d3z2rc87rhjk9d5104jv3qn9wfwz1d2aqh6izpdiawzjxqq")))

(define-public crate-near-env-0.1.4 (c (n "near-env") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0fyqw83vb8kd2pas07sdyv0mb25di8fjdv94msp03i6v7bhgzkd2")))

(define-public crate-near-env-0.1.5 (c (n "near-env") (v "0.1.5") (d (list (d (n "near-sdk") (r "^3.0.0-pre.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0pdfsmz85irix3syixjp9kpp7zqqbm4d15rjf35vrysskp3fv6rr")))

(define-public crate-near-env-0.1.6 (c (n "near-env") (v "0.1.6") (d (list (d (n "near-sdk") (r "^3.0.0-pre.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "128kzcsq4aakbna8xf6q6qnhj3pxc3921py9g9asm9b4p03ll856")))

(define-public crate-near-env-0.1.7 (c (n "near-env") (v "0.1.7") (d (list (d (n "near-sdk") (r "^3.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0mg917am6b1ii0gcvvxc9xqndvj9867f6bvsf61fwasxq9k70p2r")))

(define-public crate-near-env-0.1.8 (c (n "near-env") (v "0.1.8") (d (list (d (n "near-sdk") (r "^3.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0q4zcxm1ijg9gwng66nsz80h9d5mi7yhhic4pdr2a50q95zlbr4s")))

(define-public crate-near-env-0.1.9 (c (n "near-env") (v "0.1.9") (d (list (d (n "near-sdk") (r "^3.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0sixzd70fvdflq72gcgpsz95v0z8n8iqjba65r99zx7f0gi5rd38")))

(define-public crate-near-env-0.1.10 (c (n "near-env") (v "0.1.10") (d (list (d (n "near-sdk") (r "^3.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "1jdp40mxyvfm7cdbdz1qrwg7ky51wf0bajwckm0pm75l3vrflwpp")))

