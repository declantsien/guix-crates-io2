(define-module (crates-io ne ar near-actix-test-utils) #:use-module (crates-io))

(define-public crate-near-actix-test-utils-0.1.0 (c (n "near-actix-test-utils") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1pdalgd9d26c2p199h1nadmwdz6xwa659x4lniairc9aipw0pvwf")))

