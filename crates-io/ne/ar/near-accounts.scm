(define-module (crates-io ne ar near-accounts) #:use-module (crates-io))

(define-public crate-near-accounts-0.1.0-alpha (c (n "near-accounts") (v "0.1.0-alpha") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "near-crypto") (r "^0.20.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.20.1") (d #t) (k 0)) (d (n "near-providers") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "near-transactions") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)))) (h "0sd2gyhx59q6i8qzc56ykz1f5ly2f9z53zyszj2xpg80gnncv2x8")))

