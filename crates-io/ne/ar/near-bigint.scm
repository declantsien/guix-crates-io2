(define-module (crates-io ne ar near-bigint) #:use-module (crates-io))

(define-public crate-near-bigint-1.0.0 (c (n "near-bigint") (v "1.0.0") (d (list (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rstest") (r "^0.14.0") (d #t) (k 2)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "0ya2m86np6h1x4n91j01as0xrk2f8shvfbsrq8rkqaikr2l0qcd9")))

(define-public crate-near-bigint-1.0.1 (c (n "near-bigint") (v "1.0.1") (d (list (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rstest") (r "^0.14.0") (d #t) (k 2)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "1czxlxa9ngv8kg3rirrpkk21z7s1zraiqi0z9k8wxkarv12bbjg9")))

