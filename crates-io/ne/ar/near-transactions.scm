(define-module (crates-io ne ar near-transactions) #:use-module (crates-io))

(define-public crate-near-transactions-0.1.0-alpha (c (n "near-transactions") (v "0.1.0-alpha") (d (list (d (n "near-crypto") (r "^0.20.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.20.1") (d #t) (k 0)))) (h "1j5fb3ri95dr9hba8sqw6akimd6q16l80jvyjjb8dswdizyc74kw")))

