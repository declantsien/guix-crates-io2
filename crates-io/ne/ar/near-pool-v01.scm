(define-module (crates-io ne ar near-pool-v01) #:use-module (crates-io))

(define-public crate-near-pool-v01-0.1.0 (c (n "near-pool-v01") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "near-crypto-v01") (r "^0.1.0") (d #t) (k 0)) (d (n "near-primitives-v01") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0jx43b17rpq8kikk5ksyi8naancq35a2za79z9x01fc4cvj1vvhz")))

