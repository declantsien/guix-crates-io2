(define-module (crates-io ne ar near-gas) #:use-module (crates-io))

(define-public crate-near-gas-0.1.0 (c (n "near-gas") (v "0.1.0") (h "1wh1wmyljparl6kyahv3agj99mjhns0r9ybc52idkrkjljqwx96g") (r "1.68.2")))

(define-public crate-near-gas-0.1.1 (c (n "near-gas") (v "0.1.1") (h "078zxk6mh8qa694gq5hh44il5lv56pvjl8kyc37h4ik1dl30h04l") (y #t) (r "1.68.2")))

(define-public crate-near-gas-0.1.2 (c (n "near-gas") (v "0.1.2") (h "1yk850akp531819bgn8dqkcynn6h964359dslzjhj7a654xl64gx") (y #t) (r "1.68.2")))

(define-public crate-near-gas-0.1.3 (c (n "near-gas") (v "0.1.3") (h "03wf6ayxldrg3khm94cbrkjqdrbmh18dqkzmxc50bj00fmlby9db") (r "1.68.2")))

(define-public crate-near-gas-0.2.1 (c (n "near-gas") (v "0.2.1") (h "1v281sxyiasmzzn1acaqrlfsdnssgwm84wdn8rsz4nbnhj6xjz6m") (r "1.68.2")))

(define-public crate-near-gas-0.2.2 (c (n "near-gas") (v "0.2.2") (h "0bhizjzl4dm4mwpwf68qmq4im99vijc3jq8rw0l7ys2cya6ldy8h") (r "1.68.2")))

(define-public crate-near-gas-0.2.3 (c (n "near-gas") (v "0.2.3") (d (list (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0k4yliflqwgrx52xbfjj4nl1rnglr07yxk3nk23mvflx1zc85b0d") (r "1.68.2")))

(define-public crate-near-gas-0.2.4 (c (n "near-gas") (v "0.2.4") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1b5ah6vdyckvd670hak1zllzxkgxl1c6biax8jdvmkhyvdh0icwm") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.0")))

(define-public crate-near-gas-0.2.5 (c (n "near-gas") (v "0.2.5") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1lcs7qkm8a8hddsblsgc66v3fws90ic47r35s019j8i6a23mrrql") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.0")))

