(define-module (crates-io ne ar near-runtime-utils) #:use-module (crates-io))

(define-public crate-near-runtime-utils-2.2.0 (c (n "near-runtime-utils") (v "2.2.0") (h "0j3lgg3immh5zbnq99vfx5dc54hjdmr01pqzfdr09b7p40xdkm0v")))

(define-public crate-near-runtime-utils-3.0.0-pre-release (c (n "near-runtime-utils") (v "3.0.0-pre-release") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1msm4pa4hp1wc5wfmgv99wa21j32lyl9i6i8jq1j89hzrg4vj602")))

(define-public crate-near-runtime-utils-3.0.0 (c (n "near-runtime-utils") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1826kr02prac9kb5jbjy60b9bc882h1qbycrvkih6npc3j4w3xa7")))

(define-public crate-near-runtime-utils-4.0.0-pre.1 (c (n "near-runtime-utils") (v "4.0.0-pre.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01wfv73lb3lik320lhf4vx8c29lqskiy3434q6dzjk0xrb2813d4")))

