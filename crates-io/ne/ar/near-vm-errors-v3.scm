(define-module (crates-io ne ar near-vm-errors-v3) #:use-module (crates-io))

(define-public crate-near-vm-errors-v3-3.0.0 (c (n "near-vm-errors-v3") (v "3.0.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "near-account-id") (r "^0.1.0") (d #t) (k 0)) (d (n "near-rpc-error-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wcn17id7ngvc7wbdw865wy253x6q0aycfwi15qr5kgryaf9l7qb") (f (quote (("protocol_feature_alt_bn128") ("dump_errors_schema" "near-rpc-error-macro/dump_errors_schema"))))))

