(define-module (crates-io ne ar nearly) #:use-module (crates-io))

(define-public crate-nearly-0.1.0 (c (n "nearly") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1yy8z4mcbp1zcgricfl7s6xivl772cyjwjpbk639pjr9nn61cf9m") (f (quote (("std") ("default" "std"))))))

(define-public crate-nearly-0.2.0 (c (n "nearly") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "14zf3bgqzdcxvrmy8mgl5220sm6qjj54yrb6pxx23la9n2zljdjv") (f (quote (("std") ("default" "std"))))))

(define-public crate-nearly-0.3.0 (c (n "nearly") (v "0.3.0") (d (list (d (n "mockall") (r "^0.12") (d #t) (k 2)) (d (n "nearly-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1yvmn1sz0qbfkgj0021d6gmd8f6cdd3bsi2a4kswm15xw2snnmvq") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-nearly-0.4.0 (c (n "nearly") (v "0.4.0") (d (list (d (n "mockall") (r "^0.12") (d #t) (k 2)) (d (n "nearly-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "035rn864v54c8j4jjx3cq1cvgm4sl31ls82s8125r86l3ysxpgqb") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

