(define-module (crates-io ne ar near-sys) #:use-module (crates-io))

(define-public crate-near-sys-0.0.1 (c (n "near-sys") (v "0.0.1") (h "12jasvkfzqz71pwsp0v6r1zf5w0rpic2d4pzg8lr1r3shsfprwzv")))

(define-public crate-near-sys-0.1.0 (c (n "near-sys") (v "0.1.0") (h "0ji4bi3mzaxgzx5z2sqw19rlsb2n1bri8gd9v0b49i7s8qzsm9zn")))

(define-public crate-near-sys-0.2.0 (c (n "near-sys") (v "0.2.0") (h "1ly6m0d5y44xh18i22kzr6vjac8zkrivah2pm4nfvv7afqr321z3")))

(define-public crate-near-sys-0.2.1 (c (n "near-sys") (v "0.2.1") (h "0szhzndigdy0isny6d2kml5c2kxjshrbm1945jzkx3fg39cqhxir")))

