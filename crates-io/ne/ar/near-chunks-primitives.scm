(define-module (crates-io ne ar near-chunks-primitives) #:use-module (crates-io))

(define-public crate-near-chunks-primitives-0.1.0 (c (n "near-chunks-primitives") (v "0.1.0") (d (list (d (n "near-chain-primitives") (r "^0.1.0") (d #t) (k 0)))) (h "1znlri0zn2wrxx0bhr8fp7pqfcglbg7pnazsxdnmkdll44cy07sv")))

(define-public crate-near-chunks-primitives-0.5.0 (c (n "near-chunks-primitives") (v "0.5.0") (d (list (d (n "near-chain-primitives") (r "^0.5.0") (d #t) (k 0)))) (h "178prknzspf0dqp6l6b1aiciv09ksp220h8im9c2n2d54hy2xpyb")))

(define-public crate-near-chunks-primitives-0.10.0 (c (n "near-chunks-primitives") (v "0.10.0") (d (list (d (n "near-chain-primitives") (r "^0.10.0") (d #t) (k 0)))) (h "0zvjyxn6dhnwp2sy7d9i0mgdll97f9iq79h3dma72cv4dnwgzxcd")))

(define-public crate-near-chunks-primitives-0.11.0 (c (n "near-chunks-primitives") (v "0.11.0") (d (list (d (n "near-chain-primitives") (r "^0.11.0") (d #t) (k 0)))) (h "19d6a6kgzd700r9n7rwy6cmvg1nddv4fpj6dsd7i64wx2i67acv2") (r "1.56.0")))

(define-public crate-near-chunks-primitives-0.12.0 (c (n "near-chunks-primitives") (v "0.12.0") (d (list (d (n "near-chain-primitives") (r "^0.12.0") (d #t) (k 0)))) (h "1xj9z1a1xqq81lpxy857maajd2rjlvpjsvw2zh0j0if76yqhd343") (r "1.56.0")))

(define-public crate-near-chunks-primitives-0.13.0 (c (n "near-chunks-primitives") (v "0.13.0") (d (list (d (n "near-chain-primitives") (r "^0.13.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.13.0") (d #t) (k 0)))) (h "0iln5bgznj63yb6gj4hhj90ig3i7s013anpk9a2bza3528hsiq1k") (r "1.60.0")))

(define-public crate-near-chunks-primitives-0.14.0 (c (n "near-chunks-primitives") (v "0.14.0") (d (list (d (n "near-chain-primitives") (r "^0.14.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.14.0") (d #t) (k 0)))) (h "0xqljyw5ncxxk6cbk05gsqhc3jlr5bqh2hdsvwqhfwdby4i6yzy1") (r "1.60.0")))

(define-public crate-near-chunks-primitives-0.15.0 (c (n "near-chunks-primitives") (v "0.15.0") (d (list (d (n "near-chain-primitives") (r "^0.15.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.15.0") (d #t) (k 0)))) (h "1iv8x99ydmiia13m8hfv5kjw9czj0lwqirj6xycbi5sqv6cf64gj") (r "1.64.0")))

(define-public crate-near-chunks-primitives-0.16.0 (c (n "near-chunks-primitives") (v "0.16.0") (d (list (d (n "near-chain-primitives") (r "^0.16.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.16.0") (d #t) (k 0)))) (h "0lzdcwgbrx3apr8g6jnbxkq3pajayi3j61ay23f4q7vvrzf3k04s") (r "1.67.1")))

(define-public crate-near-chunks-primitives-0.16.1 (c (n "near-chunks-primitives") (v "0.16.1") (d (list (d (n "near-chain-primitives") (r "^0.16.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.16.1") (d #t) (k 0)))) (h "11i8qw6l2vvsfips9bkxbzi25qfkp2x2x2v9aai7i10n2pyim8w5") (r "1.68.0")))

(define-public crate-near-chunks-primitives-0.17.0 (c (n "near-chunks-primitives") (v "0.17.0") (d (list (d (n "near-chain-primitives") (r "^0.17.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.17.0") (d #t) (k 0)))) (h "1nifq1jz8zz31f9y61yhpy7wsh5n1a1ajjvvgmvwvkk69klbyydz") (r "1.69.0")))

(define-public crate-near-chunks-primitives-0.19.0-pre.1 (c (n "near-chunks-primitives") (v "0.19.0-pre.1") (d (list (d (n "near-chain-primitives") (r "^0.19.0-pre.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.19.0-pre.1") (d #t) (k 0)))) (h "1iz06pkgg6r76y745qk123l6p4cmi1fxnv0344s601iziv8vglwj")))

(define-public crate-near-chunks-primitives-0.19.0-pre.2 (c (n "near-chunks-primitives") (v "0.19.0-pre.2") (d (list (d (n "near-chain-primitives") (r "^0.19.0-pre.2") (d #t) (k 0)) (d (n "near-primitives") (r "^0.19.0-pre.2") (d #t) (k 0)))) (h "1i842y7yin8jjmiar5rlz008ixxin5qzb3km9wvgmlsn769kbbqv")))

(define-public crate-near-chunks-primitives-0.19.0 (c (n "near-chunks-primitives") (v "0.19.0") (d (list (d (n "near-chain-primitives") (r "^0.19.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.19.0") (d #t) (k 0)))) (h "0wf1xb81br97nmfkp6wzn0nw9i8j4wgp86ggfmai9nd5v2px177c")))

(define-public crate-near-chunks-primitives-0.20.0 (c (n "near-chunks-primitives") (v "0.20.0") (d (list (d (n "near-chain-primitives") (r "^0.20.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.20.0") (d #t) (k 0)))) (h "06kyxy245231wdvrfbmbgmsrbb12r20qiryid4b36qkwndb08fq9")))

(define-public crate-near-chunks-primitives-0.20.1 (c (n "near-chunks-primitives") (v "0.20.1") (d (list (d (n "near-chain-primitives") (r "^0.20.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.20.1") (d #t) (k 0)))) (h "0j3nxi9dp93038i0pywiyc0gjvpdfyrgzy0nqhx4mf4db4v800hf")))

(define-public crate-near-chunks-primitives-0.21.0 (c (n "near-chunks-primitives") (v "0.21.0") (d (list (d (n "near-chain-primitives") (r "^0.21.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.21.0") (d #t) (k 0)))) (h "0sgd9zygsw1lp6vvgp541qrhqv2a0jllppj570ips4c2vngnlc6j")))

(define-public crate-near-chunks-primitives-0.21.1 (c (n "near-chunks-primitives") (v "0.21.1") (d (list (d (n "near-chain-primitives") (r "^0.21.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.21.1") (d #t) (k 0)))) (h "0q326ipgwsr78v8ixcdy9i4bnkpf8yk6mfbl254ksfgb9vyq7aas")))

(define-public crate-near-chunks-primitives-0.21.2 (c (n "near-chunks-primitives") (v "0.21.2") (d (list (d (n "near-chain-primitives") (r "^0.21.2") (d #t) (k 0)) (d (n "near-primitives") (r "^0.21.2") (d #t) (k 0)))) (h "06qshydkhajy46mxa6b2an0fyg752r2qs1bfww9w2vy81scp7g45")))

(define-public crate-near-chunks-primitives-0.22.0 (c (n "near-chunks-primitives") (v "0.22.0") (d (list (d (n "near-chain-primitives") (r "^0.22.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.22.0") (d #t) (k 0)))) (h "0nfpcqpvyhjlkyx46my5ghnlf105xxnn4qh0l48fy20d0qdhnpaz")))

