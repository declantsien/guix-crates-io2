(define-module (crates-io ne ar near-fixed-bit-tree) #:use-module (crates-io))

(define-public crate-near-fixed-bit-tree-0.0.0 (c (n "near-fixed-bit-tree") (v "0.0.0") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05079hdiplfzirpy078sf2h66axqssrny5hy8zzfwwlzg1drwkh2")))

