(define-module (crates-io ne ar near-bindgen-promise) #:use-module (crates-io))

(define-public crate-near-bindgen-promise-0.3.1 (c (n "near-bindgen-promise") (v "0.3.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18m00y2zcbb4kg9xvfkxwcp954dy3nhas3609qg9ffgi883wfkms")))

(define-public crate-near-bindgen-promise-0.3.2 (c (n "near-bindgen-promise") (v "0.3.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1pflk4b2mm82ha1hndahvj09aaj91i7sykn3qsgygx36jxqqgqhg")))

(define-public crate-near-bindgen-promise-0.3.3 (c (n "near-bindgen-promise") (v "0.3.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "178444y78wwsgvb91fyfvfsm71spavb9rvgnapgcsfjv6sy98xbz")))

(define-public crate-near-bindgen-promise-0.3.4 (c (n "near-bindgen-promise") (v "0.3.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1msrc9j8b7dc4nbsrxhyd2rc4i9m3d1c6kwzb048wfdqwvadlhg0")))

(define-public crate-near-bindgen-promise-0.3.5 (c (n "near-bindgen-promise") (v "0.3.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "195vcfqbgx0vl4y26ihva3syxzbm3wi2xjlfbpmgymx0sp85wpcf")))

(define-public crate-near-bindgen-promise-0.3.6 (c (n "near-bindgen-promise") (v "0.3.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1qli07693pj6l22y42xjfaj5dzhcigh24smz4ppcv47dijif5gl0")))

(define-public crate-near-bindgen-promise-0.3.7 (c (n "near-bindgen-promise") (v "0.3.7") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10hfdi3pfwhs9k9d9jqbiy373dalw0r1m3gjx3wjvx3lpylkfzlg")))

(define-public crate-near-bindgen-promise-0.3.8 (c (n "near-bindgen-promise") (v "0.3.8") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1f06m454mjizwhvn2x7fiv8gfln54fd1jk9szb23cxxm9q0dbfvc")))

(define-public crate-near-bindgen-promise-0.3.9 (c (n "near-bindgen-promise") (v "0.3.9") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0k7f6fcnhvpvhlyjvdb1z8x9a1xwcxqfzg3i4ksf7pmhds9wd5za")))

(define-public crate-near-bindgen-promise-0.3.10 (c (n "near-bindgen-promise") (v "0.3.10") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0srjcs5x60pv614bplqsarjh5sjg35jp8ycaf3mp4623i64ds5ki")))

(define-public crate-near-bindgen-promise-0.3.11 (c (n "near-bindgen-promise") (v "0.3.11") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ni1fq13g6c637h86mhwfqb8mbhbbzq9sizn7q9m7wxxm7jr6cq9")))

