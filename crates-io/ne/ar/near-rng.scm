(define-module (crates-io ne ar near-rng) #:use-module (crates-io))

(define-public crate-near-rng-0.1.0 (c (n "near-rng") (v "0.1.0") (h "00rcyacx7jq4mkvrm8gsnzjs448l8p6299ymfp41c02m594sc0fn") (y #t)))

(define-public crate-near-rng-0.1.1 (c (n "near-rng") (v "0.1.1") (h "04isymp98qbzx4x70ip021gq2m380l38900gappwby9fz5pkgz8y")))

