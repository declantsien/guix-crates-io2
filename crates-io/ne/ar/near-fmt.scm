(define-module (crates-io ne ar near-fmt) #:use-module (crates-io))

(define-public crate-near-fmt-0.16.1 (c (n "near-fmt") (v "0.16.1") (d (list (d (n "near-primitives-core") (r "^0.16.1") (d #t) (k 0)))) (h "07bjkm7059r2kn3n6y5575844lma5hfl4gwzr8zl2pfp5yg6s7in") (r "1.69.0")))

(define-public crate-near-fmt-0.17.0 (c (n "near-fmt") (v "0.17.0") (d (list (d (n "near-primitives-core") (r "^0.17.0") (d #t) (k 0)))) (h "0wh6a505vk7k58lmqpfy1j922pwsc784rkfchz1vingwdhn88k64") (r "1.69.0")))

(define-public crate-near-fmt-0.17.1 (c (n "near-fmt") (v "0.17.1") (d (list (d (n "near-primitives-core") (r "^0.17.1") (d #t) (k 0)))) (h "11wz3q6ag9s4qhf83qh12gc4392sap0xg3pfn74ibhz6gsk6s90j") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly")))) (y #t) (r "1.71.0")))

(define-public crate-near-fmt-0.18.0 (c (n "near-fmt") (v "0.18.0") (d (list (d (n "near-primitives-core") (r "^0.18.0") (d #t) (k 0)))) (h "14s2hhdsk2jdmi2z7d71jz5f3z7f3gk66q93kwyhhfv9jkn8kqyr") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly")))) (r "1.71.0")))

(define-public crate-near-fmt-0.19.0-pre.1 (c (n "near-fmt") (v "0.19.0-pre.1") (d (list (d (n "near-primitives-core") (r "^0.19.0-pre.1") (d #t) (k 0)))) (h "1niafqxxhxxk6614jyh9lms2grxw55xwz0d5nbbclclq7kzv8pvj") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.19.0-pre.2 (c (n "near-fmt") (v "0.19.0-pre.2") (d (list (d (n "near-primitives-core") (r "^0.19.0-pre.2") (d #t) (k 0)))) (h "0jrx2f2vnkhgvnwjrif4pwnixbw81wlryxxv8v5sdra8jgqz431g") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.19.0 (c (n "near-fmt") (v "0.19.0") (d (list (d (n "near-primitives-core") (r "^0.19.0") (d #t) (k 0)))) (h "09zdngss6gb1xn4zcsh6g9p2wpdcc9hb9arj9pv86hif1dj2nz35") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.20.0 (c (n "near-fmt") (v "0.20.0") (d (list (d (n "near-primitives-core") (r "^0.20.0") (d #t) (k 0)))) (h "1s9k9n0x1rvv1vlvim6givdn3g9dxsbpm9kgibxfdi38diq2gi8j") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.20.1 (c (n "near-fmt") (v "0.20.1") (d (list (d (n "near-primitives-core") (r "^0.20.1") (d #t) (k 0)))) (h "11ljp56rc8fy23anjdxp6v8cg0m7sn54kclri1h02h70q7grindp") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.21.0 (c (n "near-fmt") (v "0.21.0") (d (list (d (n "near-primitives-core") (r "^0.21.0") (d #t) (k 0)))) (h "01mvld2llvadqq588gih2lk53jy918kcfpixrq2j53bgrcfq7x6r") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.21.1 (c (n "near-fmt") (v "0.21.1") (d (list (d (n "near-primitives-core") (r "^0.21.1") (d #t) (k 0)))) (h "0ci77g011hqc4l02p9w7wpqj70m0248618zsnf0x9adp19n56x4d") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.21.2 (c (n "near-fmt") (v "0.21.2") (d (list (d (n "near-primitives-core") (r "^0.21.2") (d #t) (k 0)))) (h "0n056m88m4d710z1hy7r2k81rdf5h201njs9vxwxpvjf05iai6ls") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

(define-public crate-near-fmt-0.22.0 (c (n "near-fmt") (v "0.22.0") (d (list (d (n "near-primitives-core") (r "^0.22.0") (d #t) (k 0)))) (h "0ggbz4zxpka1s7yjg50na7a11555mgbk44bn06d4csbgzcv6cjsr") (f (quote (("nightly_protocol" "near-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "near-primitives-core/nightly"))))))

