(define-module (crates-io ne ar near-logger-utils) #:use-module (crates-io))

(define-public crate-near-logger-utils-0.1.0 (c (n "near-logger-utils") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.4") (d #t) (k 0)))) (h "085f4g6mvrm42nkmb1yyrjsy6f2mji9za7dsm4w8szpkzxf6qg1f")))

