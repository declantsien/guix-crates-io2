(define-module (crates-io ne ar near-vm-types) #:use-module (crates-io))

(define-public crate-near-vm-types-0.16.1 (c (n "near-vm-types") (v "0.16.1") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0x996rdy25vl5vn62m3mcjhlqsjdrr2w815jznpz8jl1pmya82ki") (f (quote (("std") ("default" "std")))) (r "1.68.2")))

(define-public crate-near-vm-types-0.17.0 (c (n "near-vm-types") (v "0.17.0") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qxzih4h7v8gd6vjhdh1nvkfxjqrbb2s7kk62a2fzfgavwzwigxg") (r "1.69.0")))

(define-public crate-near-vm-types-0.17.1 (c (n "near-vm-types") (v "0.17.1") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pikgrbnpg2gwmcrchbycpy9wlky8jhr3qwfycc9qvnzr0j8jccb") (y #t) (r "1.71.0")))

(define-public crate-near-vm-types-0.18.0 (c (n "near-vm-types") (v "0.18.0") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1f6x7mbyq90ahvqq7r7mc2jgbhw4dn2mzy9snz5nkw1chvmixn6a") (r "1.71.0")))

(define-public crate-near-vm-types-0.19.0-pre.1 (c (n "near-vm-types") (v "0.19.0-pre.1") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yfz8294qil920jxxfkzx2cb8w34hqahqi3yq98a9hxya3dwwhyv")))

(define-public crate-near-vm-types-0.19.0-pre.2 (c (n "near-vm-types") (v "0.19.0-pre.2") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qkg9z0h5yia0mp4fxrlfn6xq34qi5ws5sv8gan892g8pzkrbi39")))

(define-public crate-near-vm-types-0.19.0 (c (n "near-vm-types") (v "0.19.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zgczfxapkw75yp15xmhkcjnaa8kszx50qvnp5bnkld6mdligd6c")))

(define-public crate-near-vm-types-0.20.0 (c (n "near-vm-types") (v "0.20.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cclniq2iqccngbnv2l6v5fqpmr2k4mmb1s8363klwc2sx80ijak")))

(define-public crate-near-vm-types-0.20.1 (c (n "near-vm-types") (v "0.20.1") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05asqr18j9csqh3hcy9v546ygssckrvf71hm1yb5ipphi2cv1h8y")))

(define-public crate-near-vm-types-0.21.0 (c (n "near-vm-types") (v "0.21.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kxrfch0kbkdhqjb06l62s8rdm9j3rmbwcznf3mxxz1hsmxxdbj0")))

(define-public crate-near-vm-types-0.21.1 (c (n "near-vm-types") (v "0.21.1") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0md3xlpczlcy059xk2000ajznci4jsfvf8xyjl6w94j2qhc6ggf1")))

(define-public crate-near-vm-types-0.21.2 (c (n "near-vm-types") (v "0.21.2") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "149l4mdjvwlsp22hwg3xlid7p9szr5k8gbhhix7fvafjhnh99s75")))

(define-public crate-near-vm-types-0.22.0 (c (n "near-vm-types") (v "0.22.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "085sjzhy0zmfa9kj1k294hkw9d8q7jzczx0g06s0559lc6jdqn9d")))

