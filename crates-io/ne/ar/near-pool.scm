(define-module (crates-io ne ar near-pool) #:use-module (crates-io))

(define-public crate-near-pool-0.1.0-pre.1 (c (n "near-pool") (v "0.1.0-pre.1") (d (list (d (n "borsh") (r "^0.8.1") (d #t) (k 0)) (d (n "near-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "near-primitives") (r "^0.1.0-pre.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1kfw8nd8c5i9hyr8la86k2xvy80cwpwpjpxn6dbngh6gyxr99rxx")))

