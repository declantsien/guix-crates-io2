(define-module (crates-io ne ar near-contract-tools) #:use-module (crates-io))

(define-public crate-near-contract-tools-0.1.0 (c (n "near-contract-tools") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.0.0-pre.8") (d #t) (k 0)))) (h "0jsvlf57y8qx2l5kbfdq8jvgy7ai34svxp5wgcnq6zs74289zcfb")))

(define-public crate-near-contract-tools-0.2.0 (c (n "near-contract-tools") (v "0.2.0") (d (list (d (n "near-sdk") (r "^4.0.0-pre.8") (d #t) (k 0)))) (h "0id4w52akc186id95v1nm6vn19qhwg1041arzq0ivryiz4dd1zbl")))

(define-public crate-near-contract-tools-0.3.0 (c (n "near-contract-tools") (v "0.3.0") (d (list (d (n "near-contract-tools-macros") (r "=0.3.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0-pre.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1hmdnknpl09sv14hrsqva4hjcwqnhh30bh91yydrby9242g02idd")))

(define-public crate-near-contract-tools-0.3.1 (c (n "near-contract-tools") (v "0.3.1") (d (list (d (n "near-contract-tools-macros") (r "=0.3.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1ija49n671dcbnv7zdjrf59058qd0lwawrz69dz1p2wz0z4ks7kx")))

(define-public crate-near-contract-tools-0.3.2 (c (n "near-contract-tools") (v "0.3.2") (d (list (d (n "near-contract-tools-macros") (r "=0.3.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "12pf1hbvj2bar8b0jqs69y22c5k4gm2f4j7s6bi59yj13i8xwck3")))

(define-public crate-near-contract-tools-0.3.3 (c (n "near-contract-tools") (v "0.3.3") (d (list (d (n "near-contract-tools-macros") (r "=0.3.3") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "14hrr0c09bkqwrqjzzpzm7xfhj9wwibymm5q0mngy275yhdj15q6")))

(define-public crate-near-contract-tools-0.3.4 (c (n "near-contract-tools") (v "0.3.4") (d (list (d (n "near-contract-tools-macros") (r "=0.3.4") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1ik8gjs879ak9fc4zj1a8b7fgv5ya119wa211v0460qwjkb71nc9")))

(define-public crate-near-contract-tools-0.3.5 (c (n "near-contract-tools") (v "0.3.5") (d (list (d (n "near-contract-tools-macros") (r "=0.3.5") (d #t) (k 0)) (d (n "near-sdk") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jk7s8vi5fnbakcrfy7i6cz3ziwzxgxgmbmcmmw7a0jn8hyz574y")))

(define-public crate-near-contract-tools-0.4.0 (c (n "near-contract-tools") (v "0.4.0") (d (list (d (n "near-contract-tools-macros") (r "=0.4.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1z1v7zpvzwl903kg874h6vgfzd5fdkyqi8h0g9wrbkc4x6z4f30h")))

(define-public crate-near-contract-tools-0.5.0 (c (n "near-contract-tools") (v "0.5.0") (d (list (d (n "near-contract-tools-macros") (r "=0.5.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1h7khfmgqln6xkc5m4gs5kchnkflnr26a6pi4jmpd7ckm6znk6q3")))

(define-public crate-near-contract-tools-0.6.0 (c (n "near-contract-tools") (v "0.6.0") (d (list (d (n "near-contract-tools-macros") (r "=0.6.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "12hra6nwc0pjyb5ngbmsmnzz2sddy08jl7a1hbiw7ngs8xfb6npr")))

(define-public crate-near-contract-tools-0.6.1 (c (n "near-contract-tools") (v "0.6.1") (d (list (d (n "near-contract-tools-macros") (r "=0.6.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "18isbpfvrkxagsrifff91f7kqilw0f2gmhp58jy1rib4kz95f6q1")))

(define-public crate-near-contract-tools-0.7.0 (c (n "near-contract-tools") (v "0.7.0") (d (list (d (n "near-contract-tools-macros") (r "=0.7.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0") (k 0)) (d (n "near-sdk") (r "^4.1.0") (f (quote ("unit-testing" "legacy"))) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "16fh5bqjbbgxds7769qb9hrpy9d3ndch7nnhn9c2m7gqb2li37hl") (f (quote (("unstable" "near-sdk/unstable"))))))

(define-public crate-near-contract-tools-0.7.1 (c (n "near-contract-tools") (v "0.7.1") (d (list (d (n "near-contract-tools-macros") (r "=0.7.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0") (k 0)) (d (n "near-sdk") (r "^4.1.0") (f (quote ("unit-testing" "legacy"))) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0lqc5vi92s568gr5g3s8vqzdapfr124g9nkfns3178csv683nzjy") (f (quote (("unstable" "near-sdk/unstable"))))))

(define-public crate-near-contract-tools-0.7.2 (c (n "near-contract-tools") (v "0.7.2") (d (list (d (n "near-contract-tools-macros") (r "=0.7.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0") (k 0)) (d (n "near-sdk") (r "^4.1.0") (f (quote ("unit-testing" "legacy"))) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0f40y0ds2gvbbypn31nq99smq9vl7j90zs42b46l3mkma2bbn348") (f (quote (("unstable" "near-sdk/unstable"))))))

