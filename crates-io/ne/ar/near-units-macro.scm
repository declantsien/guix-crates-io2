(define-module (crates-io ne ar near-units-macro) #:use-module (crates-io))

(define-public crate-near-units-macro-0.1.0 (c (n "near-units-macro") (v "0.1.0") (d (list (d (n "near-units-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.78") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1bi77kpq6wghyy0js7d6nm863ansfyhkn6p0xam79z0hkxfpajrv")))

(define-public crate-near-units-macro-0.2.0 (c (n "near-units-macro") (v "0.2.0") (d (list (d (n "near-units-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "06hp4f2k1y4blynn5y9nvlp8jz64i2myj8awppwlc212cv84baw9")))

