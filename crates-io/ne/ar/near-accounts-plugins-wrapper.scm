(define-module (crates-io ne ar near-accounts-plugins-wrapper) #:use-module (crates-io))

(define-public crate-near-accounts-plugins-wrapper-0.0.1 (c (n "near-accounts-plugins-wrapper") (v "0.0.1") (d (list (d (n "near-contract-standards") (r "^4.0.0-pre.2") (d #t) (k 2)) (d (n "near-sdk") (r "^4.0.0-pre.2") (d #t) (k 2)) (d (n "near-sdk-sim") (r "^4.0.0-pre.2") (d #t) (k 2)))) (h "09qb5d4gzdp4h25vzn6p7qdfxa5h1pfk77pcay47wlmaqc0qxpz2")))

