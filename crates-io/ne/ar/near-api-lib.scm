(define-module (crates-io ne ar near-api-lib) #:use-module (crates-io))

(define-public crate-near-api-lib-0.1.0-alpha (c (n "near-api-lib") (v "0.1.0-alpha") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "near-accounts") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "near-crypto") (r "^0.20.1") (d #t) (k 0)) (d (n "near-primitives") (r "^0.20.1") (d #t) (k 0)) (d (n "near-providers") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lpfv410jbmdmjg6bglsm2xc6c5qdz2xlmdin4h8z63k2c44b8rf")))

