(define-module (crates-io ne ar near-account) #:use-module (crates-io))

(define-public crate-near-account-0.1.0 (c (n "near-account") (v "0.1.0") (d (list (d (n "near-contract-standards") (r "^4.0.0-pre.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0-pre.2") (d #t) (k 0)))) (h "02q4m76g44r7kgrzki1b0n4rs15vxbpd4xv3v7wh7drcg9d38pf8")))

(define-public crate-near-account-0.1.1 (c (n "near-account") (v "0.1.1") (d (list (d (n "near-contract-standards") (r "^4.0.0-pre.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0-pre.2") (d #t) (k 0)))) (h "01zjafq7sc9kshgjx7xs021axj9xkvhhw1a110h4vm24djhhbhjq")))

(define-public crate-near-account-0.1.2 (c (n "near-account") (v "0.1.2") (d (list (d (n "near-contract-standards") (r "^4.0.0-pre.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0-pre.2") (d #t) (k 0)))) (h "1wvbxrlz9li8cjp8vrgvzzgf355rskr6amfdsp9p3mdn3i90fixm")))

