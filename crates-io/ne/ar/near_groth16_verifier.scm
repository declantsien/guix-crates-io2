(define-module (crates-io ne ar near_groth16_verifier) #:use-module (crates-io))

(define-public crate-near_groth16_verifier-1.0.0 (c (n "near_groth16_verifier") (v "1.0.0") (d (list (d (n "near-bigint") (r "^1.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "rstest") (r "^0.14.0") (d #t) (k 2)))) (h "0xsacfnd04gjhw1hbz57m1mx2i2z9jf209va0gjz3zbwj0dwii37")))

(define-public crate-near_groth16_verifier-1.0.1 (c (n "near_groth16_verifier") (v "1.0.1") (d (list (d (n "near-bigint") (r "^1.0.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "rstest") (r "^0.14.0") (d #t) (k 2)))) (h "1yvp1gh1a593c9nzr2z16a1vwgcg3lz3jzlm0xhdcb9l9cgzz06s")))

