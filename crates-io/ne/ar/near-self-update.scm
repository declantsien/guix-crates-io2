(define-module (crates-io ne ar near-self-update) #:use-module (crates-io))

(define-public crate-near-self-update-0.1.0 (c (n "near-self-update") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "near-workspaces") (r "^0.10.0") (d #t) (k 0)) (d (n "nitka") (r "^0.2.0") (d #t) (k 0)))) (h "0j8p2f6rmc8w0j9zxajpk6pvd7dz00z60b22k58b8syz5sicfsjx")))

(define-public crate-near-self-update-0.1.1 (c (n "near-self-update") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "near-workspaces") (r "^0.10.0") (d #t) (k 0)) (d (n "nitka") (r "^0.2.0") (d #t) (k 0)))) (h "17s62abn9f1vghkaf4gbdb4fw9r1nicz1lk26v83wqkmrdi13qyr")))

(define-public crate-near-self-update-0.1.2 (c (n "near-self-update") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "near-workspaces") (r "^0.10.0") (d #t) (k 0)) (d (n "nitka") (r "^0.2.0") (d #t) (k 0)))) (h "0lng6ay7x076r5w3rvkarxhy9slby3nidyzz746hvin285n9fbdi")))

