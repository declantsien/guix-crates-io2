(define-module (crates-io ne ar near-safe-cell) #:use-module (crates-io))

(define-public crate-near-safe-cell-0.1.0 (c (n "near-safe-cell") (v "0.1.0") (h "1nvizh7sm2mydr0qxq2jvszy3s85x06z3sizf1vz9cgk2vjkvyjk")))

(define-public crate-near-safe-cell-0.1.1 (c (n "near-safe-cell") (v "0.1.1") (h "06s94623gdh8f59wnbzy4wkbil6a8i2s8l92mbwq5ldra2rr56zz")))

(define-public crate-near-safe-cell-0.1.2 (c (n "near-safe-cell") (v "0.1.2") (h "12rd2f9gxjvj0nnq10j554g7ld6rhcmkvyzfkylhb0xmlpvc8kla")))

(define-public crate-near-safe-cell-0.1.3 (c (n "near-safe-cell") (v "0.1.3") (h "0nqcay14hqn1q7y5xqs9rzjm3gy4yabmmw825c8zxvxy9n33j9r6")))

(define-public crate-near-safe-cell-0.1.4 (c (n "near-safe-cell") (v "0.1.4") (h "092v25dfmzj3j4hafa8x2xky6xsv5agbyqsj449bqfk8r5k0d04i")))

(define-public crate-near-safe-cell-0.1.5 (c (n "near-safe-cell") (v "0.1.5") (h "1s803ldfv5sp51cphgiiaxdf7mad150wz4a92qqq9z7i6aslqj15")))

