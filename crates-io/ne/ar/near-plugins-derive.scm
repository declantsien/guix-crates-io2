(define-module (crates-io ne ar near-plugins-derive) #:use-module (crates-io))

(define-public crate-near-plugins-derive-0.1.0 (c (n "near-plugins-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full"))) (d #t) (k 0)))) (h "0csf9rvwfqkqfd2i47448p0gz3ybfy44p2yip49lljbp5vlnk7d3")))

