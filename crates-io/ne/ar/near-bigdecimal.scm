(define-module (crates-io ne ar near-bigdecimal) #:use-module (crates-io))

(define-public crate-near-bigdecimal-0.1.0 (c (n "near-bigdecimal") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "uint") (r "^0.9.3") (k 0)))) (h "1wwxfk7wyncljp9cxrrdffdzmns5bq3d80nbmyd2dh1cbc2r9ck5")))

(define-public crate-near-bigdecimal-0.1.1 (c (n "near-bigdecimal") (v "0.1.1") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "uint") (r "^0.9.3") (k 0)))) (h "02lmdnz30bgi6q548s4mcrc0qayg3nc0199l8ir9f4i93wkbn8is")))

