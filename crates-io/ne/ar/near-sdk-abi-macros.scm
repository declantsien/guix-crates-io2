(define-module (crates-io ne ar near-sdk-abi-macros) #:use-module (crates-io))

(define-public crate-near-sdk-abi-macros-0.1.0 (c (n "near-sdk-abi-macros") (v "0.1.0") (d (list (d (n "near-sdk-abi-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dybkrkrj0f61fryx83n5q9l1mg2z0anhqm9fd85mw15fg8kcm82")))

