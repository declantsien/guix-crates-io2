(define-module (crates-io ne ar nearly_eq) #:use-module (crates-io))

(define-public crate-nearly_eq-0.1.0 (c (n "nearly_eq") (v "0.1.0") (h "0v4aqw15kj23bzs66i13g5cbh0c0wnaf0cfqqiv68qxb5l4s8dqx")))

(define-public crate-nearly_eq-0.1.1 (c (n "nearly_eq") (v "0.1.1") (h "16bpflg8ybphbncnj3garrqy7nrcnr9j71100y5l6as96y3gjqdl")))

(define-public crate-nearly_eq-0.1.2 (c (n "nearly_eq") (v "0.1.2") (d (list (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "0hqp7rlk62l5a5vgj7m01kbz019i70jv4vdrmnga8kff1ynvzx2j")))

(define-public crate-nearly_eq-0.1.3 (c (n "nearly_eq") (v "0.1.3") (d (list (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "1aiy79ci0jd6229dmyijmpm4wqn8h5w01acl9w8zywiq50iaz5ky")))

(define-public crate-nearly_eq-0.2.0 (c (n "nearly_eq") (v "0.2.0") (d (list (d (n "ndarray") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "0v3iipv17zi2m2j5dxkmxhd2qgx8cz0gg8n6pa680wwvzx4x04xd") (f (quote (("nightly") ("i128" "nightly") ("docs" "num-complex" "ndarray" "i128"))))))

(define-public crate-nearly_eq-0.2.1 (c (n "nearly_eq") (v "0.2.1") (d (list (d (n "ndarray") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-integer") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "05n523rh0wxbafx89vhdd56m8jwadppndg3h1fyfgsgqdz64hffc") (f (quote (("rational" "num-rational" "num-integer" "num-traits") ("i128") ("docs" "num-complex" "rational" "ndarray" "i128"))))))

(define-public crate-nearly_eq-0.2.2 (c (n "nearly_eq") (v "0.2.2") (d (list (d (n "ndarray") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-integer") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "1d7zlgvn09bk1q2mp0ihskmpxda40gilzx3yiyy6z7n0l88qlvm9") (f (quote (("rational" "num-rational" "num-integer" "num-traits") ("i128") ("docs" "num-complex" "rational" "ndarray" "i128"))))))

(define-public crate-nearly_eq-0.2.3 (c (n "nearly_eq") (v "0.2.3") (d (list (d (n "ndarray") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-integer") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r ">= 0.0.0") (o #t) (d #t) (k 0)))) (h "1di6k2n8fmwmsns0snqbp8crvhy0602b9msqvwgghz7lf233wnlb") (f (quote (("rational" "num-rational" "num-integer" "num-traits") ("i128") ("docs" "complex" "rational" "ndarray" "i128") ("complex" "num-complex"))))))

(define-public crate-nearly_eq-0.2.4 (c (n "nearly_eq") (v "0.2.4") (d (list (d (n "fpa") (r ">= 0.1.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-integer") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r ">= 0.0.0") (o #t) (d #t) (k 0)) (d (n "typenum") (r ">= 1.10.0") (o #t) (d #t) (k 0)))) (h "0dan5ihg9g590zjy87f7wd1a0s4acjs1y7jfcmfw6a1k8f58cad6") (f (quote (("use_fpa" "fpa" "typenum" "num-traits") ("rational" "num-rational" "num-integer" "num-traits") ("i128") ("docs" "complex" "rational" "ndarray" "use_fpa" "i128") ("complex" "num-complex"))))))

