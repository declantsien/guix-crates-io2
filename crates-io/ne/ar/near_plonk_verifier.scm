(define-module (crates-io ne ar near_plonk_verifier) #:use-module (crates-io))

(define-public crate-near_plonk_verifier-1.0.0 (c (n "near_plonk_verifier") (v "1.0.0") (d (list (d (n "near-bigint") (r "^1.0.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "rstest") (r "^0.14.0") (d #t) (k 2)))) (h "1np4wif6gmkw59j7rnwaphyxkcc11kzylmwjxz5rj4apzvs7y1pd")))

