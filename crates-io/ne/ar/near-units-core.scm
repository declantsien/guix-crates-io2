(define-module (crates-io ne ar near-units-core) #:use-module (crates-io))

(define-public crate-near-units-core-0.1.0 (c (n "near-units-core") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "189lh02i04mr1h27psq27hbx1wn9f8c5ziz051sdap9vp9zx1xn8")))

(define-public crate-near-units-core-0.2.0 (c (n "near-units-core") (v "0.2.0") (d (list (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1ikb720zkpma57hfcms2f0g903qgs3cana2zlf6a11yyhmwjmal9")))

