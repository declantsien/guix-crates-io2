(define-module (crates-io ne ar near-runtime-fees) #:use-module (crates-io))

(define-public crate-near-runtime-fees-0.2.0 (c (n "near-runtime-fees") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13d3iiigh2kbcrd3gr2ja2k5cicy9znniwbnpsr78nwkbk3kn40w")))

(define-public crate-near-runtime-fees-0.2.1 (c (n "near-runtime-fees") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fqr472c4z6n95bgpmv3n2ljs9xmjzjrwfb061yfjjbql56zwp45")))

(define-public crate-near-runtime-fees-0.2.5 (c (n "near-runtime-fees") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bh9phmaamzcyc8lyilr6kwzbvfydy701l0p06x2g2dkrv0cjzib")))

(define-public crate-near-runtime-fees-0.3.0 (c (n "near-runtime-fees") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p6dk5vl888wy8my60jz3bfd7c4r5p291d75164s24cbfx31kl5x")))

(define-public crate-near-runtime-fees-0.3.1 (c (n "near-runtime-fees") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08pgn9vdkzaklrbryya23ixhxcjn3lpy13aiix2xwwm8yfjbc8by")))

(define-public crate-near-runtime-fees-0.3.2 (c (n "near-runtime-fees") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yi52xyvq82ynh8gx8sfkyyxsdk620cyy9riybhh2bs695wfa7hh")))

(define-public crate-near-runtime-fees-0.4.1 (c (n "near-runtime-fees") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "010gqqpvzqxf1x0fn86yrn7dffhf25hc1i1dvb37clag4wqrggsw")))

(define-public crate-near-runtime-fees-0.4.2 (c (n "near-runtime-fees") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ql74rfaccsalnml9kjgsjzrxibhan298vh50g5i5bh9d16affxq")))

(define-public crate-near-runtime-fees-0.4.3 (c (n "near-runtime-fees") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k04zj779x1x9ib6raw6xglhi9zsm935qnklghvgn42h7cxjm5ml")))

(define-public crate-near-runtime-fees-0.4.4 (c (n "near-runtime-fees") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qd3jvy5sxz0mwplb7w34839xav21sbkym6yfd03593dz2bg60r7")))

(define-public crate-near-runtime-fees-0.4.5 (c (n "near-runtime-fees") (v "0.4.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0666vdcn1l2mg71b8zdi8xw13rg677rac74ifsqq3sjh5m25zajf")))

(define-public crate-near-runtime-fees-0.5.0 (c (n "near-runtime-fees") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0daqc5djk190fscnwm8l14gvw2rqp8ps4qlgs0fwiqkicn8yj7ln")))

(define-public crate-near-runtime-fees-0.6.0 (c (n "near-runtime-fees") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gdd2150z44by46s9x76kwksvlcq79ic9cwsz9s2di5kaznn19pv")))

(define-public crate-near-runtime-fees-0.6.1 (c (n "near-runtime-fees") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gc700620py1v2qvyh5glf3b3nssq7a9j0z6smmr7sg06z8adg1d")))

(define-public crate-near-runtime-fees-0.7.0 (c (n "near-runtime-fees") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fs0vimkb45f0rqfskg5naxxpbsjb9xi4j8ijypiik97ibv6smd9")))

(define-public crate-near-runtime-fees-0.7.1 (c (n "near-runtime-fees") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c0aq5j05qyl6w4s3wdvwbxm88w7gighbnl7kmych62m4mr0mwzd")))

(define-public crate-near-runtime-fees-0.8.0 (c (n "near-runtime-fees") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c6vdzjl0q6chdpd79ph83ygfpm02yn6x8sdvczb4xg6imvzl923")))

(define-public crate-near-runtime-fees-0.9.0 (c (n "near-runtime-fees") (v "0.9.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fm58highn1sip6zjna5aa2cnjz06z0xqi5cwglbhwwl620kzn33")))

(define-public crate-near-runtime-fees-0.9.1 (c (n "near-runtime-fees") (v "0.9.1") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wdn3gzymy31nmwwfhr2x256irdwmwx5swa62bx37jwa9hkr4jcg")))

(define-public crate-near-runtime-fees-1.0.0 (c (n "near-runtime-fees") (v "1.0.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17bnmg5wy4434884svgp2lassjq4vr8yzjdns4ffxy8kx30izlpr")))

(define-public crate-near-runtime-fees-1.1.0 (c (n "near-runtime-fees") (v "1.1.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fh0xgl43ydaawnb03fk6hi4pwfnhfhb27m099cvyizhwscx0rpx")))

(define-public crate-near-runtime-fees-1.2.0 (c (n "near-runtime-fees") (v "1.2.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xj9m2s6bzh15796hv6v0fynj3pg89s4v5ah31d4yglxj7nfq8jp")))

(define-public crate-near-runtime-fees-2.0.0 (c (n "near-runtime-fees") (v "2.0.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c15pxmpmqzxl4y4hc7znab6j715j48pgyasffp6jrmxh5vbl4cd")))

(define-public crate-near-runtime-fees-2.2.0 (c (n "near-runtime-fees") (v "2.2.0") (d (list (d (n "num-rational") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bi02z5zszwvp2kkq9b6vm0lgr8hblsj5v8fw22ncd3alrk28wz8")))

