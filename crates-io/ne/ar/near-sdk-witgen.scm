(define-module (crates-io ne ar near-sdk-witgen) #:use-module (crates-io))

(define-public crate-near-sdk-witgen-0.0.1 (c (n "near-sdk-witgen") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1j2522qnmv3j0hrf3576gw00xw13vnqzax046yhqabsi27iyg6jh")))

(define-public crate-near-sdk-witgen-0.0.2 (c (n "near-sdk-witgen") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "03zhyk1si6agg0gldlw19w2c792w3lc1r2gfaca177k8bi15n90f")))

