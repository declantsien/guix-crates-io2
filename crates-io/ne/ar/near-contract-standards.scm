(define-module (crates-io ne ar near-contract-standards) #:use-module (crates-io))

(define-public crate-near-contract-standards-0.1.0-pre.1 (c (n "near-contract-standards") (v "0.1.0-pre.1") (d (list (d (n "near-sdk") (r "=3.0.0-pre.2") (d #t) (k 0)))) (h "0fdgxwcjs0rh3lh03y7vpw5hnl8spx8915xmv1nhf2ng1vq6h5xg")))

(define-public crate-near-contract-standards-0.1.0-pre.2 (c (n "near-contract-standards") (v "0.1.0-pre.2") (d (list (d (n "near-sdk") (r "=3.0.0-pre.2") (d #t) (k 0)))) (h "11ib4zwawvzhkvm6gispfvqql9vrcb63h7k9yzk0psh25plzmk8z")))

(define-public crate-near-contract-standards-3.0.1-pre.1 (c (n "near-contract-standards") (v "3.0.1-pre.1") (d (list (d (n "near-sdk") (r "=3.0.1-pre.1") (d #t) (k 0)))) (h "0pz429va3xv5chnrcg1iqmsadk3k0gwvwwbr0dhr11858pk9dvkj")))

(define-public crate-near-contract-standards-3.0.1 (c (n "near-contract-standards") (v "3.0.1") (d (list (d (n "near-sdk") (r "=3.0.1") (d #t) (k 0)))) (h "0bwiv71jvxx3shnpvkj1hsird8cvyv6020xvwbdaxaf7nqqbminl")))

(define-public crate-near-contract-standards-3.1.0 (c (n "near-contract-standards") (v "3.1.0") (d (list (d (n "near-sdk") (r "=3.1.0") (d #t) (k 0)))) (h "13jqk0jg4lnpx1a8ppj94k1ygn8yhnrqci8hjx7xrvlf4zypz0ym")))

(define-public crate-near-contract-standards-3.1.1 (c (n "near-contract-standards") (v "3.1.1") (d (list (d (n "near-sdk") (r "=3.1.0") (d #t) (k 0)))) (h "11fk3ng616ra03a5rhwl0vgkqijb1kbpggaw7cnswbras1rjgjh2")))

(define-public crate-near-contract-standards-3.2.0 (c (n "near-contract-standards") (v "3.2.0") (d (list (d (n "near-sdk") (r "=3.1.0") (d #t) (k 0)))) (h "1lz41m7mx9bgv2fiv0gr4xvxrfzgg8dagwqcgarmgsvsbh51shp7")))

(define-public crate-near-contract-standards-4.0.0-pre.1 (c (n "near-contract-standards") (v "4.0.0-pre.1") (d (list (d (n "near-sdk") (r "=4.0.0-pre.1") (d #t) (k 0)))) (h "0agmr2d13b1iv2f6icf72zjq0w3qi6vwgdgb0c5a04ys6jzkx2ib")))

(define-public crate-near-contract-standards-4.0.0-pre.2 (c (n "near-contract-standards") (v "4.0.0-pre.2") (d (list (d (n "near-sdk") (r "=4.0.0-pre.2") (d #t) (k 0)))) (h "0pz6j0i5sx04wvhq18rr0bkr4m6rc1rmpjw3yqnkm6ws3rqdzgmh")))

(define-public crate-near-contract-standards-4.0.0-pre.3 (c (n "near-contract-standards") (v "4.0.0-pre.3") (d (list (d (n "near-sdk") (r "=4.0.0-pre.3") (d #t) (k 0)))) (h "0spj986v039wckfr4df8a44fbqrfhiwi1gih9nmhk79iw1njvcab")))

(define-public crate-near-contract-standards-4.0.0-pre.4 (c (n "near-contract-standards") (v "4.0.0-pre.4") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "0wkqfdz3nsp77gll7l44sb2pgw23hf7kd31c3v51dykjvvs8hwsm")))

(define-public crate-near-contract-standards-4.0.0-pre.5 (c (n "near-contract-standards") (v "4.0.0-pre.5") (d (list (d (n "near-sdk") (r "=4.0.0-pre.5") (d #t) (k 0)))) (h "0lb84rx2h8zyff09x2mchq8hldzn27yx8zl22wi2r26q05242i28")))

(define-public crate-near-contract-standards-4.0.0-pre.6 (c (n "near-contract-standards") (v "4.0.0-pre.6") (d (list (d (n "near-sdk") (r "=4.0.0-pre.6") (d #t) (k 0)))) (h "0ilcjlyla6wpd2c9sbn7azlq2a1vpdhrwpfp9rl3ympd509ab4dj")))

(define-public crate-near-contract-standards-4.0.0-pre.7 (c (n "near-contract-standards") (v "4.0.0-pre.7") (d (list (d (n "near-sdk") (r "=4.0.0-pre.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17s5w2hm5wi1f1h4xsprzk4ccr2ci52n9jqhjqwxkyxzmlvgdi2j")))

(define-public crate-near-contract-standards-4.0.0-pre.8 (c (n "near-contract-standards") (v "4.0.0-pre.8") (d (list (d (n "near-sdk") (r "=4.0.0-pre.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03m65iy9w6qxr176b7ih5hn13h3c6jmhg8rgacjjd49a7a7s4zm4")))

(define-public crate-near-contract-standards-4.0.0-pre.9 (c (n "near-contract-standards") (v "4.0.0-pre.9") (d (list (d (n "near-sdk") (r "=4.0.0-pre.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hyw0j3qfmzhjg9bzljll71rj178kvqqh8463x4z398gnkfls084")))

(define-public crate-near-contract-standards-4.0.0 (c (n "near-contract-standards") (v "4.0.0") (d (list (d (n "near-sdk") (r "=4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rvi7z21v6r8p0j544ql8wa9xmwgvhkl846dlgv0z00qmnmccrk4")))

(define-public crate-near-contract-standards-4.1.0-pre.0 (c (n "near-contract-standards") (v "4.1.0-pre.0") (d (list (d (n "near-sdk") (r "=4.1.0-pre.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03lnh3k0rbw8k83pij0dpa4zkha6q9n851qxmzpg6k98qda25pqf") (f (quote (("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-4.1.0-pre.1 (c (n "near-contract-standards") (v "4.1.0-pre.1") (d (list (d (n "near-sdk") (r "=4.1.0-pre.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07nfviswrchrh5mszzn2wb6lh7hbkxpgn71bipc16ys82kl25v1b") (f (quote (("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-4.1.0-pre.2 (c (n "near-contract-standards") (v "4.1.0-pre.2") (d (list (d (n "near-sdk") (r "=4.1.0-pre.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bky1c473kagl5vpdsiajllnx2wysnw9w51fbj5qy3qwv732a1nc") (f (quote (("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-4.1.0-pre.3 (c (n "near-contract-standards") (v "4.1.0-pre.3") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1994kyjqq0j3bmyw6xvishnix398d3baf98kzilgwn0mzvsdz7ki") (f (quote (("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-4.1.0 (c (n "near-contract-standards") (v "4.1.0") (d (list (d (n "near-sdk") (r "=4.1.0") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1n91wib7n9xgrifbz7rkjm92hr6fydnkd9lcc50rwkc8yfb97qwv") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-4.1.1 (c (n "near-contract-standards") (v "4.1.1") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dfbkysq5dp0y6m0smyic04zkdllj9l23yxdjwklf9lvwwrckb3v") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-5.0.0-alpha.1 (c (n "near-contract-standards") (v "5.0.0-alpha.1") (d (list (d (n "near-account-id") (r "^1.0.0-alpha.3") (f (quote ("abi" "serde" "borsh" "schemars"))) (d #t) (k 0)) (d (n "near-sdk") (r "~5.0.0-alpha.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0098gnagj91c9dz2jiwwpmmjpvj3535xwbw9aczfifk0acl0ifw2") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-5.0.0-alpha.2 (c (n "near-contract-standards") (v "5.0.0-alpha.2") (d (list (d (n "near-sdk") (r "~5.0.0-alpha.2") (f (quote ("legacy"))) (k 0)))) (h "1ai2z9pg1qh69aysfcj6q8x6h6hkqs83jazqjvvxq8087mnv0f2d") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-5.0.0-alpha.3 (c (n "near-contract-standards") (v "5.0.0-alpha.3") (d (list (d (n "near-sdk") (r "~5.0.0-alpha.3") (f (quote ("legacy"))) (k 0)))) (h "0yl0sxrivgzw3pn4r4p45r7d3jf8gmjz3f70gmnn5lp39llpjb1v") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-5.0.0 (c (n "near-contract-standards") (v "5.0.0") (d (list (d (n "near-sdk") (r "~5.0.0") (f (quote ("legacy"))) (k 0)))) (h "0nqng8w0rx50jj5fninffdxdrifmsmfzx6pb0yrnzs8rv2aafq3d") (f (quote (("default") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-5.1.0 (c (n "near-contract-standards") (v "5.1.0") (d (list (d (n "near-sdk") (r "~5.1.0") (f (quote ("legacy"))) (k 0)))) (h "0974n67p3bgm1cqrf4fnpsi350vbi88a4d97g5pyg6iq9phqzrx0") (f (quote (("default") ("abi" "near-sdk/abi"))))))

