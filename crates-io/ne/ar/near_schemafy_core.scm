(define-module (crates-io ne ar near_schemafy_core) #:use-module (crates-io))

(define-public crate-near_schemafy_core-0.7.0 (c (n "near_schemafy_core") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xnayw2p2hvzncls81dvpw58z35zkm98k4rjff3mf6d317wa3ms2")))

