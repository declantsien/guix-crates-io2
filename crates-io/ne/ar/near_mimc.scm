(define-module (crates-io ne ar near_mimc) #:use-module (crates-io))

(define-public crate-near_mimc-1.0.0 (c (n "near_mimc") (v "1.0.0") (d (list (d (n "ff_wasm_unknown_unknown") (r "^0.12.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "near-bigint") (r "^1.0.1") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)))) (h "0h86cax9xjnasi8klbbabzlw9la2vqppnnzbrcfvz8hc1hxlg30c")))

