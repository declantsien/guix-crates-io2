(define-module (crates-io ne ar near-lake-parent-transaction-cache) #:use-module (crates-io))

(define-public crate-near-lake-parent-transaction-cache-0.8.0-beta.2 (c (n "near-lake-parent-transaction-cache") (v "0.8.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "cached") (r "^0.43.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "near-lake-framework") (r "^0.8.0-beta.2") (d #t) (k 0)))) (h "1dnm28593livfhf5yy2crnrgb6x23qr2fymjb8zyqn3gdf8p02v6")))

(define-public crate-near-lake-parent-transaction-cache-0.8.0-beta.3 (c (n "near-lake-parent-transaction-cache") (v "0.8.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "cached") (r "^0.43.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "near-lake-framework") (r "^0.8.0-beta.2") (d #t) (k 0)))) (h "0jdvszjiqnkvbwjwmab4xh4zijxg2avwmg4797h812bza3k7rmh8")))

