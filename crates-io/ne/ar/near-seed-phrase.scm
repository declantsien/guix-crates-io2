(define-module (crates-io ne ar near-seed-phrase) #:use-module (crates-io))

(define-public crate-near-seed-phrase-0.1.0 (c (n "near-seed-phrase") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("rand"))) (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)))) (h "0ghw57zmld3ibirg50kba8ivcxdk2i8kpp1mkmjicfk40xpbmv62")))

(define-public crate-near-seed-phrase-0.2.0 (c (n "near-seed-phrase") (v "0.2.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("rand"))) (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0simwpdcj5hphq4hqr5phjml96bzdx6ag3zlypsdr80056d10cdy")))

(define-public crate-near-seed-phrase-0.3.0 (c (n "near-seed-phrase") (v "0.3.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1lx7n0ldjyzsk4pavmyb4kiw1nxyzir3s73lmm79ss7acxg65a1l")))

(define-public crate-near-seed-phrase-0.3.1 (c (n "near-seed-phrase") (v "0.3.1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "15jnihyz6gr81jcy3hpdwmx3g11985vlx1nnxzidfvlwchig0g33")))

(define-public crate-near-seed-phrase-0.3.2 (c (n "near-seed-phrase") (v "0.3.2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1i1zlax61x4428pz41xfvckwdy0254b983h2wqd4zcccywj6dg3r")))

(define-public crate-near-seed-phrase-0.3.3 (c (n "near-seed-phrase") (v "0.3.3") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "04wy7c5znh6ik1s2sxqkggz9c8ifvsgqqvwm5ih7pqkr3qzyq5s4")))

