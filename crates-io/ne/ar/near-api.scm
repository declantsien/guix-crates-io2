(define-module (crates-io ne ar near-api) #:use-module (crates-io))

(define-public crate-near-api-0.1.0 (c (n "near-api") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "near-jsonrpc-primitives") (r "^0.2.0") (d #t) (k 0)) (d (n "near-primitives-v01") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mpi4z41n7vsygs4460ka9d6gfnpc92l65g7p561clgwxbsy943h")))

