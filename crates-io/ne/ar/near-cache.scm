(define-module (crates-io ne ar near-cache) #:use-module (crates-io))

(define-public crate-near-cache-0.16.1 (c (n "near-cache") (v "0.16.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0dggmkr827arjxi81fchi8scdl010zvmr9c0z2pc45c61mcrg9cz") (r "1.68.0")))

(define-public crate-near-cache-0.17.0 (c (n "near-cache") (v "0.17.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0frl4nwn4z6w2mnr7wk9ww47nm1h6qbk0waqnqpb4fsxyy8gr4qv") (r "1.69.0")))

(define-public crate-near-cache-0.17.1 (c (n "near-cache") (v "0.17.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07z23m35pkvv6x62a26v7xicbin694621ip2qz0rvvbnxc13azss") (y #t) (r "1.71.0")))

(define-public crate-near-cache-0.18.0 (c (n "near-cache") (v "0.18.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0l6syagyj4d2x9drc5jsbixjck68qnx2par941dj907kx04ryypa") (r "1.71.0")))

(define-public crate-near-cache-0.19.0-pre.1 (c (n "near-cache") (v "0.19.0-pre.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bmzf6i2scj00dhrsk1n33l8crsx91wcspjvgzjv7nfdnmq291g2")))

(define-public crate-near-cache-0.19.0-pre.2 (c (n "near-cache") (v "0.19.0-pre.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0b2y8dfna1yyf7brx4d9kiz73y5mzsih3vhcb4fdq3qxl939d43z")))

(define-public crate-near-cache-0.19.0 (c (n "near-cache") (v "0.19.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "078382ab74v4ida0a1im4isincmzqwpcvk2aprp1ihqb1cr373jm")))

(define-public crate-near-cache-0.20.0 (c (n "near-cache") (v "0.20.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18jk2zx5avc93j3jky2lspnqk4jzp14096sas8b4q5qmfi0gf84k")))

(define-public crate-near-cache-0.20.1 (c (n "near-cache") (v "0.20.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v6xzgdrqg16kxbx6axpx6gjbnrw5gbr8919l5s3b6vfq56cgjmp")))

(define-public crate-near-cache-0.21.0 (c (n "near-cache") (v "0.21.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1272llx38z0752az7f40vm54ky2i81xwphxxcyh7jjripp4bb0ki")))

(define-public crate-near-cache-0.21.1 (c (n "near-cache") (v "0.21.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00cyd51s1zxvnmfr7f8cl3w63fywpr4x1z3bzz3v31zpfbiwpmg1")))

(define-public crate-near-cache-0.21.2 (c (n "near-cache") (v "0.21.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mw3zaqivvy77n8m9b2a7sry52yf0qw73kph4sb898ysp3z2l8hb")))

(define-public crate-near-cache-0.22.0 (c (n "near-cache") (v "0.22.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1q6y4cs958s8bs7wixgs8n0663k1d2nd4vnshrn67jblmgm0abba")))

