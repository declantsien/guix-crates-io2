(define-module (crates-io ne ar near-config-utils) #:use-module (crates-io))

(define-public crate-near-config-utils-0.15.0 (c (n "near-config-utils") (v "0.15.0") (d (list (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)))) (h "0rwz959dsjdjxahshjgxi8761n68fyzjkzak9ys9n2lngldjvlsb") (r "1.66.1")))

(define-public crate-near-config-utils-0.16.0 (c (n "near-config-utils") (v "0.16.0") (d (list (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)))) (h "0c7ysfnwj145ri1n0yxyb9bxrhz2znwgk2jx00v091zzqjnrfgwq") (r "1.67.1")))

(define-public crate-near-config-utils-0.16.1 (c (n "near-config-utils") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "01imvy0xp9aqy0ciazynv4viz2hn7wgmwb72948zzh3x8yynnyqk") (r "1.68.0")))

(define-public crate-near-config-utils-0.17.0 (c (n "near-config-utils") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0cbhy6nrwy1ldas887wv5y2p3i1yjh6i1cqy4k1mpi4kwkfff8sm") (r "1.69.0")))

(define-public crate-near-config-utils-0.17.1 (c (n "near-config-utils") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1dvvf1k7iyy91lqcv7xwk9hp24qw2ikvaxffbz7aa3c6sjf26axb") (y #t) (r "1.71.0")))

(define-public crate-near-config-utils-0.18.0 (c (n "near-config-utils") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1292hvskjaqhaj7mh44m52xj4psl732y6v7q5dy9vvn39zx41330") (r "1.71.0")))

(define-public crate-near-config-utils-0.19.0-pre.1 (c (n "near-config-utils") (v "0.19.0-pre.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "189lcgabz6xd5yjq9rrxiczdmamknwqw40l6k1hdfrd1gz1kddl6")))

(define-public crate-near-config-utils-0.19.0-pre.2 (c (n "near-config-utils") (v "0.19.0-pre.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0cyl602j86fz8icghrxi7wpvj4f2f1p6xqf2si39hpqgl8fa58lz")))

(define-public crate-near-config-utils-0.19.0 (c (n "near-config-utils") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0kbdmbcsc54mrzxxkbvjk4fnqib2kzz97hf34qx8p6v1bqxk974v")))

(define-public crate-near-config-utils-0.20.0 (c (n "near-config-utils") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "043vx0r1c0cg2n167cl1bjaq2mx67c14fdmxdrjqi9a9alp4q4ci")))

(define-public crate-near-config-utils-0.20.1 (c (n "near-config-utils") (v "0.20.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0n3ysb3c04hxb53ka1h37bwif82w6sgz0vjvlpkrnnjl3nmymq9a")))

(define-public crate-near-config-utils-0.21.0 (c (n "near-config-utils") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "02k6kchbaavh60x715rqyzn22asqgxyg5ds4dkssb9sk6qlz0m54")))

(define-public crate-near-config-utils-0.21.1 (c (n "near-config-utils") (v "0.21.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0wvdmrsiiwa43b8by7h3lqsxasgyjfg1hjilnl9yh7fky0ffhg34")))

(define-public crate-near-config-utils-0.21.2 (c (n "near-config-utils") (v "0.21.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0sj9gvgkp9psmvz53bp8yjnyi4zf3nbk9yj1sdwcga7g37sry75f")))

(define-public crate-near-config-utils-0.22.0 (c (n "near-config-utils") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "04zmfzdynnia17kfa8r5jg2vv4hppan0iyvr2n7gsy79bzkb0dd2")))

