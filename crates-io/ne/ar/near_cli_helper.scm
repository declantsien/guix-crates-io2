(define-module (crates-io ne ar near_cli_helper) #:use-module (crates-io))

(define-public crate-near_cli_helper-0.1.0 (c (n "near_cli_helper") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.0.0-pre.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xg06ngx8nzipx7c6cw06jrsbr0knynp91ca2hzl3ggszvrqv82c")))

