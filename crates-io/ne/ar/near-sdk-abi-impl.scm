(define-module (crates-io ne ar near-sdk-abi-impl) #:use-module (crates-io))

(define-public crate-near-sdk-abi-impl-0.1.0 (c (n "near-sdk-abi-impl") (v "0.1.0") (d (list (d (n "near-abi") (r "^0.3.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1800wih2gzwc3bsqnzd7vf2aivdgci4gzz7nj0rz4bqab8nj9dn3")))

