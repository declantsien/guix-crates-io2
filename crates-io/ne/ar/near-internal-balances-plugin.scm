(define-module (crates-io ne ar near-internal-balances-plugin) #:use-module (crates-io))

(define-public crate-near-internal-balances-plugin-0.1.0 (c (n "near-internal-balances-plugin") (v "0.1.0") (d (list (d (n "near-account") (r "^0.1.2") (d #t) (k 0)) (d (n "near-contract-standards") (r "^4.0.0-pre.2") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0-pre.2") (d #t) (k 0)))) (h "10vkq4x8b52jqq97yj2f66b8prqavfv5pq4k6y4fwafmv6gdjyl8")))

