(define-module (crates-io ne ar near-async-derive) #:use-module (crates-io))

(define-public crate-near-async-derive-0.20.1 (c (n "near-async-derive") (v "0.20.1") (d (list (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "02h67zd0n74wkbnn906q0xk49hv0bqjsja1qpghxmxw7kbjzfhd5")))

