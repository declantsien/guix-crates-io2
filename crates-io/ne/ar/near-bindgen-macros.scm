(define-module (crates-io ne ar near-bindgen-macros) #:use-module (crates-io))

(define-public crate-near-bindgen-macros-0.1.0 (c (n "near-bindgen-macros") (v "0.1.0") (d (list (d (n "near-bindgen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0hj6lf09jf80dfr54bwawljfjza5h50ffhjcq8gbsj9v7bix9nb2")))

(define-public crate-near-bindgen-macros-0.2.0 (c (n "near-bindgen-macros") (v "0.2.0") (d (list (d (n "near-bindgen-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0hwajxb3rj9cvnvwk3m7h8pkbxv7lhn5w4cbbklf2bs4fy0ys8nw")))

(define-public crate-near-bindgen-macros-0.2.1 (c (n "near-bindgen-macros") (v "0.2.1") (d (list (d (n "near-bindgen-core") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "15c55ip33xhxj7grynbp9vwi4q76cgaclhb0jz69l3d2jsbyrvda")))

(define-public crate-near-bindgen-macros-0.2.2 (c (n "near-bindgen-macros") (v "0.2.2") (d (list (d (n "near-bindgen-core") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "19v7gd7vnzwck40gx7svwz7xx4cshlcprfz4adp8ngnx373ab91m")))

(define-public crate-near-bindgen-macros-0.2.3 (c (n "near-bindgen-macros") (v "0.2.3") (d (list (d (n "near-bindgen-core") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "15k031g53bxvq5m5didh7s4n449y6hy45sz8x6rinc7dzsvgxd94")))

(define-public crate-near-bindgen-macros-0.2.4 (c (n "near-bindgen-macros") (v "0.2.4") (d (list (d (n "near-bindgen-core") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "12niv63fp1asxn2gv8ddvnlzgiw7gzm7n0gf8kgpwq4ijllnmh92")))

(define-public crate-near-bindgen-macros-0.3.1 (c (n "near-bindgen-macros") (v "0.3.1") (d (list (d (n "near-bindgen-core") (r "^0.3.1") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0jnvv1fziq1h3jy47c98jrjksdylvp3wvp9hidp9847ca0i3jzby")))

(define-public crate-near-bindgen-macros-0.3.2 (c (n "near-bindgen-macros") (v "0.3.2") (d (list (d (n "near-bindgen-core") (r "^0.3.2") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "045gi37g8d9cyffmrk1fzpjb19damx8x7dzmklzsasnzx61w1bgv")))

(define-public crate-near-bindgen-macros-0.3.3 (c (n "near-bindgen-macros") (v "0.3.3") (d (list (d (n "near-bindgen-core") (r "^0.3.3") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0fr2q0h6563vh5cggg10msikwpfrwds4rp0zwzf9n34p377bd7d1")))

(define-public crate-near-bindgen-macros-0.3.4 (c (n "near-bindgen-macros") (v "0.3.4") (d (list (d (n "near-bindgen-core") (r "^0.3.4") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "13bxkcnxjqd48n3vc1k7nm885in1kwllpp480nyvp03j6vrik6ff")))

(define-public crate-near-bindgen-macros-0.3.5 (c (n "near-bindgen-macros") (v "0.3.5") (d (list (d (n "near-bindgen-core") (r "^0.3.5") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1l6d5yh5hvya5ixkpk3n8fm8b2v1qw3kr08g52dkx03yzmw4ba45")))

(define-public crate-near-bindgen-macros-0.3.6 (c (n "near-bindgen-macros") (v "0.3.6") (d (list (d (n "near-bindgen-core") (r "^0.3.6") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1rlqwkdl3y5hkin1mq9ydj27qvb3a69rjg8nj682viyc8psdac57")))

(define-public crate-near-bindgen-macros-0.3.7 (c (n "near-bindgen-macros") (v "0.3.7") (d (list (d (n "near-bindgen-core") (r "^0.3.7") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "12l0j138j3p23ym49ycq9w7ya3x5nq0gp1i0fdqam9vjz0rlnrnr")))

(define-public crate-near-bindgen-macros-0.3.8 (c (n "near-bindgen-macros") (v "0.3.8") (d (list (d (n "near-bindgen-core") (r "^0.3.8") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0dsjbrhmk896swxwrz8mnzy657v1mw980gzkxj96jdf9rcnczfrl")))

(define-public crate-near-bindgen-macros-0.3.9 (c (n "near-bindgen-macros") (v "0.3.9") (d (list (d (n "near-bindgen-core") (r "^0.3.9") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1yvkdpbv2b7y85yydsgajbxp8xryli62lbmlmmg8458jnc2pcc8q")))

(define-public crate-near-bindgen-macros-0.3.10 (c (n "near-bindgen-macros") (v "0.3.10") (d (list (d (n "near-bindgen-core") (r "^0.3.10") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1g20ks2l3m3pkjb0aq498mwb7rwa9ffg12r4hb33rrf85qz6d0cy")))

(define-public crate-near-bindgen-macros-0.3.11 (c (n "near-bindgen-macros") (v "0.3.11") (d (list (d (n "near-bindgen-core") (r "^0.3.10") (d #t) (k 0)) (d (n "near-bindgen-promise") (r "^0.3.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0db0nhpqr5vi6gmp5hv3py7907hc3pvhf2lg2b72kd5rf3ir2ny3")))

(define-public crate-near-bindgen-macros-0.4.0 (c (n "near-bindgen-macros") (v "0.4.0") (d (list (d (n "near-bindgen-core") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ir39vxzhkr232yny0jn980swxgp8yfghqggc0n84glavxj12gi1")))

(define-public crate-near-bindgen-macros-0.4.1 (c (n "near-bindgen-macros") (v "0.4.1") (d (list (d (n "near-bindgen-core") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1l0sk433vxqm85hny23hiphjabvzcsqhrc6dl7g547dgp7hlgw9v")))

(define-public crate-near-bindgen-macros-0.4.2 (c (n "near-bindgen-macros") (v "0.4.2") (d (list (d (n "near-bindgen-core") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1akmilk099877gn9cpvhqrsb200j750llxaadghqagnkngjvxpis")))

(define-public crate-near-bindgen-macros-0.5.0 (c (n "near-bindgen-macros") (v "0.5.0") (d (list (d (n "near-bindgen-core") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "025dg98jq9li7ydl2fk6wb88qd4lsgxz8jzmd1hg5p9cjb7ncvzn")))

(define-public crate-near-bindgen-macros-0.6.0 (c (n "near-bindgen-macros") (v "0.6.0") (d (list (d (n "near-bindgen-core") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "1va5zl4bhvhsqi4ha0xilcyn5znni3fp7wb0d1m5sk3jhbr77zxr")))

