(define-module (crates-io ne ar near-non-transferrable-token) #:use-module (crates-io))

(define-public crate-near-non-transferrable-token-0.0.0 (c (n "near-non-transferrable-token") (v "0.0.0") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wd1b79n67p85csc9nnbrfhcn40ab9gqpvcwgrkvsjdjhsa0i8yi")))

(define-public crate-near-non-transferrable-token-0.0.1 (c (n "near-non-transferrable-token") (v "0.0.1") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qssi8s77qqai5ajq54y1yzkxy379h0bg352g3x0jbp2pm1839cq")))

(define-public crate-near-non-transferrable-token-0.0.2 (c (n "near-non-transferrable-token") (v "0.0.2") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0si9v6xb9yfhk2ql9b6clq6gpjqhjznf0zz9zxg4z3zds5y3nv15")))

(define-public crate-near-non-transferrable-token-0.0.3 (c (n "near-non-transferrable-token") (v "0.0.3") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b1bkql8sk3s5kvcfs66j9qwkp8sgmqhwlak7qn7p0gsl9l8f2b1")))

(define-public crate-near-non-transferrable-token-0.0.4 (c (n "near-non-transferrable-token") (v "0.0.4") (d (list (d (n "near-sdk") (r "=4.1.0-pre.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ln8m3xcvf2nbimfqz23h26qf7f7zymznhxx52f18qrm15sqxy12")))

