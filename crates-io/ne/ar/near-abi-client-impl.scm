(define-module (crates-io ne ar near-abi-client-impl) #:use-module (crates-io))

(define-public crate-near-abi-client-impl-0.1.0 (c (n "near-abi-client-impl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "near-abi") (r "^0.3.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18szsqcrbdgdhlihbvgy1inmf9saxf09rvdayjvi7r5s653qnmnr")))

(define-public crate-near-abi-client-impl-0.1.1 (c (n "near-abi-client-impl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "near-abi") (r "^0.4.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14cfi86pdsjykndbwyzln6sdxxnbrrxvc067acffvn0gyskfhf8i")))

