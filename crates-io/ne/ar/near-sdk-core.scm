(define-module (crates-io ne ar near-sdk-core) #:use-module (crates-io))

(define-public crate-near-sdk-core-0.6.0 (c (n "near-sdk-core") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1l8k5d06pfnd5i6gn017ckpf8l899cbv3hihx1bw1zwkjkk4ykpr")))

(define-public crate-near-sdk-core-0.6.1 (c (n "near-sdk-core") (v "0.6.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1nrfkz3c0llfckfv4z9scmxp7b6m6c4jqmvr87dlj3d058z09pns")))

(define-public crate-near-sdk-core-0.6.2 (c (n "near-sdk-core") (v "0.6.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1ps1as4m3knshqgz457s1dpw5zysp7f0ymwqn5msmxbikylxc3ll")))

(define-public crate-near-sdk-core-0.6.3 (c (n "near-sdk-core") (v "0.6.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "01frm8c0n0ks70z9fr8hma4ciqc6x9jl2jbiygpdhdd8dlwfl18s")))

(define-public crate-near-sdk-core-0.7.0 (c (n "near-sdk-core") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0qxqsv1wc4d1q4vp2pql0iza99lbamrh77w2ak0gdk7sx1lhl7vi")))

(define-public crate-near-sdk-core-0.8.0 (c (n "near-sdk-core") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "096b70ssk98csqglv9n1n9v0qx8i95ch882q281vs9c29jhq4dki")))

(define-public crate-near-sdk-core-0.9.0 (c (n "near-sdk-core") (v "0.9.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "16xz89k6m7zrzmc4nyxw8hjd7q82r0fzgzpp40piqv0v54n69vjg")))

(define-public crate-near-sdk-core-0.9.1 (c (n "near-sdk-core") (v "0.9.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0xb6534i78d59bqxgia0hi06hym42gsic7swmq5qhx1414fvbrbq")))

(define-public crate-near-sdk-core-0.9.2 (c (n "near-sdk-core") (v "0.9.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0pf6kn707kf6jxq0i3av8q508gbgrrcq94iz8z38p5sjjdb7pyc8")))

(define-public crate-near-sdk-core-0.10.0 (c (n "near-sdk-core") (v "0.10.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1qcnsjlnnf3lswcq5fpi6ikrhvzmqm406958cxw6583pmbh9ndaw")))

(define-public crate-near-sdk-core-0.11.0 (c (n "near-sdk-core") (v "0.11.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1yvgx6fd0vv94n53nmwrjb1izim6c9lnvq1n2dzmgrk15by6fdqz")))

(define-public crate-near-sdk-core-1.0.0 (c (n "near-sdk-core") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0mkp2nbjsdgmpbg5gwnf3s5spavrj3lb7j1spwkp0ls7p7fdqfif")))

(define-public crate-near-sdk-core-1.0.1 (c (n "near-sdk-core") (v "1.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1i5pbg2gmk509sqfq3di7gviwg6mwarvagq7g5v3d84fbgd86iws")))

(define-public crate-near-sdk-core-2.0.0 (c (n "near-sdk-core") (v "2.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1dbfx5vww2jvqlzl81jszs83qxwwg5jhsyx6mg5admxm20rralhm")))

(define-public crate-near-sdk-core-2.0.1 (c (n "near-sdk-core") (v "2.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "04j0ys9r12n2pbs21h26wl81hgx1gfrari9wg2wqnnszhd5ql18x")))

(define-public crate-near-sdk-core-3.0.0-pre-release (c (n "near-sdk-core") (v "3.0.0-pre-release") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "089cmr5cc8p3a3cj7jkqm3b2jlyfdck8522bpm6lfrl215b9pzqj")))

(define-public crate-near-sdk-core-3.0.0-pre.1 (c (n "near-sdk-core") (v "3.0.0-pre.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1iy8gy5xxcnqa104959q3i9ybf5vmxsf2p79k9qgvwlxmsqwncy3")))

(define-public crate-near-sdk-core-3.0.0-pre.2 (c (n "near-sdk-core") (v "3.0.0-pre.2") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "03fzg784v88z3drsn2c07yzskm8yxw5zjfjyjib3rhy7gz7g39kw")))

(define-public crate-near-sdk-core-3.0.1-pre.1 (c (n "near-sdk-core") (v "3.0.1-pre.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0mpd6s1bknbwvs328vldxkxd643gd1gi0gg7m8xj37hkzmciac3y")))

(define-public crate-near-sdk-core-3.0.1 (c (n "near-sdk-core") (v "3.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "024izvv0nswflk1xiibd28nc26ddk10wkp3yxs5yy5s9bj4147nn")))

(define-public crate-near-sdk-core-3.1.0 (c (n "near-sdk-core") (v "3.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1kib1zfd3pz7jmq3iw7i3na4q0fplqih1yk20hrminlfxgcphji8")))

