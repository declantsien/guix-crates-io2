(define-module (crates-io ne ar near-sdk-abi) #:use-module (crates-io))

(define-public crate-near-sdk-abi-0.1.0 (c (n "near-sdk-abi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "near-abi") (r "^0.3.0") (d #t) (k 0)) (d (n "near-sdk-abi-impl") (r "^0.1") (d #t) (k 0)) (d (n "near-sdk-abi-macros") (r "^0.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1736mrjxks9g73ggy6m7vg73icy8qmh7shczjw4j6dj5s1x64a3j") (r "1.56.0")))

