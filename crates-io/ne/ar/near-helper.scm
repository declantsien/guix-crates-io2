(define-module (crates-io ne ar near-helper) #:use-module (crates-io))

(define-public crate-near-helper-0.1.0 (c (n "near-helper") (v "0.1.0") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "0an8v3whdc3j3dh6xl9f2c8zrnpwyhhgcfklnmm11fbk80w7d9ak") (y #t)))

(define-public crate-near-helper-0.1.1 (c (n "near-helper") (v "0.1.1") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "10vlyj3asx836lmf0c6yz1paga9zcsnvh2b2jjm1xhawd8sp2cbm")))

(define-public crate-near-helper-0.1.2 (c (n "near-helper") (v "0.1.2") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "0nyz608l2c3wjyax7w2sjb3g8ga0z3197wkgjzrykmy9ykycpbir")))

(define-public crate-near-helper-0.2.0 (c (n "near-helper") (v "0.2.0") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "0841c8gvv69xld4gaqbgm49ww02qxkmvlnwj7gks7sja57inab9i") (y #t)))

(define-public crate-near-helper-0.2.1 (c (n "near-helper") (v "0.2.1") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "14bfr3ydx00wxv97v722myhk9j5iaak3hh3bn9sh1py9asr6l8sv")))

(define-public crate-near-helper-0.3.0 (c (n "near-helper") (v "0.3.0") (d (list (d (n "near-sdk") (r "=4.0.0-pre.4") (d #t) (k 0)))) (h "00iyj8mdi43ibpjss3f7m6l4l1yhm4sydjvlpaz70mb97pv92jix")))

(define-public crate-near-helper-0.4.0 (c (n "near-helper") (v "0.4.0") (h "1fvh3bc4zqxllli01f1rkyz4z8ihmgigyx5dr435nclrk0amsx18") (y #t)))

(define-public crate-near-helper-0.4.1 (c (n "near-helper") (v "0.4.1") (h "0kgb4d5msmnkma6irfkcqh31b21ipkj0x58d3zf7cgn3k5yvhap3")))

(define-public crate-near-helper-0.5.0 (c (n "near-helper") (v "0.5.0") (h "107a6dnyda49jsv9367mi96865jja4j3pbbpx98pgar068yzzz36")))

