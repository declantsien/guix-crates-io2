(define-module (crates-io ne ar near-contract-standards-sbt) #:use-module (crates-io))

(define-public crate-near-contract-standards-sbt-4.1.1 (c (n "near-contract-standards-sbt") (v "4.1.1") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jiv45rx2h4hj929a6jaby5w02c5zk8z4qrvn043c85b7vlywlqd") (f (quote (("default" "abi") ("abi" "near-sdk/abi")))) (y #t)))

(define-public crate-near-contract-standards-sbt-4.1.2 (c (n "near-contract-standards-sbt") (v "4.1.2") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i7c1hih5qf71d00ia9z7l05cc1d2r21p38bkfxpjwgrixf28ja0") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-sbt-4.1.3 (c (n "near-contract-standards-sbt") (v "4.1.3") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sqkp3i6jybq053s5zi23mr0knv7kk43gsg8jgdxqd6gy7iqzdc1") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-sbt-4.1.4 (c (n "near-contract-standards-sbt") (v "4.1.4") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nfcvf9z3lza9mv18z3jmvxln677b9v7vcqmri0i161qkf0gwdl7") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-sbt-4.1.5 (c (n "near-contract-standards-sbt") (v "4.1.5") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gki5f2flpqxchmw04pbyv8k02mjvn0n8qmbjxwa1xh2qjqp8gdm") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

(define-public crate-near-contract-standards-sbt-4.1.6 (c (n "near-contract-standards-sbt") (v "4.1.6") (d (list (d (n "near-sdk") (r "~4.1.1") (f (quote ("legacy"))) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "061rrw5wqlhxqk6mj8b67d9img0s9w9527drfpcxyabmcpi5rz2h") (f (quote (("default" "abi") ("abi" "near-sdk/abi"))))))

