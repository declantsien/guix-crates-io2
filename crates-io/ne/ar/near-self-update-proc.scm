(define-module (crates-io ne ar near-self-update-proc) #:use-module (crates-io))

(define-public crate-near-self-update-proc-0.0.1 (c (n "near-self-update-proc") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14m0ifq8zqyhzmxzcibmm912vjhysdbzw6gmz62vwbzkwvpnhizb")))

(define-public crate-near-self-update-proc-0.1.1 (c (n "near-self-update-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00rxbayb2y116x2zr29xpc0qck3djw7b5iw98m7qywkhrfnmx4m3")))

(define-public crate-near-self-update-proc-0.1.2 (c (n "near-self-update-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k8vnw41fksv1crrahcg5gschgxsaamjzn04am3409s2xd20v4zp")))

