(define-module (crates-io ne pt neptune-cli) #:use-module (crates-io))

(define-public crate-neptune-cli-0.1.0 (c (n "neptune-cli") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "neptune-lang") (r "^0.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "1gjkwc5qyh5sl2w80ych8ih8116hzy10qx338psdzw3npm6ngh19")))

(define-public crate-neptune-cli-0.1.1 (c (n "neptune-cli") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "neptune-lang") (r "^0.1.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "1qnvzjygw5nxas65b85rwz9laakf6l6dkgni55p3vsg7cca89dqk")))

(define-public crate-neptune-cli-0.1.2 (c (n "neptune-cli") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "neptune-lang") (r "^0.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "0zydaan877fvrnxs0q3kamhs8glp751igjr102d39p8bmgcqymh9")))

