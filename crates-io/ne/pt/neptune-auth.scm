(define-module (crates-io ne pt neptune-auth) #:use-module (crates-io))

(define-public crate-neptune-auth-0.1.0 (c (n "neptune-auth") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gi7nz1p7wz6wh3prp3vb6dsik2mmqjpgbkdj6dwsh5v8pq0kb95")))

(define-public crate-neptune-auth-0.1.1 (c (n "neptune-auth") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zi4jygdcbbwmmkfm1qxh83nl943qr3bg0c2g1ggr166xsyhlgjs")))

(define-public crate-neptune-auth-1.0.0 (c (n "neptune-auth") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1grdy5b4z296zq8yl07r33smv51mpjpm085mfq6rrb0vwy59in96")))

