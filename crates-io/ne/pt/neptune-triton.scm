(define-module (crates-io ne pt neptune-triton) #:use-module (crates-io))

(define-public crate-neptune-triton-0.1.0 (c (n "neptune-triton") (v "0.1.0") (h "1wzpb575z509rj5dygy79amfyvmqih3zlivccsscsmcf9j9z5jck")))

(define-public crate-neptune-triton-0.1.1 (c (n "neptune-triton") (v "0.1.1") (d (list (d (n "genfut") (r "^0.1.2") (d #t) (k 1)))) (h "0lnlj01kpasc45ib3qnh0s4dbkylif3ndlkzx6qipy238lkvq7z5") (f (quote (("sequential_c" "genfut/sequential_c") ("opencl" "genfut/opencl") ("no_futhark" "genfut/no_futhark") ("default" "genfut/sequential_c"))))))

(define-public crate-neptune-triton-0.1.2 (c (n "neptune-triton") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ikcv5hbycwjjlvcdb7qwm2s81dp9b5i1g8dvbj2h4d2p3cafdx5") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-0.2.0 (c (n "neptune-triton") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0rs8mynjxb12c9rsfafk9c031fg3mvq3z98p3d8djl0n1pml1jxa") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-0.2.1 (c (n "neptune-triton") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14nx0y0q21zh4n4bgkrfi7ndlkqgpw7fg48k8b5rcfwnh4g8368k") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-0.2.2 (c (n "neptune-triton") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jx3z859xjp6kaj0vgx0a6qmxnjz4p975hxy8gcdmgc5pll4hv9s") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-1.0.0 (c (n "neptune-triton") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "02skadpq6p64jk656y6qynixiy83l4x4f8xhdaam0y9m9x7cd9v5") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-1.1.0 (c (n "neptune-triton") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h8bfpqcb1iapn4r6iy900y27l9iy0fdri1r1prwsyaj864ph630") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-1.1.1 (c (n "neptune-triton") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1h9fqgwc7zcz4386bl8dpgql05v74621afhzpd8m82qmh3jf1gzj") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-1.1.2 (c (n "neptune-triton") (v "1.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14b4c9ha4cpch2xhlgdrg15z44hs9p82whlqwbryn4cl6s6qs1cs") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-1.1.3 (c (n "neptune-triton") (v "1.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1aqicppav9g19nikz4s72f14xyh8m5jjis658w9ca6vxfchx41lw") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-2.0.0 (c (n "neptune-triton") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0darwydwrdmxczlgmqqdscm3daxcxri2bb455a0qk6gb7lnqp6wz") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

(define-public crate-neptune-triton-2.0.1 (c (n "neptune-triton") (v "2.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0921ws9vvpr9jlc2p5kl9nqrk307dd33a54772nxf9ay24h13snx") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda")))) (y #t)))

(define-public crate-neptune-triton-2.1.0 (c (n "neptune-triton") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ynvim3m7k1nlah2yaach746m8j7dy7hjqzara352aywlsvpxvqn") (f (quote (("sequential_c") ("opencl") ("default" "sequential_c") ("cuda"))))))

