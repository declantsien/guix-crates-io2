(define-module (crates-io ne ve nevermind) #:use-module (crates-io))

(define-public crate-nevermind-0.0.1 (c (n "nevermind") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "11g1cm0mrp7q2pyc7zj2sy6187kdmd90gsfrcxgbmr0qhrdajjm5")))

(define-public crate-nevermind-0.1.0 (c (n "nevermind") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0kki4625y4mnkj4a029hh7c48hk5rsy88i175gnphdi3pb780zj0")))

(define-public crate-nevermind-0.1.1 (c (n "nevermind") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "083mdzgl50558fqip7qb1dsihaczfa03ra1dd45qnpnrs1b6ps77")))

(define-public crate-nevermind-0.1.2 (c (n "nevermind") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1d7gy07wsc3ddmcc91csm8j89k52z3f6zc9wvzsc3hhdg7i4c2v1")))

