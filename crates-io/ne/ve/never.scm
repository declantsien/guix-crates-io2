(define-module (crates-io ne ve never) #:use-module (crates-io))

(define-public crate-never-0.0.0 (c (n "never") (v "0.0.0") (h "0f08jq3swr4xjahvvl28m91gw8r2zisri1kkq1lr9z3xxmi6li8d")))

(define-public crate-never-0.1.0 (c (n "never") (v "0.1.0") (h "149whplrasa92hdyg0bfcih2xy71d6ln6snxysrinq3pm1dblsn9") (f (quote (("std") ("default" "std"))))))

