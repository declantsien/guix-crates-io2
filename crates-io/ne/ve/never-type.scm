(define-module (crates-io ne ve never-type) #:use-module (crates-io))

(define-public crate-never-type-0.1.0 (c (n "never-type") (v "0.1.0") (h "0lb5v9qzb26jiy4r6ghzqq9abwwbjipg6xyibfvwmmlhi3gwn9n0")))

(define-public crate-never-type-0.1.1 (c (n "never-type") (v "0.1.1") (h "0z3lsnpamzmzv1ngrg4jhj9x1i4dgz8pnchbb98j9cmnv6a7fmk4")))

