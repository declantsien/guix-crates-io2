(define-module (crates-io ne ve never-say-never) #:use-module (crates-io))

(define-public crate-never-say-never-6.6.6 (c (n "never-say-never") (v "6.6.6") (h "0w2l6yhnj9cx3gfqq9zz37d9zz720hg863brl4swsnd2axanahgh") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-never-say-never-6.6.666 (c (n "never-say-never") (v "6.6.666") (h "1misllg9irf7lp3f3ciz66w330z2lp5kx0kimbg1m56pmm6mfnng")))

