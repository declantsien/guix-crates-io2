(define-module (crates-io ne ut neutron-std) #:use-module (crates-io))

(define-public crate-neutron-std-1.0.4-rc.1 (c (n "neutron-std") (v "1.0.4-rc.1") (d (list (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "cosmwasm-std") (r "^1.1.2") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "osmosis-std-derive") (r "^0.16.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-types") (r "^0.11.1") (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)))) (h "0166vzwd1fvqm122dk3p3825qpy22vq9cdr6drwvwlhyxlgf4znk") (f (quote (("backtraces" "cosmwasm-std/backtraces" "osmosis-std-derive/backtraces"))))))

