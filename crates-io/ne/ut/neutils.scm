(define-module (crates-io ne ut neutils) #:use-module (crates-io))

(define-public crate-neutils-0.1.0 (c (n "neutils") (v "0.1.0") (d (list (d (n "rand") (r "~0.8") (d #t) (k 2)))) (h "106iv782can3430svdj07jaw1ipkhi1kb66csmqlxh64ylavl5gj")))

(define-public crate-neutils-0.1.1 (c (n "neutils") (v "0.1.1") (d (list (d (n "rand") (r "~0.8") (d #t) (k 2)))) (h "1gwl3g4wp7kpjvyacz5svysi896dqqxm5qvxbp3qyg4x20m0v4q3")))

