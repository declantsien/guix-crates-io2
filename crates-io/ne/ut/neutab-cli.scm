(define-module (crates-io ne ut neutab-cli) #:use-module (crates-io))

(define-public crate-neutab-cli-0.1.0 (c (n "neutab-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "neutab") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0pkyzkd95sm2i4p3gfy4x7k4dv0v638mrphsv5khwnf9yrsk80f3") (y #t)))

(define-public crate-neutab-cli-0.2.0 (c (n "neutab-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "neutab") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0dz5mdkf3inlrlf12hj23dm57g32inkw95vv5dpy5snj8ab4rqzv") (y #t)))

(define-public crate-neutab-cli-0.2.1 (c (n "neutab-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "neutab") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.1") (d #t) (k 0)))) (h "0v0dg6dzf37lg58mcgkf5iia8v6rs4fji3kj5vxgpdryrq3p8bf3") (y #t)))

