(define-module (crates-io ne ut neutrino) #:use-module (crates-io))

(define-public crate-neutrino-0.1.0 (c (n "neutrino") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "web-view") (r "^0.4.1") (d #t) (k 0)))) (h "0dylz0ggfikvkyaa2cjzw0piahiikkqfpqkl9q4j0dzp7mkwzspv") (y #t)))

(define-public crate-neutrino-0.1.1 (c (n "neutrino") (v "0.1.1") (d (list (d (n "rsass") (r "^0.11.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "web-view") (r "^0.4.1") (d #t) (k 0)))) (h "0bkjxf8gpw2f0kkxm8507mdkn5s42v2axwyfjr5md94jlvg06pyd")))

(define-public crate-neutrino-0.2.0 (c (n "neutrino") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 1)) (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "rsass") (r "^0.11.0") (d #t) (k 1)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "web-view") (r "^0.4.1") (d #t) (k 0)))) (h "0vhmnlp6qhvyrbrj8i6dyah1cr5slhl70d7pginjhjnr0hn38fr0")))

(define-public crate-neutrino-0.3.0 (c (n "neutrino") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 1)) (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "rsass") (r "^0.11.0") (d #t) (k 0)) (d (n "rsass") (r "^0.11.0") (d #t) (k 1)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "web-view") (r "^0.4.1") (d #t) (k 0)))) (h "0kysxrvpwlysgwyvzr8qjq26fv5a5k1ihi4y54k83rrpamqfbwga")))

(define-public crate-neutrino-0.3.1 (c (n "neutrino") (v "0.3.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 1)) (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "rsass") (r "^0.11.0") (d #t) (k 0)) (d (n "rsass") (r "^0.11.0") (d #t) (k 1)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "web-view") (r "^0.4.1") (d #t) (k 0)))) (h "0mwfbibgdk0ffw1apmlg96vcy0qkx59qhv1irz1az6p2jpyympwi")))

