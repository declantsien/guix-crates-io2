(define-module (crates-io ne ut neutral_types) #:use-module (crates-io))

(define-public crate-neutral_types-0.1.0 (c (n "neutral_types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1.14.0") (d #t) (k 0)))) (h "06lpxfi92c0gdkdwm62d6bd9m8m5agg2jlsvx09skp1bsknpx901")))

(define-public crate-neutral_types-0.2.0 (c (n "neutral_types") (v "0.2.0") (d (list (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)))) (h "174p9imggvf5g51dq15zl2fas89zc8am2r6p6khd1gy394y3jssr")))

