(define-module (crates-io ne ut neutron-star-rt) #:use-module (crates-io))

(define-public crate-neutron-star-rt-0.1.0 (c (n "neutron-star-rt") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gbazicxqfiiyn4mq6a63311srh6q9hwv4js3m9xak8xs07y832q")))

(define-public crate-neutron-star-rt-0.2.0 (c (n "neutron-star-rt") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wf4c7m1p31fdjz7yd53jfld47sb2lsz3xgjn95k6nwrc6xlakj4")))

(define-public crate-neutron-star-rt-0.2.1 (c (n "neutron-star-rt") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0bgri1yj1dg08mhgi0v8d3f5yx5g25d6g3vcijbj8q8czisj7wld")))

