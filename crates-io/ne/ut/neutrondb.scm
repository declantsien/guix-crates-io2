(define-module (crates-io ne ut neutrondb) #:use-module (crates-io))

(define-public crate-neutrondb-0.9.0 (c (n "neutrondb") (v "0.9.0") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.0") (d #t) (k 0)))) (h "0kqlyya7jcihfd0q5sf47s2371i2y0dxabg0zgm2xmr9k3p7wlmi")))

(define-public crate-neutrondb-0.9.1 (c (n "neutrondb") (v "0.9.1") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.0") (d #t) (k 0)))) (h "197c416zrzpp4mb4lr8xbcdwmn4jfh4flbvg8k50lqyib9f55zk8")))

(define-public crate-neutrondb-0.9.2 (c (n "neutrondb") (v "0.9.2") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.3") (d #t) (k 0)))) (h "057y5dl9z1ch19i1szc7vshxc62bii4v8avjk4gyca07924mq6cq")))

(define-public crate-neutrondb-0.9.3 (c (n "neutrondb") (v "0.9.3") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.4") (d #t) (k 0)))) (h "0j3h8dmbgkbjfnwc31ig1n5yfspyn62z4f0k4j7a0nnfk0wv6npq")))

(define-public crate-neutrondb-0.9.4 (c (n "neutrondb") (v "0.9.4") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.4") (d #t) (k 0)))) (h "0a4p1g5bd9jayv9wb44ms0d9ssvqy6nw2l8smk4vq604zzdlqmgi")))

(define-public crate-neutrondb-0.9.6 (c (n "neutrondb") (v "0.9.6") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.6") (d #t) (k 0)))) (h "04zky7kbjcbzxdqmr0ikq33vngh9wnyzb1s9hnp9pg1pdw1z6nxi")))

(define-public crate-neutrondb-0.9.7 (c (n "neutrondb") (v "0.9.7") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.6") (d #t) (k 0)))) (h "0qq61mvh45ga97icai0gw3z04nwgfm3lg9cnsq9kn700ymlnx2zy")))

(define-public crate-neutrondb-0.9.8 (c (n "neutrondb") (v "0.9.8") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.6") (d #t) (k 0)))) (h "17bxdv9j88322j8mwvnafjvz7zp8bjimar0pj6fmbyfxhin7mlhk")))

(define-public crate-neutrondb-0.9.10 (c (n "neutrondb") (v "0.9.10") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.11") (d #t) (k 0)))) (h "1gmdqvzcprj7p8ya7dn7c0i8kshi8p6lbhggwb6gfvq8fn014ywx")))

(define-public crate-neutrondb-0.9.11 (c (n "neutrondb") (v "0.9.11") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.11") (d #t) (k 0)))) (h "18wbv0662idd4h58vjfw7h960vb6pmlybdw07mj0g66dcc28jnp9")))

(define-public crate-neutrondb-0.9.13 (c (n "neutrondb") (v "0.9.13") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.12") (d #t) (k 0)))) (h "1mbz37rri6w85hp13vyfswpli1dnmk2wzgfbm7wwi78z3xw3v4l9")))

(define-public crate-neutrondb-0.9.14 (c (n "neutrondb") (v "0.9.14") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.13") (d #t) (k 0)))) (h "12wakmjkhq8hm1l6jvgfigj1m4cpdpwsc22bsyyii63ymiz704fl")))

(define-public crate-neutrondb-0.9.15 (c (n "neutrondb") (v "0.9.15") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.13") (d #t) (k 0)))) (h "19pi3yv097q044jzkjpsg5wkpjzz50br9yxyyjj0qy3d50fdh7m1")))

(define-public crate-neutrondb-0.9.16 (c (n "neutrondb") (v "0.9.16") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "0dnh2ip98j2n55pv6sxblmvhyqcnb3xh94pdgb5bzg2c8p416mnv")))

(define-public crate-neutrondb-0.9.18 (c (n "neutrondb") (v "0.9.18") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "1w69smvb9zpx2bask1sldhxsqnsp2hhzc2vy907g2v2bf6bbk2z0")))

(define-public crate-neutrondb-0.9.19 (c (n "neutrondb") (v "0.9.19") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "0pkiv3qc5x1pwc085phvxhaqpssnliihf1k62h70rr6ri04nwzvs")))

(define-public crate-neutrondb-1.0.0 (c (n "neutrondb") (v "1.0.0") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "0i6k9nya9g0df7w3l1mv5vkkidhzc4vxcfl8l2dmnjks7p5rccmn")))

(define-public crate-neutrondb-1.0.1 (c (n "neutrondb") (v "1.0.1") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "179pb5clgm9b4lfhwg984ibycxx1yb46wr5irw3zdkw8rhylbf3a")))

(define-public crate-neutrondb-2.0.0 (c (n "neutrondb") (v "2.0.0") (d (list (d (n "astro-notation") (r "^1.1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "135q346jv6bwvnp5xwgd5jw24rginfhpqpmnhg00a9hr4dciq7sx")))

(define-public crate-neutrondb-2.1.1 (c (n "neutrondb") (v "2.1.1") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1s669vfq6v9j0f5b2l5w6xvnj0jlx39k7xj4kvay0ynhv32wjh8d")))

(define-public crate-neutrondb-2.2.0 (c (n "neutrondb") (v "2.2.0") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1qd0x1871fhazfxf0354f76i0xzv13hq9x8zcd0a0mbb7csshfhg")))

(define-public crate-neutrondb-2.2.1 (c (n "neutrondb") (v "2.2.1") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1i89z45sbj6q80f5vk88sr3q5p2agbydmskhhq0ycmlxbwpbhv3r")))

(define-public crate-neutrondb-2.3.0 (c (n "neutrondb") (v "2.3.0") (d (list (d (n "astro-format") (r "^0.6.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.1") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "1c2bbbj2lg0fq2yzfxa3jm704rbhv8w0p0hfdaxhah9559mn041j")))

(define-public crate-neutrondb-4.0.0 (c (n "neutrondb") (v "4.0.0") (d (list (d (n "astro-format") (r "^1.0.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^4.1.0") (d #t) (k 0)))) (h "137199qzz48b1xbn1xng2bx41dzlr42qjm7jkgijsrc444g3gm5d")))

(define-public crate-neutrondb-5.0.0 (c (n "neutrondb") (v "5.0.0") (d (list (d (n "astro-format") (r "^1.0.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "1bg38gp5s5wq2caj5hg9fvagczav8yafrxxd7cgy83q4ky6w9d6w")))

(define-public crate-neutrondb-5.0.1 (c (n "neutrondb") (v "5.0.1") (d (list (d (n "astro-format") (r "^1.0.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "035bd6vbdkfja6fx8pmi8yfhm6szn256dcwjdq5z0wrrlzyjrl2r")))

(define-public crate-neutrondb-5.0.2 (c (n "neutrondb") (v "5.0.2") (d (list (d (n "astro-format") (r "^1.0.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "1j4m3fqhws1fbhwdmvp12rfl5n3n9n5mkzby97s46y7qfh16y5m7")))

(define-public crate-neutrondb-5.0.3 (c (n "neutrondb") (v "5.0.3") (d (list (d (n "astro-format") (r "^1.0.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "1mjywn6b8hnxs4536hqwd29435akiw9rvbi6zzij77ihz1fi36mc")))

(define-public crate-neutrondb-5.0.4 (c (n "neutrondb") (v "5.0.4") (d (list (d (n "astro-format") (r "^1.2.0") (d #t) (k 0)) (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "14dksdn06wvad4jzvh396caps2fzlgzm0mwd3f6qxzghbrcmb4fk")))

(define-public crate-neutrondb-6.0.0 (c (n "neutrondb") (v "6.0.0") (d (list (d (n "fides") (r "^3.0.3") (d #t) (k 0)) (d (n "opis") (r "^5.0.2") (d #t) (k 0)))) (h "13x3rlyzsj8rksgk4bl09kn13a9f8i7gj66q6mk6bcvqd2y3yfhv")))

