(define-module (crates-io ne wb newbase60) #:use-module (crates-io))

(define-public crate-newbase60-0.1.0 (c (n "newbase60") (v "0.1.0") (d (list (d (n "rstest") (r "^0.7.0") (d #t) (k 0)))) (h "0is1lsi602sch2x02yqa5n4xxb302w7xgz7vn7y1hm5x8crrkqhw")))

(define-public crate-newbase60-0.1.1 (c (n "newbase60") (v "0.1.1") (d (list (d (n "rstest") (r "^0.7.0") (d #t) (k 0)))) (h "11in6f5paqbcwdc8rjb15siky4pniwfzk0xra9c8dwlhn2hhg6yd") (y #t)))

(define-public crate-newbase60-0.1.2 (c (n "newbase60") (v "0.1.2") (d (list (d (n "rstest") (r "^0.7.0") (d #t) (k 2)))) (h "0k7rfa6ykc558hfa6g3v5s68mg15a5c392p9pl5m3wxvxnfm6qf2")))

(define-public crate-newbase60-0.1.3 (c (n "newbase60") (v "0.1.3") (d (list (d (n "rstest") (r "^0.7.0") (d #t) (k 2)))) (h "1fmm6dm61sk9pyybbd5k02hbgiqrg0j6p8lbjsyqqn18wsfy2vsg")))

(define-public crate-newbase60-0.1.4 (c (n "newbase60") (v "0.1.4") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1h2xgl354cqlik8d2h01vgv7xvjv96plz8ipy08zmz5chzjdgm63")))

