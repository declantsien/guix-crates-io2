(define-module (crates-io ne wb newbee) #:use-module (crates-io))

(define-public crate-newbee-0.1.0 (c (n "newbee") (v "0.1.0") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "11ywfxnsyzzclwx777j5fvpmaf9wvi3gg9lhvbqz3rjj4xy1phlk")))

(define-public crate-newbee-0.1.1 (c (n "newbee") (v "0.1.1") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0fvfcwkf7vab75g22dnj9vqbf3plz1flj5vb27mhsjpz5rk2rxl0")))

(define-public crate-newbee-0.1.2 (c (n "newbee") (v "0.1.2") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "1093a5lvld27mbrd5a82pra873agxr6wcnynz61nnffa9f1crfs0")))

(define-public crate-newbee-0.1.3 (c (n "newbee") (v "0.1.3") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0dwy3c0qjpmpxm5c3zs11kzxlj2gl7093v4y24mpmi0ibpnircri")))

(define-public crate-newbee-0.1.4 (c (n "newbee") (v "0.1.4") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "1kci5r48myw917rkms1kv13fj7vpfjz45r2s0bgyqpc2czjxjzl8")))

(define-public crate-newbee-0.1.5 (c (n "newbee") (v "0.1.5") (d (list (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0xgiwv2304r9yyj34fi5b4xhgf362gx0n1kzn7bzjm4imsb42d6k")))

(define-public crate-newbee-0.1.6 (c (n "newbee") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0w5qhi9zpqaifr0q4ca2p0j4lwsccy0m29khlnak4zw20q83hmqs")))

(define-public crate-newbee-0.1.7 (c (n "newbee") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0j7ldn5i72magsdhr4rhf8lmlnls5zsb97yv7znciqapjilxakc7")))

