(define-module (crates-io ne xt nextcloud_appinfo) #:use-module (crates-io))

(define-public crate-nextcloud_appinfo-0.1.0 (c (n "nextcloud_appinfo") (v "0.1.0") (d (list (d (n "xpath_reader") (r "^0.3.2") (d #t) (k 0)))) (h "1d7iksr04y8pibpmzk3a70v38z3mgliw9casz7jgxpn45aqz7fb3")))

(define-public crate-nextcloud_appinfo-0.2.0 (c (n "nextcloud_appinfo") (v "0.2.0") (d (list (d (n "xpath_reader") (r "^0.3.2") (d #t) (k 0)))) (h "1g9x0xzr7z1v7j2m5xxspn9n91qawa2cx0phhrf84yy1dad8jf9d")))

(define-public crate-nextcloud_appinfo-0.3.0 (c (n "nextcloud_appinfo") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "xpath_reader") (r "^0.3.2") (d #t) (k 0)))) (h "1bq72jrdz579p0vb7c5nv436agbc4d77xv23pkv0r7sl8g7qgrcf")))

(define-public crate-nextcloud_appinfo-0.4.0 (c (n "nextcloud_appinfo") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "xpath_reader") (r "^0.4.0") (d #t) (k 0)))) (h "14r4nvsgakv2blqn2lapjlvkfnf4wh8xrf1i67hvb4a13r6hpf02")))

(define-public crate-nextcloud_appinfo-0.4.1 (c (n "nextcloud_appinfo") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "xpath_reader") (r "^0.5.1") (d #t) (k 0)))) (h "0hszzkr9akpy2x0lvs293zyx2gp8miqchrkk6xi707zf1215jc84")))

(define-public crate-nextcloud_appinfo-0.5.0 (c (n "nextcloud_appinfo") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "xpath_reader") (r "^0.5") (d #t) (k 0)))) (h "0nlbnsxw57ilaf0sx3s2d7krn00abg2vxgw4bym4508cban8xrdx")))

(define-public crate-nextcloud_appinfo-0.6.0 (c (n "nextcloud_appinfo") (v "0.6.0") (d (list (d (n "semver") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xpath_reader") (r "^0.5") (d #t) (k 0)))) (h "1kqm2bxllmn1cc11czmfy30378sfv8d3mrnv328blrqvwfvwancx")))

