(define-module (crates-io ne xt next_episode) #:use-module (crates-io))

(define-public crate-next_episode-0.1.0 (c (n "next_episode") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1ipk8h1n5cj26xaxzijxcbkwjpx3fsvyx7h653dlcgip86jpn4v1")))

(define-public crate-next_episode-0.2.0 (c (n "next_episode") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "107abgj6307w4nbmxlaqabdbaybgqllqi9hcr1l1l32nyx12b7ll")))

(define-public crate-next_episode-0.3.0 (c (n "next_episode") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "08f8rwwkzzczixblwdafhjd9z6w6dqzsvxgai8d563xpiqzcmwfn")))

