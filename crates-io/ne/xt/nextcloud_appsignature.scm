(define-module (crates-io ne xt nextcloud_appsignature) #:use-module (crates-io))

(define-public crate-nextcloud_appsignature-0.1.0 (c (n "nextcloud_appsignature") (v "0.1.0") (d (list (d (n "base64") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)))) (h "05wybqqjyvgiky5fbx09vjr0rgwm5j5pp9fwhil6rcsbaklhbylv")))

(define-public crate-nextcloud_appsignature-0.2.0 (c (n "nextcloud_appsignature") (v "0.2.0") (d (list (d (n "base64") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)))) (h "163bx6fz8ibh92klpbz1slpnq6my2r3z916sr8gyp9gry8c2i3zs")))

(define-public crate-nextcloud_appsignature-0.3.0 (c (n "nextcloud_appsignature") (v "0.3.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1pw9pd9bqv3gyfvj89rdcsvbps8qi7pi8rbi4xg952xvwsmcvg9i")))

(define-public crate-nextcloud_appsignature-0.4.0 (c (n "nextcloud_appsignature") (v "0.4.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "03xb6zsz2bb5bala76h3308v84lgbhklhhp9y9j7damhvx0bsk92")))

(define-public crate-nextcloud_appsignature-0.5.0 (c (n "nextcloud_appsignature") (v "0.5.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yw26na168zmaw6q193m01l4zw0wf9nslfwix3lyk1zwibpy420h")))

(define-public crate-nextcloud_appsignature-0.6.0 (c (n "nextcloud_appsignature") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0si7zd4zxhqca8xwgx06023x1y0aa3x44dancym3gm5v3p4r45z8")))

(define-public crate-nextcloud_appsignature-0.7.0 (c (n "nextcloud_appsignature") (v "0.7.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03p2791daq5zjm0ym52wjpdlv2gcl6hmfr9wwwhvmz1h5chi67ps")))

(define-public crate-nextcloud_appsignature-0.7.1 (c (n "nextcloud_appsignature") (v "0.7.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rsa") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06nkn5wgyj5kj7l7bjv0cdr9kgxc082373rn9iwb5njwljiw1cmy")))

