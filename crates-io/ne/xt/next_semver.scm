(define-module (crates-io ne xt next_semver) #:use-module (crates-io))

(define-public crate-next_semver-1.0.0 (c (n "next_semver") (v "1.0.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)))) (h "0ynfjn6xcp9ya3hwcmynh9s7dgsn9k3d2g93s1l02zv692ckpapy") (f (quote (("web" "rocket"))))))

