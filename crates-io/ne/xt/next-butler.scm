(define-module (crates-io ne xt next-butler) #:use-module (crates-io))

(define-public crate-next-butler-0.1.0 (c (n "next-butler") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1j6bzv87d2ssm5g2lcpwsx22m8vrai6i6xwyriafd02zz34pblq7")))

(define-public crate-next-butler-0.2.0 (c (n "next-butler") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0vch9ab50lp1712f63byr06glzflpmh1s8fjz3abimmbdpqavhpq")))

(define-public crate-next-butler-1.0.0 (c (n "next-butler") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("help" "suggestions" "error-context" "color"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.2") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "path-clean") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.12") (d #t) (k 0)))) (h "0c1023iqlzldcd4l551aq26m9jxfxjjsf60piww6wb8cpmz186hd")))

(define-public crate-next-butler-1.0.1 (c (n "next-butler") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("help" "suggestions" "error-context" "color"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.2") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "path-clean") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.12") (d #t) (k 0)))) (h "0x9bg743fwmw6mi7ihmyzhslsp9035kd2c6rfdm8ac4zr4br9l97")))

