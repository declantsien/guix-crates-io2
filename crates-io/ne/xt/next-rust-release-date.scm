(define-module (crates-io ne xt next-rust-release-date) #:use-module (crates-io))

(define-public crate-next-rust-release-date-0.0.0 (c (n "next-rust-release-date") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0cgagxzg47sf1n45fld79wmjs9kzqdg0967f6g1givzwyayw75m6")))

(define-public crate-next-rust-release-date-0.1.0 (c (n "next-rust-release-date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1nwcry83ymmb0ikdqv43w5i4crmsj96kbqaaics141vzdpw4ykvk")))

(define-public crate-next-rust-release-date-0.2.0 (c (n "next-rust-release-date") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1spxr34140pcvpmym7a33x7mqm5mf5i7vpgqhk206n7lg42z2qcw") (r "1.60")))

