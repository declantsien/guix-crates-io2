(define-module (crates-io ne xt nextapi) #:use-module (crates-io))

(define-public crate-nextapi-0.0.0 (c (n "nextapi") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1r226s5hnfwv9wpqml8ryvb8i0bzkx71r0b40wbchgldd90l4n0z")))

