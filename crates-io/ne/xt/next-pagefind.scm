(define-module (crates-io ne xt next-pagefind) #:use-module (crates-io))

(define-public crate-next-pagefind-0.1.0 (c (n "next-pagefind") (v "0.1.0") (h "1jndsw4mmjw3mbc580hlm0pl6slwli4mkr79ca4h4vizx8w3m0dk")))

(define-public crate-next-pagefind-0.1.1 (c (n "next-pagefind") (v "0.1.1") (h "09h44xmvlbv2kmrmkv7nlnmi9nzlc8hss32pnbnk6lh59nzfwd7p")))

(define-public crate-next-pagefind-0.1.2 (c (n "next-pagefind") (v "0.1.2") (h "1l2kplgn94kaf961x3ahvpdmz3l497w2cr4r9d6np00iamkn3rb9")))

(define-public crate-next-pagefind-0.1.3 (c (n "next-pagefind") (v "0.1.3") (h "02qp5mnf4bc21d3kk12pwkpyy676mjw2qixs65hhjdgq0330zsmz")))

(define-public crate-next-pagefind-0.1.4 (c (n "next-pagefind") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ci6r1f30czgfncls0wpzwvb3w4f2g39zsj00kyjvjkcjyiyzcz")))

