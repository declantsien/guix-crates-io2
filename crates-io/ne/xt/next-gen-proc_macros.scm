(define-module (crates-io ne xt next-gen-proc_macros) #:use-module (crates-io))

(define-public crate-next-gen-proc_macros-0.1.0-rc1 (c (n "next-gen-proc_macros") (v "0.1.0-rc1") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "1mr8891xr2i4h24032bj69bdbyszv5ia4gqkrqnnanahc0s4hbnp") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0-rc2 (c (n "next-gen-proc_macros") (v "0.1.0-rc2") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6r3qvmas54d0zx8mgxn76ngz3q1fq410hdflcwqmyf15nc7wrz") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0-rc3 (c (n "next-gen-proc_macros") (v "0.1.0-rc3") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "16134dw8gyi7bgiw25b7x4ipj6h79jdqhhv7f5prqd0p4gddm9a4") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0-rc4 (c (n "next-gen-proc_macros") (v "0.1.0-rc4") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9fab1dzmy6pdj9ihw24xsin8vi7yrsrycacg51gpb02203akby") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0-rc5 (c (n "next-gen-proc_macros") (v "0.1.0-rc5") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "14mf6fz8vg4bgr8cj82fab5d7nvpn1g7za1vbinjv8jxrfhcbnws") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0-rc6 (c (n "next-gen-proc_macros") (v "0.1.0-rc6") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0clkd0i40nzd7av335xzpqrh917f7a4ci3xq8m4v4mg5z6jib3jy") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.0 (c (n "next-gen-proc_macros") (v "0.1.0") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "14q0jjicb5maixk5zjsmqzg1nnqisplmrhmwf9j6i18kad2yc21h") (f (quote (("verbose-expansions" "bat"))))))

(define-public crate-next-gen-proc_macros-0.1.1 (c (n "next-gen-proc_macros") (v "0.1.1") (d (list (d (n "bat") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "18650y3q5972rsbd7whpsmwx6007gr5wxlfdg528j0yxzz99b4x5") (f (quote (("verbose-expansions" "bat"))))))

