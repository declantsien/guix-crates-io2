(define-module (crates-io ne xt next-gen) #:use-module (crates-io))

(define-public crate-next-gen-0.0.3-alpha (c (n "next-gen") (v "0.0.3-alpha") (d (list (d (n "pin-project") (r "^0.4.5") (d #t) (k 0)) (d (n "proc_macro") (r "^0.0.3-alpha") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "1n2aszq1m31gb3fxfynppbic1w1gyin6x6c6nxahs14c1344x2sg") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("external_doc") ("default") ("allow-warnings")))) (y #t)))

(define-public crate-next-gen-0.0.4 (c (n "next-gen") (v "0.0.4") (d (list (d (n "proc_macro") (r "^0.0.4") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "16gil43h00c64gi71kpqfjqc1syyp52w7yyc3bxlb1bbkp5wplg1") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("external_doc" "proc_macro/external_doc") ("default") ("allow-warnings")))) (y #t)))

(define-public crate-next-gen-0.0.5 (c (n "next-gen") (v "0.0.5") (d (list (d (n "proc_macro") (r "^0.0.5") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "17yy7zr032h7a61adnd2l43qnjwvlk6m4b78mbdaq7wxjd28qdp8") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("external_doc" "proc_macro/external_doc") ("default" "alloc") ("allow-warnings") ("alloc")))) (y #t)))

(define-public crate-next-gen-0.0.6 (c (n "next-gen") (v "0.0.6") (d (list (d (n "proc_macro") (r "^0.0.6") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "1v5bp4szj4air5m6wfqv7hvlxqb7ip94v89k6vr21rf0lmsja95l") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("external_doc" "proc_macro/external_doc") ("default" "alloc") ("allow-warnings") ("alloc"))))))

(define-public crate-next-gen-0.0.7 (c (n "next-gen") (v "0.0.7") (d (list (d (n "proc_macro") (r "^0.0.7") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "122wz832p51ryjbffb0l5y11kk30ch61zwpjk2w5yaxd94gfvkph") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("external_doc" "proc_macro/external_doc") ("default" "alloc") ("allow-warnings") ("alloc"))))))

(define-public crate-next-gen-0.0.8 (c (n "next-gen") (v "0.0.8") (d (list (d (n "proc_macro") (r "^0.0.8") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "1cny7iyin1f2xgl4ja5jbdn76m17ajmvlscbjv8d7lp77y4lhfnv") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("std") ("external_doc" "proc_macro/external_doc") ("default" "std") ("allow-warnings"))))))

(define-public crate-next-gen-0.0.9 (c (n "next-gen") (v "0.0.9") (d (list (d (n "proc_macro") (r "^0.0.9") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "1ngfwkp2ifc0lf8rfbz6py1s0v592azj2217x3kwljbyvvpds0bp") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("std") ("external_doc" "proc_macro/external_doc") ("default" "std") ("allow-warnings"))))))

(define-public crate-next-gen-0.0.10 (c (n "next-gen") (v "0.0.10") (d (list (d (n "proc_macro") (r "^0.0.10") (d #t) (k 0) (p "next-gen_proc-macro")))) (h "0lc8rk1vq13bmm29bwm427ppwzlwhma9vsnwwz8f8hvk1ika73as") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("std") ("external_doc" "proc_macro/external_doc") ("default" "std") ("allow-warnings"))))))

(define-public crate-next-gen-0.1.0-rc1 (c (n "next-gen") (v "0.1.0-rc1") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc1") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "1bavxdbw632f53hcnsl93zzmg7862q3nb0rxi1wlfhiwi66wrfwk") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("default" "std") ("better-docs") ("alloc"))))))

(define-public crate-next-gen-0.1.0-rc2 (c (n "next-gen") (v "0.1.0-rc2") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc2") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "0yf7fzlf0imi41b7pcf8pv61zwnyrvxz7d8czzk4yrlwb3142cad") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.0-rc3 (c (n "next-gen") (v "0.1.0-rc3") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc3") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "01hp57d5101ijw1svfyfz8ag9qa8g7aslin94qrg0acp7vrrclcp") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.0-rc4 (c (n "next-gen") (v "0.1.0-rc4") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc4") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "08r8bc040963qhsdvl5k5aznism8m1yly3j7sw99ah7r5qs25mgx") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.0-rc5 (c (n "next-gen") (v "0.1.0-rc5") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc5") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "1197n7ga52l26dpf4h8m1dhvyp7d15c0px285lfhgbb29pjhprq3") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.0-rc6 (c (n "next-gen") (v "0.1.0-rc6") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0-rc6") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "13k03kszpyiwf5qmm0wgp70m3zsr9nsvpjjp4g1gx7shswzinz58") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.0 (c (n "next-gen") (v "0.1.0") (d (list (d (n "next-gen-proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "17srvlqp6vmxw83cfbdbqx9cdrj00jw3jnihra8bcdz70f6429jl") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

(define-public crate-next-gen-0.1.1 (c (n "next-gen") (v "0.1.1") (d (list (d (n "next-gen-proc_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "unwind_safe") (r "^0.1.0") (d #t) (k 0)))) (h "1ssv8m9x2cavxg2zdnhq6nl0y2f7psysyx0wapwjg7w59jvg0qhr") (f (quote (("verbose-expansions" "next-gen-proc_macros/verbose-expansions") ("ui-tests" "better-docs") ("std" "alloc") ("nightly" "better-docs") ("default" "std") ("better-docs") ("alloc")))) (r "1.45.0")))

