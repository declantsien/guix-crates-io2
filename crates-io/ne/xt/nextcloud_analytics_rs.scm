(define-module (crates-io ne xt nextcloud_analytics_rs) #:use-module (crates-io))

(define-public crate-nextcloud_analytics_rs-0.1.0 (c (n "nextcloud_analytics_rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (d #t) (k 0)))) (h "12dcb40qwar7xwfmh9qd5hmmqq22wdwnijgwad2iynacz5k2jw0q")))

