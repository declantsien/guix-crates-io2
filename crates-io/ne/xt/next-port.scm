(define-module (crates-io ne xt next-port) #:use-module (crates-io))

(define-public crate-next-port-0.1.0 (c (n "next-port") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wl9i2p7ffmfshdg3fqrlcy85w78rmrsrlnhnrz1birwblzq5960")))

(define-public crate-next-port-0.1.1 (c (n "next-port") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xanzsr8dl70pknma12rs5xmsd7mjnhm41prak1gg70kywf64f0l")))

