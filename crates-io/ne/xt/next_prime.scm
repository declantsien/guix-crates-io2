(define-module (crates-io ne xt next_prime) #:use-module (crates-io))

(define-public crate-next_prime-0.1.0 (c (n "next_prime") (v "0.1.0") (h "0gba1azi6g29gc8mxpwc3rbzpy71w1mshc2qfic98r6g8wzaszf9")))

(define-public crate-next_prime-0.1.1 (c (n "next_prime") (v "0.1.1") (h "06f9xxk7bia5dqki3xjryysp4anldzhx7b8kv98jrk1z54wxjrda")))

