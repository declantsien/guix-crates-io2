(define-module (crates-io ne ny nenya) #:use-module (crates-io))

(define-public crate-nenya-0.0.1 (c (n "nenya") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 2)) (d (n "eframe") (r "^0.27.2") (d #t) (k 2)) (d (n "egui") (r "^0.27.2") (d #t) (k 2)) (d (n "egui_plot") (r "^0.27.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)))) (h "0g90bwpphmph8sgjz95lir25x2116w6ma9if9xh9nq5mz15phvqa")))

