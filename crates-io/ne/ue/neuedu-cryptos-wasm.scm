(define-module (crates-io ne ue neuedu-cryptos-wasm) #:use-module (crates-io))

(define-public crate-neuedu-cryptos-wasm-0.5.0 (c (n "neuedu-cryptos-wasm") (v "0.5.0") (d (list (d (n "neuedu-cryptos") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0wlscwnp5apxcmbgn5lipvcm47lkl9vivcbb6kyclis7zvr09cmz")))

(define-public crate-neuedu-cryptos-wasm-0.5.1 (c (n "neuedu-cryptos-wasm") (v "0.5.1") (d (list (d (n "neuedu-cryptos") (r "^0.5.0") (d #t) (k 0)))) (h "1003lfxhgd4w67fijmmr3pzhh00qvyw05yqjfd310ysx17j9xdz1") (y #t)))

(define-public crate-neuedu-cryptos-wasm-0.5.2 (c (n "neuedu-cryptos-wasm") (v "0.5.2") (d (list (d (n "neuedu-cryptos") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0zhggyad0b1kxxwzcmrc42ym11wzcgsvvfkgf4fj46sdi42ryrki") (f (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("cfb") ("cbc") ("block"))))))

(define-public crate-neuedu-cryptos-wasm-0.5.3 (c (n "neuedu-cryptos-wasm") (v "0.5.3") (d (list (d (n "neuedu-cryptos") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "1wq2jkjrflsaz5r783z8dawgigyw1vclzf6lkljd922dqljgpz4c") (f (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("default" "sm3" "sm4") ("cfb") ("cbc") ("block")))) (y #t)))

(define-public crate-neuedu-cryptos-wasm-0.5.4 (c (n "neuedu-cryptos-wasm") (v "0.5.4") (d (list (d (n "neuedu-cryptos") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "120kq2ssza3a87idj1lrkfh89i8anzhaj8qh6h9vd0f2292hv9fs") (f (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("default" "sm3" "sm4" "block" "stream") ("cfb") ("cbc") ("block"))))))

