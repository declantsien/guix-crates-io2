(define-module (crates-io ne tb netbuf) #:use-module (crates-io))

(define-public crate-netbuf-0.1.0 (c (n "netbuf") (v "0.1.0") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "05h8advb7vgrbff3zgsjhh9i7ibyavcv2mmzb0j40jv7qsvc1qiw")))

(define-public crate-netbuf-0.2.0 (c (n "netbuf") (v "0.2.0") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "17yxlhhkhxjbrj3s2nz15h6aw9rinb95xcxxgzrl3p4z8sni2wrx")))

(define-public crate-netbuf-0.2.1 (c (n "netbuf") (v "0.2.1") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "1bvz452n40dn6r177kd0xw7ygkw2pqs372iycn6d1ajq16jwgdkn") (y #t)))

(define-public crate-netbuf-0.2.2 (c (n "netbuf") (v "0.2.2") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "1jycqs4abp08kbpwf325gz5g9q5rvlzask3manh2x6550215jnlq")))

(define-public crate-netbuf-0.2.3 (c (n "netbuf") (v "0.2.3") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "1cfxml6asp7hlcjdyb5zsramqfbgn9dhgaizvz885zh7m457m2sg")))

(define-public crate-netbuf-0.3.0 (c (n "netbuf") (v "0.3.0") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "069ycs6jybrvhs9pkj67x72yr43mavif3dq9lds7mhqgk0j5dhm8")))

(define-public crate-netbuf-0.3.1 (c (n "netbuf") (v "0.3.1") (d (list (d (n "mockstream") (r "*") (d #t) (k 2)))) (h "0x2js6r0d3gs8bijgx98dzpfmzmy5hjgjk81yrk3zy76jvf36sdi")))

(define-public crate-netbuf-0.3.2 (c (n "netbuf") (v "0.3.2") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "0lirajwyzallppw7hisnx4r3g2k6i7vpli41lf9k48j6awwi0ja6")))

(define-public crate-netbuf-0.3.3 (c (n "netbuf") (v "0.3.3") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "1n3r1k2g0sl80rdf09npci6c2zls70jps1bf2iymhv52had4wibx")))

(define-public crate-netbuf-0.3.4 (c (n "netbuf") (v "0.3.4") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "1fccr6r4pnaz93w5v1wx1ihys726i82g3z8aggdhs8f3r9jq2xvh")))

(define-public crate-netbuf-0.3.5 (c (n "netbuf") (v "0.3.5") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "1lcgqdrz138gzh6ggbfqyx9fybbqp6rzd47nfi8bgvcggvxpiwhz")))

(define-public crate-netbuf-0.3.6 (c (n "netbuf") (v "0.3.6") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "09srravinfr1wbd7vlq5z5fwrfkpn9wlnlzb6pkbz8qwih44j7if")))

(define-public crate-netbuf-0.3.7 (c (n "netbuf") (v "0.3.7") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "0kf2ciw4309hpml2dj6iqc1y31nszg1f1nxvh4n0cm8f1ank2db0")))

(define-public crate-netbuf-0.3.8 (c (n "netbuf") (v "0.3.8") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "11mcxzmi7cqfrs0dicdklcvdxc44qx3q214wlnzbnjd7pnhdc5v9")))

(define-public crate-netbuf-0.4.0 (c (n "netbuf") (v "0.4.0") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "06sbq46my1n1l7lnfnz15hwrss6w74kmw1zfwlbjpqww5930gqjm")))

(define-public crate-netbuf-0.4.1 (c (n "netbuf") (v "0.4.1") (d (list (d (n "mockstream") (r "^0.0.2") (d #t) (k 2)))) (h "1nx9c4bz2s8zf2j8lz769482kbcs66w30vzivk312g5g6sjra7pl")))

