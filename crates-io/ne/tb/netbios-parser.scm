(define-module (crates-io ne tb netbios-parser) #:use-module (crates-io))

(define-public crate-netbios-parser-0.1.0 (c (n "netbios-parser") (v "0.1.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom-derive") (r "^0.8") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dladzfcv7i3g1d1s0s5s43mxd0sjq56gr649w25rgv2fd0pxha2")))

(define-public crate-netbios-parser-0.2.0 (c (n "netbios-parser") (v "0.2.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom-derive") (r "^0.10") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z073vk78c8am0y2k9zy4zqk7855qm762viys0mxkvjnj0zwml39")))

