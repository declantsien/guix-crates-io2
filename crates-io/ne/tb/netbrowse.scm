(define-module (crates-io ne tb netbrowse) #:use-module (crates-io))

(define-public crate-netbrowse-0.1.0 (c (n "netbrowse") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "eframe") (r "^0.15.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0q42g6fqzv6kmi6aw61wsvv62j9j0j0gjnvrskzfynk8qcynp0b1")))

(define-public crate-netbrowse-0.1.1 (c (n "netbrowse") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "eframe") (r "^0.15.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1y5pc5nspjr7z2p3m220jlab1z4qpf4dy110bpilnljmxkfgy7q6")))

