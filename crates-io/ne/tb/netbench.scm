(define-module (crates-io ne tb netbench) #:use-module (crates-io))

(define-public crate-netbench-0.1.0 (c (n "netbench") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "0dlmdaa9bvhnzkwyjmdadg2fqg9c1hd8fl5zjb0pv2yka540z15f")))

