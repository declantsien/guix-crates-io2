(define-module (crates-io ne td netdog) #:use-module (crates-io))

(define-public crate-netdog-0.1.0 (c (n "netdog") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s2ivj0bcl5mqv9xr5664rnqqasdpxc7ljd01i124ypsdavsd2sb") (y #t)))

(define-public crate-netdog-1.0.0 (c (n "netdog") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pgxal2k445mb0awr48j17cnhgwps36yga5psd5x6bq7y648aqkg")))

