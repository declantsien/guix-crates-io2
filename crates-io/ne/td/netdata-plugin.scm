(define-module (crates-io ne td netdata-plugin) #:use-module (crates-io))

(define-public crate-netdata-plugin-0.2.0 (c (n "netdata-plugin") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "validator") (r "^0.14") (f (quote ("derive"))) (k 0)))) (h "019n914mvjla643rdfcbv0644db7n0gd45z1s190jr8l0s879snl")))

