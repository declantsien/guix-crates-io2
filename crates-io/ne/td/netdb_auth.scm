(define-module (crates-io ne td netdb_auth) #:use-module (crates-io))

(define-public crate-netdb_auth-0.1.0 (c (n "netdb_auth") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "1i8vkyd6sd0dxa6lyhpccc9ij7g4pj6cnki8j723fh5grql9m1kl")))

(define-public crate-netdb_auth-0.1.1 (c (n "netdb_auth") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^9.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "0svryklc7iccpda1vzbff3xykpvr1vwr37yl8si43bl2h3sq8xli")))

(define-public crate-netdb_auth-0.1.2 (c (n "netdb_auth") (v "0.1.2") (d (list (d (n "jsonwebtoken") (r "^9.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "1n2ac9ckmp3nma1cqa7ikn9qy2wwmx255lk1kdz6pygjjz38xgp3")))

