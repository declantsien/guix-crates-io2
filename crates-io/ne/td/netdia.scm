(define-module (crates-io ne td netdia) #:use-module (crates-io))

(define-public crate-netdia-0.0.1 (c (n "netdia") (v "0.0.1") (h "155snnchyd6m5sr37gbcv5d6nxih8r4chzcnapad5brjirq3mxkq")))

(define-public crate-netdia-0.0.2 (c (n "netdia") (v "0.0.2") (h "03m47cviyf8dkr8shsr1f1srn2vspri6044n2hdka9g1zqknr32n")))

