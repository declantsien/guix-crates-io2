(define-module (crates-io ne x- nex-core) #:use-module (crates-io))

(define-public crate-nex-core-0.6.0 (c (n "nex-core") (v "0.6.0") (d (list (d (n "netdev") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03nlqc10xqcfp4g9gxmsvqm0mn7q0z28ipyl1blz5ja02gvl9hpa") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

(define-public crate-nex-core-0.7.0 (c (n "nex-core") (v "0.7.0") (d (list (d (n "netdev") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p9j1d4yph6l3q2zl1yipr97wpb2mnbnlls4bp65iffn67z1m30a") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

(define-public crate-nex-core-0.8.0 (c (n "nex-core") (v "0.8.0") (d (list (d (n "netdev") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zc07k3p9cxbzh7cqj5hn4gljvla1qsjl8apyjvvp6rg4q4aidql") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

(define-public crate-nex-core-0.9.0 (c (n "nex-core") (v "0.9.0") (d (list (d (n "netdev") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00hdhn80b1p7vqi9zi5qg064vax9h7dvxlfsrpdkxcpqfynspjip") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

(define-public crate-nex-core-0.10.0 (c (n "nex-core") (v "0.10.0") (d (list (d (n "netdev") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gf9lislq7dzysy767sngv8l0lfcp1nacygrwm3rz9rx6g2gg608") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

(define-public crate-nex-core-0.11.0 (c (n "nex-core") (v "0.11.0") (d (list (d (n "netdev") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z1b32k0wg9jm2ag1c9scq7b5jpxfc1mk1lq0xaaab0dpij4w95l") (s 2) (e (quote (("serde" "dep:serde" "netdev/serde") ("default" "dep:netdev"))))))

