(define-module (crates-io ne x- nex-macro) #:use-module (crates-io))

(define-public crate-nex-macro-0.6.0 (c (n "nex-macro") (v "0.6.0") (d (list (d (n "nex-macro-helper") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ln5ijsfa54kgc443r74b9pz5jfgzsssp8ppywlfmginzf9vdi35")))

(define-public crate-nex-macro-0.7.0 (c (n "nex-macro") (v "0.7.0") (d (list (d (n "nex-macro-helper") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y3cs04rscdj9jhf2x7v6pxvdbv6q1lkwzjslknkh3ni5qpy5b31")))

(define-public crate-nex-macro-0.8.0 (c (n "nex-macro") (v "0.8.0") (d (list (d (n "nex-macro-helper") (r "^0.8.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rymdm7fsklxzqsgp6fxa3p61jkkkn2fzl8hlqhkk348nx51s2sy")))

(define-public crate-nex-macro-0.9.0 (c (n "nex-macro") (v "0.9.0") (d (list (d (n "nex-macro-helper") (r "^0.9.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wyqpx5nnv7pyrinm6h25i7kdd4rf4pc8x5ddspr8wwk3z7c1kh7")))

(define-public crate-nex-macro-0.10.0 (c (n "nex-macro") (v "0.10.0") (d (list (d (n "nex-macro-helper") (r "^0.10.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mc75p7f70xpggrz5wrs9nrsyisypwagsigy9v6njvdjjxk05882")))

(define-public crate-nex-macro-0.11.0 (c (n "nex-macro") (v "0.11.0") (d (list (d (n "nex-macro-helper") (r "^0.11.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2s4f87va3i4sx7zhr725k3xf0hadmbx97ziph529sry4xnm3y2")))

