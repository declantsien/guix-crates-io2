(define-module (crates-io ne x- nex-macro-helper) #:use-module (crates-io))

(define-public crate-nex-macro-helper-0.6.0 (c (n "nex-macro-helper") (v "0.6.0") (d (list (d (n "nex-core") (r "^0.6.0") (d #t) (k 0)))) (h "0asv3gj56kqanxhnimxd45gz88cbaqi0j638j7cr5w032jb6k47p")))

(define-public crate-nex-macro-helper-0.7.0 (c (n "nex-macro-helper") (v "0.7.0") (d (list (d (n "nex-core") (r "^0.7.0") (d #t) (k 0)))) (h "0vxgs4gqyhfikqb5ja1rjknmbjlipzzb45m5dh0hwgzpsm6pdsnk")))

(define-public crate-nex-macro-helper-0.8.0 (c (n "nex-macro-helper") (v "0.8.0") (d (list (d (n "nex-core") (r "^0.8.0") (d #t) (k 0)))) (h "1bn4aqz90xrxyw1y3aiyi2ynngqm0lk99kx2gssg5slwcgv6jik8")))

(define-public crate-nex-macro-helper-0.9.0 (c (n "nex-macro-helper") (v "0.9.0") (d (list (d (n "nex-core") (r "^0.9.0") (d #t) (k 0)))) (h "02rjv1vqa2glvwb3qbn85hqivi5jadxr1nhcvlxy3vl3y2czipp8")))

(define-public crate-nex-macro-helper-0.10.0 (c (n "nex-macro-helper") (v "0.10.0") (d (list (d (n "nex-core") (r "^0.10.0") (d #t) (k 0)))) (h "013pz6m35by3sblxpfhnx41vnxcyqg9mvylx3nq4dmb493djg44w")))

(define-public crate-nex-macro-helper-0.11.0 (c (n "nex-macro-helper") (v "0.11.0") (d (list (d (n "nex-core") (r "^0.11.0") (d #t) (k 0)))) (h "1v9grx3wj3wybpg3myjw1l0cy79aighzda5h6maar2wymla2sg97")))

