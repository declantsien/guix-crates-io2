(define-module (crates-io ne x- nex-packet-builder) #:use-module (crates-io))

(define-public crate-nex-packet-builder-0.6.0 (c (n "nex-packet-builder") (v "0.6.0") (d (list (d (n "nex-core") (r "^0.6.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jmxln2jfm75l3lqnx4a0g92s0asd6fg2lmap9cdz2q7nfcwqmqj")))

(define-public crate-nex-packet-builder-0.7.0 (c (n "nex-packet-builder") (v "0.7.0") (d (list (d (n "nex-core") (r "^0.7.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vpc0hbmi5a9mn9m4dch07kjm1gl8plrvpjp31azsp7pq6qk1va4")))

(define-public crate-nex-packet-builder-0.8.0 (c (n "nex-packet-builder") (v "0.8.0") (d (list (d (n "nex-core") (r "^0.8.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hf8mxl5w6asplvp39yn4pxa5bcyswqfy9bxq1887hhbxcqif344")))

(define-public crate-nex-packet-builder-0.9.0 (c (n "nex-packet-builder") (v "0.9.0") (d (list (d (n "nex-core") (r "^0.9.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hmzjhl42j02q8729n3pz0bv0bp7x0kzygfan990jh3p0bdh6qxm")))

(define-public crate-nex-packet-builder-0.10.0 (c (n "nex-packet-builder") (v "0.10.0") (d (list (d (n "nex-core") (r "^0.10.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13cx9zrgw18k8c7qy4zvqvkrfrhin0vv9ndyhn9yzbs95cm7qs1p")))

(define-public crate-nex-packet-builder-0.11.0 (c (n "nex-packet-builder") (v "0.11.0") (d (list (d (n "nex-core") (r "^0.11.0") (d #t) (k 0)) (d (n "nex-packet") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "080k4yq9g5l05yfwfqwnjlxkkh32b46jhxkwhjirk8ch044za856")))

