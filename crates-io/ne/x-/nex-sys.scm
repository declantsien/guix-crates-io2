(define-module (crates-io ne x- nex-sys) #:use-module (crates-io))

(define-public crate-nex-sys-0.6.0 (c (n "nex-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sbd7dgzfdcvzgsy5rmm6gi5m0i54q001fqgb5vrj4lgaz6ba3jj")))

(define-public crate-nex-sys-0.7.0 (c (n "nex-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dvr313s94kzk8gwv6zn5nzk77i4qywinhl6d8bawbhd2a9bm5fb")))

(define-public crate-nex-sys-0.8.0 (c (n "nex-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fncaz3ibavlbg31x9c8d5slq3xk4rj33f277pqi1jwm2ix97mhy")))

(define-public crate-nex-sys-0.9.0 (c (n "nex-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17c6d80ws4mbxj1hy57mwwsxwyp14r661y2v6p3pfmyb0q363ahh")))

(define-public crate-nex-sys-0.10.0 (c (n "nex-sys") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ng4aslf0j4zf9pscfv38w8ifmmnipaa6sn96pgwfm0yslkhm5v0")))

(define-public crate-nex-sys-0.11.0 (c (n "nex-sys") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_System_IO" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01r7zbn4fjvr376lzd0ingprl31p77bclw5miyhqb0kmmvbbg6pl")))

