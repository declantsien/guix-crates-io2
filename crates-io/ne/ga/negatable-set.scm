(define-module (crates-io ne ga negatable-set) #:use-module (crates-io))

(define-public crate-negatable-set-0.1.0 (c (n "negatable-set") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "vec-collections") (r "^0.3.3") (o #t) (k 0)) (d (n "vec-collections") (r "^0.3.3") (d #t) (k 2)))) (h "0pgb9ry1mn9yhxjv1vlrmm8ml1mdc5nwz33750lifdz8yxbwd5as") (f (quote (("vc" "vec-collections" "smallvec") ("default" "serde"))))))

