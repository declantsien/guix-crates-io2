(define-module (crates-io ne ga negate) #:use-module (crates-io))

(define-public crate-negate-0.1.0 (c (n "negate") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ib66rmw4yb5rv82dbrazy0qvnqhjn9ssvxp9pg5drnidr9cqcb6")))

(define-public crate-negate-0.1.1 (c (n "negate") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0lsffa60zk13il0f6qggxr62fa2mzin8diiqr7i4xrc4xnvsj4in")))

