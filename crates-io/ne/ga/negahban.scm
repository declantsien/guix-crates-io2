(define-module (crates-io ne ga negahban) #:use-module (crates-io))

(define-public crate-negahban-0.1.0 (c (n "negahban") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "0an9cdv2lrdw5h38hwbfaivh0rmaii8i4gc4phyvs7ang2mggb6l") (y #t) (r "1.67.0")))

(define-public crate-negahban-0.2.0 (c (n "negahban") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "0nc94kj8jcwkzp71fjx49c1g0yhkf1wh4j6yvkmbr0vh5fqaznfr") (y #t) (r "1.67.0")))

(define-public crate-negahban-0.2.1 (c (n "negahban") (v "0.2.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "0nirjwbq8cg5drdqqlvdyivba4b5m7ji4hj9zd1b6qgcyb24bzg6") (r "1.67.0")))

(define-public crate-negahban-0.2.2 (c (n "negahban") (v "0.2.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "1hp4q0dzkcp1hwxhdic3msprg3dvc0hfjdi5giax344z59i7ygi2") (r "1.67.0")))

(define-public crate-negahban-0.2.3 (c (n "negahban") (v "0.2.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "1593jdlpdx0skf2c3lc78g1r0iy8vi6lx4ngk07zyqgf4ardsy1f") (r "1.67.0")))

(define-public crate-negahban-0.2.4 (c (n "negahban") (v "0.2.4") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "0lc7s3y4c7yf22bic3ciddlypswszjfnbxx9mjhv0r7fx41lwzm6") (r "1.67.0")))

(define-public crate-negahban-0.2.5 (c (n "negahban") (v "0.2.5") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "0ljfwpw9232gm9pfdl3wylfchh5xvilpglm2gjycjjsvd1ai7w95") (r "1.67.0")))

(define-public crate-negahban-0.3.0 (c (n "negahban") (v "0.3.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "1rjkib994n7mkl9s0kqywxwcn4ysy2knd146w0whgqmhjbw0braf") (y #t) (r "1.67.0")))

(define-public crate-negahban-0.3.1 (c (n "negahban") (v "0.3.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (f (quote ("mio"))) (d #t) (k 0)))) (h "19gi9wx86rc3cipahgql24fm9wnpcxrl2va5lxrh8p8qikqrx9vn") (r "1.67.0")))

