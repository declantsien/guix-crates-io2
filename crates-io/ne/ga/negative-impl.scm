(define-module (crates-io ne ga negative-impl) #:use-module (crates-io))

(define-public crate-negative-impl-0.0.0 (c (n "negative-impl") (v "0.0.0") (h "0cl42vlp5c1wz88ypfz18d8llpma2jbfjbjnqwxv0i6lwxvqf27h") (y #t)))

(define-public crate-negative-impl-0.1.0 (c (n "negative-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1wn9qsm96a6ljqb9as1pzraf9mvd5vwmqcplk1l11c931qmbbvhh")))

(define-public crate-negative-impl-0.1.1 (c (n "negative-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0r1dd4l1vyw89d7zaicw90cairb7hxd1i0866y9qagqzwpf7qsl7") (r "1.37")))

(define-public crate-negative-impl-0.1.2 (c (n "negative-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0d4871npz7jyyq4yl5nbv3s2xsy9w4f62rkcw6g3xva61a9glr56") (r "1.37")))

(define-public crate-negative-impl-0.1.3 (c (n "negative-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n509fi7cndmzqbq4q9ygwk8bmp73x90p2z6zkifcjn5kcgrri2v") (r "1.56")))

(define-public crate-negative-impl-0.1.4 (c (n "negative-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5pbwayrbbqnrprvnyfjj0p81wliwmxq45kylnjk2akjbhac7v8") (r "1.56")))

(define-public crate-negative-impl-0.1.5 (c (n "negative-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "05hmd37c304szyxvwjni60mifzm3qby1bqwjbxlds0jhd737d4xd") (r "1.56")))

