(define-module (crates-io ne or neorg-dirman) #:use-module (crates-io))

(define-public crate-neorg-dirman-0.1.0 (c (n "neorg-dirman") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)))) (h "0wfbizp1nmx6s7rnfhmdl7dnpn3v39cyaldiz3hsiyrapkw1h4j9")))

(define-public crate-neorg-dirman-0.1.1 (c (n "neorg-dirman") (v "0.1.1") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)))) (h "1yw6sa9ci8lpm1sfn859liqm0iiqivi2fjxpja10qi150sgrvmxs")))

