(define-module (crates-io ne or neorg-language-server) #:use-module (crates-io))

(define-public crate-neorg-language-server-0.1.0 (c (n "neorg-language-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.4") (d #t) (k 0)) (d (n "lsp-types") (r "^0.95.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-norg") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0h9pih4j9wyh2jcacqh4g4381w9lf6hj5jp8y8yavh4rb04hhrql")))

