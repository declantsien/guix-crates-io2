(define-module (crates-io ne tu netutils) #:use-module (crates-io))

(define-public crate-netutils-0.1.0 (c (n "netutils") (v "0.1.0") (h "0z966dy2hj2sxvip2ik2g3v5yhxjaxfd5zd276x6r8xrl8y4jpzw")))

(define-public crate-netutils-0.1.1 (c (n "netutils") (v "0.1.1") (h "12zb24m33xd4krsfh0iy5qvxvb9zaqv6vfwv0khzhnaj6yh89mw1")))

