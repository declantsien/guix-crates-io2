(define-module (crates-io ne tu netu) #:use-module (crates-io))

(define-public crate-netu-0.1.0 (c (n "netu") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "igd") (r "^0.12") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0arfp1910g3x77pnza24r2h11x1ljqr3s9lpwc6ggq1w4ggsr8m4") (f (quote (("async" "tokio" "igd/aio"))))))

(define-public crate-netu-0.1.1 (c (n "netu") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "igd") (r "^0.12") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0acd82yi8lympqjls8b3l5v40r0187s9xhpsb9rgxhl866y6kd2j") (f (quote (("async" "tokio" "igd/aio"))))))

(define-public crate-netu-0.2.0 (c (n "netu") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "igd") (r "^0.12") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "00zddashybhhk5j9f4f5ikxsax7ffhlpvi0dym87jyxbfc8av5wn") (f (quote (("async" "tokio" "igd/aio"))))))

