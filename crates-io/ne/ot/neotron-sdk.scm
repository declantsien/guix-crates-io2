(define-module (crates-io ne ot neotron-sdk) #:use-module (crates-io))

(define-public crate-neotron-sdk-0.1.0 (c (n "neotron-sdk") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (t "cfg(windows)") (k 0)) (d (n "neotron-api") (r "^0.2") (d #t) (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)))) (h "05pd7cavxmk1vcl1qgsjpmwd4ckdpxfw661wr5f1dcdl0d6qmfaf") (f (quote (("fancy-panic"))))))

(define-public crate-neotron-sdk-0.2.0 (c (n "neotron-sdk") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (t "cfg(windows)") (k 0)) (d (n "neotron-api") (r "^0.2") (d #t) (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)))) (h "0cg3bnmsbrpy0apmawr8zcpxxflq2h6mmj82pznfpdj254hdchr0") (f (quote (("fancy-panic"))))))

