(define-module (crates-io ne ot neotron-os) #:use-module (crates-io))

(define-public crate-neotron-os-0.1.0 (c (n "neotron-os") (v "0.1.0") (d (list (d (n "neotron-common-bios") (r "^0.1.0") (d #t) (k 0)) (d (n "postcard") (r "^0.5") (d #t) (k 0)) (d (n "r0") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0kwnx6v0bn8slfd982xalh7irq9k76mkmgxk7g71yk2xdxri162f")))

