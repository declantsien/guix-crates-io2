(define-module (crates-io ne ot neotest-docker-phpunit) #:use-module (crates-io))

(define-public crate-neotest-docker-phpunit-0.1.0 (c (n "neotest-docker-phpunit") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "04wp5w2gzcpszn96q7s4a3gm0yz13v7jsxmfp0qfig24m91cmdrf")))

(define-public crate-neotest-docker-phpunit-0.1.1 (c (n "neotest-docker-phpunit") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1mcnll7v6b2807f6gfcq1i1ki5i5539blxywh2i8wp388sbb8yxr")))

(define-public crate-neotest-docker-phpunit-0.1.2 (c (n "neotest-docker-phpunit") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1xliw042av845xxfdrkzba5vpi7ig509d0rrbh5jcs5zzr7yg6pz")))

