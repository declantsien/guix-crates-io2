(define-module (crates-io ne ot neotron-api) #:use-module (crates-io))

(define-public crate-neotron-api-0.1.0 (c (n "neotron-api") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)))) (h "0bz86n5y7xa744nvadv1v3vxdpchdf356yp6088441kh5s20zh42")))

(define-public crate-neotron-api-0.2.0 (c (n "neotron-api") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)))) (h "14y4w7hqxhhbgi00z5541yc03myjvz545c5zk83frwxn0rkwkmk7")))

