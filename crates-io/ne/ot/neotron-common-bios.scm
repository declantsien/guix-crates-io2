(define-module (crates-io ne ot neotron-common-bios) #:use-module (crates-io))

(define-public crate-neotron-common-bios-0.1.0 (c (n "neotron-common-bios") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0wyffj02lz9c9706iq26d9c7vp2li05qhqnhnyspladqhv3y8xcb")))

(define-public crate-neotron-common-bios-0.4.0 (c (n "neotron-common-bios") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0fzgabkijsshlapxxkrfxz8lwf3arddinygqrzk0szvgxlzjlzi5")))

(define-public crate-neotron-common-bios-0.5.0 (c (n "neotron-common-bios") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "17whmgds453b9z6a2pv5gsrpfkg1zmbq4kjk6j2nqsmgyfy248mh")))

(define-public crate-neotron-common-bios-0.6.0 (c (n "neotron-common-bios") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1zgv43wx5xwdz0mxfd2bwhn8qmig31g9cpgrpljci0674mkbk8i8") (y #t)))

(define-public crate-neotron-common-bios-0.6.1 (c (n "neotron-common-bios") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0vykadvpdlhs8mndgj0112y0djx0c38m6y5x10xd8avha6rrp4cr")))

(define-public crate-neotron-common-bios-0.7.0 (c (n "neotron-common-bios") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "029h48p244b03n8c4siz3la010dlflsd87pwp0v2l08qka9xm02l")))

(define-public crate-neotron-common-bios-0.8.0 (c (n "neotron-common-bios") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "0jxrc5f1khfvijg2ricr1qyn21ndm08zvfp221hvd0d8cvpl6jzs")))

(define-public crate-neotron-common-bios-0.9.0 (c (n "neotron-common-bios") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "1qpkz69v6gl8ad4cpcycx6lwsmdirikk85ziz5pq27zzplaxrc7a")))

(define-public crate-neotron-common-bios-0.10.0 (c (n "neotron-common-bios") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "1m5ar3x33k9ycmsk5vps3j4wgrgd0b2pmk91v22c68g5y2121zcj")))

(define-public crate-neotron-common-bios-0.11.0 (c (n "neotron-common-bios") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "0fmd9hadha46glp37rkycdbcvq5qafgka5n544izfqf7sa6dknls")))

(define-public crate-neotron-common-bios-0.11.1 (c (n "neotron-common-bios") (v "0.11.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "0xn27dk07z5g2fma3dispmvi8xmhcbzrqjfvhwjgd9kbnl9nq4kq")))

(define-public crate-neotron-common-bios-0.12.0 (c (n "neotron-common-bios") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "neotron-ffi") (r "^0.1") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.7") (d #t) (k 0)))) (h "005dz45km68zpgz81634f8jx6l1bd81crr8pk66xdv5xlhcv7z94")))

