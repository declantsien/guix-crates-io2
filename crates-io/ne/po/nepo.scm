(define-module (crates-io ne po nepo) #:use-module (crates-io))

(define-public crate-nepo-0.1.0 (c (n "nepo") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "17npzkp8a5w6h0cp6zmccy5p89g132i98gwd2mjf9j16inazh146")))

(define-public crate-nepo-0.1.1 (c (n "nepo") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "1m978zzg2sk019wsgkxp9ng5zs9jvi6y3mjilhpy6msxsy1kgfxp")))

