(define-module (crates-io ne ta netaddr) #:use-module (crates-io))

(define-public crate-netaddr-0.1.0 (c (n "netaddr") (v "0.1.0") (h "0w3b4bfwq3m589d6mfa1rwhkg1rjb6bk5pjhfjrfisv98z550zjp")))

(define-public crate-netaddr-0.1.1 (c (n "netaddr") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0afyq5bgj156n3wx0s1z09lmdfbwy8scpyvn1zkmk2i5gldwprzq")))

(define-public crate-netaddr-0.1.2 (c (n "netaddr") (v "0.1.2") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fnnvk86mi5sg8wfdimdjs9lawaywl1lbaryj35yyrwqpdzcan6p")))

