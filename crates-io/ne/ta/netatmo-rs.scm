(define-module (crates-io ne ta netatmo-rs) #:use-module (crates-io))

(define-public crate-netatmo-rs-0.0.1 (c (n "netatmo-rs") (v "0.0.1") (h "0s5cxmacy42k71p68n0dz5a666dasssv9x8kyfhn56mmcgmpwc1l")))

(define-public crate-netatmo-rs-0.1.0 (c (n "netatmo-rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1akbyj85r7ls6bd1al16ynwy4gqiihhl0mlf2r529v7zn0jynrjh") (f (quote (("cli"))))))

(define-public crate-netatmo-rs-0.2.0 (c (n "netatmo-rs") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0ig1f6ha6wgm1c611x418dx94cj7mndrmj1dvanz5px3ylpwn55m")))

(define-public crate-netatmo-rs-0.2.1 (c (n "netatmo-rs") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1c2asxfwjd61rwn52nh4m9r7ppibiqds8c6n8vp6k92l44015s3s")))

(define-public crate-netatmo-rs-0.3.0 (c (n "netatmo-rs") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0jqjj9cv6x57509xjwhhy64h4ac14b88ljwk2fp7mmifm36jianf")))

(define-public crate-netatmo-rs-0.4.0 (c (n "netatmo-rs") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0kx419pf7clfv5mzgw1cz5iw17fn100lnkfkjx0gzp6vwnsp4mpb")))

(define-public crate-netatmo-rs-0.5.0 (c (n "netatmo-rs") (v "0.5.0") (d (list (d (n "env_logger") (r "0.7.*") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1g9jdgn5mywzicb3k85ykcnxdq1awb34wvm6ysi39fih9lgr0w6a")))

