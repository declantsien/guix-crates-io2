(define-module (crates-io ne ta netatmo-api-rs) #:use-module (crates-io))

(define-public crate-netatmo-api-rs-0.0.1 (c (n "netatmo-api-rs") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nml7fw0dlkn72r6plcj4msilwqlknzsg2730cd4b6h2yg013j9r")))

