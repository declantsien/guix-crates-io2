(define-module (crates-io ne ta netapi32-sys) #:use-module (crates-io))

(define-public crate-netapi32-sys-0.0.1 (c (n "netapi32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0709hfgi6vzsgmg4kzhjkb3g520yn9zfmxxdvq9jhq0i1amnxpxb")))

(define-public crate-netapi32-sys-0.2.0 (c (n "netapi32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1ps1vlnkxv94lha1ls56mach45ihkq9m0icqimnmprq9a4hwimz4")))

