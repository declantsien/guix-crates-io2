(define-module (crates-io ne wi newick-rs) #:use-module (crates-io))

(define-public crate-newick-rs-0.1.0 (c (n "newick-rs") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q595wk1pc9grr7acm2w7filpbg2mf97g031jywi5a3mlldcqj6x")))

(define-public crate-newick-rs-0.2.0 (c (n "newick-rs") (v "0.2.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5mb0a8ni85qr7vwp7yc6yip9s4rd6zadsy70v6lr4dcqkqqkqh")))

