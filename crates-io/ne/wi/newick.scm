(define-module (crates-io ne wi newick) #:use-module (crates-io))

(define-public crate-newick-0.5.0 (c (n "newick") (v "0.5.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ckp8n3yidq9w99zvafr26mdcc9xxxas1k5rsivj6knw9amnl8mv")))

(define-public crate-newick-0.6.1 (c (n "newick") (v "0.6.1") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0avmkkgmsr1wf7qzq73m57fqcamc0cvfvzdl1vmgj6xxpw79zw50")))

(define-public crate-newick-0.7.0 (c (n "newick") (v "0.7.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h82ss92s7dm6875vbigjzcmncf0cwz0gpvzgvb0m65vvbz8zml6")))

(define-public crate-newick-0.7.1 (c (n "newick") (v "0.7.1") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dzssj2n00vz7admra3m5qm181aadx7w83c1s5r70gjqdj2p2k9x")))

(define-public crate-newick-0.7.2 (c (n "newick") (v "0.7.2") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d1h3ks2l3qcr44hwv15hbw5qnilqqwa5wvpdvi1q0sq41xxf1ns")))

(define-public crate-newick-0.8.0 (c (n "newick") (v "0.8.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07l6scbgn5hp9xhzf7ym1iljla6zhab11g3pxpz9dkv4rvbixv77")))

(define-public crate-newick-0.9.0 (c (n "newick") (v "0.9.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c5dd5h89ydxz493gddv8d4jzdw87npsl1v5linjlw53g0r38clg")))

(define-public crate-newick-0.10.0 (c (n "newick") (v "0.10.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bfvyl9f2axbnmpbyrrj4zyr0rab3ag4vklk9bhh27mfvcr9r512")))

(define-public crate-newick-0.11.0 (c (n "newick") (v "0.11.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "sorbus") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "034w2qqxpssz8agxlgva2l1dd2s40j4lsl7xx0qihlxd49qad6np")))

