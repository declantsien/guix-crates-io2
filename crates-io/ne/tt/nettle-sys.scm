(define-module (crates-io ne tt nettle-sys) #:use-module (crates-io))

(define-public crate-nettle-sys-0.1.0 (c (n "nettle-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0v9z170gawfb6i75xlq2bvjhrfpdpsp5gb79czdgpi17x81awnc3")))

(define-public crate-nettle-sys-1.0.0 (c (n "nettle-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09wbmi0fh4xymfwcb5aia4a2cq0vyvjvx5f7xllbxgr2kqbxhci7")))

(define-public crate-nettle-sys-1.0.1 (c (n "nettle-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nb0hizkk0h2ak7ry36b2s143gbi6wljg55maxlb5ipzwv41hcdw")))

(define-public crate-nettle-sys-1.0.2 (c (n "nettle-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r3l6k2s2d7cqmglwk07an1gzwrnzm9vn31vvy756g5ngnk4cdik")))

(define-public crate-nettle-sys-1.0.3 (c (n "nettle-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lpwnavbvwmim1blkz4jl2d07y670kqlk0ydaf3yavdfs6b4hbv0")))

(define-public crate-nettle-sys-2.0.0 (c (n "nettle-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00lbw0li4vzx50iybf9cvj19idyj77jrg7nrwf3lcgnhw7g2v7h5")))

(define-public crate-nettle-sys-2.0.1 (c (n "nettle-sys") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pg9hyrg9c3r62nwj6bl89qwdn59qsm9p1zrdmfkv6fnbdcsrv9k")))

(define-public crate-nettle-sys-2.0.2 (c (n "nettle-sys") (v "2.0.2") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mn2vv4xkff80sr4m7bln1f375fkk94lzgl0zlki1y677b8phbds") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.3 (c (n "nettle-sys") (v "2.0.3") (d (list (d (n "bindgen") (r "^0.47") (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0clz04m282wycdnxs9w95djjq0mc5dk5ks62ly6kyd2yipld7c8z") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.4 (c (n "nettle-sys") (v "2.0.4") (d (list (d (n "bindgen") (r "^0.51.1") (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yq1w6dlcmg89x529i7s20j29afdhgim7qnsa7978fszzwrr6qmq") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.5 (c (n "nettle-sys") (v "2.0.5") (d (list (d (n "bindgen") (r ">=0.53.1, <0.56") (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1kjw2rnw4ww5vygrp1y486xg55xzxznc3yz7bz7hrd24mc4cj2i9") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.6 (c (n "nettle-sys") (v "2.0.6") (d (list (d (n "bindgen") (r ">=0.53.1, <0.58.0") (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1pcfcz1gb19kp8qmbgcw30q4dbd1zfzdk22plxa5mfclmjql8n9m") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.7 (c (n "nettle-sys") (v "2.0.7") (d (list (d (n "bindgen") (r ">=0.53.1, <0.58.0") (f (quote ("runtime"))) (k 1)) (d (n "nettle-src") (r "^3.5.1-0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "11r16g92f5sf9a57khhs76ibq7r54az1x14nbin4g0dq64k40mjq") (f (quote (("vendored" "nettle-src"))))))

(define-public crate-nettle-sys-2.0.8 (c (n "nettle-sys") (v "2.0.8") (d (list (d (n "bindgen") (r ">=0.53.1, <0.58.0") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "09jjwyvrl2rhgb47400jinndqvjjdr6fgsnw84p17n68c6ggynmr") (l "nettle")))

(define-public crate-nettle-sys-2.1.0 (c (n "nettle-sys") (v "2.1.0") (d (list (d (n "bindgen") (r ">=0.53.1, <0.58.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "05rdwqzkv83wvrby2x1xwjn3gv2795cwxwycjqhs7qw3g1f6hfxi") (l "nettle")))

(define-public crate-nettle-sys-2.1.1 (c (n "nettle-sys") (v "2.1.1") (d (list (d (n "bindgen") (r ">=0.58.0, <0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0asjk74cpqv099xyzfv2ws34ij28qqg8sc9ibw2rfaaximzcvxsx") (l "nettle")))

(define-public crate-nettle-sys-2.2.0 (c (n "nettle-sys") (v "2.2.0") (d (list (d (n "bindgen") (r ">=0.58.0, <0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0wwa7pmxdz7yl9jwybml2kmrj3i87jcn0h0cdc5xl0lhgcs1rs5m") (l "nettle")))

(define-public crate-nettle-sys-2.3.0 (c (n "nettle-sys") (v "2.3.0") (d (list (d (n "bindgen") (r ">=0.58.0, <0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.9") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1v36is0kygwc4rcdqnszgdwm14z2j8p23wbblbiq16m120x0b5dl") (l "nettle")))

