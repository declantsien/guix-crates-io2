(define-module (crates-io ne tt nettu_scheduler_api_structs) #:use-module (crates-io))

(define-public crate-nettu_scheduler_api_structs-0.1.0 (c (n "nettu_scheduler_api_structs") (v "0.1.0") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02n2fx6fs7vxyqrprnam306j2q71xxnshrr3v0gvafyv5cbbs0ip")))

(define-public crate-nettu_scheduler_api_structs-0.1.1 (c (n "nettu_scheduler_api_structs") (v "0.1.1") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mackhny7lrb3icha2ix0fgyxjm4gjqbmw9jfjz8kxxk05i0kcmd")))

(define-public crate-nettu_scheduler_api_structs-0.1.2 (c (n "nettu_scheduler_api_structs") (v "0.1.2") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17n10i37sys42cc1m4h5az701c67skp5db5hdrxx6p3z7pv5wlfz")))

(define-public crate-nettu_scheduler_api_structs-0.1.3 (c (n "nettu_scheduler_api_structs") (v "0.1.3") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rxwfdwbvnm5z28xw7ybsi7dnpxgdvxx0ylbj8j6m7i7fpjxc218")))

(define-public crate-nettu_scheduler_api_structs-0.1.4 (c (n "nettu_scheduler_api_structs") (v "0.1.4") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ii43mdwqjc4y8bcj462hmf0z6xck3jk63myl85y4v96gpy1sqdh")))

(define-public crate-nettu_scheduler_api_structs-0.1.5 (c (n "nettu_scheduler_api_structs") (v "0.1.5") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zi7fqph9dp8nm8szfw4p88fcwb7djyd4c9p9p4lz3h11c0h3dqb")))

(define-public crate-nettu_scheduler_api_structs-0.1.6 (c (n "nettu_scheduler_api_structs") (v "0.1.6") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xs2c3ywhs1k873970yzr6gqa3z0gas380gxc9j7s04037q92zzn")))

(define-public crate-nettu_scheduler_api_structs-0.1.7 (c (n "nettu_scheduler_api_structs") (v "0.1.7") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nmgyaz3q94k9c9kq9g0r82gzfxzfwxkxfl8mklwsdgfq4acj2d3")))

(define-public crate-nettu_scheduler_api_structs-0.1.8 (c (n "nettu_scheduler_api_structs") (v "0.1.8") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19cr6vziawx243cvw14c254gbvbizcwzxhpqnbnyjpig97dz6iy2")))

(define-public crate-nettu_scheduler_api_structs-0.1.9 (c (n "nettu_scheduler_api_structs") (v "0.1.9") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aaavwyiql32pax0psbl0qy1iwqhxdrfmpm4rp9hpjvfpwwni58i")))

(define-public crate-nettu_scheduler_api_structs-0.2.0 (c (n "nettu_scheduler_api_structs") (v "0.2.0") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xgl9xblsljprj47dbw7hag7rbzq4w0arkb6mx63fh7gjz3c8zdk")))

(define-public crate-nettu_scheduler_api_structs-0.2.1 (c (n "nettu_scheduler_api_structs") (v "0.2.1") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s8mb3h3n39n4a8iz7mj8m3dn8i1hjgp62yk52fx0hwjs8qpbsyg")))

(define-public crate-nettu_scheduler_api_structs-0.2.2 (c (n "nettu_scheduler_api_structs") (v "0.2.2") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dnylp7l3qbnm065zmhp7qniwfflqrc0cykgc61qhqkakp4hdp2j")))

(define-public crate-nettu_scheduler_api_structs-0.2.3 (c (n "nettu_scheduler_api_structs") (v "0.2.3") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15hf9sv39hmbymxl5z2ir7nczlwkdggbx7w7sa7zvxznrr6mj28y")))

(define-public crate-nettu_scheduler_api_structs-0.2.4 (c (n "nettu_scheduler_api_structs") (v "0.2.4") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ixkfibrqh481w4fj9d0h60sk56cgas1hfs5za0iym4ggm8p9j5p")))

(define-public crate-nettu_scheduler_api_structs-0.2.5 (c (n "nettu_scheduler_api_structs") (v "0.2.5") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15ndsvj5f3w4whlzzdzy86kifdmrwcrbfn6mwxws17784my9h32k")))

(define-public crate-nettu_scheduler_api_structs-0.2.6 (c (n "nettu_scheduler_api_structs") (v "0.2.6") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z7h5iq9pigjvc1zcyzgk7qnqac1214rhfzp0qq5z3rmswmrbs3q")))

(define-public crate-nettu_scheduler_api_structs-0.2.7 (c (n "nettu_scheduler_api_structs") (v "0.2.7") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06dxha4bskvmx7z7qxrawwwajy4bif2nvj8hyf5bb2ija2bvj9h9")))

(define-public crate-nettu_scheduler_api_structs-0.2.8 (c (n "nettu_scheduler_api_structs") (v "0.2.8") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "174i3zczlikbrgm6kqd0n32p6shz8l62pmv9fmz4hhm54qfzwzkf")))

(define-public crate-nettu_scheduler_api_structs-0.2.9 (c (n "nettu_scheduler_api_structs") (v "0.2.9") (d (list (d (n "nettu_scheduler_domain") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0da5isqsmarkrvxvsl2jkwrz7218yivp3gradyci8i25i5ijd8dq")))

(define-public crate-nettu_scheduler_api_structs-0.3.0 (c (n "nettu_scheduler_api_structs") (v "0.3.0") (d (list (d (n "nettu_scheduler_domain") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kl9lcwxrkj7k0lm5z8ydmawvq8gxzmdddb1z0yfw6jcnzcy45q2")))

(define-public crate-nettu_scheduler_api_structs-0.3.1 (c (n "nettu_scheduler_api_structs") (v "0.3.1") (d (list (d (n "nettu_scheduler_domain") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l9l3xlnsdzlj550crx7fk0lh3km2drjwh49dn53x6ink0v59pcw")))

(define-public crate-nettu_scheduler_api_structs-0.4.3 (c (n "nettu_scheduler_api_structs") (v "0.4.3") (d (list (d (n "nettu_scheduler_domain") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19x0d79yvlj2jrcd0na8f5ppx973yc0sfjnb4qgmycr51gk3yp7b")))

(define-public crate-nettu_scheduler_api_structs-0.4.7 (c (n "nettu_scheduler_api_structs") (v "0.4.7") (d (list (d (n "nettu_scheduler_domain") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kk4k6xqdd3fa5la0zyj1lw2fq7wqj17vzkxk7j6sv9hzdc4j4vx")))

(define-public crate-nettu_scheduler_api_structs-0.5.5 (c (n "nettu_scheduler_api_structs") (v "0.5.5") (d (list (d (n "nettu_scheduler_domain") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iz0z7jmb8r0izs3c9nmq5lhyv7jqsfp09f0klr0v7xsyffn279n")))

(define-public crate-nettu_scheduler_api_structs-0.5.7 (c (n "nettu_scheduler_api_structs") (v "0.5.7") (d (list (d (n "nettu_scheduler_domain") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xjg4nmxv06bkx9fmrj735vh1rcndw407bm33f9qpj3yvxxjh3qc")))

(define-public crate-nettu_scheduler_api_structs-0.5.9 (c (n "nettu_scheduler_api_structs") (v "0.5.9") (d (list (d (n "nettu_scheduler_domain") (r "^0.5.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zrpm6pirsrhzppwmbwanzsim8h0y7m1s5d59pgs5cf7k5vipyz2")))

(define-public crate-nettu_scheduler_api_structs-0.6.0 (c (n "nettu_scheduler_api_structs") (v "0.6.0") (d (list (d (n "nettu_scheduler_domain") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r7pdnfah3jkg9c4mj0lm2lmsfyq3gwbbgyvawd8r5rk3h3h3av0")))

