(define-module (crates-io ne tt nettu_scheduler_utils) #:use-module (crates-io))

(define-public crate-nettu_scheduler_utils-0.1.0 (c (n "nettu_scheduler_utils") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1vragcdj3ghq09lnsxys4mx9y9fbc7iw85brsx70i24gx20jwapf")))

(define-public crate-nettu_scheduler_utils-0.4.3 (c (n "nettu_scheduler_utils") (v "0.4.3") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0r6bxy2vdf1i8wydr7mx9mjb50wrj1593cn0p4pl3jrdg5y383kf")))

(define-public crate-nettu_scheduler_utils-0.4.7 (c (n "nettu_scheduler_utils") (v "0.4.7") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0r7jvkkdmmma04kxg2rsc57lr5p6k5ad9nfpxja09xvj1kwq4xkb")))

(define-public crate-nettu_scheduler_utils-0.5.4 (c (n "nettu_scheduler_utils") (v "0.5.4") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "12dgiinfxa62wmvmzdv2h7xy6r7k7x82cz6c9x2lmyq9wi70dzpg")))

(define-public crate-nettu_scheduler_utils-0.5.5 (c (n "nettu_scheduler_utils") (v "0.5.5") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "12y0s4yng9sjg2h58v1rq1q591c12py014wll6pyi4bl8522lcwn")))

(define-public crate-nettu_scheduler_utils-0.5.7 (c (n "nettu_scheduler_utils") (v "0.5.7") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1yqqd4bh31vix9bdddm70chxwr42z4ppx21ycjf9bqbfz8si94w2")))

(define-public crate-nettu_scheduler_utils-0.5.9 (c (n "nettu_scheduler_utils") (v "0.5.9") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1x2ifrs5kh6bx447k2kx3wvi1s7qwryfl663zyrlbnh49q850aiz")))

(define-public crate-nettu_scheduler_utils-0.6.0 (c (n "nettu_scheduler_utils") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "17wvn0fjw5xm9869sxb6ql7l7cb7sl8v7i46if4y0pdk1wb38zlv")))

