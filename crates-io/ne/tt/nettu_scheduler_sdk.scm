(define-module (crates-io ne tt nettu_scheduler_sdk) #:use-module (crates-io))

(define-public crate-nettu_scheduler_sdk-0.1.0 (c (n "nettu_scheduler_sdk") (v "0.1.0") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.0") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0rg8zpkpqid67688g37nz23wlnxszpa9hj3jl5raggx8af3p75ka")))

(define-public crate-nettu_scheduler_sdk-0.1.1 (c (n "nettu_scheduler_sdk") (v "0.1.1") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.1") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0329h4amf9fdxhrclvxa8pj9dbb61z8867qy9ndsbhx4nzqw9qfp")))

(define-public crate-nettu_scheduler_sdk-0.1.3 (c (n "nettu_scheduler_sdk") (v "0.1.3") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.2") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1llg0hhllb0rj3rxqsv87lvvklj95agg48ji4kr4abkkj9zy53xv")))

(define-public crate-nettu_scheduler_sdk-0.1.4 (c (n "nettu_scheduler_sdk") (v "0.1.4") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.3") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1nf521yky9b21cp413j1c51l1bin6qhw315xfpa7d5snsyz0cxm3")))

(define-public crate-nettu_scheduler_sdk-0.1.5 (c (n "nettu_scheduler_sdk") (v "0.1.5") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.4") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0641n5fx594h2g5l6l0pmqv7fz6hxh8bx9qjw3k9fzgi5c7if9l8")))

(define-public crate-nettu_scheduler_sdk-0.1.6 (c (n "nettu_scheduler_sdk") (v "0.1.6") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.5") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "062gq3nvy11wfv0pqlqzdb4yaawzmp8l1wix4drb28q2ypcvy5v9")))

(define-public crate-nettu_scheduler_sdk-0.1.7 (c (n "nettu_scheduler_sdk") (v "0.1.7") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.6") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0rkp47jj4n69rfhi47xcxynhvn3z97013vvjf0v58v8qikfzyp0s")))

(define-public crate-nettu_scheduler_sdk-0.1.8 (c (n "nettu_scheduler_sdk") (v "0.1.8") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1376n3dmj3cnzfdw8gczq24kyasif4qw3jwh3nx1kfl7p0dvr5wi")))

(define-public crate-nettu_scheduler_sdk-0.1.9 (c (n "nettu_scheduler_sdk") (v "0.1.9") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.1.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1klprhjrh9nd87rv26bl92bz5j4dpva4bp0rx6wbn6mfrbpyzxl8")))

(define-public crate-nettu_scheduler_sdk-0.2.0 (c (n "nettu_scheduler_sdk") (v "0.2.0") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.0") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1ldvspkkdgsc94x0acbsq22rw9xz09zwvwpj6s2ygbisajcrigg3")))

(define-public crate-nettu_scheduler_sdk-0.2.1 (c (n "nettu_scheduler_sdk") (v "0.2.1") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.1") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0rngz1vlp6qy7jwv32ivlai1xmlh3a18d3xzmwvlkjddlf6ir1j4")))

(define-public crate-nettu_scheduler_sdk-0.2.2 (c (n "nettu_scheduler_sdk") (v "0.2.2") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.2") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0y96kk822l8i4g66z7xbjqm0y5dhlm6dzkgsdwygwlzsag5l21vv")))

(define-public crate-nettu_scheduler_sdk-0.2.3 (c (n "nettu_scheduler_sdk") (v "0.2.3") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.3") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "02d3s23ydmibdy6i60qnk7j87dddsk8wfyi947d3fhbkvrmf55w9")))

(define-public crate-nettu_scheduler_sdk-0.2.4 (c (n "nettu_scheduler_sdk") (v "0.2.4") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.4") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0f7alm6w0yj9k494zjvhkm2rn9fx1ddnhgjbncym8017n9gw5qbr")))

(define-public crate-nettu_scheduler_sdk-0.2.5 (c (n "nettu_scheduler_sdk") (v "0.2.5") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.4") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0k85znja8ql04jazm1hdlpmx6fg4x9qy0xzzhdch6rj0gm9vxy01")))

(define-public crate-nettu_scheduler_sdk-0.2.6 (c (n "nettu_scheduler_sdk") (v "0.2.6") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.5") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0v48f4gci8637z08pfjm7yssf0y6zl9mbp7g8kxwh2d1fljs2fnd")))

(define-public crate-nettu_scheduler_sdk-0.2.7 (c (n "nettu_scheduler_sdk") (v "0.2.7") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.5") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "18a42h0pz498fbz2409ag2ba5b1265955pwqmigzxl3vj6c1z5f3")))

(define-public crate-nettu_scheduler_sdk-0.2.8 (c (n "nettu_scheduler_sdk") (v "0.2.8") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.6") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0bzi5nqzp9faxd39dy5sfxq0v5vz146dd5n1hv4yaax2w44a289x")))

(define-public crate-nettu_scheduler_sdk-0.2.9 (c (n "nettu_scheduler_sdk") (v "0.2.9") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.7") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0275ia4y83ys650lbqsad1yijaw6gjik0w336car5aa9cz4v24z8")))

(define-public crate-nettu_scheduler_sdk-0.3.0 (c (n "nettu_scheduler_sdk") (v "0.3.0") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.8") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0j84nph0n89bs4gn6dmvmfm3cqr819fsamjd8abd8g05j7j3li9k")))

(define-public crate-nettu_scheduler_sdk-0.3.1 (c (n "nettu_scheduler_sdk") (v "0.3.1") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.8") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "097a3jhpw3yh9g1flazgzgmd3b8k0mkk5isfs7zwwdzhahica3lj")))

(define-public crate-nettu_scheduler_sdk-0.3.2 (c (n "nettu_scheduler_sdk") (v "0.3.2") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.8") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0903q9if0zc7kf5vrcz6nk41yarf3qbcs78wbpn56zd8dk74pjzi")))

(define-public crate-nettu_scheduler_sdk-0.3.3 (c (n "nettu_scheduler_sdk") (v "0.3.3") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "03aj60x2vjplr3ijyr4jzdx698m199919r1hra5cf404g1nmpwrq")))

(define-public crate-nettu_scheduler_sdk-0.3.4 (c (n "nettu_scheduler_sdk") (v "0.3.4") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0fj7176vypmzpasar44m4fkpm6m37ia4inhz7rydc86y45hj520b")))

(define-public crate-nettu_scheduler_sdk-0.3.5 (c (n "nettu_scheduler_sdk") (v "0.3.5") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0grggdr7hxj3yipghw63gnv0ajxh41yj77c7sl4x2v7wqaxygcji")))

(define-public crate-nettu_scheduler_sdk-0.3.6 (c (n "nettu_scheduler_sdk") (v "0.3.6") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.2.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0hpjsrrmdkp9cg6fqvwvm8cy2rir43aqam7h7255yd4p99spvla5")))

(define-public crate-nettu_scheduler_sdk-0.4.0 (c (n "nettu_scheduler_sdk") (v "0.4.0") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.3.0") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0czhgwnl8vimlw4l847rbnn4sc91zgc7d85ak423fd81ishlh4nf")))

(define-public crate-nettu_scheduler_sdk-0.4.1 (c (n "nettu_scheduler_sdk") (v "0.4.1") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.3.1") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1nr61ycsgfj2gc2sbrq39wr46md5zp0m6gsi5y2hd7cczaag89hy")))

(define-public crate-nettu_scheduler_sdk-0.4.7 (c (n "nettu_scheduler_sdk") (v "0.4.7") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.4.7") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0j02kb5l8i7zr8jknw81ws9yvp65y1xbiaqq70wp3px8al17nxpv")))

(define-public crate-nettu_scheduler_sdk-0.5.5 (c (n "nettu_scheduler_sdk") (v "0.5.5") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.5.5") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.5.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "180i87zz65zh545jx0vsylly9v5l18gcq8bfhlc429rn97ba4k8m")))

(define-public crate-nettu_scheduler_sdk-0.5.7 (c (n "nettu_scheduler_sdk") (v "0.5.7") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.5.7") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.5.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1s2cypyps17qfgd285dbay3qhcbkcfmkgh2vgljz0jykirq77wg7")))

(define-public crate-nettu_scheduler_sdk-0.5.9 (c (n "nettu_scheduler_sdk") (v "0.5.9") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.5.9") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.5.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1kb5cc0rqmzwqzapcpnfglyp8ykbf34mz13yxgyzlvvf2zfl2jmm")))

(define-public crate-nettu_scheduler_sdk-0.6.0 (c (n "nettu_scheduler_sdk") (v "0.6.0") (d (list (d (n "nettu_scheduler_api_structs") (r "^0.6.0") (d #t) (k 0)) (d (n "nettu_scheduler_domain") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0gkkpimv96b34y7z89zqzc1gqfz80v490crflybywhanb8kv745w")))

