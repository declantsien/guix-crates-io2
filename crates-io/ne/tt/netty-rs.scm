(define-module (crates-io ne tt netty-rs) #:use-module (crates-io))

(define-public crate-netty-rs-0.1.0 (c (n "netty-rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("sync" "rt-multi-thread" "macros" "time" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 0)))) (h "0zz2vc32ddh60rifka37kpjfs93pswzmpkgyi5n1ljgzihnxg8dm")))

