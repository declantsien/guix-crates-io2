(define-module (crates-io ne tt nettest) #:use-module (crates-io))

(define-public crate-nettest-0.1.0 (c (n "nettest") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "13igxca6gl1zcscvbhsx1kzlbjrql24l88fnafz07wxgj7pwsyfx")))

(define-public crate-nettest-0.1.1 (c (n "nettest") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17plhqdcknsmyv83rrshq6alzh3ix0kgv114nzxvrw97px715dsn")))

