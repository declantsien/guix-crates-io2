(define-module (crates-io ne tt nettle) #:use-module (crates-io))

(define-public crate-nettle-1.0.0 (c (n "nettle") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1k8x413sk3ci7q0bzn9byks50a79ns9iq3z6k7pi46scbmrqsg0b")))

(define-public crate-nettle-2.0.0 (c (n "nettle") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1pqaash77k4zpkw9wjz76dnp8imnlqvq8dx5c4hpaxspyrljp596")))

(define-public crate-nettle-2.1.0 (c (n "nettle") (v "2.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0sgri9px3n0fi2kvn708h4pgk2ghqnk7b45nz2mfb54kzyqg5xlh")))

(define-public crate-nettle-2.2.0 (c (n "nettle") (v "2.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1mn2q9ksklllr6v5pja5ziqgq8hxy2qf51ahcw28qc8l2w1m6g9n")))

(define-public crate-nettle-3.0.0 (c (n "nettle") (v "3.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0qpn13w1hvgqrsy1428aw3w1ch4dbaa7r0m3c7qmb5m43xw6l09k")))

(define-public crate-nettle-4.0.0 (c (n "nettle") (v "4.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1vz2fv5vm4jflvqnd5q8zk86y5n39bs95xwh478p34v6h299bw8n")))

(define-public crate-nettle-5.0.0 (c (n "nettle") (v "5.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1i3x7qgjv6ib04pxaw9l3f4mi10n9yzm2ffax89hw4lspsh25rml")))

(define-public crate-nettle-5.0.1 (c (n "nettle") (v "5.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1cfp4nwhiwga3fj9vvhjwnxfngc0knws6hymhv9xmwlz1ndg813p")))

(define-public crate-nettle-5.0.2 (c (n "nettle") (v "5.0.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ipi8db88p8k8jlzj5wcrswqdk93pcgirshj71dnrhnfi23rv5xc")))

(define-public crate-nettle-5.0.3 (c (n "nettle") (v "5.0.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)))) (h "0zfplqdf3mag8r7lc124hl24vri8yg711jmm8gl1mpwnlhass2n4") (f (quote (("vendored" "nettle-sys/vendored"))))))

(define-public crate-nettle-6.0.0 (c (n "nettle") (v "6.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)))) (h "03qswgz9qgfccvlz4nm7liv4y47pvwyqakfky27l44fs1slagl96") (f (quote (("vendored" "nettle-sys/vendored"))))))

(define-public crate-nettle-6.0.1 (c (n "nettle") (v "6.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)))) (h "0kracfqwmgcjf94jpc0g8kd4gi477ac235xhcpx3f14cs3rlrmbg") (f (quote (("vendored" "nettle-sys/vendored"))))))

(define-public crate-nettle-7.0.0 (c (n "nettle") (v "7.0.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1n6dwy9zba8853bmxzhwaashd3np0wxpx0pj43brm0hb8n2sxbxi") (f (quote (("vendored" "nettle-sys/vendored"))))))

(define-public crate-nettle-7.0.1 (c (n "nettle") (v "7.0.1") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f2165smbd4ka4hpra6kiqgrcsy3qbbq9r9azl5mgsna21n2icgb") (f (quote (("vendored" "nettle-sys/vendored"))))))

(define-public crate-nettle-7.0.2 (c (n "nettle") (v "7.0.2") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dzd7jb8bks4gyv6qdbnz2041j3m760d7idj0gxxmq04rsbfzfid")))

(define-public crate-nettle-7.0.3 (c (n "nettle") (v "7.0.3") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qlsq3szglkw7s089h5qh9xa787qyvkdj5cgxm4qj30fazwr0hx0")))

(define-public crate-nettle-7.1.0 (c (n "nettle") (v "7.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16d1sr5q0jwd27cafp66cm2lzzbsw2xvbr2va00j2lic8pwxy4f5")))

(define-public crate-nettle-7.2.0 (c (n "nettle") (v "7.2.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0da0d5kv2w93zjy7acsxvmg969ybi05bqibfs72nj0ri16l97lgm")))

(define-public crate-nettle-7.2.1 (c (n "nettle") (v "7.2.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ckmm5im02vq841pacsc9zd6spv7qrmjwdaq5266gz6nwpvhb825")))

(define-public crate-nettle-7.2.2 (c (n "nettle") (v "7.2.2") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kvdmqmgg10f3n16bgvjnjwbnvx6ibi1dnq1ny5bak01ii9mvzwi")))

(define-public crate-nettle-7.3.0 (c (n "nettle") (v "7.3.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "0dk36l90p79c3xgmrzp8489h8dfaal0jzaid1n8n3cg7xbrwrzdr")))

(define-public crate-nettle-7.4.0 (c (n "nettle") (v "7.4.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "nettle-sys") (r "^2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "0dk9rlpz4c0kf2c7298vllpnwr3lh10kkgdbslglmlz5ji5gzrj4")))

