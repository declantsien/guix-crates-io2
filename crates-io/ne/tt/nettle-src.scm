(define-module (crates-io ne tt nettle-src) #:use-module (crates-io))

(define-public crate-nettle-src-3.5.1-0 (c (n "nettle-src") (v "3.5.1-0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1cg68bzwkw3c1y1wkc5km7zxhfzgmg68civ4ygcgf2cyck2fv9xp") (y #t)))

(define-public crate-nettle-src-3.5.1-1 (c (n "nettle-src") (v "3.5.1-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1fyqz7nqdasyjwhc76jdrcrlnp32qgsf9gq8239cb83d6k37pp6n") (y #t)))

(define-public crate-nettle-src-3.5.1-2 (c (n "nettle-src") (v "3.5.1-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1j20nv9qyww9bjrxhgaj5a2dvc6ly44z040c2y90x213pvac1lzs") (y #t)))

