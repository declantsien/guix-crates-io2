(define-module (crates-io ne wp newport_editor) #:use-module (crates-io))

(define-public crate-newport_editor-0.2.0 (c (n "newport_editor") (v "0.2.0") (d (list (d (n "newport_asset") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_cache") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_gpu") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_graphics") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_math") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_os") (r "^0.2.0") (d #t) (k 0)))) (h "0l58whndvnw89p3jg8nkk1nh4ah290kp1fbk8rvprjdf698v7d0w")))

