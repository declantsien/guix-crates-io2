(define-module (crates-io ne wp newport_log) #:use-module (crates-io))

(define-public crate-newport_log-0.0.1 (c (n "newport_log") (v "0.0.1") (d (list (d (n "newport_core") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_engine") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_os") (r "^0.0.1") (d #t) (k 0)))) (h "1946c6i79lc26pqkd667mfhy1ixh41m5jrx2bv44dxi32hn7102q")))

(define-public crate-newport_log-0.2.0 (c (n "newport_log") (v "0.2.0") (d (list (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_os") (r "^0.2.0") (d #t) (k 0)))) (h "0qk8aq8jls1p2dqhp10c4wx70draqhs4sz135k93z9y27309846q")))

