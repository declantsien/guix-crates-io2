(define-module (crates-io ne wp newport_engine) #:use-module (crates-io))

(define-public crate-newport_engine-0.0.1 (c (n "newport_engine") (v "0.0.1") (d (list (d (n "newport_core") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_os") (r "^0.0.1") (d #t) (k 0)))) (h "1j5nqyhd111lr49hh2fkd0g22k06v0ydkrs8k3ws7r4varaxzsaz")))

(define-public crate-newport_engine-0.2.0 (c (n "newport_engine") (v "0.2.0") (d (list (d (n "newport_math") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_os") (r "^0.2.0") (d #t) (k 0)))) (h "16561cgbzmgrlblhbdykfjl7dzq2dj5srgk7dryhx9rjkk71vjlm")))

