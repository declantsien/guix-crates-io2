(define-module (crates-io ne wp newport_math) #:use-module (crates-io))

(define-public crate-newport_math-0.0.1 (c (n "newport_math") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std") (r "^0.4.0-alpha.3") (f (quote ("const-generics"))) (d #t) (t "cfg(target_arch = \"spirv\")") (k 0)))) (h "16a3i13wq1rhvx0vjwbgwl5nb6x1id6pm59n3bwk4j7rlzm2wzdv")))

(define-public crate-newport_math-0.2.0 (c (n "newport_math") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "01a0dbpnxqjhbbhy3nd1gz516ppbfmahxss4rkq0dn1ywymh1a6i")))

