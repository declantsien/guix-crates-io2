(define-module (crates-io ne wp newport_os) #:use-module (crates-io))

(define-public crate-newport_os-0.0.1 (c (n "newport_os") (v "0.0.1") (h "0pcgzx7lmvlbhrlqnf5chr27k1knr6rqrg5cgrgzl03j7d1nv04v")))

(define-public crate-newport_os-0.2.0 (c (n "newport_os") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "newport_math") (r "^0.2.0") (d #t) (k 0)))) (h "04mlnmkyxvfz3630rl3n5jkhwyjvgzinsx40zw77mb3ly4jyg873")))

