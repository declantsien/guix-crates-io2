(define-module (crates-io ne wp newport_serde) #:use-module (crates-io))

(define-public crate-newport_serde-0.2.0 (c (n "newport_serde") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1npc70xr7rvyb7pqngqalvpyfn0zznhzxyr556ci95m8nvq22kwd")))

