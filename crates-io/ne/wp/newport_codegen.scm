(define-module (crates-io ne wp newport_codegen) #:use-module (crates-io))

(define-public crate-newport_codegen-0.2.0 (c (n "newport_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11mcb57p8d6z3w9apr2avrwak1j0hsqks1kwkgp5zhbaczbmbpcb")))

