(define-module (crates-io ne wp newport_cache) #:use-module (crates-io))

(define-public crate-newport_cache-0.2.0 (c (n "newport_cache") (v "0.2.0") (d (list (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_serde") (r "^0.2.0") (d #t) (k 0)))) (h "1k8jf5djagwzhz26qbaxxnm2ph2q7w6y7did10x4jjbvj214v1ci")))

