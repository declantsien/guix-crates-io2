(define-module (crates-io ne wp newport_game) #:use-module (crates-io))

(define-public crate-newport_game-0.2.0 (c (n "newport_game") (v "0.2.0") (d (list (d (n "newport_asset") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_ecs") (r "^0.2.0") (f (quote ("editable"))) (d #t) (k 0)) (d (n "newport_editor") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_gpu") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_graphics") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_math") (r "^0.2.0") (d #t) (k 0)))) (h "1r8jq0xkhb1xlvx49wp05vplmx6bs4qy667iy723fsn0mkwmis3j") (f (quote (("editor" "newport_editor"))))))

