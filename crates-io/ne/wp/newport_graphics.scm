(define-module (crates-io ne wp newport_graphics) #:use-module (crates-io))

(define-public crate-newport_graphics-0.0.1 (c (n "newport_graphics") (v "0.0.1") (d (list (d (n "newport_engine") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_gpu") (r "^0.0.1") (d #t) (k 0)))) (h "0dvjdmw7q8amkhazzvnsmmyblffxq23fk49qr3da31vkbgyqdhn7")))

(define-public crate-newport_graphics-0.2.0 (c (n "newport_graphics") (v "0.2.0") (d (list (d (n "freetype-rs") (r "^0.27.0") (d #t) (k 0)) (d (n "gltf") (r "^0.16") (d #t) (k 0)) (d (n "newport_asset") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_gpu") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_log") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_math") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_serde") (r "^0.2.0") (d #t) (k 0)) (d (n "stb_image") (r "^0.2.3") (d #t) (k 0)))) (h "1qwhk18hndca0yai4lwf25pdivgwc5v53cbf5vrbprgs4wbz6kr9")))

