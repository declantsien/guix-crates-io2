(define-module (crates-io ne wp newport_asset) #:use-module (crates-io))

(define-public crate-newport_asset-0.0.1 (c (n "newport_asset") (v "0.0.1") (d (list (d (n "newport_core") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_engine") (r "^0.0.1") (d #t) (k 0)) (d (n "newport_log") (r "^0.0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hhvbn3fdcb68rzlvrbfvzasqq5cdkr8fh03yk4vyxim6zj58w57")))

(define-public crate-newport_asset-0.2.0 (c (n "newport_asset") (v "0.2.0") (d (list (d (n "newport_cache") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_log") (r "^0.2.0") (d #t) (k 0)) (d (n "newport_serde") (r "^0.2.0") (d #t) (k 0)))) (h "0w3v3q176na51pnr6sr74vk4j043allqby27vfcfxc20kvhlnk4w")))

