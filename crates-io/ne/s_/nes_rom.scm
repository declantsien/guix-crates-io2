(define-module (crates-io ne s_ nes_rom) #:use-module (crates-io))

(define-public crate-nes_rom-0.1.0 (c (n "nes_rom") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xqr21prldxpa98vi2l3gff6nci72adb50khmhbkifi5qv8hxk92")))

