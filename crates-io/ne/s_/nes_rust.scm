(define-module (crates-io ne s_ nes_rust) #:use-module (crates-io))

(define-public crate-nes_rust-0.1.0 (c (n "nes_rust") (v "0.1.0") (h "0v85k22npfs6iizm7nymj4jgy8nprksq0hlv5jjcqpwqkzbcjdiy")))

(define-public crate-nes_rust-0.1.2 (c (n "nes_rust") (v "0.1.2") (h "1ij5rxhps3bhl0zh2gcwja4pgg3cwvzs0p3hwks3pra63c22pr0f")))

