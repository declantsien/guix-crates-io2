(define-module (crates-io ne to netoptim-rs) #:use-module (crates-io))

(define-public crate-netoptim-rs-0.1.0 (c (n "netoptim-rs") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1d6hn7w08f1llav3bh6zj2bv6x2pnx1r2vbkc4yhbknw102ysbvj")))

