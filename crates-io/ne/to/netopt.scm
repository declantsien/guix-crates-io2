(define-module (crates-io ne to netopt) #:use-module (crates-io))

(define-public crate-netopt-0.1.0 (c (n "netopt") (v "0.1.0") (d (list (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0lsn1mql7pvj04f58k3368fj986hb2k6zdc2jnvdr75g67z0vc37") (f (quote (("ssl" "openssl") ("default" "ssl"))))))

(define-public crate-netopt-0.1.1 (c (n "netopt") (v "0.1.1") (d (list (d (n "mqtt3") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (f (quote ("tlsv1_1" "tlsv1_2"))) (o #t) (d #t) (k 0)))) (h "1mddzbpxj3l28c2h9i0mfx8gdf6675vs6kf6553ph6qfdpqqy1db") (f (quote (("ssl" "openssl") ("default" "ssl"))))))

(define-public crate-netopt-0.1.2 (c (n "netopt") (v "0.1.2") (d (list (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (f (quote ("tlsv1_1" "tlsv1_2"))) (o #t) (d #t) (k 0)))) (h "12z3p46gz8basppx8yhmrlh5i5d7cp1vycy1pskwnf6b6xlib8m4") (f (quote (("ssl" "openssl") ("default" "ssl"))))))

(define-public crate-netopt-0.1.3 (c (n "netopt") (v "0.1.3") (d (list (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (f (quote ("tlsv1_1" "tlsv1_2"))) (o #t) (d #t) (k 0)))) (h "03anjfzbcfs7ffjpfvgclnrz2hmhklq8ggiaxc06srv2814rsq48") (f (quote (("ssl" "openssl") ("default" "ssl"))))))

