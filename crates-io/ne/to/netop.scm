(define-module (crates-io ne to netop) #:use-module (crates-io))

(define-public crate-netop-0.1.0 (c (n "netop") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "netraffic") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0xgd409ixxv4hkrs08v1inj7in8vrjirfhrhdwrzxjb2mzlnp0zv")))

(define-public crate-netop-0.1.1 (c (n "netop") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "netraffic") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "00d78ylycjxjwq2llp1ixj841mkf38ir24w0ky60kn89jv36zgx4")))

(define-public crate-netop-0.1.2 (c (n "netop") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "netraffic") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "1sa1z93mdr685lpkb0ws0ql7q5893ac160lnp9dssv28mqxfdq2y")))

(define-public crate-netop-0.1.3 (c (n "netop") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "netraffic") (r "^0.1.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0lsj59bsmsw5jcaymb80xamiz1rlqk2jxnahzn6iqccb1bx2d8l5")))

(define-public crate-netop-0.1.4 (c (n "netop") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "netraffic") (r "^0.1.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0065nyg3ajzbv9bvjsfbx9b6fyv4778jj52q4x509rwn2khv8j6f")))

