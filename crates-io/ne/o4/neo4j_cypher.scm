(define-module (crates-io ne o4 neo4j_cypher) #:use-module (crates-io))

(define-public crate-neo4j_cypher-0.1.1 (c (n "neo4j_cypher") (v "0.1.1") (d (list (d (n "cypher_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0m2m7ag3xf8akbvf0kviv93f3l2z7zi59iik5iib9s0n3rqkxnjq") (f (quote (("derive" "cypher_derive")))) (y #t)))

(define-public crate-neo4j_cypher-0.1.2 (c (n "neo4j_cypher") (v "0.1.2") (d (list (d (n "cypher_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0nz3ym1jgysd0dsbs5yh31g2cbd0az6hrs3r6liglbhk7pi7z0jq") (f (quote (("derive" "cypher_derive")))) (y #t)))

(define-public crate-neo4j_cypher-0.1.3 (c (n "neo4j_cypher") (v "0.1.3") (d (list (d (n "cypher_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "18va7dcdm8xsmvqjpp5jmc7qbridigl4672pjqdlnngd3xybyrma") (f (quote (("derive" "cypher_derive")))) (y #t)))

(define-public crate-neo4j_cypher-0.1.4 (c (n "neo4j_cypher") (v "0.1.4") (d (list (d (n "cypher_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0nm8qni06alac119zvffcp6sdy8zg5w8iyn2jv3j32af9d1an5ds") (f (quote (("derive" "cypher_derive"))))))

(define-public crate-neo4j_cypher-0.2.0 (c (n "neo4j_cypher") (v "0.2.0") (d (list (d (n "cypher_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "12pf2ybxfbv0fxcz6mnqbzr4w66ry6wrq740sn4sk1k9z8mhxvcx") (f (quote (("templates") ("derive" "cypher_derive")))) (y #t)))

(define-public crate-neo4j_cypher-0.2.1 (c (n "neo4j_cypher") (v "0.2.1") (d (list (d (n "cypher_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1iglbpsj782bqj24racr158sfcfz9x1sg2x8pqw3dbf8am06h9wa") (f (quote (("templates") ("derive" "cypher_derive"))))))

(define-public crate-neo4j_cypher-0.2.2 (c (n "neo4j_cypher") (v "0.2.2") (d (list (d (n "cypher_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0vpb4zdr0cipccj8b7bq6gk62jap60llw6vk8rxwcpdkqg916cyb") (f (quote (("templates") ("derive" "cypher_derive"))))))

