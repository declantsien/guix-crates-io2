(define-module (crates-io ne o4 neo4j_testcontainers) #:use-module (crates-io))

(define-public crate-neo4j_testcontainers-0.1.0 (c (n "neo4j_testcontainers") (v "0.1.0") (d (list (d (n "testcontainers") (r "^0.14.0") (d #t) (k 0)))) (h "1xxhf3f3a7fwjfa504213wn6r2idcigrgirspw23c5jqfq7cy0pm") (r "1.60.0")))

(define-public crate-neo4j_testcontainers-0.2.0 (c (n "neo4j_testcontainers") (v "0.2.0") (d (list (d (n "lenient_semver") (r "^0.4.2") (k 0)) (d (n "neo4rs") (r "^0.6.2") (d #t) (k 2)) (d (n "testcontainers") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "18aksdrl4k3z8a0qjfafvp57gp1d8baxkrh5k069qxxc0h8kjhxv") (r "1.63.0")))

(define-public crate-neo4j_testcontainers-0.3.0 (c (n "neo4j_testcontainers") (v "0.3.0") (d (list (d (n "neo4rs") (r "^0.7.0") (d #t) (k 2)) (d (n "testcontainers") (r "^0.16.7") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "testcontainers-modules") (r "^0.4.0") (f (quote ("neo4j"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0vkq85hj5n6v3kks2g618gval9j3hlwx1f37z694wm0dyg51jlgy") (r "1.70.0")))

