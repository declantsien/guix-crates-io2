(define-module (crates-io ne o4 neo4j-cli) #:use-module (crates-io))

(define-public crate-neo4j-cli-0.1.0 (c (n "neo4j-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "neo4rs") (r "^0.6.2") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1px4azvakpfyzg1vdac9m6853gxa08xx6im3ysn481d8cq6hy1jh") (r "1.70.0")))

