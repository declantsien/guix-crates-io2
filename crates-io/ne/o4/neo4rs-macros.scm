(define-module (crates-io ne o4 neo4rs-macros) #:use-module (crates-io))

(define-public crate-neo4rs-macros-0.1.0 (c (n "neo4rs-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "001076najqr7zj11hcx78sm6dmw7lsxs0f7gm06kzn3356rfgz27")))

(define-public crate-neo4rs-macros-0.2.0 (c (n "neo4rs-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10an7gkkl4rwhvfnjz556izlfzcy5471v3dsgx04w69x672qarx1")))

(define-public crate-neo4rs-macros-0.2.1 (c (n "neo4rs-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aplhml0m7wfj5kih8hpqkcfhlsa14c8zwsqyj2kad22c3x2px9c")))

(define-public crate-neo4rs-macros-0.3.0 (c (n "neo4rs-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00khxj149aw2smlg9g9ngkb7isvr2056mldil9idrlfjamydb82k") (r "1.63")))

