(define-module (crates-io ne re nereus) #:use-module (crates-io))

(define-public crate-nereus-0.1.0 (c (n "nereus") (v "0.1.0") (h "0k80bbxivwvw006wzi8w7sz9isigj425i8pn100rc2bs3y0b9j04")))

(define-public crate-nereus-0.1.1 (c (n "nereus") (v "0.1.1") (h "1pfpj38lvqfhpzijmap883rhcx6wxpa4fcgm6n6672is8aq7cq8y")))

(define-public crate-nereus-0.1.2 (c (n "nereus") (v "0.1.2") (h "1d8ckzz3rwypikn7cqb5jj0ah9z90w6ixg0l68rrvrr48p989n48")))

(define-public crate-nereus-0.1.4 (c (n "nereus") (v "0.1.4") (h "0i2b3cavs8bp3glbj7dg404ixi522vi0v0w17711pd8a849f70rp")))

(define-public crate-nereus-0.1.5 (c (n "nereus") (v "0.1.5") (h "032wybp82hz1f159m1kayga9ba7c6rbmf0i343nzqs1a972jqy37")))

(define-public crate-nereus-0.1.6 (c (n "nereus") (v "0.1.6") (h "0zrh3pcz0rx3n2fphazaizk8p9nv149kwxwnnlavvzv03i5c1h7r")))

(define-public crate-nereus-0.2.0 (c (n "nereus") (v "0.2.0") (h "1w540bqldg53bmbwqdc3ki5ijhlwj1lp73a0lhcd8bcl9x5c2m3a")))

(define-public crate-nereus-0.2.1 (c (n "nereus") (v "0.2.1") (h "1gkw7hkxrq66m5fylnl39pwg7ksgxmslj87vwlqvskaldn9119kk")))

(define-public crate-nereus-0.2.2 (c (n "nereus") (v "0.2.2") (h "0kii8l5jm7zi8wh4rnx4zcaxgca8ax7r802j0nyi87d7ipahc536")))

(define-public crate-nereus-0.2.3 (c (n "nereus") (v "0.2.3") (h "0zfnqv3h0v18x6hgn2mlk1ggvhd8igc3paslwc9461clax1zm7sf")))

(define-public crate-nereus-0.2.4 (c (n "nereus") (v "0.2.4") (h "0mcxlhgjhczbg7snr8aymdrz56qgkq6alxvsxciklbkx5h4cjjgd")))

(define-public crate-nereus-0.2.5 (c (n "nereus") (v "0.2.5") (h "0jirf6ch7v7bp4w35wjmdnvfffrjz9xawrzq85d8i5kmpd9x61x8")))

(define-public crate-nereus-0.2.6 (c (n "nereus") (v "0.2.6") (h "0y0chxm917g2g559cb4sbphvls2g979i60ahmsfy02381fpi5np4")))

(define-public crate-nereus-0.2.7 (c (n "nereus") (v "0.2.7") (h "0ax51f3154dgj0zybdvcd24rp4vpbxwdx1ylnabaq47g93lqi59d")))

(define-public crate-nereus-0.2.8 (c (n "nereus") (v "0.2.8") (h "1azrkdccqp9ki7r6q47ab032217x5dc8xg0riq4mkxrrh2j95a3c")))

