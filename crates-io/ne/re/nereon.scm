(define-module (crates-io ne re nereon) #:use-module (crates-io))

(define-public crate-nereon-0.1.0 (c (n "nereon") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 0)))) (h "0sphr55byldbw4das217y3cdbf6xz648dzrx76addqd2lmsxayd2")))

(define-public crate-nereon-0.1.1 (c (n "nereon") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a4cgcj7m6ikzn3iajfdph93b74vqvrlh200krjjsvswkjrb2kh5")))

(define-public crate-nereon-0.1.2 (c (n "nereon") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n3x6x9sfap8hzs2ds4fvq5ig9acqlamxgfam72ayz3llj2a25dy")))

(define-public crate-nereon-0.1.3 (c (n "nereon") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02ab5xnq9y8asm1d5vv8h9nk023bxgrcpvvf46hpwqb0saz6a6yc")))

(define-public crate-nereon-0.1.4 (c (n "nereon") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fjbjb8161ygjy9xra5haj36yyq0rhgzzradxnjxq7bhhihbly5m")))

(define-public crate-nereon-0.1.5 (c (n "nereon") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jfj99b2qxkc69bxfcv4803s89ryhflb0cny16xky31v742rd3c1")))

(define-public crate-nereon-0.1.6 (c (n "nereon") (v "0.1.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "066v8gj7vmgr2zdhs2haqm5jz57pi2sgaa63adbhzak6irdfcf4g")))

(define-public crate-nereon-0.1.7 (c (n "nereon") (v "0.1.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ypwa3fwpvykwbp8khz0zvh8j8ynvj9jjjvv8hvdfkinyq6x1p1")))

(define-public crate-nereon-0.1.8 (c (n "nereon") (v "0.1.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pcaxsdn84lv498a8qsi6n0cmkla83aa2ka8il3bb54r0kc9q2ci")))

(define-public crate-nereon-0.2.0 (c (n "nereon") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1if6q5h8hlp79a30j4rnw3d74d675dh1isdxjcv9xjqzg50l2k5g")))

(define-public crate-nereon-0.3.0 (c (n "nereon") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1n84q1qsq7pkdww6ahr1y2pq1xz76n6hqdq7xg8xc79hqnw6b7nq")))

(define-public crate-nereon-0.3.2 (c (n "nereon") (v "0.3.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0r2qca6wqkjpa6zn89271pmwmplp41qsr1rpg6nhi46dkfqni64h")))

(define-public crate-nereon-0.3.3 (c (n "nereon") (v "0.3.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0h3a36nvywp0dwrb5xjpvb4z6mdiqsha9m0b02ykkjv1656knk82")))

(define-public crate-nereon-0.4.0 (c (n "nereon") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1sxhby6g5dfka0ybs23yvrljyp5c0f4ihyz6yynw138iln04rm3k")))

(define-public crate-nereon-0.4.1 (c (n "nereon") (v "0.4.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0syxv69p0r10hx7mpfa21k03bdpvyjb0kl4agmcy21ayyi5q6azk")))

(define-public crate-nereon-0.4.2 (c (n "nereon") (v "0.4.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0wpqchnyv4avbqhhg1kyfzkh2as9al5z6iywmxfac6y62a1qsxk2")))

(define-public crate-nereon-0.4.3 (c (n "nereon") (v "0.4.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0zni040rffwys7gj0s99cgq9wsjrjzmx6j6rlnsj75018lzvcbmg")))

(define-public crate-nereon-0.4.4 (c (n "nereon") (v "0.4.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0ili9yg60f3q95w2i4ax36ryqzm05shn36426zy73gmwp3n98kda")))

(define-public crate-nereon-0.5.0 (c (n "nereon") (v "0.5.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0zzg6hcgjr4yvv7823qj0f6bdj5dbrqbgsjf411fm3pbd66fypi0")))

(define-public crate-nereon-0.6.0 (c (n "nereon") (v "0.6.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.6") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1kb06h7cpwnq8lw0fj7z8vzwg66cj0bcd2fb4ixvbc806bfdakmp")))

