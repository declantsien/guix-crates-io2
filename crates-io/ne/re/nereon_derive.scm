(define-module (crates-io ne re nereon_derive) #:use-module (crates-io))

(define-public crate-nereon_derive-0.1.0 (c (n "nereon_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0w6hii5g4rsy52clq2n374saxs1fkgkcffhwvjr29rhn6wsc6kd8")))

(define-public crate-nereon_derive-0.2.0 (c (n "nereon_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "05gx31wzhnvspa9gqqgmg31wga99yryriidd9p0720i21nxvxdvd")))

(define-public crate-nereon_derive-0.2.1 (c (n "nereon_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qnp0gj9gnnlm5bhx3zzkwm2apzxnb2v5x28rrab6wyqkmhv31xl")))

(define-public crate-nereon_derive-0.4.0 (c (n "nereon_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0r13hkb43mzans4yb9byxrf27iq69sh02vs1rqlbdac39c5y4vcd")))

(define-public crate-nereon_derive-0.4.2 (c (n "nereon_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rxpfs0q02wxf6l32yw8bv7vil9bwan3p3r4y8b350bl6iv4n0sq")))

(define-public crate-nereon_derive-0.4.3 (c (n "nereon_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "163fp7b4fbx01kpnh1kaicz6783vh7g204sfn3bwpvbfd25yvaha")))

(define-public crate-nereon_derive-0.5.0 (c (n "nereon_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vix83jsbsmik8968lgmf79w598mz5zvb8wd16h0d9xz8ms46ici")))

(define-public crate-nereon_derive-0.6.0 (c (n "nereon_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0lzl3wkaqca7dyvib2195i8gabh9ddh0y2i133n6akar4naryx9k")))

