(define-module (crates-io ne re nereond) #:use-module (crates-io))

(define-public crate-nereond-0.1.1 (c (n "nereond") (v "0.1.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "0j082zy04jyqs7vmzvjyi12xi5xzabb0nlny4mczg33qy0r3v1bm")))

(define-public crate-nereond-0.2.0 (c (n "nereond") (v "0.2.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "10pi03awm6sqzp9a05p95qz8j2lx0ajw3zn9pv442id6ynvq8lrg")))

(define-public crate-nereond-0.3.0 (c (n "nereond") (v "0.3.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "09pyy5s7h44aswgib7mhbqdr95my8p48081bph62j09xig428vy6")))

