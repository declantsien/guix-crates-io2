(define-module (crates-io ne te neteye-tool-grrs) #:use-module (crates-io))

(define-public crate-neteye-tool-grrs-0.1.0 (c (n "neteye-tool-grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0f90civxn9cn8wqsjl3w30bsiamamq2mxjhqg8ci7drxswgha12b")))

