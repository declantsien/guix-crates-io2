(define-module (crates-io ne xs nexsys-core) #:use-module (crates-io))

(define-public crate-nexsys-core-0.1.0 (c (n "nexsys-core") (v "0.1.0") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "10sa9dd9j08afp96lr7pyg4b3q38vqr4yg8wg6fvmlirary3amqa") (y #t)))

(define-public crate-nexsys-core-0.1.1 (c (n "nexsys-core") (v "0.1.1") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1n8v069f2qdrmmb53s89d0n0vvxqq7jypsk3nr63kx076sn8j54i") (f (quote (("python_ffi" "pyo3")))) (y #t)))

(define-public crate-nexsys-core-0.1.2 (c (n "nexsys-core") (v "0.1.2") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0mlcndfy73f946xj4bi79s5b1crpq1r7sphp8jw9pkdgixrzv3jb") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi")))) (y #t)))

(define-public crate-nexsys-core-0.1.3 (c (n "nexsys-core") (v "0.1.3") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "00pxm51882ckzgd3bba3kw0wml85z3qbcy7sv1c5l7pqiw07w17y") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi")))) (y #t)))

(define-public crate-nexsys-core-0.1.4 (c (n "nexsys-core") (v "0.1.4") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0d5cx1clmil3nw0v9car4grqvxmzj4pryjf10yawsvgpi46gzc8f") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi")))) (y #t)))

(define-public crate-nexsys-core-0.0.0 (c (n "nexsys-core") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "nexsys-math") (r "^0.0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0yvchx6zvf52qqbsxf4k00j34sn92qg8192sn5bcbkamgsgp4b33") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi")))) (y #t)))

(define-public crate-nexsys-core-0.2.0 (c (n "nexsys-core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "nexsys-math") (r "^0.0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0w37596gz6vdf9ygjcclij1dw88nimzdzwsyap96z4ffxz25ynca") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi")))) (y #t)))

