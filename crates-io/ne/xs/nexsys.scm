(define-module (crates-io ne xs nexsys) #:use-module (crates-io))

(define-public crate-nexsys-0.0.1 (c (n "nexsys") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0mqnjm4bif1a7d0n7fflbkwr7a8p85pylrm7hy7pz0d23g6s2bcb") (f (quote (("python_ffi" "pyo3") ("default" "python_ffi"))))))

(define-public crate-nexsys-0.0.2 (c (n "nexsys") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "156ka6g70sb6ksv3pk6c4hclj28z6rchslc41m5i3xyj0ggvfwb4") (f (quote (("python_ffi" "pyo3"))))))

