(define-module (crates-io ne um neum) #:use-module (crates-io))

(define-public crate-neum-0.1.0 (c (n "neum") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "neum-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "neum-parse") (r "^0.1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "05y87dmj42abranvd555cymj62iixhlw9z92p24xcgmynn91xbxr")))

