(define-module (crates-io ne um neumann) #:use-module (crates-io))

(define-public crate-neumann-0.1.0 (c (n "neumann") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "fasteval") (r "^0.2.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "minilp") (r "^0.2.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (d #t) (k 0)) (d (n "polynomials") (r "^0.2.4") (d #t) (k 0)) (d (n "preexplorer") (r "^0.3.2") (d #t) (k 2)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "0rws9vmfk66zy0592jmv48l67y3cr1vi8qhn9m3czhy8qxrssr2w")))

