(define-module (crates-io ne um neum-parse) #:use-module (crates-io))

(define-public crate-neum-parse-0.1.0 (c (n "neum-parse") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0ikspbpiw5x9h2amw4nlnszn6p3w46rijvisg1mnaisf3n33mfrp")))

