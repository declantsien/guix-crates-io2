(define-module (crates-io ne um neum-cli) #:use-module (crates-io))

(define-public crate-neum-cli-0.1.0 (c (n "neum-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "neum") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1lx1kmlwb8a85a1jvpj03nispphs4bsyn5rc8apadajs5kjwm8b0")))

