(define-module (crates-io ne wy newyears) #:use-module (crates-io))

(define-public crate-newyears-1.0.0 (c (n "newyears") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "165yk4yn8w0vw4svgyvmjx247w903388vgxs1gdagjnbw7kpksr2")))

(define-public crate-newyears-1.0.1 (c (n "newyears") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01a8sfb4y3cgl6rrj26gfp3zjh73rjbb8ycnvs9qds33ya15n4zs")))

(define-public crate-newyears-1.0.2 (c (n "newyears") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "17h309y8cv42wr2hzjyb935g93wxnj0r56ycmcfmwlgr3jfv6qmy")))

(define-public crate-newyears-1.0.3 (c (n "newyears") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "12m72d79nc4c4mnj8ym9xvhgi3nlci02a2gm105bf79r5vncz616")))

(define-public crate-newyears-1.0.4 (c (n "newyears") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0nngmf2f1pa7aidblsp8lc5rbqgx5rxvh11s3cnr5mbdr9jd77bg")))

