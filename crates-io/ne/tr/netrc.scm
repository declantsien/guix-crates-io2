(define-module (crates-io ne tr netrc) #:use-module (crates-io))

(define-public crate-netrc-0.0.1 (c (n "netrc") (v "0.0.1") (h "0fby7xjn187xqggi5lp7p160wv858mcdn91nwfaanv9xvp41qd36")))

(define-public crate-netrc-0.0.2 (c (n "netrc") (v "0.0.2") (h "1hd1vrm21kyvqh4y108yiwkcaj8d3nmhixfgkh9m60r68ck50ibm")))

(define-public crate-netrc-0.3.0 (c (n "netrc") (v "0.3.0") (h "0lsij4j92z0nibg6abw39phkjz22hqvd51gy77virsk3bm9syaib")))

(define-public crate-netrc-0.4.0 (c (n "netrc") (v "0.4.0") (h "1x80w6i0jr1cwh2xnwlbcnb8794i2xfg99f34vx8v8275v6skaa2")))

(define-public crate-netrc-0.4.1 (c (n "netrc") (v "0.4.1") (h "1q0nf7cwgz9a846g3g8sh2b1qkids8gyrn3yf0ka5z1lchr1paf9")))

