(define-module (crates-io ne tr netres) #:use-module (crates-io))

(define-public crate-netres-0.1.0 (c (n "netres") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miette") (r "^5.1.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "system_shutdown") (r "^4.0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (d #t) (k 0)))) (h "0fx97pfrdn1zb3xz70zaksz95x5rjdrxpvspk6xdf0dyzvkh8llv")))

(define-public crate-netres-0.1.1 (c (n "netres") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "system_shutdown") (r "^4.0.1") (d #t) (k 0)))) (h "17icky1j9lizf8k5fg7vx1c9mzlrn90l8sbmzn0yp1airgcbfy49")))

(define-public crate-netres-0.1.2 (c (n "netres") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "system_shutdown") (r "^4.0.1") (d #t) (k 0)))) (h "0ixbfjwr4ga5r4c81b4nk4addrjqkz8fq8k3w0dgmdhjxzkfjjcn")))

(define-public crate-netres-0.1.3 (c (n "netres") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "system_shutdown") (r "^4.0.1") (d #t) (k 0)))) (h "1if2f5znsr9v0la2gmwi2pjcw4l7qiwvbfjmj9hv97j4knffrzjz")))

