(define-module (crates-io ne tr netron) #:use-module (crates-io))

(define-public crate-netron-0.1.0 (c (n "netron") (v "0.1.0") (h "0y00vygqb8qzqrcwyn9hams7di8fzy0pd2x8kzfrslqi29fjqr5y")))

(define-public crate-netron-0.1.1 (c (n "netron") (v "0.1.1") (h "0ygk1wh5b8pgxfhsdizb0rb3ygz2pfak2k90113jyacdf77vcffr")))

