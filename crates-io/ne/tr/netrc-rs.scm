(define-module (crates-io ne tr netrc-rs) #:use-module (crates-io))

(define-public crate-netrc-rs-0.1.0 (c (n "netrc-rs") (v "0.1.0") (h "1fnq35hn8702iglq5w5pi00flqi0i4blqflh791jc3398iha5q66")))

(define-public crate-netrc-rs-0.1.1 (c (n "netrc-rs") (v "0.1.1") (h "0qgr2zj6l58wrqfzv80gj3q4n2k5bk7jw6ni6m2nnw50na7rq07g")))

(define-public crate-netrc-rs-0.1.2 (c (n "netrc-rs") (v "0.1.2") (h "0dmq37j4a9d9d4vcfpm3s4gcsrpd0k87nji38s15wy4cpkxp0aga")))

