(define-module (crates-io ne tr netr) #:use-module (crates-io))

(define-public crate-netr-0.1.0 (c (n "netr") (v "0.1.0") (h "09np3vvbirknqrl9l53bc00l2x7bpqqhl3mwfc2vv21c6c7l8a17")))

(define-public crate-netr-0.1.1 (c (n "netr") (v "0.1.1") (h "1j1l27c0kb4ykddkbwr36z5l0izn5g8y29nlb5r2v84y38d0fvyh")))

(define-public crate-netr-0.1.3 (c (n "netr") (v "0.1.3") (h "154sphhwbb2hhz49g4l86dz48wqplf16j8xrchc83l816sha0j55")))

(define-public crate-netr-0.1.4 (c (n "netr") (v "0.1.4") (h "0zx1v6768yqhc77c5j5lr71c1j9qiwrlpnixsi5asg0zsfqc5q8z")))

(define-public crate-netr-0.1.5 (c (n "netr") (v "0.1.5") (h "0dz9fmdrymk9mg3a8cqk9agqm6s91qf963wc5xdc2k16frgq4viz")))

(define-public crate-netr-0.1.6 (c (n "netr") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)))) (h "0xrf4yrz2kcm36gyxj3jyngbpnmwxhnrlar5dr3p35k1220m6had")))

