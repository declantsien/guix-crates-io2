(define-module (crates-io ne ow neowatch) #:use-module (crates-io))

(define-public crate-neowatch-0.1.0 (c (n "neowatch") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vnmxzz0ihhi1v50xs9nsf1vwgflc7lwva8x7dk46ij25srn30vm")))

(define-public crate-neowatch-0.1.1 (c (n "neowatch") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1x5rj8ps3fwbdr64qc60f1lcq7ixjcb4qvkcw892aab8j1p79nal")))

(define-public crate-neowatch-0.1.2 (c (n "neowatch") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "09xcx2fdhpyjisqzx48p539qigrr1appzpdw14q47fyyf9as453c")))

(define-public crate-neowatch-0.1.3 (c (n "neowatch") (v "0.1.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("std"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "03fa7cvhb1c27y0757gza87b5dm74si4ng4l182p0fzjkn4xymhs")))

(define-public crate-neowatch-0.2.0 (c (n "neowatch") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("std"))) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0k0kkdplx0af6pzd5xzn52wxnmi5n143vy5rmk5is7gbhvj82b7p") (r "1.61")))

(define-public crate-neowatch-0.2.1 (c (n "neowatch") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "help"))) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0rdp63zv86lc65w19cy28qfwd56nw82m76mz9q1lk3xy2f07ja39") (r "1.61")))

