(define-module (crates-io ne ls nelson137-scratch) #:use-module (crates-io))

(define-public crate-nelson137-scratch-0.1.0 (c (n "nelson137-scratch") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)))) (h "1xgyq27g8ki9hgzva2g3fx85q902n2zz82pqqb8z7sna0hnk392z")))

(define-public crate-nelson137-scratch-0.1.1 (c (n "nelson137-scratch") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)))) (h "02y2gan6b1q2hx96rgxxxi2bpavmhzinfijv44qxcqzcbnak9wm5")))

