(define-module (crates-io ne op neophyte-macro) #:use-module (crates-io))

(define-public crate-neophyte-macro-0.2.0 (c (n "neophyte-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zl1arc7mx4qyxb8jiycl1cf92051bvjfy8wxhqnc5489jlzkjwc")))

