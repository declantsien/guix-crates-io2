(define-module (crates-io ne ob neobirth) #:use-module (crates-io))

(define-public crate-neobirth-0.1.0 (c (n "neobirth") (v "0.1.0") (d (list (d (n "zengarden") (r "^0") (d #t) (k 0)))) (h "1mc39k3495dvd26jlpm9kckfcirai6x2n4sdrsb0zlm9ylq7saby")))

(define-public crate-neobirth-0.1.1 (c (n "neobirth") (v "0.1.1") (d (list (d (n "panic-halt") (r "^0.2") (d #t) (k 0)) (d (n "purezen") (r "^0.0.2") (k 0)) (d (n "smart-leds") (r "^0.1") (d #t) (k 0)) (d (n "trellis_m4") (r "^0.1") (d #t) (k 0)) (d (n "ws2812-timer-delay") (r "^0.1") (d #t) (k 0)))) (h "1dlmg6x6x772527w1ppa6j9liciyc0hbj1gf3vf186i7pys0rsd8")))

