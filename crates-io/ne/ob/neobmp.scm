(define-module (crates-io ne ob neobmp) #:use-module (crates-io))

(define-public crate-neobmp-0.1.5 (c (n "neobmp") (v "0.1.5") (h "1ich447nnykr8myipygxgvzhxqwlz439fk22vnf54a9i5l0g9q9k") (y #t)))

(define-public crate-neobmp-0.1.6 (c (n "neobmp") (v "0.1.6") (h "1vw4c347nq8qgz21bv2kc4j8zhqylb1dakh5rnb1pzp0hll5iw5r") (y #t)))

(define-public crate-neobmp-0.1.7 (c (n "neobmp") (v "0.1.7") (h "1n7y338fldmfvxz83d1w56gg8ddcibp2cpbc82sqry3z76g3xlpk") (y #t)))

(define-public crate-neobmp-0.2.0 (c (n "neobmp") (v "0.2.0") (h "0hi3ndryrdv3zx3bdbqibvn6630rkjrh54cwn752azka55hplm7z") (y #t)))

