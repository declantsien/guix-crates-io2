(define-module (crates-io ne tm netmod-wd) #:use-module (crates-io))

(define-public crate-netmod-wd-0.1.0 (c (n "netmod-wd") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "jni") (r "^0.14") (k 0)) (d (n "netmod") (r "^0.4") (d #t) (k 0) (p "ratman-netmod")))) (h "1sr42a4526j443vdxqg40rpani93rc27cl86mqbfv29r70ikhlfb")))

