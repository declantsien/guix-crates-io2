(define-module (crates-io ne tm netmon) #:use-module (crates-io))

(define-public crate-netmon-0.1.0 (c (n "netmon") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "0c63vjc9hr9flk1gd31sbzmj2l3n7wwcrd9ia0qlb1xjywvkf09y") (y #t)))

(define-public crate-netmon-0.1.1 (c (n "netmon") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1gmzjk65zf0j04l2qvxn0cz9314s3x31lr5ixv9lwnv5rb2vh8ai") (y #t)))

(define-public crate-netmon-0.1.2 (c (n "netmon") (v "0.1.2") (h "1yz7nki9acz1n2wzjz5r3jqxgmfvgpnw5vz2lrygfmvlk7ww5wg0")))

(define-public crate-netmon-0.1.3 (c (n "netmon") (v "0.1.3") (h "1p9i7py9d52ig2mdnqh1a9qrdbfssirfk18lvhxvfscf5q4rhkh7")))

