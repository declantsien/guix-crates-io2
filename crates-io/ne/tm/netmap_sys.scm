(define-module (crates-io ne tm netmap_sys) #:use-module (crates-io))

(define-public crate-netmap_sys-0.0.8 (c (n "netmap_sys") (v "0.0.8") (h "1sjsz59wgggq591bjaxi30rzpr07h30i1z7m1vx5s717774qwpvf") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.0.9 (c (n "netmap_sys") (v "0.0.9") (h "1knfd75v1l7c4rkk7k3zd6kxi1ygyscnsjdvf8aq99chdz7451d9") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.0.10 (c (n "netmap_sys") (v "0.0.10") (h "1c0ab02i2csg625mk5shfw48vzys629rb6azghwkl2zgdgnb319n") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.0.11 (c (n "netmap_sys") (v "0.0.11") (h "0r14zmn0hcm43k12frl5sa0mbq06l1l9py80crac5hx58z20v782") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.0.12 (c (n "netmap_sys") (v "0.0.12") (h "0y9cp1mby9w873frzls083g673ahxhzps12w07bm5za43bn7lv45") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.0.13 (c (n "netmap_sys") (v "0.0.13") (h "0254nn0mjkrl649pzh65adx7r8b5n2pd5i73sa6akksp8znr6bb2") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.1.0 (c (n "netmap_sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00xi2blqmlzxkxhgcfzdixc1h9006p806jp5pg863hy42q94jhic") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.1.1 (c (n "netmap_sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wz05gzbbhvr9adilirif0hh4ggqp0qgswbfs68ywjy6p6p88gqp") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.1.2 (c (n "netmap_sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07i40clx7rlnjifmwicrd642y4h7izyg9dhv9pg130z33p49kg8j") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.1.3 (c (n "netmap_sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jjyj06d608vai9m53rbkwxa5bqh23fq37bv2ml9gd9v8h1xawld") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-netmap_sys-0.1.4 (c (n "netmap_sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wgwjx2hdqbjvdpccqb0cg4gjxazgxfz8s966bq5g8z56xik95a5") (f (quote (("netmap_with_libs") ("default")))) (l "rust_netmap_user")))

