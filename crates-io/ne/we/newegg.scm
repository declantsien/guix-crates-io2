(define-module (crates-io ne we newegg) #:use-module (crates-io))

(define-public crate-newegg-0.1.0 (c (n "newegg") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vgj61xd9i63ds3avqxz34jfzaaqv04aksdc1w68gsq80r40qpig")))

