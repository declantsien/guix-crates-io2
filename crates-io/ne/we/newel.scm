(define-module (crates-io ne we newel) #:use-module (crates-io))

(define-public crate-newel-0.1.0 (c (n "newel") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1m4wmxfrlb9v1j22zbng1kn3zk9yavrxkw5zbx4m7nln0s355xw5")))

(define-public crate-newel-0.2.0 (c (n "newel") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0g5dr9nhbcz5ibl8hihs755szxiihp37d5r33yynhbwdfcr47hzf")))

