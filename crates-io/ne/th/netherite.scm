(define-module (crates-io ne th netherite) #:use-module (crates-io))

(define-public crate-netherite-0.1.0 (c (n "netherite") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "netherite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "0dh34zrliq0m9rw0s5jy6c5fms4gyjcvpwrd98126qi4njy0cnyc")))

(define-public crate-netherite-0.1.1 (c (n "netherite") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "netherite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "1l1z44bmr7yd5w7n4r6pg0hv8xwk83kclvcm1al97dia24gpi84j")))

(define-public crate-netherite-0.1.2 (c (n "netherite") (v "0.1.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "netherite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "0ziff8749y5gnx4a3zj0j9rd4myyddw8nsi3a0awdclllvx0ajsi")))

(define-public crate-netherite-0.1.3 (c (n "netherite") (v "0.1.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "netherite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "14cfsipjk8z7z1crd54j8ghhrjx2gid4w5g3cl2w90610k35rqyq")))

