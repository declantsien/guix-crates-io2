(define-module (crates-io ne th netherrack) #:use-module (crates-io))

(define-public crate-netherrack-0.0.0 (c (n "netherrack") (v "0.0.0") (d (list (d (n "integral_square_root") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "17vy2y2z772sszanhxc83vhaa6zc201yli4gr6dsb62krdfnq1zc")))

(define-public crate-netherrack-0.0.1 (c (n "netherrack") (v "0.0.1") (d (list (d (n "bit_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.2.15") (d #t) (k 0)) (d (n "integral_square_root") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)) (d (n "uuid") (r "^0.1.17") (d #t) (k 0)) (d (n "varint") (r "^0.2.0") (d #t) (k 0)))) (h "07h18p129lyqlds4bpz02js63d792w3jxy0fjlmx9lzr8kb2d0iy")))

