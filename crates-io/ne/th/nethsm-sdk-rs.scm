(define-module (crates-io ne th nethsm-sdk-rs) #:use-module (crates-io))

(define-public crate-nethsm-sdk-rs-0.3.0 (c (n "nethsm-sdk-rs") (v "0.3.0") (d (list (d (n "base64") (r "^0.21") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0sc78rs56jiqbah0pd54fl268pyks3p616yiffyq55af4zys22vc")))

(define-public crate-nethsm-sdk-rs-0.4.0 (c (n "nethsm-sdk-rs") (v "0.4.0") (d (list (d (n "base64") (r "^0.21") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1wb4z0s5y799prigk679xdp7xmnxy8iyis94w3jh1hxlcdhl4b5r")))

(define-public crate-nethsm-sdk-rs-0.5.0 (c (n "nethsm-sdk-rs") (v "0.5.0") (d (list (d (n "base64") (r "^0.21") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1fimv0bk7jxpzf0i07h4lwxjns4n5bi7zpvg8rrn630vx3cw3jw5")))

(define-public crate-nethsm-sdk-rs-1.0.0 (c (n "nethsm-sdk-rs") (v "1.0.0") (d (list (d (n "base64") (r "^0.21") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1hn1lwxblsxwh82957lk3sfy2n08c24cf2sw17b4h90vlwzp4wkv")))

(define-public crate-nethsm-sdk-rs-1.0.1 (c (n "nethsm-sdk-rs") (v "1.0.1") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0kwxiqq8n98h0d46lfn88kr1y9kp4hxc3wssz73r2gvv9cfvd5rd")))

