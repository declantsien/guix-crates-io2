(define-module (crates-io ne th nethoscope) #:use-module (crates-io))

(define-public crate-nethoscope-0.1.0 (c (n "nethoscope") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cpal") (r "^0.13") (d #t) (k 0)) (d (n "pcap") (r "^0.8") (d #t) (k 0)))) (h "09b3vv9lbncclcl77ynggvia74wj5ckmha3xy0p7443a1713yrwc")))

(define-public crate-nethoscope-0.1.1 (c (n "nethoscope") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cpal") (r "^0.13") (d #t) (k 0)) (d (n "pcap") (r "^0.8") (d #t) (k 0)))) (h "144x3xd104ws6b104676zdc17a2x5lhnrp9pw9h3c09wwi225j3q")))

