(define-module (crates-io ne oc neocortex) #:use-module (crates-io))

(define-public crate-neocortex-0.1.0 (c (n "neocortex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "15v5wfg97kgafhi95p2f6v8nmgcfp7wqfc1y9y3acypmnsscagp5")))

(define-public crate-neocortex-0.1.1 (c (n "neocortex") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0bd9rpn2i72a637517a2pgdsyyyxrjnardkmv98d39dh1ywjp5nb")))

(define-public crate-neocortex-0.1.2 (c (n "neocortex") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "10ar4zcicskkv1ap70csgggb9152v454djmyclgmj7jv6zd93ksk")))

(define-public crate-neocortex-0.1.3 (c (n "neocortex") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0jsw80mjbl3vcr7m0i4vqz0bvh5hg8lrh0zh4qx9521mibdj3mhw")))

(define-public crate-neocortex-0.1.4 (c (n "neocortex") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "021hbkz3i2cggrbzik8mm2r0hbjy99n0yisvkdrxn4m59x9hq8pi")))

(define-public crate-neocortex-0.1.5 (c (n "neocortex") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1l4wiva0lgrwazpfhw6c5xv35k016yqnksmka28v2j9vdg6j86i5")))

(define-public crate-neocortex-1.1.5 (c (n "neocortex") (v "1.1.5") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04rrlqfavs97spc3d3sq3chcz1jq36bs89rz070r31s7ldv3w16n") (f (quote (("semaphore"))))))

(define-public crate-neocortex-1.1.6 (c (n "neocortex") (v "1.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "13cv4pc3ham83vqipy1vxmrihxdr66ii6l69mjl7agwf1vglsq68") (f (quote (("semaphore"))))))

(define-public crate-neocortex-1.1.7 (c (n "neocortex") (v "1.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1hdgqs1n18ysbmhz6042w8c08lrnqg9vjixywz1qqhyxll5nqc1x") (f (quote (("semaphore"))))))

(define-public crate-neocortex-1.2.0 (c (n "neocortex") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1371k73f9xq403x7r8441wczylwyscvrwz4vkjmd7d4nqdj7jfqd") (f (quote (("semaphore"))))))

(define-public crate-neocortex-1.2.1 (c (n "neocortex") (v "1.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0zm7zmwxajpwn2g9m8sy1b0rk7aa9zvbl3wnzfkp1c448h9hkkdw") (f (quote (("semaphore"))))))

(define-public crate-neocortex-2.0.0 (c (n "neocortex") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "16vhivy6ajqck0k7crsbw75l2fr6zg74m8a1gmw3jsp9flxzi5d5") (f (quote (("semaphore"))))))

(define-public crate-neocortex-3.0.0 (c (n "neocortex") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0l3vbkachi5rg1rhkqahih93qvidj4lil2d6w7asnpif6br5g94x") (f (quote (("semaphore"))))))

(define-public crate-neocortex-3.0.1 (c (n "neocortex") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0wfxgy39s8fcq4z91q1k9ipqqna8dj0f7px50x7yh46k1ibw5gz3") (f (quote (("semaphore"))))))

(define-public crate-neocortex-3.0.2 (c (n "neocortex") (v "3.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0dznfxy1ffah5ij7sl9lvgw22wdw458pjyv4q8csir44gxkgi1sg") (f (quote (("semaphore"))))))

(define-public crate-neocortex-3.0.3 (c (n "neocortex") (v "3.0.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1qc6j95w8333ipzrkdnl2fddm7hsa5dgwbcwa3rpd15brx3q4znc") (f (quote (("semaphore"))))))

