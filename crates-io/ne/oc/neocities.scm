(define-module (crates-io ne oc neocities) #:use-module (crates-io))

(define-public crate-neocities-0.1.0 (c (n "neocities") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "1f4yx54j0rmxdfc3x1c5268jlprfli79g8xxg2y11pvk305qx47j")))

(define-public crate-neocities-0.9.0 (c (n "neocities") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "1j35m05ixdy05iifwi6958ifgmji22g2kd8d3qir2xr0sss39dcd")))

(define-public crate-neocities-0.9.1 (c (n "neocities") (v "0.9.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "1n086vi2qhrdn6nrfwxsdyl8fw1998cv5lpfzfnmbh9hzb8x9fq7")))

