(define-module (crates-io ne xu nexus-revo-io) #:use-module (crates-io))

(define-public crate-nexus-revo-io-0.1.0 (c (n "nexus-revo-io") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^1.0.0") (d #t) (k 0)) (d (n "libftd2xx") (r "~0.31.0") (d #t) (k 2)) (d (n "libftd2xx-cc1101") (r "~0.1.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "~0.4.0") (d #t) (k 2)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0igqd7hp5ly7wsa658g9rbznrqc8vji71g3ghkpn0g80dh70m7np") (f (quote (("static" "libftd2xx/static" "libftd2xx-cc1101/static"))))))

