(define-module (crates-io ne xu nexuslab_port_sniffer) #:use-module (crates-io))

(define-public crate-nexuslab_port_sniffer-0.1.0 (c (n "nexuslab_port_sniffer") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xwgnp47w81drif7ki0744bjmrpwapr6aki58qwd2pjljjv26xjc")))

(define-public crate-nexuslab_port_sniffer-0.2.0 (c (n "nexuslab_port_sniffer") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "18jzk6f9cb0n7y9dazzqfbcirpwy07rrrak2l88chzcm9y9rg0p4")))

(define-public crate-nexuslab_port_sniffer-0.2.1 (c (n "nexuslab_port_sniffer") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "00ggrhq0pf6sjmps3x290khkdq5hhsaaaz1b0bfi2xxf9pgp3wnr")))

