(define-module (crates-io ne xu nexus-api) #:use-module (crates-io))

(define-public crate-nexus-api-0.1.0 (c (n "nexus-api") (v "0.1.0") (d (list (d (n "clutils") (r "^0.0.7") (d #t) (k 0)) (d (n "nexus-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0c1pbs7rynb2c0h66jci5rg529bm6467zidpfqw8gx5bj83bj1j4")))

