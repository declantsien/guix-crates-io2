(define-module (crates-io ne xu nexus_mods_api) #:use-module (crates-io))

(define-public crate-nexus_mods_api-0.0.1-alpha (c (n "nexus_mods_api") (v "0.0.1-alpha") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 2)) (d (n "raxios") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "1czxbr94icd0azs9fyp73d4q58y78n6qr6p0xiz2vx42xp7xf7ki")))

