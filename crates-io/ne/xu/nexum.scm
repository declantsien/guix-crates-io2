(define-module (crates-io ne xu nexum) #:use-module (crates-io))

(define-public crate-nexum-0.1.0 (c (n "nexum") (v "0.1.0") (h "0pbxdb0gnv75vvdi2s2pjyin7mb1fvyl0gr1s7d12ia52fd96kin")))

(define-public crate-nexum-0.1.1 (c (n "nexum") (v "0.1.1") (h "1q2n8ir167kvbm3ahgdnzl8llqfnbnpsrd49x5z372v80zlvg31s")))

(define-public crate-nexum-0.2.0 (c (n "nexum") (v "0.2.0") (h "15x3v6xq26yk214sjrks0p11mdfgg06grhfkcgvkxqssh4qwacqm")))

