(define-module (crates-io ne xu nexus-rs) #:use-module (crates-io))

(define-public crate-nexus-rs-0.1.0 (c (n "nexus-rs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "mockito") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0m8f3gmcsf4js7dm1xz155qp8zpsdl6h8diz6zalhx8v9p5bz8p2")))

