(define-module (crates-io ne xu nexusmods-api-rust-client) #:use-module (crates-io))

(define-public crate-nexusmods-api-rust-client-1.0.0 (c (n "nexusmods-api-rust-client") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i07pp7dwnl1ak78wbpxrn81abmga5djbiv866az9iyfnxdj88q1") (y #t)))

(define-public crate-nexusmods-api-rust-client-1.0.1 (c (n "nexusmods-api-rust-client") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9g0r6iihm25vfrnw42dx6s79v7zfb1pla35jqbra2l2gi5cgq1") (y #t)))

(define-public crate-nexusmods-api-rust-client-1.0.2 (c (n "nexusmods-api-rust-client") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aykvgxcqgf3r51pdwgq3k1ljnjj3vzy6s8cmcvgn25xyl09rl6k") (y #t)))

