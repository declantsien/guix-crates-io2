(define-module (crates-io ne ov neovim) #:use-module (crates-io))

(define-public crate-neovim-0.0.1 (c (n "neovim") (v "0.0.1") (h "044f9gavp64w1yv64fxxv7v9mb8ywj7013x2vfbljk9g5r1l8h1f")))

(define-public crate-neovim-0.0.2 (c (n "neovim") (v "0.0.2") (d (list (d (n "mpack") (r "^0.1.1") (d #t) (k 0)))) (h "1d74i74r1vmvcapg8xpxb6i0qg68p0hn54p8k6sx8f621xjs5f82")))

(define-public crate-neovim-0.1.0 (c (n "neovim") (v "0.1.0") (d (list (d (n "mpack") (r "^0.1.2") (d #t) (k 0)))) (h "0x6f1h05zx4q2ypgfz51d6f642q7k3bgj4gc2hd89i4vyx7b7rn1")))

