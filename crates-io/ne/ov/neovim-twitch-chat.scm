(define-module (crates-io ne ov neovim-twitch-chat) #:use-module (crates-io))

(define-public crate-neovim-twitch-chat-1.0.0 (c (n "neovim-twitch-chat") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "1xvhk9dj3qkmrwn48cpf5xgwppfn6lgxkkyvhps3p4zb1dhx12x9")))

(define-public crate-neovim-twitch-chat-1.0.2 (c (n "neovim-twitch-chat") (v "1.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "0kf3rw74k7fbh8n9x468h26ghsi4gqkpvn69wik6zwv80krkkal1")))

(define-public crate-neovim-twitch-chat-1.0.3 (c (n "neovim-twitch-chat") (v "1.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (d #t) (k 0)) (d (n "twitch-irc") (r "^2.2.0") (d #t) (k 0)))) (h "1w4ddi5ly60slb1dj5dzkf24p5rvrj3ghbri3zbvnyal6z01q0w6")))

