(define-module (crates-io ne ov neovim-lib) #:use-module (crates-io))

(define-public crate-neovim-lib-0.1.0 (c (n "neovim-lib") (v "0.1.0") (d (list (d (n "rmp") (r "^0.7") (d #t) (k 0)) (d (n "rmp-serialize") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0c2659sf11ay94byjabhrnmfvxif5ndc4a91lksyrlh1ip5zf4h9")))

(define-public crate-neovim-lib-0.1.1 (c (n "neovim-lib") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.7") (d #t) (k 0)) (d (n "rmp-serialize") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1vbcs6z7alazxi2p6xlv71fiwn06xyl14n9f1lq4r3x2afy2yimk")))

(define-public crate-neovim-lib-0.1.2 (c (n "neovim-lib") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.7") (d #t) (k 0)) (d (n "rmp-serialize") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0aknkmwqw6l4p4kr7niy73jymxcl0qdgmshqhmazxhpjjya2lrx6")))

(define-public crate-neovim-lib-0.2.0 (c (n "neovim-lib") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)))) (h "1cjyzbp3qc51f37qkvjwh3nrkgyyki7kgarghfdhz0js5q2cxc4f")))

(define-public crate-neovim-lib-0.2.1 (c (n "neovim-lib") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1k0xs2r3ynkvv9bg5xzx7vlh10fvixb6j1v64330irkjmm9493p7")))

(define-public crate-neovim-lib-0.3.0 (c (n "neovim-lib") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1jpmszd2glra1f2hivz8h2qbjhbc4bq7sa0jbymrg4g92l5n4hs8")))

(define-public crate-neovim-lib-0.4.0 (c (n "neovim-lib") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "06xy808pims2yddk35z3d0ycab89p1fsdy49q35m8iy63s37f15a")))

(define-public crate-neovim-lib-0.4.1 (c (n "neovim-lib") (v "0.4.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1fgi4vy5knqyzmdxiy5ja8as6kdnl3pc27w6jpg9kvidb9vczx64")))

(define-public crate-neovim-lib-0.4.2 (c (n "neovim-lib") (v "0.4.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0sm71xrnx7jk4z6scn15mvyyac073f8fbsvm2zkcp7rh80hwv0fd")))

(define-public crate-neovim-lib-0.4.3 (c (n "neovim-lib") (v "0.4.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0m9zhb3jnfji79l01adyjx88a6ncxb51h5brq6gx7mp4myb74fv9")))

(define-public crate-neovim-lib-0.5.0 (c (n "neovim-lib") (v "0.5.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1v1gv3nrylmrl8y88qiaz1599h4pzmpjcflx7wgv2rpcrjgvg28q")))

(define-public crate-neovim-lib-0.5.1 (c (n "neovim-lib") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "15pnrr0n5q872gd5jn8c9jdwp3srlqcnabbxzpry6csy9vyphlyv")))

(define-public crate-neovim-lib-0.5.2 (c (n "neovim-lib") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "05w1l5mkcrynb1yjvqk3m2vpwrfzgxv774kxilllz5xmknr6gc1r")))

(define-public crate-neovim-lib-0.5.3 (c (n "neovim-lib") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0i88s7m56lg8qrjmrwf6zni3dgqab0h2c2i54zarhs4i3yrwhd7g")))

(define-public crate-neovim-lib-0.5.4 (c (n "neovim-lib") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0z3k64lmvy1agxwbqpggs6cqi7p7jnkxphdxriq471bjvrc6iml9")))

(define-public crate-neovim-lib-0.6.0 (c (n "neovim-lib") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1ansakda16jnzhhkbhvwpdagcbqzknis2fxpk38dpra6x3kg4vas")))

(define-public crate-neovim-lib-0.6.1 (c (n "neovim-lib") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "11zmf0h7khbhfgyzljjxa8x6zpjalfaw9hk9nvi0q5myw6hzba6n")))

