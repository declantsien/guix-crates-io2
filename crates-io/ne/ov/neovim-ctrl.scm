(define-module (crates-io ne ov neovim-ctrl) #:use-module (crates-io))

(define-public crate-neovim-ctrl-0.1.0 (c (n "neovim-ctrl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6") (d #t) (k 0)))) (h "0bh15gh12hca3cfkmka3a4xdscqvz2gn5dlwlgg39zpqmk0gmdg2")))

(define-public crate-neovim-ctrl-0.1.1 (c (n "neovim-ctrl") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6") (d #t) (k 0)))) (h "1lj8xbahigmx8p889zwkzflbjmgv8jx8qvdvscsvhs5py12lfwkv")))

(define-public crate-neovim-ctrl-0.2.0 (c (n "neovim-ctrl") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6") (d #t) (k 0)))) (h "0jqsxf2wx81isl3qrw90c28fwbj7j8gzfr9vqlrz6r7zsy9sbsmb")))

