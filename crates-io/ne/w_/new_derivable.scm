(define-module (crates-io ne w_ new_derivable) #:use-module (crates-io))

(define-public crate-new_derivable-0.1.0 (c (n "new_derivable") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "152rkvjjhy13hx57blzf2sqyxdsh0an3a762hqsgpmhbjad8w3bj")))

(define-public crate-new_derivable-0.2.0 (c (n "new_derivable") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (f (quote ("span-locations" "proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "=1.0.15") (d #t) (k 0)) (d (n "syn") (r "=1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dlpzwl539aqkik60zrwn3sd7blxhs2s5gaycv7d9ic49y6pyxn2")))

