(define-module (crates-io ne w_ new_type) #:use-module (crates-io))

(define-public crate-new_type-0.1.0 (c (n "new_type") (v "0.1.0") (h "0i9p3wxz6msjk50x8p33p7bz741b8naw800lw5w2za0dc1sab00h")))

(define-public crate-new_type-0.2.0 (c (n "new_type") (v "0.2.0") (h "0d0555bdszd2wx68y2m37s98blaaa212aip7vph35hgh0a6bbi72")))

(define-public crate-new_type-0.3.0 (c (n "new_type") (v "0.3.0") (h "1nzj8g364kff4qx6c5p7pk9dp4024dhdi4qmlaw8i8ggmwz067sm")))

(define-public crate-new_type-0.4.0 (c (n "new_type") (v "0.4.0") (h "134zr4q6naxjl14i6jgsp0piam9ln2ganz4362331j2rfkd6xcfj")))

(define-public crate-new_type-0.4.1 (c (n "new_type") (v "0.4.1") (h "08knybc01y2bcbq3rs1s8x2bcygww8x8sxzz907siyzrnhcr2kcr")))

