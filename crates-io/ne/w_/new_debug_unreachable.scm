(define-module (crates-io ne w_ new_debug_unreachable) #:use-module (crates-io))

(define-public crate-new_debug_unreachable-1.0.0 (c (n "new_debug_unreachable") (v "1.0.0") (d (list (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "1qbbw6hdhypvzcfgmgnhwn27jbmc1ldzw7vdss8qcxskqbzagn13")))

(define-public crate-new_debug_unreachable-1.0.1 (c (n "new_debug_unreachable") (v "1.0.1") (d (list (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "1i46wpnps8bzg29gyffw6hg7yxc1g72afvqdbr6vb2n7frq4bp0c")))

(define-public crate-new_debug_unreachable-1.0.2 (c (n "new_debug_unreachable") (v "1.0.2") (h "02p9mwzkamndqcmyi5nv5wyyfdisvjwq2r3nwr06b3zhx5jynbgy")))

(define-public crate-new_debug_unreachable-1.0.3 (c (n "new_debug_unreachable") (v "1.0.3") (h "0c1br326qa0rrzxrn2rd5ah7xaprig2i9r4rwsx06vnvc1f003zl")))

(define-public crate-new_debug_unreachable-1.0.4 (c (n "new_debug_unreachable") (v "1.0.4") (h "0m1bg3wz3nvxdryg78x4i8hh9fys4wp2bi0zg821dhvf44v4g8p4")))

(define-public crate-new_debug_unreachable-1.0.6 (c (n "new_debug_unreachable") (v "1.0.6") (h "11phpf1mjxq6khk91yzcbd3ympm78m3ivl7xg6lg2c0lf66fy3k5")))

