(define-module (crates-io ne w_ new_york_utils) #:use-module (crates-io))

(define-public crate-new_york_utils-0.1.0 (c (n "new_york_utils") (v "0.1.0") (h "0df70wylsmd4g9c3a4fr5cvvzbp42n9gp4r441f8vn3fbznpfafm")))

(define-public crate-new_york_utils-0.1.1 (c (n "new_york_utils") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1yg0ds6yh83cv6h4rld45rfm730kmwi8wc6y4r5gwv9nj72m5pyl")))

(define-public crate-new_york_utils-0.1.2 (c (n "new_york_utils") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1d5x4xwhfixhdi39s4hdnmzx91wx02dnx2p68n4vsafsf5gsizhg")))

(define-public crate-new_york_utils-0.1.3 (c (n "new_york_utils") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1qv2qcs2l41xmfqx1l2qn6fy8pjdmwm123p6zirx50vci9w6k4nj")))

(define-public crate-new_york_utils-0.1.4 (c (n "new_york_utils") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0i1az36m031paq2sfa8gkbzqmrhqb2nz131gvxca6xflg9ynykz4")))

(define-public crate-new_york_utils-0.1.5 (c (n "new_york_utils") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "08xamq92cfhyj9l5xdybvzqxp63qnmgxzc8ayvj5axg8vbdfm1ba")))

(define-public crate-new_york_utils-0.1.6 (c (n "new_york_utils") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "1lvhji0gibz5k7lkaz3vxnj7f0b7961xi6ilx5n94x7acqq6f9zs")))

(define-public crate-new_york_utils-0.1.7 (c (n "new_york_utils") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0149vzgcyp68nbf0h5qydyd7p8fr5fml4nv5d8wll02zbqsc6zsp")))

(define-public crate-new_york_utils-0.1.8 (c (n "new_york_utils") (v "0.1.8") (d (list (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0gnwj5ls49yc8q5j5j4kcd1i0v5dz786gqqmv2zjgmxhx6qrsg35")))

(define-public crate-new_york_utils-0.1.9 (c (n "new_york_utils") (v "0.1.9") (d (list (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0a29c88fvs9nb481zvdk3fg6215mhqr49iz760cygjh3ncmnw5r0")))

(define-public crate-new_york_utils-0.1.10 (c (n "new_york_utils") (v "0.1.10") (d (list (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "1j9aj7hxqffcl1q7wjxkbagk5q5drkksj92m9slqwfbkmgl9caax")))

(define-public crate-new_york_utils-0.1.11 (c (n "new_york_utils") (v "0.1.11") (d (list (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0myasnmp56h0j82qic90l1mz330c31p418g2vi7piy4nmf7lr1i3")))

(define-public crate-new_york_utils-0.1.12 (c (n "new_york_utils") (v "0.1.12") (d (list (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "15dqy4n8fa8kzfka79q06q0vxcvlqj7kv63mrhqg4chhjq5vy6y6")))

