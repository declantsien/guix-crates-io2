(define-module (crates-io ne w_ new_flattened) #:use-module (crates-io))

(define-public crate-new_flattened-0.1.0 (c (n "new_flattened") (v "0.1.0") (h "0jlkq8ag09v00mbsg2cvdsc7qg177mj6wmiv65wa53w102g7qcp0")))

(define-public crate-new_flattened-0.1.1 (c (n "new_flattened") (v "0.1.1") (h "1sg1jx2la4wk5izvkkfr76qbfl0jpkmwrmp7fzbbaz40i4ck94sr")))

