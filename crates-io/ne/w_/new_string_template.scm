(define-module (crates-io ne w_ new_string_template) #:use-module (crates-io))

(define-public crate-new_string_template-1.0.0 (c (n "new_string_template") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bfmlrnfbhqpz766zhl4569wxgl9s8dplq5yvz7pc46lnghjla6n")))

(define-public crate-new_string_template-1.1.0 (c (n "new_string_template") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18y02h3n82yr49qdg6hmv550c8z0736mlz6yjzqx0kv9rsvmgv3d")))

(define-public crate-new_string_template-1.2.0 (c (n "new_string_template") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "038n21819qibnp1ivqi1f8h9c3a0i93c5jpdrix55lg1abk4f215")))

(define-public crate-new_string_template-1.3.0 (c (n "new_string_template") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jjwjq1q9xir48ycbd1lgs5515rwhpy7xzdwc2nw660y2sjhmmrb")))

(define-public crate-new_string_template-1.3.1 (c (n "new_string_template") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dv6a6alzkm2dr42inghvqz7lin8prz6nbclphdfb3j8bicjlza2")))

(define-public crate-new_string_template-1.4.0 (c (n "new_string_template") (v "1.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lcpvkplab8id5lpb1jlx68x4yfw4c515229rn623sbgxrdrqdfp")))

(define-public crate-new_string_template-1.5.0 (c (n "new_string_template") (v "1.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0d8l2cmza7la5lldvmjh4ab49z0ap9l0mf261ypnpn50i5ydlm8g") (r "1.40")))

(define-public crate-new_string_template-1.5.1 (c (n "new_string_template") (v "1.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0bng7844dshccq2mf439khpscy76ay62p13pmq06gkcr6yhz6h5f") (r "1.40")))

