(define-module (crates-io ne w_ new_macro) #:use-module (crates-io))

(define-public crate-new_macro-0.0.0 (c (n "new_macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10d7yqji9fms0cz5wyjc6mj71v13jd1ffngx207mrk0grvk095dc") (y #t)))

