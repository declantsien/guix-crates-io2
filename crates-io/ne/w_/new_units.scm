(define-module (crates-io ne w_ new_units) #:use-module (crates-io))

(define-public crate-new_units-0.1.0 (c (n "new_units") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ix4al50mjird3l432c4lyr8fycrvlfrfs0dmdm286k9pk3785jy")))

(define-public crate-new_units-0.1.1 (c (n "new_units") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1rbcbz0alhh4ivk49g6c17mc8aljl3a049rf97v88qbdxww0k8x8")))

(define-public crate-new_units-0.3.0 (c (n "new_units") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "00b29g6flqjnxywvzn68qa7lnf6m2lwf61b77rs29rdc4zyd8pl4")))

(define-public crate-new_units-0.3.1 (c (n "new_units") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1q8jvvzvp2ha30hm33k2gqscs3bsdpwfv0frdpmcsklk5l32ailn")))

