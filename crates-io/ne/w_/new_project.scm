(define-module (crates-io ne w_ new_project) #:use-module (crates-io))

(define-public crate-new_project-0.1.0 (c (n "new_project") (v "0.1.0") (h "1r73w0r1zapkj1f0whyf1rvij9dy5rcim4p8bl9danh454cbx6c2")))

(define-public crate-new_project-0.1.1 (c (n "new_project") (v "0.1.1") (h "0wa7gqa9ic9k7vby68xihni7rw9lzl73izs38pvkbw6i1285dl0b") (y #t)))

(define-public crate-new_project-0.1.2 (c (n "new_project") (v "0.1.2") (h "1vd0w51gncimfakhxifn8z1ysbsgzs2pbcc3qyii9lab7nb7ns5a") (y #t)))

(define-public crate-new_project-0.1.3 (c (n "new_project") (v "0.1.3") (d (list (d (n "new_project") (r "^0.1.2") (d #t) (k 0)))) (h "1vw0sm24cmkqzqb9klmclcsq89gmwzf283vv92m61w9cpdln4qi2")))

