(define-module (crates-io ne ti netidx-tools-core) #:use-module (crates-io))

(define-public crate-netidx-tools-core-0.12.1 (c (n "netidx-tools-core") (v "0.12.1") (d (list (d (n "netidx") (r "^0.12.1") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0imqff5rxsdd7jvn66s3dqqydz39ynmb3p4vf06zk610shmwkjbz") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default" "krb5_iov"))))))

(define-public crate-netidx-tools-core-0.15.0 (c (n "netidx-tools-core") (v "0.15.0") (d (list (d (n "netidx") (r "^0.15") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1sq7h1baff5b5qjw2v510ig5d3w7a2i5dqa71kc2fs2w65salgdl") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default" "krb5_iov"))))))

(define-public crate-netidx-tools-core-0.16.0 (c (n "netidx-tools-core") (v "0.16.0") (d (list (d (n "netidx") (r "^0.16") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01wn94539j0imi72b51dmrjimmyfbndnl171b2ppc2p9jdf98h62") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default" "krb5_iov"))))))

(define-public crate-netidx-tools-core-0.16.1 (c (n "netidx-tools-core") (v "0.16.1") (d (list (d (n "netidx") (r "^0.16") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1azhpbxs9q3nv0ihshc5rsj0hx7ahg1qyhric7sxxjbsx66cpnnp") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default" "krb5_iov"))))))

(define-public crate-netidx-tools-core-0.16.2 (c (n "netidx-tools-core") (v "0.16.2") (d (list (d (n "netidx") (r "^0.16") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17kp0w5kqgsd1ybrz3j7wfljyfl2yglrkjqgk89sygkk4n3ydyw1") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.17.0 (c (n "netidx-tools-core") (v "0.17.0") (d (list (d (n "netidx") (r "^0.17") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0q1p0v8lcg56afcxqgxa9598vwz2i3dy3j2cpl8afkz7zr5p2i1d") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.18.0 (c (n "netidx-tools-core") (v "0.18.0") (d (list (d (n "netidx") (r "^0.18") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1nbvqi6l0nsk68qkmlvqs8dqzslya3xpwr7dxvq2zqa9zniizpm4") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.19.0 (c (n "netidx-tools-core") (v "0.19.0") (d (list (d (n "netidx") (r "^0.19") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rladvqa21bz0w2fchi6qgsd59h0xvjsjfv1rikh8v4filjmssvi") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.22.0 (c (n "netidx-tools-core") (v "0.22.0") (d (list (d (n "netidx") (r "^0.22.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "091dgpn1fd8d00nvg6pk0x47szdffi6zm7s8a6vhmwayq85n2pb3") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.24.0 (c (n "netidx-tools-core") (v "0.24.0") (d (list (d (n "netidx") (r "^0.24.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13hi6qsr5y1ccbianldl1lzp979sfcb43kiiwd30rkfsix1bxlgr") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.24.2 (c (n "netidx-tools-core") (v "0.24.2") (d (list (d (n "netidx") (r "^0.24.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0x3iz9vsd1bc9br6wrrrhiibzwdk1ch9qc7di2nrsc6scxyy2blx") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.25.0 (c (n "netidx-tools-core") (v "0.25.0") (d (list (d (n "netidx") (r "^0.25.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1sc5ps05wf9p9b1r92sfz123i8pzc2f23a3x1phgydbcijcq5avf") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

(define-public crate-netidx-tools-core-0.26.0 (c (n "netidx-tools-core") (v "0.26.0") (d (list (d (n "netidx") (r "^0.26.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13cq02wgrp6wjmydsn9mdjacxnvhrmli8j108cdra57imhhn94jc") (f (quote (("krb5_iov" "netidx/krb5_iov") ("default"))))))

