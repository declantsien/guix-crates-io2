(define-module (crates-io ne ti netio) #:use-module (crates-io))

(define-public crate-netio-0.1.0 (c (n "netio") (v "0.1.0") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "16c6qjnmfj0wrnmy524khkgk9lkdx8p6jv0js4vk6v7p0hf6k3pr")))

(define-public crate-netio-0.1.1 (c (n "netio") (v "0.1.1") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0sgv9s7pfc3lidw06nsasz51ls80zw9vyx3q6z9nikdj4lgk9dcf")))

(define-public crate-netio-0.1.2 (c (n "netio") (v "0.1.2") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "04phlzsc70y7lyq3578db1d1ykxqnb6k45cm9l7dghs4v4qh7dca")))

(define-public crate-netio-0.1.3 (c (n "netio") (v "0.1.3") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0kg72k5b875vlq23kgd0rn406vn5qm279ssdb252hpfkrb381jyh")))

(define-public crate-netio-0.2.0 (c (n "netio") (v "0.2.0") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0p3q9kw5zln0xrmcrj85ck0l13ihm45g0vfxy4pc0pdysdilrxk0")))

(define-public crate-netio-0.2.1 (c (n "netio") (v "0.2.1") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "18pc6smkvm25fqijv81b54qfxy475famx0vwf8pyrl2ynib0srap")))

(define-public crate-netio-0.2.2 (c (n "netio") (v "0.2.2") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0im1q1znklp0jlzhl96mihbh8l9pmi1zs30svwrk4dwrzkf3d96l")))

(define-public crate-netio-0.2.3 (c (n "netio") (v "0.2.3") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1cwp6iy16rchrpiw477pyfzq3l057147qnp3dbvsy13zvrn9bgm2")))

(define-public crate-netio-0.2.4 (c (n "netio") (v "0.2.4") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1yr1vb3gchdix4w1n8hsj9hr0bj4cyd3sa30046wdmjlvsqscjk3")))

(define-public crate-netio-0.2.5 (c (n "netio") (v "0.2.5") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1vk40bvfpxcfjvpssjzn7wm4qpjbak3z78vyja3k2hal0cxx3adx")))

(define-public crate-netio-0.2.6 (c (n "netio") (v "0.2.6") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1wi7jhig2xqvi052pfbyw6yyd0hh5c3yhw48ij8pkx4fqw6vni9g")))

(define-public crate-netio-0.3.0 (c (n "netio") (v "0.3.0") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "036qlwj1i4gz478hawlbmyrqb2ck3znfjakvlfwwx6c35midxl0m")))

(define-public crate-netio-0.2.7 (c (n "netio") (v "0.2.7") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0kmsc1djf35l190xbpdyrqj20ygs1634cxl4wmlhxnvhal57974f")))

(define-public crate-netio-0.3.1 (c (n "netio") (v "0.3.1") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0kcx1qxyx65f1v81sx92f3l61hkvk6r3w6p2li67fk70w4n9r9gc")))

(define-public crate-netio-0.1.4 (c (n "netio") (v "0.1.4") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1r19qyk2c3vq6m57z2qlf6gb5q1pgrp1fbhnf305v5vlnvdzdrn9")))

(define-public crate-netio-0.2.8 (c (n "netio") (v "0.2.8") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1vk9a3il4f671xxcwzx9zbzixwvvbkxls8ya5v2km5h9ckylvk7s")))

(define-public crate-netio-0.3.2 (c (n "netio") (v "0.3.2") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "09v8yfhdc2mz7v7cb211vq530q9nyla4jh9z22qyymhmz1bay387")))

(define-public crate-netio-0.4.0 (c (n "netio") (v "0.4.0") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0mkk81fjs8ny573v861zr45c6sninfmqzawrmr2l9hkqb2zqasqi")))

(define-public crate-netio-0.4.1 (c (n "netio") (v "0.4.1") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0cq0sgmxnzcw54xnj4nqs6fkbazqvbzh42vi29dagwr69bqc1524")))

(define-public crate-netio-0.5.0 (c (n "netio") (v "0.5.0") (d (list (d (n "buf_redux") (r "= 0.3.0") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "15ihzlxf1sqmnp6jw6j4vkrxsn21p3rsq2x4lxhyya0an1s2nnr9")))

(define-public crate-netio-0.2.9 (c (n "netio") (v "0.2.9") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "09lqcprjc6yc5ywfb2h495lsczyzqpq0hnk0q8p8amh3h2lgd1g0")))

(define-public crate-netio-0.3.3 (c (n "netio") (v "0.3.3") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0wka884zhgvi4zrknv3y1lvfcbrm17m0day3bryzygddkv4r5xpa")))

(define-public crate-netio-0.4.2 (c (n "netio") (v "0.4.2") (d (list (d (n "buf_redux") (r "< 0.3") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "1y4c6fnhv9z0w5s81sy3hzzayg5f95gyry08m2l30i8a5fssalaa")))

(define-public crate-netio-0.5.1 (c (n "netio") (v "0.5.1") (d (list (d (n "buf_redux") (r "= 0.3.0") (d #t) (k 0)) (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "0vv1qwbvbj18f50v7c88b0r7znzrdxsk66bky87x738xh3dxpxqz")))

(define-public crate-netio-0.6.0 (c (n "netio") (v "0.6.0") (d (list (d (n "buf_redux") (r "= 0.6") (d #t) (k 0)) (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "1daig61lw85794zjf5ndqp4gxcbdh2zfh892vb4b8vzrlapj05m6")))

(define-public crate-netio-0.6.1 (c (n "netio") (v "0.6.1") (d (list (d (n "buf_redux") (r "= 0.6") (d #t) (k 0)) (d (n "memchr") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0dlaya28sdh49w33kcrkxxq9pblp8lgv46rg11fykghqqrvx8y5q")))

