(define-module (crates-io ne ti netidx-derive) #:use-module (crates-io))

(define-public crate-netidx-derive-0.16.0 (c (n "netidx-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zgd5q9hw71idyizd1amz531j2fbx6asb68ickpmchi5dhaqymys")))

(define-public crate-netidx-derive-0.16.1 (c (n "netidx-derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02vgzic119vfqpazz4c7h8mb9j69cq7r41cp4nh1b9lr3z6c4faf")))

(define-public crate-netidx-derive-0.17.0 (c (n "netidx-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01x0ff7s9iv7qi1j9lpk93dm3qjrvxn3s12a8288a2l7hqj7frmk")))

(define-public crate-netidx-derive-0.17.1 (c (n "netidx-derive") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s7slv5aajqq270xxd0ps9292ciw6ga4mk94rwqaazbi8wyipx1i")))

(define-public crate-netidx-derive-0.17.2 (c (n "netidx-derive") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1m0ln6ybmvbrl0lijfbs1djlc5qarsdmrn1l46g8f455zlsn55sn")))

(define-public crate-netidx-derive-0.18.0 (c (n "netidx-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "17a2s4jlk238jbq3mya7hbpqfbnqh1if68nvhv2jbkbpk5zx6rc5")))

(define-public crate-netidx-derive-0.18.1 (c (n "netidx-derive") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0cw8bn2g957ljr2jvp84xy2gps9sdp4jb426f0xklf4zs2j3x1ad")))

(define-public crate-netidx-derive-0.18.2 (c (n "netidx-derive") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02riryn1w4ns0qlw3jyp4s5lcpcz1s0240sgzm7jprs8xbnk1ib7")))

(define-public crate-netidx-derive-0.22.0 (c (n "netidx-derive") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1sjr8mr3i28h35i1a10xgjxgbn9085gjgwqx8rff1r3nkciv79lm")))

