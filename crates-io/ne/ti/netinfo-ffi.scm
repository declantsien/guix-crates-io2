(define-module (crates-io ne ti netinfo-ffi) #:use-module (crates-io))

(define-public crate-netinfo-ffi-0.3.0 (c (n "netinfo-ffi") (v "0.3.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "netinfo") (r "^0.3.0") (d #t) (k 0)))) (h "03r8a4ksg1bpv1ikqqxyrznc79rva9gfwkbr3g5ldjm386sjwn2j")))

(define-public crate-netinfo-ffi-0.5.0 (c (n "netinfo-ffi") (v "0.5.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "netinfo") (r "^0.5.0") (d #t) (k 0)))) (h "0yyqqx1vgc710351dwcbyad8j1cl54q06gpq5pw3h3gkj7gk2l0g")))

