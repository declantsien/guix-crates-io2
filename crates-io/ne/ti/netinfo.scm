(define-module (crates-io ne ti netinfo) #:use-module (crates-io))

(define-public crate-netinfo-0.1.0 (c (n "netinfo") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.14.0") (d #t) (k 0)))) (h "1430nv5h2zr7pbkrmal9zih6k2zz21j5rarhnswzd76zj1lfxcfs")))

(define-public crate-netinfo-0.1.1 (c (n "netinfo") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.14.0") (d #t) (k 0)))) (h "16728khnm9wk9856ibrnfgw24527shcb8gra5hci8sic5w0vq4a5")))

(define-public crate-netinfo-0.2.0 (c (n "netinfo") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.14.0") (d #t) (k 0)))) (h "1mnfb3w4wkw6aw5qpygipixz1kmjn014mjln6gmcldaadmvi9gfp")))

(define-public crate-netinfo-0.3.0 (c (n "netinfo") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.14.0") (d #t) (k 0)))) (h "00gi50jf8kfcmbyz70dgblglv3wb7amij9aa8zxvbcihgd9n6rxw")))

(define-public crate-netinfo-0.3.1 (c (n "netinfo") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.15.0") (d #t) (k 0)))) (h "0ld2jhj8ncvvfqa8ngrh37yyi1z25l14n03pvgyk5g3zsc1nb45q")))

(define-public crate-netinfo-0.4.0 (c (n "netinfo") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (d #t) (k 0)))) (h "1vimv3c0sawamjq733kpdxqvmcqls84wql8065fn2pvv3b46r6a1")))

(define-public crate-netinfo-0.5.0 (c (n "netinfo") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (d #t) (k 0)))) (h "15s308xg5qplrqppsmib7r038s1fv8nsnz9zz870vnz4hpvm2x4w")))

(define-public crate-netinfo-0.5.1 (c (n "netinfo") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (d #t) (k 0)))) (h "09knw2x9rl7hg7hwkq0flz9578g1i4vsqvbarniqcn8zvsk6b14r")))

