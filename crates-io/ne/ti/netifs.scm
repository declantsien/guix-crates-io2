(define-module (crates-io ne ti netifs) #:use-module (crates-io))

(define-public crate-netifs-0.1.0 (c (n "netifs") (v "0.1.0") (d (list (d (n "eui48") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "winerror" "ifdef" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "10fa77z60bdmzmmwy2yvrxk0iki6m3mk1c5qi258dimmhbfk8nai")))

(define-public crate-netifs-0.2.0 (c (n "netifs") (v "0.2.0") (d (list (d (n "eui48") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "winerror" "ifdef" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "0ymkykzs4mir070cfxrvw6hvw91hkjikky4l6jdf4djirvs94jpw")))

(define-public crate-netifs-0.3.0 (c (n "netifs") (v "0.3.0") (d (list (d (n "eui48") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "winerror" "ifdef" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "091zr93sr0phdzm4z1fbkc661mdky3y7v1hhw845p41q2bink1cc")))

