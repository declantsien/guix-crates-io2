(define-module (crates-io ne ed needleman) #:use-module (crates-io))

(define-public crate-needleman-0.1.0 (c (n "needleman") (v "0.1.0") (h "1bd5ci87k9231xszgm7d3hxd4vii01mvshsnsp12b9b0yxfmlxi7")))

(define-public crate-needleman-0.2.0 (c (n "needleman") (v "0.2.0") (h "0vghb9dl2vmrqrhhi1j7vb6q5mm55rbfnwkl00921jl7a43q99n3")))

