(define-module (crates-io ne ed needle) #:use-module (crates-io))

(define-public crate-needle-0.1.0 (c (n "needle") (v "0.1.0") (h "13zgw1av13syjnjm2s21irvhng9v0x25jbk0fd920jnhnrspv3rc")))

(define-public crate-needle-0.1.1 (c (n "needle") (v "0.1.1") (h "0hyapiab7ma0ncj8j9427iz7f2b5yv5mjlbvlva7gb6k789a4b88")))

