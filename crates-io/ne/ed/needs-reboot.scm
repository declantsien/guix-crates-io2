(define-module (crates-io ne ed needs-reboot) #:use-module (crates-io))

(define-public crate-needs-reboot-0.1.0 (c (n "needs-reboot") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mmwj7ykrryyf1n473q7h6y036rdr5avyblnzi0xacl4ap5yzmb7") (f (quote (("tool" "clap"))))))

(define-public crate-needs-reboot-0.1.1 (c (n "needs-reboot") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14czq5zb2qb66z5rxb80in2z6hvw9hl7l85ihh1wdfwas1rp4fmq") (f (quote (("tool" "clap"))))))

(define-public crate-needs-reboot-0.1.2 (c (n "needs-reboot") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0117wk5jqfxfa0qv7v8d49bjdfcjgksy55nbkhgq5vc47kb7kzrv") (f (quote (("tool" "clap"))))))

