(define-module (crates-io ne rd nerd-font-symbols) #:use-module (crates-io))

(define-public crate-nerd-font-symbols-0.1.0 (c (n "nerd-font-symbols") (v "0.1.0") (h "108vzw72y95myvjm4d91vf8g6gsfg4rv705ivd4ap00p44y2gaw2")))

(define-public crate-nerd-font-symbols-0.2.0 (c (n "nerd-font-symbols") (v "0.2.0") (h "1jrpi1y4fiycrxylj3xnyzin8g8isqjpbldlcz5j3vgkf5h8x54c")))

