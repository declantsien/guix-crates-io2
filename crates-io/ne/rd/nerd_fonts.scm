(define-module (crates-io ne rd nerd_fonts) #:use-module (crates-io))

(define-public crate-nerd_fonts-0.1.0 (c (n "nerd_fonts") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0dqgn4y4jq7shy5r6c3fibxls1lbh9yi0hy7nn8k2rhx72k3di22") (y #t)))

(define-public crate-nerd_fonts-0.1.1 (c (n "nerd_fonts") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "10nifj4zwlnpxjq2dncdyhf5pczr3524szniz1c1zn5ncb2ncswa") (y #t)))

(define-public crate-nerd_fonts-0.1.2 (c (n "nerd_fonts") (v "0.1.2") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1k3jz43lgb1jriy5wp4lmp7v8pgxyfg6wyq6ylw5bbs6c0bkhz09") (y #t)))

(define-public crate-nerd_fonts-0.1.3 (c (n "nerd_fonts") (v "0.1.3") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1c7pv28bjndp0f10dn8q1zqcq6cy1gkn56hm7yc9f9ala5i0q0pd") (y #t)))

(define-public crate-nerd_fonts-0.1.4 (c (n "nerd_fonts") (v "0.1.4") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1x72ln4sp6mp5r3kns7c3zajp12p16v5r0zragnwrqidg7pmdhwc") (y #t)))

(define-public crate-nerd_fonts-0.1.5 (c (n "nerd_fonts") (v "0.1.5") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0fy9dmyng2g2gg2jj06kh3j106sclhw62rbhn3sb3fxa3dxdczs7")))

(define-public crate-nerd_fonts-0.1.6 (c (n "nerd_fonts") (v "0.1.6") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0w0ddd4gdip2whpwwlzp0xdrwnhrzv1zc5n4n2zh8z9yzbr0ffws")))

(define-public crate-nerd_fonts-0.1.7 (c (n "nerd_fonts") (v "0.1.7") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "06s1ibifziwzs9jd7qfvmy27lzjp53jpdbcnz3mxdk9igr7kh7k2")))

(define-public crate-nerd_fonts-0.1.8 (c (n "nerd_fonts") (v "0.1.8") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1yl1g09ck3h39wvp91d4w4zclmpjy2hgiwdcjqh951y0xjzxmbf7")))

(define-public crate-nerd_fonts-0.1.9 (c (n "nerd_fonts") (v "0.1.9") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "06gf4gi2czbw6xqwwyj89k0k4caz1myvzgxhdnvzcdzldkq2zghq")))

