(define-module (crates-io ne sd nesdie) #:use-module (crates-io))

(define-public crate-nesdie-0.1.0 (c (n "nesdie") (v "0.1.0") (d (list (d (n "wee_alloc") (r "^0.4.5") (o #t) (k 0)))) (h "18v3rb09carvyza5axci2fs3ja7vg7j8mh3v0vll0976yfddv0zr") (f (quote (("panic-message") ("oom-handler") ("default" "wee_alloc"))))))

(define-public crate-nesdie-0.2.0 (c (n "nesdie") (v "0.2.0") (d (list (d (n "near-primitives-core") (r "=0.4.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "near-sys") (r "^0.1") (d #t) (k 0)) (d (n "near-vm-logic") (r "=4.0.0-pre.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (k 0)))) (h "175m1xyflgg8kdw0vd5k6kmavsciqkaq9cdzi7ahsmigi5ygnka1") (f (quote (("std") ("panic-message") ("oom-handler") ("default" "wee_alloc"))))))

