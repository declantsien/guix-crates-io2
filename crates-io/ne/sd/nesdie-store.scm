(define-module (crates-io ne sd nesdie-store) #:use-module (crates-io))

(define-public crate-nesdie-store-0.2.0 (c (n "nesdie-store") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9") (k 0)) (d (n "nesdie") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1gggb3mi0iw5xgbmlg8smxz3k2fiwsx44873bc3ram773cknqrn4")))

