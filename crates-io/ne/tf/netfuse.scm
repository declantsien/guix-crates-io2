(define-module (crates-io ne tf netfuse) #:use-module (crates-io))

(define-public crate-netfuse-0.1.0 (c (n "netfuse") (v "0.1.0") (d (list (d (n "fuse") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0.13") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1qivwkj5gw4gyhwl5v9ayaf7g2mxdihzf4g6qh5kcpmrjipgfv9j")))

(define-public crate-netfuse-0.1.1 (c (n "netfuse") (v "0.1.1") (d (list (d (n "fuse") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0.13") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1nn8n74m16f3200svl9ihl6jnzr8wqcnjqd1s8q9v1nrb05k5zlj")))

