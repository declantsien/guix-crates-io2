(define-module (crates-io ne tf netfilter_queue) #:use-module (crates-io))

(define-public crate-netfilter_queue-0.1.0 (c (n "netfilter_queue") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "13d4k2cf46a5n8hla2hii11b9vxwn79d2p2bfpljsa6425qcgjag")))

(define-public crate-netfilter_queue-0.1.1 (c (n "netfilter_queue") (v "0.1.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0gif3kysma2sgvn4xkn2sp9mvx7nrzdnbvhx9dcjcqxvha2nplrv")))

(define-public crate-netfilter_queue-0.1.2 (c (n "netfilter_queue") (v "0.1.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "1fzv50yjm3jzy09kxxhiidhqafdkiil9mjj6v7ii872f849lhg59")))

(define-public crate-netfilter_queue-0.1.3 (c (n "netfilter_queue") (v "0.1.3") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0gn7acbhhcxdnsvrnip5rg1qr962pvdm74wvay7ak66zph1c5knx")))

(define-public crate-netfilter_queue-0.1.4 (c (n "netfilter_queue") (v "0.1.4") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0445pnp585ylh473g4zyrh4f3zl83viilf8d7c5lngbpiacj8c8v")))

(define-public crate-netfilter_queue-0.1.5 (c (n "netfilter_queue") (v "0.1.5") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0s6isg9mgvkawfvh67ppmmqgq7cjhdsiq18jrlk371j7y6xqdggk")))

(define-public crate-netfilter_queue-0.1.6 (c (n "netfilter_queue") (v "0.1.6") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0mmaj1w86almp78c5h229f99anww7ks15pg8y2idgsa067aw6846")))

(define-public crate-netfilter_queue-0.1.7 (c (n "netfilter_queue") (v "0.1.7") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0hqq8b6xashn752vk2k13ipil5iqbqpmnnrsjzz22w63i3mb7pa6")))

(define-public crate-netfilter_queue-0.1.8 (c (n "netfilter_queue") (v "0.1.8") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "09fv9nysz7jvr1zrgs154ddvvf733a9ymf517zgjsnsn5vw7k59z")))

(define-public crate-netfilter_queue-0.2.0 (c (n "netfilter_queue") (v "0.2.0") (d (list (d (n "errno") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "1rl5fzz82fn9m6y0c5vp10wyg02q4byy2cpip0j0nl1d1ia7h05x")))

(define-public crate-netfilter_queue-0.2.1 (c (n "netfilter_queue") (v "0.2.1") (d (list (d (n "errno") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "06pqhw544hrq3ikn4hxwsqs8b51k5j5wy24h2rc7iiq2va568ldv")))

