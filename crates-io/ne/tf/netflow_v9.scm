(define-module (crates-io ne tf netflow_v9) #:use-module (crates-io))

(define-public crate-netflow_v9-0.2.0 (c (n "netflow_v9") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j7rfw2jayfrppsl51kgynr158c5p9y85qqdgvd82v7blzm9hqhn")))

(define-public crate-netflow_v9-0.2.1 (c (n "netflow_v9") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zygfgvkswcihjpf57hv1ldv7scaip88w8wqivwzi35lkxjcrvf1")))

