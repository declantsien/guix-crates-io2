(define-module (crates-io ne tg netget-cli) #:use-module (crates-io))

(define-public crate-netget-cli-1.0.0 (c (n "netget-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fspp") (r "^1.1.0") (d #t) (k 0)) (d (n "netget") (r "^0.1.0") (d #t) (k 0)) (d (n "piglog") (r "^1.3.1") (d #t) (k 0)) (d (n "piglog-netget-config") (r "^1.0.1") (d #t) (k 0)))) (h "1fvc8rc54i6f1wk1x5gv6vmjmscasjr8clx7hh4isy5y98p7gr1z")))

(define-public crate-netget-cli-1.0.1 (c (n "netget-cli") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fspp") (r "^1.1.0") (d #t) (k 0)) (d (n "netget") (r "^0.1.2") (d #t) (k 0)) (d (n "piglog") (r "^1.3.1") (d #t) (k 0)) (d (n "piglog-netget-config") (r "^1.0.1") (d #t) (k 0)))) (h "1x43dg7lg8j7m4bbrxrg9bngw4m0983jwhrpsafq29k7sqqyfkf2")))

