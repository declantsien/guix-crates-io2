(define-module (crates-io ne tg netget) #:use-module (crates-io))

(define-public crate-netget-0.1.0 (c (n "netget") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "06c8xffjr8n4lfaxvznzg73kn7vsjdin4psiwsmgi6gmmp9hcnz6") (y #t)))

(define-public crate-netget-0.1.1 (c (n "netget") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0pdpwdddksppgl0js8gf1hpp997hdirpxq67pqlz74cilbbmjd05") (y #t)))

(define-public crate-netget-0.1.2 (c (n "netget") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0xhcqgl4yhh32l1ni3zkshizpwrzj2kcfpvsdmy4l9v8j14n6ykp")))

