(define-module (crates-io ne tg netgauze-locate) #:use-module (crates-io))

(define-public crate-netgauze-locate-0.1.0 (c (n "netgauze-locate") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0hcmzifvb8bqwzvd3g5srfpv7xlxbhhjk7333vgzjd4vwcsgd5b6")))

(define-public crate-netgauze-locate-0.2.0 (c (n "netgauze-locate") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "084m5d0vk295xhb35qavfvp483bhs44ajra6hcvw0gpgbf03l4gp")))

(define-public crate-netgauze-locate-0.3.0 (c (n "netgauze-locate") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (k 0)))) (h "0ny7pgb9fjy4h4q317yspfvjfbyrxn1vs76xm3ack0q32hwzpwm7")))

