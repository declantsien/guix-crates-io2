(define-module (crates-io ne tg netgauze-parse-utils) #:use-module (crates-io))

(define-public crate-netgauze-parse-utils-0.1.0 (c (n "netgauze-parse-utils") (v "0.1.0") (d (list (d (n "netgauze-locate") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "08mcxbdxfhb8v0sh99xn8vkxp06hfb1w24mand5dgmfihznx4afb") (f (quote (("test-helpers"))))))

(define-public crate-netgauze-parse-utils-0.2.0 (c (n "netgauze-parse-utils") (v "0.2.0") (d (list (d (n "netgauze-locate") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0w9icfpm8sv1c1fqzzyqc437zw1bs70yy6z57vmqr6jair56r64b") (f (quote (("test-helpers"))))))

(define-public crate-netgauze-parse-utils-0.3.0 (c (n "netgauze-parse-utils") (v "0.3.0") (d (list (d (n "netgauze-locate") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1l4fbkz44k5m66myy9yqy2ddd5f7lhcjdc640fnvprp5b76f2sc0") (f (quote (("test-helpers"))))))

