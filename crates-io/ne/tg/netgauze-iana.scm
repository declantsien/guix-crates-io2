(define-module (crates-io ne tg netgauze-iana) #:use-module (crates-io))

(define-public crate-netgauze-iana-0.1.0 (c (n "netgauze-iana") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0vidbgq64900zknkdm19jylj21h7lmrffnd4qd3qk35dgx0v172f")))

(define-public crate-netgauze-iana-0.2.0 (c (n "netgauze-iana") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1l5npr6bw13dakax1i7zdmw0qd0ryv58vjxyy2bxbxhm0chdicyi") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-netgauze-iana-0.3.0 (c (n "netgauze-iana") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0hgb0lcfkkfglyv6frv11zkzsbr6d7q9vh7q7qh3zykiqsp3mznn") (f (quote (("fuzz" "arbitrary") ("default"))))))

