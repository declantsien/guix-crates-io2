(define-module (crates-io ne rv nerve_host) #:use-module (crates-io))

(define-public crate-nerve_host-0.2.0 (c (n "nerve_host") (v "0.2.0") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f9sfb3bjhkj9mnwinjidx35b40zhdlk1p5c4nnbrjq7qymfi3br") (y #t)))

(define-public crate-nerve_host-0.2.1 (c (n "nerve_host") (v "0.2.1") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11hcz99caa4acdwzzfws8irna2yjcgy5mnqsjxnppbqsl4axryv2") (y #t)))

