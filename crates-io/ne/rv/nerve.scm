(define-module (crates-io ne rv nerve) #:use-module (crates-io))

(define-public crate-nerve-0.0.1-placeholder (c (n "nerve") (v "0.0.1-placeholder") (h "1z1v1q28d5zyz60js1c57v9hwzaxg61dcyipz6bl7m0a72c16isk") (y #t)))

(define-public crate-nerve-0.1.0 (c (n "nerve") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "156lbpxfb45cb61ixkzsm5ca4kmz4asr8mda5c6fq8yyc12zqdjn") (y #t)))

(define-public crate-nerve-0.1.1 (c (n "nerve") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1s3asa9nsyil7n84vknb1rm46ikijs51zn5m43nahr8vircg835m") (y #t)))

(define-public crate-nerve-0.2.0 (c (n "nerve") (v "0.2.0") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "nerve_domain") (r "^0.2.0") (d #t) (k 0)) (d (n "nerve_host") (r "^0.2.0") (d #t) (k 0)) (d (n "nerve_port") (r "^0.2.0") (d #t) (k 0)) (d (n "nerve_uri") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1nbvjn02ah2xrb534p0vn9gkzbhd1hn5zgzckyz1883g0mqq2936") (y #t)))

(define-public crate-nerve-0.2.1 (c (n "nerve") (v "0.2.1") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 2)) (d (n "nerve_domain") (r "^0.2.1") (d #t) (k 0)) (d (n "nerve_host") (r "^0.2.1") (d #t) (k 0)) (d (n "nerve_port") (r "^0.2.1") (d #t) (k 0)) (d (n "nerve_uri") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0mxbnd5vzdfn0r284hmvyn8fclxylc3lkr1zx4l4y4kcv4wr7zgk") (y #t)))

