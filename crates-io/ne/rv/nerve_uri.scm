(define-module (crates-io ne rv nerve_uri) #:use-module (crates-io))

(define-public crate-nerve_uri-0.2.0 (c (n "nerve_uri") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "13wc7jscmjbi7j0rlf0qp7pdhyzmz35sh7d0gy2mc79587f0l66v")))

(define-public crate-nerve_uri-0.2.1 (c (n "nerve_uri") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0nhyir034wd9h6ihd45ng3axxfv0h47lpzy9pz2lygmn7sqmfvsv")))

