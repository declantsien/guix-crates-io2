(define-module (crates-io ne rv nerve_base) #:use-module (crates-io))

(define-public crate-nerve_base-0.2.0 (c (n "nerve_base") (v "0.2.0") (d (list (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nz2qm1qbmbza71l1fjgbnvwvkffb7v79mdp3vwpmsia3igzpwlf") (y #t)))

(define-public crate-nerve_base-0.2.1 (c (n "nerve_base") (v "0.2.1") (d (list (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0mmp7v52y1ww25jrm2n8y1cii420vy42b3h1lvj8zaggymxzxl1r") (y #t)))

