(define-module (crates-io ne rv nerve_domain) #:use-module (crates-io))

(define-public crate-nerve_domain-0.2.0 (c (n "nerve_domain") (v "0.2.0") (d (list (d (n "dns-lookup") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1ga5yspq58cn1caxwl0vf82z2kvp5gg4in33ph4xssmvvwn355pb") (y #t)))

(define-public crate-nerve_domain-0.2.1 (c (n "nerve_domain") (v "0.2.1") (d (list (d (n "dns-lookup") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0zgv2pbxz1zz9wwpa6mjz1q1alg3cyrab1bpmbq9hj2kchbz40hr") (y #t)))

