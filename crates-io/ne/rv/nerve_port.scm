(define-module (crates-io ne rv nerve_port) #:use-module (crates-io))

(define-public crate-nerve_port-0.2.0 (c (n "nerve_port") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xrbrslxm2xv051v83k03h6b536pcffpl27dq6p5c3n5148kwvcf") (y #t)))

(define-public crate-nerve_port-0.2.1 (c (n "nerve_port") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nerve_base") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzgzd5pjmq2wlcg0mbyrm86qz5n6mh0sqhlswlcwi6w4p2nn6zh") (y #t)))

