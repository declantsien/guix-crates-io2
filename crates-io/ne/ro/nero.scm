(define-module (crates-io ne ro nero) #:use-module (crates-io))

(define-public crate-nero-0.0.1 (c (n "nero") (v "0.0.1") (d (list (d (n "base64") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "11j381yy2jv69fv4cl80qrnznkj628igggj6hq0y29gvv86p2h0n")))

