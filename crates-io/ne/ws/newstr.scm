(define-module (crates-io ne ws newstr) #:use-module (crates-io))

(define-public crate-newstr-0.1.0 (c (n "newstr") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (o #t) (d #t) (k 0)))) (h "1yqhb0v21yjhrbza770ji99i8s7d2p9fbki28jw8xgwkb465q93f") (f (quote (("regex_is_valid" "lazy_static" "regex") ("default" "regex_is_valid"))))))

(define-public crate-newstr-0.1.1 (c (n "newstr") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 2)))) (h "1gql78zmzsdb6sykw7kx7c1cg824qhfkqy699qbbm5s9v2m8yw2g")))

(define-public crate-newstr-0.1.2 (c (n "newstr") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "00ba8l6r2v3ysfqzf53y9cclmfar79bl81szadbpbl5xylgimfvw")))

(define-public crate-newstr-0.2.0 (c (n "newstr") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "17iqw9h1cqq4ls820rix88wvfxg63vhg40q9yrlm1p7q8gk9hr0p")))

