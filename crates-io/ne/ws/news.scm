(define-module (crates-io ne ws news) #:use-module (crates-io))

(define-public crate-news-0.0.2 (c (n "news") (v "0.0.2") (h "00ix4hvpz8r37fis93gq1l4820728v1gvzab09qmlnji5kw7pr4j")))

(define-public crate-news-0.1.0 (c (n "news") (v "0.1.0") (h "0wag6w0ls9jd6ww6sq74cbpy4ps4i3p86a6wn0fzpip5sb0azvf8")))

(define-public crate-news-0.1.1 (c (n "news") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1sy9ip9frdn8wj2ijrn5r2h2c45xllx6ad96l73z4avzf20wq9q5")))

(define-public crate-news-0.1.2 (c (n "news") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "0rqph526k5x8bk5lphg62vfzvrfb1g33ja41qxcx6c9qp8fxy2cx")))

(define-public crate-news-0.1.3 (c (n "news") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "0aad7bjn9qsybs31swmazsnkpljnnbss243c1r1rjy13klwfbgkr")))

