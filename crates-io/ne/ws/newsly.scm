(define-module (crates-io ne ws newsly) #:use-module (crates-io))

(define-public crate-newsly-0.1.0 (c (n "newsly") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "simple_tables") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15inpbb5vbcbjjm3pc3iswi1wvp1yvz093v5d9kcv1lwp9vclpih")))

