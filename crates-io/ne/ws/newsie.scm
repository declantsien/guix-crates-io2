(define-module (crates-io ne ws newsie) #:use-module (crates-io))

(define-public crate-newsie-0.1.0 (c (n "newsie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1xcxyd91gpjijmi7q0ym8yjm8hj16pzg62s57w4cwv1c57xk49nq")))

(define-public crate-newsie-0.1.1 (c (n "newsie") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1k9w75gfh6b6mhn6lg2l24pnypc7ia100rdmhj15wqr4j2ywfwzk")))

