(define-module (crates-io ne ge negentropy) #:use-module (crates-io))

(define-public crate-negentropy-0.0.0 (c (n "negentropy") (v "0.0.0") (h "1wajqyrw7az74h5g8kn7n4krxs832djck7b39hkj9z2fl3yav6w6") (y #t)))

(define-public crate-negentropy-0.1.0 (c (n "negentropy") (v "0.1.0") (h "10hdh8q4l9cgg2wbi2w6jbg5sf4f9iaka0sxqpnyjk4mvrjpzcgv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-negentropy-0.2.0 (c (n "negentropy") (v "0.2.0") (h "01ib4aaxk6d343w1b36182wxyrjmchk0vb283yfkwd8gyygq3n32") (f (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.2.1 (c (n "negentropy") (v "0.2.1") (h "1kfvci6xikgi45953ksr3vi1dwlrs1sbipigj7aq816xmpsvh9xj") (f (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.3.0 (c (n "negentropy") (v "0.3.0") (h "1n84xnyxah3rxkwczg7b60zs8kq9d9kglfysr967rp60a8w7nvf4") (f (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.3.1 (c (n "negentropy") (v "0.3.1") (h "1ziaqjz8bph7xqc3fh5fk6cfhd90g1ch1qd0ywj75653g09rfr76") (f (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.4.0 (c (n "negentropy") (v "0.4.0") (h "0byrf88hd9x2ll7pmf29r7wsd5h8qhvdy7xcinfcdzlm73sdrhf1") (f (quote (("std") ("default" "std")))) (y #t)))

