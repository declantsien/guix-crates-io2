(define-module (crates-io ne xa nexara_text_engine) #:use-module (crates-io))

(define-public crate-nexara_text_engine-0.1.0 (c (n "nexara_text_engine") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1g1qb2k6i95yqlv3m3gq5dvpg018nhlc9y7bzr2v8f6fxvjav8q5")))

