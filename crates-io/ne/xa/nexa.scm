(define-module (crates-io ne xa nexa) #:use-module (crates-io))

(define-public crate-nexa-23.1.9 (c (n "nexa") (v "23.1.9") (h "1qh2iy5idnjpb1sdrgzl6j1iqp8yxyllz6z2qa0crhf7dpgbv05i")))

(define-public crate-nexa-24.4.15 (c (n "nexa") (v "24.4.15") (h "1s8ncg96i7ykl192ldr7rk5k4nrvbwmcwmrgpdwq4md2f19110k5")))

