(define-module (crates-io ne bu nebula-fbthrift-graph) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-graph-0.1.0 (c (n "nebula-fbthrift-graph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "19a2si7x288791f3y9rbmz122kaqn0wa9sg7c7r9aziiw3bhrw5k")))

(define-public crate-nebula-fbthrift-graph-0.1.1 (c (n "nebula-fbthrift-graph") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0mmmin7wk3mk1sxwd4vnbkb03raciqb8wnabgq803bxpcqr33c8y")))

(define-public crate-nebula-fbthrift-graph-0.1.2 (c (n "nebula-fbthrift-graph") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0sr92d6ay0vf1bdxfpjjnj0k5w1mw69ij7ajgj61066w2whgqdlg")))

(define-public crate-nebula-fbthrift-graph-0.1.3 (c (n "nebula-fbthrift-graph") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1vi6gls9r44akc2rr2kk0r6gbac9d2gpi4hnqgq6bmi6h006lsc7")))

(define-public crate-nebula-fbthrift-graph-0.1.4 (c (n "nebula-fbthrift-graph") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1ik0scyz5q371p229k5iyi4g702za0g0x4nccx2b2fl05hzanpxj")))

(define-public crate-nebula-fbthrift-graph-0.2.0 (c (n "nebula-fbthrift-graph") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "06k972w49an47clvxkm1l0c9br8ljblfjw2pwp6lghf75arm88b0")))

(define-public crate-nebula-fbthrift-graph-0.3.0 (c (n "nebula-fbthrift-graph") (v "0.3.0") (d (list (d (n "nebula-fbthrift-graph-v1") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-graph-v2") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-graph-v3") (r "^0.3") (k 0)))) (h "0fr2vlf43zsdm7bjdvzx9p91br0irkayg8amk0ha5nyf61m8m4li")))

