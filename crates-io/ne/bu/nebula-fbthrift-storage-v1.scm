(define-module (crates-io ne bu nebula-fbthrift-storage-v1) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-storage-v1-0.2.0 (c (n "nebula-fbthrift-storage-v1") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common-v1")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1176dvyiqibh782ik53ifdwhwji422a9xb2i2271ak9hyn25zzgz")))

(define-public crate-nebula-fbthrift-storage-v1-0.3.0 (c (n "nebula-fbthrift-storage-v1") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v1")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "1cfn3rh8z082gskh8r9jx05bs7n9g38vslskvp7ybrzlp2x2p65d")))

