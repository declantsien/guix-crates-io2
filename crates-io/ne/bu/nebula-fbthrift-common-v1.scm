(define-module (crates-io ne bu nebula-fbthrift-common-v1) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-common-v1-0.2.0 (c (n "nebula-fbthrift-common-v1") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1") (k 0)))) (h "0nfnzh6ya4bx3jhya5sqph00pii16hnrpf8zg7m54ljnq2xlzjga")))

(define-public crate-nebula-fbthrift-common-v1-0.3.0 (c (n "nebula-fbthrift-common-v1") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)))) (h "06an4fwj28h6jgwvk9cmf2lljfvjj0qlr6qlw1wazdlrnf4xbzc2")))

