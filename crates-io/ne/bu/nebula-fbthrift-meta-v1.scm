(define-module (crates-io ne bu nebula-fbthrift-meta-v1) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-meta-v1-0.2.0 (c (n "nebula-fbthrift-meta-v1") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common-v1")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0g5sab7nnywz76125rqk1yv33yn34vjhmx6c0fyakiiabsyp5h9s")))

(define-public crate-nebula-fbthrift-meta-v1-0.3.0 (c (n "nebula-fbthrift-meta-v1") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v1")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "1k85ijs1v68zg0xdlnij12pcj47nfzdzlv91k2xw4zmzajn9bppd")))

