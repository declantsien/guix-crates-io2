(define-module (crates-io ne bu nebula-fbthrift-raftex) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-raftex-0.1.0 (c (n "nebula-fbthrift-raftex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0y4k2w93j7qhzqaz7cijjafr481sajrjfjl9mnwj1m639ggyznfa")))

(define-public crate-nebula-fbthrift-raftex-0.1.1 (c (n "nebula-fbthrift-raftex") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1w2mxkd06mskfdz8iabg33d8wzlidy4hrk6zqxdyq3ifd3xs77l2")))

(define-public crate-nebula-fbthrift-raftex-0.1.2 (c (n "nebula-fbthrift-raftex") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1bdazqs1pfdm4m5xwi9x9vyw82fgx2lc9m594rlfd9ik22cfay20")))

(define-public crate-nebula-fbthrift-raftex-0.1.3 (c (n "nebula-fbthrift-raftex") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1agfbkr3yx1zjzy60ipp07xpfibx45p71x2sshbhcp9mnajw12yr")))

(define-public crate-nebula-fbthrift-raftex-0.1.4 (c (n "nebula-fbthrift-raftex") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1knp62c42bqnfrxyxkjk1rydmx35dskxj55m34iqplqmj0vq0dh1")))

(define-public crate-nebula-fbthrift-raftex-0.2.0 (c (n "nebula-fbthrift-raftex") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0xh0hijj03v025dfs7bnf5im189ywxv7p6i12vy5l37rpj85p7dp")))

(define-public crate-nebula-fbthrift-raftex-0.3.0 (c (n "nebula-fbthrift-raftex") (v "0.3.0") (d (list (d (n "nebula-fbthrift-raftex-v1") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-raftex-v2") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-raftex-v3") (r "^0.3") (k 0)))) (h "1wxbqffnmqycsk0hkzqbva5whpapvwvfcagv7kvrmqfa3m571qx7")))

