(define-module (crates-io ne bu nebula-graph-fbthrift-common) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-common-0.1.0 (c (n "nebula-graph-fbthrift-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (k 0)) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "0nkr6fzijnf5q7nbhf3r8gm6kk6k8w54wsdb5b0s0191zd3n1mpx")))

(define-public crate-nebula-graph-fbthrift-common-0.1.1 (c (n "nebula-graph-fbthrift-common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "1z3hh2bwqvfjp9wcxh1x0d8pg1szcs8a4axn543dkpcsdyi6yiw1")))

