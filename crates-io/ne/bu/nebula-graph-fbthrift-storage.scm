(define-module (crates-io ne bu nebula-graph-fbthrift-storage) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-storage-0.1.0 (c (n "nebula-graph-fbthrift-storage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1x3d2f53rvz47a18v34g67iwsyackn5qyawnfbiigv0kcxhh3v04")))

(define-public crate-nebula-graph-fbthrift-storage-0.1.1 (c (n "nebula-graph-fbthrift-storage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1l7kgl8r03m1cxy95kfzjw7g2ddi8hd6a4nk55rxfj1ajrrgskph")))

