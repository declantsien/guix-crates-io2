(define-module (crates-io ne bu nebulae) #:use-module (crates-io))

(define-public crate-nebulae-1.0.0 (c (n "nebulae") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1n10v1brlmvcdi88z084zkaxx49kv7xlalvk4i9jc71ja9ysh6y3")))

(define-public crate-nebulae-2.0.0 (c (n "nebulae") (v "2.0.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0cqpdjw42rs7yzdcbnva7h4njx96f425lnrr3jwz3krvf86qvh4q")))

