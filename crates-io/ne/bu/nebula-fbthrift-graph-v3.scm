(define-module (crates-io ne bu nebula-fbthrift-graph-v3) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-graph-v3-0.0.0 (c (n "nebula-fbthrift-graph-v3") (v "0.0.0") (h "0naqdjgrbrkvfxhrafznbg0034qnvwmjqnpsk1gj4wix8ky3gash")))

(define-public crate-nebula-fbthrift-graph-v3-0.3.0 (c (n "nebula-fbthrift-graph-v3") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v3")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "191pmvfmplynpam08vp9xz2grm75dyxg6ssnhmhnnfv9q4jfpip5")))

