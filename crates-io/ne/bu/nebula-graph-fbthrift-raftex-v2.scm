(define-module (crates-io ne bu nebula-graph-fbthrift-raftex-v2) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-raftex-v2-0.1.0 (c (n "nebula-graph-fbthrift-raftex-v2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "04jh3d2li7l37lgj2xrssn2lqkr23nb0jws09sn8c75krhcrbac4")))

(define-public crate-nebula-graph-fbthrift-raftex-v2-0.1.1 (c (n "nebula-graph-fbthrift-raftex-v2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1kc3h9kfs9qy60ab5xv0dbgnklmiw8grdjsmay9fmgkf3shlh6qh")))

(define-public crate-nebula-graph-fbthrift-raftex-v2-0.1.2 (c (n "nebula-graph-fbthrift-raftex-v2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1axji4f9vha3wf08pamzn5al4naswkhkrj10kln4jhnczfhzr0j2")))

