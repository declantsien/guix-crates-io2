(define-module (crates-io ne bu nebula-fbthrift-common-v3) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-common-v3-0.0.0 (c (n "nebula-fbthrift-common-v3") (v "0.0.0") (h "0530128p0y2ixjix1w6svpvi1sy52zfzxjncydks5ww7k34v17ks")))

(define-public crate-nebula-fbthrift-common-v3-0.3.0 (c (n "nebula-fbthrift-common-v3") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "nebula-fbthrift-double") (r "^0.3") (k 0)))) (h "0ixlri02p4gp5ramfir1hdnggzwwj43mvcrhw1dl83hd2c8rw81k")))

