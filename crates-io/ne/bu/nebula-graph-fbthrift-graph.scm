(define-module (crates-io ne bu nebula-graph-fbthrift-graph) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-graph-0.1.0 (c (n "nebula-graph-fbthrift-graph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1p851d1q7n9k5w3ygz39pfcg5vc352l36a81d6nlmdmd6rjfma7g")))

(define-public crate-nebula-graph-fbthrift-graph-0.1.1 (c (n "nebula-graph-fbthrift-graph") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "03s4l3fsykma6b6czb3g175czk2cj0bz4i0gmajyhq50wliwyfi9")))

