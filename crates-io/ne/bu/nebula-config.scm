(define-module (crates-io ne bu nebula-config) #:use-module (crates-io))

(define-public crate-nebula-config-0.1.0 (c (n "nebula-config") (v "0.1.0") (d (list (d (n "ipnet") (r "^2.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hy5rmig55s0b637jn2mwr4m8xvh2y4qdzff7zr7ds431sba83m8")))

