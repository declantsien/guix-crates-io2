(define-module (crates-io ne bu nebula-derive) #:use-module (crates-io))

(define-public crate-nebula-derive-0.0.1 (c (n "nebula-derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l7g5i4p2wqc58b5s3w9swn34z07p966vgfi8dlikv41yy4z4i0p")))

