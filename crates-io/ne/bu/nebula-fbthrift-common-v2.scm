(define-module (crates-io ne bu nebula-fbthrift-common-v2) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-common-v2-0.1.1 (c (n "nebula-fbthrift-common-v2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")))) (h "1cpsa7brnnqi0miawaq7y87dmxsnnwbkxjwg9hxjkmdvdi0cdya6")))

(define-public crate-nebula-fbthrift-common-v2-0.1.2 (c (n "nebula-fbthrift-common-v2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")))) (h "18r0dzx8sg52czpnwrd8k0pprkpq9mk82fcqg0w4fzx2s417sr2d")))

(define-public crate-nebula-fbthrift-common-v2-0.1.3 (c (n "nebula-fbthrift-common-v2") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")))) (h "0l8x7zwikwpddjx4dhbdkh4m2zdv6jb5nkl559maxh1apr666qjw")))

(define-public crate-nebula-fbthrift-common-v2-0.1.4 (c (n "nebula-fbthrift-common-v2") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")))) (h "1pl9mc0z0d1fbiz33154rh3l6lkihws4piqv9xzfsqz3if4g0sfx")))

(define-public crate-nebula-fbthrift-common-v2-0.1.5 (c (n "nebula-fbthrift-common-v2") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")))) (h "0l98br947szcz7cpcx3g5mw7v9wv3cx0bp7k8r8byq6ii4ny86xf")))

(define-public crate-nebula-fbthrift-common-v2-0.2.0 (c (n "nebula-fbthrift-common-v2") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)))) (h "0l78h6glirrx8ra6vig49fbi495spkaxa81nz4d0d6jgjsm25r1v")))

(define-public crate-nebula-fbthrift-common-v2-0.3.0 (c (n "nebula-fbthrift-common-v2") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)))) (h "0ql415jwjrvx966rfg7x6m30pk6gf84yvsgxfrjblzwn1z709bz3")))

