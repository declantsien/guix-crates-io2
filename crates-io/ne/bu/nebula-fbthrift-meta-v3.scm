(define-module (crates-io ne bu nebula-fbthrift-meta-v3) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-meta-v3-0.0.0 (c (n "nebula-fbthrift-meta-v3") (v "0.0.0") (h "04lxcr6zx6lj852blz4cy3x8kh8ba65z1lrvvhmvhh2yl34v8v9z")))

(define-public crate-nebula-fbthrift-meta-v3-0.3.0 (c (n "nebula-fbthrift-meta-v3") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v3")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "0v31vbgc9byvz2jn78av55rgi6dw6lvpi4qnf6y3q9y59gkmfrp4")))

