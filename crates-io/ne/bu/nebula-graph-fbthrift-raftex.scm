(define-module (crates-io ne bu nebula-graph-fbthrift-raftex) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-raftex-0.1.0 (c (n "nebula-graph-fbthrift-raftex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1npvx64qihxk9yyy3cdv8w35c2v0br585b6371hh5apa1vhj2m3p")))

(define-public crate-nebula-graph-fbthrift-raftex-0.1.1 (c (n "nebula-graph-fbthrift-raftex") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0jbi5xlq6ry3z0hm1schg99m4029rlz2z86wplf9sq3svv8p4y4p")))

