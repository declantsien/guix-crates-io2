(define-module (crates-io ne bu nebula) #:use-module (crates-io))

(define-public crate-nebula-0.0.1 (c (n "nebula") (v "0.0.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)) (d (n "warp") (r "^0.2.0") (d #t) (k 0)))) (h "1az16qaws4nn9nds5v53j0v2xv30j0iszdqisrr89mwr99zply4q")))

