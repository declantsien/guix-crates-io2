(define-module (crates-io ne bu nebula-graph-fbthrift-meta-v2) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-meta-v2-0.1.0 (c (n "nebula-graph-fbthrift-meta-v2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1k0rmym570zngl20nb0yd7sq9h50zagfi3hqpnfkcjvmdxjv8x4a")))

(define-public crate-nebula-graph-fbthrift-meta-v2-0.1.1 (c (n "nebula-graph-fbthrift-meta-v2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "17f6bm0kwzgh0bihvsjb4022h099khsj1cp5pvvz6chgxx6y1q8s")))

(define-public crate-nebula-graph-fbthrift-meta-v2-0.1.2 (c (n "nebula-graph-fbthrift-meta-v2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-graph-fbthrift-common-v2")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "01wby9qg5rm7q1hgyln3yag1ijwb313w3zmpfmhk3pp1wj9kp92r")))

