(define-module (crates-io ne bu nebula-fbthrift-common) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-common-0.1.0 (c (n "nebula-fbthrift-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "0lzcqq7yalygysfmkm0k5ahvz97pbzrm0r2fpsxqcfdxgbpjh1fg")))

(define-public crate-nebula-fbthrift-common-0.1.1 (c (n "nebula-fbthrift-common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "16didhfb396j9l3k9wkv4lw7maj2mxw9v9hjdfhnmvbgx0rx9h0x")))

(define-public crate-nebula-fbthrift-common-0.1.2 (c (n "nebula-fbthrift-common") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "17l8pgyibdv5pwf6gqnp895ggipa0silm1kjdzx0f5kgl9idwxya")))

(define-public crate-nebula-fbthrift-common-0.1.3 (c (n "nebula-fbthrift-common") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "0wm7vm1m77s5k7p4iw7i4pw3a16j0v3ycym0zbp3wffm0bcmjlp1")))

(define-public crate-nebula-fbthrift-common-0.1.4 (c (n "nebula-fbthrift-common") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "15ajw4pkjg8dkx1d9h765vlhp9y7rascgkyb9b9ms5v7sc2sa1lm")))

(define-public crate-nebula-fbthrift-common-0.2.0 (c (n "nebula-fbthrift-common") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "lazy_static") (r "^1.4") (k 0)))) (h "1avmbr31avpjwwh1jkwhy043h30hzrxamiqmwa0ydwdbmarv02vm")))

(define-public crate-nebula-fbthrift-common-0.3.0 (c (n "nebula-fbthrift-common") (v "0.3.0") (d (list (d (n "nebula-fbthrift-common-v1") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-common-v2") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-common-v3") (r "^0.3") (k 0)))) (h "1s9swgm87m6r2l7c4n1n1g1jwq2yq2i38s63ls5mi9pambmqz2md")))

