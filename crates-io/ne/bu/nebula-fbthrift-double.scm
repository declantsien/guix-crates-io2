(define-module (crates-io ne bu nebula-fbthrift-double) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-double-0.3.0 (c (n "nebula-fbthrift-double") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)))) (h "0rnvvhgs908zzis96nwjky7722j0q888s0md89yv3wnjcypjsmhm")))

