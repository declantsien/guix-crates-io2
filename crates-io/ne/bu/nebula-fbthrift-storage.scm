(define-module (crates-io ne bu nebula-fbthrift-storage) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-storage-0.1.0 (c (n "nebula-fbthrift-storage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1b81bmwgl7x328jw8g4ishiv0r2kcj6l9hjmciny2m3ym5qjdyb4")))

(define-public crate-nebula-fbthrift-storage-0.1.1 (c (n "nebula-fbthrift-storage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0x2d3csvrx86jr9kajl276lmf5jicqmc25fkxclv6ff409rq76ba")))

(define-public crate-nebula-fbthrift-storage-0.1.2 (c (n "nebula-fbthrift-storage") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1p0qf4n3j8kx2vd1hgd2sadgyp0xsvh2n93wq2q3v55yzyqawpjy")))

(define-public crate-nebula-fbthrift-storage-0.1.3 (c (n "nebula-fbthrift-storage") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "144zqax6l9r5qsqk1zn6wh453g31r52hzs0g7ad8pyrhc6psg834")))

(define-public crate-nebula-fbthrift-storage-0.1.4 (c (n "nebula-fbthrift-storage") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1845qy59ha9smw2d1pgk5n5gpg02825z7s50r8mymmszs8jjpy14")))

(define-public crate-nebula-fbthrift-storage-0.2.0 (c (n "nebula-fbthrift-storage") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "168k8c3w9baq5x97fh4zdc297sk4ajwhb9kh98i7sribapi7v8qy")))

(define-public crate-nebula-fbthrift-storage-0.3.0 (c (n "nebula-fbthrift-storage") (v "0.3.0") (d (list (d (n "nebula-fbthrift-storage-v1") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-storage-v2") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-storage-v3") (r "^0.3") (k 0)))) (h "0nhwcgz7aq05y0mfhs9ynrvjxk998l9v3inq4qcs6qpvsxhpjkk0")))

