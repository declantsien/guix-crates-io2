(define-module (crates-io ne bu nebula_email) #:use-module (crates-io))

(define-public crate-nebula_email-0.0.1 (c (n "nebula_email") (v "0.0.1") (d (list (d (n "lettre") (r "^0.9.2") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hgh3bf1nv3cs8cl3vvblwp7ys91czj6b89iqkb6858g5pvrwiz1")))

