(define-module (crates-io ne bu nebula-ffi) #:use-module (crates-io))

(define-public crate-nebula-ffi-0.1.0 (c (n "nebula-ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "gobuild") (r "^0.1.0-alpha.2") (d #t) (k 1)))) (h "0jg4nigjvz9dxam1y8f64bkcnpyfdl9mb2lqqdph9casg2crqz49")))

(define-public crate-nebula-ffi-0.1.3 (c (n "nebula-ffi") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "gobuild") (r "^0.1.0-alpha.2") (d #t) (k 1)))) (h "1rnfbxnxjwlygv7q2y27kiqfqjnkj9jy1in4jwxrmpm21gs28560")))

(define-public crate-nebula-ffi-0.1.4 (c (n "nebula-ffi") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "gobuild") (r "^0.1.0-alpha.2") (d #t) (k 1)))) (h "1rx79sw6l0rrwlrp6sf1d9c2k1i4g91jgm26yv28pbr2d2w9iypa")))

(define-public crate-nebula-ffi-0.2.0 (c (n "nebula-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "gobuild") (r "^0.1.0-alpha.2") (d #t) (k 1)))) (h "060z81wwyh8masr95sw890k7xgw2apfl2ljijwn8dwvhp0zn6kx0")))

(define-public crate-nebula-ffi-1.7.2 (c (n "nebula-ffi") (v "1.7.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0x2r87qw0nr130cggh1pyrz8ynvqdxizd97f56syq3y2jgm0fn50")))

(define-public crate-nebula-ffi-1.8.1 (c (n "nebula-ffi") (v "1.8.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0spc2zl2ws96l71piwg4h73hppx6q18zvlbjh6sb6jsz3z4mpwmk")))

