(define-module (crates-io ne bu nebula-fbthrift-raftex-v3) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-raftex-v3-0.0.0 (c (n "nebula-fbthrift-raftex-v3") (v "0.0.0") (h "1kc815y82yhsvspq482m3rk7n6iam1pdpx6hnhydfd1cxry5i798")))

(define-public crate-nebula-fbthrift-raftex-v3-0.3.0 (c (n "nebula-fbthrift-raftex-v3") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v3")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "119vh2a58s783lf8s6g5016disvi6l8xzz34bpjgr9ifnjrrqn8k")))

