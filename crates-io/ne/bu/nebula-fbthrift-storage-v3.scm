(define-module (crates-io ne bu nebula-fbthrift-storage-v3) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-storage-v3-0.0.0 (c (n "nebula-fbthrift-storage-v3") (v "0.0.0") (h "0arh0rly31nf8jc81x4zfaj5r42wdyx0w38542mwxfpxykfqf1px")))

(define-public crate-nebula-fbthrift-storage-v3-0.3.0 (c (n "nebula-fbthrift-storage-v3") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.3.0") (k 0) (p "nebula-fbthrift-common-v3")) (d (n "const-cstr") (r "^0.3") (k 0)) (d (n "fbthrift") (r "=0.0.7") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "meta") (r "=0.3.0") (k 0) (p "nebula-fbthrift-meta-v3")) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)))) (h "1d03wnbdm5zz7a5zwkrxc2iay7cma8837x33n3h9i6p31s7px7xw")))

