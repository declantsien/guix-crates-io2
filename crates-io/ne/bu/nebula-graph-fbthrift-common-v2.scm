(define-module (crates-io ne bu nebula-graph-fbthrift-common-v2) #:use-module (crates-io))

(define-public crate-nebula-graph-fbthrift-common-v2-0.1.0 (c (n "nebula-graph-fbthrift-common-v2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")))) (h "12jav2lm3z8lqafcd4l1vw55iahhibf0bh8f9krwl6xwr9cjbjkz")))

(define-public crate-nebula-graph-fbthrift-common-v2-0.1.1 (c (n "nebula-graph-fbthrift-common-v2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")))) (h "12l5rwkfmbpd6gdwp9sk8ncngfd5ing05bk4xmhv2vkvydyn0nq9")))

(define-public crate-nebula-graph-fbthrift-common-v2-0.1.2 (c (n "nebula-graph-fbthrift-common-v2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")))) (h "0rwq7hpd7230lwpbl7dc3rnqn86vv8rvn0jsxkhch7y420d0xh5f")))

