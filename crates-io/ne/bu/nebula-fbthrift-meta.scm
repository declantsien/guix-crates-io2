(define-module (crates-io ne bu nebula-fbthrift-meta) #:use-module (crates-io))

(define-public crate-nebula-fbthrift-meta-0.1.0 (c (n "nebula-fbthrift-meta") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.1") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "063ciii0a9m2fkp2y13pjbc05dc3h4l7iyhlkdwl7yjw1481p3dm")))

(define-public crate-nebula-fbthrift-meta-0.1.1 (c (n "nebula-fbthrift-meta") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.2") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0sfcjss82r4aan7gcsidh4gbyl23dcv3prz4qpms262cvxis324l")))

(define-public crate-nebula-fbthrift-meta-0.1.2 (c (n "nebula-fbthrift-meta") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "fbthrift") (r "^0.0.3") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1j6r0wiyin8cyf40f7vpw6xqpm4pzh8gydls2qc3zfmnbszskjxz")))

(define-public crate-nebula-fbthrift-meta-0.1.3 (c (n "nebula-fbthrift-meta") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.4") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0gh83phvi1nvq24gvjk06xdaib3d1ssvk66bcvj5scwygg43vnqh")))

(define-public crate-nebula-fbthrift-meta-0.1.4 (c (n "nebula-fbthrift-meta") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "^0.1") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.5") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "1p001xgsm7ir3d61v5jjbwvyfm4qw3d5jzh59mgyfz21bagrlrmq")))

(define-public crate-nebula-fbthrift-meta-0.2.0 (c (n "nebula-fbthrift-meta") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "common") (r "=0.2.0") (k 0) (p "nebula-fbthrift-common")) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "fbthrift") (r "=0.0.6") (k 0) (p "fbthrift-git")) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "10wr1spjhsdh7j4c1pkv2mg29k4vk25wqvcs1qrm37hl5gck5hxg")))

(define-public crate-nebula-fbthrift-meta-0.3.0 (c (n "nebula-fbthrift-meta") (v "0.3.0") (d (list (d (n "nebula-fbthrift-meta-v1") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-meta-v2") (r "^0.3") (k 0)) (d (n "nebula-fbthrift-meta-v3") (r "^0.3") (k 0)))) (h "02jh89h2d8k47b7kddpysxsr4n2j995rb6p1jclql974m1f0kjga")))

