(define-module (crates-io ne li neli-wifi) #:use-module (crates-io))

(define-public crate-neli-wifi-0.1.0 (c (n "neli-wifi") (v "0.1.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0d1f47ws23r91a2a0bzigwn7j9d66h3i48341435dddhrycyhbil")))

(define-public crate-neli-wifi-0.2.0 (c (n "neli-wifi") (v "0.2.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "16ryln31wa5jfxl5lmyzmj1na82ycv9y22jz2c3hd4jdnk9j65h2")))

(define-public crate-neli-wifi-0.3.0 (c (n "neli-wifi") (v "0.3.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "15hfailqyshx5s62mia7kwlv0lzjxk2pj51ajzsbf3g0kcwxg94b") (f (quote (("default") ("async" "neli/async"))))))

(define-public crate-neli-wifi-0.3.1 (c (n "neli-wifi") (v "0.3.1") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0701bxfz2bfb8hqcaq29fn4m0b85qk69iih0hrp6cysq75lhwz9m") (f (quote (("default") ("async" "neli/async"))))))

(define-public crate-neli-wifi-0.4.0 (c (n "neli-wifi") (v "0.4.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02x149bmrx6faqi4w1jzaw0qs144qqjbq1zsmxpsak8zc23ds527") (f (quote (("default") ("async" "neli/async"))))))

(define-public crate-neli-wifi-0.4.1 (c (n "neli-wifi") (v "0.4.1") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xkl44i1vxmpw3m80abyrpiyapplsg03gqh7yfpqkkfm7hx819mv") (f (quote (("default") ("async" "neli/async"))))))

(define-public crate-neli-wifi-0.5.0 (c (n "neli-wifi") (v "0.5.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wx8il0hvwnkh75y1hzybar7han8qv6378p49dnkafwvm24q73dd") (f (quote (("default") ("async" "neli/async")))) (y #t)))

(define-public crate-neli-wifi-0.5.1 (c (n "neli-wifi") (v "0.5.1") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0g0f5bi9y2cxcvqmyaaw8ac0m2dz9d9815zkgq1hxxfzy3p13h86") (f (quote (("default") ("async" "neli/async"))))))

(define-public crate-neli-wifi-0.6.0 (c (n "neli-wifi") (v "0.6.0") (d (list (d (n "neli") (r "^0.6.0") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w5wkg8vzdzmsy8g1g5f65fxdnxxb9nbvfi47m9fjqnsrwr2584y") (f (quote (("default") ("async" "neli/async"))))))

