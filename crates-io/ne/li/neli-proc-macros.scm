(define-module (crates-io ne li neli-proc-macros) #:use-module (crates-io))

(define-public crate-neli-proc-macros-0.1.0 (c (n "neli-proc-macros") (v "0.1.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04fyzk7glkl1s374vljxdcdd4gnyzj74qn5nxbnkpsh8bpkxlpp9")))

(define-public crate-neli-proc-macros-0.1.1 (c (n "neli-proc-macros") (v "0.1.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0484d0cjx05i4krjm102pmnrsnzz2z0q7kxp8px555hyrmmwpn3z")))

(define-public crate-neli-proc-macros-0.1.2 (c (n "neli-proc-macros") (v "0.1.2") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hmasga3pl2d3rnykvb58xkjkk1m8vpm86gd8f0ffi72qi84ygmb")))

(define-public crate-neli-proc-macros-0.1.3 (c (n "neli-proc-macros") (v "0.1.3") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m06j6vgy5zzr6dvnbpacpai6mgwwzd20h17hr3i67iv6x6ijs61")))

(define-public crate-neli-proc-macros-0.2.0-rc1 (c (n "neli-proc-macros") (v "0.2.0-rc1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0saj089a202nr2b5gqqc4rbch3cq9m4v69h3jx4p2df6hrkbl25q")))

(define-public crate-neli-proc-macros-0.2.0-rc2 (c (n "neli-proc-macros") (v "0.2.0-rc2") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zz9zximl62pgj6vrbzzsnwpcv3s5pr6gsd8gn4yhikhldwwmqhw")))

