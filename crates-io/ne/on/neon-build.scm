(define-module (crates-io ne on neon-build) #:use-module (crates-io))

(define-public crate-neon-build-0.1.1 (c (n "neon-build") (v "0.1.1") (h "1138i059an72vqv67k3b54clf57y611jaqy6cyh05gz6v69p6nf5")))

(define-public crate-neon-build-0.1.2 (c (n "neon-build") (v "0.1.2") (h "09fpysg1h1hd252y6zmsb6dj991vv7wdhlh058hyd5awdmphpad3")))

(define-public crate-neon-build-0.1.11 (c (n "neon-build") (v "0.1.11") (h "1ibav8hay17agq8h89fz9q4wcfv4zsbn593ixknssc7j491wbr8n")))

(define-public crate-neon-build-0.1.12 (c (n "neon-build") (v "0.1.12") (h "0vr8d7k54lbab76469xdf12xr60hzwbz3qmslqs8yznwpyghk2z9")))

(define-public crate-neon-build-0.1.13 (c (n "neon-build") (v "0.1.13") (h "16cm0cnqb7d0107i4h4791i8521iva336vv7j1kxq7bisb65161x")))

(define-public crate-neon-build-0.1.14 (c (n "neon-build") (v "0.1.14") (h "1qyvwwv656v9rj5dx3bjcc04v0zsjnj73ww9c5b0nwhz9hk0m63v")))

(define-public crate-neon-build-0.1.15 (c (n "neon-build") (v "0.1.15") (h "14q3kn94v0yqsaf6f2s5wr91mz31x9ds66pxpfpdv5r6m1pf1i0a")))

(define-public crate-neon-build-0.1.16 (c (n "neon-build") (v "0.1.16") (h "1s5azw8kckf55pmr3bh7y9vs64xr6c508afwvwyi4l5ns9ls2zf6")))

(define-public crate-neon-build-0.1.17 (c (n "neon-build") (v "0.1.17") (h "1r6qargdsvvpvjbz9g22kb2gx91qk8qr3c7jcnknj396v1h3rnxl")))

(define-public crate-neon-build-0.1.18 (c (n "neon-build") (v "0.1.18") (h "0xnspgzc04g3vghdc9qlcgxazv7n2x93j9jjdkz5nkxf4sdcm0lm")))

(define-public crate-neon-build-0.1.19 (c (n "neon-build") (v "0.1.19") (h "0f14d08db33c0dax2q37dbrpr4w72402s6rc7kcdbnkvynzcx79w")))

(define-public crate-neon-build-0.1.20 (c (n "neon-build") (v "0.1.20") (h "1jwl8fvkai8vy360gg5sw7a4fll3ypq3rgw7dhcs223mmrla566h")))

(define-public crate-neon-build-0.1.21 (c (n "neon-build") (v "0.1.21") (h "1fgfngyskqi594x658f0lx4rqibh6mq7q6p4qga82yi9nbzcldz1") (y #t)))

(define-public crate-neon-build-0.1.22 (c (n "neon-build") (v "0.1.22") (h "05qa1j8irschgm4krx08y9i7gwcwd7g1s061izgh1b501haw2802")))

(define-public crate-neon-build-0.1.23 (c (n "neon-build") (v "0.1.23") (h "1csb97bnv63zxxvvd9ckw09wn2xxaws1qslvkvk17cr0kadxlgi5")))

(define-public crate-neon-build-0.2.0 (c (n "neon-build") (v "0.2.0") (h "1l3zypncw41x7sn0k7pj8z05bvwacwivgfcmdnpxi0yf305qy347")))

(define-public crate-neon-build-0.3.0 (c (n "neon-build") (v "0.3.0") (h "1m3nhn0b8yj8q5s9l83spmn54zsnrbvb04h6lbhm76w6yzclizkl")))

(define-public crate-neon-build-0.3.1 (c (n "neon-build") (v "0.3.1") (h "1kjy9s8jqnx7lba8sscdb372rbyqcpmag1dijc7wkis7dhybp2ga")))

(define-public crate-neon-build-0.3.2 (c (n "neon-build") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "= 0.3.2") (o #t) (d #t) (k 0)))) (h "0q7aahxinsabzn5w0r5s8c9d1c6qrp43q94rbyfxmaw5dnjcvd5a")))

(define-public crate-neon-build-0.3.3 (c (n "neon-build") (v "0.3.3") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "= 0.3.3") (o #t) (d #t) (k 0)))) (h "0dzkzrdpgb60d2zq2f6w0nig527hskc3p2ijkpk9jhsw0vqnnh5f")))

(define-public crate-neon-build-0.4.0 (c (n "neon-build") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "= 0.4.0") (o #t) (d #t) (k 0)))) (h "1vg2xp2rfhlkjmsvhqhbc99wxn8zi9139lw39x7vh4a7zlm37vgr")))

(define-public crate-neon-build-0.4.1 (c (n "neon-build") (v "0.4.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "=0.4.1") (o #t) (d #t) (k 0)))) (h "0y9n4ls04z2g3n69r76x84nkafvc0f32w1i51qkdxvqyc3qqxz4f") (y #t)))

(define-public crate-neon-build-0.4.2 (c (n "neon-build") (v "0.4.2") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "=0.4.2") (o #t) (d #t) (k 0)))) (h "0wg43xa14w6iadgnixa74jpkkvya3065pkfymcsqnbkj61klhzfq") (y #t)))

(define-public crate-neon-build-0.5.0 (c (n "neon-build") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "=0.5.0") (o #t) (d #t) (k 0)))) (h "0sdfdln0dn88b5ky2wcbvjd5kg4jkcr3ksjn76vxfvxpbn1bp9rr")))

(define-public crate-neon-build-0.5.1 (c (n "neon-build") (v "0.5.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "=0.5.1") (o #t) (d #t) (k 0)))) (h "14nwqc3rq5i3wrw06p3lfp97xkzhi8xm66y4yixp8psrcandaiva")))

(define-public crate-neon-build-0.5.2 (c (n "neon-build") (v "0.5.2") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "neon-sys") (r "=0.5.2") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^1.3.0") (f (quote ("native-tls"))) (t "cfg(windows)") (k 0)))) (h "0lj7m8x36zbwq5sbvvhcn53b0xv5574kfnciijyhpz85pi825iq9")))

(define-public crate-neon-build-0.5.3 (c (n "neon-build") (v "0.5.3") (d (list (d (n "cfg-if") (r ">=0.1.9, <0.2.0") (d #t) (k 0)) (d (n "cfg-if") (r ">=0.1.9, <0.2.0") (d #t) (k 1)) (d (n "neon-sys") (r "=0.5.3") (o #t) (d #t) (k 0)) (d (n "ureq") (r ">=1.3.0, <2.0.0") (f (quote ("native-tls"))) (t "cfg(windows)") (k 0)))) (h "1q4fmh9zc3d2k6prr946lnn2mmn7y0y07nkb16kj9wra79w36pbh")))

(define-public crate-neon-build-0.6.0 (c (n "neon-build") (v "0.6.0") (d (list (d (n "neon-sys") (r "=0.6.0") (o #t) (d #t) (k 0)))) (h "066by655ry4ay2mv56xhzndlrd5xqypncgc755y7yi7pc2bz38gb")))

(define-public crate-neon-build-0.7.0 (c (n "neon-build") (v "0.7.0") (d (list (d (n "neon-sys") (r "=0.7.0") (o #t) (d #t) (k 0)))) (h "0zyvcf114b1fi9swla92cyiqblzg8x5ykdqx5c4fnzp06jvxvi39")))

(define-public crate-neon-build-0.7.1 (c (n "neon-build") (v "0.7.1") (d (list (d (n "neon-sys") (r "=0.7.1") (o #t) (d #t) (k 0)))) (h "1b7d1gl0grijpan5z32ykax1l46zp1n3na7rwjqg2l31h6r9iaj0")))

(define-public crate-neon-build-0.8.0 (c (n "neon-build") (v "0.8.0") (d (list (d (n "neon-sys") (r "=0.8.0") (o #t) (d #t) (k 0)))) (h "0yn3zarnglgnawb6vgyb933k3lxav5w79rnml30fj12bvg6slad5")))

(define-public crate-neon-build-0.8.1 (c (n "neon-build") (v "0.8.1") (d (list (d (n "neon-sys") (r "=0.8.1") (o #t) (d #t) (k 0)))) (h "0cf8nsy54mnjdkp9kv1hvzqgdp6s38ccc7hv2cvz6r5wnrqcmlsz")))

(define-public crate-neon-build-0.8.2 (c (n "neon-build") (v "0.8.2") (d (list (d (n "neon-sys") (r "=0.8.2") (o #t) (d #t) (k 0)))) (h "02yp7g238v0n71irgpiiwabcwybap5ixxdc0979wq4gc8hnr1zn6")))

(define-public crate-neon-build-0.8.3 (c (n "neon-build") (v "0.8.3") (d (list (d (n "neon-sys") (r "=0.8.3") (o #t) (d #t) (k 0)))) (h "1sc9799hd4gmlwszja0qlcnjb2jl15k3fsnrdkzbjmq765nmzigr")))

(define-public crate-neon-build-0.9.0 (c (n "neon-build") (v "0.9.0") (d (list (d (n "neon-sys") (r "=0.9.0") (o #t) (d #t) (k 0)))) (h "1b6sq42s861n8y8i8z1chxr69mhd8dags5g15c7rmv4bpghk0zq6")))

(define-public crate-neon-build-0.9.1 (c (n "neon-build") (v "0.9.1") (d (list (d (n "neon-sys") (r "=0.9.1") (o #t) (d #t) (k 0)))) (h "1l50y9s3kjlq3g193mfd5qsldkiskn4l630s67a5clai7z3fp7xd")))

(define-public crate-neon-build-0.10.0-alpha.3 (c (n "neon-build") (v "0.10.0-alpha.3") (d (list (d (n "neon-sys") (r "=0.10.0-alpha.3") (o #t) (d #t) (k 0)))) (h "0j19aqz7mrr7psxfq7yq0k9cg0c91lcfv0aw4qgrkk4sysqaq3zy")))

(define-public crate-neon-build-0.10.0 (c (n "neon-build") (v "0.10.0") (d (list (d (n "neon-sys") (r "=0.10.0") (o #t) (d #t) (k 0)))) (h "021nnzg1pl1ibw0y8i782abwxqpzqqm38v46v627r0x7n61ccknd")))

(define-public crate-neon-build-0.10.1 (c (n "neon-build") (v "0.10.1") (d (list (d (n "neon-sys") (r "=0.10.1") (o #t) (d #t) (k 0)))) (h "04f8n72ihvp5c6p8ks5vlxv618g4vlgf9zdc7nph86770akrib4b")))

