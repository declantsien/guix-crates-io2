(define-module (crates-io ne on neon-frame-macro) #:use-module (crates-io))

(define-public crate-neon-frame-macro-0.1.0 (c (n "neon-frame-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lj0a19p22gr4wwwakp71zafc1syjh5inp1ah454gp25x87ikkin")))

(define-public crate-neon-frame-macro-0.1.1 (c (n "neon-frame-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hba50am20ahdk30hdfxln9p3f9jrrj5m4yyc9f9g9qsa1jmdzd1")))

