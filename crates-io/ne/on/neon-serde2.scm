(define-module (crates-io ne on neon-serde2) #:use-module (crates-io))

(define-public crate-neon-serde2-0.8.0 (c (n "neon-serde2") (v "0.8.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "neon") (r "^0.8") (f (quote ("default-panic-hook" "napi-6" "try-catch-api" "event-queue-api"))) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wvssg4xj9k16xzw45bqfn8kqlmfsxfsqsh8cx4a4ncpvyc17xs5")))

