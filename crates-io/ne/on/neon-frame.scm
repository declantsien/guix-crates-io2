(define-module (crates-io ne on neon-frame) #:use-module (crates-io))

(define-public crate-neon-frame-0.1.0 (c (n "neon-frame") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x1i2v3i0c37vcsbqmwldinvhw37pxnqizgazgl23ml9m5dghj6v")))

(define-public crate-neon-frame-0.1.1 (c (n "neon-frame") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wmq87f5v9vfql4g01sc0q7c91c1fqzzwmlw86jq0knbx4fd0ixc")))

