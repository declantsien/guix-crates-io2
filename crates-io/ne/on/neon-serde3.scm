(define-module (crates-io ne on neon-serde3) #:use-module (crates-io))

(define-public crate-neon-serde3-0.10.0 (c (n "neon-serde3") (v "0.10.0") (d (list (d (n "neon") (r "^0.10") (f (quote ("default-panic-hook" "napi-6" "try-catch-api" "event-queue-api"))) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0g03ym6g48ddfgff0n30lgmx6qvaw2z1fzc15l7zjdafnqa47k42")))

