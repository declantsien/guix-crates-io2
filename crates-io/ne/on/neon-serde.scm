(define-module (crates-io ne on neon-serde) #:use-module (crates-io))

(define-public crate-neon-serde-0.0.1 (c (n "neon-serde") (v "0.0.1") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "neon") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)))) (h "1xnq060hxvgvxjbwg6pbdcig8kh4bkgkflzqzfvgz8sx9gn1ffvh")))

(define-public crate-neon-serde-0.0.2 (c (n "neon-serde") (v "0.0.2") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "neon") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "0hqsiz564g2hm3vzviw043gh6y7zx2xhi49220wa11p6ggq41f6q")))

(define-public crate-neon-serde-0.0.3 (c (n "neon-serde") (v "0.0.3") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "neon") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "0idgajki899q99krj6vja8amnmmbapmv1i5r65ijpnk05km2bsq3")))

(define-public crate-neon-serde-0.1.0 (c (n "neon-serde") (v "0.1.0") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "neon") (r "0.2.*") (d #t) (k 0)) (d (n "neon-runtime") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "08pbl25fgyi9nd17x59pgz3pwzqkqh1f5h74gvmvgf1dmg5zan2a")))

(define-public crate-neon-serde-0.1.1 (c (n "neon-serde") (v "0.1.1") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "neon") (r "0.2.*") (d #t) (k 0)) (d (n "neon-runtime") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "1671vb0y4pkh7fw2jxiqd8rgjxhpwylbb3ajwj79rs0q6zyfwva2")))

(define-public crate-neon-serde-0.2.0 (c (n "neon-serde") (v "0.2.0") (d (list (d (n "cast") (r "0.2.*") (d #t) (k 0)) (d (n "error-chain") (r "0.12.*") (d #t) (k 0)) (d (n "neon") (r "^0.3.1") (d #t) (k 0)) (d (n "neon-runtime") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "0qxzsrjgnihmdns4q43hs621nhx0aisrryni25z1lvfphy7i17ns")))

(define-public crate-neon-serde-0.3.0 (c (n "neon-serde") (v "0.3.0") (d (list (d (n "error-chain") (r "0.12.*") (d #t) (k 0)) (d (n "neon") (r "^0.3.1") (d #t) (k 0)) (d (n "neon-runtime") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "0q0blc309rwsdz09k31lqahayf4bjkcir4ifzyqnvi6fqiy88iab")))

(define-public crate-neon-serde-0.4.0 (c (n "neon-serde") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "neon") (r "^0.4") (d #t) (k 0)) (d (n "neon-runtime") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "13pbjl1jcr7zplvq8vb79ms7ib9f428cjg7xh62lgpacf43l64sz")))

