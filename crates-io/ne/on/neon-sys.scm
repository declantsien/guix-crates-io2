(define-module (crates-io ne on neon-sys) #:use-module (crates-io))

(define-public crate-neon-sys-0.0.7 (c (n "neon-sys") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0nn5qmvh7qnqa4l4c989xqxazdd7zn8f1x9rwmlxgdszngrspimz")))

(define-public crate-neon-sys-0.1.0 (c (n "neon-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1djxfck9ss9i4mb5nknmns3qy5hwxm75gzh7nzdwhw1nvijlvpdw")))

(define-public crate-neon-sys-0.1.1 (c (n "neon-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1dhf8zyl4whd3nz08dg3wpaxxa0jg2fvy7cr4zz5wh3syllbfj92")))

(define-public crate-neon-sys-0.1.2 (c (n "neon-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0k8w6c12w90ihbnjdqm7x36qzq41lqpxyaq3dzgagcricypdjnc7")))

(define-public crate-neon-sys-0.1.3 (c (n "neon-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0x70hc4ryfg46wllsh0aq89l3026manf74ma9y3m23r943vkwflw")))

(define-public crate-neon-sys-0.1.4 (c (n "neon-sys") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0qanyvrx3fd8vls4g1pkjcppk9452fs6dxqpvyzywwgnv1gw7zcn")))

(define-public crate-neon-sys-0.1.5 (c (n "neon-sys") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0yjnpn0zv0y6bc4vb3a6pfggjyw4v30q6lh7hzcxlg5q83y6h4m2")))

(define-public crate-neon-sys-0.1.6 (c (n "neon-sys") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1fj3k6yjp75f7r6hwlzvfjkdbjas4fhdw778hp3303gvf404xaz6")))

(define-public crate-neon-sys-0.1.7 (c (n "neon-sys") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0plhqfp9bpxynzwqn2lx67i1avl1ri9s4vigrsarhs27nibpf64x")))

(define-public crate-neon-sys-0.1.8 (c (n "neon-sys") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "124i4j9w2yk399mcrbfmhpmhb9vmip8fy6dfnn8nfyh28icp02m5")))

(define-public crate-neon-sys-0.1.9 (c (n "neon-sys") (v "0.1.9") (d (list (d (n "cslice") (r "= 0.1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1h2x20z0g3win3fhwf6d4v2j42qjwn5mhpm4lqfk41rvzh1002kw")))

(define-public crate-neon-sys-0.1.10 (c (n "neon-sys") (v "0.1.10") (d (list (d (n "cslice") (r "= 0.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0d6sh70zk6b36hy1qds8wwljgk0d1avs0skwx1wpqqlksd3haw5d")))

(define-public crate-neon-sys-0.1.11 (c (n "neon-sys") (v "0.1.11") (d (list (d (n "cslice") (r "= 0.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "161md4ha4kk7b465n8kjab83ndpday6v6jvbk3r632zl0npj80z9")))

(define-public crate-neon-sys-0.3.2 (c (n "neon-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0y30m865w097sxrg7rxkbj5yfl9pa69lfprafc8pl2ymnc6zprn9") (l "neon")))

(define-public crate-neon-sys-0.3.3 (c (n "neon-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "16nq7rbwm6frk61pz7ijwghyk52ffp0l90g6fx83jnnaf4wczr4a") (l "neon")))

(define-public crate-neon-sys-0.4.0 (c (n "neon-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "05hfgakjl8w0c1imzwyrizbn5l29rqcdx5pmsgs4crwjdfxc39k9") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.4.1 (c (n "neon-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1pcv3ws5rkczr3k247dw7hqh4yaw1x1mm330jym1afamggcjvxig") (f (quote (("docs-only") ("default")))) (y #t) (l "neon")))

(define-public crate-neon-sys-0.4.2 (c (n "neon-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1v99j6jras2r96svasl6fa6623v242agxh3j0jhdfjn7f4hsb5hw") (f (quote (("docs-only") ("default")))) (y #t) (l "neon")))

(define-public crate-neon-sys-0.5.0 (c (n "neon-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0phkdi0vggvdrk5v4g2xdbargf8s0kf68p1z0z43wrxi0yxr05n4") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.5.1 (c (n "neon-sys") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "17b15kza8zjdaq810dclwnx2bsbglacppln6arjv9hqxpqknjavw") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.5.2 (c (n "neon-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1nc4a64l0ip4wl9g85895zv7bpk7sj7r5fy2n5l13529ic4001c9") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.5.3 (c (n "neon-sys") (v "0.5.3") (d (list (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0ki9mk2f50d65pr981wi2kg321v2z716150z9faf86gpv214daj0") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.6.0 (c (n "neon-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0dj7cav47jllyl6fmh1505mn6py88m4smvr4bqq9smx8qwv0zjyl") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.7.0 (c (n "neon-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "05f1krwvvf08x4frb3m02zp5yfjvvpc0y72jj9knhvbynzrv4m7h") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.7.1 (c (n "neon-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "035v0hxkx2z29cs4ix71nmz4227p4p4qr9b5vqygqwd9aa45aadn") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.8.0 (c (n "neon-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0pdcbh7rwvlyk0mk0fiqb1gl1w9wgm6djk2yn5bnqrp4wkfa15zd") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.8.1 (c (n "neon-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1q23kj8hyfh7bv3mgcdr4kls2ji626raz5wq1yl84szkzjka7z4l") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.8.2 (c (n "neon-sys") (v "0.8.2") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0s5rjja4ksmg7v9767wh9ql7djfbi4dqhyl7074cws5xi6pc1wvb") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.8.3 (c (n "neon-sys") (v "0.8.3") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "04vn71iks36r3h11ic8d8ihc790r9fwmwkjwp5xl64m2mjl12wzy") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.9.0 (c (n "neon-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1snhigs1xiah2sciprfm7rfkl37yf4rz2m2wd2jiicp888nvnmrd") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.9.1 (c (n "neon-sys") (v "0.9.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0c1qh0s30hmcncxxx3bvmwiq0yk4kxmf3p5k0h01ns6410pd6byw") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.10.0-alpha.3 (c (n "neon-sys") (v "0.10.0-alpha.3") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1kcdv8c3hpy7bg3jlr6219qhk28rdjvym1i5b94dz25n451nfpgr") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.10.0 (c (n "neon-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1dmfb6zpw519yqhljz4cbk6ajdhgn7hbvllgy7920ak3rxfcfxn3") (f (quote (("docs-only") ("default")))) (l "neon")))

(define-public crate-neon-sys-0.10.1 (c (n "neon-sys") (v "0.10.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1xkxxxmw3k8qifm4vsvhmk5m81a796ksmd2m8hc5gica60iwksx5") (f (quote (("docs-only") ("default")))) (l "neon")))

