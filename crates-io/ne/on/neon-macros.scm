(define-module (crates-io ne on neon-macros) #:use-module (crates-io))

(define-public crate-neon-macros-0.5.3 (c (n "neon-macros") (v "0.5.3") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlahrc0zxrnf5z6z6g36mzp1gs5rjyabv1dbc5ny9gz8zyww1kx") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.6.0 (c (n "neon-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13q9q3llr2rnzis135fc2b4h2d1y1rfnvsrhjxs3a5z73vf7vzxb") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.7.0 (c (n "neon-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09azwhnl13xycjh9fyhsiazadp7qkaqqhm90k5ff7nf15938l9a8") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.7.1 (c (n "neon-macros") (v "0.7.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06mf84j5llkc9vi5cnc0k9vm8xarw3myhhjkg1zfb81inrph92vw") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.8.0 (c (n "neon-macros") (v "0.8.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w8v9kxbc7z7whpyilywfn6wvrc0aq9vwx6mj1vrkhvmhmawvzlc") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.8.1 (c (n "neon-macros") (v "0.8.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19pbzpc961i5kc0l1z840jzsnc07s92yci7p8a07sh2gmpy51xww") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.8.2 (c (n "neon-macros") (v "0.8.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zvp3dwyjcaaqmdwvf050xvqi20nl9ac7z0n7i287vvgw2070lzc") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.8.3 (c (n "neon-macros") (v "0.8.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zvz0ndgcmbsp8lv9jd3i51rna9y596pvbcwwb7izx0n7g4mn1j7") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.9.0 (c (n "neon-macros") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0750nqrr1gzyxbj57rfhbmaqqq57kwbqcl2nishw77d7v1zzzbbj") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.9.1 (c (n "neon-macros") (v "0.9.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cj376ihfdgh4z85wq0w877n9plisfs5yz4zh5khpkmn3v4i4zwq") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.10.0 (c (n "neon-macros") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0j75n0izqjhblcqnsc8qc0zkpbfqyw7sv5smsdycmvb59vplc1q4") (f (quote (("napi"))))))

(define-public crate-neon-macros-0.10.1 (c (n "neon-macros") (v "0.10.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "1kwpm1dh73z22pyv6ailzbgj0c38lzib03hfqq9pkbslifn8wa5p") (f (quote (("napi"))))))

(define-public crate-neon-macros-1.0.0-alpha.1 (c (n "neon-macros") (v "1.0.0-alpha.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0pdvyx458pzby6r5h8wy93ay9nw860r1shiayz1zz1414vlhmblm")))

(define-public crate-neon-macros-1.0.0-alpha.2 (c (n "neon-macros") (v "1.0.0-alpha.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "1h43yyxvn1mcfvpgcl8109x82dkkh1ww9m7ccfif5ldsphpb6jz3")))

(define-public crate-neon-macros-1.0.0-alpha.3 (c (n "neon-macros") (v "1.0.0-alpha.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "1qp95h0b7h53zahlzixgcpqfck1kp218awwypxw7m99yvpn2k83v")))

(define-public crate-neon-macros-1.0.0-alpha.4 (c (n "neon-macros") (v "1.0.0-alpha.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "1p8x1g2qk452gpkd41wy2zadjpa2c9bpqyrxlrvhc56m0m26dkgs")))

(define-public crate-neon-macros-1.0.0 (c (n "neon-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "syn-mid") (r "^0.6.0") (d #t) (k 0)))) (h "12z3kqb325cn0xs73qlrjmfnmxqfm054hpypg9zf8imng7g3z0f6")))

