(define-module (crates-io ne on neoncore) #:use-module (crates-io))

(define-public crate-neoncore-1.0.0-rc1 (c (n "neoncore") (v "1.0.0-rc1") (h "17l0c3vm6fp581r28k04ni2fv0vrzakbx3fgqgy34b1kpwwsrmqr")))

(define-public crate-neoncore-1.0.0 (c (n "neoncore") (v "1.0.0") (h "0p7ajkm83dbz8k2dpqd3v986dba9xwy4w7jz5f6x4b7z9zj69amr")))

(define-public crate-neoncore-2.0.0 (c (n "neoncore") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0pbs41zq30byxcwms9gsxv1r4ncw7kzid6cb0vbxzcq3k0r3xr9n")))

(define-public crate-neoncore-3.0.0 (c (n "neoncore") (v "3.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "07lr90bm35kg57qbh4cmc4010nmgan8krdx3xxn8kzfbkfn1rkqv")))

(define-public crate-neoncore-4.0.0 (c (n "neoncore") (v "4.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1by90gj608pg6g0jz84ysir7acllrybd8z4anbraq2h7wakphszs")))

(define-public crate-neoncore-5.0.0 (c (n "neoncore") (v "5.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "16vkn0gxklsv3dm14rjhka4qwiqv8zy2xfmia0xkmcqaa92s712h")))

(define-public crate-neoncore-6.0.0+beta (c (n "neoncore") (v "6.0.0+beta") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1jncc4njx06hghnvpyzb0ynjmq0na0jl5956iyfzs7pdwl098zrw")))

(define-public crate-neoncore-7.0.0-beta1 (c (n "neoncore") (v "7.0.0-beta1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0l5ab6asg2r9s08b25zm62crziibph2irj9690r2ag53s1x77msn")))

(define-public crate-neoncore-7.0.0-beta2 (c (n "neoncore") (v "7.0.0-beta2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "11fzgxzjn9b7b48ysy329yfkz2bgf01n9x6p5zjn69b1b16nvzgw")))

(define-public crate-neoncore-7.0.0 (c (n "neoncore") (v "7.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "1km6sq5gr65wlpdbsg5z5mcsil80m4biprvjcshidp66rxqd2h2x") (f (quote (("std" "parking_lot" "thiserror") ("default" "std"))))))

