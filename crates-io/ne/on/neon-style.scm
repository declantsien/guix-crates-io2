(define-module (crates-io ne on neon-style) #:use-module (crates-io))

(define-public crate-neon-style-0.1.0 (c (n "neon-style") (v "0.1.0") (h "0n05q6bf0ban697vx3z4cp684rbgfvjy2yvd9gkc7xv8mk9s3r0x")))

(define-public crate-neon-style-0.1.1 (c (n "neon-style") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("hyphenation"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1gbxy2z962cnissfxbqi2wmbhgm336943yam22710hjhccn8033c") (r "1.64.0")))

