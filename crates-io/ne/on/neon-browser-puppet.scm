(define-module (crates-io ne on neon-browser-puppet) #:use-module (crates-io))

(define-public crate-neon-browser-puppet-0.1.0 (c (n "neon-browser-puppet") (v "0.1.0") (d (list (d (n "fantoccini") (r "^0.19.3") (f (quote ("native-tls"))) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (f (quote ("native-tls" "thirtyfour-macros" "component"))) (k 0)) (d (n "tokio") (r "^1.20.1") (d #t) (k 0)))) (h "1chj194msjqhhjg561r8rrqz6v02k46pv6hvxq8pqhwlzmvyjlpv")))

(define-public crate-neon-browser-puppet-0.1.1 (c (n "neon-browser-puppet") (v "0.1.1") (d (list (d (n "fantoccini") (r "^0.19.3") (f (quote ("native-tls"))) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (f (quote ("native-tls" "thirtyfour-macros" "component"))) (k 0)) (d (n "tokio") (r "^1.20.1") (d #t) (k 0)))) (h "0b6q2qmsq56gdcdhf9lspfljr061hrrvn61r1qn7lb032mfdd83s")))

