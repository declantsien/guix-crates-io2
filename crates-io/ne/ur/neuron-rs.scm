(define-module (crates-io ne ur neuron-rs) #:use-module (crates-io))

(define-public crate-neuron-rs-0.0.1 (c (n "neuron-rs") (v "0.0.1") (h "1936synf585bqx4svyps66l4bflrcvq8hjk6h0s01vcakmx7c10d")))

(define-public crate-neuron-rs-0.0.2 (c (n "neuron-rs") (v "0.0.2") (h "0la63wwq8ngig6qqyrhlnfc9nklki7yhfb0rgv5vqq8845vkydh8") (r "1.73")))

(define-public crate-neuron-rs-0.0.3 (c (n "neuron-rs") (v "0.0.3") (h "00wdki8914kplnm8q79s1kpq7d3h80bm0lnsbicbrysq64dad8ns") (r "1.73")))

