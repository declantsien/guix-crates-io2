(define-module (crates-io ne ur neurons) #:use-module (crates-io))

(define-public crate-neurons-0.0.1 (c (n "neurons") (v "0.0.1") (h "0p2bhhk3c88p6ssjh6dfac6gh9lw84ac2ln2x6893z4p4w43084i")))

(define-public crate-neurons-0.0.2 (c (n "neurons") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1avcyai1jcpfwn7gn38kfzvlvlww1fy94w1q4is12sgdz0djmd76")))

(define-public crate-neurons-0.0.3 (c (n "neurons") (v "0.0.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hifij6nw3d73vk71li28ywngaxbmk5czpjz8ip3620ay9cyfznn")))

(define-public crate-neurons-0.0.4 (c (n "neurons") (v "0.0.4") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zr92pw5hnj78kx4ym5qdfr1vwi4s5mpl3b1rb5w88g2j9jjmjpb")))

