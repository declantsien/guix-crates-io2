(define-module (crates-io ne ur neuros) #:use-module (crates-io))

(define-public crate-neuros-0.1.0 (c (n "neuros") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "linfa") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sefar") (r "^0.1.3") (d #t) (k 0)))) (h "00z4glcqykhwgczg20cy9cqah307lzq8rj24pwdp90p4b3cdcwq2")))

(define-public crate-neuros-0.1.1 (c (n "neuros") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "linfa") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sefar") (r "^0.1.3") (d #t) (k 0)))) (h "1yysgrmd8qyvhl0dpvdld4zzmx0r31vw5293w013gazrm7qqq54r") (y #t)))

(define-public crate-neuros-0.1.2 (c (n "neuros") (v "0.1.2") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "linfa") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sefar") (r "^0.1.3") (d #t) (k 0)))) (h "0pwmaiih6dbl106dm2p641md2zsdf258sh1xj7h4217087fipv94") (y #t)))

(define-public crate-neuros-0.1.3 (c (n "neuros") (v "0.1.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "linfa") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sefar") (r "^0.1.3") (d #t) (k 0)))) (h "16n9210s8pryn2c1b73jnhjfjs9bpfrwwjhg313y3hrlbfjdj1h2") (y #t)))

