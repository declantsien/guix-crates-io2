(define-module (crates-io ne ur neure) #:use-module (crates-io))

(define-public crate-neure-0.0.1 (c (n "neure") (v "0.0.1") (h "0rdmmvzn8q13jii40fhdifrfixjhzmqxsy7gjdhx6k9pgixkx3bg") (y #t)))

(define-public crate-neure-0.1.0 (c (n "neure") (v "0.1.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "099yjjs7ikwrnziikl24si2h1b8vbgkwqczirss69jrc64nfidkd") (y #t)))

(define-public crate-neure-0.1.1 (c (n "neure") (v "0.1.1") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "1yxrynybx1a5fslpsp434ffjlzmym2sn90f33m6kc8ar6c0kivin") (y #t)))

(define-public crate-neure-0.1.2 (c (n "neure") (v "0.1.2") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "06zhzk7hj1p9rpv16m4h7n95g642k58ndswb5lx7gjpxbh27zqhg") (y #t)))

(define-public crate-neure-0.2.0 (c (n "neure") (v "0.2.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "0hmazr04w22fr89abh5q3w41fpiiqg2sxd21r0dkr6njkswara9l") (y #t)))

(define-public crate-neure-0.2.1 (c (n "neure") (v "0.2.1") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "0132njkjjlhg2dpwrga0qvds8fsxscc615akvzggcb94y67irrk8") (y #t)))

(define-public crate-neure-0.2.2 (c (n "neure") (v "0.2.2") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "0xibr0djbmqqb9r5sdwp6iq06b0jfl55yqjcicwpk4c7yg96g6x7") (y #t)))

(define-public crate-neure-0.3.0 (c (n "neure") (v "0.3.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "0sbglfpnn8qas4bj0m6s0zjsn527pgrz1jbpafcvpf2n3bcmdnb0") (y #t)))

(define-public crate-neure-0.3.1 (c (n "neure") (v "0.3.1") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "1bdxdh1bvgx2kpbh8gw57xm66zwknrjb7vwcji1p0h3w51r5bqad")))

(define-public crate-neure-0.4.0 (c (n "neure") (v "0.4.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.2") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "151lh9b04fyrs5848ggx7ibrdz17ky3c07z6qa0jyq4ad0f6gpv3") (f (quote (("log" "tracing")))) (y #t)))

(define-public crate-neure-0.5.0 (c (n "neure") (v "0.5.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0vdl9j71dmdk44c3f57fwz0bb17zya3ziz70smkhla77g6nhz1k5") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.5.1 (c (n "neure") (v "0.5.1") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "12xyhvkzg38vkgg3ya3kn93dh63hz6kv4f4gisf8hpz6hfa28zm1") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.5.2 (c (n "neure") (v "0.5.2") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "15bzrkimvkbzigmarr9g7js9v2jqk5h1w8hdyspkgmh3icyvwi4x") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.5.3 (c (n "neure") (v "0.5.3") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1x57gwvskjjjmb2v2ca98vii89s2sc0hq1mghvcc516cbvm0mhh5") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.6.0 (c (n "neure") (v "0.6.0") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "11ay00kr2xpdnbml694dh4wwfkn8p4cc7v7af9rz6hjn6v0xj1ab") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.6.1 (c (n "neure") (v "0.6.1") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "084ikwnqzvlwryv3bmn2kwwm8l4q0zk24rp5fp2s5acn9m5ivfkh") (f (quote (("log" "tracing"))))))

(define-public crate-neure-0.6.2 (c (n "neure") (v "0.6.2") (d (list (d (n "charize") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0f9wzcqb4c2vsifx2d6f1r0cm7gk4ijd14kcvcb3lp9ax9n2sjd5") (f (quote (("log" "tracing"))))))

