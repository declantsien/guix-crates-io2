(define-module (crates-io ne ur neuromorphic-types) #:use-module (crates-io))

(define-public crate-neuromorphic-types-0.1.0 (c (n "neuromorphic-types") (v "0.1.0") (h "0wv2mni45wcr3jg75cdh3hmz4addls3p7g0f7n72abkpjayngb4d") (y #t)))

(define-public crate-neuromorphic-types-0.1.1 (c (n "neuromorphic-types") (v "0.1.1") (h "0npafzja27im3y415spsv5l7ikkrr0js0pxy0a1csl6jzwz6fy1m")))

(define-public crate-neuromorphic-types-0.2.0 (c (n "neuromorphic-types") (v "0.2.0") (h "1m7281hgrb5prnb9c5ia8yi54rasivd7im0d9gk45msb882pz30i")))

(define-public crate-neuromorphic-types-0.3.0 (c (n "neuromorphic-types") (v "0.3.0") (h "0w1z30rm7j2vwnpf573ci9fjviznk5dg4s0s6jzc4iq68qzf4h21")))

(define-public crate-neuromorphic-types-0.4.0 (c (n "neuromorphic-types") (v "0.4.0") (h "0km815hc5b0bgqmqjd2waik57rp440m1j6bd21c1fvcag01a7s9a")))

