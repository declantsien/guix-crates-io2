(define-module (crates-io ne ur neuralnetwork) #:use-module (crates-io))

(define-public crate-neuralnetwork-0.1.0 (c (n "neuralnetwork") (v "0.1.0") (h "056jn6kyw5mbhk9q50znrk2s7sjm6w8kpb2lg1q4wl9zj9rmcnjg")))

(define-public crate-neuralnetwork-0.1.1 (c (n "neuralnetwork") (v "0.1.1") (h "0491z3njj4p1di0krpvf6j5jvqfv2hwpsrr6arfvk2fwkmd757x0")))

