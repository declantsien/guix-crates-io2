(define-module (crates-io ne ur neural-network-rs) #:use-module (crates-io))

(define-public crate-neural-network-rs-0.1.0 (c (n "neural-network-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)))) (h "1w8lnkxg1mvlgvbjih8ir3zj0jrb8q6b5njddm7jqllb3293jkxm")))

(define-public crate-neural-network-rs-0.1.1 (c (n "neural-network-rs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)))) (h "1jwa842fjy9nplpp25rgrxg6c62vbxvsbkylrllng0fr02rm0dy3")))

(define-public crate-neural-network-rs-0.1.2 (c (n "neural-network-rs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)))) (h "1w1al4i926hr5ymivgyn1j04qjqgf0vmkdidxczdmz54p0qq8m1b")))

(define-public crate-neural-network-rs-0.1.3 (c (n "neural-network-rs") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)))) (h "0q6km5arci8gv9y76vazw9mmm9h4l8mq1ii1m0502852s6hhkj3d")))

(define-public crate-neural-network-rs-0.1.4 (c (n "neural-network-rs") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)))) (h "1wfyhn84l4h83yp52nqbdvcr3b96dvxarpafcv1hnsj2cix89r7c")))

