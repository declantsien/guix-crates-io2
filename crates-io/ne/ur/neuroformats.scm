(define-module (crates-io ne ur neuroformats) #:use-module (crates-io))

(define-public crate-neuroformats-0.1.0 (c (n "neuroformats") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "17yqm8raykya145mni9w74v9rbqcf0d6favsrp193990ssshj32g")))

(define-public crate-neuroformats-0.2.0 (c (n "neuroformats") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0vr9fp87rls9d92z29b5115dm42hx0k1jy61lc6jyq9h0nhw5cwb")))

(define-public crate-neuroformats-0.2.1 (c (n "neuroformats") (v "0.2.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1m9c10dclrbfmmvvxp9ajg8jlcp8ingr34p61cm13hb9khixc2jq")))

(define-public crate-neuroformats-0.2.2 (c (n "neuroformats") (v "0.2.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "11mrn3l97x9vslvs5agshrwjslqgszxi1d9vhpvwz0wn8jwffsrh")))

(define-public crate-neuroformats-0.2.3 (c (n "neuroformats") (v "0.2.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1yccs4rcpxyf8i5ynsrk3zksalbifpkjlacl50byqvgi6dg263mi")))

(define-public crate-neuroformats-0.2.4 (c (n "neuroformats") (v "0.2.4") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "byteordered") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0h8x2sfzhqi5p4qmm2q0kxij0q5h74c8pwd3ny4jpqdyi6wz0v6x")))

