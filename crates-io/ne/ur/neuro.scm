(define-module (crates-io ne ur neuro) #:use-module (crates-io))

(define-public crate-neuro-0.1.0 (c (n "neuro") (v "0.1.0") (d (list (d (n "arrayfire") (r "^3.6.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "hdf5") (r "^0.6.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0fknrpysxgm8mx7wvls9j4l51r88y4zj76nvabadwdwz4msjx0bq")))

