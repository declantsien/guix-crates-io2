(define-module (crates-io ne ur neural_network) #:use-module (crates-io))

(define-public crate-neural_network-0.0.0 (c (n "neural_network") (v "0.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p6giz1bail839gzfpgd7ic6pvim6rdrzqmjjm994392yj4h35rq")))

(define-public crate-neural_network-0.1.0 (c (n "neural_network") (v "0.1.0") (d (list (d (n "fast_io") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "027r1w7f4c9sx4vbg86yz82iv17sf091h5lxf5cj9smpl30fwaw5")))

(define-public crate-neural_network-0.1.1 (c (n "neural_network") (v "0.1.1") (d (list (d (n "fast_io") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0f71k6qc8rq5y8nlj0lh2pkal386k5rjy5d3k38b7hy30zspj6xg")))

(define-public crate-neural_network-0.1.2 (c (n "neural_network") (v "0.1.2") (d (list (d (n "fast_io") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0yg72diwlgsqzf6rjnf4sdlrwcin874c6pad45cwhz15prpwx61f")))

(define-public crate-neural_network-0.1.3 (c (n "neural_network") (v "0.1.3") (d (list (d (n "fast_io") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1y7f21mzbipfkp3qjyhdqsijfz3y1zy1lsfwkpjkw36wm6cvlpfd")))

