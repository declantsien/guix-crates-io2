(define-module (crates-io ne ur neuralneat) #:use-module (crates-io))

(define-public crate-neuralneat-0.1.0 (c (n "neuralneat") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "183k2dwyb62zfidgyvg9njzdpyy2c1syf24fd23lm9dlicsqpv9w")))

(define-public crate-neuralneat-0.2.0 (c (n "neuralneat") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "15izn4bi59nm44pr9skijziy4k56pdyw71phqdilmhggcm7swqkx")))

(define-public crate-neuralneat-0.3.0 (c (n "neuralneat") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "1qii0xj4n0zi3322i7klh69qsnjxsv5myj0302pcwilfg9lffrjy")))

