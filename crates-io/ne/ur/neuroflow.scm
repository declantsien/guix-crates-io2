(define-module (crates-io ne ur neuroflow) #:use-module (crates-io))

(define-public crate-neuroflow-0.1.0 (c (n "neuroflow") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "04z150g0345alwjswyqw7rys4ach2pnscq0kmvrlq60nyi5i0mra")))

(define-public crate-neuroflow-0.1.1 (c (n "neuroflow") (v "0.1.1") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0wr6isbqjrr8dvrqjd8bv4vcdyghan80rl0r1nzvnm4xq4n7hkb8")))

(define-public crate-neuroflow-0.1.2 (c (n "neuroflow") (v "0.1.2") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1w06byxk4fi7amcaxgk9rshfq91280hpr1c9iphp9xswba8ficzx")))

(define-public crate-neuroflow-0.1.3 (c (n "neuroflow") (v "0.1.3") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "07ajbcx3zw1mm5s4yyjzfvsgi6bskk3sfvlf8wg4m5f6fs3x3hac")))

(define-public crate-neuroflow-0.2.0 (c (n "neuroflow") (v "0.2.0") (d (list (d (n "bincode") (r "~1.3") (d #t) (k 0)) (d (n "csv") (r "~1.2") (d #t) (k 0)) (d (n "rand") (r "~0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0m1pad54rp76g4jl1amg4ij4hh7cs2bzlh0q349ahpz8z8ip9s1s")))

