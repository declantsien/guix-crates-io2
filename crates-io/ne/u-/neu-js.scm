(define-module (crates-io ne u- neu-js) #:use-module (crates-io))

(define-public crate-neu-js-0.1.0 (c (n "neu-js") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.164.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv2rnfzph601bawjh9mpsxg5586v0nrsacblgy1qv1q3viccirj")))

