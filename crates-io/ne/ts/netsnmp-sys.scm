(define-module (crates-io ne ts netsnmp-sys) #:use-module (crates-io))

(define-public crate-netsnmp-sys-0.1.0 (c (n "netsnmp-sys") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0ha2a9nysjsiw6qixv5almhgvx91l1xpksc97s3bsgfckfs8f8cr")))

(define-public crate-netsnmp-sys-0.1.1 (c (n "netsnmp-sys") (v "0.1.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1hr7ghijmihlaw10crfmpqmcki6aliglljdsj3izirq79yjm66v3")))

