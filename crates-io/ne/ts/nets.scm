(define-module (crates-io ne ts nets) #:use-module (crates-io))

(define-public crate-nets-0.1.1 (c (n "nets") (v "0.1.1") (h "14m0p6sxdir5q5400cw2ks8l5qrfq6iil7k9bs8yhkh2vaan78y5")))

(define-public crate-nets-0.1.2 (c (n "nets") (v "0.1.2") (h "0jw0kj1737w4c82avr24s75h2gvw8mk2q9v7mygg27ckqmgkqs55")))

(define-public crate-nets-0.1.3 (c (n "nets") (v "0.1.3") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "http") (r "0.*") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 0)) (d (n "pcap") (r "0.9.*") (d #t) (k 0)) (d (n "pktparse") (r "0.4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tls-parser") (r "^0.7") (d #t) (k 0)))) (h "012w6jcbvbb6xv61dyxb7dwkl7qkrdwccha23w5ypavdfgrzhiy6")))

(define-public crate-nets-0.1.4 (c (n "nets") (v "0.1.4") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "http") (r "0.*") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 1)) (d (n "libpcap") (r "0.1.*") (d #t) (k 0)) (d (n "pktparse") (r "0.4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tls-parser") (r "^0.7") (d #t) (k 0)))) (h "16kid9s57hl5i71l9isb8kmy28v8frks28h74g8bp1d1m8dl8h23")))

(define-public crate-nets-0.1.5 (c (n "nets") (v "0.1.5") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "http") (r "0.*") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 1)) (d (n "libpcap") (r "0.1.*") (d #t) (k 0)) (d (n "pktparse") (r "0.4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tls-parser") (r "^0.7") (d #t) (k 0)))) (h "1ix8n94pgqs892jnwxwaiwmyn6spcp384cf6z9qc39l66l1qg3qb")))

