(define-module (crates-io ne ts netscan-service) #:use-module (crates-io))

(define-public crate-netscan-service-0.1.0 (c (n "netscan-service") (v "0.1.0") (d (list (d (n "dns-lookup") (r "^1.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1svaczxwi6i2rsjprgnddsihzkgz5sqnhagdla6qzk7mq0a07v1q") (y #t)))

(define-public crate-netscan-service-0.11.0 (c (n "netscan-service") (v "0.11.0") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1cw8x5qp6h9msqx8zcglg1kdnn8np0zs8yn7wmamsdxjny9dm77z")))

(define-public crate-netscan-service-0.12.0 (c (n "netscan-service") (v "0.12.0") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1vb7377fqs0wm281hx3b9jsxw91x9ww5pw2lqswfd5qxj19q1cv0")))

(define-public crate-netscan-service-0.13.0 (c (n "netscan-service") (v "0.13.0") (d (list (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1chfgkm8q3h9gbhf5g2xh9fs36kz5m863fvi5v0lwc16xbisf4kg")))

(define-public crate-netscan-service-0.14.0 (c (n "netscan-service") (v "0.14.0") (d (list (d (n "dns-lookup") (r "^2.0.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "027m8sj575nw2wha2gqgf0k7h27akfj27pkyf7kfldx97bv5ap54")))

(define-public crate-netscan-service-0.15.0 (c (n "netscan-service") (v "0.15.0") (d (list (d (n "dns-lookup") (r "^2.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.1") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.2") (d #t) (k 0)))) (h "05z6v0bqrcmbmhi1f1hskgkismwvs33955c6dviya1pyq9qjpk82")))

(define-public crate-netscan-service-0.15.2 (c (n "netscan-service") (v "0.15.2") (d (list (d (n "dns-lookup") (r "^2.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.5") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.3") (d #t) (k 0)))) (h "1p5i8fbhqk71g5956g4as821wa4pwlczrysif3q0ddb7sgrk7z2x")))

(define-public crate-netscan-service-0.15.4 (c (n "netscan-service") (v "0.15.4") (d (list (d (n "dns-lookup") (r "^2.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.5") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.3") (d #t) (k 0)))) (h "17pc5y4nwwx70ihcnk9vk0kzayq18k86cd8m29zlhs0w9k4fqw2g")))

(define-public crate-netscan-service-0.16.0 (c (n "netscan-service") (v "0.16.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0gcj9c1r0x7d597ggfhcpmjrranzqs39zy53n2mfw9rm71v813xq")))

(define-public crate-netscan-service-0.17.0 (c (n "netscan-service") (v "0.17.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "15hpckcvz9jbn3anr8msdnjl0rp8dqf82dd3jqkxa8y8v9m976y4")))

(define-public crate-netscan-service-0.18.0 (c (n "netscan-service") (v "0.18.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0w7d60qrjrlnn2pdwlsx8b3sc26h0dfkzhjyb72aarbm1989m52c")))

(define-public crate-netscan-service-0.19.0 (c (n "netscan-service") (v "0.19.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "1wllv5jd87mbj1d1v80q76faqvkwngw83mhk7igwzd8zi72qivjm")))

(define-public crate-netscan-service-0.20.0 (c (n "netscan-service") (v "0.20.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0zn89jkndnaf3zgl2m7fhcb3dj0g4gq3dhl8ila41v6qcasifh41")))

(define-public crate-netscan-service-0.21.0 (c (n "netscan-service") (v "0.21.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0lpzll7i47ji2z76fjgwjm1v059dqbg08am91lp9hc0ga73yl167")))

(define-public crate-netscan-service-0.22.0 (c (n "netscan-service") (v "0.22.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "11rw7qvsp8636skz02g35h7ksmzv7pb59dx77677nc38lssjh8ya")))

(define-public crate-netscan-service-0.23.0 (c (n "netscan-service") (v "0.23.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0nwrr4i4avxwxq7vslm17cmp6zjr78v9xcrkgfw4bwq927krw83d")))

(define-public crate-netscan-service-0.24.0 (c (n "netscan-service") (v "0.24.0") (d (list (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "05v8d09zwwv264ij2mwzgmxq38r9bmqbzrslmhyznxcjs4n1h4d2")))

(define-public crate-netscan-service-0.25.0 (c (n "netscan-service") (v "0.25.0") (d (list (d (n "async-io") (r "^1.13") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0csz6d3sri1wpmzcm0nhxw7g5l6mgwmspssqy23dwixhnmrzi9j4")))

(define-public crate-netscan-service-0.26.0 (c (n "netscan-service") (v "0.26.0") (d (list (d (n "async-io") (r "^1.13") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)))) (h "0f9q19g8x8zhy0qxairxpad2qyb7gxlh6sjm4nndh5d20493hkiz")))

