(define-module (crates-io ne ts netscan-pcap) #:use-module (crates-io))

(define-public crate-netscan-pcap-0.22.0 (c (n "netscan-pcap") (v "0.22.0") (d (list (d (n "xenet") (r "^0.2.0") (d #t) (k 0)))) (h "1zm38zvnxrprpxq06a4x2w3szcvsmzbpf3q080870v03520m8dxm")))

(define-public crate-netscan-pcap-0.23.0 (c (n "netscan-pcap") (v "0.23.0") (d (list (d (n "xenet") (r "^0.3.0") (d #t) (k 0)))) (h "0br1jmwql8g05vwnpm8x5ak9ng7c05g83ib8768i16c17q0s3zwv")))

(define-public crate-netscan-pcap-0.24.0 (c (n "netscan-pcap") (v "0.24.0") (d (list (d (n "xenet") (r "^0.3.0") (d #t) (k 0)))) (h "0h6rm3zmplaqb979j5gizf5s593v81dpnz0rrr5w74rb221bvvig")))

(define-public crate-netscan-pcap-0.25.0 (c (n "netscan-pcap") (v "0.25.0") (d (list (d (n "xenet") (r "^0.4") (d #t) (k 0)))) (h "1b1ysq6w4kc2k26x91jf5rbw2329z0zsbahw2pnqhal0vh6v3ihf")))

(define-public crate-netscan-pcap-0.26.0 (c (n "netscan-pcap") (v "0.26.0") (d (list (d (n "xenet") (r "^0.4") (d #t) (k 0)))) (h "1i2p3q8d5xwi2n1sn4lyhsmx6ak9qgc1wglvnri9z1fznvhp6kn6")))

