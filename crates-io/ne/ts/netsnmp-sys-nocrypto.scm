(define-module (crates-io ne ts netsnmp-sys-nocrypto) #:use-module (crates-io))

(define-public crate-netsnmp-sys-nocrypto-0.1.2 (c (n "netsnmp-sys-nocrypto") (v "0.1.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0zrr6syadgnd9h61z6id94n5vr7znw8lvzg64rj363iv3pyzmqnh") (l "netsnmp")))

(define-public crate-netsnmp-sys-nocrypto-0.1.3 (c (n "netsnmp-sys-nocrypto") (v "0.1.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0wm1srx6kl9fyfwxdmplwdp5v1lga7iwncpha68xch4yxdjv9h31") (l "netsnmp")))

