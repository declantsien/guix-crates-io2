(define-module (crates-io ne ts netsim-embed-core) #:use-module (crates-io))

(define-public crate-netsim-embed-core-0.1.0 (c (n "netsim-embed-core") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0jm8bn8bf75c3k1dhqf09z5xr6kaiswjp0ni4k9gr6yv074gcsc8")))

(define-public crate-netsim-embed-core-0.1.1 (c (n "netsim-embed-core") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1kpw5c0cfpiz3maxb5i04yp55b0h5dwxkbm1rkw9cmfhn36fnjb0")))

(define-public crate-netsim-embed-core-0.2.0 (c (n "netsim-embed-core") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1h4kq9nxcm7a38p860bdw4rphcpkpcc80sc82x5fj7ik72yix00s")))

(define-public crate-netsim-embed-core-0.2.1 (c (n "netsim-embed-core") (v "0.2.1") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1qfrb2kcl05kyjm7rh7lfxqnwyj4s7pwa800qplbb8fyprgznpkq")))

(define-public crate-netsim-embed-core-0.2.2 (c (n "netsim-embed-core") (v "0.2.2") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1ifd6av0gcg4rmw5zy7d9ymnwg1fgcd42y4144llzz80qcfh8fnl")))

(define-public crate-netsim-embed-core-0.4.0 (c (n "netsim-embed-core") (v "0.4.0") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1yp8p753yha6ib5630qk2x98rjgclj8rsm1kcz7w9j1k2s4yp4m3")))

(define-public crate-netsim-embed-core-0.4.1 (c (n "netsim-embed-core") (v "0.4.1") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0zaj5g4al2djwkdr10jvx1mydd9j74p8zs8576mcb03c250j5fdj")))

(define-public crate-netsim-embed-core-0.4.2 (c (n "netsim-embed-core") (v "0.4.2") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "05kc256ds1rjy7jxxdsfmrcc1ybga046ps6nl0qkcv2yirh9j3hw")))

(define-public crate-netsim-embed-core-0.4.3 (c (n "netsim-embed-core") (v "0.4.3") (d (list (d (n "async-global-executor") (r "^2.3.1") (d #t) (k 0)) (d (n "async-io") (r "^1.13.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "020h21z659jnh9f5hqlsscgzqlf04g3xb8h0wail4a0a67rp2g12")))

