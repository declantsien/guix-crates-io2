(define-module (crates-io ne ts netscan-os) #:use-module (crates-io))

(define-public crate-netscan-os-0.1.0 (c (n "netscan-os") (v "0.1.0") (d (list (d (n "default-net") (r "^0.5.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.28") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.28") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16zqsnl76gcw6p99hm9s47kfh9jj4qkii2f8crwgm6qbik6pymw0") (y #t)))

(define-public crate-netscan-os-0.11.0 (c (n "netscan-os") (v "0.11.0") (d (list (d (n "default-net") (r "^0.11.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1znzwqz3rn4ymw6knw5wpiyyc9zj5bzq1fp9a44mwjk57pmflqh7")))

(define-public crate-netscan-os-0.12.0 (c (n "netscan-os") (v "0.12.0") (d (list (d (n "default-net") (r "^0.11.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1g2p4l4m87p67l50ahi20gy773rznp3x30km1j0r5yj6vkm3p29q")))

(define-public crate-netscan-os-0.13.0 (c (n "netscan-os") (v "0.13.0") (d (list (d (n "default-net") (r "^0.11.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0zkxl9bjivj6rpdyn7kqwb9qyf4j3x8a67cfwjjyzcpzl9q3bfi8")))

(define-public crate-netscan-os-0.14.0 (c (n "netscan-os") (v "0.14.0") (d (list (d (n "default-net") (r "^0.15.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "05vm9p9vsqhq4gi0k5n3x1l69bqfg1fn711d38ra3sp3rkqhlk11")))

(define-public crate-netscan-os-0.15.0 (c (n "netscan-os") (v "0.15.0") (d (list (d (n "default-net") (r "^0.15.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "01vcll5j1l7aldiy7l9jw6qx467cpqcgsagb3rjbqkib7mzgmk7q")))

(define-public crate-netscan-os-0.15.2 (c (n "netscan-os") (v "0.15.2") (d (list (d (n "default-net") (r "^0.16.1") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1f733g6663mqmyy5wqhzgksr88jzamjk831m4spa6h9vz2d6x3xw")))

(define-public crate-netscan-os-0.15.4 (c (n "netscan-os") (v "0.15.4") (d (list (d (n "default-net") (r "^0.16") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1h892x03vjl1r2xznwaip96fgklq8rrn1kdlslcs2qpa8lycax3i")))

(define-public crate-netscan-os-0.16.0 (c (n "netscan-os") (v "0.16.0") (d (list (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "np-listener") (r "^0.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "16w9mb9rkhmf49v9kvb12ijs559vf1n8bkidh1sli65hyjcnjayb")))

(define-public crate-netscan-os-0.17.0 (c (n "netscan-os") (v "0.17.0") (d (list (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "np-listener") (r "^0.6") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "01q294bvcl2vr7v890vrshg5na8ibsvnhpyzhnirdmykjgimwwyl")))

(define-public crate-netscan-os-0.18.0 (c (n "netscan-os") (v "0.18.0") (d (list (d (n "cross-socket") (r "^0.5") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0356x4aci8dp5dcc91m79jdgv8gwgvqz37ld6wmmdda3ircrlx1h")))

(define-public crate-netscan-os-0.19.0 (c (n "netscan-os") (v "0.19.0") (d (list (d (n "cross-socket") (r "^0.5") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "072l5n6jfmig6cyaldyhmx63pg8v4m1h692kx28k0zglli9ha7dc")))

(define-public crate-netscan-os-0.20.0 (c (n "netscan-os") (v "0.20.0") (d (list (d (n "cross-socket") (r "^0.5") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1liks0yw3pjdl307nwk5fc7xxcdji81s7mw6ib0zp8m1fvgbf9d9")))

(define-public crate-netscan-os-0.21.0 (c (n "netscan-os") (v "0.21.0") (d (list (d (n "cross-socket") (r "^0.8") (d #t) (k 0)) (d (n "default-net") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1snx1m1qipfsx3n9n2ldflz7y9ch4dns21hbyw2dsqmvh3gn5wf6")))

(define-public crate-netscan-os-0.22.0 (c (n "netscan-os") (v "0.22.0") (d (list (d (n "default-net") (r "^0.21") (d #t) (k 0)) (d (n "netscan-pcap") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "xenet") (r "^0.2.0") (d #t) (k 0)))) (h "0xgrfh61mlka1gislm4fys39imyjfhflxrppmw0lg8x6civgj9w8")))

(define-public crate-netscan-os-0.23.0 (c (n "netscan-os") (v "0.23.0") (d (list (d (n "default-net") (r "^0.21") (d #t) (k 0)) (d (n "netscan-pcap") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "xenet") (r "^0.3.0") (d #t) (k 0)))) (h "151bgyjfw06s6p6zwlq35zixf3g6yd5cngl2ygv5fcl0rg4iar1h")))

(define-public crate-netscan-os-0.24.0 (c (n "netscan-os") (v "0.24.0") (d (list (d (n "default-net") (r "^0.21") (d #t) (k 0)) (d (n "netscan-pcap") (r "^0.24.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "xenet") (r "^0.3.0") (d #t) (k 0)))) (h "1c2b1hqgh8j3ca3zj3y76ixn0k9wzgcr5cbxahq996glhrpfdy6x")))

(define-public crate-netscan-os-0.25.0 (c (n "netscan-os") (v "0.25.0") (d (list (d (n "default-net") (r "^0.21") (d #t) (k 0)) (d (n "netscan-pcap") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "xenet") (r "^0.4") (d #t) (k 0)))) (h "01f554sknifrlrnsxp7zlafb8fmnz22hly2p5v4ymjjpqx4lpxqi")))

(define-public crate-netscan-os-0.26.0 (c (n "netscan-os") (v "0.26.0") (d (list (d (n "default-net") (r "^0.21") (d #t) (k 0)) (d (n "netscan-pcap") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "xenet") (r "^0.4") (d #t) (k 0)))) (h "1ldqdd5k30s3wqhlsi8xim8piwrhv2rfkpbiyib5sg5ccpdbniqg")))

