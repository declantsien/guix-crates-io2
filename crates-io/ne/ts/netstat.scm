(define-module (crates-io ne ts netstat) #:use-module (crates-io))

(define-public crate-netstat-0.6.0 (c (n "netstat") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qjfkdczlcszi7dh813z9ixkc73qdd998nmbfpza8yjadgqmdz38") (y #t)))

(define-public crate-netstat-0.6.1 (c (n "netstat") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1050s86iggmc3scpp2i86iqpmfishaaw339vn0pal0wpjv5i07q0")))

(define-public crate-netstat-0.6.2 (c (n "netstat") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "easybench") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jw92yrh92z7z34vlr1g5zx51ri1fc621bmvxk6gvic7vhy6lzhs")))

(define-public crate-netstat-0.7.0 (c (n "netstat") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "easybench") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wqvh0i71dnqfnzb2ld7lcfyfjvjskf4x0wcgslrnaa34ip1zds8")))

