(define-module (crates-io ne ts netscape-to-universal) #:use-module (crates-io))

(define-public crate-netscape-to-universal-1.1.0 (c (n "netscape-to-universal") (v "1.1.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)))) (h "1clkwxm6v6x8yjzv6qaxk4jnrfyvzy3m9ixm002bfqp9xii1gn5q")))

(define-public crate-netscape-to-universal-1.2.0 (c (n "netscape-to-universal") (v "1.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)))) (h "0r1975m3lqg16wkq9gm9bclj7n1d048ksz4cvkm2frxmsp4hn0kf")))

(define-public crate-netscape-to-universal-1.3.0 (c (n "netscape-to-universal") (v "1.3.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)))) (h "0h4mcw5pj3pvmacx96358nl8ccbbx6fldhyc0kida5slg6fcgvhl")))

(define-public crate-netscape-to-universal-1.4.0 (c (n "netscape-to-universal") (v "1.4.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)))) (h "0ib8hwbn3hfnbwib4xnqr778cza6glipnc1b7hmg4fz9p7v44v7c")))

