(define-module (crates-io ne ts netsim-embed-macros) #:use-module (crates-io))

(define-public crate-netsim-embed-macros-0.1.0 (c (n "netsim-embed-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hskd3yd23wxz6c0i466aycpa1cy7nq5fg1p5kax04dj003rhjvd")))

(define-public crate-netsim-embed-macros-0.2.0 (c (n "netsim-embed-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "06ffdbqmb3a3j4mv2fg9vx3dm3lawwxi5bcshi6na48106fri2h5")))

