(define-module (crates-io ne ts netstack) #:use-module (crates-io))

(define-public crate-netstack-0.1.0 (c (n "netstack") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "178p4hfpwz3mz9yngv68flmn2mfb54rbf0222xdz1zwx6x9j2s1y")))

(define-public crate-netstack-0.1.1 (c (n "netstack") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1spymfdvh3al1bvy33w06gq42zs8m4zhpnm9j382x06y8dcisgdx")))

(define-public crate-netstack-0.2.0 (c (n "netstack") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0fmnqgg6ryylqd0cmkxmbwss5dy6lywyh2f6vc84lrmzds5hwnx8")))

(define-public crate-netstack-0.3.0 (c (n "netstack") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "11cyrl2maqp1cp5fia3af87ci3nj4yn7l4ph79vdcfhfg38zq5ca")))

