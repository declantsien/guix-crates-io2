(define-module (crates-io ne ts netserver) #:use-module (crates-io))

(define-public crate-netserver-0.1.0 (c (n "netserver") (v "0.1.0") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "1z46jvl338d5dp8lanswg4jrzr3cx1ya1fli6lb227rvpmshdl0v")))

(define-public crate-netserver-0.2.0 (c (n "netserver") (v "0.2.0") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "1d77a6dbx8hq3f65zp7x7843k03ryqhzjp60g8ilny6bc6106r2j")))

(define-public crate-netserver-0.2.1 (c (n "netserver") (v "0.2.1") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "1i3al6s0b76m5zcl3ajmg7dcpgc5frkdhs62f6i8zqzb49gnjkg6")))

(define-public crate-netserver-0.2.2 (c (n "netserver") (v "0.2.2") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "1hxxxqkm5cljxaxmvkpjapr5icawf95klz6ki8g11vprklgygcvd")))

(define-public crate-netserver-0.2.3 (c (n "netserver") (v "0.2.3") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "14fsbkz41ykv5jn7k0kr3gvwcl8iypiddb1drjxr1927pg5p9jxl")))

(define-public crate-netserver-0.2.4 (c (n "netserver") (v "0.2.4") (d (list (d (n "my_threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "0g87mkv3w8xqas1im9z1yfpz23fc4wfbq57kflg178spnawfyh2c")))

(define-public crate-netserver-0.2.5 (c (n "netserver") (v "0.2.5") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "0bkkd82wpvgvc1dyqswnznxl04lc9wa48946dmw5qmsdy8638wpd")))

(define-public crate-netserver-0.3.0 (c (n "netserver") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "0mh0jy9hahx9w9z3ykqm7dfr0xacm0mngrvj91rkljca5z3ylhpv")))

(define-public crate-netserver-0.3.1 (c (n "netserver") (v "0.3.1") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "0yzpg1q8pdlcnfgmigspmf6vl3nqzbgh21rxa5zwi3czcyg6m480")))

(define-public crate-netserver-0.3.2 (c (n "netserver") (v "0.3.2") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "0k884n0cdvgzfgdiz2j2wlsrzfz6ggpbn2777vbjzx92ack79gnw")))

(define-public crate-netserver-0.3.3 (c (n "netserver") (v "0.3.3") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "0iqixyhc8qhl9iadxxsyqqnidsidm3j3nir13bqbxvj8kva9irzp")))

(define-public crate-netserver-0.3.4 (c (n "netserver") (v "0.3.4") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "1sbq4g4wswj4nqnrygf6p9q95bhajl742rr6g1ln7r8wfiybamjg")))

(define-public crate-netserver-0.3.5 (c (n "netserver") (v "0.3.5") (d (list (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "my_threadpool") (r "^1.0.1") (d #t) (k 0)))) (h "1gwqwvbla17xhdfv8z8fwif4avm21wmndpffx4lkalg708q4nsig")))

