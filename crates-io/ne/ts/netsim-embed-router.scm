(define-module (crates-io ne ts netsim-embed-router) #:use-module (crates-io))

(define-public crate-netsim-embed-router-0.1.0 (c (n "netsim-embed-router") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.26.0") (d #t) (k 0)))) (h "04qz9gd9idx4wf15k3p26g9d50yy00p52qqc9b80vkc65kvvqm5m")))

(define-public crate-netsim-embed-router-0.1.1 (c (n "netsim-embed-router") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.1.1") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)))) (h "0lg1q76li2q6f4h1dcfv1m580wxl0hilbmgh7rnbwag289ag63jp")))

(define-public crate-netsim-embed-router-0.2.0 (c (n "netsim-embed-router") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)))) (h "0wblmq1lmdx7n90xl36iqdn7dx2xgag6xharqb13cvysb901xg1v")))

(define-public crate-netsim-embed-router-0.2.1 (c (n "netsim-embed-router") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)))) (h "1m3ydh7d9x2y5r10564rk9fdwa2mpyxnn1x5v19m3j58cqkzfnyi")))

(define-public crate-netsim-embed-router-0.4.0 (c (n "netsim-embed-router") (v "0.4.0") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "07q08r8qc8c1f2z1xa9c44cvdka5vivn5whdq4r3g7kqlllskgbs")))

(define-public crate-netsim-embed-router-0.4.1 (c (n "netsim-embed-router") (v "0.4.1") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "1946ispfahaxdkd4b009831k91wlzyy9cmpb6585w12lxhwmlfzv")))

(define-public crate-netsim-embed-router-0.4.2 (c (n "netsim-embed-router") (v "0.4.2") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "0fr1cg21m57d1z8qyy9qiwjcb2phz74xja4ldwzsmiksw7njhmip")))

(define-public crate-netsim-embed-router-0.4.3 (c (n "netsim-embed-router") (v "0.4.3") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "1kp09h4vrhb0kcll4v04k1c7vgycl55rnc5sw3qlhxqpmvjhc50h")))

(define-public crate-netsim-embed-router-0.4.4 (c (n "netsim-embed-router") (v "0.4.4") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "02zg3xlyiacg83rrji58shicdnmydwdy4zi15bbj6w27ip40ica3")))

(define-public crate-netsim-embed-router-0.4.5 (c (n "netsim-embed-router") (v "0.4.5") (d (list (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.0") (d #t) (k 0)))) (h "1gryzifrgb710yp9skvpqs7llz9dryj2x4hydk75z7s58x01p65p")))

(define-public crate-netsim-embed-router-0.4.6 (c (n "netsim-embed-router") (v "0.4.6") (d (list (d (n "async-global-executor") (r "^2.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.3") (d #t) (k 0)))) (h "1phi2gbnjm986sgi5lccmrkwmhxfjpghpg5wdp5d9sff8dzsr56s")))

(define-public crate-netsim-embed-router-0.4.7 (c (n "netsim-embed-router") (v "0.4.7") (d (list (d (n "async-global-executor") (r "^2.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "libpacket") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "netsim-embed-core") (r "^0.4.3") (d #t) (k 0)))) (h "1axvg357gvdvycq70az2z04n8x4c5vqj4c90h5byn9rjdzh1pnqy")))

