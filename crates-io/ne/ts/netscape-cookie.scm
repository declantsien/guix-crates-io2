(define-module (crates-io ne ts netscape-cookie) #:use-module (crates-io))

(define-public crate-netscape-cookie-0.1.0 (c (n "netscape-cookie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "cookie") (r "^0.14") (o #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "time") (r "^0.2") (o #t) (k 0)))) (h "0lbm0lz33l92mbcsk4l0k1zfg0l16ws0iis7zy5wy8bsawykwc24") (f (quote (("feature-cookie" "cookie" "time"))))))

(define-public crate-netscape-cookie-0.1.1 (c (n "netscape-cookie") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "cookie") (r "^0.14") (o #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "time") (r "^0.2") (o #t) (k 0)))) (h "1jfcydfn2vaqznjfx8qv8z58agx8v3kjnvrw6zhk8h1qmql0wdqp") (f (quote (("feature-cookie" "cookie" "time"))))))

