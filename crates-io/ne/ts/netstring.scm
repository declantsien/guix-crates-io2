(define-module (crates-io ne ts netstring) #:use-module (crates-io))

(define-public crate-netstring-0.0.1 (c (n "netstring") (v "0.0.1") (h "0pfwz0csq3zgcxmbck1f0v0glikf7qq1j0fzcfvyxj2hhxlmrz3k")))

(define-public crate-netstring-0.1.0 (c (n "netstring") (v "0.1.0") (h "1b04x65pjqi1ck17jq16c3jlxnz3aiz5v6yf6cjmd74gznmwipsm")))

(define-public crate-netstring-0.1.1 (c (n "netstring") (v "0.1.1") (h "03488m7f0am9yajxy7ppdwin5n3szkd9jcc2yp0jvnr1wybl8f4w")))

(define-public crate-netstring-0.2.0 (c (n "netstring") (v "0.2.0") (h "0s0vgwaxv376l1486yy6lmr42qk3dlf8m66mjvg3ibvf6426fpi0")))

(define-public crate-netstring-0.2.1 (c (n "netstring") (v "0.2.1") (h "1nw1hif21w1mjr0hb75yr5bfxk43ra3m33c5vhmdkkski3blxbfa")))

(define-public crate-netstring-0.2.2 (c (n "netstring") (v "0.2.2") (h "1n4nqypc0lq922sbd1p419cfirgyhbw945xbhl2y2lk1b84wfxmi")))

(define-public crate-netstring-0.3.1 (c (n "netstring") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0grabc22zrjh04kjrpcq0ks83z46kv6d3fl1dpighirpa8n8prj2")))

(define-public crate-netstring-0.4.0 (c (n "netstring") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "05bbvpgy92j7ja56zajhwhqashvhyg4yv2c3kyg2pnk76zjsrczs")))

(define-public crate-netstring-0.4.1 (c (n "netstring") (v "0.4.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1amabv4bw82ajnpgy4kznw4dymmffhnxwasdzmg3gmfyliijy659")))

