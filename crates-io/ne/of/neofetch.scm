(define-module (crates-io ne of neofetch) #:use-module (crates-io))

(define-public crate-neofetch-0.1.0 (c (n "neofetch") (v "0.1.0") (d (list (d (n "human_bytes") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0fnygxy77h3r3c11i60fgajalhz8k1kdpaikdlzcsvpxg2164p4v")))

(define-public crate-neofetch-0.1.1 (c (n "neofetch") (v "0.1.1") (d (list (d (n "human_bytes") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1hlpxj1yllmlc332gvcbddww2bjcipg2mza2ajswrkbbp3bfk6ij")))

(define-public crate-neofetch-0.1.2 (c (n "neofetch") (v "0.1.2") (d (list (d (n "ansi-width") (r "^0.1.0") (d #t) (k 0)) (d (n "human_bytes") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0ciz63qwzjiq3qbj12m93s6yz2s0frvcil2qg5aajmnnf4sg3azk")))

(define-public crate-neofetch-0.1.3 (c (n "neofetch") (v "0.1.3") (d (list (d (n "ansi-width") (r "^0.1.0") (d #t) (k 0)) (d (n "human_bytes") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "00hfx768jk01fp09l84si2g5p6lmgg32mlym32r82ila6h9p6zsx")))

