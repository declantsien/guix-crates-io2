(define-module (crates-io ne of neofiglet) #:use-module (crates-io))

(define-public crate-neofiglet-0.1.0 (c (n "neofiglet") (v "0.1.0") (h "06v6cmi2j3xj6zvvxx4l0ad628vkg599ka1941azn2vlj6rznhc9")))

(define-public crate-neofiglet-0.1.1 (c (n "neofiglet") (v "0.1.1") (h "06khlq95m5pph6k1ix7ykhddx5rfgxsrr69r7f5cg4n67hf3xn0a")))

