(define-module (crates-io ne eq neeqaque) #:use-module (crates-io))

(define-public crate-neeqaque-0.1.0 (c (n "neeqaque") (v "0.1.0") (h "0y484na74yddkszp1v56283ca0xl5179nnl54isxmm17ip3191p6") (y #t)))

(define-public crate-neeqaque-0.2.0 (c (n "neeqaque") (v "0.2.0") (h "0785qwbam2jhvw9jb2kp8spq0k03rhyy9xfi2ijcvphzil4riqxh")))

