(define-module (crates-io ne oe neoercities) #:use-module (crates-io))

(define-public crate-neoercities-0.1.0 (c (n "neoercities") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13riiwfrlfczmh7aqd1caxq1cb6sankzfabyh83vln38vvjnx1vm") (f (quote (("site_info" "serde_json" "sha-1" "chrono") ("default"))))))

(define-public crate-neoercities-0.1.1 (c (n "neoercities") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vhmvcrw84lk413qfawwiqn5yiy2c6nm88vb8k0h6zz3qk75zpml") (f (quote (("site_info" "serde_json" "sha-1" "chrono") ("default"))))))

