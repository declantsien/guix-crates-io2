(define-module (crates-io ne tx netxt) #:use-module (crates-io))

(define-public crate-netxt-0.1.0 (c (n "netxt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1xbwy0gddqjcmc63zfdz71c5wnckd35r3qi03hfvj463lk10ynrz")))

(define-public crate-netxt-0.1.1 (c (n "netxt") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "018pf2m47sm9idlz5jwcl4fhq5j7ph3wqdsm3qc7qbv2k7m7m7qc")))

(define-public crate-netxt-0.1.2 (c (n "netxt") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1xfmdnmjjmagj1vd1vsk5ayrmh6hchfp8a4iqc4zisk2if0j34hc")))

(define-public crate-netxt-0.1.3 (c (n "netxt") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "00rk8nw8da6nj4bclhyr19xk57abxfbvf2ldshqc95g0r88hqy35")))

(define-public crate-netxt-0.1.4 (c (n "netxt") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0d0czvqqlw8ndh6fr0iq4ws44523gf1bykal3bqlcd6sxlfkv06b")))

(define-public crate-netxt-0.2.0 (c (n "netxt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "02ygl9zps2pi31h08gmp29iq9j8r0m8r73dh7bdxc22gd6rxy9ay")))

