(define-module (crates-io ne il neil) #:use-module (crates-io))

(define-public crate-neil-0.1.0 (c (n "neil") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0v38najsr0lps4wzj6gwjfjnqdk0iji7aghrzpz4cy3rypx9pw2k")))

(define-public crate-neil-0.2.0 (c (n "neil") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1ygsc7dm6qhg5lms7dbr7bn8j5xkcl1f5ldza069kvd2jwhpm7wq")))

