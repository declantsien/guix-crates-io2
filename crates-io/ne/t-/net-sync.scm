(define-module (crates-io ne t- net-sync) #:use-module (crates-io))

(define-public crate-net-sync-0.0.0 (c (n "net-sync") (v "0.0.0") (h "0r26z4b7s6h3hq91a6ajlgvfdn7b1snwrshlcaz9wihy1az2biv3")))

(define-public crate-net-sync-0.0.1 (c (n "net-sync") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lz4-compress") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "track") (r "^0.1.0") (d #t) (k 0)))) (h "1m5b8xfi41ii3iri9bg3vr0865jvs5hzcyi7dwpk1zvzrs8k3sak") (f (quote (("lz4-compresion" "lz4-compress") ("default" "lz4-compresion"))))))

