(define-module (crates-io ne t- net-updater-api) #:use-module (crates-io))

(define-public crate-net-updater-api-0.1.0 (c (n "net-updater-api") (v "0.1.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0lraqbml7vkwlpdclsx0sh7pqmpxx3gi6sar41x4wq0mhhh07kgh")))

(define-public crate-net-updater-api-0.3.0 (c (n "net-updater-api") (v "0.3.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0m76xki0alm3yw2d2jn4gm1j3gww5w1q1vhl3k5pa72wswbsq01d")))

(define-public crate-net-updater-api-0.4.0 (c (n "net-updater-api") (v "0.4.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "18j28fjqvmv7hg1b42v32rhqcjcpq1d5akd8k8sf5nch05cb2k36")))

(define-public crate-net-updater-api-0.4.1 (c (n "net-updater-api") (v "0.4.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "1bxvx8ivcqspwgcfj3q3bdfv09dncq422ibh32l2j53vyhyzdqil")))

