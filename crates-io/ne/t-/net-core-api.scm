(define-module (crates-io ne t- net-core-api) #:use-module (crates-io))

(define-public crate-net-core-api-0.1.0 (c (n "net-core-api") (v "0.1.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "1xkcgavgyib65mb9c69h5g23nh6kpqg3arl4h0x939xrqr8ijw2p")))

(define-public crate-net-core-api-0.2.0 (c (n "net-core-api") (v "0.2.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "0vmplzs6g58vy17a28dj48p9kpwjrvly6bsb4vx20lzm6l6q4lg4")))

(define-public crate-net-core-api-0.3.0 (c (n "net-core-api") (v "0.3.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "0flhandic1ir55dn2dwj3kfl0rcnl197n22w270cn3r5w4kg12rn")))

(define-public crate-net-core-api-0.4.0 (c (n "net-core-api") (v "0.4.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "17502i2jp1w9ia50zi2asna8b0q1q670cky7iq810a08vs6bn5yz")))

(define-public crate-net-core-api-0.5.0 (c (n "net-core-api") (v "0.5.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "1pp0fd23rz5s8xmqyd5k0m91dmvvvkx5pybgfn3rly7w2xn1nbvj")))

(define-public crate-net-core-api-0.5.1 (c (n "net-core-api") (v "0.5.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "1mrwjhk5jq9r99hhnadr2grz9safbx9js6fs80qnhr4c0gpayhay")))

(define-public crate-net-core-api-0.5.2 (c (n "net-core-api") (v "0.5.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "09mgscb23wl27s4k6kzbs6y7q2a18ifla5mj88j7chh0qn2g8xpg")))

(define-public crate-net-core-api-0.5.3 (c (n "net-core-api") (v "0.5.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)))) (h "1swnbbs6ibg90bmzy7ap72dpsaq6a1qdmcsbsf01lljknkz0nias")))

