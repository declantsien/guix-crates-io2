(define-module (crates-io ne t- net-reporter-api) #:use-module (crates-io))

(define-public crate-net-reporter-api-0.1.2 (c (n "net-reporter-api") (v "0.1.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "0wpg94l3d5m7kggpxbxvv5z9fvvvmr807cjcc23n8j50x8dbc5bj")))

(define-public crate-net-reporter-api-0.1.3 (c (n "net-reporter-api") (v "0.1.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "0dcd3whq20kbjhydq5c340pr2kckwa577vf9azxnm3mzvx6v92x0")))

(define-public crate-net-reporter-api-0.1.31 (c (n "net-reporter-api") (v "0.1.31") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "0c5iiriavxs9k72wzz1k5ps28l10q40vnjkxgcdwkx7p576zksw8")))

(define-public crate-net-reporter-api-0.1.32 (c (n "net-reporter-api") (v "0.1.32") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "1cvxd39ldjnhpvyp7jjb3dzvval6q36c0xdgg4b4k7sh3fabbf2s")))

(define-public crate-net-reporter-api-0.2.0 (c (n "net-reporter-api") (v "0.2.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "0icfwdjs2hkfzrsmabrq4vvh3lgjjlgzjy2xhjy3hgxda7wb8l1f")))

(define-public crate-net-reporter-api-0.3.0 (c (n "net-reporter-api") (v "0.3.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "1jlfp63s6gk09hi27s8snbz86d7kazljh2k7gx8z49kz9dkqkdx6")))

(define-public crate-net-reporter-api-0.3.1 (c (n "net-reporter-api") (v "0.3.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "024xvsgrxhwll52zzfni4dq0xk5i73q0nwyifkk8xynvgrbsz610")))

(define-public crate-net-reporter-api-0.3.2 (c (n "net-reporter-api") (v "0.3.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "0ii00xxrgpw069irvgi1dwx1cfb0w2r4j9ba4axkypvp51axhgmz")))

(define-public crate-net-reporter-api-0.4.0 (c (n "net-reporter-api") (v "0.4.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "0bjgj4n60r57srspx34wchlmgjn05g8hh417c1111v9y9czg1dss")))

(define-public crate-net-reporter-api-0.4.1 (c (n "net-reporter-api") (v "0.4.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "1m0nhjqvy10hib1ixd6s3v69w8ixz76c6zk063s6a2m1ba6wr6hy")))

(define-public crate-net-reporter-api-0.4.2 (c (n "net-reporter-api") (v "0.4.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "1k8645j7d9j8pjvk8r2qm3a96ixnnfmdgf6ww2axpkxhmxl8cqvn")))

(define-public crate-net-reporter-api-0.4.3 (c (n "net-reporter-api") (v "0.4.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "0qhsd1g3xf7hgvbq18qwv865l89bp6h1fclfawmbxyb7hvmrx59f")))

(define-public crate-net-reporter-api-0.4.4 (c (n "net-reporter-api") (v "0.4.4") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "1pqxqr092wphkakm31p287jiljgkx4nl4ngl1w2lsb99id4ji668")))

(define-public crate-net-reporter-api-0.4.5 (c (n "net-reporter-api") (v "0.4.5") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "0jwbhsxpgscmdpazhc4549ps2qy7467ga7qacw0y91klv4lxzg18")))

(define-public crate-net-reporter-api-0.5.0 (c (n "net-reporter-api") (v "0.5.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "02gj6bsjmsy7gfq8fzkw8ilfdkq7365pdiqszdp4b0x28m46h96b")))

(define-public crate-net-reporter-api-0.5.1 (c (n "net-reporter-api") (v "0.5.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.4.0") (d #t) (k 0)))) (h "17bg7yhyj19s1ziw42g4fq0kcbrgg2fn06lld6p6kawxgxxblf7c")))

(define-public crate-net-reporter-api-0.5.2 (c (n "net-reporter-api") (v "0.5.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "066rwm6zv6mypksz5wcz6v2yr6m2c15h8ysid4gwc8ja5hmhvabf")))

(define-public crate-net-reporter-api-0.6.0 (c (n "net-reporter-api") (v "0.6.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "1a6sli25acjvnj39dcc0pskhdaasyhyqagmpwd9x6qvwzflw8jn2")))

(define-public crate-net-reporter-api-0.6.1 (c (n "net-reporter-api") (v "0.6.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "1cx4n3wzdw5yzyn59rwkgchzxc7nssh5g6qnqn996y6h7hpc8kc6")))

(define-public crate-net-reporter-api-0.6.2 (c (n "net-reporter-api") (v "0.6.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "01mal8fvl0jyhp8zfrphdd056mjr05n3jm1s227fmpschjnca4cw")))

(define-public crate-net-reporter-api-0.7.0 (c (n "net-reporter-api") (v "0.7.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0my4m62xyaaik9vwv601dk69fxxrcnsvdxbzf1fqckfcwmbwa7xy")))

(define-public crate-net-reporter-api-0.7.1 (c (n "net-reporter-api") (v "0.7.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "04650vx0fy42m05is715kli7g53sspgb7dwcqmc4rh09lmymc1n3")))

(define-public crate-net-reporter-api-0.7.2 (c (n "net-reporter-api") (v "0.7.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "080ps8ybz07m6xmz45gx2jgq40rjswgdqy259sqz3y02lircbh5z")))

(define-public crate-net-reporter-api-0.7.3 (c (n "net-reporter-api") (v "0.7.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0jsm23qw8b9ki4jji3bwzn1bbi2xdk98sf8dj2vyi1lzka1imvi0")))

