(define-module (crates-io ne t- net-config) #:use-module (crates-io))

(define-public crate-net-config-0.1.0 (c (n "net-config") (v "0.1.0") (d (list (d (n "net-file") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "15lvnzfn9b6951daryfy47w0qfnyl7brcdh7pz5xh6a202lyljpk")))

(define-public crate-net-config-0.1.1 (c (n "net-config") (v "0.1.1") (d (list (d (n "net-file") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1bc64mkz8k09plnwm5nv5shnhvry6lhy0k4rnc8kl1p10s7f0zj7")))

