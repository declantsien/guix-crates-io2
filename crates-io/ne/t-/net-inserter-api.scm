(define-module (crates-io ne t- net-inserter-api) #:use-module (crates-io))

(define-public crate-net-inserter-api-0.1.2 (c (n "net-inserter-api") (v "0.1.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "018pg97cphkb4agqf3k8y8bz90dwvw745pzxlwsrqfhrr869p7zz")))

(define-public crate-net-inserter-api-0.1.3 (c (n "net-inserter-api") (v "0.1.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "122ap8ngbv03dizvrxfbzvaq8pz125w3jph1bqxf87slzbr3mirh")))

(define-public crate-net-inserter-api-0.1.4 (c (n "net-inserter-api") (v "0.1.4") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.4.0") (d #t) (k 0)))) (h "01qb3jw0n854q3nrkdps2zimmwbs0xkq2cvisy1ysd1d1fzh5kix")))

(define-public crate-net-inserter-api-0.1.5 (c (n "net-inserter-api") (v "0.1.5") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "00dy9bnsg6nn86mi8yadr8wr66ydzdhmgmyns6344h7bwxa6r8bp")))

(define-public crate-net-inserter-api-0.1.6 (c (n "net-inserter-api") (v "0.1.6") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "12rxk751j78k22r2ybsziicf939yp0vg7hwqq4clkaalgy2lcbld")))

(define-public crate-net-inserter-api-0.2.0 (c (n "net-inserter-api") (v "0.2.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0nsbgfaakb6z8sclaa297l585jnav215q76qh22wc73lcw1wkm5n")))

(define-public crate-net-inserter-api-0.4.0 (c (n "net-inserter-api") (v "0.4.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0xch0xxrkxkpld4gk4hfrg6fi26rwnwm1z5f067cc4a4hqkr6j0k")))

(define-public crate-net-inserter-api-0.4.1 (c (n "net-inserter-api") (v "0.4.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1msffzi98whgxfhjxrv29p2r2z0n011dl9sd93gwcz08n78xp5qc")))

