(define-module (crates-io ne t- net-literals-impl) #:use-module (crates-io))

(define-public crate-net-literals-impl-0.1.0 (c (n "net-literals-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "1l8by4ardd10m43sshimg1lidad424iaca57d13jss1z24hcksvd")))

(define-public crate-net-literals-impl-0.1.2 (c (n "net-literals-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1yp225hg52sl7mw5swqmi04xx30yq7kj52q9nnrbmsj9x15r7d95")))

