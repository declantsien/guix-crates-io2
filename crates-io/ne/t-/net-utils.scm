(define-module (crates-io ne t- net-utils) #:use-module (crates-io))

(define-public crate-net-utils-0.0.1 (c (n "net-utils") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "openssl") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "17l9ql1wqsirpy31s3rpyy72my05x4vq7lkxsn4fq9vk3kdqp1j0") (f (quote (("ssl" "openssl"))))))

(define-public crate-net-utils-0.0.2 (c (n "net-utils") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "openssl") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "144n9dxqs2vicc6aw2n0vy59wsigvzys245m3cv8355jdb26bm21") (f (quote (("ssl" "openssl"))))))

(define-public crate-net-utils-0.0.3 (c (n "net-utils") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "0g0sjznmx19jrnlgmzr8qdnhq8jvqs41ysyqricx1palqpzlbsp3") (f (quote (("ssl" "openssl"))))))

(define-public crate-net-utils-0.0.4 (c (n "net-utils") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9.22") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1qcxqa05f3zddbcr0zks2y644cmgrgizxr9jnhh6j17lxns36l41") (f (quote (("ssl" "openssl"))))))

(define-public crate-net-utils-0.0.5 (c (n "net-utils") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "042m5is0n0g0yws1m3f7298riv9fgsra35y09kdi461h32bkk9jp") (f (quote (("ssl" "openssl"))))))

