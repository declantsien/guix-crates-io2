(define-module (crates-io ne t- net-parse) #:use-module (crates-io))

(define-public crate-net-parse-0.1.0 (c (n "net-parse") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-alpha") (d #t) (k 0)))) (h "0mhpcxmn9j4n5kj87q9jxxq8l1krcx0cd8k7h17w33nph90sg4zc")))

