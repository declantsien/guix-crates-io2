(define-module (crates-io ne t- net-agent-api) #:use-module (crates-io))

(define-public crate-net-agent-api-0.1.2 (c (n "net-agent-api") (v "0.1.2") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.1.0") (d #t) (k 0)))) (h "1g35gbq5zg4gxd38y7axwn72q6vzzlffs903janynw8lflrqzcjy")))

(define-public crate-net-agent-api-0.1.3 (c (n "net-agent-api") (v "0.1.3") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.2.0") (d #t) (k 0)))) (h "0nk78w3zfh8a0vn967y0hnp1a5w3yr547c3s3qj415jk7r9ddb9z")))

(define-public crate-net-agent-api-0.1.4 (c (n "net-agent-api") (v "0.1.4") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.4.0") (d #t) (k 0)))) (h "1b8jhnbldicmwzfwydch8s3yalchl3028ps9m90wp0jd1z23idas")))

(define-public crate-net-agent-api-0.1.5 (c (n "net-agent-api") (v "0.1.5") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0d4nlf1dyyhxmia2i8jbl6r5asmwzss4x872v6jq1h226kqd2h63")))

(define-public crate-net-agent-api-0.1.6 (c (n "net-agent-api") (v "0.1.6") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "1y2c9qif0apmlz34ps11k8v97nfs3d5fzkl37mcwwn06mhwz5f1h")))

