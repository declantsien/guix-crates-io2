(define-module (crates-io ne t- net-fs) #:use-module (crates-io))

(define-public crate-net-fs-0.0.0 (c (n "net-fs") (v "0.0.0") (h "0nx4qb8askxq3bgyppx27zbkdq7vyzlbxx5xyiyfmhmfph9jpmw2")))

(define-public crate-net-fs-0.0.1 (c (n "net-fs") (v "0.0.1") (h "0jm9v4cfgypd930mrdx577lp5jy4dzw4k5z1g7ca1baca6bhxxn4")))

(define-public crate-net-fs-0.0.2 (c (n "net-fs") (v "0.0.2") (h "00krzgkz99k64c4slfpf4ak7i42csk43ryk19cyq3aj1nrslh9sa")))

(define-public crate-net-fs-0.0.3 (c (n "net-fs") (v "0.0.3") (h "1mjfx579wf4rh1wx63j9bslslxp2wz9z750a1m3r827xcgyvgm7m")))

