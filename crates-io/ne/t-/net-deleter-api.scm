(define-module (crates-io ne t- net-deleter-api) #:use-module (crates-io))

(define-public crate-net-deleter-api-0.1.0 (c (n "net-deleter-api") (v "0.1.0") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "119i70hbd6z8yy9757ywm77ck0mkzpd3jh6qxya5dmidklmccmrw")))

(define-public crate-net-deleter-api-0.1.1 (c (n "net-deleter-api") (v "0.1.1") (d (list (d (n "ion-rs") (r "^0.18.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)))) (h "0ni40pvwp29z8bx16y1spcnrm7x2ylpg5rvn14i7w4yz2p59w67g")))

