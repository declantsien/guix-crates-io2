(define-module (crates-io ne t- net-compute) #:use-module (crates-io))

(define-public crate-net-compute-0.0.1 (c (n "net-compute") (v "0.0.1") (h "058v3hg2r70lh4sh7hhmjxahqxc6gi6dx0as6z649ww12hidfa53")))

(define-public crate-net-compute-0.0.2 (c (n "net-compute") (v "0.0.2") (h "1ca9hjhk7x40nw2j6d67r2yax0zf6z4y2iwmm6pm2g9wjka7g8k7")))

(define-public crate-net-compute-0.0.3 (c (n "net-compute") (v "0.0.3") (h "1shgnx54kfymr01rj8z3zd2d9nv56lbmgf669xc31jg06kc923vl")))

