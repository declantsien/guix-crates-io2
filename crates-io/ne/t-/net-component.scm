(define-module (crates-io ne t- net-component) #:use-module (crates-io))

(define-public crate-net-component-0.1.0 (c (n "net-component") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "net-config") (r "^0.1.1") (d #t) (k 0)) (d (n "net-core-api") (r "^0.5.0") (d #t) (k 0)) (d (n "net-transport") (r "^0.1.1") (f (quote ("quinn"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.0") (f (quote ("runtime-async-std" "tls-native-tls" "postgres" "chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r5w6vxidqw0fjh7gkvvqhk079vfwy01yg507rmg1ziwpan8wjl1")))

