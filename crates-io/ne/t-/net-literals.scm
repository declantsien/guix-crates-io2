(define-module (crates-io ne t- net-literals) #:use-module (crates-io))

(define-public crate-net-literals-0.1.0 (c (n "net-literals") (v "0.1.0") (d (list (d (n "net-literals-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0sfjc8vs90ni4ykhqvnj47nqrzi8cs8avyy48kf026g18y3qgfij")))

(define-public crate-net-literals-0.1.1 (c (n "net-literals") (v "0.1.1") (d (list (d (n "net-literals-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0agis3zra7sx1qbmiawvg9i7xvrz1h98f3kqm4yjvqgx5h2qrqgm") (y #t)))

(define-public crate-net-literals-0.1.2 (c (n "net-literals") (v "0.1.2") (d (list (d (n "net-literals-impl") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0sxpyihlspfk02px52sr16y84cp8c97ixf10gkq15fvk8q8grrjx")))

(define-public crate-net-literals-0.2.0 (c (n "net-literals") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0vacc5844wbwjpcbnigqyfwsdxwgz77c8xg474r4jdvwx1d2d4ys")))

