(define-module (crates-io ne t- net-replay) #:use-module (crates-io))

(define-public crate-net-replay-0.1.0 (c (n "net-replay") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hv42saf2rz8pvczg334znazidviq327km46ygr8f4bkyhn9hy56")))

(define-public crate-net-replay-0.2.0 (c (n "net-replay") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gh1ab1fzgdaqf9r44mlh0m6qh6cj4ai9qg2mxsdl1savg8k8w3s")))

(define-public crate-net-replay-0.3.0 (c (n "net-replay") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xnpkjzgk9lpr0z3djy2isns376mwfn8d2kjd0scfr9j6p7d2l33")))

