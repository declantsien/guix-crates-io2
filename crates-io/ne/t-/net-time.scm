(define-module (crates-io ne t- net-time) #:use-module (crates-io))

(define-public crate-net-time-0.1.0 (c (n "net-time") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0hjndnnai74r76fcivgrqazvrjmvjbp8q485z67rqd9d6q73c7jm")))

