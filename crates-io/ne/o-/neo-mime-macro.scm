(define-module (crates-io ne o- neo-mime-macro) #:use-module (crates-io))

(define-public crate-neo-mime-macro-0.1.0 (c (n "neo-mime-macro") (v "0.1.0") (d (list (d (n "neo-mime-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1wrhwbi6r876al29s9mrdvg9dknrvshcfg46z3869j71gqd6fi9c")))

(define-public crate-neo-mime-macro-0.1.1 (c (n "neo-mime-macro") (v "0.1.1") (d (list (d (n "neo-mime-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "1avk7mfd6y0i4mb4pnqcfv95s1y225fjy40hlyckcc5s0z594vdd")))

