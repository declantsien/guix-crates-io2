(define-module (crates-io ne o- neo-crypto) #:use-module (crates-io))

(define-public crate-neo-crypto-0.1.0 (c (n "neo-crypto") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-primes") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "014m9i9d1xjhxjsizhhf5d6shhdcax22h92g5jymj5g3wpsrndig")))

(define-public crate-neo-crypto-0.1.1 (c (n "neo-crypto") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-primes") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f92b16vzm753hjmna0c9k8wb7pgxb0fmfprb4fqp6kanapjaaqk")))

(define-public crate-neo-crypto-0.1.2 (c (n "neo-crypto") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-primes") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bc3ahdkyrb7a2nwq282jd49p9zr4dxgsfybfsvfv1inrs9fisx1")))

