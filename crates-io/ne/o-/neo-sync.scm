(define-module (crates-io ne o- neo-sync) #:use-module (crates-io))

(define-public crate-neo-sync-0.0.1 (c (n "neo-sync") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ni0ivw0xrphivp6mgbvwjqmjjd0mm625a92pckss9jy39qbm3wg")))

(define-public crate-neo-sync-0.0.2 (c (n "neo-sync") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wvs87vxdkpfh8m5sai0bgwrz1hxin4bj6jppwia9ax93sza7si2")))

(define-public crate-neo-sync-0.0.3 (c (n "neo-sync") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nklhz8nawm3v5rxwcciahvw86r8c7krc897308z89z7aha2705x")))

(define-public crate-neo-sync-0.0.4 (c (n "neo-sync") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0884xkqhlz46kdw9bpg62zs8kx6nshymjwv5d33iqp13vk5xsvja")))

(define-public crate-neo-sync-0.0.5 (c (n "neo-sync") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10yjv3vj02nfqzwwwczw53n02x4yfd6rakizq8lg2d6j58b7sfh0")))

(define-public crate-neo-sync-0.0.6 (c (n "neo-sync") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a49py2lr3g6fhn40qzw7870xl2ky0i7g6bq75vynj9v5c4r15gn")))

(define-public crate-neo-sync-0.0.7 (c (n "neo-sync") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "10bqc5m6jqbg7mjwhfnamkgbx21a03g92jk997qclykwhyy85g1i")))

(define-public crate-neo-sync-0.0.8 (c (n "neo-sync") (v "0.0.8") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1qx69fi51cs19qlp68mpvqk8aar0lkskawpmmbidkvdky144fljr")))

