(define-module (crates-io ne o- neo-mime) #:use-module (crates-io))

(define-public crate-neo-mime-0.1.0 (c (n "neo-mime") (v "0.1.0") (d (list (d (n "neo-mime-macro") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "neo-mime-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (o #t) (d #t) (k 0)) (d (n "quoted-string") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "05lvwlavrx46rql56l26qxgp27asvqm3012ikqzj8mhivqww8hcg") (f (quote (("macro" "neo-mime-macro" "proc-macro-hack"))))))

(define-public crate-neo-mime-0.1.1 (c (n "neo-mime") (v "0.1.1") (d (list (d (n "neo-mime-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "neo-mime-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.19") (o #t) (d #t) (k 0)) (d (n "quoted-string") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1d2gg4mc87j1kqdv1jzx5jgf6ar1w1fhv8q0df484xyz5g53xla4") (f (quote (("macro" "neo-mime-macro" "proc-macro-hack"))))))

