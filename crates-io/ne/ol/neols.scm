(define-module (crates-io ne ol neols) #:use-module (crates-io))

(define-public crate-neols-0.2.0 (c (n "neols") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1wbwpn86w3x6l520bqw4q8087y23kanv4a9kfniczw64h6nxar45")))

(define-public crate-neols-0.2.1 (c (n "neols") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "04pdn7abl975jmw3vnv6bpmi86ikmpj1h45wcqahpr2b1hqy1630") (y #t)))

(define-public crate-neols-0.2.2 (c (n "neols") (v "0.2.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0w8nmris9pdz1kqy51q44xw1v67xs3gyl8gpp46cs64iyrbl3x97") (y #t)))

(define-public crate-neols-0.2.3 (c (n "neols") (v "0.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0hp5cdgcx0gk8mk30q13j7x7vx86xh4hmkjb4q0mnl8hlfrd9xrv") (y #t)))

(define-public crate-neols-0.3.0 (c (n "neols") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1wm17993hjvzhc2b1ah2xh4mp9lkljfxpz802wrwk9m2cmms3561")))

(define-public crate-neols-0.4.0 (c (n "neols") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0w5agf9dr23pywrysjnq8mv8r8kf9ygf4k0vck6qma9vsmgfzwhk") (y #t)))

(define-public crate-neols-0.4.1 (c (n "neols") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "04z62af7vn6v7kx9h44iwzf2wlfhfxfyikbs007w2jr9iz1a3pl1")))

(define-public crate-neols-0.5.0 (c (n "neols") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1fsm3s9sx0f6k86vd6agi644v3qdm3ck4r5ljbi8wkx2g5xpyjjm") (y #t)))

(define-public crate-neols-0.5.1 (c (n "neols") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "13524lkw5zdk3jh58qnrv829bpbsmwykrzns6g0pgicg4rhzgj95")))

(define-public crate-neols-0.6.0 (c (n "neols") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "09fyw8p7kpdqacj4bhwjawm9n6knn4x8ma364w400l0mfg37pjkj") (y #t)))

(define-public crate-neols-0.6.1 (c (n "neols") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1jkvj3rbjqz14hk0c1v7sy8i1w36b43g94r58wj0fmmzx7qzcip3")))

(define-public crate-neols-0.7.0 (c (n "neols") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0pc0r968b7mvshl27fcwz9lhdyp05bs3vzii91n10k5bpigznc03")))

