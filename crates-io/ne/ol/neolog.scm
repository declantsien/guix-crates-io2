(define-module (crates-io ne ol neolog) #:use-module (crates-io))

(define-public crate-neolog-0.0.1 (c (n "neolog") (v "0.0.1") (h "0dn670rhvy1whwq9k1zd1sr5rq5l8vdwywwjb1566mmc2rfpa6zj")))

(define-public crate-neolog-0.0.2 (c (n "neolog") (v "0.0.2") (h "1whadr27047iya5gijmjlr3f6vsn2f3z5ng4fvi403s0ia0iam3z")))

(define-public crate-neolog-0.0.3 (c (n "neolog") (v "0.0.3") (h "1f1678vqk3rwk2ih1k3g8csbm2lks7brf9ss7rxr4p7sj534vwjh")))

(define-public crate-neolog-0.0.4 (c (n "neolog") (v "0.0.4") (h "1f3kvcwxzkwvs8aaqxvggcsrljdvjk555wqxiq7z80b72qsa820j")))

