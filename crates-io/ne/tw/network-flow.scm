(define-module (crates-io ne tw network-flow) #:use-module (crates-io))

(define-public crate-network-flow-0.1.0 (c (n "network-flow") (v "0.1.0") (d (list (d (n "dot-generator") (r "^0.2.0") (d #t) (k 0)) (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)) (d (n "graphviz-rust-bla") (r "0.*") (d #t) (k 0)))) (h "0ww0qim3sq5y6acvsq8pparrk7i99nx4855c5c6i2qdg4d7k0lnr")))

