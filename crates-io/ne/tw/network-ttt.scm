(define-module (crates-io ne tw network-ttt) #:use-module (crates-io))

(define-public crate-network-ttt-0.1.0 (c (n "network-ttt") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.4.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1cch9l9fzlc3bi2d9k8sdrvyaa39n32hddym4ysr0qj1js4wn8r1") (y #t)))

(define-public crate-network-ttt-1.0.0 (c (n "network-ttt") (v "1.0.0") (d (list (d (n "ctrlc") (r "^3.4.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1s3bvf8qyl1w7vb436hm3436rc3y5jg7y34r00jmzi9lzpr7gk0q")))

(define-public crate-network-ttt-1.0.1 (c (n "network-ttt") (v "1.0.1") (d (list (d (n "ctrlc") (r "^3.4.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1lr14jcqicp9yc22x495cafkpa4wf1jsyrdp19mmsil4g0bil790")))

