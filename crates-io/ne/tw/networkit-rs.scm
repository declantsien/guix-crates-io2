(define-module (crates-io ne tw networkit-rs) #:use-module (crates-io))

(define-public crate-networkit-rs-0.1.0-alpha.0 (c (n "networkit-rs") (v "0.1.0-alpha.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "openmp-sys") (r "^1.2.3") (f (quote ("static"))) (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)))) (h "00z1sd6fcxzlvifn6dpa3ilx4wcv897yb6mcw7s1s64rcl4bi7sc")))

(define-public crate-networkit-rs-0.1.0 (c (n "networkit-rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "openmp-sys") (r "^1.2.3") (f (quote ("static"))) (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 1)))) (h "1rmk1b3v9gdw26m96mjh41s6c6535sarc715fka32al58pphllfh")))

