(define-module (crates-io ne tw networking_accumlator_types) #:use-module (crates-io))

(define-public crate-networking_accumlator_types-0.1.0 (c (n "networking_accumlator_types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j94jrzb76gymwzz0ny3ncaxx8rx9xi4qqvjn9grblpagl299iyl") (y #t)))

