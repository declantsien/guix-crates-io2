(define-module (crates-io ne tw networkdirect-sys) #:use-module (crates-io))

(define-public crate-networkdirect-sys-0.1.0 (c (n "networkdirect-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_IO" "Win32_Networking_WinSock"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 1)))) (h "1qylrpyaqij50q5y1qqm23yhjx1ff1n5daldxbv30j9lr3h9a7pm")))

(define-public crate-networkdirect-sys-0.1.2 (c (n "networkdirect-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_IO" "Win32_Networking_WinSock"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 1)))) (h "0ddpkgx3j0sycrri0n31n7lxp90s525vlxnyhjm9iqfvvljaqn7p")))

