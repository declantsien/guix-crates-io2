(define-module (crates-io ne tw network-communicator) #:use-module (crates-io))

(define-public crate-network-communicator-0.1.0 (c (n "network-communicator") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-curl") (r "^0.1") (d #t) (k 0)))) (h "06sb09nigqv7nhkynr5gsi5daj58vc5jsy9glp95z9m2hdk1sm7r")))

(define-public crate-network-communicator-0.1.1 (c (n "network-communicator") (v "0.1.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-curl") (r "^0.1") (d #t) (k 0)))) (h "1g52n122jghc7vkfa4kmw33l5wv9q75fm1837s299nzx21f4qdik")))

