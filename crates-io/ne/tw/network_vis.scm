(define-module (crates-io ne tw network_vis) #:use-module (crates-io))

(define-public crate-network_vis-0.1.0 (c (n "network_vis") (v "0.1.0") (h "1w5bqd5sgx3q4rq7f7wpfchj6347xaasabp9fcpbdcw1bxplcc7f")))

(define-public crate-network_vis-0.1.2 (c (n "network_vis") (v "0.1.2") (h "15fjvzrr5dglmbk86bzcd4n8315d9mljd4kcibhdy9xg168imdyv")))

