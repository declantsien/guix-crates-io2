(define-module (crates-io ne tw networkmanager) #:use-module (crates-io))

(define-public crate-networkmanager-0.0.1 (c (n "networkmanager") (v "0.0.1") (d (list (d (n "dbus") (r "^0.8.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wfkvzrjv8bq5c7jcx7n46119cgkq3mr5sw8j710z92h48y6c6vs") (y #t)))

(define-public crate-networkmanager-0.1.0 (c (n "networkmanager") (v "0.1.0") (d (list (d (n "dbus") (r "^0.8.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qq3m9rpd4yc5mcy3mqzk7pyhg1hndq05r5x9cf0qamh5x6im1ai")))

(define-public crate-networkmanager-0.2.0 (c (n "networkmanager") (v "0.2.0") (d (list (d (n "dbus") (r "^0.8.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kdm6gjnzf7038hdd8h0srcni11zq2v5k0vwf1czrwx516vrn1w9")))

(define-public crate-networkmanager-0.3.0 (c (n "networkmanager") (v "0.3.0") (d (list (d (n "dbus") (r "^0.8.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14cxf21j0x7cdyn5gnas05lrnlqi3a5k0kzi3m9wgx77dfp2a4qq")))

(define-public crate-networkmanager-0.3.1 (c (n "networkmanager") (v "0.3.1") (d (list (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ngpjksxq4ym1cvzb6i79qp539jm6vch9gdxxp79rm4s0jz72g2l")))

(define-public crate-networkmanager-0.3.2 (c (n "networkmanager") (v "0.3.2") (d (list (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1080mwz4x6dradlp8vcs89kgg87bfzg0cx3wlkclbxhzgb4i1msi")))

(define-public crate-networkmanager-0.3.3 (c (n "networkmanager") (v "0.3.3") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wn39l2y8f68xqr7m50rkh3ndc2p3na7swy7agzx8vajfjm704fq")))

(define-public crate-networkmanager-0.3.4 (c (n "networkmanager") (v "0.3.4") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1x68hcwvzh6z0yc1426bf2n8rl1cy5y7qwkyadwgw1b5vp312ir1") (y #t)))

(define-public crate-networkmanager-0.3.5 (c (n "networkmanager") (v "0.3.5") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bk40y5wy1kf4c6ch3kcqxz7npf7lwhmbahyhn3m0s7krd0iyzhz") (y #t)))

(define-public crate-networkmanager-0.4.0 (c (n "networkmanager") (v "0.4.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vl7jz1gdb9zir2a9g6l8kpdjag39y5diggvv1x5y08xbm4w7hjv")))

(define-public crate-networkmanager-0.4.1 (c (n "networkmanager") (v "0.4.1") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1n4cb4hslsd53c2jgb3h06wbp1vl3w9qmpjsc1fdyjsza15akirn")))

