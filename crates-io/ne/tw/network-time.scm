(define-module (crates-io ne tw network-time) #:use-module (crates-io))

(define-public crate-network-time-0.1.0 (c (n "network-time") (v "0.1.0") (d (list (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1q4f8h3b3xk4prvxw984a6sqyvgks3ziv4l27mjqmlln708zk4ix")))

(define-public crate-network-time-0.1.1 (c (n "network-time") (v "0.1.1") (d (list (d (n "likely") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19jvnvhqd56dq464bcjh08by6w93rymqsp2wy0v4w0r8qpzbbg2b")))

