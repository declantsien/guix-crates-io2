(define-module (crates-io ne tw network-check-sum) #:use-module (crates-io))

(define-public crate-network-check-sum-0.1.0 (c (n "network-check-sum") (v "0.1.0") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "network-endian") (r "^0.1") (d #t) (k 0)))) (h "16ywyk9asvy5ldsffbgyz5qzgnxq6w4a5bvsphaifzhbs1vpnvdi")))

(define-public crate-network-check-sum-0.1.1 (c (n "network-check-sum") (v "0.1.1") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "network-endian") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1j83n4908p14yzpgc9rx55hvh5mx53vp6ncyckmysqz6ph3r5kkl")))

(define-public crate-network-check-sum-0.1.3 (c (n "network-check-sum") (v "0.1.3") (d (list (d (n "network-endian") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1m9ll0dxqd3mc32m2bwfw84s48x6n72n3vm6myh5ycmsq8dn272k")))

(define-public crate-network-check-sum-0.1.4 (c (n "network-check-sum") (v "0.1.4") (d (list (d (n "network-endian") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kawgpaf6mgb2rwysbv3w3a0x8h1k017gfj5ccxa9my7cc01vij6")))

