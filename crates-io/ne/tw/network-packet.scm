(define-module (crates-io ne tw network-packet) #:use-module (crates-io))

(define-public crate-network-packet-0.1.0 (c (n "network-packet") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16hvh54djkn2drkaaffzy4dfz70g3rsnhk26nav5bgi7v0mc0rld")))

(define-public crate-network-packet-0.1.1 (c (n "network-packet") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15q6cfi84cf4cjnbw3p4xzrijz32h17d5m91k012wzq31dfgvbh4")))

