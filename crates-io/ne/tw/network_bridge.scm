(define-module (crates-io ne tw network_bridge) #:use-module (crates-io))

(define-public crate-network_bridge-0.1.0 (c (n "network_bridge") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "0yn48z6fv70v1jyjr9nqjqzkyq3jw8hk26hbky85w1vrk56h6liz")))

(define-public crate-network_bridge-0.1.1 (c (n "network_bridge") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "0sz6bqyy9g6q3l6sl232h2xi8n8vjr50mjkc4pksmj9vh11c6cjd")))

