(define-module (crates-io ne tw network-endian) #:use-module (crates-io))

(define-public crate-network-endian-0.1.0 (c (n "network-endian") (v "0.1.0") (h "0cb03zkxvk7812xm3cx3xh5msqp5xdj52m6ycxsfikib58hjyz8q")))

(define-public crate-network-endian-0.1.1 (c (n "network-endian") (v "0.1.1") (h "19fx653cp2jzfv04brwl01gdl32866j2s703da2l40g7m96dfynz")))

(define-public crate-network-endian-0.1.2 (c (n "network-endian") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sgqr421rmh5bdnllcrisdk068smdgd7c2a17klrwzfwzdqn10bd")))

(define-public crate-network-endian-0.1.3 (c (n "network-endian") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hd1vrddkms9kj3k5izzqpf76kv3f0fzwrjnm4qd461zfnpr12cz")))

(define-public crate-network-endian-0.1.4 (c (n "network-endian") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1g10xrxj52sk0hi5x0gdbmsk47m10av0xigk8ka1dkvnwy3pblrs")))

(define-public crate-network-endian-0.1.5 (c (n "network-endian") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1b4c5h6cxzlrzcshi4sjdxac9jg3993dvnjbmp0zxl3dg6yfjrba")))

