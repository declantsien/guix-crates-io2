(define-module (crates-io ne tw network_peers_discovery) #:use-module (crates-io))

(define-public crate-network_peers_discovery-0.0.1 (c (n "network_peers_discovery") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (d #t) (k 0)))) (h "1sicgcbhkj5ridwhp8lj68yx1fj72wabkdpwwfb71q4c8l631vwy")))

(define-public crate-network_peers_discovery-0.0.2 (c (n "network_peers_discovery") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.1") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (d #t) (k 0)))) (h "0zk9kks9z4967zkaqnz9my43wlqbpk7pk2r8g6bcz20jwk4aix77")))

