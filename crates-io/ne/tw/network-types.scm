(define-module (crates-io ne tw network-types) #:use-module (crates-io))

(define-public crate-network-types-0.0.1 (c (n "network-types") (v "0.0.1") (h "0wf5b1mxra1pvfz656k4qkk7ywnf28qq5mbpdjbvqr1q9i7r9kgz")))

(define-public crate-network-types-0.0.2 (c (n "network-types") (v "0.0.2") (d (list (d (n "enum-try-from") (r "^0.0.1") (d #t) (k 0)))) (h "1nix1w1lq91v6i8rc533pk5cajpdfs5b9x7ygb8jjg10ii3ki1i8")))

(define-public crate-network-types-0.0.3 (c (n "network-types") (v "0.0.3") (h "128xg7c4xpdghaa1n28mirfsnqc567lzfvlijgqwgxibd0i9r3iv")))

(define-public crate-network-types-0.0.4 (c (n "network-types") (v "0.0.4") (h "0i4grfw9qa6i0l5jf65wbgh4nh2f4nyvg74pb763qpis5cvgmqs7")))

(define-public crate-network-types-0.0.5 (c (n "network-types") (v "0.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fnb64xbhcc5jcl176ai5lvjaaiphz7s65zmi0q7ylq2iys54ygh") (f (quote (("std"))))))

