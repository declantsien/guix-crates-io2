(define-module (crates-io ne tw network_connectivity) #:use-module (crates-io))

(define-public crate-network_connectivity-1.0.0 (c (n "network_connectivity") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rtnetlink") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("time"))) (d #t) (k 2)))) (h "142rypp0gp4l0xwhbxhwjhf0ag1hzckc753sh1l9dkmpr5s261qk") (r "1.63.0")))

