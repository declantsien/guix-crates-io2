(define-module (crates-io qq mu qqmusic-rs) #:use-module (crates-io))

(define-public crate-qqmusic-rs-0.1.0 (c (n "qqmusic-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0hgkjcc4i7lgm3r2yfp89rn1mq7d8n9lcw90b9gsg8fgayrqqsf7")))

