(define-module (crates-io qq x- qqx-macro) #:use-module (crates-io))

(define-public crate-qqx-macro-0.0.1 (c (n "qqx-macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ywc33vzxmq5yr14wamwdb47ab2d72izfgyz1wdmdzg8l8rwrdyz")))

(define-public crate-qqx-macro-0.0.2 (c (n "qqx-macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1qv8igvc53xbk06d758100i1rznbwwrrvy1db0n6s9nf5x3dk641")))

(define-public crate-qqx-macro-0.0.3 (c (n "qqx-macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0szbkjds67mx9x4lnln6ic8gjdrs57mk1n3dd4rdcidr7zvhk221")))

