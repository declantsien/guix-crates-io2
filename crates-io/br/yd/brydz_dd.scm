(define-module (crates-io br yd brydz_dd) #:use-module (crates-io))

(define-public crate-brydz_dd-0.1.0 (c (n "brydz_dd") (v "0.1.0") (d (list (d (n "brydz_core") (r "^0.1.1") (f (quote ("amfiteatr"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "1vhk9i42xzz7lmcvqcbyl2zz565p4xwl54gfym7mccj9hmsir02j")))

(define-public crate-brydz_dd-0.2.0 (c (n "brydz_dd") (v "0.2.0") (d (list (d (n "brydz_core") (r "^0.2.0") (f (quote ("amfiteatr"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "0j95603l55alpf39521vh2ma6k8bw2klbiiq9jd0hxjmqv9935n9")))

(define-public crate-brydz_dd-0.3.0 (c (n "brydz_dd") (v "0.3.0") (d (list (d (n "brydz_core") (r "^0.3.0") (f (quote ("amfiteatr"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "03cynmswdfs1lvzfy7wcmryvqbwb6dsw3nl7nmd0ikdr8vkcy6c9")))

(define-public crate-brydz_dd-0.5.0 (c (n "brydz_dd") (v "0.5.0") (d (list (d (n "brydz_core") (r "^0.5.0") (f (quote ("amfiteatr"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1zvwp5hklzv9kpzppjwn3ly7bkx2sd0bwza1iy4cd5n3xazv47hb")))

