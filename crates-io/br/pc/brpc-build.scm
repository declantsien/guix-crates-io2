(define-module (crates-io br pc brpc-build) #:use-module (crates-io))

(define-public crate-brpc-build-0.1.0-alpha (c (n "brpc-build") (v "0.1.0-alpha") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 0)))) (h "0wcsk61hw0dyxf0blhqz7d0wmxax8jgx6lc58k5mzp5jxsdhamx0") (y #t)))

(define-public crate-brpc-build-0.1.0 (c (n "brpc-build") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 0)))) (h "0dk1vkcpfgc4a6721sn6ji8bczj6dzvkac1sr01p65s1p27xxvym")))

