(define-module (crates-io br pc brpc-rs) #:use-module (crates-io))

(define-public crate-brpc-rs-0.1.0-alpha (c (n "brpc-rs") (v "0.1.0-alpha") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "02fwaycfjmkmbgwygrxkzspw56x1px6h13m1v209s3f2napak60w") (y #t)))

(define-public crate-brpc-rs-0.1.0 (c (n "brpc-rs") (v "0.1.0") (d (list (d (n "brpc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "0kfkm6mj4cs5vkha9v8fj71k5sx9mxxg4xahkdl39r0ly6f3m8sg")))

