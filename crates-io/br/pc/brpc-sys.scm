(define-module (crates-io br pc brpc-sys) #:use-module (crates-io))

(define-public crate-brpc-sys-0.1.0-alpha (c (n "brpc-sys") (v "0.1.0-alpha") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kcpn0m9dprjwsifnp3m58lq7y85dhlm74xs9mmna5hclvk5nraq") (y #t)))

(define-public crate-brpc-sys-0.1.0 (c (n "brpc-sys") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "cc") (r "^1.0.38") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16dsrmv4hn9ig866b5jpaa11v7xbxfnfwslf7izw5fsx1mmb5a71")))

