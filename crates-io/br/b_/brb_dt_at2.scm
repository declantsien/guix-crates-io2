(define-module (crates-io br b_ brb_dt_at2) #:use-module (crates-io))

(define-public crate-brb_dt_at2-1.0.1 (c (n "brb_dt_at2") (v "1.0.1") (d (list (d (n "brb") (r "^1.0.1") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)))) (h "1mwxd9jiqzn0drw9q1i6pf2dshp5fw13fdsqww1gmkb5y1hif6qm")))

(define-public crate-brb_dt_at2-1.0.2 (c (n "brb_dt_at2") (v "1.0.2") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "024bkwxb7szimaydbrf8x4gk3hlbxzdfzxqq5rgrvq19sbhcaldz")))

(define-public crate-brb_dt_at2-1.0.3 (c (n "brb_dt_at2") (v "1.0.3") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0h41zn0qzxhlf3ib68vd07z6dkrkcq8qvw0mygvmk8xadmncgikp")))

(define-public crate-brb_dt_at2-1.0.4 (c (n "brb_dt_at2") (v "1.0.4") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02qfsmkw1sizy087mxkq2pjzg6wca59val2j2hrlfa50gbfccahk")))

(define-public crate-brb_dt_at2-1.0.5 (c (n "brb_dt_at2") (v "1.0.5") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0i1b42y73cjich913kjllfsq1ci6gr1pn5k4xsc7q3g850001bzl")))

(define-public crate-brb_dt_at2-1.0.6 (c (n "brb_dt_at2") (v "1.0.6") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0mrbbrn4h1w4rii5zwj3vwj8sswdiqkx2kb4yks4l6db4vsczw8d")))

(define-public crate-brb_dt_at2-1.0.7 (c (n "brb_dt_at2") (v "1.0.7") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1drj4zjxa17cvqgj1nqllcs0fj0yjbr4jsfdliar742x9374q772")))

(define-public crate-brb_dt_at2-1.0.8 (c (n "brb_dt_at2") (v "1.0.8") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "07p7hcgn83qkwp0w2b6645vdaymd0npm6879iammvzsw0cnbs87w")))

