(define-module (crates-io br b_ brb_dt_orswot) #:use-module (crates-io))

(define-public crate-brb_dt_orswot-1.0.1 (c (n "brb_dt_orswot") (v "1.0.1") (d (list (d (n "brb") (r "^1.0.1") (d #t) (k 0)) (d (n "crdts") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)))) (h "1wswbfk91vnh0f8l020yfhjijnqk9hvlxg6v54flas3p2ha2dv9g")))

(define-public crate-brb_dt_orswot-1.0.2 (c (n "brb_dt_orswot") (v "1.0.2") (d (list (d (n "brb") (r "^1.0.1") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01gxih3zwxf7v47q7946n30fmhxz54vbfsx9dkv3mv0nx8qrn0js")))

(define-public crate-brb_dt_orswot-1.0.3 (c (n "brb_dt_orswot") (v "1.0.3") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qiq31v35vzqlnnbx10clz87sap6a77yjk8ay6pakf4jvfrf36mg")))

(define-public crate-brb_dt_orswot-1.0.4 (c (n "brb_dt_orswot") (v "1.0.4") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05m7l8z2p02mv249kcwzqnr8vp0md15fp95aj22vpg5fp46n681j")))

(define-public crate-brb_dt_orswot-1.0.5 (c (n "brb_dt_orswot") (v "1.0.5") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09kmi8kag7hw5rv9sakxr5by9plqj5q5xiq9a289i001hqi4cmk1")))

(define-public crate-brb_dt_orswot-1.0.6 (c (n "brb_dt_orswot") (v "1.0.6") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xy4p1p79x0ji97gnyi7xg4mac7w1kmmdn74398w4pda6cr5wmpw")))

(define-public crate-brb_dt_orswot-1.0.7 (c (n "brb_dt_orswot") (v "1.0.7") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ql4yd6zsdhgb6wnfrxp59p24abph86hpiqiq2iqlw0y2hlvcj5q")))

(define-public crate-brb_dt_orswot-1.0.8 (c (n "brb_dt_orswot") (v "1.0.8") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04gkv6ham2w6qpbvivzzqjd8crinm4cq9g9ym9rxl0dfn8i094gf")))

(define-public crate-brb_dt_orswot-1.0.9 (c (n "brb_dt_orswot") (v "1.0.9") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdts") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04m6vcjzn82r40ykgaxzv3z90p44x4m4y2k4v6fl7mk3vj9zrj7w")))

