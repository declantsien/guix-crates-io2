(define-module (crates-io br b_ brb_dt_tree) #:use-module (crates-io))

(define-public crate-brb_dt_tree-1.0.1 (c (n "brb_dt_tree") (v "1.0.1") (d (list (d (n "brb") (r "^1.0.1") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)))) (h "0r85mi573p4y2wn27267arwbh90ygsfd4msjqa1mdi14fk24l24q")))

(define-public crate-brb_dt_tree-1.0.2 (c (n "brb_dt_tree") (v "1.0.2") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1sw46anybpc9gqq96nhs0vp14nk3m7p15xy4n4kcadz0g6sf0vg3")))

(define-public crate-brb_dt_tree-1.0.3 (c (n "brb_dt_tree") (v "1.0.3") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "194dawiz4whn17jnnc94hsv5dfmgi8gmy6hjhalcnq9wyv9gmrd9")))

(define-public crate-brb_dt_tree-1.0.4 (c (n "brb_dt_tree") (v "1.0.4") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "042m2a10wfyxim1r5wwr4s40nc488mik474zfanwbbf0nds5lj8v")))

(define-public crate-brb_dt_tree-1.0.5 (c (n "brb_dt_tree") (v "1.0.5") (d (list (d (n "brb") (r "^1.0.2") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0p3ak10vg7vzzhgcmrz6fcmhkxqw4dg4yx6g96x7ppvrzjrysn2g")))

(define-public crate-brb_dt_tree-1.1.0 (c (n "brb_dt_tree") (v "1.1.0") (d (list (d (n "brb") (r "^1.0.4") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1kabr5l0ywqgzl4qbsywwcc3yk0r7kyq1gmads1vmy3lwjz02pbk")))

(define-public crate-brb_dt_tree-1.1.1 (c (n "brb_dt_tree") (v "1.1.1") (d (list (d (n "brb") (r "^1.0.4") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1fj53yvgwkxd088r5bvrivqg561iin7m69xkvq7m5004kp7azgxd")))

(define-public crate-brb_dt_tree-1.1.2 (c (n "brb_dt_tree") (v "1.1.2") (d (list (d (n "brb") (r "^1.0.4") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0wgyj3la8fawm5ny4rzgjp4xv6p09g1bmly4xgpzgv4c85lflqsr")))

(define-public crate-brb_dt_tree-1.1.3 (c (n "brb_dt_tree") (v "1.1.3") (d (list (d (n "brb") (r "^1.0.4") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0vak3whkgiz2747ldwklbxmjhilndxljrx1gqq0d5d090p2jijgr")))

(define-public crate-brb_dt_tree-1.1.4 (c (n "brb_dt_tree") (v "1.1.4") (d (list (d (n "brb") (r "^1.0.4") (d #t) (k 0)) (d (n "crdt_tree") (r "^0.0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0cy4vwfksv5x17pkyvcsmd9lapm2bi4prnaz8yddajbxfqrz5nh6")))

