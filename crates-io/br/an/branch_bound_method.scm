(define-module (crates-io br an branch_bound_method) #:use-module (crates-io))

(define-public crate-branch_bound_method-0.1.0 (c (n "branch_bound_method") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "simplex_method") (r "^0.1.1") (d #t) (k 0)))) (h "07n9kjxzsjhwiaz1l4lphb7vp94ljjijbsrbmqfb6vwalhqzc7hs")))

(define-public crate-branch_bound_method-0.1.1 (c (n "branch_bound_method") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "simplex_method") (r "^0.1.3") (d #t) (k 0)))) (h "1f47rxsmz5mf4x36i2klfxjxhlwhrnl23f4wsyii353bnni89xb6")))

