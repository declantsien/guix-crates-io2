(define-module (crates-io br an branchy) #:use-module (crates-io))

(define-public crate-branchy-0.1.0 (c (n "branchy") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1sm8mnp8m9030ndc4cnv97x0q7b6zqc0jqdsh63bfgp7b2gqj9mk")))

(define-public crate-branchy-0.1.1 (c (n "branchy") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0pi2hq9ws1ksry1rbd91lr83mf6pyzb9598mf4hx382wy0zbxqn5")))

(define-public crate-branchy-0.2.0 (c (n "branchy") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "08yxy3p8xy6y9f9p6jziwz3y334zqxi32zwl7l7afrgcjm7n04mw")))

(define-public crate-branchy-0.2.1 (c (n "branchy") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0k5wjcwa2qmwv02ga31rs0bmj2xcysy4zqqcc7vx041kxall0gpy")))

