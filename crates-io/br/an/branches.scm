(define-module (crates-io br an branches) #:use-module (crates-io))

(define-public crate-branches-0.1.0 (c (n "branches") (v "0.1.0") (h "0l1pqj2wvmbanyphdrsrv65p4prar1s7gdvvg7yl29miyvbn1yi2")))

(define-public crate-branches-0.1.1 (c (n "branches") (v "0.1.1") (h "13f9grgxiwbwfpr06nhmb2zhij3b681x19vqm090gfs40px0dmbh")))

(define-public crate-branches-0.1.2 (c (n "branches") (v "0.1.2") (h "003h77ipbd3p00m6m2xn7kjs85vzgxz17l9aj4g6y360j573q5ml")))

(define-public crate-branches-0.1.3 (c (n "branches") (v "0.1.3") (h "0alxmzmms4s8vv1v94y2kdq49a4p7728fgkpxx36z2m092bznn3r")))

