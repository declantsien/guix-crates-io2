(define-module (crates-io br an branch-d) #:use-module (crates-io))

(define-public crate-branch-d-0.1.1 (c (n "branch-d") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "0fx594c2gs6k2mgnqafiy6mg4znrjcg03wf8y2sdcs367gs45nwq")))

(define-public crate-branch-d-0.2.0 (c (n "branch-d") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "1wp1p21z2q8kgrk2z4s0mj5mf917vb33sp8aryhjmaldn7j9s5qb")))

