(define-module (crates-io br an branchless) #:use-module (crates-io))

(define-public crate-branchless-0.1.0 (c (n "branchless") (v "0.1.0") (h "1fax8af69rjd0h9gf8bg3lf9g95qirwzc1zkxyj30dmcqdc4n7q2")))

(define-public crate-branchless-0.1.1 (c (n "branchless") (v "0.1.1") (d (list (d (n "branchless_core") (r "^0.1") (d #t) (k 0)))) (h "0d6wwgzn46xc76wrxqqkcsa049miqryxf2slx3pzs2xrkknbs7bc")))

