(define-module (crates-io br an branch_hints) #:use-module (crates-io))

(define-public crate-branch_hints-0.1.0 (c (n "branch_hints") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0129rcrv4qrk6625zwhs41g1llk5rgvyl9j3g0qwv1xynixp7hw9") (y #t)))

(define-public crate-branch_hints-0.1.1 (c (n "branch_hints") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1f4dayhmk7gbq85fbz2ad75lf6m61avjr5nzhf140r0qp7k5sz9g") (y #t)))

(define-public crate-branch_hints-0.1.2 (c (n "branch_hints") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vjbky6lq2jhs79gd2y8myh4ar19zqbl2qcrl9gi0b8y3rvab4cc") (y #t)))

(define-public crate-branch_hints-0.2.0 (c (n "branch_hints") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0fa0drp3p85w6s4qam52a942sh16bnkp7cffqdq2cfa4x5cr45ch")))

(define-public crate-branch_hints-0.2.1 (c (n "branch_hints") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10dvvsglm5jlarzlyii8pfk1fv3x1sf9145ig6i9y56002mw12mc")))

(define-public crate-branch_hints-0.3.0 (c (n "branch_hints") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1j46r0v514c0vh2aj3xsqp8p5gc60w9z7s02djqyxrdai0wgmrcl")))

(define-public crate-branch_hints-0.3.1 (c (n "branch_hints") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1wrsvz7hw40vcms57mh1zlgf6z62s4yr1gfkkiijdmhz1dkvjwn0")))

(define-public crate-branch_hints-0.4.0 (c (n "branch_hints") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ds5s7fls51p1572fdjypd7lw70m5b4dhmkq15w8scihm3vh252c")))

