(define-module (crates-io br an branchout) #:use-module (crates-io))

(define-public crate-branchout-0.1.0 (c (n "branchout") (v "0.1.0") (h "0al5hd870s8xzjnlqp156frlqgp8ipi90jskd7dx0xlvwr83d61c")))

(define-public crate-branchout-0.1.1 (c (n "branchout") (v "0.1.1") (h "15dnsq1bsxf9n09ja6815jab733q0mynrca326cdgj2ck2kjwhca")))

(define-public crate-branchout-0.1.2 (c (n "branchout") (v "0.1.2") (h "0wlngngkkii39gm9jvcfk2baqdgzmgh57gqyzid20lz0m80p3a5f")))

