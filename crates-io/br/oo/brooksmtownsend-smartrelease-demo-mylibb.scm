(define-module (crates-io br oo brooksmtownsend-smartrelease-demo-mylibb) #:use-module (crates-io))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibb-0.1.0 (c (n "brooksmtownsend-smartrelease-demo-mylibb") (v "0.1.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibc") (r "^0.1.0") (d #t) (k 0)))) (h "069jc8d8cyl2h89af72y088laa239pl6fin0074xlvd47d7ls7l7")))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibb-0.1.1 (c (n "brooksmtownsend-smartrelease-demo-mylibb") (v "0.1.1") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibc") (r "^0.1.1") (d #t) (k 0)))) (h "1mc621cj2i1mr5kii9ira58cbgilzvhxwizqifjq7d7akp2mh3r2")))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibb-0.2.0 (c (n "brooksmtownsend-smartrelease-demo-mylibb") (v "0.2.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibc") (r "^0.2.0") (d #t) (k 0)))) (h "0bmin30wqrzapsr9wfmc8r1zc4p6cfm25m08sighljnhzlk5sp0b")))

