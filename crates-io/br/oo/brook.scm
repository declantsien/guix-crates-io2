(define-module (crates-io br oo brook) #:use-module (crates-io))

(define-public crate-brook-0.1.0 (c (n "brook") (v "0.1.0") (h "1ksga19ycli5ykf3a1khh5ndai18q3xz9zcdiwxlg8dv8x2frd3m")))

(define-public crate-brook-0.1.1 (c (n "brook") (v "0.1.1") (h "14y0iqf7z30sgjh9v6wzz5j4blqrdxyzkbv69hmrfjdjfr1vg2fh")))

