(define-module (crates-io br oo brooksmtownsend-smartrelease-demo-mybin) #:use-module (crates-io))

(define-public crate-brooksmtownsend-smartrelease-demo-mybin-0.1.0 (c (n "brooksmtownsend-smartrelease-demo-mybin") (v "0.1.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-myliba") (r "^0.1.0") (d #t) (k 0)) (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.1.0") (d #t) (k 0)))) (h "0lnzwgqwbsfv17kksfdws1zmhrdmg682jdhl0xgr2lrz4mqkh5yq")))

(define-public crate-brooksmtownsend-smartrelease-demo-mybin-0.2.0 (c (n "brooksmtownsend-smartrelease-demo-mybin") (v "0.2.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-myliba") (r "^0.2.0") (d #t) (k 0)) (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.1.1") (d #t) (k 0)))) (h "0r714fc2ijn8w7vzhvm8zm9jgxmxhvkvvg5924y8l9fn1b4q9pka")))

(define-public crate-brooksmtownsend-smartrelease-demo-mybin-0.3.0 (c (n "brooksmtownsend-smartrelease-demo-mybin") (v "0.3.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-myliba") (r "^0.3.0") (d #t) (k 0)) (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.2.0") (d #t) (k 0)))) (h "1asigwc8sr79qpsq0cw9lmzxifyfry3952bmdv8vc8px3iy4wwv7")))

