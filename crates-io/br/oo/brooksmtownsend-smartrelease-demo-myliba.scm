(define-module (crates-io br oo brooksmtownsend-smartrelease-demo-myliba) #:use-module (crates-io))

(define-public crate-brooksmtownsend-smartrelease-demo-myliba-0.1.0 (c (n "brooksmtownsend-smartrelease-demo-myliba") (v "0.1.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.1.0") (d #t) (k 0)))) (h "0p0z05lnpfyfmd3mqljkxnzxhmnihlfaab181kp1p2nx9yv9nh7g")))

(define-public crate-brooksmtownsend-smartrelease-demo-myliba-0.2.0 (c (n "brooksmtownsend-smartrelease-demo-myliba") (v "0.2.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.1.1") (d #t) (k 0)))) (h "0x7gx6zxwpma3ganrhqy6s197i70gjpy6m76d0qps97ccswdchhh")))

(define-public crate-brooksmtownsend-smartrelease-demo-myliba-0.3.0 (c (n "brooksmtownsend-smartrelease-demo-myliba") (v "0.3.0") (d (list (d (n "brooksmtownsend-smartrelease-demo-mylibb") (r "^0.2.0") (d #t) (k 0)))) (h "079pf1fnxkgjss9vysfnxr6y0az4kmcac2mq7npdc2bncfg59s23")))

