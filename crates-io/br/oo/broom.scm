(define-module (crates-io br oo broom) #:use-module (crates-io))

(define-public crate-broom-0.1.0 (c (n "broom") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "158gzrzqbzbmwgv6h26j2zpik0xhhrp10ap7hrdjql3b24jxb025")))

(define-public crate-broom-0.1.1 (c (n "broom") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "00hqax73pwwij4xrpalna09f45mcj493m4nb64arysmms2270cww")))

(define-public crate-broom-0.1.2 (c (n "broom") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0kr8ii09gjwbr1sqqdc8crilhj6adwh3p4lzvnxz7k2ln5j42yn1")))

(define-public crate-broom-0.1.3 (c (n "broom") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1b1v1c7a8l7n6sdd5w0pscy9zgyryrhz5yrjvchh97659pjfcsj3")))

(define-public crate-broom-0.2.0 (c (n "broom") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1g12wbn0k08vx6d5zv2mfn60skblhwn0hs0yxw1kvhfs6s8da1ip")))

(define-public crate-broom-0.3.0 (c (n "broom") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0kdjlk5j1kyp7fqhq8r8h7xlhjpk977d8hrbggxmiiwdiybh99z4") (y #t)))

(define-public crate-broom-0.3.1 (c (n "broom") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0bymyy36wgpg17wgwyf2zvs9nhzlzm7m4p7b5qz7vdavxg7pr3w8")))

(define-public crate-broom-0.3.2 (c (n "broom") (v "0.3.2") (d (list (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0gs80mqpxn7gcc2cgb9fbkdw5sbk6f969mc6fq89zb37k5vb0l14")))

