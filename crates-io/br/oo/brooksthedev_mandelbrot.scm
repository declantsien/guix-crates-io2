(define-module (crates-io br oo brooksthedev_mandelbrot) #:use-module (crates-io))

(define-public crate-brooksthedev_mandelbrot-0.0.1 (c (n "brooksthedev_mandelbrot") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1abr0yifa08x5zzh2sb619pw8xph3ax2n29q832hfizl1xs2w4fm")))

