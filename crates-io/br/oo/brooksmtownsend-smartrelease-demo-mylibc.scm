(define-module (crates-io br oo brooksmtownsend-smartrelease-demo-mylibc) #:use-module (crates-io))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibc-0.1.0 (c (n "brooksmtownsend-smartrelease-demo-mylibc") (v "0.1.0") (h "0bp7dj244jq6k2dzrd9m9184p41vgji4wdwjr5gkxvm4bavl21il")))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibc-0.1.1 (c (n "brooksmtownsend-smartrelease-demo-mylibc") (v "0.1.1") (h "033lvnx2ww4cffndxs1bf7vjfs125lyz563d5dnl4v17anzj6gzm")))

(define-public crate-brooksmtownsend-smartrelease-demo-mylibc-0.2.0 (c (n "brooksmtownsend-smartrelease-demo-mylibc") (v "0.2.0") (h "1fqa7ilfw9cx5rrfg394inl7vdcq4g9g5jnwam3wlkpr5mpkm45a")))

