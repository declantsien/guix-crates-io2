(define-module (crates-io br -c br-cache) #:use-module (crates-io))

(define-public crate-br-cache-0.0.1 (c (n "br-cache") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.22.3") (d #t) (k 0)))) (h "1nfsa89b2q6gzm2ns2q24y086sv8j67pdc5zvghr4y2gw82k6m9s")))

(define-public crate-br-cache-0.0.2 (c (n "br-cache") (v "0.0.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.22.3") (d #t) (k 0)))) (h "1653i5l5pbvsdqnxqm54qnap7cg8rhk3ilhxa77r1rvsafsl5whq")))

(define-public crate-br-cache-0.0.3 (c (n "br-cache") (v "0.0.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "1a8qlf3q2rn66ndw395ld1ma5cwwfi2ngcvla4a9lzbjfdrwaf45")))

(define-public crate-br-cache-0.0.4 (c (n "br-cache") (v "0.0.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "0r6dxd8nc7z821c0ycndrspswn3bwghpxmp1raafn6af3f5xrzjf")))

(define-public crate-br-cache-0.0.5 (c (n "br-cache") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "0qg4nskfhk1hqkiq7nymc78nz2i20w920grz54k8sq13wkkjj0nj")))

(define-public crate-br-cache-0.0.6 (c (n "br-cache") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "12cf0jpvgwh98d2nsvh5vp2kw1nhx47abmhk5wglxvv2bbqg05qk")))

(define-public crate-br-cache-0.0.7 (c (n "br-cache") (v "0.0.7") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "1h4ib7lnwa8zny2x2zbaynjsizmvmfbwcxyv4k0j5xir6ijfg7b5")))

(define-public crate-br-cache-0.0.8 (c (n "br-cache") (v "0.0.8") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "1d2r9f4jagwzqzhy4ryidzxgzbzvwvafbj9zqmyi21cg9ybblnzr")))

