(define-module (crates-io br av bravia_api) #:use-module (crates-io))

(define-public crate-bravia_api-0.1.0 (c (n "bravia_api") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "test-util"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5") (d #t) (k 2)))) (h "0vxr3k2gs2x29hpl1rbwa9x5r3kx38djmgr89x9zwhj66m7prp8r")))

