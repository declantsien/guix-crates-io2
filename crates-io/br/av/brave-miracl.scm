(define-module (crates-io br av brave-miracl) #:use-module (crates-io))

(define-public crate-brave-miracl-0.1.0 (c (n "brave-miracl") (v "0.1.0") (h "1g80w07kvjfpc13nbipyz0pyrv99hbjz131w7qiyq3ll7aa8vlhq") (f (quote (("std")))) (y #t)))

(define-public crate-brave-miracl-0.1.1 (c (n "brave-miracl") (v "0.1.1") (h "1zappxsjgxrcm02n4k9c70khm0findznwvgzcm44hdw4g2h6pwcy") (f (quote (("std") ("default" "std"))))))

(define-public crate-brave-miracl-0.1.2 (c (n "brave-miracl") (v "0.1.2") (h "1czkd6ahzyq94kp1wrdny9z2q29nhshj6wbzaw9pgwwcial3zg12") (f (quote (("std") ("default" "std"))))))

