(define-module (crates-io br av bravery_router) #:use-module (crates-io))

(define-public crate-bravery_router-0.1.0 (c (n "bravery_router") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "route-recognizer") (r "0.*") (d #t) (k 2)))) (h "1c81fdm88l812is5p41lmn8wif0k0kr97dcp57mmxgy37i091qwh")))

(define-public crate-bravery_router-0.1.1 (c (n "bravery_router") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "route-recognizer") (r "0.*") (d #t) (k 2)))) (h "047ws4bcj0ch8v5xngsbp4bazp09j13wm2ja5i0c4sqpim4n2hs9")))

(define-public crate-bravery_router-0.1.2 (c (n "bravery_router") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "route-recognizer") (r "0.*") (d #t) (k 2)))) (h "0lqbhx5853xszvfaf8676jha1p8sr0m1xrl8ivc0ggxd2kwqhdky")))

(define-public crate-bravery_router-0.1.3 (c (n "bravery_router") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "route-recognizer") (r "0.*") (d #t) (k 2)))) (h "103aq0a9380p0g6z38ja9464ij5mmwbs6hb878cmh9w4y8izki1v")))

