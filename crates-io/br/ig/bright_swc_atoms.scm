(define-module (crates-io br ig bright_swc_atoms) #:use-module (crates-io))

(define-public crate-bright_swc_atoms-0.4.0 (c (n "bright_swc_atoms") (v "0.4.0") (d (list (d (n "bytecheck") (r "^0.6.9") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.4") (d #t) (k 0)) (d (n "string_cache_codegen") (r "^0.5.2") (d #t) (k 1)))) (h "1jsfjxibp4m2mfrw4zp14yzss63q9x6jj5hgy0yqpkxi0hc1nc9r") (f (quote (("rkyv-impl" "rkyv" "bytecheck"))))))

