(define-module (crates-io br ig bright_swc_css_ast) #:use-module (crates-io))

(define-public crate-bright_swc_css_ast-0.105.0 (c (n "bright_swc_css_ast") (v "0.105.0") (d (list (d (n "bytecheck") (r "^0.6.9") (o #t) (d #t) (k 0)) (d (n "is-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_enum") (r "^0.3.1") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.0") (d #t) (k 0)))) (h "1vyyfjrifpd4wv4zsigjvyswrm779acm1y0ghaz503hmapcw5zi1") (f (quote (("rkyv-impl" "rkyv" "bytecheck" "swc_atoms/rkyv-impl" "swc_common/rkyv-impl") ("default"))))))

