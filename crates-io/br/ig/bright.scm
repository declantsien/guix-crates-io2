(define-module (crates-io br ig bright) #:use-module (crates-io))

(define-public crate-bright-0.1.0 (c (n "bright") (v "0.1.0") (h "1j1h4xfscdfp5dzgd9pb30c3w78vc8vsnifqz97nmy3m1hsvh7cq")))

(define-public crate-bright-0.2.0 (c (n "bright") (v "0.2.0") (h "0kab6w8svgckka34nqwf1w2b8077sqwvdynnsz9y1whang1j94np")))

(define-public crate-bright-0.3.0 (c (n "bright") (v "0.3.0") (h "0n6vyd9zkvh2bx08xh0nlwkk2smyzwrdgnnq83amjzgzjw7xywkn")))

(define-public crate-bright-0.4.0 (c (n "bright") (v "0.4.0") (h "0fysvdhgz60bwk7a62pczlp77739h2ykva25gx21c6gkv1mqjk1f")))

(define-public crate-bright-0.4.1 (c (n "bright") (v "0.4.1") (h "0x271bnwjq50mqidqvrdbn1wspj8hmms2l0irqp19z8qbi42gsnd")))

