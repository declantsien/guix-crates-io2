(define-module (crates-io br ig brightness-ctl) #:use-module (crates-io))

(define-public crate-brightness-ctl-0.1.0 (c (n "brightness-ctl") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify-rust") (r "^4.10.0") (k 0)))) (h "178c382j167cmkhavb6pdswkyhhshkq1inras4xhfryrj9ngilhp") (f (quote (("zbus" "notify-rust/z") ("default" "dbus") ("dbus" "notify-rust/dbus"))))))

(define-public crate-brightness-ctl-0.1.1 (c (n "brightness-ctl") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify-rust") (r "^4.10.0") (k 0)))) (h "06hj26ci7pyicgrx0pm07yvyiiqq8kanr78i4ij6iwdlc85gd614") (f (quote (("zbus" "notify-rust/z") ("default" "dbus") ("dbus" "notify-rust/dbus"))))))

(define-public crate-brightness-ctl-0.1.2 (c (n "brightness-ctl") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify-rust") (r "^4.10.0") (k 0)))) (h "1axd049zqijcawfy115a1grzsp586m4mxxp3dy7xcnpmf1y00zwj") (f (quote (("zbus" "notify-rust/z") ("default" "dbus") ("dbus" "notify-rust/dbus"))))))

