(define-module (crates-io br ig brightness-windows) #:use-module (crates-io))

(define-public crate-brightness-windows-0.1.0 (c (n "brightness-windows") (v "0.1.0") (d (list (d (n "windows") (r "^0.18.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (t "cfg(windows)") (k 1)))) (h "1vwpid1gpwsxci5rw5qyzwp9yv6p23zlf9340948km3wvx9zrjfb")))

(define-public crate-brightness-windows-0.1.1 (c (n "brightness-windows") (v "0.1.1") (d (list (d (n "windows") (r "^0.18.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0rhbhpzilhlw4dal32q9042mpsc4a1xdbgsj8zip8mmfk6lxs7vl")))

