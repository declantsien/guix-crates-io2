(define-module (crates-io br ig brightcli) #:use-module (crates-io))

(define-public crate-brightcli-0.1.0 (c (n "brightcli") (v "0.1.0") (h "0fj9p7lhavq7br3gp08wl0yczvrq8yv6hf4zd5a5v8hzpck4l56d") (y #t)))

(define-public crate-brightcli-0.1.1 (c (n "brightcli") (v "0.1.1") (h "0rgkvq1wbsf8f2gx0yy25wyjcg3wwlsj61s0cgcaxm3ds5bajykr")))

(define-public crate-brightcli-0.1.2 (c (n "brightcli") (v "0.1.2") (h "1kh5ybvz009m3ls5c5vpybjgc8x9jlm6pyfq738qsz6y1lhm03c5")))

(define-public crate-brightcli-0.1.3 (c (n "brightcli") (v "0.1.3") (h "0h1lj6bgas7gkyi5yxmmdk6d9v7rz6rb0haz0k8w5a650384rx2a")))

(define-public crate-brightcli-0.1.4 (c (n "brightcli") (v "0.1.4") (h "04xbvbvsm6gl43ywcnss5rd7ma083vq38aiqs3y1dhdcmfcd155a")))

(define-public crate-brightcli-0.1.5 (c (n "brightcli") (v "0.1.5") (h "0hm5i6h9z23mxyr279w6219hkawfdd1vl6wdhk9h9xs68axb4j7r")))

(define-public crate-brightcli-0.1.6 (c (n "brightcli") (v "0.1.6") (h "15jl4qfjvl76khnbflfgwar8l4i2vrd06hcxqsw941jsz81wdfwh")))

(define-public crate-brightcli-0.1.7 (c (n "brightcli") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0avnfkr7im99m954apq0ada93zws9rzf9s1k2565lxy94g95xwxp")))

(define-public crate-brightcli-1.0.0 (c (n "brightcli") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1hkxvwz7mni1aksjgpk9yhwdgj1hyfcjlm8yqfn7g8c3gpbrsk8f")))

(define-public crate-brightcli-1.0.1 (c (n "brightcli") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "102xi3p22l4cdgz6d777g263q53vl7dc381wjwc5057xg6j6iijz")))

(define-public crate-brightcli-1.0.2 (c (n "brightcli") (v "1.0.2") (h "00wlr07svfvr1f9253z3m60jjsa5wlq6gvlkakmks3k4hj7i4fj0")))

