(define-module (crates-io br ug brug) #:use-module (crates-io))

(define-public crate-brug-0.1.0 (c (n "brug") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "brug-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0nlm2p2hrlrpqw3fvpzalj1ckywfpk2cpcmkxssx5shkm512b0ah") (f (quote (("macros" "brug-macros")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-brug-0.2.0 (c (n "brug") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "brug-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "12q8yv8sf0yr2qhl72plpq17alphvj5s6nb0qh2nd0ak2fy01159") (f (quote (("macros" "brug-macros")))) (y #t) (s 2) (e (quote (("tokio" "dep:tokio") ("kanal" "dep:kanal"))))))

(define-public crate-brug-0.2.1 (c (n "brug") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "brug-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0my04ghyhaxi6jwvqzqp0wnbjsay55hij6ljic4zh711wyiin89j") (f (quote (("macros" "brug-macros")))) (s 2) (e (quote (("tokio" "dep:tokio") ("kanal" "dep:kanal"))))))

(define-public crate-brug-0.2.2 (c (n "brug") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "brug-macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1gwl88a2gcsr9grkr33ldq5454kw8s5dqbfk8y50yky64bz2zbnh") (f (quote (("macros" "brug-macros")))) (s 2) (e (quote (("tokio" "dep:tokio") ("kanal" "dep:kanal"))))))

(define-public crate-brug-0.3.0-alpha0 (c (n "brug") (v "0.3.0-alpha0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "brug-macros") (r "^0.3.0-alpha0") (o #t) (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1rmfa2fmwwcs8r0kfy192mj9rdsfmixj6zka3qv7lph7kplqp7s1") (f (quote (("macros" "brug-macros") ("async_trait" "brug-macros/async_trait" "async-trait")))) (s 2) (e (quote (("tokio" "dep:tokio") ("kanal" "dep:kanal"))))))

