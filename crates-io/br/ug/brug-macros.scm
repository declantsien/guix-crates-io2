(define-module (crates-io br ug brug-macros) #:use-module (crates-io))

(define-public crate-brug-macros-0.1.0 (c (n "brug-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0absyf39762afihbz0lzbrlhk34vmg7mf8zkwgxrmhnqvvrf2kim")))

(define-public crate-brug-macros-0.1.1 (c (n "brug-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xmk4isp2r8p38q9pfghscadlq6as6n5n4zbbd2mznnx9j1jx02v")))

(define-public crate-brug-macros-0.3.0-alpha0 (c (n "brug-macros") (v "0.3.0-alpha0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kzi6szahbwl3vln40vdsd3hgdvdq2ccy6qg53m11511i1jwsvq4") (f (quote (("async_trait"))))))

