(define-module (crates-io br -f br-file) #:use-module (crates-io))

(define-public crate-br-file-0.0.1 (c (n "br-file") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "oxipng") (r "^8.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0bs8273wz1p55q8kppbbin79vam0v7p0qcgx83czzmbz549q6pzy")))

(define-public crate-br-file-0.0.3 (c (n "br-file") (v "0.0.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0mgri93mr08xpbaxhldc7y7ndc8p399l2nla123g8nsxxn4xhad0")))

