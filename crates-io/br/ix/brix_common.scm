(define-module (crates-io br ix brix_common) #:use-module (crates-io))

(define-public crate-brix_common-0.1.0 (c (n "brix_common") (v "0.1.0") (d (list (d (n "brix_cli") (r "^0.3.0") (d #t) (k 0)) (d (n "brix_processor") (r "^0.1.0") (d #t) (k 0)))) (h "04gshdyyj0pw6ci11ma5j14zdkxrv5rznrsdq86p5f82fnr4z8qh")))

(define-public crate-brix_common-0.1.1 (c (n "brix_common") (v "0.1.1") (d (list (d (n "brix_cli") (r "^0.3.3") (d #t) (k 0)) (d (n "brix_processor") (r "^0.1.1") (d #t) (k 0)))) (h "1i3vfb4fj4xf3c8al6fww0daasgcff81fx0jic03qnydaklszn9i")))

(define-public crate-brix_common-0.1.2 (c (n "brix_common") (v "0.1.2") (d (list (d (n "brix_cli") (r "^0.4") (d #t) (k 0)) (d (n "brix_processor") (r "^0.1.2") (d #t) (k 0)))) (h "0gkzvqmgm9pgjigb65yllqffky3rbwz3zgzi2bnwhk8xxcjhc0sk")))

(define-public crate-brix_common-0.1.3 (c (n "brix_common") (v "0.1.3") (d (list (d (n "brix_cli") (r "^0.4") (d #t) (k 0)) (d (n "brix_processor") (r "^0.1.3") (d #t) (k 0)))) (h "100q6bcn91jnkvdl3wag4c2j3j49qbpj916cq6pzj55kp8g2zj74")))

