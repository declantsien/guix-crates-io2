(define-module (crates-io br ix brix_errors) #:use-module (crates-io))

(define-public crate-brix_errors-0.1.0 (c (n "brix_errors") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "validator") (r "^0.14.0") (d #t) (k 0)))) (h "0b0h6ixj1kchk7lrq99zbz6gv1gi4140k07sd3qaswpc030c2z28")))

(define-public crate-brix_errors-0.1.1 (c (n "brix_errors") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "validator") (r "^0.14.0") (d #t) (k 0)))) (h "16q16g4rcgwc22yxxkyyqxwdkcqkvjzalnv73yh98lq1f4d6mlgi")))

(define-public crate-brix_errors-0.2.0 (c (n "brix_errors") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "validator") (r "^0.14.0") (d #t) (k 0)))) (h "0nkkhjccrg79s9bzlq7yjmlm5c1k461jx95b01j4rgpaq2kjk8kk")))

(define-public crate-brix_errors-0.2.1 (c (n "brix_errors") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "validator") (r "^0.14.0") (d #t) (k 0)))) (h "0qa43mnnhm0jk7m4nrwzplvjaz3ppsjfskyd2h1s450slssa7ans")))

