(define-module (crates-io br ea breakpad-handler) #:use-module (crates-io))

(define-public crate-breakpad-handler-0.1.0 (c (n "breakpad-handler") (v "0.1.0") (d (list (d (n "breakpad-sys") (r "^0.1.0") (d #t) (k 0)))) (h "053g2b2i13ah4bskfw73rgz9c2h45ndvzjgn4rn4bdmx77wcc867")))

(define-public crate-breakpad-handler-0.2.0 (c (n "breakpad-handler") (v "0.2.0") (d (list (d (n "breakpad-sys") (r "^0.2.0") (d #t) (k 0)))) (h "13lfm92ni25iapz14sqdkb55gwh4c1vkjmf0jxh0gyyfj4imzx82")))

