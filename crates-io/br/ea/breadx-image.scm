(define-module (crates-io br ea breadx-image) #:use-module (crates-io))

(define-public crate-breadx-image-0.1.0 (c (n "breadx-image") (v "0.1.0") (d (list (d (n "breadx") (r "^3") (k 0)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 2)))) (h "1xczdk8z7bnkvfhpkn5nrs1v3hxff1fwrid7b5mpmv4bscsx9kjj") (f (quote (("std" "breadx/std") ("default" "std") ("async" "breadx/async"))))))

