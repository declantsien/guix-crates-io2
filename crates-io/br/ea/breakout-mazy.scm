(define-module (crates-io br ea breakout-mazy) #:use-module (crates-io))

(define-public crate-breakout-mazy-0.1.0 (c (n "breakout-mazy") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "macroquad-text") (r "^0.1.1") (d #t) (k 0)))) (h "05qn27sa1v635ipfqvxphvhs34n3a4b1k1a1rd7mlhwbdpi1n66m") (y #t)))

(define-public crate-breakout-mazy-0.1.1 (c (n "breakout-mazy") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "macroquad-text") (r "^0.1.1") (d #t) (k 0)))) (h "1x9jfzvh8rcmvfk5xh3p9c777965cjljik6j865klrc3mdhwgvrm") (y #t)))

(define-public crate-breakout-mazy-0.1.2 (c (n "breakout-mazy") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "macroquad-text") (r "^0.1.1") (d #t) (k 0)))) (h "194nahkg7kwh8cbgkrni4fdkcwmgrwjsb387xfg4f9ni11yif5m6") (y #t)))

