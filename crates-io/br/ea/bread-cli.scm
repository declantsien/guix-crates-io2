(define-module (crates-io br ea bread-cli) #:use-module (crates-io))

(define-public crate-bread-cli-1.0.0 (c (n "bread-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "0iqca2bl9ibsvd7nhs7fplbss6sgspraklzh2k7vijnq6rm9xsg3") (f (quote (("benchmark")))) (y #t)))

(define-public crate-bread-cli-1.0.1 (c (n "bread-cli") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "1iaiqzx6vw4zkgaixiwvrhpj9s4n70gakigs64zqfv8ib825zyby") (f (quote (("benchmark"))))))

(define-public crate-bread-cli-1.1.0 (c (n "bread-cli") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "1wx3dy3zx5x5f84z0vs0fd75688y7ywqwmfdaxjls2gjv9n7mm1c") (f (quote (("benchmark"))))))

(define-public crate-bread-cli-2.0.0 (c (n "bread-cli") (v "2.0.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "1r61l1711bgsrf89ni8ajsiljkg8i9rwzdngnah71hdw9fwzgksh") (f (quote (("benchmark"))))))

