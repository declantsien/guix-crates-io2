(define-module (crates-io br ea breakout-wasm) #:use-module (crates-io))

(define-public crate-breakout-wasm-0.2.0 (c (n "breakout-wasm") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "02zi9ydrwbjqjc7kqq6ziksc1xdh1x3lc6icbwyvi29d3a5s8l0r")))

(define-public crate-breakout-wasm-0.2.1 (c (n "breakout-wasm") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "05ikrbmmnky96l3bv5rfd0kqmd0jp46g3w8sr282pzd2xw6bm3s1")))

(define-public crate-breakout-wasm-0.2.2 (c (n "breakout-wasm") (v "0.2.2") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0jl6lp643vx8sbr2qbyj8f0q2y7k7shm0kyfw09a2nhv578sx70s") (f (quote (("default" "console_error_panic_hook"))))))

