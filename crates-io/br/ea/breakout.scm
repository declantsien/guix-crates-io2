(define-module (crates-io br ea breakout) #:use-module (crates-io))

(define-public crate-breakout-0.1.0 (c (n "breakout") (v "0.1.0") (h "1xkq1fbdz1ma50b7p5vaclhkyd9a0yf8w4k01jv9fjvw9wbvp78s")))

(define-public crate-breakout-0.1.1 (c (n "breakout") (v "0.1.1") (h "194mhjxanqk7v4r2j8xmzar433hyv7rh730lya2vns17nla8955a")))

(define-public crate-breakout-0.2.0 (c (n "breakout") (v "0.2.0") (h "1i3ivnyma6vv32d0xd350npw07ciz1pnxr8499a3wiickbhiq03m")))

(define-public crate-breakout-0.2.1 (c (n "breakout") (v "0.2.1") (h "14r4rx76v5gzbrgmabzw8qlhpb32lpn8c8l1bihpa5s1wr229r1g")))

