(define-module (crates-io br ea breadx-special-events) #:use-module (crates-io))

(define-public crate-breadx-special-events-0.1.0 (c (n "breadx-special-events") (v "0.1.0") (d (list (d (n "breadx") (r "^3") (k 0)) (d (n "slab") (r "^0.4.6") (k 0)))) (h "01aq11516y66c9kidsw469ld5kpfp66lsrlfml7y9yzkf5h36biz") (f (quote (("default") ("async" "breadx/async"))))))

