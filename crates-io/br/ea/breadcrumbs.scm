(define-module (crates-io br ea breadcrumbs) #:use-module (crates-io))

(define-public crate-breadcrumbs-0.1.0 (c (n "breadcrumbs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0whxkn8nyrgh4wxyf3ny5abaq0q6b631xi639ww28cpaac9pjv3i")))

(define-public crate-breadcrumbs-0.1.1 (c (n "breadcrumbs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1a2dmwjcdmjzkxi4v65bg4g8kiq528hxhf1xpksj1lqn7k6d039j")))

(define-public crate-breadcrumbs-0.1.2 (c (n "breadcrumbs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1v8kanb1d6x8fqs2m0scgk3vd0jhwvxarb44m214hganynwak47f")))

(define-public crate-breadcrumbs-0.1.3 (c (n "breadcrumbs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0zx13g4qv0z1mqhph69cwx4gqqiziwbyr7ph6p7fr5pcjrkb4d19")))

(define-public crate-breadcrumbs-0.1.4 (c (n "breadcrumbs") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "15d07917g0s2gjwr7pph4kx06zzzk8dkxwh1spgwz3g1laps6i4l")))

(define-public crate-breadcrumbs-0.1.5 (c (n "breadcrumbs") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "07f982rf0hv1mi0h7vahggvh16aqa8vhvmvzalr0h0w9nw1mhr81")))

