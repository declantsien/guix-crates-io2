(define-module (crates-io br ea breadx-blocking) #:use-module (crates-io))

(define-public crate-breadx-blocking-0.1.0 (c (n "breadx-blocking") (v "0.1.0") (d (list (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "breadx") (r "^1") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "16wdrijvg0v434y0wfx1ma9kqijpl228r21711x3if4vy4czqz69") (f (quote (("tokio-support" "tokio" "breadx/tokio-support") ("default"))))))

