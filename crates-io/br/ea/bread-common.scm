(define-module (crates-io br ea bread-common) #:use-module (crates-io))

(define-public crate-bread-common-0.0.1 (c (n "bread-common") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "05k3n01d2bziywhlj2cr89zcnippasq2f3wk3v7bsqrx5crd7i0s")))

(define-public crate-bread-common-0.0.2 (c (n "bread-common") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c6wcg61p3wixsifgk0zc3q6rk7y7lpci44z0grgnp4ka918rcr9")))

(define-public crate-bread-common-0.0.3 (c (n "bread-common") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1jz8qqdnhjjpyzk75qrnw1c9754jr63a89rjksmhq9gw4jbnspab")))

