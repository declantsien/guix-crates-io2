(define-module (crates-io br ea breakdance) #:use-module (crates-io))

(define-public crate-breakdance-0.0.1 (c (n "breakdance") (v "0.0.1") (d (list (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbn2q7b627gilpbk5vg176i2c8j53183s508ma5yhg2bkmjdizb") (y #t)))

(define-public crate-breakdance-0.0.2 (c (n "breakdance") (v "0.0.2") (d (list (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0ni01jq3pvgnfi9178czkxaji0hpraqa8i99567q33j1wgbz4msk") (y #t)))

(define-public crate-breakdance-0.0.3 (c (n "breakdance") (v "0.0.3") (d (list (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgkb7w2w6h2mjvps514lcwwbp9pg1bi2dpl9kq2i5pnp9fxrivy") (y #t)))

