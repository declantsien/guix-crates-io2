(define-module (crates-io br ea breadth-first-zip-macros) #:use-module (crates-io))

(define-public crate-breadth-first-zip-macros-0.1.0 (c (n "breadth-first-zip-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.63") (d #t) (k 0)) (d (n "quote") (r ">=1.0.28") (d #t) (k 0)) (d (n "syn") (r ">=2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6fs5cybvx44bjlsnx61iig7qnmrb8lgfv2dmbi9x7s425lsny3")))

(define-public crate-breadth-first-zip-macros-0.2.0 (c (n "breadth-first-zip-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r ">=1.0.63") (d #t) (k 0)) (d (n "quote") (r ">=1.0.28") (d #t) (k 0)) (d (n "syn") (r ">=2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0blcslfl9hjk2qm0319k5bwqn3b8rsb6630lp0wdg298ymii9160")))

(define-public crate-breadth-first-zip-macros-0.2.1 (c (n "breadth-first-zip-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r ">=1.0.63") (d #t) (k 0)) (d (n "quote") (r ">=1.0.28") (d #t) (k 0)) (d (n "syn") (r ">=2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0igv3bz3wmp9zbxl2daqmjkylcix09mdiswvr4lsv4284953n0pm")))

(define-public crate-breadth-first-zip-macros-0.2.2 (c (n "breadth-first-zip-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r ">=1.0.63") (d #t) (k 0)) (d (n "quote") (r ">=1.0.28") (d #t) (k 0)) (d (n "syn") (r ">=2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1rpyr9i135a3c2l6xsr0r3n1qqnilz1bdhdgja71117w32i5fzcd")))

(define-public crate-breadth-first-zip-macros-0.3.0 (c (n "breadth-first-zip-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r ">=1.0.63") (d #t) (k 0)) (d (n "quote") (r ">=1.0.28") (d #t) (k 0)) (d (n "syn") (r ">=2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "16zvdxd3xciygkv213k0vlnvc88dkb3fix8syx4gl7scf0pz966m")))

