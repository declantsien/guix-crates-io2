(define-module (crates-io br ea break-block-macro) #:use-module (crates-io))

(define-public crate-break-block-macro-0.1.0 (c (n "break-block-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "someok") (r "^0.1.0") (d #t) (k 0)))) (h "1lsami6x6xyl1z03jkzasi6hkwvsxzljyyn8yk300dxd1vv6b6y1")))

