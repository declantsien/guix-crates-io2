(define-module (crates-io br ea break_infinity) #:use-module (crates-io))

(define-public crate-break_infinity-0.1.0 (c (n "break_infinity") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0sr6rxszxyjb4h34v9g534m75nrways8gcq2ndrijrapvfnyg4qz")))

(define-public crate-break_infinity-0.2.0 (c (n "break_infinity") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1rmn8hwbqvr3p7bvn53bjw8krwn96ndv9381dxn4f158vy3878q0")))

(define-public crate-break_infinity-0.2.1 (c (n "break_infinity") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0d4182ccqpyrj4xh3vi0pg8j7zbvffc60gdly1lj6rnd7n0w237b")))

(define-public crate-break_infinity-0.3.0 (c (n "break_infinity") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "131ybbxagq4p1gzixbby14ggk8b9sn7phdlq2bi0lgwyc9r6zk22")))

