(define-module (crates-io br ea breakpad-sys) #:use-module (crates-io))

(define-public crate-breakpad-sys-0.1.0 (c (n "breakpad-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0liw8hb52ynsr5sqs0alnmvjw37aii8wasd8r3kanyf656yb480x")))

(define-public crate-breakpad-sys-0.1.1 (c (n "breakpad-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1iz2vx9fbj3zppzz8m1nx2srqfi5hpn5m6zn5aq9slcq1zc4nzax")))

(define-public crate-breakpad-sys-0.2.0 (c (n "breakpad-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1zxjnr9gny4mx20yjnwbckf0xgy97nwrk8zhqfjrb736l1wf8vwg")))

