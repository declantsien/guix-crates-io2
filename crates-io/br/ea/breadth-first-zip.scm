(define-module (crates-io br ea breadth-first-zip) #:use-module (crates-io))

(define-public crate-breadth-first-zip-0.1.0 (c (n "breadth-first-zip") (v "0.1.0") (d (list (d (n "breadth-first-zip-macros") (r ">=0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 2)))) (h "050vdxk8p9vs4a4g16wffkqxfb7594p95h0q03m3v53pxm66y722")))

(define-public crate-breadth-first-zip-0.2.2 (c (n "breadth-first-zip") (v "0.2.2") (d (list (d (n "breadth-first-zip-macros") (r ">=0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 2)))) (h "1r2v77q92bz3x54b7q0lxykzzhpsn1q29jvbnhkppq2c5i3nsppr") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-breadth-first-zip-0.3.0 (c (n "breadth-first-zip") (v "0.3.0") (d (list (d (n "breadth-first-zip-macros") (r ">=0.3.0") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 2)) (d (n "reiterator") (r ">=0.1.3") (d #t) (k 0)))) (h "1yg9rldn70lf2d45ahpsngm7xwzj0h29lkai10khxd209x3a65af")))

