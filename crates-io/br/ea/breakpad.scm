(define-module (crates-io br ea breakpad) #:use-module (crates-io))

(define-public crate-breakpad-0.1.0 (c (n "breakpad") (v "0.1.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "050wj5iymwh6fx1hjmx7mb88qrq1kp4x96csc4n09g271mk1sr9z")))

