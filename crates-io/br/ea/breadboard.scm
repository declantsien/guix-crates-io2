(define-module (crates-io br ea breadboard) #:use-module (crates-io))

(define-public crate-breadboard-0.1.0 (c (n "breadboard") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "hyper") (r "^0.12.13") (k 0)) (d (n "hyper") (r "^0.12.13") (d #t) (k 2)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "17i3lf98n3fy331pp18qbwhms6g7y3wdwcnf44ld5ins1rbiklyw")))

