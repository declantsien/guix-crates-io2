(define-module (crates-io br ea breadx-keysyms) #:use-module (crates-io))

(define-public crate-breadx-keysyms-0.1.0 (c (n "breadx-keysyms") (v "0.1.0") (d (list (d (n "breadx") (r "^3") (k 0)))) (h "17q1zc7xia85bb3zg4i5zxy9mqznffbsfncyrwcaq0khrd6l6y9x") (f (quote (("default") ("async" "breadx/async"))))))

(define-public crate-breadx-keysyms-0.1.1 (c (n "breadx-keysyms") (v "0.1.1") (d (list (d (n "breadx") (r "^3") (k 0)))) (h "0diywn0azvm43c2hlxdxxaapnzp044lpj4zric7h35c05hhpvqm1") (f (quote (("default") ("async" "breadx/async"))))))

(define-public crate-breadx-keysyms-0.1.2 (c (n "breadx-keysyms") (v "0.1.2") (d (list (d (n "breadx") (r "^3") (k 0)))) (h "073fhg6vd8g3nxaddlk2hnasy5237yz0jfxm2gfl4fgd65a123ip") (f (quote (("default") ("async" "breadx/async"))))))

