(define-module (crates-io br ea break_array) #:use-module (crates-io))

(define-public crate-break_array-9001.0.0 (c (n "break_array") (v "9001.0.0") (h "1121n5ak9f5x2qdb7amiswc31mawfnhv9iqr0p5fgf6sdhfkw1b9")))

(define-public crate-break_array-9001.0.1 (c (n "break_array") (v "9001.0.1") (h "0zd1wb9bv8j909r96167q5ay1f4kyz7x700cw9hc4jd2r1nhvksv")))

