(define-module (crates-io br ea bread) #:use-module (crates-io))

(define-public crate-bread-0.0.1 (c (n "bread") (v "0.0.1") (h "1myg4cmj7i3qwr8z9zlfybk355pdy8qz5iglhnbjy1m621qmxszy")))

(define-public crate-bread-0.1.0 (c (n "bread") (v "0.1.0") (d (list (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1jkl8nylpz12yb3b88w82zg45x0rhz0hxrxqrziqv1ms2wajgizn")))

