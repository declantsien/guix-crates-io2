(define-module (crates-io br so brson-rs) #:use-module (crates-io))

(define-public crate-brson-rs-0.1.0 (c (n "brson-rs") (v "0.1.0") (d (list (d (n "brotli") (r "^3.4.0") (d #t) (k 0)) (d (n "bson") (r "^2.8.1") (d #t) (k 0)))) (h "1a7rw73jh3vif2hbskzw3pp54pq97rmxyi4fniiz56wfa6vp8qb8")))

