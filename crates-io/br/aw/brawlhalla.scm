(define-module (crates-io br aw brawlhalla) #:use-module (crates-io))

(define-public crate-brawlhalla-0.1.0 (c (n "brawlhalla") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shorthand") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01dg6qziazl2hn78n88rvyh570yy6zki9nnkmsghyc73s3p9dckv")))

(define-public crate-brawlhalla-0.1.1 (c (n "brawlhalla") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shorthand") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jk8whqrxvpckyvag2adqiafdh6mxv23b5rbvp5fhc9lhw9qzjal")))

(define-public crate-brawlhalla-0.1.2 (c (n "brawlhalla") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shorthand") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08z813412c4rj489jw37r11qz7j82bhg951mqlyalx5skw68767b")))

(define-public crate-brawlhalla-0.1.3 (c (n "brawlhalla") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shorthand") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05gpzmk99nyr2y17jpl0ny7gq418xsw21n3ksaa6qhf6riwqvi7g")))

(define-public crate-brawlhalla-0.1.4 (c (n "brawlhalla") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shorthand") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "020ri9j60ihw2isc3d8k41wipbprlvx93b3i6b5638i3s3vckzvd")))

