(define-module (crates-io br aw brawl_sun) #:use-module (crates-io))

(define-public crate-brawl_sun-0.3.0 (c (n "brawl_sun") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08nywkpdzm03zc9wxazrmkardq4fc01akn2s52s8kibxp7swzwk3")))

