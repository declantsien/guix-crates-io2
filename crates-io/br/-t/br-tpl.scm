(define-module (crates-io br -t br-tpl) #:use-module (crates-io))

(define-public crate-br-tpl-0.1.0 (c (n "br-tpl") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zd865q0il64gvv2gjyd75d6kwy4ysbylfmc0d1ap61976hhk5cg")))

