(define-module (crates-io br ew brewcalc) #:use-module (crates-io))

(define-public crate-brewcalc-0.1.0 (c (n "brewcalc") (v "0.1.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "1sjjxdvdv0r744cy2s3bgbnvrmlsi2vysq1sbxs8lmxc1mjwd7bp")))

(define-public crate-brewcalc-0.1.1 (c (n "brewcalc") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "0ggjqva13jg9rai5h4w1dv393d60rwdyzidxvd2igvci369g3vmi")))

(define-public crate-brewcalc-0.1.2 (c (n "brewcalc") (v "0.1.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "06yycywx8bz41gspdgg73b1k9h47afibph35vi50ij6xwjmgh0w6")))

