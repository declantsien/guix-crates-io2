(define-module (crates-io br ew brewstillery) #:use-module (crates-io))

(define-public crate-BrewStillery-1.0.0 (c (n "BrewStillery") (v "1.0.0") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0jwsmbrrrg2ycsphwwhwvv7m07pfb6alj43zpli2c25l3snmqjjh")))

(define-public crate-BrewStillery-1.0.1 (c (n "BrewStillery") (v "1.0.1") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "11r4ckiiar1iv2mihpad8idvykb2821ff73jn6yk2pb0k9xbr28b")))

(define-public crate-BrewStillery-1.1.0 (c (n "BrewStillery") (v "1.1.0") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1yc6gqmvdd1qzbx5nix14pf61x7vdj7rn45i0gv35ckr7555h660")))

(define-public crate-BrewStillery-2.0.0 (c (n "BrewStillery") (v "2.0.0") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0gaj6i495wjy5k77zb7nw38i2ksrxgq0hqhfl4sdgxja688i0a3m")))

(define-public crate-BrewStillery-2.0.1 (c (n "BrewStillery") (v "2.0.1") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "10f7358l1l7d3v9dwbvqr0xryql6nrahy9s9is3m78dzfl5q8d27")))

(define-public crate-BrewStillery-3.0.0 (c (n "BrewStillery") (v "3.0.0") (d (list (d (n "gtk") (r "^0.2.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0dw4sr0sg8df7nadqq5fd6987hvznpals02m0qrxdfz8i5w3n38n")))

(define-public crate-BrewStillery-5.0.0 (c (n "BrewStillery") (v "5.0.0") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "086bzj186da0ifz4gks337m8c2f4rsy7325d0z1vais79cxsqhjk") (f (quote (("default"))))))

(define-public crate-BrewStillery-5.1.0 (c (n "BrewStillery") (v "5.1.0") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0r1a46wjbz146ndhfizjq1lk1fdnfahwqkyvy3dgyklvacn2ag4d") (f (quote (("default"))))))

(define-public crate-BrewStillery-5.2.0 (c (n "BrewStillery") (v "5.2.0") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0wz5w230ki68y07m9xlgdy6pxyz3kagjrvysv6v714i4zja12ppp") (f (quote (("default"))))))

(define-public crate-BrewStillery-5.3.0 (c (n "BrewStillery") (v "5.3.0") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0yn30xw9vn0hh4mhibqbnscz5gair66lnkdw8ci6snpymdzihznf") (f (quote (("default"))))))

(define-public crate-BrewStillery-5.3.1 (c (n "BrewStillery") (v "5.3.1") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "06c99scimmvq013d3l0dwpwijrbbbxpvyxwilzm2wxpjj8wc8bhd") (f (quote (("default"))))))

(define-public crate-BrewStillery-6.0.0 (c (n "BrewStillery") (v "6.0.0") (d (list (d (n "gdk") (r "^0.7.0") (d #t) (k 0)) (d (n "gio") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0cgfmalymzfbah71fsv9ccgyw2xjha0iglmyf6vpd649bsi70m78") (f (quote (("default"))))))

(define-public crate-BrewStillery-6.0.1 (c (n "BrewStillery") (v "6.0.1") (d (list (d (n "clippy") (r "^0.0.189") (o #t) (d #t) (k 0)) (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gio") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0k30b4bxmdckd1ixf8b6j7f545r8b5i6p7g88lbd6rhjbadzjcjs") (f (quote (("default"))))))

(define-public crate-BrewStillery-6.0.2 (c (n "BrewStillery") (v "6.0.2") (d (list (d (n "clippy") (r "^0.0.189") (o #t) (d #t) (k 0)) (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gio") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "12pc79isfhzdhbq59b9hhfrv934rq4zscxhzlh5y9zwgxx5w48h6") (f (quote (("default"))))))

(define-public crate-BrewStillery-6.1.0 (c (n "BrewStillery") (v "6.1.0") (d (list (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gio") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.1") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "0i3qv4h18j1pssrcd7l9rf7h0aiggp77i9bvsrg0w758d7h8qkv5") (f (quote (("default"))))))

(define-public crate-BrewStillery-6.2.0 (c (n "BrewStillery") (v "6.2.0") (d (list (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gio") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.1") (f (quote ("v3_22"))) (d #t) (k 0)))) (h "1iy8zigjhvq8cg2h5hrlq9gj7crmd6dbp1w2l15s6hvpa3iqvrh2") (f (quote (("default"))))))

