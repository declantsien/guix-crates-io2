(define-module (crates-io br ew breweri) #:use-module (crates-io))

(define-public crate-breweri-1.0.0 (c (n "breweri") (v "1.0.0") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "atomic") (r "^0.6.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "compact_strings") (r "^4.0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("events"))) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("process" "rt-multi-thread" "time" "macros" "sync"))) (d #t) (k 0)) (d (n "tui") (r "^0.26.1") (f (quote ("crossterm"))) (d #t) (k 0) (p "ratatui")))) (h "0cslw5y0nzsjcks85x9qzs5nnk3b0pzjkrz7cg49b1n5l9187n51") (r "1.71.0")))

