(define-module (crates-io br ew brew) #:use-module (crates-io))

(define-public crate-brew-0.1.0 (c (n "brew") (v "0.1.0") (d (list (d (n "command-builder") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-rs") (r "^0.2") (f (quote ("serde" "lossy"))) (d #t) (k 0)))) (h "0b3rkbwbk7j0ff57hilvs7h2pbkari2xz5dhbapyghikqwlyk24b")))

(define-public crate-brew-0.2.0 (c (n "brew") (v "0.2.0") (d (list (d (n "command-builder") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-rs") (r "^0.2") (f (quote ("serde" "lossy"))) (d #t) (k 0)))) (h "0h7jh5ghds1h3a8jr1cby6k56gr9smxxg3ly24df2w1y7r18wn2l")))

