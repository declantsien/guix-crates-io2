(define-module (crates-io br ew brewr) #:use-module (crates-io))

(define-public crate-brewr-0.1.0 (c (n "brewr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)))) (h "0lj873gmj3sc2ixavrxwwd6pm6q7f1qcbjdy4x20k3f0px3i8d82") (r "1.70.0")))

(define-public crate-brewr-0.1.1 (c (n "brewr") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)))) (h "1npd466rnvy0m8yc9f3q28mcrrypjf9d0lj221m8b8j723bk4zan") (r "1.70.0")))

(define-public crate-brewr-0.1.2 (c (n "brewr") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)))) (h "15lx0w0zlbs4c7lha80qah7gdjk206m6izq7s2gp4w8m5mx7gyai") (r "1.70.0")))

