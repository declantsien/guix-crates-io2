(define-module (crates-io br ew brewmaster) #:use-module (crates-io))

(define-public crate-brewmaster-0.1.0 (c (n "brewmaster") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^0.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0j33gbax3901kw6dbrhgrxhr89g1gm53ckabfj0azrydfjn766sj")))

