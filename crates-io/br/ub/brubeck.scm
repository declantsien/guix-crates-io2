(define-module (crates-io br ub brubeck) #:use-module (crates-io))

(define-public crate-brubeck-0.0.1 (c (n "brubeck") (v "0.0.1") (h "14xxdpj530l0pdq89brgnjm64bykw9r21s5ng8fz6hpvcry39ggz")))

(define-public crate-brubeck-0.0.2 (c (n "brubeck") (v "0.0.2") (h "0c4lvjd8cwk6m5d8al9jf4iglcnk1ag4qwr743bz5ylcb9qkbnpa")))

(define-public crate-brubeck-0.0.3 (c (n "brubeck") (v "0.0.3") (h "07q7fp51rjdxvvj1h34cvm8xrmpqf2kn1lj0xr70jw72qmsx8a9b")))

(define-public crate-brubeck-0.0.4 (c (n "brubeck") (v "0.0.4") (h "1ywmq5ini2dpgkf6qc1l3rjajg58lxnnih6i6qljfjxml910rcaf")))

