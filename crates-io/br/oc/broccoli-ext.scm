(define-module (crates-io br oc broccoli-ext) #:use-module (crates-io))

(define-public crate-broccoli-ext-0.1.0 (c (n "broccoli-ext") (v "0.1.0") (d (list (d (n "broccoli") (r "^3.0") (k 0)))) (h "04r0zlkh2rq2jh8m36hfv44b201z0m07sxcdmjalpqzvp51p05ir") (y #t)))

(define-public crate-broccoli-ext-0.1.1 (c (n "broccoli-ext") (v "0.1.1") (d (list (d (n "broccoli") (r "^3.0") (k 0)))) (h "1p61hn3n3da7ck36b5faldag6rs6q8yrvwmrplysymfsw6r42hh3") (y #t)))

(define-public crate-broccoli-ext-0.1.2 (c (n "broccoli-ext") (v "0.1.2") (d (list (d (n "broccoli") (r "^3.2") (k 0)))) (h "19jrs24zq59j79limgj7pc1i0vm688n02kpdwqpp0adzgnpdsjxc") (y #t)))

(define-public crate-broccoli-ext-0.1.3 (c (n "broccoli-ext") (v "0.1.3") (d (list (d (n "broccoli") (r "^3.3") (k 0)))) (h "043p6x8k21sqhnw37w5mdq3kqa56pr31rjyi9yxixh544h165lfb") (y #t)))

(define-public crate-broccoli-ext-0.2.0 (c (n "broccoli-ext") (v "0.2.0") (d (list (d (n "broccoli") (r "^3.3") (k 0)))) (h "17hmz6dwsam1kz8y940hmc99sdijx5iv378jscfks09aybx6gsf2") (y #t)))

(define-public crate-broccoli-ext-0.3.0 (c (n "broccoli-ext") (v "0.3.0") (d (list (d (n "broccoli") (r "^3.4") (k 0)))) (h "08gsqfdi8ihg43saxv6rmqv1h8p457893wfnf1j4b5hl9qji0blk") (y #t)))

(define-public crate-broccoli-ext-0.4.0 (c (n "broccoli-ext") (v "0.4.0") (d (list (d (n "broccoli") (r "^3.5") (k 0)))) (h "0wii1acyhsaac5gl41jn35lhrx5zgrpl77gvnrb0s87bj53y9frc") (y #t)))

(define-public crate-broccoli-ext-0.5.0 (c (n "broccoli-ext") (v "0.5.0") (d (list (d (n "broccoli") (r "^3.6") (k 0)))) (h "09dr4rlfhn3pyxb30843qx2dvnfnnbfckxz97ll748x3pnp5y3y9") (y #t)))

(define-public crate-broccoli-ext-0.6.0 (c (n "broccoli-ext") (v "0.6.0") (d (list (d (n "broccoli") (r "^6.0") (d #t) (k 0)))) (h "07lby78zvl5pgalcd3xq1rcfll5lkmhm9lbw2zvmcdhjxy88dqfm")))

(define-public crate-broccoli-ext-0.6.1 (c (n "broccoli-ext") (v "0.6.1") (d (list (d (n "broccoli") (r "^6.0") (d #t) (k 0)))) (h "0pc8ck3jiivm66s0ddn8iqdyq8idffhkk06ggmmcgbvrhm3mbb8z")))

(define-public crate-broccoli-ext-0.6.2 (c (n "broccoli-ext") (v "0.6.2") (d (list (d (n "broccoli") (r "^6.0") (d #t) (k 0)))) (h "118xzn8gidhsygbs2j3gpddiail8l4zkxikpnqphf01wj47vkxzr")))

(define-public crate-broccoli-ext-0.7.2 (c (n "broccoli-ext") (v "0.7.2") (d (list (d (n "broccoli") (r "^6.1") (d #t) (k 0)))) (h "1a8pq7l7cc2cwmgrb8rdlir891qqlpgg83n7q5cwdcdpmmak4lk5")))

(define-public crate-broccoli-ext-0.8.0 (c (n "broccoli-ext") (v "0.8.0") (d (list (d (n "broccoli") (r "^6.2") (d #t) (k 0)))) (h "0n9i0xmg8czklm2d3w256nxb2y2m1pwxapimz2r1cqw89djzfk2m")))

