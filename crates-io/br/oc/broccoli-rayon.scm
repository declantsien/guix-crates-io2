(define-module (crates-io br oc broccoli-rayon) #:use-module (crates-io))

(define-public crate-broccoli-rayon-0.1.0 (c (n "broccoli-rayon") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "broccoli") (r "^6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "04jakl5g0k4cipj9wym26vjb6fin8hj4f2bcwxcy76rksgm4zi80")))

(define-public crate-broccoli-rayon-0.1.1 (c (n "broccoli-rayon") (v "0.1.1") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "broccoli") (r "^6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1a7pcym414sid5akkpjmk6sfi5skprn158f2wpq5v0ags3jyg4x0")))

(define-public crate-broccoli-rayon-0.1.2 (c (n "broccoli-rayon") (v "0.1.2") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "broccoli") (r "^6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1jk9s0zcf9cijad1k7rnhmkvshsdcmz7288371lh6fvbghlhg6ij")))

(define-public crate-broccoli-rayon-0.2.0 (c (n "broccoli-rayon") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "broccoli") (r "^6.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0nrddw5xh7d3la7886276gs30q9jvmz1458sl9ibq1b437mja4ss")))

(define-public crate-broccoli-rayon-0.3.0 (c (n "broccoli-rayon") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "broccoli") (r "^6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0ar8j383bgh33a6vghandzndpkysq6yfpxnsc6qm8qkpkmwmwxxm")))

(define-public crate-broccoli-rayon-0.4.0 (c (n "broccoli-rayon") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.13") (k 2)) (d (n "broccoli") (r "^6.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "154rf6q9zz8l6n27jwxpkv3n51658zhj8pninswwlq20hq5ava4z")))

