(define-module (crates-io br oc broccoli-tree) #:use-module (crates-io))

(define-public crate-broccoli-tree-0.1.0 (c (n "broccoli-tree") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1b7qx4v9b3vy08l3lc955pxcjjc5vpgzdad0x1da3gziss0xwsq0") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.1.1 (c (n "broccoli-tree") (v "0.1.1") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1raq7w3jr6bsmpwilzb6a64fgx7xx700s91hmizkn9cqxal7gv13") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.1.2 (c (n "broccoli-tree") (v "0.1.2") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1hn6lxqh3fsymsphjy9i0i172kibpkjvn9xjjyry15c771pvhczz") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.1.3 (c (n "broccoli-tree") (v "0.1.3") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1f15wlh9axgk0fp0w0kdh9x9lfv1z36hhp9r9lfsw64a690k412s") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.1.4 (c (n "broccoli-tree") (v "0.1.4") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0hzi5zv1b78qv040195hyp3q3bw231vxnm8hx2zm3hznv1cgg6ni") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.2.0 (c (n "broccoli-tree") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.10") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0i30kxsgaiqx8lq3nzyvlishp6hibl5hb0i0524wrg5k6jg39lqq") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.3.0 (c (n "broccoli-tree") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0jkqf35l8y30wzwnv7y1wz35zr65nln1c26sng88p64c3x2ircks") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.3.1 (c (n "broccoli-tree") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0c3rgv4sqv60z87pkjx630aixkmkxs3ji7y0z1w17v6bzbn9jp3p") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.4.0 (c (n "broccoli-tree") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1da7wfq671m2vvd3g8fkhkxrwr98ikrd3sm9idwa2mxq5amipp0b") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.4.1 (c (n "broccoli-tree") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1bjz44sp0qw9lg6bdd5wnl90zplza9rbvl8cmx0v6mcy911vx04b") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.4.2 (c (n "broccoli-tree") (v "0.4.2") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0v5f8mrgcjzrhwqzcvsdy8qhvbkd6qr9v5khvgsxpw31iannxk1z") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.5.0 (c (n "broccoli-tree") (v "0.5.0") (d (list (d (n "axgeom") (r "^1.11") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1x51v1qkxmd62fqns7y38pz0k3ra4qyn5dkp6djn73h3br9nh992") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.6.0 (c (n "broccoli-tree") (v "0.6.0") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "029cnsn1nlarnsh6x8rq671kwi14dpg55kizhkcjg415wnv7h4py") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-broccoli-tree-0.7.0 (c (n "broccoli-tree") (v "0.7.0") (d (list (d (n "axgeom") (r "^1.12") (k 0)) (d (n "compt") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1f6265628rx9zcm21d7pb5m3ml19rls01k82gx9w9hwqi9wybs0z") (f (quote (("default" "rayon")))) (y #t)))

