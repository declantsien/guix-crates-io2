(define-module (crates-io br co brcode) #:use-module (crates-io))

(define-public crate-brcode-0.1.0 (c (n "brcode") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0cm4p24hikg8kfqnzgp5dhs7nsqbm7b2s3wymx5x1wycv62c8ks8")))

(define-public crate-brcode-0.1.1 (c (n "brcode") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0jr50xgyapmx02gm33dblv7sivv484scm474l4v5m94d2zm7cw7f")))

(define-public crate-brcode-0.1.2 (c (n "brcode") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dr655jqrqf8f96hyy1s5yzzl4a4ijhk82qbjl1r295ih42scmw1")))

(define-public crate-brcode-0.2.0 (c (n "brcode") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xhlhk0kchkckkgqij57cv0g647wa1czbqzw2fd2r33y52z7nlsc")))

(define-public crate-brcode-1.0.0 (c (n "brcode") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07m5h6d3wr4n01if7ivw4j54s9nw022f859c4sgq4b2fx6qdgs0d")))

(define-public crate-brcode-1.0.1 (c (n "brcode") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xilnh43afk8pn3q3n05m3klgbn8wszm412sf9myv7crqz98virz")))

(define-public crate-brcode-1.1.0 (c (n "brcode") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1azqy3ifvi67jii8askz93vxk0rjsmj9k7g8dh0lhqpq0vsnwvhw")))

(define-public crate-brcode-1.1.1 (c (n "brcode") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15i04bab7mkxvg6yxcvsgscvjv40csclrr1wd8fha83hvzk1xqpf")))

(define-public crate-brcode-1.2.0 (c (n "brcode") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y6dk61bc4z259kwbd5ixscxcd346dlcjqciq3bg765ywznkm6zn")))

(define-public crate-brcode-1.3.0 (c (n "brcode") (v "1.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jiym6ffpdxq8cv47fzky7ykvhcmpx6szxzawl3byf42inz810yn")))

(define-public crate-brcode-1.3.1 (c (n "brcode") (v "1.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b3wsy7q0r76qgbjncw1gbsyjrknbp0ndfwy3jdi36wj9lpdh4wi")))

(define-public crate-brcode-1.4.0 (c (n "brcode") (v "1.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qm6zr4s7psccqix66r3s0fc669pf9i8zgym999cn1kvglb0zfkk")))

(define-public crate-brcode-1.4.1 (c (n "brcode") (v "1.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d3q2w16p78nkp4a3lcvb968kxixj01i02sig86nr80jq8vqbm5d")))

(define-public crate-brcode-1.4.2 (c (n "brcode") (v "1.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "edn-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "007y0bwdcnzxgsvp7ha4mdzli85np3yir4imz21dd1xsyc2qnmc8")))

