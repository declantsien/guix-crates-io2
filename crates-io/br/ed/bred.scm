(define-module (crates-io br ed bred) #:use-module (crates-io))

(define-public crate-bred-0.1.2 (c (n "bred") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n9dzdkfl1azqhbmvf8nfq0y5xifzqf4cayxl57p3xlvx5701xkw")))

(define-public crate-bred-0.2.1 (c (n "bred") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "04wq730mzr31w0lwrjhvp0wr1pa1c70fzya55jff0fjfrhzpms17")))

(define-public crate-bred-0.2.2 (c (n "bred") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fx9mlxhy83l56ipnqlwn0hhb70ffn7xzjqh7cnssbxnd00p7mhq")))

(define-public crate-bred-0.3.1 (c (n "bred") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iir3ixax0ck6b75gjfm3avbsf8g28p9jxqih82bv3dzdms8vxxq")))

(define-public crate-bred-0.3.2 (c (n "bred") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "15g55i7k5ryz5p01n1w4gmib5cbd8lx2940d9y7c1fc37xgvkq96")))

