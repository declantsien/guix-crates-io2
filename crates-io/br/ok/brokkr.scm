(define-module (crates-io br ok brokkr) #:use-module (crates-io))

(define-public crate-brokkr-0.1.0 (c (n "brokkr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "redis") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0dx1r91zhnhcincqlbgvyaicz9mrpn9srd6700apll9q5h5n6llr")))

(define-public crate-brokkr-0.1.1 (c (n "brokkr") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "redis") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lnhhdz0gbbayx5zmm5mc1m7nv3k5k5fab36p2ialacykp4mafij")))

