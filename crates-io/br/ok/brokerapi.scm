(define-module (crates-io br ok brokerapi) #:use-module (crates-io))

(define-public crate-brokerapi-0.1.0 (c (n "brokerapi") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 2)))) (h "0fshbib7pkzx1mx7fnfrwhaw8i1mammc87vzmgl8ifx9rbb908fz")))

