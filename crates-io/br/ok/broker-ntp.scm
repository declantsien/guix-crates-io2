(define-module (crates-io br ok broker-ntp) #:use-module (crates-io))

(define-public crate-broker-ntp-0.0.1 (c (n "broker-ntp") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.4") (d #t) (k 2)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "14j2lfdciv83zijgcn0lvipkdrh60vad6d1xv89h7qm4la7hyz83")))

