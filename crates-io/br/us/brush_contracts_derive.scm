(define-module (crates-io br us brush_contracts_derive) #:use-module (crates-io))

(define-public crate-brush_contracts_derive-1.8.0 (c (n "brush_contracts_derive") (v "1.8.0") (d (list (d (n "brush_lang") (r "^1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bsgc5jw0jhph9lvd1yv51sfaziwcchpdzkm4xc26xqj26qs45x7") (f (quote (("timelock_controller") ("std") ("reentrancy_guard") ("psp34") ("psp22") ("psp1155") ("proxy") ("payment_splitter") ("pausable") ("ownable") ("diamond") ("default" "std") ("access_control"))))))

