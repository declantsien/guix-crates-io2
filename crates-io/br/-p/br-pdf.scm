(define-module (crates-io br -p br-pdf) #:use-module (crates-io))

(define-public crate-br-pdf-0.0.1 (c (n "br-pdf") (v "0.0.1") (d (list (d (n "adobe-cmap-parser") (r "^0.3.3") (d #t) (k 0)) (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)) (d (n "postscript") (r "^0.17.4") (d #t) (k 0)) (d (n "type1-encoding-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1sr2a7zj1178dpigp5fbwq8v3dp3zh9wnnb0klgcq2jfbm7x9x1g")))

