(define-module (crates-io br ac bracket_ratatui) #:use-module (crates-io))

(define-public crate-bracket_ratatui-0.1.0 (c (n "bracket_ratatui") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bracket-lib") (r "^0.8.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "0dal3nbfb5315dkbj1810d8qshrp57bif8djpk7jf6gfpih4f08z")))

(define-public crate-bracket_ratatui-0.1.1 (c (n "bracket_ratatui") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bracket-lib") (r "^0.8.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "18mb9ql0f4n3z0nl4f13gly56vyy91qxv1iqcn9ganlz3fjyq7sq")))

(define-public crate-bracket_ratatui-0.1.2 (c (n "bracket_ratatui") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bracket-lib") (r "^0.8.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1ckbp1jpgxqmxcjgqhb5djzqb6d4qykhv3ah5d5j848lzfl076pk")))

(define-public crate-bracket_ratatui-0.1.3 (c (n "bracket_ratatui") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bracket-lib") (r "^0.8.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "096sgh4bpf94njxzk060l3fbd9lwyk2akbg8v10rdcwih08x73l8")))

