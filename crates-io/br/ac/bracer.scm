(define-module (crates-io br ac bracer) #:use-module (crates-io))

(define-public crate-bracer-0.0.0 (c (n "bracer") (v "0.0.0") (h "08fzj200m669m3b5vi74lg4icw5pczzw6l2l96c2bg3kvqmjpnzy")))

(define-public crate-bracer-0.1.0 (c (n "bracer") (v "0.1.0") (h "1zpgb4ialz3d070pm6i60fnk74ijn315vxjp30gycd10p7mdrbb1")))

(define-public crate-bracer-0.1.1 (c (n "bracer") (v "0.1.1") (h "1l8yb6yrj7g1b4h5vs5scjxr75gfbffm5gz5qa16h1c74k45k3qh") (y #t)))

(define-public crate-bracer-0.1.2 (c (n "bracer") (v "0.1.2") (h "1nkgpbb2in3sjhk695rwjmg1g2pyfd90klf2biph2iap0v1vhnb5")))

(define-public crate-bracer-0.2.0 (c (n "bracer") (v "0.2.0") (h "1fnr5arzssdw5h92inzfra5wldkaq6y4pmafaxbh5izvga2ri7v3")))

(define-public crate-bracer-0.2.1 (c (n "bracer") (v "0.2.1") (h "14yc8fg8xchbsywqbv4id44r153gr4w79papfbd45gz2wys77bsw")))

(define-public crate-bracer-0.3.0 (c (n "bracer") (v "0.3.0") (h "15qyjd5wqnzfpx486z24d6m4ww8cnbvilc95gc5j6xa8pfipbw4v")))

