(define-module (crates-io br ac brachiologo) #:use-module (crates-io))

(define-public crate-brachiologo-0.1.0 (c (n "brachiologo") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.2") (d #t) (k 0)))) (h "1vnp4sy7kaz1xcgqd5mwlr63dinlvj2hpm7pyznkmmiy7l6k5w83")))

(define-public crate-brachiologo-0.1.1 (c (n "brachiologo") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0iwi6bbl3ss151bgwdmicjbmqk22mb2gzzvq4xwj28wr12z4y7v5")))

