(define-module (crates-io br ac bracket-state-machine) #:use-module (crates-io))

(define-public crate-bracket-state-machine-0.1.0 (c (n "bracket-state-machine") (v "0.1.0") (d (list (d (n "bracket-terminal") (r "^0.8") (d #t) (k 0)))) (h "0y6dysw9zfm2ybxj53v1n9rl9g0nag0iswzb35z847kis29spq0l") (f (quote (("opengl" "bracket-terminal/opengl") ("default" "opengl") ("curses" "bracket-terminal/curses") ("cross_term" "bracket-terminal/cross_term"))))))

(define-public crate-bracket-state-machine-0.2.0 (c (n "bracket-state-machine") (v "0.2.0") (d (list (d (n "bracket-lib") (r "^0.8") (d #t) (k 0)))) (h "0b134ih65w9qdah35gmwldbanrrmvjy6cxvspww12mf110aszw59") (f (quote (("threaded" "bracket-lib/threaded") ("serde" "bracket-lib/serde") ("opengl" "bracket-lib/opengl") ("curses" "bracket-lib/curses") ("crssterm" "bracket-lib/crossterm"))))))

