(define-module (crates-io br ac brack-sdk-rs) #:use-module (crates-io))

(define-public crate-brack-sdk-rs-0.1.0 (c (n "brack-sdk-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "17a77r89ch1zpq32c2wkcmhj5psgzw1kddz461s5lw2zdizvhpjd")))

