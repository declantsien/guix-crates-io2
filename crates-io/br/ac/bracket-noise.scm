(define-module (crates-io br ac bracket-noise) #:use-module (crates-io))

(define-public crate-bracket-noise-0.1.0 (c (n "bracket-noise") (v "0.1.0") (d (list (d (n "bracket-color") (r "^0.1.0") (d #t) (k 2)) (d (n "bracket-random") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 2)))) (h "01kf6sp4q21lpzpwz86q3lbqqwdlw1qf5ik1n45l9k5l13a2q31p")))

(define-public crate-bracket-noise-0.7.0 (c (n "bracket-noise") (v "0.7.0") (d (list (d (n "bracket-color") (r "^0.7.0") (d #t) (k 2)) (d (n "bracket-random") (r "^0.7.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 2)))) (h "1dxlgaq7r6jn5zbyq85a6i2smwpgqv9i8v54jy5xlw9hz4i3msvk")))

(define-public crate-bracket-noise-0.8.0 (c (n "bracket-noise") (v "0.8.0") (d (list (d (n "bracket-random") (r "^0.8.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.2") (d #t) (k 2)))) (h "1z0ljmnbl26v816nwhwal9d60vzdl02qpnif18gq3pb3ih522krb")))

(define-public crate-bracket-noise-0.8.1 (c (n "bracket-noise") (v "0.8.1") (d (list (d (n "bracket-color") (r "~0.8.1") (f (quote ("rex" "palette"))) (d #t) (k 2)) (d (n "bracket-random") (r "~0.8.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 2)))) (h "07v1kyrv8nwd0cwnpkc8scz338lypqm90cns87aiwpljrxdj7cgn")))

(define-public crate-bracket-noise-0.8.2 (c (n "bracket-noise") (v "0.8.2") (d (list (d (n "bracket-color") (r "~0.8.2") (f (quote ("rex" "palette"))) (d #t) (k 2)) (d (n "bracket-random") (r "~0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "~0.19") (d #t) (k 2)))) (h "08v7nh4vxgl9y3kpvxh6b6gndw1pz36gcv3acrqcfn56yrj9bpk0")))

(define-public crate-bracket-noise-0.8.7 (c (n "bracket-noise") (v "0.8.7") (d (list (d (n "bracket-color") (r "~0.8") (f (quote ("palette"))) (d #t) (k 2)) (d (n "bracket-random") (r "~0.8") (d #t) (k 0)) (d (n "crossterm") (r "~0.25") (d #t) (k 2)))) (h "1p88icndmyxz6bwyb60nj45c516n040nydrzm2vrvilh14yl9dzh")))

