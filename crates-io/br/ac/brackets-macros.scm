(define-module (crates-io br ac brackets-macros) #:use-module (crates-io))

(define-public crate-brackets-macros-0.1.3 (c (n "brackets-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1xn4vm6k1y1mda3v3wwkh3y0zwpzjywwdldf0yx72hc59bdplwxd")))

(define-public crate-brackets-macros-0.1.4 (c (n "brackets-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "04h3jhysbr54rwyl2x3hs3kmhx50b9jajlp4p9gicbihvis17nk3")))

(define-public crate-brackets-macros-0.1.5 (c (n "brackets-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0w753gcw3k3v9nq7gpp7yarz83rxkpqvq16qkd2m2dkrasvm20vj")))

(define-public crate-brackets-macros-0.1.6 (c (n "brackets-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3fwkb12a3ggb5c9hnf6zmp8g6ij2rhisf8zssrwi6h19ii1s0p")))

(define-public crate-brackets-macros-0.1.7 (c (n "brackets-macros") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0zvkd3ianq52dw2344dd74wrngx181vshdqsm8lrvwf9pclg10mz")))

