(define-module (crates-io br ac bracket-embedding) #:use-module (crates-io))

(define-public crate-bracket-embedding-0.8.7 (c (n "bracket-embedding") (v "0.8.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "~0.12") (d #t) (k 0)))) (h "1612dqwy2wz4xi8zm0f2zyrzfpzgh3ay66d615hhc0sry93ngjy1")))

