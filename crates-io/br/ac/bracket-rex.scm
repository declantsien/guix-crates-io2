(define-module (crates-io br ac bracket-rex) #:use-module (crates-io))

(define-public crate-bracket-rex-0.8.7 (c (n "bracket-rex") (v "0.8.7") (d (list (d (n "bracket-color") (r "~0.8") (f (quote ("palette"))) (d #t) (k 0)) (d (n "bracket-embedding") (r "~0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)))) (h "0shrxyk3fdppg9nf9jhra0r7jijjs4a2kmk0fgqd160b0wamswy4")))

