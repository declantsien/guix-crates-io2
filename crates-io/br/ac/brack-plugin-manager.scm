(define-module (crates-io br ac brack-plugin-manager) #:use-module (crates-io))

(define-public crate-brack-plugin-manager-0.1.0 (c (n "brack-plugin-manager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1vz2jgphf172m38m57qvhwawb4a2a47v290g19709aciis5d8li2")))

