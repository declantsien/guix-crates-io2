(define-module (crates-io br ac brack-tokenizer) #:use-module (crates-io))

(define-public crate-brack-tokenizer-0.1.0 (c (n "brack-tokenizer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)))) (h "0jcdxkh2d5blcwpi455y7fcj4x99l1b24r332kll3yzknclf7dgd")))

