(define-module (crates-io br ac bracket-algorithm-traits) #:use-module (crates-io))

(define-public crate-bracket-algorithm-traits-0.1.0 (c (n "bracket-algorithm-traits") (v "0.1.0") (d (list (d (n "bracket-geometry") (r "^0.1.0") (d #t) (k 0)))) (h "07ixqd6v3s2yj2s5rmmjfbkaa485img3gadahy5ydc9z2z0cf4is")))

(define-public crate-bracket-algorithm-traits-0.7.0 (c (n "bracket-algorithm-traits") (v "0.7.0") (d (list (d (n "bracket-geometry") (r "^0.7.0") (d #t) (k 0)))) (h "009n8784nybglw516hbr3cs7qgswjqpcqfv67g54arf615jrhd1b")))

(define-public crate-bracket-algorithm-traits-0.8.0 (c (n "bracket-algorithm-traits") (v "0.8.0") (d (list (d (n "bracket-geometry") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1dj3q1lx15pi1kz13ld110kxy2a0aykhb78gc6zmi380mzvh76v3")))

(define-public crate-bracket-algorithm-traits-0.8.1 (c (n "bracket-algorithm-traits") (v "0.8.1") (d (list (d (n "bracket-geometry") (r "~0.8.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0wxb6fd2slsmra9wqsrcj2spk9js8rlqhwsjwwg8jk5shv9j3h5y")))

(define-public crate-bracket-algorithm-traits-0.8.2 (c (n "bracket-algorithm-traits") (v "0.8.2") (d (list (d (n "bracket-geometry") (r "~0.8.2") (d #t) (k 0)) (d (n "smallvec") (r "~1") (d #t) (k 0)))) (h "0wrf1q31gy6xnlw1inad4l3lrm75p0fqa57nwpmizm2xf90yw8xg")))

(define-public crate-bracket-algorithm-traits-0.8.7 (c (n "bracket-algorithm-traits") (v "0.8.7") (d (list (d (n "bracket-geometry") (r "~0.8") (d #t) (k 0)) (d (n "smallvec") (r "~1") (d #t) (k 0)))) (h "1lygcphbk900nj29yz3k4g2kh2x8z01qap83zh1wvrikmjcgn4sa")))

