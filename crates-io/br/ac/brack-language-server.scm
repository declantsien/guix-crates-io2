(define-module (crates-io br ac brack-language-server) #:use-module (crates-io))

(define-public crate-brack-language-server-0.1.0 (c (n "brack-language-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lsp-types") (r "^0.95") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18qf15s5l6d9qpz8x1gxlbn37z0qrjvij399ywyz34g30y4adm5i")))

