(define-module (crates-io br ac bracoxide) #:use-module (crates-io))

(define-public crate-bracoxide-0.1.0 (c (n "bracoxide") (v "0.1.0") (h "1yyfpm1jwc72qkjyaj0kwa7mjvyx76hh3xfzpakp3w6dx78l5391")))

(define-public crate-bracoxide-0.1.1 (c (n "bracoxide") (v "0.1.1") (h "19dyxi8a4bhxb4a6sd1c923l48bq2wkp7dap3kv5j10salbw2xmf")))

(define-public crate-bracoxide-0.1.2 (c (n "bracoxide") (v "0.1.2") (h "0sq7vaisz8ld5x8q7l08gfqrh5r4byr2y4g9rq0iw1yk67nlqrfq")))

(define-public crate-bracoxide-0.1.3 (c (n "bracoxide") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1j71fs71ss70rj7n4na8hg63w93czhrjdgi7di6nma12lrfg79xd")))

