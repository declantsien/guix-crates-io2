(define-module (crates-io br ac brackets) #:use-module (crates-io))

(define-public crate-brackets-0.1.4 (c (n "brackets") (v "0.1.4") (d (list (d (n "brackets-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "1rf1pw9rcci4gvwi6yz48mka0afy791pxsh63hjlc1xzn3bz2j0a") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-brackets-0.1.5 (c (n "brackets") (v "0.1.5") (d (list (d (n "brackets-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "06xb10nn6j0rqgm0qgyd4h3nzxss4kh769qpxcnz72i8i2bfwbn7") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-brackets-0.1.6 (c (n "brackets") (v "0.1.6") (d (list (d (n "brackets-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "01p4zkici71spn29sh7816m2d1hik7vcyyzc84iap4srlv9ff5k2") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-brackets-0.1.7 (c (n "brackets") (v "0.1.7") (d (list (d (n "brackets-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "18mqglzn9x05am937cbbvlq02rnmqa7v7la7d0bz42bwmhb58w63") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

