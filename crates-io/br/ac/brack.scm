(define-module (crates-io br ac brack) #:use-module (crates-io))

(define-public crate-brack-0.1.0 (c (n "brack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "09kzprjg88d1j8k5ag4kg76x4w41cxzsy4gwa7373zmxc83csarf")))

(define-public crate-brack-0.2.0 (c (n "brack") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0892x5v43mr9djhmvakskwgvalg5srvga218273p80d9f5rm0sfc")))

(define-public crate-brack-0.3.0 (c (n "brack") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0gyifkbxmz5yxzrf3immg6zhg6mqld46w7jajlmkbq2vfbvl94wm")))

