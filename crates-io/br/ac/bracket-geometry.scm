(define-module (crates-io br ac bracket-geometry) #:use-module (crates-io))

(define-public crate-bracket-geometry-0.1.0 (c (n "bracket-geometry") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.4.5") (d #t) (k 0)))) (h "02h926s763i2wwrvl6c80niqcmilmb8kh7r7s5wdlbn05d7cppp3")))

(define-public crate-bracket-geometry-0.7.0 (c (n "bracket-geometry") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.16.1") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.4.5") (d #t) (k 0)))) (h "044l5wm43pqxf6qvcfvi2rfl2ma046gzh30v9swzc09cl4wqh6av")))

(define-public crate-bracket-geometry-0.8.0 (c (n "bracket-geometry") (v "0.8.0") (d (list (d (n "crossterm") (r "^0.17.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.16.1") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.4.5") (d #t) (k 0)))) (h "18yi5nzbs5y91g1i5mn26dgbqy4ji6j4h1m6vkgr1493wlgyqrva")))

(define-public crate-bracket-geometry-0.8.1 (c (n "bracket-geometry") (v "0.8.1") (d (list (d (n "crossterm") (r "^0.17.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.16.1") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "~0.4.6") (d #t) (k 0)))) (h "0n7krkapvf0rpj2ffacrvdnkk73zp65crqjp7pq0ka35zsn17ikf")))

(define-public crate-bracket-geometry-0.8.2 (c (n "bracket-geometry") (v "0.8.2") (d (list (d (n "crossterm") (r "~0.19") (d #t) (k 2)) (d (n "serde") (r "~1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.16.1") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "~0.7.5") (d #t) (k 0)))) (h "1y42ddswrjjgwbf94p6yfkdm38fa1y71fbdqlicad2jkmp4s0lf5")))

(define-public crate-bracket-geometry-0.8.3 (c (n "bracket-geometry") (v "0.8.3") (d (list (d (n "crossterm") (r "~0.19") (d #t) (k 2)) (d (n "serde") (r "~1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "~0.17") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "~0.8.1") (d #t) (k 0)))) (h "0as0a2c4jx60m98pszga2qfm843vzrzzzy42p4537ncbqqr2rcjd")))

(define-public crate-bracket-geometry-0.8.7 (c (n "bracket-geometry") (v "0.8.7") (d (list (d (n "bevy") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "~0.25") (d #t) (k 2)) (d (n "serde") (r "~1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "specs") (r "~0.18.0") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "~0.9.0") (d #t) (k 0)))) (h "168ljpphngph4nhsb9p41pbrglsx18jkl7isby42fl6nzhjvac8g")))

