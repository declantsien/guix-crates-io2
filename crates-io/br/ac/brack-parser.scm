(define-module (crates-io br ac brack-parser) #:use-module (crates-io))

(define-public crate-brack-parser-0.1.0 (c (n "brack-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "brack-sdk-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "brack-tokenizer") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0h9l3bk81jqw8nl9xhs91azslf7wyyajmascg8zs9k8p8hipajfr")))

