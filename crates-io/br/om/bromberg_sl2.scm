(define-module (crates-io br om bromberg_sl2) #:use-module (crates-io))

(define-public crate-bromberg_sl2-0.1.0 (c (n "bromberg_sl2") (v "0.1.0") (d (list (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1qm2skjpgq6l3sk1pkx522f2i3f224fr0kswbqn3x93b8ipr0rly") (y #t)))

(define-public crate-bromberg_sl2-0.1.1 (c (n "bromberg_sl2") (v "0.1.1") (d (list (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1wjcrjih25wz9a67z8g6ha8pikcn79yjnw1yzd3kz93j0izn27km") (y #t)))

(define-public crate-bromberg_sl2-0.1.2 (c (n "bromberg_sl2") (v "0.1.2") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1fjfqzx4ysyma4f2jsqa7f16q0ypd9vpynbaaxi9kgmyw0hj2bya") (y #t)))

(define-public crate-bromberg_sl2-0.2.0 (c (n "bromberg_sl2") (v "0.2.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1wbvvj2dfsmcqpmcz3qjhc5gawi5qbyqn2vqgp3g59w8656fwrcg") (y #t)))

(define-public crate-bromberg_sl2-0.3.0 (c (n "bromberg_sl2") (v "0.3.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1hldlkl6halpjchxq6z43sms8micxdvx9xfr1ka32hs3wq2by8zb") (y #t)))

(define-public crate-bromberg_sl2-0.3.1 (c (n "bromberg_sl2") (v "0.3.1") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "0syr19636p93503dik0iaan1b6bik01cj29dld4c7kx99q5ycr73") (y #t)))

(define-public crate-bromberg_sl2-0.3.2 (c (n "bromberg_sl2") (v "0.3.2") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "seq-macro") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "096qcys959zgjnid5xbmdpi63v6ica3b9ww7zzib05ykscwxrd3l") (y #t)))

(define-public crate-bromberg_sl2-0.4.0 (c (n "bromberg_sl2") (v "0.4.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "seq-macro") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1z2i5zxm28ilmpiakraxjm1d4kb9pf5v21pl7aksfx0db9rfj4hd") (y #t)))

(define-public crate-bromberg_sl2-0.4.1 (c (n "bromberg_sl2") (v "0.4.1") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "num-bigint") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "seq-macro") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "sha3") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1ynd4ysg81cnc5vvsbp1rayw3cj1rbkl3wqpjf4d2a17z92mlv6z") (y #t)))

(define-public crate-bromberg_sl2-0.4.2 (c (n "bromberg_sl2") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "0fflvq194wzmnnb36pjw384jvgvs1wibjyksnr0gvgzwsis1jc3x") (y #t)))

(define-public crate-bromberg_sl2-0.4.3 (c (n "bromberg_sl2") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1ibav12hz4yz4h4pwfd3fcrlcxpr8cgc858l35z7ar2xihbhibzx") (y #t)))

(define-public crate-bromberg_sl2-0.4.4 (c (n "bromberg_sl2") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1v1hrmxz355p1sj3k41jpi6d3w0cdb8fkfyplg2jxkwd2cnlckcm") (y #t)))

(define-public crate-bromberg_sl2-0.4.5 (c (n "bromberg_sl2") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1a07a44rpxgnznv19m7vnwrdhafqv4ndwzg9xsppymd4nycafwjn") (y #t)))

(define-public crate-bromberg_sl2-0.5.0 (c (n "bromberg_sl2") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1qqqxh524npnf9nal4blgl2laif0rg63c9rq17pny30ya676vm8w") (y #t)))

(define-public crate-bromberg_sl2-0.5.1 (c (n "bromberg_sl2") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "04xn7zphgcnarqax2xh55yv8ignwm9bci8pbdi8vxb3c1jwwm0kd") (y #t)))

(define-public crate-bromberg_sl2-0.6.0 (c (n "bromberg_sl2") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "0kad2c8vdj75bawpg8qqhqj1jgylc4dzrv2hxbivf64myrj81n1f") (f (quote (("std" "rayon") ("default" "std"))))))

