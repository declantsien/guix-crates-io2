(define-module (crates-io br ai brainfreak) #:use-module (crates-io))

(define-public crate-brainfreak-0.1.0-alpha (c (n "brainfreak") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cranelift") (r "^0.103.0") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.103.0") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.103.0") (d #t) (k 0)) (d (n "cranelift-object") (r "^0.103.0") (d #t) (k 0)))) (h "0vpp62x12dmlqykmsh0dcs89n92m1mlx8fmlvycac5ln8k92m3c5")))

