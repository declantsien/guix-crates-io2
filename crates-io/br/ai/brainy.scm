(define-module (crates-io br ai brainy) #:use-module (crates-io))

(define-public crate-brainy-0.1.0 (c (n "brainy") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01313rwprsn76fsiwyc3di56axs7r89rfspmykkb4ar05lj3np5q")))

(define-public crate-brainy-0.1.1 (c (n "brainy") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rk12dpl6plrf0ij8dc7w3zklprwd1fxzjhrp2d8wskmcvxjljdh")))

(define-public crate-brainy-0.2.0 (c (n "brainy") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1aay3wyfk2b2079ls5hi42dyn34njckn4a3f8r4nnl0jw54bbxha")))

(define-public crate-brainy-0.2.1 (c (n "brainy") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17pqljcrpadq5jxsj181p8dn5w73mazlp310i8mvzsaxpv2rmq38")))

(define-public crate-brainy-0.2.2 (c (n "brainy") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02bbi00d3akfkzvv3pfp5jjcl0dfc106lj3rxqkl4smlwrfba8ir")))

(define-public crate-brainy-0.2.3 (c (n "brainy") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rn30abc9n9pdcxr0mlsxpqprg5fb1zzyhh1jckdvrnalj9ysv28")))

