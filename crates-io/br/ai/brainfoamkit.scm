(define-module (crates-io br ai brainfoamkit) #:use-module (crates-io))

(define-public crate-brainfoamkit-1.0.0-alpha.1 (c (n "brainfoamkit") (v "1.0.0-alpha.1") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)))) (h "027qsif5f78pscmra9s5bbynxp04mj66g0i13mba8l617fna9hj1") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.2 (c (n "brainfoamkit") (v "1.0.0-alpha.2") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)))) (h "0ndc8wq8avrmig4yc4p5gbsa4hcvjbg2k2s0glrv3acaffx1p8jk") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.3 (c (n "brainfoamkit") (v "1.0.0-alpha.3") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)))) (h "1aqk8wiw4gh3h35s1rz2v63w1vcw4qp5v8clpivj1zvz8gmvjxj5") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.4 (c (n "brainfoamkit") (v "1.0.0-alpha.4") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)))) (h "13xjixqgp5qkrq43cx0silzrs8dx5bvh6vw0rbrzvj0rwbgcjk7c") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.5 (c (n "brainfoamkit") (v "1.0.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "1bn8c01fk839h1ns6z3whwa4wayss2qws0cds9gj55jplx35m24b") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.6 (c (n "brainfoamkit") (v "1.0.0-alpha.6") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "167wdhiwq7accrsl2nyx5975x7jp5av4v86lq67f06cff1i4qaaq") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.7 (c (n "brainfoamkit") (v "1.0.0-alpha.7") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "0csgjvs24lgf51zkv6x5pkandi8krzavaml3n0bxm126l26yk2b3") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.8 (c (n "brainfoamkit") (v "1.0.0-alpha.8") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "1jp4v2kfyf7wqglwa35i8gj14lvhdq9hlp5xc233wjcg250j7snn") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.9 (c (n "brainfoamkit") (v "1.0.0-alpha.9") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "1n7q182kn8h5l361md7ywbnix8ikz7h81h37ms5giy6aqr9pdwhp") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.10 (c (n "brainfoamkit") (v "1.0.0-alpha.10") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "08njjlr3jlnvdppd2k8g327f990zvd1rlah1gzx5c166jgm787kp") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.11 (c (n "brainfoamkit") (v "1.0.0-alpha.11") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "15prj7g3nrkcgy95gmhpmas3f686y6xrfi5cgkc7h0avzq8scd6v") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.12 (c (n "brainfoamkit") (v "1.0.0-alpha.12") (d (list (d (n "anyhow") (r "^1.0.77") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "04ysss3s5wzljl48pf08c3jjmgyqhf71w0g6bhfw7lxv3sjjmhs7") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.13 (c (n "brainfoamkit") (v "1.0.0-alpha.13") (d (list (d (n "anyhow") (r "^1.0.77") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "11j05hlppkf56kqafdi9jl2x0rmqlb3wnkbn70x17k8ln44y1yz8") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-beta.1 (c (n "brainfoamkit") (v "1.0.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.77") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "0fmrch746c71wmp0h4q0chb5cnq8fq1lp5fd0cxjgkggm0bfxiax") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-alpha.14 (c (n "brainfoamkit") (v "1.0.0-alpha.14") (d (list (d (n "anyhow") (r "^1.0.77") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("macros" "serde" "document-features"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1mbic29z28z8n2g2w4lvapf3isj4z326y7nyxfprcaapxqr1p29v") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0-beta.2 (c (n "brainfoamkit") (v "1.0.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("macros" "serde" "document-features"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1c0kvxj5n2ad5l6cm384xbbkny3hf6995ncgs6jya75gd2hrkgq0") (r "1.70.0")))

(define-public crate-brainfoamkit-1.0.0 (c (n "brainfoamkit") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("macros" "serde" "document-features"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "17sm4c9ss1vscmwy251rlp90v5iwk5f29q2ajk8g30ykrf91cp9f") (r "1.70.0")))

