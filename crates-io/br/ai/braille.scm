(define-module (crates-io br ai braille) #:use-module (crates-io))

(define-public crate-braille-1.0.0 (c (n "braille") (v "1.0.0") (h "10na2lhga7pamy2hgwzq88ak9lljhvqvdwzw8430fnhz25yv7cxw")))

(define-public crate-braille-1.0.1 (c (n "braille") (v "1.0.1") (h "1qpq8sbgfjz1735x923b095shafff7yw9mkij8wiq8asiyr09q29")))

(define-public crate-braille-1.0.2 (c (n "braille") (v "1.0.2") (h "1bv1k732qn2s0fj5aj419i8r6dgnz3h81hf46ih44zbjcsfmvf55")))

(define-public crate-braille-1.0.3 (c (n "braille") (v "1.0.3") (h "1686d5q86gxqd1f980xqqgprylgx8939jpxq8jfnsrf3n3ykb7ry")))

