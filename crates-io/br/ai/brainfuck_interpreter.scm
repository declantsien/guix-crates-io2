(define-module (crates-io br ai brainfuck_interpreter) #:use-module (crates-io))

(define-public crate-brainfuck_interpreter-0.1.0 (c (n "brainfuck_interpreter") (v "0.1.0") (h "1629fay1a6gh8hhwkm1n4789brl8fn3svkgq05syqrhi6a8qcdgp") (f (quote (("ignore-input-error"))))))

(define-public crate-brainfuck_interpreter-0.1.1 (c (n "brainfuck_interpreter") (v "0.1.1") (h "09bksahma4mrg3sxbf5rqpc9n6y94qjhykclgfmvmjyw6c6if7bw") (f (quote (("ignore-input-error"))))))

