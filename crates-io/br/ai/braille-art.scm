(define-module (crates-io br ai braille-art) #:use-module (crates-io))

(define-public crate-braille-art-0.1.0 (c (n "braille-art") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "1g7b8nrx7yk8xmh065nj4srrq3znrv5qyld3civc9z8pawll2szk") (f (quote (("default"))))))

