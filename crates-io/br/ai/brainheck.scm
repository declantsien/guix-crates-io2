(define-module (crates-io br ai brainheck) #:use-module (crates-io))

(define-public crate-brainheck-0.1.0 (c (n "brainheck") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.2") (f (quote ("yaml"))) (d #t) (k 0)))) (h "04pr2kk1q8269cdjp0i2aiwcx4hih9707j1ypv6n1y854g8b7lwi")))

(define-public crate-brainheck-0.1.1 (c (n "brainheck") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.2") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0m8i5hmdgs8jrzqnrzw5cx4j66wyjp03zphkl21dmdlyv8cv7d3c")))

(define-public crate-brainheck-0.1.2 (c (n "brainheck") (v "0.1.2") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0x38w4qhw96119hhrn5gd5ivwf4ckjx2p552pkmdnrdnrcnz49m5")))

(define-public crate-brainheck-0.1.3 (c (n "brainheck") (v "0.1.3") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0jcgz27zsvzc2lw0q1y01i0j7j8v6y3110j3j6sx5cqsx9rxh8ga")))

(define-public crate-brainheck-0.1.4 (c (n "brainheck") (v "0.1.4") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1n7gk0xb0bxnb1dpp5px69j1r5cm7l87idyz2jsdq0q2m994ai6j")))

(define-public crate-brainheck-0.1.5 (c (n "brainheck") (v "0.1.5") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1q8384kk63kcjdfwwwl02gfp8234z7mpx1kfbryw44xm0hhfc7ry")))

(define-public crate-brainheck-0.1.7 (c (n "brainheck") (v "0.1.7") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "17kmiasj4icvd7n3s8qkm4h3jwbf1flf4ff66wg4k1hwr3axgkb2")))

(define-public crate-brainheck-0.1.8 (c (n "brainheck") (v "0.1.8") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0m0zp53rbcn4mzpg36i5bjzqwxplxcmqk17g48z8gy6ahj9p6x01")))

