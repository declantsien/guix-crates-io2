(define-module (crates-io br ai brainease_runtime) #:use-module (crates-io))

(define-public crate-brainease_runtime-1.0.2 (c (n "brainease_runtime") (v "1.0.2") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wk21gln1pxb0p8halcrwa23b9x9qfn0803fbz15pqan42iaa175")))

(define-public crate-brainease_runtime-1.0.3 (c (n "brainease_runtime") (v "1.0.3") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r59j7mwz9kzz2d3y5b1rfsnxx4pm5n6rrqj691f8y32g2wn73lg")))

(define-public crate-brainease_runtime-1.0.4 (c (n "brainease_runtime") (v "1.0.4") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vqw0ymymvdcr902a6kcbslni452bf33zsb3w7slbmrnbf3hbi8f")))

(define-public crate-brainease_runtime-1.0.5 (c (n "brainease_runtime") (v "1.0.5") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b9rn261pwz065qijyjwyglvn9vc1h0gbi5yxwq014n5n71bampq")))

(define-public crate-brainease_runtime-1.0.6 (c (n "brainease_runtime") (v "1.0.6") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pwhbrsk38k2f92lvslddprjrdf06haqg14fhbyf8asskhkd0r9l")))

(define-public crate-brainease_runtime-1.0.8 (c (n "brainease_runtime") (v "1.0.8") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.9") (d #t) (k 0)))) (h "09sqf8nxfmkv4rggvsab1bigyb7m0j27kdkizbrsmaf71ys02cfh")))

