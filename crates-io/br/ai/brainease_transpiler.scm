(define-module (crates-io br ai brainease_transpiler) #:use-module (crates-io))

(define-public crate-brainease_transpiler-1.0.4 (c (n "brainease_transpiler") (v "1.0.4") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ycpicfhkaq6a18bw21q54rhpl441wmvb2f40lgricjnv2nyv40f")))

(define-public crate-brainease_transpiler-1.0.5 (c (n "brainease_transpiler") (v "1.0.5") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09jqxn11hrdy1s1ahfmxdck2fcd9scm5l9dlf99284sn8npwxacv")))

(define-public crate-brainease_transpiler-1.0.6 (c (n "brainease_transpiler") (v "1.0.6") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "082fr7qi4md9sjlg4wil09yps1qj1mdh5mrzxm0i46b400hc6kmc")))

(define-public crate-brainease_transpiler-1.0.8 (c (n "brainease_transpiler") (v "1.0.8") (d (list (d (n "brainease_lexer") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0nqv6kd9dnf3a5j4x02wm3xi80fyj7sp26154vxw73prk0yfrik9")))

