(define-module (crates-io br ai brainfuckr) #:use-module (crates-io))

(define-public crate-brainfuckr-0.1.0 (c (n "brainfuckr") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-files") (r "^0.1.0") (d #t) (k 0)))) (h "06kiq0lkk7p7k3mi1a2cah64q1bqabzb1542valwwl7yqcggx9d1")))

(define-public crate-brainfuckr-0.1.1 (c (n "brainfuckr") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-files") (r "^0.1.0") (d #t) (k 0)))) (h "1mhj6skazx26h1vc97r3zpyxbj36h85pbzwd32z2hrq12wvsf3ix")))

(define-public crate-brainfuckr-0.1.2 (c (n "brainfuckr") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-files") (r "^0.1.0") (d #t) (k 0)))) (h "14qz05i00cqzigwvqjgl74zifi49241s9d92xinkc83anrxjpykv")))

(define-public crate-brainfuckr-0.1.3 (c (n "brainfuckr") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-files") (r "^0.1.0") (d #t) (k 0)))) (h "0vws22dflbxafrcy7p3zqli41nl4j1kawcwh7y93mq6frwf8i2qf")))

