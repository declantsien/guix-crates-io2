(define-module (crates-io br ai brainfuck-plusplus) #:use-module (crates-io))

(define-public crate-brainfuck-plusplus-1.0.0 (c (n "brainfuck-plusplus") (v "1.0.0") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gwpzxlimxc7ya7qcrgj8mz1j6hw0yxxsbimrqvd2i4451s5crfd")))

(define-public crate-brainfuck-plusplus-1.0.1 (c (n "brainfuck-plusplus") (v "1.0.1") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "05dvy2ls7w3mxy962v3587krb1s1xz2rvspcbpkg3xzbag32h6wk")))

