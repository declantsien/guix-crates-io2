(define-module (crates-io br ai brainhecc) #:use-module (crates-io))

(define-public crate-brainhecc-0.1.0 (c (n "brainhecc") (v "0.1.0") (d (list (d (n "cranelift") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-object") (r "^0.76.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.2") (d #t) (k 0)))) (h "19c54xix8l5lymmr4vz3bb353dsid32cvyhwjpzby5xd8w04ivw7")))

