(define-module (crates-io br ai brainfuck_plus-core) #:use-module (crates-io))

(define-public crate-brainfuck_plus-core-0.1.0 (c (n "brainfuck_plus-core") (v "0.1.0") (h "0f139aivprsjsvqplgn0c9g9wnxfm0rgy3iy3cdq3g09kq16qsfg")))

(define-public crate-brainfuck_plus-core-0.1.1 (c (n "brainfuck_plus-core") (v "0.1.1") (h "00cjnjbc5njlcvxzd0036m46d8x5nkl243qpnch6k0k9rj1la6mg")))

(define-public crate-brainfuck_plus-core-0.1.2 (c (n "brainfuck_plus-core") (v "0.1.2") (h "1dmy1k4a2c2ik95llq8ssgm07zk7phf3p35bz9z63kahrgli7vql")))

(define-public crate-brainfuck_plus-core-0.1.3 (c (n "brainfuck_plus-core") (v "0.1.3") (h "102bq7qcirpihy52prcwgpaj1npywc7fbff9q1mc579xlv0r5b3m")))

(define-public crate-brainfuck_plus-core-0.1.35 (c (n "brainfuck_plus-core") (v "0.1.35") (h "0klnrjnlad3z6x13spfwsb502rfywh2d627ih3sfpnz8pni62lsa")))

(define-public crate-brainfuck_plus-core-0.1.36 (c (n "brainfuck_plus-core") (v "0.1.36") (h "18494kkf77wp6jpsa2c11x127m134acjqa6jq92vpwp9sxj6v067")))

(define-public crate-brainfuck_plus-core-0.1.37 (c (n "brainfuck_plus-core") (v "0.1.37") (h "0yaqi3gmw8d2hba1w4gky6iibb52z029d2f2ac2zc2bkjsz4rdr5")))

(define-public crate-brainfuck_plus-core-0.1.38 (c (n "brainfuck_plus-core") (v "0.1.38") (h "0gz0lljkngh2hh29fjqb7jk4rz5lpxlrgxv54mnvx8jgccq1lha6")))

