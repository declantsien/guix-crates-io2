(define-module (crates-io br ai brainterpreter) #:use-module (crates-io))

(define-public crate-brainterpreter-0.1.1 (c (n "brainterpreter") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kxmwdk15q1m7mh9js98jj1hyqlj7x1xrwgmsi8wyd4wyyv5iamg") (f (quote (("cli" "clap" "env_logger"))))))

(define-public crate-brainterpreter-0.1.2 (c (n "brainterpreter") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "166v3n822c1psh272lh4p50f9kv684wvxx9nb4f8ima3mh0wss7p") (f (quote (("cli" "clap" "env_logger"))))))

