(define-module (crates-io br ai brainfuck_rs) #:use-module (crates-io))

(define-public crate-brainfuck_rs-0.1.0 (c (n "brainfuck_rs") (v "0.1.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "0ivl2n2x6mj5qwxclar360r0hj4gzmv0b01kxx1l35vdcn0idim0")))

(define-public crate-brainfuck_rs-0.2.0 (c (n "brainfuck_rs") (v "0.2.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "0pq5y12ifbzs7hb3npfgaysxjblc1w9v3359gl8g1d1anpwda0ck")))

(define-public crate-brainfuck_rs-0.3.0 (c (n "brainfuck_rs") (v "0.3.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "0q52mf9v6lks6h6wsrlrgr5nxm2sg6ksas5r1ijxcqan5wvlwal4")))

(define-public crate-brainfuck_rs-0.3.1 (c (n "brainfuck_rs") (v "0.3.1") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0v1snf60dp4drhvp3by3bda3qi5a2sxz7c4wqb11l76m4pcynhbf")))

(define-public crate-brainfuck_rs-1.0.0 (c (n "brainfuck_rs") (v "1.0.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "nom") (r "^2.1.0") (d #t) (k 0)))) (h "0a27bhnb3as6j41285dxfmy2jwpmybq486qmb258wlpx8m4dzc0i")))

(define-public crate-brainfuck_rs-1.1.0 (c (n "brainfuck_rs") (v "1.1.0") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)))) (h "018qfkj46cq3syx8s7w805hvjlc9sfckjhyizbv7r22cgpw4jdkl")))

(define-public crate-brainfuck_rs-1.1.1 (c (n "brainfuck_rs") (v "1.1.1") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "01165wnmm9mi68ivbbj6acq69rkblgi40gsps0rvj5l5d3k5cq4x")))

(define-public crate-brainfuck_rs-2.0.0 (c (n "brainfuck_rs") (v "2.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0fav9jnqf5hn8s6sx2bjklv65kp5l4wgnzizxcxzg3m2p03d7p1f")))

