(define-module (crates-io br ai brainfuck_macros) #:use-module (crates-io))

(define-public crate-brainfuck_macros-0.1.0 (c (n "brainfuck_macros") (v "0.1.0") (h "1yamsa8w3ndx3rsfzqz0pg0n4kllm1v9nydfamc1gbig4b3i42xv")))

(define-public crate-brainfuck_macros-0.1.1 (c (n "brainfuck_macros") (v "0.1.1") (h "0b2l85bi1j5hqhbw7s3x9ir7znlpwarn5f59ypa7dyj1zfjk4pd6")))

(define-public crate-brainfuck_macros-0.1.2 (c (n "brainfuck_macros") (v "0.1.2") (h "05nrhyxw41681hc3m814w68m2nk14bh7z3w61cfmnkdgiihf7xdx")))

(define-public crate-brainfuck_macros-0.1.3 (c (n "brainfuck_macros") (v "0.1.3") (h "06ffgbsdz122g8pbigzigddfg2ak9lcb3ramgml8v9vjwk06xl4y")))

(define-public crate-brainfuck_macros-0.1.4 (c (n "brainfuck_macros") (v "0.1.4") (h "0idsqsg1hwvbwmffk875y57v3wghvjx5nvxm434nxys9pa423npp")))

(define-public crate-brainfuck_macros-0.1.5 (c (n "brainfuck_macros") (v "0.1.5") (h "16csd82dap9x5wipxscbbj18bpyz50cvj6hr60g150k3i9gjahn4")))

