(define-module (crates-io br ai braille-rs) #:use-module (crates-io))

(define-public crate-braille-rs-1.0.0 (c (n "braille-rs") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1262h91bf6jy4pv2dh0ly9khw8qaqffvky4wgpddm4bgm35m32f0")))

