(define-module (crates-io br ai brainfrick) #:use-module (crates-io))

(define-public crate-brainfrick-1.0.0 (c (n "brainfrick") (v "1.0.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "078qfrl74w81lm8yrs80rdi4bn6svgb13ixwnwd3dy9blvh12qnx")))

(define-public crate-brainfrick-1.1.0 (c (n "brainfrick") (v "1.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "151v56f1ssgp0s4xhcmyvgklnh7rqccz434x7mx9jdjcxz9m3jx5")))

(define-public crate-brainfrick-1.1.1 (c (n "brainfrick") (v "1.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0hvdd32pdnzc2vk74x8lnncjxz6dz9wf1p4f3mqqxgpgp74lw1hw")))

(define-public crate-brainfrick-1.1.2 (c (n "brainfrick") (v "1.1.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "045w519nxny12pkva98qvlhf65kr7sdqkd0vfz4rxcgakric8jxj")))

