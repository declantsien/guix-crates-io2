(define-module (crates-io br ai brainoxide) #:use-module (crates-io))

(define-public crate-brainoxide-0.1.0 (c (n "brainoxide") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1l2rnrkkcsjiaiv16i4qs4zv00zfhx6c4vsy7581dg86zpb5j5rs") (f (quote (("default"))))))

(define-public crate-brainoxide-0.1.1 (c (n "brainoxide") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.1.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1mjixsz09wj4wi34dm8g5q783s1hj1fxhq7n818f4z1kqfgdl564") (f (quote (("default"))))))

(define-public crate-brainoxide-0.1.2 (c (n "brainoxide") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0p5lqwv0452icq2pyv9ayl0sr78xdkqvcy0llbx57q3kia699pig") (f (quote (("default"))))))

