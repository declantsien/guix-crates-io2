(define-module (crates-io br ai braillefb) #:use-module (crates-io))

(define-public crate-braillefb-0.1.0 (c (n "braillefb") (v "0.1.0") (h "0qk44hiibka4swx6i8xapa77y52b2vyq68vpmsjx9c2dqpmjknz9")))

(define-public crate-braillefb-0.2.0 (c (n "braillefb") (v "0.2.0") (h "0w947v8lydvdf0n30xf6czzhczc46fs222nvkqfwfb7qahhcaiyb")))

