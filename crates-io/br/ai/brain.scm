(define-module (crates-io br ai brain) #:use-module (crates-io))

(define-public crate-brain-0.0.1 (c (n "brain") (v "0.0.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1g57lg8ab345rihjjf4cb5h21lkfkpwpi8vq0mnbjjrzmz52lvfs")))

(define-public crate-brain-0.1.0 (c (n "brain") (v "0.1.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1qzbb0wmpa6zj620nz1p0vm7vj2i4qqq3qvz0x6k76j4mvzs5fji")))

(define-public crate-brain-0.1.1 (c (n "brain") (v "0.1.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "048ad80s7j27hgnkvlnpnpv5pvriv73qzs3c1lmid1lhz6xb8ika")))

(define-public crate-brain-0.1.2 (c (n "brain") (v "0.1.2") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1ndvaxxdklrhn9xww9mx11qbzr9kd85wsdnhac6gdscblyvsrkzx")))

