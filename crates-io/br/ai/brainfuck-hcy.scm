(define-module (crates-io br ai brainfuck-hcy) #:use-module (crates-io))

(define-public crate-brainfuck-hcy-1.0.1 (c (n "brainfuck-hcy") (v "1.0.1") (h "1pigw1kiik010rz83jyfrmp45rxjwfdq6rcsg31dpp86l8n15gqq")))

(define-public crate-brainfuck-hcy-1.0.2 (c (n "brainfuck-hcy") (v "1.0.2") (h "154az1pdvb481qkhyfmy671kwq11as7bqcqghhimp3qzd7ll4700")))

