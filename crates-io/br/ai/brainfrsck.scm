(define-module (crates-io br ai brainfrsck) #:use-module (crates-io))

(define-public crate-brainfrsck-1.0.0 (c (n "brainfrsck") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0wc9qlsnxnwshnxxp4xc4nfjv7ck375aqslwwf8c2kgff7l7frwd")))

(define-public crate-brainfrsck-1.0.1 (c (n "brainfrsck") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "148rlzmi2bw0mizlvrjrfgngs3i53vxmx0nlkn17h1zp0v63jj5s")))

(define-public crate-brainfrsck-1.0.2 (c (n "brainfrsck") (v "1.0.2") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0bkcw7mb924gs2kcgb0pxxf81b883n61jfprysk0syzaxb674z76")))

(define-public crate-brainfrsck-1.0.3 (c (n "brainfrsck") (v "1.0.3") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1p8r3j02prcpz769038s0l0b4afpz55zrpw0k8bvlhjg2xg82iyb")))

(define-public crate-brainfrsck-1.0.4 (c (n "brainfrsck") (v "1.0.4") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "162xy0fwbs7gaxzwa4kgdq2a1mnp63waglhzxaql0g3ml3r4qygm")))

