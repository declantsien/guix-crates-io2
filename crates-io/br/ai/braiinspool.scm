(define-module (crates-io br ai braiinspool) #:use-module (crates-io))

(define-public crate-braiinspool-0.1.0 (c (n "braiinspool") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0vpiwxckhvqfrwlp5xk52iyghmdw7jn7g3aj8g00bmzwwd0hpzzq")))

(define-public crate-braiinspool-0.1.1 (c (n "braiinspool") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "19flxdb5yhf97mf1jzaq19j3w9q84zld2anz7byasmvvifbxdkdb")))

