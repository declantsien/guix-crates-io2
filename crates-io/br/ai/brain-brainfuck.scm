(define-module (crates-io br ai brain-brainfuck) #:use-module (crates-io))

(define-public crate-brain-brainfuck-1.0.0 (c (n "brain-brainfuck") (v "1.0.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)))) (h "1fgkqvkj3vfhgiw5462cpyxxkgsv5b08m4w1l4swrjxxf61rvz0g")))

(define-public crate-brain-brainfuck-1.1.0 (c (n "brain-brainfuck") (v "1.1.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)))) (h "0s2pzghy7x4ncklabg1qk541r6z7jzrg92lz0xml96qxhaf85076")))

(define-public crate-brain-brainfuck-1.1.1 (c (n "brain-brainfuck") (v "1.1.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)))) (h "1b2jbbcg8hlzyg7d4zn221v8mlx1gvidrx4is1qpvk4dabz8ksy9")))

(define-public crate-brain-brainfuck-1.1.2 (c (n "brain-brainfuck") (v "1.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "0d4n5d7ncqilg9dlv5n2nzxzrf8p8hfrg26im48qqyayhkk63xbb")))

(define-public crate-brain-brainfuck-1.2.0 (c (n "brain-brainfuck") (v "1.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "0ydz70i7p5l8dfkfwdb1fdcy7hyvai1hvpy2hfmyinhrjfmxh4ny")))

(define-public crate-brain-brainfuck-1.3.0 (c (n "brain-brainfuck") (v "1.3.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1fsladhypmfi5ci4lcnmrqf3hbkavf3vbmymy61xzhbc17x60d02")))

