(define-module (crates-io br ai braid-triggerbox-comms) #:use-module (crates-io))

(define-public crate-braid-triggerbox-comms-0.1.0 (c (n "braid-triggerbox-comms") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.5.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (d #t) (k 0)))) (h "1hmgng4anw5pf1mrvln55c3xm1fx2lxidxgjmspwrk0njxmkzwnb") (f (quote (("thumbv6" "bbqueue/thumbv6") ("std") ("default" "std")))) (r "1.58")))

