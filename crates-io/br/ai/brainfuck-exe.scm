(define-module (crates-io br ai brainfuck-exe) #:use-module (crates-io))

(define-public crate-brainfuck-exe-0.1.0 (c (n "brainfuck-exe") (v "0.1.0") (h "070ls09ri6z34bkhsxw63q9xj17x8kr8l5vvir4ifhfnc8m6z0w2")))

(define-public crate-brainfuck-exe-0.1.1 (c (n "brainfuck-exe") (v "0.1.1") (h "1zw23bnxy929v3l2k00wbhrc7hb7livyb85zrd6y358yqghwyz7d")))

(define-public crate-brainfuck-exe-0.1.2 (c (n "brainfuck-exe") (v "0.1.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ygvdfajcg9r210kcglx7fqqgvmr8bsl1pfgfxjzh7haks1jf19g")))

(define-public crate-brainfuck-exe-0.1.3 (c (n "brainfuck-exe") (v "0.1.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zavsic63brynr4xq0l03cvpylzasga7i86l62c7phdg4kbzg96z")))

(define-public crate-brainfuck-exe-0.1.6 (c (n "brainfuck-exe") (v "0.1.6") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vdyzaw3rfhblqaznwgpwsbjzv7xbrdpr8mcafxwfyqxqpydkxwc")))

(define-public crate-brainfuck-exe-0.1.7 (c (n "brainfuck-exe") (v "0.1.7") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0x5z1a1ccrr23hpnpf8hm33xwr01798lnzrykbd4syqzjqc2k78m") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.1.8 (c (n "brainfuck-exe") (v "0.1.8") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1smy4kaavd0ba6gdlvlv6r6rqbgshrvr589yf6nn7bj78v5gv5q6") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.2.0 (c (n "brainfuck-exe") (v "0.2.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0591m1hibqrszvzvrbscgqclfbjmmzqxr3sa9zhli1bg6cdvld4m") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.2.1 (c (n "brainfuck-exe") (v "0.2.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "07k4srw80l2fkv484xmmpzhj7c7x14nnrz9h64i2qa1q03gjjf01") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.2.2 (c (n "brainfuck-exe") (v "0.2.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "04fqabp6p09paikrwp9vkzc4hjzfydavrnmzh92rdynvrpnhilqx") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.2.3 (c (n "brainfuck-exe") (v "0.2.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0dgvlp3kyx4mkkbkv94sk2k00jkmyd0mr9iwgiqi5wsnpy6frksk") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-brainfuck-exe-0.2.4 (c (n "brainfuck-exe") (v "0.2.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0vxa4503hbi14ijl87a6si94abrslvadybzaliacl41gn43h2v2w") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

