(define-module (crates-io br ai brainease_lexer) #:use-module (crates-io))

(define-public crate-brainease_lexer-1.0.0 (c (n "brainease_lexer") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vp7ih6rgy9b9lg13b1ijvz9i7049627987wh96l4qyqldamncw5")))

(define-public crate-brainease_lexer-1.0.1 (c (n "brainease_lexer") (v "1.0.1") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bw9j4gggg6yd9c6mr6gcry6z5qjms5ayabs1f5irf2qb1nijxzv")))

(define-public crate-brainease_lexer-1.0.2 (c (n "brainease_lexer") (v "1.0.2") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ixv2695b2qxqsb02f45pfk7xspzz61hfki987blg5v9wk8dl0ak")))

(define-public crate-brainease_lexer-1.0.3 (c (n "brainease_lexer") (v "1.0.3") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r1g36l7mmw5f9b6612b2sxjikw2jg316ki8fld79kk6hmc3m0hm")))

(define-public crate-brainease_lexer-1.0.4 (c (n "brainease_lexer") (v "1.0.4") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b8b5qhb646mrcjjj6f2z0fzc5ilz4vvp7ckbq6pd914w9ps786y")))

(define-public crate-brainease_lexer-1.0.5 (c (n "brainease_lexer") (v "1.0.5") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pg1bkhbjph8kp886hs8gr3kz8iqrfzhplyxhnhk0561spw3a62i")))

(define-public crate-brainease_lexer-1.0.6 (c (n "brainease_lexer") (v "1.0.6") (d (list (d (n "lazy-regex") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "093fvqdk42vc019pbfy9phi2bj4zz20mwaf9qrrnlzr6grrhq7a6")))

(define-public crate-brainease_lexer-1.0.8 (c (n "brainease_lexer") (v "1.0.8") (d (list (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1wx3l69531sbgzcfxqwpmq82h0pjgq51bravmjwpimp59ax9f8qv")))

