(define-module (crates-io br ai brainhug) #:use-module (crates-io))

(define-public crate-brainhug-0.1.0 (c (n "brainhug") (v "0.1.0") (h "0zmvsldbqq49nfabnhkj0nsnbz176dcykhdnhgi92ypcvq4kmv08")))

(define-public crate-brainhug-0.2.0 (c (n "brainhug") (v "0.2.0") (h "1zai9m5jy6gffwqhxqdv7ysf5xlrgjji4iwrcl7q1as5i7znzasn")))

(define-public crate-brainhug-0.2.1 (c (n "brainhug") (v "0.2.1") (h "18nhhfj13mdcvklrsvnm7r79cbl38yfr5d0rj5jlwcqmfmm4ixsf")))

(define-public crate-brainhug-0.2.2 (c (n "brainhug") (v "0.2.2") (h "16l3a7c0i758bk2a4dwr3xry92gqnvk8hi9iv6v316fcfpq9x3cf")))

(define-public crate-brainhug-0.3.0 (c (n "brainhug") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1d4ppjzpndnw5c464698pm630lbkgqm3rn0q0agxk46yj1cz0gl3")))

(define-public crate-brainhug-0.4.0 (c (n "brainhug") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1i0g05pmg0n3x17pfbnkffgrai88flw0mydpmlgdkffnbv45rdiw")))

(define-public crate-brainhug-0.5.0 (c (n "brainhug") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0dkwn2ibaxgcz0b2dhhs3s4paws0p8zcxgzzcmkihk9lx33mc7md")))

(define-public crate-brainhug-0.6.0 (c (n "brainhug") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1x2c0w33schpakg4vahw9i5k0jn5hk0vk9c5h68bnq7bsjszki3f")))

(define-public crate-brainhug-0.6.1 (c (n "brainhug") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0n43h3y345g9fnwp2i19w99zfmnz57b1wys5sd835bzs23bqvz4c")))

(define-public crate-brainhug-0.7.0 (c (n "brainhug") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0s53d0rsb688ph271nhmvpsm2vn5s41v1wn1jbpy14lir37d4624")))

