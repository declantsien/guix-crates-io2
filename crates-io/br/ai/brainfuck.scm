(define-module (crates-io br ai brainfuck) #:use-module (crates-io))

(define-public crate-brainfuck-0.2.0 (c (n "brainfuck") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "012qyjx4p723k08b7ka8wz39y1fwxfa3kw3rcg9x754mp3pignzy")))

(define-public crate-brainfuck-0.2.1 (c (n "brainfuck") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1y5khyvwixj6x2cndpvxsyslp8af5fj2yarik5via8m91rxwby46")))

