(define-module (crates-io br ai brain_flak_macro) #:use-module (crates-io))

(define-public crate-brain_flak_macro-0.1.0 (c (n "brain_flak_macro") (v "0.1.0") (h "1v9bka4zy4pslw59z0rh7ld83lqkzximpfhvd5k5sz01vzh5zppz")))

(define-public crate-brain_flak_macro-0.1.1 (c (n "brain_flak_macro") (v "0.1.1") (h "1ji8w026zq6dkpdfssd44cvlpnsjv89jsxw8l1sf0lfnp5mr7hhd")))

(define-public crate-brain_flak_macro-0.2.0 (c (n "brain_flak_macro") (v "0.2.0") (h "19c7ca04ijm3f922jlzrq9hvi668nrn0vs8wdkvdvrwrha9n257b")))

