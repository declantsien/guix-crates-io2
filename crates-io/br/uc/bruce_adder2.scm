(define-module (crates-io br uc bruce_adder2) #:use-module (crates-io))

(define-public crate-bruce_adder2-0.1.0 (c (n "bruce_adder2") (v "0.1.0") (h "0a0hp2bf2w69ia34zy7xvnpc2dwr9lfmna592f7j72v35fcaw0ga")))

(define-public crate-bruce_adder2-0.1.1 (c (n "bruce_adder2") (v "0.1.1") (h "104ypa6vfgv154r98i5j41rzk3ss37bqw16x6r5hmf67fj0bcsgd")))

