(define-module (crates-io br en bren) #:use-module (crates-io))

(define-public crate-bren-0.1.0 (c (n "bren") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)))) (h "007h2ggpg20x2ma94czhsnknsm20krw4ccfzbnzpjgyk5pzxkxbf")))

