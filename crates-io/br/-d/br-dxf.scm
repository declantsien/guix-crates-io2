(define-module (crates-io br -d br-dxf) #:use-module (crates-io))

(define-public crate-br-dxf-0.0.2 (c (n "br-dxf") (v "0.0.2") (d (list (d (n "br-crypto") (r "^0.1.18") (d #t) (k 0)) (d (n "dxf") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dzwklmw2xv4zl6bf66m5wzv96jazyczdc6qi1b7pjbxhwwzkh4s")))

(define-public crate-br-dxf-0.0.3 (c (n "br-dxf") (v "0.0.3") (d (list (d (n "dxf") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zjb24dnnvpqsnxixj7nybnkbibc9357ddhvi35l4g8p26c2d24n")))

(define-public crate-br-dxf-0.0.4 (c (n "br-dxf") (v "0.0.4") (d (list (d (n "dxf") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18l2cy9akqphh21lcpasayfqsgk0rcv9jwrdsbfvvz9035vdzd72")))

(define-public crate-br-dxf-0.0.5 (c (n "br-dxf") (v "0.0.5") (d (list (d (n "dxf") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yvnvmza569h5whfmgd6vzc82xxxydisgw1gp6fwgv26qkbjz606")))

(define-public crate-br-dxf-0.0.6 (c (n "br-dxf") (v "0.0.6") (d (list (d (n "dxf") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13y7r6w148srcmnbk4x0f4vxls9a2rdhza47pfvfw2cfggb9ksgx")))

