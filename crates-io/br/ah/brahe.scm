(define-module (crates-io br ah brahe) #:use-module (crates-io))

(define-public crate-brahe-0.0.1 (c (n "brahe") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rsofa") (r "^0.5.0") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1r92xns2pnl4lrcq240j569185gl8kpx2g65gm81ifbmmg8z5jjw")))

