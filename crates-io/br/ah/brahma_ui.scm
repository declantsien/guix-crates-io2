(define-module (crates-io br ah brahma_ui) #:use-module (crates-io))

(define-public crate-brahma_ui-0.1.0 (c (n "brahma_ui") (v "0.1.0") (h "1dp9rysrkiqifiq8xi7j56mpngdfl2rbhp7f118b5i2fjmqwni3y") (y #t)))

(define-public crate-brahma_ui-0.0.1 (c (n "brahma_ui") (v "0.0.1") (h "00hbwi99si56xhkrkjk5hmm7n9spbjgxa0bdmr9bkh4vkf8h9ynr")))

