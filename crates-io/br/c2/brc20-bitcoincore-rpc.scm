(define-module (crates-io br c2 brc20-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-brc20-bitcoincore-rpc-0.18.1 (c (n "brc20-bitcoincore-rpc") (v "0.18.1") (d (list (d (n "bitcoincore-rpc-json") (r "^0.18.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.16.0") (f (quote ("minreq_http"))) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1h4in59k17vl78b74hih1w6mhsl884hij750jpfay4di1z74wsjc")))

(define-public crate-brc20-bitcoincore-rpc-0.18.2 (c (n "brc20-bitcoincore-rpc") (v "0.18.2") (d (list (d (n "brc20-bitcoincore-rpc-json") (r "^0.18.1") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.16.0") (f (quote ("minreq_http"))) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "00jj9bpgv5pdmzzy5d3bl9m7p0ijj9dmvyamwipzvfvs6cjchcq1")))

(define-public crate-brc20-bitcoincore-rpc-0.18.3 (c (n "brc20-bitcoincore-rpc") (v "0.18.3") (d (list (d (n "brc20-bitcoincore-rpc-json") (r "^0.18.2") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.16.0") (f (quote ("minreq_http"))) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17kfgpj1nwyis2h5ss7v74jzp4x117r2rars2q46xi9k052dscsa")))

(define-public crate-brc20-bitcoincore-rpc-0.17.0 (c (n "brc20-bitcoincore-rpc") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "brc20-bitcoincore-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.16.0") (f (quote ("minreq_http"))) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m36kx6xqc6v4nmgfvall6hnvqb4qdjyhcyx5lh9nss0n0akh486")))

