(define-module (crates-io br c2 brc20-bitcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-brc20-bitcoincore-rpc-json-0.18.1 (c (n "brc20-bitcoincore-rpc-json") (v "0.18.1") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cylmclpxmh367dijhzbjhf70bx0sg58la1z08l81zx9hy0mxrzr")))

(define-public crate-brc20-bitcoincore-rpc-json-0.18.2 (c (n "brc20-bitcoincore-rpc-json") (v "0.18.2") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p7la84wp5zinvkg9xk99pyp88crc0jfhqs3kgmmmld9f242vl4i")))

(define-public crate-brc20-bitcoincore-rpc-json-0.17.0 (c (n "brc20-bitcoincore-rpc-json") (v "0.17.0") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06q21ny5ci8053hs7pg2laqlrdndraqhsljwxl2735995hq5sdf0")))

