(define-module (crates-io br r- brr-editor) #:use-module (crates-io))

(define-public crate-brr-editor-1.0.0 (c (n "brr-editor") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)) (d (n "words-count") (r "^0.1.6") (d #t) (k 0)))) (h "19b2m39z4869lhgcn7hq2aaa73w3n6ah3wprvvvylaq073lzfn3i")))

(define-public crate-brr-editor-1.0.2 (c (n "brr-editor") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)) (d (n "words-count") (r "^0.1.6") (d #t) (k 0)))) (h "1lxjmx9imhgckn4i03w0zmcpjdi35kkx72fkygwrjilqpfz3bim8")))

