(define-module (crates-io br ou brouter-client) #:use-module (crates-io))

(define-public crate-brouter-client-0.1.0 (c (n "brouter-client") (v "0.1.0") (d (list (d (n "gpx") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "07cs46nixjydmhqxy88lrnnq2clav1p8dsx5c12k5qwd4xrj6dwn")))

(define-public crate-brouter-client-0.1.1 (c (n "brouter-client") (v "0.1.1") (d (list (d (n "geo-types") (r ">=0.6") (d #t) (k 0)) (d (n "gpx") (r "^0.9") (d #t) (k 0)) (d (n "lazy-regex") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0jdwqnvkjlzxyi6bd5hsy9wlhirl6876r3xyxcql8jk9kx0i2677")))

(define-public crate-brouter-client-0.1.3 (c (n "brouter-client") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.12") (o #t) (d #t) (k 0)) (d (n "geo-types") (r ">=0.6") (d #t) (k 0)) (d (n "gpx") (r "^0.9") (d #t) (k 0)) (d (n "lazy-regex") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "12g3s8lqf5a8z1py30g6n8qzaqm01viihkfqbi076ih663l9s1bc")))

