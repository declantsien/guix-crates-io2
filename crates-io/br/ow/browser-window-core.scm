(define-module (crates-io br ow browser-window-core) #:use-module (crates-io))

(define-public crate-browser-window-core-0.0.0 (c (n "browser-window-core") (v "0.0.0") (d (list (d (n "browser-window-c") (r "^0.0.0") (d #t) (k 0)))) (h "04k5lxkd5i41am7apnm8l6dhn9x3f3lp199jbw84gsvgwv52syfg") (f (quote (("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.1 (c (n "browser-window-core") (v "0.0.1") (d (list (d (n "browser-window-c") (r "^0.0.3") (d #t) (k 0)))) (h "12vq75m7zj6grm9mxzkx4dj3lc80v1568r24fdzcq3fw6vrvjmcv") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.2 (c (n "browser-window-core") (v "0.0.2") (d (list (d (n "browser-window-c") (r "^0.0.4") (d #t) (k 0)))) (h "1ivlppwvql90yi0ad1g73rx2n485xpyd1sgj7yr1127xl9vlg5nf") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.3 (c (n "browser-window-core") (v "0.0.3") (d (list (d (n "browser-window-c") (r "^0.0.5") (d #t) (k 0)))) (h "01ybxzhrfb0zm9s48np6mh4h9r7bzdvffl4kv0sl7cnx1vmg06pi") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.4 (c (n "browser-window-core") (v "0.0.4") (d (list (d (n "browser-window-c") (r "^0.0.6") (d #t) (k 0)))) (h "0lmpaha9q82scfl4bk5qwbrr6a9f40v8g102qx8rlmf5li3m63hl") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.5 (c (n "browser-window-core") (v "0.0.5") (d (list (d (n "browser-window-c") (r "^0.0.7") (d #t) (k 0)))) (h "0fyp3vwdmhribcghkpnjc7s4rvdjl8xqnlfzs2ky6raimrphxd8p") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.0.6 (c (n "browser-window-core") (v "0.0.6") (d (list (d (n "browser-window-c") (r "^0.0.8") (d #t) (k 0)))) (h "1yqkrw1clygkcq1wc4bwyxha449m7ywn9abcaq6g6irf4nq9gw4p") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.1.0 (c (n "browser-window-core") (v "0.1.0") (d (list (d (n "browser-window-c") (r "^0.0.9") (d #t) (k 0)))) (h "05v3rf7mlnbbbdxv9jarmpyy604crll61f89k7a3ym4sk6grzwg3") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.2.0 (c (n "browser-window-core") (v "0.2.0") (d (list (d (n "browser-window-c") (r "^0.0.10") (d #t) (k 0)))) (h "106kmv592l00n9fhafvy9ilibw1dbkzbd4w7vvipaysad9smsfgf") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

(define-public crate-browser-window-core-0.2.1 (c (n "browser-window-core") (v "0.2.1") (d (list (d (n "browser-window-c") (r "^0.0.11") (d #t) (k 0)))) (h "06k36m79cr8mxm96dm71hfmns1xhh3qqdakf0z5w6mmrxfb46yk0") (f (quote (("gtk" "browser-window-c/gtk") ("default" "cef") ("cef" "browser-window-c/cef"))))))

