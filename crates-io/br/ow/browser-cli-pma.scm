(define-module (crates-io br ow browser-cli-pma) #:use-module (crates-io))

(define-public crate-browser-cli-pma-0.1.0 (c (n "browser-cli-pma") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "0dbczcagcsdyv2pjh9fpp709fsdjva16yywrfpfdvqywp7x6ax8w")))

