(define-module (crates-io br ow browser-window-ffi) #:use-module (crates-io))

(define-public crate-browser-window-ffi-0.0.0 (c (n "browser-window-ffi") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09mhnz91zn6kqzwka6xbm0jav8ny1isbzcj3ds29xpz2ix87s4vc")))

(define-public crate-browser-window-ffi-0.0.1 (c (n "browser-window-ffi") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "090i3nis49rh64fx3i12dx563g1m6gbvjp1q9q6klxsg9jxalwkf")))

(define-public crate-browser-window-ffi-0.0.2 (c (n "browser-window-ffi") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1cc99c7jrcapzkd7i116wjz51ndja0xn2wd15iafbjq29baiy2bp")))

(define-public crate-browser-window-ffi-0.0.3 (c (n "browser-window-ffi") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1nngsi19wrdk8sydnaxlmln97l60adz391kvaya1h2z31xh7calw")))

(define-public crate-browser-window-ffi-0.0.4 (c (n "browser-window-ffi") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0k5v4kkb9cvqb6fzvz1lgjvdczvvwhwc1jdxsi67v5gjic38rxq8")))

(define-public crate-browser-window-ffi-0.0.5 (c (n "browser-window-ffi") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qbccjb1pymahdlniq3g5qc2f20m91zvh4mhl7lcw6nxa0anl61r")))

(define-public crate-browser-window-ffi-0.1.0 (c (n "browser-window-ffi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08p5k4yxm03hvs8s0c6a2frkk1wamhkrxjjjm4d7nx79ymgc36jc") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.1.1 (c (n "browser-window-ffi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1d0h96jfv6fxrh3720n9yi4hrqk57k860v6xhslmig5p5das6q2s") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.1.2 (c (n "browser-window-ffi") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hskfvfkqqayh4q2ag86h3jxzii9j7kvjs3laczdnz684lqpbwss") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.2.3 (c (n "browser-window-ffi") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0k315wv31yi90aviaq6qrjp9sns67b4a84hgl71ghaphpxx8972z") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.3.0 (c (n "browser-window-ffi") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pkxi9lx0vg00vr0g761m7rf3kci9ahbvkxsnfixfxj868ypbm5i") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.4.0 (c (n "browser-window-ffi") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vmn7gnlildrc8n2l6ps6m8sclcp6ixy19qb4s9aamm5g9p1nznq") (f (quote (("edge"))))))

(define-public crate-browser-window-ffi-0.4.1 (c (n "browser-window-ffi") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0nas8inwswbsq6321pnr6907hgrq7rrcbi12slifjnkr0xkv0bz1")))

