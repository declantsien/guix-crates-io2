(define-module (crates-io br ow browsers) #:use-module (crates-io))

(define-public crate-browsers-0.1.0 (c (n "browsers") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "0anqk838kza2nyh2cz2yjlcqlq2wxy8v6rkx8mv3nl5mwww97nkb")))

(define-public crate-browsers-0.1.1 (c (n "browsers") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "1ig7bvhkq5z2czchbvbpzpy18yljh72lqxbvq1y6mkhq1cs4xi4f")))

(define-public crate-browsers-0.1.2 (c (n "browsers") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "14xa5asql7mx7cq66m56gbxk93la0ah4xd31mh4gr0v6153hdmsr")))

