(define-module (crates-io br ow browser-window-c) #:use-module (crates-io))

(define-public crate-browser-window-c-0.0.0 (c (n "browser-window-c") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mlcd5qirnbx258mc98gmy2jfmhlsck6m0rvc93yqbanx20kbf4y") (f (quote (("gtk") ("cef"))))))

(define-public crate-browser-window-c-0.0.1 (c (n "browser-window-c") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0h7aipiwfl145ff7g62f3d9qwwr20vbmhw846s53jg5qch1l1jzz") (f (quote (("gtk") ("cef"))))))

(define-public crate-browser-window-c-0.0.2 (c (n "browser-window-c") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1348savlfyf84d97304mgmjzzpiqdynn37b78gh1ai53hgq5s763") (f (quote (("gtk") ("cef"))))))

(define-public crate-browser-window-c-0.0.3 (c (n "browser-window-c") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dfq9kvvskifdn76q065m1zx8f8vxk6pljr4q1629x9iajvv1ivv") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.4 (c (n "browser-window-c") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k2krshrj4d2q9kiw7y0v1ipn04xyddx1qcavsklbkjx7zbr0mxs") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.5 (c (n "browser-window-c") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "098wk69mm68q583zc8gr4b5zisrcm2gvi4jr004mwlp4bgagsl4f") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.6 (c (n "browser-window-c") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19fpq985jki4xslbwfhc2sf0q9i2g8acllgpdnq0m3v913vfg97z") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.7 (c (n "browser-window-c") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qhxdkj7l1j9w6lwnmgzzvfkzznbj479grnby48pcbnrsiqp8vs9") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.8 (c (n "browser-window-c") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1iwfvgkc092wnd2f1adj0awz1w3cg87klwz5znzilrfdssp0mzrn") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.9 (c (n "browser-window-c") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j9kxkqx56bbzy3nf1wsf5x5kfnjwjv07db8vydqdd8rsplrv3bj") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.10 (c (n "browser-window-c") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15cv2knqb6psbskmmp4zw89x1l44z57f3zn0j7i8f5527gc5vywd") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.11 (c (n "browser-window-c") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lz945gb0f6rdr4l829ghcyys300cflkjb0anrhxga6zrpn2b7ff") (f (quote (("gtk") ("default" "cef") ("cef"))))))

(define-public crate-browser-window-c-0.0.12 (c (n "browser-window-c") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dx05d1c5481hcldrx5zj62c2shhnd75x21b36hpm6f6x8z34zsz") (f (quote (("cef"))))))

(define-public crate-browser-window-c-0.0.13 (c (n "browser-window-c") (v "0.0.13") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ngn4gd0vzw44wqryv6186v60570g8ynha6b8z70cd3nhpbsnrh3") (f (quote (("edge") ("cef") ("bundled"))))))

(define-public crate-browser-window-c-0.1.0 (c (n "browser-window-c") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l9mqillacxmkc5nmz2wbmsj1g2c5x3sd9yqv3yb416g8w1f23f9") (f (quote (("edge") ("cef") ("bundled"))))))

(define-public crate-browser-window-c-0.2.0 (c (n "browser-window-c") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hfvx7mk4qng2l9xswipgi8px97jkj5xbckvip5v2xas74mlpq7l") (f (quote (("edge2") ("cef"))))))

(define-public crate-browser-window-c-0.2.1 (c (n "browser-window-c") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0iiaz294hhh8nqfqdj7nca4g63zm8l8z15wcs6c33pad4xq2jndx") (f (quote (("edge2") ("cef"))))))

(define-public crate-browser-window-c-0.2.2 (c (n "browser-window-c") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jl866mppa2iqs2k52krg5zs4v1h1fg1x7xqa9xi0sb1i26v286w") (f (quote (("edge2") ("cef"))))))

(define-public crate-browser-window-c-0.3.0 (c (n "browser-window-c") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qiyqqwpjwv5xi245s5wyssx0011pb5108mfcf41jb93zghwhr57") (f (quote (("edge2") ("cef"))))))

(define-public crate-browser-window-c-0.3.1 (c (n "browser-window-c") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1y5g92fjdc1hx43frwxbc7lvv5saxf3w41gpwffnxvhrfr2afzvj") (f (quote (("edge2") ("cef"))))))

(define-public crate-browser-window-c-0.3.2 (c (n "browser-window-c") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b1pj95yf7y6z4nvlw4yr479mifxc997vyj45idccp3hdbqnl21g") (f (quote (("edge2") ("cef"))))))

