(define-module (crates-io br ow browser) #:use-module (crates-io))

(define-public crate-browser-0.0.2 (c (n "browser") (v "0.0.2") (h "02hp507bsni3yp2srv075mz3zhywf13576wvxssjwngq870srppa") (y #t)))

(define-public crate-browser-0.1.0 (c (n "browser") (v "0.1.0") (h "03867zy8ink509xjn256prh5f7chl44j49y75yc4fdjvvzzgkdx9") (y #t)))

(define-public crate-browser-0.0.0 (c (n "browser") (v "0.0.0") (h "1jw6xa5m1350wadcwsskvjrvn24kppkkxcbkaqd4aqfsqjdxxlxr")))

