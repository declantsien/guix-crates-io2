(define-module (crates-io br ow brownstone) #:use-module (crates-io))

(define-public crate-brownstone-1.0.0 (c (n "brownstone") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)))) (h "1gz02inalcln327mh3bpx71yrqbpjl2vh1kv2q26wvm5z20lc0m7")))

(define-public crate-brownstone-1.0.1 (c (n "brownstone") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)))) (h "1gfiwcgss3d64a8gdp3ggwxb2j7l21kk162lj8g1cyblhwj35f43")))

(define-public crate-brownstone-1.1.0 (c (n "brownstone") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)))) (h "13cds4szl48g1znpymil3wfn6yy8d3xldc5yrjai6kzkk09sc3h3") (f (quote (("std"))))))

(define-public crate-brownstone-2.0.0 (c (n "brownstone") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)))) (h "0q33w2ybpysgy806lrblrivsniaiznxik5m9av4zy1mhaxyd2m12")))

(define-public crate-brownstone-3.0.0 (c (n "brownstone") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)))) (h "08kzq5dmf1cl8pzhp6wm8wa3wsicrc4za8zjvjzi3s2kz7j9x0y5")))

