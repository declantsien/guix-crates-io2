(define-module (crates-io br ow browser-panic-hook) #:use-module (crates-io))

(define-public crate-browser-panic-hook-0.1.0 (c (n "browser-panic-hook") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "Document" "HtmlElement" "Window"))) (d #t) (k 0)))) (h "1xzlisa5wzbabhh12p5dfzsp91921fmx1v51hdrd17j24z6q6rys")))

(define-public crate-browser-panic-hook-0.2.0 (c (n "browser-panic-hook") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "Document" "HtmlElement" "Window"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 2)))) (h "17i99mjaqih9fyyj1rza7ssdf563wdn98fvdgbrfvc7lz86khn6z")))

