(define-module (crates-io br ic brickdb) #:use-module (crates-io))

(define-public crate-brickdb-0.1.0 (c (n "brickdb") (v "0.1.0") (h "1hljs3vfl8jcdpa7j7zar2b2c5934j368wgyp01iycb1k195091a") (y #t)))

(define-public crate-brickdb-0.1.0-alpha.1 (c (n "brickdb") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bloom") (r "^0.3.2") (d #t) (k 0)) (d (n "bson") (r "^2.6.0") (f (quote ("serde_with"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w62hqrkka8wv5wb6rb3svngiapfj6a0a90ywm6ax2b50rdakfh7")))

