(define-module (crates-io br ic brick_bird) #:use-module (crates-io))

(define-public crate-brick_bird-1.2.0 (c (n "brick_bird") (v "1.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit" "bevy_asset" "bevy_audio" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "bevy_ui" "multi-threaded" "x11" "webgl2" "mp3"))) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "006gn91dmpga7x56yfbm54fp62nkii5fs482669npl5h99pl5mrz") (f (quote (("dlink" "bevy/dynamic_linking"))))))

