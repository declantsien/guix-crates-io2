(define-module (crates-io br ic bricks) #:use-module (crates-io))

(define-public crate-bricks-0.1.0 (c (n "bricks") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo" "string"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lego-powered-up") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1mf5v96sbvl0jchvkdkxmb1ybh18i94pv4ypnx7q1n06gs0s4kaa")))

