(define-module (crates-io br -e br-excel) #:use-module (crates-io))

(define-public crate-br-excel-0.0.1 (c (n "br-excel") (v "0.0.1") (d (list (d (n "df-file") (r "^0.1.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "1np60hgpx4h5a5hr9g2h587gfic46wb9xfq9mymvxdx78d249js0")))

(define-public crate-br-excel-0.0.2 (c (n "br-excel") (v "0.0.2") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "1y5k0b2rsww8i7kb9cq84lpagb3idgwiqfxswqm6yz48chxj2yc6")))

(define-public crate-br-excel-0.0.3 (c (n "br-excel") (v "0.0.3") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "0kvpr2697izhaalqskvsrg9b4k12mn1hh99qb0a9mzfrf8gzqcj0")))

(define-public crate-br-excel-0.0.4 (c (n "br-excel") (v "0.0.4") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "1sp8lsxrwnyamdz4f3852via63c15l2wwarlxgm7xjmpyk2g7vf8")))

(define-public crate-br-excel-0.0.5 (c (n "br-excel") (v "0.0.5") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "0n50zvdwijn43c9xrrcs32yzmx52hh6p04pz1zl32cavsvhy3z8r")))

(define-public crate-br-excel-0.0.6 (c (n "br-excel") (v "0.0.6") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "0dwq8pbbhq4xh5sphj2ivxv24mylddybjkr55vjz5qnnx8ns1qv8")))

(define-public crate-br-excel-0.0.8 (c (n "br-excel") (v "0.0.8") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "0pvbmxq7bkfp6irjrb8bvwcw68i1r5icx292b6ac96yshc4pnywc")))

(define-public crate-br-excel-0.0.9 (c (n "br-excel") (v "0.0.9") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "04jrnn8wqzanfmi5sv7b1qxx27nba3vljy9362jnk6b5srbmflsg")))

(define-public crate-br-excel-0.1.0 (c (n "br-excel") (v "0.1.0") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.0.0") (d #t) (k 0)))) (h "0xc818sbmrx6lfv1bl0f1r3yj5l8x2jncyrbhqnwha338pr89pmf")))

(define-public crate-br-excel-0.1.1 (c (n "br-excel") (v "0.1.1") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.0.0") (d #t) (k 0)))) (h "1ypqxim0f7dxabaykb1ppn0ik9nfxyqbm0j5mlng96469v4g1hn3")))

(define-public crate-br-excel-0.1.2 (c (n "br-excel") (v "0.1.2") (d (list (d (n "br-file") (r "^0.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.0.0") (d #t) (k 0)))) (h "0gfcwzr59h08q5ljjld7nlcy9cj40afqla92c0nyp660h51nc0xj")))

(define-public crate-br-excel-0.1.3 (c (n "br-excel") (v "0.1.3") (d (list (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.0") (d #t) (k 0)))) (h "0f0mr2d90zgmlxw5qx8160gga5j2xlsy0gjnxqnc9d15wyrbazc2")))

(define-public crate-br-excel-0.1.4 (c (n "br-excel") (v "0.1.4") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.3") (d #t) (k 0)))) (h "16iimv8lf2qh37ic0c291i5v0s0zs2rbsd8pvgq63y5l07i6qcbq")))

(define-public crate-br-excel-0.1.5 (c (n "br-excel") (v "0.1.5") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.3") (d #t) (k 0)))) (h "1a5y9k7zhz08869q5542q41lzh3vzwk1967dp8xvmqa33fpyavrd")))

(define-public crate-br-excel-0.1.6 (c (n "br-excel") (v "0.1.6") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.5") (d #t) (k 0)))) (h "0ijcjz1qnizks4y1fjp5mp5s65xzp5a4wfj37x82gi6qh3mknpz7")))

(define-public crate-br-excel-0.1.7 (c (n "br-excel") (v "0.1.7") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.5") (d #t) (k 0)))) (h "00nv9x01392ibil6gnx2ymsm2jdlj98cvkf5fzgsqcl5hsmy8j4v")))

(define-public crate-br-excel-0.1.8 (c (n "br-excel") (v "0.1.8") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.5") (d #t) (k 0)))) (h "1f5mrqykjcd0cy871pmrzy64c0b46nbv9wvdal085pcanras72as")))

(define-public crate-br-excel-0.1.9 (c (n "br-excel") (v "0.1.9") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.6") (d #t) (k 0)))) (h "1cfwd02h6kvzs6ldqxkpw5qry5za9akijla9m0bfd16afnvh7qrg")))

(define-public crate-br-excel-0.1.10 (c (n "br-excel") (v "0.1.10") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.6") (d #t) (k 0)))) (h "12q9jrmn7rw5yrlhvjnwvhrgwj2dgb58kbsp8w9q4xzyj3m5hwgs")))

(define-public crate-br-excel-0.1.11 (c (n "br-excel") (v "0.1.11") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "br-file") (r "^0.0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^1.2.6") (d #t) (k 0)))) (h "1zrb7dby3krc12flf2srw30f84gd2gxv47v55xkm8114xyic5fi9")))

