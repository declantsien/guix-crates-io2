(define-module (crates-io br f- brf-macro) #:use-module (crates-io))

(define-public crate-brf-macro-0.1.0 (c (n "brf-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "13hn3wz65jga73hfv1zxfv45fp5iz9x1m3xy73nyy0g10aplnl4a")))

(define-public crate-brf-macro-0.1.1 (c (n "brf-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "17ihfqj1y2l19nmyq8f624prrcgjg4yzdfk0v0v2kma454nqf0wi")))

(define-public crate-brf-macro-0.1.2 (c (n "brf-macro") (v "0.1.2") (d (list (d (n "braille-ascii") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0rkpnwl1zqzg41k4yini7rlx8f0gyns2q5g97xwm3w17mrcw6qwr")))

(define-public crate-brf-macro-0.1.3 (c (n "brf-macro") (v "0.1.3") (d (list (d (n "braille-ascii") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1ib0310fg9l2hwwh8dbmi9yfi30raj03pwwhgcgp87wyzywkfil4")))

