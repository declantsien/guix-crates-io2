(define-module (crates-io br as brasilapi) #:use-module (crates-io))

(define-public crate-brasilapi-0.1.0 (c (n "brasilapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1mwlrkr05km6h1z2k4mj7xww1vpcxhhm2znbn3ibr2jyhhyn2w79")))

(define-public crate-brasilapi-0.2.0 (c (n "brasilapi") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0smh226bp67bjfz33swycmpsrp5avpfa55pp8xsw7vg9hayz5cr3")))

(define-public crate-brasilapi-0.3.0 (c (n "brasilapi") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1qhxp9qrmpm0nh4flvdbq3mlfxddnzb4cbd7lag6mwbh7d5k86in")))

(define-public crate-brasilapi-0.4.0 (c (n "brasilapi") (v "0.4.0") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0y9jql4dmmim6h05lywn978hrfkn4lqikyw8jcr8db8c22ywgzps")))

(define-public crate-brasilapi-0.5.0 (c (n "brasilapi") (v "0.5.0") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1hjfxwsaya69jnxfxrxhkyfq8nlixgn3sxnyb13jfb3rnxpcpg64")))

(define-public crate-brasilapi-0.5.1 (c (n "brasilapi") (v "0.5.1") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1lzynqrrlv19bgqz7zz24l8575y066g079fzc85dsfw9iisj0irb")))

(define-public crate-brasilapi-0.5.2 (c (n "brasilapi") (v "0.5.2") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "06bqk45v5wxmjjihd5y94bv8ssvy3m4zg4v8xyx06gv8m6qzw0r7")))

