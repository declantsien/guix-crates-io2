(define-module (crates-io br as brassfibre) #:use-module (crates-io))

(define-public crate-brassfibre-0.1.0 (c (n "brassfibre") (v "0.1.0") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0jnvgnrycn0190l396p0riy6ik7wkzp26vmcjccbp1xqn7srxxmr")))

(define-public crate-brassfibre-0.2.0 (c (n "brassfibre") (v "0.2.0") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "nullvec") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "0jsb7jmsj3b0z0yxmf9j5w7nbhvv3wizihksigynlkwwpszkr6qq")))

