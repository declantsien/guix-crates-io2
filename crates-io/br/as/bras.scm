(define-module (crates-io br as bras) #:use-module (crates-io))

(define-public crate-bras-0.1.0 (c (n "bras") (v "0.1.0") (h "1f6r8jyapv3xb4dr96nff3y91n9yyxwqha2l3x5kran3iqn7hhxy")))

(define-public crate-bras-0.1.1 (c (n "bras") (v "0.1.1") (h "1lfwnw6qqjw8bnlfjvax8xf284rmwbdlvns3kk3h2z5jiakgm78w")))

(define-public crate-bras-0.1.2 (c (n "bras") (v "0.1.2") (h "0syvhz3pbbgsi84vsryb5n075qrjdicrxvxdph8mx15fpbj3m70p")))

