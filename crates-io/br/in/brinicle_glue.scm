(define-module (crates-io br in brinicle_glue) #:use-module (crates-io))

(define-public crate-brinicle_glue-1.0.0 (c (n "brinicle_glue") (v "1.0.0") (d (list (d (n "brinicle_kernel") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1dzdk69si77nfd78q2z9170jidx720n4gj55anj2hp7pwayp14hi")))

