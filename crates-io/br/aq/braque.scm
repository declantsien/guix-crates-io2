(define-module (crates-io br aq braque) #:use-module (crates-io))

(define-public crate-braque-0.1.0 (c (n "braque") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("jpeg_rayon" "png"))) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0cgm6r3rm2lq7ml8kr8gbpmq38aslqiias5mbjp3ljl3ws9bg4y4") (f (quote (("cli" "clap" "eyre"))))))

