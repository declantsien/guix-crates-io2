(define-module (crates-io br ut brute_forcing) #:use-module (crates-io))

(define-public crate-brute_forcing-0.1.0 (c (n "brute_forcing") (v "0.1.0") (h "0234acvjzjb9n0mxy7x8m09hs0j8n8vh2yyxv2m0gjzipiq8jwj8")))

(define-public crate-brute_forcing-0.1.1 (c (n "brute_forcing") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0gzj88v3jd6ki7xyl3v0pp6zm22hn949hgf8afmc3l46r0wc9gl7")))

