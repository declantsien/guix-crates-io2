(define-module (crates-io br ut bruteforus) #:use-module (crates-io))

(define-public crate-bruteforus-0.1.0 (c (n "bruteforus") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "color" "wrap_help"))) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 1)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "0vmxp2z4nvgpwxx6c8wa0kqdai9np7kcg73kahx4h2dz315izxph")))

