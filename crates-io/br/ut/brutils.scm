(define-module (crates-io br ut brutils) #:use-module (crates-io))

(define-public crate-brutils-0.1.0 (c (n "brutils") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00lvx45nam4n6id9k00az2hqiwr0dzgyrnqgqy463avd174552d2")))

(define-public crate-brutils-0.1.1 (c (n "brutils") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12nkv0xxjyslglk2pm7py1aclgkqh9xwxhybj6b26z2cld2sqa2c")))

(define-public crate-brutils-0.1.2 (c (n "brutils") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "04gzbpx61b3nf7s3r421dbkj27zfzsr2i6d9zdazcxmvzajdgrrq")))

(define-public crate-brutils-0.1.21 (c (n "brutils") (v "0.1.21") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "0vhlb56rmhkan8bax478vyfzw6hbs7r1c4s5z5bcf3264qxjgw8f")))

(define-public crate-brutils-0.1.22 (c (n "brutils") (v "0.1.22") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1vj3hi8w07fi3s72bgpggjf137w4piwqifi3di01yx3n9ifxjrji")))

(define-public crate-brutils-0.1.3 (c (n "brutils") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1s5cr2xirii06ldyplwp0j6h0mrg5m7kas09nsajhms6qb1y5qfk")))

(define-public crate-brutils-0.1.31 (c (n "brutils") (v "0.1.31") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1ys30ikdm6gfd8mww28f0ij32ngyicngkza5qwj6yxf5yggz6kmi")))

(define-public crate-brutils-0.1.4 (c (n "brutils") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "16xrf30mhdfyyhdkb960bl5rk68ihajwv7f595d7brqmyzndcjqn")))

(define-public crate-brutils-0.1.41 (c (n "brutils") (v "0.1.41") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "0419a6qyjykkr7n6qvf8585x9msc6jp4lcgfr6mxz8nars4mz7gv")))

(define-public crate-brutils-0.1.5 (c (n "brutils") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1w27910wj6y0v0vfdc39vfzs83q378jck267s4c4mxbr1fv8hq6r")))

(define-public crate-brutils-0.1.51 (c (n "brutils") (v "0.1.51") (d (list (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1f5h20i7md8w5a1kwm0ri4mssdfs00jpk4prkz0gd3s09hk52j9d")))

