(define-module (crates-io br ut brutemoji) #:use-module (crates-io))

(define-public crate-brutemoji-0.1.0 (c (n "brutemoji") (v "0.1.0") (h "1n9lj7fv3asv61fgap0141hkya84jflxs5yydabpmng59a94bn8c")))

(define-public crate-brutemoji-0.2.0 (c (n "brutemoji") (v "0.2.0") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1qkvv4676qcc6zgyh04g5crz5ybxxzh0rh8icfalr7nkwrvzx8qm")))

(define-public crate-brutemoji-0.3.0 (c (n "brutemoji") (v "0.3.0") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "15m34gba19nv2vbylshirwgipvqp7nc5gpj700ddajkcj944zhws")))

(define-public crate-brutemoji-0.3.1 (c (n "brutemoji") (v "0.3.1") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nd1pai6ffpwj5jpfqyc9j270v693p5x101hxn62y6xfrbdd4n57")))

(define-public crate-brutemoji-0.3.2 (c (n "brutemoji") (v "0.3.2") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "095si049306p04y8qg5aqgbmqk4hmvnw7rg2rafif8r7q10iqqxs")))

(define-public crate-brutemoji-0.4.0 (c (n "brutemoji") (v "0.4.0") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0cl4zblh90q08im2jqgmr7f1bs5jlipwqpp5vwlsi7dcv18qv7j4")))

(define-public crate-brutemoji-0.5.0 (c (n "brutemoji") (v "0.5.0") (d (list (d (n "image") (r "^0.23.8") (f (quote ("gif" "jpeg" "png" "webp"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xdpd5n7h1cj7yrf8ay8maggdsdvdlgc3vd1nhc45i8fipcsgk98")))

