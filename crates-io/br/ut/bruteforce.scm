(define-module (crates-io br ut bruteforce) #:use-module (crates-io))

(define-public crate-bruteforce-0.1.0 (c (n "bruteforce") (v "0.1.0") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "03726bycs53gfnlbcf7jdi6m7qv9zibvz2myppk2m6agq55g2fy7") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.1 (c (n "bruteforce") (v "0.1.1") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1kkn18ilck1d854j01h9z5z5532k4spcpn661ngb8yf6my04wfi3") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.2 (c (n "bruteforce") (v "0.1.2") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "19qvylkzv0hwls760n4l3h0kppjm6vgdzr0wkqniq4b00ff73kd0") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.3 (c (n "bruteforce") (v "0.1.3") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1zzmb4vrwnvs48xchx5ga82p7319jfrvlxkm1a2n1pyi6nm5zfgh") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.4 (c (n "bruteforce") (v "0.1.4") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1vk7i8wxw1kwb4bim36bf6h89ki2w753rm57qji97iln6x5lydp3") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.5 (c (n "bruteforce") (v "0.1.5") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "023r0fpb1c45c03qwbya5k662d0g4di22vd7s67cqj835dckvvin") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.6 (c (n "bruteforce") (v "0.1.6") (d (list (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "17iba4lhgblag07b8s09a25ldqsqffhsq0iawhq9abd3jjx8f2xs") (f (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1.7 (c (n "bruteforce") (v "0.1.7") (d (list (d (n "no-std-compat") (r "^0.3.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0rg43pgly94ca3rm6nn9d5j5wl60bxhd8xfb2qg9sb63z6lrl8dq") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.2.0 (c (n "bruteforce") (v "0.2.0") (d (list (d (n "bruteforce-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "no-std-compat") (r "^0.3.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "03g3bww5ps4pxapwg4sz7vmdwliqik4033xksxb4v1rza1dcgv89") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

(define-public crate-bruteforce-0.2.1 (c (n "bruteforce") (v "0.2.1") (d (list (d (n "bruteforce-macros") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "02crb90zvv6x36as6jn2kfa1b5skjy2y1avvlw7km9accsx3smfc") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

(define-public crate-bruteforce-0.3.0 (c (n "bruteforce") (v "0.3.0") (d (list (d (n "bruteforce-macros") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "121nich2qn9i4zqbwnij8kxkcz6pvn90blv9nm0cbnxdmjcinh8v") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

