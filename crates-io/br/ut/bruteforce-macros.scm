(define-module (crates-io br ut bruteforce-macros) #:use-module (crates-io))

(define-public crate-bruteforce-macros-0.2.0 (c (n "bruteforce-macros") (v "0.2.0") (d (list (d (n "no-std-compat") (r "^0.3.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10zrch5yl1ssk1ixhwdxrwb3z9m23if0zyjrzhnqjldjnici21pr") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

(define-public crate-bruteforce-macros-0.2.1 (c (n "bruteforce-macros") (v "0.2.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1zddc7nmkwvxx6x7nk7rfvqb3x33gbabqsgdgh0w972p1l22cvzh") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

(define-public crate-bruteforce-macros-0.3.0 (c (n "bruteforce-macros") (v "0.3.0") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)))) (h "12q7856m6jn59rkfgngbc0q49w8qc8lfl70i2x0fkbzyw0j7rxjs") (f (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

