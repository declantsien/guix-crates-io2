(define-module (crates-io br zo brzozowski) #:use-module (crates-io))

(define-public crate-brzozowski-0.1.0 (c (n "brzozowski") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (f (quote ("regex" "with-regex"))) (d #t) (k 2)))) (h "1incqa9ch5d4kdd3zh998jqh87wjm005yhvmbbp7infkzfkv8jsp") (y #t)))

(define-public crate-brzozowski-0.1.1 (c (n "brzozowski") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (f (quote ("regex" "with-regex"))) (d #t) (k 2)))) (h "1f9lghwmi5pfyf5385ym835hdp75267rnxqd8kh3rr8sfzph6bzk")))

(define-public crate-brzozowski-0.1.2 (c (n "brzozowski") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (f (quote ("regex" "with-regex"))) (d #t) (k 2)))) (h "1plsvgfa2nkyjq6rs2sqk3kzfchh8sg9vd8js6hclpxk8y50wwvy")))

