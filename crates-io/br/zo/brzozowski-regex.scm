(define-module (crates-io br zo brzozowski-regex) #:use-module (crates-io))

(define-public crate-brzozowski-regex-0.1.0 (c (n "brzozowski-regex") (v "0.1.0") (h "09khpj8bvrqqlf4dkvki28w0cmja8h9vk9r85iiidr040bidjlxk")))

(define-public crate-brzozowski-regex-0.2.0 (c (n "brzozowski-regex") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (d #t) (k 0)))) (h "0bn14x09h95zmg0zkds5a4aakywjkgqcfb3ary76rjyq0vnfkkcf")))

