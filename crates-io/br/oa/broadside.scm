(define-module (crates-io br oa broadside) #:use-module (crates-io))

(define-public crate-broadside-0.1.0 (c (n "broadside") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1aqdq0pq6zbf6fa57gxzndd9adnzwa6chkh4m8ds55s7sf1lfg9z")))

