(define-module (crates-io br oa broadword) #:use-module (crates-io))

(define-public crate-broadword-0.1.0 (c (n "broadword") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "11n4y2l0vcnvsq205pyd55968jgav6nfip1gy6qcrrzb8ia53hs5")))

(define-public crate-broadword-0.2.0 (c (n "broadword") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0v8lcsjma2sq9w8abwydkjgb7syqgki54rfbva317806raffdbg3")))

(define-public crate-broadword-0.2.1 (c (n "broadword") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "13s2j8fipd7s68bc3g1vwhgk8kpkzhjkq7y4995mbhhdvy97fs0a")))

(define-public crate-broadword-0.2.2 (c (n "broadword") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "140zwhm1x2q8sds1mgvgb7sppdjjzh74mm1i3p1w7cgjjxcafxg5")))

