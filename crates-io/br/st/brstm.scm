(define-module (crates-io br st brstm) #:use-module (crates-io))

(define-public crate-brstm-0.1.0 (c (n "brstm") (v "0.1.0") (d (list (d (n "binrw") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0wga9zfx2i18hdycml42jrlxh6ylncndvhlfxw6i8rc66fa7mzbr")))

(define-public crate-brstm-0.2.0 (c (n "brstm") (v "0.2.0") (d (list (d (n "binrw") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "07z65klrazwf8zdm45lqwqjbiz6ngyzpidckls3rb41zr97n1376")))

(define-public crate-brstm-0.3.0 (c (n "brstm") (v "0.3.0") (d (list (d (n "binrw") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "06g6fiy7qi23xhys5gpf23cmdm4i78i8w9v2zc7qnw4hzy9gg7gk")))

(define-public crate-brstm-0.4.0 (c (n "brstm") (v "0.4.0") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1zc9fzy13fk0hnwy4c5wpgqn57azkq3sv68gm5sry6kmpz7aaa4h")))

(define-public crate-brstm-0.4.1 (c (n "brstm") (v "0.4.1") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "031gcdw5g5jhia2s2gii92qbl27fx4fsr8frj9h130yzkaa99baz")))

(define-public crate-brstm-0.4.2 (c (n "brstm") (v "0.4.2") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0d2jn2bd7zmrph9xrxdpv1g9c89a380wpg3hi473fdwx1fp6rvw3")))

