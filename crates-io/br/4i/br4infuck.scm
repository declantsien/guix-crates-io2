(define-module (crates-io br #{4i}# br4infuck) #:use-module (crates-io))

(define-public crate-br4infuck-0.1.0 (c (n "br4infuck") (v "0.1.0") (h "0q52fhdk7gilx3qfhymgixhzrfxmzi7k7a1nnd0a4kmgwwnfkwhi")))

(define-public crate-br4infuck-0.1.1 (c (n "br4infuck") (v "0.1.1") (h "0m4rnhi0l76kjlw4n3p35ap687ybm5hva48af9c13f1w8cpym09r")))

(define-public crate-br4infuck-0.1.2 (c (n "br4infuck") (v "0.1.2") (h "1j15y4bag0119bgywj400sz6myxxwihc20b0drvara4zbkp5jyka")))

(define-public crate-br4infuck-0.1.3 (c (n "br4infuck") (v "0.1.3") (h "1q8gjljanvx6bdyir178karg6z9q89l2whg6wx1nh32n27klkx43")))

(define-public crate-br4infuck-0.1.4 (c (n "br4infuck") (v "0.1.4") (h "13s3s5hsz0q5f7i1mvs8l3wzsyzlrmkizhp9fdbrb11kbyd4xm0v")))

(define-public crate-br4infuck-0.1.5 (c (n "br4infuck") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1x2wjd0xc1qrn408vq3pi4vc0w1jsfk5ypzcjlkcn2b0aszvg5k6")))

