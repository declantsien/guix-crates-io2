(define-module (crates-io br ad brads_leet_code_string_to_integer) #:use-module (crates-io))

(define-public crate-brads_leet_code_string_to_integer-0.1.0 (c (n "brads_leet_code_string_to_integer") (v "0.1.0") (h "1nz3633jyzprzfi2cc7kannyk8d5nl95aci7bv7n8mmj1ccydaqb")))

(define-public crate-brads_leet_code_string_to_integer-0.1.1 (c (n "brads_leet_code_string_to_integer") (v "0.1.1") (h "197qv0clims998cwnvzqxkl1f74n1wkq2bsiywv28ikl9v71zmi9")))

