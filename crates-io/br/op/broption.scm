(define-module (crates-io br op broption) #:use-module (crates-io))

(define-public crate-broption-0.1.0 (c (n "broption") (v "0.1.0") (h "16a39qgrw1rmr807darzra0i0iqkc05lkqmvwss0gbw7r8n2094y")))

(define-public crate-broption-0.1.1 (c (n "broption") (v "0.1.1") (h "0xyl977grl8ln79hhfnpx6qr7ggi4d0yi6zxhni5j8cry6m3vaqj")))

