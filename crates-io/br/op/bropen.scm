(define-module (crates-io br op bropen) #:use-module (crates-io))

(define-public crate-bropen-1.0.0 (c (n "bropen") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "04hy28phmd6gcvjg29n8hskvdz95hz9ncq7ibapnknnzl5gy5c81")))

(define-public crate-bropen-2.0.0 (c (n "bropen") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "0qiq9nf8gqw2y81yvpvqhpr2nsdcvrf0xjslb863br1nrllq5ll4")))

