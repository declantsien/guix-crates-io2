(define-module (crates-io br di brdiff) #:use-module (crates-io))

(define-public crate-brdiff-0.1.0 (c (n "brdiff") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1n4zkvmamabdikqfbfqh8ksaxbmx9klpjm7cfmlvflia70s0im2l")))

(define-public crate-brdiff-0.2.0 (c (n "brdiff") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "0qh5b3jzf01kfc9af4rsrnn52rqbdrfdmwz1s4714kcr1gvbndj7")))

