(define-module (crates-io br il brillig) #:use-module (crates-io))

(define-public crate-brillig-0.18.1 (c (n "brillig") (v "0.18.1") (d (list (d (n "acir_field") (r "^0.18.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "04r776z02d2ljjjkj72pxykwj43006cmilyilfri9988qh0mqwsh") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.18.2 (c (n "brillig") (v "0.18.2") (d (list (d (n "acir_field") (r "^0.18.2") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "05c5c7i344sxdg95dribqgmvfnbk3w3ssd3pjig92dbpllakdj4j") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.19.0 (c (n "brillig") (v "0.19.0") (d (list (d (n "acir_field") (r "^0.19.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1riaivb45h4lbnsxd1zf4g0r2fpxn5smz58f7hx3yz1ls6ipsr1h") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.19.1 (c (n "brillig") (v "0.19.1") (d (list (d (n "acir_field") (r "^0.19.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yq9mrwjj5w9dsjb7sz21kcbli2s9db70sh713d84y10x8c9lcr3") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.20.0 (c (n "brillig") (v "0.20.0") (d (list (d (n "acir_field") (r "^0.20.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q0ffz9crl7znh9zgi7kfxkjblqm2cfqb8iqfrfimk0bgj59w4rn") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.20.1 (c (n "brillig") (v "0.20.1") (d (list (d (n "acir_field") (r "^0.20.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c3jj28dzp3sny84zyn866xzhddpva1dvnz8vcw169gp5cggzj0d") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.21.0 (c (n "brillig") (v "0.21.0") (d (list (d (n "acir_field") (r "^0.21.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l7rizlimccl8scjfb2xwnq1gdryy1msna26qw8ai0gm158g694x") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.22.0 (c (n "brillig") (v "0.22.0") (d (list (d (n "acir_field") (r "^0.22.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aw3q81pgvs1j1fp51pf94lywgn2dfy69pg434jzr5idgpgz6kfn") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.23.0 (c (n "brillig") (v "0.23.0") (d (list (d (n "acir_field") (r "^0.23.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "08cgrczz32s5bvbl80pvzp6kyvwpdj5clpawdnd0sji60m6y8pyx") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.24.0 (c (n "brillig") (v "0.24.0") (d (list (d (n "acir_field") (r "^0.24.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qi17sg67p6aqn5ryzycsw6fgjzhx6b0yw6919d3l63gqzv376xl") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.24.1 (c (n "brillig") (v "0.24.1") (d (list (d (n "acir_field") (r "^0.24.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k2qmiavz5yw1bypsqp2rhxf185kppp1jr8h6fk6x07wl070w8wb") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.25.0 (c (n "brillig") (v "0.25.0") (d (list (d (n "acir_field") (r "^0.25.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ifyv5pvavgfqcjzw857vw7kvv08c9z5m4wv2yhw3vfsbvbs7am") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.26.0 (c (n "brillig") (v "0.26.0") (d (list (d (n "acir_field") (r "^0.26.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kyqfkafj2m4h9c2fwzjsq6bjm665c6ph3jbm29nl2qvjvfdf1gg") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.26.1 (c (n "brillig") (v "0.26.1") (d (list (d (n "acir_field") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i6pafwappb3600a4xrws2306252pra7d41mqa2xfnj0hq73i2j4") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.27.0 (c (n "brillig") (v "0.27.0") (d (list (d (n "acir_field") (r "^0.27.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zrwd41wb6p2xnab27b41gk0lcnpxxavd1dvgj98zqgj3yaxd5z2") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.37.1 (c (n "brillig") (v "0.37.1") (d (list (d (n "acir_field") (r "^0.37.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "15wzc1y1i4q67z3vbk7fbiw9m30rrbjfwgijxyssl5vgs0n1gs4j") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.38.0 (c (n "brillig") (v "0.38.0") (d (list (d (n "acir_field") (r "^0.38.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wxzy3qdfd8jq08gjn4jzzxbffbc8bj7n0q9mny55jhh2mvwmih4") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.66")))

(define-public crate-brillig-0.39.0 (c (n "brillig") (v "0.39.0") (d (list (d (n "acir_field") (r "^0.39.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bsc2zijs5danjmmb1zav97xikqzfbc8691d0n1617x2gwizaqfg") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.71.1")))

(define-public crate-brillig-0.41.0 (c (n "brillig") (v "0.41.0") (d (list (d (n "acir_field") (r "^0.41.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "12wx8n2whb4kx6hcvvabq52rmkhxr4ml8q64gpxv8d5ycl10vjd8") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.73.0")))

(define-public crate-brillig-0.42.0 (c (n "brillig") (v "0.42.0") (d (list (d (n "acir_field") (r "^0.42.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jjfsrnmpbm865kbnh38jrrkvwnakw6jl1p33q2h066bxdl5i0cn") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.73.0")))

(define-public crate-brillig-0.43.0 (c (n "brillig") (v "0.43.0") (d (list (d (n "acir_field") (r "^0.43.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yih8k9nimnzf1nbflkgkix1hv7wx5a3hhnwkyjzypwv9xj5n10x") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.73.0")))

(define-public crate-brillig-0.44.0 (c (n "brillig") (v "0.44.0") (d (list (d (n "acir_field") (r "^0.44.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sbc3z37j7fwsnq9rm0xp6zpfrw4mdmq35v1mq6rrj04qlwnzkng") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.74.1")))

(define-public crate-brillig-0.45.0 (c (n "brillig") (v "0.45.0") (d (list (d (n "acir_field") (r "^0.45.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "14n9c64fsz0wzssdcvd4mdqr9amd3q678i12nk2sfx9sqfm3y8hf") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.74.1")))

(define-public crate-brillig-0.46.0 (c (n "brillig") (v "0.46.0") (d (list (d (n "acir_field") (r "^0.46.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rbbyvbsv7lb2p8pzrha494pigwikcfibaz1ybad21b0r7hnim0x") (f (quote (("default" "bn254") ("bn254" "acir_field/bn254") ("bls12_381" "acir_field/bls12_381")))) (r "1.74.1")))

