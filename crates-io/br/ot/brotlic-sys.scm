(define-module (crates-io br ot brotlic-sys) #:use-module (crates-io))

(define-public crate-brotlic-sys-0.1.0 (c (n "brotlic-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vifx4a082j8kd54bnky6j47hdzbrnpyy2j4dcawm3zzhbpz7db4")))

(define-public crate-brotlic-sys-0.2.0 (c (n "brotlic-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1f7s342d01q9wv1pcjgavlg90n36xbgdx4vjypali1q3552zzx1x")))

(define-public crate-brotlic-sys-0.2.1 (c (n "brotlic-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wk4nvsa3f3phja7czx6skg3ga7j4dywkcpwpgdrs9ibnj8k90y3")))

(define-public crate-brotlic-sys-0.2.2 (c (n "brotlic-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0x75awdz39bhmp1n3glihm2c5x9sa2x6dkskj0s5cyy95g3cbpmg")))

