(define-module (crates-io br ot brotli-no-stdlib) #:use-module (crates-io))

(define-public crate-brotli-no-stdlib-1.0.0 (c (n "brotli-no-stdlib") (v "1.0.0") (d (list (d (n "alloc-no-stdlib") (r "^1.0") (d #t) (k 0)))) (h "17rf8wjhj7ih38zi5y7kmnddmhgbkkhjnfp0hjhzihx2vgql4b63")))

(define-public crate-brotli-no-stdlib-1.0.1 (c (n "brotli-no-stdlib") (v "1.0.1") (d (list (d (n "alloc-no-stdlib") (r "^1.0") (d #t) (k 0)))) (h "0grpgx602686sjxw3378sc6l0n3s305rk7kml2mbkdrc7s2yrdhl")))

(define-public crate-brotli-no-stdlib-1.0.3 (c (n "brotli-no-stdlib") (v "1.0.3") (d (list (d (n "alloc-no-stdlib") (r "^1.0") (d #t) (k 0)))) (h "159h79nfb4l1i7p0bx0k6rdkk5xqgp66y293whkscmif8zxx15hm") (f (quote (("disable-timer"))))))

(define-public crate-brotli-no-stdlib-1.0.4 (c (n "brotli-no-stdlib") (v "1.0.4") (d (list (d (n "alloc-no-stdlib") (r "~1.0") (d #t) (k 0)))) (h "1f6m06igd0awmbh43247xqfdzsbiq6j6nal0sq6ry5r11c8q40g2") (f (quote (("disable-timer"))))))

