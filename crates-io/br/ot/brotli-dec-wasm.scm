(define-module (crates-io br ot brotli-dec-wasm) #:use-module (crates-io))

(define-public crate-brotli-dec-wasm-2.0.1 (c (n "brotli-dec-wasm") (v "2.0.1") (d (list (d (n "brotli-decompressor") (r "^2.3.4") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "18xzgkhrr3qxkqv7szqkbdx2vf0jlwkg7xndj1wp1qxsdsrs2ji2")))

(define-public crate-brotli-dec-wasm-2.3.0 (c (n "brotli-dec-wasm") (v "2.3.0") (d (list (d (n "brotli-decompressor") (r "^4.0.0") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "03g2dqbxxvbz80vr386hdnfdf0x1nwh95m7dck0pa6s3pw2rym2r")))

