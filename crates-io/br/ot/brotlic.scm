(define-module (crates-io br ot brotlic) #:use-module (crates-io))

(define-public crate-brotlic-0.1.0 (c (n "brotlic") (v "0.1.0") (d (list (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)))) (h "11gvzvf6dvwryv1cb7ma600rz9q0hnvazgryz0ksavsyzf3vc133") (y #t)))

(define-public crate-brotlic-0.1.1 (c (n "brotlic") (v "0.1.1") (d (list (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)))) (h "07lyvhzvp1y640cf0cr1y08nj0jdsc2jhjryca2vzl73xnkssf6k")))

(define-public crate-brotlic-0.2.0 (c (n "brotlic") (v "0.2.0") (d (list (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0d68qd3z6jfwjzdnwgsgamqrr83xhlcpih9w6dgzq4rrnnq6gfzr")))

(define-public crate-brotlic-0.2.1 (c (n "brotlic") (v "0.2.1") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1swxziqrdgpwq1dinrfkv3q29syj8941k3j3pyyp7jssc49ii8y8")))

(define-public crate-brotlic-0.2.2 (c (n "brotlic") (v "0.2.2") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1xr0h664nl59swljivag8rl68mv4w9x1gavzbnn0sh32dg2xn7qq")))

(define-public crate-brotlic-0.3.0 (c (n "brotlic") (v "0.3.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "18w0i27j3wxmc4m4l130vjnsvslzrr0pkp0ib5i8my8d89vjaxk5")))

(define-public crate-brotlic-0.4.0 (c (n "brotlic") (v "0.4.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1q2wgnpgk819fcwlk3ciixik3rq97jhc8jyqqf3pk7hj0dp55d8h")))

(define-public crate-brotlic-0.4.1 (c (n "brotlic") (v "0.4.1") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1rn2qzmjdbqlz7j3fxm8yckws1rkxfaimr0yz9q1kqd7z7fy2pjk")))

(define-public crate-brotlic-0.5.0 (c (n "brotlic") (v "0.5.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0ylsl8m23h2x9095mdcrjjgb66ny7r0sdx0cq25ib43ha5h5iki8")))

(define-public crate-brotlic-0.6.0 (c (n "brotlic") (v "0.6.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1x6cfdw7ls7rldq36mlwp8pr0r695dylwxahbb1dinpdhsv37xcd")))

(define-public crate-brotlic-0.6.1 (c (n "brotlic") (v "0.6.1") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0vygdal39bgy6klnpxsmbiwsdg0yqg0m87hqmcrkn83b2prc8mh5")))

(define-public crate-brotlic-0.6.2 (c (n "brotlic") (v "0.6.2") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1d80lgb3ikqhg7lp6phlskp5cw0dazgml7nhhax7j6xviiyzaqvd")))

(define-public crate-brotlic-0.7.0 (c (n "brotlic") (v "0.7.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0k940dgpabrv5yvpq1ssgbgwv8s59qmbs992s7r7m5vns4q5mc1l")))

(define-public crate-brotlic-0.8.0 (c (n "brotlic") (v "0.8.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1n88wsshwx41jjsb9gl7704m5xncahx0is62ln7hanq12awy7714")))

(define-public crate-brotlic-0.8.1 (c (n "brotlic") (v "0.8.1") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.23") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0nsfhmnq14pczkazmyyhnhi6ahnss2y0c4mwfi96zq3im18a7mf8")))

(define-public crate-brotlic-0.8.2 (c (n "brotlic") (v "0.8.2") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 2)) (d (n "brotlic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.23") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1066zlgdpvsz4bvbjfiwldmzv5j6vcmzl2xm69n01w1a61pzalpm")))

