(define-module (crates-io br ot brotli-sys) #:use-module (crates-io))

(define-public crate-brotli-sys-0.1.0 (c (n "brotli-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j52p2fyl16gz3da2n3z4ad1bwk024n09wlpjv32czmklvjwklf5")))

(define-public crate-brotli-sys-0.1.1 (c (n "brotli-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "153cmyjkr4cfyin0maay2737g44qavdbsjxbgfxlnbrhn600am11")))

(define-public crate-brotli-sys-0.1.2 (c (n "brotli-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "129ywc4w6k6gwgblb0xxm52q9cd59raicvhv1h0frcrcaxky5av5")))

(define-public crate-brotli-sys-0.2.0 (c (n "brotli-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cyc23bx9kqxik6dbd0wgf78sn4mb9r523gvlbyyqs47xf04d1m0")))

(define-public crate-brotli-sys-0.2.1 (c (n "brotli-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fqn47j8i8jwsval150pps9v5jxsgqipcyk3y5z1nrqc5r5zal6b")))

(define-public crate-brotli-sys-0.3.0 (c (n "brotli-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f3b5zzgy45j5nmhjf0byals5w3ifkr65kaapdcsysbn333ix3zh")))

(define-public crate-brotli-sys-0.3.2 (c (n "brotli-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kdfdbcba6zwa13xpjwgiplblkdf6vigxjbwwp6l2ascbylxwia4")))

