(define-module (crates-io br ot brotli-ffi) #:use-module (crates-io))

(define-public crate-brotli-ffi-1.0.0 (c (n "brotli-ffi") (v "1.0.0") (d (list (d (n "brotli") (r "~3.0") (k 0)))) (h "06y0k3z6kizvbilg5hngiir85mz0q8ryby2i9iin3nna3j9q03zl") (f (quote (("vector_scratch_space" "brotli/vector_scratch_space") ("validation" "brotli/validation") ("std" "brotli/std") ("simd" "brotli/simd") ("seccomp" "brotli/seccomp") ("disable-timer" "brotli/disable-timer") ("default" "std") ("benchmark" "brotli/benchmark"))))))

(define-public crate-brotli-ffi-1.0.1 (c (n "brotli-ffi") (v "1.0.1") (d (list (d (n "brotli") (r "~3.0") (k 0)))) (h "01dcl2pmmxrd61nfsqf5hdgix50554qivwml0vzpvq8z6jzyfrwv") (f (quote (("vector_scratch_space" "brotli/vector_scratch_space") ("validation" "brotli/validation") ("std" "brotli/std") ("simd" "brotli/simd") ("seccomp" "brotli/seccomp") ("disable-timer" "brotli/disable-timer") ("default" "std") ("benchmark" "brotli/benchmark"))))))

(define-public crate-brotli-ffi-1.1.0 (c (n "brotli-ffi") (v "1.1.0") (d (list (d (n "brotli") (r "~3.1") (k 0)))) (h "0a2h8ljgqxrhccn7nd1ks08mlkr289zfzf80qbd283ffa6wr8lr8") (f (quote (("vector_scratch_space" "brotli/vector_scratch_space") ("validation" "brotli/validation") ("std" "brotli/std") ("simd" "brotli/simd") ("seccomp" "brotli/seccomp") ("disable-timer" "brotli/disable-timer") ("default" "std") ("benchmark" "brotli/benchmark"))))))

(define-public crate-brotli-ffi-1.1.1 (c (n "brotli-ffi") (v "1.1.1") (d (list (d (n "brotli") (r "~3.1") (k 0)))) (h "1zydwj5bkjlp37qvijb4jx4r3qh1nwdk70qncpacfl2ijz50ssp3") (f (quote (("vector_scratch_space" "brotli/vector_scratch_space") ("validation" "brotli/validation") ("std" "brotli/std") ("simd" "brotli/simd") ("seccomp" "brotli/seccomp") ("disable-timer" "brotli/disable-timer") ("default" "std") ("benchmark" "brotli/benchmark"))))))

(define-public crate-brotli-ffi-1.1.2 (c (n "brotli-ffi") (v "1.1.2") (d (list (d (n "brotli") (r "~3.3") (f (quote ("ffi-api"))) (k 0)))) (h "166h27hqgdaxf5rp93fgsaa0hwwydpdw07rj79qfbxlj1imgfq1n") (f (quote (("vector_scratch_space" "brotli/vector_scratch_space") ("validation" "brotli/validation") ("std" "brotli/std") ("simd" "brotli/simd") ("seccomp" "brotli/seccomp") ("disable-timer" "brotli/disable-timer") ("default" "std") ("benchmark" "brotli/benchmark"))))))

