(define-module (crates-io br ot brother-ql-rs) #:use-module (crates-io))

(define-public crate-brother-ql-rs-0.1.0 (c (n "brother-ql-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.5") (d #t) (k 0)))) (h "0dmsn6wyy45rrv7bz1jpn3pfvl2504xaf8y6qh5h0va70lv7i5q8")))

(define-public crate-brother-ql-rs-0.1.1 (c (n "brother-ql-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.5") (d #t) (k 0)))) (h "1lz910bmfs3d7xjrc5nc1w4ximxq9wqwdskx02sbhkp9xi81ywix")))

(define-public crate-brother-ql-rs-0.2.0 (c (n "brother-ql-rs") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "rusb") (r "^0.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.8.2") (d #t) (k 0)))) (h "12bmqj5m4ihdhc4cl6dr5dak1gdqlbrwmnqvm7pm4fw8vxqlj8p0")))

(define-public crate-brother-ql-rs-0.2.1 (c (n "brother-ql-rs") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "rusb") (r "^0.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.8.2") (d #t) (k 0)))) (h "0d97mqc7abi2r95ppg042rmsb993wnhmrb3qrj4g1vskxdv9gard")))

