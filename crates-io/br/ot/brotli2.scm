(define-module (crates-io br ot brotli2) #:use-module (crates-io))

(define-public crate-brotli2-0.1.0 (c (n "brotli2") (v "0.1.0") (d (list (d (n "brotli-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "10wgvibqs8y2m0qbrb4ffkrm1makxfxv1rw3cp55ay5705c7ph60")))

(define-public crate-brotli2-0.2.0 (c (n "brotli2") (v "0.2.0") (d (list (d (n "brotli-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1j0mr19av9v3nmnd5sgyib1svgibmyx2qpl3xjjwcbaz2n5kh1vz")))

(define-public crate-brotli2-0.2.1 (c (n "brotli2") (v "0.2.1") (d (list (d (n "brotli-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14yqamkfh8bsybsiwgg0fgkanq4ms6002imn2hp6zw4gbrfjhqzf")))

(define-public crate-brotli2-0.2.2 (c (n "brotli2") (v "0.2.2") (d (list (d (n "brotli-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1jid13m7kb0ss0mi1dmnxkwlpjivfgnpkc16jah1fl13n6x0p7ga")))

(define-public crate-brotli2-0.2.3 (c (n "brotli2") (v "0.2.3") (d (list (d (n "brotli-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1dvsn32pnad0rqyq2kc09z7ap8ni0qb1gcp8j5x6vfcsd9zfqzwc") (y #t)))

(define-public crate-brotli2-0.3.0 (c (n "brotli2") (v "0.3.0") (d (list (d (n "brotli-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0y5n3zasamwzxz487nx1yxi55h82vcxcl8qvr7gy3b5cvq1d2c8y")))

(define-public crate-brotli2-0.3.2 (c (n "brotli2") (v "0.3.2") (d (list (d (n "brotli-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "13jnhpmfkqy2xar4lxrsk3rx3i12bgnarnsxq4arhc6yxb1kdc0c")))

