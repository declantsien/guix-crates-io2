(define-module (crates-io br ot brother_ql) #:use-module (crates-io))

(define-public crate-brother_ql-1.0.0 (c (n "brother_ql") (v "1.0.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (f (quote ("use_alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0fh74x410hvsvh998h0fczlviiijnw4b1s97wbsr10y6hk943sz0")))

(define-public crate-brother_ql-1.0.1 (c (n "brother_ql") (v "1.0.1") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (f (quote ("use_alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "044gx7rmdr4pnlszcbdblf2466bib0wbmncrbfhlq5w1ifdfz4g9")))

(define-public crate-brother_ql-1.0.2 (c (n "brother_ql") (v "1.0.2") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (f (quote ("use_alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1dhqxlp9ani0dygji1jnxzk44xl37n9rvp9a4y6w7qw3yz8c57rc")))

(define-public crate-brother_ql-1.0.3 (c (n "brother_ql") (v "1.0.3") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (f (quote ("use_alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0i90zwsrwvlsm82g6ln448jcyyf8dwjwqxv4k2261qfm1502rh2l")))

