(define-module (crates-io br un brunhild) #:use-module (crates-io))

(define-public crate-brunhild-0.5.0 (c (n "brunhild") (v "0.5.0") (d (list (d (n "js-sys") (r "^0.3.31") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.4") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0rvyqz2j60hwfl076wvpxpss840ayn9wdkvr0cj39yvxdzd725xm")))

(define-public crate-brunhild-0.5.1 (c (n "brunhild") (v "0.5.1") (d (list (d (n "js-sys") (r "^0.3.31") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.4") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0qd0l6b4av5c1fyipd1vk6p8hnx41yzmf50nm39c14rns6ravfzi")))

(define-public crate-brunhild-0.5.2 (c (n "brunhild") (v "0.5.2") (d (list (d (n "js-sys") (r "^0.3.31") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.4") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0k5w4ngnpjp970lqr3a5p5slypfdmc29gxzbbphyf77baprndk9s")))

(define-public crate-brunhild-0.5.3 (c (n "brunhild") (v "0.5.3") (d (list (d (n "js-sys") (r "^0.3.31") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.4") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "15d59z6dlnx41nkwg9cs2ikgx0smi3vl2y0i6ybv93ssynxq7vlp")))

(define-public crate-brunhild-0.5.4 (c (n "brunhild") (v "0.5.4") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "16jpg9nlsh4936n33s74kjhdgrnpnb8glf141g7bcg9z26ygnygj")))

(define-public crate-brunhild-0.5.5 (c (n "brunhild") (v "0.5.5") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0wiy8xrznnqkh8jr55x5irdy8m4dwydhwn89avm1aa21adzl059r")))

(define-public crate-brunhild-0.5.6 (c (n "brunhild") (v "0.5.6") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0rwv7c01n9nhdb1bdqx1rbszna21db7hbzr3fki87jqnd4vjkyw0")))

(define-public crate-brunhild-0.5.7 (c (n "brunhild") (v "0.5.7") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "13vna9sgcq164qbksjs9lrsraa736d3h61rcpg5p2p2mjaxq011f")))

(define-public crate-brunhild-0.6.0 (c (n "brunhild") (v "0.6.0") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "0f9m16bmw0drw5r1qvk29rk107lx4626swlh12y5m7x2g22hqblx")))

(define-public crate-brunhild-0.6.1 (c (n "brunhild") (v "0.6.1") (d (list (d (n "js-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.54") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.31") (f (quote ("Document" "Window" "HtmlElement" "Element" "Node"))) (d #t) (k 0)))) (h "06piq2rv7rd00hx6qdd5y8rgilwsn008cfk6qv5f8gg0hmy5165n")))

