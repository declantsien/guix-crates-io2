(define-module (crates-io br un brunch) #:use-module (crates-io))

(define-public crate-brunch-0.1.1 (c (n "brunch") (v "0.1.1") (d (list (d (n "dactyl") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1rpniaza4hwggfri5iwr7sdmbzqd3qnmsgn31hxv4asavw3hnk4l")))

(define-public crate-brunch-0.1.2 (c (n "brunch") (v "0.1.2") (d (list (d (n "dactyl") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0h0gw2yp5s2y8y710xf99bvr1jahwfsp1cdg5igbbfdc88c1pk04")))

(define-public crate-brunch-0.1.3 (c (n "brunch") (v "0.1.3") (d (list (d (n "dactyl") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0kigs7njwpgnc0lsihs0h0017ih9jr5ifgi4brs7xk2w7frf57zq")))

(define-public crate-brunch-0.2.0 (c (n "brunch") (v "0.2.0") (d (list (d (n "dactyl") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "03hv81ivbl8v39pj18dckw86rr3s17r5libw0xagdq79xd3d0l6z") (r "1.56")))

(define-public crate-brunch-0.2.1 (c (n "brunch") (v "0.2.1") (d (list (d (n "dactyl") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1wh70kpczww79idcyyapb9in5wci0g3bf1h3qh6l091gg96475jd") (r "1.56")))

(define-public crate-brunch-0.2.2 (c (n "brunch") (v "0.2.2") (d (list (d (n "dactyl") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0hn9aazzjf6l6gjbysjricydj6xyjga5zp7f0dr97bs5d5lchw1c") (r "1.56")))

(define-public crate-brunch-0.2.3 (c (n "brunch") (v "0.2.3") (d (list (d (n "dactyl") (r "0.3.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1dbid7j0chqiqpgaq82gapmqzl2h4sd6f2qj7hx4xw65njqd55lc") (r "1.56")))

(define-public crate-brunch-0.2.4 (c (n "brunch") (v "0.2.4") (d (list (d (n "dactyl") (r "0.3.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "18j1wki3byz8jwlk44lssavf6pssc5y40rjx2gb4mykp7z5pj7l2") (r "1.56")))

(define-public crate-brunch-0.2.5 (c (n "brunch") (v "0.2.5") (d (list (d (n "dactyl") (r "0.3.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1zz1sd7qq3mm6lmv9nqcsn7wcvkkaadzjlj1cqa7gfbwwm324hv5") (r "1.56")))

(define-public crate-brunch-0.2.6 (c (n "brunch") (v "0.2.6") (d (list (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "041a6fl2b61c221ay1vd8zk28bw5k1v02bymbj8n635z4r23b148") (r "1.61")))

(define-public crate-brunch-0.3.0 (c (n "brunch") (v "0.3.0") (d (list (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1v7gyh9ggmgin84hj1icmmhhjy6y3fmbl6zvvc8d514c2k3s85yz") (r "1.61")))

(define-public crate-brunch-0.3.1 (c (n "brunch") (v "0.3.1") (d (list (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "184c6ljv72sfa4hvipy25k8v69cgxxmc70ijb8g0f0b2hiw53wxl") (r "1.61")))

(define-public crate-brunch-0.3.2 (c (n "brunch") (v "0.3.2") (d (list (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "0221pm38brcjilc5fh1q56l8f6hkp1s28w0mdqjnc010x57hcpah") (r "1.61")))

(define-public crate-brunch-0.3.3 (c (n "brunch") (v "0.3.3") (d (list (d (n "dactyl") (r "0.4.*, >=0.4.4") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "0j2w9s9vb7c32yfj8q9xgb351hs46132klgd131z45qjfy7968i2") (r "1.61")))

(define-public crate-brunch-0.3.4 (c (n "brunch") (v "0.3.4") (d (list (d (n "dactyl") (r "0.4.*, >=0.4.5") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1dad5wlci8dzsc9903i901jv1ihv01npf7wx9v5p27033ygkl6ik") (r "1.61")))

(define-public crate-brunch-0.3.5 (c (n "brunch") (v "0.3.5") (d (list (d (n "dactyl") (r "0.4.*, >=0.4.5") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1zk59v88mz4yqxz7kham504409vawidb7hak64pa1ypcn5ry007i") (r "1.63")))

(define-public crate-brunch-0.3.6 (c (n "brunch") (v "0.3.6") (d (list (d (n "dactyl") (r "0.4.*, >=0.4.5") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1j8ngsylpl3izgc44a3hw9i3kva97aiyy03wappkbnfjpc63kr97") (r "1.63")))

(define-public crate-brunch-0.3.7 (c (n "brunch") (v "0.3.7") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "dactyl") (r "0.4.*, >=0.4.5") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1xkxcbxcxg579fq9x1nivi05xgcs8z7clkvfrrs756rnbc4x9png") (r "1.63")))

(define-public crate-brunch-0.4.0 (c (n "brunch") (v "0.4.0") (d (list (d (n "dactyl") (r "0.4.*, >=0.4.5") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1f7lh4l0pdy19x9x9y955d6iys6f7cm2fdq179vx8xl8wmxl17fm") (r "1.66")))

(define-public crate-brunch-0.5.0 (c (n "brunch") (v "0.5.0") (d (list (d (n "dactyl") (r "0.5.*") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "122n58gikz2z3ihqiirzn21j2bgsghnk40q2fz0chkldxrk8iv0j") (r "1.70")))

(define-public crate-brunch-0.5.1 (c (n "brunch") (v "0.5.1") (d (list (d (n "dactyl") (r "0.5.*") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "0plf3x0r08n87idg1a9glgsyq2i9imncwacr9mvvbxwbdvxswick") (r "1.70")))

(define-public crate-brunch-0.5.2 (c (n "brunch") (v "0.5.2") (d (list (d (n "dactyl") (r "0.6.*") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "12q864d79ybmdmnyv6v77hbryqyg9h3sbaj5rbx7bdn7y6b4yz6i") (r "1.70")))

(define-public crate-brunch-0.5.3 (c (n "brunch") (v "0.5.3") (d (list (d (n "dactyl") (r "0.7.*") (d #t) (k 0)) (d (n "quantogram") (r "=0.4.4") (d #t) (k 2)) (d (n "unicode-width") (r "0.1.*") (d #t) (k 0)))) (h "1ranrp9qqmq8z2qkwdwxc67sl1w6mjk3kr7711xmrlw5v48dfcir") (r "1.70")))

