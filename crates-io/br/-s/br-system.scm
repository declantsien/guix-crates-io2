(define-module (crates-io br -s br-system) #:use-module (crates-io))

(define-public crate-br-system-0.0.1 (c (n "br-system") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)))) (h "06s9xdfqslgbcy9w6g8x0vfxgnrhc3xvb78fiq7ffnlqai9w8mga")))

(define-public crate-br-system-0.0.2 (c (n "br-system") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)))) (h "1ig0ark1jg01jl6ifq5m1wqlwfwi7rrgc982gyiln7xx2qsd0hrq")))

(define-public crate-br-system-0.0.3 (c (n "br-system") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)))) (h "0nllxl4y70kwrlf6aqjffzbrdp5b9121ym4fwybfjfx95siqph0x")))

(define-public crate-br-system-0.0.4 (c (n "br-system") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.12") (d #t) (k 0)))) (h "08b44i7g9bxzqn366jpkl826fhmhcb3mb3bf4cidym7vxixdx6v5")))

