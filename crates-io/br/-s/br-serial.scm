(define-module (crates-io br -s br-serial) #:use-module (crates-io))

(define-public crate-br-serial-0.0.1 (c (n "br-serial") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serial2") (r "^0.2.1") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "07ymgdfwk2bn928hafn08wqva27rwhvwp1qrv164qg4p8rvay1d0")))

