(define-module (crates-io br -m br-mysql) #:use-module (crates-io))

(define-public crate-br-mysql-0.0.1 (c (n "br-mysql") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0qlwn6hgg5wa6d6zzqnsp3dg0lshz821qjljx2m7afcdhnw1wby9")))

(define-public crate-br-mysql-0.0.2 (c (n "br-mysql") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1jfj9i54xi83ddppfm2ayj93625q2l4sg4jpw59kkb95wdkjmqma")))

(define-public crate-br-mysql-0.0.3 (c (n "br-mysql") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1gavsq2ldb7kmyb9kgsf0qgnpiy88gwf7kpsa8cqv47yh40c0cw0")))

(define-public crate-br-mysql-0.0.4 (c (n "br-mysql") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "14qxz1g3z3hrllfx9x76rpq64cl7kdfgpzk0455sr93aq7kjrmr6")))

