(define-module (crates-io br -m br-maths) #:use-module (crates-io))

(define-public crate-br-maths-0.1.1 (c (n "br-maths") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1i61jfnidl1p745kcjc75dpp3iyhcwdrf8izdjy9giamqwyy1ndb")))

(define-public crate-br-maths-0.1.2 (c (n "br-maths") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1gc1j3rw8l7mkxli2qqw5ziyj7wagacvs9hh2azn47c9xxnxmv3w")))

(define-public crate-br-maths-0.1.3 (c (n "br-maths") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "17ydgw2zwl6n2zl0dkk0wrxb6wldczbli7kh412c271xcypkfzcd")))

(define-public crate-br-maths-0.1.4 (c (n "br-maths") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1ar6sy7dkl15k97l5219qw0a14f5vy111447bsr0c00dgvd4jnd6")))

(define-public crate-br-maths-0.1.5 (c (n "br-maths") (v "0.1.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "04irhk18jjab50dr8mc0fpgssnw9v1zcvgvbrl14gwxpw0a32r4s")))

(define-public crate-br-maths-0.1.6 (c (n "br-maths") (v "0.1.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0wl7jzkd62ya3s5is1w0n1qghmzvx4ajv2ybphiy124lavclpr1h")))

(define-public crate-br-maths-0.1.8 (c (n "br-maths") (v "0.1.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0n0j1adysawvzbwkfr2d146fgngrkzr5s92cssw6ffll9p2qqcha")))

(define-public crate-br-maths-0.1.9 (c (n "br-maths") (v "0.1.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1z71pkh7q965dyh3mcf77j1qddbybbsvh1zrwzgab9kxc7a25j5j")))

(define-public crate-br-maths-0.1.10 (c (n "br-maths") (v "0.1.10") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0kgq11w44ymnvijz28j3rj9l2caj30v3pkg1ya21bjc6bqsiywh5")))

(define-public crate-br-maths-0.1.11 (c (n "br-maths") (v "0.1.11") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0sw3rmyw25yvl3w61l6nb6nq5jlfh5snj4l07zxz03h9kyzwwkjz")))

