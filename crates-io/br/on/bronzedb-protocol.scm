(define-module (crates-io br on bronzedb-protocol) #:use-module (crates-io))

(define-public crate-bronzedb-protocol-0.1.0 (c (n "bronzedb-protocol") (v "0.1.0") (d (list (d (n "bronzedb-util") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "speculate") (r "^0.1") (d #t) (k 2)))) (h "18jak6m071zv0i4zd7g9mi0l1ml93njvhs7m4brq3xgvandw8ik7")))

