(define-module (crates-io br on bronco) #:use-module (crates-io))

(define-public crate-bronco-0.1.0 (c (n "bronco") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base64") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "sodiumoxide") (r "^0.2.6") (f (quote ("std"))) (k 0)))) (h "017v34671f26q9wmz8y1iy8mw2gjwxxh1vn2v7za473c753s52vm")))

(define-public crate-bronco-0.1.1 (c (n "bronco") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base64") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "sodiumoxide") (r "^0.2.6") (f (quote ("std"))) (k 0)))) (h "1nxv5sfk2bcvl9i1dywnrwpf102mn1p6hx4nc4x9rh5qgas90yi3")))

