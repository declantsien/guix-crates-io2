(define-module (crates-io br on bronzeflow-utils) #:use-module (crates-io))

(define-public crate-bronzeflow-utils-0.1.0 (c (n "bronzeflow-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)))) (h "167jz9ql8v91h452k2rs65wp6jp8w8z39x66xzqxqfzwjwil7wmq")))

(define-public crate-bronzeflow-utils-0.1.1 (c (n "bronzeflow-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)))) (h "139brzvm65zp8y0in6d40aglwmszkaw6rs0xnflz44kpqq86azd4")))

