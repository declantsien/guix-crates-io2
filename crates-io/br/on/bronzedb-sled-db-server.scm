(define-module (crates-io br on bronzedb-sled-db-server) #:use-module (crates-io))

(define-public crate-bronzedb-sled-db-server-0.1.0 (c (n "bronzedb-sled-db-server") (v "0.1.0") (d (list (d (n "bronzedb-engine") (r "^0.1") (d #t) (k 0)) (d (n "bronzedb-server") (r "^0.1") (d #t) (k 0)) (d (n "bronzedb-util") (r "^0.1") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.22") (d #t) (k 0)))) (h "16r7m65qfk22rah8cnl3s1lxlq7n67f3qr1pvnn6hwxlik2rbv2h")))

