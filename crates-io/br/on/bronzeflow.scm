(define-module (crates-io br on bronzeflow) #:use-module (crates-io))

(define-public crate-bronzeflow-0.1.0 (c (n "bronzeflow") (v "0.1.0") (d (list (d (n "bronzeflow-core") (r "^0.1.0") (d #t) (k 0)))) (h "1p7dcj2ra5ziasch124nxdh2dd29k7pc8b833caax328j39v78m1")))

(define-public crate-bronzeflow-0.1.1 (c (n "bronzeflow") (v "0.1.1") (d (list (d (n "bronzeflow-core") (r "^0.1.1") (k 0)))) (h "1iyamz1jfsbbxal5xshd8zp1albrm974l4qh8cyq3hkjqx9qrqs1") (f (quote (("default" "async" "async_tokio") ("async_tokio" "bronzeflow-core/async_tokio") ("async" "bronzeflow-core/async"))))))

