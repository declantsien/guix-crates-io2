(define-module (crates-io br on bronze_derive) #:use-module (crates-io))

(define-public crate-bronze_derive-0.0.1 (c (n "bronze_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1a7rnhzpn3w7vygv16g6rs0m65g35z9y55ps4f6ksrlq42n6w0bw")))

(define-public crate-bronze_derive-0.1.0 (c (n "bronze_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1v49rd5w7j51fw9fa9y784n4q0lkgsv1zsqk0r9jklkhk5lil5lk")))

