(define-module (crates-io br on bronzedb-client) #:use-module (crates-io))

(define-public crate-bronzedb-client-0.1.0 (c (n "bronzedb-client") (v "0.1.0") (d (list (d (n "bronzedb-protocol") (r "^0.1") (d #t) (k 0)) (d (n "bronzedb-util") (r "^0.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0jqyacjkdwrhbkfmpfpz1db96bvrb6mbzm0d8vla7wcgf77bgywh")))

(define-public crate-bronzedb-client-0.1.1 (c (n "bronzedb-client") (v "0.1.1") (d (list (d (n "bronzedb-protocol") (r "^0.1") (d #t) (k 0)) (d (n "bronzedb-util") (r "^0.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0fsf4l2k856ww48ll93fxp2xxl2nh93v335cnsl62vi0zb4s4c89")))

