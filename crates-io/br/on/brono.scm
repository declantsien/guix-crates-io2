(define-module (crates-io br on brono) #:use-module (crates-io))

(define-public crate-brono-0.0.0 (c (n "brono") (v "0.0.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)))) (h "171ccxc54nnyw6fj5a2v09dmcz5hfn36avvcmq714ig52dk37ywy")))

