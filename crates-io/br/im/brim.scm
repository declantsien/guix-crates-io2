(define-module (crates-io br im brim) #:use-module (crates-io))

(define-public crate-brim-2.1.0 (c (n "brim") (v "2.1.0") (d (list (d (n "sarge") (r "^3.0.1") (d #t) (k 0)))) (h "16fmrh73lq6jq4f4mj2yfqv5pw53scxvq5hl5w919c242iw3q6b0") (f (quote (("default") ("debug"))))))

(define-public crate-brim-2.2.0 (c (n "brim") (v "2.2.0") (d (list (d (n "sarge") (r "^3.0.1") (d #t) (k 0)))) (h "00f1d412jzrcbx1flgwr516hcq4g71sh4wlfvnc9ww5vbf7grn7v") (f (quote (("default") ("debug"))))))

(define-public crate-brim-2.2.1 (c (n "brim") (v "2.2.1") (d (list (d (n "sarge") (r "^3.0.1") (d #t) (k 0)))) (h "01l4y7394lxk6y0x10pblxpcsahqhw7d40n3yzciv8iwrysyzssa") (f (quote (("default") ("debug"))))))

(define-public crate-brim-2.2.2 (c (n "brim") (v "2.2.2") (d (list (d (n "sarge") (r "^3.0.1") (d #t) (k 0)))) (h "11fhpram4yax33p03mnz9y7ca3rvvzkl6kwb0lzxlkavx2iay81v") (f (quote (("default") ("debug"))))))

(define-public crate-brim-3.0.0 (c (n "brim") (v "3.0.0") (d (list (d (n "sarge") (r "^3.0.1") (d #t) (k 0)))) (h "13i0kqvvxpmjv6d0ghcygn2hlz2njwvq9ck7k7z3b9bs0k3kbn0w") (f (quote (("default") ("debug"))))))

(define-public crate-brim-3.0.1 (c (n "brim") (v "3.0.1") (d (list (d (n "sarge") (r "^4.0.2") (d #t) (k 0)))) (h "1zm84w0nifagdi43l8yjj9h8f6f94qlgdx37y7iqnlxaqydj5x6z") (f (quote (("default") ("debug"))))))

(define-public crate-brim-3.0.3 (c (n "brim") (v "3.0.3") (d (list (d (n "sarge") (r "^4.0.2") (d #t) (k 0)))) (h "0mkcfdvv2x8yk4hzpy4r3qpgzgdqahgr4c78binnjlxxn2zqa9kd") (f (quote (("default") ("debug"))))))

(define-public crate-brim-4.0.0 (c (n "brim") (v "4.0.0") (d (list (d (n "sarge") (r "^7.0") (d #t) (k 0)))) (h "00wrzz9k3hdjjc44lgnhd8gaabv70c6jssszzybd3si9hbcbfv9a") (f (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

(define-public crate-brim-4.0.1 (c (n "brim") (v "4.0.1") (d (list (d (n "sarge") (r "^7.0") (d #t) (k 0)))) (h "1x5kvxp7ksg78pav3mva3ipla80q4d0q70kjkmrd6bsz66shwcrh") (f (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

(define-public crate-brim-4.1.0 (c (n "brim") (v "4.1.0") (d (list (d (n "sarge") (r "^7.0") (d #t) (k 0)))) (h "1wr4wv0sjmpzv6zxxf8q8yzjlkhl9g82jwlci0yd608m0yyxpakx") (f (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

