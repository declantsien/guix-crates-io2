(define-module (crates-io br es brest) #:use-module (crates-io))

(define-public crate-brest-0.1.0 (c (n "brest") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "09xcyll0pgaa5xysg66a8dlv6vqhlfnqmym7c7x1frjxp1k321xf")))

(define-public crate-brest-0.1.1 (c (n "brest") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ch6p64gg6lvicqyd9qd5x99fk2z5sr22b5kpadqdx1p82ahqbyf")))

(define-public crate-brest-0.1.2 (c (n "brest") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dl0vvjk703vj7bl4grz4gzjm4j4rzz355sqq8bcrr6c6d4a61ym")))

(define-public crate-brest-0.1.3 (c (n "brest") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w34baasm3mg2wyc95dy0yc89gg06jijbbl0bf79pjpakrs5icwf")))

(define-public crate-brest-0.1.4 (c (n "brest") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d0labddw08ynplmsb8rn7b9yc8d1g3qyflqs80vaq1sipzdksz0")))

(define-public crate-brest-0.1.5 (c (n "brest") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "080q5qkkch0ylh8nadz35h6a4dzqlwwlbb1dgwqgrwwf7fb2iiwx") (s 2) (e (quote (("schemars" "dep:schemars"))))))

