(define-module (crates-io br es bresenham_zip) #:use-module (crates-io))

(define-public crate-bresenham_zip-0.1.0 (c (n "bresenham_zip") (v "0.1.0") (d (list (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)))) (h "0ll1sji6w4dmbqf2qmh6y0q8fda0zy0c2cjafb6zci4cx9jypbmp")))

(define-public crate-bresenham_zip-0.1.1 (c (n "bresenham_zip") (v "0.1.1") (d (list (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)))) (h "0miyap05j6aj92h70phabgpwdl6x5y2rd56ya0i4m63gx3nlpfig")))

(define-public crate-bresenham_zip-1.0.0 (c (n "bresenham_zip") (v "1.0.0") (d (list (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)))) (h "1gk8j7p1x81cyn6adf3q7zjsq0v2ld057j8raar1bfhvlcq93pfg")))

