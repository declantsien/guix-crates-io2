(define-module (crates-io br es bresenham) #:use-module (crates-io))

(define-public crate-bresenham-0.1.0 (c (n "bresenham") (v "0.1.0") (h "16d8xxwwcbzzad35nrxh2pyy9r729nj9mp3zmcrcjs304bjdhk4z")))

(define-public crate-bresenham-0.1.1 (c (n "bresenham") (v "0.1.1") (h "1mvg3zcyll0m3z79jwbg183ha4kb7bw06rd286ijwvgn4mi13hdz")))

