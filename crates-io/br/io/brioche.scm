(define-module (crates-io br io brioche) #:use-module (crates-io))

(define-public crate-brioche-0.0.0 (c (n "brioche") (v "0.0.0") (d (list (d (n "ducc") (r "^0.1.2-brioche") (d #t) (k 0) (p "brioche-ducc")) (d (n "ducc-serde") (r "^0.1.2-brioche") (d #t) (k 0) (p "brioche-ducc-serde")) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "06w6p16sh31f1kpny13if80bbb58g2p8jiwhr9pnb0da6dda33kj")))

