(define-module (crates-io br io brioche-ducc-sys) #:use-module (crates-io))

(define-public crate-brioche-ducc-sys-0.1.2-brioche (c (n "brioche-ducc-sys") (v "0.1.2-brioche") (d (list (d (n "bindgen") (r "^0.36") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ws40m1h9brs1mg1yxwngw77s4n0zkqh3rsbbl5iis419h41xrgn") (f (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

