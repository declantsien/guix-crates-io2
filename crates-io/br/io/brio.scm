(define-module (crates-io br io brio) #:use-module (crates-io))

(define-public crate-brio-0.1.0 (c (n "brio") (v "0.1.0") (h "1cjs4m2bq3ynbnyrkdw6dn91yi5mnicn39mddv69xr6rnn37bkjy")))

(define-public crate-brio-0.2.0 (c (n "brio") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "hyper") (r "^0.13.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "1wk952ablmbqnfym91w4wlkgxwgfz82hd39xif7xc8vrdqvfrw52")))

