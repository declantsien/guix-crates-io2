(define-module (crates-io br io brioche-ducc) #:use-module (crates-io))

(define-public crate-brioche-ducc-0.1.2-brioche (c (n "brioche-ducc") (v "0.1.2-brioche") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.2-brioche") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0) (p "brioche-ducc-sys")))) (h "1hfbx5vaw5v99j6vmy1ix806h53ymknwl5pbvczdx4wi6m7nly52")))

