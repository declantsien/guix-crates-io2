(define-module (crates-io br io brioche-ducc-serde) #:use-module (crates-io))

(define-public crate-brioche-ducc-serde-0.1.2-brioche (c (n "brioche-ducc-serde") (v "0.1.2-brioche") (d (list (d (n "ducc") (r "^0.1.2-brioche") (d #t) (k 0) (p "brioche-ducc")) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1cm7ngm6il5829q5mhp25h6vmbawx83b38cfbhvi4wfpqncargqx")))

