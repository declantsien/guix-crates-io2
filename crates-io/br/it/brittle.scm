(define-module (crates-io br it brittle) #:use-module (crates-io))

(define-public crate-brittle-0.1.0 (c (n "brittle") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0170pk1gdpln18zadxwh2m5cifbclhnn99jdvck76jnmackf4f6y")))

(define-public crate-brittle-0.1.1 (c (n "brittle") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0pi52fpggxkb8gdlwbzyzm2cs827s0dw8r0kz3lb8hy57mfym4bs")))

