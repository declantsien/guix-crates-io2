(define-module (crates-io br -o br-oss) #:use-module (crates-io))

(define-public crate-br-oss-0.0.1 (c (n "br-oss") (v "0.0.1") (d (list (d (n "br-crypto") (r "^0.0.9") (d #t) (k 0)) (d (n "br-http") (r "^0.0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0p1cmrf9b2bym5j832yq31c60fknpfmz5pjjqgz26dbrp6x2mkhf")))

(define-public crate-br-oss-0.0.2 (c (n "br-oss") (v "0.0.2") (d (list (d (n "br-crypto") (r "^0.1.2") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1s1j9b7l7ks11war8ljcv8v87c7gc4k09dcrdwi5zpngw10hf7ij")))

(define-public crate-br-oss-0.0.3 (c (n "br-oss") (v "0.0.3") (d (list (d (n "br-crypto") (r "^0.1.13") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09xkh65myg4i5cpz9w22lj7iysgg7pr753nq33sjbs0gy7j2fih0")))

(define-public crate-br-oss-0.0.4 (c (n "br-oss") (v "0.0.4") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "02h814wi876f2m7csc3y5cn7bbw6p2m9fq6jdfmwq3b42irynk9m")))

(define-public crate-br-oss-0.0.5 (c (n "br-oss") (v "0.0.5") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09i7d4393ih0l5wdgksgk7vkl4lhlaj8zpfi62i6y8h4wadkqrq7")))

(define-public crate-br-oss-0.0.6 (c (n "br-oss") (v "0.0.6") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0k77lvlmjjmkkimfl1zzha68f27lx2a8zy6qgnivysjam9a607fr")))

(define-public crate-br-oss-0.0.7 (c (n "br-oss") (v "0.0.7") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09mhbpn5h80sk1874hhddasamikvbiq53hikgnig19vhqd8f9c4c")))

(define-public crate-br-oss-0.0.8 (c (n "br-oss") (v "0.0.8") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1d8d8i7hq63qx0ka3ffcq05bas954cbb5k2hf7cdxb9bcd017ywv")))

(define-public crate-br-oss-0.0.9 (c (n "br-oss") (v "0.0.9") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1d8wr6xr01v4jq58jcscrwab4dbzlaq20aqaxml04fs3wz1rdgs5")))

(define-public crate-br-oss-0.0.10 (c (n "br-oss") (v "0.0.10") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1kdcbq03lhznxak4b7cczhh71v3g9d236by56a85zc3q5q6akigx")))

