(define-module (crates-io br -o br-openai) #:use-module (crates-io))

(define-public crate-br-openai-0.0.1 (c (n "br-openai") (v "0.0.1") (d (list (d (n "br-http") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1f8wx990zccn2jiz3yzmd0ggi6dmm37xl494h7haxvrs6x70yfph")))

(define-public crate-br-openai-0.0.2 (c (n "br-openai") (v "0.0.2") (d (list (d (n "br-http") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1vimsqxwa9dn5yyqv6kgpvsyi5i1gvxkrixhwf4nnwm8mfmwj4kq")))

(define-public crate-br-openai-0.0.3 (c (n "br-openai") (v "0.0.3") (d (list (d (n "br-http") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dgkwkskqz4cj355m6f27vya78gg1sxxkq2ba3mgc283md9h4v77")))

(define-public crate-br-openai-0.0.4 (c (n "br-openai") (v "0.0.4") (d (list (d (n "br-http") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0jckca6n495g9m1mcb63059s0arm5ycnpldlff1s10ffvwi6g11k")))

(define-public crate-br-openai-0.0.5 (c (n "br-openai") (v "0.0.5") (d (list (d (n "br-http") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1zjg5x17rwcznhfkkwc9gkmgg1sisx7ni38p5salwphvqapf3p8c")))

(define-public crate-br-openai-0.0.7 (c (n "br-openai") (v "0.0.7") (d (list (d (n "br-http") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1pzbqqpxckdxl0c08mh2qmhdc0m1qnbdiplk17b1j9ifj8mawv9x")))

