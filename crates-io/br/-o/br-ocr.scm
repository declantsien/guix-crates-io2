(define-module (crates-io br -o br-ocr) #:use-module (crates-io))

(define-public crate-br-ocr-0.0.2 (c (n "br-ocr") (v "0.0.2") (d (list (d (n "br-crypto") (r "^0.1.10") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dlv0ka3v7qq9n48vi1nbasfcp24w35f46487qvzi553kx13f2n6")))

(define-public crate-br-ocr-0.0.3 (c (n "br-ocr") (v "0.0.3") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0zvk4jxsrc6zhf7syxh565056h6hk4mfvkfvvx3i9446l55rddz2")))

(define-public crate-br-ocr-0.0.4 (c (n "br-ocr") (v "0.0.4") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1r2dph19yr2996b1igiff8dj6r6sdqzllglca70lpq0a0vxgiav8")))

(define-public crate-br-ocr-0.0.5 (c (n "br-ocr") (v "0.0.5") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1kfzwc7fj5wa761zdavp2qh3fygyj08l1g51am0jgcvlldn88yav")))

(define-public crate-br-ocr-0.0.6 (c (n "br-ocr") (v "0.0.6") (d (list (d (n "br-crypto") (r "^0.1.14") (f (quote ("br_hash" "br_hmac"))) (k 0)) (d (n "br-http") (r "^0.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0jwss3993xsacmpakvaddxf1wmwqkahjcdyln936bdp2yvdk47n7")))

