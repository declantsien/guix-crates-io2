(define-module (crates-io br ex brexit) #:use-module (crates-io))

(define-public crate-brexit-1.0.0 (c (n "brexit") (v "1.0.0") (h "0nh8wz0xvff2ahm5vb0biza207irq5viacj7j7z8cgk4jhgjh6aa")))

(define-public crate-brexit-1.0.1 (c (n "brexit") (v "1.0.1") (h "0acgm713qdkpznr5821l9llcjb5pggwmr6dsb4fjpx1bpapmhw47")))

