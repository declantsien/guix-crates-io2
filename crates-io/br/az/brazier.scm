(define-module (crates-io br az brazier) #:use-module (crates-io))

(define-public crate-brazier-0.0.1 (c (n "brazier") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0lvkrg8542r0larqf9ympb2c5gqck1sskkainms3g5ixdczcbqzs")))

(define-public crate-brazier-0.1.0 (c (n "brazier") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1dybcgp7csf5nj0z8ylwp4yrx4bn4iyv3wy4yylmjaj318fjhimy")))

