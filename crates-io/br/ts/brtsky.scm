(define-module (crates-io br ts brtsky) #:use-module (crates-io))

(define-public crate-brtsky-0.1.0 (c (n "brtsky") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fsqd4imdg2p89aai9191h5xx11x8fj6bxalrzwj1dz742rm97rm")))

(define-public crate-brtsky-0.1.1 (c (n "brtsky") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ixfqal8qrjgq1nx8vab02f9n1xrz2d0m9hg4ikaprcsypzlw2kh")))

