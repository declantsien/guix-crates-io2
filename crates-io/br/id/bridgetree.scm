(define-module (crates-io br id bridgetree) #:use-module (crates-io))

(define-public crate-bridgetree-0.1.0 (c (n "bridgetree") (v "0.1.0") (h "01fz33ymnmfsjplrsy1qnjd55z9f6a9j2zavgzpccnfwabwhnd4d") (y #t)))

(define-public crate-bridgetree-0.2.0 (c (n "bridgetree") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1akdlyjpzlwd5hj5jc10d5vb67y5ipv6pf2096ij4hydpbrrqj33") (f (quote (("test-dependencies" "proptest")))) (y #t)))

(define-public crate-bridgetree-0.2.1 (c (n "bridgetree") (v "0.2.1") (d (list (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "064m344sfknz99701ng87ivc71qh7iyaaxhfamalhkrk6b7gm6gy") (f (quote (("test-dependencies" "proptest"))))))

(define-public crate-bridgetree-0.3.0 (c (n "bridgetree") (v "0.3.0") (d (list (d (n "incrementalmerkletree") (r "^0.4") (d #t) (k 0)) (d (n "incrementalmerkletree") (r "^0.4") (f (quote ("test-dependencies"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1lks6ga3x48h6b64q3nr8ljjhrlnv7mpsxa78n5cgr44qsnkv09s") (f (quote (("test-dependencies" "proptest")))) (r "1.60")))

(define-public crate-bridgetree-0.4.0 (c (n "bridgetree") (v "0.4.0") (d (list (d (n "incrementalmerkletree") (r "^0.5") (d #t) (k 0)) (d (n "incrementalmerkletree") (r "^0.5") (f (quote ("test-dependencies"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0qpvygl1p7qkkf9gkcb5a36k2img4vqsghdhsfrhrs4il32vdz7v") (f (quote (("test-dependencies" "proptest")))) (r "1.60")))

