(define-module (crates-io br id brids) #:use-module (crates-io))

(define-public crate-brids-0.1.0 (c (n "brids") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0c7n80s38zzk5r572jibp6153dbhhix16yh4mzbak859sy8wgwin") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-brids-0.2.0 (c (n "brids") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1sib5vp3fv7a8vi7x0c2dskxyyng90iz89m1ssjvxb1k8f8mnk01") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-brids-0.2.1 (c (n "brids") (v "0.2.1") (d (list (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "02fh68f1gj2vww1r3xvm4byxjic8hck447rgc41k5b09gvsws6vw") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-brids-0.3.0 (c (n "brids") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0nc1m15biv5v27nmbnvbxf3n132gdz28w0jzp03ps36rfj4sm4x2") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-brids-0.3.1 (c (n "brids") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "03i37q1qb59g19xdsjzrybgza25f7fsx16wqpsj8qqzmz81ps3fz") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-brids-0.4.0 (c (n "brids") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "14rdrfk66vc7k1qgngal4bq659xwrqhdmsp7wqiy8qp3vdyr9czh") (f (quote (("default" "rand"))))))

