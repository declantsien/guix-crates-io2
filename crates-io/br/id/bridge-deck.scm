(define-module (crates-io br id bridge-deck) #:use-module (crates-io))

(define-public crate-bridge-deck-0.1.0 (c (n "bridge-deck") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0yaaxqa2hcsl4kcwdcdsznzmxhy0gwk252rfs2bjhv5hb262sj8y")))

(define-public crate-bridge-deck-0.1.1 (c (n "bridge-deck") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hf08wcrg6h4xqrq4yykgi01jdp1s2vw1rkgbrj31lw7sg9r5zs7")))

