(define-module (crates-io br id bridge_lmdb) #:use-module (crates-io))

(define-public crate-bridge_lmdb-0.1.0 (c (n "bridge_lmdb") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "lmdb-rs") (r "^0.7.6") (d #t) (k 0)) (d (n "rustler") (r "^0.21.0") (d #t) (k 0)))) (h "1mrvhr85rc6x6jrxvfrk5aw9gw4w5cmh8dkm62fz2zd5jwchgq09")))

