(define-module (crates-io br id bridgestan) #:use-module (crates-io))

(define-public crate-bridgestan-2.1.0 (c (n "bridgestan") (v "2.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hd79g1sf0557anfmwbrpg7lyg401izy2cd1fdxh0g1mhcqcxd1p") (r "1.69")))

(define-public crate-bridgestan-2.1.1 (c (n "bridgestan") (v "2.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "147ydais7r951gmvj0328cy85mg97m3ykda1m082gzl6ns511gj0") (r "1.69")))

(define-public crate-bridgestan-2.1.2 (c (n "bridgestan") (v "2.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0p06z999kixndai41ws28mshss5qcs3dg8s30lyj4r9308w7l1xi") (r "1.69")))

(define-public crate-bridgestan-2.2.0 (c (n "bridgestan") (v "2.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0vdaxgxzd632mkxw0wgczsg6lq39wamv52gpxb665qjfs14lhf30") (r "1.69")))

(define-public crate-bridgestan-2.2.1 (c (n "bridgestan") (v "2.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03625fl1dnx878cx9w1wbxfr53gm34mf72vz1p05zil8di0a9hla") (r "1.69")))

(define-public crate-bridgestan-2.2.2 (c (n "bridgestan") (v "2.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1z5l9xf9f2dd9vr0x7k3l09sggy1pwxai1lnacyi5jkhapqwf26q") (r "1.69")))

(define-public crate-bridgestan-2.3.0 (c (n "bridgestan") (v "2.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03m5hl3adlsyjai0fk9dwk43kcvgmbbga3aav88qayyfq3gkr99p") (r "1.69")))

(define-public crate-bridgestan-2.4.0 (c (n "bridgestan") (v "2.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1ikjfxlig2cdy2l6qk7qavxnbzg8m682aimfr4mw5v1y37jifckh") (r "1.69")))

(define-public crate-bridgestan-2.4.1 (c (n "bridgestan") (v "2.4.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0v1nsj0ibpifx0dcy7ljjg78g04v9bkswh7qzhii4qhhdlrar74z") (r "1.69")))

