(define-module (crates-io br id bridges) #:use-module (crates-io))

(define-public crate-bridges-0.3.0 (c (n "bridges") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-serial") (r "^3.2.0") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1h2kqqg6l7ja59p4q299zqkvw67dpww22kl0nmr8xlmardpzpisp")))

(define-public crate-bridges-0.5.0 (c (n "bridges") (v "0.5.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-serial") (r "^3.2.0") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "078sddl03cjq3xzl04bw5yybiixn7mhn4955cqyfr8dnx4qw4glr")))

