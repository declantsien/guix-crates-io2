(define-module (crates-io br uh bruh) #:use-module (crates-io))

(define-public crate-bruh-0.1.0 (c (n "bruh") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "tar") (r "^0.4.32") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1svayzdqq1pq9wkgnidlf3jd7hyygw7q64lq6y0q6rjfllrsvzbz") (y #t)))

