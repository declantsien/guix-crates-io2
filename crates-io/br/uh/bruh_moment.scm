(define-module (crates-io br uh bruh_moment) #:use-module (crates-io))

(define-public crate-bruh_moment-0.1.0 (c (n "bruh_moment") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "0kx7s37kbfzki804bfjjhiyzbn6q5cwgy6p04y5zgdy6rvrmsayx")))

(define-public crate-bruh_moment-0.1.1 (c (n "bruh_moment") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "0js8jbvlhxh77s7jnbglfhnadaa7dzkz6i99ckabfpq66hln6i65")))

