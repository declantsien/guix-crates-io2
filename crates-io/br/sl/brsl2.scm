(define-module (crates-io br sl brsl2) #:use-module (crates-io))

(define-public crate-brsl2-0.7.0 (c (n "brsl2") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.10") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0a9m7nkhszsjl3hcadxhgplwc45qh3a7vhvyhpv335qv6k3m9zv5") (f (quote (("std" "rayon") ("default" "std"))))))

