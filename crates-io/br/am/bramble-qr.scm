(define-module (crates-io br am bramble-qr) #:use-module (crates-io))

(define-public crate-bramble-qr-0.1.0 (c (n "bramble-qr") (v "0.1.0") (d (list (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "bramble-crypto") (r "^0.1") (d #t) (k 0)) (d (n "bramble-data") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01lhy68ign4m17z2rmmwpgh46vw3maxaywbja65x0vazybpap8fn")))

