(define-module (crates-io br am bramble-crypto) #:use-module (crates-io))

(define-public crate-bramble-crypto-0.1.0 (c (n "bramble-crypto") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "salsa20") (r "^0.9") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8") (d #t) (k 0)))) (h "1ln26j6dx65b2p43119z1vnw94mdnzwsywjmf4iifcazws3da1gn")))

