(define-module (crates-io br am bramble-rendezvous) #:use-module (crates-io))

(define-public crate-bramble-rendezvous-0.1.0 (c (n "bramble-rendezvous") (v "0.1.0") (d (list (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "bramble-crypto") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0z1x3crp232zssnif84lgq2rpabmvjj9dxgaw5mww1361zj3k6vv") (f (quote (("tor"))))))

