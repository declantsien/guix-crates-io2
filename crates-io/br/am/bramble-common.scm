(define-module (crates-io br am bramble-common) #:use-module (crates-io))

(define-public crate-bramble-common-0.1.0 (c (n "bramble-common") (v "0.1.0") (d (list (d (n "async-io") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("unstable" "bilock"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8") (d #t) (k 0)))) (h "05lixq6sxf80zzq61jlq3r2lgvfnz9h1s6vv52krml8789nb1sbw")))

