(define-module (crates-io br am bramble-transport) #:use-module (crates-io))

(define-public crate-bramble-transport-0.1.0 (c (n "bramble-transport") (v "0.1.0") (d (list (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "bramble-crypto") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0swy9kkdzy28vd08xcnlx2yv6qqmflqz8pvmwxc2w6wsfmbcznp3")))

