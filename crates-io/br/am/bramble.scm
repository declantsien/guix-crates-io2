(define-module (crates-io br am bramble) #:use-module (crates-io))

(define-public crate-bramble-0.1.0 (c (n "bramble") (v "0.1.0") (d (list (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "bramble-crypto") (r "^0.1") (d #t) (k 0)) (d (n "bramble-data") (r "^0.1") (d #t) (k 0)) (d (n "bramble-handshake") (r "^0.1") (d #t) (k 0)) (d (n "bramble-qr") (r "^0.1") (d #t) (k 0)) (d (n "bramble-rendezvous") (r "^0.1") (d #t) (k 0)) (d (n "bramble-sync") (r "^0.1") (d #t) (k 0)) (d (n "bramble-transport") (r "^0.1") (d #t) (k 0)))) (h "0bnk8q68g4xbmkn0qxgsf4dnykiv6gl9r5g5c1vm5hw8wh4wd2vd")))

