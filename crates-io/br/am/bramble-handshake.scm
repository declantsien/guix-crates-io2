(define-module (crates-io br am bramble-handshake) #:use-module (crates-io))

(define-public crate-bramble-handshake-0.1.0 (c (n "bramble-handshake") (v "0.1.0") (d (list (d (n "bramble-common") (r "^0.1") (d #t) (k 0)) (d (n "bramble-crypto") (r "^0.1") (d #t) (k 0)) (d (n "bramble-data") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hschkm0nspb8w3zfincb4b2alj7llzzdfis4316j3gn2lkcf6yh")))

