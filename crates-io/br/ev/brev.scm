(define-module (crates-io br ev brev) #:use-module (crates-io))

(define-public crate-brev-0.1.0 (c (n "brev") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0ysfv7a42k52np6zpxmnyhacwas7ycvzjvar8rdb7bpb94fz0fym")))

(define-public crate-brev-0.1.1 (c (n "brev") (v "0.1.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1dwwy37svp6rfij8amh4dllcq7i2d31ahldwjdn3ph9qprhsyl63")))

(define-public crate-brev-0.1.2 (c (n "brev") (v "0.1.2") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "046lm70cpkmwmanr69kdri1z1zlpbk2dqnc1jrpv9v7grgq1wdgb")))

(define-public crate-brev-0.1.3 (c (n "brev") (v "0.1.3") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1lf1wylxjki2dzwfkr1hjwjb98syjfykiyb7s44j9yapjkyxg5hy")))

(define-public crate-brev-0.1.4 (c (n "brev") (v "0.1.4") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0f6ijv99b06pwz9acvff8a7iccwxd09qaqz3rjjcl5zai1ssrj67")))

(define-public crate-brev-0.1.5 (c (n "brev") (v "0.1.5") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "12z7mf3aiyg2is3907brbn1m1xfqdnf0ymg3m66s13jg3qlxd51w")))

(define-public crate-brev-0.1.6 (c (n "brev") (v "0.1.6") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0h1vqv85w5sx047fl0qkxrdqmlpyjsxdhw33ni1kyadam1h1nmvr")))

(define-public crate-brev-0.1.7 (c (n "brev") (v "0.1.7") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0brd1dbjb9xicpcg6xb5pjvxhhh7s6ai180k28v8fbaqipw30d6x")))

(define-public crate-brev-0.1.8 (c (n "brev") (v "0.1.8") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1bb2xzx03z07mm111819s22j85zb8s7xl08a3xv2cp2ay0qqwfrn")))

(define-public crate-brev-0.1.9 (c (n "brev") (v "0.1.9") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "15vd40rcnq2n5xwmhl6xm26mrhdwcddyylbib38pccl7clxwwdzb")))

(define-public crate-brev-0.1.10 (c (n "brev") (v "0.1.10") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1djgslql4w1167ag5siq38s6ffwr7fccwfar694s5pc6mwhjn5iw")))

(define-public crate-brev-0.1.11 (c (n "brev") (v "0.1.11") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0w4mh7cs7m4s06r4nh0g2mb8gdq9k4519zdnk42hkq22xcj9yzr6")))

(define-public crate-brev-0.1.12 (c (n "brev") (v "0.1.12") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1cjvdasicvslf3a5xy5fv5i7433bh2hj1zcgywppag12axwknp6q")))

(define-public crate-brev-0.1.13 (c (n "brev") (v "0.1.13") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "08ig21r6aw0abk5xhghga4vrgbpqgmnn1f5fc89pzq4hgybcc48n")))

(define-public crate-brev-0.1.14 (c (n "brev") (v "0.1.14") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0ymyq1zys5xqp2k06c92s4jyn90b8pfn80jhdi44806s1cj0cg22")))

(define-public crate-brev-0.2.0 (c (n "brev") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 0)))) (h "0iva4gjr67df8nbnigaxxl8lgs2ri28xvhxsphn41bzxxgvrqdkp")))

