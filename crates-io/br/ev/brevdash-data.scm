(define-module (crates-io br ev brevdash-data) #:use-module (crates-io))

(define-public crate-brevdash-data-0.1.0 (c (n "brevdash-data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("indexmap"))) (d #t) (k 0)))) (h "0si8jxh24jppvqsjjdawbmh39whv7i68rc2c7b4y2hk2dg56hkbj")))

(define-public crate-brevdash-data-0.1.1 (c (n "brevdash-data") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("indexmap"))) (d #t) (k 0)))) (h "1v9rivbvp332l9nhir8qd435i0wix2jyxyvc4ark49rmrnxk46vn")))

