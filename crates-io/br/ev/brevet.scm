(define-module (crates-io br ev brevet) #:use-module (crates-io))

(define-public crate-brevet-0.1.0 (c (n "brevet") (v "0.1.0") (h "16na4i0wlfiqyhnw9nvmcqx63r8j8zxjslgs599pkg0mhpqyxcjr")))

(define-public crate-brevet-0.2.0 (c (n "brevet") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "073s54s3yp4v2gpynzlydpf7allcp2p3npmpljnfx3y7knjx3204")))

(define-public crate-brevet-0.3.0 (c (n "brevet") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zd5kp05208razpniichvz572b3zkcn5ymi4flpf8x3i0c8z7npf")))

(define-public crate-brevet-0.4.0 (c (n "brevet") (v "0.4.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "18b4swiga21lyg6rjh7fsbd8j7qpq1yp3dayk92yf5s4qmmwn006")))

