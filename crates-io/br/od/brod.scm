(define-module (crates-io br od brod) #:use-module (crates-io))

(define-public crate-brod-0.1.0 (c (n "brod") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.25") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1dvam2wwzls6b2q8cb8as46lp7s0s4l7b2lc35k288dbghgr037m")))

(define-public crate-brod-0.1.1 (c (n "brod") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.25") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1b5zvppp2d9i5bq03c2kaxkr04q34j6dg932l32y0367brz4vzsx")))

(define-public crate-brod-0.1.2 (c (n "brod") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.25") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "0nh5nph1wxjvffmfyb83r5b7vm7alk6x0qc8cv1x3raf4598vz7q")))

(define-public crate-brod-0.1.3 (c (n "brod") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.25") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1icjin4jkj9h998x78zbfs0dsxf6h3hnxj9nii7ibjdcrw3lwsn5")))

