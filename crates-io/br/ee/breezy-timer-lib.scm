(define-module (crates-io br ee breezy-timer-lib) #:use-module (crates-io))

(define-public crate-breezy-timer-lib-1.0.0 (c (n "breezy-timer-lib") (v "1.0.0") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)))) (h "0khswchf9psyzg4gw5d36jh3gbdfqq3qj6sk3ysbnm940msh6vka") (f (quote (("breezy_timer"))))))

