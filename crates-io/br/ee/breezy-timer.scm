(define-module (crates-io br ee breezy-timer) #:use-module (crates-io))

(define-public crate-breezy-timer-0.1.0 (c (n "breezy-timer") (v "0.1.0") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "global") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1rrvbbkqinwd5kmn6hw3964aqcbvwla0fy17w3bp89k1j0iv7jf9") (f (quote (("breezy_timer"))))))

(define-public crate-breezy-timer-0.1.1 (c (n "breezy-timer") (v "0.1.1") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "global") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0wkkn8rzr7v2biw6bknrmzk2vxmharspnjk4bhhi6nz4vfm69fz9") (f (quote (("breezy_timer"))))))

(define-public crate-breezy-timer-0.1.2 (c (n "breezy-timer") (v "0.1.2") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "global") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1rlaj98aiv69fasvb977p2p6y2l0sn229jyk039izcajy8fwa200") (f (quote (("breezy_timer"))))))

(define-public crate-breezy-timer-1.0.0 (c (n "breezy-timer") (v "1.0.0") (d (list (d (n "breezy-timer-lib") (r "^1.0.0") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "global") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1d5b54pmk2w2nbdlxdnq15zwlmxll2bvphwkmv9v3k9jc45zirrb") (f (quote (("breezy_timer" "breezy-timer-lib/breezy_timer"))))))

