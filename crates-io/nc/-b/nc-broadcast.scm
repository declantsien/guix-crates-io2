(define-module (crates-io nc -b nc-broadcast) #:use-module (crates-io))

(define-public crate-nc-broadcast-0.1.0 (c (n "nc-broadcast") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.7") (d #t) (k 0)))) (h "15ik9cil9ldzzd451m9x4673f7cr3rmlcx9hclxk68ahaf8sglli")))

(define-public crate-nc-broadcast-0.1.1 (c (n "nc-broadcast") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.7") (d #t) (k 0)))) (h "0pg3g81z5x11pv2lsgs54a38dnk8sbyybyp0kx21lfsrpqp2fsys")))

(define-public crate-nc-broadcast-0.1.2 (c (n "nc-broadcast") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.7") (d #t) (k 0)))) (h "0b6qz6h4lgd8i1jk950wgijyylb2h1ajcg1f2ldp6f38d00my5y4")))

(define-public crate-nc-broadcast-0.1.3 (c (n "nc-broadcast") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.7") (d #t) (k 0)) (d (n "inotify") (r "^0.10.0") (d #t) (k 0)))) (h "047gw357wmzlyl9kax1ib74wggzdj22i680wbhq8ywhj8wmw4yv2")))

