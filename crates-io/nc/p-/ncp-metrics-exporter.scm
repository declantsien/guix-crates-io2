(define-module (crates-io nc p- ncp-metrics-exporter) #:use-module (crates-io))

(define-public crate-ncp-metrics-exporter-1.0.0 (c (n "ncp-metrics-exporter") (v "1.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-mounts") (r "^0.2.4") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1qdnfaf256mb7f5zjwxlx085zrfs54x2gxzrn4057wsvc5yay589")))

