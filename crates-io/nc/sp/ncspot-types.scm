(define-module (crates-io nc sp ncspot-types) #:use-module (crates-io))

(define-public crate-ncspot-types-0.1.0 (c (n "ncspot-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1px5bjaw2h4gdg9ilzzd8mzvj4xb8n35g90fyzflnf0z3lq5lrgj")))

(define-public crate-ncspot-types-0.2.0 (c (n "ncspot-types") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)))) (h "1dwfq2d7cfnq1ypzs6bbbcfzr1zmdpipwvn57i3nw0vxd9aihy5v")))

(define-public crate-ncspot-types-0.2.1 (c (n "ncspot-types") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)))) (h "0wbrmmbhwbqj5fzpr03wx1zn22i8mxzix9dm6bnqj8azpcl94la9")))

