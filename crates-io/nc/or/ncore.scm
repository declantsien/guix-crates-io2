(define-module (crates-io nc or ncore) #:use-module (crates-io))

(define-public crate-ncore-0.1.0 (c (n "ncore") (v "0.1.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("color" "derive" "std" "unstable-doc"))) (k 0)))) (h "1d6461byb1vi1zgc40xgqsiyhwimyrqrjj4ch2axgxyxa2n4nl2h")))

