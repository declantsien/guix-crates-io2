(define-module (crates-io nc p5 ncp5623) #:use-module (crates-io))

(define-public crate-ncp5623-0.1.0 (c (n "ncp5623") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "07w7c3gd47qalfsjyc18hxyp6p8cls60k6vvhgnb55cj58kxppl8")))

(define-public crate-ncp5623-0.1.1 (c (n "ncp5623") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)))) (h "1wf7m4fd88alcvvcydhq6za48kahk3ndjlr1wz3fj3rhdxw8vby9")))

(define-public crate-ncp5623-0.1.2 (c (n "ncp5623") (v "0.1.2") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)))) (h "0hc6r0mpcphfhzvikghpykyqnzcdvjwy5nyh442ddy2mahyq283h")))

(define-public crate-ncp5623-0.1.3 (c (n "ncp5623") (v "0.1.3") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)))) (h "1s8f69fqwsklsw9sb1zgip15z3anmw55x03ai55lx5bf4jj4mi4p")))

