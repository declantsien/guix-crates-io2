(define-module (crates-io nc mc ncmc) #:use-module (crates-io))

(define-public crate-ncmc-0.1.1 (c (n "ncmc") (v "0.1.1") (d (list (d (n "aes") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.4") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "0m8w2yfwc8q72krp3mksz0bvrcal8388k1010z3sjf4cvv9lddfd")))

(define-public crate-ncmc-0.1.2 (c (n "ncmc") (v "0.1.2") (d (list (d (n "aes") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.4") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "03r96x46n8iqhmq55pfi914fjr9n8agzmq7zjhnfv4iqh831n8hf")))

(define-public crate-ncmc-0.1.3 (c (n "ncmc") (v "0.1.3") (d (list (d (n "aes") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.5") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "0xk2ibpns1wvab2c82yq26i0a9vqkr0bd8isq5lnr4ppqlindipj")))

(define-public crate-ncmc-0.1.4 (c (n "ncmc") (v "0.1.4") (d (list (d (n "aes") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.5") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.1") (d #t) (k 0)))) (h "1ynkg1cnrv5wipjz2pv4bq851il0kgrh7bhc8n13al2mz0l5w8r6")))

(define-public crate-ncmc-0.1.5 (c (n "ncmc") (v "0.1.5") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.5") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.9") (d #t) (k 0)))) (h "1cw1dxa2yqs1xva9dfn32p11kis51xbcsv22535h2ycz79kqwnk4")))

(define-public crate-ncmc-0.1.6 (c (n "ncmc") (v "0.1.6") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.5") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.9") (d #t) (k 0)))) (h "0qsjx0y5wh6zwdnafnlapyqsilcswlpz8dg4k2s3p6rsixy0nycl")))

(define-public crate-ncmc-0.1.8 (c (n "ncmc") (v "0.1.8") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "id3") (r "^0.2.5") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.9") (d #t) (k 0)))) (h "08h6w9yw3vr8zynff55c8ry99yj4a8b0g9mqdi839dxxll7cd6f5")))

(define-public crate-ncmc-0.1.9 (c (n "ncmc") (v "0.1.9") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "id3") (r "^0.3.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.10") (d #t) (k 0)))) (h "124ww6imqrrf8v9ch8cvrj1kc7id5nqszzqsym922aycqn5ahgrv")))

(define-public crate-ncmc-0.1.10 (c (n "ncmc") (v "0.1.10") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "id3") (r "^0.5.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.12") (d #t) (k 0)))) (h "1zd1zz1ipg2d4n7kgzck6jxkwfbv5y2a9ln92mrmld3zv3d56yzq")))

(define-public crate-ncmc-0.2.0 (c (n "ncmc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.0") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.0") (d #t) (k 0)))) (h "1mgyk81lbzy9rmcpw0rnvxx948sbz1d3yyrq1qrirww976f9dkhl")))

(define-public crate-ncmc-0.2.1 (c (n "ncmc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.1") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.1") (d #t) (k 0)))) (h "1ndr7xkd69v24wp20l5lkq7my7vs2dn9fg9q6qbgpwkrw70992dl")))

(define-public crate-ncmc-0.2.2 (c (n "ncmc") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.2") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.2") (d #t) (k 0)))) (h "1c23mrra702q6dgkp460piwqb2ynqbk0jv890iazcw4qkkf66mhd")))

(define-public crate-ncmc-0.2.3 (c (n "ncmc") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.3") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.3") (d #t) (k 0)))) (h "0dfr8wf39vhmqwr8flyzsnil6lci2dpzvzzalwjcrpns30xrgjdn")))

(define-public crate-ncmc-0.2.4 (c (n "ncmc") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.4") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.4") (d #t) (k 0)))) (h "09pwxlq2fzxkikhg00ma7pzy61n8qqr529w0jv59bsak19r6l64b")))

(define-public crate-ncmc-0.2.5 (c (n "ncmc") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.5") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.5") (d #t) (k 0)))) (h "1h1g222cx46krzbl9srqynpvia67v52rfzhdg87l59abng1f56ba")))

(define-public crate-ncmc-0.2.6 (c (n "ncmc") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.6") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.6") (d #t) (k 0)))) (h "026ifpqb4pp6srlkzmi4hqpgra4wm9f3gxms1ivgsr4gsqpziv46")))

(define-public crate-ncmc-0.2.7 (c (n "ncmc") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.6") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.6") (d #t) (k 0)))) (h "0r0fv068gqfmz395kvypzqgqdbfxq59xpk6j29b9h56gii8x9dgs") (y #t)))

(define-public crate-ncmc-0.2.8 (c (n "ncmc") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.8") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.8") (d #t) (k 0)))) (h "1i31vwr6kfmwyvji4hd46yjn7bnwyhmmizabsdf267wjqcmiz586")))

(define-public crate-ncmc-0.2.9 (c (n "ncmc") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.9") (d #t) (k 0)) (d (n "ncm_meta") (r "^0.2.9") (d #t) (k 0)))) (h "1aks7hm9d397k1jqld98wc2bl76mam3zmvgxzrzwna1qhkjv2r1j")))

