(define-module (crates-io nc as ncase) #:use-module (crates-io))

(define-public crate-ncase-0.1.0 (c (n "ncase") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0idrg1py4pxi83spca69jw2hnkrkch6z1k0s4mjd52m6ixp4aybp")))

(define-public crate-ncase-0.1.1 (c (n "ncase") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18z1f7xzpqklsjrk3cm0dy2bb0hhabybfhh1kba9x130ig5bkj7l")))

(define-public crate-ncase-0.2.0 (c (n "ncase") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0n3paz0w64a96px5wnbxcazj62cddsyidadni2ivk1zd5lhm1x3r")))

(define-public crate-ncase-0.2.1 (c (n "ncase") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0slfngk0lldwib8dv786i5sxdczc81fmpcbsv0ngw6cb7isazqgw")))

(define-public crate-ncase-0.2.2 (c (n "ncase") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0i10062jpws5a3kqll2m6sn9csc5p01iwd7zb0hfkyj77x731pai")))

