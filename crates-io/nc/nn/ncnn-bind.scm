(define-module (crates-io nc nn ncnn-bind) #:use-module (crates-io))

(define-public crate-ncnn-bind-0.1.0 (c (n "ncnn-bind") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k5jp7i5knk7cpvw9qvdv7fbj8zp02vvikq8c6wx63298agx16rp")))

(define-public crate-ncnn-bind-0.1.1 (c (n "ncnn-bind") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11k0sybfylhdx2p0618bm4nhmgw0dhqmi8m6vnmxj2jlsddqjkhj")))

