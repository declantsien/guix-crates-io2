(define-module (crates-io nc nn ncnn-rs) #:use-module (crates-io))

(define-public crate-ncnn-rs-0.1.0 (c (n "ncnn-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncnn-bind") (r "^0.1.0") (d #t) (k 0)))) (h "0m5006m38d2428zy5vmd96ag3wfdzwnnh42ak2cd9ck1zvgklcvh") (y #t)))

(define-public crate-ncnn-rs-0.1.1 (c (n "ncnn-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncnn-bind") (r "^0.1.1") (d #t) (k 0)))) (h "1chghqzc2hh0mvzk2j18bycycz4inhbb4vpvn9jr4clqc5dimbfr")))

