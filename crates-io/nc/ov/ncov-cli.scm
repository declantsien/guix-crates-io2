(define-module (crates-io nc ov ncov-cli) #:use-module (crates-io))

(define-public crate-ncov-cli-0.1.0 (c (n "ncov-cli") (v "0.1.0") (d (list (d (n "cursive") (r "^0.17.0-alpha.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02a5xms397rvxbn3498wfkzfr7f0z7mn7dmwwhczybq9kq4g7v60")))

