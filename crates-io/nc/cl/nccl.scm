(define-module (crates-io nc cl nccl) #:use-module (crates-io))

(define-public crate-nccl-0.1.0 (c (n "nccl") (v "0.1.0") (h "04bfjvipj11gj1pd84yr9qi36ywb9j39pfbi0ap5jvfsxcb6i8i2")))

(define-public crate-nccl-0.2.0 (c (n "nccl") (v "0.2.0") (h "0dnazgpjlmmfgdpvmrfdc3234ilzdanjl4y3x10g08iz6jxjp1fg")))

(define-public crate-nccl-0.2.1 (c (n "nccl") (v "0.2.1") (h "11wb755jm0wmgxcj6viiiafmacaf1bvwa4rczchwa31c452g0g2k")))

(define-public crate-nccl-0.3.0 (c (n "nccl") (v "0.3.0") (h "0wf7r2manq5xjpm59klky15sspvkcy47h2r08i243v1x6mmfk6ky")))

(define-public crate-nccl-0.3.1 (c (n "nccl") (v "0.3.1") (h "01hchpw1b6053sysfbwbh38lqckyx6jbr1i0fk71msb8l1i9j1hv")))

(define-public crate-nccl-0.4.0 (c (n "nccl") (v "0.4.0") (h "1wlgiw3vn3mcs2jgp8fxw46xz0wjz7adhx6ybj8jvcfmrxxz620a")))

(define-public crate-nccl-0.4.2 (c (n "nccl") (v "0.4.2") (h "0j375dp92av5wq0hmqiw7nrpk4qvbp8bgfijm3zqk0jk15gdjm0l")))

(define-public crate-nccl-0.4.3 (c (n "nccl") (v "0.4.3") (h "1cm05552558626ficlyazjpgan584czkn1pfa2mf4f2f8wjm5dvb")))

(define-public crate-nccl-0.5.0 (c (n "nccl") (v "0.5.0") (h "10s3nw9r3hkvsinv9k6xq587x909s4j64r40w6wipq4dhsg7mwj6")))

(define-public crate-nccl-0.6.0 (c (n "nccl") (v "0.6.0") (h "0xvx3kq1vvvwcilsq1mcj71q9857z1gd42x067kpilhznv2sgljy")))

(define-public crate-nccl-1.0.0 (c (n "nccl") (v "1.0.0") (h "1hyy4wzprx41pwpis8q0p925l9753akcynk5l7pcld3m3zkl1pya")))

(define-public crate-nccl-2.0.0 (c (n "nccl") (v "2.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1hza76kcwcz925pf5937ggll4f99n16n894qfx02hn20z0c5yrwv")))

(define-public crate-nccl-2.0.1 (c (n "nccl") (v "2.0.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1q5ns3ashn32p3z64lcsgw1f7ijc2hzkfp8smlx90vg34pza6wk7")))

(define-public crate-nccl-2.0.2 (c (n "nccl") (v "2.0.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "058f5n8p6pixyq05aicnhkl6y3f9r2k7mj3njkwaz4673gcprsiz")))

(define-public crate-nccl-3.0.0 (c (n "nccl") (v "3.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1pwx1hdq0lp4zc5w8r2gy68yxn878p4xqpmfjblmcc68jk1m1f76")))

(define-public crate-nccl-4.0.0 (c (n "nccl") (v "4.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1hr7if7cc6pizd3nscn39n2f3p2sz12lhlj5z8ns7k8dxpdzpjya")))

(define-public crate-nccl-4.0.1 (c (n "nccl") (v "4.0.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "0y592razmc6bqr3aj3ycjcz1vpimw3isiqaxrazhp0ksrkg5aslg")))

(define-public crate-nccl-4.0.2 (c (n "nccl") (v "4.0.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "0w6v4nwd295glvkr267nsgdy2366m3vqfrk9s4iwy6bx6cpw36a6")))

(define-public crate-nccl-5.0.0 (c (n "nccl") (v "5.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "0wf7pxbh6v9mb6gpq0dxr804ai8ljdkgrx2mi7cn9mwdl8m1rsyk")))

(define-public crate-nccl-5.0.1 (c (n "nccl") (v "5.0.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1xkwzpw9vnnd450m6pzr0ml5d8817ypds12ijlfxy5vx6ga6iqs9")))

(define-public crate-nccl-5.0.2 (c (n "nccl") (v "5.0.2") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1yshlk42a8fh1g61haas3qxb5v5ai3qg62sdfg24ijbxxvc85anv") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5.1.0 (c (n "nccl") (v "5.1.0") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "1ac4is26w6i2wzkikq8dj7h5mk4323kqbdhvf59wdzpcvnfspp8v") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5.1.1 (c (n "nccl") (v "5.1.1") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)))) (h "06qk87gdk7kwhdk11mwdhzfsxlgc56hkrfb2y9ja1ilnv3jfhd48") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5.2.0 (c (n "nccl") (v "5.2.0") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17xf7r356189syqx3268315yw5gda6grax2k3hd4gwbvxsjplsvc") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5.3.0 (c (n "nccl") (v "5.3.0") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bf2r544vdn8cnd7ii6yjm7bhv0p5zw0icry5ckval37isarl766") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5.4.0 (c (n "nccl") (v "5.4.0") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1blz838d86mpl9jc4p3ym0q600bicaqqp9374hbq2wgpr4hy3hyp") (f (quote (("fuzz" "arbitrary") ("default"))))))

