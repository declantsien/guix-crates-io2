(define-module (crates-io nc pf ncpf) #:use-module (crates-io))

(define-public crate-ncpf-0.1.0 (c (n "ncpf") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "libepf") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "net" "io-std"))) (d #t) (k 0)))) (h "0sqxk7x5aq9h0j3zlhjfkz55cvqj3nrc7a11j5ivk8z8qjb75aq5")))

