(define-module (crates-io nc -u nc-ui) #:use-module (crates-io))

(define-public crate-nc-ui-0.1.1 (c (n "nc-ui") (v "0.1.1") (d (list (d (n "egui") (r "^0.14.2") (k 0)) (d (n "epaint") (r "^0.14") (f (quote ("multi_threaded"))) (k 0)) (d (n "glfw") (r "^0.42.0") (k 0)) (d (n "nc-renderer") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-math3d") (r "^0.9.15") (d #t) (k 0)))) (h "0rzdxw3pb2qh4kj7higy3zn9x3n70jabhl0lv8dffzfdg4gbzyc7")))

(define-public crate-nc-ui-0.1.2 (c (n "nc-ui") (v "0.1.2") (d (list (d (n "egui") (r "^0.14.2") (k 0)) (d (n "epaint") (r "^0.14") (f (quote ("multi_threaded"))) (k 0)) (d (n "glfw") (r "^0.42.0") (k 0)) (d (n "nc-renderer") (r "^0.1.2") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "0x044imcydd78jkgalanplrqi95r3y8zdylv1rzimracnxkdkwdb")))

(define-public crate-nc-ui-0.1.3 (c (n "nc-ui") (v "0.1.3") (d (list (d (n "egui") (r "^0.14.2") (k 0)) (d (n "epaint") (r "^0.14") (f (quote ("multi_threaded"))) (k 0)) (d (n "glfw") (r "^0.42.0") (k 0)) (d (n "nc-renderer") (r "^0.1.3") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "15y9d7mff3av05xcnfc291ffxbap3ivlq2d6qkybji734maiq28p")))

