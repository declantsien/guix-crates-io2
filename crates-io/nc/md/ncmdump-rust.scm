(define-module (crates-io nc md ncmdump-rust) #:use-module (crates-io))

(define-public crate-ncmdump-rust-0.2.2 (c (n "ncmdump-rust") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "id3") (r "^0.6.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0lgqhk1ky1i4hig2fbs3w0nb2ll58q7ribbs92ncqjbsfvwvvcqn")))

