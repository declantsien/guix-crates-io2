(define-module (crates-io nc le nclean) #:use-module (crates-io))

(define-public crate-nclean-0.1.0 (c (n "nclean") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)))) (h "11l07izbf1q9awsqc2f4xnlccg92hbpvf0r4i107ahjjrv90chx4")))

(define-public crate-nclean-0.1.1 (c (n "nclean") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)))) (h "0kjgm5jarfqh48720g5yjzjxqhrpxmvhqp8izbq6jhzjxj5lzrk8")))

