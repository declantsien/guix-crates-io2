(define-module (crates-io nc ur ncurses-lite) #:use-module (crates-io))

(define-public crate-ncurses-lite-0.1.0 (c (n "ncurses-lite") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07b66p66y6vn5lk4bal9darj9xzamiakwpa0wk6d6b5yaq7wrjkr")))

(define-public crate-ncurses-lite-0.1.1 (c (n "ncurses-lite") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07fqj3jz6pqngf9js43plaspyzzx8mw7r4cihrwc17irqa7fayqj")))

