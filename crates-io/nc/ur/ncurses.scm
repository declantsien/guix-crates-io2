(define-module (crates-io nc ur ncurses) #:use-module (crates-io))

(define-public crate-ncurses-5.71.1 (c (n "ncurses") (v "5.71.1") (h "1q2whnmvyb39n2x5li9l9z1iw85b8zm06w3pxri41lzi3nzxppmm") (f (quote (("wide") ("default" "wide"))))))

(define-public crate-ncurses-5.72.0 (c (n "ncurses") (v "5.72.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "09vk2k5p9yvw56sg3sck44dk6as6smqfhd4195n3ifi022wa7bgp") (f (quote (("wide") ("default" "wide"))))))

(define-public crate-ncurses-5.73.0 (c (n "ncurses") (v "5.73.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "12750hfi2ff52pvpcs8wqfpz21qxs1jq2q55f9kq67mnq091rryp") (f (quote (("wide") ("default" "wide"))))))

(define-public crate-ncurses-5.80.0 (c (n "ncurses") (v "5.80.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1rj9s6cn7krybajjs5fa9g3bz1cfx3ky7hzmrkrqjc7j0axw76c8") (f (quote (("wide") ("panel") ("menu") ("default" "wide"))))))

(define-public crate-ncurses-5.81.0 (c (n "ncurses") (v "5.81.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nc4vzgba43isxfscilai0xvf9z8b4bzskwrb98q8f2jn1isql8g") (f (quote (("wide") ("panel") ("menu") ("default" "wide"))))))

(define-public crate-ncurses-5.82.0 (c (n "ncurses") (v "5.82.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1syqpsfhw74clc71ik0xdfb6yfmkn77lwhcvwxdr315r46jg0q7j") (f (quote (("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.83.0 (c (n "ncurses") (v "5.83.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kzc85bvk09r95g0b42r4rjb952dzql28xkksndp3n00vjl3vcc0") (f (quote (("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.84.0 (c (n "ncurses") (v "5.84.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12dfhqn55zpxz6mx39zb77p21bmn072phpba2pn8g0fhkv7gzgih") (f (quote (("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.85.0 (c (n "ncurses") (v "5.85.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01fnabm3nlihw6b4pyz43shljcgjqj71i78y5dhmhrg93871zxr1") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.86.0 (c (n "ncurses") (v "5.86.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c7y4v93v2fj3p7fqb4hb6jw0x9klc9j68644ws0v9957riwq174") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.87.0 (c (n "ncurses") (v "5.87.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d20wchzv2w39pnp4b18sh2aac6kyflm09ma63z53q6zfc6lixqc") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.88.0 (c (n "ncurses") (v "5.88.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nasqqb0vywxh3l8a7adwpa2n0p052cpg66jrv9s622mg9y0amrd") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.89.0 (c (n "ncurses") (v "5.89.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1anjcpr778b7r8axmpnvnlipv16zy3n54wdz6pr3vyi2n67rgx57") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.90.0 (c (n "ncurses") (v "5.90.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vrpbkpi4za2w36ar0by42r4jd7agwnb2pqri4ixqh1a9qss4n09") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.91.0 (c (n "ncurses") (v "5.91.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wf8rmfy33c1v1n021lmgjkcgcz0qa58ayd4bsw1rqi5a8dc2723") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.92.0 (c (n "ncurses") (v "5.92.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zmyskgzi6qmx0gkinb56ixcyjfn4h2pjfrqvfi73xdmi6g7sssk") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("default"))))))

(define-public crate-ncurses-5.93.0 (c (n "ncurses") (v "5.93.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yl5fw9695lhyg57832lvi6rqv4vkf9spqx16g3w3sd567a03k8f") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.94.0 (c (n "ncurses") (v "5.94.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "068ksd83nn16vinkpvavxps22jwapa9r0iynbwpgl28cc3dg00ja") (f (quote (("wide_chtype") ("wide") ("panel") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.95.0 (c (n "ncurses") (v "5.95.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f7hc2s347a7yx9az99ql91wylabxh83pkqpwvh9hd5pbx6xp87z") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.96.0 (c (n "ncurses") (v "5.96.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "128khd6zdrl73h957231gxss3xx0abdljabj93jg4083i2x3sydw") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.97.0 (c (n "ncurses") (v "5.97.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rjsm21qnq3cdr8anq0xlxqy8fkq5pch302bq38x2vzywx2130pf") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.98.0 (c (n "ncurses") (v "5.98.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kblfwldxyq73shsm1y4b0r7nns1yjq5kj2pba6dq9j51cmrmpwx") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.99.0 (c (n "ncurses") (v "5.99.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10kbafzgln5p2dvm0b9z8pdlcf6if2yb4dbvij1gis9p5zp9ns8m") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.100.0 (c (n "ncurses") (v "5.100.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ss1ia7skbs5x2p5lccp38qmm3xnkq7spcp8cyr4yvvz5350gnx7") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-5.101.0 (c (n "ncurses") (v "5.101.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ci0flh7j8v7yir2y1lrqvqy90df1ba2a74acd5xqmr6sws5sb2y") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

(define-public crate-ncurses-6.0.0 (c (n "ncurses") (v "6.0.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10bqpbfvxwwsrxw7ghfl2r0jxqrkazr73rpdjcqw39v74y6zis64") (f (quote (("wide_chtype") ("wide") ("panel") ("mouse_v1") ("menu") ("extended_colors" "wide") ("default"))))))

