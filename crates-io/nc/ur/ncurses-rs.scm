(define-module (crates-io nc ur ncurses-rs) #:use-module (crates-io))

(define-public crate-ncurses-rs-0.0.1 (c (n "ncurses-rs") (v "0.0.1") (h "10h0smh0vff2vcyvzd5jkkkrlxhb6ygh90s6x1srqdwjpbvw7ywh")))

(define-public crate-ncurses-rs-0.0.2 (c (n "ncurses-rs") (v "0.0.2") (h "0s0c49kyj2hm9ggx1hh8ij40m7va1h8yw43p8r0zlc3wnkfcsa3r")))

(define-public crate-ncurses-rs-0.0.3 (c (n "ncurses-rs") (v "0.0.3") (h "0b7r5x6g838qgib9nfvbq2zjqhqf5x2dx5x2rz1can3gnfkmj6rh")))

