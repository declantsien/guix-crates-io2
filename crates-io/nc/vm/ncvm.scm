(define-module (crates-io nc vm ncvm) #:use-module (crates-io))

(define-public crate-ncvm-0.0.0 (c (n "ncvm") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vj025x0vi3xnj2kzqazjq97zzaj6pa31c4mz12rkxd9am9mslj8")))

(define-public crate-ncvm-0.0.1 (c (n "ncvm") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0fs666h82rld53iwwbmmfbpd87zrpa2dgbcqngxnnlrwrp0riavs")))

(define-public crate-ncvm-0.0.2 (c (n "ncvm") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00aq0fwf2g44az63mjkblh6zgjjz7p7vkibgnas69k4lzqbzazzi")))

