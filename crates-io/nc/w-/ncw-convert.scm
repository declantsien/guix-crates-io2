(define-module (crates-io nc w- ncw-convert) #:use-module (crates-io))

(define-public crate-ncw-convert-0.1.0 (c (n "ncw-convert") (v "0.1.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "ncw") (r "^0.1.0") (d #t) (k 0)))) (h "0lln1qs5m0z2w4zr5avpnsmcp6w5aid6hn102rj6ns0wb10j1nkm")))

(define-public crate-ncw-convert-0.1.1 (c (n "ncw-convert") (v "0.1.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "ncw") (r "^0.1.0") (d #t) (k 0)))) (h "1gknnjws4s7jhrxl33i1fmhi0qnajncsfsxw2lfgr2i7ygn9m2pr")))

(define-public crate-ncw-convert-0.1.2 (c (n "ncw-convert") (v "0.1.2") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "ncw") (r "^0.1.2") (d #t) (k 0)))) (h "1x5rlmm3s0wsp87mmw003x7as1df1mplvrs4w8i3xkbppr2j96jh")))

