(define-module (crates-io nc om ncomm) #:use-module (crates-io))

(define-public crate-ncomm-0.1.0 (c (n "ncomm") (v "0.1.0") (h "1zdfl23cqais6w6zsf3gxcflz6nmgfh9jazwlwkqhbja6kx9jbij")))

(define-public crate-ncomm-0.1.1 (c (n "ncomm") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)))) (h "1lg1rgyppz8ljcgb8v6d1lmqfsvrh774xcvq4cf70dvb8agqnmdq")))

(define-public crate-ncomm-0.1.2 (c (n "ncomm") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)))) (h "09ljcyssi8wvalrc6xril00xwyp7g5kbf4kziq8lpcpwzrb3gm05")))

(define-public crate-ncomm-0.2.0 (c (n "ncomm") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)))) (h "0vqnswj3945jp88n8ks3fs0g97gyfgw7nr0nczm5rglbn7pglzss")))

(define-public crate-ncomm-0.2.1 (c (n "ncomm") (v "0.2.1") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)))) (h "0yidlkw6hdc8yhsd24jnxzz0mdf0846lh0dhwp8c3bavmpk6gk17")))

(define-public crate-ncomm-0.3.0 (c (n "ncomm") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0mh4n9zv72s32jjx4jqn1ajbqhik08ajyvs4x83fgqd78pvlp1wv") (f (quote (("default")))) (s 2) (e (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4.0 (c (n "ncomm") (v "0.4.0") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "03nvycmv9jvslj8qzxfjg6m2z2f065minkvkd1bphg1far8pbzmg") (f (quote (("default")))) (s 2) (e (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4.1 (c (n "ncomm") (v "0.4.1") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1rhmj71k82mdfwyj34hxb0afcd1z8kj5ih3x9xbnz96l8m0ga543") (f (quote (("default")))) (s 2) (e (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4.2 (c (n "ncomm") (v "0.4.2") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1njkv25m3f10siscf862l3s4zgz0jhq65zlzif9n12slhkldb6dm") (f (quote (("default" "packed-struct")))) (s 2) (e (quote (("packed-struct" "dep:packed_struct"))))))

