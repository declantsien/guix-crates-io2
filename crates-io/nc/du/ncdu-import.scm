(define-module (crates-io nc du ncdu-import) #:use-module (crates-io))

(define-public crate-ncdu-import-0.1.0 (c (n "ncdu-import") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "goldenfile") (r "^1.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "00760h6gfd0ymazskr0zdc9vsnadsh5x0ayh09q8bvzwqh9la1lp") (y #t)))

(define-public crate-ncdu-import-0.1.1 (c (n "ncdu-import") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "goldenfile") (r "^1.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0r7jyyxdv0w9zq47mnkjc99q7bmff086b8g0c4alpdjw5va4c08v")))

