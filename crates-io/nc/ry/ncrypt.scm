(define-module (crates-io nc ry ncrypt) #:use-module (crates-io))

(define-public crate-ncrypt-0.1.0 (c (n "ncrypt") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography"))) (d #t) (k 0)))) (h "1qn8s920dsvbpv94lv71hgaq65i0xwfq84zjn6h5vx7qfqyv7s8j")))

(define-public crate-ncrypt-0.2.0 (c (n "ncrypt") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography"))) (d #t) (k 0)))) (h "1aaqxq3mr7n76iww2rlfdl774wqr40vswqi60kjxyp0py7rgqnwv")))

