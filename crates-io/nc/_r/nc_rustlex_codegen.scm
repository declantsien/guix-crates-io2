(define-module (crates-io nc _r nc_rustlex_codegen) #:use-module (crates-io))

(define-public crate-nc_rustlex_codegen-0.3.1 (c (n "nc_rustlex_codegen") (v "0.3.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_codegen") (r "*") (o #t) (d #t) (k 1)) (d (n "syntex") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "*") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "*") (o #t) (d #t) (k 0)))) (h "15qdpyd8q8jm9nyni4z50c0czbm92ilwk39yyk4xllri9jmibbkm") (f (quote (("with-syntex" "quasi/with-syntex" "quasi_codegen/with-syntex" "syntex" "syntex_syntax")))) (y #t)))

