(define-module (crates-io nc _r nc_rsdk) #:use-module (crates-io))

(define-public crate-nc_rsdk-0.1.0 (c (n "nc_rsdk") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "eosio") (r "^0.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha256") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xz8y9995if5b2hrl6603lx7ncpr44pdznv4w6m60cihy8fzdrsq")))

