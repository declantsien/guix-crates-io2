(define-module (crates-io nc bi ncbi) #:use-module (crates-io))

(define-public crate-ncbi-0.1.0-beta (c (n "ncbi") (v "0.1.0-beta") (h "0bjp60jpwyl7p80hnrgw4jzcvylmq80r8vk1racgigd3l0haxkkv")))

(define-public crate-ncbi-0.2.0-beta (c (n "ncbi") (v "0.2.0-beta") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "1xab9wps4xq4g9n16lk0h34jwkg8b8cvdirprrvy45vk930j33va")))

