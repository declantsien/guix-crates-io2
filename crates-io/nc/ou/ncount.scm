(define-module (crates-io nc ou ncount) #:use-module (crates-io))

(define-public crate-ncount-0.1.3 (c (n "ncount") (v "0.1.3") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0hlv4d4gvplx5bvxpg49kq1s0zlqh29j3ffsvzl7dbbmrl8v14r3")))

(define-public crate-ncount-0.1.4 (c (n "ncount") (v "0.1.4") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1g99r4bfyzh9435pkwkrfs34lfsi3kysxld3y987f0drhfxw4hmk")))

(define-public crate-ncount-0.1.5 (c (n "ncount") (v "0.1.5") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ac8pxd9jjxznjbcsi3f8qc57b8441vw25rx1nmm602rmqxy5rc6")))

(define-public crate-ncount-0.1.6 (c (n "ncount") (v "0.1.6") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ly1njbmaq7hla86rk7ynhx7y4kar8y30zwnmqqln5axv3hyv6bv")))

(define-public crate-ncount-0.1.7 (c (n "ncount") (v "0.1.7") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zmp7g1il9733p9lbpznz59xb55d3wgmmd4dy7jyb9xk5hfq5mdz")))

(define-public crate-ncount-0.1.8 (c (n "ncount") (v "0.1.8") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ivzpl2kln0cslc0bpx1y20jm3nm62mmzxlyj6gd9wvmk9isvcz4")))

(define-public crate-ncount-0.1.9 (c (n "ncount") (v "0.1.9") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0fhvxvvw13pdci8bwygla4d18m8nrqb8b4rnzagvzl003rpqccd7")))

(define-public crate-ncount-0.1.10 (c (n "ncount") (v "0.1.10") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16907bz30xp790hzf4ww3y1miw5q3m06i06w83pbvd2rrg7aij46")))

(define-public crate-ncount-0.1.12 (c (n "ncount") (v "0.1.12") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0lbjda7bdbxzi8gfyda6mamm0ypnd4rb824p0c3gaqc1b86f49j6")))

(define-public crate-ncount-0.1.13 (c (n "ncount") (v "0.1.13") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1bbhsjd2kcmwm8ks2ldypayidmx046j3jjrjccw4b7dl6vyy677v")))

(define-public crate-ncount-0.1.14 (c (n "ncount") (v "0.1.14") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "02lg199p5pdz994gph72js2ih8wcjvh45186yvbkz7nm468dzj8k")))

(define-public crate-ncount-0.1.15 (c (n "ncount") (v "0.1.15") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "11yqsn3h24w1bpng523pd4893ims519mcp5v3sszmivgz3xdyhpk")))

(define-public crate-ncount-0.2.0 (c (n "ncount") (v "0.2.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0jml2j9v5aw00dbvcv40p6ccqh3cnsi8pq1x8y018xim6cnyanc6")))

(define-public crate-ncount-0.2.1 (c (n "ncount") (v "0.2.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1rj13jqqlcjb36hbi89jxiy5j0r9wmlkc61f1dz1vbj7kv7zhfpl")))

(define-public crate-ncount-0.2.2 (c (n "ncount") (v "0.2.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1710f8x0spm0f8b4cs3dazvl001k0187w23k20rli3hcs8b4zrg8")))

(define-public crate-ncount-0.2.3 (c (n "ncount") (v "0.2.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1i7l965sx5ki29r9vhqbyhvv54aksy16v72sgx8rynlnnfss65hd")))

(define-public crate-ncount-0.2.4 (c (n "ncount") (v "0.2.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0ddk6739x7dh0rzrdj4jncl560hmkmvwrj84w9vryn298zz51ffm")))

(define-public crate-ncount-0.2.5 (c (n "ncount") (v "0.2.5") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "04h1j0lazwibz7kc95mfcaxim8alnxhz72fzln8slgzi5mnv42gz")))

(define-public crate-ncount-0.2.6 (c (n "ncount") (v "0.2.6") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0a67mrryshx2sb9sc2z41lm6xzz6x7g1p0aghfix8cc9q4p4yphs")))

(define-public crate-ncount-0.2.7 (c (n "ncount") (v "0.2.7") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1sb95pia39qhlgvd71v964v1s1fyr3smbj19pswyx1ay9j27n5x0")))

(define-public crate-ncount-0.2.8 (c (n "ncount") (v "0.2.8") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1bg9262vqfpdw34xafww0kj42d9k732pdbi37dmn5gf8swm1c405")))

(define-public crate-ncount-0.2.9 (c (n "ncount") (v "0.2.9") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0l0sjvh2sdnb7hqv95nqrmlpsqsl2vjr5jgcm7wsarjmdx5800vy")))

(define-public crate-ncount-0.2.11 (c (n "ncount") (v "0.2.11") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "14lx51gxszfzjdh1yjsgjyk0kl3js8ryhkrazbnf0drcb7vvdix0")))

(define-public crate-ncount-0.2.12 (c (n "ncount") (v "0.2.12") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0613vn773fvm4ziz0vipwsh2zcm6plp021p50x1g6slvb9c4zxcv")))

(define-public crate-ncount-0.2.13 (c (n "ncount") (v "0.2.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "149rdn0z9lbn5ir0qi4x1wp7pnx39zz10pm9217kw7fn8dwws09v")))

(define-public crate-ncount-0.2.14 (c (n "ncount") (v "0.2.14") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zk8v7c43bi4mj2w2c5r32k5c5mppj50psxpazrqqkd7nnj9z1x9")))

(define-public crate-ncount-0.3.0 (c (n "ncount") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "16fnh85xb5fxfb7yfjk5zlxzsvppi5llbzzz28rwx1dh1vwq125f")))

(define-public crate-ncount-0.3.1 (c (n "ncount") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "11gawsgwx66kxknkbiyviff2r26bbfgj6lnr6ga1q8irk6zfy57p")))

(define-public crate-ncount-0.4.0 (c (n "ncount") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zn7hv7k00s848xa3zzrc8fj2l6nnvfjf3vjxriqxv6ncbjbh5lj")))

(define-public crate-ncount-0.5.0 (c (n "ncount") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1z6gy0aygrzsgxpb1jvn6zv84cl1pcv9xdv01iws1xzcinv64vnw")))

(define-public crate-ncount-0.5.1 (c (n "ncount") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1vz8g8m6xj19ybh16yz74n4bkcy092v9lqnvxd47sixpkybcm727")))

(define-public crate-ncount-0.5.3 (c (n "ncount") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1ylxkj7llb2czcdj0jasv2cc35nx6zxafz4bhv8lgk3fpr42jp4z")))

(define-public crate-ncount-0.5.4 (c (n "ncount") (v "0.5.4") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0cyv0r1nag01clq7w4pb2g7argz76r0cxqysqdpxj2vh5wans6xp")))

(define-public crate-ncount-0.5.5 (c (n "ncount") (v "0.5.5") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qaw0ba8s52k80jsjms38li0lh14sgpvffvw9qfzfq704w9sv614")))

(define-public crate-ncount-0.5.6 (c (n "ncount") (v "0.5.6") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "071v8v5z03z0cf3kzw7sym1dsy917qd2qxrzcqzxwqvkv9cv5jiq")))

(define-public crate-ncount-0.5.7 (c (n "ncount") (v "0.5.7") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0pl9k0547z144q7rl5fgk5jm5d8hz4gzjfbldl27xb71splymnd7")))

(define-public crate-ncount-0.5.8 (c (n "ncount") (v "0.5.8") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "156rdkpgrgkilrwm18330q24zzm6v6r8m81hf514prx8jxnr5f6j")))

(define-public crate-ncount-0.6.0 (c (n "ncount") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1k7cjm72qwzi3sfd5awzz2njznglmbqww9hy3rl5wpmjmx85q3sa")))

(define-public crate-ncount-0.6.2 (c (n "ncount") (v "0.6.2") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0dq0daxwzqisl5jyc5wnwwncb7bdv0z9vrxs0vqnvhk817r4rc31")))

(define-public crate-ncount-0.6.3 (c (n "ncount") (v "0.6.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "normal-paths") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0nmvfj4x4rlvjflhgj0hb32r8ccd18x207b0n7gx16pz57hkdxmv")))

(define-public crate-ncount-0.7.0 (c (n "ncount") (v "0.7.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "libsw") (r "^3.3.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "09ljgz727xv82s3h5ncg4nq04q74l52smf3h2n3v8bbyh4nak77i")))

