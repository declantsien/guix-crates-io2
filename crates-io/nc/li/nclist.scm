(define-module (crates-io nc li nclist) #:use-module (crates-io))

(define-public crate-nclist-0.1.0 (c (n "nclist") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "019m14lx9yzax27xczfr72v2ardh3lybf4vr2bk95lahf6y8x0pq")))

(define-public crate-nclist-0.1.1 (c (n "nclist") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1fn2cnc0rwywkq6zgs7qba49bndczcbrkwz3yzvvh6i7zw7ilaqs")))

