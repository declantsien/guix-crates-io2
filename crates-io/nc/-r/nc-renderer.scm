(define-module (crates-io nc -r nc-renderer) #:use-module (crates-io))

(define-public crate-nc-renderer-0.1.0 (c (n "nc-renderer") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-math3d") (r "^0.9.15") (d #t) (k 0)))) (h "0yjqi14jhq8sgnxvihflhqfv29yx3qv1h7krinld0qhnx7anzfrx")))

(define-public crate-nc-renderer-0.1.1 (c (n "nc-renderer") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-math3d") (r "^0.9.15") (d #t) (k 0)))) (h "0ii6q4xn4cpyjrdapssvv9d7rs17q20d2536mjjyfl223wffb7s2")))

(define-public crate-nc-renderer-0.1.2 (c (n "nc-renderer") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-math3d") (r "^0.9.16") (d #t) (k 0)))) (h "10fnb9v6bydz6x1gcfkj3fv3xcdzry6pnxnvjzpi9f61j9qcbx1f")))

(define-public crate-nc-renderer-0.1.3 (c (n "nc-renderer") (v "0.1.3") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)) (d (n "rs-math3d") (r "^0.9.16") (d #t) (k 0)))) (h "1w87gcj2hf2fxffxz7fydyy2lc2la591995799wd9863fk5szkld")))

