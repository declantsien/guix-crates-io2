(define-module (crates-io nc rs ncrs) #:use-module (crates-io))

(define-public crate-ncrs-0.1.0 (c (n "ncrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1wvwlycbk0k004qzxr5swx869ywkdwjkcw440xmxbzcph035l776")))

(define-public crate-ncrs-0.1.1 (c (n "ncrs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1xb8zq2j5p3hsa2zvr2ahnkll2dzs5df38rfxv80lk4ync0q51f8")))

