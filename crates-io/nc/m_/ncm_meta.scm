(define-module (crates-io nc m_ ncm_meta) #:use-module (crates-io))

(define-public crate-ncm_meta-0.2.0 (c (n "ncm_meta") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.0") (d #t) (k 0)))) (h "04xp12mw2yka9hx69s185fycrd9d99hhsrf9sb34nxdiqrdy0l6q")))

(define-public crate-ncm_meta-0.2.1 (c (n "ncm_meta") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.1") (d #t) (k 0)))) (h "1slp4jimi5ayr70qgjmkghsqjwpbmdn2n6bja5y0mc1iknqajlm1")))

(define-public crate-ncm_meta-0.2.2 (c (n "ncm_meta") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.2") (d #t) (k 0)))) (h "0y17dy93mh7x7nwd69fdg98cnpmzj454gbpv5346ig302svj8fnm")))

(define-public crate-ncm_meta-0.2.3 (c (n "ncm_meta") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.3") (d #t) (k 0)))) (h "0palrri5wac83d42hd1h8myjafxi053wbbsvjhpipdy0zdaax3qp")))

(define-public crate-ncm_meta-0.2.4 (c (n "ncm_meta") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.4") (d #t) (k 0)))) (h "0fd8dlh6zbn7pjjkv20zkhybnafj0lb85iygablah9jcnrrjq66n")))

(define-public crate-ncm_meta-0.2.5 (c (n "ncm_meta") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.5") (d #t) (k 0)))) (h "0nclrb6fdrj59bqdcnh82mhpgm9xd9pczrmykx6b0ar46bsyz88c")))

(define-public crate-ncm_meta-0.2.6 (c (n "ncm_meta") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.8.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.6") (d #t) (k 0)))) (h "0pmxng6fsi9di9c6zhdk1acbzq4aw574ik4fpnzavfz12c6aq9by")))

(define-public crate-ncm_meta-0.2.7 (c (n "ncm_meta") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.10.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.6") (d #t) (k 0)))) (h "1pz646yv78xivskv29713qfgp4d3f9xwglkl6m92jhx08dklqbkc")))

(define-public crate-ncm_meta-0.2.8 (c (n "ncm_meta") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "id3") (r "^1.10.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.8") (d #t) (k 0)))) (h "0fwh0mnhqfsqbhahxq3cg86fg8am4x1m3ig1v9j3p1hswi4smhyp")))

(define-public crate-ncm_meta-0.2.9 (c (n "ncm_meta") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "id3") (r "^1.13.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ncm_core") (r "^0.2.9") (d #t) (k 0)))) (h "0wcz1gyh3aripp8379bfi68mxji3a4a87is6sv0gx8r6v55qqif8")))

