(define-module (crates-io nc m_ ncm_core) #:use-module (crates-io))

(define-public crate-ncm_core-0.2.0 (c (n "ncm_core") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "1cgywian3q4nyy27n1ngjafikhqagpj7qkrhb7p67fydll9pv5gv")))

(define-public crate-ncm_core-0.2.1 (c (n "ncm_core") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "01q4bn12p14h3lw8ch0as821zsmxn9hda7am3f8wf8dj2irr7scs")))

(define-public crate-ncm_core-0.2.2 (c (n "ncm_core") (v "0.2.2") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "1qgsn5521w7j5nj3k9frm2v5lxmz3rpsvss350cw264kciivc05g")))

(define-public crate-ncm_core-0.2.3 (c (n "ncm_core") (v "0.2.3") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "0p87c2ryyavd9xfg00jjipxc2k8sc94c5y81xnc3drcxij3dpi87")))

(define-public crate-ncm_core-0.2.4 (c (n "ncm_core") (v "0.2.4") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "03820d7l4z53bkcs1spx4knkm2ya7pi35g581jb6cglh7y4jmkm4")))

(define-public crate-ncm_core-0.2.5 (c (n "ncm_core") (v "0.2.5") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "0mcda50qfdpnv6vzi999algyz8f4k3khdasxfcslm3w4pmnv2mfw")))

(define-public crate-ncm_core-0.2.6 (c (n "ncm_core") (v "0.2.6") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "161paf4mwkx8kdrnsbpl053803da2qmjwg6x1ilckgy8jkrklyd3")))

(define-public crate-ncm_core-0.2.7 (c (n "ncm_core") (v "0.2.7") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "15qm47nlqrf7l67m2dz6z9xdsmpbppkq9xgk5fwdwp28iy4j8z7w")))

(define-public crate-ncm_core-0.2.8 (c (n "ncm_core") (v "0.2.8") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "17ppqh0nj6gd550lbllvwdk98nvv5dgrkdzr06xz1y9chx78lai4")))

(define-public crate-ncm_core-0.2.9 (c (n "ncm_core") (v "0.2.9") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "ecb") (r "^0.1.2") (d #t) (k 0)))) (h "1hm9zg6mpqz58z6jlhvidpa6mpxa8yb2bcf0skz7w0kdfi3rr800")))

