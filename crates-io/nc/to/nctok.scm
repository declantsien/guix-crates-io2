(define-module (crates-io nc to nctok) #:use-module (crates-io))

(define-public crate-nctok-0.1.0 (c (n "nctok") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "human_format") (r "^1.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1himq3jjhrr25zjv9vc6wdylmjxfxhhs59fqjhwyc3krbas8v4ky")))

