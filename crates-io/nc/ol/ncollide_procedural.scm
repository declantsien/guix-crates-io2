(define-module (crates-io nc ol ncollide_procedural) #:use-module (crates-io))

(define-public crate-ncollide_procedural-0.2.0 (c (n "ncollide_procedural") (v "0.2.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "09qnxssdp0zscbdfgvdnaaj9jd8zpvrww4i8fskykjz106fwqi3p")))

(define-public crate-ncollide_procedural-0.2.1 (c (n "ncollide_procedural") (v "0.2.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "01gbjcb5s069636i8mm4bwcy6nx2zpb9gsapbdrq0kxwldysgkvi")))

(define-public crate-ncollide_procedural-0.2.2 (c (n "ncollide_procedural") (v "0.2.2") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0kq5a5z83qjbhph2i51yq7v5l6sf2w7xr88l75qnxh5a113h918b")))

(define-public crate-ncollide_procedural-0.2.3 (c (n "ncollide_procedural") (v "0.2.3") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0l3mjw1w9h8bwsn3130qxni1l26dn45yrd0g342pd2jv5sx4vwd3")))

(define-public crate-ncollide_procedural-0.2.4 (c (n "ncollide_procedural") (v "0.2.4") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fhim1vvlkfmr9f8v5a2zzjmw20vyws3ra6bvps9a5f4300a9j6y")))

(define-public crate-ncollide_procedural-0.2.5 (c (n "ncollide_procedural") (v "0.2.5") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.2.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.2.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "145bjy7600ww45y339zpk0jisb6aqmxfs1kx4r650l3cz4lgg24p")))

(define-public crate-ncollide_procedural-0.3.0 (c (n "ncollide_procedural") (v "0.3.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1gwdfd4zdji2yyc7xlz149482kah8s7kwf9xmkj9v6r42s73fnij")))

(define-public crate-ncollide_procedural-0.3.1 (c (n "ncollide_procedural") (v "0.3.1") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "03971km3yqvbfn5nz17fqrc5zmzakpi4924si6865qd8if6i6672") (y #t)))

(define-public crate-ncollide_procedural-0.4.0 (c (n "ncollide_procedural") (v "0.4.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.4.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1d3s227qr3v880b99c16sdirkzldb6w4laabhnrs5v20dvwxky62")))

(define-public crate-ncollide_procedural-0.5.0 (c (n "ncollide_procedural") (v "0.5.0") (d (list (d (n "nalgebra") (r "0.9.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.5.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.5.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "037w39b5s2pb3m0h7piqfvxqfcnbbgzx9n6apr6hdbsi67nfssva")))

(define-public crate-ncollide_procedural-0.6.0 (c (n "ncollide_procedural") (v "0.6.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.11") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.6") (d #t) (k 0)) (d (n "ncollide_utils") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jlf8pqyd2p36ld95za8629yw6bhljdda9jb3ad428ryrl8dy347")))

(define-public crate-ncollide_procedural-0.7.0 (c (n "ncollide_procedural") (v "0.7.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.7") (d #t) (k 0)) (d (n "ncollide_utils") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0h8a5qsjiiylg9n10vmza0kfgb8ya8rn5fzhmc22dssgd8ifkw02")))

(define-public crate-ncollide_procedural-0.8.0 (c (n "ncollide_procedural") (v "0.8.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.8") (d #t) (k 0)) (d (n "ncollide_utils") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1895nf58r33nq1nnx70i71jl5vj52p4dkxgjrja0i256ccbisi33")))

(define-public crate-ncollide_procedural-0.9.0 (c (n "ncollide_procedural") (v "0.9.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.9") (d #t) (k 0)) (d (n "ncollide_utils") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1m3izl9a3zhqzyahbjfxj7l1rgd7kp8ikv9mw0q5rrkr6ayygndl")))

(define-public crate-ncollide_procedural-0.9.1 (c (n "ncollide_procedural") (v "0.9.1") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.9") (d #t) (k 0)) (d (n "ncollide_utils") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1jij80c4jkcihlzf86bqysavbgrp34vq7c5bkvwrrnsbp64s0kia")))

