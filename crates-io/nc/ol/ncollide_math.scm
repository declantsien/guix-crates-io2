(define-module (crates-io nc ol ncollide_math) #:use-module (crates-io))

(define-public crate-ncollide_math-0.2.0 (c (n "ncollide_math") (v "0.2.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "18bcyw91kchgggmk7i42j98xq1sfcky74jvh6ncw8qii94k7474x")))

(define-public crate-ncollide_math-0.2.1 (c (n "ncollide_math") (v "0.2.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0maf81j1cvp0zczdb7069npf818h602aariq1lkdp7l2qg6pbffv")))

(define-public crate-ncollide_math-0.2.2 (c (n "ncollide_math") (v "0.2.2") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "16fz679j4869wl964jk35376w1wyg1swhdh65fr5s2awaqgv1nwy")))

(define-public crate-ncollide_math-0.3.0 (c (n "ncollide_math") (v "0.3.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "16fr7d7ldbi19x10k3fki4dnijk1iv8z4nn0032c67pbqaqrw05k")))

(define-public crate-ncollide_math-0.3.1 (c (n "ncollide_math") (v "0.3.1") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0g5zshp8w76zk3iyv0pbvbwd9y4cvs8qk8i7y8idvhkihdi1r19r") (y #t)))

(define-public crate-ncollide_math-0.4.0 (c (n "ncollide_math") (v "0.4.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1b93r2r01g5z5yy27jkqw21x3pw0r9rcgc1jnkimmi8jla1f6cf7")))

(define-public crate-ncollide_math-0.5.0 (c (n "ncollide_math") (v "0.5.0") (d (list (d (n "nalgebra") (r "0.9.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1nsxg1hh96plaj2zbhmr79lckvrkd11783wlncp94s8ywlsd1dh2")))

(define-public crate-ncollide_math-0.6.0 (c (n "ncollide_math") (v "0.6.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "008k47vgi33v1gqd7zhzhhw2p2baaj7gqm51f4f52viy7y7zxpc5")))

(define-public crate-ncollide_math-0.7.0 (c (n "ncollide_math") (v "0.7.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b2x53ji2nm5q35h22m10j7bxi5vm5pgf957gwb56ijv6mbzf4kg")))

(define-public crate-ncollide_math-0.8.0 (c (n "ncollide_math") (v "0.8.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gdyvjgi5jd9np64bbqcaa1k7rsdki9gp6r94c8jxbfxqil83f34")))

(define-public crate-ncollide_math-0.9.0 (c (n "ncollide_math") (v "0.9.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0h5cdai5dpiachv7vn2jysnd4lk7q4g3jf26awkn9i839gkzfwil")))

(define-public crate-ncollide_math-0.9.1 (c (n "ncollide_math") (v "0.9.1") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1sd070awdly4mmw1qv0qq149whhldcgn9z922vq4wqc296i04ijj")))

