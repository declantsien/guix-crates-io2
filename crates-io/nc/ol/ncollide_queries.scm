(define-module (crates-io nc ol ncollide_queries) #:use-module (crates-io))

(define-public crate-ncollide_queries-0.2.0 (c (n "ncollide_queries") (v "0.2.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1qbfjn8i77a16iaa86cn5iv5p88h8w37h5q1n1il1f2i4jqlvbg2")))

(define-public crate-ncollide_queries-0.2.1 (c (n "ncollide_queries") (v "0.2.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0099g0i4xmja4z183qvcdvrbcryz0jmzz86g9w9zr3cqakvz3ygm")))

(define-public crate-ncollide_queries-0.2.2 (c (n "ncollide_queries") (v "0.2.2") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0899q7ajbn5aa8lijbglwplznismbkvxanx8giz5001zv60j8aha")))

(define-public crate-ncollide_queries-0.2.3 (c (n "ncollide_queries") (v "0.2.3") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1l3hmf3463s4qrvijd0hihc8brmx149k110cm0iny4mkz8srk2hx")))

(define-public crate-ncollide_queries-0.2.4 (c (n "ncollide_queries") (v "0.2.4") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0n412jc0s63sk35g6wnhb9hxp4nmw3jfcyzx1kkhbdkdkndf88lb")))

(define-public crate-ncollide_queries-0.2.5 (c (n "ncollide_queries") (v "0.2.5") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15qc6kchn4ybr0j0al98aaxzpjxwmahqpfrbr8zs3wv7b5ppfi2w")))

(define-public crate-ncollide_queries-0.3.1 (c (n "ncollide_queries") (v "0.3.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_entities") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "129dhb33xny4vcx9mm7qlgjxylgiswa6wy6by9v0vzzgbiiy2ni9")))

(define-public crate-ncollide_queries-0.3.2 (c (n "ncollide_queries") (v "0.3.2") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "ncollide_entities") (r "0.4.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.2.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.2.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1a5ix8j8n9vbh4gnrd6vif2x7d2c5rw6jfcav6vcylx5ci54g0d2")))

(define-public crate-ncollide_queries-0.4.0 (c (n "ncollide_queries") (v "0.4.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_entities") (r "0.5.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1l8nd2jri3yd9zpl0851zcz7y8c76vp1dcibbv7m6zwv9in4ncrz")))

(define-public crate-ncollide_queries-0.4.1 (c (n "ncollide_queries") (v "0.4.1") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0njnvhf7r37is43gpx7yzz3xx57ryk1v72chnh4s1lqp64qis70p")))

