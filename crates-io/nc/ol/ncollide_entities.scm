(define-module (crates-io nc ol ncollide_entities) #:use-module (crates-io))

(define-public crate-ncollide_entities-0.2.0 (c (n "ncollide_entities") (v "0.2.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1vfqzr3c85w7ir2300ap3pcdqncngm0nd6k82n9m1902v2hx3p0h")))

(define-public crate-ncollide_entities-0.2.1 (c (n "ncollide_entities") (v "0.2.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1yb2f53kg70rry7wlsclxvn3z8c3bkd53plh3cxvw3pcw6qnjql1")))

(define-public crate-ncollide_entities-0.2.2 (c (n "ncollide_entities") (v "0.2.2") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1mk4zlhf4dgwbqnd4r1y08jc0a2zlvf90k1iaw4b8j16ajw2md35")))

(define-public crate-ncollide_entities-0.2.3 (c (n "ncollide_entities") (v "0.2.3") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j8fi77wa7z1q1wis9cp8ld4p5nhsx2adlkixy2kq5j7dwfrdn23")))

(define-public crate-ncollide_entities-0.4.0 (c (n "ncollide_entities") (v "0.4.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "ncollide_utils") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1g01mdy8gq2x8adcd6jr5wx3f9l9wxm1a1lpng9fvkzkbzh45589")))

(define-public crate-ncollide_entities-0.4.1 (c (n "ncollide_entities") (v "0.4.1") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.2.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.2.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1ibfipv503xmcbjh5nsdh58d2627gdcg01ywqqajzxyk1hf81p0h")))

(define-public crate-ncollide_entities-0.5.0 (c (n "ncollide_entities") (v "0.5.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "ncollide_utils") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1gnzwia08wj046ggfmg7j27lgjm32fwlahbfgi9iq8jphr20msgj")))

(define-public crate-ncollide_entities-0.5.1 (c (n "ncollide_entities") (v "0.5.1") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1vwh3nvapa78wgbyzgfnasqvl36s9s8c8sbjfk8cczp30dcswjv5")))

