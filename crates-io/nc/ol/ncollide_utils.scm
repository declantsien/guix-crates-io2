(define-module (crates-io nc ol ncollide_utils) #:use-module (crates-io))

(define-public crate-ncollide_utils-0.2.0 (c (n "ncollide_utils") (v "0.2.0") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0h2l3vzzk21a7r6bv17ck52y89igs56wi03dk0vhmdavk8m64acb")))

(define-public crate-ncollide_utils-0.2.1 (c (n "ncollide_utils") (v "0.2.1") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lg7zhxjz9h06xqyqpf0pbzfqrwp020nyly610d5z363nkk8pvdi")))

(define-public crate-ncollide_utils-0.2.2 (c (n "ncollide_utils") (v "0.2.2") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fl692wwhzq11p5ap98aylmqyf9w2rd4l8dpdslx0mfyml4smirn")))

(define-public crate-ncollide_utils-0.2.3 (c (n "ncollide_utils") (v "0.2.3") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "004hlb4308q01mh12ywl7q2ww22im30s83apv22b2ghv90khnzy8")))

(define-public crate-ncollide_utils-0.2.4 (c (n "ncollide_utils") (v "0.2.4") (d (list (d (n "nalgebra") (r "*") (d #t) (k 0)) (d (n "ncollide_math") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0zgk45bjknwgsji3njxbqiz1n9jkm76l5y8dblgprk46xgfrvcnc")))

(define-public crate-ncollide_utils-0.2.5 (c (n "ncollide_utils") (v "0.2.5") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.2.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "15zrxxwsrryd88jkbl1i1a7wf0sbkfv7wggvcbm27xx8jwpd1lak")))

(define-public crate-ncollide_utils-0.2.6 (c (n "ncollide_utils") (v "0.2.6") (d (list (d (n "nalgebra") (r "0.6.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.2.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0yanzlw1xs5bhqajfz5l15sca9bps8gsjll265kp211qkw3mn2vl")))

(define-public crate-ncollide_utils-0.3.0 (c (n "ncollide_utils") (v "0.3.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0cjyl7k6vjcq3cxr5vfflcamxf0f986hx3wprbkh2dq3qy1bd5h4")))

(define-public crate-ncollide_utils-0.3.1 (c (n "ncollide_utils") (v "0.3.1") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "02bw32y5cl54mhfnfxyph39a85jbb6mlkvw5pkqbqm85z2wr06sl") (y #t)))

(define-public crate-ncollide_utils-0.4.0 (c (n "ncollide_utils") (v "0.4.0") (d (list (d (n "nalgebra") (r "0.8.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0pg9vk7k9zciaf8y1knrpfb6h77hw6hpy70n0g0bwlph6w0kcyqp")))

(define-public crate-ncollide_utils-0.5.0 (c (n "ncollide_utils") (v "0.5.0") (d (list (d (n "nalgebra") (r "0.9.*") (d #t) (k 0)) (d (n "ncollide_math") (r "0.5.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1gk6hxdf4s6mqzxhxzdny4sk9d0a0hshfrsh1ss7y91qqn8jjhg0")))

(define-public crate-ncollide_utils-0.6.0 (c (n "ncollide_utils") (v "0.6.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.11") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0i4slnqzvq29iy2fdvfbg8z1n16qhbsrm59043jcmqwvz0wjvacp")))

(define-public crate-ncollide_utils-0.7.0 (c (n "ncollide_utils") (v "0.7.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "10y7hqbgmnix230m01iimvkh6aay3lvibzh1ddajrqfk3sfih90l")))

(define-public crate-ncollide_utils-0.8.0 (c (n "ncollide_utils") (v "0.8.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0984pzyx1kbhzx9cnk7f2m1zpn41zymf71ia3ip3zvpm0f4zh2rb")))

(define-public crate-ncollide_utils-0.9.0 (c (n "ncollide_utils") (v "0.9.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1gp031ba9q02d47lw3pbcd5w6c0bj72z5jf4x11k9p8cwzrr76x3")))

(define-public crate-ncollide_utils-0.9.1 (c (n "ncollide_utils") (v "0.9.1") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "ncollide_math") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "07arlhh1awbyc25khimadd8wdpscqgl5dz4whl6hm8kmbbaymqg3")))

