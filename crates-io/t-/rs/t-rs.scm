(define-module (crates-io t- rs t-rs) #:use-module (crates-io))

(define-public crate-t-rs-0.1.0 (c (n "t-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1d5qy976wh0bv1zyf3biyrml89cabml98615iyw64yi34w6ramsd")))

(define-public crate-t-rs-0.1.1 (c (n "t-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1cfqhfb81ih0zrf4bikhg006x1s6dl1v2bxqi8q03ahf8w0wihjv")))

(define-public crate-t-rs-0.1.2 (c (n "t-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1iw9j20qk8313x5008akbc54kxqz0mq6q4l0s7mbhjj6hqks7f3y")))

(define-public crate-t-rs-0.1.3 (c (n "t-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "18rhsc1s41zy38vrgf115wpwy803nv769fdh2q06qmy1g6j0bhhv")))

(define-public crate-t-rs-0.1.4 (c (n "t-rs") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0w6pn3qbwj3bfwxh3dpfy7ax8l69f8xg7s96hnm82df7w7z7j94k")))

(define-public crate-t-rs-0.1.5 (c (n "t-rs") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1xkc8a3jhfwxkgnc8skhp3c4v8r528kgqrk0s7nr21wlkc3bw3r6")))

(define-public crate-t-rs-0.1.6 (c (n "t-rs") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "02p01xdci5ivwpfkxa8k1cy2rnpgsmp5v87dfz85icmcirvpb73m")))

(define-public crate-t-rs-0.1.7 (c (n "t-rs") (v "0.1.7") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0k0jb3l6bhbp7yxif5lqx207l5s95193310npvbavqpbw4fzxj1h")))

(define-public crate-t-rs-0.1.8 (c (n "t-rs") (v "0.1.8") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1mc3zb4paws2i0klya1a3g4n2kjbv1r15x3cwbhvcq7w0ya48smq")))

(define-public crate-t-rs-0.1.10 (c (n "t-rs") (v "0.1.10") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0zfrz8nb2i2zg86myqyqyvy5zgbgq7l8x6kvzli10lvibf7absr8")))

(define-public crate-t-rs-0.1.11 (c (n "t-rs") (v "0.1.11") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0q7xaxmy5xahcd497w7wwzjmv34jlkdshwj5jz0q69zrfbgwczq9")))

(define-public crate-t-rs-0.1.12 (c (n "t-rs") (v "0.1.12") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0fc2ra69pj5ad1n4c8z8qgv6wad6f1svshh8x0iprixqw426ml7r")))

(define-public crate-t-rs-0.2.1 (c (n "t-rs") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0jngglk8w7hk9ahbrnvv3cmrj2nyw2qjcyx5whhdjj2qxfq38yls")))

(define-public crate-t-rs-0.3.0 (c (n "t-rs") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "191rwk797wfrmxkcd50nxa70hv0alblmrj5aa3n18sjx0lrkw826")))

