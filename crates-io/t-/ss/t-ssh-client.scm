(define-module (crates-io t- ss t-ssh-client) #:use-module (crates-io))

(define-public crate-t-ssh-client-0.1.0 (c (n "t-ssh-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "thrussh") (r "^0.33.5") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1babwn1k2g4jhg6akdql5cpbjmn31kf2c24lznm02sl9kjsyc8s8")))

(define-public crate-t-ssh-client-0.1.1 (c (n "t-ssh-client") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thrussh") (r "^0.33.5") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0q0n7w1g9arq53p68zqqzifgrc4a456xw5wdq3c1lp7ffv1xvfnh")))

(define-public crate-t-ssh-client-0.1.2 (c (n "t-ssh-client") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thrussh") (r "^0.33.5") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zn81fj5gvfjnyq31v0js0xk9zshcpv677c8gf4wz5ffr4fq4sb9")))

(define-public crate-t-ssh-client-0.1.3 (c (n "t-ssh-client") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thrussh") (r "^0.33.5") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0y6qzqxprxqzh164rzy2i9w38pxnk2wv9sincllpb18nc3d5q067") (y #t)))

(define-public crate-t-ssh-client-0.2.0 (c (n "t-ssh-client") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thrussh") (r "^0.33.5") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fq0haa3wwz0zbwwg4wfc3jgcz0rjv54q5kfd010qkpxa1r3246j")))

(define-public crate-t-ssh-client-0.2.1 (c (n "t-ssh-client") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thrussh") (r "^0.33.5") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1syw07qn7x4bdicz2pqrs1q65577nb38dm0l6l3k8kn5mjs1l9lz") (f (quote (("openssl" "thrussh/openssl" "thrussh-keys/openssl"))))))

