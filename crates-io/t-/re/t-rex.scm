(define-module (crates-io t- re t-rex) #:use-module (crates-io))

(define-public crate-t-rex-0.0.0 (c (n "t-rex") (v "0.0.0") (h "02zc2hcb0vvkkgzicx9d0lqnyc3bmhb6jhmwyhm5vsf966zkl0yi")))

(define-public crate-t-rex-0.1.0 (c (n "t-rex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zhjahrqkn6q4mmq7gq2lablia3g57q4iqavp84v3xxrgf45mnwq")))

(define-public crate-t-rex-0.1.1 (c (n "t-rex") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01ad4dnnsb815wa98wisjzr2yjdwivjdm79pgcdcbmqm4xx8ywf8")))

