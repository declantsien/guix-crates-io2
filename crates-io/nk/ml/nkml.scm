(define-module (crates-io nk ml nkml) #:use-module (crates-io))

(define-public crate-nkml-0.0.1 (c (n "nkml") (v "0.0.1") (h "1bx87j62v2sbzp8611wd31m0xp5svy3mfrby5k2kvgnciz5bppgh") (y #t)))

(define-public crate-nkml-0.0.2 (c (n "nkml") (v "0.0.2") (h "0ikzs44lbfdpbzj71cwm1mf8nys7kc3vs4js7axb0014cnwgh409")))

