(define-module (crates-io nk ow nkowne63-wordle-solver-rs-01) #:use-module (crates-io))

(define-public crate-nkowne63-wordle-solver-rs-01-0.1.0 (c (n "nkowne63-wordle-solver-rs-01") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "repl-rs") (r "^0.2.6") (d #t) (k 0)))) (h "1kw72nng1495m5fqmdzmsyz8rxzym4pxhy7w9q9map6z35m1firl") (y #t)))

(define-public crate-nkowne63-wordle-solver-rs-01-0.1.1 (c (n "nkowne63-wordle-solver-rs-01") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "repl-rs") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0q6jc5kgjrjv8gnfpv1631lvfd2mng4cw1y7qhghgvvvmrxxq7k5") (f (quote (("local" "repl-rs")))) (y #t)))

(define-public crate-nkowne63-wordle-solver-rs-01-0.1.2 (c (n "nkowne63-wordle-solver-rs-01") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "repl-rs") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0693syhgk7zq34qy3h5c17mxrbrwyx9qyjli5av4v8xbikf7wjz6") (f (quote (("local" "repl-rs")))) (y #t)))

(define-public crate-nkowne63-wordle-solver-rs-01-1.0.0 (c (n "nkowne63-wordle-solver-rs-01") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "repl-rs") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0bgihdxx6qycv6nfn5jb6p36s24224r0h53ww5ycrbbqqnfscxmg") (f (quote (("local" "repl-rs")))) (y #t)))

(define-public crate-nkowne63-wordle-solver-rs-01-1.0.1 (c (n "nkowne63-wordle-solver-rs-01") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "repl-rs") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "1lvyrhfq2mkczccsvnasc5xnj1bl1l8a12412agxn3ad20gjsz28") (f (quote (("local" "repl-rs"))))))

