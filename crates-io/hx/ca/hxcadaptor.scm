(define-module (crates-io hx ca hxcadaptor) #:use-module (crates-io))

(define-public crate-hxcadaptor-0.1.0 (c (n "hxcadaptor") (v "0.1.0") (d (list (d (n "hxcadaptor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1mxdh120z21ssbz1cgpfpdfzx362lmrl5fbr2jf8lcsg8yirxvgi")))

(define-public crate-hxcadaptor-0.1.1 (c (n "hxcadaptor") (v "0.1.1") (d (list (d (n "hxcadaptor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "07qm7fvz5xypclqv86sbmxiw9i9klf5fbxkz6iy48ckwhf17fzcv")))

(define-public crate-hxcadaptor-0.1.2 (c (n "hxcadaptor") (v "0.1.2") (d (list (d (n "hxcadaptor-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0w4zj1c7dih8ma31h6qsc82arcs1y0xl55rs5z51jcsg3bqdmm37")))

(define-public crate-hxcadaptor-0.1.3 (c (n "hxcadaptor") (v "0.1.3") (d (list (d (n "hxcadaptor-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0dplc772c63zicd9zgj4aggsyp6aiziq6aigzaszlxd2v1hbq71b")))

(define-public crate-hxcadaptor-0.1.4 (c (n "hxcadaptor") (v "0.1.4") (d (list (d (n "hxcadaptor-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0xhzprkpxvfz0qk3p8pmzh8p1iw21can0x74wcb9asqy4w9w8xc5")))

