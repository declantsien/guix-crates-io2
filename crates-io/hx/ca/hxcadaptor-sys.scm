(define-module (crates-io hx ca hxcadaptor-sys) #:use-module (crates-io))

(define-public crate-hxcadaptor-sys-0.1.0 (c (n "hxcadaptor-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "04i3kqlhalihk6451vlizba4f1ybg99j66lx905nj6kwkm8ijs91") (l "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1.1 (c (n "hxcadaptor-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1giazb8z3zj4g2h1wbz86wa3kjhx9i71qiy7xnxsnhcknmd1xa37") (l "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1.2 (c (n "hxcadaptor-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0b1jd2zkzc9x7y8q9h5sb5b53jhbrszxiw8hdhg9hmc21yd166j8") (l "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1.3 (c (n "hxcadaptor-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0cl50q6c3irlanj2chka3mw6rb720ncnfvpk6g09dsqmzf5hpslk") (l "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1.4 (c (n "hxcadaptor-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1cn10jx5lcw3hiab5z474ivvw70kw1glynk7hr0j7rxrp7ip5mnd") (l "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1.5 (c (n "hxcadaptor-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1j03w724rya9v1vzgr4j5sw2cr2zy9ljizv1zk0q6dzq0izixij6") (l "hxcadaptor")))

