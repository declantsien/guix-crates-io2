(define-module (crates-io hx cf hxcfe-sys) #:use-module (crates-io))

(define-public crate-hxcfe-sys-0.1.0 (c (n "hxcfe-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0vn48sb6s8phvm3az6sq9y6y0j61imi64kyh7kskdlng1g1pbb1x") (l "hxcfe")))

(define-public crate-hxcfe-sys-0.1.1 (c (n "hxcfe-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "127xw9hb1s2w3q51kbczdhbljpch65mc8vinhp8zklimnbnh1qb9") (l "hxcfe")))

(define-public crate-hxcfe-sys-0.1.2 (c (n "hxcfe-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1yqk293pyc19xxvnc2k77fra172c2mqzc590abrkz14gxwyv086b") (l "hxcfe")))

(define-public crate-hxcfe-sys-0.1.3 (c (n "hxcfe-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0qyl5lfs2n4d5jlr0aasl3c9121sjs9z9xzwc8sfy2yqwwz3506m") (l "hxcfe")))

(define-public crate-hxcfe-sys-0.1.4 (c (n "hxcfe-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor") (r "^0.1.4") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0hdpxi4kcn82kszkizn4c0ynifbmqyxfaw6flki2n1xfgc5vkr1g") (l "hxcfe")))

(define-public crate-hxcfe-sys-0.1.5 (c (n "hxcfe-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "hxcadaptor-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0mnr1xn2arsz5n328f99zq4w18yr4jghf5i8v49kc6a8jrs12ivb") (l "hxcfe")))

