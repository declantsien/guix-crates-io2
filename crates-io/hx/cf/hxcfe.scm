(define-module (crates-io hx cf hxcfe) #:use-module (crates-io))

(define-public crate-hxcfe-0.1.0 (c (n "hxcfe") (v "0.1.0") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "13j0rbqxh8zais21h0hkcaf4225r0l0d5scwz23p1ljxvb0j91h0")))

(define-public crate-hxcfe-0.1.1 (c (n "hxcfe") (v "0.1.1") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0b28m37637r9clz8g0f3hnb9bmyqz41skvf6kwia5m3wi5k5gc9v")))

(define-public crate-hxcfe-0.1.2 (c (n "hxcfe") (v "0.1.2") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0wwb6yybnljhwcmvqjnmyqragblbbc05bh1yw4k6x9jwwxn6q4ql")))

(define-public crate-hxcfe-0.1.3 (c (n "hxcfe") (v "0.1.3") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "168xlw4s8h4jrpck2qivznp4dxl2imdfrhaprkfhlsb6m55k5bkf")))

(define-public crate-hxcfe-0.1.4 (c (n "hxcfe") (v "0.1.4") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1xkpswv0skgc374rqnnadn5y1xm0d7ywli81cla5ljwxlms6kpsw")))

(define-public crate-hxcfe-0.1.5 (c (n "hxcfe") (v "0.1.5") (d (list (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "hxcfe-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0xxmxxrw21730v45gqbxzk6x4xklwmp6wh810a5m9hyviyfibzc0")))

