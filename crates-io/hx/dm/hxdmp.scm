(define-module (crates-io hx dm hxdmp) #:use-module (crates-io))

(define-public crate-hxdmp-0.1.0 (c (n "hxdmp") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1m3fdzflhp44ianl1bl7h63pikl0085rhjv3fvvyiqjxrq1pgvis")))

(define-public crate-hxdmp-0.2.0 (c (n "hxdmp") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "09njjriy0wgkm2sigj7bdlr7xgdms45dzw3m1mwsgcc3kqnl9zqx") (y #t)))

(define-public crate-hxdmp-0.2.1 (c (n "hxdmp") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1c66j4z423w2lc3iqzzbg10y8ip58i90lpx7mimq8rklibr2fyx1")))

