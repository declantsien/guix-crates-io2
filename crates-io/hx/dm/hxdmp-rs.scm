(define-module (crates-io hx dm hxdmp-rs) #:use-module (crates-io))

(define-public crate-hxdmp-rs-0.1.0 (c (n "hxdmp-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0cdqgp5ncm0hcbsc2iax48cw6jjn4311gg01bp68snj5mcjfgiyv") (y #t)))

(define-public crate-hxdmp-rs-0.1.1 (c (n "hxdmp-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "18qcvvb4xgksf0kxrq9mw4q2yvz42wyljd8g47k8hwn2amlz1n3z") (y #t)))

