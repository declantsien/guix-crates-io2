(define-module (crates-io hx gm hxgm30-noise) #:use-module (crates-io))

(define-public crate-hxgm30-noise-0.1.0 (c (n "hxgm30-noise") (v "0.1.0") (d (list (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "0yrbmisf2g5d7dwjgqkwp90ywfq64s4gw7487sph9pp67dhfq0gq")))

(define-public crate-hxgm30-noise-0.2.0 (c (n "hxgm30-noise") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)) (d (n "twyg") (r "^0.1.7") (d #t) (k 0)))) (h "1ds4pjm9sikqq0yk7l2cdf4c4l90afhsjmgb6r752cn1awfc5ibr")))

(define-public crate-hxgm30-noise-0.3.0 (c (n "hxgm30-noise") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image-data") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)) (d (n "twyg") (r "^0.1.7") (d #t) (k 0)))) (h "02c6jxlypir6r6zqlxxhmzam49j52d70z3anmrij174ry720smpq")))

