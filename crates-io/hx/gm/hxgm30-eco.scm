(define-module (crates-io hx gm hxgm30-eco) #:use-module (crates-io))

(define-public crate-hxgm30-eco-0.1.0 (c (n "hxgm30-eco") (v "0.1.0") (d (list (d (n "hxgm30-noise") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "image-data") (r "^0.2.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "twyg") (r "^0.1.7") (d #t) (k 0)))) (h "194p0wxda3pwvzqqc2s95kfh6nkhh7i75jsdj17m45hczq7xbqd6")))

