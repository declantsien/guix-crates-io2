(define-module (crates-io hx #{71}# hx711_spi) #:use-module (crates-io))

(define-public crate-hx711_spi-0.2.1 (c (n "hx711_spi") (v "0.2.1") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1d03hf9cci17cl8b8f3p3q9ryccnw5ap9cx6h33xsprfijf5xmxk")))

(define-public crate-hx711_spi-0.3.1 (c (n "hx711_spi") (v "0.3.1") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1kir5j3p6gcapvzppbxfdpsv468mghbr2h28fy3ca6v8wwpsar2p")))

(define-public crate-hx711_spi-0.3.2 (c (n "hx711_spi") (v "0.3.2") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)))) (h "055spl6zbayxvl6h5mmxzzdy07vn1shnlmvq85fnq6b75pmxf9c1")))

(define-public crate-hx711_spi-0.4.0 (c (n "hx711_spi") (v "0.4.0") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)))) (h "1hjj2krzid9z9wxdcr9p47z1z4a36hy1jck21n1rl6gm7sc6jp4w")))

(define-public crate-hx711_spi-0.4.2 (c (n "hx711_spi") (v "0.4.2") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "rppal") (r "^0.14.1") (f (quote ("hal"))) (d #t) (k 2)) (d (n "test-case") (r "^3") (d #t) (k 2)))) (h "1y8kmgmv21yyhq68yhlxi5zlnyq35lhgk94c9p9hpiwzrchrqy66")))

(define-public crate-hx711_spi-0.5.0 (c (n "hx711_spi") (v "0.5.0") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "rppal") (r "^0.14.1") (f (quote ("hal"))) (d #t) (k 2)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "0ibc79fnc1g3qnx6i73yr17wjbbpyzclfr4j8jyr549f3a6ymqna") (f (quote (("invert-sdo"))))))

