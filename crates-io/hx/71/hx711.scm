(define-module (crates-io hx #{71}# hx711) #:use-module (crates-io))

(define-public crate-hx711-0.1.0 (c (n "hx711") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1x7rabr8kwg1ry9bn2qcdib82rc0vd0c3fhj0lzb97r3fpv66zri")))

(define-public crate-hx711-0.2.0 (c (n "hx711") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1jrr8i25cd53y67bwg6nclsz1d0jixdvawx039jr53a3lfq6ljif")))

(define-public crate-hx711-0.2.1 (c (n "hx711") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1zkcdda70iyiyjirwak6r58jlmsmnn6ysz5jjib2q6f67320cavd")))

(define-public crate-hx711-0.3.0 (c (n "hx711") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1rahhb792afcz2lsvp01mi3rqdq15si1xsn7dzabgx40xqvbhs4r")))

(define-public crate-hx711-0.4.0 (c (n "hx711") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "151v5jq5hanl0h5xhan7w4iv60fclk5ya792srh0a32kyxd9jkff")))

(define-public crate-hx711-0.5.0 (c (n "hx711") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "14a6qvnbwph8lypc6vykfn59rnvpilhvjn26xds8sdrnl3ii9dcd") (f (quote (("never_type"))))))

(define-public crate-hx711-0.6.0 (c (n "hx711") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "187z76yps70r923zzmrq3aag65aiyl8j8p2pma6zmg84db106s3v") (f (quote (("never_type"))))))

