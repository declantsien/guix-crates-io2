(define-module (crates-io s- ex s-expr) #:use-module (crates-io))

(define-public crate-s-expr-0.1.0 (c (n "s-expr") (v "0.1.0") (d (list (d (n "unicode-xid") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0al90akdh4i3likcj73a8hbsd4vmicbqq9gxa45rahaf5xk3078r") (f (quote (("unicode" "unicode-xid") ("default" "unicode"))))))

(define-public crate-s-expr-0.1.1 (c (n "s-expr") (v "0.1.1") (d (list (d (n "unicode-xid") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1x1rs3nfjy31qc3aaj5n2r40ak2phh8v2qgfb8xjj34i3g9j98kk") (f (quote (("unicode" "unicode-xid") ("default" "unicode"))))))

