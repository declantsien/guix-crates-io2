(define-module (crates-io s- se s-secp256k1) #:use-module (crates-io))

(define-public crate-s-secp256k1-0.5.7 (c (n "s-secp256k1") (v "0.5.7") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1gz8j3lmnqvzkrb9jqy9y6gd6ii42xikd2d9xzc8vsjzqhl0dbps") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

