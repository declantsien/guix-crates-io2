(define-module (crates-io s- js s-jsonrpc-tcp-server) #:use-module (crates-io))

(define-public crate-s-jsonrpc-tcp-server-8.0.0 (c (n "s-jsonrpc-tcp-server") (v "8.0.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "s-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "s-jsonrpc-server-utils") (r "^8.0") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "1vbzynh3ycvvykw0n22xhmy6yzqq3rffbgiylcxp4nr68pz3lydf")))

