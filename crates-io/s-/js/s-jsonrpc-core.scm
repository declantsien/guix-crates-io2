(define-module (crates-io s- js s-jsonrpc-core) #:use-module (crates-io))

(define-public crate-s-jsonrpc-core-8.0.0 (c (n "s-jsonrpc-core") (v "8.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05ag6ypyn4mwaai3223vb3gjyyjx3xxskf3i1mdi599m9yglz60i")))

