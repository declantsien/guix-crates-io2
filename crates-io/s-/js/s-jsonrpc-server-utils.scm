(define-module (crates-io s- js s-jsonrpc-server-utils) #:use-module (crates-io))

(define-public crate-s-jsonrpc-server-utils-8.0.0 (c (n "s-jsonrpc-server-utils") (v "8.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "globset") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "s-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0a8i77fwaigvk7xzqlil3jmcw44979fd7v2hjgw7nn78zjny7m3d")))

