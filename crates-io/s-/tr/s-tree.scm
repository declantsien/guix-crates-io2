(define-module (crates-io s- tr s-tree) #:use-module (crates-io))

(define-public crate-s-tree-0.1.0 (c (n "s-tree") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1wam2kr3512npj24g2b6g0ym844y3h7bhpfnz0znfy8znwpz1kww")))

(define-public crate-s-tree-0.3.0 (c (n "s-tree") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1cw18cx81ahqz6vi2wnj7lscq7y6mq85iazlxxiqddhp4n1zyaj7")))

