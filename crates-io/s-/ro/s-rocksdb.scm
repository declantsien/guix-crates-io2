(define-module (crates-io s- ro s-rocksdb) #:use-module (crates-io))

(define-public crate-s-rocksdb-0.5.0 (c (n "s-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "s-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "00nr77n2y2dbxbz8zaaif12ahj855gzvw4p1j3d5m0qar0brm9m0") (f (quote (("valgrind") ("default"))))))

