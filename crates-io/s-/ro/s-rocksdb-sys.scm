(define-module (crates-io s- ro s-rocksdb-sys) #:use-module (crates-io))

(define-public crate-s-rocksdb-sys-0.5.5 (c (n "s-rocksdb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "1vzwbv3ahm9qxllzqglcz1wccncjvybbj6k8xnfw9z7k4vsb8yvf") (f (quote (("default")))) (l "rocksdb")))

