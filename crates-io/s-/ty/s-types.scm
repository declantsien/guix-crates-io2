(define-module (crates-io s- ty s-types) #:use-module (crates-io))

(define-public crate-s-types-0.4.0 (c (n "s-types") (v "0.4.0") (d (list (d (n "crunchy") (r "^0.1") (d #t) (k 0)) (d (n "fixed-hash") (r "^0.2.3") (k 0)) (d (n "s-types-serialize") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sbloom") (r "^0.5.0") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uint") (r "^0.4.1") (k 0)))) (h "0w71sy7i6ddndgmn0hr85aaxkj1g4s2r1ms4ca93cqrp5f065n19") (f (quote (("std" "uint/std" "fixed-hash/std" "sbloom/std") ("serialize" "std" "s-types-serialize" "serde" "sbloom/serialize") ("heapsizeof" "uint/heapsizeof" "fixed-hash/heapsizeof" "sbloom/heapsizeof") ("default" "std" "heapsizeof" "serialize"))))))

