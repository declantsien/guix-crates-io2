(define-module (crates-io s- st s-structured-log) #:use-module (crates-io))

(define-public crate-s-structured-log-0.1.0 (c (n "s-structured-log") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.3") (d #t) (k 0)))) (h "1694ycd5wd9plc533ydsa4k6y0c730mg0ybkkak8rqk4q49g6672") (f (quote (("default"))))))

(define-public crate-s-structured-log-0.2.0 (c (n "s-structured-log") (v "0.2.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "1qvxsgwghivqz7rad3z8vdajyxf05vm5j6yrspmskya33zz74gg0") (f (quote (("default"))))))

