(define-module (crates-io se rw serwus-derive) #:use-module (crates-io))

(define-public crate-serwus-derive-0.1.0 (c (n "serwus-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1imsl3sv08znwyq8x5ly99zsqi99nqaaxz7g19qp9lqqmm9vi1nr")))

(define-public crate-serwus-derive-0.1.1 (c (n "serwus-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1r8ffr1dzcjr24d2akxf3jz5v2wxq2ifjbj2iqh7gn8w55k7lk52")))

(define-public crate-serwus-derive-0.1.2 (c (n "serwus-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1w2l78cvaiwvm9z0w5isq0an226iv4f8c88hrbg5v25dmpfcn2jd")))

(define-public crate-serwus-derive-0.2.0 (c (n "serwus-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fxdmdwfmwqz4fn7ryvlbingkm0nfq7kqa1kfcm9pjr86k3hbsiy")))

