(define-module (crates-io se gf segfaultai) #:use-module (crates-io))

(define-public crate-segfaultai-2024.1.6022324-staging (c (n "segfaultai") (v "2024.1.6022324-staging") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1545fdi3zvf9pshf80y36ldzyq9ipzzsf6b8vgbk6qrk7bpm8v2y") (f (quote (("staging") ("local") ("default"))))))

(define-public crate-segfaultai-2024.1.6022608-staging (c (n "segfaultai") (v "2024.1.6022608-staging") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "17ynab9a5vy0cnni3rxsdh800i9l4ji9qzzv8gby2hms7hn94grd") (f (quote (("staging") ("local") ("default"))))))

(define-public crate-segfaultai-2024.1.6022900-staging (c (n "segfaultai") (v "2024.1.6022900-staging") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0lk28pjrdc9sz53am0y79yi6dl56n9zs2gnn542fh7192i7r1274") (f (quote (("staging") ("local") ("default"))))))

(define-public crate-segfaultai-2024.1.6023801 (c (n "segfaultai") (v "2024.1.6023801") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01l5bs889834qfyypirihcf7fn3hsfghdvvlfqap7mm72p1hp3r6") (f (quote (("staging") ("local") ("default"))))))

(define-public crate-segfaultai-2024.1.23121311 (c (n "segfaultai") (v "2024.1.23121311") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wh4fqkib6y6idvc18bk22vzj0w8zh90d8z20nxb504y2xpk00x9") (f (quote (("staging") ("local") ("default"))))))

(define-public crate-segfaultai-2024.1.23122245-staging (c (n "segfaultai") (v "2024.1.23122245-staging") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gstreamer") (r "0.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls-webpki-roots"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1f00c9x65q7v6ip7lawisdrg51c5bsbhgmn8h6x92rhrsj4mr6zs") (f (quote (("staging") ("local") ("default"))))))

