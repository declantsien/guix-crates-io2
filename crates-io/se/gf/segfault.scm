(define-module (crates-io se gf segfault) #:use-module (crates-io))

(define-public crate-segfault-0.1.0 (c (n "segfault") (v "0.1.0") (h "19rzdma0z3f2m48k3gjzjc32waa7vinsjgxh2vlk4dy49cwj2nn4")))

(define-public crate-segfault-0.1.1 (c (n "segfault") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.142") (d #t) (k 0)))) (h "1lwcmspcw3y1z83hv381891mplk06bypg6vqwnc111jphpdshhal")))

