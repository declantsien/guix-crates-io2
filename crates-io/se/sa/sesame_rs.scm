(define-module (crates-io se sa sesame_rs) #:use-module (crates-io))

(define-public crate-sesame_rs-0.1.0 (c (n "sesame_rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1bzfqan8j3jyh1f64fj401vqi4w4vdb58arvmk1z6i8sg6dmsncn") (y #t)))

(define-public crate-sesame_rs-0.1.1 (c (n "sesame_rs") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1203kdccrwk9crq5j34hh25fjz94dgksn7ff1klrylnab20g8dqk")))

(define-public crate-sesame_rs-0.1.2 (c (n "sesame_rs") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0qdra910m4vsc8wh6nnd1yphqbkdrhh4z83grwplg9f9cs5mb6jv")))

(define-public crate-sesame_rs-0.1.3 (c (n "sesame_rs") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0d3ns47cmjwgbdsyf824mc24fiwnz9xam9qivg0wb8vv3c20xinf")))

