(define-module (crates-io se p5 sep5) #:use-module (crates-io))

(define-public crate-sep5-0.0.1 (c (n "sep5") (v "0.0.1") (d (list (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "1xlh7lgkdm2rnwmvgyq8i9nc8q3vwd15q96y3jr3qjvbwldm8sgc") (r "1.66")))

(define-public crate-sep5-0.0.2 (c (n "sep5") (v "0.0.2") (d (list (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0sik9y412xixf4dzxgn3hkjg3lykgc1wpk675w5zmdhzpp639zls") (r "1.67")))

(define-public crate-sep5-0.0.3 (c (n "sep5") (v "0.0.3") (d (list (d (n "slip10") (r "^0.4.3") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "14r5gg0q7z4ax0h9n358zcg2ahr754c09ax7zs6xnpqqb714v9hb") (r "1.67")))

