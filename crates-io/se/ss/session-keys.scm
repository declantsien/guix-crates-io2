(define-module (crates-io se ss session-keys) #:use-module (crates-io))

(define-public crate-session-keys-2.0.1 (c (n "session-keys") (v "2.0.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "session-keys-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "1nqdi897myh87xj6qr0x5dmn1ng2w1p9nw4rs23p8403wg6pqgww") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "session-keys-macros") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-session-keys-2.0.2 (c (n "session-keys") (v "2.0.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "session-keys-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "0fr6k2pccgg1hirdrxp4j2bmlbzls70s00vgs12b4431kwy0f57a") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "session-keys-macros") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-session-keys-2.0.3 (c (n "session-keys") (v "2.0.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "session-keys-macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "01mhggr17rvvll5k2psk7f2b9xbjkss67xr8gz775bp9r1z05cx9") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "session-keys-macros") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-session-keys-2.0.4 (c (n "session-keys") (v "2.0.4") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)) (d (n "session-keys-macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "=1.1.1") (d #t) (k 0)))) (h "1fvfjrn8f6wf61rh9a79g6xbnd9rnmgpyj0w6wpjixjrbj4lddfc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "session-keys-macros") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

