(define-module (crates-io se ss sesstype) #:use-module (crates-io))

(define-public crate-sesstype-0.1.0 (c (n "sesstype") (v "0.1.0") (h "0awk1dypz684bv499bxbvdzmaz6ngbn5v28qyps68vsvkisgsx31")))

(define-public crate-sesstype-0.2.0 (c (n "sesstype") (v "0.2.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0b3w2gzmdaqrqk6px3jma1kyb4izlvw4ksvxfb25in6s2dhba8ph")))

(define-public crate-sesstype-0.2.1 (c (n "sesstype") (v "0.2.1") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1dxhdbd7f5ys59nfk2svy67fzh39kasw70g8wrcmrmcrk14pqwp9")))

(define-public crate-sesstype-0.2.2 (c (n "sesstype") (v "0.2.2") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0i44b69z76y19bv5lwbqifxrzqfjkr3v0mjvvd38h7dfs6qwc86l")))

(define-public crate-sesstype-0.2.3 (c (n "sesstype") (v "0.2.3") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "155j30k0q5kmdk0faxc51daa88m7a96ahf8j22zr4dwbfxpq3avy")))

(define-public crate-sesstype-0.2.4 (c (n "sesstype") (v "0.2.4") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "092r2ralpbh0kmmgd7w76hsl64qrnp9m7zvhqr68rzbbs6zmk23s")))

