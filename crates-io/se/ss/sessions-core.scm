(define-module (crates-io se ss sessions-core) #:use-module (crates-io))

(define-public crate-sessions-core-0.1.0 (c (n "sessions-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pd9vc3d6cwb6lf1c0f4rcm3xj7c5l9kp5j8d9nsgbx0kb77rivn")))

(define-public crate-sessions-core-0.1.1 (c (n "sessions-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cxvgcn6mqxm7hbif5fj2hj3r4cix5n31x8c0d7c0pa7gprill1a")))

(define-public crate-sessions-core-0.1.2 (c (n "sessions-core") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bq6n155av9n8dd06rxpah8ilzfgv3qm1vzpfchanml6m60i4zil")))

(define-public crate-sessions-core-0.1.3 (c (n "sessions-core") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fxg9i27qn7xhicwrl339im8rh0h7dgyv8qigdvwa6id5znrlhxg")))

(define-public crate-sessions-core-0.1.4 (c (n "sessions-core") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "031iw4kyhc72yjfyrp4cxfalp5sspbf6mjfxx018qchz2g0ms4r1")))

(define-public crate-sessions-core-0.1.5 (c (n "sessions-core") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k5lpp4x4931789vsawavij478b935ajrr1yim90d5y1fyi0wrjm")))

(define-public crate-sessions-core-0.1.6 (c (n "sessions-core") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a6akc8rkyrbkr23m4w434vlyaih1zgvfjfam3iz1v1i5l8a4jg2")))

(define-public crate-sessions-core-0.1.7 (c (n "sessions-core") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hw4lc40ad1qzczfvp855nlzbl7c38xcfhakr5j2864l5slafqqr")))

(define-public crate-sessions-core-0.1.8 (c (n "sessions-core") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l41h64p8i9pnhh5r8r0ln9rlk2rwv9wvy2796jchmsjmlr0804c")))

(define-public crate-sessions-core-0.1.9 (c (n "sessions-core") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hbb6xw6h4i6by3v7aj8dzl2m6xx8xix7ibqvc6fcd0l954vlplm")))

(define-public crate-sessions-core-0.2.0 (c (n "sessions-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1278rfbpgnsjn325kyk1nzc5chh8s3sirpwp1g5mgnpijgva7y26")))

(define-public crate-sessions-core-0.2.1 (c (n "sessions-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18v6r9d59adgsh2wyas5fhi4hapbj47wdr5rw8cj9qaav2s0npcd")))

(define-public crate-sessions-core-0.2.2 (c (n "sessions-core") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f7nz9qvsbi9c537f3grvfagrx6dn2lqszv55ca4ybbzmkilymlm")))

(define-public crate-sessions-core-0.2.3 (c (n "sessions-core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l74vlq3p9khkjpkg018mc2hr4v5llyh8qihbd7ghm7vli5x8dig")))

(define-public crate-sessions-core-0.3.0 (c (n "sessions-core") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13dpb5lnrnaqcvby2ldrinw1ki7znzkpqfq6rpgflhr2zpcd1pwd")))

(define-public crate-sessions-core-0.3.1 (c (n "sessions-core") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hzvqra14nj44jdjjksqbx7zvcmhh79vyv9qswpfryih4lbz836l")))

(define-public crate-sessions-core-0.3.2 (c (n "sessions-core") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qb6znb4g4iwsmh79dsp97d5rlcn95lyxl68a6fqzgdq44vipm8w")))

(define-public crate-sessions-core-0.3.3 (c (n "sessions-core") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s8wi83q766jxxv9dvqjg7yh28f5kfcfw2blpzrm5q9yjb6xddh6") (f (quote (("session") ("default" "session"))))))

(define-public crate-sessions-core-0.3.4 (c (n "sessions-core") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lqivbamjrgqghb3l9z3pq7wwxf737bic68jis2hfhhc5cjq55yh") (f (quote (("session"))))))

(define-public crate-sessions-core-0.4.0 (c (n "sessions-core") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04pid7yz627h17w07aq4i9wb361adgakpyx4mhxl01w911kxjm7i") (f (quote (("session"))))))

(define-public crate-sessions-core-0.5.0 (c (n "sessions-core") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0plgs2mmicqhhygd6x8llxl3wzivhpkxz5i48wbsqf3sy5a6h89i") (f (quote (("session"))))))

(define-public crate-sessions-core-0.5.1 (c (n "sessions-core") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17x7byr8p318m3ihx10zfm3jzb1r7amm008hzra45r0j8rnkyyp8") (f (quote (("session"))))))

(define-public crate-sessions-core-0.6.0 (c (n "sessions-core") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vv3n6dvvsiyl6rn0y9sl5d7jg3is1ws1zmr5kfsm4sqjj76a57p") (f (quote (("session"))))))

