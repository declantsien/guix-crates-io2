(define-module (crates-io se ss sessionless) #:use-module (crates-io))

(define-public crate-sessionless-0.1.0 (c (n "sessionless") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1ljb2k7wa6f5zawms8h2f2v47aaafpv07q33c0ch0k7pl845dypi") (f (quote (("default"))))))

(define-public crate-sessionless-0.1.1 (c (n "sessionless") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "0lgcw2y0xa6iygy5cvdqk280ism3p75frxssah0gc7pp4c40s6yp") (f (quote (("default"))))))

