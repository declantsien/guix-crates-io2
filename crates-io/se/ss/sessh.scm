(define-module (crates-io se ss sessh) #:use-module (crates-io))

(define-public crate-sessh-0.1.0 (c (n "sessh") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.2") (d #t) (k 0)))) (h "0dvqf0f6zjyz85prf2qhyir62qlqrglbxg38rqi6m2cl5hk8gqzw") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

(define-public crate-sessh-0.1.1 (c (n "sessh") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)) (d (n "ssh2") (r "^0.3.2") (d #t) (k 0)))) (h "12qih0iwlbnv8w1yzr2r495fxl5vdss1lyz7awspzdzzn0zrxnby") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

(define-public crate-sessh-0.1.2 (c (n "sessh") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)) (d (n "ssh2") (r "^0.4") (d #t) (k 0)))) (h "11s7mp1m7hbn1z0nmidsf95scqkz58z6zxz2hqmx15mx0cgrv231") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

(define-public crate-sessh-0.1.3 (c (n "sessh") (v "0.1.3") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)) (d (n "ssh2") (r "^0.4") (d #t) (k 0)))) (h "1nswyf53q3m9sd0m55gklhdhs4g78z0v5pklm39ksnfs8snbfbmr") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

(define-public crate-sessh-0.1.4 (c (n "sessh") (v "0.1.4") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)) (d (n "ssh2") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1hy17cimc1w4blyaidjzsrkmbbx4hyys7ncfpalz568jlhnvz358") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

(define-public crate-sessh-0.2.0 (c (n "sessh") (v "0.2.0") (d (list (d (n "educe") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 2)) (d (n "ssh2") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0qafdh0d0h4p2z1cqi2x3w26rnn2nzk3a8lgj2ck6rngh8k8qh0y") (f (quote (("debug" "slog/max_level_trace" "slog/release_max_level_debug"))))))

