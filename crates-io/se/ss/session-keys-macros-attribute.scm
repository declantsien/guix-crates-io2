(define-module (crates-io se ss session-keys-macros-attribute) #:use-module (crates-io))

(define-public crate-session-keys-macros-attribute-0.1.0 (c (n "session-keys-macros-attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "04ax6w6na6xvg6s01lbhw89n0bvmbd5kfqhd8jjqhlxw2hhi4zg0")))

