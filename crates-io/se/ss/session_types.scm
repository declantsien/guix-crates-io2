(define-module (crates-io se ss session_types) #:use-module (crates-io))

(define-public crate-session_types-0.0.1 (c (n "session_types") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rand_macros") (r "*") (d #t) (k 2)))) (h "0x43p4jvfli5d221i5srpkzlc6wlpb3gwwbw9aqm7k6kgbcmclzf")))

(define-public crate-session_types-0.1.0 (c (n "session_types") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rand_macros") (r "*") (d #t) (k 2)))) (h "1s5759k2y5jbphwlnr197vy4yzd7plih735irzdi6n011577pxng")))

(define-public crate-session_types-0.1.1 (c (n "session_types") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rand_macros") (r "*") (d #t) (k 2)))) (h "1yki2s9rrx3w9nlldr4hsyhagi3df97m80925p6awj8nzrw5cr4s")))

(define-public crate-session_types-0.2.0 (c (n "session_types") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "085h2v9bj0driavd9q3hnc8plg59n29rbzx14qdk69fzxjwjvl55") (f (quote (("default") ("chan_select" "compiletest_rs"))))))

(define-public crate-session_types-0.3.0 (c (n "session_types") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.3.16") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "09849a6j3x5a9778fx4hx544dd83k7pq3magb6gi54dcwqlzs3ff")))

(define-public crate-session_types-0.3.1 (c (n "session_types") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.3.16") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1b6pm8ivwdaf69cfsqsnp4ia60rwk2x7gj4w3qskzdjrkwc4m7hl")))

