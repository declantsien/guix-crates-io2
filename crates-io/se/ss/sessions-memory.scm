(define-module (crates-io se ss sessions-memory) #:use-module (crates-io))

(define-public crate-sessions-memory-0.1.0 (c (n "sessions-memory") (v "0.1.0") (d (list (d (n "sessions-core") (r "^0.1.0") (d #t) (k 0)))) (h "0hf3x9zl6xd1jfvgkwracnhdg10r82432122lc8jn85iywdkx4zn")))

(define-public crate-sessions-memory-0.1.1 (c (n "sessions-memory") (v "0.1.1") (d (list (d (n "sessions-core") (r "^0.1.1") (d #t) (k 0)))) (h "0x00f1dnfw8jsspj3f7qzvp97cpp2yw0563avbpjxhi27rw7s1sw")))

(define-public crate-sessions-memory-0.1.2 (c (n "sessions-memory") (v "0.1.2") (d (list (d (n "sessions-core") (r "^0.1.2") (d #t) (k 0)))) (h "19ap4mslx41i9x732iaym3gg08vv3f8gn2d5pmn51zrfl89k5f0s")))

(define-public crate-sessions-memory-0.1.3 (c (n "sessions-memory") (v "0.1.3") (d (list (d (n "sessions-core") (r "^0.1.3") (d #t) (k 0)))) (h "0n2883f01w38v9xgvxzdj7i1dh53r7kvv08v6rw0hlwxkaw5rk3x")))

(define-public crate-sessions-memory-0.1.4 (c (n "sessions-memory") (v "0.1.4") (d (list (d (n "sessions-core") (r "^0.1.4") (d #t) (k 0)))) (h "1j21r3jvs0v1nzqbn1jfb0pp5shkqppr5j3cg1z60x3bxlcra72a")))

(define-public crate-sessions-memory-0.1.5 (c (n "sessions-memory") (v "0.1.5") (d (list (d (n "sessions-core") (r "^0.1.5") (d #t) (k 0)))) (h "0gyf2hmlzdb1sbmqg8bdj5iwja2ffm77r15dw8lznifw325dm7bk")))

(define-public crate-sessions-memory-0.1.6 (c (n "sessions-memory") (v "0.1.6") (d (list (d (n "sessions-core") (r "^0.1.6") (d #t) (k 0)))) (h "09agdvv63qgyq3dsmidb2hv1133rm20v4xz3rml497b2y73y5l3h")))

(define-public crate-sessions-memory-0.1.7 (c (n "sessions-memory") (v "0.1.7") (d (list (d (n "sessions-core") (r "^0.1.7") (d #t) (k 0)))) (h "1j0fyxbjh2ilv6q3sgrqk6d8i5shd1vx1q2sxqqpmfc1qkd3krz2")))

(define-public crate-sessions-memory-0.1.8 (c (n "sessions-memory") (v "0.1.8") (d (list (d (n "sessions-core") (r "^0.1.8") (d #t) (k 0)))) (h "0yskgjr04vx4cclimh90hc5izn7p20p9c9gbfazw66rlmszrxy56")))

(define-public crate-sessions-memory-0.1.9 (c (n "sessions-memory") (v "0.1.9") (d (list (d (n "sessions-core") (r "^0.1.9") (d #t) (k 0)))) (h "1cbga40cqwnr1hjp74fdhfgrqf3pzbavnah4y65zg0dyb4r0rlkf")))

(define-public crate-sessions-memory-0.2.0 (c (n "sessions-memory") (v "0.2.0") (d (list (d (n "sessions-core") (r "^0.2.0") (d #t) (k 0)))) (h "0ik1q2ga1rca8lz0182j5i0csiw0rs9jkp3bc9ja4z4rnvdw304w")))

(define-public crate-sessions-memory-0.2.1 (c (n "sessions-memory") (v "0.2.1") (d (list (d (n "sessions-core") (r "^0.2.1") (d #t) (k 0)))) (h "097i3q5zd74zx5191k8wiraj5ak0gdqv328bs0n40hic6zvnxns9")))

(define-public crate-sessions-memory-0.2.2 (c (n "sessions-memory") (v "0.2.2") (d (list (d (n "sessions-core") (r "^0.2.2") (d #t) (k 0)))) (h "0qr6m0rf26imqv4qbx637ja26wf0gvphv7p525h2kr06jk32xp9f")))

(define-public crate-sessions-memory-0.2.3 (c (n "sessions-memory") (v "0.2.3") (d (list (d (n "sessions-core") (r "^0.2.3") (d #t) (k 0)))) (h "101swsh29l4vpykyhijqpfvx5j07sh62qfdsmmmksggd7hm9hj8j")))

(define-public crate-sessions-memory-0.3.0 (c (n "sessions-memory") (v "0.3.0") (d (list (d (n "sessions-core") (r "^0.3.0") (d #t) (k 0)))) (h "1n6s3jnqkncbl77wvpdfkawj7rz0ki9jf5r12x51b5n21pz0nm9a")))

(define-public crate-sessions-memory-0.3.1 (c (n "sessions-memory") (v "0.3.1") (d (list (d (n "sessions-core") (r "^0.3.0") (d #t) (k 0)))) (h "06gschcp3iazd3hkgfyysk0c2jq9671jak68ixcasjy25nv6rz77")))

(define-public crate-sessions-memory-0.3.2 (c (n "sessions-memory") (v "0.3.2") (d (list (d (n "sessions-core") (r "^0.3.2") (d #t) (k 0)))) (h "1r0gx1s36d2dzw0190651fnf484bp2ip6pwla3ki6rypr4997hza")))

(define-public crate-sessions-memory-0.3.3 (c (n "sessions-memory") (v "0.3.3") (d (list (d (n "sessions-core") (r "^0.3.2") (d #t) (k 0)))) (h "0f8wslry55al7df6phjry6x8miksy2ry7sldlngsjmmhfd1x2mk6")))

(define-public crate-sessions-memory-0.3.4 (c (n "sessions-memory") (v "0.3.4") (d (list (d (n "sessions-core") (r "^0.3.4") (d #t) (k 0)))) (h "1axs4zr7dw66sv3wwb6mm8qbvs1klbq5dd9fnl2zjqr1an1yfvcw")))

(define-public crate-sessions-memory-0.4.0 (c (n "sessions-memory") (v "0.4.0") (d (list (d (n "sessions-core") (r "^0.4.0") (d #t) (k 0)))) (h "08vpfh7iyx585j6zx9shjw7za60n6ia4r9bna0d13jam6l9nwkc5")))

(define-public crate-sessions-memory-0.5.0 (c (n "sessions-memory") (v "0.5.0") (d (list (d (n "sessions-core") (r "^0.5.0") (d #t) (k 0)))) (h "0k061fi64ymqvzgd5xak16qar7q68kvm1gjq62pwnxhmrvfdw5p4")))

(define-public crate-sessions-memory-0.5.1 (c (n "sessions-memory") (v "0.5.1") (d (list (d (n "sessions-core") (r "^0.5.0") (d #t) (k 0)))) (h "1nmppl2hj8x2n3m1hlirrkjjh4br92csmv9ig0g5198fnyr8zday")))

(define-public crate-sessions-memory-0.6.0 (c (n "sessions-memory") (v "0.6.0") (d (list (d (n "sessions-core") (r "^0.6.0") (d #t) (k 0)))) (h "01p94lqjsqljpd9bsb11i8prh6x33156cznxnsjvi32vb9fpwx42")))

