(define-module (crates-io se e- see-through) #:use-module (crates-io))

(define-public crate-see-through-0.0.1 (c (n "see-through") (v "0.0.1") (d (list (d (n "see_derive") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0hq82p9ciwym3h1r1bipx7a9fp3mq9jw8afs62pafm0zy47mz06f") (f (quote (("default")))) (s 2) (e (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.2 (c (n "see-through") (v "0.0.2") (d (list (d (n "see_derive") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1hma0xfkl7mqbzsc91rl9n6k2ppkzhkn352z9vln7gcmwd10clmd") (f (quote (("default")))) (s 2) (e (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.3 (c (n "see-through") (v "0.0.3") (d (list (d (n "see_derive") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "1xiybf6zhmr6hnax2lnn3662ksh3pqdzk1yw13j4hsw7n8q3hzki") (f (quote (("default" "macros")))) (s 2) (e (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.4 (c (n "see-through") (v "0.0.4") (d (list (d (n "see_derive") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "0r9yzrdld4ibmgb26rjs1v8pavxk2y608k1sx0pvy8266r6rqqxg") (f (quote (("default" "macros")))) (s 2) (e (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.5 (c (n "see-through") (v "0.0.5") (d (list (d (n "see_derive") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "1h6hw0452m7imhm5n8jv5ywc35y1zjd3gk9kn053ahq2xcs883gb") (f (quote (("default" "macros")))) (s 2) (e (quote (("macros" "dep:see_derive"))))))

