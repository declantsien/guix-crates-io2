(define-module (crates-io se e- see-you-later) #:use-module (crates-io))

(define-public crate-see-you-later-0.1.0 (c (n "see-you-later") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 2)) (d (n "smol-potat") (r "^0.2") (d #t) (k 2)) (d (n "wait-for-me") (r "^0.1") (d #t) (k 2)))) (h "0vnyypfvwnrq5zq2hzl3fr54nsi5p01h9x3dcvkqhg2nfgrx13if")))

