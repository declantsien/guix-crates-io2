(define-module (crates-io se ru serum-dex-permissioned) #:use-module (crates-io))

(define-public crate-serum-dex-permissioned-0.4.6 (c (n "serum-dex-permissioned") (v "0.4.6") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.0") (d #t) (k 0)) (d (n "serum") (r "^0.4.0") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0dp5vfayjr5fd9sfpx7ylydpizj03sy5px20xyz8w34ba56khl3g")))

(define-public crate-serum-dex-permissioned-0.4.7 (c (n "serum-dex-permissioned") (v "0.4.7") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.0") (d #t) (k 0)) (d (n "serum") (r "^0.4.0") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "19jmlzzm9jlc9s1viw8nac6pirc2pifz6vdc7fldr8i1xbs19x39")))

