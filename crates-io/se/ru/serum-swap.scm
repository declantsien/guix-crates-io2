(define-module (crates-io se ru serum-swap) #:use-module (crates-io))

(define-public crate-serum-swap-0.3.0-beta.1 (c (n "serum-swap") (v "0.3.0-beta.1") (d (list (d (n "anchor-lang") (r "^0.11.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.11.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.4") (d #t) (k 0)))) (h "04y1jilh1j8f06mlwjw78ylw5hscjdw22qgck07zlb989vxzn0w9") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-serum-swap-0.3.0-beta.2 (c (n "serum-swap") (v "0.3.0-beta.2") (d (list (d (n "anchor-lang") (r "^0.11.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.11.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.4") (d #t) (k 0)))) (h "0xb8rjpasaxbbp80giam7jg7bpr2rzixi9yx4486kf3kabx4xv07") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-serum-swap-0.4.0 (c (n "serum-swap") (v "0.4.0") (d (list (d (n "anchor-lang") (r "^0.13.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.13.0") (d #t) (k 0)) (d (n "solana-program") (r "=1.7.8") (d #t) (k 0)))) (h "046rl9p0k5da4lslrfxdap4vrd064psn4wz0dbsm3q01jxlimrw6") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

