(define-module (crates-io se ru serust) #:use-module (crates-io))

(define-public crate-serust-0.1.0 (c (n "serust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serialport5") (r "^5.0.0") (d #t) (k 0)))) (h "0d3wmhni9xkr81ln45ag5d8xibapaq3h9aqccjfwi4mpgdlvv8dv")))

