(define-module (crates-io se ru serum-borsh-derive) #:use-module (crates-io))

(define-public crate-serum-borsh-derive-0.7.2+serum.1 (c (n "serum-borsh-derive") (v "0.7.2+serum.1") (d (list (d (n "serum-borsh-derive-internal") (r "^0.7.2") (d #t) (k 0)) (d (n "serum-borsh-schema-derive-internal") (r "^0.7.2") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1vv5snjgmyyj97bmk8a8zk5bzc65dj5xgdsmrimlcw2h1kmdx4c1")))

