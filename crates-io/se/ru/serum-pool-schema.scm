(define-module (crates-io se ru serum-pool-schema) #:use-module (crates-io))

(define-public crate-serum-pool-schema-0.4.7 (c (n "serum-pool-schema") (v "0.4.7") (d (list (d (n "serum-borsh") (r "^0.8.1-serum.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.4.14") (d #t) (k 0)))) (h "1qcpc3il7aw932zaffh19v7nnifa884aih21qdd6ah6rvyd6350v") (f (quote (("program") ("default"))))))

