(define-module (crates-io se ru serum-borsh) #:use-module (crates-io))

(define-public crate-serum-borsh-0.7.2+serum.1 (c (n "serum-borsh") (v "0.7.2+serum.1") (d (list (d (n "serum-borsh-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (o #t) (k 0)))) (h "17mghcs42ziv4km7xk588v8wx0whvyl2v83kcf1k9z1jnq13fbja") (f (quote (("std") ("serum-program" "solana-sdk/program") ("serum-client" "solana-sdk/default") ("default" "std")))) (y #t)))

(define-public crate-serum-borsh-0.7.2+serum.2 (c (n "serum-borsh") (v "0.7.2+serum.2") (d (list (d (n "serum-borsh-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (o #t) (k 0)))) (h "0wj1y3dg9s1yfgs9q2dr27ynmrwf2453xj7xfjcanh6sn12gmj4m") (f (quote (("std") ("serum-program" "solana-sdk/program") ("serum-client" "solana-sdk/default") ("default" "std")))) (y #t)))

(define-public crate-serum-borsh-0.8.0-serum.1 (c (n "serum-borsh") (v "0.8.0-serum.1") (d (list (d (n "serum-borsh-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.3.14") (o #t) (k 0)))) (h "0s0fzygkib20a2k9hyhqzwpw5mdn2dhw8j8wmrz771si324k2j9g") (f (quote (("std") ("serum-program" "solana-sdk/program") ("serum-client" "solana-sdk/default") ("default" "std"))))))

(define-public crate-serum-borsh-0.8.1-serum.1 (c (n "serum-borsh") (v "0.8.1-serum.1") (d (list (d (n "borsh-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.5.0") (d #t) (k 0)))) (h "14n8637i0pvb06mf0hca84bcybgyvgzm4vhgi7f1agxj07vd7zm4") (f (quote (("std") ("default" "std"))))))

