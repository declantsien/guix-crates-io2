(define-module (crates-io se ru serum-borsh-schema-derive-internal) #:use-module (crates-io))

(define-public crate-serum-borsh-schema-derive-internal-0.7.2+serum.1 (c (n "serum-borsh-schema-derive-internal") (v "0.7.2+serum.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ywwfglb3m9nxx25fs8kq87i2j2k0ayb8nif31h9w9kal1br5sjv")))

