(define-module (crates-io se ez seezee) #:use-module (crates-io))

(define-public crate-seezee-0.1.0 (c (n "seezee") (v "0.1.0") (d (list (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "watto") (r "^0.1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (f (quote ("experimental"))) (d #t) (k 0)))) (h "1psmpfygfjg2wj9hcqr595kxd0mdxcvnnni18wfhcbyr8gjkpk69")))

