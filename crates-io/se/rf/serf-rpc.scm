(define-module (crates-io se rf serf-rpc) #:use-module (crates-io))

(define-public crate-serf-rpc-0.1.0 (c (n "serf-rpc") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "rmp") (r "^0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (f (quote ("serde128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wy94apmipqkh3951v1qdn6mi39v68j5wipmwpqnf1pywwpmf1ar")))

(define-public crate-serf-rpc-0.1.1 (c (n "serf-rpc") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "rmp") (r "^0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (f (quote ("serde128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1iqwfvxcf01vbibqcw88s19lsi7g5mb8k3p8rkyv914nggy7qny9")))

(define-public crate-serf-rpc-0.2.0 (c (n "serf-rpc") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "rmp") (r "^0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (f (quote ("serde128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1h2fyfyb9cfdzww0bawn924pxf8x5l8ivzaprmpybkdfdmna7hsa")))

