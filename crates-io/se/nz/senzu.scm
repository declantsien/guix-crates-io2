(define-module (crates-io se nz senzu) #:use-module (crates-io))

(define-public crate-senzu-0.1.0 (c (n "senzu") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "xyzpub") (r "^0.2.1") (d #t) (k 0)))) (h "0b6ka47bcs8mx4yxxp6ipi6z7jwx9d67qkah0szxh44f58jajzmy")))

