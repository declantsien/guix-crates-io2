(define-module (crates-io se ou seoul-derive) #:use-module (crates-io))

(define-public crate-seoul-derive-0.1.0 (c (n "seoul-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0zm8vz957h9iqz82y2azp6gjndyngz0nplgnyscrqsg1v5k62wgs")))

