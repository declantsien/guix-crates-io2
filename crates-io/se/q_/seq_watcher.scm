(define-module (crates-io se q_ seq_watcher) #:use-module (crates-io))

(define-public crate-seq_watcher-0.1.0 (c (n "seq_watcher") (v "0.1.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0c04af5li2kry0sspv5z5ykkdbxvvspafn08zwzp2567dydzsgsc")))

(define-public crate-seq_watcher-0.1.1 (c (n "seq_watcher") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0h839rz9xz4vvyiir9kw4rm04xipy30b9bm41y03h2dwg3qdias7")))

