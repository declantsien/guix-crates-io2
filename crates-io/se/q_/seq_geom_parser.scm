(define-module (crates-io se q_ seq_geom_parser) #:use-module (crates-io))

(define-public crate-seq_geom_parser-0.1.1 (c (n "seq_geom_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)))) (h "18ahdh3mbgixq3dkw30chb0bngxls3gnbgh9hvqplny31s0d32mn")))

(define-public crate-seq_geom_parser-0.1.2 (c (n "seq_geom_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "1psybs9gapwdwr4h7a3yglf91n23lj0xv2y9845mn103hac15a18")))

(define-public crate-seq_geom_parser-0.1.3 (c (n "seq_geom_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "1hl5ji78f5n0vn2sszh24whcz5imv3ys6108pk26yrsf6838hqsh")))

(define-public crate-seq_geom_parser-0.2.0 (c (n "seq_geom_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "1f9wrqhz6v060r34i7fh5abbzlhhpq9hi2sd5lzvswqslicphkkj")))

(define-public crate-seq_geom_parser-0.2.1 (c (n "seq_geom_parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "1piwam0la795i2szzlf1c24n7xz1m36149wg6zn03wkq1n354rmb")))

(define-public crate-seq_geom_parser-0.3.0 (c (n "seq_geom_parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)))) (h "0x53l60mykrg54x5hmcw04nidmng5zzgsrlwpmsc8n8n1g2mrr13")))

