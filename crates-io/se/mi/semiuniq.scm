(define-module (crates-io se mi semiuniq) #:use-module (crates-io))

(define-public crate-semiuniq-0.1.0 (c (n "semiuniq") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lru") (r "^0.6.1") (d #t) (k 0)))) (h "1x17fhvpwiqz50gicxsrxrfdw79fkrddbfi041ap3jsidfmk9smv")))

(define-public crate-semiuniq-0.1.5 (c (n "semiuniq") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lru") (r "^0.6.1") (d #t) (k 0)))) (h "0lslrfcl47zxghg4phfxcpb27hyw6gd7hnwji5b1kynkbfknsqiy")))

(define-public crate-semiuniq-0.1.6 (c (n "semiuniq") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lru") (r "^0.6.1") (d #t) (k 0)))) (h "0rqn592401j80ksgz8h26pfj61zxydz611p0d037jp8cd1x8phnr")))

