(define-module (crates-io se mi seminix-std) #:use-module (crates-io))

(define-public crate-seminix-std-0.1.0 (c (n "seminix-std") (v "0.1.0") (h "0w87ayd1b8v34ynibir6dig80zbvjdy4kira9xa3bj6n09f7xvi2") (r "1.78")))

(define-public crate-seminix-std-0.1.1 (c (n "seminix-std") (v "0.1.1") (h "0iv62js6a778cvwh42rsiqpvgijc06gmwbahh39x4062m2kii5w1") (r "1.78")))

