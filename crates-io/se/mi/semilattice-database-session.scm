(define-module (crates-io se mi semilattice-database-session) #:use-module (crates-io))

(define-public crate-semilattice-database-session-0.1.0 (c (n "semilattice-database-session") (v "0.1.0") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ga7zbgh5lsqhnhdvpd96r0fi8y8rixyifj16f4in851wd1sn5mx") (y #t)))

(define-public crate-semilattice-database-session-0.1.1 (c (n "semilattice-database-session") (v "0.1.1") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1x6imkfnssj80flp0qhqb3686p9jppp8f6flc0ipm52cli359wql") (y #t)))

(define-public crate-semilattice-database-session-0.1.2 (c (n "semilattice-database-session") (v "0.1.2") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1bl4686nz76fjh4vb3x67xwxc0lgs63sf80scyml9ixlb23arsdp") (y #t)))

(define-public crate-semilattice-database-session-0.1.3 (c (n "semilattice-database-session") (v "0.1.3") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0hdhh2nx2rxxsp760r3m7ndmcbsbx6a59zmimpbnsfv2lilfnnnc") (y #t)))

(define-public crate-semilattice-database-session-0.1.4 (c (n "semilattice-database-session") (v "0.1.4") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0k7xkj5gz4a4nlr2yc9s2nsydiin7zhkysj0jai6jnxfbf6f3v4q") (y #t)))

(define-public crate-semilattice-database-session-0.1.5 (c (n "semilattice-database-session") (v "0.1.5") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0h1f98fqgmp7l8g8ph15vxb4glh5i078b1l9d43hwj84dcfz26dc") (y #t)))

(define-public crate-semilattice-database-session-0.2.0 (c (n "semilattice-database-session") (v "0.2.0") (d (list (d (n "semilattice-database") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0pypkl5vrbyqiy5hxchmxmni8d3ppb5c7kpvbl0a0prgbzcg13da") (y #t)))

(define-public crate-semilattice-database-session-0.2.1 (c (n "semilattice-database-session") (v "0.2.1") (d (list (d (n "semilattice-database") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "03w9kp287xn9a4h2245jq8zzrmk5z45s2s7kw6ffl8v1zw553lza") (y #t)))

(define-public crate-semilattice-database-session-0.2.2 (c (n "semilattice-database-session") (v "0.2.2") (d (list (d (n "semilattice-database") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "11n2knbhqdi5vlik5q8c3pxqh3slb830sfdsjgpjdix6ikal72bc") (y #t)))

(define-public crate-semilattice-database-session-0.3.0 (c (n "semilattice-database-session") (v "0.3.0") (d (list (d (n "semilattice-database") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1ilg5k0gvj4qk8gr6bl4npxqjc36iw0gwk815165x939l5mwq1gf") (y #t)))

(define-public crate-semilattice-database-session-0.4.0 (c (n "semilattice-database-session") (v "0.4.0") (d (list (d (n "semilattice-database") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zx4b3k1gc2j680hmzn2kw4m00z3d3kw8nz8mgvf4q4cmrdaxdcq") (y #t)))

(define-public crate-semilattice-database-session-0.5.0 (c (n "semilattice-database-session") (v "0.5.0") (d (list (d (n "semilattice-database") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1c56qq3kkfjgxn94kxgnzcqnsfbx3idrm6yii54amsy87caqnrya") (y #t)))

(define-public crate-semilattice-database-session-0.6.0 (c (n "semilattice-database-session") (v "0.6.0") (d (list (d (n "semilattice-database") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0nkm9w9vzgrv34fcbr49zlimdl1j9yqj55dghwwalally4j5n4yw") (y #t)))

(define-public crate-semilattice-database-session-0.7.0 (c (n "semilattice-database-session") (v "0.7.0") (d (list (d (n "semilattice-database") (r "^0.64") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "139wdjkk2ql8k9vp0al260nybwh88kygvgdbd4h860s956qg3kvv") (y #t)))

(define-public crate-semilattice-database-session-0.8.0 (c (n "semilattice-database-session") (v "0.8.0") (d (list (d (n "semilattice-database") (r "^0.65") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1i282v31h99rfxadrdip0gv1cbx4831cc2qpjiigr2s4wlpjy5b2") (y #t)))

(define-public crate-semilattice-database-session-0.8.1 (c (n "semilattice-database-session") (v "0.8.1") (d (list (d (n "semilattice-database") (r "^0.65") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0wj581f7zjca19iiz40wi7i768xw4r63da9kjwqmpcx9b0xb72pz") (y #t)))

(define-public crate-semilattice-database-session-0.9.0 (c (n "semilattice-database-session") (v "0.9.0") (d (list (d (n "semilattice-database") (r "^0.66") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1nwh0w0g6a9jbnpgbvn1s2nvqk86bzhwrzcwwsgk195cy127q301") (y #t)))

(define-public crate-semilattice-database-session-0.10.0 (c (n "semilattice-database-session") (v "0.10.0") (d (list (d (n "semilattice-database") (r "^0.67") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0yh7hyrn1w464a5s7wbig51vkc3zxi3nshjnl69xngpnzqf4ld2y") (y #t)))

(define-public crate-semilattice-database-session-0.10.1 (c (n "semilattice-database-session") (v "0.10.1") (d (list (d (n "semilattice-database") (r "^0.68") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1dhvzj8aii65m6jkivyhj6w32sm7cps1cc168gwnnxa8w987pi5y") (y #t)))

(define-public crate-semilattice-database-session-0.10.2 (c (n "semilattice-database-session") (v "0.10.2") (d (list (d (n "semilattice-database") (r "^0.68") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "085qxw1xvv23psz9yiczs1lj4s1k9rlj894g79r4jjbck0vsmj3n") (y #t)))

(define-public crate-semilattice-database-session-0.11.0 (c (n "semilattice-database-session") (v "0.11.0") (d (list (d (n "semilattice-database") (r "^0.69") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0l0ljda3m5fdyjwsiry0m9k1h92xzi9bydypjwrp1zb9wir7xykn") (y #t)))

(define-public crate-semilattice-database-session-0.12.0 (c (n "semilattice-database-session") (v "0.12.0") (d (list (d (n "semilattice-database") (r "^0.70") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "00kl7qrzz21l88wrcxm6lgbxr2fij990qq2bkvfyykny0imb0p4y") (y #t)))

(define-public crate-semilattice-database-session-0.13.0 (c (n "semilattice-database-session") (v "0.13.0") (d (list (d (n "semilattice-database") (r "^0.71") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1nkrdkcc69ll54aqs8yj81cpji18zd93xi07lmwabzwvj55bsv48") (y #t)))

(define-public crate-semilattice-database-session-0.14.0 (c (n "semilattice-database-session") (v "0.14.0") (d (list (d (n "semilattice-database") (r "^0.72") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ih28qxy19zhgg087yvrqqm8j363rsfgjwxi52cxdc4n191c6gy9") (y #t)))

(define-public crate-semilattice-database-session-0.15.0 (c (n "semilattice-database-session") (v "0.15.0") (d (list (d (n "semilattice-database") (r "^0.73") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1mr6vh02nmrqzx75ca65kkwrz1acjfz8j436s7y4k6v8m46jxhs4") (y #t)))

(define-public crate-semilattice-database-session-0.16.0 (c (n "semilattice-database-session") (v "0.16.0") (d (list (d (n "semilattice-database") (r "^0.74") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1b7d7hfp831qsbm1xcjlqagxf9q2i2x3wa3dw8y9g6x2px2z4zhz") (y #t)))

(define-public crate-semilattice-database-session-0.17.0 (c (n "semilattice-database-session") (v "0.17.0") (d (list (d (n "semilattice-database") (r "^0.75") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0zbdnlx1jzb7js5n4rhd9s0pdlaa0czpjymgmvldz2qbj5vjlbnm")))

(define-public crate-semilattice-database-session-0.18.0 (c (n "semilattice-database-session") (v "0.18.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.76") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "01abz3clnr53wwjcxyhj25avgn9vfhxz7w0r5yjx4ks3mzfzqnfh")))

(define-public crate-semilattice-database-session-0.18.1 (c (n "semilattice-database-session") (v "0.18.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.76") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "185wnbpigawa80rbv7q8rhc9wg3inam2xpaznxciz3gckh0afhy1")))

(define-public crate-semilattice-database-session-0.18.2 (c (n "semilattice-database-session") (v "0.18.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.77") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1rfg736xydyjmaj0bqq9j8aqc6zf5fapi2vd2gv614rb1vxvicrk")))

(define-public crate-semilattice-database-session-0.18.3 (c (n "semilattice-database-session") (v "0.18.3") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.77") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1wyqkjxkxah6i0wby611gd5ccaqxkgv211qxmp4sr0lhr68zq4qc")))

(define-public crate-semilattice-database-session-0.18.4 (c (n "semilattice-database-session") (v "0.18.4") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.77") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "11rgcwc1mpilpmnnkq4l7xf9ahvf6cs02rrld312jbg8vhvdi07i")))

(define-public crate-semilattice-database-session-0.18.5 (c (n "semilattice-database-session") (v "0.18.5") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.77") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "07nc8qn64ad5vy7mcza5fl96857x1a7c5r3g84rk7j0xsmsas8iw")))

(define-public crate-semilattice-database-session-0.19.0 (c (n "semilattice-database-session") (v "0.19.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.78") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1ynfg6wd9lidq0dlxjmz05q5k8lp93wr1akvp4mzxzkqch7666zj")))

(define-public crate-semilattice-database-session-0.19.1 (c (n "semilattice-database-session") (v "0.19.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.78") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1lzbrvvbp6k7dwgr1psp69ajlvc6mdhsyj9z8q24p26a6prpglnl")))

(define-public crate-semilattice-database-session-0.20.0 (c (n "semilattice-database-session") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.78") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1c18294aw6k28a8q3fbha0x5vs3grzr6c1h1279ipbakgvfzbiaa")))

(define-public crate-semilattice-database-session-0.21.0 (c (n "semilattice-database-session") (v "0.21.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.79") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "04vgjsv12lbsacmv6d8q9rg0d0qrj6lp9i97n011k7idsyjq3nr6")))

(define-public crate-semilattice-database-session-0.22.0 (c (n "semilattice-database-session") (v "0.22.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.79") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "027qi3fm8kxnpsbq3g066vbdczqnxvaanbif9ahsy1lwmrqkvfy1")))

(define-public crate-semilattice-database-session-0.22.1 (c (n "semilattice-database-session") (v "0.22.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.79") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "005qsw3labl14hfw5bzsk1wncwr79bjnvcklbh0r776wwkhi6r1x")))

(define-public crate-semilattice-database-session-0.23.0 (c (n "semilattice-database-session") (v "0.23.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.80") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0nl9c2q5gf6289zwgnw0l9zk5064a0m3m8f2qaq3ddawkjjgi7kb")))

(define-public crate-semilattice-database-session-0.24.0 (c (n "semilattice-database-session") (v "0.24.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.81") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ch7csxh4zkvpmx4s2n8cqxsm0warj2vpds92hwbajnhri1hqq68")))

(define-public crate-semilattice-database-session-0.25.0 (c (n "semilattice-database-session") (v "0.25.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.82") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1w7435ya5hi6skg2iqljzk68pw2fg6rd9ih3zihks3s8qm7wyk08")))

(define-public crate-semilattice-database-session-0.25.1 (c (n "semilattice-database-session") (v "0.25.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.83") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1vkf2d8b48fb8gb7mqswdgwbgvl4f0r14npr8605yszf165gsnsm")))

(define-public crate-semilattice-database-session-0.26.0 (c (n "semilattice-database-session") (v "0.26.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.84") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1jvn9ywb38aqh340v93cgrgiplri4jrr4fl1x83a5hmnwbgaacsa")))

(define-public crate-semilattice-database-session-0.27.0 (c (n "semilattice-database-session") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.85") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1xs133d4rj72rpmxij3c63y5ggcznihgysgjb0f1m0zvbmp06wm4")))

(define-public crate-semilattice-database-session-0.28.0 (c (n "semilattice-database-session") (v "0.28.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.86") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1bwfi5k8zv5rzfa4qsdc05k9qy28ajl2b58rbdxhc5qknrjrmfs0")))

(define-public crate-semilattice-database-session-0.29.0 (c (n "semilattice-database-session") (v "0.29.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.87") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0fdyl3ngkvxbz5bz2wjv9fbl2lrfkb92dzhskpz50qlil0g4q8f8")))

(define-public crate-semilattice-database-session-0.30.0 (c (n "semilattice-database-session") (v "0.30.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.88") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0i0f6999f65jb8j48sz8vxr1k9lnq3frkfsay26yla8y7csv9rd7")))

(define-public crate-semilattice-database-session-0.31.0 (c (n "semilattice-database-session") (v "0.31.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.89") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0z79clschjkrn4xqlf7dpl4s3a88vypxiz2sk20bbl12b0j0kbwj")))

(define-public crate-semilattice-database-session-0.31.1 (c (n "semilattice-database-session") (v "0.31.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.89") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "19wbagz7dh8r8wvmkclg0qp98y7g3r3mg88y6hvr2lsbahzz4xxz")))

(define-public crate-semilattice-database-session-0.32.0 (c (n "semilattice-database-session") (v "0.32.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.90") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "197vsf20a58ws8kkn0gvha2fi1m3z8kpliqprw06mq6rm3mwnavw") (y #t)))

(define-public crate-semilattice-database-session-0.33.0 (c (n "semilattice-database-session") (v "0.33.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.91") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1vdrgnm6g8xs6gc526r359qy24bxwqflk7lqnir546dps80rmkwr") (y #t)))

(define-public crate-semilattice-database-session-0.33.1 (c (n "semilattice-database-session") (v "0.33.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "05fnmix0myqwj5b92lgvhqwcyfxkyv0340r8nq2phi13m3ypnls3")))

(define-public crate-semilattice-database-session-0.33.2 (c (n "semilattice-database-session") (v "0.33.2") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1vblxi7qay0xlmiqsj8wbvchbf7hmbx0rf1d9fbi8jap3gs31z0h")))

(define-public crate-semilattice-database-session-0.34.0 (c (n "semilattice-database-session") (v "0.34.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0qkcbgjawfab7ibik91lj1jx9xip6x3h49zx0fx6y46w94vbc8ym")))

(define-public crate-semilattice-database-session-0.34.1 (c (n "semilattice-database-session") (v "0.34.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "189yyyqzsxb822fddbar8pcqxvca50b9dzw35s96mgwl182nixkd")))

(define-public crate-semilattice-database-session-0.34.2 (c (n "semilattice-database-session") (v "0.34.2") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0bj3mjs46bpr1sjfaqhnmnn8saihylbfvhf4pr66b6a721sagpf7")))

(define-public crate-semilattice-database-session-0.34.3 (c (n "semilattice-database-session") (v "0.34.3") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.92") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "11rlfgxkmgvm7c6624d0pwqk5jc0ds7w330jyrcffdflj6dhs7yx")))

(define-public crate-semilattice-database-session-0.35.0 (c (n "semilattice-database-session") (v "0.35.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.93") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1yrwx94m70hyyh56qir8ddcxhq55caiv4g873225313n3jzjy6hp")))

(define-public crate-semilattice-database-session-0.35.1 (c (n "semilattice-database-session") (v "0.35.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0mxqga2c4iwm45j2gx5nq6iqcxdprrvlqg5mjhr19r0iclpgpn7s")))

(define-public crate-semilattice-database-session-0.35.2 (c (n "semilattice-database-session") (v "0.35.2") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0xb4pz67kwsns0ymx6lqzyc24vg7ybsch1g4azfqfhmbfbvq06jf")))

(define-public crate-semilattice-database-session-0.36.0 (c (n "semilattice-database-session") (v "0.36.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "085gjkrlbfcwxmpn7d1zkhwyxq2066cd1vzpxdjybqbrlbnczqp4")))

(define-public crate-semilattice-database-session-0.37.0 (c (n "semilattice-database-session") (v "0.37.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1ynz5zs57g0xjr5vx17400bfdq3094g8cw12qjwgv239jhbjc6x8")))

(define-public crate-semilattice-database-session-0.38.0 (c (n "semilattice-database-session") (v "0.38.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0n7n0mfz6px6bbbck54hjmbfb44hjh96xjfxfa9xsf3wasssb315")))

(define-public crate-semilattice-database-session-0.39.0 (c (n "semilattice-database-session") (v "0.39.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0ppmg0fhrjrrawkxmhmfdf26x8qcsz6wm63d92rsgsln1nsqvjaq")))

(define-public crate-semilattice-database-session-0.39.1 (c (n "semilattice-database-session") (v "0.39.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "04ianqjg89azyyj9i7pz9vmlvl2g2b6rjribj64jycvvy7x6x3yd")))

(define-public crate-semilattice-database-session-0.40.0 (c (n "semilattice-database-session") (v "0.40.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "17qaar49r6y6s74lbp2pah2dkzs85ghw6il9x99p5w768lxg3hxi")))

(define-public crate-semilattice-database-session-0.41.0 (c (n "semilattice-database-session") (v "0.41.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1dwjrj9805w9x6ammyyin4gzhbc7ain28mdbp74j89lziddfncgm")))

(define-public crate-semilattice-database-session-0.41.1 (c (n "semilattice-database-session") (v "0.41.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.100.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1ccw4bvabjp2b3ybfn3v1y8gn842cw9gd86asb2l2kyzxa7xz76h")))

(define-public crate-semilattice-database-session-0.41.2 (c (n "semilattice-database-session") (v "0.41.2") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.100.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1yx3abjrrfn6ra9vbx5al84sms8xar2b2xbha68dgvr3baa30sp2")))

(define-public crate-semilattice-database-session-0.42.0 (c (n "semilattice-database-session") (v "0.42.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.101.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "09jv1vjmc3sgvi49a464znk4i2jj0by5jqnp3ywkv2v670mp3ja6")))

(define-public crate-semilattice-database-session-0.43.0 (c (n "semilattice-database-session") (v "0.43.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.101.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0jv002zvx4p1xx2v0sf6phpwgimyvps8h65pfwvsx5c20fnihn1z")))

(define-public crate-semilattice-database-session-0.43.1 (c (n "semilattice-database-session") (v "0.43.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.102.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0prxd7mbhl6bjrnylwk51gm80jx1s3k51dwmxfjg05z8y60606aw")))

(define-public crate-semilattice-database-session-0.44.0 (c (n "semilattice-database-session") (v "0.44.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.103.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0vjasjdasp637hv3ml2xd5lxrz71ihf4b9j133gi046l41m37gyw")))

(define-public crate-semilattice-database-session-0.45.0 (c (n "semilattice-database-session") (v "0.45.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.104.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "15c6qfy0x9yaq7qlirwj7h6m2mai0wzi3vi3hasbcjp8w39j8bx5")))

(define-public crate-semilattice-database-session-0.46.0 (c (n "semilattice-database-session") (v "0.46.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.105.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1s91c4fsggzn70q5hswa0hj3nb605j5q5r7wiv94vx194hk7b2gn")))

(define-public crate-semilattice-database-session-0.46.1 (c (n "semilattice-database-session") (v "0.46.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.105.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "17wcllywd3bajw7fyqpdh2fai8inlj3r5v743qvibkxn1816kdm9")))

(define-public crate-semilattice-database-session-0.47.0 (c (n "semilattice-database-session") (v "0.47.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.106.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1kvgk51nh1vxhimi7shxdwwc5rhfdzq3mvsx4zg1w2bz56hcmj3s")))

(define-public crate-semilattice-database-session-0.47.1 (c (n "semilattice-database-session") (v "0.47.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.107.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1khsinlpf691vmqnnkqrd9fr53imdi24qkg8x9q885k1qmdrlfdr")))

(define-public crate-semilattice-database-session-0.47.2 (c (n "semilattice-database-session") (v "0.47.2") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.108.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0a6y3x5damf0g2vjcb0jxr7xizp8kqa4hxsjcs5yca2dq15zfxbh")))

(define-public crate-semilattice-database-session-0.48.0 (c (n "semilattice-database-session") (v "0.48.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.109.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0gr8lgpb26wf0xpxab94fwk7g577rncpd01z6lllaq9910ak37xs")))

(define-public crate-semilattice-database-session-0.48.1 (c (n "semilattice-database-session") (v "0.48.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "semilattice-database") (r "^0.110.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "051wxbk8yyng45059bhvn2sjba0pvqs2fx3w8g5chinsxc71a7j4")))

