(define-module (crates-io se mi semilattice-database) #:use-module (crates-io))

(define-public crate-semilattice-database-0.1.0 (c (n "semilattice-database") (v "0.1.0") (d (list (d (n "versatile-data") (r "^0.22") (d #t) (k 0)))) (h "1md0cpqqwn978iayiagi3qpwmiqqczra1m2frm5klhlba95zggwf") (y #t)))

(define-public crate-semilattice-database-0.1.1 (c (n "semilattice-database") (v "0.1.1") (d (list (d (n "versatile-data") (r "^0.22") (d #t) (k 0)))) (h "1zfli08hml008rw1910srn2bdy00zx0imbbwr2pgpfj7ks10l1gl") (y #t)))

(define-public crate-semilattice-database-0.1.2 (c (n "semilattice-database") (v "0.1.2") (d (list (d (n "versatile-data") (r "^0.22") (d #t) (k 0)))) (h "1n3nwh45wf8gh4pahmy0z3j8gcjahh04bmhhyr8sw8za4synx6wa") (y #t)))

(define-public crate-semilattice-database-0.1.4 (c (n "semilattice-database") (v "0.1.4") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.22") (d #t) (k 0)))) (h "17qavlkdqpz3b40r6rz8mfgm4bi2za8a3zc974rbs9xkpb4mjisf") (y #t)))

(define-public crate-semilattice-database-0.1.5 (c (n "semilattice-database") (v "0.1.5") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.23") (d #t) (k 0)))) (h "0zvd3fq8vl1b4ymac7dg6y6dmjx243yjklg45fdrh2afpy15i263") (y #t)))

(define-public crate-semilattice-database-0.1.6 (c (n "semilattice-database") (v "0.1.6") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.25") (d #t) (k 0)))) (h "1wm107yni626d4j4ahnzhy7a4sb5kfcfrnpv32mizyf7md0fbxvj") (y #t)))

(define-public crate-semilattice-database-0.2.0 (c (n "semilattice-database") (v "0.2.0") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.25") (d #t) (k 0)))) (h "06h353s89vmfvf80qiflc82i5h0dz0y5imgddsb392q1ph79hsvi") (y #t)))

(define-public crate-semilattice-database-0.2.1 (c (n "semilattice-database") (v "0.2.1") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.25") (d #t) (k 0)))) (h "0158f1mqf8v834dia53gwc4r9pcm7lscrqw26nfbi9w64k535w04") (y #t)))

(define-public crate-semilattice-database-0.2.2 (c (n "semilattice-database") (v "0.2.2") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.26") (d #t) (k 0)))) (h "1y96kk20gj0cwgj16fs1ndfl40cdy1s3zr6rkdrhjiwk936jxxzs") (y #t)))

(define-public crate-semilattice-database-0.3.0 (c (n "semilattice-database") (v "0.3.0") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.26") (d #t) (k 0)))) (h "18ibq4jhr1i9swi73nba2g97wcm70nrxqp3b2yk6pwxy72qp16v5") (y #t)))

(define-public crate-semilattice-database-0.4.0 (c (n "semilattice-database") (v "0.4.0") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.26") (d #t) (k 0)))) (h "0hmv6qbndca9s8zzg1b80krgd4b4w6bibvqarcqx5wlh28sglhlb") (y #t)))

(define-public crate-semilattice-database-0.5.0 (c (n "semilattice-database") (v "0.5.0") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.26") (d #t) (k 0)))) (h "1d14ahvh0jvkrpa19p5z268ym98qqb3z33az3sgfppm6aix8rf5p") (y #t)))

(define-public crate-semilattice-database-0.5.1 (c (n "semilattice-database") (v "0.5.1") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.27") (d #t) (k 0)))) (h "0vsiif264n0vnk71z1j9s9w0vnr6a6x6x7znh8bn5ny6ws7lamrr") (y #t)))

(define-public crate-semilattice-database-0.5.2 (c (n "semilattice-database") (v "0.5.2") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.29") (d #t) (k 0)))) (h "07yxf4q7snwbr78r6hhwx2bhnd21zsh92rchzf60j2afvn4aff2h") (y #t)))

(define-public crate-semilattice-database-0.5.3 (c (n "semilattice-database") (v "0.5.3") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.30") (d #t) (k 0)))) (h "16xpcxf45yw0ks1v1z4g89b2gdwkllhgbgvyyv0665q048d3yfvz") (y #t)))

(define-public crate-semilattice-database-0.6.0 (c (n "semilattice-database") (v "0.6.0") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.31") (d #t) (k 0)))) (h "1y34xjs5h1jpwnk70c6czv8rgbwjg6hhn05ygv5w1n97cphpdy8p") (y #t)))

(define-public crate-semilattice-database-0.6.1 (c (n "semilattice-database") (v "0.6.1") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.32") (d #t) (k 0)))) (h "000h6x6gdrlrjfy0vx9hfxnf56vnf18w3c3dzw2gnmfwswqslzwk") (y #t)))

(define-public crate-semilattice-database-0.6.2 (c (n "semilattice-database") (v "0.6.2") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "0xbsmyj18hyjn71vk5w5rvzyf2nn5lr2xw0i0h9vawl33qildn0g") (y #t)))

(define-public crate-semilattice-database-0.6.3 (c (n "semilattice-database") (v "0.6.3") (d (list (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "1zpkvz4wz11cx7rwm1xqn3ww6rs6py8zdw9a1l78vhi34ci0mz45") (y #t)))

(define-public crate-semilattice-database-0.7.0 (c (n "semilattice-database") (v "0.7.0") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "1rcfjd8972plkaaigfa6vkgicdwbr6ppahb5wl2j3v7jappwi428") (y #t)))

(define-public crate-semilattice-database-0.7.1 (c (n "semilattice-database") (v "0.7.1") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "1jxwv8nyxpk6mg4fimphqdiglglfb3mhcirs4p8sy7mlnrwz1bhy") (y #t)))

(define-public crate-semilattice-database-0.7.2 (c (n "semilattice-database") (v "0.7.2") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "1hfag2afvxhlbgfh6p5alsmyd269vgzwiaiaa2mba9l563n47gyk") (y #t)))

(define-public crate-semilattice-database-0.8.0 (c (n "semilattice-database") (v "0.8.0") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.33") (d #t) (k 0)))) (h "15prfq1ksjjbj5a0gpcdv4bydrws6yhd34pv18bkha3px9y9cakj") (y #t)))

(define-public crate-semilattice-database-0.9.0 (c (n "semilattice-database") (v "0.9.0") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.34") (d #t) (k 0)))) (h "12cjc7x9my2pmffc1imlal8ba2s56xlf71629g2b3ia3jdfj971v") (y #t)))

(define-public crate-semilattice-database-0.10.0 (c (n "semilattice-database") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "0i41rwxjs058pybf16kpd2416m7wrrrhkn6339zipykxzlfr2cqq") (y #t)))

(define-public crate-semilattice-database-0.11.0 (c (n "semilattice-database") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "08dw9zpii6fxzianfgngzxmdhja5a63zg7ycgrjcafhvxc8xkc3f") (y #t)))

(define-public crate-semilattice-database-0.11.1 (c (n "semilattice-database") (v "0.11.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.3") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "158prpxl4vvha11c4n0m36lakl88qamvhhmp5lyr509vkhy3qq93") (y #t)))

(define-public crate-semilattice-database-0.11.2 (c (n "semilattice-database") (v "0.11.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "12crbhkvk2qmg5f4sra2h78vfphbqmjja05l1y1f8q4vzj8qjymd") (y #t)))

(define-public crate-semilattice-database-0.11.3 (c (n "semilattice-database") (v "0.11.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "1gl33xxah1wjls90m7mnqk87xs1lci776jycqc1725g8fa1f8iq0") (y #t)))

(define-public crate-semilattice-database-0.12.0 (c (n "semilattice-database") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "1ss7d884nqvdidfd8px1zcwnvissqiv0gxmqm0vb1p8d0m87djsc") (y #t)))

(define-public crate-semilattice-database-0.13.0 (c (n "semilattice-database") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.37") (d #t) (k 0)))) (h "19xr0ywlh0jsv8dfblk9ih9cc32fgxchfwvfqcbpnvfrbm6ysfmq") (y #t)))

(define-public crate-semilattice-database-0.14.0 (c (n "semilattice-database") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.38") (d #t) (k 0)))) (h "0rgr17hnqmga6n8ksjf0h39mi0j8bqqd28a31bxxbwn73kcfmn6m") (y #t)))

(define-public crate-semilattice-database-0.14.1 (c (n "semilattice-database") (v "0.14.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.38") (d #t) (k 0)))) (h "1lrq0fx9n0zgvpcywx5pqqwbq3whcbp8ym2i2g1phanv2wsgxfsr") (y #t)))

(define-public crate-semilattice-database-0.14.2 (c (n "semilattice-database") (v "0.14.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.38") (d #t) (k 0)))) (h "0h0masx4h56bgha9nbanmc09i3l94pv4c7vyrk7nzymw166v3vxg") (y #t)))

(define-public crate-semilattice-database-0.15.0 (c (n "semilattice-database") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.39") (d #t) (k 0)))) (h "1c7s7fqkwricaqagkm4n1qa2nhwir3g242k91hcj1abr8hb8jh2c") (y #t)))

(define-public crate-semilattice-database-0.15.1 (c (n "semilattice-database") (v "0.15.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.39") (d #t) (k 0)))) (h "0shvx99664dvfzmdspqkbgg86d33zzspk6faa88wqjx9yivvass8") (y #t)))

(define-public crate-semilattice-database-0.15.2 (c (n "semilattice-database") (v "0.15.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.39") (d #t) (k 0)))) (h "031w2x2vd9j7qnbxngqwlyhmkr20j9x2yhm3b99wkwsqq0wzcw31") (y #t)))

(define-public crate-semilattice-database-0.16.0 (c (n "semilattice-database") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.39") (d #t) (k 0)))) (h "0jrc02v7qdrhg95wp6l3ghshah2b14gsspm4vv56y0818ivsq8iw") (y #t)))

(define-public crate-semilattice-database-0.16.1 (c (n "semilattice-database") (v "0.16.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.39") (d #t) (k 0)))) (h "14gjkhb0ccc3wzr4sks73wsphcy908fc69hfhsl5xalvd85mvvcn") (y #t)))

(define-public crate-semilattice-database-0.17.0 (c (n "semilattice-database") (v "0.17.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.40") (d #t) (k 0)))) (h "0xza002la1x4jap9sx2p5b247lr5qdq4rgx45f8z5adx8zb47pcj") (y #t)))

(define-public crate-semilattice-database-0.18.0 (c (n "semilattice-database") (v "0.18.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.40") (d #t) (k 0)))) (h "0cnh8g66vqy5ljz7bwi8kpcakj0axa8kyvc4k11y68ah3wmvbirp") (y #t)))

(define-public crate-semilattice-database-0.20.0 (c (n "semilattice-database") (v "0.20.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.40") (d #t) (k 0)))) (h "05wa14ixlq1ffg83bl87dfc0w8dmxlxqs0yk8j4nyy493yrrlm4i") (y #t)))

(define-public crate-semilattice-database-0.20.1 (c (n "semilattice-database") (v "0.20.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.41") (d #t) (k 0)))) (h "0qy344yn7kg079fivfsczs12lwxklpkw63hn9v9hbvydzbbyyyhr") (y #t)))

(define-public crate-semilattice-database-0.20.2 (c (n "semilattice-database") (v "0.20.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.42") (d #t) (k 0)))) (h "0xdmvcb4d9xlysacsb6iwpdrr5bqm9wmkpbq718m4n6fh5xaj8i1") (y #t)))

(define-public crate-semilattice-database-0.20.3 (c (n "semilattice-database") (v "0.20.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.4") (d #t) (k 0)) (d (n "versatile-data") (r "^0.43") (d #t) (k 0)))) (h "1ja4vi4l8dwhshsmq1k2y4pvs7qy12fcvrz1jx0scz0gm4cb4qbk") (y #t)))

(define-public crate-semilattice-database-0.20.4 (c (n "semilattice-database") (v "0.20.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.5") (d #t) (k 0)) (d (n "versatile-data") (r "^0.44") (d #t) (k 0)))) (h "0d2yvf20rmg676vdnjk4a6pq5ij91hjrvmr9b186w1675jviv8zy") (y #t)))

(define-public crate-semilattice-database-0.20.5 (c (n "semilattice-database") (v "0.20.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.5") (d #t) (k 0)) (d (n "versatile-data") (r "^0.44") (d #t) (k 0)))) (h "04l73crzsdslfbl0dc5gybcj5s1191xc0cblya7dr94rfc24sblm") (y #t)))

(define-public crate-semilattice-database-0.21.0 (c (n "semilattice-database") (v "0.21.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.44") (d #t) (k 0)))) (h "0djrmmc2ra53qv1ylivlng5s99cg3vw78llh4xwkalgjfmkdrp3l") (y #t)))

(define-public crate-semilattice-database-0.22.0 (c (n "semilattice-database") (v "0.22.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.44") (d #t) (k 0)))) (h "1rhrn9gzph7gs6xccai35gpk83pgil0vhq0ayyls084p95nlkra9") (y #t)))

(define-public crate-semilattice-database-0.23.0 (c (n "semilattice-database") (v "0.23.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.45") (d #t) (k 0)))) (h "0c7hmsz19hkfr2vvs6pxl1p6xm9n5rlpc4655511a1h9kffhc3p1") (y #t)))

(define-public crate-semilattice-database-0.24.0 (c (n "semilattice-database") (v "0.24.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.45") (d #t) (k 0)))) (h "1k090110c9rhnyzf27wcaj1zhykws65bwqnnip8jhbh5yi8mgsz6") (y #t)))

(define-public crate-semilattice-database-0.24.1 (c (n "semilattice-database") (v "0.24.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.45") (d #t) (k 0)))) (h "00y37skmvyjr0w97qpn238fsdl1p380jq6yp5bfcg7xahgb7c7i5") (y #t)))

(define-public crate-semilattice-database-0.25.0 (c (n "semilattice-database") (v "0.25.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "0hldb0xkv37ip6hv77szbcsgjb9dzfw34lv7q6dr4q40933c6l9l") (y #t)))

(define-public crate-semilattice-database-0.26.0 (c (n "semilattice-database") (v "0.26.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "10bvc6w8b09vms30y2340bsrfm2gapvibrifi93ks55k6ppf1hlc") (y #t)))

(define-public crate-semilattice-database-0.26.1 (c (n "semilattice-database") (v "0.26.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "1b33x9mkn5ykm8wc19nl734il10cykcjf7y8d5pi7258iljlqsly") (y #t)))

(define-public crate-semilattice-database-0.27.0 (c (n "semilattice-database") (v "0.27.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "1kcz977r756ivhps4xhripxg8q02mbn7smcrpsss981i792lixhl") (y #t)))

(define-public crate-semilattice-database-0.28.0 (c (n "semilattice-database") (v "0.28.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "1irabypj74hbbm1savhxmgnr30c2pivxwby81vij9r39mpjcvbdn") (y #t)))

(define-public crate-semilattice-database-0.28.1 (c (n "semilattice-database") (v "0.28.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.4") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "03gwgqm3wa9gvnq46hhwz10p2dk1smbk7n04qzsagqy3izs8sb8k") (y #t)))

(define-public crate-semilattice-database-0.28.2 (c (n "semilattice-database") (v "0.28.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.5") (d #t) (k 0)) (d (n "idx_binary") (r "^0.6") (d #t) (k 0)) (d (n "versatile-data") (r "^0.46") (d #t) (k 0)))) (h "1p185zx50gxp6x82jv873w3kvn7rdwwgvrcrp7sl966jzqa94jdl") (y #t)))

(define-public crate-semilattice-database-0.28.3 (c (n "semilattice-database") (v "0.28.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.6") (d #t) (k 0)) (d (n "idx_binary") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.47") (d #t) (k 0)))) (h "1m4mmzz5nhdgqp6lfd40azncqyphyfpp1dvmb2937whk7l2sa5f4") (y #t)))

(define-public crate-semilattice-database-0.28.4 (c (n "semilattice-database") (v "0.28.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.7") (d #t) (k 0)) (d (n "idx_binary") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.47") (d #t) (k 0)))) (h "1k16dd14l0s8vaxqkrmfnqsvangqjcflbbws7aaw83y31q95sy73") (y #t)))

(define-public crate-semilattice-database-0.28.5 (c (n "semilattice-database") (v "0.28.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.7") (d #t) (k 0)) (d (n "idx_binary") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.48") (d #t) (k 0)))) (h "024njgmbfsdgf6j1djrf3nd9hzbl3xqbg0flzldgc2vl8rybx7l1") (y #t)))

(define-public crate-semilattice-database-0.29.0 (c (n "semilattice-database") (v "0.29.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.7") (d #t) (k 0)) (d (n "idx_binary") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.49") (d #t) (k 0)))) (h "124a6qnhfk6k6kx83x4f25k9x2bhd9683b3giki7w7g868d7cqzi") (y #t)))

(define-public crate-semilattice-database-0.29.1 (c (n "semilattice-database") (v "0.29.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.49") (d #t) (k 0)))) (h "04vvpxdaqac5mr2mk78lm32bk9bi7njfvd2fhqgrk8c7gxplnfnb") (y #t)))

(define-public crate-semilattice-database-0.30.0 (c (n "semilattice-database") (v "0.30.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.50") (d #t) (k 0)))) (h "06zpg66dj9d225rpqjkcb6d0xqmww4nr0n3vlds18zv2x4zl9ff0") (y #t)))

(define-public crate-semilattice-database-0.31.0 (c (n "semilattice-database") (v "0.31.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.51") (d #t) (k 0)))) (h "0m41fsn4ps0lnyf5nanqpb7b1d5aqdypd9y17zpk32rv94kws0ja") (y #t)))

(define-public crate-semilattice-database-0.32.0 (c (n "semilattice-database") (v "0.32.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.9") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "0xxc38b8s7dn6l0wll1a1p4n3dv3v6vl78gcn2rnayhjn3q1gf5p") (y #t)))

(define-public crate-semilattice-database-0.33.0 (c (n "semilattice-database") (v "0.33.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.9") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "03g397fk5lqsm91fqyx60chmz2b906vzqalb3fcfd5xg1mykmlj8") (y #t)))

(define-public crate-semilattice-database-0.33.1 (c (n "semilattice-database") (v "0.33.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.9") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "0bb91m3d94pgpmmy1alvlyjzlr9nvwqrk45y02hi8q5al0fs70jd") (y #t)))

(define-public crate-semilattice-database-0.34.0 (c (n "semilattice-database") (v "0.34.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.9") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "0r12fbvkpwr5gmbb6gpah0cr2z4fsfzfq32ij6c68r139yhzj8v1") (y #t)))

(define-public crate-semilattice-database-0.35.0 (c (n "semilattice-database") (v "0.35.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.9") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "1hrq9c22qn816lv7kbzrid8qdd0inah1rki1zq0mm48nlpsnl2av") (y #t)))

(define-public crate-semilattice-database-0.36.0 (c (n "semilattice-database") (v "0.36.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "1achhyk4crmkbbdcrq6817yvskb0f6z84zpsn7d6rxp09h5ks06k") (y #t)))

(define-public crate-semilattice-database-0.37.0 (c (n "semilattice-database") (v "0.37.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "02b49wq7q6s3g7xa61yzr9wx9y3184l9zsy876rppbrcls1b97d5") (y #t)))

(define-public crate-semilattice-database-0.37.1 (c (n "semilattice-database") (v "0.37.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "0x29awn1z4kvzqqkgljbja0lcsxr3viaxsfmr3gj3bm5ji942bxa") (y #t)))

(define-public crate-semilattice-database-0.37.2 (c (n "semilattice-database") (v "0.37.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "04mva3s6gbh1bjs6byxc83xlxzbk5x86lcwvhv0dink5f43nm7c4") (y #t)))

(define-public crate-semilattice-database-0.37.3 (c (n "semilattice-database") (v "0.37.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "16shapjfx7hpc315qslvc1lksffrq50f84pvqkdcnmdhw93y5fpg") (y #t)))

(define-public crate-semilattice-database-0.37.4 (c (n "semilattice-database") (v "0.37.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "0g12308617vhc3fxylb8qgmby6zrar9zcg7rabzlp3ava7prfwmg") (y #t)))

(define-public crate-semilattice-database-0.37.5 (c (n "semilattice-database") (v "0.37.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.54") (d #t) (k 0)))) (h "1qs3pg4avn3v76lyzxb6jvnz2lxra3fjhqkkdl4wzyq8qw14767v") (y #t)))

(define-public crate-semilattice-database-0.38.0 (c (n "semilattice-database") (v "0.38.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "1pnzai9dq32n83f9kqi23lky5ngy6j50i4cqgcikj3fh63acj5rh") (y #t)))

(define-public crate-semilattice-database-0.38.1 (c (n "semilattice-database") (v "0.38.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "1awcwp6cc2i0zyn1cb5xg3gmm9m8r0zvyi5sdqcjp7yz2s0v9mv6") (y #t)))

(define-public crate-semilattice-database-0.38.2 (c (n "semilattice-database") (v "0.38.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "132ksysx9b99lfnz8mc890g85scghiajnpjsg1nwl127xr5s01ac") (y #t)))

(define-public crate-semilattice-database-0.38.3 (c (n "semilattice-database") (v "0.38.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "067zdplzn0dfx3rx84ip5pdd6khagrqash1rww989fa62l52kdyx") (y #t)))

(define-public crate-semilattice-database-0.38.4 (c (n "semilattice-database") (v "0.38.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.8") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "1chan8rff3xznj40wczl8f0n0fq83n6p9f54zh1i4r2jjxb43f70") (y #t)))

(define-public crate-semilattice-database-0.38.5 (c (n "semilattice-database") (v "0.38.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.9") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "1klk59av8iq7qpclb2qi9b4jva1c1qaq8min08ka5fz3z70nb4ig") (y #t)))

(define-public crate-semilattice-database-0.38.6 (c (n "semilattice-database") (v "0.38.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.10") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "0ckbwa96yn9nsnmi4d2pibildiba7h8nmaihbhhlz1h61jl5jg8z") (y #t)))

(define-public crate-semilattice-database-0.38.7 (c (n "semilattice-database") (v "0.38.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.11") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "17lpa5q4ll8nb41drddql1wack0khd053p38galyd1yxadxvz5m4") (y #t)))

(define-public crate-semilattice-database-0.38.8 (c (n "semilattice-database") (v "0.38.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.12") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.55") (d #t) (k 0)))) (h "1ahf7dlj4gyavk27hkv4ywi8bix6awsz8hvpw8hcqxj2mrm2gafc") (y #t)))

(define-public crate-semilattice-database-0.38.9 (c (n "semilattice-database") (v "0.38.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.12") (d #t) (k 0)) (d (n "idx_binary") (r "^0.10") (d #t) (k 0)) (d (n "versatile-data") (r "^0.56") (d #t) (k 0)))) (h "0q3yc53n23wqgp0fqm9nfsw1xvzk7xanhdilyrv0knjr6m9835xq") (y #t)))

(define-public crate-semilattice-database-0.38.10 (c (n "semilattice-database") (v "0.38.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.12") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "versatile-data") (r "^0.57") (d #t) (k 0)))) (h "0ljlhblsvv5q8382ngbs3paw32s2b1y5p93csy2qbhc7rixfvg4k") (y #t)))

(define-public crate-semilattice-database-0.39.0 (c (n "semilattice-database") (v "0.39.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.12") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "1lr3n8y06b0n79hx1pgcz8lmc7xmv4c2wcs6sig79bwp7lzsmk2l") (y #t)))

(define-public crate-semilattice-database-0.40.0 (c (n "semilattice-database") (v "0.40.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.13") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "02agizf6jld5kdhw6x0alspsri6n8jszqj61knaqgb5gyy36fkbl") (y #t)))

(define-public crate-semilattice-database-0.40.1 (c (n "semilattice-database") (v "0.40.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.13") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "13d8j252b90ja9xifmcw4x5mfjanrrf9xz3f0x23isjvbf2nbxg3") (y #t)))

(define-public crate-semilattice-database-0.40.2 (c (n "semilattice-database") (v "0.40.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "18asdyi2yv75lw67h7nb3lxd9pavd6bscwnyizxhgqjwdp9l5g9y") (y #t)))

(define-public crate-semilattice-database-0.40.3 (c (n "semilattice-database") (v "0.40.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "12l4r4hbrkziavn37h42hbzm8gnwxldav6bgzf7p7vds183pzvxn") (y #t)))

(define-public crate-semilattice-database-0.41.0 (c (n "semilattice-database") (v "0.41.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.58") (d #t) (k 0)))) (h "159kj2q8gbaaq44871i86dddls2vk8lbvyy4b4ixj97s5ysvilcx") (y #t)))

(define-public crate-semilattice-database-0.42.0 (c (n "semilattice-database") (v "0.42.0") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "07k69vrdma1jpnwab62bra7m8v0m44g52inr5qv0glfkc5lh0ly9") (y #t)))

(define-public crate-semilattice-database-0.43.0 (c (n "semilattice-database") (v "0.43.0") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0kwpldb07v6dlwmzl9w3nxm045cinmiwfvvq6j3n7vjlgy7gjm7q") (y #t)))

(define-public crate-semilattice-database-0.43.1 (c (n "semilattice-database") (v "0.43.1") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0q4ia5hw6qhk4mqb9flgmlcz5jn7nxjky1g1k3amh0ij5gvr28b6") (y #t)))

(define-public crate-semilattice-database-0.43.2 (c (n "semilattice-database") (v "0.43.2") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0pnqi93lva4ijynpmmjsrnrddng8nh47ayda9sicbllf7blhbqfd") (y #t)))

(define-public crate-semilattice-database-0.43.3 (c (n "semilattice-database") (v "0.43.3") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0g1ixnfq7s2gqgk8zwp04gd3bflk8z4waj7wgackybdlrmvb8lhi") (y #t)))

(define-public crate-semilattice-database-0.43.4 (c (n "semilattice-database") (v "0.43.4") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0q5qz85mir8m3anbydbn4z2sfh9nfz83xp77mzf1j16zzmh687nh") (y #t)))

(define-public crate-semilattice-database-0.43.5 (c (n "semilattice-database") (v "0.43.5") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0x7pqpy1za52vs7m1qgnwl9y1sfz317m4yz7w2inlhys66v5ql89") (y #t)))

(define-public crate-semilattice-database-0.44.0 (c (n "semilattice-database") (v "0.44.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "1nkm0f53hy5x8psgr6rgp45kww1hh3yyclprwi96h41qkd919kyv") (y #t)))

(define-public crate-semilattice-database-0.44.1 (c (n "semilattice-database") (v "0.44.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0kwcy62nw02w0kwl85n3lm2h2jpccy1053qnxqq083xx0h582f92") (y #t)))

(define-public crate-semilattice-database-0.44.2 (c (n "semilattice-database") (v "0.44.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0jfnrzlhvcrjc7jgaqm7ak6hzr3x43kfq626dv19fbkcxx9dv56z") (y #t)))

(define-public crate-semilattice-database-0.45.0 (c (n "semilattice-database") (v "0.45.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.59") (d #t) (k 0)))) (h "0b936jqcs6w5dh3d89v46palx861hiaannf1xj81xb3rqgsg7c1l") (y #t)))

(define-public crate-semilattice-database-0.46.0 (c (n "semilattice-database") (v "0.46.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.60") (d #t) (k 0)))) (h "0iy2n2dq251i7fdrid3wasgzgg7wssmh5l1m7xny59rlnphkcj7n") (y #t)))

(define-public crate-semilattice-database-0.46.1 (c (n "semilattice-database") (v "0.46.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.60") (d #t) (k 0)))) (h "1f882pl4x8lasyn27ggzplvcrm6acdwwav3s8zwpb08jm9xzqfwg") (y #t)))

(define-public crate-semilattice-database-0.46.2 (c (n "semilattice-database") (v "0.46.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.62") (d #t) (k 0)))) (h "04y5837qxsk3i1spa9vy87mq6nhnwfxqcfivnpyf6y1k7pl2xl5a") (y #t)))

(define-public crate-semilattice-database-0.46.3 (c (n "semilattice-database") (v "0.46.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.63") (d #t) (k 0)))) (h "0d4qp4asi31ajfqzn13b3jn4iy8kq2xig55jwsscsnhbvfgcgpbn") (y #t)))

(define-public crate-semilattice-database-0.46.4 (c (n "semilattice-database") (v "0.46.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.63") (d #t) (k 0)))) (h "0dpfk2s6f46ww5vvggz46797bc69l4zl2griwc431id5gmwmj6mk") (y #t)))

(define-public crate-semilattice-database-0.47.0 (c (n "semilattice-database") (v "0.47.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.63") (d #t) (k 0)))) (h "1b7ipx50p88jj9vicklw4vzzfw32dn1r9wnyywbkx5ycy4b4frw3") (y #t)))

(define-public crate-semilattice-database-0.47.1 (c (n "semilattice-database") (v "0.47.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.63") (d #t) (k 0)))) (h "0lppiwb9f08z8yq05h8vddgpg0abzrqk73672m3klk7yy202qysz") (y #t)))

(define-public crate-semilattice-database-0.47.2 (c (n "semilattice-database") (v "0.47.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.63") (d #t) (k 0)))) (h "16q5ihyvmx4lbn2dmkwvgsvflnp9wmrk6ym0ibf23ldiha9rx9d2") (y #t)))

(define-public crate-semilattice-database-0.48.0 (c (n "semilattice-database") (v "0.48.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1rgqywafw44b7mzq7hz1gjp9bm1rqmcqq8imlzip5y26dk1vck3z") (y #t)))

(define-public crate-semilattice-database-0.49.0 (c (n "semilattice-database") (v "0.49.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "004fsp5qazqb9l6h1amhmsihmj6xhz6m7s61yv6ma8jrr3lz0p3j") (y #t)))

(define-public crate-semilattice-database-0.50.0 (c (n "semilattice-database") (v "0.50.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "07jd1kzz92c22jbd1j7m4pwx0sj14kd1vaq8p1rfwa8703k407hq") (y #t)))

(define-public crate-semilattice-database-0.51.0 (c (n "semilattice-database") (v "0.51.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1w007l4xm2rlm35bwfkgzch5in2905ql515py34nxzqdyng2v7si") (y #t)))

(define-public crate-semilattice-database-0.52.0 (c (n "semilattice-database") (v "0.52.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "0by9laxqhdaxv3z87f28bnx4c8yaqv88larjny8yvzqs1gbmg8av") (y #t)))

(define-public crate-semilattice-database-0.52.1 (c (n "semilattice-database") (v "0.52.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "0rhk4jzydc1xnbhgd1l8mxal6cax0h373ibfd66mmkkzwmv9rxzv") (y #t)))

(define-public crate-semilattice-database-0.53.0 (c (n "semilattice-database") (v "0.53.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "071fzjdfnvvbvd2c493hzizpic022hpw2asv7w77c5mvfd6n9y64") (y #t)))

(define-public crate-semilattice-database-0.54.0 (c (n "semilattice-database") (v "0.54.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1n5acbvng9acaawmbk2jx5w3i5ziv1m3zx244aps3lkycxag1qma") (y #t)))

(define-public crate-semilattice-database-0.54.1 (c (n "semilattice-database") (v "0.54.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1ppcnz0kx6rgc3xwm013yvmxr112ldbbzx7gbicg00v3b68kzivh") (y #t)))

(define-public crate-semilattice-database-0.54.2 (c (n "semilattice-database") (v "0.54.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1qlrx86alkvhrwcqbksnjg6hggr0qh161vn0jk18ixw12rgl26p9") (y #t)))

(define-public crate-semilattice-database-0.54.3 (c (n "semilattice-database") (v "0.54.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1w3jzkj6d9x7cmvbirwjfh8s3mkdj816r45ic2v81cwrd0n8r3fq") (y #t)))

(define-public crate-semilattice-database-0.54.4 (c (n "semilattice-database") (v "0.54.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1c3y5zi8v687nrjfidlv7djshszgbxrax9593q8g0mnyjw30ysqc") (y #t)))

(define-public crate-semilattice-database-0.55.0 (c (n "semilattice-database") (v "0.55.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "11hmv28rdgdhix1r83xq0xvvpb9l9306rzmz4adzs59rkz7104ng") (y #t)))

(define-public crate-semilattice-database-0.55.1 (c (n "semilattice-database") (v "0.55.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "13n3q5qv0gazchjyl3dd33im7v9ad2qdwi82iqwgkb1z19wmaf6l") (y #t)))

(define-public crate-semilattice-database-0.56.0 (c (n "semilattice-database") (v "0.56.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.64") (d #t) (k 0)))) (h "1355897ls7k171s4rvnayjyfqpffivq2lncandz7sbl4jnr38xdk") (y #t)))

(define-public crate-semilattice-database-0.57.0 (c (n "semilattice-database") (v "0.57.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.14") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.65") (d #t) (k 0)))) (h "1im00bf5g9li5iz8k30j58v4pq8fadmn80nvqcl0nyxp30yx898h") (y #t)))

(define-public crate-semilattice-database-0.57.1 (c (n "semilattice-database") (v "0.57.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.66") (d #t) (k 0)))) (h "0mgc0b8adsh6d2mpi6h4a7x146339b4a0xin5ip654m85fsyq0y4") (y #t)))

(define-public crate-semilattice-database-0.58.0 (c (n "semilattice-database") (v "0.58.0") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.67") (d #t) (k 0)))) (h "0byysqi0h72f5myk336c476kwb0cjyc2l8h16xsyadd1nirwa4qh") (y #t)))

(define-public crate-semilattice-database-0.58.1 (c (n "semilattice-database") (v "0.58.1") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "1ykf326mw39nlnhacmfabb7ljb0w0xs2h438wcycrimac5wbm8jr") (y #t)))

(define-public crate-semilattice-database-0.58.2 (c (n "semilattice-database") (v "0.58.2") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "0y81ypf1074jb7wmpszbc049dnaigzv7kmsbc675x8gm8qw9f4iy") (y #t)))

(define-public crate-semilattice-database-0.58.3 (c (n "semilattice-database") (v "0.58.3") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "0h5fljb85v8ywy9pkc6z1jln880bpnbd8igyjbqwd7nc3bc6jyk8") (y #t)))

(define-public crate-semilattice-database-0.58.4 (c (n "semilattice-database") (v "0.58.4") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "1gfrqaw8si018ygn05dbx131wlqc2in3rjgmriq1q32332qv74km") (y #t)))

(define-public crate-semilattice-database-0.59.0 (c (n "semilattice-database") (v "0.59.0") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "1qnf694wl5lq4091pz93gdxbp0kyyngzhyikygpljq31lb8qipx8") (y #t)))

(define-public crate-semilattice-database-0.59.1 (c (n "semilattice-database") (v "0.59.1") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "025ifgb73m819v7w6sy9942q1vqb33y69wfpjisw07sq2y1q77li") (y #t)))

(define-public crate-semilattice-database-0.59.2 (c (n "semilattice-database") (v "0.59.2") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)) (d (n "idx_binary") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.68") (d #t) (k 0)))) (h "15wvbmm804f8ca0jrgsdf7qgnd3n0b1h0fcdpp939wr8mzvj4a33") (y #t)))

(define-public crate-semilattice-database-0.59.3 (c (n "semilattice-database") (v "0.59.3") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.69") (d #t) (k 0)))) (h "121agw2d44vj1x0wivih40wv39dbrv0wqnn5yaygld789wszz364") (y #t)))

(define-public crate-semilattice-database-0.59.4 (c (n "semilattice-database") (v "0.59.4") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.70") (d #t) (k 0)))) (h "0gxjcgskk51hqk603hfrmx0vb0plwq8cn43f0vyahr0f86y7352z") (y #t)))

(define-public crate-semilattice-database-0.59.5 (c (n "semilattice-database") (v "0.59.5") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.71") (d #t) (k 0)))) (h "01ch9pk7j7f12gmymmp2mx7n79s993sg3jxmj2g2b9ykyjgvw305") (y #t)))

(define-public crate-semilattice-database-0.60.0 (c (n "semilattice-database") (v "0.60.0") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.71") (d #t) (k 0)))) (h "0hbg4rnw2a9iybkay9bh0rkqn334lwl0kz5jr8xngwilflsm53r7") (y #t)))

(define-public crate-semilattice-database-0.60.1 (c (n "semilattice-database") (v "0.60.1") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "1dhbqpbjw6yakjpby3yr5fb8abxgyd6bw9y3bs9v0f07ph84h703") (y #t)))

(define-public crate-semilattice-database-0.61.0 (c (n "semilattice-database") (v "0.61.0") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "0c3a99nab25w8z2nixa0znnag1kibda4439q44nkkvgv8il8xsla") (y #t)))

(define-public crate-semilattice-database-0.61.1 (c (n "semilattice-database") (v "0.61.1") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "1qgq7slw20gk6ysc47lxnrjw4nk4jg1mihck6yc8bd0h4cj3pc06") (y #t)))

(define-public crate-semilattice-database-0.61.2 (c (n "semilattice-database") (v "0.61.2") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "1i406b5ijqyrg3ad9rqpjjndhhcas7i20z55aamfk0yd06ag1gbw") (y #t)))

(define-public crate-semilattice-database-0.61.3 (c (n "semilattice-database") (v "0.61.3") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "146nrl3zva7lrxsq9xprwmnasa7q624ana2hmb137m03n4airj3y") (y #t)))

(define-public crate-semilattice-database-0.61.4 (c (n "semilattice-database") (v "0.61.4") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "0lkizbzzax5c5cn246bmfcvnn9353nq6pn47hqp6ikiazzx37qlf") (y #t)))

(define-public crate-semilattice-database-0.62.0 (c (n "semilattice-database") (v "0.62.0") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "1a7rmhgva6jbgy0x83vkdy5k9jpfi51s21qynkr6qkyvlhccx40r") (y #t)))

(define-public crate-semilattice-database-0.62.1 (c (n "semilattice-database") (v "0.62.1") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.72") (d #t) (k 0)))) (h "03b4fq4p48addy6n3ls3n2b1b0ds3mg849g1l03q1qs7px68b01j") (y #t)))

(define-public crate-semilattice-database-0.63.0 (c (n "semilattice-database") (v "0.63.0") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.73") (d #t) (k 0)))) (h "078xl4xp8i9bdqbv2kc8m77pycg9c9c07jdm8q7hdc5nq5gsvn50") (y #t)))

(define-public crate-semilattice-database-0.64.0 (c (n "semilattice-database") (v "0.64.0") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.74") (d #t) (k 0)))) (h "0rsj83zzllvmx8dgw0v2781chrl13k1rq5fgnz0hk56zkvi30j53") (y #t)))

(define-public crate-semilattice-database-0.64.1 (c (n "semilattice-database") (v "0.64.1") (d (list (d (n "binary_set") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.74") (d #t) (k 0)))) (h "0w6c60csc0ibkags6004sd3iwyi6qf03l3yj4zp1i3a61k4cs68k") (y #t)))

(define-public crate-semilattice-database-0.65.0 (c (n "semilattice-database") (v "0.65.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.75") (d #t) (k 0)))) (h "04rdw8vd3fg5nar3bxfghcdzyhx2amwfxw0m5ka19y0jn4pgf29p") (y #t)))

(define-public crate-semilattice-database-0.66.0 (c (n "semilattice-database") (v "0.66.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.76") (d #t) (k 0)))) (h "1bnpk3r0czbww6gq37i5jn3lxdkkc7yhww8cdpyr1js0428hj6as") (y #t)))

(define-public crate-semilattice-database-0.67.0 (c (n "semilattice-database") (v "0.67.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.77") (d #t) (k 0)))) (h "15f023ara5lh0ng6ni2b4w8dwpfbjknvpr5v6wf62hhvqamavw0q") (y #t)))

(define-public crate-semilattice-database-0.68.0 (c (n "semilattice-database") (v "0.68.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.78") (d #t) (k 0)))) (h "0llbgr6lp003f3fvlrzygynxl54g4xpml52vnnqzrh2ni9pm33d5") (y #t)))

(define-public crate-semilattice-database-0.69.0 (c (n "semilattice-database") (v "0.69.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.78") (d #t) (k 0)))) (h "0fir3jhj64rdy7f98ks82dsyrrgzrsfxxyjsqjisppvl3ix5yhj3") (y #t)))

(define-public crate-semilattice-database-0.70.0 (c (n "semilattice-database") (v "0.70.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "versatile-data") (r "^0.78") (d #t) (k 0)))) (h "1952b0l3arky0yp0ykr342h8vg11vzqqcbwa4hhvnabv1b3869q8") (y #t)))

(define-public crate-semilattice-database-0.71.0 (c (n "semilattice-database") (v "0.71.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.79") (d #t) (k 0)))) (h "0m9cqng5ii87xh0b0fj9hm5bv4210l5srz09r5qan32s3mwi514r") (y #t)))

(define-public crate-semilattice-database-0.71.1 (c (n "semilattice-database") (v "0.71.1") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.79") (d #t) (k 0)))) (h "0h5p7kmzq4n6bqz974zkvl6kjb9qlpxq9i59493b9vz7rc68zv1w") (y #t)))

(define-public crate-semilattice-database-0.72.0 (c (n "semilattice-database") (v "0.72.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.80") (d #t) (k 0)))) (h "1cnj0v0svhphjrrz0b2zm02bzzhn56v067rbyvxy9p9jbc7s6jjp") (y #t)))

(define-public crate-semilattice-database-0.73.0 (c (n "semilattice-database") (v "0.73.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.80") (d #t) (k 0)))) (h "0w2783z3kwc7alnnxipgdkhwl43nqlllywdhlqc4lfca30b8qjpa") (y #t)))

(define-public crate-semilattice-database-0.74.0 (c (n "semilattice-database") (v "0.74.0") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.81") (d #t) (k 0)))) (h "192q7cic383vq5r5vaym1sghrhd9wgaq9kncpvzsdmdszglwr5kg") (y #t)))

(define-public crate-semilattice-database-0.74.1 (c (n "semilattice-database") (v "0.74.1") (d (list (d (n "binary_set") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.82") (d #t) (k 0)))) (h "0i2dljja2n97fshyvxxyndsxkw153db0qd5q5201pgx8s3didz38") (y #t)))

(define-public crate-semilattice-database-0.75.0 (c (n "semilattice-database") (v "0.75.0") (d (list (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.83") (d #t) (k 0)))) (h "03an2ghr0h3dcyynb6cd4jkxnvhdnqkxw2x09y3axk402qsnyd0h")))

(define-public crate-semilattice-database-0.76.0 (c (n "semilattice-database") (v "0.76.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.84") (d #t) (k 0)))) (h "06q96zydrnizg76nzrxdn0mmz7zj1wmfii8wgk1yr93rvxclpvmh")))

(define-public crate-semilattice-database-0.76.1 (c (n "semilattice-database") (v "0.76.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.85") (d #t) (k 0)))) (h "1al3yrdgyaryjcxz8sl5b6mk2rimbyqn2cybzhrsc2zzww9z24mi")))

(define-public crate-semilattice-database-0.77.0 (c (n "semilattice-database") (v "0.77.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.86") (d #t) (k 0)))) (h "1ldmrdj4iwhqls1n9il381kg5ng3lz06h65v9hq01rvdz3zvg7rj")))

(define-public crate-semilattice-database-0.77.1 (c (n "semilattice-database") (v "0.77.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.86") (d #t) (k 0)))) (h "01b18vklr7gwyyh1dgjkj84jfwy9n51s2c7r53hpgq9bl2pzw7n1")))

(define-public crate-semilattice-database-0.77.2 (c (n "semilattice-database") (v "0.77.2") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.86") (d #t) (k 0)))) (h "0i04xbaqklhbgxw3y9h9jlfbx1jbhb2j2l9m71z5yh19g247laya")))

(define-public crate-semilattice-database-0.77.3 (c (n "semilattice-database") (v "0.77.3") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.86") (d #t) (k 0)))) (h "11glz5w17zgc13rnwmgn56h8z6diirgvbd15bnzg8x3bcjrnx186")))

(define-public crate-semilattice-database-0.77.4 (c (n "semilattice-database") (v "0.77.4") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.86") (d #t) (k 0)))) (h "08b7qj64dzza9xj3wc3f7ap8a8bf6jdrp4liwbkvm3drnqn1mgi1")))

(define-public crate-semilattice-database-0.77.5 (c (n "semilattice-database") (v "0.77.5") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.87") (d #t) (k 0)))) (h "1fsa8r0417pw230ly9nz9ndgas8vri50vbakww297laa2bdxv8mk")))

(define-public crate-semilattice-database-0.78.0 (c (n "semilattice-database") (v "0.78.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.88") (d #t) (k 0)))) (h "0c0c39yh4rikf289r2phg6mb2zcq8474iswdm5vhsanmmnnvf3nl")))

(define-public crate-semilattice-database-0.78.1 (c (n "semilattice-database") (v "0.78.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.88") (d #t) (k 0)))) (h "0k1xf01zjd3605qqjgkbb96mrwx479mr6dgxyx5p0xx16bh8qmqk")))

(define-public crate-semilattice-database-0.78.2 (c (n "semilattice-database") (v "0.78.2") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.88") (d #t) (k 0)))) (h "1ywj9qgdx19czik8h5y1nnk7a35m6mplx8h3bhjmaw523lbwq1ji")))

(define-public crate-semilattice-database-0.79.0 (c (n "semilattice-database") (v "0.79.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.89") (d #t) (k 0)))) (h "04cc2c0sgdc0vj14myg5m35czzhv2kbssdr6gxlwlp7r7zavi9a7")))

(define-public crate-semilattice-database-0.79.1 (c (n "semilattice-database") (v "0.79.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.89") (d #t) (k 0)))) (h "0m5c3pfmqjx844i821pqiid6c49varddm0pg85vvylx25bj4bxb4")))

(define-public crate-semilattice-database-0.79.2 (c (n "semilattice-database") (v "0.79.2") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.89") (d #t) (k 0)))) (h "0zfigf2i73xkpdkm55368xsd8pj415ff5rf6nzq08i55lnxqrpah")))

(define-public crate-semilattice-database-0.80.0 (c (n "semilattice-database") (v "0.80.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.90") (d #t) (k 0)))) (h "0hrx0vqhcd9p6kasjpyy729hhzcf16pjrx7i5cvrrhwyjiqqp0r2")))

(define-public crate-semilattice-database-0.81.0 (c (n "semilattice-database") (v "0.81.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.91") (d #t) (k 0)))) (h "0j8bqfmhd0ycn41jjrps251d052hd0y4ninkh6inxi0d1890h741")))

(define-public crate-semilattice-database-0.82.0 (c (n "semilattice-database") (v "0.82.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.92") (d #t) (k 0)))) (h "1j201g9cbx70xjfs1n500xf0x234wv8dvqbh9ja2k3plqkqszq8w")))

(define-public crate-semilattice-database-0.83.0 (c (n "semilattice-database") (v "0.83.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.92") (d #t) (k 0)))) (h "1pxkhsx9bl30hky20x6w41wgym311raish059awwdv3zlaxbsf9p")))

(define-public crate-semilattice-database-0.84.0 (c (n "semilattice-database") (v "0.84.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.92") (d #t) (k 0)))) (h "0qfwiy441w4pxib3lb936lw3i74cnmr5as3jbmrb8xa59x1rzsk9")))

(define-public crate-semilattice-database-0.85.0 (c (n "semilattice-database") (v "0.85.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "binary_set") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.93") (d #t) (k 0)))) (h "03jb5204w6k0m377p4rvsfjn9rhn415m4n20fbx86wpz8clm8fyh")))

(define-public crate-semilattice-database-0.86.0 (c (n "semilattice-database") (v "0.86.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.94") (d #t) (k 0)))) (h "1z8z73dcqw67dhpjzqa0bdvxdnjgn3yr51i0b7nxmcfksdjyks4h")))

(define-public crate-semilattice-database-0.87.0 (c (n "semilattice-database") (v "0.87.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.94") (d #t) (k 0)))) (h "0d620z09n4nlxgwavxjf5ixiqw4xirc9cxymzrxqsi27acxz2zqh")))

(define-public crate-semilattice-database-0.87.1 (c (n "semilattice-database") (v "0.87.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.95") (d #t) (k 0)))) (h "1cad3d5ik79a6bs7b7svlzhf5fkigqjhpiyfqp9988h401bd90hz")))

(define-public crate-semilattice-database-0.88.0 (c (n "semilattice-database") (v "0.88.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.96") (d #t) (k 0)))) (h "01j6jglq8lcr20a38jg2q92spdx7p359cdpz8ba85v2xhfnlkvg7")))

(define-public crate-semilattice-database-0.89.0 (c (n "semilattice-database") (v "0.89.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.97") (d #t) (k 0)))) (h "0lsairll9jp7prl7vnhxf2c8m13lix0ly8c93s1ghbfhwkpnxfpy")))

(define-public crate-semilattice-database-0.89.1 (c (n "semilattice-database") (v "0.89.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.98") (d #t) (k 0)))) (h "11a606kc6hxgz3f3cw976n5vyrcky7iwzsayndi3ab5zny8m6x91")))

(define-public crate-semilattice-database-0.90.0 (c (n "semilattice-database") (v "0.90.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.98") (d #t) (k 0)))) (h "0dmr6jml2zmi9fz4mmy8zwjjkbif4cc38zpiq9ypm7rlcd182n87")))

(define-public crate-semilattice-database-0.90.1 (c (n "semilattice-database") (v "0.90.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.98") (d #t) (k 0)))) (h "0c16xngazs56f6rgijg0nyp5g25nj2xsas4pmxknv1sjhcjdry0n") (y #t)))

(define-public crate-semilattice-database-0.90.2 (c (n "semilattice-database") (v "0.90.2") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.98") (d #t) (k 0)))) (h "10117mc858x9jwbpbl91k3klxgmz6gbx9mp8h387n9bkd6pza522") (y #t)))

(define-public crate-semilattice-database-0.91.0 (c (n "semilattice-database") (v "0.91.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.99") (d #t) (k 0)))) (h "1nwlfkj4pfbyl8z9ap2cd8vk4darkrvn14pq53h8j050b39xcm7b") (y #t)))

(define-public crate-semilattice-database-0.91.1 (c (n "semilattice-database") (v "0.91.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.100") (d #t) (k 0)))) (h "0fqyxmrrbqymmwgx89sx6zzn4n9gpsfmrz83c7n3bgfdl8lvkjlg") (y #t)))

(define-public crate-semilattice-database-0.92.0 (c (n "semilattice-database") (v "0.92.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.100") (d #t) (k 0)))) (h "07rx4b64k00whz8pkfbz440q7c1n263pyclmfw6j3g0kmrbn488r")))

(define-public crate-semilattice-database-0.92.1 (c (n "semilattice-database") (v "0.92.1") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.100") (d #t) (k 0)))) (h "0jygvd80j633v4zavmdwn2r9gvch37wkwqy8yfzqgq7ara448m0f")))

(define-public crate-semilattice-database-0.92.2 (c (n "semilattice-database") (v "0.92.2") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.100") (d #t) (k 0)))) (h "1hksngrh72lwrm4nrrsqd6ph1chjdgr98b8wlf4zmx93fnilf5wj")))

(define-public crate-semilattice-database-0.92.3 (c (n "semilattice-database") (v "0.92.3") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "1lzl4ws8xffvp8myrbxnm1m4f9lr061im7mv5fb3h03vsq0s4saa")))

(define-public crate-semilattice-database-0.92.4 (c (n "semilattice-database") (v "0.92.4") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "09hqwfdm9w838c8c5pfxk4mrsm6frwn0wd1gnjp20kbx3qyayc7i")))

(define-public crate-semilattice-database-0.92.5 (c (n "semilattice-database") (v "0.92.5") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "1p8an8fgjp2l4hh4lrlcs9j5vpqvdycgm0by91jn8pykcg045m4l")))

(define-public crate-semilattice-database-0.92.6 (c (n "semilattice-database") (v "0.92.6") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "1clla1gv5lwsz5xb9zpygkdyx4cbck5vk42c4x4cr5a4lk9kydff")))

(define-public crate-semilattice-database-0.92.7 (c (n "semilattice-database") (v "0.92.7") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "10cas7ijl738kj4ipcxyw35mnq38xw58c2c6gpnl72chzhi72b1x")))

(define-public crate-semilattice-database-0.93.0 (c (n "semilattice-database") (v "0.93.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "0823snzvy6yvzmqfs5iknp7bsyhgfck134kp4xl3vfkjrx14d9ys")))

(define-public crate-semilattice-database-0.93.1 (c (n "semilattice-database") (v "0.93.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.101") (d #t) (k 0)))) (h "0r59arsf1wkvirczxa9ffn4b58s4sx7zl34daiz3lpaczysxp1hv")))

(define-public crate-semilattice-database-0.94.0 (c (n "semilattice-database") (v "0.94.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.102.0") (d #t) (k 0)))) (h "0qf6cknlyjs7jf1f3qnkyd90xzpk0dnjfw4nxpbi0wsamf8xqlch")))

(define-public crate-semilattice-database-0.94.1 (c (n "semilattice-database") (v "0.94.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.102.0") (d #t) (k 0)))) (h "1c787whgys1ig0rkrv7ab36x83h4642g372d3mp1y27mn5abrxjc")))

(define-public crate-semilattice-database-0.95.0 (c (n "semilattice-database") (v "0.95.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.102.0") (d #t) (k 0)))) (h "1xbfj1d6iq7d3p16vplqmlf4m2n5r5lc4k690x3dr1dh77dfsrhx")))

(define-public crate-semilattice-database-0.96.0 (c (n "semilattice-database") (v "0.96.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.102.0") (d #t) (k 0)))) (h "1g42268i887j9l6j329sl132kf3drkr8vwd3rvz35sfzgj1p13wb")))

(define-public crate-semilattice-database-0.96.1 (c (n "semilattice-database") (v "0.96.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.102.0") (d #t) (k 0)))) (h "0np09k1bdzw6rb3050lga4iic2z9kh0nn5y30vpnw0m6p9lrdrzq")))

(define-public crate-semilattice-database-0.97.0 (c (n "semilattice-database") (v "0.97.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.9.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.103.0") (d #t) (k 0)))) (h "07adn65fmf77gmxmg4b0w9y22m43fxpaqw2hm4bk7hv72p875qg6")))

(define-public crate-semilattice-database-0.97.1 (c (n "semilattice-database") (v "0.97.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.103.0") (d #t) (k 0)))) (h "1m97y4vxfayir88syf69gb0c6ifhal8jhxyql8d4x2s0mfzpn0k6")))

(define-public crate-semilattice-database-0.98.0 (c (n "semilattice-database") (v "0.98.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.104.0") (d #t) (k 0)))) (h "172ca2jrjys1spk8c3fd4zy2ldylbymp2n3dqr61vjlz6h7hk116")))

(define-public crate-semilattice-database-0.99.0 (c (n "semilattice-database") (v "0.99.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.10.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.105.0") (d #t) (k 0)))) (h "0zq9qnibyly9sibnz11xv29lwqr1aqvwhbrihd35fh491d6c4phr")))

(define-public crate-semilattice-database-0.100.0 (c (n "semilattice-database") (v "0.100.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.10.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.105.1") (d #t) (k 0)))) (h "121f27vilyz3c86jq7v42givfwrw87wpafllnihavd1gfpcrhhsr")))

(define-public crate-semilattice-database-0.100.1 (c (n "semilattice-database") (v "0.100.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.106.0") (d #t) (k 0)))) (h "1c45c148y3k74ajib70vsy89gpv9qsp42x0j70pf2rkvifzlr4sg")))

(define-public crate-semilattice-database-0.100.2 (c (n "semilattice-database") (v "0.100.2") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.106.0") (d #t) (k 0)))) (h "1xv3x3d0h248d476j9r624xx4iii2zb292b1h1sknd58g8r7a68g")))

(define-public crate-semilattice-database-0.101.0 (c (n "semilattice-database") (v "0.101.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.107.0") (d #t) (k 0)))) (h "09hlmq4k8rckvhnf9bq44qp6xc75fabdwha3l3f23irncqqbv60l")))

(define-public crate-semilattice-database-0.102.0 (c (n "semilattice-database") (v "0.102.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.108.0") (d #t) (k 0)))) (h "1kj6jkbiv8k3sr56aya0bg7180c2pxrl78i4vcaqwpqf8c3p6h9i")))

(define-public crate-semilattice-database-0.103.0 (c (n "semilattice-database") (v "0.103.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.109.0") (d #t) (k 0)))) (h "0aq1dl6qb3vn8vjij8gnzglpi4lmm8lxs3j44q91hyf7wsd8yrwd")))

(define-public crate-semilattice-database-0.104.0 (c (n "semilattice-database") (v "0.104.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.110.0") (d #t) (k 0)))) (h "1mnhzklhzbxpx505h8027b7w4b963wvx8dmwgdiv6gb4kxnph921")))

(define-public crate-semilattice-database-0.105.0 (c (n "semilattice-database") (v "0.105.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.111.0") (d #t) (k 0)))) (h "1w6v7bnj78wfa2nygyvhhc8jvdrfbpgxdzr918pc4nznflr6n3fq")))

(define-public crate-semilattice-database-0.105.1 (c (n "semilattice-database") (v "0.105.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.12.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.111.0") (d #t) (k 0)))) (h "1bs5ip9qv5cvbg4zypji0drh26s8f9jqxjv28p08vg2hh1bnav86")))

(define-public crate-semilattice-database-0.106.0 (c (n "semilattice-database") (v "0.106.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.112.0") (d #t) (k 0)))) (h "1qhhk0z0w4xp1kws5l0zfmx9g7fn6rbr5asgr4nhlxv2jz3bj6lf")))

(define-public crate-semilattice-database-0.107.0 (c (n "semilattice-database") (v "0.107.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "binary_set") (r "^0.13.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.113.0") (d #t) (k 0)))) (h "0bx4rrxmc74zqb7xfaz4w5r4627w9slhi3w7zqi5ib35a8wl297s")))

(define-public crate-semilattice-database-0.108.0 (c (n "semilattice-database") (v "0.108.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.114.0") (d #t) (k 0)))) (h "1ifjg9zqicas85x0h5zzbqchq0j2ngrlc222cm238c1vzjc9rk9r")))

(define-public crate-semilattice-database-0.109.0 (c (n "semilattice-database") (v "0.109.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.115.0") (d #t) (k 0)))) (h "1yyc180fz9459qwxx7ifwjw9k2j5vd3mnkn0gkv9m6bpcz2nf7qw")))

(define-public crate-semilattice-database-0.110.0 (c (n "semilattice-database") (v "0.110.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "versatile-data") (r "^0.116.0") (d #t) (k 0)))) (h "0g1z8nlyq17ljv7n6mk571vbpk52pybpl2i6h7w87qqwq7yindj3")))

