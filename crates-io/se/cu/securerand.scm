(define-module (crates-io se cu securerand) #:use-module (crates-io))

(define-public crate-securerand-0.1.0 (c (n "securerand") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "1qg1z70cl2k4mwziqcrahxrwwnymv5wla5sqk3xzxa6kpy4g7ngq")))

(define-public crate-securerand-0.1.1 (c (n "securerand") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "1bz0d1lrsrwm1kqc5ccpwmk1gg6h2p6c0kca0818mlfvn37h8vp0")))

(define-public crate-securerand-0.1.2 (c (n "securerand") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)))) (h "072kh8hfhfz0yjcji1z6b00kzmxz4zhr2jzix5i74sj94xrjwr6h")))

