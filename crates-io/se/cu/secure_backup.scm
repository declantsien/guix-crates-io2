(define-module (crates-io se cu secure_backup) #:use-module (crates-io))

(define-public crate-secure_backup-0.1.0 (c (n "secure_backup") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ir36v7qkf3an1hzb0wcd2988alcri9wmsrb3rqs5yd8x3hjz1kg")))

