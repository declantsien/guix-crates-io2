(define-module (crates-io se cu secure-survey) #:use-module (crates-io))

(define-public crate-secure-survey-0.1.0 (c (n "secure-survey") (v "0.1.0") (d (list (d (n "curv-kzen") (r "^0.9.0") (f (quote ("num-bigint"))) (k 0)) (d (n "paillier") (r "^0.4.2") (k 0) (p "kzen-paillier")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yqy1cfh0s56qphsr2jy5vwpmmx6vsqaa8l7kwfba263l38wbf96")))

(define-public crate-secure-survey-0.1.1 (c (n "secure-survey") (v "0.1.1") (d (list (d (n "curv-kzen") (r "^0.9.0") (f (quote ("num-bigint"))) (k 0)) (d (n "paillier") (r "^0.4.2") (k 0) (p "kzen-paillier")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r5pgwvl2hizpwhfyhgx4wzc74ab7sw0yv8bwfg2r0wvgc2sz51v")))

(define-public crate-secure-survey-0.1.2 (c (n "secure-survey") (v "0.1.2") (d (list (d (n "curv-kzen") (r "^0.9.0") (f (quote ("num-bigint"))) (k 0)) (d (n "paillier") (r "^0.4.2") (k 0) (p "kzen-paillier")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04g5pf4qankjc7hbg8kvnyvcv519sc166q6l0xi2g6k8zm3irdh7")))

