(define-module (crates-io se cu secular) #:use-module (crates-io))

(define-public crate-secular-0.1.0 (c (n "secular") (v "0.1.0") (d (list (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0lw2qb5kbn6lhhz3a50bkf44v1v8galwblj53sh8dnbk56hf4dfl") (f (quote (("default" "ascii") ("bmp") ("ascii"))))))

(define-public crate-secular-0.1.1 (c (n "secular") (v "0.1.1") (d (list (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "07qj7caqcsxid8c70gn5x5ayxh3zdvsfw2a4w6i6lb0nlkbp6v1g") (f (quote (("default" "ascii") ("bmp") ("ascii"))))))

(define-public crate-secular-0.2.0 (c (n "secular") (v "0.2.0") (d (list (d (n "unicode-normalization") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ca11l9iyb99kh8mlr5091kbx4nakd9wag9122q150pd4kvs0rv7") (f (quote (("normalization" "unicode-normalization") ("default" "ascii") ("bmp") ("ascii"))))))

(define-public crate-secular-0.3.0 (c (n "secular") (v "0.3.0") (d (list (d (n "unicode-normalization") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0j0n07rwdi9crdbvmfhif3sw7y8nmb0fhm2dk2xlq2w1v6kcpl0d") (f (quote (("normalization" "unicode-normalization") ("default") ("bmp"))))))

(define-public crate-secular-1.0.0 (c (n "secular") (v "1.0.0") (d (list (d (n "unicode-normalization") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01cr44pzj1zpxajrls702l177irdarn0bp8fz6x399iw896avjd4") (f (quote (("normalization" "unicode-normalization") ("default") ("bmp"))))))

(define-public crate-secular-1.0.1 (c (n "secular") (v "1.0.1") (d (list (d (n "unicode-normalization") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1davw8k29sycm7f4674d4m44jfa7pn812jm3m3mm76srvz63xp63") (f (quote (("normalization" "unicode-normalization") ("default") ("bmp"))))))

