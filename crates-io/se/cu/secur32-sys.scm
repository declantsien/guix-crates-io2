(define-module (crates-io se cu secur32-sys) #:use-module (crates-io))

(define-public crate-secur32-sys-0.0.1 (c (n "secur32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0gnmwsaxbg7ki0afvqwnkqkg93dpr281959j8v4gm8w18zan5v8j")))

(define-public crate-secur32-sys-0.2.0 (c (n "secur32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1g0nsbw5p1awghl8rhknb53870nsdw6w2nfx04qqk39hhgx2sh9z")))

