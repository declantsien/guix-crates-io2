(define-module (crates-io se cu secure-password) #:use-module (crates-io))

(define-public crate-secure-password-0.1.0 (c (n "secure-password") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.3.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "1pwgnc1n91hl4pbfmzw5xbj8dfpnx585i3s67a2fnsn53aa1jlqa")))

(define-public crate-secure-password-0.2.0 (c (n "secure-password") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.3.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "114w48ckln01cwxanwq93qaz3fv62dbnciqrg8xf4hrwxg8zyhg6")))

(define-public crate-secure-password-0.3.0 (c (n "secure-password") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.3.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "00zkk78by2dc3sjm206jsjiyfw7xmn0m6kbirnnzcbixz19wm0rw")))

(define-public crate-secure-password-0.3.1 (c (n "secure-password") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.3.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "06x83gfdrca6vgll3bnhmi3lijl5r4xxyd1hl1kr9jpqdmpb39z9")))

