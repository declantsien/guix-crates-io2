(define-module (crates-io se cu secured-cipher) #:use-module (crates-io))

(define-public crate-secured-cipher-0.1.0 (c (n "secured-cipher") (v "0.1.0") (d (list (d (n "rayon") (r "~1.8.0") (d #t) (k 0) (p "rayon")) (d (n "secured-cipher-key") (r "^0.1.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "1s16502vip9i0rpz0hkvqvmcl0bxjqkgmxxn3r14g2gf8anj8cbw")))

(define-public crate-secured-cipher-0.2.0 (c (n "secured-cipher") (v "0.2.0") (d (list (d (n "secured-cipher-key") (r "^0.1.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "0ray03kwrm871wh7p0jdalh4yxb99lwjrcy2vfkl6wfffd21d6w5")))

(define-public crate-secured-cipher-0.3.0 (c (n "secured-cipher") (v "0.3.0") (d (list (d (n "secured-cipher-key") (r "^0.1.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "1g3bdca8q6lwmcva1xfmacsrrq3ypji8qadjcfxzj0icbb8sx4bd")))

(define-public crate-secured-cipher-0.3.1 (c (n "secured-cipher") (v "0.3.1") (d (list (d (n "secured-cipher-key") (r "^0.2.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "1z9i33v3zqs1g9b1hm7glkxfk1xkzllvd6m8i7yn1rvyp7dwp20s")))

(define-public crate-secured-cipher-0.4.0 (c (n "secured-cipher") (v "0.4.0") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "secured-cipher-key") (r "^0.2.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "0mix4kbs8amqd90kh1r529k9vg2n6djqcg8vr2ks1msj9lh250wp")))

(define-public crate-secured-cipher-0.4.1 (c (n "secured-cipher") (v "0.4.1") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "secured-cipher-key") (r "^0.3.0") (d #t) (k 0) (p "secured-cipher-key")))) (h "1by0l214mfbdarik6dn4w77ckkvr0wnd4d5ssjjpzw7byph66fh4")))

