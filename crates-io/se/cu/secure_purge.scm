(define-module (crates-io se cu secure_purge) #:use-module (crates-io))

(define-public crate-secure_purge-0.1.0 (c (n "secure_purge") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fqw5cny6z7hmkmq1lr643g8mpr95p9ccfdixc8grgjplfwn510l")))

