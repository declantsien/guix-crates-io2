(define-module (crates-io se cu securitytrails-rs) #:use-module (crates-io))

(define-public crate-securitytrails-rs-0.0.1 (c (n "securitytrails-rs") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03a820lma1c87wdd86l6cbd0lgv4zfy4l3s5c6wv6nja70z6lh9y")))

(define-public crate-securitytrails-rs-0.0.2 (c (n "securitytrails-rs") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07cyy3n694jqwiddnasbay1c7hq5zyxfd8i1vjyyp4y2xsfa034p")))

(define-public crate-securitytrails-rs-0.1.0 (c (n "securitytrails-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1giag0rzj4q5gpg3zsh0vl0zajb46fwn98v56xcc70r7cgar6r1j")))

