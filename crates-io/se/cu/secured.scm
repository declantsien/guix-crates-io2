(define-module (crates-io se cu secured) #:use-module (crates-io))

(define-public crate-secured-0.1.0 (c (n "secured") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.1.0") (d #t) (k 0) (p "secured-enclave")) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "139f1gvk16k87ksrp1h9sid4d0zwvd4ba5sxr84h49vxm315d8fq")))

(define-public crate-secured-0.1.1 (c (n "secured") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.1.0") (d #t) (k 0) (p "secured-enclave")) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "1dg6a337ah2agbkq4kw2zdhbr1h9bvhpzm6dn5vhdixsiyk4pgxh")))

(define-public crate-secured-0.1.2 (c (n "secured") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.1.0") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "0cpj6k3lkzza6g7qlhvl98clak44ph584w9gr0n13mnwmd1k5q4n")))

(define-public crate-secured-0.2.0 (c (n "secured") (v "0.2.0") (d (list (d (n "cipher") (r "^0.1.0") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.2.0") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "1w965k6cxzmg7v11sr30xq86p669pmggd8bxq8ivlhiccvva152p")))

(define-public crate-secured-0.3.0 (c (n "secured") (v "0.3.0") (d (list (d (n "cipher") (r "^0.2.0") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.3.0") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "1859f83xilmw730jlxwfwdd92mylc82jfn3lssrhyln1g8ln0bz4")))

(define-public crate-secured-0.4.0 (c (n "secured") (v "0.4.0") (d (list (d (n "cipher") (r "^0.3.0") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.4.0") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "0dj3m2v3anpqc666s3yvcp3r4ffvl34m13k1d6pm2k0wfllp5chb")))

(define-public crate-secured-0.5.0 (c (n "secured") (v "0.5.0") (d (list (d (n "cipher") (r "^0.3.1") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.5.0") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "1a20ffqbhb4sw1vx7cqpa4zzd0zsj24ckypslwywgpkangkx0jwx")))

(define-public crate-secured-0.5.1 (c (n "secured") (v "0.5.1") (d (list (d (n "cipher") (r "^0.4.0") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.5.1") (d #t) (k 0) (p "secured-enclave")) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "03l6vpjmrc1h5lbr9mw7y0yxrr1261pvy5s7ha07mkcsgg8s58iw")))

(define-public crate-secured-0.6.0 (c (n "secured") (v "0.6.0") (d (list (d (n "cipher") (r "^0.4.1") (d #t) (k 0) (p "secured-cipher")) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enclave") (r "^0.6.0") (d #t) (k 0) (p "secured-enclave")) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "0cgk2qmx13779rcdbf36x0i8v9ch9rxikwl5zgfnk6g3axnk1ia3")))

