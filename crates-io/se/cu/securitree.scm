(define-module (crates-io se cu securitree) #:use-module (crates-io))

(define-public crate-securitree-0.0.1 (c (n "securitree") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hqdqzqw8hsmdl5ixcahll6x0xdzy9h7h72gv938xw4vyvafbvzw") (r "1.67")))

