(define-module (crates-io se cu secure-string) #:use-module (crates-io))

(define-public crate-secure-string-0.1.0 (c (n "secure-string") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "pre") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "pre") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "zeroize") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)))) (h "06l0zgiz2as73qpzy14igrg38cqmb6gj15576x14i5zgjyfv9xz6")))

(define-public crate-secure-string-0.2.0 (c (n "secure-string") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "pre") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "pre") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "zeroize") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)))) (h "08rr95rngyk0wjg11fqg3rw0fbfqhryc99b40j1gqm2zgvakmid7")))

(define-public crate-secure-string-0.3.0 (c (n "secure-string") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "pre") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "zeroize") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)))) (h "123lmla57m3f94mlywlh838gc87yffnyiqadlsrpn7v3zz4si2sl")))

