(define-module (crates-io se cu secugen_rs) #:use-module (crates-io))

(define-public crate-secugen_rs-0.1.0 (c (n "secugen_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1vq634nmpj71ggy17j55waqkfzky5i1v2g06hqrfsw73hkyv5ixv")))

(define-public crate-secugen_rs-0.2.0 (c (n "secugen_rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1avl6445zqailfb1g7v434zypgsq1ij3apkhml8wq3fad8nnsdsr")))

