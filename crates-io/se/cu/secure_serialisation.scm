(define-module (crates-io se cu secure_serialisation) #:use-module (crates-io))

(define-public crate-secure_serialisation-0.1.0 (c (n "secure_serialisation") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.37") (o #t) (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.13") (d #t) (k 2)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)))) (h "1sjygi76y2b636i0zxjl0z027lrwyvbd4khznyajwzab986wvynb")))

(define-public crate-secure_serialisation-0.2.0 (c (n "secure_serialisation") (v "0.2.0") (d (list (d (n "clippy") (r "~0.0.41") (o #t) (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.1.5") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)))) (h "0h39khbcgmxvr0j6yd97q720v1kgn7vfihkd6hv89mfznvmq0l9p")))

(define-public crate-secure_serialisation-0.3.0 (c (n "secure_serialisation") (v "0.3.0") (d (list (d (n "clippy") (r "~0.0.45") (o #t) (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.9") (d #t) (k 0)))) (h "0nl6lsifbim1qxn67g7zgxxmlgpz15c0n9rb9h6wd18jzjpk9nh1")))

(define-public crate-secure_serialisation-0.4.0 (c (n "secure_serialisation") (v "0.4.0") (d (list (d (n "maidsafe_utilities") (r "~0.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.15") (d #t) (k 2)) (d (n "rust_sodium") (r "~0.5.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.11") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)))) (h "0sxvkzr6fmb8xp6ryqpsmxf7yxgsq5fssc0g5r7ih58gzm3330q3")))

(define-public crate-secure_serialisation-0.5.0 (c (n "secure_serialisation") (v "0.5.0") (d (list (d (n "maidsafe_utilities") (r "~0.14.0") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.1") (d #t) (k 0)) (d (n "rand") (r "~0.3.15") (d #t) (k 2)) (d (n "rust_sodium") (r "~0.7.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.11") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)))) (h "02zsxbvfjx8g6sb4zzp0paq2q29zwcz17dlzy87z8mmdd0m8qm1k")))

