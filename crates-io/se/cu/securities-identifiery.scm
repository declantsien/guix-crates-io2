(define-module (crates-io se cu securities-identifiery) #:use-module (crates-io))

(define-public crate-securities-identifiery-0.1.0 (c (n "securities-identifiery") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1z71p7ilzlw11r8rxg0g0nl3mk6hahgm289948bb6sz6lps3lz0w")))

