(define-module (crates-io se cu secure-store) #:use-module (crates-io))

(define-public crate-secure-store-1.0.0 (c (n "secure-store") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19zn79yi2c58pkca08pnlqq8v3hrrpidcxysp0zq9218d9b4r26c")))

