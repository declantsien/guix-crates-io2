(define-module (crates-io se cu secure_layer) #:use-module (crates-io))

(define-public crate-secure_layer-0.1.0 (c (n "secure_layer") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0jrw87zd5sajlcw2nzj72aszsbbb8ffnpk1j2xxdkh6i78rpm75j")))

