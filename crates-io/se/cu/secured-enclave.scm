(define-module (crates-io se cu secured-enclave) #:use-module (crates-io))

(define-public crate-secured-enclave-0.1.0 (c (n "secured-enclave") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "~0.9.0") (d #t) (k 0)) (d (n "hmac") (r "~0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "~0.12.2") (d #t) (k 0)) (d (n "rand_core") (r "~0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha2") (r "~0.10.5") (d #t) (k 0)))) (h "00sy7jfa2c93bjbgfwbnknj0hkmiwqz51dkaqbpcfyqxlk04kmwh")))

(define-public crate-secured-enclave-0.2.0 (c (n "secured-enclave") (v "0.2.0") (d (list (d (n "secured-cipher") (r "^0.1.0") (d #t) (k 0)))) (h "1p8g6cknjxpvv56m540qclxr5gddqf18ag1jlkpjfvlrzm60z7sq")))

(define-public crate-secured-enclave-0.3.0 (c (n "secured-enclave") (v "0.3.0") (d (list (d (n "secured-cipher") (r "^0.2.0") (d #t) (k 0)))) (h "158kvf90xvydcxkknwmxc7qaif4pg7z2cnk1f6dr0s3k0sv6a5n0")))

(define-public crate-secured-enclave-0.4.0 (c (n "secured-enclave") (v "0.4.0") (d (list (d (n "secured-cipher") (r "^0.3.0") (d #t) (k 0)))) (h "1yxzf6v1v74cr3lfy5ir3my9l7725q9d7j5ya1l13m3scxw0aand")))

(define-public crate-secured-enclave-0.5.0 (c (n "secured-enclave") (v "0.5.0") (d (list (d (n "secured-cipher") (r "^0.3.1") (d #t) (k 0)))) (h "1cvgpr8aw0z9ihmapgl4j4y1ci23m628yiif6219djdm31sr09if")))

(define-public crate-secured-enclave-0.5.1 (c (n "secured-enclave") (v "0.5.1") (d (list (d (n "secured-cipher") (r "^0.4.0") (d #t) (k 0)))) (h "0wp6pk2vyaz23zcd8d31g5n2kn06n8vfdlpg190ka2cbikwgc3pb")))

(define-public crate-secured-enclave-0.6.0 (c (n "secured-enclave") (v "0.6.0") (d (list (d (n "secured-cipher") (r "^0.4.1") (d #t) (k 0)))) (h "0grcnly6qq9x0mihvpwd9s2r3sq3lgwwgaqzara91d4w51ylb0ly")))

