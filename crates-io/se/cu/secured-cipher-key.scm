(define-module (crates-io se cu secured-cipher-key) #:use-module (crates-io))

(define-public crate-secured-cipher-key-0.1.0 (c (n "secured-cipher-key") (v "0.1.0") (d (list (d (n "hmac") (r "~0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "~0.12.2") (d #t) (k 0)) (d (n "rand_core") (r "~0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha2") (r "~0.10.5") (d #t) (k 0)))) (h "1fa7db9k49nsh3jyiwfi1q79lf9a4q3731hxsmbwinm33p5swb3h")))

(define-public crate-secured-cipher-key-0.2.0 (c (n "secured-cipher-key") (v "0.2.0") (d (list (d (n "hmac") (r "~0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "~0.12.2") (d #t) (k 0)) (d (n "rand_core") (r "~0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha2") (r "~0.10.5") (d #t) (k 0)))) (h "0i7g70nnzwifxsa3fl5kshs7cchs5zwp0g8nppnzf52r7r07fm6b")))

(define-public crate-secured-cipher-key-0.3.0 (c (n "secured-cipher-key") (v "0.3.0") (d (list (d (n "hmac") (r "~0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "~0.12.2") (d #t) (k 0)) (d (n "rand_core") (r "~0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha2") (r "~0.10.5") (d #t) (k 0)))) (h "00dlcnjinrii9y5zwhf7l7siq6m4yrsh1v061g40s28g8wd3n3pi")))

