(define-module (crates-io se cu secured_linked_list) #:use-module (crates-io))

(define-public crate-secured_linked_list-0.1.1 (c (n "secured_linked_list") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "~0.4.0") (d #t) (k 0) (p "threshold_crypto")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "17n4idpdjhd92iqdsrp61051jd837n8bsk86akx108zpszvm2wck")))

(define-public crate-secured_linked_list-0.1.2 (c (n "secured_linked_list") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "~0.4.0") (d #t) (k 0) (p "threshold_crypto")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "17bmv61aq9sxpi7grpfbbl7gqvc1blj3w6gaszi3ims62nzrqnbi")))

(define-public crate-secured_linked_list-0.1.3 (c (n "secured_linked_list") (v "0.1.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^1.0.1") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1r2k6182fm8qgwyd3bhxqapa6agvmlcksd7n0qybwrp1946j3h24") (y #t)))

(define-public crate-secured_linked_list-0.2.0 (c (n "secured_linked_list") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^1.0.1") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1rqrc20drfq0jl5fxxndr5y6x3ln3x0i8bmldyp5i99c7ibj5387")))

(define-public crate-secured_linked_list-0.3.0 (c (n "secured_linked_list") (v "0.3.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^2.0.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1g7jkh92dipnn132v0pw4jlafjmnqd5f99y5rf4inr52wfqsv50i")))

(define-public crate-secured_linked_list-0.3.1 (c (n "secured_linked_list") (v "0.3.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^2.0.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0j7f3yawq2inf461l9jhh3hdvka44spyqq4syxy314vxycgkxw8z")))

(define-public crate-secured_linked_list-0.4.0 (c (n "secured_linked_list") (v "0.4.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^3.4.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "19vi6hxslky72qp69csyigpriac5y4f25bj0p83lzlmh0364s78g")))

(define-public crate-secured_linked_list-0.5.0 (c (n "secured_linked_list") (v "0.5.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^3.4.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "137md20x8ciklgfrm1ah56bp9y49hsgjzcjh5pxx9x4wp5zjkb16")))

(define-public crate-secured_linked_list-0.5.1 (c (n "secured_linked_list") (v "0.5.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^5.2.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0c9wz9xb5zk5jhlf3344rarmficmpsmsfwwz6j6wvw49q6g7wgqp")))

(define-public crate-secured_linked_list-0.5.2 (c (n "secured_linked_list") (v "0.5.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^6.0.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02bm8jicwcp20ndfrqdys5ndjkh64qfypjdy185lm8g0vv6n3q0r")))

(define-public crate-secured_linked_list-0.5.3 (c (n "secured_linked_list") (v "0.5.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^7.0.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "08kyq863ils5glzmfx4cwxgcvcm3hyy73yv88bjx3nysc15dsp8r")))

(define-public crate-secured_linked_list-0.5.4 (c (n "secured_linked_list") (v "0.5.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bls") (r "^7.0.0") (d #t) (k 0) (p "blsttc")) (d (n "itertools") (r "~0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0dfd6wrv4sj39c3hi1i596gcza1fqmy8nkg4y17hi1rjr5a7njiz")))

