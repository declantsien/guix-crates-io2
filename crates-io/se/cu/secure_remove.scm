(define-module (crates-io se cu secure_remove) #:use-module (crates-io))

(define-public crate-secure_remove-2.2.0 (c (n "secure_remove") (v "2.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1ggzzpiwj25454ak59sg5q96amr210gijfm9g7wad1f6vnk0apwm") (y #t)))

(define-public crate-secure_remove-2.2.1 (c (n "secure_remove") (v "2.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "06fqvk1vbnnbf410x6kdigfya70kwrfi2cqmfcivczn2914xlzj2")))

(define-public crate-secure_remove-2.2.2 (c (n "secure_remove") (v "2.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1xb7xb4wmz05rdw0vm9i77i0zswh8jq6kz0k9wsw26qb501h26zr")))

(define-public crate-secure_remove-2.2.3 (c (n "secure_remove") (v "2.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1nvb0y0gixl52mr92mmbs0fglb1nxqgag7jfi4b6qj9q543hsk7q")))

