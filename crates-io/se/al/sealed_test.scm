(define-module (crates-io se al sealed_test) #:use-module (crates-io))

(define-public crate-sealed_test-0.1.0 (c (n "sealed_test") (v "0.1.0") (d (list (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 2)) (d (n "rusty-forkfork") (r "^0.3.0") (d #t) (k 0)) (d (n "sealed_test_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0klbp28qyg190p5b28rslmrsg0hylrmfrwl7jp5120sbqw1zhj19")))

(define-public crate-sealed_test-0.2.0 (c (n "sealed_test") (v "0.2.0") (d (list (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 2)) (d (n "rusty-forkfork") (r "^0.4.0") (d #t) (k 0)) (d (n "sealed_test_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "08dcz8bnasma5p3xc5kb8ms10k3h0arj7z89c7ggyxpgfrcm9jjq")))

(define-public crate-sealed_test-1.0.0 (c (n "sealed_test") (v "1.0.0") (d (list (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "rusty-forkfork") (r "^0.4.0") (d #t) (k 0)) (d (n "sealed_test_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wh29h2b1fyv9ljg14h1ccva4ivdx0mdn0mi0gi7zh8wcja8sq0s")))

