(define-module (crates-io se al sealed_test_derive) #:use-module (crates-io))

(define-public crate-sealed_test_derive-0.1.0 (c (n "sealed_test_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxl8gadb1znkwah62cy17z5var1bks8xi1abv3xwzwmyjiinzb3")))

(define-public crate-sealed_test_derive-1.0.0 (c (n "sealed_test_derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15z62n57l9cn7hdkfjhgi5hv0i2v3jghpn8rlrfyz3z5b802wrvv")))

