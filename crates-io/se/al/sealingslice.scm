(define-module (crates-io se al sealingslice) #:use-module (crates-io))

(define-public crate-sealingslice-0.1.0 (c (n "sealingslice") (v "0.1.0") (h "1kjly9c9igb9ry1g2qvqn6lb6s1kihwryhwc9xvrmckhhyidyz52")))

(define-public crate-sealingslice-0.1.1 (c (n "sealingslice") (v "0.1.1") (h "1ks1rsaac4f23dbynygg4cjimzqsa332ihgpjkg5xqm681jc18vi")))

(define-public crate-sealingslice-0.2.0 (c (n "sealingslice") (v "0.2.0") (h "1r00kfcl6a2i2sr3imiqqpf0kswd1hvp17ag43a4gc3j4fjqvb87")))

