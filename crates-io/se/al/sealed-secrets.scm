(define-module (crates-io se al sealed-secrets) #:use-module (crates-io))

(define-public crate-sealed-secrets-0.0.1 (c (n "sealed-secrets") (v "0.0.1") (d (list (d (n "securestore") (r "^0.100.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ip85ak806cm9m1qwaq7hmf911dgkrca473b3s9dkq87ylnxwwli")))

(define-public crate-sealed-secrets-0.1.0 (c (n "sealed-secrets") (v "0.1.0") (d (list (d (n "securestore") (r "^0.100.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ak4kr1q36ki9lf75sckcaqv5vgkrvggm1ywavm0pzh28vf03jhq")))

(define-public crate-sealed-secrets-0.2.0 (c (n "sealed-secrets") (v "0.2.0") (d (list (d (n "securestore") (r "^0.100.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0g896s77s1rgigby1lm9zsjc9i9g8sq092jmdp49jfv4355mxkd9")))

(define-public crate-sealed-secrets-0.2.1 (c (n "sealed-secrets") (v "0.2.1") (d (list (d (n "securestore") (r "^0.100.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0w4yzdh31vzh33xy24hca4pqpcmhm1yriclpv00dx32hwxvmzlrw")))

