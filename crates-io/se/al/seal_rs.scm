(define-module (crates-io se al seal_rs) #:use-module (crates-io))

(define-public crate-seal_rs-0.1.0 (c (n "seal_rs") (v "0.1.0") (d (list (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)))) (h "1bslisq7ls94j4psi7zvgrhz3dn8bp56fhcryq9wf4hs8z41jx5p")))

(define-public crate-seal_rs-0.1.1 (c (n "seal_rs") (v "0.1.1") (d (list (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)))) (h "00ifk8di1ci8f1hialagyl8yw8am45qg1q88sl3h414cqaal8g5f")))

(define-public crate-seal_rs-0.2.0 (c (n "seal_rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "1zi4sbcc3ljyyhyz3bdqfz9hkb0m6c1rkyqih70s6jcpq449cvhm")))

(define-public crate-seal_rs-0.3.0 (c (n "seal_rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "1n3dkc2yqwbzdmwa2x9pmqv18i626frwl056l5d5zvb8cs65v26m")))

(define-public crate-seal_rs-0.3.1 (c (n "seal_rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0bwwvyg8sp5nr7m6ms29l8f039w3116gn6zmjp54z012dnl29yc1")))

(define-public crate-seal_rs-0.3.2 (c (n "seal_rs") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "match-downcast") (r "^0.1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "0smr1qpzy42hg1dl07sll1zjf4p6v6y0mpz3sgscj6bjba8j6g4l")))

