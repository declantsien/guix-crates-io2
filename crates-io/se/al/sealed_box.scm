(define-module (crates-io se al sealed_box) #:use-module (crates-io))

(define-public crate-sealed_box-0.1.0 (c (n "sealed_box") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "crypto_box") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1v06xbxfnb9k5ba497mfwlkciz5qskplbd2x5y2qi4qnv5h203hv")))

(define-public crate-sealed_box-0.2.0 (c (n "sealed_box") (v "0.2.0") (h "0vdqarlpi3ck1bnbiqmrvfqfa9gfwfdf9a56r97byqi4jhhyl84d")))

(define-public crate-sealed_box-0.2.1 (c (n "sealed_box") (v "0.2.1") (h "0a6q10vwwz6xwyj7v4dvp8gry5bgy276pdx0hkpgb53q5isln6ap")))

(define-public crate-sealed_box-0.2.2 (c (n "sealed_box") (v "0.2.2") (h "13s32rjqhqd0y0hl95s5s98n1i14fwl3pvmihg1yhhg3h890dx4a")))

(define-public crate-sealed_box-0.2.3 (c (n "sealed_box") (v "0.2.3") (h "1d8w950pgv9v7kz6k84m0pbavkbrircjsa800xxy4y11fdr3njkq")))

(define-public crate-sealed_box-0.2.4 (c (n "sealed_box") (v "0.2.4") (h "06crm55bydbvql8x277qy6f9qh7yc3zidg39nggwiw06p4jdxipy")))

(define-public crate-sealed_box-0.2.5 (c (n "sealed_box") (v "0.2.5") (h "01zhsf2jm9kfs4v60f4421ssl85z75xgf8p92zxxb1iqlgwgdl75")))

(define-public crate-sealed_box-0.2.6 (c (n "sealed_box") (v "0.2.6") (h "0zz867iy9f84zcc5jk1f6yqvliglz8nhmv6038p57v0zjkm96l21")))

(define-public crate-sealed_box-0.2.7 (c (n "sealed_box") (v "0.2.7") (h "096dlr5zi8bva3r29sjggcd7lgwsr6zl3adkj3f13lmf881cn8gs")))

(define-public crate-sealed_box-0.2.8 (c (n "sealed_box") (v "0.2.8") (h "1fbn0k7pbkkz0yn6ncm1cy1q1dkp485waas7gflj1sq47iy1li0c")))

