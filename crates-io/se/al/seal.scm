(define-module (crates-io se al seal) #:use-module (crates-io))

(define-public crate-seal-0.1.0 (c (n "seal") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "0m2d9rvb53jhk49v8xkjy2lvk1qgm5vxp249fgfx4wlq8jxl176q") (y #t)))

(define-public crate-seal-0.1.1 (c (n "seal") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d1nrrip2kawmzlgdayklswj0manfxyjixp3mall7n0s38k7wi0v") (y #t)))

(define-public crate-seal-0.1.2 (c (n "seal") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bbvpvr0i6zdcx5z4s84nc1y6bfqlfwn7zhwn1iibv1ddfbbx4f7")))

(define-public crate-seal-0.1.3 (c (n "seal") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xmnp6fs1csas3ijs1ix1f3s6qy5scwlxin2gyjc3s9xkb3rki52")))

(define-public crate-seal-0.1.4 (c (n "seal") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bc9sv52xxdaklixdzql2gxhr2615ldimh9q1bb48b5wy616lc3s")))

(define-public crate-seal-0.1.5 (c (n "seal") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "088hgibraxzzdmqsnms0h7mqy8gx7l4n5a7fgrqkbngz496p7q79")))

