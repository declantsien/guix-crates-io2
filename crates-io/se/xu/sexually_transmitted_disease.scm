(define-module (crates-io se xu sexually_transmitted_disease) #:use-module (crates-io))

(define-public crate-sexually_transmitted_disease-1.0.0 (c (n "sexually_transmitted_disease") (v "1.0.0") (h "0yb707cad335csv11bq45j09hhq5z99y9zbha7ccnlq37d4crcch")))

(define-public crate-sexually_transmitted_disease-1.0.1 (c (n "sexually_transmitted_disease") (v "1.0.1") (h "1ad3zr058vvq5lmvq2qjjv6m1wfhyxy4n7rkasi0g33kmx0vxnya")))

(define-public crate-sexually_transmitted_disease-1.1.1 (c (n "sexually_transmitted_disease") (v "1.1.1") (h "1v7wg9qily0vc54hb18hq7vdarijyq61wk9ck37r3ldnh8qfq74n")))

