(define-module (crates-io se nd send-mail) #:use-module (crates-io))

(define-public crate-send-mail-0.1.0 (c (n "send-mail") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mail-send") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mf8sg8xb96cbgs2nrby5b1n6dqz3bp44h8azczpr0hi78shsgnc")))

(define-public crate-send-mail-0.1.1 (c (n "send-mail") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mail-send") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0102k8sgjirdh1q97sg9gqh9myym29k503lfwqxi43i5xpr8l505")))

