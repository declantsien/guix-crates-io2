(define-module (crates-io se nd sendgrid_thin) #:use-module (crates-io))

(define-public crate-sendgrid_thin-0.1.0 (c (n "sendgrid_thin") (v "0.1.0") (d (list (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0knms7mpkqzy0xhm5635m7ms72g09ywg2357z144qjnwv8nkkwpw")))

(define-public crate-sendgrid_thin-0.1.1 (c (n "sendgrid_thin") (v "0.1.1") (d (list (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1apwykyj5fmkj1kcxmg0kqcgc0z3sgfdyi1z6n2s86axrf0l835v")))

(define-public crate-sendgrid_thin-0.1.2 (c (n "sendgrid_thin") (v "0.1.2") (d (list (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1c3k6j212h4b9bb6isv8hs8sr1qg49zfrvyshjqc315q60xpsrz4")))

(define-public crate-sendgrid_thin-0.1.3 (c (n "sendgrid_thin") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1p7lg92rl8cixh36ynfvbq35asjsadrqgv2yp2zvdlidrcwmjknx")))

(define-public crate-sendgrid_thin-0.1.4 (c (n "sendgrid_thin") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0qppsn5mnpzm7q7jinvrpd41gf40v4jh4k7wa9avswbfxam4bjdb")))

(define-public crate-sendgrid_thin-0.1.5 (c (n "sendgrid_thin") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0jpg64q0bcii5g7pmyigjzbkpriyiy8pixkyi7qwywp6il3b0qpb")))

(define-public crate-sendgrid_thin-0.1.6 (c (n "sendgrid_thin") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0k30pcpxxyqm1xv3p3ngbh69cj20f7cq949q52dbna9vmn1cs9ns")))

(define-public crate-sendgrid_thin-0.1.7 (c (n "sendgrid_thin") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0b76p3wmgfdnmjr9yzc9qyzyx41d3kl0429x8jsky247yc91zbcj")))

(define-public crate-sendgrid_thin-0.1.8 (c (n "sendgrid_thin") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1dqp9s9gv021yqrarp1j2krnfpfcf5wx9l8np9b6viwimzncg0wg")))

(define-public crate-sendgrid_thin-0.1.9 (c (n "sendgrid_thin") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1a3y5013j2fzp36n3j61pj83fym21kj5b9wprlxjq6sz409d6xvr")))

(define-public crate-sendgrid_thin-0.2.0 (c (n "sendgrid_thin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1bzr25i33ss2zz1fl9r1cv4j0wa4pjxlip5qr25wr5187iifdr2l")))

(define-public crate-sendgrid_thin-0.3.0 (c (n "sendgrid_thin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "014kzhdi1xbcg814nqqifyd6x8mndnhz7fw90zrknw10z0iwjsph")))

(define-public crate-sendgrid_thin-0.4.0 (c (n "sendgrid_thin") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1kcji3wlwppgk36q1aqfl650rc2x7k0cj8hk3ysanl2v8j960dy6") (f (quote (("default" "blocking") ("blocking"))))))

(define-public crate-sendgrid_thin-0.4.1 (c (n "sendgrid_thin") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0s7cn1fw7p25i522qwbbk3h0ik1lf14dhs91ahnvi5953z35zlqh") (f (quote (("default" "blocking") ("blocking"))))))

(define-public crate-sendgrid_thin-0.4.2 (c (n "sendgrid_thin") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0aqmwkd46f19szqqbi971hz1hkkapc0s5rg4nrd5yz20xkm4p472") (f (quote (("default" "blocking") ("blocking"))))))

(define-public crate-sendgrid_thin-0.4.3 (c (n "sendgrid_thin") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "19283kzd2q56kjsp7alrdwjyai0jil9qdzgchnvf3r1glf4kci8w") (f (quote (("default" "blocking") ("blocking"))))))

(define-public crate-sendgrid_thin-0.4.4 (c (n "sendgrid_thin") (v "0.4.4") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "02mxn5x0w1hcn28917185swfb9k8xc67z2a9s2l7qhbzgy7wi5w2") (f (quote (("default" "blocking") ("blocking"))))))

(define-public crate-sendgrid_thin-0.4.5 (c (n "sendgrid_thin") (v "0.4.5") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1qlwfka00hg8n9fvdr79bb4ilch43bxgc51db8n128gy083nafs9") (f (quote (("default" "blocking") ("blocking"))))))

