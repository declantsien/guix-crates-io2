(define-module (crates-io se nd sendkeys-rs) #:use-module (crates-io))

(define-public crate-sendkeys-rs-0.1.0 (c (n "sendkeys-rs") (v "0.1.0") (d (list (d (n "windows") (r "^0.52.0") (f (quote ("Win32_UI_Input_KeyboardAndMouse"))) (d #t) (k 0)))) (h "19dya4dc51ir8imgrgg6g2cwls4ajd1dchw9524nafvf31v2093c")))

(define-public crate-sendkeys-rs-0.2.0 (c (n "sendkeys-rs") (v "0.2.0") (d (list (d (n "windows") (r "^0.52.0") (f (quote ("Win32_UI_Input_KeyboardAndMouse"))) (d #t) (k 0)))) (h "0d85wpqnyqvrcd9fad4vz7cyhrhds5w8dl5f4nn7wr584mmihrpi")))

