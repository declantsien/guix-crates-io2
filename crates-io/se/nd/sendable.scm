(define-module (crates-io se nd sendable) #:use-module (crates-io))

(define-public crate-sendable-0.1.0 (c (n "sendable") (v "0.1.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0cyk5chrqpcr0jx336s0m4654rn5kwkwci5rhl0qvdhy54vczncl") (f (quote (("default"))))))

(define-public crate-sendable-0.2.0 (c (n "sendable") (v "0.2.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0rrp1jqsp6l7pdnas4gwlc4acmh77rg9vg89wspsd5ksznl4bb9j") (f (quote (("default"))))))

(define-public crate-sendable-0.2.1 (c (n "sendable") (v "0.2.1") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1wdfh89mgnymlzi7d9xhfpm82h8vx0b8f1gzj1k05iqlbfs0a5ym") (f (quote (("default"))))))

(define-public crate-sendable-0.2.2 (c (n "sendable") (v "0.2.2") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "17bjcs7ir3rj8m77i7kx11jf3ynkwdcxhqwflfc8lpclqmflbql3") (f (quote (("default"))))))

(define-public crate-sendable-0.3.0 (c (n "sendable") (v "0.3.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1vb6q2kwxdk1hi2k9b0zjmar3ndpvq8pjxwsjgf49cfggrzpim36") (f (quote (("default"))))))

(define-public crate-sendable-0.3.1 (c (n "sendable") (v "0.3.1") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "02700yi2l9abqyar84bidnfr0pfrj30h5lvwnak5cvjvs7lkh2wg") (f (quote (("default"))))))

(define-public crate-sendable-0.4.0 (c (n "sendable") (v "0.4.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0g9c5xjahz95j1q0fgq1d8fa5isbdx2ib9ycqqva37ii3b9pr2lf") (f (quote (("default"))))))

(define-public crate-sendable-0.5.0 (c (n "sendable") (v "0.5.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1vwprjcx3pywvg66i9fvx1mknj09grxacv19n6bgbz59bcn69rwx") (f (quote (("default"))))))

(define-public crate-sendable-0.5.1 (c (n "sendable") (v "0.5.1") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0jnwcjpjlpq97hz01dsx71lwjynbw5qdjzbcalyll048vcr3ag3z") (f (quote (("default"))))))

(define-public crate-sendable-0.5.2 (c (n "sendable") (v "0.5.2") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1mhkawp3hx0lhs9r6dhpnsmdfkxwlz6b0i09wk8klnbf0ap0rmzf") (f (quote (("default"))))))

(define-public crate-sendable-0.6.0 (c (n "sendable") (v "0.6.0") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "140zdsca7czrd066cjjz1z64yrzp7fn8haw8vyzw94d2qw0c4rl0") (f (quote (("default"))))))

(define-public crate-sendable-0.6.1 (c (n "sendable") (v "0.6.1") (d (list (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1nxdx15z16hvw4j0clcv62v6q37vyv0563zr998w9g88xlsahvjv") (f (quote (("default"))))))

