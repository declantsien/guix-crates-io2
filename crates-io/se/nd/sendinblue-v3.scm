(define-module (crates-io se nd sendinblue-v3) #:use-module (crates-io))

(define-public crate-sendinblue-v3-3.0.0 (c (n "sendinblue-v3") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "09512x998dynd50zxaxb4q2phzp64khnxm48bi3l659g2nch2dcc")))

(define-public crate-sendinblue-v3-3.1.0 (c (n "sendinblue-v3") (v "3.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls" "trust-dns"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ka3cg1kl7ffijilzm5wyhx4aqgw45nd4mz0n8n118qmk652w0kb")))

(define-public crate-sendinblue-v3-3.3.0 (c (n "sendinblue-v3") (v "3.3.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "hickory-dns"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1x6jrvlfxi61in98p0z7c89hnxbwlsxxl24xvk0cbn2a9h2xqy29")))

