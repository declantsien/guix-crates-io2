(define-module (crates-io se nd sender-sink) #:use-module (crates-io))

(define-public crate-sender-sink-0.1.0 (c (n "sender-sink") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1mljn34xbmjpqdlhw7skxal4v79ynyf9y0h2jlrbayashad7h14r")))

(define-public crate-sender-sink-0.2.0 (c (n "sender-sink") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1ad4vfjz71fnh9yrdvsgs04whz5i1cl4s0bhadq8yzxgvpas90rb")))

(define-public crate-sender-sink-0.2.1 (c (n "sender-sink") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "10917zhv44w28hb32bkhi8swydid7zm8ms2lcjhwxvia04wgp17s")))

