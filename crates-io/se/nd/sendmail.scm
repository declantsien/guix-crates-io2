(define-module (crates-io se nd sendmail) #:use-module (crates-io))

(define-public crate-sendmail-1.0.0 (c (n "sendmail") (v "1.0.0") (h "08y6vhfh4jb2dg935xjm1rcmlpnwcwc38ln2q4kyqq87jhh16xsv")))

(define-public crate-sendmail-1.0.1 (c (n "sendmail") (v "1.0.1") (h "0czjv8m7jxjjjlxfkalvj49nzqmhhah48m3qsd9i60k8l3gf80b7")))

(define-public crate-sendmail-2.0.0 (c (n "sendmail") (v "2.0.0") (h "0jly5f0j2nnx0cd23201pli8f83yy2pypmdbl36ndpy5c3fdcd5m")))

