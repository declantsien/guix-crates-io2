(define-module (crates-io se nd sendify) #:use-module (crates-io))

(define-public crate-sendify-1.0.0 (c (n "sendify") (v "1.0.0") (h "05qkqkb5lsiki64hppnz16873vbqiidzsrfmgrsriwf22mpj34p0") (f (quote (("default") ("all"))))))

(define-public crate-sendify-1.0.1 (c (n "sendify") (v "1.0.1") (h "0xcyirgadyjwjnxd47yvjnsdnwifj3l2bamjc6h3j7qi8yk5md4x") (f (quote (("default") ("all"))))))

(define-public crate-sendify-1.1.0 (c (n "sendify") (v "1.1.0") (h "0z2a8zijrrk9hlb3nym007gfyb6cy6zj7pq0q4jiibjvq4wqxp3h") (f (quote (("default") ("all"))))))

