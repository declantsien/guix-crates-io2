(define-module (crates-io se nd send-reqwest) #:use-module (crates-io))

(define-public crate-send-reqwest-0.1.0 (c (n "send-reqwest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest-retry") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h7fw5hq977q4hxcmj1i5rhw6j2p4vb6pswmz401axdpysyph375")))

