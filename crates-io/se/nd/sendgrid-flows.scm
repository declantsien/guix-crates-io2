(define-module (crates-io se nd sendgrid-flows) #:use-module (crates-io))

(define-public crate-sendgrid-flows-0.1.0 (c (n "sendgrid-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "07h21n04xr6n8g95v35498wlgxnhybigs1g1zfgprc16axabid33")))

(define-public crate-sendgrid-flows-0.1.1 (c (n "sendgrid-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1a6zsw7q8dcqpsam1n306js374lngylw7acjs9gal9n9ab9w081j")))

(define-public crate-sendgrid-flows-0.1.2 (c (n "sendgrid-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1f0r08f6c16rixhwas1jg20mhcqb34k0r696i8nmxldgiz9ylwkx")))

