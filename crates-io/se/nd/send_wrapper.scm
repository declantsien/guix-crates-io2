(define-module (crates-io se nd send_wrapper) #:use-module (crates-io))

(define-public crate-send_wrapper-0.1.0 (c (n "send_wrapper") (v "0.1.0") (h "05y4yc9ddm8yhknim25px8yabkm37q4l96qkcnnzank3byzqcqsn")))

(define-public crate-send_wrapper-0.2.0 (c (n "send_wrapper") (v "0.2.0") (h "1d5y1adlc1rvybdmijz3g4vjm9qzc8c9zhc8ya0xgkjhiwpdzvd0")))

(define-public crate-send_wrapper-0.3.0 (c (n "send_wrapper") (v "0.3.0") (h "08pq3iw7csibdia197mqnwkddbfn3xj4fw4sr958vb90y0fgjvk8")))

(define-public crate-send_wrapper-0.4.0 (c (n "send_wrapper") (v "0.4.0") (h "1l7s28vfnwdbjyrrk3lx81jy4f0dcrv4iwyah2wj6vndxhqxaf7n")))

(define-public crate-send_wrapper-0.5.0 (c (n "send_wrapper") (v "0.5.0") (d (list (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)))) (h "1mwbg1nv36d5kdjb0iwmprz24km0m8ck08dn59gdngqdc77hl34k") (f (quote (("futures" "futures-core"))))))

(define-public crate-send_wrapper-0.6.0 (c (n "send_wrapper") (v "0.6.0") (d (list (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)))) (h "0wrxzsh9fzgkkkms621ydnz8mj30ilyq299a8cf65jn1y72hw2yd") (f (quote (("futures" "futures-core"))))))

