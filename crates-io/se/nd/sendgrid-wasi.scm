(define-module (crates-io se nd sendgrid-wasi) #:use-module (crates-io))

(define-public crate-sendgrid-wasi-0.19.2 (c (n "sendgrid-wasi") (v "0.19.2") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "reqwest_wasi") (r "^0.11.16") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1biyinngfvh91zryf32cr1s89sx3fb9vgniay1dsjjf3q44zbqba") (f (quote (("wasmedge-tls" "reqwest_wasi/wasmedge-tls") ("default" "wasmedge-tls" "async") ("async"))))))

