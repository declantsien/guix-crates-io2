(define-module (crates-io se nd sendfd) #:use-module (crates-io))

(define-public crate-sendfd-0.1.0 (c (n "sendfd") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0fn807jzvgdnkq85bx1gx2a1ck8vnz5gcidipqjpps70dh0gplf8")))

(define-public crate-sendfd-0.2.0 (c (n "sendfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1aikbi5vfc9w551ix5am8y0djr741n7mf3dgm1ccyzhkzpbdwffg") (y #t)))

(define-public crate-sendfd-0.2.1 (c (n "sendfd") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dmvdrhr4z2j6c9ky44vmr542q7gxg9124iny4xp3ck04dsldw7b")))

(define-public crate-sendfd-0.3.0 (c (n "sendfd") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "003f3qarszfdwfk4yy4h5i3ppaw7ckkc5py5prrs6riscn8hh2y3")))

(define-public crate-sendfd-0.3.1 (c (n "sendfd") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f6mf0dk6aa09h2699c0wlr32j0cmm0hc791mr8sxfhlh6fagi1j")))

(define-public crate-sendfd-0.3.2 (c (n "sendfd") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kqvhvn8yw41l50cnbqk60rn7d7s5v2jbmghp29s2v13jn1gv93h")))

(define-public crate-sendfd-0.3.3 (c (n "sendfd") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio_0_2") (r "^0.2") (f (quote ("uds"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio_0_3") (r "^0.3") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "1wb7b12qh4bf9564196ya54agr5p5fmjfccjp4w93yxykafi23qm")))

(define-public crate-sendfd-0.4.0 (c (n "sendfd") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "1mrvw53b3mi62s4d83b23082d89fpl38hgynhbdgi46yqq0558lz")))

(define-public crate-sendfd-0.4.1 (c (n "sendfd") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "0lm8qy89py511wzb79imc3dfrxf7z82cdlp9wrqlj7rc6piq2pfc")))

(define-public crate-sendfd-0.4.2 (c (n "sendfd") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "1h44apj2616mdpfpkrjfrfjvbz7kypkp4jwhdll3vw6vx65iyy1l")))

(define-public crate-sendfd-0.4.3 (c (n "sendfd") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "160pf2dp5r8smjc6ssk6jf9k93vc280wk8i362xi6zi6zjw72jv0")))

