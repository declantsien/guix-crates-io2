(define-module (crates-io se nd sendgrid-rs) #:use-module (crates-io))

(define-public crate-sendgrid-rs-0.1.0 (c (n "sendgrid-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05y45yl1qxj74svypb5qig4b8qvs0qz7lsiyk6lx6zcg4wr6myc6")))

(define-public crate-sendgrid-rs-0.1.1 (c (n "sendgrid-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f5bc3603f1s60szdi4hlk4xi4s6l22nb8y70i6kadmy72j28y7y")))

