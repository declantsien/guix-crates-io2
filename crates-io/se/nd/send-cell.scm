(define-module (crates-io se nd send-cell) #:use-module (crates-io))

(define-public crate-send-cell-0.1.0 (c (n "send-cell") (v "0.1.0") (h "1pql6v7m1kbn6vx46l4vqzdya3y9zxn6j5378br28argqj44m6bs")))

(define-public crate-send-cell-0.1.1 (c (n "send-cell") (v "0.1.1") (h "14ldm8vxm5p0fgyv27765ym13zy9gj2iydfgnplbybrj6p2i3zvi")))

(define-public crate-send-cell-0.1.2 (c (n "send-cell") (v "0.1.2") (h "0h9czym9j3gsbqpfcknam0bvzd3bz8fgbablsfwnid2nw3bhsqhw")))

(define-public crate-send-cell-0.1.3 (c (n "send-cell") (v "0.1.3") (h "105jjsf1zqbq7y3qjr1r7c1cprqli5xaai66gf375nr60zrizk7y")))

(define-public crate-send-cell-0.1.4 (c (n "send-cell") (v "0.1.4") (d (list (d (n "fragile") (r "^0.2") (d #t) (k 0)))) (h "00gl2zz7wi6ms7s2di00b9b8jf0q3i3hrk2bicamk5ln6fadv6in")))

