(define-module (crates-io se nd send-to-kindle) #:use-module (crates-io))

(define-public crate-send-to-kindle-1.0.0 (c (n "send-to-kindle") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "geckodriver") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1ja5hgbl5zk54yjnhbk4m8kv85sqnn3h0775xicipvngpszncm0f")))

(define-public crate-send-to-kindle-1.0.1 (c (n "send-to-kindle") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "geckodriver") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1yknh57msp0m2bqn94f4d5s82qvvpc2909869xng6g0zbsjz9494")))

