(define-module (crates-io se nd sendgrid2) #:use-module (crates-io))

(define-public crate-sendgrid2-0.1.0 (c (n "sendgrid2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "09rzm7ym6a4g80102ahacrgw8i10lbpvwr9bfyhjgbpfljxavaaz")))

(define-public crate-sendgrid2-1.0.0 (c (n "sendgrid2") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "07afmifs995wf68v5n4pjn513kmngb4jdpcq599gm2h2hd84ppyl")))

(define-public crate-sendgrid2-2.0.0 (c (n "sendgrid2") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "163s4gdbdzs07pcrx9j2x9aw6589lypc7axz1swv7h176fmgc6w5")))

(define-public crate-sendgrid2-4.0.0 (c (n "sendgrid2") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1fl5rrmdkya060bwj18jkzd9b1k0gvzm2qv9fjyykqdbkf24fb9l")))

