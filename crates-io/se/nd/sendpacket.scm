(define-module (crates-io se nd sendpacket) #:use-module (crates-io))

(define-public crate-sendpacket-0.1.0 (c (n "sendpacket") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.20.0") (d #t) (k 0)))) (h "0aff6xhvk6bzcj05q75w5zq868ybidyxb4p453b5r4ibbgjcmjaq")))

(define-public crate-sendpacket-0.1.1 (c (n "sendpacket") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12.8") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.20.0") (d #t) (k 0)))) (h "1jj3ghqww9dfrdky3l9gx4vl9fl5mycabqwchqwi3wd81ndp8qbn")))

