(define-module (crates-io se nd send) #:use-module (crates-io))

(define-public crate-send-0.0.2 (c (n "send") (v "0.0.2") (d (list (d (n "dia-args") (r "^0") (d #t) (k 0)))) (h "0cygiqssv4zfp9sj09nnzqjx876psbxrm1m0mgkifgaping0mjr0")))

(define-public crate-send-0.1.0 (c (n "send") (v "0.1.0") (d (list (d (n "dia-args") (r "^0") (d #t) (k 0)))) (h "0l0npnkjjn2lskzc4adpjjc0511fgmd2jhzxa5vc9y9m0qs68h4q")))

(define-public crate-send-0.2.0 (c (n "send") (v "0.2.0") (d (list (d (n "dia-args") (r "^0") (d #t) (k 0)))) (h "0lw2fr6sy1qfvdj4v1qlirixgnf3qnqwnw6sc0k1ha95x3gjyb37")))

(define-public crate-send-0.3.0 (c (n "send") (v "0.3.0") (d (list (d (n "dia-args") (r "= 0.14.0") (d #t) (k 0)))) (h "0p3njd67v28mf9g3mrk0k4g05iz9ha82xmkw5wgic0rn9p2m4f5h")))

(define-public crate-send-0.3.1 (c (n "send") (v "0.3.1") (d (list (d (n "dia-args") (r "^0.14") (d #t) (k 0)))) (h "046qdzba0m11kbx67hj4nxf45a52v2z96s14kx6f1izi7pgwzn7f")))

(define-public crate-send-0.4.0 (c (n "send") (v "0.4.0") (d (list (d (n "dia-args") (r "^0.19.1") (d #t) (k 0)))) (h "0caxs0v6zssdgw6vzm2glpcglvs6abvlk9xrxiyrjbdckyhm195i")))

(define-public crate-send-0.5.0 (c (n "send") (v "0.5.0") (d (list (d (n "dia-args") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1vay6wbr4i5j5czndjihkr42j14mzkr58a7zi8x9cpnjrd3p2a7v") (f (quote (("bin" "dia-args"))))))

(define-public crate-send-0.6.0 (c (n "send") (v "0.6.0") (d (list (d (n "dia-args") (r "^0.24") (o #t) (d #t) (k 0)))) (h "0ri8jax9lf75gm2q2h4a8fkb9n1mqvqrph3m3p2h1d9fdgdpmaci") (f (quote (("bin" "dia-args"))))))

(define-public crate-send-0.7.0 (c (n "send") (v "0.7.0") (d (list (d (n "dia-args") (r "^0.46") (o #t) (d #t) (k 0)))) (h "1cdw2788cx7xq6371zmq1az90n3h9cpksy9sddd4cc707ci1gh14") (f (quote (("bin" "dia-args"))))))

(define-public crate-send-0.8.0 (c (n "send") (v "0.8.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)))) (h "0hxq5mbc9ali4a9a2f9i42p240dp6nxsjrbs08qxnw2z5havampn") (f (quote (("bin" "dia-args"))))))

