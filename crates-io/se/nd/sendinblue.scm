(define-module (crates-io se nd sendinblue) #:use-module (crates-io))

(define-public crate-sendinblue-0.1.0 (c (n "sendinblue") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yr2cb9l0cjr8p4j25ibicgzcimi810lq53f3i9xnbw5hx990xpj")))

(define-public crate-sendinblue-0.2.0 (c (n "sendinblue") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wwydzn9jcpc6fb8l71j18l6s119xgylbq3myfc4xy2s3sb4qd41")))

(define-public crate-sendinblue-0.2.1 (c (n "sendinblue") (v "0.2.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "048235ynpnl6gkwcs4pm5rsz1w2f5zivav49qnd6ilixafhy8r2w")))

