(define-module (crates-io se nd send-it) #:use-module (crates-io))

(define-public crate-send-it-0.1.0 (c (n "send-it") (v "0.1.0") (h "0m1qfdfgb8wymsqpl804ibs33k2kij1n0lg1r350qp3qvmqpgq57") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian")))) (y #t)))

(define-public crate-send-it-0.1.1 (c (n "send-it") (v "0.1.1") (h "0waihjs6yf65cd5973p20b5ys0n20hjfnqxdlbbg5wq6wc852yw3") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.1.2 (c (n "send-it") (v "0.1.2") (h "0xggh2g8lg30g6r2mkkknd632933pvg2g5n4rjvb1lrrzh6hrq3i") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.1.3 (c (n "send-it") (v "0.1.3") (h "1vzigkfsijqr7lq881w2bnvshr6w6l1fgdgz6sikjspdh0bk3xp8") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.1.4 (c (n "send-it") (v "0.1.4") (h "1gd5p20hcs7prdh2p7lfm0i0j1494d6hbr4m5zbamvb1s96qd45i") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.1.5 (c (n "send-it") (v "0.1.5") (h "0rh610m984zwg6cd86kx1ia7dvx6arg0amzflikv7r3y6bs71ms1") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.2.1 (c (n "send-it") (v "0.2.1") (d (list (d (n "tokio") (r "^1.35.1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "0ngxjhfdkarg0hfzlqkjrkn45vkd1zc4clhvxl3d1x4bqy3v4i1h") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

(define-public crate-send-it-0.2.2 (c (n "send-it") (v "0.2.2") (d (list (d (n "tokio") (r "^1.35.1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "0zng2q7aryjih4yzzhrfk21k5i5nb6hw1ik87dlq1lsp5s4c9zc2") (f (quote (("writing") ("reading") ("default" "writing" "reading") ("big-endian"))))))

