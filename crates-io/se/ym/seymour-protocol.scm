(define-module (crates-io se ym seymour-protocol) #:use-module (crates-io))

(define-public crate-seymour-protocol-0.1.0 (c (n "seymour-protocol") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0imc56xxgn2gdl47gjswy49j1mwqijhl1w0f09bz3nj5gazrs82f")))

(define-public crate-seymour-protocol-0.1.1 (c (n "seymour-protocol") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10rkw7bglmnhh7mzgw2gs1ymkndkdhqla52p0j7dq6bgdcq4x8lp")))

(define-public crate-seymour-protocol-0.1.2 (c (n "seymour-protocol") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0czm9bya5ia3bdmnsl6bhv79c6d52khb4f3dlqbqrxsvfnzdb5ny")))

(define-public crate-seymour-protocol-0.1.3 (c (n "seymour-protocol") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a1799nzljg3zqcb1r1yn277jdbplchvx1201fm22r0vcjavpagr")))

(define-public crate-seymour-protocol-0.1.4 (c (n "seymour-protocol") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14wh6gagriyf6cyaz0x2600grkryakwxqzclrl49qpa3avv8aaqz")))

