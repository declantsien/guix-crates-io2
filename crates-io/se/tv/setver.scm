(define-module (crates-io se tv setver) #:use-module (crates-io))

(define-public crate-setver-0.1.0 (c (n "setver") (v "0.1.0") (h "1bkzvx8kk6hf9vf8llb6lr61ia1pv54dwpcxlylaifc72wbf5l95")))

(define-public crate-setver-0.2.0 (c (n "setver") (v "0.2.0") (h "1ianjl81agrnhnb39dbqvnz75y2mpf2rrl60r2i01pyd76qs1c0x")))

(define-public crate-setver-0.3.0 (c (n "setver") (v "0.3.0") (h "16r56p9dpwc36083r19rbzff20af0hmh7sz2m43ck3v4rkcbi1ph")))

