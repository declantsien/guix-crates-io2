(define-module (crates-io se li selinux-cascade) #:use-module (crates-io))

(define-public crate-selinux-cascade-0.0.1 (c (n "selinux-cascade") (v "0.0.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sexp") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04gd253vbfcghw3gp8binc7h5b5a9frmss10w42qlkhjcbqapagl")))

(define-public crate-selinux-cascade-0.0.2 (c (n "selinux-cascade") (v "0.0.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sexp") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18hayvg8nmfz7bdfk46nxadfafij379mp4x11j65s978d0a901jv")))

