(define-module (crates-io se li selium-tools) #:use-module (crates-io))

(define-public crate-selium-tools-0.1.0 (c (n "selium-tools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1bp220rfr5wzbyb9vslqwdxmq0mgjbfcrsd9sl1v2gi6a0a80rbi")))

(define-public crate-selium-tools-0.2.0 (c (n "selium-tools") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0c99zcd89l3mfdg4n2bn0wp9va4p8v06jpa52cp7nfwwxrw43yvm")))

