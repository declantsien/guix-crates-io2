(define-module (crates-io se li selinux-sys) #:use-module (crates-io))

(define-public crate-selinux-sys-0.1.0 (c (n "selinux-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12ldibhzhl8i2pcxzfzc616ydh5lxwxmam28iklar5mhk9017w8d") (l "selinux")))

(define-public crate-selinux-sys-0.2.0 (c (n "selinux-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "098l8xzd5bi3s9irc793l53a4lpg5dkng37m7nwfz13cg5wnn4gi") (l "selinux")))

(define-public crate-selinux-sys-0.2.1 (c (n "selinux-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l5hbchn95zwmhy1vgnpdxkkajd1m2xv0mwg5aiqxzqgdzpfwmnm") (l "selinux")))

(define-public crate-selinux-sys-0.3.0 (c (n "selinux-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yxkp7js6vqw08v25vajylidfcfi5hi57nylhfmy40fmi91q3yba") (l "selinux")))

(define-public crate-selinux-sys-0.3.1 (c (n "selinux-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q769v9sqalspnzi5llfsn8il9gfkfiplcl76qz5d2j9j6n6ffl1") (l "selinux")))

(define-public crate-selinux-sys-0.4.0 (c (n "selinux-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1p6xlnfn4zra7m39f71whf063qk5cs76azlz92qhchbs35m30z92") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.4.1 (c (n "selinux-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1np0pg3ybkljid48ppxlqwpamkbqzbjn2qqhrycl7vzpz37r0hlq") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.4.2 (c (n "selinux-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "17ra71a5nm82ckywcvxw81iiypanhnip4np8bljvy4fnjwz2xp6r") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.5.0 (c (n "selinux-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0vnrfi0xwfx1nx83hbi9bsrzidq0hjimgspskp1d66r0qanrxh2d") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.5.1 (c (n "selinux-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1ip41dn4n0xjy68m0qzcvkis7wy5wggmdjy6qj06aw90f4bjv12x") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.5.2 (c (n "selinux-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "19dighj7nf8gzy9bgh206znbh1p9gp56wrcwwklv9yiq2zwp9lqj") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.5.3 (c (n "selinux-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0r7q98mpgqhza79h0fw6b92sr48y8zzajwrk631h12vrxwam0gi2") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.0 (c (n "selinux-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1s5v1i1w4sk9km72yh6a168wlab8417qj8pbjbhbzpag28mw7vac") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.1 (c (n "selinux-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.62") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0hw4v3s08m8faq7krfk8f2ympnpwfmpndz7z7y5sgf1ddrf2rh74") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.2 (c (n "selinux-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0pbm7izyqi5n5i4f4f6p266i663lh4l0fdfji5qk91dv94b3hvc0") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.3 (c (n "selinux-sys") (v "0.6.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "135gs5g7i0382b56a947c174az423jrkzdbmm2iwr24iy77h6wfc") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.4 (c (n "selinux-sys") (v "0.6.4") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1nd1d2sza6hf7ppbdkw0mld7s8da788ddrhpy48yrjjp0pywqdsz") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.5 (c (n "selinux-sys") (v "0.6.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "10r48d2mi7gra9z7yhp1g0cddi5jh501k1l7adkd888bvpsxpf7j") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.6 (c (n "selinux-sys") (v "0.6.6") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0hbgs3081gpwczsfhancs0ivf4lfljk78qazwibqq91hb4w04rnm") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.7 (c (n "selinux-sys") (v "0.6.7") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "0asigjnxa88004cgd3vxqyslmnmah6jk5jdl2ql4n9jmm3l5bz4h") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.8 (c (n "selinux-sys") (v "0.6.8") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "1d3blfchpaa1sdsapqb8gnx36dvjkfyvw5355lbzx422h4bfdmnn") (f (quote (("static")))) (l "selinux")))

(define-public crate-selinux-sys-0.6.9 (c (n "selinux-sys") (v "0.6.9") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "1v2z4002x3k7vzfq3z2p05xn85f0439iybmpxg47xh9x6yc59m49") (f (quote (("static")))) (l "selinux")))

