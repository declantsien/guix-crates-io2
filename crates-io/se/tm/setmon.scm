(define-module (crates-io se tm setmon) #:use-module (crates-io))

(define-public crate-setmon-0.1.0 (c (n "setmon") (v "0.1.0") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Devices_Display"))) (d #t) (k 0)))) (h "08bgcahw5qnb36dpw394xrw1n5khkzhvpjgz8sffwdgasxdb5s3h")))

(define-public crate-setmon-0.1.1 (c (n "setmon") (v "0.1.1") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Devices_Display"))) (d #t) (k 0)))) (h "017ha8yg596mv8ybvrhya7a9j60vlia5hv1rl3f261x62859ggjr")))

