(define-module (crates-io se vs sevseg_3642bs) #:use-module (crates-io))

(define-public crate-sevseg_3642bs-0.3.1 (c (n "sevseg_3642bs") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "00lhq20wf70ibbq6i1y74v14v90wp847bqbhy2h5cq5bvw82sb0s")))

(define-public crate-sevseg_3642bs-0.3.2 (c (n "sevseg_3642bs") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1g6x7dmsjia7mjqc4waaqv4k0lw10lg79rgvi0bv37x4iq4jyc6d")))

(define-public crate-sevseg_3642bs-0.3.3 (c (n "sevseg_3642bs") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "18kkmbikfqyk9flkf8yzhhpfiw3q10rahq48wvhb33sqdlbq89il")))

(define-public crate-sevseg_3642bs-0.3.4 (c (n "sevseg_3642bs") (v "0.3.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "19rvgcw1bls7gjrjlkvzbs7g4y3hizqjx4zjic4g7nhajds8i5sg")))

