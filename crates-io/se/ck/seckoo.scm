(define-module (crates-io se ck seckoo) #:use-module (crates-io))

(define-public crate-seckoo-0.0.0-alpha0.1 (c (n "seckoo") (v "0.0.0-alpha0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12q68bc39dhr6yjaprlad73jyxkzq565ql5m7ss9f4czvxql9qdi")))

(define-public crate-seckoo-0.0.0-alpha0.2 (c (n "seckoo") (v "0.0.0-alpha0.2") (d (list (d (n "crossbeam-epoch") (r "^0.9.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1rn3pvmgq3kcyz3krfd70m790r8bvjl84gh4qh6q78zif5dwlil3")))

(define-public crate-seckoo-0.0.0-alpha1.0 (c (n "seckoo") (v "0.0.0-alpha1.0") (d (list (d (n "crossbeam-epoch") (r "^0.9.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1shmn5mqmps4gfxl2s86akb953q1nndpivbp2x8p0wgn4v2x2fxv")))

(define-public crate-seckoo-0.0.0-alpha2.0 (c (n "seckoo") (v "0.0.0-alpha2.0") (d (list (d (n "crossbeam-epoch") (r "^0.9.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "moka") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10bwd2bdp4c2b10gfw1idc8w2qiczicppxj5abkzi4rr1ixvdaxg")))

(define-public crate-seckoo-0.0.0-alpha2.1 (c (n "seckoo") (v "0.0.0-alpha2.1") (d (list (d (n "crossbeam-epoch") (r "^0.9.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "moka") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0cpjhwk352ssh9a4mfa799ibki5f20akv89vwkd63xr4jjs8y7dw")))

