(define-module (crates-io se r- ser-write-msgpack) #:use-module (crates-io))

(define-public crate-ser-write-msgpack-0.1.0 (c (n "ser-write-msgpack") (v "0.1.0") (d (list (d (n "ser-write") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.201") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1ay23q0ylx7f6yrd9yrjsx3pq998a2l6sxsx4y0bc8nnxvgmplc0") (f (quote (("std" "ser-write/std") ("default" "std") ("alloc" "ser-write/alloc")))) (r "1.75")))

(define-public crate-ser-write-msgpack-0.2.0 (c (n "ser-write-msgpack") (v "0.2.0") (d (list (d (n "ser-write") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.201") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0p1cfw9n2z3ybw8fp30byysggp3vchkg15wa7f585yf3q8p7vhpj") (f (quote (("std" "ser-write/std") ("default" "std") ("alloc" "ser-write/alloc")))) (r "1.75")))

