(define-module (crates-io se r- ser-write) #:use-module (crates-io))

(define-public crate-ser-write-0.1.0 (c (n "ser-write") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (k 0)))) (h "0x3lagr2j9d8xhyyfr80clxi2ymw2wrs2b46cggxybvjkcnsm30q") (f (quote (("std") ("default" "std") ("alloc")))) (s 2) (e (quote (("heapless" "dep:heapless") ("arrayvec" "dep:arrayvec")))) (r "1.75")))

(define-public crate-ser-write-0.2.0 (c (n "ser-write") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (k 0)))) (h "0bfl215fi3nnifsjx1lj8cgfprm59k6hjlf4gkymx0lzvr4ya14i") (f (quote (("std") ("default" "std") ("alloc")))) (s 2) (e (quote (("heapless" "dep:heapless") ("arrayvec" "dep:arrayvec")))) (r "1.75")))

