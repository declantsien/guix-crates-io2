(define-module (crates-io se r- ser-write-json) #:use-module (crates-io))

(define-public crate-ser-write-json-0.1.0 (c (n "ser-write-json") (v "0.1.0") (d (list (d (n "ryu-js") (r "^1.0") (k 0)) (d (n "ser-write") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.201") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "003p2z470y0nfp20h92mmg0ghp5463jall5pixcqj1nk2pzibfl6") (f (quote (("std" "ser-write/std") ("default" "std") ("alloc" "ser-write/alloc")))) (r "1.75")))

(define-public crate-ser-write-json-0.2.0 (c (n "ser-write-json") (v "0.2.0") (d (list (d (n "ryu-js") (r "^1.0") (k 0)) (d (n "ser-write") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.201") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r197yqbjpfgdgd2vk5axgszaj2s7vbqwr4rcf1w1yv9qdd617nw") (f (quote (("std" "ser-write/std") ("default" "std") ("alloc" "ser-write/alloc")))) (r "1.75")))

