(define-module (crates-io se r- ser-io) #:use-module (crates-io))

(define-public crate-ser-io-0.1.0 (c (n "ser-io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0l8qch19n9xw91f5jkbmfryhg744hjrhvazrxba4284prwcrp6hc")))

(define-public crate-ser-io-0.1.1 (c (n "ser-io") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0ip8gsbabrc6p60s3n4n0y4qvy7irwwxgkmv0k3xkk3ihdba7ygb")))

(define-public crate-ser-io-0.2.0 (c (n "ser-io") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0mmjb9gg09zdhkjxqas63qnlsz6l5xmiaqqwdwrd5qbrfwmwgnvb")))

(define-public crate-ser-io-0.3.0 (c (n "ser-io") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1swaa7nafp3lf0m7jlplja02pkf9g52k2sxz5wf9gmbrd1cmh5v8")))

