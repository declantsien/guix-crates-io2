(define-module (crates-io se a- sea-orm-rocket) #:use-module (crates-io))

(define-public crate-sea-orm-rocket-0.5.0-rc.1 (c (n "sea-orm-rocket") (v "0.5.0-rc.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (k 2)) (d (n "sea-orm-rocket-codegen") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1yaq3jhlnva72g6jinbg75z858zg9msb557j2503zmsjn3q1pa4i")))

(define-public crate-sea-orm-rocket-0.5.0 (c (n "sea-orm-rocket") (v "0.5.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (k 2)) (d (n "sea-orm-rocket-codegen") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0kx121p2r2rxwcjj4aw1qjk996g36k8w92v01xjgc9gqk1spnhi9")))

(define-public crate-sea-orm-rocket-0.5.1 (c (n "sea-orm-rocket") (v "0.5.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (k 2)) (d (n "rocket_okapi") (r "^0.8.0-rc.2") (o #t) (k 0)) (d (n "sea-orm-rocket-codegen") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "01w7m10583nzdcciiqmc8pgr09jrxwkym5f1z8pq2fwy0f2sxvc4")))

(define-public crate-sea-orm-rocket-0.5.2 (c (n "sea-orm-rocket") (v "0.5.2") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (k 2)) (d (n "rocket_okapi") (r "^0.8.0-rc.2") (o #t) (k 0)) (d (n "sea-orm-rocket-codegen") (r "^0.5.0") (d #t) (k 0)))) (h "18ka8bzm0rrl2fkrf1pv2apa87rlbril51dg8bppscaxyhn0pwrh")))

(define-public crate-sea-orm-rocket-0.5.3 (c (n "sea-orm-rocket") (v "0.5.3") (d (list (d (n "rocket") (r "^0.5.0-rc.4") (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (f (quote ("json"))) (k 2)) (d (n "sea-orm-rocket-codegen") (r "^0.5.1") (d #t) (k 0)))) (h "0zv8km9ns4smxmzg1vky6sms7m9zfkisj61wjij26vrfgxg7rmh5") (f (quote (("rocket_okapi"))))))

(define-public crate-sea-orm-rocket-0.5.4 (c (n "sea-orm-rocket") (v "0.5.4") (d (list (d (n "rocket") (r "^0.5.0") (k 0)) (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (k 2)) (d (n "sea-orm-rocket-codegen") (r "^0.5.1") (d #t) (k 0)))) (h "1fy5jdsnd1m2njjzygacppj42pyjyyayhwa2xv56p25l9pa2q7my") (f (quote (("rocket_okapi"))))))

