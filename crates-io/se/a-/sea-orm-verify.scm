(define-module (crates-io se a- sea-orm-verify) #:use-module (crates-io))

(define-public crate-sea-orm-verify-0.1.0 (c (n "sea-orm-verify") (v "0.1.0") (d (list (d (n "convert_ident") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f8j1mvm4y46mhmr6xc7il9ri5lq19ckm492ldmdck7wwh6hc1cy")))

