(define-module (crates-io se a- sea-orm-field-updater) #:use-module (crates-io))

(define-public crate-sea-orm-field-updater-0.0.1 (c (n "sea-orm-field-updater") (v "0.0.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01ld8xkl3qwc7bxxgjps0nc7kbzvadjnq47vls616ak0l5ssikas")))

(define-public crate-sea-orm-field-updater-0.0.2 (c (n "sea-orm-field-updater") (v "0.0.2") (d (list (d (n "convert_ident") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "027gbjfmlkv1vhkqm1ns1acnla7dcasgp4jxl2d94f8zmhc59nzd")))

