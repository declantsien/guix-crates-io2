(define-module (crates-io se a- sea-streamer-runtime) #:use-module (crates-io))

(define-public crate-sea-streamer-runtime-0.1.0 (c (n "sea-streamer-runtime") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0)))) (h "1l2sinz30p211c65mqszp6cwb1nvcp8fzi2m881c8h1qk0zsi4lz") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (r "1.60")))

(define-public crate-sea-streamer-runtime-0.2.0 (c (n "sea-streamer-runtime") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0)))) (h "0wbf2gw5a7wql9ayy22g3k6wbrsm8130zinjasgmghb5kj8bi9ck") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (r "1.60")))

(define-public crate-sea-streamer-runtime-0.3.0 (c (n "sea-streamer-runtime") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0)))) (h "1avw97szvlvmc23f84jvi4bpqjngi65fc7ms14ggwb9pqsvfj50h") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (s 2) (e (quote (("file" "tokio?/fs" "tokio?/io-util")))) (r "1.60")))

(define-public crate-sea-streamer-runtime-0.3.2 (c (n "sea-streamer-runtime") (v "0.3.2") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "sync" "rt"))) (o #t) (d #t) (k 0)))) (h "149nhwg4y6z6akih5fgiy7irglcx2y220mgm0kvh1gl6mh849662") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (s 2) (e (quote (("file" "tokio?/fs" "tokio?/io-util")))) (r "1.60")))

(define-public crate-sea-streamer-runtime-0.3.3 (c (n "sea-streamer-runtime") (v "0.3.3") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "sync" "rt"))) (o #t) (d #t) (k 0)))) (h "1nwr4ij4ls01akgzgv6sva7i6jm5vhdj6g2y1r7hfiip1i0ajrq9") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (s 2) (e (quote (("file" "tokio?/fs" "tokio?/io-util")))) (r "1.60")))

(define-public crate-sea-streamer-runtime-0.5.0 (c (n "sea-streamer-runtime") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "sync" "rt"))) (o #t) (d #t) (k 0)))) (h "1dkh44zm866nc16yly8jb8qm85cqywv8ixl0rjivqzs5ip9yjnsf") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "async-std")))) (s 2) (e (quote (("file" "tokio?/fs" "tokio?/io-util")))) (r "1.60")))

