(define-module (crates-io se a- sea-orm-rocket-codegen) #:use-module (crates-io))

(define-public crate-sea-orm-rocket-codegen-0.5.0-rc.1 (c (n "sea-orm-rocket-codegen") (v "0.5.0-rc.1") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "195p20ai95w4rgj98wi6a7nsfnp5p3ynb6ks6jqy6z8cibc2i4ns")))

(define-public crate-sea-orm-rocket-codegen-0.5.0 (c (n "sea-orm-rocket-codegen") (v "0.5.0") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1wi2c258v34fd8icb2h4gv0il0zp1glqcbayhl5z6h95vc57bz8b")))

(define-public crate-sea-orm-rocket-codegen-0.5.1 (c (n "sea-orm-rocket-codegen") (v "0.5.1") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0b8ivwcickml40n6hzhccx5s7czqs2zidxnn14cfsmc2szka0rgk")))

(define-public crate-sea-orm-rocket-codegen-0.5.4 (c (n "sea-orm-rocket-codegen") (v "0.5.4") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1qb0g11r31il4ndlhnby0fv2i4my3hx1dvchm9v1jh16hkwmplyb")))

