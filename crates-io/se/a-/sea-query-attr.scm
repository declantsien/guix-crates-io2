(define-module (crates-io se a- sea-query-attr) #:use-module (crates-io))

(define-public crate-sea-query-attr-0.1.0 (c (n "sea-query-attr") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sea-query") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0kmq2kj0x0rklmrpx3an39x1h6jc4i4swwncic2q8pwr151rvk10")))

(define-public crate-sea-query-attr-0.1.1 (c (n "sea-query-attr") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sea-query") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0238cy40r8hhhz13zikigf5dc1jcig2wrnjw8b6zlnqfgzaz7347")))

(define-public crate-sea-query-attr-0.1.2 (c (n "sea-query-attr") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (k 0)) (d (n "heck") (r "^0.4") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "sea-query") (r "^0.31.0-rc.4") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xxlwqmy3ha7k6gj9cr2vkzs3lnqmqp50z59db91lyasxzh332hn") (r "1.60")))

