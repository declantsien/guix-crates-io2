(define-module (crates-io se a- sea-orm-newtype) #:use-module (crates-io))

(define-public crate-sea-orm-newtype-0.0.1 (c (n "sea-orm-newtype") (v "0.0.1") (d (list (d (n "email_address") (r "^0.2.4") (d #t) (k 2)) (d (n "sea-orm") (r "^0") (d #t) (k 0)) (d (n "sea-orm-newtype-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (d #t) (k 2)))) (h "1hj5pfx5vbnm369a5ycnx9ixq1bhlxkhi04avpfb95dsk3z05xq8")))

