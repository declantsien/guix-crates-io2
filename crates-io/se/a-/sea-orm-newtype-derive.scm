(define-module (crates-io se a- sea-orm-newtype-derive) #:use-module (crates-io))

(define-public crate-sea-orm-newtype-derive-0.0.1 (c (n "sea-orm-newtype-derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jp6lc7nb86d9m0i542k90z2x69d9smjwawylp9b56l7gksgdydc")))

