(define-module (crates-io se a- sea-strum) #:use-module (crates-io))

(define-public crate-sea-strum-0.21.0 (c (n "sea-strum") (v "0.21.0") (d (list (d (n "sea-strum_macros") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "sea-strum_macros") (r "^0.21") (d #t) (k 2)))) (h "00f0kqvg0lsm46dcnsgnr3s7flsvk025vpaynv60z5dkr33ccxvc") (f (quote (("sea-orm" "sea-strum_macros/sea-orm") ("derive" "sea-strum_macros"))))))

(define-public crate-sea-strum-0.23.0 (c (n "sea-strum") (v "0.23.0") (d (list (d (n "sea-strum_macros") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "sea-strum_macros") (r "^0.23") (d #t) (k 2)))) (h "06qfw8nd2sb5rzqnjrwszilzsvdp24wzbxy6kbkwyhkq02k0c79r") (f (quote (("std") ("sea-orm" "sea-strum_macros/sea-orm") ("derive" "sea-strum_macros") ("default" "std"))))))

