(define-module (crates-io se a- sea-migrations) #:use-module (crates-io))

(define-public crate-sea-migrations-0.0.1 (c (n "sea-migrations") (v "0.0.1") (d (list (d (n "sea-orm") (r "^0.3.2") (f (quote ("mock"))) (k 0)) (d (n "sea-orm") (r "^0.3.2") (f (quote ("sqlx-sqlite" "runtime-tokio-native-tls" "macros"))) (k 2)) (d (n "tokio") (r "^1.13.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "19h88d5bz090ja7y93lfmhlh9x05r96jbx52vrq5p0saj1d8micx")))

(define-public crate-sea-migrations-0.0.2 (c (n "sea-migrations") (v "0.0.2") (d (list (d (n "sea-orm") (r "^0.3.2") (f (quote ("mock"))) (k 0)) (d (n "sea-orm") (r "^0.3.2") (f (quote ("sqlx-sqlite" "runtime-tokio-native-tls" "macros"))) (k 2)) (d (n "tokio") (r "^1.13.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1x9hawxjmgx5b923h8zkyivs3rb7i57jiv83czzzcm2isldi93w9")))

(define-public crate-sea-migrations-0.0.3 (c (n "sea-migrations") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "sea-orm") (r "^0.4.2") (f (quote ("mock"))) (k 0)) (d (n "sea-orm") (r "^0.4.2") (f (quote ("sqlx-sqlite" "runtime-tokio-native-tls" "macros"))) (k 2)) (d (n "tokio") (r "^1.13.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12mk7awqxz9qnx8ki73ppyv4h6f06apmxzbay9hddjwc63kq7saf")))

