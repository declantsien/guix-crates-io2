(define-module (crates-io se a- sea-schema-derive) #:use-module (crates-io))

(define-public crate-sea-schema-derive-0.1.0 (c (n "sea-schema-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "09qrzxhw95jslrvw93p0k0jci6as4nnr29vffa7nn2gmfrq1p0jn")))

(define-public crate-sea-schema-derive-0.2.0-rc.1 (c (n "sea-schema-derive") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1qk0cd29q7y9al5f5334p700jfb6l9ws7h12sz0xqdcjjq21p0cs")))

(define-public crate-sea-schema-derive-0.2.0-rc.2 (c (n "sea-schema-derive") (v "0.2.0-rc.2") (d (list (d (n "heck") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "08al1kl5xfrc6yhbn0zi0377c57mqx1bbw4swc3z2m9m6r818197")))

(define-public crate-sea-schema-derive-0.2.0 (c (n "sea-schema-derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "07niaq8f2zdwx23j7d9hp1k8cm7mdpm8mnjwcd7zrgvn1w2qdxn6")))

