(define-module (crates-io se a- sea-orm-dbml) #:use-module (crates-io))

(define-public crate-sea-orm-dbml-0.1.0-beta.1 (c (n "sea-orm-dbml") (v "0.1.0-beta.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dbml-rs") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "sea-orm") (r "^0.10") (d #t) (k 2)))) (h "1d2sy1gwkbv8a2xms2iaxxi9sxla7lzphmmvhyjr4sz9x9cdzjc2") (f (quote (("default"))))))

(define-public crate-sea-orm-dbml-0.1.0-beta.2 (c (n "sea-orm-dbml") (v "0.1.0-beta.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dbml-rs") (r "^0.1.0-beta.2") (d #t) (k 0)) (d (n "sea-orm") (r "^0.11") (d #t) (k 2)))) (h "1gin714jywpkaqy9fscq8l9wx68hmdbmk2zjbgg247hhs89clav4") (f (quote (("default"))))))

