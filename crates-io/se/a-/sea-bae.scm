(define-module (crates-io se a- sea-bae) #:use-module (crates-io))

(define-public crate-sea-bae-0.2.0 (c (n "sea-bae") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "054i7jgqxjdkjzcp3pqxsgnd1i1iiwgxq218vmzaml3qk5557lrv")))

