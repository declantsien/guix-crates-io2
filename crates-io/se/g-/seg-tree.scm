(define-module (crates-io se g- seg-tree) #:use-module (crates-io))

(define-public crate-seg-tree-0.1.0 (c (n "seg-tree") (v "0.1.0") (h "1bhvf3cfnsmb8vwragz875zdaw8isbp9x59i3bibazsr1758ilqv")))

(define-public crate-seg-tree-0.1.1 (c (n "seg-tree") (v "0.1.1") (h "0r7babgj82d3g7cmv0mw644m9jv0w8agazqaqp1mfkdzg2rb44hx")))

(define-public crate-seg-tree-0.1.2 (c (n "seg-tree") (v "0.1.2") (h "0x822nal8z8zpsgsa0976z6hhpkp3x1imnqb683inrbkav90hj4b")))

(define-public crate-seg-tree-0.1.3 (c (n "seg-tree") (v "0.1.3") (h "0d4kl8qpah9m9l51mpsv2c0xrgvfp7l9xwbpmx1cvp7xs3v42bhk")))

(define-public crate-seg-tree-0.2.0 (c (n "seg-tree") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jkny00b62zz2nddxmvm343s58i86lns31ny3mmgb8hagz4484qv")))

(define-public crate-seg-tree-0.2.1 (c (n "seg-tree") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c866fqv60iwysnp30swzzg2wsxx5v8m1m8pp5xgc7bdnbb0kp2q")))

(define-public crate-seg-tree-0.2.2 (c (n "seg-tree") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mwk5m9mnbnjm23hm53rfn0nmbkl6j7n7smhcc7whfj7685xbyvj") (y #t)))

(define-public crate-seg-tree-0.3.0 (c (n "seg-tree") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16n4ac7nqqsliy3rs6g2yp8zvr9fkfz473q2haw46x8bqjyrflxk")))

