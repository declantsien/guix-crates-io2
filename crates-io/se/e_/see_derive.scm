(define-module (crates-io se e_ see_derive) #:use-module (crates-io))

(define-public crate-see_derive-0.0.1 (c (n "see_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "03kn00lyq6nl6pzyy5rs9x5hmwmbbw4y482afmcsfszr930lqx51")))

(define-public crate-see_derive-0.0.2 (c (n "see_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0hkgqba3vp3mxn0586yr4dbs21bzl9axa939c71n0ig50bi8airq")))

(define-public crate-see_derive-0.0.3 (c (n "see_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0js2kf2aghpahs5pcpaj769rhpls2y6hw4ll5jvdr6ih535w77mr")))

(define-public crate-see_derive-0.0.4 (c (n "see_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "177cvqfnr12khlnivy2r0d99z42jlc8hgii74dn15pg9sjwlxz2p")))

(define-public crate-see_derive-0.0.5 (c (n "see_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1d20qn85cd69jdvba8ja3z7jblr4ng3idx0syd75b99nmxm40wgb")))

