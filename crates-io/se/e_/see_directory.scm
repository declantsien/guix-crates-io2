(define-module (crates-io se e_ see_directory) #:use-module (crates-io))

(define-public crate-see_directory-0.1.0 (c (n "see_directory") (v "0.1.0") (h "16yv19napbmvb8m0r9i7splq8zalrd9vr5lakwg7cl80dmjbdvqc")))

(define-public crate-see_directory-0.2.1 (c (n "see_directory") (v "0.2.1") (h "1b3iz4hhsc6kkvlnmjlqjkm82hd2h9cd7kmq5vbid3dixv7ka886")))

(define-public crate-see_directory-0.3.1 (c (n "see_directory") (v "0.3.1") (h "0dj505myrj8zncfp61v74cn8jqzf8x1n9lkxfwcr8z91c52ch7da")))

