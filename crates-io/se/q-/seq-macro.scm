(define-module (crates-io se q- seq-macro) #:use-module (crates-io))

(define-public crate-seq-macro-0.1.0 (c (n "seq-macro") (v "0.1.0") (h "19ca7icy4b3zbcf5nvkln5lyp49mrisx24amjqh0q13kkinnis9h")))

(define-public crate-seq-macro-0.1.1 (c (n "seq-macro") (v "0.1.1") (h "09f0pqaashmn8q9djlvlfc7vj44xfgj9kyp8rmn01b2w5997nr5p") (f (quote (("docs-rs"))))))

(define-public crate-seq-macro-0.1.2 (c (n "seq-macro") (v "0.1.2") (h "0xz5rlxl2dx5pq2fg7dq9rkrq1nhksm86166mmxn5if1cz04bijh") (f (quote (("docs-rs"))))))

(define-public crate-seq-macro-0.1.3 (c (n "seq-macro") (v "0.1.3") (h "0acjla5kzmhrzi5ki49xmz4jfcd660aw556nj5j4kxmvslc6znmc")))

(define-public crate-seq-macro-0.1.4 (c (n "seq-macro") (v "0.1.4") (h "0hz846c2zs6mh6dkxsqbafg3x3jwy8bmdr4iykx7w2bsp1r1b2zy")))

(define-public crate-seq-macro-0.1.5 (c (n "seq-macro") (v "0.1.5") (h "036sxc6h92gh2a1mx86dqp03lpr9zlycqj8ggyk50i3q29af2qma")))

(define-public crate-seq-macro-0.2.0 (c (n "seq-macro") (v "0.2.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yzs4qdah0rhcmz8kd80sbymkjcg5hnxxaczvb3yi1cg6m8gb6wk")))

(define-public crate-seq-macro-0.2.1 (c (n "seq-macro") (v "0.2.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0nnjm5gcdpqa6ipmqyv6hx0xgvd62jyvy8wi0zbp739jbxkbvcym")))

(define-public crate-seq-macro-0.2.2 (c (n "seq-macro") (v "0.2.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "168y8k344gssy1q0q6napy8cswgl4hyh2kcim9pk3b9wxbx4g7ss")))

(define-public crate-seq-macro-0.3.0 (c (n "seq-macro") (v "0.3.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1li007dx6939alzf35ixhayyx4i2hv94cygdby7r5jfv8c989nbh") (r "1.45")))

(define-public crate-seq-macro-0.3.1 (c (n "seq-macro") (v "0.3.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1628inf65zd9js6waflszafhj718d5f593jghgv9238s1v1wawh7") (r "1.45")))

(define-public crate-seq-macro-0.3.2 (c (n "seq-macro") (v "0.3.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kn5spxw9s2l1q29jdng4qpyz7aiv03vkgdx4f21jwmjv7gdx18n") (r "1.45")))

(define-public crate-seq-macro-0.3.3 (c (n "seq-macro") (v "0.3.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g6h3aki8xdw17zfib6c0m38plc3v96rac6j6riyc51sr67lxd76") (r "1.45")))

(define-public crate-seq-macro-0.3.4 (c (n "seq-macro") (v "0.3.4") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "00yg1j96gwp4nm1kshl9rb62abyl7w81n4rc9pz32cav2wwlj4v3") (r "1.45")))

(define-public crate-seq-macro-0.3.5 (c (n "seq-macro") (v "0.3.5") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1d50kbaslrrd0374ivx15jg57f03y5xzil1wd2ajlvajzlkbzw53") (r "1.45")))

