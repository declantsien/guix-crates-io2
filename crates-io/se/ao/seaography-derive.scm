(define-module (crates-io se ao seaography-derive) #:use-module (crates-io))

(define-public crate-seaography-derive-0.1.0 (c (n "seaography-derive") (v "0.1.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1pk3dvpvay6mf6li0zl5cn4y9qcn1z75cgrvcr2djj7nhj5c997y") (r "1.60")))

(define-public crate-seaography-derive-0.2.0 (c (n "seaography-derive") (v "0.2.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1rfz1xb5mdpdcb5n2qkzrnviw967zn80d4sx5jgirzn7bc7fd48i") (f (quote (("with-uuid") ("with-json") ("with-decimal") ("with-chrono") ("default")))) (r "1.60")))

(define-public crate-seaography-derive-0.3.0 (c (n "seaography-derive") (v "0.3.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0gfpmlxck3jcsg3n975yi8fd7jxg3jdp5wqq6d25bd87nv7dh5ja") (f (quote (("with-uuid") ("with-json") ("with-decimal") ("with-chrono") ("default")))) (r "1.60")))

