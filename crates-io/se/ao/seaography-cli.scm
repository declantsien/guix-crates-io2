(define-module (crates-io se ao seaography-cli) #:use-module (crates-io))

(define-public crate-seaography-cli-0.1.0 (c (n "seaography-cli") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^0.1") (d #t) (k 0)))) (h "0sfi3szh8zz4pz4z4s1sh5nqakng631sgcq9c2piq6daf9wa6aql") (r "1.60")))

(define-public crate-seaography-cli-0.1.1 (c (n "seaography-cli") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-discoverer") (r "^0.1.1") (d #t) (k 0)) (d (n "seaography-generator") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1iqkamff8d3mradsba8c8ypx59pahkrr3iy19cs8yraadaqkzwcv") (r "1.60")))

(define-public crate-seaography-cli-0.1.2 (c (n "seaography-cli") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-discoverer") (r "^0.1.1") (d #t) (k 0)) (d (n "seaography-generator") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "11nff8vm7857cxxa5nfk0lc5y431m12khyimm702vkq8lwi7d3c7") (r "1.60")))

(define-public crate-seaography-cli-0.2.0 (c (n "seaography-cli") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-discoverer") (r "^0.2.0") (d #t) (k 0)) (d (n "seaography-generator") (r "^0.2.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0sd9v73555djqj35pgnjxycx7wd3xvhspvy4pv74r838qdzsaf07") (r "1.60")))

(define-public crate-seaography-cli-0.3.0 (c (n "seaography-cli") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-discoverer") (r "^0.3.0") (d #t) (k 0)) (d (n "seaography-generator") (r "^0.3.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "193a70qz80a56rd2my31xnwm3j1kb07sf5kmfn9pjagrb8vicrk0") (r "1.60")))

(define-public crate-seaography-cli-1.0.0-rc.1 (c (n "seaography-cli") (v "1.0.0-rc.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "08ag7yvpdxk6dg20zpnzz2vir83av6rsngadvg94s5rn3yz5zf7x") (r "1.60")))

(define-public crate-seaography-cli-1.0.0-rc.2 (c (n "seaography-cli") (v "1.0.0-rc.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^1.0.0-rc.2") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "08xaz0w4x8fnqr2n2844ic4cmidw1gsmwg7arf9jp8gbiqsc238r") (r "1.60")))

(define-public crate-seaography-cli-0.12.0 (c (n "seaography-cli") (v "0.12.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "14a92ibh5wic8f8f4hwaymvzjh4f6258qmsw94y32zzgjn2z82ps") (r "1.70")))

(define-public crate-seaography-cli-1.0.0-rc.3 (c (n "seaography-cli") (v "1.0.0-rc.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1wi51nazfg0k3mvjlgcd043vrdpcdwq2sczs2yrr0r3b62k3s2g2") (r "1.70")))

(define-public crate-seaography-cli-1.0.0-rc.4 (c (n "seaography-cli") (v "1.0.0-rc.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seaography-generator") (r "^1.0.0-rc.4") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0454q0ricmh8pk8qdmk14z5x2sfjvlhmvm8fgx5q3yhgs9p2sj0x") (r "1.70")))

