(define-module (crates-io se cs secsy) #:use-module (crates-io))

(define-public crate-secsy-0.0.1 (c (n "secsy") (v "0.0.1") (d (list (d (n "secsy_ecs") (r "^0") (d #t) (k 0)))) (h "0cip2zk02vpm6zvppp96g8niwav03jvgqlc3b8q9igfh7dlzdgn8") (y #t)))

(define-public crate-secsy-0.1.0 (c (n "secsy") (v "0.1.0") (d (list (d (n "secsy_world") (r "^0") (d #t) (k 0)))) (h "1pcxidz9g38gqvdkyjdns379isy36kvxh9nkj09f4zryngl94hyj") (y #t)))

(define-public crate-secsy-0.1.1 (c (n "secsy") (v "0.1.1") (d (list (d (n "secsy_world") (r "^0") (d #t) (k 0)))) (h "1hga1fm22x97cpjn7pxnpygjlp6z0gsnzv6lshfjkbpx5p4rn2ba") (y #t)))

