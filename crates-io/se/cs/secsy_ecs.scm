(define-module (crates-io se cs secsy_ecs) #:use-module (crates-io))

(define-public crate-secsy_ecs-0.0.1 (c (n "secsy_ecs") (v "0.0.1") (h "1vkj9bxl5avafsi61ip9z4i16q6dr440lf154g5agq9nc9m79saj")))

(define-public crate-secsy_ecs-0.0.2 (c (n "secsy_ecs") (v "0.0.2") (h "17bxwi1v9xg6ry43a93fa0slgf42yf3dgifvv653injbbdivkq3r")))

