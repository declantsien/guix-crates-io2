(define-module (crates-io se cs secstr) #:use-module (crates-io))

(define-public crate-secstr-0.1.0 (c (n "secstr") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0xg60hvw6b6qv5734w12ffjz7nm9qikdlxljffs91l49mgml2l85")))

(define-public crate-secstr-0.2.0 (c (n "secstr") (v "0.2.0") (d (list (d (n "cbor") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (o #t) (d #t) (k 0)))) (h "0rc4dqy0gszabl9avnflp08gc6xccj2s2dldvidn88nlql6hn1ah") (f (quote (("cbor-serialize" "rustc-serialize" "cbor"))))))

(define-public crate-secstr-0.2.1 (c (n "secstr") (v "0.2.1") (d (list (d (n "cbor") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (o #t) (d #t) (k 0)))) (h "1g2g4y79bmnla6958f2jp78n5np5a51cw1qavjws7aywqx2x6q59") (f (quote (("cbor-serialize" "rustc-serialize" "cbor"))))))

(define-public crate-secstr-0.3.0 (c (n "secstr") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.6.0") (d #t) (k 2)))) (h "1521qspwj0k7ir8cvg4sx1rqwfi94whwk7n83203q9s826fgyknz")))

(define-public crate-secstr-0.3.1 (c (n "secstr") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.6.0") (d #t) (k 2)))) (h "0n1izkvrq5bl4pa4ycymp9by67cbdw3sagjzqfsrvasj1q3p8ir1")))

(define-public crate-secstr-0.3.2 (c (n "secstr") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)))) (h "12113228mjgn68y96519nmpgmispa0ah2sr22rssji7jrr9ks07v")))

(define-public crate-secstr-0.4.0 (c (n "secstr") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)))) (h "1z6s8fhipn213l0gsjjijjp95hn2nchrmp8yixdhwchwfhkcgqnc")))

(define-public crate-secstr-0.5.0 (c (n "secstr") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (o #t) (d #t) (k 0)) (d (n "pre") (r "=0.2") (o #t) (d #t) (k 0)) (d (n "pre") (r "=0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)))) (h "01416mhw1lcyrjhlvgw3myxn3zw8n3c0gxgvfqkmxp77i4fqryj9")))

(define-public crate-secstr-0.5.1 (c (n "secstr") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pre") (r "=0.2") (o #t) (d #t) (k 0)) (d (n "pre") (r "=0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)))) (h "04iy25y5qb8lzymsx1iz6250q1dxx29mkppn737w81gn8ir6akz0")))

