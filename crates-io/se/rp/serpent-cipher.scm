(define-module (crates-io se rp serpent-cipher) #:use-module (crates-io))

(define-public crate-serpent-cipher-0.1.0 (c (n "serpent-cipher") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "15vkf7shjbc6l31slhf9l84z640qxy88pa0zmvgdq4c0mb7l2nb6")))

(define-public crate-serpent-cipher-0.1.1 (c (n "serpent-cipher") (v "0.1.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "12d12vw51xmfwvbm7r0w020x8rq31c7g2d1wlg8z2v7w4wi38y6n")))

(define-public crate-serpent-cipher-0.1.2 (c (n "serpent-cipher") (v "0.1.2") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0n1vc0ww9jy9p8ggkwhmm0v5rm8fd0qc8i1q3vblnnmxs9fdxg7k")))

