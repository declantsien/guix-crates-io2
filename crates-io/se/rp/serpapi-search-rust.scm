(define-module (crates-io se rp serpapi-search-rust) #:use-module (crates-io))

(define-public crate-serpapi-search-rust-0.1.0 (c (n "serpapi-search-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "083j0bm4r4q6irznpv5vm7ss2wdxwhs01j9z17lhgbprxrk64j3n")))

(define-public crate-serpapi-search-rust-0.1.1 (c (n "serpapi-search-rust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bgsps0nli2a7ahamn7hln4rpvxpfpr40pzx0hh05fsdhka2bbs6")))

