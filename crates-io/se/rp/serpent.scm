(define-module (crates-io se rp serpent) #:use-module (crates-io))

(define-public crate-serpent-0.0.0 (c (n "serpent") (v "0.0.0") (h "07gi5v89b6v43wpgr2qk4n9kg88s9n8xh1qxjv79x0bh6h5w6aaz") (y #t)))

(define-public crate-serpent-0.0.1 (c (n "serpent") (v "0.0.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0w04n588dkna1dg9h5f3hbxqp1wrcnbzzqmjzv4kb9d02748fd1w")))

(define-public crate-serpent-0.1.0 (c (n "serpent") (v "0.1.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "149nf7nqh24m986fi24wgcsm287arry5kgaxa99x1qwk63z8ny4f")))

(define-public crate-serpent-0.2.0 (c (n "serpent") (v "0.2.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1n75jz2fil4c1hrmjklgfpbs7djjb7y0dpamkj13kasi1ld1m11f")))

(define-public crate-serpent-0.3.0 (c (n "serpent") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1isyj9bybr04h5gfyyl52z4nlnyyl5bvg7l49bgbk39wcd7x8jnp")))

(define-public crate-serpent-0.4.0 (c (n "serpent") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0h5xqnwk5jvi37vqzwrbknpif4r6z9ny4k3aki8ch8w8b3mxf05d")))

(define-public crate-serpent-0.5.0 (c (n "serpent") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0503gfhf7l51wwiqiqxz6idd7vd98zbbsjxdapch6sv1w7j00h23") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-serpent-0.5.1 (c (n "serpent") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.1") (k 0)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "0lmvmg2gz5zpzb0dx0pxz69bf2nhs1px2hl3sbkqh0x7anc844ln") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

