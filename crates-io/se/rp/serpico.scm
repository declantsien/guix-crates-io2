(define-module (crates-io se rp serpico) #:use-module (crates-io))

(define-public crate-serpico-0.0.1 (c (n "serpico") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "0i9mfm826ppi3bc1f7ly9vcfdl7zbpdiwxaz6rrhzkg2irm12dc8")))

(define-public crate-serpico-0.0.2 (c (n "serpico") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "03vcxhd7knb3pam7mmp6q259b900g0swd7amh4v8k91n7ak24jyp")))

(define-public crate-serpico-0.0.3 (c (n "serpico") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "0gmn4x502gylbhyk8jw4xscdhcp1zq14s447m44hdkinb73sa0q4")))

