(define-module (crates-io se rp serp) #:use-module (crates-io))

(define-public crate-serp-0.1.0 (c (n "serp") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.3") (d #t) (k 0)))) (h "0f4i2ib7m7vcy9i09lwxnh12fb0dipb4pi0830xf3542n51nh5gd")))

(define-public crate-serp-0.1.1 (c (n "serp") (v "0.1.1") (d (list (d (n "handlebars") (r "^4.3") (d #t) (k 0)))) (h "0a4fi4yy7jdslgvc5h15zkkbkjy73ycgpz7v4jnbd1kmxyhppcp5")))

