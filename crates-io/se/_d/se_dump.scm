(define-module (crates-io se _d se_dump) #:use-module (crates-io))

(define-public crate-se_dump-0.1.0 (c (n "se_dump") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)))) (h "01zl8nj7phwv5vi2pmfxl93hwsl1cs19cdjqhxz5w5r9r5kz1sil")))

