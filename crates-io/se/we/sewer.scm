(define-module (crates-io se we sewer) #:use-module (crates-io))

(define-public crate-sewer-0.1.0 (c (n "sewer") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sewer-replacement") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hr8isn0wmmryisqbwm48i0daridalww4d9znmghalyr1r4z19qf")))

