(define-module (crates-io se we sewer-replacement) #:use-module (crates-io))

(define-public crate-sewer-replacement-0.1.0 (c (n "sewer-replacement") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v1m2w4bf5lf7p0pk99jif203z9yfwqq4jarvl0w8xkxxhya62ha")))

