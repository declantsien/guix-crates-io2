(define-module (crates-io se rl serlp) #:use-module (crates-io))

(define-public crate-serlp-0.1.0 (c (n "serlp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0ziq7xdg12pb3fdfhhvbw5skppihjfkbkrll534n8jc3974gprl1") (y #t) (r "1.57")))

(define-public crate-serlp-0.1.1 (c (n "serlp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1gj2s0gmmvmcmqmiz7k5ys6jdvvkdrhrh4xma472bvbbprz3piid") (y #t) (r "1.57")))

(define-public crate-serlp-0.1.2 (c (n "serlp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0ivvck1wwn3zfkhw7f9z3y29xrjsdqlmkc7vs7b48wqy9r2wj3wy") (y #t) (r "1.57")))

(define-public crate-serlp-0.1.3 (c (n "serlp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1mj85yzpqjfrgwbrnm1qhxspwpywyr3yd3mrzbi010mfflsi4gwl") (y #t) (r "1.57")))

(define-public crate-serlp-0.1.4 (c (n "serlp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1mmhhwczia1lzzdsj955rv0cza5a0hm8az67idm439dyxyd5wn3h") (y #t) (r "1.57")))

(define-public crate-serlp-0.2.0 (c (n "serlp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1widwmsk4fijwwp6h6dfax7c4dfjabdbkf0qxsrgkzifr3n4qh1n") (y #t) (r "1.57")))

(define-public crate-serlp-0.2.1 (c (n "serlp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1gk1vbycray4rk4wl6mqwyd1i7ac4gk64dvb1pgdngy3rv3v7dpn") (y #t) (r "1.57")))

(define-public crate-serlp-0.2.2 (c (n "serlp") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "06i8142yrskjn958wv6f4z3n6w0pkim2a0wa8s47xk4rfqqxm66g") (y #t) (r "1.57")))

(define-public crate-serlp-0.3.0 (c (n "serlp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1ldpg42ivhj2cjh0rzyy1b9003fsmzqcllvdqa6yz3w4rk225cvh") (r "1.57")))

(define-public crate-serlp-0.3.1 (c (n "serlp") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0a3n0pm4mp5p68iyq550zdg6ak1bl7nvgxpv1qva9a27dgldlmmq") (r "1.57")))

