(define-module (crates-io se qk seqknock-common) #:use-module (crates-io))

(define-public crate-seqknock-common-0.1.0 (c (n "seqknock-common") (v "0.1.0") (d (list (d (n "async-io") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "caps") (r "^0.5") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "06ibl3y7pgil2pa20d8j05i6qapm11sqjhnj30bh1y1x3pv8idcg") (f (quote (("async" "async-io" "async-std"))))))

