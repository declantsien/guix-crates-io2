(define-module (crates-io se qk seqknock-proxycmd) #:use-module (crates-io))

(define-public crate-seqknock-proxycmd-0.2.0 (c (n "seqknock-proxycmd") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "seqknock-common") (r "^0.1") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1ayir4cpxmvka6kql0x5k1s0939flbnj47484c36vhj6xhr4x7p4")))

