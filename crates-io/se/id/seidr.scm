(define-module (crates-io se id seidr) #:use-module (crates-io))

(define-public crate-seidr-0.2.0 (c (n "seidr") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive" "cargo" "env" "help"))) (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.4") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "relative-path") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "019j5gmfvwcg3alr14wxfpx9603cbc9hyigwviwgxpcnirfnhpd8") (r "1.70.0")))

