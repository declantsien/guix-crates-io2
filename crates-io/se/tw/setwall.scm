(define-module (crates-io se tw setwall) #:use-module (crates-io))

(define-public crate-setwall-1.0.1 (c (n "setwall") (v "1.0.1") (d (list (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0q9l5x028g6303yfqhh24dax9927ap2l2gwnd7n34ls8ncp81pvn")))

(define-public crate-setwall-1.0.2 (c (n "setwall") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1lrc40vp7gp4pyykk5pd2rsming2m5zg5qc6lggmaakywik5jq6m")))

(define-public crate-setwall-1.0.3 (c (n "setwall") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "03zalcdjyj159hg783yj06z7frlap0w79xp2v932vdl3jgpvm601")))

(define-public crate-setwall-1.1.0 (c (n "setwall") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yp4z85xw4pbkl6m0h5bad09x6j50z5i1as33b71r3s9vm8lm8wl")))

(define-public crate-setwall-1.2.0 (c (n "setwall") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0la28hygyd4a3aypprjsvawyqmg53s31n9q10k6jay0p8f6y55p6")))

