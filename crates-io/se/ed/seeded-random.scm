(define-module (crates-io se ed seeded-random) #:use-module (crates-io))

(define-public crate-seeded-random-0.1.0 (c (n "seeded-random") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dz61s63aggzwil4b70qqrsxm09zc1r1qiavv8dwjv7d75ip36g1")))

(define-public crate-seeded-random-0.2.0 (c (n "seeded-random") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0gsb1p67kw58ijl67mkwr2mfjd1gdwialy1hyv7jyv2n3s524g11")))

(define-public crate-seeded-random-0.3.0 (c (n "seeded-random") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sgqvd121c4h4nx2mh6wkycjyvbvl25371ir04lsyi5c4yiiqhr3")))

(define-public crate-seeded-random-0.3.1 (c (n "seeded-random") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "uuid") (r "^1.1") (o #t) (k 0)))) (h "03sxv04lgqxjlhr8pq9imysw90mz4qx6gj4nh1xddpa6y58h3lfw") (f (quote (("std" "rand/std" "rand/std_rng") ("rng" "rand/rand_chacha" "rand/small_rng") ("default" "rng"))))))

(define-public crate-seeded-random-0.4.0 (c (n "seeded-random") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "uuid") (r "^1.1") (o #t) (k 0)))) (h "09wb8hvcccyqpy94y7vl0z7lc07pnj3b5xxdd9p7cj8kn6dxfrc8") (f (quote (("std" "rand/std" "rand/std_rng") ("rng" "rand/rand_chacha" "rand/small_rng") ("default" "rng"))))))

(define-public crate-seeded-random-0.5.0 (c (n "seeded-random") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "uuid") (r "^1.1") (o #t) (k 0)))) (h "0jxrxqi32shclj6gnj9s997z2l06jbgs5g8q7p328pv5pwgv1bil") (f (quote (("std" "rand/std" "rand/std_rng") ("rng" "rand/rand_chacha" "rand/small_rng") ("default" "rng"))))))

(define-public crate-seeded-random-0.6.0 (c (n "seeded-random") (v "0.6.0") (d (list (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "uuid") (r "^1.1") (o #t) (k 0)))) (h "0mpja63hs446z18c91ib7dsd5jl7affkqsqrn5cdwbk24m638yr3") (f (quote (("std" "rand/std" "rand/std_rng") ("rng" "rand/rand_chacha" "rand/small_rng") ("default" "rng"))))))

