(define-module (crates-io se ed seed-datepicker) #:use-module (crates-io))

(define-public crate-seed-datepicker-0.1.0 (c (n "seed-datepicker") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive-getters") (r "^0.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "mockall") (r "^0.9") (d #t) (k 2)) (d (n "mockall_double") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "1bf44z3kzw8gfppw9ychnidrswnv35szxcwxj3mfmli7j05fvjpc")))

(define-public crate-seed-datepicker-0.1.1 (c (n "seed-datepicker") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive-getters") (r "^0.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "mockall") (r "^0.9") (d #t) (k 2)) (d (n "mockall_double") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "0iywix96jlf0drynmr9k8wxq8mya06xdfs7nvbwlbim78jfw2scq")))

(define-public crate-seed-datepicker-1.0.0 (c (n "seed-datepicker") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-datepicker-core") (r "^1.0") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "1s2v138s4gb7cifaqljw2kj1w8wvv2sgvmlcpigfnww4qidwprfq")))

(define-public crate-seed-datepicker-1.0.1 (c (n "seed-datepicker") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-datepicker-core") (r "^1.0") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "1899d04qjazsj4xfmz9nxd9ahfg0r6xkwpjjp0bpr1jmyq0i1gga")))

(define-public crate-seed-datepicker-1.0.2 (c (n "seed-datepicker") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono-datepicker-core") (r "^1.0") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "0zqp9sphawfaxmglmj363y5bn7951yy4gdvfm7s7md3wj1zdwz1z")))

(define-public crate-seed-datepicker-1.1.0 (c (n "seed-datepicker") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono-datepicker-core") (r "^1.0") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "seed") (r "^0.9") (d #t) (k 0)))) (h "1gvy76pha0m1w7myavfjliilkrnm9myn9vxqn6h8n2gxi8cwzxxh")))

