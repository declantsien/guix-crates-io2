(define-module (crates-io se ed seedstudio-gd32v) #:use-module (crates-io))

(define-public crate-seedstudio-gd32v-0.1.0 (c (n "seedstudio-gd32v") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "gd32vf103xx-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (d #t) (k 2)))) (h "08gagi626gkph85rwkdq22xbi0q5jhpvmsb3gdjzjbnp5089wmhb")))

