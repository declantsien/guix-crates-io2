(define-module (crates-io se ed seed-xor) #:use-module (crates-io))

(define-public crate-seed-xor-0.1.0 (c (n "seed-xor") (v "0.1.0") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)))) (h "04prqmpa7qfvmp4qsgkn9vgahi8i5iracv5596v97995qq07qcr4") (y #t)))

(define-public crate-seed-xor-0.1.1 (c (n "seed-xor") (v "0.1.1") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)))) (h "0n1gi3q0bjgqfwv8sybmf3zdh18nbkya37j6z3vi9cxs138fxns9")))

(define-public crate-seed-xor-1.0.0 (c (n "seed-xor") (v "1.0.0") (d (list (d (n "bip39") (r "1.0.*") (d #t) (k 0)))) (h "0kyb31nh2mrmkypvyr34qk1zrdbs21f7yfpcr9fibfwmd5x3phx9") (y #t)))

(define-public crate-seed-xor-0.2.0 (c (n "seed-xor") (v "0.2.0") (d (list (d (n "bip39") (r "1.0.*") (d #t) (k 0)))) (h "0101myqxvv4234b2dqm7ag3ks5gq8i9bax8asidd7935whfrm6gc")))

