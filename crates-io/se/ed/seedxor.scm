(define-module (crates-io se ed seedxor) #:use-module (crates-io))

(define-public crate-seedxor-1.0.0 (c (n "seedxor") (v "1.0.0") (d (list (d (n "bip39") (r "^2.0") (k 0)) (d (n "getrandom") (r "^0.2") (k 0)))) (h "16v9rjmndkbzn4bfqyapw14076hrjvhxphnbnp25ganx172vvlhq")))

(define-public crate-seedxor-1.1.0 (c (n "seedxor") (v "1.1.0") (d (list (d (n "bip39") (r "^2.0") (k 0)) (d (n "getrandom") (r "^0.2") (k 0)) (d (n "permutohedron") (r "^0.2.4") (k 0)))) (h "1ys69pgdsx2kzdsjsl1ycllpgangkj24mgz9fhsk18r9avgsi2a1")))

