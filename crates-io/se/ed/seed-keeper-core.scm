(define-module (crates-io se ed seed-keeper-core) #:use-module (crates-io))

(define-public crate-seed-keeper-core-0.1.0 (c (n "seed-keeper-core") (v "0.1.0") (d (list (d (n "aes-kw") (r "^0.2.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)))) (h "0pl8wxvxprnxidgm1vsz07qx4bbcjy6px0icpq99ylpryws85hnl")))

(define-public crate-seed-keeper-core-0.3.1 (c (n "seed-keeper-core") (v "0.3.1") (d (list (d (n "aes-kw") (r "^0.2.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "11igjggrmr6iwjkaqfpqq8992n2qrcrbmfr3j94x3rfciza4k8h9")))

(define-public crate-seed-keeper-core-0.3.2 (c (n "seed-keeper-core") (v "0.3.2") (d (list (d (n "aes-kw") (r "^0.2.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0kz91rfd9bwjaavy3rrdk1hikgnrpsj4qklx9mammslmxlizqxd1")))

