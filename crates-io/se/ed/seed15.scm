(define-module (crates-io se ed seed15) #:use-module (crates-io))

(define-public crate-seed15-0.1.0 (c (n "seed15") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "userspace-rng") (r "^0.1") (d #t) (k 0)))) (h "0drr5irgnym2rs4x6swdzm43ib9a5s0bi7ach58sjja9qq53rv52")))

(define-public crate-seed15-0.1.1 (c (n "seed15") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "userspace-rng") (r "^0.1") (d #t) (k 0)))) (h "065l2175fz28fli1x48a4mlxc8cgcaj6is2d4c3s2vjn8d2ilqmg")))

(define-public crate-seed15-0.1.2 (c (n "seed15") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "userspace-rng") (r "^0.1") (d #t) (k 0)))) (h "1qx63xpa0lfnkm8hblld31j3xwwk3sgwn7g9fakqk5arsq9zq264")))

(define-public crate-seed15-0.1.3 (c (n "seed15") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "userspace-rng") (r "^0.1") (d #t) (k 0)))) (h "1gg3gjpxh3qfsp7z8xmsc2jyvpqj8rcrz1ydw2xmycb0fhymkrck")))

(define-public crate-seed15-0.1.5 (c (n "seed15") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dictionary-1024") (r "^0.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "userspace-rng") (r "^1") (d #t) (k 0)))) (h "19wnd2man1d792z9y4rw3azrd54mf9lzgkk0wn387i2bdxa0fd7w")))

