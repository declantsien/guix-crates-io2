(define-module (crates-io se ed seed-rs) #:use-module (crates-io))

(define-public crate-seed-rs-0.1.0 (c (n "seed-rs") (v "0.1.0") (h "1jfc2rxx4wh67nrcvv3p1p5dfck75y79zylmm867fk3bixqq6iak")))

(define-public crate-seed-rs-0.2.0 (c (n "seed-rs") (v "0.2.0") (h "1kk3hdy5ifbhfppc082pggxwrbwlvyg6vfgfmz9xdds5nh0xbz22")))

