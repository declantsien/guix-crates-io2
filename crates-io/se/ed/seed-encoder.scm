(define-module (crates-io se ed seed-encoder) #:use-module (crates-io))

(define-public crate-seed-encoder-0.1.0 (c (n "seed-encoder") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04xi1in446lnvrxcan9yzv19rycxbjd9syx9fv17bil1scp337nr")))

