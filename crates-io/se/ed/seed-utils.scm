(define-module (crates-io se ed seed-utils) #:use-module (crates-io))

(define-public crate-seed-utils-0.1.0 (c (n "seed-utils") (v "0.1.0") (d (list (d (n "bip85") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin") (r ">=0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "seed-xor") (r "^0.2.0") (d #t) (k 0)) (d (n "xyzpub") (r "^0.2.1") (d #t) (k 0)))) (h "0rhyss9v97iqmd0jbydv9igpvhcp2h44klj0ppiqvxqss99lqjf8")))

