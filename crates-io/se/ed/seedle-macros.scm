(define-module (crates-io se ed seedle-macros) #:use-module (crates-io))

(define-public crate-seedle-macros-0.0.1 (c (n "seedle-macros") (v "0.0.1") (d (list (d (n "cddl-cat") (r "^0.6.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pj29mjf6psx17agq4wxwfmcjdwn68iw9jnj6lkpdknpwy809nnm") (f (quote (("testing"))))))

(define-public crate-seedle-macros-1.7.0 (c (n "seedle-macros") (v "1.7.0") (d (list (d (n "cddl-cat") (r "^0.6.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "17d17zz17xpvlxgx519y7hvccsdd9vcys29bvypx96gk0rsnyr3i") (f (quote (("testing"))))))

(define-public crate-seedle-macros-1.7.1 (c (n "seedle-macros") (v "1.7.1") (d (list (d (n "cddl-cat") (r "^0.6.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q594ifagwznmraqvmjscrqlmajls1kkc4mxhcd0ckcqybdcbbcg") (f (quote (("testing"))))))

