(define-module (crates-io se ed seedbox) #:use-module (crates-io))

(define-public crate-seedbox-0.1.0 (c (n "seedbox") (v "0.1.0") (d (list (d (n "blake2_rfc") (r "^0.0.1") (d #t) (k 0) (p "blake2-rfc_bellman_edition")) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0wp2cp8858iqz9gpvylkw1y483swc77dm1mkw1kmffrylz0770qz") (f (quote (("default"))))))

(define-public crate-seedbox-0.1.1 (c (n "seedbox") (v "0.1.1") (d (list (d (n "blake2_rfc") (r "^0.0.1") (k 0) (p "blake2-rfc_bellman_edition")) (d (n "byteorder") (r "^1.3.4") (k 0)))) (h "1dv43nxbiww8zq6aha4qvkpjbqksp2h3chihcvb2jk3v8akglbiw") (f (quote (("std" "blake2_rfc/std" "byteorder/std") ("default" "std"))))))

(define-public crate-seedbox-0.2.0 (c (n "seedbox") (v "0.2.0") (d (list (d (n "rand_chacha") (r "^0.3.1") (k 0)) (d (n "rand_core") (r "^0.6.0") (k 0)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "0s1cbbf9bpjcxnk2kyxzb38zandlyvq0qshq3zd2nsi9spnhzqrs") (f (quote (("std" "rand_chacha/std" "rand_core/std" "sha3/std") ("default" "std"))))))

