(define-module (crates-io se mx semx_bsp) #:use-module (crates-io))

(define-public crate-semx_bsp-0.1.0 (c (n "semx_bsp") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0i30fnir84cxypza99jp6g1kg921mafw1lng5xlh549716symwkj") (r "1.78")))

(define-public crate-semx_bsp-0.1.1 (c (n "semx_bsp") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ncxlsiy917dhwaf0pik5zdg44wyl3in74yivrscphdz4pflpza7") (r "1.78")))

(define-public crate-semx_bsp-0.1.2 (c (n "semx_bsp") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "16b3mgxzlmx38knjb1iykk44bb1n5s8zkj6d6qzzdqchyy5x1xwj") (r "1.78")))

