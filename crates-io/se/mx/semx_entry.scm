(define-module (crates-io se mx semx_entry) #:use-module (crates-io))

(define-public crate-semx_entry-0.1.0 (c (n "semx_entry") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zkiccb2g3yva8l0hif6vnwl7k08z3wqjsiwqfxp9q9ks111pvha") (r "1.78")))

(define-public crate-semx_entry-0.1.1 (c (n "semx_entry") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0swczbw9aldjk7fh6rl1cp7nnqqpafrca4zzpjkjzkga7ynpwd57") (r "1.78")))

(define-public crate-semx_entry-0.1.2 (c (n "semx_entry") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pjr4jpl8m1x6mdfcgsvg3vy41wfg5fb8jqr6dpy98wvx5qh7j8w") (r "1.78")))

(define-public crate-semx_entry-0.1.4 (c (n "semx_entry") (v "0.1.4") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "1l5dnmr6mqh7ydgwk91hmh4siqjip5xzy4jvyixiv3b5pq0b89h7") (r "1.78")))

(define-public crate-semx_entry-0.1.5 (c (n "semx_entry") (v "0.1.5") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "02qdkpqa726ljsk6rxgimn9fxgv0gpd74nnfzh3q897frv3jiyhg") (r "1.78")))

(define-public crate-semx_entry-0.1.6 (c (n "semx_entry") (v "0.1.6") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "1rzs5fiklw8j3mqzz5f65gh0w9icczrrw5xinlb2gj8b8bwa0j4b") (r "1.78")))

(define-public crate-semx_entry-0.1.7 (c (n "semx_entry") (v "0.1.7") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "0kcdjd0bvg9flynkmikzlb7vxndj4qdnc20h628y1mlprrng7gbz") (r "1.78")))

(define-public crate-semx_entry-0.1.8 (c (n "semx_entry") (v "0.1.8") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "0ab5r2lgkqpfc18gclwllbspyd6khl45y8n7yv83yb59hvpdqzd5") (r "1.78")))

(define-public crate-semx_entry-0.1.9 (c (n "semx_entry") (v "0.1.9") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "1pg7pd7sj2fiqwv26yvzw067pv9a25zss5mrrs09q52wbxi89lnb") (r "1.78")))

(define-public crate-semx_entry-0.1.10 (c (n "semx_entry") (v "0.1.10") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "1yfbjjvxq2k8ibwwy14bc87ndwis3nrq6phwlmf95nw9hxfml93b") (r "1.78")))

(define-public crate-semx_entry-0.1.11 (c (n "semx_entry") (v "0.1.11") (d (list (d (n "aarch64_define") (r "^0.1") (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "task_define") (r "^0.1") (k 1)))) (h "1zg2sqh6yd4pnlgzak9r3wxmf33nnfx0w5smjarmjms5b0yhl0xz") (r "1.78")))

