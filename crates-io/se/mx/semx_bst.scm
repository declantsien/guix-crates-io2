(define-module (crates-io se mx semx_bst) #:use-module (crates-io))

(define-public crate-semx_bst-0.1.0 (c (n "semx_bst") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ackpjs8226c0jga8k129vw4irf17zsc5g848l7pviamd792fxx8") (r "1.78")))

