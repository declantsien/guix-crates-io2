(define-module (crates-io se mx semx_processor) #:use-module (crates-io))

(define-public crate-semx_processor-0.1.0 (c (n "semx_processor") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0fw38k62ky1imjgk750gis0hg8a7lq08irn25daj63cmlh5cqsmc") (r "1.78")))

(define-public crate-semx_processor-0.1.1 (c (n "semx_processor") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "18jbr3fiswca5dwf6xrrxk8hs2l0biipqgfjj4xixwgby6aljn6n") (r "1.78")))

