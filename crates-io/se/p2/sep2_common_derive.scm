(define-module (crates-io se p2 sep2_common_derive) #:use-module (crates-io))

(define-public crate-sep2_common_derive-0.1.0 (c (n "sep2_common_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r2hfz9l8vrjiq19wn38945h30lmv2ifml4yxmr1yka1vpfn2281")))

