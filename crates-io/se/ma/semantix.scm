(define-module (crates-io se ma semantix) #:use-module (crates-io))

(define-public crate-semantix-0.1.0 (c (n "semantix") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b1iw9w2km3qlclhwkzc7padiwxamhflq12zykf9rgcf8rncwj45")))

(define-public crate-semantix-0.2.0 (c (n "semantix") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f31v6ma1nn6868mgl2r3liq9q6z9zx2xmbm3k34lkiiznf6klfm")))

(define-public crate-semantix-0.2.1 (c (n "semantix") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vjb0n1h2hdhdhr0ywfq51pd0ajdp66dmpja44rjnx1mgyhfv9x5")))

