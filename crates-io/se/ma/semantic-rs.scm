(define-module (crates-io se ma semantic-rs) #:use-module (crates-io))

(define-public crate-semantic-rs-0.0.1 (c (n "semantic-rs") (v "0.0.1") (h "1ggh1hpqgm7022mv2b52jj3bvvs6685gqf3z9yx5mpaizyi5kbah")))

(define-public crate-semantic-rs-1.0.0 (c (n "semantic-rs") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "< 0.1.0") (o #t) (d #t) (k 0)) (d (n "clog") (r "^0.9.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)) (d (n "hubcaps") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "semver") (r "^0.2.1") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "travis-after-all") (r "^2.0.0") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "01jm068gq1klmipmjqzcrjj8i76yfjmfw4iz9mdypfl7gawa15xc") (f (quote (("dev" "clippy") ("default"))))))

