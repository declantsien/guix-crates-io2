(define-module (crates-io se ma semalock) #:use-module (crates-io))

(define-public crate-semalock-0.1.0 (c (n "semalock") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "00z1hdv3hc8myz2fzgmr6fxb3yxcvrgfa7y385iwc71h9agazwyr")))

(define-public crate-semalock-0.1.1 (c (n "semalock") (v "0.1.1") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1dx624qq9hwsn05hw1i9cy7wqvimg5r3bca17yw2cq2ldny6nr78")))

(define-public crate-semalock-0.2.0 (c (n "semalock") (v "0.2.0") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1agm2lmf1fckcjgawr2fk60yabzl65i1jil1xwrdy31jlcs7dg1x")))

(define-public crate-semalock-0.3.0 (c (n "semalock") (v "0.3.0") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0ac1fyvhb4yyldfd054drr62yvxbv96v6zj3r76r1gdyzyx6c34q")))

(define-public crate-semalock-0.3.1 (c (n "semalock") (v "0.3.1") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1jj2j7ams6wi60325pmmn36wvc02mw77kl5sraxfwvvwfw7jd7x8")))

