(define-module (crates-io se ma semaphorus) #:use-module (crates-io))

(define-public crate-semaphorus-0.1.0 (c (n "semaphorus") (v "0.1.0") (h "0kbcxwfqp21qbln4ai89zqsw7lw99n90pbnikijr8lpib1q3j65g") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-semaphorus-0.2.0 (c (n "semaphorus") (v "0.2.0") (h "0kg5cr8ygs9bxs9vr5slddfikk4mxl5i77zzy2049xxkzalsvzkn") (f (quote (("wrapper") ("std") ("nightly") ("default" "std" "wrapper")))) (y #t)))

(define-public crate-semaphorus-0.2.1 (c (n "semaphorus") (v "0.2.1") (h "1hbr9aqhc2n3qnyqk0ksrx982miwpgfshys2lgf3rfb6pwlc6c5c") (f (quote (("wrapper") ("std") ("nightly") ("default" "std" "wrapper"))))))

