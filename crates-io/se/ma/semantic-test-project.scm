(define-module (crates-io se ma semantic-test-project) #:use-module (crates-io))

(define-public crate-semantic-test-project-1.2.0 (c (n "semantic-test-project") (v "1.2.0") (h "1pkal4a12sbzn8id57i73zfq9psyyc6v6yyrzy4x7fvr8fj6rnfc")))

(define-public crate-semantic-test-project-1.9.0 (c (n "semantic-test-project") (v "1.9.0") (h "183b5fw9sbpc4dqdafwjl0bijpa3mm5wkp2k3nmyvgraaa159plf")))

(define-public crate-semantic-test-project-2.0.0 (c (n "semantic-test-project") (v "2.0.0") (h "0m0wz7rbp0j52wdk8a3bylc98yw0v067kbiw75nwyksiwdj7k8fr")))

