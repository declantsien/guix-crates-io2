(define-module (crates-io se ma semantic-exit) #:use-module (crates-io))

(define-public crate-semantic-exit-1.0.0 (c (n "semantic-exit") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1imyyy2lshyi7jjczsv9fvg40zlq0nki74bzlg4mfa6md8wxzdy6")))

