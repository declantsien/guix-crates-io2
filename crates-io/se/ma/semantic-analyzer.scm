(define-module (crates-io se ma semantic-analyzer) #:use-module (crates-io))

(define-public crate-semantic-analyzer-0.0.0 (c (n "semantic-analyzer") (v "0.0.0") (d (list (d (n "nom_locate") (r "^4.1") (d #t) (k 0)))) (h "0xxpnkb2rvnv5pdin6yzjas3nxpp0bpwn7yybdncck125amx9v8y")))

(define-public crate-semantic-analyzer-0.1.0 (c (n "semantic-analyzer") (v "0.1.0") (d (list (d (n "nom_locate") (r "^4.1") (d #t) (k 0)))) (h "0w3iyygcc05y66p7c4xj0k2va0v1bxwp7gqn3mbjmfsbivs2rkkm")))

(define-public crate-semantic-analyzer-0.1.1 (c (n "semantic-analyzer") (v "0.1.1") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0xp99b58j4jgzkp6lszc7s3h0qz653x52lx01fvgyykywpw59214")))

(define-public crate-semantic-analyzer-0.2.0 (c (n "semantic-analyzer") (v "0.2.0") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0zlhvy09cknfh2hl1jnnxlbxqsi8akqa1m226cyymhhw173aqczm")))

(define-public crate-semantic-analyzer-0.2.1 (c (n "semantic-analyzer") (v "0.2.1") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "1hwyc007792ga3z5b61i505qmlrwcl9lp7xmi1s30j1lzynm0mk2")))

(define-public crate-semantic-analyzer-0.2.2 (c (n "semantic-analyzer") (v "0.2.2") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0lza7qv6ab3w2pfy9yzhrdvcq3bjwzajp3m0r40nhkp3vhzlaqg1")))

(define-public crate-semantic-analyzer-0.2.3 (c (n "semantic-analyzer") (v "0.2.3") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0prsiq2is05zlpjl674srkc4dnliqqm67ydqisafkpf6rcri4508")))

(define-public crate-semantic-analyzer-0.2.4 (c (n "semantic-analyzer") (v "0.2.4") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0vfhqcdb4j72cf4yb1ighxgkrqmsvjs4p0vrna6dih4lpdw140wb")))

(define-public crate-semantic-analyzer-0.2.6 (c (n "semantic-analyzer") (v "0.2.6") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0dhwkr9hn97di9ix8rrzgc78baba68981pb8snl58sa3858fdmhs")))

(define-public crate-semantic-analyzer-0.2.5 (c (n "semantic-analyzer") (v "0.2.5") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "0cz2d7qmk4ak76bgh0fv1ifgd9h7rbkrs06mlaafasvyg4626c51")))

(define-public crate-semantic-analyzer-0.3.0 (c (n "semantic-analyzer") (v "0.3.0") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "1drrciag6zjbg6n9z8abvbhwxnlcnd2hblwli8dppxjycyqlkmh1")))

(define-public crate-semantic-analyzer-0.3.1 (c (n "semantic-analyzer") (v "0.3.1") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "127h6zhbj4s1vlxvkm8bfa37vz9x52wln31n5k10inn2d71h8y4i")))

(define-public crate-semantic-analyzer-0.3.2 (c (n "semantic-analyzer") (v "0.3.2") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)))) (h "01dxv539w7pd239c1n24zs7jd63357iw6f56amxmbpxzhjdw8j6x")))

(define-public crate-semantic-analyzer-0.3.3 (c (n "semantic-analyzer") (v "0.3.3") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1m3lfckvfmc7aqnpl0csw54a44kxvcirgrbjrydcrv9ln17hl8jk") (f (quote (("codec" "serde"))))))

(define-public crate-semantic-analyzer-0.4.0 (c (n "semantic-analyzer") (v "0.4.0") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1m7dbyw6dif220xi9781fck4yp93rq0jh3g6vpxxpx83f0pz60r6") (f (quote (("codec" "serde"))))))

(define-public crate-semantic-analyzer-0.4.1 (c (n "semantic-analyzer") (v "0.4.1") (d (list (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ny96iiidrm0rjdjf72sya0kpi882jsi7n5j84xqyfvww0cww97x") (f (quote (("codec" "serde"))))))

