(define-module (crates-io se ma semanticscholar) #:use-module (crates-io))

(define-public crate-semanticscholar-0.1.0 (c (n "semanticscholar") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pz0qp5n2vczkda34zf4163g27anwr5jmmm38jj2n4rpbnrqnxpx")))

