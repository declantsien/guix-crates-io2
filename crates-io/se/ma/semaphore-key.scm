(define-module (crates-io se ma semaphore-key) #:use-module (crates-io))

(define-public crate-semaphore-key-0.1.0 (c (n "semaphore-key") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1c953kj124m83da99zvz5q99cf1yvfmhplsi294s5wgbx0kma511")))

(define-public crate-semaphore-key-0.2.0 (c (n "semaphore-key") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1m8i0431jj9zsm5vxmvcgp01cnyf2li4jv1cik0nsd7nrcplcqyh")))

(define-public crate-semaphore-key-1.0.0 (c (n "semaphore-key") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1bhq63in3k499n782ark156969rz9fhwsfhfrmy9s9wf4xgmv15d")))

(define-public crate-semaphore-key-1.0.1 (c (n "semaphore-key") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ffamiw4yzvg1ybgy30558jh3zbxmk26q9cv4k1zq90ld1rjzjpp")))

(define-public crate-semaphore-key-1.0.2 (c (n "semaphore-key") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1pc3xcl9ariy8p8iql87bffnxx8nnak6ndr4rjzilkb3841ilzk1")))

(define-public crate-semaphore-key-1.0.3 (c (n "semaphore-key") (v "1.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "076g540nsdsgqgm76r8k28wnirk5imrbq8q4g74yxk5gwzwmiv3k")))

(define-public crate-semaphore-key-1.0.4 (c (n "semaphore-key") (v "1.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "023jzxwrld27bkq39xsajgly4p5scb1sm8wn0qrliqnprjvfalmz")))

