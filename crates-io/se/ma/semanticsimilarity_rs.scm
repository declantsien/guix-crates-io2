(define-module (crates-io se ma semanticsimilarity_rs) #:use-module (crates-io))

(define-public crate-semanticsimilarity_rs-0.1.0 (c (n "semanticsimilarity_rs") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0fmzbi5m7737xzhyn80gj86hwny2a641vzafjy3qk6pai9vpw33a")))

(define-public crate-semanticsimilarity_rs-0.1.1 (c (n "semanticsimilarity_rs") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "045cwi2m7kfsm6yqkcjfjdrj28nd493vhw6181afppk99gmzl4n2")))

