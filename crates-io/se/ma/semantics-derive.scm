(define-module (crates-io se ma semantics-derive) #:use-module (crates-io))

(define-public crate-semantics-derive-0.1.0 (c (n "semantics-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s1dnql309jv6syiv77h71qmslz2s0739f2i9s8p1xaljrcwzayv")))

