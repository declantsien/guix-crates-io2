(define-module (crates-io se ma sema) #:use-module (crates-io))

(define-public crate-sema-0.1.0 (c (n "sema") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0v6gk9g9l7fm361yd75z96c5jhkyvl2xmc2r8pqh78lhchc7av1m")))

(define-public crate-sema-0.1.1 (c (n "sema") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "139dzrgh4w1a5rrgm02qhklq720hi7cqxv1l03nix8f7hr6jxwy1")))

(define-public crate-sema-0.1.2 (c (n "sema") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0gs3z0fi46b2jaz13c56xkw7i25pw3f6awqxn987f0b42fck2lkx")))

(define-public crate-sema-0.1.3 (c (n "sema") (v "0.1.3") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0q5m006l1w1gkl13nmh0cpd772wbklaz6gn741vmamggg943v7zv")))

(define-public crate-sema-0.1.4 (c (n "sema") (v "0.1.4") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ckq33sg84785p195m54h03jcn7fai8w08hjnb94nzaakgzibbz3")))

