(define-module (crates-io se rt sertools) #:use-module (crates-io))

(define-public crate-sertools-0.1.0 (c (n "sertools") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lw6p5ab98i18nmr0h92z27clx84z31pxb7rqx3fq3v8ipnd8i72")))

(define-public crate-sertools-0.2.0 (c (n "sertools") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00jzxz0lmsk0pi01bhbx16b5yyv2ayv620kda9z77bnc7ldibvi6")))

