(define-module (crates-io se qs seqset) #:use-module (crates-io))

(define-public crate-seqset-0.1.0 (c (n "seqset") (v "0.1.0") (h "0inpji5wmfy75qa7my5h6im7g6896q0n7795fxl00bsd2l2cynjx") (y #t)))

(define-public crate-seqset-0.1.1 (c (n "seqset") (v "0.1.1") (h "03g77nw4jly3cjp9jvr191kpwhampkd0wwn283230lwfsck8z24p")))

