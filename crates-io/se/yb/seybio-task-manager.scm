(define-module (crates-io se yb seybio-task-manager) #:use-module (crates-io))

(define-public crate-seybio-task-manager-0.1.0 (c (n "seybio-task-manager") (v "0.1.0") (h "1xqxxbds5hziwllxyb9bzvxhnpyzkz4pccci5dqqngwmxhxalzm9")))

(define-public crate-seybio-task-manager-0.1.1 (c (n "seybio-task-manager") (v "0.1.1") (h "1l86mcg3nkn05wahzqafj9gr38l4wcvsg3nvd3vshg59rdshap5d")))

