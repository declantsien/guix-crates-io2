(define-module (crates-io se mo semolina) #:use-module (crates-io))

(define-public crate-semolina-0.1.0 (c (n "semolina") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)))) (h "0bc1lx1fxhzwf7fcas1rhlz77jaq609sw23df1l5dwhy932bpkwp") (f (quote (("portable") ("force-adx") ("default")))) (l "semolina")))

(define-public crate-semolina-0.1.1 (c (n "semolina") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)))) (h "1p62iak64j77xyhyiasajpsazpv693k15qr00qvffyhkams4sjyj") (f (quote (("portable") ("force-adx") ("default")))) (l "semolina")))

(define-public crate-semolina-0.1.2 (c (n "semolina") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)))) (h "019fjw0168x01pxw71hzw7ky75hx9nahn63hpbb9a6x06gvmj1z9") (f (quote (("portable") ("force-adx") ("default")))) (l "semolina")))

(define-public crate-semolina-0.1.3 (c (n "semolina") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)))) (h "1j1m5b1fcmdz38dzcfcrjmjn445v8i68p3qgxmmzfl1xbsb2m2wm") (f (quote (("portable") ("force-adx") ("default")))) (l "semolina")))

(define-public crate-semolina-0.1.4 (c (n "semolina") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)))) (h "08zypvli95dfp04h4s8dlz3f7gpkbqla5f860v5vwcd89zyi209b") (f (quote (("portable") ("force-adx") ("default")))) (l "semolina")))

