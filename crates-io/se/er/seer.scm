(define-module (crates-io se er seer) #:use-module (crates-io))

(define-public crate-seer-0.0.1 (c (n "seer") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.6") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "log_settings") (r "^0.1.1") (d #t) (k 0)) (d (n "seer-z3") (r "^0.1.0") (d #t) (k 0)))) (h "0n8ysr1ndangha9j1bpgwhmll5kjdi8334khwjlq62az5d2y0lna")))

(define-public crate-seer-0.0.2 (c (n "seer") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "log_settings") (r "^0.1.1") (d #t) (k 0)) (d (n "seer-z3") (r "^0.1.0") (d #t) (k 0)))) (h "00l3r9z7vld82wldv3cz7smfnvxvs63ldp4hk8w686gjzq31rvr0")))

(define-public crate-seer-0.0.3 (c (n "seer") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "log_settings") (r "^0.1.1") (d #t) (k 0)) (d (n "seer-z3") (r "^0.1.0") (d #t) (k 0)))) (h "01nx62dzfrw24qimb2ab753y5jv6kbib34lzapvxih1jsc9kiywx")))

