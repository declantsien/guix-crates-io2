(define-module (crates-io se ps sepserde) #:use-module (crates-io))

(define-public crate-sepserde-0.8.0 (c (n "sepserde") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sepserde_derive") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "sepserde_derive") (r "^0.8.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1v8jrg3lynj5j62fgy95p4b47w9avl1zyblck7f20zgmz11900gs") (y #t)))

(define-public crate-sepserde-0.8.2 (c (n "sepserde") (v "0.8.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sepserde_derive") (r "^0.8.1") (d #t) (k 0)) (d (n "sepserde_derive") (r "^0.8.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0p5dp1m0l5k602cvp4s8a9vplkm2fl0xz19vsigbdny5chrb5fpa")))

