(define-module (crates-io se ps sepserde_derive) #:use-module (crates-io))

(define-public crate-sepserde_derive-0.8.0 (c (n "sepserde_derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1v6bkil438a63fn4sgi2mxv8i8mvfygbz5zq5z8x9c9ryv5a31h4") (y #t)))

(define-public crate-sepserde_derive-0.8.1 (c (n "sepserde_derive") (v "0.8.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0yh87m4ics05q2z0j6r86iyxfwz91z71wi62j0b0hwia9gnl06ln")))

