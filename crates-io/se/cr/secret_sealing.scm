(define-module (crates-io se cr secret_sealing) #:use-module (crates-io))

(define-public crate-secret_sealing-0.1.0 (c (n "secret_sealing") (v "0.1.0") (d (list (d (n "argon2") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.0.0") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "oqs") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (f (quote ("std"))) (d #t) (k 0)))) (h "1akvwgq4yai3ljb6pj84bc8097zrdnmk336cbfiwsx04xb7zphas") (y #t)))

