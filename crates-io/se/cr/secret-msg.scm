(define-module (crates-io se cr secret-msg) #:use-module (crates-io))

(define-public crate-secret-msg-0.3.0 (c (n "secret-msg") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "12x73pxii43ynddgic0qsqj0qizbmp9c4kacsn9wadxpk54kpn9b")))

(define-public crate-secret-msg-0.3.1 (c (n "secret-msg") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1k7q2qw36ly7xpmgrslzmq60kb0vr5x9rns006b1jn1cxh5flm6h")))

(define-public crate-secret-msg-0.3.2 (c (n "secret-msg") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0mdkpwamigc5zr15s62yn27735f25vwszw63mr5gc5yhq0hdmiy6")))

(define-public crate-secret-msg-0.3.4 (c (n "secret-msg") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1xwva5y1jgldp463d88l2192cir3r42ycgar718sl53brbkwhmwl")))

(define-public crate-secret-msg-0.3.5 (c (n "secret-msg") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1anvsb4hrpdbycm72b86ryr5yiadh5bgpzg175rglf2gag3812rw")))

(define-public crate-secret-msg-0.3.6 (c (n "secret-msg") (v "0.3.6") (d (list (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0hfmwba01r7qkpbddalklxhffx9xikpjqycwl515vgyy8q6m5har")))

