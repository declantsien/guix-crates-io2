(define-module (crates-io se cr secrets-manager-macro) #:use-module (crates-io))

(define-public crate-secrets-manager-macro-0.1.0 (c (n "secrets-manager-macro") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.54.1") (d #t) (k 0)) (d (n "aws-sdk-secretsmanager") (r "^0.24.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.10") (d #t) (k 0)))) (h "0ysah292vfmsn5faflpdc3rappd1q3fskp2np5slkd7p4p9faajj")))

