(define-module (crates-io se cr secret_santa) #:use-module (crates-io))

(define-public crate-secret_santa-0.1.0 (c (n "secret_santa") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "15pxcbpjv3aing6fx4d0059an4hg7hk9lnka9c51ssk0zc9mi5ji")))

