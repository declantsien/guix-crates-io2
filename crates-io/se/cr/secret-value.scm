(define-module (crates-io se cr secret-value) #:use-module (crates-io))

(define-public crate-secret-value-0.1.0 (c (n "secret-value") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.57") (d #t) (k 2)))) (h "0601m8fxj9g7gp04qk937bi1vp3xv99qs0z222jp2qm423dv7jkj")))

