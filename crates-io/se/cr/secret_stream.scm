(define-module (crates-io se cr secret_stream) #:use-module (crates-io))

(define-public crate-secret_stream-0.1.0 (c (n "secret_stream") (v "0.1.0") (d (list (d (n "box_stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "secret_handshake") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.5") (d #t) (k 0)))) (h "1sgskh2mvhxjkmkiv23x23y0dppzz4sx5jdjifc6qnaz0ii08wnw")))

(define-public crate-secret_stream-0.1.1 (c (n "secret_stream") (v "0.1.1") (d (list (d (n "box_stream") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "secret_handshake") (r "^3.0.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.5") (d #t) (k 0)))) (h "194yxccbaazdxird5wvn54jc51s4y7y9kgg82xgm8lyn6dpi92gc")))

(define-public crate-secret_stream-0.1.2 (c (n "secret_stream") (v "0.1.2") (d (list (d (n "box_stream") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "secret_handshake") (r "^3.0.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.5") (d #t) (k 0)))) (h "0wbyighz909qzf3zy34h6wds0wczbby6qss37jrxrp9li8qkigaj")))

(define-public crate-secret_stream-0.2.0 (c (n "secret_stream") (v "0.2.0") (d (list (d (n "box_stream") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "secret_handshake") (r "^4.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.5") (d #t) (k 0)))) (h "0nmw3rlcw4z3ddklzp40za0c3bna5jk5v6zbw6dv9kn03b76hwr3")))

(define-public crate-secret_stream-0.3.0 (c (n "secret_stream") (v "0.3.0") (d (list (d (n "box_stream") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "secret_handshake") (r "^5.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.16") (d #t) (k 0)))) (h "16rk0l4qvh9kq3bjvniynp7k39r0maglnfrdm6n0wj2jsjavg1ag")))

