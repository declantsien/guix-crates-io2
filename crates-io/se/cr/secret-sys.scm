(define-module (crates-io se cr secret-sys) #:use-module (crates-io))

(define-public crate-secret-sys-0.2.0 (c (n "secret-sys") (v "0.2.0") (d (list (d (n "glib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fn4cc1mdazigi3fj294k17zsdq6ya53nbqygyxbkbp8fidzhfhs")))

