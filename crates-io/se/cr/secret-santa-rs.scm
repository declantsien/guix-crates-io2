(define-module (crates-io se cr secret-santa-rs) #:use-module (crates-io))

(define-public crate-secret-santa-rs-1.0.1 (c (n "secret-santa-rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k5l26g7aqij6bvyjdvsbjrdg7mjj9cazjcgkpribkac1v9sp02z")))

(define-public crate-secret-santa-rs-1.0.2 (c (n "secret-santa-rs") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n38nfl79b81w98kaybdq6hq2pi4xw2m3mv9rw3nlzvm2ls2axan")))

(define-public crate-secret-santa-rs-1.0.3 (c (n "secret-santa-rs") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xjdypkql4kch6nlfn2dk4p5jji162l6svjz8azw5wh88jh8gyr0")))

(define-public crate-secret-santa-rs-1.1.0 (c (n "secret-santa-rs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1587xy49mj0bg2rj996ykywr6zbs6h1fjl5h4qv3a052g6gz41x6")))

(define-public crate-secret-santa-rs-1.2.0 (c (n "secret-santa-rs") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n3c78hvvysnlbfrymhm2q787w2gk3p0xrhk08ld3cg35k2w572n")))

(define-public crate-secret-santa-rs-1.2.1 (c (n "secret-santa-rs") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grouper") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gwavknin50nl1984fjg3iwfbgsm7vyfr2avlx5366x81lpbv4rq")))

(define-public crate-secret-santa-rs-1.2.2 (c (n "secret-santa-rs") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grouper") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ps9xk9xbag9k9ynqb4p5vzldzxh50l17ndyg3q7jiibv8cg00ch")))

(define-public crate-secret-santa-rs-1.2.3 (c (n "secret-santa-rs") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grouper") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bjdl1ma29p26h90yi4b7za2iv4sdp5s67884p7ivcqgc1ajc7c7")))

