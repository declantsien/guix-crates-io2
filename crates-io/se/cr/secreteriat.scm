(define-module (crates-io se cr secreteriat) #:use-module (crates-io))

(define-public crate-secreteriat-1.0.0 (c (n "secreteriat") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1i8c0fsci6v3wbn6ykdyi1jacsb03wxr0digdk8ywkcd2qpkgha8")))

(define-public crate-secreteriat-1.1.0 (c (n "secreteriat") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0wsy6m70xs8sjnkac64fljq687997b7an8n7jvmi68r24189b03p")))

(define-public crate-secreteriat-1.1.1 (c (n "secreteriat") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "16iqgx0qwnki5gdld5g2kqiaf83myf5xay3jn1sgqpaxdazbxd7p")))

(define-public crate-secreteriat-1.1.2 (c (n "secreteriat") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1sw98nlfmw618x7zf8s840f7visc1c8bk86qxhsl2hvgx3ajwsi4")))

(define-public crate-secreteriat-1.2.0 (c (n "secreteriat") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "12m0z9zpanash4naqnfbi2kdhlj0bfk4ga0l49jchgvxmma8ymzm")))

(define-public crate-secreteriat-2.0.0 (c (n "secreteriat") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1s60szz6a3b5vksqs7fklh216d69gjmd6i8zzsgbq8f57r9sz9h4")))

(define-public crate-secreteriat-2.0.1 (c (n "secreteriat") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1k37xsgjmwb4sd6369580ky70i92mclc7svyjmbyqfrr2g3kbfia")))

(define-public crate-secreteriat-2.1.0 (c (n "secreteriat") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1na16fxk79xyf3qx0b5bgibq9rd6dx2a507s7ijp61gr2xg9mcn6")))

(define-public crate-secreteriat-2.1.1 (c (n "secreteriat") (v "2.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1p1l70ivdj9jj0d2cp2a1hvdp2z5sk3pmi72y1hpk868wjldn6ab")))

(define-public crate-secreteriat-2.1.2 (c (n "secreteriat") (v "2.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "065ki1r0sidj4vdib9rckd6ni2kgng4xa58qgrgnh4vad91qaxg4")))

(define-public crate-secreteriat-2.2.0 (c (n "secreteriat") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "0nzkkswqszk63q0fac7qffr66dgj9lbakdx7hpr7qv0np03s0d15")))

(define-public crate-secreteriat-2.2.1 (c (n "secreteriat") (v "2.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1y0qgycizwp9r71gkpmgmplmmbfsvj5ighkh5qc7fs5vlrvf0zcf")))

(define-public crate-secreteriat-3.0.0 (c (n "secreteriat") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1zh1nwy0791gkj8psx0pgd649a2mslhhb2bqbpijf8f3s4ik4yx3")))

(define-public crate-secreteriat-3.0.1 (c (n "secreteriat") (v "3.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1sz48p8hcqz1kgzsy5dnqgiiibyifdlpnfy542x8rhds3qhapjq5")))

(define-public crate-secreteriat-3.1.0 (c (n "secreteriat") (v "3.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "05yzjjvp6hpng8gs4krbypkg567lmik7m4h5nfx78jrx2x1raddp")))

(define-public crate-secreteriat-3.1.1 (c (n "secreteriat") (v "3.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "0pxsrwhhwd1f6igmgz06gmbxqcy9z84k0kw4f2if1w5qf8q2irmi")))

(define-public crate-secreteriat-3.1.2 (c (n "secreteriat") (v "3.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "01kbj4bsffv22bn322my1l1c9qb9fm9cqb6ds6g8rswqdavmpnm2")))

