(define-module (crates-io se cr secret_inject) #:use-module (crates-io))

(define-public crate-secret_inject-0.1.0 (c (n "secret_inject") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vc1xwlw6s19yz8pa5rli7kldgm3imi4d9r4kr6wmfsqw5xpggqp")))

(define-public crate-secret_inject-0.1.1 (c (n "secret_inject") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wzbmis3s5lgr1nprm5bpxa6fk31dkpj8qywnh4n1bp1i180dlgp")))

