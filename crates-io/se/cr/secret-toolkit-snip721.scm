(define-module (crates-io se cr secret-toolkit-snip721) #:use-module (crates-io))

(define-public crate-secret-toolkit-snip721-0.2.0 (c (n "secret-toolkit-snip721") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04v2isw5zch1wxzaj3l007fys2r2a3p8pkgjbwb5mjd8d9by34w1")))

(define-public crate-secret-toolkit-snip721-0.3.0 (c (n "secret-toolkit-snip721") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0dlz1sgryy6nkhh2j0zjmy6mr06yvn18yi3qkirhiyg604584iql")))

(define-public crate-secret-toolkit-snip721-0.6.0 (c (n "secret-toolkit-snip721") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xz13ypyql4ixnyxh14pmnbh6hja0ripf9wyl5s1s6gyaz8hzhhg")))

(define-public crate-secret-toolkit-snip721-0.7.0 (c (n "secret-toolkit-snip721") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0p83y1rz25pifi7i5fscnz2a00k7fcgi0bpfip6jnsgsavb1dcm0")))

(define-public crate-secret-toolkit-snip721-0.9.0 (c (n "secret-toolkit-snip721") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^1.1.10") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hzg7xizf8bz1234ybga7p0nmh6ry397y5zcd6d1z2m8bdsbqnxf")))

(define-public crate-secret-toolkit-snip721-0.10.0 (c (n "secret-toolkit-snip721") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.1.11") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wg91qpkf28cwgwsqhswr63ipk2laavymp5qgbw0c8x5s9gv6ang")))

