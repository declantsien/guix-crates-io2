(define-module (crates-io se cr secretkeeper) #:use-module (crates-io))

(define-public crate-secretkeeper-0.1.0 (c (n "secretkeeper") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "0c7gw6jax202h4v3rkbc2f4clj3rjpmsffd38vcwnw22wdqr30n3")))

