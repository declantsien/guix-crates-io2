(define-module (crates-io se cr secret-cosmwasm-storage) #:use-module (crates-io))

(define-public crate-secret-cosmwasm-storage-0.10.0 (c (n "secret-cosmwasm-storage") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "09w3blrffj2xmj0kdi1f88iyqdld50k8ai8mvkf2vh0kpd1k5vpz") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-secret-cosmwasm-storage-1.0.0 (c (n "secret-cosmwasm-storage") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0) (p "secret-cosmwasm-std")) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "02zbzga8h1bn0ymjdsmn0v357j0dbmnc666ng9y23x5bgsbz5xdv") (f (quote (("iterator" "cosmwasm-std/iterator") ("default"))))))

(define-public crate-secret-cosmwasm-storage-1.1.10 (c (n "secret-cosmwasm-storage") (v "1.1.10") (d (list (d (n "secret-cosmwasm-std") (r "^1.1.10") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0x13na8047fgwf9hfcigyiy5shil72k364zb1x42qinlwh9ji22q") (f (quote (("iterator" "secret-cosmwasm-std/iterator") ("default"))))))

(define-public crate-secret-cosmwasm-storage-1.1.11 (c (n "secret-cosmwasm-storage") (v "1.1.11") (d (list (d (n "secret-cosmwasm-std") (r "^1.1.11") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "10x78lf8iw96irgv9gjfna5b8a5q9xwwlnsml5pb2lranwndlhxv") (f (quote (("iterator" "secret-cosmwasm-std/iterator") ("default"))))))

