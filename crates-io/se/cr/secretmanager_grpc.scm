(define-module (crates-io se cr secretmanager_grpc) #:use-module (crates-io))

(define-public crate-secretmanager_grpc-0.1.0 (c (n "secretmanager_grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4.0") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.4.0") (d #t) (k 1)))) (h "1vc157j9ix0jsh3w1rq11v8x11w1bhydh2r9vxvanfnxpmb9bwkb")))

(define-public crate-secretmanager_grpc-0.5.2 (c (n "secretmanager_grpc") (v "0.5.2") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.2") (d #t) (k 1)))) (h "0cjqp4qrdbjxppskj25qd7h6i50yxl96kswb8psxw7daxmnsvqcp")))

