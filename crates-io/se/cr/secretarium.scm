(define-module (crates-io se cr secretarium) #:use-module (crates-io))

(define-public crate-secretarium-0.0.1 (c (n "secretarium") (v "0.0.1") (h "1l9i0l09apfvzbzlv3w4psx8wmxr3g3wnwv8q71a08ydfwm5mk87")))

(define-public crate-secretarium-0.0.2 (c (n "secretarium") (v "0.0.2") (h "1300y8pjwrlcrki71fbywdphg6i6xx85hf8lb850zw1bvixid65x")))

