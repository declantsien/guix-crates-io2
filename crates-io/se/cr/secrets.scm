(define-module (crates-io se cr secrets) #:use-module (crates-io))

(define-public crate-secrets-0.9.0 (c (n "secrets") (v "0.9.0") (h "01p2ncdbnsrzxcncif7p23ngbchp5r57528r5mrk1b9blb6d6117")))

(define-public crate-secrets-0.9.1 (c (n "secrets") (v "0.9.1") (h "0x8gr7kymr6rg1hck6akd9zzzb13hgq6v4bb2narggwlx86ckrj2")))

(define-public crate-secrets-0.10.0 (c (n "secrets") (v "0.10.0") (d (list (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1habs67f378qrbbvcidd3pih1jrq2m6dsiywx66ll70wjrlq4x4j")))

(define-public crate-secrets-0.11.0 (c (n "secrets") (v "0.11.0") (d (list (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1q5pq9vcbvyld9gci5mpnn7l93g30a9cy09dfracgnkywi9daf2g")))

(define-public crate-secrets-0.11.1 (c (n "secrets") (v "0.11.1") (d (list (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1bhd0zsy1mygfjn9v4psjybw9yxq7f3ac9pcr1hsppxs4v8fid17")))

(define-public crate-secrets-0.12.0 (c (n "secrets") (v "0.12.0") (d (list (d (n "ctest") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vw2v0zcn7gvpr5nj7shxd6nlax075yhjk6cnd5fai8jfx42ydqm")))

(define-public crate-secrets-0.12.1 (c (n "secrets") (v "0.12.1") (d (list (d (n "ctest") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "056xb98bn6hwc4kgzy733x97y9bcklxnnp921p1sci8rw1ik0hp4")))

(define-public crate-secrets-1.0.0 (c (n "secrets") (v "1.0.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (f (quote ("use-pkg-config"))) (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r9ry85fpq4dqlypjidzbvwxsdiywj8dsywym1xgzg914370ar41")))

(define-public crate-secrets-1.1.0 (c (n "secrets") (v "1.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0idrjklr0mb285jmcvdbxwlnbd4xwl8qiwf065wrl622hnddbfaq") (f (quote (("use-libsodium-sys" "libsodium-sys") ("allow-coredumps"))))))

(define-public crate-secrets-1.2.0 (c (n "secrets") (v "1.2.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(target_family = \"unix\")") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_family = \"windows\")") (k 1)))) (h "123rb4fd8lvqpi7fnv6vhfy9cs9p80g53kc0mnmsr8n42fi4a5zm") (f (quote (("use-libsodium-sys" "libsodium-sys") ("allow-coredumps"))))))

