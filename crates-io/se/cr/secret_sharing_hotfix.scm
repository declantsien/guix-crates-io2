(define-module (crates-io se cr secret_sharing_hotfix) #:use-module (crates-io))

(define-public crate-secret_sharing_hotfix-0.2.1 (c (n "secret_sharing_hotfix") (v "0.2.1") (d (list (d (n "amcl_wrapper") (r "^0.3") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1fh0y4nba1p0ffkp5yvpm0h0yz3y1mdxv8748fdb714fl0jlg5ny")))

