(define-module (crates-io se cr secret_integers) #:use-module (crates-io))

(define-public crate-secret_integers-0.1.0 (c (n "secret_integers") (v "0.1.0") (h "01mw7w7q0ih8273n8kgbig5620g07c9hd6aqmxcmymxza24dx981")))

(define-public crate-secret_integers-0.1.1 (c (n "secret_integers") (v "0.1.1") (h "0m7qap3xwp1x000jrgvsngwpvkdvgpw19jk75hphmb27fg3zibc7")))

(define-public crate-secret_integers-0.1.2 (c (n "secret_integers") (v "0.1.2") (h "0mm41c6iv6dilpm4awqwnmnl9sl1a3sah9m0gq1ld8n1f0zcgqaz")))

(define-public crate-secret_integers-0.1.3 (c (n "secret_integers") (v "0.1.3") (h "13r4fbz40zf66pc7ib3r7l9ip6hy9sm32qngd44ng9jaa4prcsl4")))

(define-public crate-secret_integers-0.1.4 (c (n "secret_integers") (v "0.1.4") (h "1jl5lll99xm8nwlzc6vs8ay8hpcqj4vl1qwjkjbbarppfwmn6yc4")))

(define-public crate-secret_integers-0.1.5 (c (n "secret_integers") (v "0.1.5") (h "1mfghns1k3mg36hm2z9p12047vxhjp6jglly4yhj0izk4vlny723")))

(define-public crate-secret_integers-0.1.6 (c (n "secret_integers") (v "0.1.6") (h "15n78qg7dgjjwp8vxq6dzvwak7yjgi9c1afkl3fnnfx1xadf78b0")))

(define-public crate-secret_integers-0.1.7 (c (n "secret_integers") (v "0.1.7") (h "0vx3pzhzba7x1y2sm4sy4hfjf5n066jcvys0drhvf2b4iigfqrv2")))

