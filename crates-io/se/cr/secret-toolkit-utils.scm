(define-module (crates-io se cr secret-toolkit-utils) #:use-module (crates-io))

(define-public crate-secret-toolkit-utils-0.2.0 (c (n "secret-toolkit-utils") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1y9r02g5dpq5wqsq69ardpapj276j4p1v4f8f0pfpxr2hp1dgz1q")))

(define-public crate-secret-toolkit-utils-0.3.0 (c (n "secret-toolkit-utils") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0rwy1mpn8flhqbczhx0pykvc5iim439lkvhdmcm575gvh1idlyv3") (y #t)))

(define-public crate-secret-toolkit-utils-0.3.1 (c (n "secret-toolkit-utils") (v "0.3.1") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qrfqc515g8r0imb229vjmcxdjysr61b1am06af6xzxaqlqc7m1i")))

(define-public crate-secret-toolkit-utils-0.6.0 (c (n "secret-toolkit-utils") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "01rvll2q0l00hgmh1ydxvadalv7czsppl9vlzd6zj8nccdi5srj7")))

(define-public crate-secret-toolkit-utils-0.7.0 (c (n "secret-toolkit-utils") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dw64x5ym34mb6092xm5j6rb7ffh9llvzh792l0g971z4pcr8as0")))

(define-public crate-secret-toolkit-utils-0.9.0 (c (n "secret-toolkit-utils") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^1.1.10") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^1.1.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "00bqysriibbs61ndby598bcy7kh2fzh1jzarkv48h2yrdn5varp9")))

(define-public crate-secret-toolkit-utils-0.10.0 (c (n "secret-toolkit-utils") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.1.11") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^1.1.11") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j20zy25sx67m53f6jgfddvvkvifq03jh1y6vzih3mqgwyicpwc3")))

