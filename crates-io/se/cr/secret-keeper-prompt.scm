(define-module (crates-io se cr secret-keeper-prompt) #:use-module (crates-io))

(define-public crate-secret-keeper-prompt-0.3.0 (c (n "secret-keeper-prompt") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "secret-keeper") (r "^0.3") (d #t) (k 0)))) (h "0wxfhy9kvkvvqmzy0rl4iq9hfpcbhisjkg4gccn1rjca2q9110j7")))

(define-public crate-secret-keeper-prompt-0.3.4 (c (n "secret-keeper-prompt") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "secret-keeper") (r "^0.3") (d #t) (k 0)))) (h "1irc3m2w7g848iwfpbyzynywz09gp2gab29isxbm88jp1cra9092")))

