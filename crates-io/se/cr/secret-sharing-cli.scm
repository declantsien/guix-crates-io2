(define-module (crates-io se cr secret-sharing-cli) #:use-module (crates-io))

(define-public crate-secret-sharing-cli-0.1.0 (c (n "secret-sharing-cli") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "191x4j021g3h2gqliwx0c6zg2qwhgs2z223hyr0q8hi9pscnvnj1")))

(define-public crate-secret-sharing-cli-0.1.1 (c (n "secret-sharing-cli") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "0ycw1dczrs2ifkfglgcr17c15d87wwhcw3das8zb0k0b4l8gdyz9")))

(define-public crate-secret-sharing-cli-0.1.2 (c (n "secret-sharing-cli") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "0p74sfv9whh0pc5ibjmi4b5nm3znzzlq5m63fhlgpc64641wjr5j")))

