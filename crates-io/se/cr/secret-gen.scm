(define-module (crates-io se cr secret-gen) #:use-module (crates-io))

(define-public crate-secret-gen-0.1.0 (c (n "secret-gen") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0khfvpm8l688cr12qv5l03g63q2zvhg167brsd32wrj0x3kfah19") (r "1.65")))

