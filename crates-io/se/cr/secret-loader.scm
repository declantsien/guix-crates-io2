(define-module (crates-io se cr secret-loader) #:use-module (crates-io))

(define-public crate-secret-loader-0.1.0 (c (n "secret-loader") (v "0.1.0") (d (list (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "1qzmabrcvvswffyyaha45v5xxr2032y84n77y69vz793w4xasasz")))

