(define-module (crates-io se cr secret-keeper-test_util) #:use-module (crates-io))

(define-public crate-secret-keeper-test_util-0.3.0 (c (n "secret-keeper-test_util") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "0b9q0ig2w9msrciqdkaf4jzagck5m31waqgdysahhm65gwjr187a")))

(define-public crate-secret-keeper-test_util-0.3.4 (c (n "secret-keeper-test_util") (v "0.3.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "0hi206i4n42m22rd1w1gwshiggf86w4lxgn2ynci5vflss230z22")))

(define-public crate-secret-keeper-test_util-0.3.5 (c (n "secret-keeper-test_util") (v "0.3.5") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "13lq798mzj96wlpz5058igwcnsnpslpi95dp7pk43fx08x8p10i6")))

