(define-module (crates-io se cr secret_sharing) #:use-module (crates-io))

(define-public crate-secret_sharing-0.1.0 (c (n "secret_sharing") (v "0.1.0") (d (list (d (n "amcl_wrapper") (r "^0.1.7") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1nsj3cpnqknl5irmn0i1w5g2rbshbi3na47p90dgnzddhkk0pj32")))

(define-public crate-secret_sharing-0.1.1 (c (n "secret_sharing") (v "0.1.1") (d (list (d (n "amcl_wrapper") (r "^0.2.1") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1m6rh4mk585vl95i5y211zr5j561wa61hzxvpg91k97v9lgq5mdw")))

(define-public crate-secret_sharing-0.2.0 (c (n "secret_sharing") (v "0.2.0") (d (list (d (n "amcl_wrapper") (r "^0.3") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1yb746wp2jq6bw9azg5yn5wskcyyl7px4pl5d91kh2a1bsmlqapw")))

