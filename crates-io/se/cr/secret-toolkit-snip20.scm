(define-module (crates-io se cr secret-toolkit-snip20) #:use-module (crates-io))

(define-public crate-secret-toolkit-snip20-0.2.0 (c (n "secret-toolkit-snip20") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lskywkmclmh9mh2bfc4lxgzbh217654mwzc4q45xjp9hsz1l49c")))

(define-public crate-secret-toolkit-snip20-0.3.0 (c (n "secret-toolkit-snip20") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wn4mnj91x33dh9mx2ams0zj4si92pblcqixbl3p0jglm1di4gwz")))

(define-public crate-secret-toolkit-snip20-0.6.0 (c (n "secret-toolkit-snip20") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1z7by2mbyaabi0xnknbcx8qycyms7lp61jxj9jd9y3jk533cb3d6")))

(define-public crate-secret-toolkit-snip20-0.7.0 (c (n "secret-toolkit-snip20") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0r8aikylrxbc6vskabmq575dm8l5zhlrf2m63zj0c3gc52siq1a0")))

(define-public crate-secret-toolkit-snip20-0.9.0 (c (n "secret-toolkit-snip20") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^1.1.10") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xgfnp5jdk0ry422s2y1ifr2jdpb50w0gbdk09g1xkn662gyy5p9")))

(define-public crate-secret-toolkit-snip20-0.10.0 (c (n "secret-toolkit-snip20") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.1.11") (f (quote ("random"))) (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit-utils") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "019gglj75rmihkmk6837sq4ba6icxcwgjifnmi1dynm7z4fs2i41")))

