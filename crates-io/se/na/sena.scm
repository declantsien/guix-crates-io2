(define-module (crates-io se na sena) #:use-module (crates-io))

(define-public crate-sena-0.1.0 (c (n "sena") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (k 2)) (d (n "either") (r "^1.11.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "1dw7vd6r51daqyl4z4k2f1xs8c7pxsy6nl18lhxbnd3djvd28z3p") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-sena-0.1.1 (c (n "sena") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (k 2)) (d (n "either") (r "^1.11.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "0mkzcpry8zkccpbfc9161pxiha8h3xpdi88va3gy74igdfxka4l5") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-sena-0.2.0 (c (n "sena") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from"))) (k 2)) (d (n "either") (r "^1.11.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "1mgrlsfa8f2lp2yi2rap7y5317rd8ww2hy5icfz7p4s0lhwgvgmf") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

