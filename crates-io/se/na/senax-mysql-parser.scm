(define-module (crates-io se na senax-mysql-parser) #:use-module (crates-io))

(define-public crate-senax-mysql-parser-0.1.0 (c (n "senax-mysql-parser") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "07i8120bb7wdzpsr3qxlfr5g3ja4kycja1c6jf2dgwajaja8cmm2")))

(define-public crate-senax-mysql-parser-0.1.1 (c (n "senax-mysql-parser") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0gsvx65rfidvg1zldh5wm0ms7dccn92b2nv5lbbgqvf00b545sac")))

(define-public crate-senax-mysql-parser-0.1.2 (c (n "senax-mysql-parser") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0h2b0cqy9a5cpiccbyd0zj0d21lcfkr697hwaggp622hdklfgx30")))

(define-public crate-senax-mysql-parser-0.1.3 (c (n "senax-mysql-parser") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0s8l08gdcbaxra95cs6fajwmb6hm9j8dprja71ryrg2sralkaab2")))

(define-public crate-senax-mysql-parser-0.1.4 (c (n "senax-mysql-parser") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1vpc9hdsvbihr8bnkp3flwgg9n6772nfi25iimp7212686rqv5kx")))

