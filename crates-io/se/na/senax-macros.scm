(define-module (crates-io se na senax-macros) #:use-module (crates-io))

(define-public crate-senax-macros-0.1.0 (c (n "senax-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xd7j89j061mcr7x91qhs8sv463x8csc75wn0n9szci1hvnmr8ja")))

(define-public crate-senax-macros-0.2.0 (c (n "senax-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rphkk49lf0prqzqajhc10mbhf12n2hxhfy0p23d62whvjwpsiql")))

