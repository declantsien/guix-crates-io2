(define-module (crates-io se ui seuif97) #:use-module (crates-io))

(define-public crate-seuif97-1.0.0 (c (n "seuif97") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "09igdhhi5rvlkqlxnhnr1p50gwsi2mg0gdwhj0hsv988f21ncl0z") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.1 (c (n "seuif97") (v "1.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0kw3gs7b11dcrzxy0v8ms5ml7270qj5n2z2hz8wagwzg7v1hpsc7") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.2 (c (n "seuif97") (v "1.0.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "11zcpncqvrpqcz9ly7j0ph63kzrga83ssxdnc1agjgicx4qd62jl") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.3 (c (n "seuif97") (v "1.0.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0ydb6yz7qv3wvdsnfqxy99gqjqj5zd179qs0hqr22pqxicdvv3pz") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.4 (c (n "seuif97") (v "1.0.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "164479f33gzag78m4mqsphwvbg1vh09fkiwmyzz5vc0znbj1hyva") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.5 (c (n "seuif97") (v "1.0.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1qwh48zw11vl7pmrwxq6wrhxk9f3dd6mm585jj4kz1n8mpwqzywx") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.6 (c (n "seuif97") (v "1.0.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0vm2zapqlqlf95sskdxanbsk6wgz5qcxh524pfrqwcl05q9nkg09") (f (quote (("stdcall") ("python") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.7 (c (n "seuif97") (v "1.0.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1mi40ii4pb9hm4vy8piiq10039xqmj8ymqi251n7zlxlvsw5bwxg") (f (quote (("stdcall") ("python") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.8 (c (n "seuif97") (v "1.0.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0im25i2l3vv60f79mlj78bp3g3mhx9fdfil4b2iqic7z50ribqcm") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.0.9 (c (n "seuif97") (v "1.0.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1pjr7c34wiawgc7caknwqb1qj06p3hxnv95vyhq6i4dkdwnr9hfr") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.1.0 (c (n "seuif97") (v "1.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1kgr7n4ijqvryik4wrb1z5zxswfmcwwink7bxfgyayfy4pf85xcj") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.1.1 (c (n "seuif97") (v "1.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0mxb2dgajy91agvj564g2mdphwdkjim4w09p2yyxg72ak7ym5jy6") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.1.2 (c (n "seuif97") (v "1.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0zv88rq3cmq1mm9gsh7533dxrs16qk1dnnm48h414wcqrqidfknf") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.1.3 (c (n "seuif97") (v "1.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0z3b27n5l6zj9llj6ksjwfmzifgm4z5vc6813zj3ga0ahpcnw3qn") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-seuif97-1.1.4 (c (n "seuif97") (v "1.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "04w42q4cl147rf300h7scihy5na0q3c5mq1iawijy97wcizmx7b7") (f (quote (("stdcall") ("python" "pyo3") ("cdecl"))))))

