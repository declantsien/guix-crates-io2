(define-module (crates-io se a_ sea_orm_newtype_id) #:use-module (crates-io))

(define-public crate-sea_orm_newtype_id-0.1.1-alpha.0 (c (n "sea_orm_newtype_id") (v "0.1.1-alpha.0") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.1-alpha.0") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.1-alpha.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "1rp3vkhx696x67xpbil18q807pvvq1ad9mdzdh8928i3jya8njjq")))

(define-public crate-sea_orm_newtype_id-0.1.1-alpha.1 (c (n "sea_orm_newtype_id") (v "0.1.1-alpha.1") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.1-alpha.1") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.1-alpha.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "0959lcliliqi64q8sdhwx16dx8fv1w5kwzwsb9jszg8c4xs4a35b")))

(define-public crate-sea_orm_newtype_id-0.1.1-alpha.2 (c (n "sea_orm_newtype_id") (v "0.1.1-alpha.2") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.1-alpha.2") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.1-alpha.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "13vr0aalh3yjc94bs784n8r9hzpd97aqb4a380gdq986p6lm8bcy") (f (quote (("with-serde" "sea_orm_newtype_id_macros/with_serde") ("with-async-graphql" "sea_orm_newtype_id_macros/with_async_graphql"))))))

(define-public crate-sea_orm_newtype_id-0.1.1-alpha.3 (c (n "sea_orm_newtype_id") (v "0.1.1-alpha.3") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.1-alpha.3") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.1-alpha.3") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "0fzq8sr488n53nz5d3v9cr54v8k7zjgrhlwd9bqr30c3l68kg46x") (f (quote (("with-serde" "sea_orm_newtype_id_macros/with-serde") ("with-async-graphql" "sea_orm_newtype_id_macros/with-async-graphql"))))))

(define-public crate-sea_orm_newtype_id-0.1.1-alpha.4 (c (n "sea_orm_newtype_id") (v "0.1.1-alpha.4") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.1-alpha.4") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.1-alpha.4") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "1vakcxgj6pg0wzna8074b2i3s1y34rcs84mrgx9sxdpil7iw701m") (f (quote (("with-serde" "sea_orm_newtype_id_macros/with-serde") ("with-async-graphql" "sea_orm_newtype_id_macros/with-async-graphql"))))))

(define-public crate-sea_orm_newtype_id-0.1.2 (c (n "sea_orm_newtype_id") (v "0.1.2") (d (list (d (n "sea_orm_newtype_id_domain") (r "^0.1.2") (d #t) (k 0)) (d (n "sea_orm_newtype_id_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)))) (h "17q34vy04njfsr8lzq29bgwkxxv63jqmzxgwnmga3pf8ssii5sxk") (f (quote (("with-serde" "sea_orm_newtype_id_macros/with-serde") ("with-async-graphql" "sea_orm_newtype_id_macros/with-async-graphql"))))))

