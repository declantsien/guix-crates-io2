(define-module (crates-io se a_ sea_orm_newtype_id_domain) #:use-module (crates-io))

(define-public crate-sea_orm_newtype_id_domain-0.1.1-alpha.0 (c (n "sea_orm_newtype_id_domain") (v "0.1.1-alpha.0") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "0mgv9qy1k6marzazn9ms1pbdxf177jmqw2vimq6izi8jnpx84lqs")))

(define-public crate-sea_orm_newtype_id_domain-0.1.1-alpha.1 (c (n "sea_orm_newtype_id_domain") (v "0.1.1-alpha.1") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "15gzb12x12b8qz4ixp1cf17mjjr045i3s6swlfcipd4f848db293")))

(define-public crate-sea_orm_newtype_id_domain-0.1.1-alpha.2 (c (n "sea_orm_newtype_id_domain") (v "0.1.1-alpha.2") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "1j8k7dw34crjvw3nfqh53j8y39kf2zl3j9gzpma2vj1ncpq0mdqp")))

(define-public crate-sea_orm_newtype_id_domain-0.1.1-alpha.3 (c (n "sea_orm_newtype_id_domain") (v "0.1.1-alpha.3") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "0dffd3asnkwz2wmq105xkxvyw5qbm7m6ksakk8vf2fags1a9g50h")))

(define-public crate-sea_orm_newtype_id_domain-0.1.1-alpha.4 (c (n "sea_orm_newtype_id_domain") (v "0.1.1-alpha.4") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "0mxl8vgx7pz2wcxm8hkl9rd94h4w4jy9x94y0b5i37yrrxjanc6c")))

(define-public crate-sea_orm_newtype_id_domain-0.1.2 (c (n "sea_orm_newtype_id_domain") (v "0.1.2") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)))) (h "08qmwrcjg9db4p4nxxnl2mjkxl9hhghqkwzhd5s6hm8knly306vh")))

