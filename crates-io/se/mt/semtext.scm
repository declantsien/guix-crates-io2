(define-module (crates-io se mt semtext) #:use-module (crates-io))

(define-public crate-semtext-0.1.0 (c (n "semtext") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1dnr0zjmq2fdvphiizjs2gscizwq66npazqxwyyl280p94lazc7c")))

