(define-module (crates-io se ar search_trail) #:use-module (crates-io))

(define-public crate-search_trail-0.1.0 (c (n "search_trail") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0d35zgzncm1rg17kh5z7p6miqjn8d5vys9mps6r9h0xnhvl7ligg")))

(define-public crate-search_trail-0.1.1 (c (n "search_trail") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0975aqxjbq0zfg52mdrnqw35hf9p58klrdjjz4xb1h3r6h1vsy8q")))

(define-public crate-search_trail-0.1.2 (c (n "search_trail") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1q5bsknzsps5dgi57zs0ijxvv1i8wanifdvihn7kvva7qw0ap1br")))

(define-public crate-search_trail-0.1.3 (c (n "search_trail") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1rv1qv9hhb25ld2g93aw7l7rjp34vqn2183lfwjgks74l90cz84f")))

