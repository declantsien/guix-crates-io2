(define-module (crates-io se ar search_dir) #:use-module (crates-io))

(define-public crate-search_dir-0.1.0 (c (n "search_dir") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0ncal09ch431q1si1za56n3s1iwfp2svfv54pb0rn6fhwqcyw4b6")))

(define-public crate-search_dir-0.1.1 (c (n "search_dir") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1kb59gk0kw29d3vx91yj4i3hz4mhyc8076bmc7hpm25x05658lpv")))

(define-public crate-search_dir-0.1.2 (c (n "search_dir") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0zpmqhjkkh7wiyn1mln5v33yzyx3z0lm1n3873biklcgy7xw05mw")))

(define-public crate-search_dir-0.1.3 (c (n "search_dir") (v "0.1.3") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "00gzdaqgcy2i3i1n50khcbsznpxx6ri2hgnk1n844md2k7al3ghd")))

