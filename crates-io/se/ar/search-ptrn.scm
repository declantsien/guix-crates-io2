(define-module (crates-io se ar search-ptrn) #:use-module (crates-io))

(define-public crate-search-ptrn-0.1.0 (c (n "search-ptrn") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vdxhwvk7ymmgawbdqf0rzdddadhyhqnavj0l87hhvnnfl220zcm")))

(define-public crate-search-ptrn-0.1.1 (c (n "search-ptrn") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bk1hh4bw0dh704wqgiy3496bl0316fiy1qn6c9kdxxjz4fhza41")))

