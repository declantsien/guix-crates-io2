(define-module (crates-io se ar searchy) #:use-module (crates-io))

(define-public crate-searchy-0.1.0 (c (n "searchy") (v "0.1.0") (d (list (d (n "expry") (r "^0.1.0") (d #t) (k 0)))) (h "1jv9awvw3g080siwr78nziv80gc84lpb2skaycl3fkx4n1l2ri88")))

(define-public crate-searchy-0.1.1 (c (n "searchy") (v "0.1.1") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)))) (h "15bncx96dzfh4r0idc1csbzs9v4lcvqd16qp2660s550zlr2l7ac")))

(define-public crate-searchy-0.1.2 (c (n "searchy") (v "0.1.2") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)))) (h "0c0bxcipamvd50vachgnwiibsbpzjabnc6fhq3amb6y2gkj00y80")))

(define-public crate-searchy-0.2.0 (c (n "searchy") (v "0.2.0") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "1gljnnr9ps689njlpgxwfk17hc6rryr4jpdxjfzy0wvcngssf1ar")))

(define-public crate-searchy-0.2.1 (c (n "searchy") (v "0.2.1") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "0bimcx2jdnzczd73fy8mr0lbdvjp8x3ca5r6m1igifgxfgj12975")))

(define-public crate-searchy-0.2.2 (c (n "searchy") (v "0.2.2") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "1z1gyx4b1lsaxqkqscx4fbv8gkpld6z9h3fg0lkd2msz8vy2kzx2")))

(define-public crate-searchy-0.3.0 (c (n "searchy") (v "0.3.0") (d (list (d (n "expry") (r "^0.3") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "04rpm9jadimhswc1vf7n6b9bg7zpz2nmjhrnqxp6b56q088r5fpq")))

(define-public crate-searchy-0.4.0 (c (n "searchy") (v "0.4.0") (d (list (d (n "expry") (r "^0.4") (d #t) (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)))) (h "1j0k4sys4hixz672qjcns8nrdb5n84ayzfhq19gmxdav4j54hnyf")))

