(define-module (crates-io se ar search_file) #:use-module (crates-io))

(define-public crate-search_file-0.1.0 (c (n "search_file") (v "0.1.0") (h "1f494gk8m9k2hpmvh2gpm1iifzmwvj9jhkqav84q1wbjlqpaxx4h")))

(define-public crate-search_file-0.1.1 (c (n "search_file") (v "0.1.1") (h "1fqxqd2v07f8sdqyhxmbd9b4zn8amjjblk4iflnjsfzs522vqih7")))

