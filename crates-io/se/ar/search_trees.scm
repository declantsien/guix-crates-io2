(define-module (crates-io se ar search_trees) #:use-module (crates-io))

(define-public crate-search_trees-0.0.0 (c (n "search_trees") (v "0.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0ksgpb7m83pqh7ygsgdfd415x3m1n6g7cx6lwnr1c5np8jc666vi") (y #t)))

(define-public crate-search_trees-0.0.1 (c (n "search_trees") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1g71qvwfk359d7mp5y7d50jh15cj1a3zcl0z2dwg3ak42f84h77p") (y #t)))

(define-public crate-search_trees-0.0.2 (c (n "search_trees") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s4gdl93csyrqy8sqq193li7610h1pid9nd4xxgr3dbddjl0wsc9") (y #t)))

(define-public crate-search_trees-0.0.3 (c (n "search_trees") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1djp3v4cpyai5n0bpgnrv2yxb1044xw3x58fbxznkvqff6qpx0yq")))

