(define-module (crates-io se ar search_json) #:use-module (crates-io))

(define-public crate-search_json-0.1.0 (c (n "search_json") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zffsg370wcqqy87bvf5aqyap9lhzg9bql4dksncjkd6csaqcn9z")))

