(define-module (crates-io se ar search-rs) #:use-module (crates-io))

(define-public crate-search-rs-0.1.0 (c (n "search-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "19vx738jk07fwb1kgqydayxbwyd9wjab6dp841p7v42g4pzwlrlp")))

(define-public crate-search-rs-0.1.1 (c (n "search-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "16nff5qijhzf4yl11qj42yxh6kfday5jnh8d56caw644fxp6b03k")))

(define-public crate-search-rs-0.1.2 (c (n "search-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "04ghsdp58fpin55nnx7a1gq7gsf91zxy4mq9afczqiw6dy4yai3w")))

(define-public crate-search-rs-0.2.0 (c (n "search-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "171mp268vl1f640kn1x13992rga454n7pqas4s6dqhk91jq1nh5s")))

