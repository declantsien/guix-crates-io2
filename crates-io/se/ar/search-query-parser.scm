(define-module (crates-io se ar search-query-parser) #:use-module (crates-io))

(define-public crate-search-query-parser-0.1.0 (c (n "search-query-parser") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "12civ5pm2n594q1r20yyrgz3v84m13raclbw3w88lzizs13w6pjm")))

(define-public crate-search-query-parser-0.1.1 (c (n "search-query-parser") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1iximyqwyznmpnksxi2kjjvpdc7b955lvsrri30adgrip4wx8n12")))

(define-public crate-search-query-parser-0.1.2 (c (n "search-query-parser") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "12c6vbvyamkhh56nxwk6ic7z98fm77w77hcc5vxhq450d4l3rsjd")))

(define-public crate-search-query-parser-0.1.3 (c (n "search-query-parser") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0alv66syif64zv3dfkkp58if1099qh18ma0yfyiis6q5lfspk75r")))

(define-public crate-search-query-parser-0.1.4 (c (n "search-query-parser") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18gfph9f5zk7qwjl1v6jzv6qlpifvllz4051sw1jj02fyk65yk5g")))

