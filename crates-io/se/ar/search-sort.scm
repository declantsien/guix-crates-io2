(define-module (crates-io se ar search-sort) #:use-module (crates-io))

(define-public crate-search-sort-0.1.0 (c (n "search-sort") (v "0.1.0") (h "1zzl3g06x1ihljxmp6qpph0g6pd136b8yx1rndqhfj8z42yq38hh") (y #t)))

(define-public crate-search-sort-0.1.1 (c (n "search-sort") (v "0.1.1") (h "024xdjr2bxaqpry6a3grs8n5gwv5qv5ghd53h8lnclxbfxa7yb82") (y #t)))

(define-public crate-search-sort-0.1.2 (c (n "search-sort") (v "0.1.2") (h "0dwn3xyy5ly6827986lid5nx7dckps4akdpy6xyg14fff32561g7") (y #t)))

(define-public crate-search-sort-0.2.0 (c (n "search-sort") (v "0.2.0") (h "0kwkvr3qjsxv2cqk8af2xkj8kcgn0c5vv5nfsvs6knrv4gx3qf20") (y #t)))

(define-public crate-search-sort-0.3.0 (c (n "search-sort") (v "0.3.0") (h "1y913955vv9iv24498g5bcjsjpjhnxyxfdwyk3q2v528ghcdfws0") (y #t)))

(define-public crate-search-sort-0.3.1 (c (n "search-sort") (v "0.3.1") (h "0bgdhn1ajq34k0wfybmliigkv7lwjd2055dy28qngdi42w5hjrsm") (y #t)))

