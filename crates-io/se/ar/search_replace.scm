(define-module (crates-io se ar search_replace) #:use-module (crates-io))

(define-public crate-search_replace-0.1.0 (c (n "search_replace") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "195dkllpr7pci5dlcg9p9na6mghk2zyyf1jsv2mdk6vn4h1x0qx1") (y #t)))

(define-public crate-search_replace-0.1.1 (c (n "search_replace") (v "0.1.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1zp0h89h1a2gbkysdiscjidh1ix4simfkbsxxc260wzdkyfvd54a")))

