(define-module (crates-io se ar search-once) #:use-module (crates-io))

(define-public crate-search-once-0.1.0 (c (n "search-once") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("yaml_conf"))) (k 0)) (d (n "open") (r "^5.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m5p6g2ii75cnvqv958dwqh06fmr1ryzccz16cswgn7f1i3f1x7c")))

(define-public crate-search-once-0.1.1 (c (n "search-once") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("yaml_conf"))) (k 0)) (d (n "open") (r "^5.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0amkyamv0d3q4zfk3bh8fyjwv7qn89fy4sssylrpldfqz5dvxhnd")))

