(define-module (crates-io se ar search_path) #:use-module (crates-io))

(define-public crate-search_path-0.1.0 (c (n "search_path") (v "0.1.0") (h "134mcspdv11pnb1l3ckkfpnb3n4hk5d8g82rhmkl5jalx93csgqg")))

(define-public crate-search_path-0.1.1 (c (n "search_path") (v "0.1.1") (h "0hi31731ciij7b7x785md3482wxcbysasz83s6naivf1nqcyf4dk")))

(define-public crate-search_path-0.1.2 (c (n "search_path") (v "0.1.2") (h "1gvj738lna3b8zifh0j3l27i8vg5f14xd3569sqrk56ziyp1ms4n")))

(define-public crate-search_path-0.1.3 (c (n "search_path") (v "0.1.3") (h "1rw6g40xqjfwifj5df2q37chxf244i7cqnn5pwh8a3i7hn0hj6hm")))

(define-public crate-search_path-0.1.4 (c (n "search_path") (v "0.1.4") (h "0w27nv2z3z3hz4iai8i36yid99zzrqhcilals0j8527c7ckb4iai")))

