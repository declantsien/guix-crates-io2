(define-module (crates-io se ar search-autocompletion) #:use-module (crates-io))

(define-public crate-search-autocompletion-0.1.0 (c (n "search-autocompletion") (v "0.1.0") (h "138irz118jg77wc4cb1jkz0cc07lm253i1vlqdnc1m23471nvghp")))

(define-public crate-search-autocompletion-0.1.1 (c (n "search-autocompletion") (v "0.1.1") (h "14jp9yb0dw3vc0wnzzgpb41faznjnvw8sajhl6zgazi4yq6w008z")))

(define-public crate-search-autocompletion-0.1.2 (c (n "search-autocompletion") (v "0.1.2") (h "168iq9x6wgqq8rbqxgk1pj5ha2dvnxcczrkz01xkapi8gcvihskh")))

(define-public crate-search-autocompletion-0.2.0 (c (n "search-autocompletion") (v "0.2.0") (h "0pfssklbcra0d4zagfhwjr9acxwcmprga0a1qy7v78a69mppgf11")))

(define-public crate-search-autocompletion-0.3.0 (c (n "search-autocompletion") (v "0.3.0") (h "13hakx5ghcppi0fhvxjza9qc0mhxaq0flmjxvvd3fncr610lfn5f")))

(define-public crate-search-autocompletion-0.4.0 (c (n "search-autocompletion") (v "0.4.0") (h "1wdlhmv1x0j1s32zzg27zh7ycd44ngczpymkpj6yanqg2pg8vyh4")))

