(define-module (crates-io se ar search) #:use-module (crates-io))

(define-public crate-search-0.0.2 (c (n "search") (v "0.0.2") (h "0ysnc4xm0dp3xgchrjk9302m0hwp65v050r1w8vlavipjfkk12yg") (y #t)))

(define-public crate-search-0.1.0 (c (n "search") (v "0.1.0") (h "05hrk6m85d6gz911phll0fa52xngzlm09hnk1mi2vjbhv9h6m90b") (y #t)))

(define-public crate-search-1.0.0-beta (c (n "search") (v "1.0.0-beta") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "077s8i7dch2n0m3nn3xc76nrbb20gk4f48jv4h3z2y81v5hlz0x7")))

