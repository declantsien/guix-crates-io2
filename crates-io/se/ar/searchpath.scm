(define-module (crates-io se ar searchpath) #:use-module (crates-io))

(define-public crate-searchpath-0.1.0 (c (n "searchpath") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "176r40b19kng0f7yf2vy1hzhmn3wldfzqwcl1jczdpy625y04i5k")))

(define-public crate-searchpath-0.1.1 (c (n "searchpath") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09q8sxp7p3hhx7c3rkpam46ayz995c4kh49pz5qs76yv6jzx3g6n")))

(define-public crate-searchpath-0.1.2 (c (n "searchpath") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01kp4xq2qzzqq4z6mq83s05v3anmgr5whylf7rcy7wf3yvjvlbkg")))

(define-public crate-searchpath-0.1.3 (c (n "searchpath") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a7c7hm3gfnw4yq05wwxkixgsqmgsh3k6m8bgc6w367qrk7xavx7")))

