(define-module (crates-io se ar search_with_google) #:use-module (crates-io))

(define-public crate-search_with_google-0.1.0 (c (n "search_with_google") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "1nm4bf66lblgc28zarwpjq6ga9wq31x21ahs4ifq87sdxa69736y")))

(define-public crate-search_with_google-0.2.0 (c (n "search_with_google") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "05v070vwqp346ddjqfnvkd07q1kga9cijdcb9pjzfzbbaf8j2k0l")))

(define-public crate-search_with_google-0.2.1 (c (n "search_with_google") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0hqmhz05p2fm41yi6g15n7w2hr0p3gvgyrx2i5hxdpxzwqsmr202")))

(define-public crate-search_with_google-0.2.2 (c (n "search_with_google") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0wmq9a9fi1p716sbyp6nslmm4yvhjjcikz3l0iswki57bfzf0mq1")))

(define-public crate-search_with_google-0.3.0 (c (n "search_with_google") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.5") (d #t) (k 0)))) (h "186ckj70h5hz83zy032590fdm7ird0kfvbfyv8nxfc8p7vgijgq0")))

(define-public crate-search_with_google-0.3.1 (c (n "search_with_google") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "1blq3sbngv5a1y2qwnyfv6gwalhf95x3551gp0ivzbwi95mq2kjk")))

(define-public crate-search_with_google-0.4.1 (c (n "search_with_google") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "18bblicfbv4pl39l0wrzbjlv3qdyphvjgy97jwpcyzwkmiax20ms")))

(define-public crate-search_with_google-0.5.0 (c (n "search_with_google") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "select") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "10j3ds8fp5nv457qss4gsv3sgxbz6mng5fa99kkjh3d5bn762sjg")))

