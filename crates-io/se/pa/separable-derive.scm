(define-module (crates-io se pa separable-derive) #:use-module (crates-io))

(define-public crate-separable-derive-0.1.0 (c (n "separable-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "19zjl0yb8mifwyicvzxz6227ab5kww45hyagcbxkldpxb9qf9rl3")))

(define-public crate-separable-derive-0.2.0 (c (n "separable-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1jzsdwqivaazkwylyij7qi7k8g9iwwwkqwjgvfvzjav3p45b5apv")))

(define-public crate-separable-derive-0.2.1 (c (n "separable-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1dcap56qj02ygp35dflayqfpq1za9y30p002i2yqq66w1b6i6nnv")))

