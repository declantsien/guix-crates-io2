(define-module (crates-io se pa sepax2d) #:use-module (crates-io))

(define-public crate-sepax2d-0.1.0 (c (n "sepax2d") (v "0.1.0") (h "1c889y84fr7a98nr2fdpjcjqdcbnckzq2wwgqj6iyr5m968dfzp4")))

(define-public crate-sepax2d-0.2.0 (c (n "sepax2d") (v "0.2.0") (h "07fkxws30xyj6gfd7fpmxqrxsn3p2pgf97m9gpwx8djga7nmkjn1")))

(define-public crate-sepax2d-0.3.0 (c (n "sepax2d") (v "0.3.0") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)))) (h "00862qav2ydckmglwilsfc850x183ai6pmdiwhmnlbqc8218391z")))

(define-public crate-sepax2d-0.3.1 (c (n "sepax2d") (v "0.3.1") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)))) (h "0gi5gljpzz6igmnhwkak2af4mmn6cg5igy6kaqdj929dfplaj5lv")))

(define-public crate-sepax2d-0.3.2 (c (n "sepax2d") (v "0.3.2") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)))) (h "15i67kzmmzwj1jixjymadr1apkb1lbq0ra30bpzv0as453jfg58d")))

(define-public crate-sepax2d-0.3.3 (c (n "sepax2d") (v "0.3.3") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)))) (h "0r38c3xpmdi7sypa0khyf14ff1ljx523zfrcqjkq3518s0ayprca")))

(define-public crate-sepax2d-0.3.4 (c (n "sepax2d") (v "0.3.4") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)))) (h "1yygppm85hnsjpia35bk539fv1c0bcy8lxayyln3awl22599pnfi")))

(define-public crate-sepax2d-0.3.5 (c (n "sepax2d") (v "0.3.5") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mzmqnqqc9gxf3fp6b016nlxgnjiahj286lixwxp9zrg8ljaqjab") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3.6 (c (n "sepax2d") (v "0.3.6") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02yfygkdlz3zkn6n2gcgr966n9lccs2hk4h204nawlryglc49zag") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3.7 (c (n "sepax2d") (v "0.3.7") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v2qapiaxa5n3ya4rvfik9fa2czrgh0rzlyq9b7jh7c17gkz586q") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3.8 (c (n "sepax2d") (v "0.3.8") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "083pkqjlakrk9vfbmvbfgmc6q6wayz9pr21p6wbpvpmc9543499r") (s 2) (e (quote (("serde" "dep:serde"))))))

