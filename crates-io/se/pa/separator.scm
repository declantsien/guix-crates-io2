(define-module (crates-io se pa separator) #:use-module (crates-io))

(define-public crate-separator-0.1.0 (c (n "separator") (v "0.1.0") (h "0d80la7qgcpi481jjmx9zvb8qqbbdk9ammgdc6dn7ghbjbn4f18k")))

(define-public crate-separator-0.2.0 (c (n "separator") (v "0.2.0") (h "0qk7rpgg1ybmx49h129kiyj4yi7vzga7mf4x2dfvz1yawlz84bqb")))

(define-public crate-separator-0.3.0 (c (n "separator") (v "0.3.0") (h "1dz34x3568v0jhq329mkps50wf1ggvv32v4yih4pq1m28qpasdz2")))

(define-public crate-separator-0.3.1 (c (n "separator") (v "0.3.1") (h "0ay3ixpbal576ph34a5sqnih12wc16l5vpqig4pd0blmiy0lvk3s")))

(define-public crate-separator-0.4.0 (c (n "separator") (v "0.4.0") (h "0nf29j2vxcd4k13wbdfhq2rdp8dvxjnlvshhdfgf4haiyb7y8ia8")))

(define-public crate-separator-0.4.1 (c (n "separator") (v "0.4.1") (h "1r9cfwknv4p4v5ryd204wqk5j85742ibkcz7sby41w7f8ykl2y7r")))

