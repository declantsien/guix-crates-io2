(define-module (crates-io se pa separable) #:use-module (crates-io))

(define-public crate-separable-0.1.0 (c (n "separable") (v "0.1.0") (d (list (d (n "separable-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0m3jbl7qss82w4xbgblc9lwkak1bd8f6iw5iwq3myvw3j1w6paj5")))

(define-public crate-separable-0.2.0 (c (n "separable") (v "0.2.0") (d (list (d (n "separable-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1k4wy60lzwlnnzz3vi6m16r66g45ayqn89yxri030bcb6n1wm801")))

(define-public crate-separable-0.2.1 (c (n "separable") (v "0.2.1") (d (list (d (n "separable-derive") (r "^0.2.1") (d #t) (k 0)))) (h "080z6bw329x71z5m1gbvn3nsb9h35chjx4hdi233hiryg55wjkg3")))

