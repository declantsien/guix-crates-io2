(define-module (crates-io se gm segment-map) #:use-module (crates-io))

(define-public crate-segment-map-0.1.0 (c (n "segment-map") (v "0.1.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0wpkbn3qykd5h0hy3dp6q418y8dcxc3g2lvy5l2ari7c5ivlv38q")))

(define-public crate-segment-map-0.1.1 (c (n "segment-map") (v "0.1.1") (h "0i38d7f1j4sganv3rf58x19gyi59hdi59cpf2a7jq4h5fkdfnp0v")))

