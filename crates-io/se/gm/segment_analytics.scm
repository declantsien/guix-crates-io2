(define-module (crates-io se gm segment_analytics) #:use-module (crates-io))

(define-public crate-segment_analytics-0.1.1 (c (n "segment_analytics") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0y589wds6sk7r4z6mfy52rg296gqaaai9s8kgd4ckzb1pv8wc7ja")))

(define-public crate-segment_analytics-0.1.2 (c (n "segment_analytics") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "05adrhvzwg036cgcp4l02x24xi6jhcyr5diq1v8nmn7pisb8ym6y")))

(define-public crate-segment_analytics-0.1.3 (c (n "segment_analytics") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1b7dygvcp61r4zbhgrz6w8jv74f7m1z26vhyzgbxrz6h6i27wlai")))

