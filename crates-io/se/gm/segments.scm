(define-module (crates-io se gm segments) #:use-module (crates-io))

(define-public crate-segments-0.1.0 (c (n "segments") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "19hqxq6mm89b8a7q5z4ddzgkrz0avkqjhaxzffjxyqy8z4qrrf38")))

(define-public crate-segments-0.2.0 (c (n "segments") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0bpjsr73frf6cf8favjdms085d787026kf33a9sqn4zmsrxian00")))

(define-public crate-segments-0.3.0 (c (n "segments") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0hifncaayialqaz36n18g14dadhcvvsycf42hblvwzy19rm9r2zx")))

