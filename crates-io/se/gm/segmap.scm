(define-module (crates-io se gm segmap) #:use-module (crates-io))

(define-public crate-segmap-0.1.0 (c (n "segmap") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "permutator") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rangemap") (r "^0.1.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)))) (h "0qqbzp6y1dhc3w0amjjwgsxajqjk6a2jpcfqnwlxb2fvfx2dhl89")))

