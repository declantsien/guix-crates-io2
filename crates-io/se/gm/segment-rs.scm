(define-module (crates-io se gm segment-rs) #:use-module (crates-io))

(define-public crate-segment-rs-0.0.1-alpha.1 (c (n "segment-rs") (v "0.0.1-alpha.1") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "0sn83v6q9mnh63si2svy1dpa8n35b3lksw53zkwm97pihwh52m7d") (y #t)))

(define-public crate-segment-rs-0.0.1-alpha.2 (c (n "segment-rs") (v "0.0.1-alpha.2") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "0liw8mrx0psibigldkxsyz8pgfj83s7dwnwp3vjw7i497lr4ffr2")))

(define-public crate-segment-rs-0.0.1-alpha.3 (c (n "segment-rs") (v "0.0.1-alpha.3") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "1qi6l91xisxjw4igax4rqwabjb6jd6jazgx31mapxgs33i6mzpns")))

(define-public crate-segment-rs-0.0.1-alpha.4 (c (n "segment-rs") (v "0.0.1-alpha.4") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "00p86swnkglwkg66mfrw3mhvg5m3ql7qrfdnyxhc780n3i0pbjwf")))

(define-public crate-segment-rs-0.0.1-alpha.5 (c (n "segment-rs") (v "0.0.1-alpha.5") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "1z0vxhh3am1937cazd2ri1vl1mwjy1jx1dsy8z5z4356r7n6dm90")))

(define-public crate-segment-rs-0.0.1-alpha.6 (c (n "segment-rs") (v "0.0.1-alpha.6") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "05xpyy6l9anklmckjpild4xbk87gqyb4im3jy0l0ifnmknapshnp")))

(define-public crate-segment-rs-0.0.1-alpha.7 (c (n "segment-rs") (v "0.0.1-alpha.7") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "1gaxvg18ym2k5njq3382mif9ld7mndiv7n4z370csk7192n1hx3z")))

(define-public crate-segment-rs-0.0.1-alpha.8 (c (n "segment-rs") (v "0.0.1-alpha.8") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "0sxpnwrri9k70b48681p015ym04y2jpjm61skwimq28544plknkf")))

(define-public crate-segment-rs-0.0.1-alpha.9 (c (n "segment-rs") (v "0.0.1-alpha.9") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "1yivd76k0rp9cldj1h4xv9l722dwcf3dj3dmpgna26r64asnadxw")))

