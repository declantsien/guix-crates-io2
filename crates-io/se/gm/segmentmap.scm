(define-module (crates-io se gm segmentmap) #:use-module (crates-io))

(define-public crate-segmentmap-0.1.3 (c (n "segmentmap") (v "0.1.3") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)))) (h "1g7kldixf9yvgr4irsx50365yj0hx1i3k78rys2bqps42iagrv7j")))

(define-public crate-segmentmap-0.1.4 (c (n "segmentmap") (v "0.1.4") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1qzlyvdv0a0idr00k0qlzmqyw1ws0v9ipn2y973mgrji7zzmz954")))

(define-public crate-segmentmap-0.1.5 (c (n "segmentmap") (v "0.1.5") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0lxjxkksc03r42pfvzs710jmsn0y4kdy6xar2lpqpl581ivcasv2")))

