(define-module (crates-io se gm segment-tree) #:use-module (crates-io))

(define-public crate-segment-tree-1.0.0 (c (n "segment-tree") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0lcq8nwqfjx5xdabh6nlbxkchlfhil4vay6d4j37ryvvlcggjag6")))

(define-public crate-segment-tree-1.1.0 (c (n "segment-tree") (v "1.1.0") (d (list (d (n "num") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1kk39z9lgy6jyiq5x16yy4h4czb33cdpysdczfh60yffs41jvzgx") (f (quote (("num-ops" "num"))))))

(define-public crate-segment-tree-2.0.0 (c (n "segment-tree") (v "2.0.0") (d (list (d (n "num-bigint") (r "~0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "06i7pdxps8srzrxj48dn2gb3hx94d1i7av98qg3sdfna686vsz9z")))

