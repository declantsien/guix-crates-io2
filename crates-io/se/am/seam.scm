(define-module (crates-io se am seam) #:use-module (crates-io))

(define-public crate-seam-0.1.2 (c (n "seam") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "15k3s8kxvn3ap87amzh8y9cr7cb00j6dnvcx6fmj1yplbxglcnms") (f (quote (("debug"))))))

(define-public crate-seam-0.1.3 (c (n "seam") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "1l4md9sz4mpmxqc26fwvggiipkmb183ixjm96fkj315hdgzjxv2n") (f (quote (("debug"))))))

(define-public crate-seam-0.1.4 (c (n "seam") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "1341abgjfgicy3z4pb7h5psshymy1nqh2xha6dz2n2xq7xz5bhrx") (f (quote (("debug"))))))

(define-public crate-seam-0.1.5 (c (n "seam") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "1sv2l6kg2n5wnnkr5dkc7p83i4pfr99l2n2x3vdsnzg3akk98ws6") (f (quote (("debug"))))))

(define-public crate-seam-0.1.6 (c (n "seam") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "0zfws815g6xs6gkg84svp5ij86ga4j9aqsljpcwc6sjrr3gz9mdw") (f (quote (("debug"))))))

(define-public crate-seam-0.1.7 (c (n "seam") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "1vx23wc59c227zl59lwc1ari79skkrmr7h7wbawcimgkvak1byy7") (f (quote (("debug"))))))

(define-public crate-seam-0.1.8 (c (n "seam") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)))) (h "0r5pc5zdbnp1282pl9333wzsg02i05ak8hly47syja36yh10dk1b") (f (quote (("debug"))))))

