(define-module (crates-io se am seamapi-rs) #:use-module (crates-io))

(define-public crate-seamapi-rs-0.1.0 (c (n "seamapi-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0f1cvbnlycwjw7s1m7m7cik5pzzf05rf8084z9ld5hmwnsyxc5jx")))

(define-public crate-seamapi-rs-0.1.1 (c (n "seamapi-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0yxng68dklp723h5sh7zkpilsjd4qacyw130r80028wvf2x4c587")))

