(define-module (crates-io se am seamless_macros) #:use-module (crates-io))

(define-public crate-seamless_macros-0.0.1 (c (n "seamless_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06gvpzssgvfgmhcaqn876psgqcd011d0byvhhrgyh2rnaqq12zfa")))

(define-public crate-seamless_macros-0.1.0 (c (n "seamless_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08q5m4zwss45c2ap66y0bmfwh7wq7cn0wxb4vjsvggpm4yfi9zc3")))

(define-public crate-seamless_macros-0.2.0 (c (n "seamless_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xdcq88qy99rrnqpmpf5x2s1l82inzf36b5rvyag5zdc8q4jhzjc")))

(define-public crate-seamless_macros-0.3.0 (c (n "seamless_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1idsp8bvaqyq6wqy7f5kjx879zqn3pz8mhzkvskvdifj7mq4ppmw")))

(define-public crate-seamless_macros-0.4.0 (c (n "seamless_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sqn0n58wvksghn7j3qq8js95y6xz2vwdfd5c183vv16nbhnmz6k")))

(define-public crate-seamless_macros-0.5.0 (c (n "seamless_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07gxy982rbmf0yya25lsmdphhanjchk9pfa152z4kmw5chnf1817")))

(define-public crate-seamless_macros-0.5.1 (c (n "seamless_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ijzx28x0kp4zr5jbd4x33hfg1d5if1ys4s2rr73yd32sssww917")))

(define-public crate-seamless_macros-0.6.0 (c (n "seamless_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02vi6fx5kzll6xgrak0sxd8lffwdj5y6vin8m2pzhzw0qf0zd8fn")))

(define-public crate-seamless_macros-0.7.0 (c (n "seamless_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iy6xcq0lfa7phpffppymxnkq7vg8833bmqgd6invncrdap5ajq8")))

(define-public crate-seamless_macros-0.7.1 (c (n "seamless_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qyf8d37b6mzcmrhqky6h4dkvh5ljj4xdazd53crw5lmvihhh40z")))

(define-public crate-seamless_macros-0.7.2 (c (n "seamless_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gwh5gdlyv5vksp3ah6pw9rhxx9mwwx66mw6cgckcd4zfjjfw2zh")))

(define-public crate-seamless_macros-0.8.0 (c (n "seamless_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07g63hgh3a03hrgzi6gfs4r4p059achkda6ypnn374arcb9ybc12")))

(define-public crate-seamless_macros-0.9.0 (c (n "seamless_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19zc8hl0a5nx4gkq2k8fip9smjxap5h47i70lq8p3js64fnhkqr0")))

(define-public crate-seamless_macros-0.10.0 (c (n "seamless_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seamless") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15szq6aca7s20z5l9rza1lss40h8i0y2q44lglg38yypicjzymf9")))

