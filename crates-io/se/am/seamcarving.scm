(define-module (crates-io se am seamcarving) #:use-module (crates-io))

(define-public crate-seamcarving-0.0.1 (c (n "seamcarving") (v "0.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0fmih8i0nxlgi2bjcnqhhvnrvq289kfxssxcl2w8vh1zvbiiw6wv")))

(define-public crate-seamcarving-0.0.2 (c (n "seamcarving") (v "0.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1bgbdvi8qfpbcr8lsfhdls7f57cwfgdpq4v87g2zx71y88ai05zs")))

(define-public crate-seamcarving-0.1.0 (c (n "seamcarving") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)))) (h "1zma5bwxr20icyhs8frfr3z01idm0qcrc94g5ywh2pkyvz69nyy1")))

(define-public crate-seamcarving-0.2.0 (c (n "seamcarving") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "imageproc") (r "^0.18.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pathfinding") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0ziavsa9ccqzgkc5ccdbkivqmrlkfphdp7m3rky3kvqqpn66cfqm")))

(define-public crate-seamcarving-0.2.1 (c (n "seamcarving") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "imageproc") (r "^0.18") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hmj287yysmhf8i9zzws3nh98rvh3cqb213giis9l68yc1wpn0ph")))

(define-public crate-seamcarving-0.2.2 (c (n "seamcarving") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "imageproc") (r "^0.18") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h2xmgmwzs28ab19d75ivvciyycz16dqa4dm636mbx241yshlk8s")))

(define-public crate-seamcarving-0.2.3 (c (n "seamcarving") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07f39l03p9frq3ck6x4vj5d53sr5x83dpmxqi6xknrd4m5nj0wfp")))

