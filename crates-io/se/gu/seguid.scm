(define-module (crates-io se gu seguid) #:use-module (crates-io))

(define-public crate-seguid-0.1.0 (c (n "seguid") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "duval-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1w25dbvyz9ka8cs1fr21pkx5s6rms0q052vc5vyw4hbdqnifgm5d")))

