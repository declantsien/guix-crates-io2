(define-module (crates-io se xi seximal) #:use-module (crates-io))

(define-public crate-seximal-0.1.0 (c (n "seximal") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1f57rh77bnxl7acq5ilrg34n0rsdci6k3yxjmz8z5f2y0x5b8cv6")))

(define-public crate-seximal-0.1.1 (c (n "seximal") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "17z90qx3f9wd9krrdc52gd9n6adxlv92fz6vzsxc8bjzc844r6l4")))

(define-public crate-seximal-0.1.2 (c (n "seximal") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "14aws5qq1gai05gr20brak70l1xd8dfgkqn3zqdlw0iicdg5k5fn")))

(define-public crate-seximal-0.1.3 (c (n "seximal") (v "0.1.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1k3a3q3igx0ckhd0jiq7hibxm29m7lpwplxwn3rxy3swc0pp7snc")))

(define-public crate-seximal-0.1.4 (c (n "seximal") (v "0.1.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1460prng7wcdhvj4akl5ivhww2mrfzd90ma5b388ac80ly7cs8yl")))

(define-public crate-seximal-0.1.5 (c (n "seximal") (v "0.1.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0lj238lprgcy2724hldxlq4z5aqgig1biiqw9l53syfny1y6846f")))

