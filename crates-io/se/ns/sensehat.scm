(define-module (crates-io se ns sensehat) #:use-module (crates-io))

(define-public crate-sensehat-0.1.0 (c (n "sensehat") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "measurements") (r "^0.3.0") (d #t) (k 0)))) (h "1im62ar0ya0q7xrd6p05772riv1phv0j330ygb6yldsbh038y6vi")))

(define-public crate-sensehat-0.2.0 (c (n "sensehat") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "measurements") (r "^0.4.0") (d #t) (k 0)))) (h "0nlnjifgk69ncd249xcxiw45l3prdin2qldj7zjvb6gsd2r3m81q")))

(define-public crate-sensehat-0.2.1 (c (n "sensehat") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.4.0") (d #t) (k 0)))) (h "17rzk2i9cd929wcdwky07xmddgz7jwmiidx9cpxn5w75l5cmhqrw") (f (quote (("rtimu" "libc") ("default" "rtimu"))))))

(define-public crate-sensehat-0.3.0 (c (n "sensehat") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.7.0") (d #t) (k 0)))) (h "1sax2hcy4d5kc8zqpbv6z5qyb5mjff9h4p6zyzxwjxvagwhjxcck") (f (quote (("rtimu" "libc") ("default" "rtimu"))))))

(define-public crate-sensehat-0.4.0 (c (n "sensehat") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.10.2") (d #t) (k 0)))) (h "0f4z26l5w38l5bnrl32hr6jq0nr7bck5yc0z7p5hgpksiihbgbxi") (f (quote (("rtimu" "libc") ("default" "rtimu"))))))

(define-public crate-sensehat-0.5.0 (c (n "sensehat") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.10.2") (d #t) (k 0)) (d (n "sensehat-screen") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17kdyw9j5i0fifvqsgdzcqmdvxvmm3w95hm2jk3zlg6wlq823mvp") (f (quote (("rtimu" "libc") ("led-matrix" "sensehat-screen") ("default" "rtimu" "led-matrix"))))))

(define-public crate-sensehat-1.0.0 (c (n "sensehat") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.10.2") (d #t) (k 0)) (d (n "sensehat-screen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tint") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07n30sgisbpf8x9gh37l7dlp5crvf7afd1l3qpak04f63bfka4pn") (f (quote (("rtimu" "libc") ("led-matrix" "sensehat-screen" "tint") ("default" "rtimu" "led-matrix"))))))

(define-public crate-sensehat-1.1.0 (c (n "sensehat") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "i2cdev") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "measurements") (r "^0.10.2") (d #t) (k 0)) (d (n "sensehat-screen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tint") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1448iag1hl4vm9lqlcac8y779v4gl7kgg4v64rx1dxsd7j1dmqyr") (f (quote (("rtimu" "libc") ("led-matrix" "sensehat-screen" "tint") ("default" "rtimu" "led-matrix"))))))

