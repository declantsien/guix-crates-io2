(define-module (crates-io se ns sensors) #:use-module (crates-io))

(define-public crate-sensors-0.0.1 (c (n "sensors") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.1.0") (d #t) (k 0)))) (h "04f5zgmf24wm6i8mqmrg00x7c6hn006aipfxgs2l9w1n1vk7w3zh")))

(define-public crate-sensors-0.0.2 (c (n "sensors") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1mgngnsfi1k0c2cggmac956b61lg0rki5ak2p8p5rydg4mjxii4r")))

(define-public crate-sensors-0.0.3 (c (n "sensors") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0bi74g1zncdckdf6agm7ffnbw9p83i2aw76kr43s1r4w14wh4fnc")))

(define-public crate-sensors-0.1.0 (c (n "sensors") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1dr7n6j7khz4z98qvhv4m7hq126qx3faq3lpiikswvp18a800477")))

(define-public crate-sensors-0.1.2 (c (n "sensors") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "libsensors-sys") (r "~0.2") (d #t) (k 0)))) (h "12bsvz5k280avmq0h7k1s37jan9zrc38vj0r0rmwy2mn308aklj3")))

(define-public crate-sensors-0.2.0 (c (n "sensors") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.2") (d #t) (k 0)))) (h "0403pfsafiyal81dy73fy4lspmbd5x4bdynpa6jaw0dz14xxsv5x")))

(define-public crate-sensors-0.2.1 (c (n "sensors") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.2") (d #t) (k 0)))) (h "0vn0y0a0vf5apnxwkw109dkv5j6ph1zvwpsv53s9c7zgjvnai4v9")))

(define-public crate-sensors-0.2.2 (c (n "sensors") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsensors-sys") (r "^0.2") (d #t) (k 0)))) (h "1l6nvmbbbbl9mdkd9806mmls68d6hvqb2nl0nm14chva5xwz8fks")))

