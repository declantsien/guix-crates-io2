(define-module (crates-io se ns sensorlog) #:use-module (crates-io))

(define-public crate-sensorlog-0.1.0 (c (n "sensorlog") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "0072f4wjd800l9jy9h8vfi8x2z3nr3yn62m10wn71l00x1lgfg66")))

(define-public crate-sensorlog-1.0.0 (c (n "sensorlog") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "174bkzz281g4kxkgpi6jr3chw8k54namz80jfwi71px04mpkhwv3")))

