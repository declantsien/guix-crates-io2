(define-module (crates-io se ns sensitive-words-recognizer-new) #:use-module (crates-io))

(define-public crate-sensitive-words-recognizer-new-0.2.0 (c (n "sensitive-words-recognizer-new") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fsbglnjghgv9djqvk1n1q4v470fwxicf126nalm8yipdaaii6xv")))

(define-public crate-sensitive-words-recognizer-new-0.3.0 (c (n "sensitive-words-recognizer-new") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "17gq29n7hq634kd8x96d3fwn2z60777rmpc4k50gycgy3wg12329")))

