(define-module (crates-io se ns senseair) #:use-module (crates-io))

(define-public crate-senseair-0.1.0 (c (n "senseair") (v "0.1.0") (h "1scqpszngxwsh90nqcdb7hbayjpg2jhm8647x1vfs0rbagf3gjbp")))

(define-public crate-senseair-0.2.0 (c (n "senseair") (v "0.2.0") (h "0v02f1s86vphnr42g3dy3rcra1dwr5sjjdi5hkbphxdrkqfvvzqy")))

(define-public crate-senseair-0.3.0 (c (n "senseair") (v "0.3.0") (h "1ppjlsgcr8xg885sl4sb9n2lpdrjv55lqij4lvcr7jsqyylwv10q")))

(define-public crate-senseair-0.4.0 (c (n "senseair") (v "0.4.0") (h "1kj5x0a69qqkhji09dgjfsarzmklswfb33d8mq8yjab2asnb86ff")))

