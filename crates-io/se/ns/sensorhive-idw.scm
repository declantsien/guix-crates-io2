(define-module (crates-io se ns sensorhive-idw) #:use-module (crates-io))

(define-public crate-sensorhive-idw-0.1.0 (c (n "sensorhive-idw") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.12") (d #t) (k 0)))) (h "03k1l9y1nk96mh91c8sdvcdj4qkg3m8hm80fyi82gz3vkp2q7a1m")))

(define-public crate-sensorhive-idw-0.1.1 (c (n "sensorhive-idw") (v "0.1.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)))) (h "1gqq7almwl6vszgzckn1zj0j93qn998qxp2d4glcfa47hbn0hj3h")))

