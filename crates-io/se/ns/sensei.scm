(define-module (crates-io se ns sensei) #:use-module (crates-io))

(define-public crate-sensei-0.1.0 (c (n "sensei") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1j77h36zlz5hjdz08fxk6a919g5vic1ggxzx1h03x73wflpsxmi7")))

(define-public crate-sensei-0.1.1 (c (n "sensei") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1jwacc7mmgzfdp6qcykybpikiijq0w6wnfj8hylmi7i78sr05vd8")))

(define-public crate-sensei-0.1.2 (c (n "sensei") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1f1f8cprxhi99n6vngb19yls8rhjbp5c76jhv1rbmlfgm1v9rig3")))

(define-public crate-sensei-0.1.3 (c (n "sensei") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1xppi5i79yz9c3lxq8wwv10mia9slii7fnsqdn6h2j97gagsdp63")))

(define-public crate-sensei-0.1.4 (c (n "sensei") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0ckr1s1463kbhism6fx7z4vzswkd5ldsj0mk34r2vilwcb3586yv")))

(define-public crate-sensei-0.1.5 (c (n "sensei") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "isahc") (r "^0.9.13") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1zkhcdsvnlf0flr24nx3rcayh2crqb7235j4ay9mzy3c7k2v0gl8")))

(define-public crate-sensei-0.1.6 (c (n "sensei") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "isahc") (r "^0.9.13") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "15laacbfx025jd47959b36ryz6klmjckk5wab43y79yhcvk5j0b5")))

(define-public crate-sensei-0.1.7 (c (n "sensei") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0bcq2p15qp7nv8wyf1mad3waw2f5gw2i1q0gnf7g285kb69ijccv")))

(define-public crate-sensei-0.1.8 (c (n "sensei") (v "0.1.8") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "00zjpkvmq0wphcv4r7mm1qm3263l9kxd1b3fiqdf2i9vvrka5r32")))

(define-public crate-sensei-0.1.9 (c (n "sensei") (v "0.1.9") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1xbs4f4w4a820w4106x23qg425k1smabpmjyg7diq19v37hlwr2m")))

(define-public crate-sensei-0.1.10 (c (n "sensei") (v "0.1.10") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1lbhwwh8dmbrc9i9xryx79a8mkk5mqgn1713ax12df79cj11fpsi")))

(define-public crate-sensei-0.1.11 (c (n "sensei") (v "0.1.11") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0ba3ps266f53lpwg8xw3hhkbr6h11h28k4rjlrm8dybwqn0v35xa")))

(define-public crate-sensei-0.1.12 (c (n "sensei") (v "0.1.12") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0agh80ng93mzj37zv6g8nanz0apjdyy78fbr06yaddvyignsq5cj")))

(define-public crate-sensei-0.2.0 (c (n "sensei") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "19w898h9zf1vjci6s9hiv35ff7w6i4b541mpnz35ys0h1p2m4378")))

(define-public crate-sensei-0.2.1 (c (n "sensei") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "0bya8zz8kg4cjjix59gca4nkvbzk3m3sn18fldiikl12yxb5d88y")))

(define-public crate-sensei-0.2.2 (c (n "sensei") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "1mp7l0ly3bsbcm001scd2bi0zyzyms2cr25npv7f9s1f2fmgg1x7")))

(define-public crate-sensei-0.2.3 (c (n "sensei") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "09dwfw44zqx10kz3bn8md4as2wsdrpm57d812g7mawm4l6r3pcm1")))

(define-public crate-sensei-0.2.4 (c (n "sensei") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "12wx1md5ja5qm4bnqk7gd3y37c4lxmmj5al5s97gxpw6zih0j6lv")))

(define-public crate-sensei-0.2.5 (c (n "sensei") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)))) (h "0ks5p2gadwgk66qwlkwl45802ks8nhlr65y66p3rckgg5lk72qyg")))

(define-public crate-sensei-0.2.6 (c (n "sensei") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "open") (r "^2.0.0") (d #t) (k 0)))) (h "1iaiqmvbvxwxn1zq5f1sapx7ww92v2n6c5wq706gblwm27vf7ysv")))

(define-public crate-sensei-0.2.7 (c (n "sensei") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "open") (r "^2.0.0") (d #t) (k 0)))) (h "0szm1drviysm1fajw2m2k8idgl444zf4yn2xd4kdfkn841nrjyqf")))

(define-public crate-sensei-0.2.8 (c (n "sensei") (v "0.2.8") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "open") (r "^2.0.0") (d #t) (k 0)))) (h "1w3lp6n61hgi3c1p11l3q97cf9acrngjp96brizg58f3xf3pq8xb")))

(define-public crate-sensei-0.2.9 (c (n "sensei") (v "0.2.9") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "open") (r "^2.0.0") (d #t) (k 0)))) (h "0lasn77w6z4z05nl3l7yvqdmxfnj5v1kxgah689334fazd16gsav")))

