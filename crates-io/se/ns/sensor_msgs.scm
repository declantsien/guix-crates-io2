(define-module (crates-io se ns sensor_msgs) #:use-module (crates-io))

(define-public crate-sensor_msgs-4.2.3 (c (n "sensor_msgs") (v "4.2.3") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "geometry_msgs") (r "^4.2.3") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "std_msgs") (r "^4.2.3") (d #t) (k 0)))) (h "0bv7278xqdg324gql0jr266lrpz4ws54clq2divw0wdflwj6qbyc") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "builtin_interfaces/serde" "std_msgs/serde" "geometry_msgs/serde"))))))

