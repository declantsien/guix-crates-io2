(define-module (crates-io se ns sensirion-hdlc) #:use-module (crates-io))

(define-public crate-sensirion-hdlc-0.1.0 (c (n "sensirion-hdlc") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)))) (h "1gflwvp6bzcnixz7jxxnia0gl2qahwg012g94vcw2s6a6aksdclx")))

(define-public crate-sensirion-hdlc-0.2.0 (c (n "sensirion-hdlc") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)))) (h "1n0nlwwhjs7hyj77zd7mkh0k89ndcgb4bzyaj7lwixz6m81wmnqs")))

