(define-module (crates-io se ns sense) #:use-module (crates-io))

(define-public crate-sense-0.0.0 (c (n "sense") (v "0.0.0") (h "0l8m5lnccsyf95b1wsz62qf49ay3jd2hrs0k507klajqiqc5svj6")))

(define-public crate-sense-0.0.3 (c (n "sense") (v "0.0.3") (h "1ka41wy709gv9m0mv5nv9iw3bn4jjnhdrdw38ivdc00jjykgpd54")))

