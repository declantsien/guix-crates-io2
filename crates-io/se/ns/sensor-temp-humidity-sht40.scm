(define-module (crates-io se ns sensor-temp-humidity-sht40) #:use-module (crates-io))

(define-public crate-sensor-temp-humidity-sht40-0.2.6 (c (n "sensor-temp-humidity-sht40") (v "0.2.6") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1.1") (d #t) (k 0)))) (h "1jwl1pny9kykq3682wgwipnw588xy8byia5akp3zsa8r12aidx7f")))

(define-public crate-sensor-temp-humidity-sht40-0.2.600 (c (n "sensor-temp-humidity-sht40") (v "0.2.600") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1.1") (d #t) (k 0)))) (h "1r1n3pciyimn1ds8jfgnspp8k1a3xpby2hbl6n0vxw4h9z7slmzp") (f (quote (("fp"))))))

(define-public crate-sensor-temp-humidity-sht40-0.2.601 (c (n "sensor-temp-humidity-sht40") (v "0.2.601") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1.1") (d #t) (k 0)))) (h "13ih2wg97vgg3461hyxjqipb4j7xqvr2kykb91h8cycc9whg47cw") (f (quote (("fp"))))))

