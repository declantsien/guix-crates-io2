(define-module (crates-io se ns sensible-env-logger) #:use-module (crates-io))

(define-public crate-sensible-env-logger-0.0.1 (c (n "sensible-env-logger") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "15l76j1nf5sdxznj3za4q3ci5caaj6hyp0dl490aby339g82gaxn") (f (quote (("local-time" "chrono/clock")))) (y #t)))

(define-public crate-sensible-env-logger-0.0.2 (c (n "sensible-env-logger") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "12wn4ms548icrc5l5247fr0zn9f715w1ix3a9r6lzn665dvl6s9k") (f (quote (("local-time" "chrono/clock")))) (y #t)))

(define-public crate-sensible-env-logger-0.0.3 (c (n "sensible-env-logger") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1yxns8gry9g9vc1sn764zmqsw5ymwf87nxc4jm1rwydbb95gifmm") (f (quote (("local-time" "chrono/clock")))) (y #t)))

(define-public crate-sensible-env-logger-0.0.4 (c (n "sensible-env-logger") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "19s5fc1z4slmvwv3ksb8957fv26d3rr43fm20wqd33fybgrn6jgb") (f (quote (("local-time" "chrono/clock")))) (y #t)))

(define-public crate-sensible-env-logger-0.0.5 (c (n "sensible-env-logger") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0fd7xbwi7pqrsm3ahv2zpg12h9xp1ym66zll5lz5knz2f305mfas") (f (quote (("local-time" "chrono/clock")))) (y #t)))

(define-public crate-sensible-env-logger-0.0.6 (c (n "sensible-env-logger") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1q9bwpc3h7bnrm8qiavkzxwfqp137rfahw32s9aspq05h6aqi2gf") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.0.7 (c (n "sensible-env-logger") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0nqbdihv6ss4hg1m0bllzkik2qdr9fvq5i435cnw0j4kjf5cr4v9") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.1.0 (c (n "sensible-env-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1jzh1yss44gps894vc3qhxv3i5gkdcmx0d2rdpcgdgffm1ps7zzb") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.2.0 (c (n "sensible-env-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "09fp1ncshrwd8l95z83bjmck6xkjqcfsjxy4np1y5wbs5raax9v1") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.3.0 (c (n "sensible-env-logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1s1yycihz1z60p4j2is1v4l8fsrvv5z9mci406nkzmyw1jxsgnm2") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.3.1 (c (n "sensible-env-logger") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0cag54d3193y0naibily85dzbfw1xsp40qf9494ybkvnqm1q1mnf") (f (quote (("local-time" "chrono/clock"))))))

(define-public crate-sensible-env-logger-0.3.2 (c (n "sensible-env-logger") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1xrgf188dxddfc93axcp4zqhcg12g78fqw14pa5lc7xxbkxx0411") (f (quote (("local-time" "chrono/clock"))))))

