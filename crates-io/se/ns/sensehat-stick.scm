(define-module (crates-io se ns sensehat-stick) #:use-module (crates-io))

(define-public crate-sensehat-stick-0.1.0 (c (n "sensehat-stick") (v "0.1.0") (d (list (d (n "evdev") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)))) (h "10skik4ypl76qhvxrwgvk3ny25wzpb9aalmf181z1vgz9pnkamr4") (f (quote (("poll" "mio") ("linux-evdev" "evdev") ("default" "linux-evdev" "poll"))))))

