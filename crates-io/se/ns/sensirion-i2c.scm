(define-module (crates-io se ns sensirion-i2c) #:use-module (crates-io))

(define-public crate-sensirion-i2c-0.1.0 (c (n "sensirion-i2c") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0zbzl4z128flfsbz76pdn0530bbg0cp18f9vw0x17cl3mbf7njpz")))

(define-public crate-sensirion-i2c-0.1.1 (c (n "sensirion-i2c") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "1mk8hwjidcw6cs0d7pz1djfs5jk8kp09vgdfkwgjh3w55zssiy1p")))

(define-public crate-sensirion-i2c-0.2.0 (c (n "sensirion-i2c") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "196cag4a312yb6pws8580ch9g4zi2lfpbr02i22dz8daaxxfz0nj")))

(define-public crate-sensirion-i2c-0.3.0 (c (n "sensirion-i2c") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (d #t) (k 2)))) (h "1z5s8l24jvsb5n9v0bxcxcbc51dggd0cqw766yg4x6zw8n801faz")))

