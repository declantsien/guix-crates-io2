(define-module (crates-io se ns sensulator) #:use-module (crates-io))

(define-public crate-sensulator-0.4.0 (c (n "sensulator") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1pdh0cv89pmfg0k02jz8cvmcxv7zvxbmzpiizhc70rb5i2f1r7zz")))

(define-public crate-sensulator-0.4.2 (c (n "sensulator") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0xa11jaq8g2h5phj23fy0lrf0dwvrgfcm0khncrvz9bd7bylxaad")))

(define-public crate-sensulator-0.5.0 (c (n "sensulator") (v "0.5.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (t "cfg(unix)") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "rand_distr") (r "^0.4.3") (k 0)))) (h "02fxxfjr2mn0bbxp47jdzq5gzswxpxbjydj89qxh3zm0swxzfrix")))

