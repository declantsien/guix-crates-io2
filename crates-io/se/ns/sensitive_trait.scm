(define-module (crates-io se ns sensitive_trait) #:use-module (crates-io))

(define-public crate-sensitive_trait-0.1.0 (c (n "sensitive_trait") (v "0.1.0") (h "020m4chjha80r9c7vvql3pbmplmbydrfrsqpyj2svfafby5nrdrr")))

(define-public crate-sensitive_trait-0.1.1 (c (n "sensitive_trait") (v "0.1.1") (h "00680ajny0qm4ny8klvpichnc4hahmlvm9ic8wdlr7bv4d7skpw3")))

(define-public crate-sensitive_trait-0.1.2 (c (n "sensitive_trait") (v "0.1.2") (h "1z8padiw9ix86c8z57j54i7q3c04wmrz0cl4p5m1h2i8dpx0j6ys")))

