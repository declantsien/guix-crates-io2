(define-module (crates-io se ns sensitive-words-recognizer) #:use-module (crates-io))

(define-public crate-sensitive-words-recognizer-0.1.0 (c (n "sensitive-words-recognizer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09l4ngmiqhlzlnv6wsgykqncx422kx1gw1r7nsdfbdqamgib67ha")))

(define-public crate-sensitive-words-recognizer-0.1.1 (c (n "sensitive-words-recognizer") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1rx2bmk85f72fr1n9f0cdw3y8cjz4r1n2vn608q6ljnd9a676li2")))

