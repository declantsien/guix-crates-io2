(define-module (crates-io se ns sensd) #:use-module (crates-io))

(define-public crate-sensd-0.0.7-beta (c (n "sensd") (v "0.0.7-beta") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "pid") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1xnxybzkydrmby652vlw5nprhwidlmvz96gf1402ch70hnb41c4f")))

