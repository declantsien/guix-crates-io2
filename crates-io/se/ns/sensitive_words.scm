(define-module (crates-io se ns sensitive_words) #:use-module (crates-io))

(define-public crate-sensitive_words-1.0.0 (c (n "sensitive_words") (v "1.0.0") (h "0lb7vr1vrdsqfyzi1x7f2fywk2k38069hv8vi0zkrfrw2a5xdzdl")))

(define-public crate-sensitive_words-1.0.1 (c (n "sensitive_words") (v "1.0.1") (h "1133rbjgqmdldbpf9yzz2k3psj8xgw2ngn0c01h8hr9ag0n0rj8h")))

(define-public crate-sensitive_words-1.0.2 (c (n "sensitive_words") (v "1.0.2") (h "0pqfifsqbjwx84mf24xhmr917c6rj7zgqnlzjp88wcnaxlajpir7")))

(define-public crate-sensitive_words-1.0.3 (c (n "sensitive_words") (v "1.0.3") (h "0ymnk8nfxb2sv5ia65s921mn5zg0m6xgbwmv8v6asdpvrkfbwzww")))

