(define-module (crates-io se ns sensors-sys) #:use-module (crates-io))

(define-public crate-sensors-sys-0.1.0 (c (n "sensors-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1nxp03558jl1mmjgqb7dbyv0g9rvxczj821zj695i5fjy1pbxyvq") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.0 (c (n "sensors-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0wzmairyxm4xvzj052xlii6x4f9534zbqm95dj5fh0hcgb2qsyxs") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.1 (c (n "sensors-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0rqhhqa20dknydr7yf927jci1bglj7jgff4d9f9ml5fvmhs3h0jl") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.2 (c (n "sensors-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "07pp5ais71hadk0w6hn8ms86mzdv4w80k41agm47xm2ikb8drqp5") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.3 (c (n "sensors-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.62") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0hy9dpmc83a95bcr67xfjiik5lhi0hz9lvwgq2wg65zwfbpb6bhv") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.4 (c (n "sensors-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "105y2ggyzki7wcsda0vmc13m5wwkln3zq0ssrsdm7sd1zy49rbsi") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.5 (c (n "sensors-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1gxw51m09l0fr2mragfhssg78jcdviszisdpbg7p41g2wcc2azi2") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.6 (c (n "sensors-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1lk3pcs1788bhkr3fcxdhwk0rbh5nbi7cxaqvj67smhkaygmxxg8") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.7 (c (n "sensors-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "01zf50vwyy7636x4rvldz3pn5ngj5vylxr9pabq276ggy520jfac") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.8 (c (n "sensors-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "0zi5zhz1spy1q0gqwv2xbfc4mk75dnc1q91msa0j4pxjza6n79g3") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.9 (c (n "sensors-sys") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "0mx0cwvl4q16qf199a0av4g1aipfgz2wqhanzvqbbqnb0xd949na") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.10 (c (n "sensors-sys") (v "0.2.10") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "080slmlixzcaf1x40403srjlh8h992lma6303inkwh7pcssss3bk") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.11 (c (n "sensors-sys") (v "0.2.11") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "1acmx1fclc453my5fxn0cjd072zasiss9qbksh8qsvqywxqijv9s") (f (quote (("static")))) (l "sensors")))

(define-public crate-sensors-sys-0.2.12 (c (n "sensors-sys") (v "0.2.12") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "0d8xj4f1fs773nn9wnmfx4z9zdp167ypidxqcgjv1lgfxlds2ah7") (f (quote (("static")))) (l "sensors")))

