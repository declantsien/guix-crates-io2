(define-module (crates-io se ns sensible) #:use-module (crates-io))

(define-public crate-sensible-0.1.0 (c (n "sensible") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l1lhaxfjjql0pfk7yk8pz6x0i02mqwa0nhj91c4yksngdpffa6z")))

