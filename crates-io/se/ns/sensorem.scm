(define-module (crates-io se ns sensorem) #:use-module (crates-io))

(define-public crate-sensorem-0.1.0 (c (n "sensorem") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sensors") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0r54iv98207wvc5x1q72fbd9d00xk2axybhvqmrablk1zm6hsi7b")))

