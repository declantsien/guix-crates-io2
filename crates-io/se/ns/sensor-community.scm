(define-module (crates-io se ns sensor-community) #:use-module (crates-io))

(define-public crate-sensor-community-0.1.0 (c (n "sensor-community") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1iqiwxs6ka0x49msf7s559bx6pkr4n8458my9hgf27706jf9hlpy")))

