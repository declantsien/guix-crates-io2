(define-module (crates-io se me semeion) #:use-module (crates-io))

(define-public crate-semeion-0.1.0 (c (n "semeion") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 2)))) (h "1nwyqcgnqrk3ylw0jkxzdkwd7l5zxz65xww7dp7qw5k9cc636n6n")))

(define-public crate-semeion-0.2.0 (c (n "semeion") (v "0.2.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0dp04kjz13msdxfdw8y23q1rsqqj13clsl5jg19mgp0warasb3v0")))

(define-public crate-semeion-0.3.0 (c (n "semeion") (v "0.3.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gfc6a19s9lfkv8d0sx7bn1l7vlvmizmkm1jxsszaqk2q2yl2qnb")))

(define-public crate-semeion-0.4.0 (c (n "semeion") (v "0.4.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "16ph3jqp91ibwl7skdm4l5yfmydw6640704lnq9bsv191vxaz08z")))

(define-public crate-semeion-0.5.0 (c (n "semeion") (v "0.5.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1a10hjqghqizfz5pyzwmwnr4b5l1gjqihj2gdkl2qnbwbv7bsziy")))

(define-public crate-semeion-0.6.0 (c (n "semeion") (v "0.6.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17yyyy32hzsywnf0jl9n9d9w2cx6dmimj96b4nhnb1nll8dcm6v5")))

(define-public crate-semeion-0.7.0 (c (n "semeion") (v "0.7.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06w3f0wy7g34xqyfm6dvi5im0bw67482fj2lmk6jncjh9v3h5qm4")))

(define-public crate-semeion-0.8.0 (c (n "semeion") (v "0.8.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0i00f6fwlp9dxjnsi0bw68v7fxs0ml9mzhpw72bfa5w9b6fd58dw") (f (quote (("parallel" "rayon"))))))

(define-public crate-semeion-0.8.1 (c (n "semeion") (v "0.8.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1r1r5jj7ppy4a2ynqhpwpiz83r4xfbbm6hj4chm9ilixmwnwklpb") (f (quote (("parallel" "rayon"))))))

(define-public crate-semeion-0.9.0 (c (n "semeion") (v "0.9.0") (d (list (d (n "ggez") (r "^0.6.0-rc2") (d #t) (k 2)) (d (n "num-complex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0sf9fp5w4jfmsz3nmii8bphy7cdp8jz39p1nsix05fvnnrpg27ad") (f (quote (("parallel" "rayon"))))))

(define-public crate-semeion-0.9.1 (c (n "semeion") (v "0.9.1") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1c4bpv5c2pzxv73bchjdgmybs2mnjg631wvb184mpdf368v64f6i") (f (quote (("parallel" "rayon"))))))

