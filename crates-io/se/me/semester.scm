(define-module (crates-io se me semester) #:use-module (crates-io))

(define-public crate-semester-1.0.0 (c (n "semester") (v "1.0.0") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "semester-macro") (r "^1.0.0") (k 0)))) (h "0cqfwvajszq7zddgp7wxbicqx9qkc32bzghi58ylhkgpyklkw691") (f (quote (("std" "alloc" "semester-macro/std") ("default" "std") ("alloc" "semester-macro/alloc"))))))

(define-public crate-semester-1.0.1 (c (n "semester") (v "1.0.1") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "semester-macro") (r "^2.0.0") (k 0)))) (h "1klqyrnzmbji6id3sblpp5xvrbll37dks5jh5vfv61cww37l7va1") (f (quote (("std" "alloc" "semester-macro/std") ("default" "std") ("alloc" "semester-macro/alloc"))))))

(define-public crate-semester-1.0.2 (c (n "semester") (v "1.0.2") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "semester-macro") (r "^2.0.1") (k 0)))) (h "05rvxcimvnv2wn0hpgrpc8xvlk3nfq9by0brcl8fd2k0iq5jn7mb") (f (quote (("std" "alloc" "semester-macro/std") ("default" "std") ("alloc" "semester-macro/alloc"))))))

(define-public crate-semester-1.0.3 (c (n "semester") (v "1.0.3") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)) (d (n "semester-macro") (r "^2.0.2") (k 0)))) (h "1w6fiz56l0k02pkf3bg934b36kinjd7ws43am02vcgf4b33k7ky0") (f (quote (("std" "alloc" "semester-macro/std") ("default" "std") ("alloc" "semester-macro/alloc"))))))

