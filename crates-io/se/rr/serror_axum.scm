(define-module (crates-io se rr serror_axum) #:use-module (crates-io))

(define-public crate-serror_axum-0.1.0 (c (n "serror_axum") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "serror") (r "^0.1.3") (d #t) (k 0)))) (h "1m06vsf1a1fxs9nnb13z4kvz0inq0rl3fi4755azrk0xc0by8b2y")))

(define-public crate-serror_axum-0.1.1 (c (n "serror_axum") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "serror") (r "^0.1.3") (d #t) (k 0)))) (h "174g2aqwkxzk34x0pk0294yyjbn4wlnfplwv51a17agjmr0h5k68")))

(define-public crate-serror_axum-0.1.2 (c (n "serror_axum") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "serror") (r "^0.1.4") (d #t) (k 0)))) (h "19bfbdly5p7x8jg5f42ahgzypxmvr5z1lwz2x1g0aqy15yy39azp")))

(define-public crate-serror_axum-0.1.5 (c (n "serror_axum") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "serror") (r "^0.1.5") (d #t) (k 0)))) (h "1a8mygiwipxvw24krbgkhmhybpmssfd6vv21mgwh2fmdra09y78q")))

