(define-module (crates-io se rr serry) #:use-module (crates-io))

(define-public crate-serry-0.1.0 (c (n "serry") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (o #t) (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serry-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "0xd58pxda8n7w0zarl9nzdw6k17x51vxwj549xmzb6a46c12r45v") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:serry-derive") ("checksum-sha2" "checksum" "dep:sha2") ("checksum" "dep:digest"))))))

