(define-module (crates-io se rr serry-derive) #:use-module (crates-io))

(define-public crate-serry-derive-0.1.0 (c (n "serry-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1s3m9rjy1kmapgayipwpzrqkyq2c0n7ff2a2nawggx01vhknr5ld")))

