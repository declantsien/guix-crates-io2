(define-module (crates-io se rg sergen_x_openapi) #:use-module (crates-io))

(define-public crate-sergen_x_openapi-1.0.0 (c (n "sergen_x_openapi") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gvnw5ghi2ragw0zafwkdbnx31zga31z3gx50r5ric9ypfrv2r3l")))

