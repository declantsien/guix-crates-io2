(define-module (crates-io se is seismo) #:use-module (crates-io))

(define-public crate-seismo-0.1.0 (c (n "seismo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hotwatch") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1bpcxyvpz0f0992vcv7hlqq7afbd30x0dv9hfdjhssw3plwb1jnf")))

