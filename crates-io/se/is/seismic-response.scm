(define-module (crates-io se is seismic-response) #:use-module (crates-io))

(define-public crate-seismic-response-0.1.0 (c (n "seismic-response") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tsify") (r "^0.4.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "0fl1x1d8z8lyprad08s1lkp0xqx1q4iaj2hd42g2ij0qvsjlswkk")))

(define-public crate-seismic-response-0.2.0 (c (n "seismic-response") (v "0.2.0") (d (list (d (n "close-to") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tsify") (r "^0.4.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "10lb05500002synhx8518m96c6wqp4afzwxnfdzbjhr6r1x5yfz7")))

(define-public crate-seismic-response-0.2.1 (c (n "seismic-response") (v "0.2.1") (d (list (d (n "close-to") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tsify") (r "^0.4.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "08n512dj8vwpn148wkz05fw9hsmz8yh3j1bqxa22018mwnjs3fdf")))

