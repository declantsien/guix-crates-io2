(define-module (crates-io se hx sehx) #:use-module (crates-io))

(define-public crate-sehx-0.1.0 (c (n "sehx") (v "0.1.0") (h "0sdza67vy90yk8ya2i8371vhvcyx8r9nzrfikyd7aib3g17k6inl")))

(define-public crate-sehx-0.1.1 (c (n "sehx") (v "0.1.1") (h "1w44jnpmskggkg7cwaj4rrqjflirk62m54y0rpf4jz1qrp4d7i3h")))

(define-public crate-sehx-0.1.2 (c (n "sehx") (v "0.1.2") (h "1l5lkvvb7y4cm5sqrmigc1ij5lg25x4r4i5gbnzvq3in2qknq06x")))

