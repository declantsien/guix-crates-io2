(define-module (crates-io se q2 seq2xypic) #:use-module (crates-io))

(define-public crate-seq2xypic-0.1.0 (c (n "seq2xypic") (v "0.1.0") (h "1a10z8n33w7i17w6k4hfrd5jkj2aiirjvbv2d3761id05sfw4lb6")))

(define-public crate-seq2xypic-0.1.1 (c (n "seq2xypic") (v "0.1.1") (h "11gbnwqw7wzk2gb66ga6lyzs6y2rdx0j15xxjh6njk3d5v4bznia")))

