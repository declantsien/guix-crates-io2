(define-module (crates-io se qa seqalign) #:use-module (crates-io))

(define-public crate-seqalign-0.1.0 (c (n "seqalign") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "1k1i7v2q15spvrg3kq7jyx5fjz8zw761kb8aclvwz07vgz7h90mm")))

(define-public crate-seqalign-0.1.1 (c (n "seqalign") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0k6rwl1y852h4k6r1jwlhnw391y7bmhjz3h0ncm3m3634xnfdjm7")))

(define-public crate-seqalign-0.2.1 (c (n "seqalign") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0qlswjxvaxkx3058w1061m404q833p752inqgrwkdd2m47jr98xw")))

(define-public crate-seqalign-0.2.2 (c (n "seqalign") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0pdv8r9a6hf5kya2jgax8hc493kpmyg7q06207lzh1h03xscbmwr")))

(define-public crate-seqalign-0.2.3 (c (n "seqalign") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xv3kcyhp9x1bkvbdy28lh7k8fd3p4pm84cya0xqbacp47ycqja7")))

