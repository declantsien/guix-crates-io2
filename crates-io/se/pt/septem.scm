(define-module (crates-io se pt septem) #:use-module (crates-io))

(define-public crate-septem-0.9.0 (c (n "septem") (v "0.9.0") (h "0i787903nqc2rrvk9vh5mnzp61ry6vd53vvp45kk1kqcnlqdia6w")))

(define-public crate-septem-0.9.1 (c (n "septem") (v "0.9.1") (h "00vqksfcraw86jkg1h77q12iqw9jbnrcn505hmc2myxrc1dnhn7w")))

(define-public crate-septem-1.0.0 (c (n "septem") (v "1.0.0") (h "1by6mbi3rv1rm85iaqvdymhbfghzdxnwwbzm32sai6858laynr5d")))

(define-public crate-septem-1.0.1 (c (n "septem") (v "1.0.1") (h "0x2bfh4xkrshcrga12ajl78sjic5mzqx7xzb24bcqisprfk2ixc2")))

(define-public crate-septem-1.1.0 (c (n "septem") (v "1.1.0") (h "1g5ff7rs41d4h396mqbzm4kizy6c84acnp2lwj36d5p4mqgwzgf3")))

