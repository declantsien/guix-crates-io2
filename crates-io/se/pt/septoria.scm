(define-module (crates-io se pt septoria) #:use-module (crates-io))

(define-public crate-septoria-0.1.0 (c (n "septoria") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1150hk7fxlb8pf58s40abqmf3qvkjx1mqfs7zhmd3a8b44y6li9m") (f (quote (("live"))))))

