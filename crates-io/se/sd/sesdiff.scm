(define-module (crates-io se sd sesdiff) #:use-module (crates-io))

(define-public crate-sesdiff-0.1.0 (c (n "sesdiff") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "0h5n2v0aii3myk9lrm83y7hqhs64wfi61v2vqps0i6npgz70nvg8")))

(define-public crate-sesdiff-0.1.1 (c (n "sesdiff") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "096j0rdsj1qsc0m8k903r0vbf1nzghi45gqpqswq7v5nck179sq7")))

(define-public crate-sesdiff-0.1.2 (c (n "sesdiff") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "17qcvibx260hlng7qrsn27b18w3lln5i61d2xlr5ni4zzkykqf6c")))

(define-public crate-sesdiff-0.1.4 (c (n "sesdiff") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "0zq35zzdpz4lyifb53py4jsgzf8i1jd0l9cmdgby764m55dsdqb9")))

(define-public crate-sesdiff-0.1.5 (c (n "sesdiff") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "1qggmd25hh16sc91rkf6dn50hcsvpmgdbiml4bi37983i9q3cq47")))

(define-public crate-sesdiff-0.2.0 (c (n "sesdiff") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "0wvfdlvjf6sb09kf1r78sk8zpfbfl0yn07yn80x5p9raqfclkkqz")))

(define-public crate-sesdiff-0.3.0 (c (n "sesdiff") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.2") (d #t) (k 0)))) (h "10dmfjcrsxfnrw7243syabchrjf7asaikb3xfvspqq6f53rjh9w7")))

