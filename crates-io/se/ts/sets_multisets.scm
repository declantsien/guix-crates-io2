(define-module (crates-io se ts sets_multisets) #:use-module (crates-io))

(define-public crate-sets_multisets-0.0.1 (c (n "sets_multisets") (v "0.0.1") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0jbmainyxbnfb6zjxasj9ycva6bsqfgzwbyjhy4m6aakbn71mb8y")))

(define-public crate-sets_multisets-0.1.0 (c (n "sets_multisets") (v "0.1.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ks23fhqhdxdsp0sabqn0vpk34rhf5wy6bs0svgmib45lnivxgi5")))

(define-public crate-sets_multisets-0.2.0 (c (n "sets_multisets") (v "0.2.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1m4fp25n6ipxbb6sv53y6wsi0md2827bc6xi3x0fjxfb1svh6aa6")))

(define-public crate-sets_multisets-0.2.1 (c (n "sets_multisets") (v "0.2.1") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0qxqr2rv56vs79rpml1nj44g4f2yj4yw2m3hx2ywl77mvyz41anm")))

(define-public crate-sets_multisets-0.3.1 (c (n "sets_multisets") (v "0.3.1") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "05ddn8p1l3j7jh275qgrnybb4s0vvx96j5n6yz051gvrcd8ibbmq")))

(define-public crate-sets_multisets-0.3.2 (c (n "sets_multisets") (v "0.3.2") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15sawwf05sk27425qly6dkdm7bf3w5w69nl92njys5zbrw7v458f")))

(define-public crate-sets_multisets-0.4.0 (c (n "sets_multisets") (v "0.4.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0p44a5zx8kaapi20zwq247c33j8p5q4h1ym56hyz0cynlfl4rl7d")))

(define-public crate-sets_multisets-0.4.1 (c (n "sets_multisets") (v "0.4.1") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0xd85ybyblab4p5ac45fzqf26jyqxq74xvps8fjmfcp86gdl7jns")))

(define-public crate-sets_multisets-0.4.2 (c (n "sets_multisets") (v "0.4.2") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0zfqj3k69fq1rwia5xn22ihqny8i32gba7cmnwlzh6mr9wxa5f1a")))

(define-public crate-sets_multisets-0.5.0 (c (n "sets_multisets") (v "0.5.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (d #t) (k 0)))) (h "16srag2xk9hbpcgfvk1i24lq7ry86gr3zjkqblgw7y01gg83g4wk") (y #t)))

(define-public crate-sets_multisets-0.6.0 (c (n "sets_multisets") (v "0.6.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (d #t) (k 0)))) (h "0xhjqyffl1hmcqjwnv6g3hhdhl827ha4jqkx9w3jcjwvyw4d47dn")))

(define-public crate-sets_multisets-0.7.0 (c (n "sets_multisets") (v "0.7.0") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06krasq7aylnd3zn26pmqiqyh01r5nmf7afhvnyvzwmgs6kcknbw") (f (quote (("default" "xxh3")))) (s 2) (e (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3"))))))

(define-public crate-sets_multisets-0.7.1 (c (n "sets_multisets") (v "0.7.1") (d (list (d (n "blake3") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ywbb31mjc5wg5yz035hj7cmwg7z20c6wl68z9pr2dccx969r2i4") (f (quote (("default" "xxh3")))) (s 2) (e (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

(define-public crate-sets_multisets-0.7.2 (c (n "sets_multisets") (v "0.7.2") (d (list (d (n "blake3") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1f43z9g8rkginyxsd2gqfhwnph6hys9g88csix82siq174qjj4in") (f (quote (("default" "xxh3")))) (s 2) (e (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

(define-public crate-sets_multisets-0.8.0 (c (n "sets_multisets") (v "0.8.0") (d (list (d (n "blake3") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1yhnhzkpmw7dcdrrp0xdzzcn64vy2a2szgnjq9zmm1nrlskvq3j9") (f (quote (("default" "xxh3")))) (s 2) (e (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

