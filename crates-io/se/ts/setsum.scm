(define-module (crates-io se ts setsum) #:use-module (crates-io))

(define-public crate-setsum-0.1.0 (c (n "setsum") (v "0.1.0") (d (list (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "18nhjc41c7mp4v37wnlffxnnjzm3493lxzyszx8n8crf75f81b4r")))

(define-public crate-setsum-0.2.0 (c (n "setsum") (v "0.2.0") (d (list (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1i39ynblz4jbq91yyii0dxdg1hj10w2hzbdnd3xn626531bpxwhz")))

(define-public crate-setsum-0.3.0 (c (n "setsum") (v "0.3.0") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "17fdqylw8wm96zd1yc1kmwfxcyk9q96dvrmkjb7zx1jws64g33kv")))

(define-public crate-setsum-0.4.0 (c (n "setsum") (v "0.4.0") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0ml6qllsa917zbivzc1abxa3xmg0dgs3iffzk7bnvzcic9j3978c")))

(define-public crate-setsum-0.5.0 (c (n "setsum") (v "0.5.0") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0b8snrz761ar6i53prasfwplcwrmxab763cq6278ppfjy7lvsn14") (f (quote (("default" "binaries") ("binaries"))))))

