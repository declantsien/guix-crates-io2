(define-module (crates-io se ts sets) #:use-module (crates-io))

(define-public crate-sets-0.1.0 (c (n "sets") (v "0.1.0") (d (list (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "1z053nd883pi59mn25n1c9bp8ld83wrajlcqs4blfs9yw9rv6mkb")))

(define-public crate-sets-0.1.1 (c (n "sets") (v "0.1.1") (d (list (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "1n27phxfyv7qa2k3zfh5n55mw6s9m9m3975rmn04gg7cq0qb4ny6")))

(define-public crate-sets-0.1.2 (c (n "sets") (v "0.1.2") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1khhgk9bjx7m2qkq2mp1kzai414zwz6rgb2896ka06pzicx5gcbk")))

(define-public crate-sets-0.1.3 (c (n "sets") (v "0.1.3") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "07gjsq1v9z1a7404srx6iyxd0wbilhv6hqpv9nmybfnxm0f8cg8l")))

(define-public crate-sets-0.1.4 (c (n "sets") (v "0.1.4") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0hn3b36jp9b23kjvnb2czhybfnllxic6d1plasb7i2yfaq40hsws")))

(define-public crate-sets-0.1.5 (c (n "sets") (v "0.1.5") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1linxi9zmpkd822mrm7rqchy2lygx6f7hg5wpg7lvjrf3w850rbc")))

(define-public crate-sets-0.1.6 (c (n "sets") (v "0.1.6") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0kqhz3rxxgjal5lck3wpxip87xlq69qzwz36mklyixmrlbqrijyb")))

(define-public crate-sets-0.1.7 (c (n "sets") (v "0.1.7") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "064hm5cpc10h5pwhikl09ybhwgvd9ili50jk33hayqxayfsrn7ga")))

(define-public crate-sets-0.1.8 (c (n "sets") (v "0.1.8") (d (list (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1m7jcxnhrypzd575yzycvirmp43hzhlnlxr5b2ai3k2wx9mn6ali")))

(define-public crate-sets-1.0.0 (c (n "sets") (v "1.0.0") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1jrmd75kcf3d7lgf2kqcv8h2psg4gw79dz5him1xybw0sgnbqncx")))

(define-public crate-sets-1.0.1 (c (n "sets") (v "1.0.1") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "10dgxpzgy00xqga0whv7ybb8pbj5raviwdrk0mrqii29s70ns350")))

(define-public crate-sets-1.0.2 (c (n "sets") (v "1.0.2") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0916frxs95whp4nqza55l59dim9xalxzicplspmikimds92z175y")))

(define-public crate-sets-1.0.3 (c (n "sets") (v "1.0.3") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0zr1hk7indg76zkyidkc1kc7yqdyib0r0q79ya9mbmpiq220d6fm")))

(define-public crate-sets-1.0.4 (c (n "sets") (v "1.0.4") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1g1pqh7bynq5k23dqsabm3zii7zldjx436bqi9lsq6fld6bzyjpv")))

(define-public crate-sets-1.0.5 (c (n "sets") (v "1.0.5") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0h80n4f60gh1pmngkyy7y25h4b8halyry6h55ps31z650s00dl06")))

(define-public crate-sets-1.0.6 (c (n "sets") (v "1.0.6") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "00kz2cnipl29j3dc79rhgd9x4kf09154kladwrby4hchq4s49ai5")))

(define-public crate-sets-1.1.0 (c (n "sets") (v "1.1.0") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0xr5n49kny8z5mf09mw5nkpxmcfs2d3vbckqxnryjyw76md4fbz0")))

(define-public crate-sets-1.1.1 (c (n "sets") (v "1.1.1") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1d5wiijmpkixy60n44dshpabhzy6whj8m61bp86w5gbsfmgw72sh")))

(define-public crate-sets-1.1.2 (c (n "sets") (v "1.1.2") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "07j40r60g3wc558nbw3sw9phsn81xg911nhxzdhlafq2qy3y0r9k")))

(define-public crate-sets-1.1.3 (c (n "sets") (v "1.1.3") (d (list (d (n "indxvec") (r "^1.4.2") (d #t) (k 0)))) (h "02mi9h7kmf94a5q4395vh541fkz8wxqzzh9v9c7j89qdkqqwy117")))

(define-public crate-sets-1.1.4 (c (n "sets") (v "1.1.4") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)))) (h "114475yjgizl2xxr1l82934vvwsn2m31ma550ld1li5lq0q28n7j")))

(define-public crate-sets-1.2.0 (c (n "sets") (v "1.2.0") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)))) (h "1mab4g378cddyfhdzka9n1qvpa4g3lcblk25d569as3hm230ggjd")))

(define-public crate-sets-1.2.1 (c (n "sets") (v "1.2.1") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)))) (h "0p4cdkc47hp09nln139pw396z5lhh0dkqi6mxz8q1igk495zjv7s")))

