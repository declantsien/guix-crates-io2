(define-module (crates-io se lf self-meter) #:use-module (crates-io))

(define-public crate-self-meter-0.1.0 (c (n "self-meter") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1cr8msj8skb8dals647ac2w95qyf34zyp8kqxw2k6gsh3kqfbd4r")))

(define-public crate-self-meter-0.2.0 (c (n "self-meter") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1vv20h7hlmmky3ap6q05lm48kw6khdwj2q74jd685z6710fad905") (y #t)))

(define-public crate-self-meter-0.3.0 (c (n "self-meter") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0vjlw18q9294xalnkf6ivpwkf8gn4vzyn86rnjcy0jvz0y9f78ky")))

(define-public crate-self-meter-0.4.0 (c (n "self-meter") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "14iyfw6h63bxi63d39sd7i9r1hdz27rnb4n1bzya9rcfwgsv60rk") (y #t)))

(define-public crate-self-meter-0.4.1 (c (n "self-meter") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 2)))) (h "09mr5bgd4yi91ap3if5467mwx59q79myl6bn2qzl2db8ynwa0rac")))

(define-public crate-self-meter-0.4.2 (c (n "self-meter") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 2)))) (h "0dw2d5ffpz1iddq6bpjrh6b9lx1d9lwyss1xcxblxmw1bwm3ald8")))

(define-public crate-self-meter-0.4.3 (c (n "self-meter") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 2)))) (h "13mcwbm4aii5ndp7rvd4j78r84f8ww7hz9hva6wqij3i7chns65g")))

(define-public crate-self-meter-0.4.4 (c (n "self-meter") (v "0.4.4") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 2)))) (h "0gycch159x3am65sbxwnhx2rrkybcdzb02n4c8w6vjkmybn40fji")))

(define-public crate-self-meter-0.5.0 (c (n "self-meter") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 2)))) (h "03ac972iczhgz212lb3ha3vvrv9yfjv1hl7fljshdy3p298dxiw0")))

(define-public crate-self-meter-0.6.0 (c (n "self-meter") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1j4bpaklk9byrjzci5cakn5fbgpxnj30h1ny9jfys6xj5bgbapbz")))

