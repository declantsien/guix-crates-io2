(define-module (crates-io se lf self_cell) #:use-module (crates-io))

(define-public crate-self_cell-0.7.0 (c (n "self_cell") (v "0.7.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1l1daywk0pm9234bhmfqj7inpkpjrxlzfcy5wljn5ll6qb1g78jw") (y #t)))

(define-public crate-self_cell-0.7.1 (c (n "self_cell") (v "0.7.1") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1aqkx1f0bmb4wqn8aw674pnhhjllxzc5fsnsw382apbdyjjn5v11") (y #t)))

(define-public crate-self_cell-0.8.0 (c (n "self_cell") (v "0.8.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0lskhjzc7gdlwsshyvy4x5aihzv549qqidks659s9yi1wpl29jfi") (y #t)))

(define-public crate-self_cell-0.9.0 (c (n "self_cell") (v "0.9.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0grgn8x16asjfrwisj5fxq9k4swsy57injl7bgpnmv4bd26g8dbb") (y #t)))

(define-public crate-self_cell-0.9.1 (c (n "self_cell") (v "0.9.1") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0sghrhgmigjqna5a6hp4yrls2anxawdx3b1q1lvxzna3ig8nhjc8") (y #t)))

(define-public crate-self_cell-0.9.2 (c (n "self_cell") (v "0.9.2") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "17b5l74zmprrfa5wj4nbq3yd8hrb19qc0dvwnj3l1hgxf2prsqpy") (y #t)))

(define-public crate-self_cell-0.10.0 (c (n "self_cell") (v "0.10.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "19n3ravhfwnyzm2vmrabb3rbdgbpfjlsdianvn496zgz8pcps9jm") (y #t)))

(define-public crate-self_cell-0.10.1 (c (n "self_cell") (v "0.10.1") (d (list (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "1q8s868p30d8qcsz5az4w65k7dnrvm29cin628873waxcbk57ivs") (f (quote (("old_rust" "rustversion")))) (y #t)))

(define-public crate-self_cell-0.10.2 (c (n "self_cell") (v "0.10.2") (d (list (d (n "once_cell") (r ">=1") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "1by8h3axgpbiph5nbq80z6a41hd4cqlqc66hgnngs57y42j6by8y") (f (quote (("old_rust" "rustversion")))) (y #t)))

(define-public crate-self_cell-1.0.0 (c (n "self_cell") (v "1.0.0") (d (list (d (n "once_cell") (r "=1.1.0") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "12z86cnwncg0gzggm8a5k8a94bsh27sydzq91433d3bk77i2cfaa") (f (quote (("old_rust" "rustversion")))) (y #t)))

(define-public crate-self_cell-1.0.1 (c (n "self_cell") (v "1.0.1") (d (list (d (n "once_cell") (r "=1.1.0") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "1djgfccbfhj2zv7xmqc2nxwn41g1swyrxg1d488pirj3am8rwc2c") (f (quote (("old_rust" "rustversion")))) (y #t)))

(define-public crate-self_cell-1.0.2 (c (n "self_cell") (v "1.0.2") (d (list (d (n "once_cell") (r "=1.1.0") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "1rmdglwnd77wcw2gv76finpgzjhkynx422d0jpahrf2fsqn37273") (f (quote (("old_rust" "rustversion"))))))

(define-public crate-self_cell-0.10.3 (c (n "self_cell") (v "0.10.3") (d (list (d (n "new_self_cell") (r "^1") (d #t) (k 0) (p "self_cell")))) (h "0pci3zh23b7dg6jmlxbn8k4plb7hcg5jprd1qiz0rp04p1ilskp1") (f (quote (("old_rust" "new_self_cell/old_rust"))))))

(define-public crate-self_cell-1.0.3 (c (n "self_cell") (v "1.0.3") (d (list (d (n "once_cell") (r "=1.1.0") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "1fk5dl2751y26vnpzpw8dw364l1vv08jmjj1wv2a5f9v5likggsq") (f (quote (("old_rust" "rustversion"))))))

(define-public crate-self_cell-1.0.4 (c (n "self_cell") (v "1.0.4") (d (list (d (n "once_cell") (r "=1.1.0") (d #t) (k 2)) (d (n "rustversion") (r ">=1") (o #t) (d #t) (k 0)))) (h "0jki9brixzzy032d799xspz1gikc5n2w81w8q8yyn8w6jxpsjsfk") (f (quote (("old_rust" "rustversion"))))))

