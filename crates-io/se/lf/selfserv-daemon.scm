(define-module (crates-io se lf selfserv-daemon) #:use-module (crates-io))

(define-public crate-selfserv-daemon-0.2.0 (c (n "selfserv-daemon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1kwr8h4xp27fl0gjxdk98p8lc23c0lh5i1q3pmwf2724hpbr2f2p")))

