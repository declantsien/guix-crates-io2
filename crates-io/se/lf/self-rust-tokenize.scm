(define-module (crates-io se lf self-rust-tokenize) #:use-module (crates-io))

(define-public crate-self-rust-tokenize-0.1.0 (c (n "self-rust-tokenize") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0f602icvxr2vpgvs619ns70yy8vgqcxakb4144pggfn2a47snlks")))

(define-public crate-self-rust-tokenize-0.2.0 (c (n "self-rust-tokenize") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1976zi2dcbxy4byypg6yspndq56d1czk546q79mb4w8dxxxwpfhv") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.51")))

(define-public crate-self-rust-tokenize-0.3.0 (c (n "self-rust-tokenize") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1klv4lfkx1pn3p5svqd6fn1li461hw7rlayz6iv4qxn7f9jfgbqj") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.51")))

(define-public crate-self-rust-tokenize-0.3.1 (c (n "self-rust-tokenize") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1pbasvr59f8mvafrivgjlp5dckhai77gl6g4pgka26iz2kc0fw5b") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.51")))

(define-public crate-self-rust-tokenize-0.3.2 (c (n "self-rust-tokenize") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0xgrx6l3kdibvwda03glbqgs881qxlaxic37iw5mvsc8iwlc1yn1") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.51")))

(define-public crate-self-rust-tokenize-0.3.3 (c (n "self-rust-tokenize") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0y20dhyrmij1sq7p3cbj0nj63dcbmgax0mn5kk33s03bnzzk0p13") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.51")))

(define-public crate-self-rust-tokenize-0.3.4 (c (n "self-rust-tokenize") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize-derive") (r "^0.3.4") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1rninpygqqikkwy0yg1pc0h9hkkqzczg5agk35f3wjxy6c8ikhs5") (f (quote (("references")))) (s 2) (e (quote (("smallvec" "dep:smallvec")))) (r "1.60")))

