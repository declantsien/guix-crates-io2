(define-module (crates-io se lf selfie) #:use-module (crates-io))

(define-public crate-selfie-0.0.1 (c (n "selfie") (v "0.0.1") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (t "cfg(not(miri))") (k 2)))) (h "1qzzcv34178cqajrhcv1cwkypj9xp1vcm4mpk4b6r5ws3yhaqf5n") (f (quote (("std" "stable_deref_trait/std") ("default" "std") ("alloc" "stable_deref_trait/alloc")))) (y #t) (r "1.56")))

(define-public crate-selfie-0.0.2 (c (n "selfie") (v "0.0.2") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "trybuild") (r "=1.0.63") (d #t) (t "cfg(not(miri))") (k 2)))) (h "1h6jak80axg9ba7sqw45lyg6bdl7ymksi6igik8r8240f4pv9451") (f (quote (("std" "stable_deref_trait/std") ("default" "std") ("alloc" "stable_deref_trait/alloc")))) (y #t) (r "1.56")))

(define-public crate-selfie-0.0.3 (c (n "selfie") (v "0.0.3") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "trybuild") (r "=1.0.63") (d #t) (t "cfg(not(miri))") (k 2)))) (h "1f6dd0sihvzixkjp9whs1im06nas0bhq6jcb55rakif65qnhdfg7") (f (quote (("std" "stable_deref_trait/std") ("default" "std") ("alloc" "stable_deref_trait/alloc")))) (r "1.56")))

