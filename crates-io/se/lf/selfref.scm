(define-module (crates-io se lf selfref) #:use-module (crates-io))

(define-public crate-selfref-0.1.0 (c (n "selfref") (v "0.1.0") (h "186jm8xfkjnvifyprdzmy1jakx8xvdaxp74r7r0rimqs4hh5a8r5")))

(define-public crate-selfref-0.1.1 (c (n "selfref") (v "0.1.1") (h "0zxs3ls1a1qy1i923a8gl5dlbg4gs3hplb5ydv1vfh3irpx7mfqf")))

(define-public crate-selfref-0.1.2 (c (n "selfref") (v "0.1.2") (h "1nd725vfgphyp8613mza1xxskwxs52bp0d2d09380qxwmjw3i3zv")))

(define-public crate-selfref-0.1.3 (c (n "selfref") (v "0.1.3") (h "0fd9xlhr7xk7dssgjpjcxjz9cxcdnmxiapcg9vq9m1612y99p4d8")))

(define-public crate-selfref-0.1.4 (c (n "selfref") (v "0.1.4") (h "179g11wz966jwxiar0c6why5hwpv88qrmjr708mqk5hhd9bpja5n")))

(define-public crate-selfref-0.2.0 (c (n "selfref") (v "0.2.0") (h "0aqzg5f5najnv1gv3ggp3zlxg3vkg2raxwff8a6kxp4saypfbh8k")))

(define-public crate-selfref-0.2.1 (c (n "selfref") (v "0.2.1") (h "1q42dhd0ivap0xi5xmajbj829k67m0824822cq8pm11mc13hyvnp")))

(define-public crate-selfref-0.2.2 (c (n "selfref") (v "0.2.2") (h "1y21iz3c9jwp45cnpswq84gdamcirzhhwcqdky8v09p0vg345wm8")))

(define-public crate-selfref-0.2.3 (c (n "selfref") (v "0.2.3") (h "0c6a8195x8jca3f7217rjmlpgqrxw0c0pdpa6122hnflb0fl2phb") (f (quote (("nightly") ("alloc"))))))

(define-public crate-selfref-0.2.4 (c (n "selfref") (v "0.2.4") (h "0l77rwbddiw265b1bqalwiffgkfhb2ani4n5b8dbb6zz0kl2lq7y") (f (quote (("nightly") ("alloc"))))))

(define-public crate-selfref-0.3.0 (c (n "selfref") (v "0.3.0") (d (list (d (n "trybuild") (r "^1.0.72") (d #t) (k 2)))) (h "1cw5b5kmfprdng9j0ipalq9665k9pavpy01vnqfywj85n35p65yz") (f (quote (("nightly") ("alloc"))))))

(define-public crate-selfref-0.4.0 (c (n "selfref") (v "0.4.0") (d (list (d (n "trybuild") (r "^1.0.72") (d #t) (k 2)))) (h "0j63icjwrl4s8c0h1s4hp9ikphyqy9s01mjwhlbabkkx8z7l2wzk") (f (quote (("nightly") ("alloc"))))))

(define-public crate-selfref-0.4.1 (c (n "selfref") (v "0.4.1") (d (list (d (n "generativity") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "qcell") (r "^0.5.2") (f (quote ("generativity"))) (o #t) (k 0)) (d (n "trybuild") (r "^1.0.72") (d #t) (k 2)))) (h "160rqgprwflnzhkwnzkygvjggw8r92d73wmnqj28pjg9kvndrd06") (f (quote (("nightly") ("alloc")))) (s 2) (e (quote (("qcell" "dep:qcell" "dep:generativity"))))))

(define-public crate-selfref-0.4.2 (c (n "selfref") (v "0.4.2") (d (list (d (n "generativity") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "qcell") (r "^0.5.2") (f (quote ("generativity"))) (o #t) (k 0)) (d (n "trybuild") (r "^1.0.72") (d #t) (k 2)))) (h "0hmhnpz4gq8lzmvkpl87ki9ba5d22dvwhz5yyb15zlsdbr9c8qq2") (f (quote (("nightly") ("alloc")))) (s 2) (e (quote (("qcell" "dep:qcell" "dep:generativity"))))))

(define-public crate-selfref-0.4.3 (c (n "selfref") (v "0.4.3") (d (list (d (n "generativity") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "qcell") (r "^0.5.2") (f (quote ("generativity"))) (o #t) (k 0)) (d (n "trybuild") (r "^1.0.72") (d #t) (k 2)))) (h "06s67rff29r5a4200lh2vl8kb3kryj982ifq271yxkffqy8jylrm") (f (quote (("nightly") ("alloc")))) (s 2) (e (quote (("qcell" "dep:qcell" "dep:generativity"))))))

