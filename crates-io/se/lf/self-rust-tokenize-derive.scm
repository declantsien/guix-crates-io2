(define-module (crates-io se lf self-rust-tokenize-derive) #:use-module (crates-io))

(define-public crate-self-rust-tokenize-derive-0.1.0 (c (n "self-rust-tokenize-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.2.1") (d #t) (k 0)))) (h "1kiid37xw9a3hljk5a72zymbgnr50m9n0w6gdar4hqg4qxhipa1s")))

(define-public crate-self-rust-tokenize-derive-0.3.0 (c (n "self-rust-tokenize-derive") (v "0.3.0") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.0") (d #t) (k 0)))) (h "09x3kjl6zj2flij86lf01haabkgifbavsgv6xnssgf5fxc2mibgx")))

(define-public crate-self-rust-tokenize-derive-0.3.1 (c (n "self-rust-tokenize-derive") (v "0.3.1") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.1") (d #t) (k 0)))) (h "05mdz8s5wscrxms2xnxxyn4p09y9f0zksz78s1lzngqw85i584ia")))

(define-public crate-self-rust-tokenize-derive-0.3.2 (c (n "self-rust-tokenize-derive") (v "0.3.2") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "126lwiy6pfr6939xxkk5shvn3kvacagni2wvy2rqpfc5iiimx60i")))

(define-public crate-self-rust-tokenize-derive-0.3.3 (c (n "self-rust-tokenize-derive") (v "0.3.3") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.3") (d #t) (k 0)))) (h "0svz85rfplfp7n5k2zmak493qlpq12c1m0d7hpqnbh7fmmb5a1wy")))

(define-public crate-self-rust-tokenize-derive-0.4.0 (c (n "self-rust-tokenize-derive") (v "0.4.0") (d (list (d (n "syn-helpers") (r "^0.5") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "1g5br0kj5mpyzqzawp9fwrm38grrmmjf8qqb5hiibi0w8zz7rhf4") (y #t)))

(define-public crate-self-rust-tokenize-derive-0.3.4 (c (n "self-rust-tokenize-derive") (v "0.3.4") (d (list (d (n "syn-helpers") (r "^0.5") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "09kqxl850h63ay9il448pnz465zf4cqqnzhshb02p723c3h9q38g")))

