(define-module (crates-io se lf self-ref) #:use-module (crates-io))

(define-public crate-self-ref-0.1.0 (c (n "self-ref") (v "0.1.0") (h "0ccfyfpi4rm54fghfccyhn19j460qljap34xl14llcgm791d8na1") (y #t)))

(define-public crate-self-ref-0.1.1 (c (n "self-ref") (v "0.1.1") (h "0gyxkxbinlhd9l9jw3b20mr4lvqm4wiaizlvjl9wyg1lm99m3py8") (y #t)))

(define-public crate-self-ref-0.1.2 (c (n "self-ref") (v "0.1.2") (h "1p45nzivf0isd5ywpdyg6svl4xrh665r61lswa0sswbbwqrnp7zd") (y #t)))

