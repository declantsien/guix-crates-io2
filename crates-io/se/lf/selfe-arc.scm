(define-module (crates-io se lf selfe-arc) #:use-module (crates-io))

(define-public crate-selfe-arc-0.1.1 (c (n "selfe-arc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06vws1a5l8q5ysk1c7jbcb2yy8shmm7g37bngprw07kw92n4cgv4") (f (quote (("std" "byteorder/std") ("default" "std"))))))

