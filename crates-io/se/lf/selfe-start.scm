(define-module (crates-io se lf selfe-start) #:use-module (crates-io))

(define-public crate-selfe-start-0.0.28 (c (n "selfe-start") (v "0.0.28") (d (list (d (n "selfe-config") (r "^0.2") (d #t) (k 1)) (d (n "selfe-runtime") (r "^0.1") (d #t) (k 0)) (d (n "selfe-sys") (r "^0.1") (d #t) (k 0)))) (h "1j7pg9wyhfj0bc71wvswdxzbnhv1f00fdim1sh5zgnzmh07bq5dd") (f (quote (("panic_handler" "selfe-runtime/panic_handler") ("default"))))))

(define-public crate-selfe-start-0.1.0 (c (n "selfe-start") (v "0.1.0") (d (list (d (n "selfe-config") (r "^0.2") (d #t) (k 1)) (d (n "selfe-runtime") (r "^0.1") (d #t) (k 0)) (d (n "selfe-sys") (r "^0.1") (d #t) (k 0)))) (h "07k8sys3yq0a6bkfc1b52mykvzmvzq8sc29nnnyx3k08k1v4sx2a") (f (quote (("panic_handler" "selfe-runtime/panic_handler") ("default"))))))

