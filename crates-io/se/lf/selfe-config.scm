(define-module (crates-io se lf selfe-config) #:use-module (crates-io))

(define-public crate-selfe-config-0.2.1 (c (n "selfe-config") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xk4wabmx78j0apxg84l1k2rj2mxa14l8vy8rzy2cws39nhjkf6d") (f (quote (("default") ("bin" "clap"))))))

