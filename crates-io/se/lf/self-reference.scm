(define-module (crates-io se lf self-reference) #:use-module (crates-io))

(define-public crate-self-reference-0.1.0 (c (n "self-reference") (v "0.1.0") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "12zn91j42x573l31hv8sykq5lrv87c6f7d097hxyz7vkzaa5505x")))

(define-public crate-self-reference-0.1.1 (c (n "self-reference") (v "0.1.1") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1xphd4a6n0yrw839cjjlnnjpsm9m5xqjxxhxwgimh73qp68nb8zj")))

(define-public crate-self-reference-0.1.2 (c (n "self-reference") (v "0.1.2") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0y476c2rylg8y2yhj46vr374s33qqak3yi8im0an322yh0ms74ih")))

(define-public crate-self-reference-0.1.3 (c (n "self-reference") (v "0.1.3") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0pv65m39fan8ifagdg24ssqiab5iam4jll01f6wyqa0mhls539aw")))

(define-public crate-self-reference-0.1.4 (c (n "self-reference") (v "0.1.4") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "14vm20skcfw1rjbkrl2v9qzkvh9bx6f54pwyikmi1q0arn4zr2pp")))

(define-public crate-self-reference-0.1.5 (c (n "self-reference") (v "0.1.5") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "1jkw1k2ax0gkyjzfic5yql61980rbc425lk2jliyj3r68qbjgyp7")))

(define-public crate-self-reference-0.1.6 (c (n "self-reference") (v "0.1.6") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1fxxly4fijdbry4xjxzlbl8pl52dyl03ifvf8n4b4xs0vlgjhgx4") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.1.7 (c (n "self-reference") (v "0.1.7") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0lfhp6fndsy5w1lq6cjgj4aaxn3b8f3dizzmkbr6p67sgld0l338") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.1.8 (c (n "self-reference") (v "0.1.8") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1mam61fvdnx1d3115590z1q0rpg5x2a1gjlhxwhf1kgjzcnn48rn") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.1.9 (c (n "self-reference") (v "0.1.9") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "14430rqdfarghj8iqvy8cy4jnamqp5vkx79ynkri619g5hni5mi9") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.1.10 (c (n "self-reference") (v "0.1.10") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0fn85z9cb1pv3z1k38ylyaagjz3l7g0gh3xigmgm07hz95gv8n9v") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.2.0 (c (n "self-reference") (v "0.2.0") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "17v53wq1jm3qyms3iwggs2dkk2rlzfpvsl7lmi71glnh1din8fr9") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.2.1 (c (n "self-reference") (v "0.2.1") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "15pq07rx27xaaw282z0cy8l3c8xb3dapiw50c8gk5f91mlg2fr7l") (f (quote (("std") ("default" "std"))))))

(define-public crate-self-reference-0.2.2 (c (n "self-reference") (v "0.2.2") (d (list (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "11fxswz9kwzjay34lwh3xf12nhz0ncdrkcvi1jc0ax73z51vkrr5") (f (quote (("std") ("default" "std"))))))

