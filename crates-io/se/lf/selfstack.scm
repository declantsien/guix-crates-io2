(define-module (crates-io se lf selfstack) #:use-module (crates-io))

(define-public crate-selfstack-0.3.0 (c (n "selfstack") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wa97f83rbnc9jcrigjzxrmxi7wy135l7yy3c7alivipj6y3p1yq")))

