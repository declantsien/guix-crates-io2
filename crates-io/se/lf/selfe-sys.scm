(define-module (crates-io se lf selfe-sys) #:use-module (crates-io))

(define-public crate-selfe-sys-0.1.1 (c (n "selfe-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "itertools") (r "^0.8.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 1)) (d (n "proptest") (r "^0.9.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 1)) (d (n "selfe-config") (r "^0.2") (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1fgj30fs84sh73s7bx6iijfanrdyzj9z6cdczhpn3saqv67fiyvx")))

