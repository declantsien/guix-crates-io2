(define-module (crates-io se lf selfe-runtime) #:use-module (crates-io))

(define-public crate-selfe-runtime-0.1.1 (c (n "selfe-runtime") (v "0.1.1") (d (list (d (n "selfe-config") (r "^0.2") (d #t) (k 1)) (d (n "selfe-sys") (r "^0.1") (d #t) (k 0)))) (h "0xyw1z6ycm3ni7fny2hnzl8bpjidzjpiv35kdd9ry00zmgn7505p") (f (quote (("panic_handler") ("default"))))))

