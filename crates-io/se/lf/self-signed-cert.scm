(define-module (crates-io se lf self-signed-cert) #:use-module (crates-io))

(define-public crate-self-signed-cert-1.0.1 (c (n "self-signed-cert") (v "1.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1lahxh4g00mv6p8w17i2kqskwfc2naickfic7ji32rsc9k40hw0i")))

(define-public crate-self-signed-cert-1.0.2 (c (n "self-signed-cert") (v "1.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("time" "deflate"))) (k 0)))) (h "1zdah2rh4x2dv145aanhdc91iw37pkb4fkpzhf32g1v8w19pcvj0")))

(define-public crate-self-signed-cert-1.0.3 (c (n "self-signed-cert") (v "1.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("time" "deflate"))) (k 0)))) (h "08zkmcqqhggki9bm0vvr2ijndyxr3dxvilgcx2zwlbdvsg5s5cwz")))

