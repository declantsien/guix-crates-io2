(define-module (crates-io se lf self-compare) #:use-module (crates-io))

(define-public crate-self-compare-0.1.2 (c (n "self-compare") (v "0.1.2") (h "1c111yc49g536mr9lia8644rih9bzymnakva5skc35nf1dlyfgx7")))

(define-public crate-self-compare-0.2.0 (c (n "self-compare") (v "0.2.0") (h "1magsi4hgs6z5jsmwgnz8gai10pfg5fm93m5b25kg9xws0qrq8m4")))

