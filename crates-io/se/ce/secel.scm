(define-module (crates-io se ce secel) #:use-module (crates-io))

(define-public crate-secel-0.0.1 (c (n "secel") (v "0.0.1") (h "0vkln2084lqwadipsh8iq9yxyz6wfzh974qfcpvh18m2vdy46f8f")))

(define-public crate-secel-0.0.2 (c (n "secel") (v "0.0.2") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "089i20yll9a0pig8vnwckfgn600rxdsv2ivnx5cp35r1nb5d31fy")))

(define-public crate-secel-0.0.3 (c (n "secel") (v "0.0.3") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "0nnqbinl7sc2kyslsbis154nkkw46i1w7nwv6g2isb7216iz3csj")))

(define-public crate-secel-0.0.4 (c (n "secel") (v "0.0.4") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "0za7zb6rp6v16il1afhrvqj8n773bmr2f12s7rwcy17d7gigqvf5")))

(define-public crate-secel-0.0.5 (c (n "secel") (v "0.0.5") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)))) (h "1pfwwplgpr3hj0zq5riz2b9rm8kpla9vxgbifj6fkg6as2yy8i5q")))

(define-public crate-secel-0.0.6 (c (n "secel") (v "0.0.6") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0ydr2zhqc9fy5iabh73hw90xyrq8dk7s01qv7mjjalhwzl178flg")))

(define-public crate-secel-0.0.7 (c (n "secel") (v "0.0.7") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0y2ki2ba302hinaxxrzapvz430sk3s4l0fqp2fmc2zp1z2mvjl3b")))

