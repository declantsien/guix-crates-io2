(define-module (crates-io se as seastar) #:use-module (crates-io))

(define-public crate-seastar-0.1.0 (c (n "seastar") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iridescent") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "083fyjx33ff1nfw6n250jiwayhrnx8a69vzgn8if7aypgdnn3b52")))

(define-public crate-seastar-0.2.0 (c (n "seastar") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iridescent") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pzgcl9q1sfij36gvcjbbbgxjjdzxakkrmxadwbckkragmg7xdp7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

