(define-module (crates-io se as seaslug) #:use-module (crates-io))

(define-public crate-seaslug-0.0.0 (c (n "seaslug") (v "0.0.0") (d (list (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)))) (h "1pgijj2819j4nrz0vm08k12lz8niwd3g0cw9r5w5sh0bx854xq2i")))

