(define-module (crates-io se sh sesh-cli) #:use-module (crates-io))

(define-public crate-sesh-cli-0.1.0 (c (n "sesh-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iyggm62pf2c1lzv9p2m5546as1dgrz5viv6k8k775fgn756a5yq")))

(define-public crate-sesh-cli-0.1.1 (c (n "sesh-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zsg9gxyhkisxg8lwxjc1cmq83zacz6w93kfg2h330vcybjcksrf")))

(define-public crate-sesh-cli-0.1.2 (c (n "sesh-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y4f54jgx9s5jkhs2qrnymxzavjm9a62jsfipsw8dbrb6sns4hl3")))

(define-public crate-sesh-cli-0.1.3 (c (n "sesh-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qa8w6bycd9xri687rpxhdkzz62mffsb95zh1b5p6wlbxgcwmf10")))

(define-public crate-sesh-cli-0.1.4 (c (n "sesh-cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1whjawa1hj5lkyjivxrnfm330giy2c6ifwrljvd0k2fyk3rnnq60")))

(define-public crate-sesh-cli-0.1.5 (c (n "sesh-cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dckax7fvyr8jkp1fnsfd2lqp81xv8kq57alj03pg1f1i2hf7qhl")))

(define-public crate-sesh-cli-0.1.10 (c (n "sesh-cli") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cndj6mf66a6k7jqh6jrv6zc3fbzifxvwrsxnp337sn8am1yhsig")))

(define-public crate-sesh-cli-0.1.12 (c (n "sesh-cli") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n9zynnwjkskwnl4h8n0b5w7mmkmg6bzrqwgwhljf1z4q6nqcfsi")))

