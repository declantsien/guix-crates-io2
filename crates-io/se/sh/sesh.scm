(define-module (crates-io se sh sesh) #:use-module (crates-io))

(define-public crate-sesh-0.1.0 (c (n "sesh") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0p7mmw5y2bbj2z6gl841hz21l4ixb0f3scy9gs5fw2idv1qdnbhw")))

(define-public crate-sesh-0.1.1 (c (n "sesh") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0szgwg38nrvlxy7s7fqwfh0jp7w4gyqx2qs88asz9d0xygakylwa")))

