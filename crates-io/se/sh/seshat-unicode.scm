(define-module (crates-io se sh seshat-unicode) #:use-module (crates-io))

(define-public crate-seshat-unicode-0.0.1 (c (n "seshat-unicode") (v "0.0.1") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1wpwf625yc63sg2r917ajyhrqn487h5kiyz06mqj15i1l6fjrllk")))

(define-public crate-seshat-unicode-0.0.2 (c (n "seshat-unicode") (v "0.0.2") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "07z9gx8mkfjkksf4q9avy75z3zj8p1bnh7mghh9w78d9rsz9xh6i")))

(define-public crate-seshat-unicode-0.0.3 (c (n "seshat-unicode") (v "0.0.3") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1zspdn9yqi5sf7icv77kr1xdfc6xvwlcwjlrccg4bfy9ss516fdx")))

(define-public crate-seshat-unicode-0.0.4 (c (n "seshat-unicode") (v "0.0.4") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0vxnl7752cvrwxk5qj1yrjj9in8hmsffhjq9gllwpdlj3ki8brd8")))

(define-public crate-seshat-unicode-0.0.5 (c (n "seshat-unicode") (v "0.0.5") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1dnbvxsg3qxf8fy7m9cnjiwf73qxz1si6k3qaf91qk7rljsmppk4")))

(define-public crate-seshat-unicode-0.0.6 (c (n "seshat-unicode") (v "0.0.6") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1zfn46vh4xljzy9yilllqfhwqgrzy1gxhmmwiy487hr48cf1bv6m")))

(define-public crate-seshat-unicode-0.0.7 (c (n "seshat-unicode") (v "0.0.7") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "01nh43pm56bychm6shz004x47xwlf9897cyg90l4inzy4d7vafvw")))

(define-public crate-seshat-unicode-0.0.8 (c (n "seshat-unicode") (v "0.0.8") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0w4jjc0by8kxp2mdmcv49r7h4y5cflyfj5xricimwvnvxial4kab")))

(define-public crate-seshat-unicode-0.0.9 (c (n "seshat-unicode") (v "0.0.9") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0i08ajjmpxslainphj16ffwbzn0fnjvpd2z4fn446aaisml5i6pm")))

(define-public crate-seshat-unicode-0.0.10 (c (n "seshat-unicode") (v "0.0.10") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0lkkdiq1z6i8vdcxdpmlackn3pifh6rx0b79vsxmkp2f41ya8w4h")))

(define-public crate-seshat-unicode-0.0.11 (c (n "seshat-unicode") (v "0.0.11") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1a5x712jzhqgqwp4vmcl8wir1m4psf6xzfv5zbszhspl1mzl167i")))

(define-public crate-seshat-unicode-0.0.12 (c (n "seshat-unicode") (v "0.0.12") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1p5syxiw2jm0581yg7i7fqm5nryaicl6laavarrf0kjqs5762zyw")))

(define-public crate-seshat-unicode-0.0.13 (c (n "seshat-unicode") (v "0.0.13") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1xfjya8xjlk6114l7r1lq4n7byij1whbc92vr4szkgg2jvzzswn2")))

(define-public crate-seshat-unicode-0.0.14 (c (n "seshat-unicode") (v "0.0.14") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "1hb992swzkjp0j5js93cv4vhffnwzvggq60qm4j1mcxa17p8aj0g")))

(define-public crate-seshat-unicode-0.0.15 (c (n "seshat-unicode") (v "0.0.15") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0cakwzb021q6j1i0jl14m6a959r78xybr5mpv29xli7qnip03ks6")))

(define-public crate-seshat-unicode-0.1.0 (c (n "seshat-unicode") (v "0.1.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "087020xprh5m27mgb812hnnr3prjdrypl43fydh9lasshcrr3np2")))

(define-public crate-seshat-unicode-0.2.0 (c (n "seshat-unicode") (v "0.2.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0r72rscd5lvbkyqjjw0apcdx88dlzy61hgjpd27n71m12phrwily")))

(define-public crate-seshat-unicode-0.2.1 (c (n "seshat-unicode") (v "0.2.1") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)))) (h "0k2r324p5vbkxx5ng8807rvzpcipk0qa923dzwsmkc6bmdbxcki2")))

