(define-module (crates-io se sh sesh-proto) #:use-module (crates-io))

(define-public crate-sesh-proto-0.1.0 (c (n "sesh-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1zqxxa9k85x13ndf6inzn2m90v0fv4nzfhj89r4m1mdg02bi5ss5")))

(define-public crate-sesh-proto-0.1.1 (c (n "sesh-proto") (v "0.1.1") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1pj7j3fvdyslqw56q06x2y1wxw82v9scv038z5knp8wphmr4ssw2")))

(define-public crate-sesh-proto-0.1.2 (c (n "sesh-proto") (v "0.1.2") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1h9g9psckr0v1dbakkcs2bxhsq55an7yy0kxz2fwk5a6jgbzd92w")))

(define-public crate-sesh-proto-0.1.3 (c (n "sesh-proto") (v "0.1.3") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "029mwa5bg163s526mi4rd9b0b4h389xmsd04f36nd1ajii94j05w")))

(define-public crate-sesh-proto-0.1.4 (c (n "sesh-proto") (v "0.1.4") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "167nsf1jy1iz7s7zi1g43zc2xl035pawn62ypd0xnynivclyc43n")))

(define-public crate-sesh-proto-0.1.10 (c (n "sesh-proto") (v "0.1.10") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1x5rk3gzj5w458q64p0bryyag6xhjv3fqp7lsph5bywr7isip0xi")))

(define-public crate-sesh-proto-0.1.12 (c (n "sesh-proto") (v "0.1.12") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (f (quote ("transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1nxj8byf7di7ybmygwhfwp6x7i15r3rzv85lgr8j0i8jdziwk2a6")))

