(define-module (crates-io se ri serializers) #:use-module (crates-io))

(define-public crate-serializers-0.1.0 (c (n "serializers") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1q18gz9bsb8xzan1m8vgqm1xmsgnnhr0vcmv4fiv90pvlrlwfv3j")))

(define-public crate-serializers-0.1.1 (c (n "serializers") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1j0anic9kr8cnhf4nj96wgc4b7253w89n1i17vgxa56311076y8k")))

(define-public crate-serializers-0.1.2 (c (n "serializers") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1pjqxbw6h9py746xljpqwxmza667xif2b9bwpvaicjdbrr7d0rrx")))

(define-public crate-serializers-0.2.0 (c (n "serializers") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0sksvyhig7wk4qapkasqg325zn2bzhj73kslj3sh31ki3qrwjpb9")))

(define-public crate-serializers-0.2.1 (c (n "serializers") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "04mnlsxsqjjqlrdylk2552bsa32whzcw1b98ddrn4y1gxjx72gcw")))

(define-public crate-serializers-0.2.2 (c (n "serializers") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0x20fip1jpmw1dp9hxfm9f8isjb73b9f4z9ac6sl7pjvrf58sj2v")))

(define-public crate-serializers-0.2.3 (c (n "serializers") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "14rbjcs0ami9448jpaplpsshmfj48izjswh1q22vs6rj3va8x9jc")))

