(define-module (crates-io se ri serial-unix) #:use-module (crates-io))

(define-public crate-serial-unix-0.4.0 (c (n "serial-unix") (v "0.4.0") (d (list (d (n "ioctl-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "serial-core") (r "^0.4") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)))) (h "1dyaaca8g4q5qzc2l01yirzs6igmhc9agg4w8m5f4rnqr6jbqgzh")))

