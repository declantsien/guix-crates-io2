(define-module (crates-io se ri series) #:use-module (crates-io))

(define-public crate-series-0.1.0 (c (n "series") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cg0rx2lz5b9xqdfn6037qz29gp51hvf5744s4sb4bvq5pjd869j")))

(define-public crate-series-0.2.0 (c (n "series") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k4azzdzayggxjjgn2gw8yzvr6rsgy37jqv5av6yqlpz2gn131ir")))

(define-public crate-series-0.3.0 (c (n "series") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r9rrshcdfgv71vlg57i9fpjr5s75afhckm7jg6w3y8dqm2ix3r0")))

(define-public crate-series-0.4.0 (c (n "series") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ifkxq2cvmw2kylzkyr6m2l21bv2mji5vlqs6jwvvys6vv7siyks")))

(define-public crate-series-0.5.0 (c (n "series") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pkdrxz535b7vnwyfzhigxjznz38hdns9a0npba6g9lj7gn51r8y")))

(define-public crate-series-0.6.0 (c (n "series") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11w5v26h60lmy4is5vy5asmn87xhqg7g5l5890pbf0mb9navlhzv")))

(define-public crate-series-0.7.0 (c (n "series") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xny7134lyd9vzf6iz09d275wll93qnh5hph0h61k16is2d01c88")))

(define-public crate-series-0.7.1 (c (n "series") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k82d3h6iby2gszn2npq9dxp7656rvbn5xjyw2m1iia9q3w5n5jj")))

(define-public crate-series-0.8.0 (c (n "series") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15gvrsfyhnwzxqxx68jykkifci2jvl791fyy65nmcaxrpcwi7nf6")))

(define-public crate-series-0.10.0 (c (n "series") (v "0.10.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qavaw6sh2bgcjhbnv2jljchcs80fpza9cfpvdmc0kjbj4rsl4z5")))

(define-public crate-series-0.9.0 (c (n "series") (v "0.9.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04d0lqi8m1y88k4aaz8jabwqpwrjn642jn2mzwz4myx2p0dn6378")))

(define-public crate-series-0.11.0 (c (n "series") (v "0.11.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vqsgcrdasvark5iqalbvy28y58j3nlh8z40pazpwb2cf2x4i66d")))

