(define-module (crates-io se ri serialize-to-javascript-impl) #:use-module (crates-io))

(define-public crate-serialize-to-javascript-impl-0.1.0 (c (n "serialize-to-javascript-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0blh085mzlq0qp3sigdn4d3ik5y02vahzjkmwvdr1xk2g4yzhgsf") (r "1.31")))

(define-public crate-serialize-to-javascript-impl-0.1.1 (c (n "serialize-to-javascript-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qqpky462g47za56cdmzmc8b9rnh09wn5jzkq425z8gnx5s4h1kl") (r "1.31")))

