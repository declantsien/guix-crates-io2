(define-module (crates-io se ri serialitm) #:use-module (crates-io))

(define-public crate-serialitm-0.1.0 (c (n "serialitm") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "itm") (r "^0.3") (d #t) (k 0)) (d (n "serialport") (r "^3.2") (d #t) (k 0)))) (h "0gjhk8n2lh6mkszm84f31bxrk5l2wwyr5xphgajyjw4xh9ixj2nw")))

(define-public crate-serialitm-0.2.2 (c (n "serialitm") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "itm") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "mio-named-pipes") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "mio-serial") (r "^3.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("commapi" "handleapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1063a7archcflg1ydcsh3wv8p2llxnk0ivjdfvycxd1935cwha54")))

