(define-module (crates-io se ri serial_enumerate) #:use-module (crates-io))

(define-public crate-serial_enumerate-0.1.0 (c (n "serial_enumerate") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1619yi0zc851z8w2hvs4pqiaxyg0hwphr9il2p7k738nkcvs3fq6")))

