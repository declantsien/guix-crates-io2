(define-module (crates-io se ri serial-rs) #:use-module (crates-io))

(define-public crate-serial-rs-0.2.0 (c (n "serial-rs") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt" "synchapi" "ioapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d6hkv073hmnb47hlaxj7sl7ijb5zgzxiks7a4zdjmqd09sxfn4i")))

(define-public crate-serial-rs-0.2.1 (c (n "serial-rs") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt" "synchapi" "ioapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s65hk5pb8d0rg76yymy8ns29v667678vhva1rd33nr3328izjm6")))

