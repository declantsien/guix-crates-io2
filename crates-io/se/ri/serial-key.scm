(define-module (crates-io se ri serial-key) #:use-module (crates-io))

(define-public crate-serial-key-1.0.0 (c (n "serial-key") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0k6070g1d3m1dbcdffka6dpy3r2rv2mx4xz3acj630avsmy2j10m")))

(define-public crate-serial-key-2.0.0 (c (n "serial-key") (v "2.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0v3n2m3mh6b4kb60ihlkknzmddq4lz95rimh83im3mpncangy0cq")))

