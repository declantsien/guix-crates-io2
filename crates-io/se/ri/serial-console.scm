(define-module (crates-io se ri serial-console) #:use-module (crates-io))

(define-public crate-serial-console-0.1.0 (c (n "serial-console") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1c3akx61ldd2m8553a13d70pwlnycjp6mnvsjkkpbxsri3cvd2nv") (y #t)))

(define-public crate-serial-console-0.2.0 (c (n "serial-console") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "04hgsfq7s7lff2a024jnigx9psi3vd6h784zh42rcpvyir11gbyf")))

(define-public crate-serial-console-0.4.0 (c (n "serial-console") (v "0.4.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "11gq4l4ld7ph6ykj24kl88k5n4pw8gvnyffi3f2llgzmid7n1850")))

(define-public crate-serial-console-0.4.1 (c (n "serial-console") (v "0.4.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0xn7dcjcagckykfkgqy3d3fzwgblh15awbgj4nfbb6yw0gd2k468")))

(define-public crate-serial-console-0.4.2 (c (n "serial-console") (v "0.4.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1v84l138q8x4h6wm1mvrqw5zicjbahri3igxlwrpxgffa7lcaz32") (y #t)))

(define-public crate-serial-console-0.4.3 (c (n "serial-console") (v "0.4.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "12ds0bibd6gxjs3g6wr7qbrshqccb7v38q5kipp2lag290gydany")))

(define-public crate-serial-console-0.4.4 (c (n "serial-console") (v "0.4.4") (d (list (d (n "clap") (r "^3.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1gcqd7bjblyij2c20fj7smmf54mxkz1rckk4r2kbp0fn1k9vaczj")))

(define-public crate-serial-console-1.0.0 (c (n "serial-console") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ddczasi6y0558mqg607a3rv6jdvn001glrv1v9kq8z87m9kp3a3")))

(define-public crate-serial-console-1.0.1 (c (n "serial-console") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1rkby5mp865xqvnmbd3r43ndydsrrif4qx8xv5vamj81scjzhp5s")))

