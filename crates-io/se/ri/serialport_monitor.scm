(define-module (crates-io se ri serialport_monitor) #:use-module (crates-io))

(define-public crate-serialport_monitor-1.0.0 (c (n "serialport_monitor") (v "1.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1sns4hkznhf3jn8sf26vrnkx8w1y41nakv2w9zh4an2ksds05s3w")))

