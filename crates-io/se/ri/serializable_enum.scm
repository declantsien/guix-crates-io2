(define-module (crates-io se ri serializable_enum) #:use-module (crates-io))

(define-public crate-serializable_enum-0.1.0 (c (n "serializable_enum") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6.13") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "0rs54gfjaz40fcy9m2j2iwfmsc1q2md867x1216q1xahnscg9nfy") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-serializable_enum-0.2.0 (c (n "serializable_enum") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6.13") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "0a6pxj7k5zn2ppxnz05hwqwfj481nbs1rld3639rv188zfnmzr8d") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-serializable_enum-0.3.0 (c (n "serializable_enum") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 2)))) (h "066dzdj7ncgn4i5xip7586wbjs09p31sfkjac9zpxnj5y2daa4xi") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-serializable_enum-0.3.1 (c (n "serializable_enum") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.7.0, < 0.9.0") (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7.0, < 0.9.0") (d #t) (k 2)))) (h "0842ayskrnm9nrvfzdx63002h3dx2ajqrag2bbnvjhfw8ffwa1i9") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-serializable_enum-0.4.0 (c (n "serializable_enum") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "1wsfq0xxf43zjnps45ydkqx2c0pwvnv93knhk0wdw6hwa1x3z7fh") (f (quote (("nightly-testing" "clippy"))))))

