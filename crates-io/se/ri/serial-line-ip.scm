(define-module (crates-io se ri serial-line-ip) #:use-module (crates-io))

(define-public crate-serial-line-ip-0.1.0 (c (n "serial-line-ip") (v "0.1.0") (h "0rry8rvz6ik3fvssvs6yldgzsk02as6kz3h901ndr0z01rsgzjx8")))

(define-public crate-serial-line-ip-0.2.0 (c (n "serial-line-ip") (v "0.2.0") (h "006vswr226bdzvhajjjfmb4blr3qb667q4676590bs69p6iq4fl6")))

(define-public crate-serial-line-ip-0.3.0 (c (n "serial-line-ip") (v "0.3.0") (h "0r1y18xms6n7nvvgg4chk44485yhiwfa1bmx8hz9843h57fl6264")))

(define-public crate-serial-line-ip-0.4.0 (c (n "serial-line-ip") (v "0.4.0") (h "0f8qpagnx5j4ljddnilq4mx80hv4cms441vzkg5i3m21mcvvxf1g")))

(define-public crate-serial-line-ip-0.5.0 (c (n "serial-line-ip") (v "0.5.0") (h "0n7r0vik2p5acyhca3h7ivagbnysmkl7mqzpkw11asxbrmmvi808")))

