(define-module (crates-io se ri serialize_display_adapter_macro_derive) #:use-module (crates-io))

(define-public crate-serialize_display_adapter_macro_derive-0.1.0 (c (n "serialize_display_adapter_macro_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "18f54fk850x0lkzwbxzrm9anspn7iixbr9s72vri1igdk41cld69")))

(define-public crate-serialize_display_adapter_macro_derive-0.1.1 (c (n "serialize_display_adapter_macro_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "16gv0fwsna0lhml4fs6spfvhsqzpp8hfrdp27s4a9b5dk8vwvghq")))

(define-public crate-serialize_display_adapter_macro_derive-0.1.2 (c (n "serialize_display_adapter_macro_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "0gfxap9rkdpyzwrcc6h7k84l547adcxgci1czfd021r5wipps3i2")))

(define-public crate-serialize_display_adapter_macro_derive-0.1.3 (c (n "serialize_display_adapter_macro_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "0k4icj9bgnb3g6bxs8fpmyv018269d7fyisrxdji2ylp7bcq5jv3")))

