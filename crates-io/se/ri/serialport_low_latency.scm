(define-module (crates-io se ri serialport_low_latency) #:use-module (crates-io))

(define-public crate-serialport_low_latency-0.1.0 (c (n "serialport_low_latency") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (k 0)) (d (n "serialport") (r "^4.2.0") (k 0)))) (h "0rr8ab0p7h7g03zf199z9b3wvilgxx3cp3c10bsn1jb405cnrc9n")))

