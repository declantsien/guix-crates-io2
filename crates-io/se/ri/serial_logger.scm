(define-module (crates-io se ri serial_logger) #:use-module (crates-io))

(define-public crate-serial_logger-0.1.0 (c (n "serial_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "06d1xv0kxgv3nd69lbixny34xdnmidx0naxn40hz5qbx9284k0qv")))

