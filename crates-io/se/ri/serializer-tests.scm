(define-module (crates-io se ri serializer-tests) #:use-module (crates-io))

(define-public crate-serializer-tests-0.1.4 (c (n "serializer-tests") (v "0.1.4") (d (list (d (n "mv-binary-format") (r "^0.1.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "0wzw59gjshkhq7vn20kgwzfcikwqfj4599n8dqkbgk8c4g6212fn") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.1.5 (c (n "serializer-tests") (v "0.1.5") (d (list (d (n "mv-binary-format") (r "^0.1.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "14wvzff05vk70l9zn7q6qqvw6a8hwfx1cyqdsrlp2bkd70qjc20l") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.1.6 (c (n "serializer-tests") (v "0.1.6") (d (list (d (n "mv-binary-format") (r "^0.1.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "08lghdhqv4p3fncdcs3x87vr1a3j37jw47qrgqcchk4ca54iqdci") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.2.0 (c (n "serializer-tests") (v "0.2.0") (d (list (d (n "mv-binary-format") (r "^0.2.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "1r2zh2jkaym9y6jk3cmhk7vcph5qnkv9qrq727z97z1l5vzddkzm") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.2.1 (c (n "serializer-tests") (v "0.2.1") (d (list (d (n "mv-binary-format") (r "^0.2.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "02rx22gj8ci8pizbw1hxhhs7ml7qkg8y1basia9yssj13akmqpcr") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.3.0 (c (n "serializer-tests") (v "0.3.0") (d (list (d (n "mv-binary-format") (r "^0.3.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "14vk5iag768v5c976zrvdn8f6qp9nvnvghx6cnjrsbvazyp2pnfs") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.3.1 (c (n "serializer-tests") (v "0.3.1") (d (list (d (n "mv-binary-format") (r "^0.3.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "1vhjx3wgkg60x059g8hsf3r9n1i0v2dhr27zcjq4n7klgp4h900m") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

(define-public crate-serializer-tests-0.3.2 (c (n "serializer-tests") (v "0.3.2") (d (list (d (n "mv-binary-format") (r "^0.3.0") (f (quote ("fuzzing"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "0d1wgnmghd38nbiw58i610hcaq6ixxjxvqbwv3bv0y3a1aihmi86") (f (quote (("fuzzing" "mv-binary-format/fuzzing")))) (y #t)))

