(define-module (crates-io se ri serialio) #:use-module (crates-io))

(define-public crate-serialio-0.1.0 (c (n "serialio") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0lzv4810c3fm0j8xgqrnl6xpzwg2ahdkv0amvnh623n4r95awpxh")))

(define-public crate-serialio-0.1.1 (c (n "serialio") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.5") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "f3") (r "^0.6.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.0") (d #t) (k 2)) (d (n "stm32f30x-hal") (r "^0.2.0") (d #t) (k 0)))) (h "08v1f0wrr7qq4g9qhv9w8fcpp4y9pwymx3j60cc2kbckvkir36b0")))

(define-public crate-serialio-0.1.2 (c (n "serialio") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.5") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "f3") (r "^0.6.1") (f (quote ("rt"))) (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.0") (d #t) (k 2)) (d (n "stm32f30x-hal") (r "^0.2.0") (d #t) (k 2)))) (h "1qbk42lk68x7hb567c19pnvlp8m0am610ibpglc6224r1llngg5s")))

