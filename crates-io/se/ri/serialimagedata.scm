(define-module (crates-io se ri serialimagedata) #:use-module (crates-io))

(define-public crate-serialimagedata-1.0.0 (c (n "serialimagedata") (v "1.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("raw_value"))) (d #t) (k 2)))) (h "05j9a41rxjl9m20z459q58mla0yy0ac84yfhfj7pqipl6dpjlijx") (y #t) (r "1.68.0")))

