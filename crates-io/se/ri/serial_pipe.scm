(define-module (crates-io se ri serial_pipe) #:use-module (crates-io))

(define-public crate-serial_pipe-0.1.0 (c (n "serial_pipe") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ebacktrace") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "0a3dflcmav8dzancnlck165adx9hpp5z4yq7qk13039im6c72z0l") (f (quote (("default")))) (y #t)))

