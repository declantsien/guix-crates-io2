(define-module (crates-io se ri serialize_deserialize_u8_i32) #:use-module (crates-io))

(define-public crate-serialize_deserialize_u8_i32-0.1.0 (c (n "serialize_deserialize_u8_i32") (v "0.1.0") (h "0icl4q76cd71aamk6nfa4b783dcy3b00rj31wndifz13hlylp05s")))

(define-public crate-serialize_deserialize_u8_i32-0.1.1 (c (n "serialize_deserialize_u8_i32") (v "0.1.1") (h "082z4x629cj3gnmqhln5h3s35jpc3jvix4laqfyfxxan7mmfq637")))

(define-public crate-serialize_deserialize_u8_i32-0.1.2 (c (n "serialize_deserialize_u8_i32") (v "0.1.2") (h "03az3naz6i976nsb1ln8r4sws0id642sc0jwf3r5s2q31b8w6q8y")))

(define-public crate-serialize_deserialize_u8_i32-0.1.4 (c (n "serialize_deserialize_u8_i32") (v "0.1.4") (h "0z89ijxkbvghxxdc8p02y3m2w014cf2kgvw0y3bb00ddncqa8hn2")))

(define-public crate-serialize_deserialize_u8_i32-0.1.5 (c (n "serialize_deserialize_u8_i32") (v "0.1.5") (h "1rd9378vp0is6cl8hmd0x6gvysz0r5dvyhqkg0p4194v2gl7j6fa")))

(define-public crate-serialize_deserialize_u8_i32-0.1.6 (c (n "serialize_deserialize_u8_i32") (v "0.1.6") (h "0q7wplgdjfgjghbrg8i2p2p67dbg4snmjnab7x6pv9wc0b3y4ar8")))

(define-public crate-serialize_deserialize_u8_i32-0.1.7 (c (n "serialize_deserialize_u8_i32") (v "0.1.7") (h "04b7c3x2sphyrl4iidiqrj2qd4idjcqccx9r9ckhhz0gy309a899")))

(define-public crate-serialize_deserialize_u8_i32-0.1.8 (c (n "serialize_deserialize_u8_i32") (v "0.1.8") (h "1yf6li5fnxwq0qw5m0mj9m0mma3ynv4fks580b1sqlm60xzr3hk6")))

(define-public crate-serialize_deserialize_u8_i32-0.1.9 (c (n "serialize_deserialize_u8_i32") (v "0.1.9") (h "1zf917c20469zqac2a8mz0d2aik6bj9cc3salivrsyh70jqzr68x")))

(define-public crate-serialize_deserialize_u8_i32-0.1.10 (c (n "serialize_deserialize_u8_i32") (v "0.1.10") (h "0b37p9vnnw2cq1680735m25sp06pbms91fcbvzmsd4jj1gpvbg2g")))

(define-public crate-serialize_deserialize_u8_i32-0.1.11 (c (n "serialize_deserialize_u8_i32") (v "0.1.11") (h "0sgsz76xkgnbdbi71aqi6a0428pjazgz2p0z2qgbz10l59i9z1vd")))

(define-public crate-serialize_deserialize_u8_i32-0.1.12 (c (n "serialize_deserialize_u8_i32") (v "0.1.12") (h "1fafh26jr4baaaifpxwkxzibsm5i2l6jgf4f39byrnhrx9pv6p8w")))

(define-public crate-serialize_deserialize_u8_i32-0.1.13 (c (n "serialize_deserialize_u8_i32") (v "0.1.13") (h "0ds7yl5hr3n612s3kfgj749xxb98bbfmq7rapbl6na133g9dz490")))

(define-public crate-serialize_deserialize_u8_i32-0.1.14 (c (n "serialize_deserialize_u8_i32") (v "0.1.14") (h "0sk7di5wh93lx479yccdgmp9zvqaiawkbd6iqcy1bnkqns6jpnmz")))

(define-public crate-serialize_deserialize_u8_i32-0.1.15 (c (n "serialize_deserialize_u8_i32") (v "0.1.15") (h "02cmpbvaand50l0zwwn6bzbl3c0n0i2nwnlmnvq5b1q9l6spq42f")))

(define-public crate-serialize_deserialize_u8_i32-0.1.16 (c (n "serialize_deserialize_u8_i32") (v "0.1.16") (h "1prk4fccxy24zf7cp23l2wbbsbfy324s0d0s22vwbkkdw2qlh595")))

(define-public crate-serialize_deserialize_u8_i32-0.1.17 (c (n "serialize_deserialize_u8_i32") (v "0.1.17") (h "08av7a3syv1i42slkds89jm874vlk2l9dd8j04cc65nc2nxamnld")))

(define-public crate-serialize_deserialize_u8_i32-0.1.18 (c (n "serialize_deserialize_u8_i32") (v "0.1.18") (h "1gcbcyjmk3c5x9dzdn1xhlrf2p5ywv35n0v5vm3ac5mb722dj1ms")))

(define-public crate-serialize_deserialize_u8_i32-0.1.19 (c (n "serialize_deserialize_u8_i32") (v "0.1.19") (h "1777izj42dm0vjxnjrx9v85gkxqppqb22w3rlp2qg4hgviikshmc")))

(define-public crate-serialize_deserialize_u8_i32-0.1.20 (c (n "serialize_deserialize_u8_i32") (v "0.1.20") (h "1g103bq6cb1a5p77wsyhz38w2p219b34a4dznjydlnl2p9zwdrn5")))

(define-public crate-serialize_deserialize_u8_i32-0.1.21 (c (n "serialize_deserialize_u8_i32") (v "0.1.21") (h "1gyk2h8iyjn99vfvwr6slpza2q9cjc0135fdabk4bdxrad1zmy0j")))

(define-public crate-serialize_deserialize_u8_i32-0.1.23 (c (n "serialize_deserialize_u8_i32") (v "0.1.23") (h "0daw2vng2ig8yz9bw02bx0fb2g2fznlj9dxhfja4l03i6hzh3zlq")))

(define-public crate-serialize_deserialize_u8_i32-0.1.24 (c (n "serialize_deserialize_u8_i32") (v "0.1.24") (h "1imnmqkfwyi09rn2jhr63a4dmifbkyqs41s5qw92svf29a1hir7p")))

(define-public crate-serialize_deserialize_u8_i32-0.1.25 (c (n "serialize_deserialize_u8_i32") (v "0.1.25") (h "0hpriggf8hvm9a5whl08583w1d7maklpymxgdby6zcaj4y8yainj")))

(define-public crate-serialize_deserialize_u8_i32-0.1.26 (c (n "serialize_deserialize_u8_i32") (v "0.1.26") (h "1g84i2y7akgcnm53kl77banggxhxci4li8wllz4d9zg1419gy51g")))

(define-public crate-serialize_deserialize_u8_i32-0.1.27 (c (n "serialize_deserialize_u8_i32") (v "0.1.27") (h "0q3bkx5s9z2jyrqcmj25l99v7c1x7zbqrrxja1g8yi5b0ziq567z")))

(define-public crate-serialize_deserialize_u8_i32-0.1.28 (c (n "serialize_deserialize_u8_i32") (v "0.1.28") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0nl5iy5yxw8f2qn7nmhh56narl4yswsiav53smf05nn19861jp8b")))

(define-public crate-serialize_deserialize_u8_i32-0.1.29 (c (n "serialize_deserialize_u8_i32") (v "0.1.29") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1lwkkpsnlnpafsrzgsykv9diijhgm3j59936s76nr4v0bkn504bf")))

(define-public crate-serialize_deserialize_u8_i32-0.1.30 (c (n "serialize_deserialize_u8_i32") (v "0.1.30") (h "0h96g2hw92vs4zz4455m34045zp03xz7vkhn6d2qmf73k00wmi4w")))

(define-public crate-serialize_deserialize_u8_i32-0.1.31 (c (n "serialize_deserialize_u8_i32") (v "0.1.31") (h "0cxzmirdh9kcik0fwhmfhb4vzwjryq9ldzgy5xsc0z9axxi0zrz0")))

(define-public crate-serialize_deserialize_u8_i32-0.1.34 (c (n "serialize_deserialize_u8_i32") (v "0.1.34") (h "0hfdb74anrq387kdpx3rvm9wbih1fg41rrfjmjgqfw9y8dc4qrgg")))

