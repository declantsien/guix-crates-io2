(define-module (crates-io se ri serializer) #:use-module (crates-io))

(define-public crate-serializer-0.0.0 (c (n "serializer") (v "0.0.0") (h "085npfc376l8iq6qfgimm7y0k3351kkda8yyn0xyh5xflkgdkb85") (y #t)))

(define-public crate-serializer-2.0.1 (c (n "serializer") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1xz1mansckjklv6kvjxfnzh2v22ch8n799qfx4xkwjn838axi8y7")))

(define-public crate-serializer-2.0.2 (c (n "serializer") (v "2.0.2") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0zsd4h9f17sqnk2akk0l7259ld78w037c1rrfgdnyr07wfdnsm9a")))

(define-public crate-serializer-2.1.2 (c (n "serializer") (v "2.1.2") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0xl3pkmwy00v2a5fgw0v4k4px3cnbap14ly4w70qlqg14xsbvgd1")))

