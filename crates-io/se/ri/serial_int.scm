(define-module (crates-io se ri serial_int) #:use-module (crates-io))

(define-public crate-serial_int-0.1.0 (c (n "serial_int") (v "0.1.0") (h "11cb2j4xbvsildwj9yqvkrjarkyjxkdnfgsabbxwbr0amrjhhc4d")))

(define-public crate-serial_int-0.2.0 (c (n "serial_int") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "147qbyy0ha8v8n4qbqrd85264976l54vjiyx42zb8ald6x7b2s83")))

(define-public crate-serial_int-0.2.1 (c (n "serial_int") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "18w0v67cgiv57szr0pb7nas6i00kwg7z17s6d62p9wpkffgnzaxb")))

(define-public crate-serial_int-0.2.2 (c (n "serial_int") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0lqv1zb1frdr9s4fv0lb6p56vinbqg6pr7pw2n7xj9zycnmkc9vc")))

(define-public crate-serial_int-0.2.3 (c (n "serial_int") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "19h4v05dgqhdmlfp5xp7qz1w8ysjkz4g04rr6nmwyw7y2dhimykw")))

(define-public crate-serial_int-0.3.0 (c (n "serial_int") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1mgy3x3yjhd786mij3hlqz6mjx6k1w28mfi73n7kjz0mm7jqmw1f")))

(define-public crate-serial_int-0.3.1 (c (n "serial_int") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "094iqfvb1j234mg84xcs80nya8jdyvja2m4jwkvf50nhagmx3w8p")))

(define-public crate-serial_int-0.4.0 (c (n "serial_int") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "18k780qracz1020ppvxxmbpwj33qcx6avvdzmzx620axllnlk2xk") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-1.0.0 (c (n "serial_int") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1nbk9wnnzqxzdd7bh86p1ah4f5lfk92czglsz9f7kqrn8jnw8pif") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-1.1.0 (c (n "serial_int") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "197q0g6ngyak104ggn151nivi6fw0986fh8rc2kyj0v12kxmmk4m") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-1.1.1 (c (n "serial_int") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0w3sb4x0v82r3gvr68klnvc40w6x5gcaf1q86dds06g2lpylv17n") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-1.1.2 (c (n "serial_int") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0cc50kjjs1vsb7wzbrh33kjxy77afph7ls8ywiq4ib092cxzz5gr") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-1.1.3 (c (n "serial_int") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0h722187ym4mad8vj45r0nk4ml6jjz7s0kbb5j55z6ypqkbn81pb") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-2.0.0 (c (n "serial_int") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "069mrgp7ya43j1y5607528c1m7c0ginnv6smcz1m5n28w2nacf1g") (f (quote (("serde_impl" "serde"))))))

(define-public crate-serial_int-2.0.1 (c (n "serial_int") (v "2.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "17rf7gwv2glbrn3263wg5ghxnhx7nsr8ql7dlhcry00qkrqhcs19") (f (quote (("serde_impl" "serde"))))))

