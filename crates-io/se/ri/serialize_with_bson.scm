(define-module (crates-io se ri serialize_with_bson) #:use-module (crates-io))

(define-public crate-serialize_with_bson-0.1.0 (c (n "serialize_with_bson") (v "0.1.0") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1whm4c99han4pxhw4pkb1nf1mhx7bbsixnzarbdnxwv28c858hc5") (y #t)))

(define-public crate-serialize_with_bson-0.1.1 (c (n "serialize_with_bson") (v "0.1.1") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1mz5f0w6lw0z85zgiyhs5v5db19xrljwq4zgkmhr5yqcjfrqkrg2")))

(define-public crate-serialize_with_bson-0.1.2 (c (n "serialize_with_bson") (v "0.1.2") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0kl5baampn9vdqschs37i30w2m8c2xp93g0zlhdjxgh8d5dhjzmm")))

(define-public crate-serialize_with_bson-0.1.3 (c (n "serialize_with_bson") (v "0.1.3") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gshdxyyl1bn9872gqyhghdqa5r9dfvjbi6h16snr3ccwl96lriw")))

(define-public crate-serialize_with_bson-0.1.4 (c (n "serialize_with_bson") (v "0.1.4") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "gostd_time") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rxygmh0imgdsafa121bn6qkvhzdgyyz3jy4ii3dnkprqqmb3i5r")))

