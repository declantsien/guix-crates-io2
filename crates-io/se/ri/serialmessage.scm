(define-module (crates-io se ri serialmessage) #:use-module (crates-io))

(define-public crate-serialmessage-0.1.0 (c (n "serialmessage") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 2)))) (h "0j8piz4ar6pzm9jc8l16f7arfsmxjc1y2wlgrf9122gnp9cw0ksb") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-serialmessage-0.1.1 (c (n "serialmessage") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 2)))) (h "193jiw0xax8l3jd7mysx00yzbf2p31zhxkyciz78ww7rw0nlslkv") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-serialmessage-0.2.0 (c (n "serialmessage") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 2)))) (h "087y0v7c636fd73qbar23wpgzl87miwfv2k10gjiifkimm8q5f0i") (f (quote (("default" "alloc") ("alloc"))))))

