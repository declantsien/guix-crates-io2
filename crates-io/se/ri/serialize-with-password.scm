(define-module (crates-io se ri serialize-with-password) #:use-module (crates-io))

(define-public crate-serialize-with-password-0.1.0 (c (n "serialize-with-password") (v "0.1.0") (d (list (d (n "argon2") (r "^0.5.2") (f (quote ("alloc"))) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("alloc"))) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cmc0mifb8qrna66faqmdcl06sx5z7j6kszzwskg9i95j2fwaf37") (s 2) (e (quote (("serde" "dep:serde" "dep:rmp-serde"))))))

