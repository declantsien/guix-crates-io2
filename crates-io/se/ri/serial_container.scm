(define-module (crates-io se ri serial_container) #:use-module (crates-io))

(define-public crate-serial_container-0.1.0 (c (n "serial_container") (v "0.1.0") (h "150damrj7ik4jc62dw5kzjzdys9vnzyr2c76ivgacvn64f40abc6") (y #t)))

(define-public crate-serial_container-0.2.0 (c (n "serial_container") (v "0.2.0") (h "0lsqzyvxcasjvbv2qrig4a82712g4d58q12sg5j1sj0xy4s4q05m") (y #t)))

(define-public crate-serial_container-0.3.1 (c (n "serial_container") (v "0.3.1") (h "0nw9qwvn7hafcqrj82fbridhs78ki2amzdlqkf7ab5cg8z74xg1k") (y #t)))

