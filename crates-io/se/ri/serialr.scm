(define-module (crates-io se ri serialr) #:use-module (crates-io))

(define-public crate-serialr-0.1.0 (c (n "serialr") (v "0.1.0") (h "1rv9dl7p220lsq8mdxw7rayy6qjxh44gbz84ivif67qmsh78a5bp")))

(define-public crate-serialr-0.1.1 (c (n "serialr") (v "0.1.1") (h "101gwlqw2xzg846ignbhf6agkm60bws9k6xw84cgqk13ms1dw8r3")))

(define-public crate-serialr-0.1.3 (c (n "serialr") (v "0.1.3") (d (list (d (n "serialize_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0q2si663wcinf8i746ivwawgrq1wdqlmws646i48gabd9hlddadz")))

(define-public crate-serialr-0.1.4 (c (n "serialr") (v "0.1.4") (d (list (d (n "serialize_derive") (r "^0.1.0") (d #t) (k 0)))) (h "13m7skda5d3ks1ci3hjc4hf1bf0qbfma39yiqfwlndh0c9308zbn")))

(define-public crate-serialr-0.1.5 (c (n "serialr") (v "0.1.5") (d (list (d (n "serialize_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0780g3dr1xyjab1i4lj037nbl9wyll8rh7x4i9xil0ndy4a81nhd")))

(define-public crate-serialr-0.1.6 (c (n "serialr") (v "0.1.6") (d (list (d (n "serialize_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0zybxdasadxs5fkvn90qr2k64vjicwvqa18vbg43hfckdidfw17b")))

(define-public crate-serialr-0.1.7 (c (n "serialr") (v "0.1.7") (d (list (d (n "serialize_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0yq8iyfm4nf9j4hdnk2jfg6sgjpxw1i1n8j31r7az907wrw9a67w")))

(define-public crate-serialr-0.1.8 (c (n "serialr") (v "0.1.8") (d (list (d (n "serialize_derive") (r "^0.1.3") (d #t) (k 0)))) (h "0ah1wn4lvggswgzapl0f6chl4zp9vn5pzbfxirwk1d9vk26d260b")))

(define-public crate-serialr-0.1.9 (c (n "serialr") (v "0.1.9") (d (list (d (n "serialize_derive") (r "^0.1.3") (d #t) (k 0)))) (h "0lf9ra71qm2wwnfdc72lc73ndx37z8r8vgd9mjr78b4g9d1lf604")))

