(define-module (crates-io se ri serialize-quill-delta) #:use-module (crates-io))

(define-public crate-serialize-quill-delta-0.1.0 (c (n "serialize-quill-delta") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1inv6qv3kkcjjqi5hd26kd02j4a1pzip8ys3aiw662yx591gqwa2")))

