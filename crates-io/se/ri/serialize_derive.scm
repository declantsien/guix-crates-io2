(define-module (crates-io se ri serialize_derive) #:use-module (crates-io))

(define-public crate-serialize_derive-0.1.0 (c (n "serialize_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "0xjw6mz3ig8vfb5nfv7jqvz6cbsv06rgpr43rv7d0nd4dl61w2y0")))

(define-public crate-serialize_derive-0.1.1 (c (n "serialize_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "1jxc9mkq2mj7r3vvamfma68adx8rjpm2h6pj21ga037b0lbb7ky0") (y #t)))

(define-public crate-serialize_derive-0.1.2 (c (n "serialize_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "11lq7g0bhh6n0ry02wn4mik8ivfhcxvzcidjpcx7wx2wr3syv9k5")))

(define-public crate-serialize_derive-0.1.3 (c (n "serialize_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "1id00zkvyyagww6s02sd03y2rva44lf8axf94knlhcgj6bky67zn")))

