(define-module (crates-io se ri seripack_macros) #:use-module (crates-io))

(define-public crate-seripack_macros-0.1.0 (c (n "seripack_macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xidf645mr1a08vrlnw6gwpimgihc309p20a9rbsbxlmzyg68gw5")))

