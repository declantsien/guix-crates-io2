(define-module (crates-io se ri serializable_log_record) #:use-module (crates-io))

(define-public crate-serializable_log_record-0.3.0 (c (n "serializable_log_record") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ix0fjh7ya1d02np3p60xp1d2z5sw8x6nw2mr1w8r8r1cwy6w8hx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-serializable_log_record-0.3.1 (c (n "serializable_log_record") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xmisl37lavaqdi0j2dm6icq8v8p4gbf6cbd0cmgy9rwp8ah9lfy") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-serializable_log_record-0.3.2 (c (n "serializable_log_record") (v "0.3.2") (d (list (d (n "bincode") (r "^2.0.0-rc") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b686xnhgqgsk915x4ysnxbxsz6c4yfgas96rghql6qk26xjlxfj") (s 2) (e (quote (("serde" "dep:serde") ("bincode2" "dep:bincode"))))))

