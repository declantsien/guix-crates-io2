(define-module (crates-io se ri serifs) #:use-module (crates-io))

(define-public crate-serifs-0.1.0 (c (n "serifs") (v "0.1.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1pk2fq2x320s2dn0wrxgwlpfgzbd9z5lmb7ix5xfagj6q6ybmcgs") (y #t)))

