(define-module (crates-io se ri serializable-yaml) #:use-module (crates-io))

(define-public crate-serializable-yaml-0.1.0 (c (n "serializable-yaml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1j2r17znip55f87b9axbqpm0nndzq42876w3mc733qd9158xynwm")))

(define-public crate-serializable-yaml-0.2.0 (c (n "serializable-yaml") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1h26zy0ckyrj4qwx8a711wz9ia470xc5ka312fml2hc61lqlphfx")))

(define-public crate-serializable-yaml-0.3.0 (c (n "serializable-yaml") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "00mi24cgk2xfa5sljla3lr58kjw8h85ljj81yp8g99y899ys5qp8")))

(define-public crate-serializable-yaml-0.4.0 (c (n "serializable-yaml") (v "0.4.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "11610m55ik92qnsdh22irym6hap2jdjr18h4ppkdm6yibxmqy83q")))

(define-public crate-serializable-yaml-0.5.0 (c (n "serializable-yaml") (v "0.5.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "138yzv6jc8sk2srbk4fm38c7yk0afp7sfj9fb6q3iv173dc09ddg")))

(define-public crate-serializable-yaml-0.6.0 (c (n "serializable-yaml") (v "0.6.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "093c1is7kn0bgvrr03awg13b8x2krgzzbn5qf1lsv7lih7f1p81a")))

(define-public crate-serializable-yaml-0.6.1 (c (n "serializable-yaml") (v "0.6.1") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1xs1dzr1cgispgi7shkpg62bnpz08m8gxvxaq24h3y92l8jphlq6")))

