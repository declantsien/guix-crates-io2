(define-module (crates-io se ri serial_frame) #:use-module (crates-io))

(define-public crate-serial_frame-0.1.0 (c (n "serial_frame") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "1hzdv38akc6589p9f8byn5wlw5mhz6y5jbs62q7qjlsx0nsv093a")))

(define-public crate-serial_frame-0.2.0 (c (n "serial_frame") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "06z0fky50wypkjvcmnnbsj890gch0hzjqq7y5cdbhaj6jjjnlyhq")))

(define-public crate-serial_frame-0.2.1 (c (n "serial_frame") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "0wq9ifhjncqb6kk42c0yn0z1kfi1gcs7q2nxmjrqw7jvjlmr7rqg")))

(define-public crate-serial_frame-0.2.2 (c (n "serial_frame") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "0b24j6x7qyf7np0g28ha1qwpnkpg9b7wg1ij4byvspdqb19ivf9s")))

(define-public crate-serial_frame-0.3.0 (c (n "serial_frame") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 2)))) (h "0xfdx1dqhb878hv3zaa2bnwi5s5nz48w4p06g02s1x0krmc15jv4")))

