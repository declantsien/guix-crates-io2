(define-module (crates-io se ri serialstudio) #:use-module (crates-io))

(define-public crate-serialstudio-0.1.0 (c (n "serialstudio") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1mima5ih0dvhvxsxmizhn82c55hi5vzr04751b1lmj48ybadhg6j")))

(define-public crate-serialstudio-0.1.1 (c (n "serialstudio") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0i0qpl90q3msvamag6a0iwlwhlnlf4vf9xn4cmbcw494irxz5x9y")))

