(define-module (crates-io se ri seripack) #:use-module (crates-io))

(define-public crate-seripack-0.1.0 (c (n "seripack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "blosc") (r "^0.1.3") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.1") (d #t) (k 0)) (d (n "seripack_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1p5z8a7g5ai4csxv93i1hn82r7wblp6y7c3vs86xsdr18j9vngq5")))

