(define-module (crates-io se ri serialize-to-javascript) #:use-module (crates-io))

(define-public crate-serialize-to-javascript-0.1.0 (c (n "serialize-to-javascript") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serialize-to-javascript-impl") (r "=0.1.0") (d #t) (k 0)))) (h "10lvnwfvlcfjyaif2xhyrzhgy2v26hsq8vd77ghsb8iwfqgai1li") (r "1.31")))

(define-public crate-serialize-to-javascript-0.1.1 (c (n "serialize-to-javascript") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serialize-to-javascript-impl") (r "=0.1.1") (d #t) (k 0)))) (h "1yyacsd48w7qkfzjn13sba2sf1l2hjpxw7qm521dk0ba7cnkz0n9") (r "1.31")))

