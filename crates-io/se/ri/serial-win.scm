(define-module (crates-io se ri serial-win) #:use-module (crates-io))

(define-public crate-serial-win-0.0.1 (c (n "serial-win") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1cxmsbn0d1v5wixwhp374z3j821ckx4511y1kkas52cg15lyp3p8")))

(define-public crate-serial-win-0.0.3 (c (n "serial-win") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0gdw1230dx1zjdcc6yvsrr574ygq1l597yp9yskxl6yckywcg7n1")))

(define-public crate-serial-win-0.1.0 (c (n "serial-win") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fcqwy1a1v7lz9zykflpl6rfmy8c50kbqm5napmmv5lmrhic0rka")))

(define-public crate-serial-win-0.1.1 (c (n "serial-win") (v "0.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1was0mjmdcz69yjwdp597v5ifhmdx0lc0w1cjxb0m4f5mv7wq5v1")))

(define-public crate-serial-win-0.2.0 (c (n "serial-win") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0swnngzwinn6pj5prbfaza0v821hzzl3icjxvycgbs0jc9s2whbk")))

