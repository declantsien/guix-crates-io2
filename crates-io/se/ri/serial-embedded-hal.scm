(define-module (crates-io se ri serial-embedded-hal) #:use-module (crates-io))

(define-public crate-serial-embedded-hal-0.1.0 (c (n "serial-embedded-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0sf52plflx0m5zn20n3fa3xaidg65hsfm92364293hrxw3r4qcis")))

(define-public crate-serial-embedded-hal-0.1.1 (c (n "serial-embedded-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "18kgm3v52qnd63py614cz3lsniwrsmb2p2hdm15kw19vkyiqkhk3")))

(define-public crate-serial-embedded-hal-0.1.2 (c (n "serial-embedded-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "1k5ijcp3avzsv4fbk39dqjqkjh4ghx3slqj2mala5xjzhz1ssz1a")))

