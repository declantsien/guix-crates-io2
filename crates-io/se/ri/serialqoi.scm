(define-module (crates-io se ri serialqoi) #:use-module (crates-io))

(define-public crate-serialqoi-0.1.7 (c (n "serialqoi") (v "0.1.7") (h "1i6gcvl1cqmdrrxd6mxgwd5ar6cwi33rb94fm4am5scpjn8r2xs6") (y #t)))

(define-public crate-serialqoi-0.2.0 (c (n "serialqoi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0l7pl4phbc0666yba749lyn739k4jarsfy87y84k99b7yf72l609")))

