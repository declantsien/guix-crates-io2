(define-module (crates-io se st sestring) #:use-module (crates-io))

(define-public crate-sestring-0.1.0 (c (n "sestring") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10cq0k4bn6rk1disizwhfbncs5x9d2qamcgsl876pv8l3zi6wac6")))

(define-public crate-sestring-0.1.1 (c (n "sestring") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rhxbjs85rw41n62wjiapsp1j23nqxm6ag9bds5wq7xw8hf6wgp7")))

(define-public crate-sestring-0.1.2 (c (n "sestring") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12b9mhlfif5a2fk69is49zs9lb0fjy5dwbv04w5l0wdnfnc06w4a")))

(define-public crate-sestring-0.2.0 (c (n "sestring") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xi36l5ar8v7pn9gv54vfgpv6s4mq3bcs1z4wasxxljw2gcn06y0")))

(define-public crate-sestring-0.3.0 (c (n "sestring") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lzylww7wynbii0yfic5cgb5idridg6l9mqgvyn5fdck53rx748g")))

