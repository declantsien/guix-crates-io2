(define-module (crates-io se cl seclip) #:use-module (crates-io))

(define-public crate-seclip-1.0.0 (c (n "seclip") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "05myfz6lvpcl16l7z57in6k7ydl9dqwq84ym7rrs81l8xjlznspj")))

