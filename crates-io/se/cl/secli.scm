(define-module (crates-io se cl secli) #:use-module (crates-io))

(define-public crate-secli-0.1.0 (c (n "secli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("cargo" "std" "color"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0mwvzyc40qki1p0r83mzbxp3sl33a3ggv7b624hm7flrdr1g7ry1")))

(define-public crate-secli-0.2.0 (c (n "secli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("cargo" "std" "color"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1brzvan8rr70dz2a9w87abwrpnl26pz99m7mnny4x58mn4wc584n")))

(define-public crate-secli-1.0.0 (c (n "secli") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("cargo" "std" "color"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0pi0fa03qw2hx1p34vaf3nq8nga6sv4yc75904bvz47iy2lzajj6")))

