(define-module (crates-io se qc seqcol_rs) #:use-module (crates-io))

(define-public crate-seqcol_rs-0.1.0 (c (n "seqcol_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "needletail") (r "^0.5.1") (d #t) (k 0)) (d (n "noodles-sam") (r "^0.54.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order" "indexmap"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (f (quote ("asm" "asm-aarch64"))) (d #t) (k 0)))) (h "0b2ksq47xkjn28jg4h3xhp3q36a4s2jcb7ykrcngx40l8k60gw5v")))

(define-public crate-seqcol_rs-0.1.1 (c (n "seqcol_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "needletail") (r "^0.5.1") (d #t) (k 0)) (d (n "noodles-sam") (r "^0.54.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order" "indexmap"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (f (quote ("asm" "asm-aarch64"))) (d #t) (k 0)))) (h "0hp4gs3xibdh7xx8z5p0mlk1lly4ix5nz8y2gxax8pxwj15z921b")))

