(define-module (crates-io se rd serde_closure_derive) #:use-module (crates-io))

(define-public crate-serde_closure_derive-0.2.0 (c (n "serde_closure_derive") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11n8ygimkc9a8fvcc5cn5y4kjfqdf6lyyd2na2lajblnr64cihmv") (f (quote (("assert-hack"))))))

(define-public crate-serde_closure_derive-0.2.1 (c (n "serde_closure_derive") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1msjhqiqkf4b2rp6ni9jzf6rr5wlka8cl8hbj72p6yi404cgh2g3") (f (quote (("assert-hack"))))))

(define-public crate-serde_closure_derive-0.2.2 (c (n "serde_closure_derive") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1rpi3xyid6madndzas3idk4n5s1xm0fiyb76b05wcxys59h8pv9y") (f (quote (("assert-hack"))))))

(define-public crate-serde_closure_derive-0.2.3 (c (n "serde_closure_derive") (v "0.2.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0i2yb643bc73v3d6vl86nggzddp5hp0c5qjqn1v0cgn2irrgrpxd") (f (quote (("assert-hack"))))))

(define-public crate-serde_closure_derive-0.2.4 (c (n "serde_closure_derive") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0add49ddjs9in8z8bmpzbyibplbj8isgh8a823rn8s0qws7vfwr8")))

(define-public crate-serde_closure_derive-0.2.5 (c (n "serde_closure_derive") (v "0.2.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1af05f33c2cd5arp1k8zrz7gnls8dfi3mxps4asxzdmxp015nf08")))

(define-public crate-serde_closure_derive-0.2.6 (c (n "serde_closure_derive") (v "0.2.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "07pqjgc5d5rssvxdfqb4mm4fi1fxyqj1wf2rl3g28yb5n7s9xbpq")))

(define-public crate-serde_closure_derive-0.2.7 (c (n "serde_closure_derive") (v "0.2.7") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "10m1v9635fq0g1x8rd7y1lnm2685iyd7mv0vdnvknqdf4qrxnhic")))

(define-public crate-serde_closure_derive-0.2.8 (c (n "serde_closure_derive") (v "0.2.8") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "04zlvc8gb5lvckzsgr89f0zqn301xjdnqiivzg7b54wl42rawyfl")))

(define-public crate-serde_closure_derive-0.2.9 (c (n "serde_closure_derive") (v "0.2.9") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "099vfhjf43nwccnwd2hvs388gl791fd9csf1fcjrmvx7f5l1m9ki")))

(define-public crate-serde_closure_derive-0.2.10 (c (n "serde_closure_derive") (v "0.2.10") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0fxv7scahmqsaa5jqhhfmx0a2isiv8hmsv9wmqij4li19a8sxb9d")))

(define-public crate-serde_closure_derive-0.2.11 (c (n "serde_closure_derive") (v "0.2.11") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0n8kbkjdcxvsg7hbf4mcznfz4zcsmsjznqlq9m31zi7204k6z04v")))

(define-public crate-serde_closure_derive-0.2.12 (c (n "serde_closure_derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0z27jqi20xqm3x038fvp6vwkick4ywxrixpidny6jxivb82v5z0f")))

(define-public crate-serde_closure_derive-0.2.13 (c (n "serde_closure_derive") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "093gmmvi9z5c22189r59in2fyi45sacf0slb7cwpq1r7b7n7rljz")))

(define-public crate-serde_closure_derive-0.2.14 (c (n "serde_closure_derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.1") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "1w113nmsky4mmc687vk74907kbhl6h68yavfnf90wjna43zrmyms")))

(define-public crate-serde_closure_derive-0.3.0 (c (n "serde_closure_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "1jjkk6dc4dk6vk0fpg9dpdwmafm890r55svz94f1yy3h4njzglwp")))

(define-public crate-serde_closure_derive-0.3.1 (c (n "serde_closure_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "070n30dx1dwib7ln9k03m0g6rr3lfnb231pvf292m12gdni1747n")))

(define-public crate-serde_closure_derive-0.3.2 (c (n "serde_closure_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "0z7bhi7pkpvddkjm51a38b05jgz1h48vznsrj1ln31fbbsp6j5g5")))

(define-public crate-serde_closure_derive-0.3.3 (c (n "serde_closure_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "0ilgl8fgz0nqq1y60xa0lwil3qs50iayw2csd93avjmm2bbb8sqs")))

