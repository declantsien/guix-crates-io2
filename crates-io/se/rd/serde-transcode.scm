(define-module (crates-io se rd serde-transcode) #:use-module (crates-io))

(define-public crate-serde-transcode-0.1.0 (c (n "serde-transcode") (v "0.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.4") (d #t) (k 2)))) (h "17c6fdprw6j6l00zqbr4sl4j2qri7whccc88y4q7dblsri7wy45g")))

(define-public crate-serde-transcode-0.1.1 (c (n "serde-transcode") (v "0.1.1") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.4") (d #t) (k 2)))) (h "1g9rf5ccvapfm3ms5mkqni41cva3vrlv9abwbbgxyw4b4r7aqpkj")))

(define-public crate-serde-transcode-0.2.0 (c (n "serde-transcode") (v "0.2.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0jw8i2wvv21qif4hl5wwrx5p42d4wrgi3nr8g9z10dfidlmxqrmg")))

(define-public crate-serde-transcode-0.2.1 (c (n "serde-transcode") (v "0.2.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "1dll8j5sz2hx89ps1g9viyz8pl0vvrnn05n6d66npx8lnj35jm7r")))

(define-public crate-serde-transcode-1.0.0 (c (n "serde-transcode") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1k2jzdc6iilds4qfr3j2xfp189j7fpp0nv4qng8i6lzh6nn7z6vl")))

(define-public crate-serde-transcode-1.0.1 (c (n "serde-transcode") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13jmh5600cbkv2rbgackpv2nh1qysywkmwmwsimydsq0pxikdksy")))

(define-public crate-serde-transcode-1.1.0 (c (n "serde-transcode") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "072f3lw79yaymsf7lf1zvh0x771v20x53jwmv58jvkpqzh6qyllp")))

(define-public crate-serde-transcode-1.1.1 (c (n "serde-transcode") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qlh34kw4knbs83xy6x0wwbb71pbiivcw6swpy2nxfx5q8jhw32r")))

