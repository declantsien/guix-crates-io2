(define-module (crates-io se rd serde_newtype) #:use-module (crates-io))

(define-public crate-serde_newtype-0.1.0 (c (n "serde_newtype") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_with") (r "^3") (d #t) (k 2)))) (h "0q07572qv2c2a69ll5kghv2ypjq4y6ngvrxvy9nnhz6y6y886la1")))

(define-public crate-serde_newtype-0.1.1 (c (n "serde_newtype") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_with") (r "^3") (d #t) (k 2)))) (h "0q0zkn0xd9ahxr8xrlmp29d9c2x82ddzyqsq5brmay46c9zni9v6")))

