(define-module (crates-io se rd serde_skip) #:use-module (crates-io))

(define-public crate-serde_skip-0.1.0 (c (n "serde_skip") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a6qcb4gyp81l9h7kyffmzwhknaf868mv5fvr79n46myiqarjwky")))

(define-public crate-serde_skip-0.1.1 (c (n "serde_skip") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 2)))) (h "15zj3n83igz3p7i84lnk2mz2cxxq8ghl53676kc3a68zsiqbx6k8") (y #t)))

