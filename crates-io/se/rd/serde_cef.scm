(define-module (crates-io se rd serde_cef) #:use-module (crates-io))

(define-public crate-serde_cef-0.1.0 (c (n "serde_cef") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde-value-flatten") (r "^0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ad8ni03l3vkj6lggys3l28zp5agmvc4kkdj1m5fz0fgq0nn8fxm") (f (quote (("ovh-ldp" "serde-value-flatten/ovh-ldp"))))))

