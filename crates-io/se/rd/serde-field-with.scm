(define-module (crates-io se rd serde-field-with) #:use-module (crates-io))

(define-public crate-serde-field-with-0.1.0 (c (n "serde-field-with") (v "0.1.0") (d (list (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19lq5smb4lccfh1kz7z6abzz9zgk3bmjqyddfzj766g2cc38w2m3") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-serde-field-with-0.1.1 (c (n "serde-field-with") (v "0.1.1") (d (list (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a6yi21361pigg1md2rk3cy9p5idsbwxin8spar442y3kf71mg3h") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-field-with-0.1.2 (c (n "serde-field-with") (v "0.1.2") (d (list (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r1b62rxcw7xlc0sxm88k6mnd8ldbsj5f2imq2n69byhn4njwjiv") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-field-with-0.2.0 (c (n "serde-field-with") (v "0.2.0") (d (list (d (n "chrono-tz") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0nsb21ihpzawmarpqx2wmf9psqnvayd98h20aicbdbbnx1y1r3h7") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

