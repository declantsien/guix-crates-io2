(define-module (crates-io se rd serde-pyobject) #:use-module (crates-io))

(define-public crate-serde-pyobject-0.1.0 (c (n "serde-pyobject") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)))) (h "1j0jfs8cg21cmxhyzgpr3lbka5sg20pwan1ky1wv5i41q77s6i22")))

(define-public crate-serde-pyobject-0.2.0 (c (n "serde-pyobject") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1xddd7q63pc0dji8zhmkcrwpb548dyflm7l61abjxy3dffg3nh66")))

(define-public crate-serde-pyobject-0.2.1 (c (n "serde-pyobject") (v "0.2.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "15p46l8x68ydgv2v44s4yd2dn52aswa873x246777mqd5xmbnq2i")))

(define-public crate-serde-pyobject-0.3.0 (c (n "serde-pyobject") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "pyo3") (r "^0.21.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.0") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1viza340qxf87f7dv5wvzbwwd9x3lh05pmb881vsbsyqfzmfgzvh")))

