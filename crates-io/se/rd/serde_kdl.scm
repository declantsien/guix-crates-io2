(define-module (crates-io se rd serde_kdl) #:use-module (crates-io))

(define-public crate-serde_kdl-0.1.0 (c (n "serde_kdl") (v "0.1.0") (d (list (d (n "kdl") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "12fk0iffnj02s7sp8ygl4bb88m4bah64x9l9vkxj1dr4bn16nim3") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

