(define-module (crates-io se rd serde_fancy_regex) #:use-module (crates-io))

(define-public crate-serde_fancy_regex-2.0.0 (c (n "serde_fancy_regex") (v "2.0.0") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1s13x702s23l0yj50h3902jkzfnkg2aw2vadc7zqw12ryqbfgr6j")))

