(define-module (crates-io se rd serde-scale) #:use-module (crates-io))

(define-public crate-serde-scale-0.1.0 (c (n "serde-scale") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (k 0)))) (h "1k1n28y95263wfsw4vhzrpjkg44vlnc47r65xjz7c2jic0prv4sa") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-scale-0.2.0 (c (n "serde-scale") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (k 0)))) (h "0m99ccjxhy7dshwmrr9zzhzy0wjls67j1qgj0i8vz0yq62jhzx4m") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-scale-0.2.1 (c (n "serde-scale") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (k 0)))) (h "0n7sibnflmzlywh5hzqby56g4q6ndrxwp2krsx2j8h43jy6kw2ma") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-scale-0.2.2 (c (n "serde-scale") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (k 0)))) (h "1a63gryz6d3gcdv8xzczcq1ph3s7i585av2qzx1ylkknxrwnfblg") (f (quote (("std" "alloc" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

