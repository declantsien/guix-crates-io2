(define-module (crates-io se rd serde_apply) #:use-module (crates-io))

(define-public crate-serde_apply-0.1.0 (c (n "serde_apply") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_apply_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 2)))) (h "1hydbc08z0n4n3ibmygvchpmakbqnv5gv7kr8icbciddas0wwyg3")))

