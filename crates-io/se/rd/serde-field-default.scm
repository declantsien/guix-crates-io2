(define-module (crates-io se rd serde-field-default) #:use-module (crates-io))

(define-public crate-serde-field-default-0.1.0 (c (n "serde-field-default") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 2)) (d (n "chrono-tz") (r "^0.6") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1067bvnshnxicb6mkx375qi1rb6ncrd3w0714gj7kw7y88vs2ynh") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-serde-field-default-0.1.1 (c (n "serde-field-default") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 2)) (d (n "chrono-tz") (r "^0.6") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y91rx6hbbqis5ysfvgnadvkymc2vdzjs4jxsva8cl50y8s2bkbr") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-serde-field-default-0.2.0 (c (n "serde-field-default") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0g59gasc8ryi0xfw91swx2j6gfs7mlm77kj01nihymsxcv5x304z") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

