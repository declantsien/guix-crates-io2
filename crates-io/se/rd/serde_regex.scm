(define-module (crates-io se rd serde_regex) #:use-module (crates-io))

(define-public crate-serde_regex-0.1.0 (c (n "serde_regex") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0czg2g01zkg4vbb2y3x6nc2an1qd9wp30qmaf6wp1c0af4h3ymgl")))

(define-public crate-serde_regex-0.2.0 (c (n "serde_regex") (v "0.2.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0x14kyx5zwpsnqkcs5facxbabg2q59bf847yi69p2rlym2kpnb2q")))

(define-public crate-serde_regex-0.3.0 (c (n "serde_regex") (v "0.3.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "11s7bd84zigm9nzbsffxcxz3alcvwwsflxb8c4ycm36wgbfnf8ip")))

(define-public crate-serde_regex-0.3.1 (c (n "serde_regex") (v "0.3.1") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1ijy3y36whf0cllfcn5hdmvxhgjdakia3jsmifqx3wl1k9lqa4rd")))

(define-public crate-serde_regex-0.4.0 (c (n "serde_regex") (v "0.4.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "02djqyvq08pmsq7lbgp42kabwpvadjngd1rj6dgcij3k5781ppzn")))

(define-public crate-serde_regex-1.0.0 (c (n "serde_regex") (v "1.0.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1jvsqmjnhl631v7wz1r8kr58y9pn00qxjhig6dvsvmfpza4zjjgz")))

(define-public crate-serde_regex-1.1.0 (c (n "serde_regex") (v "1.1.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1pxsnxb8c198szghk1hvzvhva36w2q5zs70hqkmdf5d89qd6y4x8")))

