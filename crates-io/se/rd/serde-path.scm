(define-module (crates-io se rd serde-path) #:use-module (crates-io))

(define-public crate-serde-path-0.1.0 (c (n "serde-path") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "109207z2m14cyysnwyc94zc97gsg00rxwgc8xsnh95l7zvb73l63")))

