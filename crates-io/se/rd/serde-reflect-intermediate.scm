(define-module (crates-io se rd serde-reflect-intermediate) #:use-module (crates-io))

(define-public crate-serde-reflect-intermediate-1.1.0 (c (n "serde-reflect-intermediate") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)))) (h "1fppa8n9lrcna07hks1y1mhir9d79wgjsxzjbiwm1bc1p5nf7h26")))

(define-public crate-serde-reflect-intermediate-1.2.0 (c (n "serde-reflect-intermediate") (v "1.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.2") (d #t) (k 0)) (d (n "serde-reflect-intermediate-derive") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1wvml44gkk97a1fwv9hhmp5j8mnhk05krq6vn92chpaz8224c5qc") (f (quote (("derive" "serde-reflect-intermediate-derive"))))))

(define-public crate-serde-reflect-intermediate-1.2.2 (c (n "serde-reflect-intermediate") (v "1.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.2") (d #t) (k 0)) (d (n "serde-reflect-intermediate-derive") (r "^1.2") (o #t) (d #t) (k 0)))) (h "02gsmrk7i5pqaz5j5pbnj7i13dlsnf1rq6jifhw4nlzkgjkhq30c") (f (quote (("derive" "serde-reflect-intermediate-derive"))))))

(define-public crate-serde-reflect-intermediate-1.2.3 (c (n "serde-reflect-intermediate") (v "1.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.2") (d #t) (k 0)) (d (n "serde-reflect-intermediate-derive") (r "^1.2") (o #t) (d #t) (k 0)))) (h "16mhfk8z4ldhd5n9qg57s3imdrksaxn484qwaa27nakn5vpag6zm") (f (quote (("derive" "serde-reflect-intermediate-derive"))))))

(define-public crate-serde-reflect-intermediate-1.2.4 (c (n "serde-reflect-intermediate") (v "1.2.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.2") (d #t) (k 0)) (d (n "serde-reflect-intermediate-derive") (r "^1.2") (o #t) (d #t) (k 0)))) (h "04crn194x2mcl2ysfzprrczb4dap2s059krfjc5nr9qzh18adrx6") (f (quote (("derive" "serde-reflect-intermediate-derive"))))))

