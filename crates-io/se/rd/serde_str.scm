(define-module (crates-io se rd serde_str) #:use-module (crates-io))

(define-public crate-serde_str-0.1.0 (c (n "serde_str") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0j3as4drch7p0kck46yk27q4w6kn60h5ipd9zvlm3v6zp47qavm2")))

