(define-module (crates-io se rd serde_transit) #:use-module (crates-io))

(define-public crate-serde_transit-0.1.0 (c (n "serde_transit") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xq2vgxajkblwrmgiv64dljpw0kyy6p3yp3cn29x9nsyc4nf7p76")))

