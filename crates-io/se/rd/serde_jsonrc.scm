(define-module (crates-io se rd serde_jsonrc) #:use-module (crates-io))

(define-public crate-serde_jsonrc-0.1.0 (c (n "serde_jsonrc") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1a5q0ba7jhgfl63l16plgl7sx1k58l2caxf7z2j5l677rh5yk4dm") (f (quote (("raw_value") ("preserve_order" "indexmap") ("default") ("arbitrary_precision"))))))

