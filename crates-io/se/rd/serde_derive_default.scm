(define-module (crates-io se rd serde_derive_default) #:use-module (crates-io))

(define-public crate-serde_derive_default-0.1.0 (c (n "serde_derive_default") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.84") (d #t) (k 2)))) (h "0j5ig9p56j71h1ran58k0wj9jvsldb86xkc06xx5i7ayxk58b559")))

(define-public crate-serde_derive_default-0.1.1 (c (n "serde_derive_default") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "19yqmjcdqkk6srsg0xll3gppfa6izqkk2jdkqbv7n4w758n55cmg")))

