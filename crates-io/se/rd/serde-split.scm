(define-module (crates-io se rd serde-split) #:use-module (crates-io))

(define-public crate-serde-split-0.1.0 (c (n "serde-split") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "05zb9l051zcz1wwn4i7425wxkqxgda07qpzf6537vxqcckk48gzh")))

(define-public crate-serde-split-0.1.1 (c (n "serde-split") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1p1b6nw2wq9vyz0lkwwazy65pxq3p5m3vjsmq5fl9478gyr2ybwq")))

(define-public crate-serde-split-0.1.2 (c (n "serde-split") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1bq4wyn9xyqyx3l9wbnhs1j8dzaqb7k01scvanl17xv8d2sljz3p")))

(define-public crate-serde-split-0.1.3 (c (n "serde-split") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0xbkdz6wg6vrcj7cf81yl3lhkifzvblldjpmvcd4sdpnkb1gyskn")))

