(define-module (crates-io se rd serde-bench) #:use-module (crates-io))

(define-public crate-serde-bench-0.0.1 (c (n "serde-bench") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "1lx636499qryhlpnlm536qpzii3r5cp0xq2gs6b6ykyp0yslrw29")))

(define-public crate-serde-bench-0.0.2 (c (n "serde-bench") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0hfjqb8y9lz4lzb1nj39c34xhiyqrb6bgqdl6gqxxazr2a1rzz18")))

(define-public crate-serde-bench-0.0.3 (c (n "serde-bench") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1dqvfh5w5mnj8nifmspsssyi7zbdgaar0li1x20x4qc7mnmjgsqy")))

(define-public crate-serde-bench-0.0.4 (c (n "serde-bench") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "084pd3v4ialr03a61hjmqrdi1b5mb2ssrzm0kxhi05r96bs1p793")))

(define-public crate-serde-bench-0.0.5 (c (n "serde-bench") (v "0.0.5") (d (list (d (n "bincode") (r "^1.0.0-alpha2") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1bgcl6q0j2vsgq8gnlwwa4c2qc9r4cmbf57idasz2hkq1bn6a11a")))

(define-public crate-serde-bench-0.0.6 (c (n "serde-bench") (v "0.0.6") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0q2qjk74ivh4wyijl15g004zx85nzi62r47304msjclij4qw3lw6")))

(define-public crate-serde-bench-0.0.7 (c (n "serde-bench") (v "0.0.7") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1za3lbp8y7g9n35lc9yl9kyl64d4k5id4cvfc4jsralzwy3xlcyp")))

(define-public crate-serde-bench-0.0.8 (c (n "serde-bench") (v "0.0.8") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "01gpslqz16nvwrqsx167195gyhadpwnrhlyjsizm97g7bdkbr52z") (r "1.34")))

(define-public crate-serde-bench-0.0.9 (c (n "serde-bench") (v "0.0.9") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1hglv9qrhh43jcfz8xpm8b5hxdny00vryc0yir52dnk29m5la0bz") (r "1.34")))

(define-public crate-serde-bench-0.0.10 (c (n "serde-bench") (v "0.0.10") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0phapsi4qgafj64dy9ww1sw1zs0br2v5hj0l34hr1hvlxmvkm9zb") (r "1.34")))

(define-public crate-serde-bench-0.0.11 (c (n "serde-bench") (v "0.0.11") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "078jz56wy7mpmywjgh92gw869b7l4n920silcxc4jngh2krjgc2p") (r "1.56")))

