(define-module (crates-io se rd serde-java-properties) #:use-module (crates-io))

(define-public crate-serde-java-properties-0.1.0 (c (n "serde-java-properties") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "java-properties") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1k0gg0bnmwb4kzixkmqc0kxvjgdav83fmjpn200x85zp0ddqfz02")))

(define-public crate-serde-java-properties-0.1.1 (c (n "serde-java-properties") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "java-properties") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0i0s48rsxgr72j9klfasm37zlbmlz33gf6pcix2fwcjnhfv01bd7")))

(define-public crate-serde-java-properties-0.2.0 (c (n "serde-java-properties") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.34") (d #t) (k 0)) (d (n "java-properties") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1isbbp4d8aw6yw71dm6l43lxj4xkarwyzj5cm3w8wmrlp62xpddq")))

