(define-module (crates-io se rd serde-pointer) #:use-module (crates-io))

(define-public crate-serde-pointer-0.1.0 (c (n "serde-pointer") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "serde-value") (r "^0.6.0") (d #t) (k 0)))) (h "0fmgimxan1lhywa4xxc7kdd6d0cy1z8jyx06lw6ly9nczp84xzbd") (y #t)))

(define-public crate-serde-pointer-0.1.1 (c (n "serde-pointer") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "serde-value") (r "^0.6.0") (d #t) (k 0)))) (h "177b0vp8g0craw6bj3r74mz02lka31a9nahqaa50353rbgm8r95j")))

(define-public crate-serde-pointer-0.2.0 (c (n "serde-pointer") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "serde-value") (r "^0.6.0") (d #t) (k 0)))) (h "0qy85pxzjq17cn37ri8lhaa7ji3yck2f7xzmhy485qpz3adx83nr")))

