(define-module (crates-io se rd serde_xml) #:use-module (crates-io))

(define-public crate-serde_xml-0.6.0 (c (n "serde_xml") (v "0.6.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.5") (d #t) (k 2)))) (h "0bhb89aq71imi3jqwwjf9nb8dfahh186y2h895xbiy9s5iwawbg1")))

(define-public crate-serde_xml-0.6.1 (c (n "serde_xml") (v "0.6.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 2)))) (h "19sd8ljkb724pva6dw6wbbaqjrbmypkcx223yww66g5ag3cjna96")))

(define-public crate-serde_xml-0.6.2 (c (n "serde_xml") (v "0.6.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 2)))) (h "1s1dq30i1gdr3sqm40vqz5xmg6d1z5milk077cqpvl1mxsm7xvy5")))

(define-public crate-serde_xml-0.6.3 (c (n "serde_xml") (v "0.6.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 2)))) (h "0ld3kr3s9r8q2kgphfbf4rdqy1cg1spkwqz1znwgx1qfqxwqx8n1")))

(define-public crate-serde_xml-0.7.0 (c (n "serde_xml") (v "0.7.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 2)))) (h "06fjhbnwgbczwyknngmg6kp8dbbj6r67glqbqb38455g0fqh8273")))

(define-public crate-serde_xml-0.7.1 (c (n "serde_xml") (v "0.7.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 2)))) (h "1rmcljxc7zacn2pkvzdyqfal0g08x6cgpl62x02phhbn9v1b5mdh")))

(define-public crate-serde_xml-0.8.0 (c (n "serde_xml") (v "0.8.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)))) (h "1zrcw6s25l38bis0jajx3f25z84kw3bzrv1dqfcfaajrdw3n6l9z")))

(define-public crate-serde_xml-0.8.1 (c (n "serde_xml") (v "0.8.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)))) (h "01im6banpx3wg9hsxnfcz87bqd60hqc33df5akgsskc1a0vjah17")))

(define-public crate-serde_xml-0.9.0 (c (n "serde_xml") (v "0.9.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)))) (h "0zz44iwrppmcdp3si7zfz1crivj6izp7py0wxrh9bq2s7xpgjkfq")))

(define-public crate-serde_xml-0.9.1 (c (n "serde_xml") (v "0.9.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)))) (h "15dsy3g1bn7v9wdm39fpzy4d28mflrv711kag9mxla08dd96wd2n")))

