(define-module (crates-io se rd serde_derive_state) #:use-module (crates-io))

(define-public crate-serde_derive_state-0.1.0 (c (n "serde_derive_state") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_state_internals") (r "= 0.1.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1nwx97a024mnv2x9ssl9991iy8fkppg875kr78lxw2miva8xqx3y")))

(define-public crate-serde_derive_state-0.2.0 (c (n "serde_derive_state") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_state_internals") (r "= 0.2.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1mv1cx6fm3zp26d9yb2013dy2bp73g4ppvpg9d66hp1c4gdkkvlh")))

(define-public crate-serde_derive_state-0.3.0 (c (n "serde_derive_state") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_state_internals") (r "= 0.3.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "17hqj6spmi1zxnkglzsxnls0r0sjsf3ycckkmh8bzcj08gbj8jhp")))

(define-public crate-serde_derive_state-0.4.0 (c (n "serde_derive_state") (v "0.4.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_state_internals") (r "= 0.4.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0bnis86w0h6pxkpv76lvyrccl9m3crbjzafip0jqgzxrw22fm466")))

(define-public crate-serde_derive_state-0.4.1 (c (n "serde_derive_state") (v "0.4.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_state_internals") (r "= 0.4.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1yj8rsnlf23dllfiql6fs9sknj6p3myap094i3q6wal16cqvapf3")))

(define-public crate-serde_derive_state-0.4.2 (c (n "serde_derive_state") (v "0.4.2") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_state_internals") (r "= 0.4.2") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1yx5nbq4ik7qia3s2v4nqajkb2ncbdhxb8kancfzsy0ck7k2i8m7")))

(define-public crate-serde_derive_state-0.4.3 (c (n "serde_derive_state") (v "0.4.3") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_state_internals") (r "= 0.4.3") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1i68m2fzhy2rzzvk0w4j4mcan6k7l8ywrv6yywgmfz596v7b0mgl")))

(define-public crate-serde_derive_state-0.4.4 (c (n "serde_derive_state") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "0r22i0cnrlai5fximyygzzi4j0hxi3pgw174s162qcda7yy4x8dx") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.5 (c (n "serde_derive_state") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1fk0zaa9nf3ah11n36xz2ns8fryblrr77kr3q8rapjdirs6m3bpf") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.6 (c (n "serde_derive_state") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1drcgshm5q5fkn1xv206wkd8vp57vjp44bfpwnr6zwrr55y3zdz1") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.7 (c (n "serde_derive_state") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "07x5y53c8z47b5r5i0iz087bg2r07n91niiqg2nj5wk38sxr2hfc") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.8 (c (n "serde_derive_state") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "15q2hslz7grlkcb99rf7dmj2zwl73vhwqv0p91mmifb7rs3y0xpp") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.9 (c (n "serde_derive_state") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1gzk06gha8j9m7fzk3v3b2scq04bhkp92x2x0kqm6hyy2as1yh1a") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive_state-0.4.10 (c (n "serde_derive_state") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "04lbbqr2fj612lrlmvqbpvkiw4l7f12pin51hm731igpljlixnjb") (f (quote (("deserialize_in_place") ("default"))))))

