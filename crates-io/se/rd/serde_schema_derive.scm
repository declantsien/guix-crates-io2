(define-module (crates-io se rd serde_schema_derive) #:use-module (crates-io))

(define-public crate-serde_schema_derive-0.0.1 (c (n "serde_schema_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.39") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.42") (d #t) (k 2)) (d (n "serde_derive_internals") (r "^0.23.1") (d #t) (k 0)) (d (n "serde_schema") (r "^0.0.1") (d #t) (k 2)) (d (n "syn") (r "^0.13.0") (d #t) (k 0)))) (h "1iq0xyi43h24hch7xb5p9rw2sb73mhkkbcdcf25l3y57wly6mxhg")))

