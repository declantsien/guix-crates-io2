(define-module (crates-io se rd serde_pipe) #:use-module (crates-io))

(define-public crate-serde_pipe-0.1.0 (c (n "serde_pipe") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "deploy-temp-fringe") (r "^1.2") (f (quote ("valgrind"))) (d #t) (k 0)) (d (n "either") (r "^1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0macayyd28p8ba3lp842z354crs69811qpb65k058ar0hgmwr6z2")))

(define-public crate-serde_pipe-0.1.1 (c (n "serde_pipe") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "fringe") (r "^1.2.4") (d #t) (k 0) (p "deploy-temp-fringe")) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04787awsalrnzb1rkizdkncl67vbwzlnlnljff64ci2injgbsl5z")))

(define-public crate-serde_pipe-0.1.2 (c (n "serde_pipe") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "fringe") (r "^1.2.4") (o #t) (d #t) (k 0) (p "deploy-temp-fringe")) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0jr9qjv0935mdsa074y6r3qx862bxb32h041vrxlhswkb76cmz7n") (y #t)))

(define-public crate-serde_pipe-0.1.3 (c (n "serde_pipe") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "fringe") (r "^1.2.4") (o #t) (d #t) (k 0) (p "deploy-temp-fringe")) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nfrxrh9c2qipchf3rg0jks8l7nl7750ia70zh8js18cw0p97naq")))

