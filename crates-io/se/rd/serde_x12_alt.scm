(define-module (crates-io se rd serde_x12_alt) #:use-module (crates-io))

(define-public crate-serde_x12_alt-0.1.0 (c (n "serde_x12_alt") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0clpvcv1cimibsp7dww36nr08v6bm9dxl68rxhfa6g6jjp9bqnkf")))

(define-public crate-serde_x12_alt-0.2.0 (c (n "serde_x12_alt") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1cjrn8cg4cm0hiz5ig1r7ic536gnimpckwr6h1bhcij5zhg2v8av")))

