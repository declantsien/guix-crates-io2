(define-module (crates-io se rd serde_piecewise_default) #:use-module (crates-io))

(define-public crate-serde_piecewise_default-0.1.0 (c (n "serde_piecewise_default") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.22") (f (quote ("stable"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_piecewise_default_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1xv6mjg8q7irbqqh0sa2535kik6jprdqh14nwv2ji1q3hrmh5ghd")))

(define-public crate-serde_piecewise_default-0.2.0 (c (n "serde_piecewise_default") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3.22") (f (quote ("stable"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_piecewise_default_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1c5l2z7crk2brq45q155hyhq56b23wg86p7jz0vx5v8synq486nr")))

