(define-module (crates-io se rd serde-tc-macro) #:use-module (crates-io))

(define-public crate-serde-tc-macro-0.1.0 (c (n "serde-tc-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "0yp1jbs7i5pch6lv93cxcsxf9bc3132clj1cpq4qmw2a9nd8f725")))

(define-public crate-serde-tc-macro-0.1.1 (c (n "serde-tc-macro") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "1xqyflmpjf6nqr7l62vp505nybqbl1xixasgdwvjlf1j544pwzk1")))

(define-public crate-serde-tc-macro-0.1.2 (c (n "serde-tc-macro") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "0q3ipmqma3iz1a4xsasw5aw0gxr4l0cc89m4hk8s5wp44yfidn52")))

(define-public crate-serde-tc-macro-0.2.0 (c (n "serde-tc-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "078gjh04xq6qbc1syb1zgrm6bfsvxzb62jdnyk8a89f752ax5f67")))

(define-public crate-serde-tc-macro-0.2.1 (c (n "serde-tc-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "1w0lwhwmadq33bhf8sn18v15clmwdz760bi2fvg11fmqxwiiprrx")))

(define-public crate-serde-tc-macro-0.2.2 (c (n "serde-tc-macro") (v "0.2.2") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "0ciy5ns3kiqbnq18dd1l4pfgdami1pamc657fr3cgbwsifcg18ir")))

(define-public crate-serde-tc-macro-0.2.3 (c (n "serde-tc-macro") (v "0.2.3") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "023rbj9zsap1gcap8fxzarqlpdarmi6lbxy639py8mxljf969ggb")))

(define-public crate-serde-tc-macro-0.4.0 (c (n "serde-tc-macro") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)))) (h "15lwqzarqjy0ay01h8454vhzpnx3yir6sz7q9fwh6nfg7y3axfjr")))

