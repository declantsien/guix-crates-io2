(define-module (crates-io se rd serde_dokechi) #:use-module (crates-io))

(define-public crate-serde_dokechi-0.1.0 (c (n "serde_dokechi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0ygfdr0gfb1md7kqylw6n24indqm37bkn7lbnv4ql8lziakr17x1")))

(define-public crate-serde_dokechi-0.1.1 (c (n "serde_dokechi") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0f944nw7lyghh03d2dykr36pcgpyhbdafjncis04j1grv4gp0zh3")))

