(define-module (crates-io se rd serde-lsp) #:use-module (crates-io))

(define-public crate-serde-lsp-0.0.0 (c (n "serde-lsp") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vsy8dj1ibhjj3b146sf8qj46x72vf6xx60k68yy8m4aq6017rz9")))

(define-public crate-serde-lsp-0.0.1 (c (n "serde-lsp") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0i2lbbnw9pczvzip79ksl33799d2q0jv2idmhamd6869zrfx7fpq")))

