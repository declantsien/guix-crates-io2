(define-module (crates-io se rd serde-byte-array) #:use-module (crates-io))

(define-public crate-serde-byte-array-0.1.0 (c (n "serde-byte-array") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 2)) (d (n "serde_test") (r "^1.0.152") (d #t) (k 2)))) (h "1b87p2358hi2zyzmw83fpivym1fhdjnnfa7047rll5i7x46zkkrg") (r "1.56")))

(define-public crate-serde-byte-array-0.1.1 (c (n "serde-byte-array") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 2)) (d (n "serde_test") (r "^1.0.152") (d #t) (k 2)))) (h "1bnyfrx4h3r9pbd0j25rp9kwvip5l2lna3ihbpm26r2h9z4xb1y5") (r "1.56")))

(define-public crate-serde-byte-array-0.1.2 (c (n "serde-byte-array") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 2)) (d (n "serde_test") (r "^1.0.152") (d #t) (k 2)))) (h "0hpp50mpq025c1sv75gxvnsbyijv4za97abgvf3vv3b4xpj3w8b3") (r "1.56")))

