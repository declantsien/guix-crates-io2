(define-module (crates-io se rd serde_iter) #:use-module (crates-io))

(define-public crate-serde_iter-0.1.0 (c (n "serde_iter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)))) (h "0wicnn4pflx177fb6ps7nrlih1xwjszv0k9id5awlxy4n4igngr9") (f (quote (("seq") ("map") ("default" "seq" "map"))))))

(define-public crate-serde_iter-0.1.1 (c (n "serde_iter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)))) (h "00iyvc16aqcminsigflkjvnhxkn5vqpwcmvdd2m837c11nfpbl6b") (f (quote (("seq") ("once") ("map") ("default" "seq" "map" "once"))))))

(define-public crate-serde_iter-0.2.0-alpha.1 (c (n "serde_iter") (v "0.2.0-alpha.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)))) (h "05ra6bd2ac1yylrw4cjjfybxm235a3qb9gv78mh86mnjwbwap3iy") (f (quote (("seq") ("once") ("map") ("default" "seq" "map" "once"))))))

