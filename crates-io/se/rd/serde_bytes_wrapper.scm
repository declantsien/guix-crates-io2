(define-module (crates-io se rd serde_bytes_wrapper) #:use-module (crates-io))

(define-public crate-serde_bytes_wrapper-0.1.0 (c (n "serde_bytes_wrapper") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-bytes-repr") (r "^0.1") (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pybh3g1rwsj3hy6zfmq29ivb3vk93229szykvn3cslyw567wb6d")))

