(define-module (crates-io se rd serde_bson) #:use-module (crates-io))

(define-public crate-serde_bson-0.0.1 (c (n "serde_bson") (v "0.0.1") (d (list (d (n "bson") (r "^1.2") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "181s2b50hmsv5v9wm00y9mfr8k3ny2p8smhhvgjfdmj31hh9qnpm")))

