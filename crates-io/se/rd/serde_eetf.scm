(define-module (crates-io se rd serde_eetf) #:use-module (crates-io))

(define-public crate-serde_eetf-0.1.0 (c (n "serde_eetf") (v "0.1.0") (d (list (d (n "eetf") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0lvnshqpcs52b4nrxhzv59ims7d8b750id4ndq8vvp1rhi7kzl35")))

(define-public crate-serde_eetf-0.2.0 (c (n "serde_eetf") (v "0.2.0") (d (list (d (n "eetf") (r "^0.4") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01lxi1vsn379nhxd5dhxsp08wp4j5ggybxxs009nssv4as95dvdn")))

