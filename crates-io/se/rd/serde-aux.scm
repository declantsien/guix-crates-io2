(define-module (crates-io se rd serde-aux) #:use-module (crates-io))

(define-public crate-serde-aux-0.1.0 (c (n "serde-aux") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bnr49k3p7xz06qr5wijdqfyw7nb5s6mzm01y73r8m4v8yk3hg0l")))

(define-public crate-serde-aux-0.1.1 (c (n "serde-aux") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0g9d1w1dqc76qixfzls5qvb8jh77r0mjqz9694pr7i25qiidszp7")))

(define-public crate-serde-aux-0.2.0 (c (n "serde-aux") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03hmf6ccibydz55qgjd4686c7lfz4bza1mqm94g4ymcjiy3947yb")))

(define-public crate-serde-aux-0.3.0 (c (n "serde-aux") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xil3y3855z7nlcp79v13599kgwfxjhfq0d30pzkf8fl41ynjk4y")))

(define-public crate-serde-aux-0.5.0 (c (n "serde-aux") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "196fnqlz6rw5a3m6jcjy7nysaajms4yfhajv1yghl1dg45bxk0c0") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-0.5.1 (c (n "serde-aux") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w3sl88l591jbg1pdrzw6w2aqv060qqn39mvx3arp7zcsnp8nvvs") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-0.5.2 (c (n "serde-aux") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pl19qihp0sg8p60f3sa6yrdqgicj8w1441fgfmbnhvl9gfxn8md") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-0.5.3 (c (n "serde-aux") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0893xfq67wdlrjzjm6y1iz6a6406zx3jz5c48rrav9arxi7dnj4q") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-0.6.0 (c (n "serde-aux") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ll82prnwdxk0x65snpqkqad7ab5bhb07c83vh0qfspvsm773imi") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-0.6.1 (c (n "serde-aux") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ln83p53cgqdhn44jmbyx9ag0wclsm68anqzkhqm9s019cyzal5f") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-1.0.1 (c (n "serde-aux") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wghjak0bqqy16s8fc2cpl2pcy6kh90vg1k9yrfb76957071ryy7") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-1.1.0 (c (n "serde-aux") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08jgg9q457zvgy9p75i39qfz08804hc83993b65lwmyiyg4jypwh") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-2.0.0 (c (n "serde-aux") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "1xmbdw9xb608lb5fa7idypw7k1ndxnkq6al9igzqyr9zqfm88426") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-2.1.0 (c (n "serde-aux") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "1syswansixfbqzgbww4i0y2206qk91spcwjxh680kl8vhr77c5is") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-2.1.1 (c (n "serde-aux") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "1gp87hjbl464cwsxlpaim9vmfg66qnlsmaw3wlqv79s6p00daqh1") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-2.2.0 (c (n "serde-aux") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "007fr8f06iw5bj407pk8ca70n63ba128l2lpixgdzbpbys1qrsvp") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-2.3.0 (c (n "serde-aux") (v "2.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "041q4hbmbh8cxvkzzqg4r8fxbh4fqmz5daccn8sf2p7lz0734z4h") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-3.0.0 (c (n "serde-aux") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "0fy4b1kmx5kf310w26qwd0v2maxyrrlsib5b2fgqqv9rbcncjyva") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-3.0.1 (c (n "serde-aux") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 2)))) (h "095dzxsk9zhj9m5yj4nyaiygn9w5sml5385ja9100vspkiwzkawk") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-3.1.0 (c (n "serde-aux") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.9") (d #t) (k 2)))) (h "1pp1slhsz63bbgc45sw70qbyvr4kbfr3wkw6z7rrbyjknqip59yh") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-3.2.0 (c (n "serde-aux") (v "3.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "1yzj5qbly7p0m21v9chsc0vyyc84y14b0lgwmbw0n4xcnq2a62vk") (f (quote (("default" "chrono")))) (y #t)))

(define-public crate-serde-aux-4.0.0 (c (n "serde-aux") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "1ig4srp2zdky7jcjz5d5h06i4l9ham4di6sbgagvya0c65d1m767") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-4.1.0 (c (n "serde-aux") (v "4.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "1zbl6p59c0zig8kkdcyx6wnd12z5m7p6mkmrhnsszw91rd432spb") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-4.1.1 (c (n "serde-aux") (v "4.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "0f3in0x70cv0jfsgjj7rrgjmis8k08yj1zk4p2nhfh69qb56j7yj") (f (quote (("default" "chrono")))) (y #t)))

(define-public crate-serde-aux-4.1.2 (c (n "serde-aux") (v "4.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "1h0ll044h44pj3izjkyvf7vwmk0jvcynkgnjsqc0qpm7i7yv76f5") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-4.2.0 (c (n "serde-aux") (v "4.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "0fdf1hdv85ghlfcil6ynl3npbbparmlx3ynn3c0wz7bgxfvy3py3") (f (quote (("default" "chrono"))))))

(define-public crate-serde-aux-4.3.1 (c (n "serde-aux") (v "4.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 2)))) (h "165lhmzy43cz31i5731ipxq9ks6y5s10i2vwd5c1ddyxxdiblkhq") (f (quote (("default" "chrono")))) (r "1.57")))

(define-public crate-serde-aux-4.4.0 (c (n "serde-aux") (v "4.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r ">=0.10") (d #t) (k 2)))) (h "0r9710f5iwd0i1ck6gwarms6kxvi36h3aiig1kakm7qj3i84hqx8") (f (quote (("default" "chrono")))) (r "1.57")))

(define-public crate-serde-aux-4.5.0 (c (n "serde-aux") (v "4.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r ">=0.10") (d #t) (k 2)))) (h "158w68035kzy0h202bdlln3s792is3a12cqyw496bl39lkxqnbhd") (f (quote (("default" "chrono")))) (r "1.57")))

