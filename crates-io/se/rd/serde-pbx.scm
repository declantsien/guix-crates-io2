(define-module (crates-io se rd serde-pbx) #:use-module (crates-io))

(define-public crate-serde-pbx-0.1.0 (c (n "serde-pbx") (v "0.1.0") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1xx1ssg4kyh0m165m8rdk0zz27vf2mf2js1xph4sl3v7vr53y9d9")))

