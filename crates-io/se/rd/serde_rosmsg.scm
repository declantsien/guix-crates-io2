(define-module (crates-io se rd serde_rosmsg) #:use-module (crates-io))

(define-public crate-serde_rosmsg-0.1.0 (c (n "serde_rosmsg") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "09vjxgdh3ndcpsrm9wjmpb8m6jrzxz8g3bi7bxlh7pcdh6i0w5w5")))

(define-public crate-serde_rosmsg-0.2.0 (c (n "serde_rosmsg") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)))) (h "101nk52n5fvzb6crjgdf72jlzhwkgp3qg8a8b4myhw7zic6d4mcy")))

