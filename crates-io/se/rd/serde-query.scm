(define-module (crates-io se rd serde-query) #:use-module (crates-io))

(define-public crate-serde-query-0.1.0 (c (n "serde-query") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "1pcddkpjbq70dw2f6nl1f1bpcqrzp9x3wwn43yx9m460kiizz77z") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.1.1 (c (n "serde-query") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "0qmbw8b71p8nb8nf3868rc3fk31fb0ag97bw5v40x3v34s3xyxmx") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.1.2 (c (n "serde-query") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "ureq") (r "^1.4.0") (d #t) (k 2)))) (h "0832wvmnlysakgp0rf3f78gysb0xj380vg7bl1acawjhr2d6v554") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.1.3 (c (n "serde-query") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "ureq") (r "^1.4.0") (d #t) (k 2)))) (h "1zd487n7pvh8q1x45hxy8vrngv9dqfd73f7h7lcvqq3q2h6qlipz") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.1.4 (c (n "serde-query") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)) (d (n "ureq") (r "^1.4.0") (d #t) (k 2)))) (h "1y262iq1cgsp5fgpdj48bf8mm1d0mkaqyrmnfmbpdpn295994ylb") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.1.5 (c (n "serde-query") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)) (d (n "ureq") (r "^1.4.0") (d #t) (k 2)))) (h "1yp1ma4ibihzn4snl0g92fz8b6l4gq7nvhy35ipqsb84b4ip5ll8") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

(define-public crate-serde-query-0.2.0 (c (n "serde-query") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde-query-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "k9") (r "^0.11.6") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)) (d (n "ureq") (r "^1.4.0") (d #t) (k 2)))) (h "0pxgmqlfq374pgryz0pj45wl89v4q7ja67cs2lgriwdnac26xkzc") (f (quote (("derive" "serde-query-derive") ("default" "derive"))))))

