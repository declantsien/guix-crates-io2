(define-module (crates-io se rd serde-xml-any) #:use-module (crates-io))

(define-public crate-serde-xml-any-0.0.3 (c (n "serde-xml-any") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0ssqwyi5lj1jq2a3ln7j5ik2jz7gyz77i6yivc4yh11i7s5950g2")))

