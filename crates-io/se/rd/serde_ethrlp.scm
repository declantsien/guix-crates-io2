(define-module (crates-io se rd serde_ethrlp) #:use-module (crates-io))

(define-public crate-serde_ethrlp-0.2.2 (c (n "serde_ethrlp") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1y60ffvxn3myh080605g7fymbihd7mn1lc4q0vwxqpadq69mwyhj")))

