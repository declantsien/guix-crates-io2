(define-module (crates-io se rd serde06) #:use-module (crates-io))

(define-public crate-serde06-0.6.0 (c (n "serde06") (v "0.6.0") (d (list (d (n "serde") (r "^0.6") (k 0)))) (h "0ns5maiy5np88fllvcyp6sdww50678hld6dxrgyxqqrys3wl4l9a") (f (quote (("num-rational" "serde/num-rational") ("num-impls" "serde/num-impls") ("num-complex" "serde/num-complex") ("num-bigint" "serde/num-bigint") ("nightly-testing" "serde/nightly-testing") ("nightly" "serde/nightly"))))))

