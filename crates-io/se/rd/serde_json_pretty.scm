(define-module (crates-io se rd serde_json_pretty) #:use-module (crates-io))

(define-public crate-serde_json_pretty-0.1.0 (c (n "serde_json_pretty") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19xnim8ckb921zsqd0bzaqkir3gn55c96hid644v9f9bdvf3nxcq")))

