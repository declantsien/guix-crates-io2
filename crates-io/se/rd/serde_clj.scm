(define-module (crates-io se rd serde_clj) #:use-module (crates-io))

(define-public crate-serde_clj-0.1.0 (c (n "serde_clj") (v "0.1.0") (d (list (d (n "jni") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0m9d984x6ph4b9ma01f6sinni7812nabkc6f5xm7alykhraxcmw1")))

(define-public crate-serde_clj-0.1.1 (c (n "serde_clj") (v "0.1.1") (d (list (d (n "jni") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "16xvny5262q6s4s4zpnx5wih9kfdxy04d38dx31pf07294v9yi2w")))

