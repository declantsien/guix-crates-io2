(define-module (crates-io se rd serde_dis) #:use-module (crates-io))

(define-public crate-serde_dis-0.1.1 (c (n "serde_dis") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9pyw8y1am4yhqjb3yb8nk5878c8dgmw9ydbh5ayfzqlsiy3x96")))

(define-public crate-serde_dis-0.1.2 (c (n "serde_dis") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09khza5hj2i2hxv0mwlsgnfaryikffsl9a1zvnsr2nwj1jkc1f05")))

(define-public crate-serde_dis-0.1.3 (c (n "serde_dis") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0c36cclsd96bxs7vjn24xpvcb3ar0jipn5si82dk7dhfsmh55mfj")))

