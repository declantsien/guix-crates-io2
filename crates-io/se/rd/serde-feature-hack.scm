(define-module (crates-io se rd serde-feature-hack) #:use-module (crates-io))

(define-public crate-serde-feature-hack-0.1.0 (c (n "serde-feature-hack") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.75") (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 2)))) (h "1ijcvjl2ba83gsj6m9pgfmp7xr839350w7w06slwrn4a616k752j") (f (quote (("unstable" "serde/unstable") ("std" "serde/std") ("rc" "serde/rc") ("derive" "serde/derive") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde-feature-hack-0.2.0 (c (n "serde-feature-hack") (v "0.2.0") (d (list (d (n "real_serde") (r "^1.0.75") (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.26") (d #t) (k 2)))) (h "0w8w73r3cxxxkcmxpz6sqp4s7vb5igzf9mbclg8h38c1yragzxbg") (f (quote (("unstable" "real_serde/unstable") ("std" "real_serde/std") ("rc" "real_serde/rc") ("derive" "real_serde/derive") ("default" "std") ("alloc" "real_serde/alloc"))))))

(define-public crate-serde-feature-hack-0.2.1 (c (n "serde-feature-hack") (v "0.2.1") (d (list (d (n "real_serde") (r "^1.0.75") (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.26") (d #t) (k 2)))) (h "146jmlkddi7bqwbkgg3wimiqsaysngj9568x193hm4fz8335d6zq") (f (quote (("unstable" "real_serde/unstable") ("std" "real_serde/std") ("rc" "real_serde/rc") ("derive" "real_serde/derive") ("default" "std") ("alloc" "real_serde/alloc"))))))

